/**
 * ECOS_414_0_F_Pruefling.java
 *
 * automatisch erzeugt �ber AdminGui
 * Datum: Thu Mar 01 09:48:58 CET 2018
 */

package com.bmw.cascade.auftrag.prueflinge;

import com.bmw.cascade.*;
import com.bmw.cascade.auftrag.*;
import com.bmw.cascade.auftrag.report.FilterKonfigurationException;
import com.bmw.cascade.pruefstand.*;
import com.bmw.cascade.pruefprozeduren.*;

/*
 * DIESER KOMMENTAR WIRD AUTOMATISCH ERZEUGT UND GELESEN
 * NICHT VER�NDERN!
 * HISTORIE_XML START
<HISTORY version="2">
 <MANUAL_COMMENT date="01.03.2018 09:48" version="414_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
 <COMMENT>Plant spezific Jobs: Block TEST_Handterminal angelegt; neue Kopplung CASACDE MDEs html</COMMENT>
 </MANUAL_COMMENT>
 <MANUAL_COMMENT date="20.02.2018 10:18" version="413_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs: ZIGARETTENANZ�NDER_MITTELKONSOLE_FOND_MITTE_VORHANDEN Varianteninfo um NIO Vorbelegung erweitert / falscher Aufruf bei Fahrzeugen ohne Zigarettenanz�nder hinten Mitte; IF Else Abfrage greift nicht richtig;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="14.02.2018 08:56" version="412_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Kommentare �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="14.02.2018 08:53" version="411_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Kommentare �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="07.02.2018 10:51" version="410_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spezific attributes:
Zeitkriterien f�r Ruhestrommessung von 03/2018 auf 07/2018 ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="30.01.2018 11:05" version="409_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Block MECH TUERSCHLOSSPRUEFUNG / MECH KEY LOCK CHECK aus FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="30.01.2018 10:55" version="408_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>�nderungen FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="26.01.2018 08:56" version="407_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Zeitkriterien f�r L6 Ruhestrom ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.01.2018 10:36" version="406_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs: Im Block 12V Steckdosen, Attribute f�r WF_STECKDOSE_MITTELKONSOLE_FOND_RECHTS ge�ndert; LCI G11 G12 bekommt an der Stelle die USB Dual Ladebuchsen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="07.12.2017 08:08" version="405_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spezific Jobs:

Block: ECOS_Beipack_pruefen hinzugef�gt; tempor�r; bis Lesestation Inline funktioniert ab 2018 Aprl Mai;

Block: China Airbaglabel entfernt / wird nicht mehr ben�tigt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="06.11.2017 12:56" version="404_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>GLOBAL:
SCAN_AIRBAG_LABEL modified (Sync V56 Leipzig)
Anpassung an neuen Barcode;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="11.10.2017 10:56" version="403_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant W2 Attributes:
Zeitkriterien Ruhestrommessung auf 03/2018 ge�ndert; Typen F0X bereinigt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="09.10.2017 08:13" version="402_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>W2 PLANT SPECIFIC JOBS:
LUFTAUSTROEMER_MECH Pr�fung auch auf LK erweitert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="27.09.2017 09:04" version="401_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs: Attribute / Kommentare �bernommen aus FIZ;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="27.09.2017 08:55" version="400_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Attribute 12V Steckdosen und Beleuchtung heckklappe angepasst F9 gegen F90 / F91 ersetzt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="27.09.2017 08:34" version="399_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Attribute aus FIZ �bernommen;
Attribute 12V Steckdosen aus FIZ �bernommen;
zwischenspeichern;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="12.09.2017 10:40" version="398_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spezific Attributes:
G14 / G15 / G16 Vorbereitung Ruhestrommessung hinzugef�gt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="17.08.2017 12:33" version="397_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs:
Text bei WerkerAnweisung: von WQ auf WA ge�ndert; Bitte im FIZ nachziehen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="17.08.2017 09:41" version="396_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Kommentare / neue Varianten FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="17.08.2017 09:37" version="395_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>New existence (F4x)

- WF_FUEL_FILLER_FLAP_CLOSED
- WF_FUEL_FILLER_FLAP_OPEN</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="27.07.2017 12:49" version="394_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs: 
G32 Attribute Steckdosen / Zig.Anz�nder bereinigt; G32 bekommt keine Executive Lounge Fondkonsole (nicht SA4F5)</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="24.07.2017 11:12" version="393_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs: WF_STECKDOSE_MITTELKONSOLE_HINTEN_MITTE reihenfolge SAs an FIZ angepasst;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="11.07.2017 12:54" version="392_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs: Attribute aus FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="11.07.2017 12:36" version="391_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs: WF_STECKDOSE_MITTELKONSOLE_HINTEN_LINKS bei G32 und nicht SA4F5 Executive Loung Fondkonsole ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="10.07.2017 12:19" version="390_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global jobs: Steckdose Mittelkonsole hinten links Attribute ge�ndert G32 hat trotz Raucherpaket SA 441 hinten nur Steckdosne verbaut, keinen Zig.Anz�nder hi links (!); Bitte im FIZ �bernehmen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="10.07.2017 10:02" version="389_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>�nderungen FIZ �bernommen; G32 hinzugef�gt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="10.07.2017 09:56" version="388_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>�nderungen Steckdosen FIZ �bernommen; G32 executive lounge fondkonsole hinzugef�gt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="10.07.2017 09:28" version="387_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>aktuellen Stand mit FIZ abgeglichen, zwischenspeichern;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="10.07.2017 08:32" version="386_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Aktuelle �nderungen FIZ �bernommen; zwischenspeicher;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="07.07.2017 11:00" version="385_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>aktueller Stand FIZ �bernommmen; zwischenspeichern;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="07.07.2017 10:55" version="384_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Aktueller Stand FIZ �bernommen; zwischenspeichern;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="07.07.2017 10:51" version="383_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>aktueller Stand FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="07.07.2017 10:48" version="382_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Aktueller Stand FIZ �bernommen; zwischenspeichern;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="07.07.2017 09:58" version="381_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Aktueller Stand FIZ �bernommen; zwischenspeichern;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="20.06.2017 09:26" version="380_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>plant specific attributes:
Zeitkriterien f�r Ruhestrommessung von 07/2017 auf 11/2017 ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.04.2017 13:46" version="379_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>HWT T�rknopfpr�fung BFs und FAs verk�rzt; Anzeige Handterminal;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.04.2017 10:09" version="378_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spezific Jobs:
Auftrennung Sichtpr�fung T�rkn�pfe auf Fahrer und Beifahrerseite; get�nte Scheiben lassen keine zuverl�assige Pr�fung zu;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="21.03.2017 12:55" version="377_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Attributes:
Text�nderung bei MSA-Taster durchgef�hrt; Texte sind zu �hnlich zw. Hybrid / Alpina und Serie; MA verwechseln die pr�fungen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="16.03.2017 09:12" version="376_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>�nderungen FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="09.03.2017 08:11" version="375_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>ECOS existence always TRUE</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="21.02.2017 10:48" version="374_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant Spezific Jobs:
WF_MAL_DECKEL nur f�r 5er Derivate in H52 (R�cksprache mit Schmid Markus; prozessbedingt);</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="20.02.2017 08:36" version="373_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs:
WF_STECKDOSE_GEPAECKRAUM Attribute bei G31 ge�ndert;
NICHT bei Beh�rdenfahrzeuge SA146</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="14.02.2017 11:15" version="372_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs:
Block SITZLEHNENVERSTELLUNG HINTEN hinzugef�gt;
G32 mit SA 461 hat elektr. Lehnenverstellung hinten aber kein SG / nicht diagnosef�hig (Funktionspr�fung notwendig);

Bitte im FIZ �bernehmen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="07.02.2017 14:04" version="371_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spezific Attributes:
Zeitkriterium f�r Ruhestrommessung von 03/2017 auf 07/2017 ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="02.02.2017 10:08" version="370_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs:
Derivate Luftaustr�mer aus FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="02.02.2017 10:03" version="369_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs: 
Umf�nge aus Block 12V Steckdosen aus FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.01.2017 13:04" version="368_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>F49 und F46 Lehnenfernentriegelung �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.01.2017 12:58" version="367_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>ZIG Anz�nder hi re G07 aus FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.01.2017 12:56" version="366_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Attribute Bel Heckklappe aus FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="20.01.2017 09:08" version="365_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs: Attribute G11/12 WF_STECKDOSE_GEPAECKRAUM ge�ndert; 
ab 07/2017 enf�llt bei China SA8AA diese Steckdose im Kofferraum; Bitte im FIZ nachziehen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.01.2017 08:21" version="364_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Existenzen vom FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.01.2017 08:14" version="363_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>- Modify WF_AIR_VENT_A_COLUMN_DRIVERSIDE und WF_AIR_VENT_A_COLUMN_PASSENGER_SIDE (Add F9x immer)</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="17.01.2017 09:32" version="362_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spezific Jobs:
T�rknopfpr�fung bei allen 35up-Derivaten; PQM: 195230619</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="19.12.2016 08:52" version="361_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Attribute bei WF_BEL_LUFTAUSTROEMER_A_SAEULE ge�ndert; F90 hat immer Serie 2,5 Zonen Klima; Bitte im FIZ nachziehen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="24.11.2016 13:05" version="360_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Globale Attribute: 
G31 hat nur ein Heckklappen Licht; Bitte im FIZ �bernehmen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.11.2016 12:17" version="359_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Baureihen F97 und F98 �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.11.2016 12:11" version="358_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Abfragen Steckdoesen FOND zu HINTEN �bernommen; zwischenspeichern;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.11.2016 11:59" version="357_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Beleuchtung Ein und Beleuchtung Ablagefach Mittelkonsole hinzugef�gt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.11.2016 11:18" version="356_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>WF_KANN_FENSTER_INIT und WF_SCHIEBEDACH_SCHLIESST �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.11.2016 11:16" version="355_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Block Zigarettenanz�nder �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.11.2016 11:13" version="354_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Block Anweisung vom FIZ �bernommen;
Block Laufzeitvariablen gel�scht;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.11.2016 11:10" version="353_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>- Add RR31 to Existenz
- Add F97 (X3 M) and F98 (X4 M)</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.10.2016 11:10" version="352_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs; 
bei zus�tzlichen 12V-Steckdosen ebenfalls Ausschlu� SA 146 Beh�rdenfahrzeug hinzugef�gt; Beh�rdenfahrzeug keine MiKo verbaut;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.10.2016 10:39" version="351_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs:
Werker Fragen ATTRIBUTE ge�ndert SA138 in SA146 Beh�rdenfahrzueg ge�ndert; SA 146 hinzugef�gt; 
Beh�rdenfahrzeuge haben keine MiKo verbaut;
Bitte im FIZ �bernehmen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="20.10.2016 09:26" version="350_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs:
Attribute f�r WF_STECKDOSE_MITTELKONSOLE_FOND_LINKS ge�ndert; Anpassung f�r G32; Bitte im FIZ �bernehmen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.10.2016 11:13" version="349_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>plant spezific Attributes; Zeitkriterium f�r Ruhestrommessung von 11/2016 auf 3/2017 ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.09.2016 13:26" version="348_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>e-mail Verteiler �bernommen; Derviate G05 und G38 angepasst;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="16.09.2016 08:05" version="347_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spezific Jobs:
WF_MAL_DECKEL hinzugef�gt; RiFi TP-5 ergab keine Wiederentdeckungswahrscheinlichkeit; Lehnhard Alexander; TVG: LK, 0.0, K 5116 007 15R A 01</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="08.09.2016 07:34" version="346_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs:
12V Steckdose MiKo und 12V Steckdose Kofferraum G3X Auschluss Beh�rdenfahrzeuge; Bitte im FIZ nachziehen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="05.09.2016 12:37" version="345_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>12V Steckdosen:
bei F90 / G30 und G31 ist die 12V Steckdose MAL Serie ( keine SA 575 Verkn�pfung)</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.07.2016 07:28" version="344_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global attributes:
G31 bei Fragetexte Beleuchtung Heckklappe entfernt; hat nur eine Leuchte in der HKL mittig;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="20.07.2016 12:49" version="343_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs:
Zigarettenanz�nder Mitelkonsole Fond links gibt es beim G32 auch nicht; (trotzdem stimmt die PUSY noch nicht)</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="19.07.2016 09:23" version="342_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Attributes: Ruhestrom nicht bei SX 00DL Datenlogger</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="19.07.2016 09:16" version="341_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>W2 Attributes:
keine Ruhestrommessung wenn Datenlogger SX 00DL verbaut;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.07.2016 09:56" version="340_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Varianteninfo Linke Steckdose negiert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.07.2016 09:38" version="339_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Varinten 12V Steckdosen aus T-Version �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="14.07.2016 08:31" version="336_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs: WQ_FZG_VERLASSEN_FAHRERSEITE HINTEN bel Einstiegsleisten G30 und G31 hinzugef�gt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="12.07.2016 08:54" version="335_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs:
WQ_FZG_VERLASSEN_FAHRERSEITE_HINTEN um Pr�fung bel Einstiegsleisten erweitert; G11 G12;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="21.06.2016 08:14" version="334_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Zeitkriterien f�r Ruhestrommessung von 7/2016 auf 11/2016 ge�ndert; Einsatz neue I-Stufe 7/2016;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="16.06.2016 11:28" version="333_0_F" author="Q108678 / Schreieder Hubert - TI-542">
  <COMMENT>GLOBAL ATTRIBUTES/FRAGETEXTE:
G12 Alpina US bekommt keine MSA</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="06.06.2016 08:56" version="332_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs: WERKER FRAGEN
G30 oder G31 bekommen bei SA 575 und SA 6FH Fondentertainment immer zwei Steckdosen; bei SA 441 gibt es beim G30 und G31 im Fond keinen ZigAnz�nder;

- added following test steps for SP2018
	==&gt; WD_MECH_KEY_DRIVER_DOOR_UNLOCKED
	==&gt; WD_MECH_KEY_DRIVER_DOOR_LOCKED
	==&gt; WC_TAKE_MECH_KEY_AND_REMOTE
	==&gt; WC_PLACE_MECH_KEY_AND_REMOTE
	==&gt; WC_REMOVE_KEY_LOCK_COVER
	==&gt; WC_FIX_KEY_LOCK_COVER

- G30/ F90 Attribute Steckdose Fond Mitte ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.05.2016 13:17" version="331_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs: WERKER FRAGEN
G30 oder G31 bekommen bei SA 575 und SA 6FH Fondentertainment immer zwei Steckdosen; bei SA 441 gibt es beim G30 und G31 im Fond keinen ZigAnz�nder;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.04.2016 09:12" version="330_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>G31 wegen zu vieler problem und zu langer Pr�fdauer vorerts aus Inline Ruhestrommessung entfernt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="28.04.2016 11:08" version="329_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>35up:
- modified test steps for 12V-Powersockets and Cigarette Lighter

Aktuellen Stand FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.03.2016 12:40" version="328_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Attribute: 
G31 hat nur eine Leuchte in der Hecklappe;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="17.03.2016 14:18" version="327_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spezific Jobs:
Luftaustr�mer
Text ge�ndert Mechanik und Luft IO</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="14.03.2016 12:04" version="326_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>WF_TUERKNOPFPRUEFUNG nur noch G11 G12 (G3X hat das Problem nicht) e-mail Deistler Wolfgang;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="24.02.2016 10:44" version="325_0_F" author="Q185125 / Mueller Alexander - TI-542">
  <COMMENT>added SA06NV to WCA - WIRELESS CHARGING ABLAGE

gAMS:  AI1742</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.02.2016 09:28" version="324_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Zeitkriterien f�r Ruhestrompr�fung von 3/2016 auf 7/2016 ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="15.02.2016 07:50" version="323_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spezific attributes:
Wegen IBS Fehler Ruhestrommessungeb bei G30 entfernt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="02.02.2016 07:40" version="322_0_F" author="Q108678 / Schreieder Hubert - TI-542">
  <COMMENT>F9x integriert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="20.01.2016 10:58" version="321_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>-&gt; modified test step WQ_SLEEP_CURRENT_MEASURED_BY_CURRENT_PROBE

-&gt; Removed warnings and syntax errors in case of new CASCADE-version

-&gt; added WF for secret box light for RR11/RR12

-&gt; deleted block for glove box light for RR11/RR12

-&gt; added siphiwe.mathebula@bmw.co.za to contact list plant 9

-&gt; updated contact list plant 34</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="26.11.2015 07:50" version="320_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>added test step WQ_FZG_VERLASSEN_FAHRERSEITE_HINTEN</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="24.11.2015 08:39" version="319_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>aktuellen Stand FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="19.11.2015 14:01" version="318_0_F" author="Q163569 / Karl Gerhard - TI-542 Admin CASCADE">
  <COMMENT>Attribut bei &quot;Vorserie&quot; den F90 hinzu - der ganze Ablauf bzgl. &quot;Vorserie&quot; sollte ge�ndert werden und auf I-Stufe &lt;500 abgebragt werden</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="19.11.2015 12:12" version="317_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Attribute:
F90 G30 M5 hinzugef�gt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.10.2015 14:29" version="316_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>STECKDOSE FOND MITTE nicht bei SA 575 (zus�tzliche 12V Steckdosen); dann li und re</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.10.2015 14:00" version="315_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>VariantenInfo li und re Steckdosen bei G30 mit SA 575 zus�tzl. 12V Steckdosen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.10.2015 09:15" version="314_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs:
ZIGARETTENANZUENDER_MITTELKONSOLE_FOND_LINKS_VORHANDEN nicht bei G30;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="19.10.2015 10:33" version="313_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Attribute:
Zeitkriterien f�r Vorserie Ruhestrommessungen auf 3/2016 ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="09.10.2015 10:49" version="312_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Workaround MSA taster bei G11 / 12 wieder r�ckg�ngig;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="07.10.2015 15:22" version="311_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>- added test steps for fuel filler flap</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="01.10.2015 12:47" version="309_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs:
WF_BRIGHTNESS_ADJ_MAX hinzugef�gt; IO bzw NIO Eingabe im ECOS m�glich wegen tech. def. Drehregler;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="14.09.2015 09:39" version="308_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs:
MSA Tster:
Wegen Workaround FS Ausblendung Abfrage nur beim PHEV; Workaround l�uft �ber MSA-Taster / LED Abfrage im BDC Master;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="07.09.2015 09:54" version="307_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>- moved test step WQ_CHECK_LIGHTS from Plant Spec Spartanburg to the Global Block and renamed to WC_SWITCH_OFF_LIGHT
- replaced test step ECOS.STATUS_ANLAGENTYP_NACHARBEIT  by PU_STEUERUNG.STATUS_ANLAGENTYP_NACHARBEIT 
- moved test step WF_CHECK_LOWER_TAILGATE from Plant Spec Spartanburg to the Global Block
- deleted not used test steps
 
- moved all waiting times to W2 plant specific (Bereinigung PUs notwendig)

- added XNF to the existence criteria</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="03.09.2015 10:39" version="306_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant specific Jobs:
HKL_schliessen_bei_TOP_VIEW hinzugef�gt; bei L6 mu� HKL geschlossen werden;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="26.08.2015 09:33" version="305_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant specific Jobs:
Workaround G11/12 mit Pseudo SX 0K5;
Beilegen DISPLAY-Schl�ssel vorerst beim H�ndler;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.08.2015 10:02" version="304_0_F" author="Q083674 / Alois Lehner - TI-542">
  <COMMENT>SCAN_SKID_NR: Parameter MAX entfernt</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.08.2015 09:39" version="303_0_F" author="Q083674 / Alois Lehner - TI-542">
  <COMMENT>SCAN_SKID_NR: MIN/MAX Parameter erg�nzt</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="10.08.2015 07:59" version="302_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant specific Jobs: 
SCAN_SKID_NR nur bei G11 / G12;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="07.08.2015 10:47" version="301_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spezific Jobs:
Block PLT SKID PRUEFEN hinzugef�gt;
Pr�fschritt SCAN_SKID_NR hinzugef�gt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="06.08.2015 08:11" version="300_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spezific Jobs:
Tankdeckelpr�fung; WQ_TUEREN_SCHLIESSEN; Probleme wenn FFB noch im Innenraum erkannt wird;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="05.08.2015 08:32" version="299_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spezific Jobs:
WF_LUFTAUSSTR�MER_MECH_B_SAULE_FAS und BFS hinzugef�gt;
Q-Themen: def. / Lammellen ausgeh�ngt / gebrochen ...;
eventuell entfernen wir das wieder; irgendwann;... TVG Weigert Franz / nicht in ECOS PPG;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.07.2015 10:31" version="298_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>WQ_ECOS_KLIMA_AUS und WQ_HELLIGKEITSREGLER_MAX vom plant spec Block in den globalen Block verschoben;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="27.07.2015 08:41" version="297_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs: Werker-ident und Werker-Quittung �bernommen; Zusammenf�hrung mit ECOS kleine Baureihen ist erfolgt;

@Riedel Marcel: jetzt gilts: L6 MUSS in die Existenzen und der Pr�fschritt WQ_BRIGHTNESS_ADJ_MAX fehlt im FIZ noch;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="24.07.2015 08:09" version="296_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Block Werkerinterface an FIZ angepasst;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.07.2015 10:42" version="295_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Attribute / Texte vom FIZ �bernommen;
WerkerFragen vom FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="22.07.2015 12:43" version="294_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Aktueller Stand FIZ �bernommen; bis auf Werker-interface; Fragetexte Attribute m�ssen noch angepasst werden;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="22.07.2015 11:14" version="293_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Zusammenf�hren ECOS Pr�flinge kleine Baureihen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="21.07.2015 12:45" version="292_0_T" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>T-Version erstellt; LK Derviate hinzugef�gt; Test Zusamenf�hrung ECOS Pr�flinge</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="16.07.2015 08:32" version="291_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Zusammenf�hrung ECOS Pr�flingen:
Plant spezific Jobs:
um LK Derviate erweitert;
==&gt; W2 Block OK</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="16.07.2015 08:26" version="290_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Zusammenf�hrung mit ECOS kleine Baureihen begonnen;
erster Schritt: Abgleich beider ECOS-Pr�flinge im Werk DGF (L6 schon lange aktiv)
zweiter Schritt: Abgleich mit ECOS im FIZ;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="15.07.2015 09:03" version="289_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant specific Jobs:
SICHTPRUEFUNG SCHLOSSNUSS L&amp; hinzugef�gt; Entfall Hallsensoren bei BASIS T�ren 
==&gt; Beschlu� Werk DGF: Alle Baureihen erhalten eine Sichtpr�fung analog zur LK und LG;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="15.07.2015 08:18" version="288_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant specific Jobs:
ARMLEHNENHEIZUNG_FAHS_VORHANDEN hinzugef�gt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="09.07.2015 13:30" version="287_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant Spezific Jobs:
ARMLEHNENHEIZUNG_FAS_VORHANDEN hinzugef�gt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="24.06.2015 08:05" version="286_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT></COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="24.06.2015 08:04" version="285_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spezific Jobs: WQ_ECOS_KLIMA_AUS hinzugef�gt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.06.2015 13:14" version="284_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Attribute: Alle DERVIVATE / VORSERIE von 07 auf 11/2015 ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="20.05.2015 09:45" version="283_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant specific Jobs:
ECOS_KLIMA_AUS hinzugef�gt; Ruhestrommessung IHKA schl�ft nicht ein;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="19.05.2015 08:55" version="282_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs:
Korrektur Captains_Chair ==&gt; Executive Loung Fondkonsole</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="05.05.2015 10:12" version="281_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs:
HWT WF_COOL_BOX_ILLUMINATION_CHECK ge�ndert; Pr�fung Einschalten K�hlox;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="27.04.2015 11:00" version="280_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs:
NICHT SA 441 bei WF_STECKDOSE_MITTELKONSOLE_FOND_MITTE zu SA 4F5 Capatins Chair hinzugef�gt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="24.04.2015 09:38" version="279_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>added plant specific W0 test steps for system testing

OPERATOR CONFIRMATION:
-&gt; moved Block: START PRUEFUNG / START TEST from Operator question to operator confirmation

added test step WF_COOL_BOX_BUTTON_ILLUMINATION_CHECK

modified plant 0 test steps

Plant Specific Jobs:
berinigt nach �bernahme in Global Jobs:</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="16.04.2015 08:44" version="278_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spezific Jobs: W0 �bernommen; R�cksprungfunktion</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="16.04.2015 08:41" version="277_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>RR �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="10.04.2015 12:07" version="276_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs 35up:
Zigarrenanz�nder bei Captains chair nur mit SA 441 Raucherpacket;
==&gt; Bitte im FIZ nachziehen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.03.2015 08:45" version="275_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>STATUS_DESICION_RUHESTROMMESSUNG: PU_STEUERUNG.VAR_ECOS_LINIE hinzugef�gt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="16.03.2015 09:54" version="274_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spezific Jobs:
Varianten Tankdeckel ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="16.03.2015 09:49" version="273_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spezific Jobs:
35up Tankdeckelpr�fung hinzugef�gt; Q-Thema Drehriegel Tankdeckel NIO;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="16.03.2015 09:34" version="272_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant Spezific Jobs: 
Abfrage T�rkn�pfe unten / oben hinzugef�gt; Bei T�rschlosspr�fung notwendig; Q-Themen Gest�nge T�rkn�pfe;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="26.02.2015 09:11" version="271_0_F" author="Q163569 / Karl Gerhard - TI-542 Admin CASCADE">
  <COMMENT>bei dem W2 Attribut VORSERIE die Baureihen G30, G31, G32 hinzu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="17.02.2015 10:40" version="270_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Zeitkriterien VORSERIE Ruhestrommessung ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.02.2015 12:17" version="269_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>W2 Plant Spezific Jobs:
Fahrpedalmodul; SKI und SNOWBOARDTASCHE und Prozessbedingte Abfragen vom VERSCHIENEDES Pr�fling �bernommen; Auslauf;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="03.02.2015 09:14" version="268_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>- deleted SA captains chair from test step WF_FOND_POWER_SOCKET_CENTER_CONSOLE_MIDDLE

- added test steps:
	=&gt; STATUS_VAR_WF
	=&gt; SET_VAR_WF_IO
	=&gt; SET_VAR_WF_NIO
	=&gt; GET_VAR_WF

-added teststep WF_STECKDOSE_MITTELKONSOLE_ABLAGEFACH_FOND for RR6</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="22.01.2015 14:43" version="267_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs: WF_FACKELHALTER_JAPAN hinzugef�gt; Bitte im FIZ aus LK �bernehmen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="15.01.2015 08:15" version="266_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Global Jobs:
Pr�fschritte zur Beleuchtung Drehsteller Luftgitter hinzugef�gt; (Verkabelung im BDC)</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="17.12.2014 10:34" version="265_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>added test steps for STROMMESSZANGE / CURRENT PROBE
added test step WQ_FAHRZEUG_UND_ICOM_WECKEN</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="10.12.2014 08:18" version="264_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant specific Jobs:
WF_TUER_BFS_SCHLOSS_FALSCH_VERBAUT hinzugef�gt; Q-Massanhame; Sequenzanlieferung neue STVM problematisch;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="28.11.2014 10:47" version="263_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>- added test step WF_FOND_POWER_SOCKET_CENTER_CONSOLE_MIDDLE</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="27.10.2014 12:32" version="262_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>- added test steps 
WQ_CLOSE_ALL_WINDOWS; 
WQ_WAKE_UP_VEHICLE_AND_ICOM; 
WQ_NO_SLEEP_CURRENT_MEASUREMENT

- test step WF_TAILGATE LIGHT_VISUAL_CHECK
	==&gt;added !G30 to existence criteria</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="27.10.2014 08:36" version="261_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spec. attributes: 
Ruhestrompr�fung F0x und F1x und auf 3/2015 ge�ndert: Start 11/2014</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="17.09.2014 12:25" version="260_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>global jobs: aktueller Stand FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="16.09.2014 09:53" version="259_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Kommentare ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.07.2014 09:26" version="258_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Aktueller Stand FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="14.07.2014 13:31" version="257_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>aktueller Stand FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="26.06.2014 12:45" version="256_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Motor N47 um B47 erweitert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.06.2014 12:58" version="255_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Steuerung Vorserie RUHESTROM von 07/2014 auf 11/2014 ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="26.05.2014 10:35" version="254_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>�nderungen FIZ �bernommen; Plant specific bereinigt; Werker; Scan Airbaglabel / Powerdown;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.04.2014 11:08" version="253_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant specific Jobs: HWT CHINA Airbaglabel ge�ndert; HWS CHINA verbaut? Oder B-S�ule?; Vorbereitung Auslauf Pr�fung im ECOS;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.04.2014 09:43" version="252_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Aktueller Stand 35up aus FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="08.04.2014 14:09" version="251_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>HWS China ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="01.04.2014 11:24" version="250_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Aktueller Stand aus FIZ �bernommen;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="01.04.2014 11:21" version="249_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Attribute werkspezifisch f�r G11 / G12 ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="28.03.2014 09:09" version="248_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant specific Jobs: HWT China Airbag Label ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="27.03.2014 08:28" version="247_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>T-Pr�fling nach Test auf Prod umgestellt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="26.03.2014 10:35" version="246_0_T" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>test</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="26.03.2014 10:27" version="245_0_T" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant Spezific Jobs: Vorbereitet f�r Integration ECOSkleineBaureihen Pr�fling;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.03.2014 12:58" version="244_0_T" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Abschlu� Bereinigung / Test</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.03.2014 12:51" version="243_0_T" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Zwischenspeichern</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.03.2014 12:45" version="242_0_T" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Zwischenspeichern</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.03.2014 12:33" version="241_0_T" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Zwischenspeichern</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.03.2014 12:31" version="240_0_T" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Zwischenspeichern</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.03.2014 12:23" version="239_0_T" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Zwischenspeichern</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.03.2014 10:29" version="238_0_T" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Plant spezific Jobs aus Werks-ECOS Pr�fling zugeordnet;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.03.2014 10:26" version="237_0_T" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>T-Version erstellt; vorbereitet f�r Zusammenf�hrung mit FIZ ECOS;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="24.03.2014 08:13" version="236_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>pr�fungen HWS Airbag f�r US und China hinzugef�gt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.02.2014 08:19" version="235_0_F" author="Q061588 / Kronbeck Manfred - TI-542">
  <COMMENT>Attribute Ruhestrommessung ge�ndert &quot;03/2014&quot; auf 07/2014&quot; umgestellt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.01.2014 13:27" version="234_0_F" author="Q061588 / Kronbeck Manfred - TI-532">
  <COMMENT>WF_KUEHLBOX_LED_CHECK HWT ge�ndert: K�hlbox einschalten, leuchtet/blinkt LED?</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="28.11.2013 10:16" version="233_0_F" author="Q061588 / Kronbeck Manfred - TI-532">
  <COMMENT></COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="28.11.2013 10:14" version="232_0_F" author="Q061588 / Kronbeck Manfred - TI-532">
  <COMMENT>Text WF_KUEHLBOX_LED_CHECK ge�ndert; &quot;Leuchten beide LEDs dauernd?&quot;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="31.10.2013 11:48" version="231_0_F" author="Q061588 / Kronbeck Manfred - TI-532">
  <COMMENT>Attribute ge�ndert von 11/2013 auf 03/2014 umgestellt; Powerdown Ruhestrommessung</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="12.07.2013 15:50" version="230_0_F" author="Q061588 / Kronbeck Manfred - TI-532">
  <COMMENT>Attribute: Unterscheidung Vorserie mit SA 6WB entfernt; Ruhestrommessung ENDE;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="01.07.2013 12:36" version="229_0_F" author="Q061588 / Kronbeck Manfred - TI-532">
  <COMMENT>Attribute: Unterscheidung Vorserie mit SA 6WB hinzugef�gt; Ruhestrommessung erf; tempor�r; FZG I-Kombi Hintergrundbeleuchtung schaltet nicht aus;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="17.06.2013 10:14" version="228_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Attribut Vorserie f�r Ruhestrompr�fung auf 11/2013 ge�ndert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.02.2013 07:43" version="227_0_F" author="Q061588 / Kronbeck Manfred - TI-532">
  <COMMENT>Attribute: Zeitkriterien f�r Vorserie / Ruhestrommessung ge�ndert von 03/2013 auf 07/2013 umgestellt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.01.2013 15:23" version="226_0_F" author="Q061588 / Kronbeck Manfred - TI-532">
  <COMMENT>PRUEFSTAND_INFO.VAR_ECOS_LINIE_50 korrigiert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.01.2013 15:03" version="225_0_F" author="Q061588 / Kronbeck Manfred - TI-532">
  <COMMENT>STATUS_DESICION_USB_AUX_IN_H50</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="17.10.2012 15:35" version="224_0_F" author="Q061588 / Kronbeck Manfred - TI-532">
  <COMMENT>Plant specific Jobs: WARTEZEIT_3S_Kl15_Aktivieren hinzugef�gt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="21.09.2012 10:54" version="223_0_F" author="Q061588 / Kronbeck Manfred - TI-532">
  <COMMENT>Pr�fschritt: STEUERN_KLEMME_KL15_AKTIVIEREN_N52 Motoren N47 und N57 hinzugef�gt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="02.08.2012 13:11" version="222_0_F" author="Q061588 / Kronbeck Manfred - TI-532">
  <COMMENT>�nderungen N47 wieder R�ckg�ngig; neue Erkenntnisse Ruhestrom Trigger Dauer ca 5 -6 sek;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="02.08.2012 11:34" version="221_0_F" author="Q061588 / Kronbeck Manfred - TI-532">
  <COMMENT>Im Block Werksumfang FXX STEUERN_KLEMME_KL_15_AKTIVIEREN_N52 Motor N47 hinzugef�gt;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="21.06.2012 08:46" version="220_0_F" author="Q061588 / Kronbeck Manfred - TI-532">
  <COMMENT>Attribute: Zeitkriterien f�r VORSERIE von 7 / 2012 auf 3 / 2013 ge�ndert; Anlauf 3/2012 am 21.06. um 19:00;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="15.03.2012 13:32" version="219_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Pr�fling nur noch f�r F0x/F1x g�ltig; aufger�umt</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="20.02.2012 08:37" version="218_0_F" author="Q061588 / Kronbeck Manfred - TI-532">
  <COMMENT>Attribute: Zeitkriterien f�r VORSERIE von Monat 3 auf 7 ge�ndert;</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.11.2011 11:24" version="217_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>WQ_ZUENDUNG_EIN AWT angepasst</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.09.2011 13:01" version="216_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Vorbereitung f�r Ruhestrommessung Oktober</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="26.07.2011 08:35" version="215_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Vorbereitung f�r Ruhestrommessung F13</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="13.07.2011 12:32" version="214_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Attribut VORSERIE auf gr��er 03/12</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="06.07.2011 08:42" version="213_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Ruhestrommessung f�r M-Modelle wieder aktiviert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="05.07.2011 09:27" version="212_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Ruhestrommessung f�r M-Modelle wieder aktiviert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="20.06.2011 10:06" version="211_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>M-Modelle von Ruhestrommessung ausgeschlossen, wegen ZEWP-Nachlauf f�r 30-45min</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.05.2011 07:45" version="210_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>FR13 wieder f�r Ruhestrompr�fung freigeschaltet</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="16.05.2011 07:41" version="209_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>5s Extra-Wartezeit Power-Down f�r M-Modelle</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="05.04.2011 12:15" version="208_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>WARTEZEIT_5S_N52T</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="05.04.2011 07:41" version="207_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>FR13 aus Ruhestrompr�fung genommen wegen verl�ngerter Auslesezeit des Ruhestromwerts</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.04.2011 12:23" version="206_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>STICHPROBE_RUHESTROM_DURCHFUEHREN DEBUG=TRUE zur Fehlersuche</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.04.2011 07:46" version="205_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Bedingungen f�r Ruhestrompr�fung festgelegt</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.02.2011 08:14" version="204_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Attribut VORSERIE auf 09/11</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="31.01.2011 09:54" version="203_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>F06 dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="27.01.2011 09:26" version="202_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>WF_FH_SCHALTERBLOCK f�r F12 dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.12.2010 11:36" version="201_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Attribut VORSERIE f�r F12/F13 auf &gt;03/11, somit Ruhestrommessung erst wieder ab dieser I-Stufe</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="16.11.2010 07:28" version="200_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Parameter AFTER_ERROR_ONLY_LOCAL bei Ruhestrompr�fung wegen neuer PP</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="12.11.2010 10:21" version="199_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Stichprobe nicht bei N63 wegen zu heissem Motor; Quote auf 65%</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.10.2010 11:27" version="198_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Stichprobe Ruhestrom auf F01-F04 vorbereitet</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.10.2010 10:41" version="197_0_F" author="Q105907 / Falter Armin - TI-532">
  <COMMENT>dauer bei WF_KUEHLBOX_LED_CHECK erh�ht. damit kein timeout w�hrend mittagspause.</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="06.10.2010 13:24" version="196_0_F" author="Q105907 / Falter Armin - TI-532">
  <COMMENT>WF_KUEHLBOX_LED_CHECK und WF_KUEHLBOX_LIGHT_CHECK hinzu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="06.10.2010 08:30" version="195_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Stichprobe Ruhestrom im Oktober nur H50</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="01.10.2010 07:47" version="194_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Ruhestrompr�fung F10 nur H50; etwas aufger�umt</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="26.08.2010 07:52" version="193_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>I-Stufe f�r Vorserie auf &gt; 11-03; Stichprobenaufrufe jetzt auch f�r F10/F11</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="01.07.2010 08:30" version="192_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>F11 Vorserie auf 09/2010</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="12.04.2010 00:00" version="191_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Pseudo-SA auf 0OS ge�ndert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="12.04.2010 00:00" version="190_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>SX001B integriert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="31.03.2010 00:00" version="189_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Vorserie bei F10 auf gr��er 09/10, somit Ruhestrommessung in Serie deaktiv</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="30.03.2010 00:00" version="188_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Attribut VORSERIE nochmals aufgeteilt; Stichprobe nur bei F01-F07 g�ltig; Workaround F11 Ruhestrom dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="02.03.2010 00:00" version="187_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>L6: UI_RUHESTROMMESSUNG auf Punkt 100</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.02.2010 00:00" version="186_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Attribut VORSERIE f�r F01-F07, E6x auf &gt;=09/2010</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.02.2010 00:00" version="185_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>FT Sitzverstellung angepasst</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.02.2010 00:00" version="184_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>WF_SITZLAENGSVERSTELLUNG_FA_F10 und WF_SITZLAENGSVERSTELLUNG_BF_F10 dazu wegen Fahrzeugen ohne Sitzmodulen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="21.01.2010 00:00" version="183_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>STATUS_RUHESTROMMESSUNG und RUHESTROM_NIO f�r L6 dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="12.11.2009 00:00" version="182_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>WF_LEHNENKOPF_FA und WF_LEHNENKOPF_BF tempor�r dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="27.10.2009 00:00" version="181_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>F18: SB_BEIFAHRERSEITE_SITZ_H mit SA0456 als Bedingung</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="08.10.2009 00:00" version="180_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>L6: WF_LADEGERAET_EIN dazu wegen R�cksprungm�glichkeit</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="02.10.2009 00:00" version="179_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>E6x: Batteriehauptschalter nur noch f�r E63/64; Pr�fling aufger�umt</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.09.2009 00:00" version="178_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>L6: F04 mit Langkarosse zu Fussst�tzen dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.09.2009 00:00" version="177_0_F" author="Q108678 / Schreieder Hubert - TI-532">
  <COMMENT>Attribut VORSERIE separat f�r alle Baureihen angelegt</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="31.07.2009 00:00" version="176_0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Attribut VORSERIE auf &gt;=01/10 ge�ndert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="06.07.2009 00:00" version="175_0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Batteriehauptschalter wieder f�r alle E6x</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="26.06.2009 00:00" version="174_0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>WQ_N55_WARTE_3M wieder gel�scht</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="16.06.2009 00:00" version="173_0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>WQ_N55_WARTE_3M f�r F10/11 N55 I405</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.06.2009 00:00" version="172_0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Abfrage Batteriehaupschalter nur noch f�r H52 E63/64</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="28.05.2009 00:00" version="171_0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>E65 raus; E6x unbenutzte Schritte raus; L6 Block IBS_TESTSCHRITTE dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="13.05.2009 00:00" version="170_0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>E6x SX0026 bei Batterie-Hauptschalter raus</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.04.2009 00:00" version="169_0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>WQ_N55_WARTE_3M nur noch bis I470</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.03.2009 00:00" version="168.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>WQ_VERSANDSCHUTZ f�r E60/61 dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="03.03.2009 00:00" version="167.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>WQ_N55_WARTE_3M f�r N55 dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="16.02.2009 00:00" version="166.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>F18-Pr�fschritte wegen CKD dazu; IStufeMonat f�r VORSERIE auf gr��er/gleich 8 gesetzt</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="12.02.2009 00:00" version="165.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Weitere englische Texte dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="10.02.2009 00:00" version="164.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>L6 Englische Texte dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="16.12.2008 00:00" version="163.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Stichprobe dazu; STATUS_DECISION_RUHESTROM und VORSERIE angepasst</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="24.11.2008 00:00" version="162.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>WF_HANDSCHUHFACHBELEUCHTUNG dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="19.11.2008 00:00" version="161.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Pr�fling f�r E65-Auftr�ge deaktiviert; Parameter f�r Ruhestrommessung E60 an Vorgaben Brumma Frank angepasst</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="21.10.2008 00:00" version="160.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Attribut VORSERIE zur Steuerung der Ruhestrommessung dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="20.10.2008 00:00" version="159.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>PP WQ_ZUENDUNG_EIN allgemein g�ltig; Baureihenattribut E65 raus</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="02.10.2008 00:00" version="158.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>PP FALSCHE_FG_NUMMER dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.09.2008 00:00" version="157.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>ZV-Schl�sselpr�fung umgestellt, Hallgeber entf�llt bei E6x zu 03/09 ausser SA302/322/323/US</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="12.09.2008 00:00" version="156.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>WQ_VDC_AKTIVIEREN dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.07.2008 00:00" version="155.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Ruhestrommessung L6 auf 24s erh�ht; WQ_LADEGERAET_EIN dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="17.07.2008 00:00" version="154.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>WF_WINDSCHOTT_BEIGELEGT bei E64 dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="08.07.2008 00:00" version="153.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>WQ_TUER_SCHUTZKAPPEN dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="01.07.2008 00:00" version="152.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>WQ_ACHTUNG_POWER_DOWN dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="19.06.2008 00:00" version="151.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Datenlogger nur noch bei SA909</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="10.06.2008 00:00" version="150.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Grenzwerte Heckscheibenheizung E65 angepasst; FS_LOESCHEN_DMTL f�r F01 dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="30.05.2008 00:00" version="149.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Fussst�tzen und Fussmattenabfrage Fxx dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="28.05.2008 00:00" version="148.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Dauer der Vorruhestrommessung auf 22s erh�ht, um Messung evtl. ohne Wiederholung zu schaffen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="15.05.2008 00:00" version="147.0_F" author="Q108678 / Schreieder Hubert - TD-426">
  <COMMENT>WF_HINWEISSCHILD_DWA bei Fxx dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="17.04.2008 00:00" version="146.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>WQ_ZUENDUNG_EIN f�r L6 dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="08.04.2008 00:00" version="145.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>UI_LADEGERAET_EIN auf UIVoltage ge�ndert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="08.04.2008 00:00" version="144.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>UI_LADEGERAET_EIN auf UIToleranzWait ge�ndert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.04.2008 00:00" version="143.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>UI_LADEGERAET_EIN angepasst</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.04.2008 00:00" version="142.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>UI_LADEGERAET_EIN f�r Fxx dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="01.04.2008 00:00" version="141.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>UI_GEPAECKRAUMLEUCHTE neu parametriert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="21.02.2008 00:00" version="140.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>KISI-Taster und Radiostellung E6x auf Diagnose vorbereitet</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="20.02.2008 00:00" version="139.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Centerlock ge�ndert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="20.02.2008 00:00" version="138.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>MODE=1 bei UI_WISCHER_HECK entfernt</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="19.02.2008 00:00" version="137.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>LUZ wieder bei AR und M-Modellen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.02.2008 00:00" version="136.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>END_TRIGGER_LEVEL bei Frontscheibenwischer auf 3700; LUZ nur noch bei AR ohne SA205</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="31.01.2008 00:00" version="135.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Parametrierung der Blinker E63/64 angepasst; Datenloggerjobs L6 dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="30.01.2008 00:00" version="134.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>PPs KOPFSTUETZE_FOND_FS und KOPFSTUETZE_FOND_BS jetzt g�ltig bei SA261 ohne SA460</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="30.01.2008 00:00" version="133.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Einstiegsleisten E65 ge�ndert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="26.11.2007 00:00" version="132.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Bei PP SCHLUESSEL_ZV FT ge�ndert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="19.11.2007 00:00" version="131.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Texte bei SRA-Ansteuerung ge�ndert, abh�ngig von NIVI; DELAY_1S dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="13.11.2007 00:00" version="130.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Parametrierung Make-Up-Leuchte hinten E65 ge�ndert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.10.2007 00:00" version="129.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Werkerfrage TASCHENLAMPE_US f�r E60/61 US dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="11.10.2007 00:00" version="128.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Oberen Toleranzbereich bei UI_MDA_EIN hochgesetzt wegen Ruhestrom F01</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="27.09.2007 00:00" version="127.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>�berpr�fung Fondmonitor bei SA0XXA deaktiviert, wird erst in MUC verbaut</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="24.09.2007 00:00" version="126.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>SA940 bei Abfrage der beleuchteten Einstiegsleisten raus</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.09.2007 00:00" version="125.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Testbild-Weiss nicht mehr bei SA06VA</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="07.09.2007 00:00" version="124.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>ID-Geberabfrage f�r E6x g�ltig; AWT f�r START_IM_FAHRZEUG ge�ndert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="28.08.2007 00:00" version="123.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Grenzwerte beim Blinker E63/64 angepasst</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="22.08.2007 00:00" version="122.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Fxx in Existenz dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="26.07.2007 00:00" version="121.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>PTC auch bei E63/64 Diesel; Predrive E64 raus</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.07.2007 00:00" version="120.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Tempor�ren Aufruf zur SHZH-Initialisierung H52 dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="19.07.2007 00:00" version="119.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Grenzwerte Blinker E63/64 US 09/07 angepasst wegen h�herer Helligkeit durch andere Codierdaten</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.07.2007 00:00" version="118.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Grenzwerte Gep�ckraumleuchte ge�ndert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="17.07.2007 00:00" version="117.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Mess- und Triggerfilter bei Gep�ckraummessung auf 200ms</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="11.07.2007 00:00" version="116.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Trigger_Level bei fallender Flanke positiv</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="10.07.2007 00:00" version="115.0_F" author="q108678 / Schreieder Hubert - TD-426">
  <COMMENT>Strommessung Gepaeckraum angepasst</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="10.07.2007 00:00" version="114.0_F" author="q108678 / Schreieder - TD-426">
  <COMMENT>Messung Gep�ckraumleuchte ohne status_pp</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="10.07.2007 00:00" version="113.0_F" author="q108678 / Schreieder - TD-426">
  <COMMENT>Messschritte f�r Gep�ckraumleuchte E60 dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="05.07.2007 00:00" version="112.0_F" author="q108678 / Schreieder - TD-426">
  <COMMENT>Werkerfrage ACC-NIVI-BLENDE dazu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.06.2007 00:00" version="111.0_F" author="q108678 / Schreieder - TD-426">
  <COMMENT>Fahrzeuge mit SX0026 bekommen keinen Batteriehauptschalter (Grossversuch)</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="21.06.2007 00:00" version="110.0_F" author="q108678 / Schreieder - TD-426">
  <COMMENT>FSR und Heckwaschd�se bei SA0903/0906 ebenfalls deaktiviert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="21.06.2007 00:00" version="109.0_F" author="q108678 / Schreieder - TD-426">
  <COMMENT>SRA nicht bei SA0903/0906; FPM verschoben auf 09/08</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="14.06.2007 00:00" version="108.0_F" author="q108678 / Schreieder - TD-426">
  <COMMENT>Messgrenzen f�r Blinker E63/64 MJ09/07 angepasst</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="15.05.2007 00:00" version="107.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Anpassung E63/64 Nichtraucherpaket und E65 Einstiegsleisten</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="11.05.2007 00:00" version="106.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>ID CD-Laufwerk optimiert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="09.05.2007 00:00" version="105.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>ID CD-Laufwerk anpassen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="09.05.2007 00:00" version="104.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Pr�fung CD-Laufwerk hinzugef�gt</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="08.05.2007 00:00" version="103.0_F" author="q108678 / Schreieder - TD-426">
  <COMMENT>Text Gebl�se BF</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="08.05.2007 00:00" version="102.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Optimierung Geblaese E65</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="07.05.2007 00:00" version="101.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>PPDB-ID Einstiegsleisten ge�ndert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.05.2007 00:00" version="100.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Einstiegsleisten optimiert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.05.2007 00:00" version="99.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Make-Up hinten E65 optimiert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.05.2007 00:00" version="98.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>EINSTEIGEN_FOND hinzu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.05.2007 00:00" version="97.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Make-Up-Spiegel E65 HINTEN OPTIMIERT</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.05.2007 00:00" version="96.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Einstiegsleisten optimiert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="03.05.2007 00:00" version="95.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>ID bei Wartezeiten hinzugef�gt.</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="24.04.2007 00:00" version="94.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>HWT ZV Pr�fen neu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.04.2007 00:00" version="93.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>STATUS_EMF_EIN hinzugef�gt (Abfrage Ist-Position EMF)</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="11.04.2007 00:00" version="92.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Anpassung Fragetexte E65 Diagnoseschritte</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.03.2007 00:00" version="91.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>ID eingef�gt bei Baureihenabfrage L6</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="27.03.2007 00:00" version="90.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Modellpflege 09/07 als Serienumfang eingetragen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="15.03.2007 00:00" version="89.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Sporttaster ID korrigiert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="12.03.2007 00:00" version="88.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Sporttaster nur bei SA 2TB</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="12.03.2007 00:00" version="87.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Anpassung Gebl�se-Messung E65</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="12.03.2007 00:00" version="86.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Sporttaster hinzugef�gt, diverse nicht mehr ben�tigte Pr�fschritte gel�scht</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="05.03.2007 00:00" version="85.0_F" author="q234455 / PPDB Zaal ANdrea -&gt; Igor Rochal - PPDB Zaal ANdrea -&gt; Igor Rochal">
  <COMMENT>PPDB ID f�r &quot;GEPAECKRAUMLEUCHTE&quot; f�r E61 eingetragen.</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="02.03.2007 00:00" version="84.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>3 Gep�ckraumleuchten bei Touring E61 abfragen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="28.02.2007 00:00" version="83.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>SA 906 und 903 bei SRA und Scheibenwaschd�sen vervollst�ndigt</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="26.02.2007 00:00" version="82.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT></COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="26.02.2007 00:00" version="81.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Innenspiegel optimiert, Beifahrerairbag Werkerfragen gel�scht</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="22.02.2007 00:00" version="80.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>PWG M5 Optimierung</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="22.02.2007 00:00" version="79.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>H2-Taste und Heckrollo E68 angepasst</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="19.02.2007 00:00" version="78.0_F" author="q234455 / PPDB Zaal ANdrea -&gt; Igor Rochal - PPDB Zaal ANdrea -&gt; Igor Rochal">
  <COMMENT>PPDB ID&#39;s eingetragen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="15.02.2007 00:00" version="77.0_F" author="q234455 / PPDB Zaal ANdrea -&gt; Igor Rochal - PPDB Zaal ANdrea -&gt; Igor Rochal">
  <COMMENT>PPDB ID&#39;s eingetragen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="14.02.2007 00:00" version="76.0_F" author="q234455 / PPDB Zaal ANdrea -&gt; Igor Rochal - PPDB Zaal ANdrea -&gt; Igor Rochal">
  <COMMENT>PPDB ID&#39;s eingetragen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="14.02.2007 00:00" version="75.0_F" author="q234455 / PPDB Zaal ANdrea -&gt; Igor Rochal - PPDB Zaal ANdrea -&gt; Igor Rochal">
  <COMMENT>PPDB ID&#39;s eingetragen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="12.02.2007 00:00" version="74.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>PPs und Attribute f�r optimierte Baureihenabfrage hinzu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="24.01.2007 00:00" version="73.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>STATUS BEIFAHRERAIRBAG_AUS hinzugef�gt</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="22.01.2007 00:00" version="72.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Anpassung Ruhestrom (Blue Pirat) und Schl�ssel abziehen f�r 03/07</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="19.01.2007 00:00" version="71.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Blue Pirat SA 991 statt 909</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="12.01.2007 00:00" version="70.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Wasserstoff-Taste nicht bei Entwicklungsfahrzeugen, Standheizung wieder aufgenommen!</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="11.01.2007 00:00" version="69.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>PWG f�r M5/M6 angepasst</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="15.12.2006 00:00" version="68.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Anpassung Blinker E63 M�, LA TV E65</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="13.12.2006 00:00" version="67.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Optimierung Spritzd�senheizung E6x</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="05.12.2006 00:00" version="66.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Anpassung PWG</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.12.2006 00:00" version="65.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Standheizung entfernt, wurde in anderen Bereich verlagert.DMTL entfernt, befindet sich jetzt vollst�ndug in den Motorpr�flingen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="01.12.2006 00:00" version="64.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Optimierung Gebl�se</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="01.12.2006 00:00" version="63.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>LBV bis 8.12. hinzu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="29.11.2006 00:00" version="62.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Optimierung Gebl�se E6x</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.11.2006 00:00" version="61.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Heckscheibenheizung E65</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="23.11.2006 00:00" version="60.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Optimierung Nassarmheizung</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="21.11.2006 00:00" version="59.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Seitenspiegel elektrisch ab 03/07 auch bei US (bei SA 430)</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="17.11.2006 00:00" version="58.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Optimierung Nassarmheizung</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="14.11.2006 00:00" version="57.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Optimierung PWG</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="13.11.2006 00:00" version="56.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>optimierung Sitzheizung E65</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="13.11.2006 00:00" version="55.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Start_Trigger_Dir=0 f�r Sitzheizung E65 gesetzt</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="13.11.2006 00:00" version="54.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Optimierung Wischer</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="13.11.2006 00:00" version="53.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>optimierung Kurzwischen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="09.11.2006 00:00" version="52.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Optimierung PWG</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="08.11.2006 00:00" version="51.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Fliesskommawert bei PWG ge�ndert (Punkt statt Komma)</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="08.11.2006 00:00" version="50.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT></COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="07.11.2006 00:00" version="49.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Lichtpaket angepasst, Werte Kurzwischen optimiert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="06.11.2006 00:00" version="48.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>STATUS_GURTSCHLOSS_AUS hinzu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="06.11.2006 00:00" version="47.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Messzeit Kurzwischen auf 1250, war 1000</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="26.10.2006 00:00" version="46.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>STEUERN_RESET hinzu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="25.10.2006 00:00" version="45.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Blinker E63/64 M�</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="20.10.2006 00:00" version="44.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Sidmarker neu f�r E6x</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="19.10.2006 00:00" version="43.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Sitzheizung E65</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.10.2006 00:00" version="42.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT></COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.10.2006 00:00" version="41.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT></COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.10.2006 00:00" version="40.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT></COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.10.2006 00:00" version="39.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Sitzheizung f�r E65 komplett optimiert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.10.2006 00:00" version="38.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>sitzhgeizung e65</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="17.10.2006 00:00" version="37.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Messzeit Ruhestrom</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="13.10.2006 00:00" version="36.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Optimierung E65 Sitzheizung und Wischer</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="12.10.2006 00:00" version="35.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Kurzwischen zu Sitzheizung hinten angepasst</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="12.10.2006 00:00" version="34.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Sitzheizung E65</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="11.10.2006 00:00" version="33.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>START/STOP Button f�r Ruhestromprobleme hinzu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="09.10.2006 00:00" version="32.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>E65 Handschuhfachbel.</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="09.10.2006 00:00" version="31.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Heckkl. Kontakt, Handschuhfach E65</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="06.10.2006 00:00" version="30.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Sitzpr�fung Zusatz bei E6x Sichtpr�fschritte f�r 1 Woche (bis 16.10.06)</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="06.10.2006 00:00" version="29.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Blue Pirat angepasst</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.10.2006 00:00" version="28.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>FS_LESEN bei DWA f�r E60 hinzu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="04.10.2006 00:00" version="27.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>STEUERN_DWA_DIAGNOSE hinzu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="28.09.2006 00:00" version="26.0_F" author="q049248 / Thomas Stuerzer - TD-426">
  <COMMENT>STATUS_TMB_RESET SGBD auf TMB_E65 ge�ndert (vorher TMF_E65)</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="27.09.2006 00:00" version="25.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Test neue Werte Heckscheibenheizung E65, Handschuhkasten E6x, Sitzheizung E65 ohne Best�tigung, Deakt. Beifahrerairbag</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="20.09.2006 00:00" version="24.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>heckklappenverriegelung</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="19.09.2006 00:00" version="23.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Anpassungen Texte, Nuer PS SCHLUESSEL_MECH bei E65</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="19.09.2006 00:00" version="22.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>div. Anpassungen Texte E65</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="18.09.2006 00:00" version="21.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Alle Sitze E6x wie Basissitz, div. Anpassungen (Titel Sichtpr�fung)</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="15.09.2006 00:00" version="20.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>div. Anpassungen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="14.09.2006 00:00" version="19.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Texte E65</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="14.09.2006 00:00" version="18.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Pedalwertgeber hinzu, Texte ge�ndert (SA322)</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="14.09.2006 00:00" version="17.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>CD-Wechsler auch E63/64, Blinker Vorserie Werte angepasst, Ruhestrom Vorstufe f�r Blue Pirat</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="13.09.2006 00:00" version="16.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Centerlock E63/64 hinzugef�gt</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="13.09.2006 00:00" version="15.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Warnblinker 03/07</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="13.09.2006 00:00" version="14.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Blinker f�r 03/07, RDC_FS_LESEN</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="11.09.2006 00:00" version="13.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>ab 03/07 Zigarrenanz�nder vorne nur bei Raucherpaket SA441</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="06.09.2006 00:00" version="12.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>div. Anpassungen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="06.09.2006 00:00" version="11.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Sitzlehne E63 / 64</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="06.09.2006 00:00" version="10.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Sidemarker E65 vorne f�r Modelljahresmassnahmen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="31.08.2006 00:00" version="9.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Testbild Weiss ersetzt durch Pr�fung Hauptmenu wg. Fehlfunktion CCC beim Ansteuern.Bei D_KLIMA alle Timeouts auf 300ms wg. fehlender Klimabedienteile.</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="30.08.2006 00:00" version="8.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Sidemarker bei E63/64 US hinzu. Abfrage Gurtschloss Reedkontakt bei E63/64 auf Status ge�ndert.</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="27.07.2006 00:00" version="7.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Spiegelbeiklappen optimiert (wie in T�rmontage: SA430 und nicht US, SA430 und M5, M6, M6 Cabrio)</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="26.07.2006 00:00" version="6.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Fehler (SGBD) bei FS_LESEN_CDC korrigiert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="24.07.2006 00:00" version="5.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>FS_LESEN_CDC hinzugef�gt</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="24.07.2006 00:00" version="4.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Anpassung Spiegelbeiklappen</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="06.07.2006 00:00" version="3.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>TRIGGER_PPs von ECOS_ALLGEMEIN auf ECOS ge�ndert</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="22.06.2006 00:00" version="2.0_F" author="q196957 / Iversen Stefan - TD-426">
  <COMMENT>Statusabfrage Gurtschloss neu</COMMENT>
 </MANUAL_COMMENT> <MANUAL_COMMENT date="08.06.2006 00:00" version="1.0_F" author="q163569 / Gerhard Karl - TD-426 Admin Cascade">
  <COMMENT>PL ECOS neu erstellt</COMMENT>
 </MANUAL_COMMENT></HISTORY>
 * HISTORIE_XML END
 */
/**
 * Pr�fling ECOS
 */
public class ECOS_414_0_F_Pruefling extends AbstractBMWPruefling {
static final long serialVersionUID = 1L;
  /**
  * DefaultKonstruktor, nur fuer die Deserialisierung
  */
 public ECOS_414_0_F_Pruefling() {}

  /**
   * Konstruktur
   *
   * @param auftrag Der Auftrag, zu dem dieser Pr�fling instantiiert werden soll.
   */
  public ECOS_414_0_F_Pruefling( Auftrag auftrag ) throws PrueflingNotAvailableException, PruefprozedurNotAvailableException {
    super( auftrag );
}
  /**
   * Liefert symbolischen Namen des Pr�flings
   *
   * @return Symbolischer Name des Pr�flings
   */
  protected String getPName() {
    return("ECOS");
  }
  /**
   * Pr�ft Verf�gbarkeit des Pr�flings
   * �blicherweise wird das Ergebnis aus den Fahrzeugdaten oder durch die Existenz anderer Pr�flinge gewonnen
   */
  protected void checkAvailability() throws PrueflingNotAvailableException {
/*
 * DIESER KOMMENTAR WIRD AUTOMATISCH ERZEUGT UND GELESEN
 * NICHT VER�NDERN!
 * EXISTENCE_XML START
<EXISTENCE version="2">
 <TRUE/>
 </EXISTENCE>
 * EXISTENCE_XML END
 */
	   // FOLGENDER CODE WIRD AUTOMATISCH NEU ERZEUGT,
	   // NICHT �NDERN
	   // EXISTENCE_CODE START
if( 
 true )
	return;
else
	throw new PrueflingNotAvailableException( getName() );

	// EXISTENCE_CODE END
}
  /**
   * Initialisiert die Attribute
   */
  protected void attributeInit() {
/*
 * DIESER KOMMENTAR WIRD AUTOMATISCH ERZEUGT UND GELESEN
 * NICHT VER�NDERN!
 * ATTRIBUTDEFINITION_XML START
<ATTRIBUTDEFINITION version="2">
 <GROUP name="--------------------------------------------------------">
 </GROUP>
 <GROUP name="CONTACT">
  <ASSIGNMENT literal="CONTACT_FIZ_FUNKTION">
   <COMMENT>function specialist plant FIZ</COMMENT>
   <CONSTANT type="String" value="Mario.MF.Friedrich@bmw.de">
    <COMMENT></COMMENT>
   </CONSTANT>
  </ASSIGNMENT>
  <ASSIGNMENT literal="CONTACT_MAIL_FIZ">
   <COMMENT>plant FIZ</COMMENT>
   <CONSTANT type="String" value="Mario.MF.Friedrich@bmw.de">
    <COMMENT></COMMENT>
   </CONSTANT>
  </ASSIGNMENT>
  <ASSIGNMENT literal="CONTACT_MAIL_W1">
   <COMMENT>plant Munich</COMMENT>
   <CONSTANT type="String" value="Siegfried.Heinzmann@bmw.de">
    <COMMENT></COMMENT>
   </CONSTANT>
  </ASSIGNMENT>
  <ASSIGNMENT literal="CONTACT_MAIL_W2">
   <COMMENT>plant Dingolfing</COMMENT>
   <CONSTANT type="String" value="Manfred.Kronbeck@bmw.de">
   </CONSTANT>
  </ASSIGNMENT>
  <ASSIGNMENT literal="CONTACT_MAIL_W6">
   <COMMENT>plant Regensburg</COMMENT>
   <CONSTANT type="String" value="Erwin.Leidel@bmw.de">
   </CONSTANT>
  </ASSIGNMENT>
  <ASSIGNMENT literal="CONTACT_MAIL_W7">
   <COMMENT>plant Leipzig</COMMENT>
   <CONSTANT type="String" value="Hendrik.Hesse@bmw.de">
   </CONSTANT>
  </ASSIGNMENT>
  <ASSIGNMENT literal="CONTACT_MAIL_W9">
   <COMMENT>plant Rosslyn/ZA</COMMENT>
   <CONSTANT type="String" value="Lucien.Jordaan@bmw.co.za">
    <COMMENT></COMMENT>
   </CONSTANT>
   <CONSTANT type="String" value="siphiwe.mathebula@bmw.co.za">
    <COMMENT></COMMENT>
   </CONSTANT>
  </ASSIGNMENT>
  <ASSIGNMENT literal="CONTACT_MAIL_W10">
   <COMMENT>plant Spartanburg/USA</COMMENT>
   <CONSTANT type="String" value="Jeffrey.Fann@bmwmc.com">
    <COMMENT></COMMENT>
   </CONSTANT>
  </ASSIGNMENT>
  <ASSIGNMENT literal="CONTACT_MAIL_W1910">
   <COMMENT>plant Dadong/China</COMMENT>
   <CONSTANT type="String" value="Xinyue.Wu@bmw-brilliance.cn">
    <COMMENT></COMMENT>
   </CONSTANT>
  </ASSIGNMENT>
  <ASSIGNMENT literal="CONTACT_MAIL_W1988">
   <COMMENT>plant Tiexi/China</COMMENT>
   <CONSTANT type="String" value="Xiuli.Xu@bmw-brilliance.cn">
    <COMMENT></COMMENT>
   </CONSTANT>
  </ASSIGNMENT>
  <ASSIGNMENT literal="CONTACT_MAIL_W30">
   <COMMENT>plant San Luis Potosi/Mexiko</COMMENT>
   <CONSTANT type="String" value="Marco.Gutierrez@bmw.com.mx">
    <COMMENT></COMMENT>
   </CONSTANT>
  </ASSIGNMENT>
  <ASSIGNMENT literal="CONTACT_MAIL_W34">
   <COMMENT>plant Oxford/GB</COMMENT>
   <CONSTANT type="String" value="Jaap.van-der-Werf@bmwgroup.com">
    <COMMENT></COMMENT>
   </CONSTANT>
  </ASSIGNMENT>
  <ASSIGNMENT literal="CONTACT_MAIL_W50">
   <COMMENT>plant Goodwood/GB</COMMENT>
   <CONSTANT type="String" value="luke.brocks@rolls-roycemotorcars.com">
    <COMMENT></COMMENT>
   </CONSTANT>
  </ASSIGNMENT>
  <ASSIGNMENT literal="CONTACT_MAIL_W0530">
   <COMMENT>plant Magna Steyr Graz/Austria</COMMENT>
   <CONSTANT type="String" value="Cascade.magnasteyr@magna.com">
    <COMMENT></COMMENT>
   </CONSTANT>
  </ASSIGNMENT>
  <ASSIGNMENT literal="CONTACT_MAIL_W58">
   <COMMENT></COMMENT>
   <CONSTANT type="String" value="Cascade@vdlnedcar.nl">
    <COMMENT></COMMENT>
   </CONSTANT>
  </ASSIGNMENT>
  <ASSIGNMENT literal="CONTACT_MAIL_GENERAL">
   <COMMENT>general</COMMENT>
   <CONSTANT type="String" value="n.r.">
    <COMMENT></COMMENT>
   </CONSTANT>
  </ASSIGNMENT>
  <ASSIGNMENT literal="FACHTEAM">
   <COMMENT>Fachteam</COMMENT>
   <CONSTANT type="String" value="Interaktion &amp; Energie">
   </CONSTANT>
  </ASSIGNMENT>
 </GROUP>
 <GROUP name="--------------------------------------------------------">
 </GROUP>
 <GROUP name="GLOBAL ATTRIBUTES">
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="FRAGETEXTE / QUESTIONTEXT">
   <COMMENT></COMMENT>
   <GROUP name="DEFAULT - DO NOT DELETE">
    <COMMENT></COMMENT>
    <ASSIGNMENT literal="QUESTION_TEXT_BOOT_LIGHT">
     <COMMENT></COMMENT>
     <CONSTANT type="String" value="default" devalue="default" envalue="default">
      <COMMENT></COMMENT>
     </CONSTANT>
    </ASSIGNMENT>
    <ASSIGNMENT literal="QUESTION_TEXT_TAILGATE_LIGHT">
     <COMMENT></COMMENT>
     <CONSTANT type="String" value="default" devalue="default" envalue="default">
      <COMMENT></COMMENT>
     </CONSTANT>
    </ASSIGNMENT>
    <ASSIGNMENT literal="QUESTION_TEXT_MSA_BUTTON">
     <COMMENT></COMMENT>
     <CONSTANT type="String" value="default" devalue="default" envalue="default">
      <COMMENT></COMMENT>
     </CONSTANT>
    </ASSIGNMENT>
   </GROUP>
   <IF>
    <COMMENT>BELEUCHTUNG GEPAECKRAUM / BOOT LIGHT</COMMENT>
    <OR>
     <COMMENT>cars with 2 lights</COMMENT>
     <OR>
      <COMMENT>35up</COMMENT>
      <BOOL_METHOD>
       <COMMENT>X5 M</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <COMMENT></COMMENT>
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="F95">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <COMMENT>X6 M</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <COMMENT></COMMENT>
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="F96">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <COMMENT>X3 M</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="F97">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <COMMENT>X4 M</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="F98">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <COMMENT>X3</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="G01">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <COMMENT>X4 Coup�</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="G02">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <COMMENT>X5</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <COMMENT></COMMENT>
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="G05">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <COMMENT>X6 Coup�</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <COMMENT></COMMENT>
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="G06">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <COMMENT>X7</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <COMMENT></COMMENT>
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="G07">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <COMMENT>X3 China</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="G08">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <COMMENT>3 series wagon</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="G21">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <COMMENT>3 series grand coup�</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="G26">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <COMMENT>5 series wagon</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="G31">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <COMMENT>5 series GT</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="G32">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
     </OR>
     <OR>
      <COMMENT>Rolls Royce</COMMENT>
      <BOOL_METHOD>
       <COMMENT>Ghost</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="RR4">
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <COMMENT>Wraith</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="RR5">
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <COMMENT>Wraith Cabrio</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="RR6">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <COMMENT></COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="RR11">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <COMMENT></COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="RR12">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
     </OR>
    </OR>
    <THEN>
     <COMMENT>cars with 2 lights</COMMENT>
     <ASSIGNMENT literal="QUESTION_TEXT_BOOT_LIGHT">
      <COMMENT></COMMENT>
      <CONSTANT type="String" value="2 Gep�ckraumleuchten i.O.?" devalue="2 Gep�ckraumleuchten i.O.?" envalue="Both Lower Boot Lights Illuminated?">
       <COMMENT></COMMENT>
      </CONSTANT>
     </ASSIGNMENT>
    </THEN>
    <ELSE>
     <COMMENT>cars with 1 light</COMMENT>
     <ASSIGNMENT literal="QUESTION_TEXT_BOOT_LIGHT">
      <COMMENT></COMMENT>
      <CONSTANT type="String" value="1 Gep�ckraumleuchte i.O.?" devalue="1 Gep�ckraumleuchte i.O.?" envalue="Boot light illuminated?">
       <COMMENT></COMMENT>
      </CONSTANT>
     </ASSIGNMENT>
    </ELSE>
   </IF>
   <IF>
    <COMMENT>BELEUCHTUNG HECKKLAPPE / TAILGATE LIGHT</COMMENT>
    <OR>
     <COMMENT>cars with 2 lights</COMMENT>
     <BOOL_METHOD>
      <COMMENT>X5 M</COMMENT>
      <METHOD name="getBaureihe" returntype="String">
       <COMMENT></COMMENT>
       <METHOD name="equalsIgnoreCase" returntype="boolean">
        <COMMENT></COMMENT>
        <PARAMS>
         <CONSTANT type="String" value="F95">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PARAMS>
       </METHOD>
      </METHOD>
          <OPERATOR type="eq" optype="boolean"> </OPERATOR>
     <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
     <BOOL_METHOD>
      <COMMENT>X6 M</COMMENT>
      <METHOD name="getBaureihe" returntype="String">
       <COMMENT></COMMENT>
       <METHOD name="equalsIgnoreCase" returntype="boolean">
        <COMMENT></COMMENT>
        <PARAMS>
         <CONSTANT type="String" value="F96">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PARAMS>
       </METHOD>
      </METHOD>
          <OPERATOR type="eq" optype="boolean"> </OPERATOR>
     <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
     <BOOL_METHOD>
      <COMMENT>X5</COMMENT>
      <METHOD name="getBaureihe" returntype="String">
       <COMMENT></COMMENT>
       <METHOD name="equalsIgnoreCase" returntype="boolean">
        <COMMENT></COMMENT>
        <PARAMS>
         <CONSTANT type="String" value="G05">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PARAMS>
       </METHOD>
      </METHOD>
          <OPERATOR type="eq" optype="boolean"> </OPERATOR>
     <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
     <BOOL_METHOD>
      <COMMENT>X6 Coup�</COMMENT>
      <METHOD name="getBaureihe" returntype="String">
       <COMMENT></COMMENT>
       <METHOD name="equalsIgnoreCase" returntype="boolean">
        <COMMENT></COMMENT>
        <PARAMS>
         <CONSTANT type="String" value="G06">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PARAMS>
       </METHOD>
      </METHOD>
          <OPERATOR type="eq" optype="boolean"> </OPERATOR>
     <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
     <BOOL_METHOD>
      <COMMENT>X7</COMMENT>
      <METHOD name="getBaureihe" returntype="String">
       <COMMENT></COMMENT>
       <METHOD name="equalsIgnoreCase" returntype="boolean">
        <COMMENT></COMMENT>
        <PARAMS>
         <CONSTANT type="String" value="G07">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PARAMS>
       </METHOD>
      </METHOD>
          <OPERATOR type="eq" optype="boolean"> </OPERATOR>
     <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
     <BOOL_METHOD>
      <COMMENT>3 series wagon</COMMENT>
      <METHOD name="getBaureihe" returntype="String">
       <METHOD name="equalsIgnoreCase" returntype="boolean">
        <COMMENT></COMMENT>
        <PARAMS>
         <CONSTANT type="String" value="G21">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PARAMS>
       </METHOD>
      </METHOD>
          <OPERATOR type="eq" optype="boolean"> </OPERATOR>
     <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
     <BOOL_METHOD>
      <COMMENT>3 series grand coup�</COMMENT>
      <METHOD name="getBaureihe" returntype="String">
       <METHOD name="equalsIgnoreCase" returntype="boolean">
        <COMMENT></COMMENT>
        <PARAMS>
         <CONSTANT type="String" value="G26">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PARAMS>
       </METHOD>
      </METHOD>
          <OPERATOR type="eq" optype="boolean"> </OPERATOR>
     <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    </OR>
    <THEN>
     <COMMENT>cars with 2 lights</COMMENT>
     <IF>
      <BOOL_METHOD>
       <METHOD name="getIStufe" returntype="String">
        <COMMENT></COMMENT>
        <METHOD name="equals" returntype="boolean">
         <PARAMS>
          <CONSTANT type="String" value="S18">
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <THEN>
       <COMMENT>SP2018</COMMENT>
       <ASSIGNMENT literal="QUESTION_TEXT_TAILGATE_LIGHT">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="Heckklappenlicht li+re: i.O.?" devalue="Heckklappenlicht li+re: i.O.?" envalue="Hatch light left and right: OK?">
         <COMMENT></COMMENT>
        </CONSTANT>
       </ASSIGNMENT>
      </THEN>
      <ELSE>
       <COMMENT>!SP2018</COMMENT>
       <ASSIGNMENT literal="QUESTION_TEXT_TAILGATE_LIGHT">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="Heckklappenlicht li+re: i.O.?" devalue="Heckklappenlicht li+re: i.O.?" envalue="Tailgate light left and right: OK?">
         <COMMENT></COMMENT>
        </CONSTANT>
       </ASSIGNMENT>
      </ELSE>
     </IF>
    </THEN>
    <ELSE>
     <COMMENT>cars with 1 light</COMMENT>
     <IF>
      <BOOL_METHOD>
       <METHOD name="getIStufe" returntype="String">
        <COMMENT></COMMENT>
        <METHOD name="equals" returntype="boolean">
         <PARAMS>
          <CONSTANT type="String" value="S18">
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <THEN>
       <COMMENT>SP2018</COMMENT>
       <ASSIGNMENT literal="QUESTION_TEXT_TAILGATE_LIGHT">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="1 Heckklappenlicht i.O.?" devalue="1 Heckklappenlicht i.O.?" envalue="Hatch Light Illuminated?">
         <COMMENT></COMMENT>
        </CONSTANT>
       </ASSIGNMENT>
      </THEN>
      <ELSE>
       <COMMENT>!SP2018</COMMENT>
       <ASSIGNMENT literal="QUESTION_TEXT_TAILGATE_LIGHT">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="1 Heckklappenlicht i.O.?" devalue="1 Heckklappenlicht i.O.?" envalue="Tailgate Light Illuminated?">
         <COMMENT></COMMENT>
        </CONSTANT>
       </ASSIGNMENT>
      </ELSE>
     </IF>
    </ELSE>
   </IF>
   <IF>
    <COMMENT>MSA-TASTER / MSA BUTTON</COMMENT>
    <AND>
     <COMMENT>F30H GEN2.0</COMMENT>
     <BOOL_METHOD>
      <METHOD name="getBaureihe" returntype="String">
       <METHOD name="equalsIgnoreCase" returntype="boolean">
        <PARAMS>
         <CONSTANT type="String" value="F30">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PARAMS>
       </METHOD>
      </METHOD>
          <OPERATOR type="eq" optype="boolean"> </OPERATOR>
     <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
     <OR>
      <BOOL_METHOD>
       <METHOD name="getHybrid" returntype="String">
        <COMMENT></COMMENT>
        <METHOD name="equals" returntype="boolean">
         <PARAMS>
          <CONSTANT type="String" value="HYBR">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <BOOL_METHOD>
       <METHOD name="getHybrid" returntype="String">
        <COMMENT></COMMENT>
        <METHOD name="equals" returntype="boolean">
         <PARAMS>
          <CONSTANT type="String" value="PHEV">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
     </OR>
    </AND>
    <THEN>
     <ASSIGNMENT literal="QUESTION_TEXT_MSA_BUTTON">
      <COMMENT></COMMENT>
      <CONSTANT type="String" value="Start-Stop-Taster ohne MSA-Taste verbaut?" devalue="Start-Stop-Taster ohne MSA-Taste verbaut?" envalue="start-stop-button without MSA-button installed?">
       <COMMENT></COMMENT>
      </CONSTANT>
     </ASSIGNMENT>
    </THEN>
    <ELSE>
     <IF>
      <AND>
       <COMMENT>35up PHEV</COMMENT>
       <BOOL_METHOD>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="startsWith" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="G">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <METHOD name="getHybrid" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="equals" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="PHEV">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </AND>
      <THEN>
       <ASSIGNMENT literal="QUESTION_TEXT_MSA_BUTTON">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="HYBRID: KEINE MSA-Taste verbaut?" devalue="HYBRID: KEINE MSA-Taste verbaut?" envalue="HYBRID: NO MSA-button installed?">
         <COMMENT></COMMENT>
        </CONSTANT>
       </ASSIGNMENT>
      </THEN>
      <ELSE>
       <IF>
        <AND>
         <COMMENT>G12 Alpina US</COMMENT>
         <BOOL_METHOD>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="G12">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>ALPINA PACKAGE</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="09XA">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <METHOD name="getL�nderVariante" returntype="String">
           <METHOD name="startsWith" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="US">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </AND>
        <THEN>
         <ASSIGNMENT literal="QUESTION_TEXT_MSA_BUTTON">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="ALPINA: KEINE MSA-Taste verbaut?" devalue="ALPINA: KEINE MSA-Taste verbaut?" envalue="ALPINA: NO MSA-button installed?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </ASSIGNMENT>
        </THEN>
        <ELSE>
         <ASSIGNMENT literal="QUESTION_TEXT_MSA_BUTTON">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="Start-Stop-Taster mit  MSA-Taste verbaut?" devalue="Start-Stop-Taster mit  MSA-Taste verbaut?" envalue="start-stop-button with MSA-button installed?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </ASSIGNMENT>
        </ELSE>
       </IF>
      </ELSE>
     </IF>
    </ELSE>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="SGBD">
   <COMMENT></COMMENT>
   <GROUP name="USB HINTEN">
    <COMMENT></COMMENT>
    <ASSIGNMENT literal="SGBD">
     <CONSTANT type="String" value="NBTEVO">
      <COMMENT></COMMENT>
     </CONSTANT>
    </ASSIGNMENT>
    <GROUP name="ENAVEVO">
     <COMMENT>ms 23.07.2015 LK: changed existences according 1er series (gAMS AG7073 - F2x: SA663 &#39;&#39;BMW Radio Professional&#39;&#39; wird Typinhalt ab 07/17)

ms 19.11.2014 G11/G12 deleted (gAMS AD5808 - No Entrynav for G11/G12)


ms 16.09.2014 !6VB added (gAMS AD2527)</COMMENT>
     <IF>
      <OR>
       <COMMENT>SGBD</COMMENT>
       <OR>
        <COMMENT>LK</COMMENT>
        <AND>
         <COMMENT>3 series from 03/17</COMMENT>
         <OR>
          <COMMENT>3series</COMMENT>
          <BOOL_METHOD>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F32">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F33">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F36">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F82">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F83">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <AND>
          <COMMENT>Not NBT</COMMENT>
<NOT>
                      <BOOL_METHOD>
            <COMMENT>TV option of Navi</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="0601">
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
         <NOT>
                      <BOOL_METHOD>
            <COMMENT>TV without Navi</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="0602">
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
         <NOT>
                      <BOOL_METHOD>
            <COMMENT>Navi High</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="0609">
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
         <NOT>
                      <BOOL_METHOD>
            <COMMENT>Hybrid NBT4Low</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="06UF">
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                  </AND>
         <OR>
          <COMMENT>from 03/17</COMMENT>
          <AND>
           <BOOL_METHOD>
            <METHOD name="getIStufeMonatInt" returntype="int">
            </METHOD>
            <OPERATOR type="g_equal" optype="int">
             <COMMENT></COMMENT>
            </OPERATOR>
            <CONSTANT type="int" value="3">
             <COMMENT></COMMENT>
            </CONSTANT>
           </BOOL_METHOD>
           <BOOL_METHOD>
            <METHOD name="getIStufeJahrInt" returntype="int">
            </METHOD>
            <OPERATOR type="eq" optype="int">
            </OPERATOR>
            <CONSTANT type="int" value="17">
             <COMMENT></COMMENT>
            </CONSTANT>
           </BOOL_METHOD>
          </AND>
          <BOOL_METHOD>
           <METHOD name="getIStufeJahrInt" returntype="int">
           </METHOD>
           <OPERATOR type="greater" optype="int">
            <COMMENT></COMMENT>
           </OPERATOR>
           <CONSTANT type="int" value="17">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
         </OR>
        </AND>
        <AND>
         <COMMENT>3 series from 07/17</COMMENT>
         <OR>
          <COMMENT>3series</COMMENT>
          <BOOL_METHOD>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F30">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F31">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F34">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F35">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F80">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <AND>
          <COMMENT>Not NBT</COMMENT>
<NOT>
                      <BOOL_METHOD>
            <COMMENT>TV option of Navi</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="0601">
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
         <NOT>
                      <BOOL_METHOD>
            <COMMENT>TV without Navi</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="0602">
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
         <NOT>
                      <BOOL_METHOD>
            <COMMENT>Navi High</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="0609">
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
         <NOT>
                      <BOOL_METHOD>
            <COMMENT>Hybrid NBT4Low</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="06UF">
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                  </AND>
         <OR>
          <COMMENT>from 07/17</COMMENT>
          <AND>
           <BOOL_METHOD>
            <METHOD name="getIStufeMonatInt" returntype="int">
            </METHOD>
            <OPERATOR type="g_equal" optype="int">
             <COMMENT></COMMENT>
            </OPERATOR>
            <CONSTANT type="int" value="7">
             <COMMENT></COMMENT>
            </CONSTANT>
           </BOOL_METHOD>
           <BOOL_METHOD>
            <METHOD name="getIStufeJahrInt" returntype="int">
            </METHOD>
            <OPERATOR type="eq" optype="int">
            </OPERATOR>
            <CONSTANT type="int" value="17">
             <COMMENT></COMMENT>
            </CONSTANT>
           </BOOL_METHOD>
          </AND>
          <BOOL_METHOD>
           <METHOD name="getIStufeJahrInt" returntype="int">
           </METHOD>
           <OPERATOR type="greater" optype="int">
            <COMMENT></COMMENT>
           </OPERATOR>
           <CONSTANT type="int" value="17">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
         </OR>
        </AND>
        <AND>
         <COMMENT>1/2 series from 07/17</COMMENT>
         <OR>
          <COMMENT>series</COMMENT>
          <BOOL_METHOD>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F20">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F21">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F22">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F23">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>M2 Coupe</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F87">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>M2 cabrio</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F88">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <AND>
          <COMMENT>Not NBTEVO</COMMENT>
<NOT>
                      <BOOL_METHOD>
            <COMMENT>TV option of Navi</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="0601">
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
         <NOT>
                      <BOOL_METHOD>
            <COMMENT>TV without Navi</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="0602">
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
         <NOT>
                      <BOOL_METHOD>
            <COMMENT>Navi High</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="0609">
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
         <NOT>
                      <BOOL_METHOD>
            <COMMENT>Hybrid NBT4Low</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="06UF">
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                  </AND>
         <OR>
          <COMMENT>from 07/17</COMMENT>
          <AND>
           <BOOL_METHOD>
            <METHOD name="getIStufeMonatInt" returntype="int">
            </METHOD>
            <OPERATOR type="g_equal" optype="int">
             <COMMENT></COMMENT>
            </OPERATOR>
            <CONSTANT type="int" value="7">
             <COMMENT></COMMENT>
            </CONSTANT>
           </BOOL_METHOD>
           <BOOL_METHOD>
            <METHOD name="getIStufeJahrInt" returntype="int">
            </METHOD>
            <OPERATOR type="eq" optype="int">
            </OPERATOR>
            <CONSTANT type="int" value="17">
             <COMMENT></COMMENT>
            </CONSTANT>
           </BOOL_METHOD>
          </AND>
          <BOOL_METHOD>
           <METHOD name="getIStufeJahrInt" returntype="int">
           </METHOD>
           <OPERATOR type="greater" optype="int">
            <COMMENT></COMMENT>
           </OPERATOR>
           <CONSTANT type="int" value="17">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
         </OR>
        </AND>
       </OR>
       <AND>
        <COMMENT>35up LK</COMMENT>
        <OR>
         <COMMENT>series
bs, v1249: F97, F98 added</COMMENT>
         <BOOL_METHOD>
          <COMMENT>F30NF</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G20">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>F31NF</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G21">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>F32NF</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G22">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>F33NF</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G23">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>F34NF</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G24">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>F36NF</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G26">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>F35NF</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G28">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>E89NF</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G29">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>F10NF</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G30">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>F07NF</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G31">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>F11NF</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G32">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>F18NF</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G38">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>F25NF</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="G01">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>F26NF</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="G02">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>F26NF China</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="G08">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>G01 M</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="F97">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>G02 M</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="F98">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <AND>
         <COMMENT>no Navi High</COMMENT>
<NOT>
                    <BOOL_METHOD>
           <COMMENT>Analog TV</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="06FS">
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
        <NOT>
                    <BOOL_METHOD>
           <COMMENT>TV option of Navi</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="0601">
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
        <NOT>
                    <BOOL_METHOD>
           <COMMENT>TV without Navi</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="0602">
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
        <NOT>
                    <BOOL_METHOD>
           <COMMENT>Navi High</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="0609">
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
        <NOT>
                    <BOOL_METHOD>
           <COMMENT>Navi preparation</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="06UF">
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                </AND>
        <OR>
         <COMMENT>from 11/16</COMMENT>
         <AND>
          <BOOL_METHOD>
           <METHOD name="getIStufeNummerInt" returntype="int">
            <COMMENT></COMMENT>
           </METHOD>
           <OPERATOR type="g_equal" optype="int">
            <COMMENT></COMMENT>
           </OPERATOR>
           <CONSTANT type="int" value="330">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getIStufeMonatInt" returntype="int">
           </METHOD>
           <OPERATOR type="eq" optype="int">
            <COMMENT></COMMENT>
           </OPERATOR>
           <CONSTANT type="int" value="11">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getIStufeJahrInt" returntype="int">
           </METHOD>
           <OPERATOR type="eq" optype="int">
           </OPERATOR>
           <CONSTANT type="int" value="16">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
         </AND>
         <AND>
          <BOOL_METHOD>
           <METHOD name="getIStufeMonatInt" returntype="int">
           </METHOD>
           <OPERATOR type="greater" optype="int">
            <COMMENT></COMMENT>
           </OPERATOR>
           <CONSTANT type="int" value="11">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getIStufeJahrInt" returntype="int">
           </METHOD>
           <OPERATOR type="eq" optype="int">
           </OPERATOR>
           <CONSTANT type="int" value="16">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
         </AND>
         <BOOL_METHOD>
          <METHOD name="getIStufeJahrInt" returntype="int">
          </METHOD>
          <OPERATOR type="greater" optype="int">
           <COMMENT></COMMENT>
          </OPERATOR>
          <CONSTANT type="int" value="16">
           <COMMENT></COMMENT>
          </CONSTANT>
         </BOOL_METHOD>
        </OR>
<NOT>
                  <BOOL_METHOD>
          <COMMENT>Navi preparation</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT>NBTEVO for ENAVEVO</COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="06VB">
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
              </AND>
       <AND>
        <COMMENT>I01</COMMENT>
        <BOOL_METHOD>
         <COMMENT>I3 MCV</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="I01">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <AND>
         <COMMENT>no Navi High</COMMENT>
<NOT>
                    <BOOL_METHOD>
           <COMMENT>Analog TV</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="06FS">
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
        <NOT>
                    <BOOL_METHOD>
           <COMMENT>TV option of Navi</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="0601">
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
        <NOT>
                    <BOOL_METHOD>
           <COMMENT>TV without Navi</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="0602">
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
        <NOT>
                    <BOOL_METHOD>
           <COMMENT>Navi High</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="0609">
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                </AND>
        <OR>
         <COMMENT>from 11/18</COMMENT>
         <AND>
          <BOOL_METHOD>
           <METHOD name="getIStufeMonatInt" returntype="int">
           </METHOD>
           <OPERATOR type="g_equal" optype="int">
            <COMMENT></COMMENT>
           </OPERATOR>
           <CONSTANT type="int" value="11">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getIStufeJahrInt" returntype="int">
           </METHOD>
           <OPERATOR type="eq" optype="int">
           </OPERATOR>
           <CONSTANT type="int" value="18">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
         </AND>
         <BOOL_METHOD>
          <METHOD name="getIStufeJahrInt" returntype="int">
          </METHOD>
          <OPERATOR type="greater" optype="int">
           <COMMENT></COMMENT>
          </OPERATOR>
          <CONSTANT type="int" value="18">
           <COMMENT></COMMENT>
          </CONSTANT>
         </BOOL_METHOD>
        </OR>
       </AND>
       <AND>
        <COMMENT>LU-BMW</COMMENT>
        <OR>
         <BOOL_METHOD>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="startsWith" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="F4">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="F39">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="F52">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <AND>
         <COMMENT>No High HU (possible in Japan/Korea)</COMMENT>
<NOT>
                    <BOOL_METHOD>
           <COMMENT>Analog TV</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="06FS">
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
        <NOT>
                    <BOOL_METHOD>
           <COMMENT>TV option of Navi</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="0601">
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
        <NOT>
                    <BOOL_METHOD>
           <COMMENT>TV without Navi</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="0602">
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
        <NOT>
                    <BOOL_METHOD>
           <COMMENT>Navi High</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="0609">
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                </AND>
        <OR>
         <COMMENT>from 07/17</COMMENT>
         <AND>
          <BOOL_METHOD>
           <METHOD name="getIStufeMonatInt" returntype="int">
           </METHOD>
           <OPERATOR type="g_equal" optype="int">
            <COMMENT></COMMENT>
           </OPERATOR>
           <CONSTANT type="int" value="7">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getIStufeJahrInt" returntype="int">
           </METHOD>
           <OPERATOR type="eq" optype="int">
           </OPERATOR>
           <CONSTANT type="int" value="17">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
         </AND>
         <BOOL_METHOD>
          <METHOD name="getIStufeJahrInt" returntype="int">
          </METHOD>
          <OPERATOR type="greater" optype="int">
           <COMMENT></COMMENT>
          </OPERATOR>
          <CONSTANT type="int" value="17">
           <COMMENT></COMMENT>
          </CONSTANT>
         </BOOL_METHOD>
        </OR>
       </AND>
       <OR>
        <COMMENT>LU-MINI</COMMENT>
        <AND>
         <COMMENT>F54/F60 from 11/17</COMMENT>
         <OR>
          <COMMENT>F54/F60</COMMENT>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F54">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F60">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <OR>
          <COMMENT>SA</COMMENT>
          <BOOL_METHOD>
           <COMMENT>Radio Mini Visual Boost</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="06FP">
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>Navi</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="06UM">
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <OR>
          <COMMENT>from 11/17</COMMENT>
          <AND>
           <BOOL_METHOD>
            <METHOD name="getIStufeMonatInt" returntype="int">
            </METHOD>
            <OPERATOR type="g_equal" optype="int">
             <COMMENT></COMMENT>
            </OPERATOR>
            <CONSTANT type="int" value="11">
             <COMMENT></COMMENT>
            </CONSTANT>
           </BOOL_METHOD>
           <BOOL_METHOD>
            <METHOD name="getIStufeJahrInt" returntype="int">
            </METHOD>
            <OPERATOR type="eq" optype="int">
            </OPERATOR>
            <CONSTANT type="int" value="17">
             <COMMENT></COMMENT>
            </CONSTANT>
           </BOOL_METHOD>
          </AND>
          <BOOL_METHOD>
           <METHOD name="getIStufeJahrInt" returntype="int">
           </METHOD>
           <OPERATOR type="greater" optype="int">
            <COMMENT></COMMENT>
           </OPERATOR>
           <CONSTANT type="int" value="17">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
         </OR>
        </AND>
        <AND>
         <COMMENT>F55/56/57 postponed from 11/17 to 03/18
(gAMS AH0046)</COMMENT>
         <OR>
          <COMMENT>F55/56/57</COMMENT>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F55">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F56">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F57">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <OR>
          <COMMENT>SA</COMMENT>
          <BOOL_METHOD>
           <COMMENT>Radio Mini Visual Boost</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="06FP">
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>Navi</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="06UM">
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <OR>
          <COMMENT>from 03/18</COMMENT>
          <AND>
           <BOOL_METHOD>
            <METHOD name="getIStufeMonatInt" returntype="int">
            </METHOD>
            <OPERATOR type="g_equal" optype="int">
             <COMMENT></COMMENT>
            </OPERATOR>
            <CONSTANT type="int" value="3">
             <COMMENT></COMMENT>
            </CONSTANT>
           </BOOL_METHOD>
           <BOOL_METHOD>
            <METHOD name="getIStufeJahrInt" returntype="int">
            </METHOD>
            <OPERATOR type="eq" optype="int">
            </OPERATOR>
            <CONSTANT type="int" value="18">
             <COMMENT></COMMENT>
            </CONSTANT>
           </BOOL_METHOD>
          </AND>
          <BOOL_METHOD>
           <METHOD name="getIStufeJahrInt" returntype="int">
           </METHOD>
           <OPERATOR type="greater" optype="int">
            <COMMENT></COMMENT>
           </OPERATOR>
           <CONSTANT type="int" value="18">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
         </OR>
        </AND>
       </OR>
      </OR>
      <THEN>
       <ASSIGNMENT literal="SGBD">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="ENAVEVO">
         <COMMENT></COMMENT>
        </CONSTANT>
       </ASSIGNMENT>
      </THEN>
     </IF>
    </GROUP>
   </GROUP>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
 </GROUP>
 <GROUP name="--------------------------------------------------------">
 </GROUP>
 <GROUP name="PLANT SPECIFIC ATTRIBUTES">
  <GROUP name="DEFAULT VARIABLES">
   <COMMENT></COMMENT>
   <ASSIGNMENT literal="VORSERIE">
    <COMMENT>Vordefinition - nicht bef�llen!</COMMENT>
    <CONSTANT type="String" value=";">
     <COMMENT></COMMENT>
    </CONSTANT>
   </ASSIGNMENT>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="ATTRIBUTES SPECIFIC PLANT W0">
   <COMMENT>FIZ - Prototypenbau</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="equalsIgnoreCase" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="01.50">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="leer">
      <COMMENT></COMMENT>
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="ATTRIBUTES SPECIFIC PLANT W1">
   <COMMENT>M�nchen</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="equalsIgnoreCase" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="01.01">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="leer">
      <COMMENT></COMMENT>
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="ATTRIBUTES SPECIFIC PLANT W2">
   <COMMENT>Dingolfing</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="startsWith" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="02">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <IF>
      <COMMENT>F1x</COMMENT>
      <OR>
       <BOOL_METHOD>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F06">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F11">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F12">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F13">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </OR>
      <THEN>
       <IF>
        <COMMENT>Vorserie</COMMENT>
        <OR>
         <COMMENT></COMMENT>
         <AND>
          <BOOL_METHOD>
           <METHOD name="getIStufeMonatInt" returntype="int">
           </METHOD>
           <OPERATOR type="g_equal" optype="int">
           </OPERATOR>
           <CONSTANT type="int" value="7">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getIStufeJahrInt" returntype="int">
           </METHOD>
           <OPERATOR type="eq" optype="int">
           </OPERATOR>
           <CONSTANT type="int" value="18">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
         </AND>
         <BOOL_METHOD>
          <METHOD name="getIStufeJahrInt" returntype="int">
          </METHOD>
          <OPERATOR type="greater" optype="int">
           <COMMENT></COMMENT>
          </OPERATOR>
          <CONSTANT type="int" value="18">
           <COMMENT></COMMENT>
          </CONSTANT>
         </BOOL_METHOD>
        </OR>
        <THEN>
         <ASSIGNMENT literal="VORSERIE">
          <CONSTANT type="String" value="TRUE">
          </CONSTANT>
         </ASSIGNMENT>
        </THEN>
        <ELSE>
         <ASSIGNMENT literal="VORSERIE">
          <CONSTANT type="String" value="FALSE">
          </CONSTANT>
         </ASSIGNMENT>
        </ELSE>
       </IF>
      </THEN>
     </IF>
     <IF>
      <COMMENT>F34 % F36 desicion Ruhestrommessung Vorserie</COMMENT>
      <OR>
       <BOOL_METHOD>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F34">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F36">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </OR>
      <THEN>
       <IF>
        <COMMENT>Vorserie</COMMENT>
        <OR>
         <COMMENT></COMMENT>
         <AND>
          <BOOL_METHOD>
           <METHOD name="getIStufeMonatInt" returntype="int">
           </METHOD>
           <OPERATOR type="g_equal" optype="int">
           </OPERATOR>
           <CONSTANT type="int" value="7">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getIStufeJahrInt" returntype="int">
           </METHOD>
           <OPERATOR type="eq" optype="int">
           </OPERATOR>
           <CONSTANT type="int" value="18">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
         </AND>
         <BOOL_METHOD>
          <METHOD name="getIStufeJahrInt" returntype="int">
          </METHOD>
          <OPERATOR type="greater" optype="int">
           <COMMENT></COMMENT>
          </OPERATOR>
          <CONSTANT type="int" value="18">
           <COMMENT></COMMENT>
          </CONSTANT>
         </BOOL_METHOD>
        </OR>
        <THEN>
         <ASSIGNMENT literal="VORSERIE">
          <CONSTANT type="String" value="TRUE">
          </CONSTANT>
         </ASSIGNMENT>
        </THEN>
        <ELSE>
         <ASSIGNMENT literal="VORSERIE">
          <CONSTANT type="String" value="FALSE">
          </CONSTANT>
         </ASSIGNMENT>
        </ELSE>
       </IF>
      </THEN>
     </IF>
     <IF>
      <COMMENT>GXX</COMMENT>
      <OR>
       <BOOL_METHOD>
        <COMMENT>7 series sedan</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="G11">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>7 series sedan extended wheel</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="G12">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>8 series convertible</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="G14">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>8 series coup�</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="G15">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>8 series grand coup�</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="G15">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>5 series sedan</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="G30">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>5 series touring</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="G31">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>5 series GT</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="G32">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>M5 muss noch ge�ndert werden Karl</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="F90">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </OR>
      <THEN>
       <IF>
        <COMMENT>Vorserie</COMMENT>
        <AND>
         <OR>
          <COMMENT></COMMENT>
          <AND>
           <BOOL_METHOD>
            <METHOD name="getIStufeMonatInt" returntype="int">
            </METHOD>
            <OPERATOR type="g_equal" optype="int">
            </OPERATOR>
            <CONSTANT type="int" value="7">
             <COMMENT></COMMENT>
            </CONSTANT>
           </BOOL_METHOD>
           <BOOL_METHOD>
            <METHOD name="getIStufeJahrInt" returntype="int">
            </METHOD>
            <OPERATOR type="eq" optype="int">
            </OPERATOR>
            <CONSTANT type="int" value="18">
             <COMMENT></COMMENT>
            </CONSTANT>
           </BOOL_METHOD>
          </AND>
          <BOOL_METHOD>
           <METHOD name="getIStufeJahrInt" returntype="int">
           </METHOD>
           <OPERATOR type="greater" optype="int">
            <COMMENT></COMMENT>
           </OPERATOR>
           <CONSTANT type="int" value="18">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
         </OR>
<NOT>
                    <BOOL_METHOD>
           <COMMENT></COMMENT>
          <METHOD name="containsSX" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="00DL">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                </AND>
        <THEN>
         <ASSIGNMENT literal="VORSERIE">
          <CONSTANT type="String" value="TRUE">
          </CONSTANT>
         </ASSIGNMENT>
        </THEN>
        <ELSE>
         <ASSIGNMENT literal="VORSERIE">
          <CONSTANT type="String" value="FALSE">
          </CONSTANT>
         </ASSIGNMENT>
        </ELSE>
       </IF>
      </THEN>
     </IF>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="ATTRIBUTES SPECIFIC PLANT W6">
   <COMMENT>Regensburg</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="startsWith" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="06">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="leer">
      <COMMENT></COMMENT>
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="ATTRIBUTES SPECIFIC PLANT W7">
   <COMMENT>Leipzig</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="startsWith" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="07">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="leer">
      <COMMENT></COMMENT>
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="ATTRIBUTES SPECIFIC PLANT W9">
   <COMMENT>S�dafrika</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="startsWith" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="09">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="leer">
      <COMMENT></COMMENT>
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="ATTRIBUTES SPECIFIC PLANT W10">
   <COMMENT>Spartanburg</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="startsWith" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="10">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="leer">
      <COMMENT></COMMENT>
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="ATTRIBUTES SPECIFIC PLANT W19.10">
   <COMMENT>Dadong</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="equalsIgnoreCase" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="19.10">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="leer">
      <COMMENT></COMMENT>
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="ATTRIBUTES SPECIFIC PLANT W19.88">
   <COMMENT>Tiexi</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="equalsIgnoreCase" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="19.88">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="leer">
      <COMMENT></COMMENT>
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="ATTRIBUTES SPECIFIC PLANT W34">
   <COMMENT>Oxford</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="startsWith" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="34">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="leer">
      <COMMENT></COMMENT>
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="ATTRIBUTES SPECIFIC PLANT W50">
   <COMMENT>Goodwood</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="startsWith" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="50">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="leer">
      <COMMENT></COMMENT>
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
 </GROUP>
 <GROUP name="--------------------------------------------------------">
 </GROUP>
</ATTRIBUTDEFINITION>
 * ATTRIBUTDEFINITION_XML END
 */
	   // FOLGENDER CODE WIRD AUTOMATISCH NEU ERZEUGT,
	   // NICHT �NDERN
	   // ATTRIBUTDEFINITION_CODE START
super.attributeInit();
attribute_1();
attribute_2();
attribute_3();
attribute_4();
attribute_28();
attribute_29();
attribute_84();
}
private void attribute_1() {

}
private void attribute_2() {
setAttribut( "CONTACT_FIZ_FUNKTION", "Mario.MF.Friedrich@bmw.de" );
setAttribut( "CONTACT_MAIL_FIZ", "Mario.MF.Friedrich@bmw.de" );
setAttribut( "CONTACT_MAIL_W1", "Siegfried.Heinzmann@bmw.de" );
setAttribut( "CONTACT_MAIL_W2", "Manfred.Kronbeck@bmw.de" );
setAttribut( "CONTACT_MAIL_W6", "Erwin.Leidel@bmw.de" );
setAttribut( "CONTACT_MAIL_W7", "Hendrik.Hesse@bmw.de" );
setAttribut( "CONTACT_MAIL_W9", "Lucien.Jordaan@bmw.co.za"+ ";" + "siphiwe.mathebula@bmw.co.za" );
setAttribut( "CONTACT_MAIL_W10", "Jeffrey.Fann@bmwmc.com" );
setAttribut( "CONTACT_MAIL_W1910", "Xinyue.Wu@bmw-brilliance.cn" );
setAttribut( "CONTACT_MAIL_W1988", "Xiuli.Xu@bmw-brilliance.cn" );
setAttribut( "CONTACT_MAIL_W30", "Marco.Gutierrez@bmw.com.mx" );
setAttribut( "CONTACT_MAIL_W34", "Jaap.van-der-Werf@bmwgroup.com" );
setAttribut( "CONTACT_MAIL_W50", "luke.brocks@rolls-roycemotorcars.com" );
setAttribut( "CONTACT_MAIL_W0530", "Cascade.magnasteyr@magna.com" );
setAttribut( "CONTACT_MAIL_W58", "Cascade@vdlnedcar.nl" );
setAttribut( "CONTACT_MAIL_GENERAL", "n.r." );
setAttribut( "FACHTEAM", "Interaktion & Energie" );

}
private void attribute_3() {

}
private void attribute_5() {

}
private void attribute_7() {
setAttribut( "QUESTION_TEXT_BOOT_LIGHT", (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"default":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"default":"default")) );
setAttribut( "QUESTION_TEXT_TAILGATE_LIGHT", (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"default":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"default":"default")) );
setAttribut( "QUESTION_TEXT_MSA_BUTTON", (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"default":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"default":"default")) );

}
private void attribute_8() {
setAttribut( "QUESTION_TEXT_BOOT_LIGHT", (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Both Lower Boot Lights Illuminated?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"2 Gep�ckraumleuchten i.O.?":"2 Gep�ckraumleuchten i.O.?")) );

}
private void attribute_9() {
setAttribut( "QUESTION_TEXT_BOOT_LIGHT", (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Boot light illuminated?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"1 Gep�ckraumleuchte i.O.?":"1 Gep�ckraumleuchte i.O.?")) );

}
private void attribute_11() {
setAttribut( "QUESTION_TEXT_TAILGATE_LIGHT", (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Hatch light left and right: OK?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Heckklappenlicht li+re: i.O.?":"Heckklappenlicht li+re: i.O.?")) );

}
private void attribute_12() {
setAttribut( "QUESTION_TEXT_TAILGATE_LIGHT", (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Tailgate light left and right: OK?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Heckklappenlicht li+re: i.O.?":"Heckklappenlicht li+re: i.O.?")) );

}
private void attribute_10() {
if( auftrag.getIStufe().equals("S18"))
attribute_11();
else
attribute_12();

}
private void attribute_14() {
setAttribut( "QUESTION_TEXT_TAILGATE_LIGHT", (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Hatch Light Illuminated?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"1 Heckklappenlicht i.O.?":"1 Heckklappenlicht i.O.?")) );

}
private void attribute_15() {
setAttribut( "QUESTION_TEXT_TAILGATE_LIGHT", (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Tailgate Light Illuminated?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"1 Heckklappenlicht i.O.?":"1 Heckklappenlicht i.O.?")) );

}
private void attribute_13() {
if( auftrag.getIStufe().equals("S18"))
attribute_14();
else
attribute_15();

}
private void attribute_16() {
setAttribut( "QUESTION_TEXT_MSA_BUTTON", (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"start-stop-button without MSA-button installed?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Start-Stop-Taster ohne MSA-Taste verbaut?":"Start-Stop-Taster ohne MSA-Taste verbaut?")) );

}
private void attribute_18() {
setAttribut( "QUESTION_TEXT_MSA_BUTTON", (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"HYBRID: NO MSA-button installed?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"HYBRID: KEINE MSA-Taste verbaut?":"HYBRID: KEINE MSA-Taste verbaut?")) );

}
private void attribute_20() {
setAttribut( "QUESTION_TEXT_MSA_BUTTON", (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"ALPINA: NO MSA-button installed?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"ALPINA: KEINE MSA-Taste verbaut?":"ALPINA: KEINE MSA-Taste verbaut?")) );

}
private void attribute_21() {
setAttribut( "QUESTION_TEXT_MSA_BUTTON", (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"start-stop-button with MSA-button installed?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Start-Stop-Taster mit  MSA-Taste verbaut?":"Start-Stop-Taster mit  MSA-Taste verbaut?")) );

}
private void attribute_19() {
if( (auftrag.getBaureihe().equalsIgnoreCase("G12") && auftrag.containsSA("09XA") && auftrag.getL�nderVariante().startsWith("US")))
attribute_20();
else
attribute_21();

}
private void attribute_17() {
if( (auftrag.getBaureihe().startsWith("G") && auftrag.getHybrid().equals("PHEV")))
attribute_18();
else
attribute_19();

}
private void attribute_6() {
attribute_7();
if( ((auftrag.getBaureihe().equalsIgnoreCase("F95") || auftrag.getBaureihe().equalsIgnoreCase("F96") || auftrag.getBaureihe().equalsIgnoreCase("F97") || auftrag.getBaureihe().equalsIgnoreCase("F98") || auftrag.getBaureihe().equalsIgnoreCase("G01") || auftrag.getBaureihe().equalsIgnoreCase("G02") || auftrag.getBaureihe().equalsIgnoreCase("G05") || auftrag.getBaureihe().equalsIgnoreCase("G06") || auftrag.getBaureihe().equalsIgnoreCase("G07") || auftrag.getBaureihe().equalsIgnoreCase("G08") || auftrag.getBaureihe().equalsIgnoreCase("G21") || auftrag.getBaureihe().equalsIgnoreCase("G26") || auftrag.getBaureihe().equalsIgnoreCase("G31") || auftrag.getBaureihe().equalsIgnoreCase("G32")) || (auftrag.getBaureihe().equalsIgnoreCase("RR4") || auftrag.getBaureihe().equalsIgnoreCase("RR5") || auftrag.getBaureihe().equalsIgnoreCase("RR6") || auftrag.getBaureihe().equalsIgnoreCase("RR11") || auftrag.getBaureihe().equalsIgnoreCase("RR12"))))
attribute_8();
else
attribute_9();
if( (auftrag.getBaureihe().equalsIgnoreCase("F95") || auftrag.getBaureihe().equalsIgnoreCase("F96") || auftrag.getBaureihe().equalsIgnoreCase("G05") || auftrag.getBaureihe().equalsIgnoreCase("G06") || auftrag.getBaureihe().equalsIgnoreCase("G07") || auftrag.getBaureihe().equalsIgnoreCase("G21") || auftrag.getBaureihe().equalsIgnoreCase("G26")))
attribute_10();
else
attribute_13();
if( (auftrag.getBaureihe().equalsIgnoreCase("F30") && (auftrag.getHybrid().equals("HYBR") || auftrag.getHybrid().equals("PHEV"))))
attribute_16();
else
attribute_17();

}
private void attribute_22() {

}
private void attribute_26() {
setAttribut( "SGBD", "ENAVEVO" );

}
private void attribute_25() {
if( ((((auftrag.getBaureihe().equalsIgnoreCase("F32") || auftrag.getBaureihe().equalsIgnoreCase("F33") || auftrag.getBaureihe().equalsIgnoreCase("F36") || auftrag.getBaureihe().equalsIgnoreCase("F82") || auftrag.getBaureihe().equalsIgnoreCase("F83")) && (!(auftrag.containsSA("0601")) && !(auftrag.containsSA("0602")) && !(auftrag.containsSA("0609")) && !(auftrag.containsSA("06UF"))) && ((auftrag.getIStufeMonatInt()>=3 && auftrag.getIStufeJahrInt()==17) || auftrag.getIStufeJahrInt()>17)) || ((auftrag.getBaureihe().equalsIgnoreCase("F30") || auftrag.getBaureihe().equalsIgnoreCase("F31") || auftrag.getBaureihe().equalsIgnoreCase("F34") || auftrag.getBaureihe().equalsIgnoreCase("F35") || auftrag.getBaureihe().equalsIgnoreCase("F80")) && (!(auftrag.containsSA("0601")) && !(auftrag.containsSA("0602")) && !(auftrag.containsSA("0609")) && !(auftrag.containsSA("06UF"))) && ((auftrag.getIStufeMonatInt()>=7 && auftrag.getIStufeJahrInt()==17) || auftrag.getIStufeJahrInt()>17)) || ((auftrag.getBaureihe().equalsIgnoreCase("F20") || auftrag.getBaureihe().equalsIgnoreCase("F21") || auftrag.getBaureihe().equalsIgnoreCase("F22") || auftrag.getBaureihe().equalsIgnoreCase("F23") || auftrag.getBaureihe().equalsIgnoreCase("F87") || auftrag.getBaureihe().equalsIgnoreCase("F88")) && (!(auftrag.containsSA("0601")) && !(auftrag.containsSA("0602")) && !(auftrag.containsSA("0609")) && !(auftrag.containsSA("06UF"))) && ((auftrag.getIStufeMonatInt()>=7 && auftrag.getIStufeJahrInt()==17) || auftrag.getIStufeJahrInt()>17))) || ((auftrag.getBaureihe().equalsIgnoreCase("G20") || auftrag.getBaureihe().equalsIgnoreCase("G21") || auftrag.getBaureihe().equalsIgnoreCase("G22") || auftrag.getBaureihe().equalsIgnoreCase("G23") || auftrag.getBaureihe().equalsIgnoreCase("G24") || auftrag.getBaureihe().equalsIgnoreCase("G26") || auftrag.getBaureihe().equalsIgnoreCase("G28") || auftrag.getBaureihe().equalsIgnoreCase("G29") || auftrag.getBaureihe().equalsIgnoreCase("G30") || auftrag.getBaureihe().equalsIgnoreCase("G31") || auftrag.getBaureihe().equalsIgnoreCase("G32") || auftrag.getBaureihe().equalsIgnoreCase("G38") || auftrag.getBaureihe().equalsIgnoreCase("G01") || auftrag.getBaureihe().equalsIgnoreCase("G02") || auftrag.getBaureihe().equalsIgnoreCase("G08") || auftrag.getBaureihe().equalsIgnoreCase("F97") || auftrag.getBaureihe().equalsIgnoreCase("F98")) && (!(auftrag.containsSA("06FS")) && !(auftrag.containsSA("0601")) && !(auftrag.containsSA("0602")) && !(auftrag.containsSA("0609")) && !(auftrag.containsSA("06UF"))) && ((auftrag.getIStufeNummerInt()>=330 && auftrag.getIStufeMonatInt()==11 && auftrag.getIStufeJahrInt()==16) || (auftrag.getIStufeMonatInt()>11 && auftrag.getIStufeJahrInt()==16) || auftrag.getIStufeJahrInt()>16) && !(auftrag.containsSA("06VB"))) || (auftrag.getBaureihe().equalsIgnoreCase("I01") && (!(auftrag.containsSA("06FS")) && !(auftrag.containsSA("0601")) && !(auftrag.containsSA("0602")) && !(auftrag.containsSA("0609"))) && ((auftrag.getIStufeMonatInt()>=11 && auftrag.getIStufeJahrInt()==18) || auftrag.getIStufeJahrInt()>18)) || ((auftrag.getBaureihe().startsWith("F4") || auftrag.getBaureihe().equalsIgnoreCase("F39") || auftrag.getBaureihe().equalsIgnoreCase("F52")) && (!(auftrag.containsSA("06FS")) && !(auftrag.containsSA("0601")) && !(auftrag.containsSA("0602")) && !(auftrag.containsSA("0609"))) && ((auftrag.getIStufeMonatInt()>=7 && auftrag.getIStufeJahrInt()==17) || auftrag.getIStufeJahrInt()>17)) || (((auftrag.getBaureihe().equalsIgnoreCase("F54") || auftrag.getBaureihe().equalsIgnoreCase("F60")) && (auftrag.containsSA("06FP") || auftrag.containsSA("06UM")) && ((auftrag.getIStufeMonatInt()>=11 && auftrag.getIStufeJahrInt()==17) || auftrag.getIStufeJahrInt()>17)) || ((auftrag.getBaureihe().equalsIgnoreCase("F55") || auftrag.getBaureihe().equalsIgnoreCase("F56") || auftrag.getBaureihe().equalsIgnoreCase("F57")) && (auftrag.containsSA("06FP") || auftrag.containsSA("06UM")) && ((auftrag.getIStufeMonatInt()>=3 && auftrag.getIStufeJahrInt()==18) || auftrag.getIStufeJahrInt()>18)))))
attribute_26();

}
private void attribute_24() {
setAttribut( "SGBD", "NBTEVO" );
attribute_25();

}
private void attribute_23() {
attribute_24();

}
private void attribute_27() {

}
private void attribute_4() {
attribute_5();
attribute_6();
attribute_22();
attribute_23();
attribute_27();

}
private void attribute_28() {

}
private void attribute_30() {
setAttribut( "VORSERIE", ";" );

}
private void attribute_31() {

}
private void attribute_34() {

}
private void attribute_33() {
attribute_34();

}
private void attribute_32() {
if( auftrag.getOrderWerk().equalsIgnoreCase("01.50"))
attribute_33();

}
private void attribute_35() {

}
private void attribute_38() {

}
private void attribute_37() {
attribute_38();

}
private void attribute_36() {
if( auftrag.getOrderWerk().equalsIgnoreCase("01.01"))
attribute_37();

}
private void attribute_39() {

}
private void attribute_43() {
setAttribut( "VORSERIE", "TRUE" );

}
private void attribute_44() {
setAttribut( "VORSERIE", "FALSE" );

}
private void attribute_42() {
if( ((auftrag.getIStufeMonatInt()>=7 && auftrag.getIStufeJahrInt()==18) || auftrag.getIStufeJahrInt()>18))
attribute_43();
else
attribute_44();

}
private void attribute_46() {
setAttribut( "VORSERIE", "TRUE" );

}
private void attribute_47() {
setAttribut( "VORSERIE", "FALSE" );

}
private void attribute_45() {
if( ((auftrag.getIStufeMonatInt()>=7 && auftrag.getIStufeJahrInt()==18) || auftrag.getIStufeJahrInt()>18))
attribute_46();
else
attribute_47();

}
private void attribute_49() {
setAttribut( "VORSERIE", "TRUE" );

}
private void attribute_50() {
setAttribut( "VORSERIE", "FALSE" );

}
private void attribute_48() {
if( (((auftrag.getIStufeMonatInt()>=7 && auftrag.getIStufeJahrInt()==18) || auftrag.getIStufeJahrInt()>18) && !(auftrag.containsSX("00DL"))))
attribute_49();
else
attribute_50();

}
private void attribute_41() {
if( (auftrag.getBaureihe().equalsIgnoreCase("F06") || auftrag.getBaureihe().equalsIgnoreCase("F11") || auftrag.getBaureihe().equalsIgnoreCase("F12") || auftrag.getBaureihe().equalsIgnoreCase("F13")))
attribute_42();
if( (auftrag.getBaureihe().equalsIgnoreCase("F34") || auftrag.getBaureihe().equalsIgnoreCase("F36")))
attribute_45();
if( (auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12") || auftrag.getBaureihe().equalsIgnoreCase("G14") || auftrag.getBaureihe().equalsIgnoreCase("G15") || auftrag.getBaureihe().equalsIgnoreCase("G15") || auftrag.getBaureihe().equalsIgnoreCase("G30") || auftrag.getBaureihe().equalsIgnoreCase("G31") || auftrag.getBaureihe().equalsIgnoreCase("G32") || auftrag.getBaureihe().equalsIgnoreCase("F90")))
attribute_48();

}
private void attribute_40() {
if( auftrag.getOrderWerk().startsWith("02"))
attribute_41();

}
private void attribute_51() {

}
private void attribute_54() {

}
private void attribute_53() {
attribute_54();

}
private void attribute_52() {
if( auftrag.getOrderWerk().startsWith("06"))
attribute_53();

}
private void attribute_55() {

}
private void attribute_58() {

}
private void attribute_57() {
attribute_58();

}
private void attribute_56() {
if( auftrag.getOrderWerk().startsWith("07"))
attribute_57();

}
private void attribute_59() {

}
private void attribute_62() {

}
private void attribute_61() {
attribute_62();

}
private void attribute_60() {
if( auftrag.getOrderWerk().startsWith("09"))
attribute_61();

}
private void attribute_63() {

}
private void attribute_66() {

}
private void attribute_65() {
attribute_66();

}
private void attribute_64() {
if( auftrag.getOrderWerk().startsWith("10"))
attribute_65();

}
private void attribute_67() {

}
private void attribute_70() {

}
private void attribute_69() {
attribute_70();

}
private void attribute_68() {
if( auftrag.getOrderWerk().equalsIgnoreCase("19.10"))
attribute_69();

}
private void attribute_71() {

}
private void attribute_74() {

}
private void attribute_73() {
attribute_74();

}
private void attribute_72() {
if( auftrag.getOrderWerk().equalsIgnoreCase("19.88"))
attribute_73();

}
private void attribute_75() {

}
private void attribute_78() {

}
private void attribute_77() {
attribute_78();

}
private void attribute_76() {
if( auftrag.getOrderWerk().startsWith("34"))
attribute_77();

}
private void attribute_79() {

}
private void attribute_82() {

}
private void attribute_81() {
attribute_82();

}
private void attribute_80() {
if( auftrag.getOrderWerk().startsWith("50"))
attribute_81();

}
private void attribute_83() {

}
private void attribute_29() {
attribute_30();
attribute_31();
attribute_32();
attribute_35();
attribute_36();
attribute_39();
attribute_40();
attribute_51();
attribute_52();
attribute_55();
attribute_56();
attribute_59();
attribute_60();
attribute_63();
attribute_64();
attribute_67();
attribute_68();
attribute_71();
attribute_72();
attribute_75();
attribute_76();
attribute_79();
attribute_80();
attribute_83();

}
private void attribute_84() {

}

	// ATTRIBUTDEFINITION_CODE END

  /**
   * Initialisieren der Pr�fprozeduren
   */
  protected void pr�fprozedurenInit() throws PruefprozedurNotAvailableException {
    super.pr�fprozedurenInit();
/*
 * DIESER KOMMENTAR WIRD AUTOMATISCH ERZEUGT UND GELESEN
 * NICHT VER�NDERN!
 * PRUEFPROZEDURDEFINITION_XML START
<PRUEFPROZEDUREN version="2">
 <GROUP name="------------------------------------------------">
  <COMMENT></COMMENT>
 </GROUP>
 <GROUP name="GLOBAL JOBS">
  <COMMENT></COMMENT>
  <GROUP name="------------------------------------------------">
   <COMMENT></COMMENT>
  </GROUP>
  <GROUP name="ANWEISUNGEN">
   <COMMENT></COMMENT>
   <LOADPP fileheader="WerkerAnweisung" name="WA_FENSTER_NICHT_INIT" dename="WA_FENSTER_NICHT_INIT" enname="WA_WINDOWS_NOT_INIT">
    <COMMENT></COMMENT>
    <PPARGS name="DAUER" required="true">
     <CONSTANT type="String" value="5000">
      <COMMENT></COMMENT>
     </CONSTANT>
    </PPARGS>
    <PPARGS name="AWT" required="true">
     <CONSTANT type="String" value="Die Fenster k�nnen nicht initialisieren" devalue="Die Fenster k�nnen nicht initialisieren" envalue="The windows can not initialize">
      <COMMENT></COMMENT>
     </CONSTANT>
    </PPARGS>
   </LOADPP>
  </GROUP>
  <GROUP name="------------------------------------------------">
   <COMMENT></COMMENT>
  </GROUP>
  <GROUP name="GETRIEBE / GEARBOX">
   <COMMENT></COMMENT>
   <LOADPP fileheader="DiagData" name="GETRIEBE_P_NIO">
    <COMMENT>Erzeugen eines NIO-Schrittes f�r die mech. Schlosspr�fung</COMMENT>
    <PPARGS name="JOBN_SGBD" required="false">
     <COMMENT></COMMENT>
     <CONSTANT type="String" value="NONE">
      <COMMENT></COMMENT>
     </CONSTANT>
    </PPARGS>
    <PPARGS name="JOBN_JOB" required="false">
     <COMMENT></COMMENT>
     <CONSTANT type="String" value="NONE">
      <COMMENT></COMMENT>
     </CONSTANT>
    </PPARGS>
    <PPARGS name="JOB1_CONDITION1" required="false">
     <COMMENT></COMMENT>
     <CONSTANT type="String" value="F">
      <COMMENT></COMMENT>
     </CONSTANT>
    </PPARGS>
    <PPARGS name="JOB1_HWT" required="false">
     <COMMENT></COMMENT>
     <CONSTANT type="String" value="T�rkontakt Fahrer defekt oder Getriebe nicht in P" devalue="T�rkontakt Fahrer defekt oder Getriebe nicht in P" envalue="contact door driverside defect or gearbox not P">
      <COMMENT></COMMENT>
     </CONSTANT>
    </PPARGS>
   </LOADPP>
  </GROUP>
  <GROUP name="------------------------------------------------">
   <COMMENT></COMMENT>
  </GROUP>
  <GROUP name="STROMMESSUNG / CURRENT MEASUREMENT">
   <COMMENT></COMMENT>
   <GROUP name="------------------------------------------------">
    <COMMENT></COMMENT>
   </GROUP>
   <GROUP name="FRONTSCHEIBENHEIZUNG / WINDSCREEN HEATER">
    <COMMENT></COMMENT>
    <IF>
     <COMMENT>nur MINI</COMMENT>
     <AND>
      <COMMENT>(F54, F55, F56, F57, F60, F61) und SA0359</COMMENT>
      <OR>
       <COMMENT>LU MINI Derivate (F54, F55, F56, F57, F60, F61)</COMMENT>
       <BOOL_METHOD>
        <COMMENT>Clubman</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="F54">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>5 T�rer</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="F55">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>3 T�rer</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="F56">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>Cabrio</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="F57">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>Countryman</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="F60">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>Paceman</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="F61">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </OR>
      <BOOL_METHOD>
       <COMMENT>Sichtpaket</COMMENT>
       <METHOD name="containsSA" returntype="boolean">
        <COMMENT></COMMENT>
        <PARAMS>
         <CONSTANT type="String" value="0359">
         </CONSTANT>
        </PARAMS>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
     </AND>
     <THEN>
      <LOADPP fileheader="DiagData" name="FRONTSCHEIBENHEIZUNG_VORHANDEN">
       <COMMENT>PS wird immer IO</COMMENT>
       <PPARGS name="JOBN_SGBD" required="false">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="NONE">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="JOB1_JOB" required="false">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="NONE">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="JOB1_CONDITION1" required="false">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="EQUALS(1,1)">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
     </THEN>
     <ELSE>
      <LOADPP fileheader="DiagData" name="FRONTSCHEIBENHEIZUNG_VORHANDEN">
       <COMMENT>PS wird immer NIO</COMMENT>
       <PPARGS name="JOBN_SGBD" required="false">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="NONE">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="JOB1_JOB" required="false">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="NONE">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="JOB1_CONDITION1" required="false">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="EQUALS(0,1)">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
     </ELSE>
    </IF>
   </GROUP>
   <GROUP name="------------------------------------------------">
    <COMMENT></COMMENT>
   </GROUP>
   <GROUP name="PTC">
    <COMMENT></COMMENT>
    <IF>
     <OR>
      <BOOL_METHOD>
       <COMMENT>Dieselmotor

Diesel engine</COMMENT>
       <METHOD name="getAntriebsKonzeption" returntype="String">
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <PARAMS>
          <CONSTANT type="String" value="D">
           <COMMENT>Dieselmotor

Diesel engine</COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <AND>
       <COMMENT>PTC im G12 mit 4-Zonen auch f�r Benzinmotor verbaut</COMMENT>
       <BOOL_METHOD>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="G12">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>4-zonig (Fondklima)</COMMENT>
        <METHOD name="containsSA" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="04NB">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
<NOT>
                <BOOL_METHOD>
         <COMMENT>Hybrid Merkmal
HYBR = Hybrid
NOHY = kein Hybrid
PHEV = Plug-In Hybrid</COMMENT>
        <METHOD name="getHybrid" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="PHEV">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
            </AND>
     </OR>
     <THEN>
      <LOADPP fileheader="DiagData" name="PTC_VORHANDEN">
       <COMMENT>PS wird immer IO</COMMENT>
       <PPARGS name="JOBN_SGBD" required="false">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="NONE">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="JOB1_JOB" required="false">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="NONE">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="JOB1_CONDITION1" required="false">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="EQUALS(1,1)">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
     </THEN>
     <ELSE>
      <LOADPP fileheader="DiagData" name="PTC_VORHANDEN">
       <COMMENT>PS wird immer NIO</COMMENT>
       <PPARGS name="JOBN_SGBD" required="false">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="NONE">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="JOB1_JOB" required="false">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="NONE">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="JOB1_CONDITION1" required="false">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="EQUALS(0,1)">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
     </ELSE>
    </IF>
   </GROUP>
   <GROUP name="------------------------------------------------">
    <COMMENT></COMMENT>
   </GROUP>
   <GROUP name="ZIGARETTENANZUENDER / CIGARETTE LIGHTER">
    <COMMENT></COMMENT>
   </GROUP>
   <GROUP name="------------------------------------------------">
    <COMMENT></COMMENT>
   </GROUP>
   <GROUP name="EFII WORKAROUND">
    <COMMENT>03/2018</COMMENT>
    <IF>
     <AND>
      <OR>
       <COMMENT>Produktlinie LU</COMMENT>
       <BOOL_METHOD>
        <COMMENT></COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F39">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>SOP: 07/14</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="F45">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>SOP: 11/14</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="F46">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>SOP: 07/15</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="F48">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>SOP:</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="F49">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="startsWith" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="F5">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="startsWith" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="F6">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT></COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="M13">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </OR>
      <AND>
       <COMMENT>03/2018</COMMENT>
       <BOOL_METHOD>
        <METHOD name="getZeitkriteriumMonatInt" returntype="int">
        </METHOD>
        <OPERATOR type="eq" optype="int">
        </OPERATOR>
        <CONSTANT type="int" value="3">
        </CONSTANT>
       </BOOL_METHOD>
       <BOOL_METHOD>
        <METHOD name="getZeitkriteriumJahrInt" returntype="int">
        </METHOD>
        <OPERATOR type="eq" optype="int">
        </OPERATOR>
        <CONSTANT type="int" value="18">
        </CONSTANT>
       </BOOL_METHOD>
      </AND>
      <BOOL_METHOD>
       <COMMENT>Dieselmotor

Diesel engine</COMMENT>
       <METHOD name="getAntriebsKonzeption" returntype="String">
        <METHOD name="equalsIgnoreCase" returntype="boolean">
         <PARAMS>
          <CONSTANT type="String" value="D">
           <COMMENT>Dieselmotor

Diesel engine</COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
     </AND>
     <THEN>
      <LOADPP fileheader="DiagToleranz" name="STEUERN_EFII_WORKAROUND">
       <COMMENT></COMMENT>
       <PPARGS name="SGBD" required="true">
        <CONSTANT type="String" value="AEP">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="JOB" required="true">
        <CONSTANT type="String" value="STEUERN_VERBRAUCHERSTROM_EFII">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="JOBPAR" required="false">
        <CONSTANT type="String" value="1000">
         <COMMENT>Starttriggerwert (in mA) / trigger current level, in 10mA steps</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="0">
         <COMMENT>Ausschalttrigger (in mA) / stop trigger current level, in 10mA steps</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="200">
         <COMMENT>Tot-Zeit (in ms) / dead time, in 10ms steps, max. 10000ms</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="100">
         <COMMENT>Mess-Zeit (in ms) / measurement time, in 10ms steps, max. 10000ms</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="80000">
         <COMMENT>Untere Sromschwelle (in mA) / lower current level, in 10mA steps</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="100000">
         <COMMENT>Obere Sromschwelle (in mA) / upper current level, in 10mA steps</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="0">
         <COMMENT>Messpunkte / Measurement dots, max. 254
0 -&gt; Mittelwertmessung
0 -&gt; average measurement</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="30">
         <COMMENT>Triggerfilter (in ms) / trigger filter, 5ms steps, vorl�ufig max. 50ms</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="20">
         <COMMENT>Messfilter (in ms) / measurement filter, 0.005s steps, vorl�ufig max. 50ms</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="10">
         <COMMENT>Timeout (in Sekunden) / timeout in seconds, 1s steps, max. 254s</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="400">
         <COMMENT>dead time + measurement time + 100ms
Posttrigger (in ms) / posttrigger in seconds, 25ms steps, max. 6000ms
0 -&gt; keine Transientenaufzeichnung
0 -&gt; no recording</COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="HWT" required="false">
        <CONSTANT type="String" value="Heckscheibenheizung;Aktivierung EFII" devalue="Heckscheibenheizung;Aktivierung EFII" envalue="Rearwindow heating;Activation EFII">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="Pause" name="WAIT_100MS_EFII_WORKAROUND">
       <COMMENT>100ms Wartezeit ohne HWT

100ms waiting time without screen display</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="100">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="DiagToleranz" name="STOP_EFII_WORKAROUND">
       <COMMENT>Stop Routine f�r die EFII vERBRAUCHER Strommessung (ab IBS2011)
END Routine for the EFII current CONSUMER measurement (from IBS2011 on)</COMMENT>
       <PPARGS name="SGBD" required="true">
        <CONSTANT type="String" value="AEP">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="JOB" required="true">
        <CONSTANT type="String" value="STEUERN_ENDE_VERBRAUCHERSTROM_EFII">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="HWT" required="false">
        <CONSTANT type="String" value="Strommessung;STOP EFII" devalue="Strommessung;STOP EFII" envalue="Current measure;STOP EFII">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
     </THEN>
    </IF>
   </GROUP>
  </GROUP>
  <GROUP name="------------------------------------------------">
   <COMMENT></COMMENT>
  </GROUP>
  <GROUP name="STROMMESSZANGE / CURRENT PROBE">
   <COMMENT></COMMENT>
   <GROUP name="------------------------------------------------">
    <COMMENT></COMMENT>
   </GROUP>
   <GROUP name="TRANSIENTENAUFZEICHNUNG / TRANSIENT HANDLING">
    <COMMENT></COMMENT>
    <LOADPP fileheader="UiLogStart" name="START_TRANSIENT_AUFZEICHNUNG">
     <PPARGS name="MAX_LOG" required="false">
      <COMMENT>Maximale Aufzeichnungszeit in Sekunden</COMMENT>
      <CONSTANT type="String" value="1800">
      </CONSTANT>
     </PPARGS>
    </LOADPP>
    <LOADPP fileheader="UiLogEnd" name="STOP_TRANSIENT_AUFZEICHNUNG">
    </LOADPP>
   </GROUP>
   <GROUP name="------------------------------------------------">
    <COMMENT></COMMENT>
   </GROUP>
   <GROUP name="STROMMESSZANGE ANKLEMMEN / CONNECT CURRENT PROBE">
    <COMMENT></COMMENT>
    <LOADPP fileheader="UiInit" name="UI_INIT">
     <PPARGS name="PROBE_MESSAGE" required="false">
      <CONSTANT type="String" value="MASSELEITUNG;UMSCHLIESSEN!" devalue="MASSELEITUNG;UMSCHLIESSEN!" envalue="CLASP PROBE ARROUND;BLACK BATTERY CABLE">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="MIN_I" required="true">
      <CONSTANT type="String" value="-1000">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="MAX_I" required="true">
      <CONSTANT type="String" value="1000">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="MIN_U" required="true">
      <CONSTANT type="String" value="8000">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="MAX_U" required="true">
      <CONSTANT type="String" value="15000">
      </CONSTANT>
     </PPARGS>
    </LOADPP>
   </GROUP>
   <GROUP name="------------------------------------------------">
    <COMMENT></COMMENT>
   </GROUP>
   <GROUP name="RUHESTROMMESSUNG / QUIESCENT CURRENT MEASUREMENT">
    <COMMENT></COMMENT>
    <LOADPP fileheader="UiToleranzTotal" name="UI_RUHESTROMMESSUNG" dename="UI_RUHESTROMMESSUNG" enname="UI_QUIESCENT_CURRENT_MEASUREMENT">
     <COMMENT>Durchf�hrung einer Ruhestrommessung 

Run of the quiescent current measurement</COMMENT>
     <PPARGS name="RESULT1" required="true">
      <CONSTANT type="String" value="I">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="MIN_GET1" required="true">
      <CONSTANT type="String" value="0">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="MAX_GET1" required="true">
      <CONSTANT type="String" value="80">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="DAUER" required="true">
      <CONSTANT type="String" value="30000">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="TRIGGER_FILTER" required="true">
      <CONSTANT type="String" value="50">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="MESS_FILTER" required="true">
      <CONSTANT type="String" value="50">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="AWT" required="true">
      <CONSTANT type="String" value="Sleep Current Measurement" devalue="Ruhestrommessung" envalue="Sleep Current Measurement">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="HWT" required="true">
      <CONSTANT type="String" value="Sleep Current Measurement;Failure" devalue="Ruhestrommessung;Fehler!" envalue="Sleep Current Measurement;Failure">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="PUNKT" required="false">
      <CONSTANT type="String" value="500">
       <COMMENT></COMMENT>
      </CONSTANT>
     </PPARGS>
     <PPARGS name="MODE" required="false">
      <CONSTANT type="String" value="3">
      </CONSTANT>
     </PPARGS>
    </LOADPP>
    <LOADPP fileheader="WerkerQuittung" name="WQ_SLEEP_CURRENT_MEASURED_BY_CURRENT_PROBE" dename="WQ_RUHESTROM_GEMESSEN_MIT_STROMMESSZANGE" enname="WQ_SLEEP_CURRENT_MEASURED_BY_CURRENT_PROBE">
     <COMMENT></COMMENT>
     <PPARGS name="AWT" required="true">
      <CONSTANT type="String" value="Ruhestrom laut SMZ:;I(mA)@UI_RUHESTROMMESSUNG; mA">
       <COMMENT></COMMENT>
      </CONSTANT>
     </PPARGS>
     <PPARGS name="STYLE" required="true">
      <CONSTANT type="String" value="1">
       <COMMENT></COMMENT>
      </CONSTANT>
     </PPARGS>
    </LOADPP>
    <LOADPP fileheader="UiToleranzTotal" name="UI_RUHESTROMMESSUNG_START" dename="UI_RUHESTROMMESSUNG_START" enname="UI_QUIESCENT_CURRENT_MEASUREMENT_START">
     <COMMENT>Start der Ruhestrommessung

Start of the quiescent current measurement</COMMENT>
     <PPARGS name="RESULT1" required="true">
      <CONSTANT type="String" value="I">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="MIN_GET1" required="true">
      <CONSTANT type="String" value="0">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="MAX_GET1" required="true">
      <CONSTANT type="String" value="80">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="DAUER" required="true">
      <CONSTANT type="String" value="3000">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="TRIGGER_FILTER" required="true">
      <CONSTANT type="String" value="50">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="MESS_FILTER" required="true">
      <CONSTANT type="String" value="50">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="AWT" required="true">
      <CONSTANT type="String" value="Sleep Current Measurement;START" devalue="Ruhestrommessung;START" envalue="Sleep Current Measurement;START">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="HWT" required="true">
      <CONSTANT type="String" value="Sleep Current Measurement;Failure" devalue="Ruhestrommessung;Fehler!" envalue="Sleep Current Measurement;Failure">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="WAIT" required="false">
      <CONSTANT type="String" value="50">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="PUNKT" required="false">
      <CONSTANT type="String" value="20">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="MODE" required="false">
      <CONSTANT type="String" value="3">
      </CONSTANT>
     </PPARGS>
    </LOADPP>
    <LOADPP fileheader="StatisticDecide" name="STICHPROBE_RUHESTROM_ERGEBNIS" dename="STICHPROBE_RUHESTROM_ERGEBNIS" enname="SAMPLE_TEST_QUIESCENT_CURRENT_MEASUREMENT_RESULT">
     <COMMENT>Nur aufrufen wenn Pr�fung iO =&gt; Pr�fungsblock auf STOP_ON_ERROR !!!</COMMENT>
     <PPARGS name="MODE" required="true">
      <CONSTANT type="String" value="TEST_IO">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="ID" required="true">
      <CONSTANT type="String" value="RUHESTROM">
      </CONSTANT>
     </PPARGS>
    </LOADPP>
    <LOADPP fileheader="StatisticDecide" name="STICHPROBE_RUHESTROM_DURCHFUEHREN" dename="STICHPROBE_RUHESTROM_DURCHFUEHREN" enname="SAMPLE_TEST_QUIESCENT_CURRENT_MEASUREMENT_RUN">
     <COMMENT>Ausf�hren einer Stichprobenartigen Ruhestrommessung

Run of a sample quiescent current measurement test</COMMENT>
     <PPARGS name="MODE" required="true">
      <CONSTANT type="String" value="DECIDE">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="ID" required="true">
      <CONSTANT type="String" value="RUHESTROM">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="QUOTE" required="false">
      <COMMENT>Stichprobe in %</COMMENT>
      <CONSTANT type="String" value="50">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="TESTS_AFTER_ERROR" required="false">
      <COMMENT>Anzahl der zu pr�fenden Fzg nach NiO Fall, 1= nur das n�chste Fzg</COMMENT>
      <CONSTANT type="String" value="1">
      </CONSTANT>
     </PPARGS>
    </LOADPP>
    <LOADPP fileheader="DiagToleranz" name="STEUERN_SLEEPMODE" dename="STEUERN_SCHLAFMODUS" enname="STEUERN_SLEEPMODE">
     <COMMENT>Setzt das Fahrzeug in den Sleepmode

Sets all ecus to sleep mode</COMMENT>
     <PPARGS name="SGBD" required="true">
      <CONSTANT type="String" value="F01">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="JOB" required="true">
      <CONSTANT type="String" value="SLEEP_MODE_FUNKTIONAL">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="AWT" required="false">
      <CONSTANT type="String" value="Jobausf�hrung l�uft!;Bitte warten!" devalue="Jobausf�hrung l�uft!;Bitte warten!" envalue="Job is still running!;Please wait!">
      </CONSTANT>
     </PPARGS>
    </LOADPP>
    <LOADPP fileheader="CombineTestStates" name="STATUS_DECISION_RUHESTROMMESSUNG" dename="STATUS_ENTSCHEIDUNG_RUHESTROMMESSUNG" enname="STATUS_DECISION_QUIESCENT_CURRENT_MEASUREMENT">
     <COMMENT>Entscheidung ob eine Ruhestrommessung durchgef�hrt wird

Decision if a quescient current measurement will be done</COMMENT>
     <PPARGS name="STEP1" required="true">
      <CONSTANT type="String" value="VORSERIE">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="STEP2" required="true">
      <CONSTANT type="String" value="PU_STEUERUNG.VAR_ECOS_LINIE">
       <COMMENT></COMMENT>
      </CONSTANT>
     </PPARGS>
     <PPARGS name="COMBINATION" required="true">
      <CONSTANT type="String" value="MIN_IO_NUMBER">
      </CONSTANT>
      <CONSTANT type="String" value="2">
      </CONSTANT>
     </PPARGS>
    </LOADPP>
    <LOADPP fileheader="UiToleranzTotal" name="WAIT_UI_RUHESTROM" teststepid="27131">
     <PPARGS name="RESULT1" required="true">
      <CONSTANT type="String" value="I">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="MIN_GET1" required="true">
      <CONSTANT type="String" value="0">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="MAX_GET1" required="true">
      <CONSTANT type="String" value="80">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="DAUER" required="true">
      <CONSTANT type="String" value="30000">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="TRIGGER_FILTER" required="true">
      <CONSTANT type="String" value="50">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="MESS_FILTER" required="true">
      <CONSTANT type="String" value="50">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="AWT" required="true">
      <CONSTANT type="String" value="Sleep Current Measurement" devalue="Bitte warten" envalue="Please wait">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="HWT" required="true">
      <CONSTANT type="String" value="Sleep Current Measurement ; Failure" devalue="Zeit abgelaufen" envalue="Time expired">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="PUNKT" required="false">
      <CONSTANT type="String" value="100">
      </CONSTANT>
     </PPARGS>
     <PPARGS name="MODE" required="false">
      <CONSTANT type="String" value="3">
      </CONSTANT>
     </PPARGS>
    </LOADPP>
    <LOADPP fileheader="WerkerQuittung" name="WQ_RUHESTROM_GEMESSEN_MIT_SMZ">
     <COMMENT></COMMENT>
     <PPARGS name="AWT" required="true">
      <CONSTANT type="String" value="Ruhestrom laut SMZ:">
       <COMMENT></COMMENT>
      </CONSTANT>
      <CONSTANT type="String" value="I@ECOS.UI_RUHESTROMMESSUNG_START">
       <COMMENT></COMMENT>
      </CONSTANT>
     </PPARGS>
     <PPARGS name="STYLE" required="true">
      <CONSTANT type="String" value="1">
      </CONSTANT>
     </PPARGS>
    </LOADPP>
   </GROUP>
   <GROUP name="------------------------------------------------">
    <COMMENT></COMMENT>
   </GROUP>
  </GROUP>
  <GROUP name="------------------------------------------------">
   <COMMENT></COMMENT>
  </GROUP>
  <GROUP name="WARTEZEIT_1S">
   <COMMENT></COMMENT>
   <LOADPP fileheader="Pause" name="WARTEZEIT_1S">
    <COMMENT>1s Wartezeit

1s waiting time</COMMENT>
    <PPARGS name="DAUER" required="true">
     <CONSTANT type="String" value="1000">
      <COMMENT></COMMENT>
     </CONSTANT>
    </PPARGS>
    <PPARGS name="HWT" required="false">
     <CONSTANT type="String" value="Waiting time 1s" devalue="Wartezeit 1s" envalue="Waiting time 1s">
      <COMMENT></COMMENT>
     </CONSTANT>
    </PPARGS>
   </LOADPP>
  </GROUP>
  <GROUP name="------------------------------------------------">
   <COMMENT></COMMENT>
  </GROUP>
  <GROUP name="WERKER-INTERFACE / OPERATOR INTERFACE">
   <COMMENT></COMMENT>
   <GROUP name="------------------------------------------------">
    <COMMENT></COMMENT>
   </GROUP>
   <GROUP name="WERKER ANWEISUNGEN / OPERATOR INSTRUCTION">
    <COMMENT></COMMENT>
   </GROUP>
   <GROUP name="------------------------------------------------">
    <COMMENT></COMMENT>
   </GROUP>
   <GROUP name="WERKER EINGABEN / OPERATOR INPUT">
    <COMMENT></COMMENT>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="AIRBAGLABEL / AIRBAG LABEL">
     <COMMENT></COMMENT>
     <IF>
      <COMMENT>Airbaglabel scannen - USA-Information ist im Fahrzeugauftrag hinterlegt (Kanada ist als SA ausgewiesen, somit nicht SA838)</COMMENT>
      <AND>
       <COMMENT></COMMENT>
       <BOOL_METHOD>
        <METHOD name="getL�nderVariante" returntype="String">
         <METHOD name="startsWith" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="US">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
<NOT>
                <BOOL_METHOD>
         <COMMENT>Kanada Ausf�hrung</COMMENT>
        <METHOD name="containsSA" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="0838">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
      <NOT>
                <AND>
         <COMMENT>Nicht F22 M-Racing</COMMENT>
        <BOOL_METHOD>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="F22">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>9ML - SPORTMANUFAKTUR</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="09ML">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </AND>
</NOT>
            </AND>
      <THEN>
       <LOADPP fileheader="WerkerEingabe" name="SCAN_AIRBAG_LABEL" dename="SCAN_AIRBAG_LABEL" enname="SCAN_AIRBAG_LABEL">
        <COMMENT></COMMENT>
        <PPARGS name="AWT" required="true">
         <CONSTANT type="String" value="Please scan the airbag label!" devalue="Bitte das Airbaglabel scannen!" envalue="Please scan the airbag label!">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="MIN" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="6949923%">
          <COMMENT>Sachnummer (7-stellig) + Prozentzeichen</COMMENT>
         </CONSTANT>
         <CONSTANT type="String" value="02694992301">
          <COMMENT>02+Sachnummer+AI01 (11-stellig)</COMMENT>
         </CONSTANT>
         <CONSTANT type="String" value="02694992302">
          <COMMENT>02+Sachnummer+AI02 (11-stellig)</COMMENT>
         </CONSTANT>
         <CONSTANT type="String" value="02694992303">
          <COMMENT>02+Sachnummer+AI03 (11-stellig)</COMMENT>
         </CONSTANT>
         <CONSTANT type="String" value="02694992304">
          <COMMENT>02+Sachnummer+AI04 (11-stellig)</COMMENT>
         </CONSTANT>
         <CONSTANT type="String" value="02694992305">
          <COMMENT>02+Sachnummer+AI05 (11-stellig)</COMMENT>
         </CONSTANT>
         <CONSTANT type="String" value="02694992306">
          <COMMENT>02+Sachnummer+AI06 (11-stellig)</COMMENT>
         </CONSTANT>
         <CONSTANT type="String" value="02694992307">
          <COMMENT>02+Sachnummer+AI07 (11-stellig)</COMMENT>
         </CONSTANT>
         <CONSTANT type="String" value="02694992308">
          <COMMENT>02+Sachnummer+AI08 (11-stellig)</COMMENT>
         </CONSTANT>
         <CONSTANT type="String" value="02694992309">
          <COMMENT>02+Sachnummer+AI09 (11-stellig)</COMMENT>
         </CONSTANT>
         <CONSTANT type="String" value="02694992310">
          <COMMENT>02+Sachnummer+AI10 (11-stellig)</COMMENT>
         </CONSTANT>
         <CONSTANT type="String" value="026949923067">
          <COMMENT>02+Sachnummer+AI10 (11-stellig)</COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
   </GROUP>
   <GROUP name="------------------------------------------------">
    <COMMENT></COMMENT>
   </GROUP>
   <GROUP name="WERKER FRAGEN / OPERATOR QUESTION">
    <COMMENT></COMMENT>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="12V STECKDOSEN / 12V POWER SOCKET">
     <COMMENT></COMMENT>
     <IF>
      <COMMENT>WF_STECKDOSE_MITTELKONSOLE_VORN/ WF_POWER_SOCKET_CENTER_CONSOLE_FRONT</COMMENT>
      <AND>
<NOT>
                <AND>
         <COMMENT>Thermocupholder !((G05, G06, G07) und SA0442)</COMMENT>
        <OR>
         <COMMENT>Derivatives (G05, G06, G07)</COMMENT>
         <BOOL_METHOD>
          <COMMENT>X5</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G05">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>X6 Coup�</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G06">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>X7</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G07">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <OR>
         <COMMENT>options (SA0442)</COMMENT>
         <BOOL_METHOD>
          <COMMENT>Thermocupholder</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="0442">
             <COMMENT>Klimaautomatik</COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
       </AND>
</NOT>
      <NOT>
                <BOOL_METHOD>
         <COMMENT>NICHT RAUCHERPAKET</COMMENT>
        <METHOD name="containsSA" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="0441">
           <COMMENT>Ablagenpaket</COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
      <NOT>
                <BOOL_METHOD>
         <COMMENT>Beh�rdenfahrzeuge; MiKo vorne nicht verbaut;</COMMENT>
        <METHOD name="containsSA" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="0146">
           <COMMENT>Beh�rdenfahrzeug</COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
      <NOT>
                <BOOL_METHOD>
         <COMMENT>2 Series Gran Coupe</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="F44">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
      <NOT>
                <BOOL_METHOD>
         <COMMENT>X3 China</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="G08">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
      <NOT>
                <BOOL_METHOD>
         <COMMENT>3 series sedan (China)</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="G28">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
      <NOT>
                <BOOL_METHOD>
         <COMMENT>5 series sedan extended wheel (China)</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="G38">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
            </AND>
      <THEN>
       <COMMENT>wenn SA441 dann EFII Messung im IBS Pr�fling</COMMENT>
       <IF>
        <BOOL_METHOD>
         <METHOD name="getIStufe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="S18">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <THEN>
         <COMMENT>SP2018</COMMENT>
         <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_MITTELKONSOLE_VORN" dename="WF_STECKDOSE_MITTELKONSOLE_VORN" enname="WF_POWER_SOCKET_CENTER_CONSOLE_FRONT">
          <COMMENT></COMMENT>
          <PPARGS name="FT" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Steckdose;Mittelkonsole Vorn iO?" devalue="Steckdose;Mittelkonsole Vorn iO?" envalue="Power socket;center console front ok?">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="DAUER" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="300000">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="HWT" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Power socket;center console front NOK" devalue="Steckdose;Mittelkonsole Vorn NOK" envalue="Power socket;center console front NOK">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
         <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_STECKDOSE_MITTELKONSOLE_VORN_VORHANDEN" dename="ZIGARETTENANZUENDER_STECKDOSE_MITTELKONSOLE_VORN_VORHANDEN" enname="CIGARETTE_LIGHTER_CENTER_CONSOLE_FRONT_AVAILABLE">
          <COMMENT>PS wird immer NIO</COMMENT>
          <PPARGS name="JOBN_SGBD" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_JOB" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_CONDITION1" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="EQUALS(0,1)">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </THEN>
        <ELSE>
         <COMMENT>!SP2018</COMMENT>
         <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_CENTERSTACK" dename="WF_STECKDOSE_CENTERSTACK" enname="WF_POWER_SOCKET_CENTERSTACK">
          <COMMENT></COMMENT>
          <PPARGS name="FT" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Steckdose;Mittelkon. vorn iO?" devalue="Steckdose;Mittelkon. vorn iO?" envalue="Power socket;front middle console ok?">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="DAUER" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="300000">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="HWT" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Power socket;front middle console NOK" devalue="Steckdose;Mittelkon. vorn NOK" envalue="Power socket;front middle console NOK">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
         <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_CENTERSTACK_VORHANDEN" dename="ZIGARETTENANZUENDER_CENTERSTACK_VORHANDEN" enname="CIGARETTE_LIGHTER_REAR_CENTER_CONSOLE_MIDDLE_AVAILABLE">
          <COMMENT>PS wird immer NIO</COMMENT>
          <PPARGS name="JOBN_SGBD" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_JOB" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_CONDITION1" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="EQUALS(0,1)">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
         <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_MITTELKONSOLE" dename="WF_STECKDOSE_MITTELKONSOLE" enname="WF_POWER_SOCKET_CENTER_CONSOLE">
          <COMMENT></COMMENT>
          <PPARGS name="FT" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Steckdose;Mittelkon. vorn iO?" devalue="Steckdose;Mittelkon. vorn iO?" envalue="Power socket;front middle console ok?">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="DAUER" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="300000">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
         <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_MITTELKONSOLE_VORHANDEN">
          <COMMENT>PS wird immer NIO</COMMENT>
          <PPARGS name="JOBN_SGBD" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_JOB" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_CONDITION1" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="EQUALS(0,1)">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </ELSE>
       </IF>
      </THEN>
      <ELSE>
       <COMMENT>EFII MEASUREMENT</COMMENT>
       <IF>
        <BOOL_METHOD>
         <METHOD name="getIStufe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="S18">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <THEN>
         <COMMENT>SP2018</COMMENT>
         <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_STECKDOSE_MITTELKONSOLE_VORN_VORHANDEN">
          <COMMENT>PS wird immer IO</COMMENT>
          <PPARGS name="JOBN_SGBD" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_JOB" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_CONDITION1" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="EQUALS(1,1)">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </THEN>
        <ELSE>
         <COMMENT>!SP2018</COMMENT>
         <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_MITTELKONSOLE_VORHANDEN">
          <COMMENT>PS wird immer IO</COMMENT>
          <PPARGS name="JOBN_SGBD" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_JOB" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_CONDITION1" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="EQUALS(1,1)">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </ELSE>
       </IF>
       <GROUP name="EFII JOBS AT IBS PRUEFLING">
        <COMMENT></COMMENT>
       </GROUP>
      </ELSE>
     </IF>
     <IF>
      <COMMENT>WF_STECKDOSE_MITTELARMLEHNE_VORNE / WF_POWER_SOCKET_CENTER_ARM_REST_FRONT</COMMENT>
      <OR>
       <COMMENT></COMMENT>
       <OR>
        <COMMENT>35up Serie (F90, F91, F92, F93, F95, F96, G05, G06, G07, G11, G12, G14, G15, G16, G32, G38)</COMMENT>
        <BOOL_METHOD>
         <COMMENT>M5</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F90">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>M8 Cabrio 2 Tuer</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F91">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>M8 Coupe 2 Tuer</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F92">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>M8 4 Tuer</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F93">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>X5 M</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F95">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>X6 M</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F96">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>X5</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G05">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>X6 Coup�</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G06">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>X7 // diese Steckdose ist mit einer Beleuchtung</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G07">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>7er Limousine</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G11">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>7er Lang</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G12">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>6er Cabrio</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G14">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>6er Coupe</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G15">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>6er Gran Coupe 4dr</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G16">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>5er GT</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G32">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>5er Limousine Lang (China)</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G38">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <AND>
         <COMMENT>(G30, G31) und !SA0146</COMMENT>
         <OR>
          <COMMENT>G30 G31</COMMENT>
          <BOOL_METHOD>
           <COMMENT>5er Limousine</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G30">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>5er Touring</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G31">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
<NOT>
                    <BOOL_METHOD>
           <COMMENT>Beh�rdenfahrzeuge; MiKo vorne nicht verbaut;</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="0146">
             <COMMENT>Beh�rdenfahrzeug</COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                </AND>
       </OR>
       <OR>
        <COMMENT>Rolls Royce</COMMENT>
        <BOOL_METHOD>
         <COMMENT>Ghost</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="RR4">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>Wraith</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="RR5">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>Wraith Cabrio</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="RR6">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="RR11">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="RR12">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="RR31">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
      </OR>
      <THEN>
       <IF>
        <BOOL_METHOD>
         <METHOD name="getIStufe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="S18">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <THEN>
         <COMMENT>SP2018</COMMENT>
         <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_MITTELARMLEHNE_VORN" dename="WF_STECKDOSE_MITTELARMLEHNE_VORN" enname="WF_POWER_SOCKET_CENTER_ARM_REST_FRONT">
          <COMMENT></COMMENT>
          <PPARGS name="FT" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Steckdose; Mittelarmlehne vorn iO?" devalue="Steckdose; Mittelarmlehne vorn iO?" envalue="Power socket;center arm rest front ok?">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="DAUER" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="300000">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="HWT" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Power socket;center arm rest front NOK" devalue="Steckdose; Mittelarmlehne vorn NOK" envalue="Power socket;center arm rest front NOK">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </THEN>
        <ELSE>
         <COMMENT>!SP2018</COMMENT>
         <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_MITTELARMLEHNE_ABLAGEFACH" dename="WF_STECKDOSE_MITTELARMLEHNE_ABLAGEFACH" enname="WF_POWER_SOCKET_CENTER_ARMREST">
          <COMMENT></COMMENT>
          <PPARGS name="FT" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Steckdose MAL;Ablagefach iO?" devalue="Steckdose MAL;Ablagefach iO?" envalue="Power socket;center armrest ok?">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="DAUER" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="300000">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="HWT" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Power socket;center armrest NOK" devalue="Steckdose MAL;Ablagefach NOK" envalue="Power socket;center armrest NOK">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </ELSE>
       </IF>
      </THEN>
     </IF>
     <IF>
      <COMMENT>WF_STECKDOSE_MITTELKONSOLE_HINTEN_RECHTS / WF_POWER_SOCKET_CENTER_CONSOLE_REAR_RIGHT_SIDE</COMMENT>
      <AND>
       <COMMENT>!SA441: Raucherpaket und (F9x, G07, G11, G12, G30, G31, G32, G38, RR5, RR6, RR11, RR12)</COMMENT>
       <OR>
        <COMMENT>F90, F93, G11, G12, G16, G28, G30, G31, G32, G38, RR5, RR6, RR11, RR12</COMMENT>
        <AND>
         <COMMENT>35up Serie (G11, G12) &amp;&amp; !Executiv Loung Fondkonsole</COMMENT>
         <OR>
          <COMMENT>35up Serie (G11, G12)</COMMENT>
          <BOOL_METHOD>
           <COMMENT>7 series sedan</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G11">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>7 series sedan extended wheel</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G12">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <OR>
          <COMMENT>nur bis LCI 03/2019; USB Dual Ladebuchsen ersetzen die rechte 12V Steckdose;</COMMENT>
          <AND>
           <BOOL_METHOD>
            <METHOD name="getIStufeMonatInt" returntype="int">
            </METHOD>
            <OPERATOR type="less" optype="int">
            </OPERATOR>
            <CONSTANT type="int" value="3">
            </CONSTANT>
           </BOOL_METHOD>
           <BOOL_METHOD>
            <METHOD name="getIStufeJahrInt" returntype="int">
            </METHOD>
            <OPERATOR type="eq" optype="int">
            </OPERATOR>
            <CONSTANT type="int" value="19">
            </CONSTANT>
           </BOOL_METHOD>
          </AND>
          <BOOL_METHOD>
           <METHOD name="getIStufeJahrInt" returntype="int">
           </METHOD>
           <OPERATOR type="less" optype="int">
           </OPERATOR>
           <CONSTANT type="int" value="19">
           </CONSTANT>
          </BOOL_METHOD>
         </OR>
<NOT>
                    <BOOL_METHOD>
           <COMMENT>Executiv Loung Fondkonsole</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="04F5">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                </AND>
        <AND>
         <COMMENT>35up (F90, F93, G16, G30, G31) und (SA0575, SA06FH)</COMMENT>
         <OR>
          <COMMENT>F90, F93, G16, G30, G31</COMMENT>
          <BOOL_METHOD>
           <COMMENT>M5</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="F90">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>M8 4 Tuer</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="F93">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>6er Gran Coupe 4dr</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G16">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>5 series sedan</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G30">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>5 series wagon</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G31">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <OR>
          <COMMENT>SA0575, SA06FH</COMMENT>
          <BOOL_METHOD>
           <COMMENT>Zus�tzliche 12V-Steckdosen</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="0575">
              <COMMENT>Zus�tzliche 12V-Steckdosen</COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>auch Fondentertainment;</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="06FH">
              <COMMENT>Ablagenpaket</COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
<NOT>
                    <BOOL_METHOD>
           <COMMENT>Beh�rdenfahrzeuge; MiKo vorne nicht verbaut;</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="0146">
             <COMMENT>Beh�rdenfahrzeug</COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                </AND>
        <OR>
         <COMMENT>G28, G32, G38</COMMENT>
         <BOOL_METHOD>
          <COMMENT>3 Series Sedan (China)</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G28">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>5 series GT</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G32">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>5 series sedan extended wheel (China)</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G38">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <OR>
         <COMMENT>Rolls Royce (RR5, RR6, RR11, RR12, RR31)</COMMENT>
         <BOOL_METHOD>
          <COMMENT>Wraith</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="RR5">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>Wraith Cabrio</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="RR6">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <AND>
          <COMMENT>(RR11, RR12, RR31) &amp; RSF</COMMENT>
          <OR>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="RR11">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="RR12">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="RR31">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
          <BOOL_METHOD>
           <COMMENT>REAR SEAT MIDDLE CONSOLE</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="0RSF">
              <COMMENT>Ablagenpaket</COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </AND>
        </OR>
       </OR>
      </AND>
      <THEN>
       <COMMENT>Stopfen</COMMENT>
       <IF>
        <BOOL_METHOD>
         <METHOD name="getIStufe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="S18">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <THEN>
         <COMMENT>SP2018</COMMENT>
         <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_MITTELKONSOLE_HINTEN_RECHTS" dename="WF_STECKDOSE_MITTELKONSOLE_FOND_RECHTS" enname="WF_POWER_SOCKET_CENTER_CONSOLE_REAR_RIGHT_SIDE">
          <COMMENT></COMMENT>
          <PPARGS name="FT" required="true">
           <COMMENT>G11 || G12 with executive loung fond console get the power socket inside the armrest center console rear.
All the outer cars get the power socket at the backside of the center console front.</COMMENT>
           <CONSTANT type="String" value="Rechte Steckdosen;Mittelk. hinten rechts iO?" devalue="Rechte Steckdosen;Mittelk. hinten rechts iO?" envalue="Power sockets right side; center console rear right sideok?">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="DAUER" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="300000">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="HWT" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Power sockets right side; center console rear right side NOK" devalue="Rechte Steckdosen;Mittelk. hinten rechts NOK" envalue="Power sockets right side; center console rear right side NOK">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
         <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_MITTELKONSOLE_HINTEN_RECHTS_VORHANDEN">
          <COMMENT>PS wird immer NIO</COMMENT>
          <PPARGS name="JOBN_SGBD" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_JOB" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_CONDITION1" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="EQUALS(0,1)">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </THEN>
        <ELSE>
         <COMMENT>!SP2018</COMMENT>
         <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_MITTELKONSOLE_FOND_RECHTS" dename="WF_STECKDOSE_MITTELKONSOLE_FOND_RECHTS" enname="WF_REAR_POWER_SOCKET_CENTER_CONSOLE_RIGHT_SIDE">
          <COMMENT></COMMENT>
          <PPARGS name="FT" required="true">
           <COMMENT>G11 || G12 with executive loung fond console get the power socket inside the armrest center console rear.
All the outer cars get the power socket at the backside of the center console front.</COMMENT>
           <CONSTANT type="String" value="Rechte Steckdosen;Mittelk. hinten iO?" devalue="Rechte Steckdosen;Mittelk. hinten iO?" envalue="Power sockets right side; rear middle console ok?">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="DAUER" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="300000">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="HWT" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Power sockets right side; rear middle console NOK" devalue="Rechte Steckdosen;Mittelk. hinten NOK" envalue="Power sockets right side; rear middle console NOK">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
         <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_MITTELKONSOLE_FOND_RECHTS_VORHANDEN">
          <COMMENT>PS wird immer NIO</COMMENT>
          <PPARGS name="JOBN_SGBD" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_JOB" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_CONDITION1" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="EQUALS(0,1)">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </ELSE>
       </IF>
      </THEN>
      <ELSE>
       <COMMENT>EFII MEASUREMENT</COMMENT>
       <IF>
        <COMMENT></COMMENT>
        <AND>
         <COMMENT>SA0441 und G07</COMMENT>
         <OR>
          <COMMENT>35up</COMMENT>
          <BOOL_METHOD>
           <COMMENT>X7</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G07">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <BOOL_METHOD>
          <COMMENT>RAUCHERPAKET</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="0441">
             <COMMENT>Ablagenpaket</COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </AND>
        <THEN>
         <COMMENT></COMMENT>
         <IF>
          <BOOL_METHOD>
           <METHOD name="getIStufe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="startsWith" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="S18">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <THEN>
           <COMMENT>SP2018</COMMENT>
           <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_MITTELKONSOLE_HINTEN_RECHTS_VORHANDEN">
            <COMMENT>PS wird immer IO</COMMENT>
            <PPARGS name="JOBN_SGBD" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_JOB" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_CONDITION1" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="EQUALS(1,1)">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
           </LOADPP>
          </THEN>
          <ELSE>
           <COMMENT>!SP2018</COMMENT>
           <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_MITTELKONSOLE_FOND_RECHTS_VORHANDEN">
            <COMMENT>PS wird immer IO</COMMENT>
            <PPARGS name="JOBN_SGBD" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_JOB" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_CONDITION1" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="EQUALS(1,1)">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
           </LOADPP>
          </ELSE>
         </IF>
        </THEN>
       </IF>
       <GROUP name="EFII JOBS AT IBS PRUEFLING">
        <COMMENT></COMMENT>
       </GROUP>
      </ELSE>
     </IF>
     <IF>
      <COMMENT>WF_STECKDOSE_MITTELKONSOLE_HINTEN_LINKS / WF_POWER_SOCKET_CENTER_CONSOLE_REAR_LEFT_SIDE</COMMENT>
      <OR>
       <COMMENT>F90, F93, G07, G11, G12, G16, G30, G31, G32, RR5, RR6, RR11, RR12, RR31</COMMENT>
       <AND>
        <COMMENT>G32</COMMENT>
        <BOOL_METHOD>
         <COMMENT>5 series GT</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G32">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </AND>
       <AND>
        <COMMENT>!SA441 und G07</COMMENT>
        <BOOL_METHOD>
         <COMMENT>X7</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G07">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
<NOT>
                  <BOOL_METHOD>
          <COMMENT>NICHT RAUCHERPAKET</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0441">
            <COMMENT>Ablagenpaket</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
              </AND>
       <AND>
        <COMMENT>35up Serie (G11, G12 ) &amp;&amp; !SA441 &amp;&amp;!SA4F5</COMMENT>
        <OR>
         <COMMENT>G11, G12</COMMENT>
         <BOOL_METHOD>
          <COMMENT>7 series sedan</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G11">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>7 series sedan extended wheel</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G12">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
<NOT>
                  <BOOL_METHOD>
          <COMMENT>NICHT RAUCHERPAKET</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0441">
            <COMMENT>Ablagenpaket</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
       <NOT>
                  <BOOL_METHOD>
          <COMMENT>Executiv Loung Fondkonsole</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="04F5">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
              </AND>
       <AND>
        <COMMENT>35up (F90, F93, G16, G30, G31) und (SA0575, SA06FH) und !SA0146</COMMENT>
        <OR>
         <COMMENT>F90, F93, G16, G30, G31</COMMENT>
         <BOOL_METHOD>
          <COMMENT>M5</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="F90">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>M8 4 Tuer</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="F93">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>6er Gran Coupe 4dr</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G16">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>5 series sedan</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G30">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>5 series wagon</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G31">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <OR>
         <COMMENT>SA0575, SA0FH</COMMENT>
         <BOOL_METHOD>
          <COMMENT>Zus�tzliche 12V-Steckdosen</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="0575">
             <COMMENT>Zus�tzliche 12V-Steckdosen</COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>auch Fondentertainment;</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="06FH">
             <COMMENT>Ablagenpaket</COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
<NOT>
                  <BOOL_METHOD>
          <COMMENT>Beh�rdenfahrzeuge; MiKo vorne nicht verbaut;</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0146">
            <COMMENT>Beh�rdenfahrzeug</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
              </AND>
       <OR>
        <COMMENT>Rolls Royce</COMMENT>
        <AND>
         <COMMENT>(RR5, RR6) &amp; !SA441</COMMENT>
         <OR>
          <COMMENT>RR5, RR6</COMMENT>
          <BOOL_METHOD>
           <COMMENT>Wraith</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="RR5">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>7Wraith Cabrio</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="RR6">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
<NOT>
                    <BOOL_METHOD>
           <COMMENT>NICHT RAUCHERPAKET</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="0441">
             <COMMENT>Ablagenpaket</COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                </AND>
        <AND>
         <COMMENT>(RR11, RR12, RR31) &amp; RSF</COMMENT>
         <OR>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="RR11">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="RR12">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="RR31">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <BOOL_METHOD>
          <COMMENT>REAR SEAT MIDDLE CONSOLE</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="0RSF">
             <COMMENT>Ablagenpaket</COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </AND>
       </OR>
      </OR>
      <THEN>
       <COMMENT>Stopfen</COMMENT>
       <IF>
        <COMMENT></COMMENT>
        <BOOL_METHOD>
         <METHOD name="getIStufe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="S18">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <THEN>
         <COMMENT>SP2018</COMMENT>
         <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_MITTELKONSOLE_HINTEN_LINKS" dename="WF_STECKDOSE_MITTELKONSOLE_HINTEN_LINKS" enname="WF_POWER_SOCKET_CENTER_CONSOLE_REAR_LEFT_SIDE">
          <COMMENT></COMMENT>
          <PPARGS name="FT" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Linke Steckdosen;Mittelk. hinten links iO?" devalue="Linke Steckdosen;Mittelk. hinten links iO?" envalue="Power sockets left side; center console rear left side ok?">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="DAUER" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="300000">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="HWT" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Power sockets left side; center console rear left side NOK" devalue="Linke Steckdosen;Mittelk. hinten links NOK" envalue="Power sockets left side; center console rear left side NOK">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
         <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_MITTELKONSOLE_HINTEN_LINKS_VORHANDEN" dename="ZIGARETTENANZUENDER_MITTELKONSOLE_HINTEN_LINKS_VORHANDEN">
          <COMMENT>PS wird immer NIO</COMMENT>
          <PPARGS name="JOBN_SGBD" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_JOB" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_CONDITION1" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="EQUALS(0,1)">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </THEN>
        <ELSE>
         <COMMENT>!SP2018</COMMENT>
         <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_MITTELKONSOLE_FOND_LINKS" dename="WF_STECKDOSE_MITTELKONSOLE_FOND_LINKS" enname="WF_REAR_POWER_SOCKET_CENTER_CONSOLE_LEFT_SIDE">
          <COMMENT></COMMENT>
          <PPARGS name="FT" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Linke Steckdosen;Mittelk. hinten iO?" devalue="Linke Steckdosen;Mittelk. hinten iO?" envalue="Power sockets left side; rear middle console ok?">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="DAUER" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="300000">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="HWT" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Power sockets left side; rear middle console NOK" devalue="Linke Steckdosen;Mittelk. hinten NOK" envalue="Power sockets left side; rear middle console NOK">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
         <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_MITTELKONSOLE_FOND_LINKS_VORHANDEN" dename="ZIGARETTENANZUENDER_MITTELKONSOLE_FOND_LINKS_VORHANDEN">
          <COMMENT>PS wird immer NIO</COMMENT>
          <PPARGS name="JOBN_SGBD" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_JOB" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_CONDITION1" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="EQUALS(0,1)">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </ELSE>
       </IF>
      </THEN>
      <ELSE>
       <COMMENT>EFII MEASUREMENT</COMMENT>
       <IF>
        <OR>
         <COMMENT>G38, (G11, G12, G32 und SA0441 und !SA04F5), (RR6 und SA441)</COMMENT>
         <OR>
          <COMMENT>Serie ohne SA</COMMENT>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G38">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <AND>
          <COMMENT>35up Serie &amp;&amp; SA441 &amp;&amp;!SA4F5</COMMENT>
          <OR>
           <BOOL_METHOD>
            <COMMENT>7 series sedan</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G11">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>7 series sedan extended wheel</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G12">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
          <BOOL_METHOD>
           <COMMENT>NICHT RAUCHERPAKET</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="0441">
              <COMMENT>Ablagenpaket</COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
<NOT>
                      <BOOL_METHOD>
            <COMMENT>Executiv Loung Hintenkonsole</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="04F5">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                  </AND>
         <OR>
          <COMMENT>Rolls Royce</COMMENT>
          <AND>
           <COMMENT>RR6 &amp; SA441</COMMENT>
           <OR>
            <BOOL_METHOD>
             <COMMENT>Wraith</COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="RR5">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <COMMENT>7Wraith Cabrio</COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="RR6">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           </OR>
           <BOOL_METHOD>
            <COMMENT>NICHT RAUCHERPAKET</COMMENT>
            <METHOD name="containsSA" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="0441">
               <COMMENT>Ablagenpaket</COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </AND>
         </OR>
        </OR>
        <THEN>
         <IF>
          <BOOL_METHOD>
           <METHOD name="getIStufe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="startsWith" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="S18">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <THEN>
           <COMMENT>SP2018</COMMENT>
           <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_MITTELKONSOLE_HINTEN_LINKS_VORHANDEN" dename="ZIGARETTENANZUENDER_MITTELKONSOLE_HINTEN_LINKS_VORHANDEN">
            <COMMENT>PS wird immer IO</COMMENT>
            <PPARGS name="JOBN_SGBD" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_JOB" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_CONDITION1" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="EQUALS(1,1)">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
           </LOADPP>
          </THEN>
          <ELSE>
           <COMMENT>!SP2018</COMMENT>
           <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_MITTELKONSOLE_FOND_LINKS_VORHANDEN" dename="ZIGARETTENANZUENDER_MITTELKONSOLE_FOND_LINKS_VORHANDEN">
            <COMMENT>PS wird immer IO</COMMENT>
            <PPARGS name="JOBN_SGBD" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_JOB" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_CONDITION1" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="EQUALS(1,1)">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
           </LOADPP>
          </ELSE>
         </IF>
        </THEN>
       </IF>
       <GROUP name="EFII JOBS AT IBS PRUEFLING">
        <COMMENT></COMMENT>
       </GROUP>
      </ELSE>
     </IF>
     <IF>
      <COMMENT>WF_STECKDOSE_MITTELKONSOLE_HINTEN_MITTE / WF_POWER_SOCKET_CENTER_CONSOLE_REAR_MIDDLE</COMMENT>
      <OR>
       <COMMENT></COMMENT>
       <AND>
        <COMMENT>35up Serie ohne SA (G08)</COMMENT>
        <BOOL_METHOD>
         <COMMENT>X3 China</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G08">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </AND>
       <AND>
        <COMMENT>35up Serie (G05, G06) &amp;&amp; !SA441</COMMENT>
        <OR>
         <COMMENT>F95, F96, G05, G06</COMMENT>
         <BOOL_METHOD>
          <COMMENT>X5 M</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="F95">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>X6 M</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="F96">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>X5</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G05">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>X6 Coup�</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G06">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
<NOT>
                  <BOOL_METHOD>
          <COMMENT>NICHT RAUCHERPAKET</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0441">
            <COMMENT>Ablagenpaket</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
              </AND>
       <AND>
        <COMMENT>35up Serie (F90, F93, G16, G30, G31) &amp;&amp; !SA0146 &amp;&amp; !SA0575 &amp;&amp; !SA06FH</COMMENT>
        <OR>
         <COMMENT>F90, F93, G16, G30, G31</COMMENT>
         <BOOL_METHOD>
          <COMMENT>M5</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="F90">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>M8 4 Tuer</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="F93">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>6er Gran Coupe 4dr</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G16">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>5 series sedan</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G30">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>5 series wagon</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G31">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
<NOT>
                  <BOOL_METHOD>
          <COMMENT>Beh�rdenfahrzeuge; MiKo vorne nicht verbaut;</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0146">
            <COMMENT>Beh�rdenfahrzeug</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
       <NOT>
                  <BOOL_METHOD>
          <COMMENT>Zus�tzliche 12V-Steckdosen / dann hat er n�mlich zwei li und re!!!</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0575">
            <COMMENT>Zus�tzliche 12V-Steckdosen</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
       <NOT>
                  <BOOL_METHOD>
          <COMMENT>auch Fondentertainment;</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="06FH">
            <COMMENT>Ablagenpaket</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
              </AND>
       <AND>
        <COMMENT>35up (F97, F98, G01, G02, G08, G2x) mit SA0493</COMMENT>
        <OR>
         <COMMENT>F97, F98, G01, G02, G08, G20, G21</COMMENT>
         <BOOL_METHOD>
          <COMMENT>X3 M</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="F97">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>X4 M</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="F98">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>X3</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G01">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>X4 Coup�</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G02">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <BOOL_METHOD>
         <COMMENT>Ablagepaket</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0493">
            <COMMENT>Zus�tzliche 12V-Steckdosen</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
<NOT>
                  <BOOL_METHOD>
          <COMMENT>NICHT RAUCHERPAKET</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0441">
            <COMMENT>Ablagenpaket</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
              </AND>
       <AND>
        <COMMENT>G2x und SA0493</COMMENT>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G2">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>Ablagepaket</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0493">
            <COMMENT>Zus�tzliche 12V-Steckdosen</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </AND>
       <OR>
        <COMMENT>RRNM</COMMENT>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="RR11">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="RR12">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="RR31">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
      </OR>
      <THEN>
       <COMMENT>Stopfen</COMMENT>
       <IF>
        <COMMENT></COMMENT>
        <BOOL_METHOD>
         <METHOD name="getIStufe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="S18">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <THEN>
         <COMMENT>SP2018</COMMENT>
         <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_MITTELKONSOLE_HINTEN_MITTE" dename="WF_STECKDOSE_MITTELKONSOLE_HINTEN_MITTE" enname="WF_POWER_SOCKET_CENTER_CONSOLE_REAR_MIDDLE">
          <COMMENT></COMMENT>
          <PPARGS name="FT" required="true">
           <COMMENT>G11 || G12 with executive loung hinten console get the power socket inside the armrest center console rear.
All the outer cars get the power socket at the backside of the center console front.</COMMENT>
           <CONSTANT type="String" value="Steckdosen;Mittelk. hinten mitte iO?" devalue="Steckdosen;Mittelk. hinten mitte iO?" envalue="Power socket;center console rear middle ok?">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="DAUER" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="300000">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="HWT" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Power socket;center console rear middle NOK" devalue="Steckdosen;Mittelk. hinten mitte NOK" envalue="Power socket;center console rear middle NOK">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
         <LOADPP fileheader="DiagData" name="ZIGARETTENANZ�NDER_MITTELKONSOLE_HINTEN_MITTE_VORHANDEN" dename="ZIGARETTENANZ�NDER_MITTELKONSOLE_HINTEN_MITTE_VORHANDEN" enname="CIGARETTE_LIGHTER_REAR_CENTER_CONSOLE_MIDDLE_AVAILABLE">
          <COMMENT>PS wird immer NIO</COMMENT>
          <PPARGS name="JOBN_SGBD" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_JOB" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_CONDITION1" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="EQUALS(0,1)">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </THEN>
        <ELSE>
         <COMMENT>!SP2018</COMMENT>
         <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_MITTELKONSOLE_FOND_MITTE" dename="WF_STECKDOSE_MITTELKONSOLE_FOND_MITTE" enname="WF_REAR_POWER_SOCKET_CENTER_CONSOLE_MIDDLE">
          <COMMENT></COMMENT>
          <PPARGS name="FT" required="true">
           <COMMENT>G11 || G12 with executive loung hinten console get the power socket inside the armrest center console rear.
All the outer cars get the power socket at the backside of the center console front.</COMMENT>
           <CONSTANT type="String" value="Steckdosen;Mittelk. hinten iO?" devalue="Steckdosen;Mittelk. hinten iO?" envalue="Power socket;rear middle console ok?">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="DAUER" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="300000">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="HWT" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Power socket;rear middle console NOK" devalue="Steckdosen;Mittelk. hinten NOK" envalue="Power socket;rear middle console NOK">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
         <LOADPP fileheader="DiagData" name="ZIGARETTENANZ�NDER_MITTELKONSOLE_FOND_MITTE_VORHANDEN" dename="ZIGARETTENANZ�NDER_MITTELKONSOLE_FOND_MITTE_VORHANDEN" enname="CIGARETTE_LIGHTER_REAR_CENTER_CONSOLE_MIDDLE_AVAILABLE">
          <COMMENT>PS wird immer NIO</COMMENT>
          <PPARGS name="JOBN_SGBD" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_JOB" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_CONDITION1" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="EQUALS(0,1)">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </ELSE>
       </IF>
      </THEN>
      <ELSE>
       <COMMENT>EFII MEASUREMENT</COMMENT>
       <IF>
        <OR>
         <COMMENT>(F95, F96, G05, G06 und SA0441), (F97, F98, G01, G02, G08, G20, G21 und SA0441 und SA0493), G28</COMMENT>
         <AND>
          <COMMENT>35up Serie &amp;&amp; SA441</COMMENT>
          <OR>
           <COMMENT>Serie</COMMENT>
           <BOOL_METHOD>
            <COMMENT>X5 M</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="F95">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>X6 M</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="F96">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>X5</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G05">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>X6 Coup�</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G06">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
          <BOOL_METHOD>
           <COMMENT>RAUCHERPAKET</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="0441">
              <COMMENT>Ablagenpaket</COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </AND>
         <AND>
          <COMMENT>35up mit SA</COMMENT>
          <OR>
           <COMMENT></COMMENT>
           <BOOL_METHOD>
            <COMMENT>X3 M</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="F97">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>X4 M</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="F98">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>X3</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G01">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>X4 Coup�</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G02">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>X3 China</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G08">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>3 series sedan</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G20">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>3 series touring</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G21">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
          <BOOL_METHOD>
           <COMMENT>RAUCHERPAKET</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="0441">
              <COMMENT>Ablagenpaket</COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>Ablagepaket</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="0493">
              <COMMENT>Zus�tzliche 12V-Steckdosen</COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </AND>
         <OR>
          <COMMENT>35up Typinhalt (G28)</COMMENT>
          <BOOL_METHOD>
           <COMMENT>3 series lim. China</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G28">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
        </OR>
        <THEN>
         <COMMENT>SP2018</COMMENT>
         <IF>
          <BOOL_METHOD>
           <METHOD name="getIStufe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="startsWith" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="S18">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <THEN>
           <COMMENT>SP2018</COMMENT>
           <LOADPP fileheader="DiagData" name="ZIGARETTENANZ�NDER_MITTELKONSOLE_HINTEN_MITTE_VORHANDEN">
            <COMMENT>PS wird immer IO</COMMENT>
            <PPARGS name="JOBN_SGBD" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_JOB" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_CONDITION1" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="EQUALS(1,1)">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
           </LOADPP>
          </THEN>
          <ELSE>
           <COMMENT>!SP2018</COMMENT>
           <LOADPP fileheader="DiagData" name="ZIGARETTENANZ�NDER_MITTELKONSOLE_FOND_MITTE_VORHANDEN">
            <COMMENT>PS wird immer IO</COMMENT>
            <PPARGS name="JOBN_SGBD" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_JOB" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_CONDITION1" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="EQUALS(1,1)">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
           </LOADPP>
          </ELSE>
         </IF>
        </THEN>
        <ELSE>
         <COMMENT>NIO</COMMENT>
         <IF>
          <BOOL_METHOD>
           <METHOD name="getIStufe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="startsWith" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="S18">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <THEN>
           <COMMENT>SP2018</COMMENT>
           <LOADPP fileheader="DiagData" name="ZIGARETTENANZ�NDER_MITTELKONSOLE_HINTEN_MITTE_VORHANDEN">
            <COMMENT>PS wird immer NIO</COMMENT>
            <PPARGS name="JOBN_SGBD" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_JOB" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_CONDITION1" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="EQUALS(0,1)">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
           </LOADPP>
          </THEN>
          <ELSE>
           <COMMENT>!SP2018</COMMENT>
           <LOADPP fileheader="DiagData" name="ZIGARETTENANZ�NDER_MITTELKONSOLE_FOND_MITTE_VORHANDEN">
            <COMMENT>PS wird immer NIO</COMMENT>
            <PPARGS name="JOBN_SGBD" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_JOB" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_CONDITION1" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="EQUALS(0,1)">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
           </LOADPP>
          </ELSE>
         </IF>
        </ELSE>
       </IF>
       <GROUP name="EFII JOBS AT IBS PRUEFLING">
        <COMMENT></COMMENT>
       </GROUP>
      </ELSE>
     </IF>
     <IF>
      <COMMENT>WF_STECKDOSE_MITTELONSOLE_HINTEN_CUP_HOLDER /  WF_POWER_SOCKET_CENTER_CONSOLE_REAR_CUP_HOLDER</COMMENT>
      <OR>
       <COMMENT>G11, G12, RR6</COMMENT>
       <AND>
        <COMMENT>35up (G11, G12) &amp;&amp; Executive Loung Fondkonsole</COMMENT>
        <OR>
         <COMMENT>35up (G11, G12)</COMMENT>
         <BOOL_METHOD>
          <COMMENT>7 series sedan</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G11">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>7 series sedan extended wheel</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G12">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <BOOL_METHOD>
         <COMMENT>Executive Loung Fondkonsole</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="04F5">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
<NOT>
                  <BOOL_METHOD>
          <COMMENT>RAUCHERPACKET</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0441">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
              </AND>
       <OR>
        <COMMENT>Rolls Royce (RR6)</COMMENT>
        <BOOL_METHOD>
         <COMMENT>Wraith Cabrio</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="RR6">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
      </OR>
      <THEN>
       <IF>
        <BOOL_METHOD>
         <METHOD name="getIStufe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="S18">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <THEN>
         <COMMENT>SP2018</COMMENT>
         <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_MITTELONSOLE_HINTEN_CUP_HOLDER" dename="WF_STECKDOSE_MITTELONSOLE_HINTEN_CUP_HOLDER" enname="WF_POWER_SOCKET_CENTER_CONSOLE_REAR_CUP_HOLDER">
          <COMMENT></COMMENT>
          <PPARGS name="FT" required="true">
           <COMMENT>G11 || G12 with Executive Loung Fonkonsole get the power socket between cup holder.
All the outer cars get the power socket at the backside of the center console front.</COMMENT>
           <CONSTANT type="String" value="Steckdosen Mittelkonsole Hinten;Cup-Holder Mitte iO?" devalue="Steckdosen Mittelkonsole Hinten;Cup-Holder Mitte iO?" envalue="Power socket; center console rear Cup Holder ok?">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="DAUER" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="300000">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="HWT" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Power socket; center console rear Cup Holder NOK" devalue="Steckdosen Mittelkonsole Hinten;Cup-Holder Mitte NOK" envalue="Power socket; center console rear Cup Holder NOK">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
         <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_EXECUTIVE_LOUNG_MITTELKONSOLE_HINTEN_VORHANDEN">
          <COMMENT>PS wird immer NIO</COMMENT>
          <PPARGS name="JOBN_SGBD" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_JOB" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_CONDITION1" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="EQUALS(0,1)">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </THEN>
        <ELSE>
         <COMMENT>!SP2018</COMMENT>
         <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_FONDKONSOLE_CUP_HOLDER" dename="WF_STECKDOSE_FONDKONSOLE_CUP_HOLDER" enname="WF_FOND_POWER_SOCKET_CENTER_CONSOLE_CUP_HOLDER">
          <COMMENT></COMMENT>
          <PPARGS name="FT" required="true">
           <COMMENT>G11 || G12 with Executive Loung Fonkonsole get the power socket between cup holder.
All the outer cars get the power socket at the backside of the center console front.</COMMENT>
           <CONSTANT type="String" value="Steckdosen Fondkonsole;Cup-Holder Mitte iO?" devalue="Steckdosen Fondkonsole;Cup-Holder Mitte iO?" envalue="Power socket fondkonsole;Cup Holder ok?">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="DAUER" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="300000">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="HWT" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Power socket fondkonsole;Cup Holder NOK" devalue="Steckdosen Fondkonsole;Cup-Holder Mitte NOK" envalue="Power socket fondkonsole;Cup Holder NOK">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
         <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_EXECUTIVE_LOUNG_FONDKONSOLE_VORHANDEN">
          <COMMENT>PS wird immer NIO</COMMENT>
          <PPARGS name="JOBN_SGBD" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_JOB" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="NONE">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="JOB1_CONDITION1" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="EQUALS(0,1)">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </ELSE>
       </IF>
      </THEN>
      <ELSE>
       <COMMENT>EFII MEASUREMENT</COMMENT>
       <IF>
        <AND>
         <COMMENT>35up &amp;&amp; Executive Loung Fondkonsole</COMMENT>
         <OR>
          <COMMENT>35up</COMMENT>
          <BOOL_METHOD>
           <COMMENT>7 series sedan</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G11">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>7 series sedan extended wheel</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G12">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <BOOL_METHOD>
          <COMMENT>Executive Loung Fondkonsole</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="04F5">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>RAUCHERPACKET</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="0441">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </AND>
        <THEN>
         <IF>
          <BOOL_METHOD>
           <METHOD name="getIStufe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="startsWith" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="S18">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <THEN>
           <COMMENT>SP2018</COMMENT>
           <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_EXECUTIVE_LOUNG_MITTELKONSOLE_HINTEN_VORHANDEN">
            <COMMENT>PS wird immer IO</COMMENT>
            <PPARGS name="JOBN_SGBD" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_JOB" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_CONDITION1" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="EQUALS(1,1)">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
           </LOADPP>
          </THEN>
          <ELSE>
           <COMMENT>!SP2018</COMMENT>
           <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_EXECUTIVE_LOUNG_FONDKONSOLE_VORHANDEN">
            <COMMENT>PS wird immer IO</COMMENT>
            <PPARGS name="JOBN_SGBD" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_JOB" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_CONDITION1" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="EQUALS(1,1)">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
           </LOADPP>
          </ELSE>
         </IF>
        </THEN>
        <ELSE>
         <IF>
          <COMMENT></COMMENT>
          <BOOL_METHOD>
           <METHOD name="getIStufe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="startsWith" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="S18">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <THEN>
           <COMMENT>SP2018</COMMENT>
           <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_EXECUTIVE_LOUNG_MITTELKONSOLE_HINTEN_VORHANDEN">
            <COMMENT>PS wird immer NIO</COMMENT>
            <PPARGS name="JOBN_SGBD" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_JOB" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_CONDITION1" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="EQUALS(0,1)">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
           </LOADPP>
          </THEN>
          <ELSE>
           <COMMENT>!SP2018</COMMENT>
           <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_EXECUTIVE_LOUNG_FONDKONSOLE_VORHANDEN">
            <COMMENT>PS wird immer NIO</COMMENT>
            <PPARGS name="JOBN_SGBD" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_JOB" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="NONE">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
            <PPARGS name="JOB1_CONDITION1" required="false">
             <COMMENT></COMMENT>
             <CONSTANT type="String" value="EQUALS(0,1)">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PPARGS>
           </LOADPP>
          </ELSE>
         </IF>
        </ELSE>
       </IF>
       <GROUP name="EFII JOBS AT IBS PRUEFLING">
        <COMMENT></COMMENT>
       </GROUP>
      </ELSE>
     </IF>
     <IF>
      <COMMENT>WF_STECKDOSE_MITTELARMLEHNE_HINTEN / WF_POWER_SOCKET_CENTER_ARM_REST_REAR</COMMENT>
      <OR>
       <COMMENT></COMMENT>
       <AND>
        <COMMENT>35up (G11, G12) &amp;&amp; Executive Loung Fondkonsole</COMMENT>
        <OR>
         <COMMENT>35up (G11, G12, G32)</COMMENT>
         <BOOL_METHOD>
          <COMMENT>7 series sedan</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G11">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>7 series sedan extended wheel</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G12">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>5 series GT</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G32">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <BOOL_METHOD>
         <COMMENT>Executive Loung Fondkonsole</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="04F5">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </AND>
       <AND>
        <COMMENT>(RR11, RR12, RR31) &amp; !RSF</COMMENT>
        <OR>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="RR11">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="RR12">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="RR31">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
<NOT>
                  <BOOL_METHOD>
          <COMMENT>!REAR SEAT MIDDLE CONSOLE</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0RSF">
            <COMMENT>Ablagenpaket</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
              </AND>
      </OR>
      <THEN>
       <IF>
        <BOOL_METHOD>
         <METHOD name="getIStufe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="S18">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <THEN>
         <COMMENT>SP2018</COMMENT>
         <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_MITTELARMLEHNE_HINTEN" dename="WF_STECKDOSE_MITTELARMLEHNE_HINTEN" enname="WF_POWER_SOCKET_CENTER_ARM_REST_REAR">
          <COMMENT></COMMENT>
          <PPARGS name="FT" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Power socket storage compartment;center console rear ok?" devalue="Steckdose Mittelarmlehne hinten iO?" envalue="Power socket;center arm rest rear ok?">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="DAUER" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="300000">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="HWT" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Power socket;center arm rest rear NOK" devalue="Steckdose Mittelarmlehne hinten NOK" envalue="Power socket;center arm rest rear NOK">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </THEN>
        <ELSE>
         <COMMENT>!SP2018</COMMENT>
         <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_MITTELKONSOLE_ABLAGEFACH_FOND" dename="WF_STECKDOSE_MITTELKONSOLE_ABLAGEFACH_FOND" enname="WF_POWER_SOCKET_CENTER_CONSOLE_STORAGE_COMPARTMENT_REAR">
          <COMMENT></COMMENT>
          <PPARGS name="FT" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Power socket storage compartment;center console rear ok?" devalue="Steckdose Mittelk.;Ablagefach hinten iO?" envalue="Power socket storage compartment;center console rear ok?">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="DAUER" required="true">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="300000">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
          <PPARGS name="HWT" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Power socket storage compartment;center console rear NOK" devalue="Steckdose Mittelk.;Ablagefach hinten NOK" envalue="Power socket storage compartment;center console rear NOK">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </ELSE>
       </IF>
      </THEN>
     </IF>
     <IF>
      <COMMENT>WF_STECKDOSE_GEPAECKRAUM / WF_POWER_SOCKET_BOOT</COMMENT>
      <OR>
       <OR>
        <COMMENT>35up Serie (F95, F96, F97, F98, G01, G02, G05, G06, G07, G08, G28, G31, G32)</COMMENT>
        <BOOL_METHOD>
         <COMMENT>X5 M</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F95">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>X6 M</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F96">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>X3 M</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F97">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>X4 M</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F98">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>X3</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G01">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>X4 Coup�</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G02">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>X5</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G05">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>X6 Coup�</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G06">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>X7</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G07">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>X3 China</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G08">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>3 Series Limousine Lang (China)</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G28">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <AND>
         <COMMENT>G31 nicht bei Beh�rdenfahrzeug SA146</COMMENT>
         <BOOL_METHOD>
          <COMMENT>5 series wagon</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G31">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
<NOT>
                    <BOOL_METHOD>
           <COMMENT>Beh�rdenfahrzeuge; 12V Steckdose Kofferraum ohne Funktion; SA 146 Zusatzbatterie noch nicht verbaut; Pr�fung erfolgt durch Beh�rdenmitarbeiter</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT>Zusatzbatterie Beh�rdenfahrzeug</COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="0146">
             <COMMENT>Beh�rdenfahrzeug</COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                </AND>
        <BOOL_METHOD>
         <COMMENT>5 series GT</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G32">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
       <AND>
        <COMMENT>35up (F90, F91, F92, F93, G16, G30) und SA575 (Zus�tzliche 12V-Steckdosen)</COMMENT>
        <OR>
         <COMMENT>F90, F91, F92, F93, G16, G30</COMMENT>
         <BOOL_METHOD>
          <COMMENT>M5</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="F90">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>M8 4 Tuer</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="F93">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>6er Gran Coupe 4dr</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G16">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>5 series sedan</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G30">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <BOOL_METHOD>
         <COMMENT>Zus�tzliche 12V-Steckdosen</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0575">
            <COMMENT>Zus�tzliche 12V-Steckdosen</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
<NOT>
                  <BOOL_METHOD>
          <COMMENT>Beh�rdenfahrzeuge; 12V Steckdose Kofferraum ohne Funktion; SA 146 Zusatzbatterie noch nicht verbaut; Pr�fung erfolgt durch Beh�rdenmitarbeiter</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT>Zusatzbatterie Beh�rdenfahrzeug</COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0146">
            <COMMENT>Beh�rdenfahrzeug</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
              </AND>
       <AND>
        <COMMENT>35up (G2x) und SA493 (Ablagenpacket)</COMMENT>
        <OR>
         <COMMENT>G2x</COMMENT>
         <BOOL_METHOD>
          <COMMENT>all 3 Series</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="startsWith" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G2">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <BOOL_METHOD>
         <COMMENT>Ablagenpacket</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0493">
            <COMMENT>Ablagenpacket</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </AND>
       <AND>
        <COMMENT>F40 und SA418</COMMENT>
        <BOOL_METHOD>
         <COMMENT>1er Sports hatch (5 door)</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F40">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>Gep�ckraumpaket</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0418">
            <COMMENT>Zus�tzliche 12V-Steckdosen</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </AND>
       <AND>
        <COMMENT>F44 und SA493</COMMENT>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F44">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>ABLAGENPAKET</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0493">
            <COMMENT>Zus�tzliche 12V-Steckdosen</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </AND>
       <AND>
        <COMMENT>35up L�ndervariante bis 07/0217 (US und CHINA)und G11 oder G12</COMMENT>
        <AND>
         <COMMENT>bis 7/17 und G11, G12</COMMENT>
         <OR>
          <COMMENT>bis 07/17</COMMENT>
          <AND>
           <COMMENT>&lt;07/17</COMMENT>
           <BOOL_METHOD>
            <METHOD name="getIStufeMonatInt" returntype="int">
            </METHOD>
            <OPERATOR type="less" optype="int">
            </OPERATOR>
            <CONSTANT type="int" value="7">
            </CONSTANT>
           </BOOL_METHOD>
           <BOOL_METHOD>
            <METHOD name="getIStufeJahrInt" returntype="int">
            </METHOD>
            <OPERATOR type="eq" optype="int">
            </OPERATOR>
            <CONSTANT type="int" value="17">
            </CONSTANT>
           </BOOL_METHOD>
          </AND>
          <BOOL_METHOD>
           <COMMENT>&lt;17</COMMENT>
           <METHOD name="getIStufeJahrInt" returntype="int">
           </METHOD>
           <OPERATOR type="less" optype="int">
           </OPERATOR>
           <CONSTANT type="int" value="17">
           </CONSTANT>
          </BOOL_METHOD>
         </OR>
         <OR>
          <COMMENT>Derivat (G11, G12)</COMMENT>
          <BOOL_METHOD>
           <COMMENT>7 series sedan</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G11">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>7 series sedan extended wheel</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G12">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
        </AND>
        <OR>
         <COMMENT>L�ndervariante (US, China)</COMMENT>
         <BOOL_METHOD>
          <COMMENT>US vehicles</COMMENT>
          <METHOD name="getL�nderVariante" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="startsWith" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="US">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>China Ausf�hrung</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="08AA">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
       </AND>
       <AND>
        <COMMENT>35up L�ndervariante ab 07/0217 (Entfall China SA 8AA)</COMMENT>
        <AND>
         <COMMENT>ab 2017</COMMENT>
         <OR>
          <AND>
           <BOOL_METHOD>
            <METHOD name="getIStufeMonatInt" returntype="int">
            </METHOD>
            <OPERATOR type="g_equal" optype="int">
            </OPERATOR>
            <CONSTANT type="int" value="7">
            </CONSTANT>
           </BOOL_METHOD>
           <BOOL_METHOD>
            <METHOD name="getIStufeJahrInt" returntype="int">
            </METHOD>
            <OPERATOR type="eq" optype="int">
            </OPERATOR>
            <CONSTANT type="int" value="17">
            </CONSTANT>
           </BOOL_METHOD>
          </AND>
          <BOOL_METHOD>
           <METHOD name="getIStufeJahrInt" returntype="int">
           </METHOD>
           <OPERATOR type="greater" optype="int">
           </OPERATOR>
           <CONSTANT type="int" value="17">
           </CONSTANT>
          </BOOL_METHOD>
         </OR>
         <OR>
          <COMMENT>Derivat</COMMENT>
          <BOOL_METHOD>
           <COMMENT>7 series sedan</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G11">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>7 series sedan extended wheel</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G12">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
        </AND>
        <OR>
         <COMMENT>L�ndervariante</COMMENT>
         <BOOL_METHOD>
          <COMMENT>US vehicles</COMMENT>
          <METHOD name="getL�nderVariante" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="startsWith" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="US">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
       </AND>
       <OR>
        <COMMENT>LK</COMMENT>
        <AND>
         <COMMENT>(F2x || F3x) &amp;&amp; SA493</COMMENT>
         <OR>
          <COMMENT>(F2x &amp;&amp; !(F25 || F26)) || F3x</COMMENT>
          <AND>
           <COMMENT>F2 und !F25 und !F26</COMMENT>
           <BOOL_METHOD>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="startsWith" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="F2">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
<NOT>
                        <BOOL_METHOD>
             <COMMENT></COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <PARAMS>
               <CONSTANT type="String" value="F25">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
          <NOT>
                        <BOOL_METHOD>
             <COMMENT></COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <PARAMS>
               <CONSTANT type="String" value="F26">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                    </AND>
          <BOOL_METHOD>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="startsWith" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="F3">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <BOOL_METHOD>
          <COMMENT>Ablagenpaket</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT>Ablagenpaket</COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="0493">
             <COMMENT>Ablagenpaket</COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </AND>
        <BOOL_METHOD>
         <COMMENT>3er Touring</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="F31">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>3er GT</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="F34">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>3er Limousine Lang (China)</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="F35">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
       <OR>
        <COMMENT>RR31</COMMENT>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="RR31">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
      </OR>
      <THEN>
       <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_GEPAECKRAUM" dename="WF_STECKDOSE_GEPAECKRAUM" enname="WF_POWER_SOCKET_BOOT">
        <COMMENT></COMMENT>
        <PPARGS name="FT" required="true">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Steckdose;Gep�ckraum iO?" devalue="Steckdose;Gep�ckraum iO?" envalue="Power socket; Luggage bay;light on tester on?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="DAUER" required="true">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="300000">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="HWT" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Power socket; Luggage bay;light NOK" devalue="Steckdose;Gep�ckraum NOK" envalue="Power socket; Luggage bay;light NOK">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
     <IF>
      <COMMENT>WF_STECKDOSE_TUER_BEIFAHRER_HINTEN / WF_CIGARETTE_LIGHTER_DOOR_PASSENGER_REAR</COMMENT>
      <OR>
       <AND>
        <COMMENT>(RR11, RR12) &amp; !SA441</COMMENT>
        <OR>
         <COMMENT>RR11, RR12</COMMENT>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equals" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="RR11">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equals" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="RR12">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
<NOT>
                  <BOOL_METHOD>
          <COMMENT>NICHT RAUCHERPAKET</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0441">
            <COMMENT>Ablagenpaket</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
              </AND>
      </OR>
      <THEN>
       <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_TUER_BEIFAHRER_HINTEN" dename="WF_STECKDOSE_TUER_BEIFAHRER_HINTEN" enname="WF_CIGARETTE_LIGHTER_DOOR_PASSENGER_REAR">
        <COMMENT></COMMENT>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="Steckdose T�r Beifahrer hinten iO?" devalue="Steckdose T�r Beifahrer hinten iO?" envalue="Cigarette lighter door passenger rear okay?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="HWT" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Cigarette lighter door passenger rear NOK" devalue="Steckdose T�r Beifahrer hinten NOK" envalue="Cigarette lighter door passenger rear NOK">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_TUER_BEIFAHRER_HINTEN" dename="ZIGARETTENANZUENDER_TUER_BEIFAHRER_HINTEN" enname="CIGARETTE_LIGHTER_DOOR_PASSENGER_REAR">
        <COMMENT>PS wird immer NIO</COMMENT>
        <PPARGS name="JOBN_SGBD" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="NONE">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOB1_JOB" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="NONE">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOB1_CONDITION1" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="EQUALS(0,1)">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
      <ELSE>
       <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_TUER_BEIFAHRER_HINTEN" dename="ZIGARETTENANZUENDER_TUER_BEIFAHRER_HINTEN" enname="CIGARETTE_LIGHTER_DOOR_PASSENGER_REAR">
        <COMMENT>PS wird immer IO</COMMENT>
        <PPARGS name="JOBN_SGBD" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="NONE">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOB1_JOB" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="NONE">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOB1_CONDITION1" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="EQUALS(1,1)">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <GROUP name="EFII JOBS AT IBS PRUEFLING">
        <COMMENT></COMMENT>
       </GROUP>
      </ELSE>
     </IF>
     <IF>
      <COMMENT>WF_STECKDOSE_TUER_FAHRER_HINTEN / WF_CIGARETTE_LIGHTER_DOOR_DRIVER_REAR</COMMENT>
      <OR>
       <AND>
        <COMMENT>(RR11, RR12) &amp; !SA441</COMMENT>
        <OR>
         <COMMENT>RR11, RR12</COMMENT>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equals" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="RR11">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equals" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="RR12">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
<NOT>
                  <BOOL_METHOD>
          <COMMENT>NICHT RAUCHERPAKET</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0441">
            <COMMENT>Ablagenpaket</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
              </AND>
      </OR>
      <THEN>
       <LOADPP fileheader="WerkerFrage" name="WF_STECKDOSE_TUER_FAHRER_HINTEN" dename="WF_STECKDOSE_TUER_FAHRER_HINTEN" enname="WF_CIGARETTE_LIGHTER_DOOR_DRIVER_REAR">
        <COMMENT></COMMENT>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="Steckdose T�r Fahrer hinten iO?" devalue="Steckdose T�r Fahrer hinten iO?" envalue="Cigarette lighter door driver rear okay?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="HWT" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Cigarette lighter door driver rear NOK" devalue="Steckdose T�r Fahrer hinten NOK" envalue="Cigarette lighter door driver rear NOK">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_TUER_FAHRER_HINTEN" dename="ZIGARETTENANZUENDER_TUER_FAHRER_HINTEN" enname="CIGARETTE_LIGHTER_DOOR_DRIVER_REAR">
        <COMMENT>PS wird immer NIO</COMMENT>
        <PPARGS name="JOBN_SGBD" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="NONE">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOB1_JOB" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="NONE">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOB1_CONDITION1" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="EQUALS(0,1)">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
      <ELSE>
       <COMMENT>EFII MEASUREMENT</COMMENT>
       <LOADPP fileheader="DiagData" name="ZIGARETTENANZUENDER_TUER_FAHRER_HINTEN" dename="ZIGARETTENANZUENDER_TUER_FAHRER_HINTEN" enname="CIGARETTE_LIGHTER_DOOR_DRIVER_REAR">
        <COMMENT>PS wird immer IO</COMMENT>
        <PPARGS name="JOBN_SGBD" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="NONE">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOB1_JOB" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="NONE">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOB1_CONDITION1" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="EQUALS(1,1)">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <GROUP name="EFII JOBS AT IBS PRUEFLING">
        <COMMENT></COMMENT>
       </GROUP>
      </ELSE>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="ANHAENGERVORRICHTUNG / TRAILER JIG">
     <COMMENT></COMMENT>
     <IF>
      <BOOL_METHOD>
       <COMMENT>ANHAENGERKUPPLUNG</COMMENT>
       <METHOD name="containsSA" returntype="boolean">
        <COMMENT></COMMENT>
        <PARAMS>
         <CONSTANT type="String" value="03AC">
          <COMMENT>Ablagenpaket</COMMENT>
         </CONSTANT>
        </PARAMS>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <THEN>
       <LOADPP fileheader="WerkerFrage" name="WF_AHM_TASTER_BELEUCHTUNG_PRUEFEN" dename="WF_AHM_TASTER_BELEUCHTUNG_PRUEFEN" enname="WF_TRAILER_JIG_BUTTON_ILLUMINATION_CHECK">
        <COMMENT>�berpr�fung der Verbindung zwischen Steuerger�t und Taster anhand der LED Leuchte im Taster selbst.

Connection check between ECU and button by LED bulb, located on press button.</COMMENT>
        <PPARGS name="FT" required="true">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Leuchtet/blinkt; der AHV-Taster?" devalue="Leuchtet/blinkt; der AHV-Taster?" envalue="Is the AHV-button;lit/blinking?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
         </CONSTANT>
        </PPARGS>
        <PPARGS name="HWT" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="AHM-button lit/blinkt NOK" devalue="AHM-button lit/blinkt NOK" envalue="AHM-button lit/blinking NOK">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="BELEUCHTUNG GEPAECKRAUM / BOOT LIGHT">
     <COMMENT></COMMENT>
     <LOADPP fileheader="WerkerFrage" name="WF_BELEUCHTUNG_GEPAECKRAUM" dename="WF_BELEUCHTUNG_GEPAECKRAUM" enname="WF_BOOT_LIGHT_VISUAL_CHECK">
      <COMMENT>Werker-Frage, ob Gep�ckraumlicht leuchtet</COMMENT>
      <PPARGS name="FT" required="true">
       <COMMENT></COMMENT>
       <METHOD name="getAttribut" returntype="String">
        <COMMENT></COMMENT>
        <PARAMS>
         <CONSTANT type="String" value="QUESTION_TEXT_BOOT_LIGHT">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PARAMS>
       </METHOD>
      </PPARGS>
      <PPARGS name="DAUER" required="true">
       <CONSTANT type="String" value="300000">
       </CONSTANT>
      </PPARGS>
      <PPARGS name="HWT" required="false">
       <COMMENT></COMMENT>
       <CONSTANT type="String" value="Boot Light Defective" devalue="Gep�ckraumleuchte defekt" envalue="Boot Light Defective">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="BELEUCHTUNG HANDSCHUHKASTEN / GLOVE BOX LIGHT">
     <COMMENT></COMMENT>
     <LOADPP fileheader="WerkerFrage" name="WF_BELEUCHTUNG_HANDSCHUHKASTEN" dename="WF_BELEUCHTUNG_HANDSCHUHKASTEN" enname="WF_GLOVE_BOX_VISUAL_CHECK">
      <COMMENT></COMMENT>
      <PPARGS name="FT" required="true">
       <COMMENT></COMMENT>
       <CONSTANT type="String" value="Handschuhkastenlicht i.O.?" devalue="Handschuhkastenlicht i.O.?" envalue="Glove Box Light OK?">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="DAUER" required="true">
       <CONSTANT type="String" value="300000">
       </CONSTANT>
      </PPARGS>
      <PPARGS name="HWT" required="false">
       <COMMENT></COMMENT>
       <CONSTANT type="String" value="Glove Box Light NOK" devalue="Handschuhkastenlicht NOK" envalue="Glove Box Light NOK">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <IF>
      <COMMENT>Licht Geheimfach RR11/RR12

Light secret box for RR11/RR12</COMMENT>
      <OR>
       <BOOL_METHOD>
        <COMMENT></COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="RR11">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT></COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="RR12">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </OR>
      <THEN>
       <LOADPP fileheader="WerkerFrage" name="WF_BELEUCHTUNG_GEHEIMFACH" dename="WF_BELEUCHTUNG_GEHEIMFACH" enname="WF_SECRET_BOX_VISUAL_CHECK">
        <COMMENT></COMMENT>
        <PPARGS name="FT" required="true">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Licht Geheimfach i.O.?" devalue="Licht Geheimfach i.O.?" envalue="Light secret box ok?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
         </CONSTANT>
        </PPARGS>
        <PPARGS name="HWT" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Glove Box Light NOK" devalue="Handschuhkastenlicht NOK" envalue="Glove Box Light NOK">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="BELEUCHTUNG HECKKLAPPE / TAILGATE LIGHT">
     <COMMENT></COMMENT>
     <IF>
      <OR>
       <AND>
<NOT>
                  <BOOL_METHOD>
          <COMMENT>M5</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F90">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
       <NOT>
                  <BOOL_METHOD>
          <COMMENT>5 series sedan</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G30">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
       <NOT>
                  <BOOL_METHOD>
          <COMMENT>5 series sedan extended wheel (China)</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G38">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
       <NOT>
                  <BOOL_METHOD>
          <COMMENT>3 series sedan</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G20">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
       <NOT>
                  <BOOL_METHOD>
          <COMMENT>3 series sedan (China)</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G28">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
       <NOT>
                  <BOOL_METHOD>
          <COMMENT>X5 SAV</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G05">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
              </AND>
       <AND>
        <BOOL_METHOD>
         <COMMENT>G05</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G05">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="04UR">
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </AND>
      </OR>
      <THEN>
       <IF>
        <BOOL_METHOD>
         <METHOD name="getIStufe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equals" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="S18">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <THEN>
         <COMMENT>SP2018</COMMENT>
         <LOADPP fileheader="WerkerFrage" name="WF_BELEUCHTUNG_HECKKLAPPE" dename="WF_BELEUCHTUNG_HECKKLAPPE" enname="WF_HATCH_LIGHT_VISUAL_CHECK">
          <COMMENT></COMMENT>
          <PPARGS name="FT" required="true">
           <COMMENT></COMMENT>
           <METHOD name="getAttribut" returntype="String">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="QUESTION_TEXT_TAILGATE_LIGHT">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </PPARGS>
          <PPARGS name="DAUER" required="true">
           <CONSTANT type="String" value="300000">
           </CONSTANT>
          </PPARGS>
          <PPARGS name="HWT" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Hatch Light Defekt" devalue="Hatch Light Defekt" envalue="Hatch Light Defective.">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </THEN>
        <ELSE>
         <COMMENT>!SP2018</COMMENT>
         <LOADPP fileheader="WerkerFrage" name="WF_BELEUCHTUNG_HECKKLAPPE" dename="WF_BELEUCHTUNG_HECKKLAPPE" enname="WF_TAILGATE LIGHT_VISUAL_CHECK">
          <COMMENT></COMMENT>
          <PPARGS name="FT" required="true">
           <COMMENT></COMMENT>
           <METHOD name="getAttribut" returntype="String">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="QUESTION_TEXT_TAILGATE_LIGHT">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </PPARGS>
          <PPARGS name="DAUER" required="true">
           <CONSTANT type="String" value="300000">
           </CONSTANT>
          </PPARGS>
          <PPARGS name="HWT" required="false">
           <COMMENT></COMMENT>
           <CONSTANT type="String" value="Hatch Light Defekt" devalue="Hatch Light Defekt" envalue="Hatch Light Defective.">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PPARGS>
         </LOADPP>
        </ELSE>
       </IF>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="BELEUCHTUNG KARTENABLAGE / MAP POCKET LIGHT">
     <COMMENT></COMMENT>
     <IF>
      <COMMENT>Roadmap Light (SA 4UR Ambient Light)</COMMENT>
      <AND>
       <COMMENT>(RR4, RR5, RR6) und Ambientes Licht</COMMENT>
       <OR>
        <COMMENT>RR4, RR5, RR6</COMMENT>
        <BOOL_METHOD>
         <COMMENT>Ghost</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equals" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="RR4">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>Wraith</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equals" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="RR5">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>Wraith Cabrio</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equals" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="RR6">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
       <BOOL_METHOD>
        <COMMENT>Ambientes Licht</COMMENT>
        <METHOD name="containsSA" returntype="boolean">
         <PARAMS>
          <CONSTANT type="String" value="04UR">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </AND>
      <THEN>
       <LOADPP fileheader="WerkerFrage" name="WF_DRIVER_DOOR_ROADMAP_LIGHT_OK" dename="LICHT_KARTENABLAGE_FAHRERSEITE_OK" enname="DRIVER_DOOR_MAP_LIGHT_OK">
        <COMMENT>Check ROADMAP light ok?</COMMENT>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
         </CONSTANT>
        </PPARGS>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="Insert hand into the driver door map pocket. Does the map pocket light illuminate?" devalue="Leuchtet die Map-Pocket Beleuchtung Fahrerseite?" envalue="Insert hand into the driver door map pocket. Does the map pocket light illuminate?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="HWT" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="The driver door map pocket light is defected" devalue="Die Map-Pocket Beleuchtung Fahrerseite ist defekt" envalue="The driver door map pocket light is defected">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="WerkerFrage" name="WF_PASSENGER_DOOR_ROADMAP_LIGHT_OK" dename="LICHT_KARTEN_ABLAGE_BEIFAHRERSEITE_OK" enname="PASSENGER_DOOR_MAP_LIGHT_OK">
        <COMMENT>Check ROADMAP light ok?</COMMENT>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
         </CONSTANT>
        </PPARGS>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="Insert hand into the passenger door map pocket. Does the map pocket light illuminate?" devalue="Leuchtet die Map-Pocket Belechtung Beifahrerseite?" envalue="Insert hand into the passenger door map pocket. Does the map pocket light illuminate?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="HWT" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="The passenger door map pocket light is defected" devalue="Die Map-Pocket Beleuchtung Beifahrerseite ist defekt" envalue="The passenger door map pocket light is defected">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="BELEUCHTUNG KENNZEICHEN / LICENSE PLATE LIGHT">
     <COMMENT></COMMENT>
     <IF>
      <OR>
       <COMMENT>F95, G05, G07</COMMENT>
       <BOOL_METHOD>
        <COMMENT>X5 M</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="equals" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F95">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>X5</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="equals" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="G05">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>X7</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="equals" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="G07">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </OR>
      <THEN>
       <LOADPP fileheader="WerkerFrage" name="WF_KENNZEICHENLEUCHTE_EIN_X_MODELS" dename="WF_KENNZEICHENLEUCHTE_EIN_X_MODELS" enname="WF_LICENSE_PLATE_LIGHT_ON_X_MODELS">
        <COMMENT></COMMENT>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="240000">
         </CONSTANT>
        </PPARGS>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="Leuchten beide Kennzeichenleuchten?" devalue="Leuchten beide Kennzeichenleuchten?" envalue="Are Both Licences Plate Lights On?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="TITEL" required="false">
         <CONSTANT type="String" value="FRAGE" devalue="FRAGE" envalue="QUESTION">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="HWT" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="The Licences Plate Lights are OFF" devalue="Die Kennzeichenleuchten leuchten nicht" envalue="The Licences Plate Lights are OFF">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
     <IF>
      <AND>
       <COMMENT>!F95 und !G05 und !G07</COMMENT>
<NOT>
                <BOOL_METHOD>
         <COMMENT>X5 M</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="equals" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F95">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
      <NOT>
                <BOOL_METHOD>
         <COMMENT>X5</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="equals" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="G05">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
      <NOT>
                <BOOL_METHOD>
         <COMMENT>X7</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="equals" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="G07">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
            </AND>
      <THEN>
       <LOADPP fileheader="WerkerFrage" name="WF_KENNZEICHENLEUCHTE_EIN" dename="WF_KENNZEICHENLEUCHTE_EIN" enname="WF_LICENSE_PLATE_LIGHT_ON">
        <COMMENT></COMMENT>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="240000">
         </CONSTANT>
        </PPARGS>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="Leuchten beide Kennzeichenleuchten?" devalue="Leuchten beide Kennzeichenleuchten?" envalue="Are Both Licences Plate Lights On?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="TITEL" required="false">
         <CONSTANT type="String" value="FRAGE" devalue="FRAGE" envalue="QUESTION">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="HWT" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="The Licences Plate Lights are OFF" devalue="Die Kennzeichenleuchten leuchten nicht" envalue="The Licences Plate Lights are OFF">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="BELEUCHTUNG PF�TZE / PUDDLE LIGHT">
     <COMMENT></COMMENT>
     <IF>
      <OR>
       <BOOL_METHOD>
        <COMMENT></COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="RR5">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT></COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="RR6">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </OR>
      <THEN>
       <GROUP name="DRIVER">
        <COMMENT></COMMENT>
        <LOADPP fileheader="WerkerFrage" name="WF_BELEUCHTUNG_PF�TZE_FAHRER" dename="WF_BELEUCHTUNG_PF�TZE_FAHRER" enname="WF_PUDDLE_LIGHT_DRIVER">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Puddle light (next to driver door handle). Illuminated?" envalue="Puddle light (next to driver door handle). Illuminated?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </GROUP>
       <GROUP name="------------------------------------------------">
        <COMMENT></COMMENT>
       </GROUP>
       <GROUP name="PASSENGER">
        <COMMENT></COMMENT>
        <LOADPP fileheader="WerkerFrage" name="WF_BELEUCHTUNG_PF�TZE_BEIFAHRER" dename="WF_BELEUCHTUNG_PF�TZE_BEIFAHRER" enname="WF_PUDDLE_LIGHT_PASSENGER">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Puddle light (next to passenger door handle). Illuminated?" envalue="Puddle light (next to passenger door handle). Illuminated?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </GROUP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="BELEUCHTUNG LESELICHT / READING LIGHT">
     <COMMENT></COMMENT>
     <GROUP name="FRONT">
      <COMMENT></COMMENT>
      <IF>
       <BOOL_METHOD>
        <COMMENT></COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equals" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="RR6">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <THEN>
        <LOADPP fileheader="WerkerFrage" name="WF_READING_LIGHT_FRONT_DRIVER" dename="WF_LESELICHT_VORNE_FAHRER_ECOS" enname="WF_READING_LIGHT_FRONT_DRIVER" teststepid="20342">
         <COMMENT>Werker-Frage, ob Leselicht vorne links IO

Question to check the reading light driver</COMMENT>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="240000">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Switch on Driver Side Reading Light. Light illuminated?" devalue="Leuchtet Leselicht Fahrerseite Front?" envalue="Switch on Driver Side Reading Light. Light illuminated?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="HWT" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="The Driver Side Reading Light is defected" devalue="Die Leselicht Fahrerseite Front Beleuchtung ist defekt" envalue="The Driver Side Reading Light is defected">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
        <LOADPP fileheader="WerkerFrage" name="WF_READING_LIGHT_FRONT_PASSENGER" dename="WF_LESELICHT_VORNE_BEIFAHRER_ECOS" enname="WF_READING_LIGHT_FRONT_PASSENGER" teststepid="20342">
         <COMMENT>Werker-Frage, ob Leselicht vorne links IO

Question to check the reading light passenger</COMMENT>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="240000">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Switch on Passenger Side Reading Light. Light illuminated?" devalue="Leuchtet Leselicht Beifahrerseite Front?" envalue="Switch on Passenger Side Reading Light. Light illuminated?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="HWT" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="The Passenger Side Reading Light is defected" devalue="Die Leselicht Beifahrerseite Front Beleuchtung ist defekt" envalue="The Passenger Side Reading Light is defected">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
     </GROUP>
     <GROUP name="------------------------------------------------">
      <COMMENT></COMMENT>
     </GROUP>
     <GROUP name="REAR">
      <COMMENT></COMMENT>
      <IF>
       <OR>
        <COMMENT>RR11, RR12</COMMENT>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equals" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="RR11">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equals" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="RR12">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
       <THEN>
        <LOADPP fileheader="WerkerFrage" name="WF_READING_LIGHT_REAR_DRIVER" dename="WF_LESELICHT_HINTEN_FAHRER_ECOS" enname="WF_READING_LIGHT_REAR_DRIVER">
         <COMMENT>Werker-Frage, ob Leselicht hinten links IO

Question to check the reading light driver rear okay</COMMENT>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="240000">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Switch on Driver Side Reading Light rear. Light illuminated?" devalue="Leuchtet Leselicht Fahrerseite hinten?" envalue="Switch on Driver Side Reading Light rear. Light illuminated?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="HWT" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="The Driver Side Rear Reading Light is defected" devalue="Die Leselicht Fahrerseite Hinten Beleuchtung ist defekt" envalue="The Driver Side Rear Reading Light is defected">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
        <LOADPP fileheader="WerkerFrage" name="WF_READING_LIGHT_REAR_PASSENGER" dename="WF_LESELICHT_HINTEN_BEIFAHRER_ECOS" enname="WF_READING_LIGHT_REAR_PASSENGER">
         <COMMENT>Werker-Frage, ob Leselicht Beifahrer hinten IO

Question to check the reading light passenger rear</COMMENT>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="240000">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Switch on Passenger Side Reading Light rear. Light illuminated?" devalue="Leuchtet Leselicht Beifahrerseite hinten?" envalue="Switch on Passenger Side Reading Light rear. Light illuminated?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="HWT" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="The Passenger Side Rear Reading Light is defected" devalue="Die Leselicht Beifahrerseite Hinten Beleuchtung ist defekt" envalue="The Passenger Side Rear Reading Light is defected">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
     </GROUP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="BELEUCHTUNG LUFTAUSTROEMER / AIR VENT LIGHT">
     <COMMENT></COMMENT>
     <GROUP name="HELLIGKEITSREGLER / BRIGHTNESS ADJUSTING">
      <COMMENT></COMMENT>
      <IF>
       <OR>
        <OR>
         <COMMENT>Serie (G11, G12, G38)</COMMENT>
         <BOOL_METHOD>
          <COMMENT>7 series sedan</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G11">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>7 series sedan extended wheel</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G12">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>5 series sedan extended wheel (China)</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G38">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <OR>
         <COMMENT>(F97, F98, G01, G02, G08) und SA534</COMMENT>
         <AND>
          <COMMENT>(F97, F98, G01, G02, G08) und SA534</COMMENT>
          <OR>
           <COMMENT>Derivatives (F97, F98, G01, G02, G08)</COMMENT>
           <BOOL_METHOD>
            <COMMENT>X3 M</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="F97">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>X4 M</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="F98">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>X3</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G01">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>X4 Coup�</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G02">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <AND>
            <COMMENT>G08 und !BEV</COMMENT>
            <BOOL_METHOD>
             <COMMENT>X3 China</COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G08">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
<NOT>
                          <BOOL_METHOD>
              <COMMENT>Hybrid Merkmal
HYBR = Hybrid
NOHY = kein Hybrid
PHEV = Plug-In Hybrid</COMMENT>
             <METHOD name="getHybrid" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <PARAMS>
                <CONSTANT type="String" value="BEVE">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                      </AND>
          </OR>
          <OR>
           <COMMENT>options (SA0534)</COMMENT>
           <BOOL_METHOD>
            <COMMENT>Klimaautomatik
- all RHD get SA534</COMMENT>
            <METHOD name="containsSA" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="0534">
               <COMMENT>Klimaautomatik</COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
         </AND>
        </OR>
        <AND>
         <COMMENT>(F90, F91, F92, F93, G30, G31, G32) und (SA04NB, SA0534)</COMMENT>
         <OR>
          <COMMENT>Derivatives (F90, F91, F92, F93, G30, G31, G32)</COMMENT>
          <BOOL_METHOD>
           <COMMENT>M5</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="F90">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>M8 Cabrio 2 Tuer</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="F91">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>M8 Coupe 2 Tuer</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="F92">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>M8 4 Tuer</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="F93">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>5 series sedan</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G30">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>5 series wagon</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G31">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>5 series GT</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G32">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <OR>
          <COMMENT>Options (SA04NB oder SA0534)</COMMENT>
          <BOOL_METHOD>
           <COMMENT>4 Zonen Klimaautomatik</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="04NB">
              <COMMENT>4 Zonen Klimaautomatik</COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>Klimaautomatik</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="0534">
              <COMMENT>Klimaautomatik</COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
        </AND>
       </OR>
       <THEN>
        <LOADPP fileheader="WerkerQuittung" name="WQ_BRIGHTNESS_ADJ_MAX" dename="WQ_HELLIGKEITSREGLER_MAX" enname="WQ_BRIGHTNESS_ADJ_MAX">
         <COMMENT>Helligkeitsregler auf Maximum stellen</COMMENT>
         <PPARGS name="AWT" required="true">
          <CONSTANT type="String" value="Brightness Adj; to maximum please!" devalue="Helligkeitsregler auf; Max drehen!" envalue="Brightness Adj; to maximum please!">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="STYLE" required="true">
          <CONSTANT type="String" value="3">
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
     </GROUP>
     <GROUP name="LICHTSCHALTER / LIGHT SWITCH">
      <COMMENT></COMMENT>
      <IF>
       <AND>
        <COMMENT>F56 BEV</COMMENT>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F56">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <METHOD name="getHybrid" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="BEVE">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>Lichpaket</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0563">
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </AND>
       <THEN>
        <LOADPP fileheader="WerkerQuittung" name="WQ_LIGHT_SWITCH_ON" dename="WQ_LICHTSCHALTER_EIN" enname="WQ_LIGHT_SWITCH_ON">
         <COMMENT>Lichtschalter von 0 (aus) auf 1 (ein) drehen</COMMENT>
         <PPARGS name="AWT" required="true">
          <CONSTANT type="String" value="Turn on the light switch!" devalue="Lichtschalter einschalten!" envalue="Turn on the light switch!">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="STYLE" required="true">
          <CONSTANT type="String" value="3">
          </CONSTANT>
         </PPARGS>
        </LOADPP>
        <LOADPP fileheader="WerkerQuittung" name="WQ_LIGHT_SWITCH_OFF" dename="WQ_LICHTSCHALTER_AUS" enname="WQ_LIGHT_SWITCH_OFF">
         <COMMENT>Lichtschalter von 1 (ein) auf 0 (aus) drehen</COMMENT>
         <PPARGS name="AWT" required="true">
          <CONSTANT type="String" value="Turn off the light switch!" devalue="Lichtschalter ausschalten!" envalue="Turn off the light switch!">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="STYLE" required="true">
          <CONSTANT type="String" value="3">
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
     </GROUP>
     <GROUP name="LUFTAUSTROEMER A SAEULE FRONT / AIR VENT FRONT A COLUMN">
      <COMMENT></COMMENT>
      <IF>
       <OR>
        <OR>
         <COMMENT>Serie (G11, G12, G38)</COMMENT>
         <BOOL_METHOD>
          <COMMENT>7 series sedan</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G11">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>7 series sedan extended wheel</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G12">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>5 series sedan extended wheel (China)</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G38">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <AND>
         <COMMENT>(F97, F98, G01, G02, G08) und SA534</COMMENT>
         <OR>
          <COMMENT>Derivatives (F97, F98, G01, G02, G08)</COMMENT>
          <BOOL_METHOD>
           <COMMENT>X3 M</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="F97">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>X4 M</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="F98">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>X3</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G01">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>X4 Coup�</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G02">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <AND>
           <COMMENT>G08 und !BEVE</COMMENT>
           <BOOL_METHOD>
            <COMMENT>X3 China</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G08">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
<NOT>
                        <BOOL_METHOD>
             <COMMENT>Hybrid Merkmal
HYBR = Hybrid
NOHY = kein Hybrid
PHEV = Plug-In Hybrid</COMMENT>
            <METHOD name="getHybrid" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <PARAMS>
               <CONSTANT type="String" value="BEVE">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                    </AND>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="startsWith" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G2">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <OR>
          <COMMENT>options (SA0534)</COMMENT>
          <BOOL_METHOD>
           <COMMENT>Klimaautomatik
- all RHD get SA534</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="0534">
              <COMMENT>Klimaautomatik</COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
        </AND>
        <OR>
         <COMMENT>Derivates (F9x oder ((G30, G31, G32) und (SA04NB, SA0534))</COMMENT>
         <BOOL_METHOD>
          <COMMENT>M5 / M6 / X3M / X4M</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="startsWith" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="F9">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <AND>
          <COMMENT>(G30, G31, G32) und (SA04NB, SA0534)</COMMENT>
          <OR>
           <COMMENT>(G30, G31, G32)</COMMENT>
           <BOOL_METHOD>
            <COMMENT>5 series sedan</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G30">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>5 series wagon</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G31">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>5 series GT</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G32">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
          <OR>
           <COMMENT>(SA04NB, SA0534)</COMMENT>
           <BOOL_METHOD>
            <COMMENT>4 Zonen Klimaautomatik</COMMENT>
            <METHOD name="containsSA" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="04NB">
               <COMMENT>4 Zonen Klimaautomatik</COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>Klimaautomatik</COMMENT>
            <METHOD name="containsSA" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="0534">
               <COMMENT>Klimaautomatik</COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
         </AND>
        </OR>
       </OR>
       <THEN>
        <LOADPP fileheader="WerkerFrage" name="WF_AIR_VENT_A_COLUMN_DRIVERSIDE" dename="WF_BEL_LUFTAUSTROEMER_A_SAEULE_FAS" enname="WF_AIR_VENT_A_COLUMN_DRIVERSIDE">
         <COMMENT>Nur S15A</COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Handwheel air vent; A column DS Light OK?" devalue="Stellrad Luftgitter FAS; A S�ule Beleuchtung IO?" envalue="Handwheel air vent; A column DS Light OK?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="HWT" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="The A column DS Handwheel air vent is defected" devalue="Die A S�ule Stellrad Luftgitter FS Beleuchtung ist defekt" envalue="The A column DS Handwheel air vent is defected">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
        <LOADPP fileheader="WerkerFrage" name="WF_AIR_VENT_A_COLUMN_PASSENGER_SIDE" dename="WF_BEL_LUFTAUSTROEMER_A_SAEULE_BFS" enname="WF_AIR_VENT_A_COLUMN_PASSENGER_SIDE">
         <COMMENT>Nur S15A</COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Handwheel air vent; A column PS Light OK?" devalue="Stellrad Luftgitter BFS; A S�ule Beleuchtung IO?" envalue="Handwheel air vent; A column PS Light OK?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="HWT" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="The A column PS Handwheel air vent is defected" devalue="Die A S�ule Stellrad Luftgitter BFS Beleuchtung ist defekt" envalue="The A column PS Handwheel air vent is defected">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
     </GROUP>
     <GROUP name="LUFTAUSTROEMER B SAEULE HINTEN / AIR VENT REAR B COLUMN">
      <COMMENT></COMMENT>
      <IF>
       <AND>
<NOT>
                  <BOOL_METHOD>
          <COMMENT>!2 T�ren</COMMENT>
         <METHOD name="getAnzahlT�ren" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="2">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
               <OR>
         <COMMENT>FG11, G12,</COMMENT>
         <BOOL_METHOD>
          <COMMENT>7 series sedan</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G11">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>7 series sedan extended wheel</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G12">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <AND>
          <COMMENT>(F90, F93, G16, G30, G31, G32, G38) und SA04NB</COMMENT>
          <OR>
           <COMMENT>F90, F93, G16, G30, G31, G32, G38</COMMENT>
           <BOOL_METHOD>
            <COMMENT>M5</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="F90">
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>M8 4 Tuer</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="F93">
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>6er Gran Coupe 4dr</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G16">
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>5 series sedan</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G30">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>5 series wagon</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G31">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>5 series GT</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G32">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>5 series sedan extended wheel (China)</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G38">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
          <OR>
           <COMMENT>SA04NB</COMMENT>
           <BOOL_METHOD>
            <COMMENT>4 Zonen Klimaautomatik</COMMENT>
            <METHOD name="containsSA" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="04NB">
               <COMMENT>4 Zonen Klimaautomatik</COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
         </AND>
        </OR>
       </AND>
       <THEN>
        <LOADPP fileheader="WerkerFrage" name="WF_AIR_VENT_B_COLUMN_DRIVERSIDE" dename="WF_BEL_LUFTAUSTROEMER_B_SAEULE_FAS" enname="WF_AIR_VENT_B_COLUMN_DRIVERSIDE">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Handwheel air vent; B column DS Light OK?" devalue="Stellrad Luftgitter FAS; B S�ule Beleuchtung IO?" envalue="Handwheel air vent; B column DS Light OK?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="HWT" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="The B column DS Handwheel air vent is defected" devalue="Die B S�ule Stellrad Luftgitter FS Beleuchtung ist defekt" envalue="The B column DS Handwheel air vent is defected">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
        <LOADPP fileheader="WerkerFrage" name="WF_AIR_VENT_B_COLUMN_PASSENGER_SIDE" dename="WF_BEL_LUFTAUSTROEMER_B_SAEULE_BFS" enname="WF_AIR_VENT_B_COLUMN_PASSENGER_SIDE">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Handwheel air susp.; B column PS Light OK?" devalue="Stellrad Luftgitter BFS; B S�ule Beleuchtung IO?" envalue="Handwheel air susp.; B column PS Light OK?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="HWT" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="The B column PS Handwheel air vent is defected" devalue="Die B S�ule Stellrad Luftgitter BFS Beleuchtung ist defekt" envalue="The B column PS Handwheel air vent is defected">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
     </GROUP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="BELEUCHTUNG MAKE UP SPIEGEL / SUNVISOR LIGHT">
     <COMMENT></COMMENT>
     <GROUP name="------------------------------------------------">
      <COMMENT></COMMENT>
     </GROUP>
     <GROUP name="FRONT">
      <COMMENT></COMMENT>
      <IF>
<NOT>
                <AND>
         <COMMENT>F40 und !SA0563</COMMENT>
        <OR>
         <BOOL_METHOD>
          <COMMENT>1er Sports hatch (5 door)</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equals" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="F40">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>2er Gran Coupe</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equals" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="F44">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
<NOT>
                  <BOOL_METHOD>
          <COMMENT>Lichtpaket</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="0563">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
              </AND>
</NOT>
             <THEN>
        <LOADPP fileheader="WerkerFrage" name="WF_BELEUCHTUNG_MAKEUPSPIEGEL_FA" dename="WF_BELEUCHTUNG_MAKEUPSPIEGEL_FA" enname="WF_Light_SUNVISOR_MAKE_UP_DS">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Make-Up Beleuchtung: Fahrerseite i.O.?" devalue="Make-Up Beleuchtung: Fahrerseite i.O.?" envalue="Sunvisor Light: Driver Side OK?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="HWT" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="Sunvisor Light: Driver Side NOK" devalue="Make-Up Beleuchtung: Fahrerseite NOK" envalue="Sunvisor Light: Driver Side NOK">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
        <LOADPP fileheader="WerkerFrage" name="WF_BELEUCHTUNG_MAKEUPSPIEGEL_BF" dename="WF_BELEUCHTUNG_MAKEUPSPIEGEL_BF" enname="WF_LIGHT_SUNVISOR_MAKE_UP_PS">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Make-Up Beleuchtung: Beifahrerseite i.O.?" devalue="Make-Up Beleuchtung: Beifahrerseite i.O.?" envalue="Sunvisor Light: Pass. Side OK?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="HWT" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="Sunvisor Light: Pass. Side NOK" devalue="Make-Up Beleuchtung: Beifahrerseite NOK" envalue="Sunvisor Light: Pass. Side NOK">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
     </GROUP>
     <GROUP name="------------------------------------------------">
      <COMMENT></COMMENT>
     </GROUP>
     <GROUP name="HINTEN">
      <COMMENT></COMMENT>
      <IF>
       <OR>
        <OR>
         <COMMENT>35up (G12, G38)</COMMENT>
         <BOOL_METHOD>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G12">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G38">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <OR>
         <COMMENT>RR11, RR12</COMMENT>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equals" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="RR11">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equals" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="RR12">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
       </OR>
       <THEN>
        <LOADPP fileheader="WerkerFrage" name="WF_BELEUCHTUNG_MAKEUPSPIEGEL_FOND_FA" dename="WF_BELEUCHTUNG_MAKEUPSPIEGEL_FOND_FA" enname="WF_Light_SUNVISOR_MAKE_UP_REAR_DS">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Make-Up Beleuchtung Hinten: Fahrerseite i.O.?" devalue="Make-Up Beleuchtung Hinten: Fahrerseite i.O.?" envalue="Sunvisor Light Rear: Driver Side OK?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="HWT" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="Sunvisor Light Rear: Driver Side NOK" devalue="Make-Up Beleuchtung Hinten: Fahrerseite NOK" envalue="Sunvisor Light Rear: Driver Side NOK">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
        <LOADPP fileheader="WerkerFrage" name="WF_BELEUCHTUNG_MAKEUPSPIEGEL_FOND_BF" dename="WF_BELEUCHTUNG_MAKEUPSPIEGEL_FOND_BF" enname="WF_Light_SUNVISOR_MAKE_UP_REAR_PS">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Make-Up Beleuchtung Hinten: Beifahrerseite i.O.?" devalue="Make-Up Beleuchtung Hinten: Beifahrerseite i.O.?" envalue="Sunvisor Light Rear: Pass. Side OK?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="HWT" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="Sunvisor Light Rear: Pass. Side NOK" devalue="Make-Up Beleuchtung Hinten: Beifahrerseite NOK" envalue="Sunvisor Light Rear: Pass. Side NOK">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
     </GROUP>
     <GROUP name="------------------------------------------------">
      <COMMENT></COMMENT>
     </GROUP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="COACH-DOOR MODUL / COACH-DOOR MODULE">
     <COMMENT></COMMENT>
     <LOADPP fileheader="WerkerFrage" name="WF_INITIALISIERUNG_COACH_DOOR_STARTEN" dename="WF_INITIALISIERUNG_COACH_DOOR_STARTEN" enname="WQ_START_INIT_COACH_DOOR">
      <COMMENT></COMMENT>
      <PPARGS name="DAUER" required="true">
       <CONSTANT type="String" value="240000">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="FT" required="true">
       <CONSTANT type="String" value="Start initialization coach door?" devalue="Initialisierung Coach Door starten?" envalue="Start initialization coach door?">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="EROLLO KLAPPE / EROLLO FLAP">
     <COMMENT></COMMENT>
     <IF>
      <COMMENT>F95, G05 mit Abdeckrollo ohne 3&#39;te Sitzreihe</COMMENT>
      <AND>
       <COMMENT>F95, G05 und SA0418</COMMENT>
       <OR>
        <COMMENT>F95, G05</COMMENT>
        <BOOL_METHOD>
         <COMMENT>X5 M</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F95">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>X5</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G05">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
       <BOOL_METHOD>
        <COMMENT>Gep�ckraumpaket</COMMENT>
        <METHOD name="containsSA" returntype="boolean">
         <PARAMS>
          <CONSTANT type="String" value="0418">
          </CONSTANT>
         </PARAMS>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </AND>
      <THEN>
       <LOADPP fileheader="WerkerFrage" name="WF_EROLLO_KLAPPE_FREI" dename="WF_EROLLO_KLAPPE_FREI" enname="WF_EROLLO_HATCH_FREE">
        <COMMENT></COMMENT>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
         </CONSTANT>
        </PPARGS>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="Is the Erollo Hatch free?" devalue="Ist die Erollo Klappe frei?" envalue="Is the Erollo Hatch free?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="HWT" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="The Erollo Hatch is not free" devalue="Die Erollo Klappe ist nicht frei" envalue="The Erollo Hatch is not free">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="FACKELHALTER_JAPAN">
     <COMMENT></COMMENT>
     <IF>
      <COMMENT>Fackelhalter</COMMENT>
      <AND>
       <COMMENT>Ansprechpartner:</COMMENT>
       <BOOL_METHOD>
        <METHOD name="containsSA" returntype="boolean">
         <PARAMS>
          <CONSTANT type="String" value="0807">
          </CONSTANT>
         </PARAMS>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </AND>
      <THEN>
       <LOADPP fileheader="WerkerFrage" name="WF_FACKELHALTER_JAPAN">
        <COMMENT></COMMENT>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
         </CONSTANT>
        </PPARGS>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="JAPAN Fackelhalter">
          <COMMENT></COMMENT>
         </CONSTANT>
         <CONSTANT type="String" value="verbaut?">
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="FENSTERHEBER / WINDOW LIFTER">
     <COMMENT></COMMENT>
     <GROUP name="------------------------------------------------">
      <COMMENT></COMMENT>
     </GROUP>
     <GROUP name="FENSTER SCHLIESSEN / CLOSE WINDOWS">
      <COMMENT></COMMENT>
      <LOADPP fileheader="WerkerFrage" name="WF_FENSTER_SCHLIESSEN" dename="WF_FENSTER_SCHLIESSEN" enname="WF_CLOSE_WINDOWS">
       <COMMENT></COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="240000">
        </CONSTANT>
       </PPARGS>
       <PPARGS name="FT" required="true">
        <CONSTANT type="String" value="ALLE FENSTER; SCHLIESSEN?" devalue="ALLE FENSTER; SCHLIESSEN?" envalue="CLOSE ALL WINDOWS?">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="TITEL" required="false">
        <CONSTANT type="String" value="FRAGE" envalue="QUESTION">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="HWT" required="false">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="The windows are not closed" devalue="Die Fenster sind nicht geschlossen" envalue="The windows are not closed">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="WerkerFrage" name="WF_WINDOW_INIT" dename="WF_FENSTER_INIT" enname="WF_WINDOW_INIT">
       <COMMENT>f�r Oxford</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="240000">
        </CONSTANT>
       </PPARGS>
       <PPARGS name="FT" required="true">
        <CONSTANT type="String" value="Initialise windows?" devalue="Fenster initialisieren?" envalue="Initialise windows?">
         <COMMENT></COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="Shut all Doors?" devalue="Alle T�ren schlie�en?" envalue="Shut all Doors?">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="HWT" required="false">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="Windows are not initialized" devalue="Die Fenster sind nicht initialisiert" envalue="Windows are not initialized">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="WerkerFrage" name="WF_AREA_CLEAR" dename="WF_PRUEF_UMGEBUNG_FREI" enname="WF_AREA_CLEAR">
       <COMMENT>f�r Oxford</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="240000">
        </CONSTANT>
       </PPARGS>
       <PPARGS name="FT" required="true">
        <CONSTANT type="String" value="Windows closing" devalue="Fenster schlie�en" envalue="Windows closing">
         <COMMENT></COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="Automatically!" devalue="automatisch!" envalue="Automatically!">
         <COMMENT></COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="-All Doors Shut?" devalue="-Sind alle T�ren geschlossen" envalue="-All Doors Shut?">
         <COMMENT></COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="-Area clear?" devalue="und ist der Bereich frei?" envalue="-Area clear?">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="TITEL" required="false">
        <CONSTANT type="String" value="***WARNING***">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="HWT" required="false">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="Windows are not closed" devalue="Fenster sind nicht geschlossen" envalue="Windows are not closed">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
     </GROUP>
     <GROUP name="------------------------------------------------">
      <COMMENT></COMMENT>
     </GROUP>
     <GROUP name="FENSTER NICHT INITIALISIEREN">
      <COMMENT></COMMENT>
      <LOADPP fileheader="WerkerFrage" name="WF_KANN_FENSTER_INIT" dename="WF_KANN_FENSTER_INIT" enname="WF_CAN_WINDOWS_INIT">
       <COMMENT></COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="240000">
        </CONSTANT>
       </PPARGS>
       <PPARGS name="FT" required="true">
        <CONSTANT type="String" value="K�nnen die Fenster initialisieren?" devalue="K�nnen die Fenster initialisieren?" envalue="Can the windows initialize?">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="TITEL" required="false">
        <CONSTANT type="String" value="FRAGE" envalue="QUESTION">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="HWT" required="false">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="The windows can not be initialize" devalue="Die Fenster k�nnen nicht initialisieren" envalue="The windows can not be initialize">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
     </GROUP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="FRISCHLUFTGRILL BEIFAHRERSEITE / AIR VENT PASSENGER SIDE">
     <COMMENT></COMMENT>
     <IF>
      <OR>
       <BOOL_METHOD>
        <COMMENT>1er 5-T�rer</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F20">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>1er 3-T�rer</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F21">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>2er Coup�</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F22">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>2er Cabriolet</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F23">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>M2 Coup�</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F87">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </OR>
      <THEN>
       <LOADPP fileheader="WerkerFrage" name="WF_FUNKTION_FRISCHLUFTGRILL_BFS_IO" dename="WF_FUNKTION_FRISCHLUFTGRILL_BFS_IO" enname="WF_AIR_VENT_FUNCTION_PASSENGER_SIDE_OK">
        <COMMENT></COMMENT>
        <PPARGS name="TITEL" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="FRISCHLUFTGRILL BFS:" devalue="FRISCHLUFTGRILL BFS:" envalue="AIR VENT PASSENGER:">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="Gitter links/rechts oben/unten?" devalue="Gitter links/rechts oben/unten?" envalue="Air vent left/right up/down?">
          <COMMENT></COMMENT>
         </CONSTANT>
         <CONSTANT type="String" value="Luftstrom auf/zu?" devalue="Luftstrom auf/zu?" envalue="Air flow on/off?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="HWT" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Frischluftgrill Beifahrerseite defekt." devalue="Frischluftgrill Beifahrerseite defekt." envalue="Air vent passenger side defective.">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="HDC_BUTTON">
     <COMMENT></COMMENT>
     <IF>
<NOT>
              <BOOL_METHOD>
        <COMMENT></COMMENT>
       <METHOD name="getAntriebsArt" returntype="String">
        <COMMENT></COMMENT>
        <METHOD name="equals" returntype="boolean">
         <PARAMS>
          <CONSTANT type="String" value="AR">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
           <THEN>
       <LOADPP fileheader="WerkerFrage" name="WQ_HDC_BUTTON_NOT_INSTALLED_CHECK" dename="WF_HDC_TASTER_NICHT_VERBAUT" enname="WQ_HDC_BUTTON_NOT_INSTALLED_CHECK">
        <COMMENT></COMMENT>
        <PPARGS name="TITEL" required="false">
         <CONSTANT type="String" value="Visual Check" devalue="Sichtpr�fung" envalue="Visual Check">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="240000">
         </CONSTANT>
        </PPARGS>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="Is the HDC button there on the middle console? YES or NO" devalue="HDC Taster NICHT verbaut" envalue="Is the HDC button there on the middle console? YES or NO">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="DiagData" name="CREATE_NOK_INNER_HDC_BUTTON" dename="CREATE_NOK_INNER_HDC_BUTTON" enname="CREATE_NOK_INNER_HDC_BUTTON">
        <COMMENT></COMMENT>
        <PPARGS name="JOBN_HWT1" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="HDC Button installed in Error!" devalue="Falschverbau HDC-Taster. Es sollte keiner verbaut sein." envalue="HDC Button installed in Error!">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOBN_SGBD" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="none">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOBN_JOB" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="none">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOBN_CONDITION1" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="EQUALS(OK,NOK)">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="KUEHLBOX / COOL BOX">
     <COMMENT></COMMENT>
     <IF>
      <AND>
       <OR>
        <COMMENT>35up LG</COMMENT>
        <BOOL_METHOD>
         <COMMENT>7 series sedan</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G11">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>7 series sedan extended wheel</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G12">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
       <BOOL_METHOD>
        <COMMENT>INDIVIDUAL KUEHLBOX</COMMENT>
        <METHOD name="containsSA" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="0791">
           <COMMENT>Ablagenpaket</COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </AND>
      <THEN>
       <LOADPP fileheader="WerkerFrage" name="WF_COOL_BOX_ILLUMINATION_CHECK" dename="WF_KUEHLBOX_BELEUCHTUNG_PRUEFEN" enname="WF_COOL_BOX_ILLUMINATION_CHECK">
        <COMMENT></COMMENT>
        <PPARGS name="FT" required="true">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Switch on icebox. LED on or blinking?" devalue="K�hlbox einschalten, leuchtet/blinkt LED?" envalue="Switch on icebox. LED on or blinking?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
         </CONSTANT>
        </PPARGS>
        <PPARGS name="HWT" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="The icebox. LED is OFF" devalue="Die K�hlbox LED Beleuchtung ist OFF" envalue="The icebox. LED is OFF">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="LEHNENFERNENTRIEGELUNG / REMOTE UNLOCK FOR BACK REST">
     <COMMENT></COMMENT>
     <IF>
      <OR>
       <AND>
        <COMMENT>(F45 || F48) &amp; SA4FD</COMMENT>
        <OR>
         <COMMENT>LU</COMMENT>
         <AND>
          <COMMENT>F45 and !China</COMMENT>
          <BOOL_METHOD>
           <COMMENT>SOP: 07/14</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="F45">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
<NOT>
                      <BOOL_METHOD>
            <COMMENT>China Ausf�hrung</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT>China</COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="08AA">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                  </AND>
         <BOOL_METHOD>
          <COMMENT>SOP: 07/15</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="F48">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>X1 Coup�</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT>X1 Coup�</COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="F39">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <BOOL_METHOD>
         <COMMENT>Sitzverstellung f�r Fondsitze</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT>Lehnenfernentriegelung</COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="04FD">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </AND>
       <BOOL_METHOD>
        <COMMENT>Serie</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="F46">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>Serie</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="M13">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </OR>
      <THEN>
       <LOADPP fileheader="WerkerAnweisung" name="WA_PULL_BOTH_SWITCH_LFE" dename="WA_BEIDE_TASTER_LFE_ZIEHEN" enname="WA_PULL_BOTH_SWITCH_LFE">
        <COMMENT></COMMENT>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="3000">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="FENSTERNAME" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Fold down the backrest:" devalue="Lehnenfernentriegelung:" envalue="Fold down the backrest:">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="AWT" required="true">
         <CONSTANT type="String" value="first pull the right then the leftt switch." devalue="zuerst rechten dann linken Taster bet�tigen." envalue="first pull the right then the left switch.">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="WerkerFrage" name="WF_FOLD_DOWN_BACKREST" dename="WF_R�CKLEHNE_UMGEKLAPPT" enname="WF_FOLD_DOWN_BACKREST">
        <COMMENT></COMMENT>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="240000">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="Is the backrest fold down completely?" devalue="Ist die R�cklehne komplett umgeklappt?" envalue="Is the backrest fold down completely?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="WerkerQuittung" name="WQ_FOLD_UP_PART_1_OF_THE_BACKREST" dename="WQ_TEIL_1_DER_RUECKENLEHNE_HOCHKLAPPEN" enname="WQ_FOLD_UP_PART_1_OF_THE_BACKREST">
        <COMMENT></COMMENT>
        <PPARGS name="AWT" required="true">
         <CONSTANT type="String" value="Please fold up backrest passenger side." devalue="Bitte R�ckenlehne BF Seite hochklappen." envalue="Please fold up backrest passenger side">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="STYLE" required="true">
         <CONSTANT type="String" value="3">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="WerkerQuittung" name="WQ_FOLD_UP_PART_2_OF_THE_BACKREST" dename="WQ_TEIL_2_DER_RUECKENLEHNE_HOCHKLAPPEN" enname="WQ_FOLD_UP_PART_2_OF_THE_BACKREST">
        <COMMENT></COMMENT>
        <PPARGS name="AWT" required="true">
         <CONSTANT type="String" value="Please fold up backrest driver side." devalue="Bitte R�ckenlehne FA Seite hochklappen." envalue="Please fold up backrest driver side.">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="STYLE" required="true">
         <CONSTANT type="String" value="3">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="WerkerQuittung" name="WQ_LEAVE_THE_CAR_LFE" dename="WQ_FZG_VERLASSEN_LFE" enname="WQ_LEAVE_THE_CAR_LFE">
        <COMMENT></COMMENT>
        <PPARGS name="AWT" required="true">
         <CONSTANT type="String" value="Fahrzeug;verlassen" devalue="Fahrzeug;verlassen" envalue="please leave car">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="STYLE" required="true">
         <CONSTANT type="String" value="3">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="SITZLEHNENVERSTELLUNG HINTEN / SEAT BACKREST MOVEMENT REAR">
     <COMMENT></COMMENT>
     <IF>
      <COMMENT>G32 und SA 461</COMMENT>
      <AND>
       <COMMENT>G32 und SA 461</COMMENT>
       <BOOL_METHOD>
        <COMMENT>G32</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="equals" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="G32">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>Sitzlehnenverstellung hinten</COMMENT>
        <METHOD name="containsSA" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="0461">
           <COMMENT>Sitzlehnenverstellung hinten</COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </AND>
      <THEN>
       <LOADPP fileheader="WerkerFrage" name="WF_SEAT_BACKREST_PS_REAR_FORWARD" dename="WF_SITZLEHNE_BFH_VOR_FAHREN" enname="WF_SEAT_BACKREST_PS_REAR_FORWARD">
        <COMMENT></COMMENT>
        <PPARGS name="FT" required="true">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Move backrest PS rear forward! Moving PS rear forward?" devalue="BFH Lehne vorfahren! F�hrt Lehne BFH vor?" envalue="Move backrest PS rear forward! Moving PS rear forward?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="WerkerFrage" name="WF_SEAT_BACKREST_PS_REAR_BACKWARD" dename="WF_SITZLEHNE_BFH_ZUR�CK_FAHREN" enname="WF_SEAT_BACKREST_PS_REAR_BACKWARD">
        <COMMENT></COMMENT>
        <PPARGS name="FT" required="true">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Move backrest PS rear backward! Moving PS rear backward?" devalue="BFH Lehne zur�ckfahren! F�hrt Lehne BFH zur�ck?" envalue="Move backrest PS rear backward! Moving PS rear backward?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="WerkerFrage" name="WF_SEAT_BACKREST_DS_REAR_FORWARD" dename="WF_SITZLEHNE_FAH_VOR_FAHREN" enname="WF_SEAT_BACKREST_DS_REAR_FORWARD">
        <COMMENT></COMMENT>
        <PPARGS name="FT" required="true">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Move backrest DS rear forward! Moving DS rear forward?" devalue="FAH Lehne vorfahren! F�hrt Lehne FAH vor?" envalue="Move backrest DS rear forward! Moving DS rear forward?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="WerkerFrage" name="WF_SEAT_BACKREST_DS_REAR_BACKWARD" dename="WF_SITZLEHNE_FAH_ZUR�CK_FAHREN" enname="WF_SEAT_BACKREST_DS_REAR_BACKWARD">
        <COMMENT></COMMENT>
        <PPARGS name="FT" required="true">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Move backrest DS rear backward! Moving DS rear backward?" devalue="FAH Lehne zur�ckfahren! F�hrt Lehne FAH zur�ck?" envalue="Move backrest DS rear backward! Moving DS rear backward?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="MECH TUERSCHLOSSPRUEFUNG / MECH KEY LOCK CHECK">
     <COMMENT></COMMENT>
     <IF>
      <OR>
<NOT>
                <BOOL_METHOD>
         <COMMENT>S15A: The PS are in the CAR_ACCESS_SYSTEM (WF_MECHAN_SCHLUESSEL_FT_ENTRIEGELT and WF_MECHAN_SCHLUESSEL_FT_VERRIEGELT)</COMMENT>
        <METHOD name="getIStufeBRV" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="startsWith" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="S15">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
             <AND>
        <COMMENT>G11, G12 LCI</COMMENT>
        <BOOL_METHOD>
         <COMMENT>S15A 
G11 7 series
G12 7 series long
G14 6 series coupe
G15 6 series convertible
G16 6 series GranCoupe
G30 5 series sedan
G31 5 series wagon
G32 5 series GT
G38 5 series extended wheel base
F90 M5
RR11 RR Phantom nachfolger.
RR12 RR Phantom EWB nachfolger.</COMMENT>
         <METHOD name="getIStufeBRV" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="S15">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <OR>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G11">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G12">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <OR>
         <COMMENT></COMMENT>
         <AND>
          <BOOL_METHOD>
           <METHOD name="getIStufeMonatInt" returntype="int">
            <COMMENT></COMMENT>
           </METHOD>
           <OPERATOR type="g_equal" optype="int">
           </OPERATOR>
           <CONSTANT type="int" value="3">
           </CONSTANT>
          </BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getIStufeJahrInt" returntype="int">
            <COMMENT></COMMENT>
           </METHOD>
           <OPERATOR type="eq" optype="int">
           </OPERATOR>
           <CONSTANT type="int" value="19">
           </CONSTANT>
          </BOOL_METHOD>
         </AND>
         <BOOL_METHOD>
          <METHOD name="getIStufeJahrInt" returntype="int">
           <COMMENT></COMMENT>
          </METHOD>
          <OPERATOR type="greater" optype="int">
          </OPERATOR>
          <CONSTANT type="int" value="19">
          </CONSTANT>
         </BOOL_METHOD>
        </OR>
       </AND>
       <AND>
        <COMMENT>KSP G11, G12 LCI</COMMENT>
        <BOOL_METHOD>
         <COMMENT>S15A 
G11 7 series
G12 7 series long
G14 6 series coupe
G15 6 series convertible
G16 6 series GranCoupe
G30 5 series sedan
G31 5 series wagon
G32 5 series GT
G38 5 series extended wheel base
F90 M5
RR11 RR Phantom nachfolger.
RR12 RR Phantom EWB nachfolger.</COMMENT>
         <METHOD name="getIStufeBRV" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="S15">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <OR>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G11">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G12">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <BOOL_METHOD>
         <METHOD name="getOrderWerk" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="02">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>SX Code DGF f�r KSP G11 G12 LCI</COMMENT>
         <METHOD name="containsSX" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="00B4">
            <COMMENT>SX Code DGF f�r KSP G11 G12 LCI</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </AND>
      </OR>
      <THEN>
       <LOADPP fileheader="WerkerFrage" name="WD_MECH_KEY_DRIVER_DOOR_UNLOCKED" dename="WF_MECHAN_SCHLUESSEL_FT_ENTRIEGELT" enname="WD_MECH_KEY_DRIVER_DOOR_UNLOCKED">
        <COMMENT></COMMENT>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="Unlock Door with Mechanical Key." devalue="Mit mechn. Schluessel oeffnen" envalue="Unlock Door with Mechanical Key.">
          <COMMENT></COMMENT>
         </CONSTANT>
         <CONSTANT type="String" value="driver door unlocked?" devalue="FT entriegelt?" envalue="driver door unlocked?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="HWT" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Door not unlocked" devalue="T�r nicht entriegelt" envalue="Door not unlocked">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="WerkerFrage" name="WD_MECH_KEY_DRIVER_DOOR_LOCKED" dename="WF_MECHAN_SCHLUESSEL_FT_VERRIEGELT" enname="WD_MECH_KEY_DRIVER_DOOR_LOCKED">
        <COMMENT></COMMENT>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="Lock Door with Mechanical Key." devalue="Mit mechn. Schluessel schliessen" envalue="Lock Door with Mechanical Key.">
          <COMMENT></COMMENT>
         </CONSTANT>
         <CONSTANT type="String" value="driver door locked?" devalue="FT verriegelt?" envalue="driver door locked?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="HWT" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Door not locked" devalue="T�r nicht verriegelt" envalue="Door not locked">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="MSA-TASTER / MSA BUTTON">
     <COMMENT></COMMENT>
     <LOADPP fileheader="WerkerFrage" name="WF_START_STOP_TASTER_MSA">
      <COMMENT></COMMENT>
      <PPARGS name="DAUER" required="true">
       <CONSTANT type="String" value="300000">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="FT" required="true">
       <METHOD name="getAttribut" returntype="String">
        <COMMENT></COMMENT>
        <PARAMS>
         <CONSTANT type="String" value="QUESTION_TEXT_MSA_BUTTON">
         </CONSTANT>
        </PARAMS>
       </METHOD>
      </PPARGS>
     </LOADPP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="PDC_BUTTON">
     <COMMENT></COMMENT>
     <IF>
      <AND>
<NOT>
                <BOOL_METHOD>
         <COMMENT>PDC</COMMENT>
        <METHOD name="containsSA" returntype="boolean">
         <PARAMS>
          <CONSTANT type="String" value="0508">
          </CONSTANT>
         </PARAMS>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
      <NOT>
                <BOOL_METHOD>
         <COMMENT>SOP: 12/14</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F85">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
      <NOT>
                <BOOL_METHOD>
         <COMMENT>SOP: 12/14</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F86">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
            </AND>
      <THEN>
       <LOADPP fileheader="WerkerFrage" name="WQ_PDC_BUTTON_NOT_INSTALLED_CHECK" dename="WF_PDC_TASTER_NICHT_VERBAUT" enname="WQ_PDC_BUTTON_NOT_INSTALLED_CHECK">
        <COMMENT></COMMENT>
        <PPARGS name="TITEL" required="false">
         <CONSTANT type="String" value="Visual Check" devalue="Sichtpr�fung" envalue="Visual Check">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="240000">
         </CONSTANT>
        </PPARGS>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="Is PDC button present? YES or NO" devalue="PDC Taster NICHT verbaut" envalue="Is PDC button present? YES or NO">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="DiagData" name="CREATE_NOK_INNER_PDC_BUTTON" dename="CREATE_NOK_INNER_PDC_BUTTON" enname="CREATE_NOK_INNER_PDC_BUTTON">
        <COMMENT></COMMENT>
        <PPARGS name="JOBN_HWT1" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="PDC Button installed in Error!" devalue="Falschverbau PDC-Taster. Es sollte keiner verbaut sein." envalue="PDC Button installed in Error!">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOBN_SGBD" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="none">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOBN_JOB" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="none">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOBN_CONDITION1" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="EQUALS(OK,NOK)">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="POWERDOWN / POWERDOWN">
     <COMMENT></COMMENT>
     <LOADPP fileheader="WerkerFrage" name="WF_POWERDOWN_DURCHFUEHREN">
      <COMMENT></COMMENT>
      <PPARGS name="TITEL" required="false">
       <CONSTANT type="String" value="Powerdown" devalue="Powerdown" envalue="Powerdown">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="DAUER" required="true">
       <CONSTANT type="String" value="240000">
       </CONSTANT>
      </PPARGS>
      <PPARGS name="FT" required="true">
       <CONSTANT type="String" value="Powerdown durchf�hren?" devalue="Powerdown durchf�hren?">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerFrage" name="WF_POWERDOWN_VERKUERZT">
      <COMMENT></COMMENT>
      <PPARGS name="TITEL" required="false">
       <CONSTANT type="String" value="verk�rzt vs. nat�rlich" devalue="verk�rzt vs. nat�rlich">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="DAUER" required="true">
       <CONSTANT type="String" value="240000">
       </CONSTANT>
      </PPARGS>
      <PPARGS name="FT" required="true">
       <CONSTANT type="String" value="Verk�rzter Powerdown? (NEIN= nat�rlicher Powerdown)" devalue="Verk�rzter Powerdown? (NEIN= nat�rlicher Powerdown)">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="SCHIEBEDACH / SUNROOF">
     <COMMENT></COMMENT>
     <IF>
      <BOOL_METHOD>
       <METHOD name="containsSA" returntype="boolean">
        <COMMENT></COMMENT>
        <PARAMS>
         <CONSTANT type="String" value="0403">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PARAMS>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <THEN>
       <LOADPP fileheader="WerkerFrage" name="WF_SCHIEBEDACH_SCHLIESST" dename="WF_SCHIEBEDACH_SCHLIESST" enname="WF_CLOSE_SUNROOF">
        <COMMENT></COMMENT>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="240000">
         </CONSTANT>
        </PPARGS>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="SCHIEBEDACH;SCHLIESST?" devalue="SCHIEBEDACH;SCHLIESST?" envalue="CLOSE THE SUNROOF?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="TITEL" required="false">
         <CONSTANT type="String" value="FRAGE" envalue="QUESTION">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="HWT" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Sunroof opened" devalue="Schiebedach ge�ffnetC" envalue="Sunroof opened">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="TANKDECKELPR�FUNG - FUEL FILLER FLAP">
     <COMMENT></COMMENT>
     <IF>
      <COMMENT>35up &amp;&amp; !PHEV</COMMENT>
      <AND>
       <OR>
        <COMMENT>35up</COMMENT>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F4">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>M5 / M6 / X3M / X4M</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F9">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>35up</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
<NOT>
                <BOOL_METHOD>
         <COMMENT>NOT PHEV</COMMENT>
        <METHOD name="getHybrid" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="startsWith" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="PHEV">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
            </AND>
      <THEN>
       <LOADPP fileheader="DiagData" name="TEST_FUEL_FILLER_FLAP_NECESSARY" dename="TEST_TANKKLAPPE_NOTWENDIG" enname="TEST_FUEL_FILLER_FLAP_NECESSARY">
        <COMMENT>PS wird immer IO</COMMENT>
        <PPARGS name="JOBN_SGBD" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="NONE">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOB1_JOB" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="NONE">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOB1_CONDITION1" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="EQUALS(1,1)">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="WerkerFrage" name="WF_FUEL_FILLER_FLAP_CLOSED" dename="WF_TANKKLAPPE_GESCHLOSSEN" enname="WF_FUEL_FILLER_FLAP_CLOSED">
        <COMMENT></COMMENT>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="Press flap, is Fuel Filler Flap locked and closed? y/n" devalue="Tankklappe;geschlossen ?" envalue="Press flap, is Fuel Filler Flap locked and closed? y/n">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
         </CONSTANT>
        </PPARGS>
        <PPARGS name="HWT" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="The Fuel Filler Flap is unlocked or open" devalue="Tankklappe ist ge�ffnet" envalue="The Fuel Filler Flap is unlocked or open">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="WerkerFrage" name="WF_FUEL_FILLER_FLAP_OPEN" dename="WF_TANKKLAPPE_GEOEFFNET" enname="WF_FUEL_FILLER_FLAP_OPEN">
        <COMMENT></COMMENT>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="Press flap, is Fuel Filler Flap unlocked and opened? y/n � then close again" devalue="Tankklappe;ge�ffnet ?" envalue="Press flap, is Fuel Filler Flap unlocked and opened? y/n � then close again">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="300000">
         </CONSTANT>
        </PPARGS>
        <PPARGS name="HWT" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="The Fuel Filler Flap is locked or closed" devalue="Tankklappe ist geschlossen" envalue="The Fuel Filler Flap is locked or closed">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
      <ELSE>
       <LOADPP fileheader="DiagData" name="TEST_FUEL_FILLER_FLAP_NECESSARY" dename="TEST_TANKKLAPPE_NOTWENDIG" enname="TEST_FUEL_FILLER_FLAP_NECESSARY">
        <COMMENT>PS wird immer NIO</COMMENT>
        <PPARGS name="JOBN_SGBD" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="NONE">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOB1_JOB" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="NONE">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="JOB1_CONDITION1" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="EQUALS(0,1)">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </ELSE>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="UNTERE HECKKLAPPE / LOWER TAILGATE">
     <COMMENT></COMMENT>
     <IF>
      <OR>
       <BOOL_METHOD>
        <COMMENT>SOP: 12/14</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F85">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT></COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F15">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </OR>
      <THEN>
       <LOADPP fileheader="WerkerFrage" name="WF_CHECK_LOWER_TAILGATE">
        <COMMENT></COMMENT>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="100000">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="Is Lower Tailgate raised and locked at rear?" envalue="Is Lower Tailgate raised and locked at rear?">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="WCA - WIRELESS CHARGING ABLAGE">
     <COMMENT></COMMENT>
     <IF>
      <COMMENT>WCA</COMMENT>
      <OR>
       <COMMENT>Mit gAMS AI1742, A90296 und AE8378 wurde die SA6NW (WCA) mit LTE-Kompensator freigegeben bzw. die Liste der zugelassenen L�nder erg�nzt. Leider ist der LTE Kompensator in vielen L�ndern nicht oder nicht mit vertretbarem Aufwand zulassungsf�hig, in diesen L�ndern kann dann die SA6NW (WCA) nicht angeboten werden.

Ausnahme: L�nder, die die WCA nicht zulassen, d�rfen weder die SA6NW noch die SA6NV erhalten. Sperrliste wird mit EG-161 abgestimmt.
--------------------------------------------------------

With gAMS A90296 and AE8378 the SA6NW (WCA) has been released with LTE-compensator and/or te list of approved countries has been supplemented.However is the LTE compensator not street legal or not street legal with reasonable expenditures in many countries, in these countries the SA6NW (WCA) cannot be offered.

With the new SA6NV the WCA (wireless charging storage tray) can be offered in significantly more countries.

In countries with registration of the LTE compensator as SA6NW and in countries without registration as SA6NV.
Exception: Countries prohibiting WCA shall neither receive SA6NW nor SA6NV. disable list is agreed with EG-161.</COMMENT>
       <BOOL_METHOD>
        <COMMENT>WCA</COMMENT>
        <METHOD name="containsSA" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="06NW">
          </CONSTANT>
         </PARAMS>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>WCA mit passivem Kompensator</COMMENT>
        <METHOD name="containsSA" returntype="boolean">
         <COMMENT></COMMENT>
         <PARAMS>
          <CONSTANT type="String" value="06NV">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </OR>
      <THEN>
       <LOADPP fileheader="WerkerQuittung" name="WQ_INSERT_WCA_TEST_ADAPTER" dename="WQ_WCA_TESTER_EINLEGEN" enname="WQ_INSERT_WCA_TEST_ADAPTER">
        <COMMENT></COMMENT>
        <PPARGS name="AWT" required="true">
         <CONSTANT type="String" value="Please insert the WCA test adapter." devalue="Bitte WCA Testadapter einstecken." envalue="Please insert the WCA test adapter.">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="STYLE" required="true">
         <CONSTANT type="String" value="3">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="WerkerQuittung" name="WQ_REMOVE_WCA_TEST_ADAPTER" enname="WQ_REMOVE_WCA_TEST_ADAPTER">
        <COMMENT></COMMENT>
        <PPARGS name="AWT" required="true">
         <CONSTANT type="String" value="Please remove the WCA test adapter." devalue="Bitte WCA Testadapter entnehmen." envalue="Please remove the WCA test adapter.">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="STYLE" required="true">
         <CONSTANT type="String" value="3">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="WerkerFrage" name="WD_WCA_OK" dename="WF_WCA_IO" enname="WD_WCA_OK">
        <COMMENT></COMMENT>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="60000">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="TITEL" required="false">
         <COMMENT></COMMENT>
         <CONSTANT type="String" value="Pull the WCA slide and hold:" devalue="Pr�fung WirelessChargingAblage:" envalue="Pull the WCA slide and hold:">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="FT" required="true">
         <CONSTANT type="String" value="Is the blue LED solidly on? LED will switch off after 10s." devalue="Leuchtet die blaue LED der WCA dauerhaft?" envalue="Is the blue LED solidly on? LED will switch off after 10s.">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
       <LOADPP fileheader="Pause" name="WAIT_TIME_WCA" dename="WARTEZEIT_WCA" enname="WAIT_TIME_WCA">
        <COMMENT></COMMENT>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="200">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
   </GROUP>
   <GROUP name="------------------------------------------------">
    <COMMENT></COMMENT>
   </GROUP>
   <GROUP name="WERKER IDENT / OPERATOR IDENT">
    <COMMENT></COMMENT>
    <LOADPP fileheader="WerkerIdent" name="ASSOCIATE_NUMBER" dename="PRUEFERNUMMER_EINGEBEN" enname="ASSOCIATE_NUMBER">
     <COMMENT></COMMENT>
    </LOADPP>
   </GROUP>
   <GROUP name="------------------------------------------------">
    <COMMENT></COMMENT>
   </GROUP>
   <GROUP name="WERKER QUITTUNG / OPERATOR CONFIRMATION">
    <COMMENT></COMMENT>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="BEDIENEINHEIT LICHT / LIGHT SWITCH">
     <COMMENT></COMMENT>
     <LOADPP fileheader="WerkerQuittung" name="WC_SWITCH_OFF_LIGHT" dename="WQ_LICHT_AUS" enname="WC_SWITCH_OFF_LIGHT">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Ensure the light switch is in the 12 o&#39;clock position." devalue="Bitte Bedieneinheit Licht in Stellung 0 (AUS) bringen!" envalue="Ensure the light switch is in the 12 o&#39;clock position.">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="3">
       </CONSTANT>
      </PPARGS>
     </LOADPP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="COACH-DOOR MODUL / COACH-DOOR MODULE">
     <COMMENT></COMMENT>
     <LOADPP fileheader="WerkerQuittung" name="WQ_TUEREN_BEIFAHRERSEITE_KONTROLLIEREN" dename="WQ_TUEREN_BEIFAHRERSEITE_KONTROLLIEREN" enname="WQ_CONTROL_OPENING_DOORS_PASSENGER_SIDE">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Doors passenger side are not fully opened! Please check that the doors passenger side are opened!" devalue="T�ren Beifahrerseite nicht komplett ge�ffnet! Bitte �ffnung der T�ren auf der Beifahrerseite kontrollieren!" envalue="Doors passenger side are not fully opened! Please check that the doors passenger side are opened!">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="3">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerQuittung" name="WQ_TUEREN_FAHRERSEITE_KONTROLLIEREN" dename="WQ_TUEREN_FAHRERSEITE_KONTROLLIEREN" enname="WQ_CONTROL_OPENING_DOORS_DRIVER_SIDE">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Doors driver side are not fully opened! Please check that the doors driver side are opened!" devalue="T�ren Fahrerseite nicht komplett ge�ffnet! Bitte �ffnung der T�ren auf der Fahrerseite kontrollieren!" envalue="Doors driver side are not fully opened! Please check that the doors driver side are opened!">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="3">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="EINSTEIGEN - AUSSTEIGEN / GET IN CAR -LEAVE CAR">
     <COMMENT></COMMENT>
     <IF>
      <COMMENT>RR4 - Ghost</COMMENT>
      <BOOL_METHOD>
       <COMMENT>Ghost</COMMENT>
       <METHOD name="getBaureihe" returntype="String">
        <METHOD name="equals" returntype="boolean">
         <PARAMS>
          <CONSTANT type="String" value="RR4">
          </CONSTANT>
         </PARAMS>
        </METHOD>
       </METHOD>
            <OPERATOR type="eq" optype="boolean"> </OPERATOR>
      <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      <THEN>
       <LOADPP fileheader="WerkerAnweisung" name="WA_LEAVE_CAR_REAR" dename="WA_FZG_VERLASSEN_HINTEN" enname="WA_LEAVE_CAR_REAR">
        <COMMENT></COMMENT>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="3000">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="AWT" required="true">
         <CONSTANT type="String" value="Fahrzeug;verlassen" devalue="Fahrzeug;verlassen" envalue="please leave;the car">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
     <IF>
      <COMMENT>Driver side rear</COMMENT>
      <AND>
<NOT>
                <BOOL_METHOD>
         <COMMENT></COMMENT>
        <METHOD name="getAnzahlT�ren" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="equals" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="2">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
             <OR>
        <COMMENT>LUFTAUSTROEMER B SAEULE FOND / AIR VENT REAR B COLUMN</COMMENT>
        <OR>
         <COMMENT>35up LG</COMMENT>
         <BOOL_METHOD>
          <COMMENT>7 series sedan</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G11">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>7 series sedan extended wheel</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G12">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <AND>
         <COMMENT>G3X</COMMENT>
         <OR>
          <COMMENT>Derivatives</COMMENT>
          <BOOL_METHOD>
           <COMMENT>M5 / M6 / X3M / X4M</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="startsWith" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="F9">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>5 series sedan</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G30">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>5 series wagon</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G31">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>5 series GT</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G32">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>5 series sedan extended wheel (China)</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G38">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <OR>
          <COMMENT>Options</COMMENT>
          <BOOL_METHOD>
           <COMMENT>4 Zonen Klimaautomatik</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="04NB">
              <COMMENT>4 Zonen Klimaautomatik</COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
        </AND>
        <OR>
         <COMMENT>Einstiegsleisten hinten (G30 PHEV nicht)</COMMENT>
         <OR>
          <COMMENT>G11 G12 Varianten</COMMENT>
          <AND>
           <COMMENT>G11 / G12 / SAs</COMMENT>
           <OR>
            <COMMENT>G11 / G12</COMMENT>
            <BOOL_METHOD>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G11">
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G12">
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           </OR>
           <OR>
            <COMMENT>SA4UR SA715 SA778 SA3E1</COMMENT>
            <BOOL_METHOD>
             <COMMENT>AmbientenLicht</COMMENT>
             <METHOD name="containsSA" returntype="boolean">
              <PARAMS>
               <CONSTANT type="String" value="04UR">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <COMMENT>Pure Excellence</COMMENT>
             <METHOD name="containsSA" returntype="boolean">
              <PARAMS>
               <CONSTANT type="String" value="03E1">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <COMMENT>EL BMW M Packet</COMMENT>
             <METHOD name="containsSA" returntype="boolean">
              <COMMENT>M Aerodynamikpaket</COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="0715">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <COMMENT>EL BMW Individual</COMMENT>
             <METHOD name="containsSA" returntype="boolean">
              <PARAMS>
               <CONSTANT type="String" value="0778">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           </OR>
          </AND>
          <AND>
           <COMMENT>G11 / G12 PHEV</COMMENT>
           <OR>
            <BOOL_METHOD>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G11">
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G12">
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           </OR>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="getHybrid" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="PHEV">
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </AND>
          <AND>
           <COMMENT>G12 mit N74 12 Zylinder</COMMENT>
           <BOOL_METHOD>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G12">
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <METHOD name="getMotorBaureihe" returntype="String">
             <METHOD name="startsWith" returntype="boolean">
              <PARAMS>
               <CONSTANT type="String" value="N74">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </AND>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G16">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <OR>
          <COMMENT>G30 G31 Varianten</COMMENT>
          <AND>
           <COMMENT>G30 / G31 / SAs</COMMENT>
           <OR>
            <COMMENT>G30 / G31</COMMENT>
            <BOOL_METHOD>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G30">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G31">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           </OR>
           <OR>
            <COMMENT>SA7S2 SA7AC SA715 SA778</COMMENT>
            <BOOL_METHOD>
             <COMMENT>Luxury line</COMMENT>
             <METHOD name="containsSA" returntype="boolean">
              <PARAMS>
               <CONSTANT type="String" value="07S2">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <COMMENT>Sport line</COMMENT>
             <METHOD name="containsSA" returntype="boolean">
              <PARAMS>
               <CONSTANT type="String" value="07AC">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <COMMENT>EL BMW M Packet</COMMENT>
             <METHOD name="containsSA" returntype="boolean">
              <COMMENT>M Aerodynamikpaket</COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="0715">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <COMMENT>EL BMW Individual</COMMENT>
             <METHOD name="containsSA" returntype="boolean">
              <PARAMS>
               <CONSTANT type="String" value="0778">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           </OR>
          </AND>
          <BOOL_METHOD>
           <COMMENT>G30 M5 Serie</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="F90">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <AND>
           <COMMENT>G30 / G31 Motorvarianten M 550d und M 550i</COMMENT>
           <OR>
            <COMMENT>G30 / G31</COMMENT>
            <BOOL_METHOD>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G30">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G31">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           </OR>
           <OR>
            <COMMENT>Motorvarianten M 550d M 550i</COMMENT>
            <BOOL_METHOD>
             <COMMENT>M 550i</COMMENT>
             <METHOD name="getMotorBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equals" returntype="boolean">
               <PARAMS>
                <CONSTANT type="String" value="B57S">
                 <COMMENT>M 550d</COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <COMMENT>M 550d</COMMENT>
             <METHOD name="getMotorBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equals" returntype="boolean">
               <PARAMS>
                <CONSTANT type="String" value="N63R">
                 <COMMENT>M 550i</COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           </OR>
          </AND>
         </OR>
         <AND>
          <COMMENT>G05/G06/G07 mit SA04UR</COMMENT>
          <OR>
           <COMMENT>G05/G06/G07</COMMENT>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G05">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G06">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G07">
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="04UR">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </AND>
        </OR>
       </OR>
      </AND>
      <THEN>
       <LOADPP fileheader="WerkerQuittung" name="WQ_FZG_VERLASSEN_FAHRERSEITE_HINTEN" dename="WQ_FZG_VERLASSEN_FAHRERSEITE_HINTEN" enname="WQ_LEAVE_CAR_DRIVER_SIDE_REAR">
        <COMMENT>S15A</COMMENT>
        <PPARGS name="AWT" required="true">
         <CONSTANT type="String" value="Fahrzeug;verlassen" devalue="Fahrzeug;verlassen" envalue="please leave;the car">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="STYLE" required="true">
         <CONSTANT type="String" value="3">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
     <LOADPP fileheader="WerkerQuittung" name="WQ_FZG_VERLASSEN" dename="WQ_FZG_VERLASSEN" enname="WQ_LEAVE_CAR">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Fahrzeug;verlassen" devalue="Fahrzeug;verlassen" envalue="please leave;the car">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="3">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerAnweisung" name="WA_FZG_VERLASSEN" dename="WA_FZG_VERLASSEN" enname="WA_LEAVE_CAR">
      <COMMENT></COMMENT>
      <PPARGS name="DAUER" required="true">
       <CONSTANT type="String" value="3000">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="please leave the car" devalue="Fahrzeug verlassen" envalue="please leave the car">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerQuittung" name="WQ_GET_IN_PS_REAR" dename="WQ_EINSTEIGEN_BF_FOND" enname="WQ_GET_IN_PS_REAR">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Get in passenger side rear!" devalue="Einsteigen BF- Seite Fond" envalue="Get in passenger side rear!">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="3">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <IF>
      <COMMENT>WQ_FZG_VERLASSEN_HINTEN_BEIFAHRER</COMMENT>
      <OR>
       <COMMENT></COMMENT>
       <OR_AND_GROUP name="12V STECKDOSEN / 12V POWER SOCKET">
        <COMMENT></COMMENT>
        <OR>
         <COMMENT>WF_STECKDOSE_MITTELKONSOLE_HINTEN_RECHTS / WF_POWER_SOCKET_CENTER_CONSOLE_REAR_RIGHT_SIDE</COMMENT>
         <AND>
          <COMMENT>35up Serie (G07) &amp;&amp; !SA441 &amp;&amp;!SA4F5</COMMENT>
          <OR>
           <COMMENT>G07</COMMENT>
           <BOOL_METHOD>
            <COMMENT>X7</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G07">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
<NOT>
                      <BOOL_METHOD>
            <COMMENT>NICHT RAUCHERPAKET</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="0441">
              <COMMENT>Ablagenpaket</COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                  </AND>
         <OR>
          <COMMENT>Rolls Royce (RR5, RR6, RR11, RR12)</COMMENT>
          <BOOL_METHOD>
           <COMMENT>Wraith</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="RR5">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>Wraith Cabrio</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="RR6">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="RR11">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="RR12">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
        </OR>
        <OR>
         <COMMENT>WF_STECKDOSE_MITTELKONSOLE_HINTEN_LINKS / WF_POWER_SOCKET_CENTER_CONSOLE_REAR_LEFT_SIDE</COMMENT>
         <AND>
          <COMMENT>G07 und !SA0441</COMMENT>
          <BOOL_METHOD>
           <COMMENT>X7</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G07">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
<NOT>
                      <BOOL_METHOD>
            <COMMENT>NICHT RAUCHERPAKET</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="0441">
              <COMMENT>Ablagenpaket</COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                  </AND>
         <OR>
          <COMMENT>Rolls Royce</COMMENT>
          <AND>
           <COMMENT>(RR5, RR6) &amp; !SA441</COMMENT>
           <OR>
            <COMMENT>RR5, RR6</COMMENT>
            <BOOL_METHOD>
             <COMMENT>Wraith</COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="RR5">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <COMMENT>7Wraith Cabrio</COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="RR6">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           </OR>
<NOT>
                        <BOOL_METHOD>
             <COMMENT>NICHT RAUCHERPAKET</COMMENT>
            <METHOD name="containsSA" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="0441">
               <COMMENT>Ablagenpaket</COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                    </AND>
          <OR>
           <COMMENT>Serie (RR11, RR12)</COMMENT>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="RR11">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="RR12">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
         </OR>
        </OR>
        <OR>
         <COMMENT>WF_STECKDOSE_MITTELKONSOLE_HINTEN_MITTE / WF_POWER_SOCKET_CENTER_CONSOLE_REAR_MIDDLE</COMMENT>
         <AND>
          <COMMENT>35up Serie (G05, G06) &amp;&amp; !SA441</COMMENT>
          <OR>
           <COMMENT>G05, G06</COMMENT>
           <BOOL_METHOD>
            <COMMENT>X5</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G05">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>X6 Coup�</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G06">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
<NOT>
                      <BOOL_METHOD>
            <COMMENT>NICHT RAUCHERPAKET</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="0441">
              <COMMENT>Ablagenpaket</COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                  </AND>
         <AND>
          <COMMENT>35up G2x mit SA0493</COMMENT>
          <OR>
           <COMMENT>G2x</COMMENT>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="startsWith" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G2">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
          <BOOL_METHOD>
           <COMMENT>Ablagepaket</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="0493">
              <COMMENT>Zus�tzliche 12V-Steckdosen</COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </AND>
         <OR>
          <COMMENT>RRNM</COMMENT>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="RR11">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="RR12">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
        </OR>
        <OR>
         <COMMENT>WF_STECKDOSE_MITTELONSOLE_HINTEN_CUP_HOLDER /  WF_POWER_SOCKET_CENTER_CONSOLE_REAR_CUP_HOLDER</COMMENT>
         <OR>
          <COMMENT>Rolls Royce (RR6)</COMMENT>
          <BOOL_METHOD>
           <COMMENT>Wraith Cabrio</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="RR6">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
        </OR>
        <OR>
         <COMMENT>WF_STECKDOSE_MITTELARMLEHNE_HINTEN / WF_POWER_SOCKET_CENTER_ARM_REST_REAR</COMMENT>
         <OR>
          <COMMENT>Rolls Royce</COMMENT>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equals" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="RR11">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equals" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="RR12">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
        </OR>
       </OR_AND_GROUP>
       <OR_AND_GROUP name="ZIGARETTENANZUENDER / CIGARETTE LIGHTER">
        <COMMENT></COMMENT>
        <OR>
         <AND>
          <COMMENT>ZIGARETTENANZ�NDER_MITTELKONSOLE_HINTEN_RECHTS / CIGARETTE_LIGHTER_CENTER_CONSOLE_REAR_RIGHT_SIDE</COMMENT>
          <OR>
           <COMMENT>35up</COMMENT>
           <BOOL_METHOD>
            <COMMENT>X7</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G07">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
          <BOOL_METHOD>
           <COMMENT>RAUCHERPAKET</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="0441">
              <COMMENT>Ablagenpaket</COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </AND>
         <OR>
          <COMMENT>ZIGARETTENANZUENDER_MITTELKONSOLE_HINTEN_LINKS / STEUERN_EFII_CIGARETTE_LIGHTER_CENTER_CONSOLE_REAR_LEFT_SIDE</COMMENT>
          <OR>
           <COMMENT>Rolls Royce</COMMENT>
           <AND>
            <COMMENT>RR6 &amp; SA441</COMMENT>
            <OR>
             <BOOL_METHOD>
              <COMMENT>Wraith</COMMENT>
              <METHOD name="getBaureihe" returntype="String">
               <METHOD name="equalsIgnoreCase" returntype="boolean">
                <COMMENT></COMMENT>
                <PARAMS>
                 <CONSTANT type="String" value="RR5">
                  <COMMENT></COMMENT>
                 </CONSTANT>
                </PARAMS>
               </METHOD>
              </METHOD>
                          <OPERATOR type="eq" optype="boolean"> </OPERATOR>
             <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
             <BOOL_METHOD>
              <COMMENT>7Wraith Cabrio</COMMENT>
              <METHOD name="getBaureihe" returntype="String">
               <METHOD name="equalsIgnoreCase" returntype="boolean">
                <COMMENT></COMMENT>
                <PARAMS>
                 <CONSTANT type="String" value="RR6">
                  <COMMENT></COMMENT>
                 </CONSTANT>
                </PARAMS>
               </METHOD>
              </METHOD>
                          <OPERATOR type="eq" optype="boolean"> </OPERATOR>
             <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            </OR>
            <BOOL_METHOD>
             <COMMENT>NICHT RAUCHERPAKET</COMMENT>
             <METHOD name="containsSA" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="0441">
                <COMMENT>Ablagenpaket</COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           </AND>
          </OR>
         </OR>
         <OR>
          <COMMENT>ZIGARETTENANZ�NDER_MITTELKONSOLE_HINTEN_MITTE / CIGARETTE_LIGHTER_CENTER_CONSOLE_REAR_MIDDLE</COMMENT>
          <AND>
           <COMMENT>35up Serie &amp;&amp; SA441</COMMENT>
           <OR>
            <COMMENT>Serie</COMMENT>
            <BOOL_METHOD>
             <COMMENT>X5</COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G05">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <COMMENT>X6 Coup�</COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G06">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           </OR>
           <BOOL_METHOD>
            <COMMENT>RAUCHERPAKET</COMMENT>
            <METHOD name="containsSA" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="0441">
               <COMMENT>Ablagenpaket</COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </AND>
          <AND>
           <COMMENT>G20, G21 mit SA0441 und SA0493</COMMENT>
           <OR>
            <COMMENT>G20, G21</COMMENT>
            <BOOL_METHOD>
             <COMMENT>3 series sedan</COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G20">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <COMMENT>3 series touring</COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G21">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           </OR>
           <BOOL_METHOD>
            <COMMENT>RAUCHERPAKET</COMMENT>
            <METHOD name="containsSA" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="0441">
               <COMMENT>Ablagenpaket</COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT>Ablagepaket</COMMENT>
            <METHOD name="containsSA" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="0493">
               <COMMENT>Zus�tzliche 12V-Steckdosen</COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </AND>
          <OR>
           <COMMENT>35up Typinhalt (G28)</COMMENT>
           <BOOL_METHOD>
            <COMMENT>3 series lim. China</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G28">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
         </OR>
         <AND>
          <COMMENT>ZIGARETTENANZUENDER_EXECUTIVE_LOUNG_MITTELKONSOLE_HINTEN / CIGARETTE_LIGHTER_EXECUTIVE_LOUNG_CONSOLE_REAR</COMMENT>
          <BOOL_METHOD>
           <COMMENT>executive loung hinten console</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="04F5">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>Raucherpacket</COMMENT>
           <METHOD name="containsSA" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="0441">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </AND>
        </OR>
       </OR_AND_GROUP>
       <OR_AND_GROUP name="EINSTIEGSLEUCHTEN / ENTRANCE LIGHT">
        <COMMENT></COMMENT>
        <OR>
         <COMMENT>G07/G16/G28 Serie oder G05/G06 mit SA4UR</COMMENT>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G07">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G16">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G28">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <AND>
          <COMMENT>G05/G06 mit SA4UR</COMMENT>
          <OR>
           <COMMENT>G05/G06</COMMENT>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G05">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G06">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
          <OR>
           <COMMENT>NAEL N44246/E44246</COMMENT>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="containsSA" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="04UR">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
         </AND>
         <OR>
          <COMMENT>RR</COMMENT>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equals" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="RR11">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equals" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="RR12">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equals" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="RR31">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
        </OR>
       </OR_AND_GROUP>
       <OR_AND_GROUP name="FUSSRAUMLEUCHTEN - FOOTWELL LIGHT">
        <COMMENT></COMMENT>
        <OR>
         <COMMENT>G2x</COMMENT>
         <AND>
          <COMMENT>G2x</COMMENT>
          <AND>
           <COMMENT>G2x</COMMENT>
<NOT>
                        <BOOL_METHOD>
             <COMMENT></COMMENT>
            <METHOD name="containsSA" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="04UR">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                     <OR>
            <BOOL_METHOD>
             <COMMENT></COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G20">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <COMMENT></COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G21">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <COMMENT></COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G24">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <COMMENT></COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G28">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <BOOL_METHOD>
             <COMMENT></COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="J29">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           </OR>
          </AND>
         </AND>
         <OR>
          <COMMENT>RR</COMMENT>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equals" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="RR11">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equals" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="RR12">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equals" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="RR31">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
        </OR>
       </OR_AND_GROUP>
       <OR_AND_GROUP name="LUFTAUSTROEMER B SAEULE FOND / AIR VENT REAR B COLUMN">
        <COMMENT></COMMENT>
        <AND>
<NOT>
                    <BOOL_METHOD>
           <COMMENT>!2 T�ren</COMMENT>
          <METHOD name="getAnzahlT�ren" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="2">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                 <OR>
          <COMMENT>F93 und SA04NB</COMMENT>
          <OR>
           <COMMENT>F93</COMMENT>
           <BOOL_METHOD>
            <COMMENT>M8 4 Tuer</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="F93">
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
          <OR>
           <COMMENT>SA04NB</COMMENT>
           <BOOL_METHOD>
            <COMMENT>4 Zonen Klimaautomatik</COMMENT>
            <METHOD name="containsSA" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="04NB">
               <COMMENT>4 Zonen Klimaautomatik</COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
         </OR>
        </AND>
       </OR_AND_GROUP>
       <OR_AND_GROUP name="BELEUCHTUNG MITTELKONSOLE HINTEN">
        <COMMENT></COMMENT>
       </OR_AND_GROUP>
       <OR_AND_GROUP name="USB HINTEN">
        <COMMENT></COMMENT>
        <OR>
         <OR_AND_GROUP name="SINGLE CHARGER">
          <COMMENT></COMMENT>
          <AND>
           <COMMENT>SA04FL (Travel &amp; Comfort Rail System) und !SA06FH (Fond-Entertainment Professional)</COMMENT>
           <BOOL_METHOD>
            <COMMENT>Travel &amp; Comfort</COMMENT>
            <METHOD name="containsSA" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="04FL">
              </CONSTANT>
             </PARAMS>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
<NOT>
                        <BOOL_METHOD>
             <COMMENT>6FH_FOND_ENTERTAIMENT_PROF</COMMENT>
            <METHOD name="containsSA" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="06FH">
              </CONSTANT>
             </PARAMS>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                    </AND>
         </OR_AND_GROUP>
         <OR_AND_GROUP name="DUAL CHARGER">
          <COMMENT></COMMENT>
          <OR>
           <OR>
            <COMMENT>G28</COMMENT>
            <BOOL_METHOD>
             <COMMENT>3 Series Sedan (China)</COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G28">
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           </OR>
           <AND>
            <COMMENT>!G2x und !F4x und !G05 und !G06 und !G07 und !G14 und !G15</COMMENT>
<NOT>
                          <BOOL_METHOD>
              <COMMENT></COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="startsWith" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G2">
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
           <NOT>
                          <BOOL_METHOD>
              <COMMENT></COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="startsWith" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="F4">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
           <NOT>
                          <BOOL_METHOD>
              <COMMENT>gAMS AL5723 - X5</COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G05">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
           <NOT>
                          <BOOL_METHOD>
              <COMMENT>gAMS AL5723 - X6</COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G06">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
           <NOT>
                          <BOOL_METHOD>
              <COMMENT>gAMS AL5723 - X7</COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G07">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
           <NOT>
                          <BOOL_METHOD>
              <COMMENT>6er Cabrio</COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G14">
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
           <NOT>
                          <BOOL_METHOD>
              <COMMENT>6er Coupe</COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G15">
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                      </AND>
           <AND>
            <COMMENT>(G2x, F4x) und SA0493</COMMENT>
            <OR>
             <COMMENT>G2x, F4x</COMMENT>
             <BOOL_METHOD>
              <COMMENT></COMMENT>
              <METHOD name="getBaureihe" returntype="String">
               <COMMENT></COMMENT>
               <METHOD name="startsWith" returntype="boolean">
                <COMMENT></COMMENT>
                <PARAMS>
                 <CONSTANT type="String" value="G2">
                 </CONSTANT>
                </PARAMS>
               </METHOD>
              </METHOD>
                          <OPERATOR type="eq" optype="boolean"> </OPERATOR>
             <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
             <BOOL_METHOD>
              <COMMENT></COMMENT>
              <METHOD name="getBaureihe" returntype="String">
               <COMMENT></COMMENT>
               <METHOD name="startsWith" returntype="boolean">
                <COMMENT></COMMENT>
                <PARAMS>
                 <CONSTANT type="String" value="F4">
                  <COMMENT></COMMENT>
                 </CONSTANT>
                </PARAMS>
               </METHOD>
              </METHOD>
                          <OPERATOR type="eq" optype="boolean"> </OPERATOR>
             <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            </OR>
            <BOOL_METHOD>
             <COMMENT>Ablagepaket</COMMENT>
             <METHOD name="containsSA" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="0493">
               </CONSTANT>
              </PARAMS>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           </AND>
           <AND>
            <COMMENT>bs, v85: G07 series from 18-11-400, gAMS AL5723</COMMENT>
            <BOOL_METHOD>
             <COMMENT>gAMS AL5723 - X7</COMMENT>
             <METHOD name="getBaureihe" returntype="String">
              <COMMENT></COMMENT>
              <METHOD name="equalsIgnoreCase" returntype="boolean">
               <COMMENT></COMMENT>
               <PARAMS>
                <CONSTANT type="String" value="G07">
                 <COMMENT></COMMENT>
                </CONSTANT>
               </PARAMS>
              </METHOD>
             </METHOD>
                        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
            <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
            <OR>
             <COMMENT>from 18-11-400</COMMENT>
             <AND>
              <COMMENT>&gt;= 18-11-400</COMMENT>
              <BOOL_METHOD>
               <METHOD name="getIStufeJahrInt" returntype="int">
                <COMMENT></COMMENT>
               </METHOD>
               <OPERATOR type="eq" optype="int">
                <COMMENT></COMMENT>
               </OPERATOR>
               <CONSTANT type="int" value="18">
                <COMMENT></COMMENT>
               </CONSTANT>
              </BOOL_METHOD>
              <BOOL_METHOD>
               <METHOD name="getIStufeMonatInt" returntype="int">
                <COMMENT></COMMENT>
               </METHOD>
               <OPERATOR type="eq" optype="int">
                <COMMENT></COMMENT>
               </OPERATOR>
               <CONSTANT type="int" value="11">
                <COMMENT></COMMENT>
               </CONSTANT>
              </BOOL_METHOD>
              <BOOL_METHOD>
               <METHOD name="getIStufeNummerInt" returntype="int">
               </METHOD>
               <OPERATOR type="g_equal" optype="int">
               </OPERATOR>
               <CONSTANT type="int" value="400">
               </CONSTANT>
              </BOOL_METHOD>
             </AND>
             <AND>
              <COMMENT>&gt;= 19-03-400</COMMENT>
              <BOOL_METHOD>
               <METHOD name="getIStufeJahrInt" returntype="int">
                <COMMENT></COMMENT>
               </METHOD>
               <OPERATOR type="eq" optype="int">
                <COMMENT></COMMENT>
               </OPERATOR>
               <CONSTANT type="int" value="19">
                <COMMENT></COMMENT>
               </CONSTANT>
              </BOOL_METHOD>
              <BOOL_METHOD>
               <METHOD name="getIStufeMonatInt" returntype="int">
                <COMMENT></COMMENT>
               </METHOD>
               <OPERATOR type="eq" optype="int">
                <COMMENT></COMMENT>
               </OPERATOR>
               <CONSTANT type="int" value="3">
                <COMMENT></COMMENT>
               </CONSTANT>
              </BOOL_METHOD>
              <BOOL_METHOD>
               <METHOD name="getIStufeNummerInt" returntype="int">
               </METHOD>
               <OPERATOR type="g_equal" optype="int">
               </OPERATOR>
               <CONSTANT type="int" value="400">
               </CONSTANT>
              </BOOL_METHOD>
             </AND>
             <AND>
              <COMMENT>&gt; 19-03</COMMENT>
              <BOOL_METHOD>
               <METHOD name="getIStufeJahrInt" returntype="int">
                <COMMENT></COMMENT>
               </METHOD>
               <OPERATOR type="eq" optype="int">
                <COMMENT></COMMENT>
               </OPERATOR>
               <CONSTANT type="int" value="19">
                <COMMENT></COMMENT>
               </CONSTANT>
              </BOOL_METHOD>
              <BOOL_METHOD>
               <METHOD name="getIStufeMonatInt" returntype="int">
                <COMMENT></COMMENT>
               </METHOD>
               <OPERATOR type="greater" optype="int">
                <COMMENT></COMMENT>
               </OPERATOR>
               <CONSTANT type="int" value="3">
                <COMMENT></COMMENT>
               </CONSTANT>
              </BOOL_METHOD>
             </AND>
             <AND>
              <COMMENT>&gt; 19</COMMENT>
              <BOOL_METHOD>
               <METHOD name="getIStufeJahrInt" returntype="int">
                <COMMENT></COMMENT>
               </METHOD>
               <OPERATOR type="greater" optype="int">
                <COMMENT></COMMENT>
               </OPERATOR>
               <CONSTANT type="int" value="19">
                <COMMENT></COMMENT>
               </CONSTANT>
              </BOOL_METHOD>
             </AND>
            </OR>
           </AND>
          </OR>
         </OR_AND_GROUP>
        </OR>
       </OR_AND_GROUP>
      </OR>
      <THEN>
       <LOADPP fileheader="WerkerAnweisung" name="WA_FZG_VERLASSEN_HINTEN_BEIFAHRER" dename="WA_FZG_VERLASSEN_HINTEN_BEIFAHRER" enname="WA_LEAVE_CAR_BACK_PASSENGER">
        <COMMENT>SP2018: Existenz BDC_MASTER.INT_OPEN_DOOR_PASSENGER_SIDE_REAR_FOR_INTERIOR_TESTING</COMMENT>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="3000">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="AWT" required="true">
         <CONSTANT type="String" value="please leave the car" devalue="Fahrzeug verlassen" envalue="please leave the car">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
     <IF>
      <COMMENT>WQ_FZG_VERLASSEN_HINTEN_FAHRER</COMMENT>
      <OR>
       <COMMENT></COMMENT>
       <OR_AND_GROUP name="EINSTIEGSLEUCHTEN HINTEN / ENTRANCE LIGHT REAR">
        <COMMENT></COMMENT>
        <OR>
         <COMMENT>G05, G06, G07, G16, G28</COMMENT>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G07">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G16">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G28">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <AND>
          <COMMENT>(G05, G06) und SA04UR</COMMENT>
          <OR>
           <COMMENT>G05, G06</COMMENT>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G05">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="G06">
                <COMMENT></COMMENT>
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
          <OR>
           <COMMENT>SA04UR</COMMENT>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="containsSA" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="04UR">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
         </AND>
        </OR>
       </OR_AND_GROUP>
       <OR_AND_GROUP name="LUFTAUSTROEMER B SAEULE FOND / AIR VENT REAR B COLUMN">
        <COMMENT></COMMENT>
        <AND>
<NOT>
                    <BOOL_METHOD>
           <COMMENT>!2 T�ren</COMMENT>
          <METHOD name="getAnzahlT�ren" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="2">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                 <OR>
          <COMMENT>F93 und SA04NB</COMMENT>
          <OR>
           <COMMENT>F93</COMMENT>
           <BOOL_METHOD>
            <COMMENT>M8 4 Tuer</COMMENT>
            <METHOD name="getBaureihe" returntype="String">
             <COMMENT></COMMENT>
             <METHOD name="equalsIgnoreCase" returntype="boolean">
              <COMMENT></COMMENT>
              <PARAMS>
               <CONSTANT type="String" value="F93">
               </CONSTANT>
              </PARAMS>
             </METHOD>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
          <OR>
           <COMMENT>SA04NB</COMMENT>
           <BOOL_METHOD>
            <COMMENT>4 Zonen Klimaautomatik</COMMENT>
            <METHOD name="containsSA" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="04NB">
               <COMMENT>4 Zonen Klimaautomatik</COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
                      <OPERATOR type="eq" optype="boolean"> </OPERATOR>
           <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          </OR>
         </OR>
        </AND>
       </OR_AND_GROUP>
      </OR>
      <THEN>
       <LOADPP fileheader="WerkerAnweisung" name="WA_FZG_VERLASSEN_HINTEN_FAHRER" dename="WA_FZG_VERLASSEN_HINTEN_FAHRER" enname="WA_LEAVE_CAR_BACK_DRIVER">
        <COMMENT>SP2018: Existenz BDC_MASTER.INT_OPEN_DOOR_DRIVER_SIDE_REAR_FOR_INTERIOR_TESTING</COMMENT>
        <PPARGS name="DAUER" required="true">
         <CONSTANT type="String" value="3000">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="AWT" required="true">
         <CONSTANT type="String" value="please leave the car" devalue="Fahrzeug verlassen" envalue="please leave the car">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="FAHRZEUG UND ICOM WECKEN / WAKE UP VEHICLE AND ICOM">
     <COMMENT></COMMENT>
     <LOADPP fileheader="WerkerQuittung" name="WQ_FAHRZEUG_UND_ICOM_WECKEN">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <COMMENT></COMMENT>
       <CONSTANT type="String" value="T�r �ffnen, Z�ndung einschalten." devalue="T�r �ffnen, Z�ndung einschalten." envalue="Open door and activate ignition.">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="1">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerQuittung" name="WQ_ZUENDUNG_EIN" dename="WQ_ZUENDUNG_EIN" enname="WQ_IGNITION_ON">
      <COMMENT>WQ: Z�ndung einschalten und ICOMP iO

WQ: Switch on ignition and ICOMP okay</COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Z�ndung;einschalten und ICOMP IO?" devalue="Z�ndung;einschalten und ICOMP IO?" envalue="Igniton on and check ICOMP OKAY?">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="3">
       </CONSTANT>
      </PPARGS>
     </LOADPP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="KLIMA AUSSCHALTEN / SWITCH OFF AIR CONDITIONER">
     <COMMENT></COMMENT>
     <LOADPP fileheader="WerkerQuittung" name="WQ_ECOS_KLIMA_AUS" dename="WQ_ECOS_KLIMA_AUS" enname="WQ_ECOS_AC_OF">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Klima L�ftung kompl.;ausschalten!" devalue="Klima L�ftung kompl.;ausschalten!" envalue="Switch off AC air vent compl!">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="3">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="LADEGER�T / BATTERIE CHARGER">
     <COMMENT></COMMENT>
     <LOADPP fileheader="WerkerQuittung" name="WQ_LADEGERAET_ANSCHLIESSEN" dename="WQ_LADEGERAET_ANSCHLIESSEN" enname="WQ_CONNECT_BATTERY_CHARGER">
      <COMMENT>Hinweis, dass Batterieladeger�t angeschlossen werden muss
Warning, connect battery cahrger</COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Achtung: Ladeger�t;anschliessen!" devalue="Achtung: Ladeger�t;anschliessen!" envalue="ATTENTION: Connect;Battery Charger!">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="3">
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerQuittung" name="WQ_LADEGERAET_ENTFERNEN" dename="WQ_LADEGERAET_ENTFERNEN" enname="WQ_DISCONNECT_BATTERY_CHARGER">
      <COMMENT>Warnung, dass Batterieladeger�t noch angeschlossen
Warning, disconnect battery cahrger</COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Achtung: Ladeger�t;entfernen!" devalue="Achtung: Ladeger�t;entfernen!" envalue="Disconnect;Battery Charger!">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="3">
       </CONSTANT>
      </PPARGS>
     </LOADPP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="MECH TUERSCHLOSSPRUEFUNG / MECH KEY LOCK CHECK">
     <COMMENT></COMMENT>
     <LOADPP fileheader="WerkerQuittung" name="WQ_KEINE_PRUEFUNG_SCHLOSS" dename="WQ_KEINE_PRUEFUNG_SCHLOSS" enname="WQ_NO_CHECK_KEY">
      <COMMENT>Quittung keine Pr�fung mech. Schloss

Question to check if igniton on</COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Keine Pr�fung mech.;Schloss" devalue="Keine Pr�fung mech.;Schloss" envalue="No check of mech.;keys">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="3">
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerAnweisung" name="WA_TAKE_MECH_KEY_AND_REMOTE" dename="WA_MECHAN_SCHLUESSEL_UND_FFB_ENTNEHMEN" enname="WA_TAKE_MECH_KEY_AND_REMOTE">
      <COMMENT></COMMENT>
      <PPARGS name="DAUER" required="true">
       <CONSTANT type="String" value="3000">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Take mechanical key" devalue="Mech. Schl�ssel nehmen" envalue="Take mechanical key">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerAnweisung" name="WA_LEAVE_MECH_KEY_AND_REMOTE" dename="WA_MECHAN_SCHLUESSEL_UND_FFB_LASSEN" enname="WA_LEAVE_MECH_KEY_AND_REMOTE">
      <COMMENT></COMMENT>
      <PPARGS name="DAUER" required="true">
       <CONSTANT type="String" value="3000">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Leave the key in the car" devalue="Schl�ssel im Fzg lassen" envalue="Leave the key in the car">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerQuittung" name="WC_TAKE_MECH_KEY_AND_REMOTE" dename="WQ_MECHAN_SCHLUESSEL_UND_FFB_ENTNEHMEN" enname="WC_TAKE_MECH_KEY_AND_REMOTE">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <COMMENT></COMMENT>
       <CONSTANT type="String" value="Take mechanical key" devalue="mech. Schl�ssel und" envalue="Take mechanical key">
        <COMMENT></COMMENT>
       </CONSTANT>
       <CONSTANT type="String" value="and remote" devalue="FFB entnehmen" envalue="and remote">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="1">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerQuittung" name="WC_LEAVE_MECH_KEY_AND_REMOTE" dename="WQ_MECHAN_SCHLUESSEL_UND_FFB_LASSEN" enname="WC_LEAVE_MECH_KEY_AND_REMOTE">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <COMMENT></COMMENT>
       <CONSTANT type="String" value="Schl�ssel im Fzg lassen" devalue="Schl�ssel im Fzg lassen" envalue="Leave the key in the car">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="1">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerQuittung" name="WC_PLACE_MECH_KEY_AND_REMOTE" dename="WQ_MECHAN_SCHLUESSEL_UND_FFB_ABLEGEN" enname="WC_PLACE_MECH_KEY_AND_REMOTE">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Place mech. key and" devalue="mech. Schl�ssel und" envalue="Place mech. key and">
        <COMMENT></COMMENT>
       </CONSTANT>
       <CONSTANT type="String" value="Remote Keys in car" devalue="FFB in Fzg. ablegen" envalue="Remote Keys in car">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="1">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerQuittung" name="WA_REMOVE_KEY_LOCK_COVER" dename="WA_ABDECK_ENTFERNEN" enname="WA_REMOVE_KEY_LOCK_COVER">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <COMMENT></COMMENT>
       <CONSTANT type="String" value="Please remove;key lock cover!" devalue="Abdeck. Tuerschloss; entfernen!" envalue="Please remove;key lock cover!">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="1">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerQuittung" name="WA_FIX_KEY_LOCK_COVER" dename="WA_ABDECK_ANBRINGEN" enname="WA_FIX_KEY_LOCK_COVER">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <COMMENT></COMMENT>
       <CONSTANT type="String" value="Please fix;key lock cover!" devalue="Abdeck. Tuerschloss;anbringen!" envalue="Please fix;key lock cover!">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="1">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerQuittung" name="WC_REMOVE_KEY_LOCK_COVER" dename="WQ_ABDECK_ENTFERNEN" enname="WC_REMOVE_KEY_LOCK_COVER">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <COMMENT></COMMENT>
       <CONSTANT type="String" value="Please remove;key lock cover!" devalue="Abdeck. Tuerschloss; entfernen!" envalue="Please remove;key lock cover!">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="1">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerQuittung" name="WC_FIX_KEY_LOCK_COVER" dename="WQ_ABDECK_ANBRINGEN" enname="WC_FIX_KEY_LOCK_COVER">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <COMMENT></COMMENT>
       <CONSTANT type="String" value="Please fix;key lock cover!" devalue="Abdeck. Tuerschloss;anbringen!" envalue="Please fix;key lock cover!">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="1">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="POWERDWON / POWERDOWN">
     <COMMENT></COMMENT>
     <LOADPP fileheader="WerkerQuittung" name="WQ_ACHTUNG_POWER_DOWN" dename="WQ_ACHTUNG_POWER_DOWN" enname="WQ_ATTENTION_POWER_DOWN">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Achtung: POWERDOWN Klappen und T�ren nicht bet�tigen!" devalue="Achtung: POWERDOWN Klappen und T�ren nicht bet�tigen!" envalue="Attention: POWERDOWN Do not touch the car!">
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="3">
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerQuittung" name="WQ_AUF_BUSRUHE_WARTEN" dename="WQ_AUF_BUSRUHE_WARTEN" enname="WQ_WAIT_BUSINACTIVITY">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Achtung Fahrzeug einschlafen lassen" devalue="Achtung Fahrzeug einschlafen lassen" envalue="Achtung Fahrzeug einschlafen lassen">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="3">
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerQuittung" name="WQ_POWER_DOWN_NOK" dename="WQ_POWER_DOWN_NOK" enname="WQ_POWER_DOWN_NOK">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Achtung: POWERDOWN NOK! Pr�fumfang bitte wiederholen!" devalue="Achtung: POWERDOWN NOK! Pr�fumfang bitte wiederholen!" envalue="Attention: POWERDOWN NOK! Please repeat the pruefumfang!">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="3">
       </CONSTANT>
      </PPARGS>
     </LOADPP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="RUHESTROMPR�FUNG / SLEEP CURRENT MEASUREMENT">
     <COMMENT></COMMENT>
     <LOADPP fileheader="WerkerQuittung" name="WQ_CLOSE_ALL_WINDOWS" dename="WQ_ALLE_FENSTER_SCHLIESSEN" enname="WQ_CLOSE_ALL_WINDOWS">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Be careful all windows will be closed." devalue="Achtung, alle Fenster werden geschlossen." envalue="Be careful all windows will be closed.">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="3">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerQuittung" name="WQ_NO_SLEEP_CURRENT_MEASUREMENT" dename="WQ_KEINE_RUHESTROMPRUEFUNG" enname="WQ_NO_SLEEP_CURRENT_MEASUREMENT">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <COMMENT></COMMENT>
       <CONSTANT type="String" value="NO SLEEP CURRENT MEASUREMENT DONE." devalue="KEINE RUHESTROMPR�FUNG DURCHGEF�HRT." envalue="NO SLEEP CURRENT MEASUREMENT DONE.">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="1">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerQuittung" name="WQ_WAKE_UP_VEHICLE_AND_ICOM" dename="WQ_FAHRZEUG_UND_ICOM_WECKEN" enname="WQ_WAKE_UP_VEHICLE_AND_ICOM">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <COMMENT></COMMENT>
       <CONSTANT type="String" value="Please open the door and switch on the ignition." devalue="Bitte T�r �ffnen und Z�ndung einschalten." envalue="Please open the door and switch on the ignition.">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="1">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="SICHTPR�FUNG / VISUAL CHECK">
     <COMMENT></COMMENT>
     <LOADPP fileheader="WerkerQuittung" name="WQ_SICHTPRUEFUNG" dename="WQ_SICHTPRUEFUNG" enname="WQ_VISUAL_CHECK">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Achtung!;Sichtpr�fung!" devalue="Achtung!;Sichtpr�fung!" envalue="Attention!;Visual check!">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="3">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
    <GROUP name="START PRUEFUNG / START TEST">
     <COMMENT></COMMENT>
     <LOADPP fileheader="WerkerQuittung" name="WQ_START_IM_FAHRZEUG_FS_WITH_KEYS">
      <COMMENT></COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="EINSTEIGEN FAHRERSEITE MIT SCHL�SSEL" devalue="EINSTEIGEN FAHRERSEITE MIT SCHL�SSEL" envalue="Sit in Front Driver seat and place keys back into vehicle.">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="3">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="TITEL" required="false">
       <CONSTANT type="String" value="ANWEISUNG" devalue="ANWEISUNG" envalue="INSTRUCTION">
       </CONSTANT>
      </PPARGS>
     </LOADPP>
     <LOADPP fileheader="WerkerQuittung" name="WQ_START_IM_FAHRZEUG_BS" dename="WQ_START_IM_FAHRZEUG_BS" enname="WQ_START_IN_VEHICLE_PASSENGER_SIDE">
      <COMMENT>Werkerfrage Start der Pr�fung auf der Beifahrerseite

Operator question start test on passenger side</COMMENT>
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="START IM FAHRZEUG; BEIFAHRERSEITE" devalue="START IM FAHRZEUG; BEIFAHRERSEITE" envalue="START INTO VEHICLE;PASSENGER SIDE">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="STYLE" required="true">
       <CONSTANT type="String" value="3">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
      <PPARGS name="TITEL" required="false">
       <CONSTANT type="String" value="ANWEISUNG">
       </CONSTANT>
      </PPARGS>
     </LOADPP>
    </GROUP>
    <GROUP name="------------------------------------------------">
     <COMMENT></COMMENT>
    </GROUP>
   </GROUP>
   <GROUP name="------------------------------------------------">
    <COMMENT></COMMENT>
   </GROUP>
  </GROUP>
  <GROUP name="------------------------------------------------">
   <COMMENT></COMMENT>
  </GROUP>
 </GROUP>
 <GROUP name="------------------------------------------------">
  <COMMENT></COMMENT>
 </GROUP>
 <GROUP name="PLANT SPECIFIC JOBS">
  <COMMENT>e.g. assembly deviations

et 09.04.2010 v305 add OrderWerk IDs

fw 02.11.2007 V53 moved FIZ specific group at end for central pruefling

fw 20.06.2008 v113 reworked parameters for plant specific test map</COMMENT>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="JOBS SPECIFIC PLANT W0">
   <COMMENT>FIZ - Prototypenbau</COMMENT>
   <LOADPP fileheader="WerkerQuittung" name="WQ_OLD_PU_VERSION">
    <COMMENT></COMMENT>
    <PPARGS name="AWT" required="true">
     <CONSTANT type="String" value="ES WIRD EIN VERALTETER PR�FUMFANG VERWENDET. BITTE SYSTEMBETREUER INFORMIEREN UND AUF ECOS_W0 UMSTELLEN!!!">
      <COMMENT></COMMENT>
     </CONSTANT>
    </PPARGS>
    <PPARGS name="STYLE" required="true">
     <CONSTANT type="String" value="3">
      <COMMENT></COMMENT>
     </CONSTANT>
    </PPARGS>
   </LOADPP>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="JOBS SPECIFIC PLANT W1">
   <COMMENT>M�nchen</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="equalsIgnoreCase" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="01.01">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="leer">
      <COMMENT></COMMENT>
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="JOBS SPECIFIC PLANT W2">
   <COMMENT>Dingolfing</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="startsWith" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="02">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
     <GROUP name="Decision Ruhestrompruefung">
      <COMMENT></COMMENT>
      <IF>
       <COMMENT>Vorserie</COMMENT>
       <BOOL_METHOD>
        <COMMENT></COMMENT>
        <METHOD name="getAttribut" returntype="String">
         <PARAMS>
          <CONSTANT type="String" value="VORSERIE">
          </CONSTANT>
         </PARAMS>
         <METHOD name="equals" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="TRUE">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <THEN>
        <LOADPP fileheader="DiagData" name="VORSERIE">
         <COMMENT>immer falsch</COMMENT>
         <PPARGS name="JOB1_CONDITION1" required="false">
          <CONSTANT type="String" value="EQUALS(RICHTIG,FALSCH)">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DEBUG" required="false">
          <CONSTANT type="String" value="FALSE">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_HWT1" required="false">
          <CONSTANT type="String" value="Kein Vorserienfahrzeug">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_JOB" required="false">
          <CONSTANT type="String" value="NONE">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_SGBD" required="false">
          <CONSTANT type="String" value="NONE">
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
       <ELSE>
        <LOADPP fileheader="DiagData" name="VORSERIE">
         <COMMENT>immer wahr</COMMENT>
         <PPARGS name="JOB1_CONDITION1" required="false">
          <CONSTANT type="String" value="EQUALS(RICHTIG,RICHTIG)">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DEBUG" required="false">
          <CONSTANT type="String" value="FALSE">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_HWT1" required="false">
          <CONSTANT type="String" value="Kein Vorserienfahrzeug">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_JOB" required="false">
          <CONSTANT type="String" value="NONE">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_SGBD" required="false">
          <CONSTANT type="String" value="NONE">
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </ELSE>
      </IF>
     </GROUP>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
     <GROUP name="ECOS Ladeger�t anschliessen">
      <COMMENT>W2 spezifisch; ECOS ist zu Beginn FINISH B�nder!</COMMENT>
      <LOADPP fileheader="WerkerFrage" name="WF_LADEGERAET_EIN">
       <COMMENT></COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="240000">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="FT" required="true">
        <CONSTANT type="String" value="Ladegeraet einschalten!" devalue="Ladegeraet einschalten!" envalue="Switch on Batterycharger!">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
     </GROUP>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
     <GROUP name="ECOS_Beipack_Pruefen">
      <COMMENT></COMMENT>
      <IF>
       <OR>
        <AND>
         <COMMENT>G30 nur H50 (SX0007)</COMMENT>
         <BOOL_METHOD>
          <COMMENT></COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G30">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
<NOT>
                    <BOOL_METHOD>
           <COMMENT></COMMENT>
          <METHOD name="containsSX" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="0007">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                </AND>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G31">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G32">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F34">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT></COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F36">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
       <THEN>
        <LOADPP fileheader="WerkerEingabe" name="WE_BEIPACK_ORDERNR_PRUEFEN">
         <COMMENT></COMMENT>
         <PPARGS name="AWT" required="true">
          <CONSTANT type="String" value="Barcode Ordernr Beipack einlesen!" devalue="Barcode Ordernr Beipack einlesen!" envalue="Barcode Ordernr Beipack einlesen!">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
        <LOADPP fileheader="SubString" name="STATUS_BEIPACK_ORDERNUMMER" dename="STATUS_BEIPACK_ORDERNUMMER" enname="STATUS_BEIPACK_ORDERNUMMER">
         <COMMENT>Extrahiert und vergleicht beipack-Ordernummer aus Barcode mit ORDER_ID aus FZG-Auftrag;</COMMENT>
         <PPARGS name="INSTRING" required="true">
          <CONSTANT type="String" value="INPUT@WE_BEIPACK_ORDERNR_PRUEFEN">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="STARTINDEX" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="2">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="STOPINDEX" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="9">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="COMPARESTRING" required="false">
          <COMMENT></COMMENT>
          <METHOD name="getOrderId" returntype="String">
           <COMMENT></COMMENT>
          </METHOD>
         </PPARGS>
         <PPARGS name="HWT" required="false">
          <CONSTANT type="String" value="Odernummer Beipack passt nicht zum Auftrag !" devalue="Odernummer Beipack passt nicht zum Auftrag!" envalue="Odernummer Beipack passt nicht zum Auftrag!">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
     </GROUP>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
     <GROUP name="ALLGEMEIN">
      <LOADPP fileheader="WerkerFrage" name="PRUEFUNG_ENDE" teststepid="16756">
       <COMMENT>�berpr�fen ggf. Entfall</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="240000">
        </CONSTANT>
       </PPARGS>
       <PPARGS name="FT" required="true">
        <CONSTANT type="String" value="Pr�fung ENDE?">
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="WerkerAnweisung" name="FALSCHE_FG_NUMMER">
       <COMMENT>wird f�r ECOS Z�hlpunkt FINISH-Band ben�tigt</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="5000">
        </CONSTANT>
       </PPARGS>
       <PPARGS name="AWT" required="true">
        <CONSTANT type="String" value="Falsche Fahrgestellnummer">
        </CONSTANT>
        <CONSTANT type="String" value="CASCADE wird beendet!">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <IF>
       <COMMENT>Extra Warten f�r N52T vor IBS-Auslesen</COMMENT>
       <AND>
        <BOOL_METHOD>
         <METHOD name="getMotorBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="N52">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
<NOT>
                  <BOOL_METHOD>
          <COMMENT></COMMENT>
         <METHOD name="containsSX" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="0007">
            <COMMENT>nur H50</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
              </AND>
       <THEN>
        <LOADPP fileheader="Pause" name="WARTEZEIT_5S_N52T">
         <COMMENT></COMMENT>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="5000">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="HWT" required="false">
          <CONSTANT type="String" value="Leider nochmal 5s">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
      <IF>
       <COMMENT>T�rkn�pfe 35up</COMMENT>
       <OR>
        <BOOL_METHOD>
         <COMMENT>M5 / M6 / X3M / X4M</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F9">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>35up</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
       <THEN>
        <LOADPP fileheader="WerkerFrage" name="WF_TUERKNOEPFE_VORN_UND_HINTEN_UNTEN">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="All Door Knob DD;down?" devalue="Alle T�rkn�pfe;unten?" envalue="All Door Knob DD;down?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
          </CONSTANT>
         </PPARGS>
        </LOADPP>
        <LOADPP fileheader="WerkerFrage" name="WF_TUERKNOEPFE_VORN_UND_HINTEN_OBEN">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="All Door Knob DD;up?" devalue="Alle T�rkn�pfe;oben?" envalue="All Door Knob DD;up?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
          </CONSTANT>
         </PPARGS>
        </LOADPP>
        <LOADPP fileheader="WerkerFrage" name="WF_TUERKNOEPFE_VORN_UND_HINTEN_FA_UNTEN">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Alle T�rkn�pfe Fahrerseite unten?" devalue="Alle T�rkn�pfe Fahrerseite unten?" envalue="All Door Knob Driverside down?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
          </CONSTANT>
         </PPARGS>
        </LOADPP>
        <LOADPP fileheader="WerkerFrage" name="WF_TUERKNOEPFE_VORN_UND_HINTEN_FA_OBEN">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Alle T�rkn�pfe Fahrerseite oben?" devalue="Alle T�rkn�pfe Fahrerseite oben?" envalue="All Door Knob driverside up?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
          </CONSTANT>
         </PPARGS>
        </LOADPP>
        <LOADPP fileheader="WerkerFrage" name="WF_TUERKNOEPFE_VORN_UND_HINTEN_BF_UNTEN">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Alle T�rkn�pfe BFseite unten?" devalue="Alle T�rkn�pfe BFseite unten?" envalue="All Door Knob passengerside down?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
          </CONSTANT>
         </PPARGS>
        </LOADPP>
        <LOADPP fileheader="WerkerFrage" name="WF_TUERKNOEPFE_VORN_UND_HINTEN_BF_OBEN">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Alle T�rkn�pfe BFseite oben?" devalue="Alle T�rkn�pfe BFseite oben?" envalue="All Door Knob passengerside up?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
      <IF>
       <COMMENT>Tankdeckel 35up</COMMENT>
       <OR>
        <COMMENT>35up</COMMENT>
        <BOOL_METHOD>
         <COMMENT>5 series sedan</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G30">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>5 series wagon</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G31">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>5 series GT</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G32">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>7 series sedan</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G11">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>7 series sedan extended wheel</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G12">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
       <THEN>
        <LOADPP fileheader="WerkerQuittung" name="WQ_TUEREN_GESCHLOSSEN" dename="WQ_TUEREN_GESCHLOSSEN" enname="WQ_TUEREN_GESCHLOSSEN">
         <COMMENT></COMMENT>
         <PPARGS name="AWT" required="true">
          <CONSTANT type="String" value="Alle T�ren; schliessen!" devalue="Alle T�ren; schliessen!" envalue="Alle T�ren; schliessen!">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="STYLE" required="true">
          <CONSTANT type="String" value="3">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
        <IF>
         <OR>
          <COMMENT>WerkerFrage Tankklappe ge�ffnet/ geschlossen</COMMENT>
          <BOOL_METHOD>
           <METHOD name="getFahrerPosition" returntype="String">
            <METHOD name="equals" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="LL">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getL�nderVariante" returntype="String">
            <METHOD name="equals" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="US">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <THEN>
          <LOADPP fileheader="WerkerFrage" name="WF_TANKKLAPPE_GESCHLOSSEN_LL" enname="WF_FUEL_FILLER_FLAP_CLOSED_LHD" teststepid="6914">
           <PPARGS name="FT" required="true">
            <CONSTANT type="String" value="Fuel filler flap;closed? y/n" devalue="Tankklappe;geschlossen ?" envalue="Fuel filler flap;closed? y/n">
            </CONSTANT>
           </PPARGS>
           <PPARGS name="DAUER" required="true">
            <CONSTANT type="String" value="300000">
            </CONSTANT>
           </PPARGS>
          </LOADPP>
          <LOADPP fileheader="WerkerFrage" name="WF_TANKKLAPPE_GEOEFFNET_LL" enname="WF_FUEL_FILLER_FLAP_OPEN_LHD" teststepid="6914">
           <PPARGS name="FT" required="true">
            <CONSTANT type="String" value="Fuel filler flap;opened? y/n" devalue="Tankklappe;ge�ffnet ?" envalue="Fuel filler flap;opened? y/n">
            </CONSTANT>
           </PPARGS>
           <PPARGS name="DAUER" required="true">
            <CONSTANT type="String" value="300000">
            </CONSTANT>
           </PPARGS>
          </LOADPP>
         </THEN>
         <ELSE>
          <LOADPP fileheader="WerkerFrage" name="WF_TANKKLAPPE_GESCHLOSSEN_RL" enname="WF_FUEL_FILLER_FLAP_CLOSED_RHD" teststepid="6914">
           <PPARGS name="FT" required="true">
            <CONSTANT type="String" value="Fuel filler flap;closed? y/n" devalue="Tankklappe;geschlossen ?" envalue="Fuel filler flap;closed? y/n">
            </CONSTANT>
           </PPARGS>
           <PPARGS name="DAUER" required="true">
            <CONSTANT type="String" value="300000">
            </CONSTANT>
           </PPARGS>
          </LOADPP>
          <LOADPP fileheader="WerkerFrage" name="WF_TANKKLAPPE_GEOEFFNET_RL" enname="WF_FUEL_FILLER_FLAP_OPEN_RHD" teststepid="6914">
           <PPARGS name="FT" required="true">
            <CONSTANT type="String" value="Fuel filler flap;opened? y/n" devalue="Tankklappe;ge�ffnet ?" envalue="Fuel filler flap;opened? y/n">
            </CONSTANT>
           </PPARGS>
           <PPARGS name="DAUER" required="true">
            <CONSTANT type="String" value="300000">
            </CONSTANT>
           </PPARGS>
          </LOADPP>
         </ELSE>
        </IF>
       </THEN>
      </IF>
      <LOADPP fileheader="WerkerFrage" name="WF_TUER_BFS_SCHLOSS_FALSCH_VERBAUT">
       <COMMENT>Q-Massnahme; Sequenzanlieferung neue STVM problematisch;</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="240000">
        </CONSTANT>
       </PPARGS>
       <PPARGS name="FT" required="true">
        <CONSTANT type="String" value="TUER BFS; KEIN SCHLOSS VERBAUT?" devalue="TUER BFS; KEIN SCHLOSS VERBAUT?" envalue="Door Passengerside; No lock assembled?">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="TITEL" required="false">
        <CONSTANT type="String" value="SICHTPRUEFUNG">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
     </GROUP>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
     <GROUP name="WERKSUMFANG_FXX">
      <COMMENT>Ecos Allgemein L6 Werk Dingolfing</COMMENT>
      <LOADPP fileheader="WerkerFrage" name="WF_HANDSCHUHFACHBELEUCHTUNG">
       <COMMENT>umbenennen in den PUs</COMMENT>
       <PPARGS name="TITEL" required="false">
        <CONSTANT type="String" value="Sichtpr�fung" devalue="Sichtpr�fung" envalue="Visual Check">
        </CONSTANT>
       </PPARGS>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="240000">
        </CONSTANT>
       </PPARGS>
       <PPARGS name="FT" required="true">
        <CONSTANT type="String" value="Handschuhfach Beleuchtung IO?" devalue="Handschuhfach Beleuchtung IO?" envalue="Glovebox light OK?">
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <IF>
       <COMMENT>N52</COMMENT>
       <OR>
        <BOOL_METHOD>
         <METHOD name="getMotorBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="N52">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <METHOD name="getMotorBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="N47">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <METHOD name="getMotorBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="B47">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <METHOD name="getMotorBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="N57">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
       <THEN>
        <LOADPP fileheader="DiagToleranz" name="STEUERN_KLEMME_KL15_AKTIVIEREN_N52">
         <COMMENT></COMMENT>
         <PPARGS name="SGBD" required="true">
          <CONSTANT type="String" value="CAS4_2">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB" required="true">
          <CONSTANT type="String" value="STEUERN_KLEMMEN">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOBPAR" required="false">
          <CONSTANT type="String" value="KL15_EIN">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="TIMEOUT" required="false">
          <CONSTANT type="String" value="1000">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="PAUSE" required="false">
          <CONSTANT type="String" value="100">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="IGNORE_ERRORS" required="false">
          <CONSTANT type="String" value="TRUE">
          </CONSTANT>
         </PPARGS>
        </LOADPP>
        <LOADPP fileheader="Pause" name="WARTEZEIT_3S_KL15_AKTIVIEREN">
         <COMMENT></COMMENT>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="3000">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
      <IF>
       <COMMENT>F01-F03 Fussstuetzen / Fussmatten</COMMENT>
       <OR>
        <COMMENT></COMMENT>
        <BOOL_METHOD>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equals" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="F01">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equals" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="F02">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equals" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="F03">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
       <THEN>
        <IF>
         <COMMENT>Fussstuetzen</COMMENT>
         <OR>
          <BOOL_METHOD>
           <COMMENT>F02</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equals" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F02">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <THEN>
          <LOADPP fileheader="WerkerFrage" name="FUSSSTUETZEN" teststepid="17516">
           <PPARGS name="FT" required="true">
            <CONSTANT type="String" value="Fussst�tzen beilegen !!!" devalue="Fussst�tzen beilegen !!!" envalue="Add foot rests !!!">
            </CONSTANT>
           </PPARGS>
           <PPARGS name="DAUER" required="true">
            <CONSTANT type="String" value="2400000">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PPARGS>
           <PPARGS name="TITEL" required="false">
            <CONSTANT type="String" value="ANWEISUNG" devalue="ANWEISUNG" envalue="INSTRUCTION">
            </CONSTANT>
           </PPARGS>
          </LOADPP>
         </THEN>
        </IF>
        <IF>
         <COMMENT>Fussmatten Velours</COMMENT>
         <BOOL_METHOD>
          <COMMENT>Fussmatten Velours</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="0423">
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <THEN>
          <LOADPP fileheader="WerkerFrage" name="FUSSRAUMMATTEN" teststepid="16756">
           <PPARGS name="TITEL" required="false">
            <CONSTANT type="String" value="Sichtpr�fung" devalue="Sichtpr�fung" envalue="Visual check">
            </CONSTANT>
           </PPARGS>
           <PPARGS name="DAUER" required="true">
            <CONSTANT type="String" value="2400000">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PPARGS>
           <PPARGS name="FT" required="true">
            <CONSTANT type="String" value="FUSSRAUMMATTEN BEIGELEGT?" devalue="FUSSRAUMMATTEN BEIGELEGT?" envalue="Footwell mats added?">
            </CONSTANT>
           </PPARGS>
          </LOADPP>
         </THEN>
        </IF>
       </THEN>
      </IF>
      <IF>
       <COMMENT>Plant specific: Datenlogger bei SA0909</COMMENT>
       <BOOL_METHOD>
        <METHOD name="containsSA" returntype="boolean">
         <PARAMS>
          <CONSTANT type="String" value="0909">
          </CONSTANT>
         </PARAMS>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <THEN>
        <LOADPP fileheader="WerkerQuittung" name="WQ_DATENLOGGER_AKTIVIEREN">
         <PPARGS name="AWT" required="true">
          <CONSTANT type="String" value="Datenlogger auf">
          </CONSTANT>
          <CONSTANT type="String" value="Pr�fzone laden">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="STYLE" required="true">
          <CONSTANT type="String" value="3">
          </CONSTANT>
         </PPARGS>
        </LOADPP>
        <LOADPP fileheader="WerkerQuittung" name="WQ_DATENLOGGER_DEAKTIVIEREN">
         <PPARGS name="AWT" required="true">
          <CONSTANT type="String" value="Datenlogger auf">
          </CONSTANT>
          <CONSTANT type="String" value="ECOS Akkubetrieb">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="STYLE" required="true">
          <CONSTANT type="String" value="3">
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
      <LOADPP fileheader="WerkerQuittung" name="WF_START_IM_FAHRZEUG_BS">
       <COMMENT>Plant specific</COMMENT>
       <PPARGS name="AWT" required="true">
        <CONSTANT type="String" value="START IM FAHRZEUG BEIFAHRERSEITE" devalue="START IM FAHRZEUG BEIFAHRERSEITE" envalue="Start Test on passenger side">
        </CONSTANT>
       </PPARGS>
       <PPARGS name="STYLE" required="true">
        <CONSTANT type="String" value="3">
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="WerkerFrage" name="WF_START_IM_FAHRZEUG_FS">
       <COMMENT>Plant specific</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="2700000">
        </CONSTANT>
       </PPARGS>
       <PPARGS name="FT" required="true">
        <CONSTANT type="String" value="EINSTEIGEN FAHRERSEITE" devalue="EINSTEIGEN FAHRERSEITE" envalue="Enter car on drviver side">
        </CONSTANT>
       </PPARGS>
       <PPARGS name="TITEL" required="false">
        <CONSTANT type="String" value="ANWEISUNG" devalue="ANWEISUNG" envalue="INSTRUCTION">
        </CONSTANT>
       </PPARGS>
      </LOADPP>
     </GROUP>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
     <GROUP name="ICEBOX CHECK">
      <COMMENT></COMMENT>
      <IF>
       <COMMENT>ICEBOX CHECK</COMMENT>
       <BOOL_METHOD>
        <COMMENT>icebox</COMMENT>
        <METHOD name="containsSA" returntype="boolean">
         <PARAMS>
          <CONSTANT type="String" value="0791">
          </CONSTANT>
         </PARAMS>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <THEN>
        <LOADPP fileheader="WerkerFrage" name="WF_KUEHLBOX_LED_CHECK">
         <COMMENT></COMMENT>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="2300000">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="K�hlbox einschalten, leuchtet/blinkt LED?" devalue="K�hlbox einschalten, leuchtet/blinkt LED?" envalue="Switch on icebox. Is the LED on/blinking?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
     </GROUP>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
     <GROUP name="FAHRPEDALMODUL">
      <COMMENT></COMMENT>
      <IF>
       <AND>
        <OR>
         <BOOL_METHOD>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="F10">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="F11">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="F12">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="F13">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
<NOT>
                  <BOOL_METHOD>
          <COMMENT></COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="0205">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
       <NOT>
                  <BOOL_METHOD>
          <COMMENT></COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="02TB">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
       <NOT>
                  <BOOL_METHOD>
          <COMMENT></COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="02MK">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
       <NOT>
                  <BOOL_METHOD>
          <COMMENT>Automatikgetriebe mit W�hlhebel und Paddles</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="02TE">
            <COMMENT>SA2TB=Sport-Automatik-Getriebe</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
       <NOT>
                  <AND>
          <COMMENT>F12/F13, ab 03/15</COMMENT>
         <OR>
          <COMMENT>F12, F13</COMMENT>
          <BOOL_METHOD>
           <COMMENT>6er Cabrio</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equals" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F12">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>6er Coupe</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equals" returntype="boolean">
             <PARAMS>
              <CONSTANT type="String" value="F13">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <OR>
          <COMMENT>ab 03/15</COMMENT>
          <BOOL_METHOD>
           <COMMENT></COMMENT>
           <METHOD name="getIStufeJahrInt" returntype="int">
            <COMMENT></COMMENT>
           </METHOD>
           <OPERATOR type="greater" optype="int">
            <COMMENT></COMMENT>
           </OPERATOR>
           <CONSTANT type="int" value="15">
            <COMMENT></COMMENT>
           </CONSTANT>
          </BOOL_METHOD>
          <AND>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="getIStufeMonatInt" returntype="int">
             <COMMENT></COMMENT>
            </METHOD>
            <OPERATOR type="g_equal" optype="int">
             <COMMENT></COMMENT>
            </OPERATOR>
            <CONSTANT type="int" value="3">
             <COMMENT></COMMENT>
            </CONSTANT>
           </BOOL_METHOD>
           <BOOL_METHOD>
            <COMMENT></COMMENT>
            <METHOD name="getIStufeJahrInt" returntype="int">
             <COMMENT></COMMENT>
            </METHOD>
            <OPERATOR type="eq" optype="int">
             <COMMENT></COMMENT>
            </OPERATOR>
            <CONSTANT type="int" value="15">
             <COMMENT></COMMENT>
            </CONSTANT>
           </BOOL_METHOD>
          </AND>
         </OR>
<NOT>
                    <BOOL_METHOD>
           <COMMENT>Handschalter</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="02MA">
             <COMMENT>in verbindung mit DKG</COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                </AND>
</NOT>
              </AND>
       <THEN>
        <LOADPP fileheader="WerkerFrage" name="WF_FAHRPEDALMODUL_OHNE_KICKDOWN">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Gaspedal durchdr�cken;KEIN Kickdown verbaut?" devalue="Gaspedal durchdr�cken;KEIN Kickdown verbaut?" envalue="push through gas pedal; NO Kick-down assembled?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="360000">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
     </GROUP>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
     <GROUP name="MAL Ablagefach Kunststoffabdeckungen">
      <COMMENT>Verbau TP-4 RiFi ergab keine Wiederentdeckungswahrscheinlichkeit im Montageablauf Lehnhardt Alexander; TVG K 5116 007 15R A 01</COMMENT>
      <IF>
       <COMMENT>Kunststoffabdeckungen vo und hi i.O.</COMMENT>
       <OR>
        <AND>
         <COMMENT>5er Derivate nur in H52</COMMENT>
         <BOOL_METHOD>
          <COMMENT>5er Lim</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G30">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>nur G30 H52</COMMENT>
          <METHOD name="containsSX" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="0007">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </AND>
        <BOOL_METHOD>
         <COMMENT>M5</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <METHOD name="equalsIgnoreCase" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F90">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
       <THEN>
        <LOADPP fileheader="WerkerFrage" name="WF_MAL_DECKEL">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="MAL Ablagefach; Deckel vo + hi i.O.?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="360000">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
     </GROUP>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
     <GROUP name="SKI UND SNOWBOARDTASCHE">
      <COMMENT></COMMENT>
      <IF>
       <COMMENT>SKI UND SNOWBOARDTASCHE</COMMENT>
       <BOOL_METHOD>
        <COMMENT></COMMENT>
        <METHOD name="containsSA" returntype="boolean">
         <PARAMS>
          <CONSTANT type="String" value="04UY">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PARAMS>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <THEN>
        <LOADPP fileheader="WerkerFrage" name="WF_SKITASCHE_BEILEGEN">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Ski-Tasche in Gep�ckraummulde beilegen!">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="360000">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
     </GROUP>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
     <GROUP name="Div Umfaenge F10 F11 und F07 in H50">
      <COMMENT>Prozessbedingt;</COMMENT>
      <IF>
       <COMMENT>Block f�r H50 F07/F10/F11</COMMENT>
       <OR>
        <AND>
         <COMMENT>F10 H50</COMMENT>
         <BOOL_METHOD>
          <METHOD name="getBaureihe" returntype="String">
           <COMMENT></COMMENT>
           <METHOD name="equals" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="F10">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
<NOT>
                    <BOOL_METHOD>
           <COMMENT>HALLE 52 F-10</COMMENT>
          <METHOD name="containsSX" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="0007">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
</NOT>
                </AND>
        <BOOL_METHOD>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equals" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F07">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equals" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F11">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equals" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F34">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="equals" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="F36">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </OR>
       <THEN>
        <IF>
         <COMMENT>Versandschutzpaket H.50</COMMENT>
         <BOOL_METHOD>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="0925">
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <THEN>
          <LOADPP fileheader="WerkerQuittung" name="WQ_VERSANDSCHUTZ">
           <PPARGS name="AWT" required="true">
            <CONSTANT type="String" value="Abdeckung Abschlepp">
            </CONSTANT>
            <CONSTANT type="String" value="oese entfernen!">
            </CONSTANT>
           </PPARGS>
           <PPARGS name="STYLE" required="true">
            <CONSTANT type="String" value="3">
            </CONSTANT>
           </PPARGS>
          </LOADPP>
         </THEN>
        </IF>
        <IF>
         <COMMENT>USA</COMMENT>
         <BOOL_METHOD>
          <COMMENT>US Verschlussstopfen</COMMENT>
          <METHOD name="getL�nderVariante" returntype="String">
           <METHOD name="equals" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="USA">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <THEN>
          <LOADPP fileheader="WerkerFrage" name="WF_HAUPTSCHEINWERFER_STOPFEN_VERBAUT">
           <COMMENT></COMMENT>
           <PPARGS name="DAUER" required="true">
            <CONSTANT type="String" value="360000">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PPARGS>
           <PPARGS name="FT" required="true">
            <CONSTANT type="String" value="US Blindstopfen ! F�r Scheinwerfer verbaut?">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PPARGS>
          </LOADPP>
         </THEN>
        </IF>
        <IF>
         <COMMENT>ECE</COMMENT>
         <BOOL_METHOD>
          <COMMENT>ECE Verschlussstopfen</COMMENT>
          <METHOD name="getL�nderVariante" returntype="String">
           <METHOD name="equals" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="ECE">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <THEN>
          <LOADPP fileheader="WerkerFrage" name="WF_HAUPTSCHEINWERFER_STOPFEN_VERBAUT">
           <COMMENT></COMMENT>
           <PPARGS name="DAUER" required="true">
            <CONSTANT type="String" value="360000">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PPARGS>
           <PPARGS name="FT" required="true">
            <CONSTANT type="String" value="ECE Blindstopfen ! F�r Scheinwerfer verbaut?">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PPARGS>
          </LOADPP>
         </THEN>
        </IF>
       </THEN>
      </IF>
     </GROUP>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
     <GROUP name="Armlehnenheizung_FAS">
      <COMMENT></COMMENT>
      <IF>
       <AND>
        <BOOL_METHOD>
         <COMMENT>35up</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <OR>
         <BOOL_METHOD>
          <COMMENT>W�rmepaket vorne</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="04HB">
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>W�rmepaket vorne + hinten</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="04HC">
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
       </AND>
       <THEN>
        <LOADPP fileheader="DiagData" name="ARMLEHNENHEIZUNG_FAS_VORHANDEN">
         <COMMENT>PS wird immer IO</COMMENT>
         <PPARGS name="JOBN_SGBD" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="NONE">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_JOB" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="NONE">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_CONDITION1" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="EQUALS(1,1)">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
       <ELSE>
        <LOADPP fileheader="DiagData" name="ARMLEHNENHEIZUNG_FAS_VORHANDEN">
         <COMMENT>PS wird immer NIO</COMMENT>
         <PPARGS name="JOBN_SGBD" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="NONE">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_JOB" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="NONE">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_CONDITION1" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="EQUALS(0,1)">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </ELSE>
      </IF>
     </GROUP>
     <GROUP name="Armlehnenheizung_FAHS">
      <COMMENT></COMMENT>
      <IF>
       <AND>
        <BOOL_METHOD>
         <COMMENT>35up</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>W�rmepaket vorne + hinten</COMMENT>
         <METHOD name="containsSA" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="04HC">
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </AND>
       <THEN>
        <LOADPP fileheader="DiagData" name="ARMLEHNENHEIZUNG_FAHS_VORHANDEN">
         <COMMENT>PS wird immer IO</COMMENT>
         <PPARGS name="JOBN_SGBD" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="NONE">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_JOB" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="NONE">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_CONDITION1" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="EQUALS(1,1)">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
       <ELSE>
        <LOADPP fileheader="DiagData" name="ARMLEHNENHEIZUNG_FAHS_VORHANDEN">
         <COMMENT>PS wird immer NIO</COMMENT>
         <PPARGS name="JOBN_SGBD" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="NONE">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_JOB" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="NONE">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_CONDITION1" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="EQUALS(0,1)">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </ELSE>
      </IF>
     </GROUP>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
     <GROUP name="Schlossnuss ohne Hallsensor L6">
      <COMMENT>Ab 11/2015 entfallen die Hallsensoren in der Schlossnuss;</COMMENT>
      <IF>
       <AND>
        <OR>
         <COMMENT>L6</COMMENT>
         <BOOL_METHOD>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="startsWith" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="F0">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="startsWith" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="F1">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <OR>
         <AND>
          <BOOL_METHOD>
           <METHOD name="getIStufeMonatInt" returntype="int">
           </METHOD>
           <OPERATOR type="g_equal" optype="int">
           </OPERATOR>
           <CONSTANT type="int" value="11">
           </CONSTANT>
          </BOOL_METHOD>
          <BOOL_METHOD>
           <METHOD name="getIStufeJahrInt" returntype="int">
           </METHOD>
           <OPERATOR type="eq" optype="int">
           </OPERATOR>
           <CONSTANT type="int" value="15">
           </CONSTANT>
          </BOOL_METHOD>
         </AND>
         <BOOL_METHOD>
          <METHOD name="getIStufeJahrInt" returntype="int">
          </METHOD>
          <OPERATOR type="greater" optype="int">
          </OPERATOR>
          <CONSTANT type="int" value="15">
          </CONSTANT>
         </BOOL_METHOD>
        </OR>
       </AND>
       <THEN>
        <LOADPP fileheader="DiagData" name="SICHTPR�FUNG_SCHLOSSNUSS">
         <COMMENT>PS wird immer IO</COMMENT>
         <PPARGS name="JOBN_SGBD" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="NONE">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_JOB" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="NONE">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_CONDITION1" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="EQUALS(1,1)">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
       <ELSE>
        <LOADPP fileheader="DiagData" name="SICHTPR�FUNG_SCHLOSSNUSS">
         <COMMENT>PS wird immer NIO</COMMENT>
         <PPARGS name="JOBN_SGBD" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="NONE">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_JOB" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="NONE">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_CONDITION1" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="EQUALS(0,1)">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </ELSE>
      </IF>
     </GROUP>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
     <GROUP name="LUFTAUSTROEMER MECH B SAEULE FOND / AIR VENT REAR MECH  B COLUMN">
      <COMMENT></COMMENT>
      <IF>
       <OR>
        <OR>
         <COMMENT>G11 G12</COMMENT>
         <BOOL_METHOD>
          <COMMENT>7 series sedan</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G11">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>7 series sedan extended wheel</COMMENT>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="equalsIgnoreCase" returntype="boolean">
            <COMMENT></COMMENT>
            <PARAMS>
             <CONSTANT type="String" value="G12">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <AND>
         <COMMENT>G30 G31 G32</COMMENT>
         <OR>
          <COMMENT>F90, G30, G31, G32</COMMENT>
          <BOOL_METHOD>
           <COMMENT>M5</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <COMMENT></COMMENT>
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="F90">
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>5 series sedan</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G30">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>5 series wagon</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G31">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
          <BOOL_METHOD>
           <COMMENT>5 series GT</COMMENT>
           <METHOD name="getBaureihe" returntype="String">
            <METHOD name="equalsIgnoreCase" returntype="boolean">
             <COMMENT></COMMENT>
             <PARAMS>
              <CONSTANT type="String" value="G32">
               <COMMENT></COMMENT>
              </CONSTANT>
             </PARAMS>
            </METHOD>
           </METHOD>
                    <OPERATOR type="eq" optype="boolean"> </OPERATOR>
          <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         </OR>
         <BOOL_METHOD>
          <COMMENT>Fondklima</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="04NB">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </AND>
       </OR>
       <THEN>
        <LOADPP fileheader="WerkerFrage" name="WF_LUFTAUSTROEMER_MECH_B_SAEULE_FAS" dename="WF_LUFTAUSTROEMER_MECH_B_SAEULE_FAS" enname="WF_LUFTAUSTROEMER_MECH_B_SAEULE_FAS">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Luftgitter FAS; B S�ule mechan. u. Luft IO?" devalue="Luftgitter FAS; B S�ule mechan. u. Luft IO?" envalue="Luftgitter FAS; B S�ule mechan. u. Luft IO?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
        <LOADPP fileheader="WerkerFrage" name="WF_LUFTAUSTROEMER_MECH_B_SAEULE_BFS" dename="WF_LUFTAUSTROEMER_MECH_B_SAEULE_BFS" enname="WF_LUFTAUSTROEMER_MECH_B_SAEULE_BFS">
         <COMMENT></COMMENT>
         <PPARGS name="FT" required="true">
          <CONSTANT type="String" value="Luftgitter BFS; B S�ule mechan. u. Luft IO?" devalue="Luftgitter BFS; B S�ule mechan. u. Luft IO?" envalue="Luftgitter BFS; B S�ule mechan. u. Luft  IO?">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="DAUER" required="true">
          <CONSTANT type="String" value="300000">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
     </GROUP>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
     <GROUP name="PLT_SKID_PRUEFEN">
      <COMMENT></COMMENT>
      <IF>
       <BOOL_METHOD>
        <COMMENT>35up</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <COMMENT></COMMENT>
         <METHOD name="startsWith" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="G">
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <THEN>
        <LOADPP fileheader="WerkerEingabe" name="SCAN_SKID_NR" dename="SCAN_SKID_NR" enname="SCAN_SKID_NR">
         <COMMENT></COMMENT>
         <PPARGS name="AWT" required="true">
          <CONSTANT type="String" value="Bitte die SKID-Nummer scannen!" devalue="Bitte die SKID-Nummer scannen!" envalue="Bitte die SKID-Nummer scannen!">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="MIN" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="SKID0%">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
     </GROUP>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
     <GROUP name="Workaround_Entfall_Pr�fung_FFB1_FFB2_wegen_SA">
      <COMMENT>Entfall Display-Schl�ssel</COMMENT>
      <IF>
       <AND>
        <BOOL_METHOD>
         <METHOD name="containsSX" returntype="boolean">
          <COMMENT></COMMENT>
          <PARAMS>
           <CONSTANT type="String" value="00K5">
            <COMMENT>Kurzfristig Entfall DISPLAY-Schl�ssel</COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        <BOOL_METHOD>
         <COMMENT>35up</COMMENT>
         <METHOD name="getBaureihe" returntype="String">
          <COMMENT></COMMENT>
          <METHOD name="startsWith" returntype="boolean">
           <COMMENT></COMMENT>
           <PARAMS>
            <CONSTANT type="String" value="G">
            </CONSTANT>
           </PARAMS>
          </METHOD>
         </METHOD>
                <OPERATOR type="eq" optype="boolean"> </OPERATOR>
        <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       </AND>
       <THEN>
        <LOADPP fileheader="DiagData" name="PRUEFUNG_OHNE_FFB1_FFB2">
         <COMMENT>PS wird immer IO</COMMENT>
         <PPARGS name="JOBN_SGBD" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="NONE">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_JOB" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="NONE">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_CONDITION1" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="EQUALS(1,1)">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
       <ELSE>
        <LOADPP fileheader="DiagData" name="PRUEFUNG_OHNE_FFB1_FFB2">
         <COMMENT>PS wird immer NIO</COMMENT>
         <PPARGS name="JOBN_SGBD" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="NONE">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_JOB" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="NONE">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB1_CONDITION1" required="false">
          <COMMENT></COMMENT>
          <CONSTANT type="String" value="EQUALS(0,1)">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </ELSE>
      </IF>
     </GROUP>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
     <GROUP name="HKL_schliessen_bei_TOP_VIEW">
      <COMMENT>L6 HKL mu� bei TOP View geschlossen sein</COMMENT>
      <IF>
       <COMMENT>L6 Top-View-Kamerabild beurteilen HKL muss geschlossen sein</COMMENT>
       <AND>
        <OR>
         <BOOL_METHOD>
          <COMMENT>R�ckfahrkamera mit Top View (nur F07 Japan)</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="03AH">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <COMMENT>Surround View</COMMENT>
          <METHOD name="containsSA" returntype="boolean">
           <PARAMS>
            <CONSTANT type="String" value="05DL">
             <COMMENT></COMMENT>
            </CONSTANT>
           </PARAMS>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
        <OR>
         <COMMENT>L6</COMMENT>
         <BOOL_METHOD>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="startsWith" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="F0">
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
         <BOOL_METHOD>
          <METHOD name="getBaureihe" returntype="String">
           <METHOD name="startsWith" returntype="boolean">
            <PARAMS>
             <CONSTANT type="String" value="F1">
              <COMMENT></COMMENT>
             </CONSTANT>
            </PARAMS>
           </METHOD>
          </METHOD>
                  <OPERATOR type="eq" optype="boolean"> </OPERATOR>
         <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
        </OR>
       </AND>
       <THEN>
        <LOADPP fileheader="DiagToleranz" name="INT_HECKKLAPPE_ZU">
         <COMMENT></COMMENT>
         <PPARGS name="SGBD" required="true">
          <CONSTANT type="String" value="JBBF3">
           <COMMENT></COMMENT>
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOB" required="true">
          <CONSTANT type="String" value="STATUS_LESEN">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="AWT" required="false">
          <CONSTANT type="String" value="Heckklappe schliessen" devalue="Heckklappe schliessen" envalue="Close Trunklid">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="JOBPAR" required="false">
          <CONSTANT type="String" value="ARG">
          </CONSTANT>
          <CONSTANT type="String" value="KONTAKT_HECKKLAPPE_EIN">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="RESULT1" required="false">
          <CONSTANT type="String" value="STAT_KONTAKT_HECKKLAPPE_EIN">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="MIN1" required="false">
          <CONSTANT type="String" value="1">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="CANCEL" required="false">
          <CONSTANT type="String" value="TRUE">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="TIMEOUT" required="false">
          <CONSTANT type="String" value="240000">
          </CONSTANT>
         </PPARGS>
         <PPARGS name="HWT" required="false">
          <CONSTANT type="String" value="Kontakt oder Taster Heckklappe defekt" devalue="Kontakt oder Taster Heckklappe defekt" envalue="Contact or Switch Trunklid defective">
          </CONSTANT>
         </PPARGS>
        </LOADPP>
       </THEN>
      </IF>
     </GROUP>
     <GROUP name="------------------------------------------------">
      <COMMENT></COMMENT>
     </GROUP>
     <GROUP name="WARTEZEIT / WAITING TIME">
      <COMMENT></COMMENT>
      <LOADPP fileheader="Pause" name="WARTEZEIT_100MS" dename="WARTEZEIT_100MS" enname="WAIT_100MS">
       <COMMENT>100ms Wartezeit

100ms waiting time</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="100">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="Pause" name="WARTEZEIT_200MS" dename="WARTEZEIT_200MS" enname="WAIT_200MS">
       <COMMENT>200ms Wartezeit

200ms waiting time</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="200">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="Pause" name="WARTEZEIT_500MS" dename="WARTEZEIT_500MS" enname="WAIT_500MS">
       <COMMENT>500ms Wartezeit

500ms waiting time</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="500">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="Pause" name="WARTEZEIT_1S" dename="WARTEZEIT_1S" enname="WAIT_1S">
       <COMMENT>1s Wartezeit

1s waiting time</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="1000">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="HWT" required="false">
        <CONSTANT type="String" value="Waiting time 1s" devalue="Wartezeit 1s" envalue="Waiting time 1s">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="Pause" name="WARTEZEIT_2S" dename="WARTEZEIT_2S" enname="WAIT_2S">
       <COMMENT>2s Wartezeit

2s waiting time</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="2000">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="HWT" required="false">
        <CONSTANT type="String" value="Waiting time 2s" devalue="Wartezeit 2s" envalue="Waiting time 2s">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="Pause" name="WARTEZEIT_3S" dename="WARTEZEIT_3S" enname="WAIT_3S">
       <COMMENT>3s Wartezeit

3s waiting time</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="3000">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="HWT" required="false">
        <CONSTANT type="String" value="Waiting time 3s" devalue="Wartezeit 3s" envalue="Waiting time 3s">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="Pause" name="WARTEZEIT_5S" dename="WARTEZEIT_5S" enname="WAIT_5S">
       <COMMENT>5s Wartezeit

5s waiting time</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="5000">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="HWT" required="false">
        <CONSTANT type="String" value="Waiting time 5s" devalue="Wartezeit 5s" envalue="Waiting time 5s">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="Pause" name="WARTEZEIT_10S" dename="WARTEZEIT_10S" enname="WAIT_10S">
       <COMMENT>10s Wartezeit

10s waiting time</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="10000">
        </CONSTANT>
       </PPARGS>
       <PPARGS name="HWT" required="false">
        <CONSTANT type="String" value="Waiting time 10s" devalue="Wartezeit 10s" envalue="Waiting time 10s">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="Pause" name="WARTEZEIT_20S" dename="WARTEZEIT_20S" enname="WAIT_20S">
       <COMMENT>20s Wartezeit

20s waiting time</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="20000">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="HWT" required="false">
        <CONSTANT type="String" value="Waiting time 20s" devalue="Wartezeit 20s" envalue="Waiting time 20s">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="Pause" name="WARTEZEIT_30S" dename="WARTEZEIT_30S" enname="WAIT_30S">
       <COMMENT>30s Wartezeit

30s waiting time</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="30000">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="HWT" required="false">
        <CONSTANT type="String" value="Waiting time 30s" devalue="Wartezeit 30s" envalue="Waiting time 30s">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="Pause" name="WARTEZEIT_40S" dename="WARTEZEIT_40S" enname="WAIT_40S">
       <COMMENT>40s Wartezeit

40s waiting time</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="40000">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="HWT" required="false">
        <CONSTANT type="String" value="Waiting time 40s" devalue="Wartezeit 40s" envalue="Waiting time 40s">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="Pause" name="WARTEZEIT_60S" dename="WARTEZEIT_60S" enname="WAIT_60S">
       <COMMENT>60s Wartezeit

60s waiting time</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="60000">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="HWT" required="false">
        <CONSTANT type="String" value="Waiting time 60s" devalue="Wartezeit 60s" envalue="Waiting time 60s">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
     </GROUP>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
     <GROUP name="TEST_Handterminal">
      <COMMENT>TEST</COMMENT>
      <LOADPP fileheader="Pause" name="TEST_HT_WARTEZEIT_10S" dename="WARTEZEIT_10S" enname="WAIT_10S">
       <COMMENT>10s Wartezeit

10s waiting time</COMMENT>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="10000">
        </CONSTANT>
       </PPARGS>
       <PPARGS name="HWT" required="false">
        <CONSTANT type="String" value="Waiting time 10s" devalue="Wartezeit 10s" envalue="Waiting time 10s">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="WerkerFrage" name="TEST_HT_WF_SKITASCHE_BEILEGEN">
       <COMMENT></COMMENT>
       <PPARGS name="FT" required="true">
        <CONSTANT type="String" value="Ski-Tasche in Gep�ckraummulde beilegen!">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="DAUER" required="true">
        <CONSTANT type="String" value="360000">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="WerkerEingabe" name="TEST_HT_SCAN_AIRBAG_LABEL" dename="TEST_HT_SCAN_AIRBAG_LABEL" enname="TEST_SCAN_AIRBAG_LABEL">
       <COMMENT></COMMENT>
       <PPARGS name="AWT" required="true">
        <CONSTANT type="String" value="Please scan the airbag label!" devalue="Bitte das Airbaglabel scannen!" envalue="Please scan the airbag label!">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="MIN" required="false">
        <COMMENT></COMMENT>
        <CONSTANT type="String" value="6949923%">
         <COMMENT>Sachnummer (7-stellig) + Prozentzeichen</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="02694992301">
         <COMMENT>02+Sachnummer+AI01 (11-stellig)</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="02694992302">
         <COMMENT>02+Sachnummer+AI02 (11-stellig)</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="02694992303">
         <COMMENT>02+Sachnummer+AI03 (11-stellig)</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="02694992304">
         <COMMENT>02+Sachnummer+AI04 (11-stellig)</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="02694992305">
         <COMMENT>02+Sachnummer+AI05 (11-stellig)</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="02694992306">
         <COMMENT>02+Sachnummer+AI06 (11-stellig)</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="02694992307">
         <COMMENT>02+Sachnummer+AI07 (11-stellig)</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="02694992308">
         <COMMENT>02+Sachnummer+AI08 (11-stellig)</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="02694992309">
         <COMMENT>02+Sachnummer+AI09 (11-stellig)</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="02694992310">
         <COMMENT>02+Sachnummer+AI10 (11-stellig)</COMMENT>
        </CONSTANT>
        <CONSTANT type="String" value="026949923067">
         <COMMENT>02+Sachnummer+AI10 (11-stellig)</COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
      <LOADPP fileheader="WerkerQuittung" name="TEST_HT_WQ_FZG_VERLASSEN" dename="TEST_HT_WQ_FZG_VERLASSEN" enname="TEST_HT_WQ_LEAVE_CAR">
       <COMMENT></COMMENT>
       <PPARGS name="AWT" required="true">
        <CONSTANT type="String" value="Fahrzeug;verlassen" devalue="Fahrzeug;verlassen" envalue="please leave;the car">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
       <PPARGS name="STYLE" required="true">
        <CONSTANT type="String" value="3">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PPARGS>
      </LOADPP>
     </GROUP>
     <GROUP name="--------------------------------------------------------">
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="JOBS SPECIFIC PLANT W6">
   <COMMENT>Regensburg</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="startsWith" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="06">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="leer">
      <COMMENT></COMMENT>
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="JOBS SPECIFIC PLANT W7">
   <COMMENT>Leipzig</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="startsWith" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="07">
         <COMMENT></COMMENT>
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="leer">
      <COMMENT></COMMENT>
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="JOBS SPECIFIC PLANT W9">
   <COMMENT>S�dafrika</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="startsWith" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="09">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="leer">
      <COMMENT></COMMENT>
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="JOBS SPECIFIC PLANT W10">
   <COMMENT>Spartanburg</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="startsWith" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="10">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <IF>
      <COMMENT>F1x and F8x</COMMENT>
      <OR>
       <BOOL_METHOD>
        <COMMENT>X5</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F15">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>X6</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F16">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>Motorsport X5</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F85">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
       <BOOL_METHOD>
        <COMMENT>Motorsport X6</COMMENT>
        <METHOD name="getBaureihe" returntype="String">
         <METHOD name="equalsIgnoreCase" returntype="boolean">
          <PARAMS>
           <CONSTANT type="String" value="F86">
            <COMMENT></COMMENT>
           </CONSTANT>
          </PARAMS>
         </METHOD>
        </METHOD>
              <OPERATOR type="eq" optype="boolean"> </OPERATOR>
       <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
      </OR>
      <THEN>
       <LOADPP fileheader="WerkerQuittung" name="WQ_DOOR_CAP_REMINDER">
        <COMMENT></COMMENT>
        <PPARGS name="AWT" required="true">
         <CONSTANT type="String" value="Fahrzeug;verlassen" devalue="Fahrzeug;verlassen" envalue="Take Door Handle Key Caps from GloveBox. Place in Center Console.">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
        <PPARGS name="STYLE" required="true">
         <CONSTANT type="String" value="3">
          <COMMENT></COMMENT>
         </CONSTANT>
        </PPARGS>
       </LOADPP>
      </THEN>
     </IF>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="JOBS SPECIFIC PLANT W19.10">
   <COMMENT>Dadong</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="equalsIgnoreCase" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="19.10">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="leer">
      <COMMENT></COMMENT>
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="JOBS SPECIFIC PLANT W19.88">
   <COMMENT>Tiexi</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="equalsIgnoreCase" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="19.88">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="leer">
      <COMMENT></COMMENT>
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="JOBS SPECIFIC PLANT W34">
   <COMMENT>Oxford</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="startsWith" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="34">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <LOADPP fileheader="WerkerEingabe" name="PRUEFERNUMMER_EINGEBEN">
      <PPARGS name="AWT" required="true">
       <CONSTANT type="String" value="Bitte Pr�fernummer;einscannen!" devalue="Bitte Pr�fernummer;einscannen!" envalue="Please scan your;operator ID!">
        <COMMENT></COMMENT>
       </CONSTANT>
      </PPARGS>
     </LOADPP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
  <GROUP name="JOBS SPECIFIC PLANT W50">
   <COMMENT>Goodwood</COMMENT>
   <IF>
    <BOOL_METHOD>
     <METHOD name="getOrderWerk" returntype="String">
      <COMMENT></COMMENT>
      <METHOD name="startsWith" returntype="boolean">
       <COMMENT></COMMENT>
       <PARAMS>
        <CONSTANT type="String" value="50">
        </CONSTANT>
       </PARAMS>
      </METHOD>
     </METHOD>
        <OPERATOR type="eq" optype="boolean"> </OPERATOR>
    <CONSTANT type="boolean" value="true"> </CONSTANT>
</BOOL_METHOD>
    <THEN>
     <GROUP name="leer">
      <COMMENT></COMMENT>
     </GROUP>
    </THEN>
   </IF>
  </GROUP>
  <GROUP name="--------------------------------------------------------">
  </GROUP>
 </GROUP>
 <GROUP name="------------------------------------------------">
  <COMMENT></COMMENT>
 </GROUP>
</PRUEFPROZEDUREN>
 * PRUEFPROZEDURDEFINITION_XML END
 */	   // FOLGENDER CODE WIRD AUTOMATISCH NEU ERZEUGT,
	   // NICHT �NDERN
	   // PRUEFPROZEDURDEFINITION_CODE START
ppinit_1();
ppinit_2();
ppinit_243();
ppinit_244();
ppinit_356();
}
private void ppinit_1() throws PruefprozedurNotAvailableException {

}
private void ppinit_3() throws PruefprozedurNotAvailableException {

}
private void ppinit_4() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerAnweisung", "WA_FENSTER_NICHT_INIT", true );
	pp.setLocalName( "DE", "WA_FENSTER_NICHT_INIT" );
	pp.setLocalName( "EN", "WA_WINDOWS_NOT_INIT" );
	pp.setArgs( "DAUER" + "=" + "5000" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The windows can not initialize":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die Fenster k�nnen nicht initialisieren":"Die Fenster k�nnen nicht initialisieren")) );

}

}
private void ppinit_5() throws PruefprozedurNotAvailableException {

}
private void ppinit_6() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "GETRIEBE_P_NIO", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOBN_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "F" );
	pp.setArgs( "JOB1_HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"contact door driverside defect or gearbox not P":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"T�rkontakt Fahrer defekt oder Getriebe nicht in P":"T�rkontakt Fahrer defekt oder Getriebe nicht in P")) );

}

}
private void ppinit_7() throws PruefprozedurNotAvailableException {

}
private void ppinit_9() throws PruefprozedurNotAvailableException {

}
private void ppinit_11() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "FRONTSCHEIBENHEIZUNG_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}

}
private void ppinit_12() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "FRONTSCHEIBENHEIZUNG_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_10() throws PruefprozedurNotAvailableException {
if( ((auftrag.getBaureihe().equalsIgnoreCase("F54") || auftrag.getBaureihe().equalsIgnoreCase("F55") || auftrag.getBaureihe().equalsIgnoreCase("F56") || auftrag.getBaureihe().equalsIgnoreCase("F57") || auftrag.getBaureihe().equalsIgnoreCase("F60") || auftrag.getBaureihe().equalsIgnoreCase("F61")) && auftrag.containsSA("0359")))
ppinit_11();
else
ppinit_12();

}
private void ppinit_13() throws PruefprozedurNotAvailableException {

}
private void ppinit_15() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "PTC_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}

}
private void ppinit_16() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "PTC_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_14() throws PruefprozedurNotAvailableException {
if( (auftrag.getAntriebsKonzeption().equalsIgnoreCase("D") || (auftrag.getBaureihe().equalsIgnoreCase("G12") && auftrag.containsSA("04NB") && !(auftrag.getHybrid().equalsIgnoreCase("PHEV")))))
ppinit_15();
else
ppinit_16();

}
private void ppinit_17() throws PruefprozedurNotAvailableException {

}
private void ppinit_18() throws PruefprozedurNotAvailableException {

}
private void ppinit_19() throws PruefprozedurNotAvailableException {

}
private void ppinit_21() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagToleranz", "STEUERN_EFII_WORKAROUND", true );
	pp.setArgs( "SGBD" + "=" + "AEP" );
	pp.setArgs( "JOB" + "=" + "STEUERN_VERBRAUCHERSTROM_EFII" );
	pp.setArgs( "JOBPAR" + "=" + "1000" + ";" +"0" + ";" +"200" + ";" +"100" + ";" +"80000" + ";" +"100000" + ";" +"0" + ";" +"30" + ";" +"20" + ";" +"10" + ";" +"400" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Rearwindow heating;Activation EFII":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Heckscheibenheizung;Aktivierung EFII":"Heckscheibenheizung;Aktivierung EFII")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "WAIT_100MS_EFII_WORKAROUND", true );
	pp.setArgs( "DAUER" + "=" + "100" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "DiagToleranz", "STOP_EFII_WORKAROUND", true );
	pp.setArgs( "SGBD" + "=" + "AEP" );
	pp.setArgs( "JOB" + "=" + "STEUERN_ENDE_VERBRAUCHERSTROM_EFII" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Current measure;STOP EFII":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Strommessung;STOP EFII":"Strommessung;STOP EFII")) );

}

}
private void ppinit_20() throws PruefprozedurNotAvailableException {
if( ((auftrag.getBaureihe().equalsIgnoreCase("F39") || auftrag.getBaureihe().equalsIgnoreCase("F45") || auftrag.getBaureihe().equalsIgnoreCase("F46") || auftrag.getBaureihe().equalsIgnoreCase("F48") || auftrag.getBaureihe().equalsIgnoreCase("F49") || auftrag.getBaureihe().startsWith("F5") || auftrag.getBaureihe().startsWith("F6") || auftrag.getBaureihe().equalsIgnoreCase("M13")) && (auftrag.getZeitkriteriumMonatInt()==3 && auftrag.getZeitkriteriumJahrInt()==18) && auftrag.getAntriebsKonzeption().equalsIgnoreCase("D")))
ppinit_21();

}
private void ppinit_8() throws PruefprozedurNotAvailableException {
ppinit_9();
ppinit_10();
ppinit_13();
ppinit_14();
ppinit_17();
ppinit_18();
ppinit_19();
ppinit_20();

}
private void ppinit_22() throws PruefprozedurNotAvailableException {

}
private void ppinit_24() throws PruefprozedurNotAvailableException {

}
private void ppinit_25() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "UiLogStart", "START_TRANSIENT_AUFZEICHNUNG", true );
	pp.setArgs( "MAX_LOG" + "=" + "1800" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "UiLogEnd", "STOP_TRANSIENT_AUFZEICHNUNG", true );

}

}
private void ppinit_26() throws PruefprozedurNotAvailableException {

}
private void ppinit_27() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "UiInit", "UI_INIT", true );
	pp.setArgs( "PROBE_MESSAGE" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"CLASP PROBE ARROUND;BLACK BATTERY CABLE":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"MASSELEITUNG;UMSCHLIESSEN!":"MASSELEITUNG;UMSCHLIESSEN!")) );
	pp.setArgs( "MIN_I" + "=" + "-1000" );
	pp.setArgs( "MAX_I" + "=" + "1000" );
	pp.setArgs( "MIN_U" + "=" + "8000" );
	pp.setArgs( "MAX_U" + "=" + "15000" );

}

}
private void ppinit_28() throws PruefprozedurNotAvailableException {

}
private void ppinit_29() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "UiToleranzTotal", "UI_RUHESTROMMESSUNG", true );
	pp.setLocalName( "DE", "UI_RUHESTROMMESSUNG" );
	pp.setLocalName( "EN", "UI_QUIESCENT_CURRENT_MEASUREMENT" );
	pp.setArgs( "RESULT1" + "=" + "I" );
	pp.setArgs( "MIN_GET1" + "=" + "0" );
	pp.setArgs( "MAX_GET1" + "=" + "80" );
	pp.setArgs( "DAUER" + "=" + "30000" );
	pp.setArgs( "TRIGGER_FILTER" + "=" + "50" );
	pp.setArgs( "MESS_FILTER" + "=" + "50" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Sleep Current Measurement":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Ruhestrommessung":"Sleep Current Measurement")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Sleep Current Measurement;Failure":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Ruhestrommessung;Fehler!":"Sleep Current Measurement;Failure")) );
	pp.setArgs( "PUNKT" + "=" + "500" );
	pp.setArgs( "MODE" + "=" + "3" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_SLEEP_CURRENT_MEASURED_BY_CURRENT_PROBE", true );
	pp.setLocalName( "DE", "WQ_RUHESTROM_GEMESSEN_MIT_STROMMESSZANGE" );
	pp.setLocalName( "EN", "WQ_SLEEP_CURRENT_MEASURED_BY_CURRENT_PROBE" );
	pp.setArgs( "AWT" + "=" + "Ruhestrom laut SMZ:;I(mA)@UI_RUHESTROMMESSUNG; mA" );
	pp.setArgs( "STYLE" + "=" + "1" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "UiToleranzTotal", "UI_RUHESTROMMESSUNG_START", true );
	pp.setLocalName( "DE", "UI_RUHESTROMMESSUNG_START" );
	pp.setLocalName( "EN", "UI_QUIESCENT_CURRENT_MEASUREMENT_START" );
	pp.setArgs( "RESULT1" + "=" + "I" );
	pp.setArgs( "MIN_GET1" + "=" + "0" );
	pp.setArgs( "MAX_GET1" + "=" + "80" );
	pp.setArgs( "DAUER" + "=" + "3000" );
	pp.setArgs( "TRIGGER_FILTER" + "=" + "50" );
	pp.setArgs( "MESS_FILTER" + "=" + "50" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Sleep Current Measurement;START":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Ruhestrommessung;START":"Sleep Current Measurement;START")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Sleep Current Measurement;Failure":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Ruhestrommessung;Fehler!":"Sleep Current Measurement;Failure")) );
	pp.setArgs( "WAIT" + "=" + "50" );
	pp.setArgs( "PUNKT" + "=" + "20" );
	pp.setArgs( "MODE" + "=" + "3" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "StatisticDecide", "STICHPROBE_RUHESTROM_ERGEBNIS", true );
	pp.setLocalName( "DE", "STICHPROBE_RUHESTROM_ERGEBNIS" );
	pp.setLocalName( "EN", "SAMPLE_TEST_QUIESCENT_CURRENT_MEASUREMENT_RESULT" );
	pp.setArgs( "MODE" + "=" + "TEST_IO" );
	pp.setArgs( "ID" + "=" + "RUHESTROM" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "StatisticDecide", "STICHPROBE_RUHESTROM_DURCHFUEHREN", true );
	pp.setLocalName( "DE", "STICHPROBE_RUHESTROM_DURCHFUEHREN" );
	pp.setLocalName( "EN", "SAMPLE_TEST_QUIESCENT_CURRENT_MEASUREMENT_RUN" );
	pp.setArgs( "MODE" + "=" + "DECIDE" );
	pp.setArgs( "ID" + "=" + "RUHESTROM" );
	pp.setArgs( "QUOTE" + "=" + "50" );
	pp.setArgs( "TESTS_AFTER_ERROR" + "=" + "1" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "DiagToleranz", "STEUERN_SLEEPMODE", true );
	pp.setLocalName( "DE", "STEUERN_SCHLAFMODUS" );
	pp.setLocalName( "EN", "STEUERN_SLEEPMODE" );
	pp.setArgs( "SGBD" + "=" + "F01" );
	pp.setArgs( "JOB" + "=" + "SLEEP_MODE_FUNKTIONAL" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Job is still running!;Please wait!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Jobausf�hrung l�uft!;Bitte warten!":"Jobausf�hrung l�uft!;Bitte warten!")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "CombineTestStates", "STATUS_DECISION_RUHESTROMMESSUNG", true );
	pp.setLocalName( "DE", "STATUS_ENTSCHEIDUNG_RUHESTROMMESSUNG" );
	pp.setLocalName( "EN", "STATUS_DECISION_QUIESCENT_CURRENT_MEASUREMENT" );
	pp.setArgs( "STEP1" + "=" + "VORSERIE" );
	pp.setArgs( "STEP2" + "=" + "PU_STEUERUNG.VAR_ECOS_LINIE" );
	pp.setArgs( "COMBINATION" + "=" + "MIN_IO_NUMBER" + ";" +"2" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "UiToleranzTotal", "WAIT_UI_RUHESTROM", true );
	pp.setArgs( "RESULT1" + "=" + "I" );
	pp.setArgs( "MIN_GET1" + "=" + "0" );
	pp.setArgs( "MAX_GET1" + "=" + "80" );
	pp.setArgs( "DAUER" + "=" + "30000" );
	pp.setArgs( "TRIGGER_FILTER" + "=" + "50" );
	pp.setArgs( "MESS_FILTER" + "=" + "50" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Please wait":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Bitte warten":"Sleep Current Measurement")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Time expired":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Zeit abgelaufen":"Sleep Current Measurement ; Failure")) );
	pp.setArgs( "PUNKT" + "=" + "100" );
	pp.setArgs( "MODE" + "=" + "3" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_RUHESTROM_GEMESSEN_MIT_SMZ", true );
	pp.setArgs( "AWT" + "=" + "Ruhestrom laut SMZ:" + ";" +"I@ECOS.UI_RUHESTROMMESSUNG_START" );
	pp.setArgs( "STYLE" + "=" + "1" );

}

}
private void ppinit_30() throws PruefprozedurNotAvailableException {

}
private void ppinit_23() throws PruefprozedurNotAvailableException {
ppinit_24();
ppinit_25();
ppinit_26();
ppinit_27();
ppinit_28();
ppinit_29();
ppinit_30();

}
private void ppinit_31() throws PruefprozedurNotAvailableException {

}
private void ppinit_32() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "WARTEZEIT_1S", true );
	pp.setArgs( "DAUER" + "=" + "1000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Waiting time 1s":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Wartezeit 1s":"Waiting time 1s")) );

}

}
private void ppinit_33() throws PruefprozedurNotAvailableException {

}
private void ppinit_35() throws PruefprozedurNotAvailableException {

}
private void ppinit_36() throws PruefprozedurNotAvailableException {

}
private void ppinit_37() throws PruefprozedurNotAvailableException {

}
private void ppinit_39() throws PruefprozedurNotAvailableException {

}
private void ppinit_41() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerEingabe", "SCAN_AIRBAG_LABEL", true );
	pp.setLocalName( "DE", "SCAN_AIRBAG_LABEL" );
	pp.setLocalName( "EN", "SCAN_AIRBAG_LABEL" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Please scan the airbag label!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Bitte das Airbaglabel scannen!":"Please scan the airbag label!")) );
	pp.setArgs( "MIN" + "=" + "6949923%" + ";" +"02694992301" + ";" +"02694992302" + ";" +"02694992303" + ";" +"02694992304" + ";" +"02694992305" + ";" +"02694992306" + ";" +"02694992307" + ";" +"02694992308" + ";" +"02694992309" + ";" +"02694992310" + ";" +"026949923067" );

}

}
private void ppinit_40() throws PruefprozedurNotAvailableException {
if( (auftrag.getL�nderVariante().startsWith("US") && !(auftrag.containsSA("0838")) && !((auftrag.getBaureihe().equalsIgnoreCase("F22") && auftrag.containsSA("09ML")))))
ppinit_41();

}
private void ppinit_42() throws PruefprozedurNotAvailableException {

}
private void ppinit_38() throws PruefprozedurNotAvailableException {
ppinit_39();
ppinit_40();
ppinit_42();

}
private void ppinit_43() throws PruefprozedurNotAvailableException {

}
private void ppinit_45() throws PruefprozedurNotAvailableException {

}
private void ppinit_48() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_MITTELKONSOLE_VORN", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_MITTELKONSOLE_VORN" );
	pp.setLocalName( "EN", "WF_POWER_SOCKET_CENTER_CONSOLE_FRONT" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket;center console front ok?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose;Mittelkonsole Vorn iO?":"Steckdose;Mittelkonsole Vorn iO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket;center console front NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose;Mittelkonsole Vorn NOK":"Power socket;center console front NOK")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_STECKDOSE_MITTELKONSOLE_VORN_VORHANDEN", true );
	pp.setLocalName( "DE", "ZIGARETTENANZUENDER_STECKDOSE_MITTELKONSOLE_VORN_VORHANDEN" );
	pp.setLocalName( "EN", "CIGARETTE_LIGHTER_CENTER_CONSOLE_FRONT_AVAILABLE" );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_49() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_CENTERSTACK", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_CENTERSTACK" );
	pp.setLocalName( "EN", "WF_POWER_SOCKET_CENTERSTACK" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket;front middle console ok?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose;Mittelkon. vorn iO?":"Steckdose;Mittelkon. vorn iO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket;front middle console NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose;Mittelkon. vorn NOK":"Power socket;front middle console NOK")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_CENTERSTACK_VORHANDEN", true );
	pp.setLocalName( "DE", "ZIGARETTENANZUENDER_CENTERSTACK_VORHANDEN" );
	pp.setLocalName( "EN", "CIGARETTE_LIGHTER_REAR_CENTER_CONSOLE_MIDDLE_AVAILABLE" );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_MITTELKONSOLE", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_MITTELKONSOLE" );
	pp.setLocalName( "EN", "WF_POWER_SOCKET_CENTER_CONSOLE" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket;front middle console ok?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose;Mittelkon. vorn iO?":"Steckdose;Mittelkon. vorn iO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_MITTELKONSOLE_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_47() throws PruefprozedurNotAvailableException {
if( auftrag.getIStufe().startsWith("S18"))
ppinit_48();
else
ppinit_49();

}
private void ppinit_51() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_STECKDOSE_MITTELKONSOLE_VORN_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}

}
private void ppinit_52() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_MITTELKONSOLE_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}

}
private void ppinit_53() throws PruefprozedurNotAvailableException {

}
private void ppinit_50() throws PruefprozedurNotAvailableException {
if( auftrag.getIStufe().startsWith("S18"))
ppinit_51();
else
ppinit_52();
ppinit_53();

}
private void ppinit_55() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_MITTELARMLEHNE_VORN", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_MITTELARMLEHNE_VORN" );
	pp.setLocalName( "EN", "WF_POWER_SOCKET_CENTER_ARM_REST_FRONT" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket;center arm rest front ok?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose; Mittelarmlehne vorn iO?":"Steckdose; Mittelarmlehne vorn iO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket;center arm rest front NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose; Mittelarmlehne vorn NOK":"Power socket;center arm rest front NOK")) );

}

}
private void ppinit_56() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_MITTELARMLEHNE_ABLAGEFACH", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_MITTELARMLEHNE_ABLAGEFACH" );
	pp.setLocalName( "EN", "WF_POWER_SOCKET_CENTER_ARMREST" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket;center armrest ok?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose MAL;Ablagefach iO?":"Steckdose MAL;Ablagefach iO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket;center armrest NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose MAL;Ablagefach NOK":"Power socket;center armrest NOK")) );

}

}
private void ppinit_54() throws PruefprozedurNotAvailableException {
if( auftrag.getIStufe().startsWith("S18"))
ppinit_55();
else
ppinit_56();

}
private void ppinit_58() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_MITTELKONSOLE_HINTEN_RECHTS", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_MITTELKONSOLE_FOND_RECHTS" );
	pp.setLocalName( "EN", "WF_POWER_SOCKET_CENTER_CONSOLE_REAR_RIGHT_SIDE" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power sockets right side; center console rear right sideok?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Rechte Steckdosen;Mittelk. hinten rechts iO?":"Rechte Steckdosen;Mittelk. hinten rechts iO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power sockets right side; center console rear right side NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Rechte Steckdosen;Mittelk. hinten rechts NOK":"Power sockets right side; center console rear right side NOK")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_MITTELKONSOLE_HINTEN_RECHTS_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_59() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_MITTELKONSOLE_FOND_RECHTS", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_MITTELKONSOLE_FOND_RECHTS" );
	pp.setLocalName( "EN", "WF_REAR_POWER_SOCKET_CENTER_CONSOLE_RIGHT_SIDE" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power sockets right side; rear middle console ok?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Rechte Steckdosen;Mittelk. hinten iO?":"Rechte Steckdosen;Mittelk. hinten iO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power sockets right side; rear middle console NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Rechte Steckdosen;Mittelk. hinten NOK":"Power sockets right side; rear middle console NOK")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_MITTELKONSOLE_FOND_RECHTS_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_57() throws PruefprozedurNotAvailableException {
if( auftrag.getIStufe().startsWith("S18"))
ppinit_58();
else
ppinit_59();

}
private void ppinit_62() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_MITTELKONSOLE_HINTEN_RECHTS_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}

}
private void ppinit_63() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_MITTELKONSOLE_FOND_RECHTS_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}

}
private void ppinit_61() throws PruefprozedurNotAvailableException {
if( auftrag.getIStufe().startsWith("S18"))
ppinit_62();
else
ppinit_63();

}
private void ppinit_64() throws PruefprozedurNotAvailableException {

}
private void ppinit_60() throws PruefprozedurNotAvailableException {
if( (auftrag.getBaureihe().equalsIgnoreCase("G07") && auftrag.containsSA("0441")))
ppinit_61();
ppinit_64();

}
private void ppinit_66() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_MITTELKONSOLE_HINTEN_LINKS", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_MITTELKONSOLE_HINTEN_LINKS" );
	pp.setLocalName( "EN", "WF_POWER_SOCKET_CENTER_CONSOLE_REAR_LEFT_SIDE" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power sockets left side; center console rear left side ok?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Linke Steckdosen;Mittelk. hinten links iO?":"Linke Steckdosen;Mittelk. hinten links iO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power sockets left side; center console rear left side NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Linke Steckdosen;Mittelk. hinten links NOK":"Power sockets left side; center console rear left side NOK")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_MITTELKONSOLE_HINTEN_LINKS_VORHANDEN", true );
	pp.setLocalName( "DE", "ZIGARETTENANZUENDER_MITTELKONSOLE_HINTEN_LINKS_VORHANDEN" );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_67() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_MITTELKONSOLE_FOND_LINKS", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_MITTELKONSOLE_FOND_LINKS" );
	pp.setLocalName( "EN", "WF_REAR_POWER_SOCKET_CENTER_CONSOLE_LEFT_SIDE" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power sockets left side; rear middle console ok?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Linke Steckdosen;Mittelk. hinten iO?":"Linke Steckdosen;Mittelk. hinten iO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power sockets left side; rear middle console NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Linke Steckdosen;Mittelk. hinten NOK":"Power sockets left side; rear middle console NOK")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_MITTELKONSOLE_FOND_LINKS_VORHANDEN", true );
	pp.setLocalName( "DE", "ZIGARETTENANZUENDER_MITTELKONSOLE_FOND_LINKS_VORHANDEN" );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_65() throws PruefprozedurNotAvailableException {
if( auftrag.getIStufe().startsWith("S18"))
ppinit_66();
else
ppinit_67();

}
private void ppinit_70() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_MITTELKONSOLE_HINTEN_LINKS_VORHANDEN", true );
	pp.setLocalName( "DE", "ZIGARETTENANZUENDER_MITTELKONSOLE_HINTEN_LINKS_VORHANDEN" );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}

}
private void ppinit_71() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_MITTELKONSOLE_FOND_LINKS_VORHANDEN", true );
	pp.setLocalName( "DE", "ZIGARETTENANZUENDER_MITTELKONSOLE_FOND_LINKS_VORHANDEN" );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}

}
private void ppinit_69() throws PruefprozedurNotAvailableException {
if( auftrag.getIStufe().startsWith("S18"))
ppinit_70();
else
ppinit_71();

}
private void ppinit_72() throws PruefprozedurNotAvailableException {

}
private void ppinit_68() throws PruefprozedurNotAvailableException {
if( (auftrag.getBaureihe().equalsIgnoreCase("G38") || ((auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12")) && auftrag.containsSA("0441") && !(auftrag.containsSA("04F5"))) || ((auftrag.getBaureihe().equalsIgnoreCase("RR5") || auftrag.getBaureihe().equalsIgnoreCase("RR6")) && auftrag.containsSA("0441"))))
ppinit_69();
ppinit_72();

}
private void ppinit_74() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_MITTELKONSOLE_HINTEN_MITTE", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_MITTELKONSOLE_HINTEN_MITTE" );
	pp.setLocalName( "EN", "WF_POWER_SOCKET_CENTER_CONSOLE_REAR_MIDDLE" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket;center console rear middle ok?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdosen;Mittelk. hinten mitte iO?":"Steckdosen;Mittelk. hinten mitte iO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket;center console rear middle NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdosen;Mittelk. hinten mitte NOK":"Power socket;center console rear middle NOK")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZ�NDER_MITTELKONSOLE_HINTEN_MITTE_VORHANDEN", true );
	pp.setLocalName( "DE", "ZIGARETTENANZ�NDER_MITTELKONSOLE_HINTEN_MITTE_VORHANDEN" );
	pp.setLocalName( "EN", "CIGARETTE_LIGHTER_REAR_CENTER_CONSOLE_MIDDLE_AVAILABLE" );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_75() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_MITTELKONSOLE_FOND_MITTE", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_MITTELKONSOLE_FOND_MITTE" );
	pp.setLocalName( "EN", "WF_REAR_POWER_SOCKET_CENTER_CONSOLE_MIDDLE" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket;rear middle console ok?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdosen;Mittelk. hinten iO?":"Steckdosen;Mittelk. hinten iO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket;rear middle console NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdosen;Mittelk. hinten NOK":"Power socket;rear middle console NOK")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZ�NDER_MITTELKONSOLE_FOND_MITTE_VORHANDEN", true );
	pp.setLocalName( "DE", "ZIGARETTENANZ�NDER_MITTELKONSOLE_FOND_MITTE_VORHANDEN" );
	pp.setLocalName( "EN", "CIGARETTE_LIGHTER_REAR_CENTER_CONSOLE_MIDDLE_AVAILABLE" );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_73() throws PruefprozedurNotAvailableException {
if( auftrag.getIStufe().startsWith("S18"))
ppinit_74();
else
ppinit_75();

}
private void ppinit_78() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZ�NDER_MITTELKONSOLE_HINTEN_MITTE_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}

}
private void ppinit_79() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZ�NDER_MITTELKONSOLE_FOND_MITTE_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}

}
private void ppinit_77() throws PruefprozedurNotAvailableException {
if( auftrag.getIStufe().startsWith("S18"))
ppinit_78();
else
ppinit_79();

}
private void ppinit_81() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZ�NDER_MITTELKONSOLE_HINTEN_MITTE_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_82() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZ�NDER_MITTELKONSOLE_FOND_MITTE_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_80() throws PruefprozedurNotAvailableException {
if( auftrag.getIStufe().startsWith("S18"))
ppinit_81();
else
ppinit_82();

}
private void ppinit_83() throws PruefprozedurNotAvailableException {

}
private void ppinit_76() throws PruefprozedurNotAvailableException {
if( (((auftrag.getBaureihe().equalsIgnoreCase("F95") || auftrag.getBaureihe().equalsIgnoreCase("F96") || auftrag.getBaureihe().equalsIgnoreCase("G05") || auftrag.getBaureihe().equalsIgnoreCase("G06")) && auftrag.containsSA("0441")) || ((auftrag.getBaureihe().equalsIgnoreCase("F97") || auftrag.getBaureihe().equalsIgnoreCase("F98") || auftrag.getBaureihe().equalsIgnoreCase("G01") || auftrag.getBaureihe().equalsIgnoreCase("G02") || auftrag.getBaureihe().equalsIgnoreCase("G08") || auftrag.getBaureihe().equalsIgnoreCase("G20") || auftrag.getBaureihe().equalsIgnoreCase("G21")) && auftrag.containsSA("0441") && auftrag.containsSA("0493")) || auftrag.getBaureihe().equalsIgnoreCase("G28")))
ppinit_77();
else
ppinit_80();
ppinit_83();

}
private void ppinit_85() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_MITTELONSOLE_HINTEN_CUP_HOLDER", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_MITTELONSOLE_HINTEN_CUP_HOLDER" );
	pp.setLocalName( "EN", "WF_POWER_SOCKET_CENTER_CONSOLE_REAR_CUP_HOLDER" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket; center console rear Cup Holder ok?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdosen Mittelkonsole Hinten;Cup-Holder Mitte iO?":"Steckdosen Mittelkonsole Hinten;Cup-Holder Mitte iO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket; center console rear Cup Holder NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdosen Mittelkonsole Hinten;Cup-Holder Mitte NOK":"Power socket; center console rear Cup Holder NOK")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_EXECUTIVE_LOUNG_MITTELKONSOLE_HINTEN_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_86() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_FONDKONSOLE_CUP_HOLDER", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_FONDKONSOLE_CUP_HOLDER" );
	pp.setLocalName( "EN", "WF_FOND_POWER_SOCKET_CENTER_CONSOLE_CUP_HOLDER" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket fondkonsole;Cup Holder ok?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdosen Fondkonsole;Cup-Holder Mitte iO?":"Steckdosen Fondkonsole;Cup-Holder Mitte iO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket fondkonsole;Cup Holder NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdosen Fondkonsole;Cup-Holder Mitte NOK":"Power socket fondkonsole;Cup Holder NOK")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_EXECUTIVE_LOUNG_FONDKONSOLE_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_84() throws PruefprozedurNotAvailableException {
if( auftrag.getIStufe().startsWith("S18"))
ppinit_85();
else
ppinit_86();

}
private void ppinit_89() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_EXECUTIVE_LOUNG_MITTELKONSOLE_HINTEN_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}

}
private void ppinit_90() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_EXECUTIVE_LOUNG_FONDKONSOLE_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}

}
private void ppinit_88() throws PruefprozedurNotAvailableException {
if( auftrag.getIStufe().startsWith("S18"))
ppinit_89();
else
ppinit_90();

}
private void ppinit_92() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_EXECUTIVE_LOUNG_MITTELKONSOLE_HINTEN_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_93() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_EXECUTIVE_LOUNG_FONDKONSOLE_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_91() throws PruefprozedurNotAvailableException {
if( auftrag.getIStufe().startsWith("S18"))
ppinit_92();
else
ppinit_93();

}
private void ppinit_94() throws PruefprozedurNotAvailableException {

}
private void ppinit_87() throws PruefprozedurNotAvailableException {
if( ((auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12")) && auftrag.containsSA("04F5") && auftrag.containsSA("0441")))
ppinit_88();
else
ppinit_91();
ppinit_94();

}
private void ppinit_96() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_MITTELARMLEHNE_HINTEN", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_MITTELARMLEHNE_HINTEN" );
	pp.setLocalName( "EN", "WF_POWER_SOCKET_CENTER_ARM_REST_REAR" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket;center arm rest rear ok?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose Mittelarmlehne hinten iO?":"Power socket storage compartment;center console rear ok?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket;center arm rest rear NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose Mittelarmlehne hinten NOK":"Power socket;center arm rest rear NOK")) );

}

}
private void ppinit_97() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_MITTELKONSOLE_ABLAGEFACH_FOND", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_MITTELKONSOLE_ABLAGEFACH_FOND" );
	pp.setLocalName( "EN", "WF_POWER_SOCKET_CENTER_CONSOLE_STORAGE_COMPARTMENT_REAR" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket storage compartment;center console rear ok?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose Mittelk.;Ablagefach hinten iO?":"Power socket storage compartment;center console rear ok?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket storage compartment;center console rear NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose Mittelk.;Ablagefach hinten NOK":"Power socket storage compartment;center console rear NOK")) );

}

}
private void ppinit_95() throws PruefprozedurNotAvailableException {
if( auftrag.getIStufe().startsWith("S18"))
ppinit_96();
else
ppinit_97();

}
private void ppinit_98() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_GEPAECKRAUM", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_GEPAECKRAUM" );
	pp.setLocalName( "EN", "WF_POWER_SOCKET_BOOT" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket; Luggage bay;light on tester on?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose;Gep�ckraum iO?":"Steckdose;Gep�ckraum iO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Power socket; Luggage bay;light NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose;Gep�ckraum NOK":"Power socket; Luggage bay;light NOK")) );

}

}
private void ppinit_99() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_TUER_BEIFAHRER_HINTEN", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_TUER_BEIFAHRER_HINTEN" );
	pp.setLocalName( "EN", "WF_CIGARETTE_LIGHTER_DOOR_PASSENGER_REAR" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Cigarette lighter door passenger rear okay?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose T�r Beifahrer hinten iO?":"Steckdose T�r Beifahrer hinten iO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Cigarette lighter door passenger rear NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose T�r Beifahrer hinten NOK":"Cigarette lighter door passenger rear NOK")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_TUER_BEIFAHRER_HINTEN", true );
	pp.setLocalName( "DE", "ZIGARETTENANZUENDER_TUER_BEIFAHRER_HINTEN" );
	pp.setLocalName( "EN", "CIGARETTE_LIGHTER_DOOR_PASSENGER_REAR" );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_101() throws PruefprozedurNotAvailableException {

}
private void ppinit_100() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_TUER_BEIFAHRER_HINTEN", true );
	pp.setLocalName( "DE", "ZIGARETTENANZUENDER_TUER_BEIFAHRER_HINTEN" );
	pp.setLocalName( "EN", "CIGARETTE_LIGHTER_DOOR_PASSENGER_REAR" );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}
ppinit_101();

}
private void ppinit_102() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_STECKDOSE_TUER_FAHRER_HINTEN", true );
	pp.setLocalName( "DE", "WF_STECKDOSE_TUER_FAHRER_HINTEN" );
	pp.setLocalName( "EN", "WF_CIGARETTE_LIGHTER_DOOR_DRIVER_REAR" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Cigarette lighter door driver rear okay?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose T�r Fahrer hinten iO?":"Steckdose T�r Fahrer hinten iO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Cigarette lighter door driver rear NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Steckdose T�r Fahrer hinten NOK":"Cigarette lighter door driver rear NOK")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_TUER_FAHRER_HINTEN", true );
	pp.setLocalName( "DE", "ZIGARETTENANZUENDER_TUER_FAHRER_HINTEN" );
	pp.setLocalName( "EN", "CIGARETTE_LIGHTER_DOOR_DRIVER_REAR" );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_104() throws PruefprozedurNotAvailableException {

}
private void ppinit_103() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ZIGARETTENANZUENDER_TUER_FAHRER_HINTEN", true );
	pp.setLocalName( "DE", "ZIGARETTENANZUENDER_TUER_FAHRER_HINTEN" );
	pp.setLocalName( "EN", "CIGARETTE_LIGHTER_DOOR_DRIVER_REAR" );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}
ppinit_104();

}
private void ppinit_46() throws PruefprozedurNotAvailableException {
if( (!(((auftrag.getBaureihe().equalsIgnoreCase("G05") || auftrag.getBaureihe().equalsIgnoreCase("G06") || auftrag.getBaureihe().equalsIgnoreCase("G07")) && auftrag.containsSA("0442"))) && !(auftrag.containsSA("0441")) && !(auftrag.containsSA("0146")) && !(auftrag.getBaureihe().equalsIgnoreCase("F44")) && !(auftrag.getBaureihe().equalsIgnoreCase("G08")) && !(auftrag.getBaureihe().equalsIgnoreCase("G28")) && !(auftrag.getBaureihe().equalsIgnoreCase("G38"))))
ppinit_47();
else
ppinit_50();
if( ((auftrag.getBaureihe().equalsIgnoreCase("F90") || auftrag.getBaureihe().equalsIgnoreCase("F91") || auftrag.getBaureihe().equalsIgnoreCase("F92") || auftrag.getBaureihe().equalsIgnoreCase("F93") || auftrag.getBaureihe().equalsIgnoreCase("F95") || auftrag.getBaureihe().equalsIgnoreCase("F96") || auftrag.getBaureihe().equalsIgnoreCase("G05") || auftrag.getBaureihe().equalsIgnoreCase("G06") || auftrag.getBaureihe().equalsIgnoreCase("G07") || auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12") || auftrag.getBaureihe().equalsIgnoreCase("G14") || auftrag.getBaureihe().equalsIgnoreCase("G15") || auftrag.getBaureihe().equalsIgnoreCase("G16") || auftrag.getBaureihe().equalsIgnoreCase("G32") || auftrag.getBaureihe().equalsIgnoreCase("G38") || ((auftrag.getBaureihe().equalsIgnoreCase("G30") || auftrag.getBaureihe().equalsIgnoreCase("G31")) && !(auftrag.containsSA("0146")))) || (auftrag.getBaureihe().equalsIgnoreCase("RR4") || auftrag.getBaureihe().equalsIgnoreCase("RR5") || auftrag.getBaureihe().equalsIgnoreCase("RR6") || auftrag.getBaureihe().equalsIgnoreCase("RR11") || auftrag.getBaureihe().equalsIgnoreCase("RR12") || auftrag.getBaureihe().equalsIgnoreCase("RR31"))))
ppinit_54();
if( (((auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12")) && ((auftrag.getIStufeMonatInt()<3 && auftrag.getIStufeJahrInt()==19) || auftrag.getIStufeJahrInt()<19) && !(auftrag.containsSA("04F5"))) || ((auftrag.getBaureihe().equalsIgnoreCase("F90") || auftrag.getBaureihe().equalsIgnoreCase("F93") || auftrag.getBaureihe().equalsIgnoreCase("G16") || auftrag.getBaureihe().equalsIgnoreCase("G30") || auftrag.getBaureihe().equalsIgnoreCase("G31")) && (auftrag.containsSA("0575") || auftrag.containsSA("06FH")) && !(auftrag.containsSA("0146"))) || (auftrag.getBaureihe().equalsIgnoreCase("G28") || auftrag.getBaureihe().equalsIgnoreCase("G32") || auftrag.getBaureihe().equalsIgnoreCase("G38")) || (auftrag.getBaureihe().equalsIgnoreCase("RR5") || auftrag.getBaureihe().equalsIgnoreCase("RR6") || ((auftrag.getBaureihe().equalsIgnoreCase("RR11") || auftrag.getBaureihe().equalsIgnoreCase("RR12") || auftrag.getBaureihe().equalsIgnoreCase("RR31")) && auftrag.containsSA("0RSF")))))
ppinit_57();
else
ppinit_60();
if( (auftrag.getBaureihe().equalsIgnoreCase("G32") || (auftrag.getBaureihe().equalsIgnoreCase("G07") && !(auftrag.containsSA("0441"))) || ((auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12")) && !(auftrag.containsSA("0441")) && !(auftrag.containsSA("04F5"))) || ((auftrag.getBaureihe().equalsIgnoreCase("F90") || auftrag.getBaureihe().equalsIgnoreCase("F93") || auftrag.getBaureihe().equalsIgnoreCase("G16") || auftrag.getBaureihe().equalsIgnoreCase("G30") || auftrag.getBaureihe().equalsIgnoreCase("G31")) && (auftrag.containsSA("0575") || auftrag.containsSA("06FH")) && !(auftrag.containsSA("0146"))) || (((auftrag.getBaureihe().equalsIgnoreCase("RR5") || auftrag.getBaureihe().equalsIgnoreCase("RR6")) && !(auftrag.containsSA("0441"))) || ((auftrag.getBaureihe().equalsIgnoreCase("RR11") || auftrag.getBaureihe().equalsIgnoreCase("RR12") || auftrag.getBaureihe().equalsIgnoreCase("RR31")) && auftrag.containsSA("0RSF")))))
ppinit_65();
else
ppinit_68();
if( (auftrag.getBaureihe().equalsIgnoreCase("G08") || ((auftrag.getBaureihe().equalsIgnoreCase("F95") || auftrag.getBaureihe().equalsIgnoreCase("F96") || auftrag.getBaureihe().equalsIgnoreCase("G05") || auftrag.getBaureihe().equalsIgnoreCase("G06")) && !(auftrag.containsSA("0441"))) || ((auftrag.getBaureihe().equalsIgnoreCase("F90") || auftrag.getBaureihe().equalsIgnoreCase("F93") || auftrag.getBaureihe().equalsIgnoreCase("G16") || auftrag.getBaureihe().equalsIgnoreCase("G30") || auftrag.getBaureihe().equalsIgnoreCase("G31")) && !(auftrag.containsSA("0146")) && !(auftrag.containsSA("0575")) && !(auftrag.containsSA("06FH"))) || ((auftrag.getBaureihe().equalsIgnoreCase("F97") || auftrag.getBaureihe().equalsIgnoreCase("F98") || auftrag.getBaureihe().equalsIgnoreCase("G01") || auftrag.getBaureihe().equalsIgnoreCase("G02")) && auftrag.containsSA("0493") && !(auftrag.containsSA("0441"))) || (auftrag.getBaureihe().startsWith("G2") && auftrag.containsSA("0493")) || (auftrag.getBaureihe().equalsIgnoreCase("RR11") || auftrag.getBaureihe().equalsIgnoreCase("RR12") || auftrag.getBaureihe().equalsIgnoreCase("RR31"))))
ppinit_73();
else
ppinit_76();
if( (((auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12")) && auftrag.containsSA("04F5") && !(auftrag.containsSA("0441"))) || auftrag.getBaureihe().equalsIgnoreCase("RR6")))
ppinit_84();
else
ppinit_87();
if( (((auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12") || auftrag.getBaureihe().equalsIgnoreCase("G32")) && auftrag.containsSA("04F5")) || ((auftrag.getBaureihe().equalsIgnoreCase("RR11") || auftrag.getBaureihe().equalsIgnoreCase("RR12") || auftrag.getBaureihe().equalsIgnoreCase("RR31")) && !(auftrag.containsSA("0RSF")))))
ppinit_95();
if( ((auftrag.getBaureihe().equalsIgnoreCase("F95") || auftrag.getBaureihe().equalsIgnoreCase("F96") || auftrag.getBaureihe().equalsIgnoreCase("F97") || auftrag.getBaureihe().equalsIgnoreCase("F98") || auftrag.getBaureihe().equalsIgnoreCase("G01") || auftrag.getBaureihe().equalsIgnoreCase("G02") || auftrag.getBaureihe().equalsIgnoreCase("G05") || auftrag.getBaureihe().equalsIgnoreCase("G06") || auftrag.getBaureihe().equalsIgnoreCase("G07") || auftrag.getBaureihe().equalsIgnoreCase("G08") || auftrag.getBaureihe().equalsIgnoreCase("G28") || (auftrag.getBaureihe().equalsIgnoreCase("G31") && !(auftrag.containsSA("0146"))) || auftrag.getBaureihe().equalsIgnoreCase("G32")) || ((auftrag.getBaureihe().equalsIgnoreCase("F90") || auftrag.getBaureihe().equalsIgnoreCase("F93") || auftrag.getBaureihe().equalsIgnoreCase("G16") || auftrag.getBaureihe().equalsIgnoreCase("G30")) && auftrag.containsSA("0575") && !(auftrag.containsSA("0146"))) || (auftrag.getBaureihe().startsWith("G2") && auftrag.containsSA("0493")) || (auftrag.getBaureihe().startsWith("F40") && auftrag.containsSA("0418")) || (auftrag.getBaureihe().startsWith("F44") && auftrag.containsSA("0493")) || ((((auftrag.getIStufeMonatInt()<7 && auftrag.getIStufeJahrInt()==17) || auftrag.getIStufeJahrInt()<17) && (auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12"))) && (auftrag.getL�nderVariante().startsWith("US") || auftrag.containsSA("08AA"))) || ((((auftrag.getIStufeMonatInt()>=7 && auftrag.getIStufeJahrInt()==17) || auftrag.getIStufeJahrInt()>17) && (auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12"))) && auftrag.getL�nderVariante().startsWith("US")) || ((((auftrag.getBaureihe().startsWith("F2") && !(auftrag.getBaureihe().equalsIgnoreCase("F25")) && !(auftrag.getBaureihe().equalsIgnoreCase("F26"))) || auftrag.getBaureihe().startsWith("F3")) && auftrag.containsSA("0493")) || auftrag.getBaureihe().equalsIgnoreCase("F31") || auftrag.getBaureihe().equalsIgnoreCase("F34") || auftrag.getBaureihe().equalsIgnoreCase("F35")) || auftrag.getBaureihe().equalsIgnoreCase("RR31")))
ppinit_98();
if( ((auftrag.getBaureihe().equals("RR11") || auftrag.getBaureihe().equals("RR12")) && !(auftrag.containsSA("0441"))))
ppinit_99();
else
ppinit_100();
if( ((auftrag.getBaureihe().equals("RR11") || auftrag.getBaureihe().equals("RR12")) && !(auftrag.containsSA("0441"))))
ppinit_102();
else
ppinit_103();

}
private void ppinit_105() throws PruefprozedurNotAvailableException {

}
private void ppinit_107() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_AHM_TASTER_BELEUCHTUNG_PRUEFEN", true );
	pp.setLocalName( "DE", "WF_AHM_TASTER_BELEUCHTUNG_PRUEFEN" );
	pp.setLocalName( "EN", "WF_TRAILER_JIG_BUTTON_ILLUMINATION_CHECK" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Is the AHV-button;lit/blinking?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Leuchtet/blinkt; der AHV-Taster?":"Leuchtet/blinkt; der AHV-Taster?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"AHM-button lit/blinking NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"AHM-button lit/blinkt NOK":"AHM-button lit/blinkt NOK")) );

}

}
private void ppinit_106() throws PruefprozedurNotAvailableException {
if( auftrag.containsSA("03AC"))
ppinit_107();

}
private void ppinit_108() throws PruefprozedurNotAvailableException {

}
private void ppinit_109() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_BELEUCHTUNG_GEPAECKRAUM", true );
	pp.setLocalName( "DE", "WF_BELEUCHTUNG_GEPAECKRAUM" );
	pp.setLocalName( "EN", "WF_BOOT_LIGHT_VISUAL_CHECK" );
	pp.setArgs( "FT" + "=" + getAttribut("QUESTION_TEXT_BOOT_LIGHT") );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Boot Light Defective":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Gep�ckraumleuchte defekt":"Boot Light Defective")) );

}

}
private void ppinit_110() throws PruefprozedurNotAvailableException {

}
private void ppinit_112() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_BELEUCHTUNG_GEHEIMFACH", true );
	pp.setLocalName( "DE", "WF_BELEUCHTUNG_GEHEIMFACH" );
	pp.setLocalName( "EN", "WF_SECRET_BOX_VISUAL_CHECK" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Light secret box ok?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Licht Geheimfach i.O.?":"Licht Geheimfach i.O.?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Glove Box Light NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Handschuhkastenlicht NOK":"Glove Box Light NOK")) );

}

}
private void ppinit_111() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_BELEUCHTUNG_HANDSCHUHKASTEN", true );
	pp.setLocalName( "DE", "WF_BELEUCHTUNG_HANDSCHUHKASTEN" );
	pp.setLocalName( "EN", "WF_GLOVE_BOX_VISUAL_CHECK" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Glove Box Light OK?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Handschuhkastenlicht i.O.?":"Handschuhkastenlicht i.O.?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Glove Box Light NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Handschuhkastenlicht NOK":"Glove Box Light NOK")) );

}
if( (auftrag.getBaureihe().equalsIgnoreCase("RR11") || auftrag.getBaureihe().equalsIgnoreCase("RR12")))
ppinit_112();

}
private void ppinit_113() throws PruefprozedurNotAvailableException {

}
private void ppinit_116() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_BELEUCHTUNG_HECKKLAPPE", true );
	pp.setLocalName( "DE", "WF_BELEUCHTUNG_HECKKLAPPE" );
	pp.setLocalName( "EN", "WF_HATCH_LIGHT_VISUAL_CHECK" );
	pp.setArgs( "FT" + "=" + getAttribut("QUESTION_TEXT_TAILGATE_LIGHT") );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Hatch Light Defective.":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Hatch Light Defekt":"Hatch Light Defekt")) );

}

}
private void ppinit_117() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_BELEUCHTUNG_HECKKLAPPE", true );
	pp.setLocalName( "DE", "WF_BELEUCHTUNG_HECKKLAPPE" );
	pp.setLocalName( "EN", "WF_TAILGATE LIGHT_VISUAL_CHECK" );
	pp.setArgs( "FT" + "=" + getAttribut("QUESTION_TEXT_TAILGATE_LIGHT") );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Hatch Light Defective.":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Hatch Light Defekt":"Hatch Light Defekt")) );

}

}
private void ppinit_115() throws PruefprozedurNotAvailableException {
if( auftrag.getIStufe().equals("S18"))
ppinit_116();
else
ppinit_117();

}
private void ppinit_114() throws PruefprozedurNotAvailableException {
if( ((!(auftrag.getBaureihe().equalsIgnoreCase("F90")) && !(auftrag.getBaureihe().equalsIgnoreCase("G30")) && !(auftrag.getBaureihe().equalsIgnoreCase("G38")) && !(auftrag.getBaureihe().equalsIgnoreCase("G20")) && !(auftrag.getBaureihe().equalsIgnoreCase("G28")) && !(auftrag.getBaureihe().equalsIgnoreCase("G05"))) || (auftrag.getBaureihe().startsWith("G05") && auftrag.containsSA("04UR"))))
ppinit_115();

}
private void ppinit_118() throws PruefprozedurNotAvailableException {

}
private void ppinit_120() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_DRIVER_DOOR_ROADMAP_LIGHT_OK", true );
	pp.setLocalName( "DE", "LICHT_KARTENABLAGE_FAHRERSEITE_OK" );
	pp.setLocalName( "EN", "DRIVER_DOOR_MAP_LIGHT_OK" );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Insert hand into the driver door map pocket. Does the map pocket light illuminate?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Leuchtet die Map-Pocket Beleuchtung Fahrerseite?":"Insert hand into the driver door map pocket. Does the map pocket light illuminate?")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The driver door map pocket light is defected":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die Map-Pocket Beleuchtung Fahrerseite ist defekt":"The driver door map pocket light is defected")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_PASSENGER_DOOR_ROADMAP_LIGHT_OK", true );
	pp.setLocalName( "DE", "LICHT_KARTEN_ABLAGE_BEIFAHRERSEITE_OK" );
	pp.setLocalName( "EN", "PASSENGER_DOOR_MAP_LIGHT_OK" );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Insert hand into the passenger door map pocket. Does the map pocket light illuminate?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Leuchtet die Map-Pocket Belechtung Beifahrerseite?":"Insert hand into the passenger door map pocket. Does the map pocket light illuminate?")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The passenger door map pocket light is defected":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die Map-Pocket Beleuchtung Beifahrerseite ist defekt":"The passenger door map pocket light is defected")) );

}

}
private void ppinit_119() throws PruefprozedurNotAvailableException {
if( ((auftrag.getBaureihe().equals("RR4") || auftrag.getBaureihe().equals("RR5") || auftrag.getBaureihe().equals("RR6")) && auftrag.containsSA("04UR")))
ppinit_120();

}
private void ppinit_121() throws PruefprozedurNotAvailableException {

}
private void ppinit_123() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_KENNZEICHENLEUCHTE_EIN_X_MODELS", true );
	pp.setLocalName( "DE", "WF_KENNZEICHENLEUCHTE_EIN_X_MODELS" );
	pp.setLocalName( "EN", "WF_LICENSE_PLATE_LIGHT_ON_X_MODELS" );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Are Both Licences Plate Lights On?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Leuchten beide Kennzeichenleuchten?":"Leuchten beide Kennzeichenleuchten?")) );
	pp.setArgs( "TITEL" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"QUESTION":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"FRAGE":"FRAGE")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The Licences Plate Lights are OFF":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die Kennzeichenleuchten leuchten nicht":"The Licences Plate Lights are OFF")) );

}

}
private void ppinit_124() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_KENNZEICHENLEUCHTE_EIN", true );
	pp.setLocalName( "DE", "WF_KENNZEICHENLEUCHTE_EIN" );
	pp.setLocalName( "EN", "WF_LICENSE_PLATE_LIGHT_ON" );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Are Both Licences Plate Lights On?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Leuchten beide Kennzeichenleuchten?":"Leuchten beide Kennzeichenleuchten?")) );
	pp.setArgs( "TITEL" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"QUESTION":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"FRAGE":"FRAGE")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The Licences Plate Lights are OFF":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die Kennzeichenleuchten leuchten nicht":"The Licences Plate Lights are OFF")) );

}

}
private void ppinit_122() throws PruefprozedurNotAvailableException {
if( (auftrag.getBaureihe().equals("F95") || auftrag.getBaureihe().equals("G05") || auftrag.getBaureihe().equals("G07")))
ppinit_123();
if( (!(auftrag.getBaureihe().equals("F95")) && !(auftrag.getBaureihe().equals("G05")) && !(auftrag.getBaureihe().equals("G07"))))
ppinit_124();

}
private void ppinit_125() throws PruefprozedurNotAvailableException {

}
private void ppinit_128() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_BELEUCHTUNG_PF�TZE_FAHRER", true );
	pp.setLocalName( "DE", "WF_BELEUCHTUNG_PF�TZE_FAHRER" );
	pp.setLocalName( "EN", "WF_PUDDLE_LIGHT_DRIVER" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Puddle light (next to driver door handle). Illuminated?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Puddle light (next to driver door handle). Illuminated?":"Puddle light (next to driver door handle). Illuminated?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}

}
private void ppinit_129() throws PruefprozedurNotAvailableException {

}
private void ppinit_130() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_BELEUCHTUNG_PF�TZE_BEIFAHRER", true );
	pp.setLocalName( "DE", "WF_BELEUCHTUNG_PF�TZE_BEIFAHRER" );
	pp.setLocalName( "EN", "WF_PUDDLE_LIGHT_PASSENGER" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Puddle light (next to passenger door handle). Illuminated?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Puddle light (next to passenger door handle). Illuminated?":"Puddle light (next to passenger door handle). Illuminated?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}

}
private void ppinit_127() throws PruefprozedurNotAvailableException {
ppinit_128();
ppinit_129();
ppinit_130();

}
private void ppinit_126() throws PruefprozedurNotAvailableException {
if( (auftrag.getBaureihe().equalsIgnoreCase("RR5") || auftrag.getBaureihe().equalsIgnoreCase("RR6")))
ppinit_127();

}
private void ppinit_131() throws PruefprozedurNotAvailableException {

}
private void ppinit_134() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_READING_LIGHT_FRONT_DRIVER", true );
	pp.setLocalName( "DE", "WF_LESELICHT_VORNE_FAHRER_ECOS" );
	pp.setLocalName( "EN", "WF_READING_LIGHT_FRONT_DRIVER" );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Switch on Driver Side Reading Light. Light illuminated?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Leuchtet Leselicht Fahrerseite Front?":"Switch on Driver Side Reading Light. Light illuminated?")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The Driver Side Reading Light is defected":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die Leselicht Fahrerseite Front Beleuchtung ist defekt":"The Driver Side Reading Light is defected")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_READING_LIGHT_FRONT_PASSENGER", true );
	pp.setLocalName( "DE", "WF_LESELICHT_VORNE_BEIFAHRER_ECOS" );
	pp.setLocalName( "EN", "WF_READING_LIGHT_FRONT_PASSENGER" );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Switch on Passenger Side Reading Light. Light illuminated?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Leuchtet Leselicht Beifahrerseite Front?":"Switch on Passenger Side Reading Light. Light illuminated?")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The Passenger Side Reading Light is defected":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die Leselicht Beifahrerseite Front Beleuchtung ist defekt":"The Passenger Side Reading Light is defected")) );

}

}
private void ppinit_133() throws PruefprozedurNotAvailableException {
if( auftrag.getBaureihe().equals("RR6"))
ppinit_134();

}
private void ppinit_135() throws PruefprozedurNotAvailableException {

}
private void ppinit_137() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_READING_LIGHT_REAR_DRIVER", true );
	pp.setLocalName( "DE", "WF_LESELICHT_HINTEN_FAHRER_ECOS" );
	pp.setLocalName( "EN", "WF_READING_LIGHT_REAR_DRIVER" );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Switch on Driver Side Reading Light rear. Light illuminated?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Leuchtet Leselicht Fahrerseite hinten?":"Switch on Driver Side Reading Light rear. Light illuminated?")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The Driver Side Rear Reading Light is defected":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die Leselicht Fahrerseite Hinten Beleuchtung ist defekt":"The Driver Side Rear Reading Light is defected")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_READING_LIGHT_REAR_PASSENGER", true );
	pp.setLocalName( "DE", "WF_LESELICHT_HINTEN_BEIFAHRER_ECOS" );
	pp.setLocalName( "EN", "WF_READING_LIGHT_REAR_PASSENGER" );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Switch on Passenger Side Reading Light rear. Light illuminated?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Leuchtet Leselicht Beifahrerseite hinten?":"Switch on Passenger Side Reading Light rear. Light illuminated?")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The Passenger Side Rear Reading Light is defected":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die Leselicht Beifahrerseite Hinten Beleuchtung ist defekt":"The Passenger Side Rear Reading Light is defected")) );

}

}
private void ppinit_136() throws PruefprozedurNotAvailableException {
if( (auftrag.getBaureihe().equals("RR11") || auftrag.getBaureihe().equals("RR12")))
ppinit_137();

}
private void ppinit_132() throws PruefprozedurNotAvailableException {
ppinit_133();
ppinit_135();
ppinit_136();

}
private void ppinit_138() throws PruefprozedurNotAvailableException {

}
private void ppinit_141() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_BRIGHTNESS_ADJ_MAX", true );
	pp.setLocalName( "DE", "WQ_HELLIGKEITSREGLER_MAX" );
	pp.setLocalName( "EN", "WQ_BRIGHTNESS_ADJ_MAX" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Brightness Adj; to maximum please!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Helligkeitsregler auf; Max drehen!":"Brightness Adj; to maximum please!")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}

}
private void ppinit_140() throws PruefprozedurNotAvailableException {
if( ((auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12") || auftrag.getBaureihe().equalsIgnoreCase("G38")) || ((auftrag.getBaureihe().equalsIgnoreCase("F97") || auftrag.getBaureihe().equalsIgnoreCase("F98") || auftrag.getBaureihe().equalsIgnoreCase("G01") || auftrag.getBaureihe().equalsIgnoreCase("G02") || (auftrag.getBaureihe().equalsIgnoreCase("G08") && !(auftrag.getHybrid().equalsIgnoreCase("BEVE")))) && auftrag.containsSA("0534")) || ((auftrag.getBaureihe().equalsIgnoreCase("F90") || auftrag.getBaureihe().equalsIgnoreCase("F91") || auftrag.getBaureihe().equalsIgnoreCase("F92") || auftrag.getBaureihe().equalsIgnoreCase("F93") || auftrag.getBaureihe().equalsIgnoreCase("G30") || auftrag.getBaureihe().equalsIgnoreCase("G31") || auftrag.getBaureihe().equalsIgnoreCase("G32")) && (auftrag.containsSA("04NB") || auftrag.containsSA("0534")))))
ppinit_141();

}
private void ppinit_143() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_LIGHT_SWITCH_ON", true );
	pp.setLocalName( "DE", "WQ_LICHTSCHALTER_EIN" );
	pp.setLocalName( "EN", "WQ_LIGHT_SWITCH_ON" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Turn on the light switch!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Lichtschalter einschalten!":"Turn on the light switch!")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_LIGHT_SWITCH_OFF", true );
	pp.setLocalName( "DE", "WQ_LICHTSCHALTER_AUS" );
	pp.setLocalName( "EN", "WQ_LIGHT_SWITCH_OFF" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Turn off the light switch!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Lichtschalter ausschalten!":"Turn off the light switch!")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}

}
private void ppinit_142() throws PruefprozedurNotAvailableException {
if( (auftrag.getBaureihe().equalsIgnoreCase("F56") && auftrag.getHybrid().equalsIgnoreCase("BEVE") && auftrag.containsSA("0563")))
ppinit_143();

}
private void ppinit_145() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_AIR_VENT_A_COLUMN_DRIVERSIDE", true );
	pp.setLocalName( "DE", "WF_BEL_LUFTAUSTROEMER_A_SAEULE_FAS" );
	pp.setLocalName( "EN", "WF_AIR_VENT_A_COLUMN_DRIVERSIDE" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Handwheel air vent; A column DS Light OK?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Stellrad Luftgitter FAS; A S�ule Beleuchtung IO?":"Handwheel air vent; A column DS Light OK?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The A column DS Handwheel air vent is defected":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die A S�ule Stellrad Luftgitter FS Beleuchtung ist defekt":"The A column DS Handwheel air vent is defected")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_AIR_VENT_A_COLUMN_PASSENGER_SIDE", true );
	pp.setLocalName( "DE", "WF_BEL_LUFTAUSTROEMER_A_SAEULE_BFS" );
	pp.setLocalName( "EN", "WF_AIR_VENT_A_COLUMN_PASSENGER_SIDE" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Handwheel air vent; A column PS Light OK?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Stellrad Luftgitter BFS; A S�ule Beleuchtung IO?":"Handwheel air vent; A column PS Light OK?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The A column PS Handwheel air vent is defected":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die A S�ule Stellrad Luftgitter BFS Beleuchtung ist defekt":"The A column PS Handwheel air vent is defected")) );

}

}
private void ppinit_144() throws PruefprozedurNotAvailableException {
if( ((auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12") || auftrag.getBaureihe().equalsIgnoreCase("G38")) || ((auftrag.getBaureihe().equalsIgnoreCase("F97") || auftrag.getBaureihe().equalsIgnoreCase("F98") || auftrag.getBaureihe().equalsIgnoreCase("G01") || auftrag.getBaureihe().equalsIgnoreCase("G02") || (auftrag.getBaureihe().equalsIgnoreCase("G08") && !(auftrag.getHybrid().equalsIgnoreCase("BEVE"))) || auftrag.getBaureihe().startsWith("G2")) && auftrag.containsSA("0534")) || (auftrag.getBaureihe().startsWith("F9") || ((auftrag.getBaureihe().equalsIgnoreCase("G30") || auftrag.getBaureihe().equalsIgnoreCase("G31") || auftrag.getBaureihe().equalsIgnoreCase("G32")) && (auftrag.containsSA("04NB") || auftrag.containsSA("0534"))))))
ppinit_145();

}
private void ppinit_147() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_AIR_VENT_B_COLUMN_DRIVERSIDE", true );
	pp.setLocalName( "DE", "WF_BEL_LUFTAUSTROEMER_B_SAEULE_FAS" );
	pp.setLocalName( "EN", "WF_AIR_VENT_B_COLUMN_DRIVERSIDE" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Handwheel air vent; B column DS Light OK?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Stellrad Luftgitter FAS; B S�ule Beleuchtung IO?":"Handwheel air vent; B column DS Light OK?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The B column DS Handwheel air vent is defected":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die B S�ule Stellrad Luftgitter FS Beleuchtung ist defekt":"The B column DS Handwheel air vent is defected")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_AIR_VENT_B_COLUMN_PASSENGER_SIDE", true );
	pp.setLocalName( "DE", "WF_BEL_LUFTAUSTROEMER_B_SAEULE_BFS" );
	pp.setLocalName( "EN", "WF_AIR_VENT_B_COLUMN_PASSENGER_SIDE" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Handwheel air susp.; B column PS Light OK?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Stellrad Luftgitter BFS; B S�ule Beleuchtung IO?":"Handwheel air susp.; B column PS Light OK?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The B column PS Handwheel air vent is defected":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die B S�ule Stellrad Luftgitter BFS Beleuchtung ist defekt":"The B column PS Handwheel air vent is defected")) );

}

}
private void ppinit_146() throws PruefprozedurNotAvailableException {
if( (!(auftrag.getAnzahlT�ren().equalsIgnoreCase("2")) && (auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12") || ((auftrag.getBaureihe().equalsIgnoreCase("F90") || auftrag.getBaureihe().equalsIgnoreCase("F93") || auftrag.getBaureihe().equalsIgnoreCase("G16") || auftrag.getBaureihe().equalsIgnoreCase("G30") || auftrag.getBaureihe().equalsIgnoreCase("G31") || auftrag.getBaureihe().equalsIgnoreCase("G32") || auftrag.getBaureihe().equalsIgnoreCase("G38")) && auftrag.containsSA("04NB")))))
ppinit_147();

}
private void ppinit_139() throws PruefprozedurNotAvailableException {
ppinit_140();
ppinit_142();
ppinit_144();
ppinit_146();

}
private void ppinit_148() throws PruefprozedurNotAvailableException {

}
private void ppinit_150() throws PruefprozedurNotAvailableException {

}
private void ppinit_152() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_BELEUCHTUNG_MAKEUPSPIEGEL_FA", true );
	pp.setLocalName( "DE", "WF_BELEUCHTUNG_MAKEUPSPIEGEL_FA" );
	pp.setLocalName( "EN", "WF_Light_SUNVISOR_MAKE_UP_DS" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Sunvisor Light: Driver Side OK?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Make-Up Beleuchtung: Fahrerseite i.O.?":"Make-Up Beleuchtung: Fahrerseite i.O.?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Sunvisor Light: Driver Side NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Make-Up Beleuchtung: Fahrerseite NOK":"Sunvisor Light: Driver Side NOK")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_BELEUCHTUNG_MAKEUPSPIEGEL_BF", true );
	pp.setLocalName( "DE", "WF_BELEUCHTUNG_MAKEUPSPIEGEL_BF" );
	pp.setLocalName( "EN", "WF_LIGHT_SUNVISOR_MAKE_UP_PS" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Sunvisor Light: Pass. Side OK?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Make-Up Beleuchtung: Beifahrerseite i.O.?":"Make-Up Beleuchtung: Beifahrerseite i.O.?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Sunvisor Light: Pass. Side NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Make-Up Beleuchtung: Beifahrerseite NOK":"Sunvisor Light: Pass. Side NOK")) );

}

}
private void ppinit_151() throws PruefprozedurNotAvailableException {
if( !(((auftrag.getBaureihe().equals("F40") || auftrag.getBaureihe().equals("F44")) && !(auftrag.containsSA("0563")))))
ppinit_152();

}
private void ppinit_153() throws PruefprozedurNotAvailableException {

}
private void ppinit_155() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_BELEUCHTUNG_MAKEUPSPIEGEL_FOND_FA", true );
	pp.setLocalName( "DE", "WF_BELEUCHTUNG_MAKEUPSPIEGEL_FOND_FA" );
	pp.setLocalName( "EN", "WF_Light_SUNVISOR_MAKE_UP_REAR_DS" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Sunvisor Light Rear: Driver Side OK?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Make-Up Beleuchtung Hinten: Fahrerseite i.O.?":"Make-Up Beleuchtung Hinten: Fahrerseite i.O.?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Sunvisor Light Rear: Driver Side NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Make-Up Beleuchtung Hinten: Fahrerseite NOK":"Sunvisor Light Rear: Driver Side NOK")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_BELEUCHTUNG_MAKEUPSPIEGEL_FOND_BF", true );
	pp.setLocalName( "DE", "WF_BELEUCHTUNG_MAKEUPSPIEGEL_FOND_BF" );
	pp.setLocalName( "EN", "WF_Light_SUNVISOR_MAKE_UP_REAR_PS" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Sunvisor Light Rear: Pass. Side OK?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Make-Up Beleuchtung Hinten: Beifahrerseite i.O.?":"Make-Up Beleuchtung Hinten: Beifahrerseite i.O.?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Sunvisor Light Rear: Pass. Side NOK":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Make-Up Beleuchtung Hinten: Beifahrerseite NOK":"Sunvisor Light Rear: Pass. Side NOK")) );

}

}
private void ppinit_154() throws PruefprozedurNotAvailableException {
if( ((auftrag.getBaureihe().equalsIgnoreCase("G12") || auftrag.getBaureihe().equalsIgnoreCase("G38")) || (auftrag.getBaureihe().equals("RR11") || auftrag.getBaureihe().equals("RR12"))))
ppinit_155();

}
private void ppinit_156() throws PruefprozedurNotAvailableException {

}
private void ppinit_149() throws PruefprozedurNotAvailableException {
ppinit_150();
ppinit_151();
ppinit_153();
ppinit_154();
ppinit_156();

}
private void ppinit_157() throws PruefprozedurNotAvailableException {

}
private void ppinit_158() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_INITIALISIERUNG_COACH_DOOR_STARTEN", true );
	pp.setLocalName( "DE", "WF_INITIALISIERUNG_COACH_DOOR_STARTEN" );
	pp.setLocalName( "EN", "WQ_START_INIT_COACH_DOOR" );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Start initialization coach door?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Initialisierung Coach Door starten?":"Start initialization coach door?")) );

}

}
private void ppinit_159() throws PruefprozedurNotAvailableException {

}
private void ppinit_161() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_EROLLO_KLAPPE_FREI", true );
	pp.setLocalName( "DE", "WF_EROLLO_KLAPPE_FREI" );
	pp.setLocalName( "EN", "WF_EROLLO_HATCH_FREE" );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Is the Erollo Hatch free?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Ist die Erollo Klappe frei?":"Is the Erollo Hatch free?")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The Erollo Hatch is not free":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die Erollo Klappe ist nicht frei":"The Erollo Hatch is not free")) );

}

}
private void ppinit_160() throws PruefprozedurNotAvailableException {
if( ((auftrag.getBaureihe().equalsIgnoreCase("F95") || auftrag.getBaureihe().equalsIgnoreCase("G05")) && auftrag.containsSA("0418")))
ppinit_161();

}
private void ppinit_162() throws PruefprozedurNotAvailableException {

}
private void ppinit_164() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_FACKELHALTER_JAPAN", true );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "FT" + "=" + "JAPAN Fackelhalter" + ";" +"verbaut?" );

}

}
private void ppinit_163() throws PruefprozedurNotAvailableException {
if( auftrag.containsSA("0807"))
ppinit_164();

}
private void ppinit_165() throws PruefprozedurNotAvailableException {

}
private void ppinit_167() throws PruefprozedurNotAvailableException {

}
private void ppinit_168() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_FENSTER_SCHLIESSEN", true );
	pp.setLocalName( "DE", "WF_FENSTER_SCHLIESSEN" );
	pp.setLocalName( "EN", "WF_CLOSE_WINDOWS" );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"CLOSE ALL WINDOWS?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"ALLE FENSTER; SCHLIESSEN?":"ALLE FENSTER; SCHLIESSEN?")) );
	pp.setArgs( "TITEL" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"QUESTION":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"FRAGE":"FRAGE")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The windows are not closed":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die Fenster sind nicht geschlossen":"The windows are not closed")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_WINDOW_INIT", true );
	pp.setLocalName( "DE", "WF_FENSTER_INIT" );
	pp.setLocalName( "EN", "WF_WINDOW_INIT" );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Initialise windows?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Fenster initialisieren?":"Initialise windows?")) + ";" +(CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Shut all Doors?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Alle T�ren schlie�en?":"Shut all Doors?")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Windows are not initialized":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die Fenster sind nicht initialisiert":"Windows are not initialized")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_AREA_CLEAR", true );
	pp.setLocalName( "DE", "WF_PRUEF_UMGEBUNG_FREI" );
	pp.setLocalName( "EN", "WF_AREA_CLEAR" );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Windows closing":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Fenster schlie�en":"Windows closing")) + ";" +(CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Automatically!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"automatisch!":"Automatically!")) + ";" +(CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"-All Doors Shut?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"-Sind alle T�ren geschlossen":"-All Doors Shut?")) + ";" +(CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"-Area clear?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"und ist der Bereich frei?":"-Area clear?")) );
	pp.setArgs( "TITEL" + "=" + "***WARNING***" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Windows are not closed":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Fenster sind nicht geschlossen":"Windows are not closed")) );

}

}
private void ppinit_169() throws PruefprozedurNotAvailableException {

}
private void ppinit_170() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_KANN_FENSTER_INIT", true );
	pp.setLocalName( "DE", "WF_KANN_FENSTER_INIT" );
	pp.setLocalName( "EN", "WF_CAN_WINDOWS_INIT" );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Can the windows initialize?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"K�nnen die Fenster initialisieren?":"K�nnen die Fenster initialisieren?")) );
	pp.setArgs( "TITEL" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"QUESTION":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"FRAGE":"FRAGE")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The windows can not be initialize":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die Fenster k�nnen nicht initialisieren":"The windows can not be initialize")) );

}

}
private void ppinit_166() throws PruefprozedurNotAvailableException {
ppinit_167();
ppinit_168();
ppinit_169();
ppinit_170();

}
private void ppinit_171() throws PruefprozedurNotAvailableException {

}
private void ppinit_173() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_FUNKTION_FRISCHLUFTGRILL_BFS_IO", true );
	pp.setLocalName( "DE", "WF_FUNKTION_FRISCHLUFTGRILL_BFS_IO" );
	pp.setLocalName( "EN", "WF_AIR_VENT_FUNCTION_PASSENGER_SIDE_OK" );
	pp.setArgs( "TITEL" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"AIR VENT PASSENGER:":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"FRISCHLUFTGRILL BFS:":"FRISCHLUFTGRILL BFS:")) );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Air vent left/right up/down?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Gitter links/rechts oben/unten?":"Gitter links/rechts oben/unten?")) + ";" +(CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Air flow on/off?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Luftstrom auf/zu?":"Luftstrom auf/zu?")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Air vent passenger side defective.":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Frischluftgrill Beifahrerseite defekt.":"Frischluftgrill Beifahrerseite defekt.")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}

}
private void ppinit_172() throws PruefprozedurNotAvailableException {
if( (auftrag.getBaureihe().equalsIgnoreCase("F20") || auftrag.getBaureihe().equalsIgnoreCase("F21") || auftrag.getBaureihe().equalsIgnoreCase("F22") || auftrag.getBaureihe().equalsIgnoreCase("F23") || auftrag.getBaureihe().equalsIgnoreCase("F87")))
ppinit_173();

}
private void ppinit_174() throws PruefprozedurNotAvailableException {

}
private void ppinit_176() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WQ_HDC_BUTTON_NOT_INSTALLED_CHECK", true );
	pp.setLocalName( "DE", "WF_HDC_TASTER_NICHT_VERBAUT" );
	pp.setLocalName( "EN", "WQ_HDC_BUTTON_NOT_INSTALLED_CHECK" );
	pp.setArgs( "TITEL" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Visual Check":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Sichtpr�fung":"Visual Check")) );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Is the HDC button there on the middle console? YES or NO":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"HDC Taster NICHT verbaut":"Is the HDC button there on the middle console? YES or NO")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "CREATE_NOK_INNER_HDC_BUTTON", true );
	pp.setLocalName( "DE", "CREATE_NOK_INNER_HDC_BUTTON" );
	pp.setLocalName( "EN", "CREATE_NOK_INNER_HDC_BUTTON" );
	pp.setArgs( "JOBN_HWT1" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"HDC Button installed in Error!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Falschverbau HDC-Taster. Es sollte keiner verbaut sein.":"HDC Button installed in Error!")) );
	pp.setArgs( "JOBN_SGBD" + "=" + "none" );
	pp.setArgs( "JOBN_JOB" + "=" + "none" );
	pp.setArgs( "JOBN_CONDITION1" + "=" + "EQUALS(OK,NOK)" );

}

}
private void ppinit_175() throws PruefprozedurNotAvailableException {
if( !(auftrag.getAntriebsArt().equals("AR")))
ppinit_176();

}
private void ppinit_177() throws PruefprozedurNotAvailableException {

}
private void ppinit_179() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_COOL_BOX_ILLUMINATION_CHECK", true );
	pp.setLocalName( "DE", "WF_KUEHLBOX_BELEUCHTUNG_PRUEFEN" );
	pp.setLocalName( "EN", "WF_COOL_BOX_ILLUMINATION_CHECK" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Switch on icebox. LED on or blinking?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"K�hlbox einschalten, leuchtet/blinkt LED?":"Switch on icebox. LED on or blinking?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The icebox. LED is OFF":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Die K�hlbox LED Beleuchtung ist OFF":"The icebox. LED is OFF")) );

}

}
private void ppinit_178() throws PruefprozedurNotAvailableException {
if( ((auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12")) && auftrag.containsSA("0791")))
ppinit_179();

}
private void ppinit_180() throws PruefprozedurNotAvailableException {

}
private void ppinit_182() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerAnweisung", "WA_PULL_BOTH_SWITCH_LFE", true );
	pp.setLocalName( "DE", "WA_BEIDE_TASTER_LFE_ZIEHEN" );
	pp.setLocalName( "EN", "WA_PULL_BOTH_SWITCH_LFE" );
	pp.setArgs( "DAUER" + "=" + "3000" );
	pp.setArgs( "FENSTERNAME" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Fold down the backrest:":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Lehnenfernentriegelung:":"Fold down the backrest:")) );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"first pull the right then the left switch.":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"zuerst rechten dann linken Taster bet�tigen.":"first pull the right then the leftt switch.")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_FOLD_DOWN_BACKREST", true );
	pp.setLocalName( "DE", "WF_R�CKLEHNE_UMGEKLAPPT" );
	pp.setLocalName( "EN", "WF_FOLD_DOWN_BACKREST" );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Is the backrest fold down completely?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Ist die R�cklehne komplett umgeklappt?":"Is the backrest fold down completely?")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_FOLD_UP_PART_1_OF_THE_BACKREST", true );
	pp.setLocalName( "DE", "WQ_TEIL_1_DER_RUECKENLEHNE_HOCHKLAPPEN" );
	pp.setLocalName( "EN", "WQ_FOLD_UP_PART_1_OF_THE_BACKREST" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Please fold up backrest passenger side":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Bitte R�ckenlehne BF Seite hochklappen.":"Please fold up backrest passenger side.")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_FOLD_UP_PART_2_OF_THE_BACKREST", true );
	pp.setLocalName( "DE", "WQ_TEIL_2_DER_RUECKENLEHNE_HOCHKLAPPEN" );
	pp.setLocalName( "EN", "WQ_FOLD_UP_PART_2_OF_THE_BACKREST" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Please fold up backrest driver side.":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Bitte R�ckenlehne FA Seite hochklappen.":"Please fold up backrest driver side.")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_LEAVE_THE_CAR_LFE", true );
	pp.setLocalName( "DE", "WQ_FZG_VERLASSEN_LFE" );
	pp.setLocalName( "EN", "WQ_LEAVE_THE_CAR_LFE" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"please leave car":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Fahrzeug;verlassen":"Fahrzeug;verlassen")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}

}
private void ppinit_181() throws PruefprozedurNotAvailableException {
if( ((((auftrag.getBaureihe().equalsIgnoreCase("F45") && !(auftrag.containsSA("08AA"))) || auftrag.getBaureihe().equalsIgnoreCase("F48") || auftrag.getBaureihe().equalsIgnoreCase("F39")) && auftrag.containsSA("04FD")) || auftrag.getBaureihe().equalsIgnoreCase("F46") || auftrag.getBaureihe().equalsIgnoreCase("M13")))
ppinit_182();

}
private void ppinit_183() throws PruefprozedurNotAvailableException {

}
private void ppinit_185() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_SEAT_BACKREST_PS_REAR_FORWARD", true );
	pp.setLocalName( "DE", "WF_SITZLEHNE_BFH_VOR_FAHREN" );
	pp.setLocalName( "EN", "WF_SEAT_BACKREST_PS_REAR_FORWARD" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Move backrest PS rear forward! Moving PS rear forward?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"BFH Lehne vorfahren! F�hrt Lehne BFH vor?":"Move backrest PS rear forward! Moving PS rear forward?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_SEAT_BACKREST_PS_REAR_BACKWARD", true );
	pp.setLocalName( "DE", "WF_SITZLEHNE_BFH_ZUR�CK_FAHREN" );
	pp.setLocalName( "EN", "WF_SEAT_BACKREST_PS_REAR_BACKWARD" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Move backrest PS rear backward! Moving PS rear backward?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"BFH Lehne zur�ckfahren! F�hrt Lehne BFH zur�ck?":"Move backrest PS rear backward! Moving PS rear backward?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_SEAT_BACKREST_DS_REAR_FORWARD", true );
	pp.setLocalName( "DE", "WF_SITZLEHNE_FAH_VOR_FAHREN" );
	pp.setLocalName( "EN", "WF_SEAT_BACKREST_DS_REAR_FORWARD" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Move backrest DS rear forward! Moving DS rear forward?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"FAH Lehne vorfahren! F�hrt Lehne FAH vor?":"Move backrest DS rear forward! Moving DS rear forward?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_SEAT_BACKREST_DS_REAR_BACKWARD", true );
	pp.setLocalName( "DE", "WF_SITZLEHNE_FAH_ZUR�CK_FAHREN" );
	pp.setLocalName( "EN", "WF_SEAT_BACKREST_DS_REAR_BACKWARD" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Move backrest DS rear backward! Moving DS rear backward?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"FAH Lehne zur�ckfahren! F�hrt Lehne FAH zur�ck?":"Move backrest DS rear backward! Moving DS rear backward?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}

}
private void ppinit_184() throws PruefprozedurNotAvailableException {
if( (auftrag.getBaureihe().equals("G32") && auftrag.containsSA("0461")))
ppinit_185();

}
private void ppinit_186() throws PruefprozedurNotAvailableException {

}
private void ppinit_188() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WD_MECH_KEY_DRIVER_DOOR_UNLOCKED", true );
	pp.setLocalName( "DE", "WF_MECHAN_SCHLUESSEL_FT_ENTRIEGELT" );
	pp.setLocalName( "EN", "WD_MECH_KEY_DRIVER_DOOR_UNLOCKED" );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Unlock Door with Mechanical Key.":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Mit mechn. Schluessel oeffnen":"Unlock Door with Mechanical Key.")) + ";" +(CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"driver door unlocked?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"FT entriegelt?":"driver door unlocked?")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Door not unlocked":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"T�r nicht entriegelt":"Door not unlocked")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WD_MECH_KEY_DRIVER_DOOR_LOCKED", true );
	pp.setLocalName( "DE", "WF_MECHAN_SCHLUESSEL_FT_VERRIEGELT" );
	pp.setLocalName( "EN", "WD_MECH_KEY_DRIVER_DOOR_LOCKED" );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Lock Door with Mechanical Key.":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Mit mechn. Schluessel schliessen":"Lock Door with Mechanical Key.")) + ";" +(CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"driver door locked?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"FT verriegelt?":"driver door locked?")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Door not locked":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"T�r nicht verriegelt":"Door not locked")) );

}

}
private void ppinit_187() throws PruefprozedurNotAvailableException {
if( (!(auftrag.getIStufeBRV().startsWith("S15")) || (auftrag.getIStufeBRV().startsWith("S15") && (auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12")) && ((auftrag.getIStufeMonatInt()>=3 && auftrag.getIStufeJahrInt()==19) || auftrag.getIStufeJahrInt()>19)) || (auftrag.getIStufeBRV().startsWith("S15") && (auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12")) && auftrag.getOrderWerk().startsWith("02") && auftrag.containsSX("00B4"))))
ppinit_188();

}
private void ppinit_189() throws PruefprozedurNotAvailableException {

}
private void ppinit_190() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_START_STOP_TASTER_MSA", true );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "FT" + "=" + getAttribut("QUESTION_TEXT_MSA_BUTTON") );

}

}
private void ppinit_191() throws PruefprozedurNotAvailableException {

}
private void ppinit_193() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WQ_PDC_BUTTON_NOT_INSTALLED_CHECK", true );
	pp.setLocalName( "DE", "WF_PDC_TASTER_NICHT_VERBAUT" );
	pp.setLocalName( "EN", "WQ_PDC_BUTTON_NOT_INSTALLED_CHECK" );
	pp.setArgs( "TITEL" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Visual Check":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Sichtpr�fung":"Visual Check")) );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Is PDC button present? YES or NO":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"PDC Taster NICHT verbaut":"Is PDC button present? YES or NO")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "CREATE_NOK_INNER_PDC_BUTTON", true );
	pp.setLocalName( "DE", "CREATE_NOK_INNER_PDC_BUTTON" );
	pp.setLocalName( "EN", "CREATE_NOK_INNER_PDC_BUTTON" );
	pp.setArgs( "JOBN_HWT1" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"PDC Button installed in Error!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Falschverbau PDC-Taster. Es sollte keiner verbaut sein.":"PDC Button installed in Error!")) );
	pp.setArgs( "JOBN_SGBD" + "=" + "none" );
	pp.setArgs( "JOBN_JOB" + "=" + "none" );
	pp.setArgs( "JOBN_CONDITION1" + "=" + "EQUALS(OK,NOK)" );

}

}
private void ppinit_192() throws PruefprozedurNotAvailableException {
if( (!(auftrag.containsSA("0508")) && !(auftrag.getBaureihe().equalsIgnoreCase("F85")) && !(auftrag.getBaureihe().equalsIgnoreCase("F86"))))
ppinit_193();

}
private void ppinit_194() throws PruefprozedurNotAvailableException {

}
private void ppinit_195() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_POWERDOWN_DURCHFUEHREN", true );
	pp.setArgs( "TITEL" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Powerdown":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Powerdown":"Powerdown")) );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Powerdown durchf�hren?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Powerdown durchf�hren?":"Powerdown durchf�hren?")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_POWERDOWN_VERKUERZT", true );
	pp.setArgs( "TITEL" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"verk�rzt vs. nat�rlich":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"verk�rzt vs. nat�rlich":"verk�rzt vs. nat�rlich")) );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Verk�rzter Powerdown? (NEIN= nat�rlicher Powerdown)":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Verk�rzter Powerdown? (NEIN= nat�rlicher Powerdown)":"Verk�rzter Powerdown? (NEIN= nat�rlicher Powerdown)")) );

}

}
private void ppinit_196() throws PruefprozedurNotAvailableException {

}
private void ppinit_198() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_SCHIEBEDACH_SCHLIESST", true );
	pp.setLocalName( "DE", "WF_SCHIEBEDACH_SCHLIESST" );
	pp.setLocalName( "EN", "WF_CLOSE_SUNROOF" );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"CLOSE THE SUNROOF?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"SCHIEBEDACH;SCHLIESST?":"SCHIEBEDACH;SCHLIESST?")) );
	pp.setArgs( "TITEL" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"QUESTION":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"FRAGE":"FRAGE")) );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Sunroof opened":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Schiebedach ge�ffnetC":"Sunroof opened")) );

}

}
private void ppinit_197() throws PruefprozedurNotAvailableException {
if( auftrag.containsSA("0403"))
ppinit_198();

}
private void ppinit_199() throws PruefprozedurNotAvailableException {

}
private void ppinit_201() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "TEST_FUEL_FILLER_FLAP_NECESSARY", true );
	pp.setLocalName( "DE", "TEST_TANKKLAPPE_NOTWENDIG" );
	pp.setLocalName( "EN", "TEST_FUEL_FILLER_FLAP_NECESSARY" );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_FUEL_FILLER_FLAP_CLOSED", true );
	pp.setLocalName( "DE", "WF_TANKKLAPPE_GESCHLOSSEN" );
	pp.setLocalName( "EN", "WF_FUEL_FILLER_FLAP_CLOSED" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Press flap, is Fuel Filler Flap locked and closed? y/n":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Tankklappe;geschlossen ?":"Press flap, is Fuel Filler Flap locked and closed? y/n")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The Fuel Filler Flap is unlocked or open":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Tankklappe ist ge�ffnet":"The Fuel Filler Flap is unlocked or open")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_FUEL_FILLER_FLAP_OPEN", true );
	pp.setLocalName( "DE", "WF_TANKKLAPPE_GEOEFFNET" );
	pp.setLocalName( "EN", "WF_FUEL_FILLER_FLAP_OPEN" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Press flap, is Fuel Filler Flap unlocked and opened? y/n � then close again":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Tankklappe;ge�ffnet ?":"Press flap, is Fuel Filler Flap unlocked and opened? y/n � then close again")) );
	pp.setArgs( "DAUER" + "=" + "300000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"The Fuel Filler Flap is locked or closed":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Tankklappe ist geschlossen":"The Fuel Filler Flap is locked or closed")) );

}

}
private void ppinit_202() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "TEST_FUEL_FILLER_FLAP_NECESSARY", true );
	pp.setLocalName( "DE", "TEST_TANKKLAPPE_NOTWENDIG" );
	pp.setLocalName( "EN", "TEST_FUEL_FILLER_FLAP_NECESSARY" );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_200() throws PruefprozedurNotAvailableException {
if( ((auftrag.getBaureihe().startsWith("F4") || auftrag.getBaureihe().startsWith("F9") || auftrag.getBaureihe().startsWith("G")) && !(auftrag.getHybrid().startsWith("PHEV"))))
ppinit_201();
else
ppinit_202();

}
private void ppinit_203() throws PruefprozedurNotAvailableException {

}
private void ppinit_205() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_CHECK_LOWER_TAILGATE", true );
	pp.setArgs( "DAUER" + "=" + "100000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Is Lower Tailgate raised and locked at rear?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Is Lower Tailgate raised and locked at rear?":"Is Lower Tailgate raised and locked at rear?")) );

}

}
private void ppinit_204() throws PruefprozedurNotAvailableException {
if( (auftrag.getBaureihe().equalsIgnoreCase("F85") || auftrag.getBaureihe().equalsIgnoreCase("F15")))
ppinit_205();

}
private void ppinit_206() throws PruefprozedurNotAvailableException {

}
private void ppinit_208() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_INSERT_WCA_TEST_ADAPTER", true );
	pp.setLocalName( "DE", "WQ_WCA_TESTER_EINLEGEN" );
	pp.setLocalName( "EN", "WQ_INSERT_WCA_TEST_ADAPTER" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Please insert the WCA test adapter.":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Bitte WCA Testadapter einstecken.":"Please insert the WCA test adapter.")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_REMOVE_WCA_TEST_ADAPTER", true );
	pp.setLocalName( "EN", "WQ_REMOVE_WCA_TEST_ADAPTER" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Please remove the WCA test adapter.":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Bitte WCA Testadapter entnehmen.":"Please remove the WCA test adapter.")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WD_WCA_OK", true );
	pp.setLocalName( "DE", "WF_WCA_IO" );
	pp.setLocalName( "EN", "WD_WCA_OK" );
	pp.setArgs( "DAUER" + "=" + "60000" );
	pp.setArgs( "TITEL" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Pull the WCA slide and hold:":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Pr�fung WirelessChargingAblage:":"Pull the WCA slide and hold:")) );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Is the blue LED solidly on? LED will switch off after 10s.":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Leuchtet die blaue LED der WCA dauerhaft?":"Is the blue LED solidly on? LED will switch off after 10s.")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "WAIT_TIME_WCA", true );
	pp.setLocalName( "DE", "WARTEZEIT_WCA" );
	pp.setLocalName( "EN", "WAIT_TIME_WCA" );
	pp.setArgs( "DAUER" + "=" + "200" );

}

}
private void ppinit_207() throws PruefprozedurNotAvailableException {
if( (auftrag.containsSA("06NW") || auftrag.containsSA("06NV")))
ppinit_208();

}
private void ppinit_209() throws PruefprozedurNotAvailableException {

}
private void ppinit_44() throws PruefprozedurNotAvailableException {
ppinit_45();
ppinit_46();
ppinit_105();
ppinit_106();
ppinit_108();
ppinit_109();
ppinit_110();
ppinit_111();
ppinit_113();
ppinit_114();
ppinit_118();
ppinit_119();
ppinit_121();
ppinit_122();
ppinit_125();
ppinit_126();
ppinit_131();
ppinit_132();
ppinit_138();
ppinit_139();
ppinit_148();
ppinit_149();
ppinit_157();
ppinit_158();
ppinit_159();
ppinit_160();
ppinit_162();
ppinit_163();
ppinit_165();
ppinit_166();
ppinit_171();
ppinit_172();
ppinit_174();
ppinit_175();
ppinit_177();
ppinit_178();
ppinit_180();
ppinit_181();
ppinit_183();
ppinit_184();
ppinit_186();
ppinit_187();
ppinit_189();
ppinit_190();
ppinit_191();
ppinit_192();
ppinit_194();
ppinit_195();
ppinit_196();
ppinit_197();
ppinit_199();
ppinit_200();
ppinit_203();
ppinit_204();
ppinit_206();
ppinit_207();
ppinit_209();

}
private void ppinit_210() throws PruefprozedurNotAvailableException {

}
private void ppinit_211() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerIdent", "ASSOCIATE_NUMBER", true );
	pp.setLocalName( "DE", "PRUEFERNUMMER_EINGEBEN" );
	pp.setLocalName( "EN", "ASSOCIATE_NUMBER" );

}

}
private void ppinit_212() throws PruefprozedurNotAvailableException {

}
private void ppinit_214() throws PruefprozedurNotAvailableException {

}
private void ppinit_215() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WC_SWITCH_OFF_LIGHT", true );
	pp.setLocalName( "DE", "WQ_LICHT_AUS" );
	pp.setLocalName( "EN", "WC_SWITCH_OFF_LIGHT" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Ensure the light switch is in the 12 o'clock position.":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Bitte Bedieneinheit Licht in Stellung 0 (AUS) bringen!":"Ensure the light switch is in the 12 o'clock position.")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}

}
private void ppinit_216() throws PruefprozedurNotAvailableException {

}
private void ppinit_217() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_TUEREN_BEIFAHRERSEITE_KONTROLLIEREN", true );
	pp.setLocalName( "DE", "WQ_TUEREN_BEIFAHRERSEITE_KONTROLLIEREN" );
	pp.setLocalName( "EN", "WQ_CONTROL_OPENING_DOORS_PASSENGER_SIDE" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Doors passenger side are not fully opened! Please check that the doors passenger side are opened!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"T�ren Beifahrerseite nicht komplett ge�ffnet! Bitte �ffnung der T�ren auf der Beifahrerseite kontrollieren!":"Doors passenger side are not fully opened! Please check that the doors passenger side are opened!")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_TUEREN_FAHRERSEITE_KONTROLLIEREN", true );
	pp.setLocalName( "DE", "WQ_TUEREN_FAHRERSEITE_KONTROLLIEREN" );
	pp.setLocalName( "EN", "WQ_CONTROL_OPENING_DOORS_DRIVER_SIDE" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Doors driver side are not fully opened! Please check that the doors driver side are opened!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"T�ren Fahrerseite nicht komplett ge�ffnet! Bitte �ffnung der T�ren auf der Fahrerseite kontrollieren!":"Doors driver side are not fully opened! Please check that the doors driver side are opened!")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}

}
private void ppinit_218() throws PruefprozedurNotAvailableException {

}
private void ppinit_220() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerAnweisung", "WA_LEAVE_CAR_REAR", true );
	pp.setLocalName( "DE", "WA_FZG_VERLASSEN_HINTEN" );
	pp.setLocalName( "EN", "WA_LEAVE_CAR_REAR" );
	pp.setArgs( "DAUER" + "=" + "3000" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"please leave;the car":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Fahrzeug;verlassen":"Fahrzeug;verlassen")) );

}

}
private void ppinit_221() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_FZG_VERLASSEN_FAHRERSEITE_HINTEN", true );
	pp.setLocalName( "DE", "WQ_FZG_VERLASSEN_FAHRERSEITE_HINTEN" );
	pp.setLocalName( "EN", "WQ_LEAVE_CAR_DRIVER_SIDE_REAR" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"please leave;the car":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Fahrzeug;verlassen":"Fahrzeug;verlassen")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}

}
private void ppinit_222() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerAnweisung", "WA_FZG_VERLASSEN_HINTEN_BEIFAHRER", true );
	pp.setLocalName( "DE", "WA_FZG_VERLASSEN_HINTEN_BEIFAHRER" );
	pp.setLocalName( "EN", "WA_LEAVE_CAR_BACK_PASSENGER" );
	pp.setArgs( "DAUER" + "=" + "3000" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"please leave the car":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Fahrzeug verlassen":"please leave the car")) );

}

}
private void ppinit_223() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerAnweisung", "WA_FZG_VERLASSEN_HINTEN_FAHRER", true );
	pp.setLocalName( "DE", "WA_FZG_VERLASSEN_HINTEN_FAHRER" );
	pp.setLocalName( "EN", "WA_LEAVE_CAR_BACK_DRIVER" );
	pp.setArgs( "DAUER" + "=" + "3000" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"please leave the car":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Fahrzeug verlassen":"please leave the car")) );

}

}
private void ppinit_219() throws PruefprozedurNotAvailableException {
if( auftrag.getBaureihe().equals("RR4"))
ppinit_220();
if( (!(auftrag.getAnzahlT�ren().equals("2")) && ((auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12")) || ((auftrag.getBaureihe().startsWith("F9") || auftrag.getBaureihe().equalsIgnoreCase("G30") || auftrag.getBaureihe().equalsIgnoreCase("G31") || auftrag.getBaureihe().equalsIgnoreCase("G32") || auftrag.getBaureihe().equalsIgnoreCase("G38")) && auftrag.containsSA("04NB")) || ((((auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12")) && (auftrag.containsSA("04UR") || auftrag.containsSA("03E1") || auftrag.containsSA("0715") || auftrag.containsSA("0778"))) || ((auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12")) && auftrag.getHybrid().equalsIgnoreCase("PHEV")) || (auftrag.getBaureihe().equalsIgnoreCase("G12") && auftrag.getMotorBaureihe().startsWith("N74")) || auftrag.getBaureihe().equalsIgnoreCase("G16")) || (((auftrag.getBaureihe().equalsIgnoreCase("G30") || auftrag.getBaureihe().equalsIgnoreCase("G31")) && (auftrag.containsSA("07S2") || auftrag.containsSA("07AC") || auftrag.containsSA("0715") || auftrag.containsSA("0778"))) || auftrag.getBaureihe().equalsIgnoreCase("F90") || ((auftrag.getBaureihe().equalsIgnoreCase("G30") || auftrag.getBaureihe().equalsIgnoreCase("G31")) && (auftrag.getMotorBaureihe().equals("B57S") || auftrag.getMotorBaureihe().equals("N63R")))) || ((auftrag.getBaureihe().equalsIgnoreCase("G05") || auftrag.getBaureihe().equalsIgnoreCase("G06") || auftrag.getBaureihe().equalsIgnoreCase("G07")) && auftrag.containsSA("04UR"))))))
ppinit_221();
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_FZG_VERLASSEN", true );
	pp.setLocalName( "DE", "WQ_FZG_VERLASSEN" );
	pp.setLocalName( "EN", "WQ_LEAVE_CAR" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"please leave;the car":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Fahrzeug;verlassen":"Fahrzeug;verlassen")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerAnweisung", "WA_FZG_VERLASSEN", true );
	pp.setLocalName( "DE", "WA_FZG_VERLASSEN" );
	pp.setLocalName( "EN", "WA_LEAVE_CAR" );
	pp.setArgs( "DAUER" + "=" + "3000" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"please leave the car":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Fahrzeug verlassen":"please leave the car")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_GET_IN_PS_REAR", true );
	pp.setLocalName( "DE", "WQ_EINSTEIGEN_BF_FOND" );
	pp.setLocalName( "EN", "WQ_GET_IN_PS_REAR" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Get in passenger side rear!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Einsteigen BF- Seite Fond":"Get in passenger side rear!")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}
if( (((auftrag.getBaureihe().equalsIgnoreCase("G07") && !(auftrag.containsSA("0441"))) || (auftrag.getBaureihe().equalsIgnoreCase("RR5") || auftrag.getBaureihe().equalsIgnoreCase("RR6") || auftrag.getBaureihe().equalsIgnoreCase("RR11") || auftrag.getBaureihe().equalsIgnoreCase("RR12"))) || ((auftrag.getBaureihe().equalsIgnoreCase("G07") && !(auftrag.containsSA("0441"))) || (((auftrag.getBaureihe().equalsIgnoreCase("RR5") || auftrag.getBaureihe().equalsIgnoreCase("RR6")) && !(auftrag.containsSA("0441"))) || (auftrag.getBaureihe().equalsIgnoreCase("RR11") || auftrag.getBaureihe().equalsIgnoreCase("RR12")))) || (((auftrag.getBaureihe().equalsIgnoreCase("G05") || auftrag.getBaureihe().equalsIgnoreCase("G06")) && !(auftrag.containsSA("0441"))) || (auftrag.getBaureihe().startsWith("G2") && auftrag.containsSA("0493")) || (auftrag.getBaureihe().equalsIgnoreCase("RR11") || auftrag.getBaureihe().equalsIgnoreCase("RR12"))) || auftrag.getBaureihe().equalsIgnoreCase("RR6") || (auftrag.getBaureihe().equals("RR11") || auftrag.getBaureihe().equals("RR12")) || ((auftrag.getBaureihe().equalsIgnoreCase("G07") && auftrag.containsSA("0441")) || ((auftrag.getBaureihe().equalsIgnoreCase("RR5") || auftrag.getBaureihe().equalsIgnoreCase("RR6")) && auftrag.containsSA("0441")) || (((auftrag.getBaureihe().equalsIgnoreCase("G05") || auftrag.getBaureihe().equalsIgnoreCase("G06")) && auftrag.containsSA("0441")) || ((auftrag.getBaureihe().equalsIgnoreCase("G20") || auftrag.getBaureihe().equalsIgnoreCase("G21")) && auftrag.containsSA("0441") && auftrag.containsSA("0493")) || auftrag.getBaureihe().equalsIgnoreCase("G28")) || (auftrag.containsSA("04F5") && auftrag.containsSA("0441"))) || (auftrag.getBaureihe().equalsIgnoreCase("G07") || auftrag.getBaureihe().equalsIgnoreCase("G16") || auftrag.getBaureihe().equalsIgnoreCase("G28") || ((auftrag.getBaureihe().equalsIgnoreCase("G05") || auftrag.getBaureihe().equalsIgnoreCase("G06")) && auftrag.containsSA("04UR")) || (auftrag.getBaureihe().equals("RR11") || auftrag.getBaureihe().equals("RR12") || auftrag.getBaureihe().equals("RR31"))) || ((!(auftrag.containsSA("04UR")) && (auftrag.getBaureihe().equalsIgnoreCase("G20") || auftrag.getBaureihe().equalsIgnoreCase("G21") || auftrag.getBaureihe().equalsIgnoreCase("G24") || auftrag.getBaureihe().equalsIgnoreCase("G28") || auftrag.getBaureihe().equalsIgnoreCase("J29"))) || (auftrag.getBaureihe().equals("RR11") || auftrag.getBaureihe().equals("RR12") || auftrag.getBaureihe().equals("RR31"))) || (!(auftrag.getAnzahlT�ren().equalsIgnoreCase("2")) && (auftrag.getBaureihe().equalsIgnoreCase("F93") || auftrag.containsSA("04NB"))) || ((auftrag.containsSA("04FL") && !(auftrag.containsSA("06FH"))) || (auftrag.getBaureihe().equalsIgnoreCase("G28") || (!(auftrag.getBaureihe().startsWith("G2")) && !(auftrag.getBaureihe().startsWith("F4")) && !(auftrag.getBaureihe().equalsIgnoreCase("G05")) && !(auftrag.getBaureihe().equalsIgnoreCase("G06")) && !(auftrag.getBaureihe().equalsIgnoreCase("G07")) && !(auftrag.getBaureihe().equalsIgnoreCase("G14")) && !(auftrag.getBaureihe().equalsIgnoreCase("G15"))) || ((auftrag.getBaureihe().startsWith("G2") || auftrag.getBaureihe().startsWith("F4")) && auftrag.containsSA("0493")) || (auftrag.getBaureihe().equalsIgnoreCase("G07") && ((auftrag.getIStufeJahrInt()==18 && auftrag.getIStufeMonatInt()==11 && auftrag.getIStufeNummerInt()>=400) || (auftrag.getIStufeJahrInt()==19 && auftrag.getIStufeMonatInt()==3 && auftrag.getIStufeNummerInt()>=400) || (auftrag.getIStufeJahrInt()==19 && auftrag.getIStufeMonatInt()>3) || auftrag.getIStufeJahrInt()>19))))))
ppinit_222();
if( ((auftrag.getBaureihe().equalsIgnoreCase("G07") || auftrag.getBaureihe().equalsIgnoreCase("G16") || auftrag.getBaureihe().equalsIgnoreCase("G28") || ((auftrag.getBaureihe().equalsIgnoreCase("G05") || auftrag.getBaureihe().equalsIgnoreCase("G06")) && auftrag.containsSA("04UR"))) || (!(auftrag.getAnzahlT�ren().equalsIgnoreCase("2")) && (auftrag.getBaureihe().equalsIgnoreCase("F93") || auftrag.containsSA("04NB")))))
ppinit_223();

}
private void ppinit_224() throws PruefprozedurNotAvailableException {

}
private void ppinit_225() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_FAHRZEUG_UND_ICOM_WECKEN", true );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Open door and activate ignition.":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"T�r �ffnen, Z�ndung einschalten.":"T�r �ffnen, Z�ndung einschalten.")) );
	pp.setArgs( "STYLE" + "=" + "1" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_ZUENDUNG_EIN", true );
	pp.setLocalName( "DE", "WQ_ZUENDUNG_EIN" );
	pp.setLocalName( "EN", "WQ_IGNITION_ON" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Igniton on and check ICOMP OKAY?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Z�ndung;einschalten und ICOMP IO?":"Z�ndung;einschalten und ICOMP IO?")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}

}
private void ppinit_226() throws PruefprozedurNotAvailableException {

}
private void ppinit_227() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_ECOS_KLIMA_AUS", true );
	pp.setLocalName( "DE", "WQ_ECOS_KLIMA_AUS" );
	pp.setLocalName( "EN", "WQ_ECOS_AC_OF" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Switch off AC air vent compl!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Klima L�ftung kompl.;ausschalten!":"Klima L�ftung kompl.;ausschalten!")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}

}
private void ppinit_228() throws PruefprozedurNotAvailableException {

}
private void ppinit_229() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_LADEGERAET_ANSCHLIESSEN", true );
	pp.setLocalName( "DE", "WQ_LADEGERAET_ANSCHLIESSEN" );
	pp.setLocalName( "EN", "WQ_CONNECT_BATTERY_CHARGER" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"ATTENTION: Connect;Battery Charger!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Achtung: Ladeger�t;anschliessen!":"Achtung: Ladeger�t;anschliessen!")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_LADEGERAET_ENTFERNEN", true );
	pp.setLocalName( "DE", "WQ_LADEGERAET_ENTFERNEN" );
	pp.setLocalName( "EN", "WQ_DISCONNECT_BATTERY_CHARGER" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Disconnect;Battery Charger!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Achtung: Ladeger�t;entfernen!":"Achtung: Ladeger�t;entfernen!")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}

}
private void ppinit_230() throws PruefprozedurNotAvailableException {

}
private void ppinit_231() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_KEINE_PRUEFUNG_SCHLOSS", true );
	pp.setLocalName( "DE", "WQ_KEINE_PRUEFUNG_SCHLOSS" );
	pp.setLocalName( "EN", "WQ_NO_CHECK_KEY" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"No check of mech.;keys":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Keine Pr�fung mech.;Schloss":"Keine Pr�fung mech.;Schloss")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerAnweisung", "WA_TAKE_MECH_KEY_AND_REMOTE", true );
	pp.setLocalName( "DE", "WA_MECHAN_SCHLUESSEL_UND_FFB_ENTNEHMEN" );
	pp.setLocalName( "EN", "WA_TAKE_MECH_KEY_AND_REMOTE" );
	pp.setArgs( "DAUER" + "=" + "3000" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Take mechanical key":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Mech. Schl�ssel nehmen":"Take mechanical key")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerAnweisung", "WA_LEAVE_MECH_KEY_AND_REMOTE", true );
	pp.setLocalName( "DE", "WA_MECHAN_SCHLUESSEL_UND_FFB_LASSEN" );
	pp.setLocalName( "EN", "WA_LEAVE_MECH_KEY_AND_REMOTE" );
	pp.setArgs( "DAUER" + "=" + "3000" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Leave the key in the car":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Schl�ssel im Fzg lassen":"Leave the key in the car")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WC_TAKE_MECH_KEY_AND_REMOTE", true );
	pp.setLocalName( "DE", "WQ_MECHAN_SCHLUESSEL_UND_FFB_ENTNEHMEN" );
	pp.setLocalName( "EN", "WC_TAKE_MECH_KEY_AND_REMOTE" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Take mechanical key":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"mech. Schl�ssel und":"Take mechanical key")) + ";" +(CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"and remote":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"FFB entnehmen":"and remote")) );
	pp.setArgs( "STYLE" + "=" + "1" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WC_LEAVE_MECH_KEY_AND_REMOTE", true );
	pp.setLocalName( "DE", "WQ_MECHAN_SCHLUESSEL_UND_FFB_LASSEN" );
	pp.setLocalName( "EN", "WC_LEAVE_MECH_KEY_AND_REMOTE" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Leave the key in the car":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Schl�ssel im Fzg lassen":"Schl�ssel im Fzg lassen")) );
	pp.setArgs( "STYLE" + "=" + "1" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WC_PLACE_MECH_KEY_AND_REMOTE", true );
	pp.setLocalName( "DE", "WQ_MECHAN_SCHLUESSEL_UND_FFB_ABLEGEN" );
	pp.setLocalName( "EN", "WC_PLACE_MECH_KEY_AND_REMOTE" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Place mech. key and":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"mech. Schl�ssel und":"Place mech. key and")) + ";" +(CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Remote Keys in car":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"FFB in Fzg. ablegen":"Remote Keys in car")) );
	pp.setArgs( "STYLE" + "=" + "1" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WA_REMOVE_KEY_LOCK_COVER", true );
	pp.setLocalName( "DE", "WA_ABDECK_ENTFERNEN" );
	pp.setLocalName( "EN", "WA_REMOVE_KEY_LOCK_COVER" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Please remove;key lock cover!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Abdeck. Tuerschloss; entfernen!":"Please remove;key lock cover!")) );
	pp.setArgs( "STYLE" + "=" + "1" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WA_FIX_KEY_LOCK_COVER", true );
	pp.setLocalName( "DE", "WA_ABDECK_ANBRINGEN" );
	pp.setLocalName( "EN", "WA_FIX_KEY_LOCK_COVER" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Please fix;key lock cover!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Abdeck. Tuerschloss;anbringen!":"Please fix;key lock cover!")) );
	pp.setArgs( "STYLE" + "=" + "1" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WC_REMOVE_KEY_LOCK_COVER", true );
	pp.setLocalName( "DE", "WQ_ABDECK_ENTFERNEN" );
	pp.setLocalName( "EN", "WC_REMOVE_KEY_LOCK_COVER" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Please remove;key lock cover!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Abdeck. Tuerschloss; entfernen!":"Please remove;key lock cover!")) );
	pp.setArgs( "STYLE" + "=" + "1" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WC_FIX_KEY_LOCK_COVER", true );
	pp.setLocalName( "DE", "WQ_ABDECK_ANBRINGEN" );
	pp.setLocalName( "EN", "WC_FIX_KEY_LOCK_COVER" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Please fix;key lock cover!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Abdeck. Tuerschloss;anbringen!":"Please fix;key lock cover!")) );
	pp.setArgs( "STYLE" + "=" + "1" );

}

}
private void ppinit_232() throws PruefprozedurNotAvailableException {

}
private void ppinit_233() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_ACHTUNG_POWER_DOWN", true );
	pp.setLocalName( "DE", "WQ_ACHTUNG_POWER_DOWN" );
	pp.setLocalName( "EN", "WQ_ATTENTION_POWER_DOWN" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Attention: POWERDOWN Do not touch the car!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Achtung: POWERDOWN Klappen und T�ren nicht bet�tigen!":"Achtung: POWERDOWN Klappen und T�ren nicht bet�tigen!")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_AUF_BUSRUHE_WARTEN", true );
	pp.setLocalName( "DE", "WQ_AUF_BUSRUHE_WARTEN" );
	pp.setLocalName( "EN", "WQ_WAIT_BUSINACTIVITY" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Achtung Fahrzeug einschlafen lassen":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Achtung Fahrzeug einschlafen lassen":"Achtung Fahrzeug einschlafen lassen")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_POWER_DOWN_NOK", true );
	pp.setLocalName( "DE", "WQ_POWER_DOWN_NOK" );
	pp.setLocalName( "EN", "WQ_POWER_DOWN_NOK" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Attention: POWERDOWN NOK! Please repeat the pruefumfang!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Achtung: POWERDOWN NOK! Pr�fumfang bitte wiederholen!":"Achtung: POWERDOWN NOK! Pr�fumfang bitte wiederholen!")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}

}
private void ppinit_234() throws PruefprozedurNotAvailableException {

}
private void ppinit_235() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_CLOSE_ALL_WINDOWS", true );
	pp.setLocalName( "DE", "WQ_ALLE_FENSTER_SCHLIESSEN" );
	pp.setLocalName( "EN", "WQ_CLOSE_ALL_WINDOWS" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Be careful all windows will be closed.":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Achtung, alle Fenster werden geschlossen.":"Be careful all windows will be closed.")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_NO_SLEEP_CURRENT_MEASUREMENT", true );
	pp.setLocalName( "DE", "WQ_KEINE_RUHESTROMPRUEFUNG" );
	pp.setLocalName( "EN", "WQ_NO_SLEEP_CURRENT_MEASUREMENT" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"NO SLEEP CURRENT MEASUREMENT DONE.":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"KEINE RUHESTROMPR�FUNG DURCHGEF�HRT.":"NO SLEEP CURRENT MEASUREMENT DONE.")) );
	pp.setArgs( "STYLE" + "=" + "1" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_WAKE_UP_VEHICLE_AND_ICOM", true );
	pp.setLocalName( "DE", "WQ_FAHRZEUG_UND_ICOM_WECKEN" );
	pp.setLocalName( "EN", "WQ_WAKE_UP_VEHICLE_AND_ICOM" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Please open the door and switch on the ignition.":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Bitte T�r �ffnen und Z�ndung einschalten.":"Please open the door and switch on the ignition.")) );
	pp.setArgs( "STYLE" + "=" + "1" );

}

}
private void ppinit_236() throws PruefprozedurNotAvailableException {

}
private void ppinit_237() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_SICHTPRUEFUNG", true );
	pp.setLocalName( "DE", "WQ_SICHTPRUEFUNG" );
	pp.setLocalName( "EN", "WQ_VISUAL_CHECK" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Attention!;Visual check!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Achtung!;Sichtpr�fung!":"Achtung!;Sichtpr�fung!")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}

}
private void ppinit_238() throws PruefprozedurNotAvailableException {

}
private void ppinit_239() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_START_IM_FAHRZEUG_FS_WITH_KEYS", true );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Sit in Front Driver seat and place keys back into vehicle.":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"EINSTEIGEN FAHRERSEITE MIT SCHL�SSEL":"EINSTEIGEN FAHRERSEITE MIT SCHL�SSEL")) );
	pp.setArgs( "STYLE" + "=" + "3" );
	pp.setArgs( "TITEL" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"INSTRUCTION":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"ANWEISUNG":"ANWEISUNG")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_START_IM_FAHRZEUG_BS", true );
	pp.setLocalName( "DE", "WQ_START_IM_FAHRZEUG_BS" );
	pp.setLocalName( "EN", "WQ_START_IN_VEHICLE_PASSENGER_SIDE" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"START INTO VEHICLE;PASSENGER SIDE":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"START IM FAHRZEUG; BEIFAHRERSEITE":"START IM FAHRZEUG; BEIFAHRERSEITE")) );
	pp.setArgs( "STYLE" + "=" + "3" );
	pp.setArgs( "TITEL" + "=" + "ANWEISUNG" );

}

}
private void ppinit_240() throws PruefprozedurNotAvailableException {

}
private void ppinit_213() throws PruefprozedurNotAvailableException {
ppinit_214();
ppinit_215();
ppinit_216();
ppinit_217();
ppinit_218();
ppinit_219();
ppinit_224();
ppinit_225();
ppinit_226();
ppinit_227();
ppinit_228();
ppinit_229();
ppinit_230();
ppinit_231();
ppinit_232();
ppinit_233();
ppinit_234();
ppinit_235();
ppinit_236();
ppinit_237();
ppinit_238();
ppinit_239();
ppinit_240();

}
private void ppinit_241() throws PruefprozedurNotAvailableException {

}
private void ppinit_34() throws PruefprozedurNotAvailableException {
ppinit_35();
ppinit_36();
ppinit_37();
ppinit_38();
ppinit_43();
ppinit_44();
ppinit_210();
ppinit_211();
ppinit_212();
ppinit_213();
ppinit_241();

}
private void ppinit_242() throws PruefprozedurNotAvailableException {

}
private void ppinit_2() throws PruefprozedurNotAvailableException {
ppinit_3();
ppinit_4();
ppinit_5();
ppinit_6();
ppinit_7();
ppinit_8();
ppinit_22();
ppinit_23();
ppinit_31();
ppinit_32();
ppinit_33();
ppinit_34();
ppinit_242();

}
private void ppinit_243() throws PruefprozedurNotAvailableException {

}
private void ppinit_245() throws PruefprozedurNotAvailableException {

}
private void ppinit_246() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_OLD_PU_VERSION", true );
	pp.setArgs( "AWT" + "=" + "ES WIRD EIN VERALTETER PR�FUMFANG VERWENDET. BITTE SYSTEMBETREUER INFORMIEREN UND AUF ECOS_W0 UMSTELLEN!!!" );
	pp.setArgs( "STYLE" + "=" + "3" );

}

}
private void ppinit_247() throws PruefprozedurNotAvailableException {

}
private void ppinit_250() throws PruefprozedurNotAvailableException {

}
private void ppinit_249() throws PruefprozedurNotAvailableException {
ppinit_250();

}
private void ppinit_248() throws PruefprozedurNotAvailableException {
if( auftrag.getOrderWerk().equalsIgnoreCase("01.01"))
ppinit_249();

}
private void ppinit_251() throws PruefprozedurNotAvailableException {

}
private void ppinit_254() throws PruefprozedurNotAvailableException {

}
private void ppinit_256() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "VORSERIE", true );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(RICHTIG,FALSCH)" );
	pp.setArgs( "DEBUG" + "=" + "FALSE" );
	pp.setArgs( "JOB1_HWT1" + "=" + "Kein Vorserienfahrzeug" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_SGBD" + "=" + "NONE" );

}

}
private void ppinit_257() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "VORSERIE", true );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(RICHTIG,RICHTIG)" );
	pp.setArgs( "DEBUG" + "=" + "FALSE" );
	pp.setArgs( "JOB1_HWT1" + "=" + "Kein Vorserienfahrzeug" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_SGBD" + "=" + "NONE" );

}

}
private void ppinit_255() throws PruefprozedurNotAvailableException {
if( getAttribut("VORSERIE").equals("TRUE"))
ppinit_256();
else
ppinit_257();

}
private void ppinit_258() throws PruefprozedurNotAvailableException {

}
private void ppinit_259() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_LADEGERAET_EIN", true );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Switch on Batterycharger!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Ladegeraet einschalten!":"Ladegeraet einschalten!")) );

}

}
private void ppinit_260() throws PruefprozedurNotAvailableException {

}
private void ppinit_262() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerEingabe", "WE_BEIPACK_ORDERNR_PRUEFEN", true );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Barcode Ordernr Beipack einlesen!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Barcode Ordernr Beipack einlesen!":"Barcode Ordernr Beipack einlesen!")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "SubString", "STATUS_BEIPACK_ORDERNUMMER", true );
	pp.setLocalName( "DE", "STATUS_BEIPACK_ORDERNUMMER" );
	pp.setLocalName( "EN", "STATUS_BEIPACK_ORDERNUMMER" );
	pp.setArgs( "INSTRING" + "=" + "INPUT@WE_BEIPACK_ORDERNR_PRUEFEN" );
	pp.setArgs( "STARTINDEX" + "=" + "2" );
	pp.setArgs( "STOPINDEX" + "=" + "9" );
	pp.setArgs( "COMPARESTRING" + "=" + auftrag.getOrderId() );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Odernummer Beipack passt nicht zum Auftrag!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Odernummer Beipack passt nicht zum Auftrag!":"Odernummer Beipack passt nicht zum Auftrag !")) );

}

}
private void ppinit_261() throws PruefprozedurNotAvailableException {
if( ((auftrag.getBaureihe().equalsIgnoreCase("G30") && !(auftrag.containsSX("0007"))) || auftrag.getBaureihe().equalsIgnoreCase("G31") || auftrag.getBaureihe().equalsIgnoreCase("G32") || auftrag.getBaureihe().equalsIgnoreCase("F34") || auftrag.getBaureihe().equalsIgnoreCase("F36")))
ppinit_262();

}
private void ppinit_263() throws PruefprozedurNotAvailableException {

}
private void ppinit_265() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "WARTEZEIT_5S_N52T", true );
	pp.setArgs( "DAUER" + "=" + "5000" );
	pp.setArgs( "HWT" + "=" + "Leider nochmal 5s" );

}

}
private void ppinit_266() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_TUERKNOEPFE_VORN_UND_HINTEN_UNTEN", true );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"All Door Knob DD;down?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Alle T�rkn�pfe;unten?":"All Door Knob DD;down?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_TUERKNOEPFE_VORN_UND_HINTEN_OBEN", true );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"All Door Knob DD;up?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Alle T�rkn�pfe;oben?":"All Door Knob DD;up?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_TUERKNOEPFE_VORN_UND_HINTEN_FA_UNTEN", true );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"All Door Knob Driverside down?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Alle T�rkn�pfe Fahrerseite unten?":"Alle T�rkn�pfe Fahrerseite unten?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_TUERKNOEPFE_VORN_UND_HINTEN_FA_OBEN", true );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"All Door Knob driverside up?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Alle T�rkn�pfe Fahrerseite oben?":"Alle T�rkn�pfe Fahrerseite oben?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_TUERKNOEPFE_VORN_UND_HINTEN_BF_UNTEN", true );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"All Door Knob passengerside down?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Alle T�rkn�pfe BFseite unten?":"Alle T�rkn�pfe BFseite unten?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_TUERKNOEPFE_VORN_UND_HINTEN_BF_OBEN", true );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"All Door Knob passengerside up?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Alle T�rkn�pfe BFseite oben?":"Alle T�rkn�pfe BFseite oben?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}

}
private void ppinit_268() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_TANKKLAPPE_GESCHLOSSEN_LL", true );
	pp.setLocalName( "EN", "WF_FUEL_FILLER_FLAP_CLOSED_LHD" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Fuel filler flap;closed? y/n":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Tankklappe;geschlossen ?":"Fuel filler flap;closed? y/n")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_TANKKLAPPE_GEOEFFNET_LL", true );
	pp.setLocalName( "EN", "WF_FUEL_FILLER_FLAP_OPEN_LHD" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Fuel filler flap;opened? y/n":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Tankklappe;ge�ffnet ?":"Fuel filler flap;opened? y/n")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}

}
private void ppinit_269() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_TANKKLAPPE_GESCHLOSSEN_RL", true );
	pp.setLocalName( "EN", "WF_FUEL_FILLER_FLAP_CLOSED_RHD" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Fuel filler flap;closed? y/n":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Tankklappe;geschlossen ?":"Fuel filler flap;closed? y/n")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_TANKKLAPPE_GEOEFFNET_RL", true );
	pp.setLocalName( "EN", "WF_FUEL_FILLER_FLAP_OPEN_RHD" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Fuel filler flap;opened? y/n":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Tankklappe;ge�ffnet ?":"Fuel filler flap;opened? y/n")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}

}
private void ppinit_267() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_TUEREN_GESCHLOSSEN", true );
	pp.setLocalName( "DE", "WQ_TUEREN_GESCHLOSSEN" );
	pp.setLocalName( "EN", "WQ_TUEREN_GESCHLOSSEN" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Alle T�ren; schliessen!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Alle T�ren; schliessen!":"Alle T�ren; schliessen!")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}
if( (auftrag.getFahrerPosition().equals("LL") || auftrag.getL�nderVariante().equals("US")))
ppinit_268();
else
ppinit_269();

}
private void ppinit_264() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "PRUEFUNG_ENDE", true );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + "Pr�fung ENDE?" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerAnweisung", "FALSCHE_FG_NUMMER", true );
	pp.setArgs( "DAUER" + "=" + "5000" );
	pp.setArgs( "AWT" + "=" + "Falsche Fahrgestellnummer" + ";" +"CASCADE wird beendet!" );

}
if( (auftrag.getMotorBaureihe().startsWith("N52") && !(auftrag.containsSX("0007"))))
ppinit_265();
if( (auftrag.getBaureihe().startsWith("F9") || auftrag.getBaureihe().startsWith("G")))
ppinit_266();
if( (auftrag.getBaureihe().equalsIgnoreCase("G30") || auftrag.getBaureihe().equalsIgnoreCase("G31") || auftrag.getBaureihe().equalsIgnoreCase("G32") || auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12")))
ppinit_267();
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_TUER_BFS_SCHLOSS_FALSCH_VERBAUT", true );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Door Passengerside; No lock assembled?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"TUER BFS; KEIN SCHLOSS VERBAUT?":"TUER BFS; KEIN SCHLOSS VERBAUT?")) );
	pp.setArgs( "TITEL" + "=" + "SICHTPRUEFUNG" );

}

}
private void ppinit_270() throws PruefprozedurNotAvailableException {

}
private void ppinit_272() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagToleranz", "STEUERN_KLEMME_KL15_AKTIVIEREN_N52", true );
	pp.setArgs( "SGBD" + "=" + "CAS4_2" );
	pp.setArgs( "JOB" + "=" + "STEUERN_KLEMMEN" );
	pp.setArgs( "JOBPAR" + "=" + "KL15_EIN" );
	pp.setArgs( "TIMEOUT" + "=" + "1000" );
	pp.setArgs( "PAUSE" + "=" + "100" );
	pp.setArgs( "IGNORE_ERRORS" + "=" + "TRUE" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "WARTEZEIT_3S_KL15_AKTIVIEREN", true );
	pp.setArgs( "DAUER" + "=" + "3000" );

}

}
private void ppinit_274() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "FUSSSTUETZEN", true );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Add foot rests !!!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Fussst�tzen beilegen !!!":"Fussst�tzen beilegen !!!")) );
	pp.setArgs( "DAUER" + "=" + "2400000" );
	pp.setArgs( "TITEL" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"INSTRUCTION":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"ANWEISUNG":"ANWEISUNG")) );

}

}
private void ppinit_275() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "FUSSRAUMMATTEN", true );
	pp.setArgs( "TITEL" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Visual check":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Sichtpr�fung":"Sichtpr�fung")) );
	pp.setArgs( "DAUER" + "=" + "2400000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Footwell mats added?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"FUSSRAUMMATTEN BEIGELEGT?":"FUSSRAUMMATTEN BEIGELEGT?")) );

}

}
private void ppinit_273() throws PruefprozedurNotAvailableException {
if( auftrag.getBaureihe().equals("F02"))
ppinit_274();
if( auftrag.containsSA("0423"))
ppinit_275();

}
private void ppinit_276() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_DATENLOGGER_AKTIVIEREN", true );
	pp.setArgs( "AWT" + "=" + "Datenlogger auf" + ";" +"Pr�fzone laden" );
	pp.setArgs( "STYLE" + "=" + "3" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_DATENLOGGER_DEAKTIVIEREN", true );
	pp.setArgs( "AWT" + "=" + "Datenlogger auf" + ";" +"ECOS Akkubetrieb" );
	pp.setArgs( "STYLE" + "=" + "3" );

}

}
private void ppinit_271() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_HANDSCHUHFACHBELEUCHTUNG", true );
	pp.setArgs( "TITEL" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Visual Check":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Sichtpr�fung":"Sichtpr�fung")) );
	pp.setArgs( "DAUER" + "=" + "240000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Glovebox light OK?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Handschuhfach Beleuchtung IO?":"Handschuhfach Beleuchtung IO?")) );

}
if( (auftrag.getMotorBaureihe().startsWith("N52") || auftrag.getMotorBaureihe().startsWith("N47") || auftrag.getMotorBaureihe().startsWith("B47") || auftrag.getMotorBaureihe().startsWith("N57")))
ppinit_272();
if( (auftrag.getBaureihe().equals("F01") || auftrag.getBaureihe().equals("F02") || auftrag.getBaureihe().equals("F03")))
ppinit_273();
if( auftrag.containsSA("0909"))
ppinit_276();
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WF_START_IM_FAHRZEUG_BS", true );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Start Test on passenger side":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"START IM FAHRZEUG BEIFAHRERSEITE":"START IM FAHRZEUG BEIFAHRERSEITE")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_START_IM_FAHRZEUG_FS", true );
	pp.setArgs( "DAUER" + "=" + "2700000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Enter car on drviver side":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"EINSTEIGEN FAHRERSEITE":"EINSTEIGEN FAHRERSEITE")) );
	pp.setArgs( "TITEL" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"INSTRUCTION":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"ANWEISUNG":"ANWEISUNG")) );

}

}
private void ppinit_277() throws PruefprozedurNotAvailableException {

}
private void ppinit_279() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_KUEHLBOX_LED_CHECK", true );
	pp.setArgs( "DAUER" + "=" + "2300000" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Switch on icebox. Is the LED on/blinking?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"K�hlbox einschalten, leuchtet/blinkt LED?":"K�hlbox einschalten, leuchtet/blinkt LED?")) );

}

}
private void ppinit_278() throws PruefprozedurNotAvailableException {
if( auftrag.containsSA("0791"))
ppinit_279();

}
private void ppinit_280() throws PruefprozedurNotAvailableException {

}
private void ppinit_282() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_FAHRPEDALMODUL_OHNE_KICKDOWN", true );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"push through gas pedal; NO Kick-down assembled?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Gaspedal durchdr�cken;KEIN Kickdown verbaut?":"Gaspedal durchdr�cken;KEIN Kickdown verbaut?")) );
	pp.setArgs( "DAUER" + "=" + "360000" );

}

}
private void ppinit_281() throws PruefprozedurNotAvailableException {
if( ((auftrag.getBaureihe().equalsIgnoreCase("F10") || auftrag.getBaureihe().equalsIgnoreCase("F11") || auftrag.getBaureihe().equalsIgnoreCase("F12") || auftrag.getBaureihe().equalsIgnoreCase("F13")) && !(auftrag.containsSA("0205")) && !(auftrag.containsSA("02TB")) && !(auftrag.containsSA("02MK")) && !(auftrag.containsSA("02TE")) && !(((auftrag.getBaureihe().equals("F12") || auftrag.getBaureihe().equals("F13")) && (auftrag.getIStufeJahrInt()>15 || (auftrag.getIStufeMonatInt()>=3 && auftrag.getIStufeJahrInt()==15)) && !(auftrag.containsSA("02MA"))))))
ppinit_282();

}
private void ppinit_283() throws PruefprozedurNotAvailableException {

}
private void ppinit_285() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_MAL_DECKEL", true );
	pp.setArgs( "FT" + "=" + "MAL Ablagefach; Deckel vo + hi i.O.?" );
	pp.setArgs( "DAUER" + "=" + "360000" );

}

}
private void ppinit_284() throws PruefprozedurNotAvailableException {
if( ((auftrag.getBaureihe().equalsIgnoreCase("G30") && auftrag.containsSX("0007")) || auftrag.getBaureihe().equalsIgnoreCase("F90")))
ppinit_285();

}
private void ppinit_286() throws PruefprozedurNotAvailableException {

}
private void ppinit_288() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_SKITASCHE_BEILEGEN", true );
	pp.setArgs( "FT" + "=" + "Ski-Tasche in Gep�ckraummulde beilegen!" );
	pp.setArgs( "DAUER" + "=" + "360000" );

}

}
private void ppinit_287() throws PruefprozedurNotAvailableException {
if( auftrag.containsSA("04UY"))
ppinit_288();

}
private void ppinit_289() throws PruefprozedurNotAvailableException {

}
private void ppinit_292() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_VERSANDSCHUTZ", true );
	pp.setArgs( "AWT" + "=" + "Abdeckung Abschlepp" + ";" +"oese entfernen!" );
	pp.setArgs( "STYLE" + "=" + "3" );

}

}
private void ppinit_293() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_HAUPTSCHEINWERFER_STOPFEN_VERBAUT", true );
	pp.setArgs( "DAUER" + "=" + "360000" );
	pp.setArgs( "FT" + "=" + "US Blindstopfen ! F�r Scheinwerfer verbaut?" );

}

}
private void ppinit_294() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_HAUPTSCHEINWERFER_STOPFEN_VERBAUT", true );
	pp.setArgs( "DAUER" + "=" + "360000" );
	pp.setArgs( "FT" + "=" + "ECE Blindstopfen ! F�r Scheinwerfer verbaut?" );

}

}
private void ppinit_291() throws PruefprozedurNotAvailableException {
if( auftrag.containsSA("0925"))
ppinit_292();
if( auftrag.getL�nderVariante().equals("USA"))
ppinit_293();
if( auftrag.getL�nderVariante().equals("ECE"))
ppinit_294();

}
private void ppinit_290() throws PruefprozedurNotAvailableException {
if( ((auftrag.getBaureihe().equals("F10") && !(auftrag.containsSX("0007"))) || auftrag.getBaureihe().equals("F07") || auftrag.getBaureihe().equals("F11") || auftrag.getBaureihe().equals("F34") || auftrag.getBaureihe().equals("F36")))
ppinit_291();

}
private void ppinit_295() throws PruefprozedurNotAvailableException {

}
private void ppinit_297() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ARMLEHNENHEIZUNG_FAS_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}

}
private void ppinit_298() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ARMLEHNENHEIZUNG_FAS_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_296() throws PruefprozedurNotAvailableException {
if( (auftrag.getBaureihe().startsWith("G") && (auftrag.containsSA("04HB") || auftrag.containsSA("04HC"))))
ppinit_297();
else
ppinit_298();

}
private void ppinit_300() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ARMLEHNENHEIZUNG_FAHS_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}

}
private void ppinit_301() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "ARMLEHNENHEIZUNG_FAHS_VORHANDEN", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_299() throws PruefprozedurNotAvailableException {
if( (auftrag.getBaureihe().startsWith("G") && auftrag.containsSA("04HC")))
ppinit_300();
else
ppinit_301();

}
private void ppinit_302() throws PruefprozedurNotAvailableException {

}
private void ppinit_304() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "SICHTPR�FUNG_SCHLOSSNUSS", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}

}
private void ppinit_305() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "SICHTPR�FUNG_SCHLOSSNUSS", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_303() throws PruefprozedurNotAvailableException {
if( ((auftrag.getBaureihe().startsWith("F0") || auftrag.getBaureihe().startsWith("F1")) && ((auftrag.getIStufeMonatInt()>=11 && auftrag.getIStufeJahrInt()==15) || auftrag.getIStufeJahrInt()>15)))
ppinit_304();
else
ppinit_305();

}
private void ppinit_306() throws PruefprozedurNotAvailableException {

}
private void ppinit_308() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_LUFTAUSTROEMER_MECH_B_SAEULE_FAS", true );
	pp.setLocalName( "DE", "WF_LUFTAUSTROEMER_MECH_B_SAEULE_FAS" );
	pp.setLocalName( "EN", "WF_LUFTAUSTROEMER_MECH_B_SAEULE_FAS" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Luftgitter FAS; B S�ule mechan. u. Luft IO?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Luftgitter FAS; B S�ule mechan. u. Luft IO?":"Luftgitter FAS; B S�ule mechan. u. Luft IO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "WF_LUFTAUSTROEMER_MECH_B_SAEULE_BFS", true );
	pp.setLocalName( "DE", "WF_LUFTAUSTROEMER_MECH_B_SAEULE_BFS" );
	pp.setLocalName( "EN", "WF_LUFTAUSTROEMER_MECH_B_SAEULE_BFS" );
	pp.setArgs( "FT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Luftgitter BFS; B S�ule mechan. u. Luft  IO?":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Luftgitter BFS; B S�ule mechan. u. Luft IO?":"Luftgitter BFS; B S�ule mechan. u. Luft IO?")) );
	pp.setArgs( "DAUER" + "=" + "300000" );

}

}
private void ppinit_307() throws PruefprozedurNotAvailableException {
if( ((auftrag.getBaureihe().equalsIgnoreCase("G11") || auftrag.getBaureihe().equalsIgnoreCase("G12")) || ((auftrag.getBaureihe().equalsIgnoreCase("F90") || auftrag.getBaureihe().equalsIgnoreCase("G30") || auftrag.getBaureihe().equalsIgnoreCase("G31") || auftrag.getBaureihe().equalsIgnoreCase("G32")) && auftrag.containsSA("04NB"))))
ppinit_308();

}
private void ppinit_309() throws PruefprozedurNotAvailableException {

}
private void ppinit_311() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerEingabe", "SCAN_SKID_NR", true );
	pp.setLocalName( "DE", "SCAN_SKID_NR" );
	pp.setLocalName( "EN", "SCAN_SKID_NR" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Bitte die SKID-Nummer scannen!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Bitte die SKID-Nummer scannen!":"Bitte die SKID-Nummer scannen!")) );
	pp.setArgs( "MIN" + "=" + "SKID0%" );

}

}
private void ppinit_310() throws PruefprozedurNotAvailableException {
if( auftrag.getBaureihe().startsWith("G"))
ppinit_311();

}
private void ppinit_312() throws PruefprozedurNotAvailableException {

}
private void ppinit_314() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "PRUEFUNG_OHNE_FFB1_FFB2", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(1,1)" );

}

}
private void ppinit_315() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagData", "PRUEFUNG_OHNE_FFB1_FFB2", true );
	pp.setArgs( "JOBN_SGBD" + "=" + "NONE" );
	pp.setArgs( "JOB1_JOB" + "=" + "NONE" );
	pp.setArgs( "JOB1_CONDITION1" + "=" + "EQUALS(0,1)" );

}

}
private void ppinit_313() throws PruefprozedurNotAvailableException {
if( (auftrag.containsSX("00K5") && auftrag.getBaureihe().startsWith("G")))
ppinit_314();
else
ppinit_315();

}
private void ppinit_316() throws PruefprozedurNotAvailableException {

}
private void ppinit_318() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "DiagToleranz", "INT_HECKKLAPPE_ZU", true );
	pp.setArgs( "SGBD" + "=" + "JBBF3" );
	pp.setArgs( "JOB" + "=" + "STATUS_LESEN" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Close Trunklid":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Heckklappe schliessen":"Heckklappe schliessen")) );
	pp.setArgs( "JOBPAR" + "=" + "ARG" + ";" +"KONTAKT_HECKKLAPPE_EIN" );
	pp.setArgs( "RESULT1" + "=" + "STAT_KONTAKT_HECKKLAPPE_EIN" );
	pp.setArgs( "MIN1" + "=" + "1" );
	pp.setArgs( "CANCEL" + "=" + "TRUE" );
	pp.setArgs( "TIMEOUT" + "=" + "240000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Contact or Switch Trunklid defective":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Kontakt oder Taster Heckklappe defekt":"Kontakt oder Taster Heckklappe defekt")) );

}

}
private void ppinit_317() throws PruefprozedurNotAvailableException {
if( ((auftrag.containsSA("03AH") || auftrag.containsSA("05DL")) && (auftrag.getBaureihe().startsWith("F0") || auftrag.getBaureihe().startsWith("F1"))))
ppinit_318();

}
private void ppinit_319() throws PruefprozedurNotAvailableException {

}
private void ppinit_320() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "WARTEZEIT_100MS", true );
	pp.setLocalName( "DE", "WARTEZEIT_100MS" );
	pp.setLocalName( "EN", "WAIT_100MS" );
	pp.setArgs( "DAUER" + "=" + "100" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "WARTEZEIT_200MS", true );
	pp.setLocalName( "DE", "WARTEZEIT_200MS" );
	pp.setLocalName( "EN", "WAIT_200MS" );
	pp.setArgs( "DAUER" + "=" + "200" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "WARTEZEIT_500MS", true );
	pp.setLocalName( "DE", "WARTEZEIT_500MS" );
	pp.setLocalName( "EN", "WAIT_500MS" );
	pp.setArgs( "DAUER" + "=" + "500" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "WARTEZEIT_1S", true );
	pp.setLocalName( "DE", "WARTEZEIT_1S" );
	pp.setLocalName( "EN", "WAIT_1S" );
	pp.setArgs( "DAUER" + "=" + "1000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Waiting time 1s":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Wartezeit 1s":"Waiting time 1s")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "WARTEZEIT_2S", true );
	pp.setLocalName( "DE", "WARTEZEIT_2S" );
	pp.setLocalName( "EN", "WAIT_2S" );
	pp.setArgs( "DAUER" + "=" + "2000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Waiting time 2s":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Wartezeit 2s":"Waiting time 2s")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "WARTEZEIT_3S", true );
	pp.setLocalName( "DE", "WARTEZEIT_3S" );
	pp.setLocalName( "EN", "WAIT_3S" );
	pp.setArgs( "DAUER" + "=" + "3000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Waiting time 3s":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Wartezeit 3s":"Waiting time 3s")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "WARTEZEIT_5S", true );
	pp.setLocalName( "DE", "WARTEZEIT_5S" );
	pp.setLocalName( "EN", "WAIT_5S" );
	pp.setArgs( "DAUER" + "=" + "5000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Waiting time 5s":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Wartezeit 5s":"Waiting time 5s")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "WARTEZEIT_10S", true );
	pp.setLocalName( "DE", "WARTEZEIT_10S" );
	pp.setLocalName( "EN", "WAIT_10S" );
	pp.setArgs( "DAUER" + "=" + "10000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Waiting time 10s":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Wartezeit 10s":"Waiting time 10s")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "WARTEZEIT_20S", true );
	pp.setLocalName( "DE", "WARTEZEIT_20S" );
	pp.setLocalName( "EN", "WAIT_20S" );
	pp.setArgs( "DAUER" + "=" + "20000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Waiting time 20s":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Wartezeit 20s":"Waiting time 20s")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "WARTEZEIT_30S", true );
	pp.setLocalName( "DE", "WARTEZEIT_30S" );
	pp.setLocalName( "EN", "WAIT_30S" );
	pp.setArgs( "DAUER" + "=" + "30000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Waiting time 30s":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Wartezeit 30s":"Waiting time 30s")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "WARTEZEIT_40S", true );
	pp.setLocalName( "DE", "WARTEZEIT_40S" );
	pp.setLocalName( "EN", "WAIT_40S" );
	pp.setArgs( "DAUER" + "=" + "40000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Waiting time 40s":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Wartezeit 40s":"Waiting time 40s")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "WARTEZEIT_60S", true );
	pp.setLocalName( "DE", "WARTEZEIT_60S" );
	pp.setLocalName( "EN", "WAIT_60S" );
	pp.setArgs( "DAUER" + "=" + "60000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Waiting time 60s":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Wartezeit 60s":"Waiting time 60s")) );

}

}
private void ppinit_321() throws PruefprozedurNotAvailableException {

}
private void ppinit_322() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "Pause", "TEST_HT_WARTEZEIT_10S", true );
	pp.setLocalName( "DE", "WARTEZEIT_10S" );
	pp.setLocalName( "EN", "WAIT_10S" );
	pp.setArgs( "DAUER" + "=" + "10000" );
	pp.setArgs( "HWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Waiting time 10s":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Wartezeit 10s":"Waiting time 10s")) );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerFrage", "TEST_HT_WF_SKITASCHE_BEILEGEN", true );
	pp.setArgs( "FT" + "=" + "Ski-Tasche in Gep�ckraummulde beilegen!" );
	pp.setArgs( "DAUER" + "=" + "360000" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerEingabe", "TEST_HT_SCAN_AIRBAG_LABEL", true );
	pp.setLocalName( "DE", "TEST_HT_SCAN_AIRBAG_LABEL" );
	pp.setLocalName( "EN", "TEST_SCAN_AIRBAG_LABEL" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Please scan the airbag label!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Bitte das Airbaglabel scannen!":"Please scan the airbag label!")) );
	pp.setArgs( "MIN" + "=" + "6949923%" + ";" +"02694992301" + ";" +"02694992302" + ";" +"02694992303" + ";" +"02694992304" + ";" +"02694992305" + ";" +"02694992306" + ";" +"02694992307" + ";" +"02694992308" + ";" +"02694992309" + ";" +"02694992310" + ";" +"026949923067" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "TEST_HT_WQ_FZG_VERLASSEN", true );
	pp.setLocalName( "DE", "TEST_HT_WQ_FZG_VERLASSEN" );
	pp.setLocalName( "EN", "TEST_HT_WQ_LEAVE_CAR" );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"please leave;the car":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Fahrzeug;verlassen":"Fahrzeug;verlassen")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}

}
private void ppinit_323() throws PruefprozedurNotAvailableException {

}
private void ppinit_253() throws PruefprozedurNotAvailableException {
ppinit_254();
ppinit_255();
ppinit_258();
ppinit_259();
ppinit_260();
ppinit_261();
ppinit_263();
ppinit_264();
ppinit_270();
ppinit_271();
ppinit_277();
ppinit_278();
ppinit_280();
ppinit_281();
ppinit_283();
ppinit_284();
ppinit_286();
ppinit_287();
ppinit_289();
ppinit_290();
ppinit_295();
ppinit_296();
ppinit_299();
ppinit_302();
ppinit_303();
ppinit_306();
ppinit_307();
ppinit_309();
ppinit_310();
ppinit_312();
ppinit_313();
ppinit_316();
ppinit_317();
ppinit_319();
ppinit_320();
ppinit_321();
ppinit_322();
ppinit_323();

}
private void ppinit_252() throws PruefprozedurNotAvailableException {
if( auftrag.getOrderWerk().startsWith("02"))
ppinit_253();

}
private void ppinit_324() throws PruefprozedurNotAvailableException {

}
private void ppinit_327() throws PruefprozedurNotAvailableException {

}
private void ppinit_326() throws PruefprozedurNotAvailableException {
ppinit_327();

}
private void ppinit_325() throws PruefprozedurNotAvailableException {
if( auftrag.getOrderWerk().startsWith("06"))
ppinit_326();

}
private void ppinit_328() throws PruefprozedurNotAvailableException {

}
private void ppinit_331() throws PruefprozedurNotAvailableException {

}
private void ppinit_330() throws PruefprozedurNotAvailableException {
ppinit_331();

}
private void ppinit_329() throws PruefprozedurNotAvailableException {
if( auftrag.getOrderWerk().startsWith("07"))
ppinit_330();

}
private void ppinit_332() throws PruefprozedurNotAvailableException {

}
private void ppinit_335() throws PruefprozedurNotAvailableException {

}
private void ppinit_334() throws PruefprozedurNotAvailableException {
ppinit_335();

}
private void ppinit_333() throws PruefprozedurNotAvailableException {
if( auftrag.getOrderWerk().startsWith("09"))
ppinit_334();

}
private void ppinit_336() throws PruefprozedurNotAvailableException {

}
private void ppinit_339() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerQuittung", "WQ_DOOR_CAP_REMINDER", true );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Take Door Handle Key Caps from GloveBox. Place in Center Console.":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Fahrzeug;verlassen":"Fahrzeug;verlassen")) );
	pp.setArgs( "STYLE" + "=" + "3" );

}

}
private void ppinit_338() throws PruefprozedurNotAvailableException {
if( (auftrag.getBaureihe().equalsIgnoreCase("F15") || auftrag.getBaureihe().equalsIgnoreCase("F16") || auftrag.getBaureihe().equalsIgnoreCase("F85") || auftrag.getBaureihe().equalsIgnoreCase("F86")))
ppinit_339();

}
private void ppinit_337() throws PruefprozedurNotAvailableException {
if( auftrag.getOrderWerk().startsWith("10"))
ppinit_338();

}
private void ppinit_340() throws PruefprozedurNotAvailableException {

}
private void ppinit_343() throws PruefprozedurNotAvailableException {

}
private void ppinit_342() throws PruefprozedurNotAvailableException {
ppinit_343();

}
private void ppinit_341() throws PruefprozedurNotAvailableException {
if( auftrag.getOrderWerk().equalsIgnoreCase("19.10"))
ppinit_342();

}
private void ppinit_344() throws PruefprozedurNotAvailableException {

}
private void ppinit_347() throws PruefprozedurNotAvailableException {

}
private void ppinit_346() throws PruefprozedurNotAvailableException {
ppinit_347();

}
private void ppinit_345() throws PruefprozedurNotAvailableException {
if( auftrag.getOrderWerk().equalsIgnoreCase("19.88"))
ppinit_346();

}
private void ppinit_348() throws PruefprozedurNotAvailableException {

}
private void ppinit_350() throws PruefprozedurNotAvailableException {
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerEingabe", "PRUEFERNUMMER_EINGEBEN", true );
	pp.setArgs( "AWT" + "=" + (CascadeProperties.getLanguage().equalsIgnoreCase( "EN" )?"Please scan your;operator ID!":(CascadeProperties.getLanguage().equalsIgnoreCase( "DE" )?"Bitte Pr�fernummer;einscannen!":"Bitte Pr�fernummer;einscannen!")) );

}

}
private void ppinit_349() throws PruefprozedurNotAvailableException {
if( auftrag.getOrderWerk().startsWith("34"))
ppinit_350();

}
private void ppinit_351() throws PruefprozedurNotAvailableException {

}
private void ppinit_354() throws PruefprozedurNotAvailableException {

}
private void ppinit_353() throws PruefprozedurNotAvailableException {
ppinit_354();

}
private void ppinit_352() throws PruefprozedurNotAvailableException {
if( auftrag.getOrderWerk().startsWith("50"))
ppinit_353();

}
private void ppinit_355() throws PruefprozedurNotAvailableException {

}
private void ppinit_244() throws PruefprozedurNotAvailableException {
ppinit_245();
ppinit_246();
ppinit_247();
ppinit_248();
ppinit_251();
ppinit_252();
ppinit_324();
ppinit_325();
ppinit_328();
ppinit_329();
ppinit_332();
ppinit_333();
ppinit_336();
ppinit_337();
ppinit_340();
ppinit_341();
ppinit_344();
ppinit_345();
ppinit_348();
ppinit_349();
ppinit_351();
ppinit_352();
ppinit_355();

}
private void ppinit_356() throws PruefprozedurNotAvailableException {

}

}

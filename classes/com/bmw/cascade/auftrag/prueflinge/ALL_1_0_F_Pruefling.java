/**
 * ALL_1_0_F_Pruefling.java
 *
 * automatisch erzeugt �ber AdminGui
 * Datum: Thu Mar 01 12:37:09 CET 2018
 */

package com.bmw.cascade.auftrag.prueflinge;

import com.bmw.cascade.*;
import com.bmw.cascade.auftrag.*;
import com.bmw.cascade.auftrag.report.FilterKonfigurationException;
import com.bmw.cascade.pruefstand.*;
import com.bmw.cascade.pruefprozeduren.*;

/*
 * DIESER KOMMENTAR WIRD AUTOMATISCH ERZEUGT UND GELESEN
 * NICHT VER�NDERN!
 * HISTORIE_XML START
<HISTORY version="2">
 <MANUAL_COMMENT date="01.03.2018 12:37" version="1_0_F" author="ADMIN / Default Admin - Default Admin, please replace as soon as possible">
 <COMMENT></COMMENT>
 </MANUAL_COMMENT>
</HISTORY>
 * HISTORIE_XML END
 */
/**
 * Pr�fling ALL
 */
public class ALL_1_0_F_Pruefling extends AbstractBMWPruefling {
static final long serialVersionUID = 1L;
  /**
  * DefaultKonstruktor, nur fuer die Deserialisierung
  */
 public ALL_1_0_F_Pruefling() {}

  /**
   * Konstruktur
   *
   * @param auftrag Der Auftrag, zu dem dieser Pr�fling instantiiert werden soll.
   */
  public ALL_1_0_F_Pruefling( Auftrag auftrag ) throws PrueflingNotAvailableException, PruefprozedurNotAvailableException {
    super( auftrag );
}
  /**
   * Liefert symbolischen Namen des Pr�flings
   *
   * @return Symbolischer Name des Pr�flings
   */
  protected String getPName() {
    return("ALL");
  }
  /**
   * Pr�ft Verf�gbarkeit des Pr�flings
   * �blicherweise wird das Ergebnis aus den Fahrzeugdaten oder durch die Existenz anderer Pr�flinge gewonnen
   */
  protected void checkAvailability() throws PrueflingNotAvailableException {
/*
 * DIESER KOMMENTAR WIRD AUTOMATISCH ERZEUGT UND GELESEN
 * NICHT VER�NDERN!
 * EXISTENCE_XML START
<EXISTENCE version="2">
 <TRUE/>
 </EXISTENCE>
 * EXISTENCE_XML END
 */
	   // FOLGENDER CODE WIRD AUTOMATISCH NEU ERZEUGT,
	   // NICHT �NDERN
	   // EXISTENCE_CODE START
if( 
 true )
	return;
else
	throw new PrueflingNotAvailableException( getName() );

	// EXISTENCE_CODE END
}
  /**
   * Initialisiert die Attribute
   */
  protected void attributeInit() {
/*
 * DIESER KOMMENTAR WIRD AUTOMATISCH ERZEUGT UND GELESEN
 * NICHT VER�NDERN!
 * ATTRIBUTDEFINITION_XML START
<ATTRIBUTDEFINITION version="2">
</ATTRIBUTDEFINITION>
 * ATTRIBUTDEFINITION_XML END
 */
	   // FOLGENDER CODE WIRD AUTOMATISCH NEU ERZEUGT,
	   // NICHT �NDERN
	   // ATTRIBUTDEFINITION_CODE START
super.attributeInit();
}

	// ATTRIBUTDEFINITION_CODE END

  /**
   * Initialisieren der Pr�fprozeduren
   */
  protected void pr�fprozedurenInit() throws PruefprozedurNotAvailableException {
    super.pr�fprozedurenInit();
/*
 * DIESER KOMMENTAR WIRD AUTOMATISCH ERZEUGT UND GELESEN
 * NICHT VER�NDERN!
 * PRUEFPROZEDURDEFINITION_XML START
<PRUEFPROZEDUREN version="2">
 <LOADPP fileheader="WerkerAnweisungUDN" name="WA_HELLOWORLD">
  <COMMENT></COMMENT>
  <PPARGS name="DAUER" required="true">
   <CONSTANT type="String" value="2000">
    <COMMENT></COMMENT>
   </CONSTANT>
  </PPARGS>
  <PPARGS name="AWT" required="true">
   <CONSTANT type="String" value="Hello World">
    <COMMENT></COMMENT>
   </CONSTANT>
  </PPARGS>
 </LOADPP>
 <LOADPP fileheader="WerkerAnweisung" name="WA_HELLOCLP">
  <COMMENT></COMMENT>
  <PPARGS name="DAUER" required="true">
   <CONSTANT type="String" value="2000">
    <COMMENT></COMMENT>
   </CONSTANT>
  </PPARGS>
  <PPARGS name="AWT" required="true">
   <CONSTANT type="String" value="Hello CLP">
    <COMMENT></COMMENT>
   </CONSTANT>
  </PPARGS>
 </LOADPP>
</PRUEFPROZEDUREN>
 * PRUEFPROZEDURDEFINITION_XML END
 */	   // FOLGENDER CODE WIRD AUTOMATISCH NEU ERZEUGT,
	   // NICHT �NDERN
	   // PRUEFPROZEDURDEFINITION_CODE START
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerAnweisungUDN", "WA_HELLOWORLD", true );
	pp.setArgs( "DAUER" + "=" + "2000" );
	pp.setArgs( "AWT" + "=" + "Hello World" );

}
{
	Pruefprozedur pp = ladePruefprozedur( "WerkerAnweisung", "WA_HELLOCLP", true );
	pp.setArgs( "DAUER" + "=" + "2000" );
	pp.setArgs( "AWT" + "=" + "Hello CLP" );

}
}

}

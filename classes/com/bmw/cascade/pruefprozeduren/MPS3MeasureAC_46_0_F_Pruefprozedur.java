/*
 * MPS3Init_Cards
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.appframework.logging.*;
import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.mps3.*;
import com.bmw.cascade.util.logging.*;
import com.bmw.cascade.pruefstand.*;

/**
 * Diese Pr�fprozedut initialisiert die Messkarten im Multifunktionspr�fstand. 
 * 
 * @author BMW TI-431 Burger
 * @version V_1_0 11.11.2008 AB Implementierung <br>
 * @version V_2_0 14.01.2001 AB Diverse �nderungen <br>
 * @version V_7_0 16.06.2009 AB Fehleraufl�sungen hinzugef�gt <br>
 * @version V_8_0 24.06.2009 AB Fehler Namensaufl�sung Messkan�le korrigiert <br>
 * @version V_9_0 26.06.2009 AB static finals entfernt, Widerstand hinzugef�gt <br>
 * @version V_10_0 13.07.2009 AB releaseMPS3Card() ausgef�hrt <br>
 * @version V_11_0 14.07.2009 AB Widerstand auf Float ge�ndert <br>
 * @version V_12_0 15.07.2009 AB Fehler- und Ergebnisausgabe angepasst. <br>
 * @version V_13_0 05.08.2009 AB Initialie Grenzen angepasst <br>
 * @version V_15_0 09.08.2009 AB Wartezeiten optimiert <br> 
 * @version V_16_0 26.08.2009 AB Deutsche Texte implementiert <br>
 * @version V_17_0 17.09.2009 AB Close card in Close channel ge�ndert
 * 								 Ergebnisse (Sprache) bereinigt)
 * 								 Ergebnisse im NIO-Zweig erg�nzt
 * 								 Polling nach CTRL_OFF erg�nzt
 * 								 Pause gefixt <br> 
 * @version V_18_0 29.09.2009 AB F-Version 
 * @version V_19_0 02.10.2009 AB Komponentenname in Fehlertext integriert <br>
 * @version V_20_0 05.10.2009 AB T-Version f�r Oxford (unver�ndert) <br>
 * @version V_21_0 05.10.2009 AB F-Version nach T-Version (unver�ndert) <br>
 * @version V_22_0 07.10.2009 AB Cancel implementiert <br>
 * @version V_23_0 07.10.2009 AB F-Version nach T-Version (unver�ndert) <br> 
 * @version V_24_0 16.10.2009 AB Timeouts erh�ht <br>
 * @version V_25_0 28.10.2009 AB Ergebnisse korrigiert <br>
 * @version V_26_0 30.10.2009 AB nicht implementierte Args enfternt: GND_TAG, GND_CARD <br>
 * @version V_27_0 30.10.2009 AB T-Version <br>
 * @version V_28_0 09.11.2009 AB Fehlerausgabe 0x8000 (Busy), Fehlerausgabe beim Empfangen, Fehlerausgabe beim Schlie�en eingebaut <br>
 * @version V_29_0 09.11.2009 AB F-Version <br>
 * @version V_30_0 29.01.2010 AB Fehlermeldungen angepasst, Warte 500ms bei Wiederholung eingebaut, Fehlerausgabe bei "STOP" entfernt <br>
 * @version V_31_0 09.08.2010 AB F-Version <br>
 * @version V_32_0 12.08.2010 AB R�ckmeldung aus Musterbau: Fehlermeldungen stimmten nicht. Korrigiert. <br>
 * @version V_33_0 19.08.2010 AB Fehlerhandling weiter optimiert <br>
 * @version V_34_0 19.08.2010 AB Layer-8-Problem korrigiert <br>
 * @version V_35_0 12.05.2011 TM Korrektur Fehlerausgabe (Kanal --> Port) <br>
 * @version V_36_0 14.06.2011 TM ADD: Optionales Argument 'AFFECTED_COMPONENT' f�r System-Fehler <br>
 * @version V_37_0 01.08.2011 TM F-Version <br>
 * @version V_38_0 19.01.2014 TM T version: Fixed error message (Flasch --> Falsche) <br>
 * @version V_39_0 24.02.2015 TM F version <br>
 * @version V_40_0 02.07.2015 TM CHA: Added 'user dialogue active' check to user dialogue cancelling request <br>
 * @version V_41_0 08.07.2015 TM CHA: Fixed p_max parameter setting <br>
 * @version V42_0_T 04.08.2015 TM T version
 * @version V43_0_F 07.08.2015 TM F version
 * @version V44_0_T 13.12.2016 AR T Version: changed maximum tolerance of optional parameters PHASE_MIN/MAX to 360 (from 180) <br>
 * @version V45_0_T 13.12.2016 PR T-Version: An zwei Stellen b_NoMeasError in b_NoResultError ge�ndert (bei Phasenverschiebung zu hoch/niedig), da andernfalls NIO-Ergebnisse generiert werden, die PP aber mit STATUS_EXECUTION_OK endet (Fehler in Dingolfing vom 13.12.2016) <br>
 * @version V46_0_F 13.12.2016 AR F-Version: Default Value of PHASE_MAX changed to 360.<br>
 */
public class MPS3MeasureAC_46_0_F_Pruefprozedur extends AbstractBMWPruefprozedur 
{
	static final long serialVersionUID = 1L;
	
	int i_Debug = 0;
	
	String s_AffectedComponent = "";
	
	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public MPS3MeasureAC_46_0_F_Pruefprozedur() 
	{
	}

	private Logger pruefstandLogger = CascadeLogging.getLogger( "PruefstandLogger" );
	
	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public MPS3MeasureAC_46_0_F_Pruefprozedur ( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) 
	{
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() 
	{		
		super.attributeInit();	
	}

	/**
	 * liefert die optionalen Argumente
	 * 
     * GND_CHANNEL: 	MKn			Zweiter Messkanal, der mit 0V beaufschlagt wird (Motore, ...)
     * 					n = Messkanal 1..4
     * DELAY: EInschwingzeit vor Messbeginn in ms
     * I_MIN: gemessene Mindestromaufnahme in mA
     * I_MAX: gemessene Maximalstromaufnahme in mA
     * U_MIN: gemessene Mindestspannung in mV
     * U_MAX: gemessene Maximalspannung in mV
     * R_MIN: errechneter Mindestswiderstand in Ohm
     * R_MAX: errechneter Maximalwiderstand in Ohm 
     * TIMEOUT: Timeout, bis Messwert erreicht
     * DEBUG: Debug-Level 0, 1, 2, 3 (0 = off) 
     * PAUSE: Wartezeit nach der Ausf�hrung
     * AWT_TEXT: Text f�r AWT
     * AWT_TITLE: Titel f�r AWT
     * AWT_TIME: Timeout f�r AWT in ms 
     * AWT_STYLE: 1 = Status, 2 = Error, 99 = Alarm, Rest = Standard
     * FWT_TEXT: Text f�r FWT
     * FWT_TITLE: Titel f�r FWT
     * FWT_TIME: Timeout f�r FWT in ms 
     * QWT_TEXT: Text f�r QWT
     * QWT_TITLE: Titel f�r QWT
     * QWT_TIME: Timeout f�r QWT in ms 
     * QWT_STYLE: 1 = Status, 2 = Error, 99 = Alarm, Rest = Standard 
     * COMPONENT: Parametrierbarer Teil des Fehlertextes
     * AFFECTED_COMPONENT: Name der zu pr�fenden Komponente
	 */
	public String[] getOptionalArgs() 
	{
		String[] args = 
		{
				"DELAY",		
				"I_MIN",
				"I_MAX",
				"U_MIN",
				"U_MAX",
				"R_MIN",
				"R_MAX",
				"PHASE_MIN",
				"PHASE_MAX",				
				"TIMEOUT",
				"DEBUG",
				"PAUSE",				
				"AWT_TEXT",
				"AWT_TITLE",	
				"AWT_STYLE",
				"AWT_TIME",
				"AWT_CANCEL",	
				"FWT_TEXT",
				"FWT_TITLE",
				"FWT_TIME",	
				"QWT_TEXT",
				"QWT_TITLE",
				"QWT_STYLE",	
				"QWT_TIME",  
				"COMPONENT",
				"AFFECTED_COMPONENT"
	    };
		
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 * 
     * TAG: Bezeichnung der anzusteuernden Messkarte wie in Pr�fstandskonfig.
     * MEAS_CHANNEL: 	MKnm_Vx		Messkanal, �ber den Strom und Spannung gemessen wird
     * 					n = Messkanal 1..4
     * 					m = Pfad B..C (B = 1R bis 1.5A, C = 4mR bis 25A)
     * 					x = Stromverst�rkung 1, 2, 5 oder 10)
     * VOLTAGE: Einzustellende Spannung (0V, 3500..13500mV) in mV
     * ILIM: Stromgrenze in mA  
     * DURATION: Messzeit in ms
	 */
	public String[] getRequiredArgs() 
	{
		String[] args = 
		{
				"TAG",
				"MEAS_CHANNEL",
				"FREQUENZ",
				"DURATION"												
	    };
		
		return args;
	}

	/**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die parent-Methode aufgrund
     * der offenen Anzahl an Results
	 */	
	public boolean checkArgs() 
	{		
		boolean b_NoError = true;
		
		if (getArg ("TAG").length() <= 0)
			b_NoError = false;
		if (getArg ("MEAS_CHANNEL").length() <= 0)
			b_NoError = false;	
		if (getArg ("FREQUENZ").length() <= 0)
			b_NoError = false;
		if (getArg ("DURATION").length() <= 0)
			b_NoError = false;			

		return b_NoError;
    }
	
	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) 
	{	
		//Result vars
		Vector v_ErgListe = new Vector();	
		int i_Status = STATUS_EXECUTION_ERROR;
		
		//Hardware vars
		DeviceManager c_DevMan = this.getPr�flingLaufzeitUmgebung().getDeviceManager();
		CardURI c_Card = null;
		Helper c_Helper = new Helper ();
		
		//Message vars
		IPDefs c_DefIp = new IPDefs ();
		MsgDefs c_DefMsg = new MsgDefs ();
		MsgProcessor c_Msg = new MsgProcessor ();
		int i_MeasPort = 0;		
		int i_MeasHwid = 0;
		TcpClient c_MeasChannel = null;
		MsgReceiver c_Receiver = null;
    	
    	//System vars
		Hashtable ht_Args = getArgs();
		Enumeration en_Keys = ht_Args.keys();		
		String s_Arg = null;        
		String s_Val = null;
		boolean b_NoExecError = true;
		boolean b_NoMeasError = true;
		boolean b_NoResultError = true;
		boolean b_NoCancel = true;
		
		//local measuring vars
		String s_Tag = "";
		String s_MeasChannel = "";
		String s_Component = "";
		int i_Frequenz = 0;
		int i_Ilim = 0;
		int i_Delay = 0;
		int i_Duration = 0;
		int i_Imin = 0;
		int i_Imax = 25000;
		int i_Umin = 0;
		int i_Umax = 15000;
		float f_Rmin = 0;
		float f_Rmax = Float.POSITIVE_INFINITY;
		int i_PhaseMin = 0;
		int i_PhaseMax = 360;		
		int i_MeasErr = 0;
		int i_MeasU = 0;
		int i_MeasI = 0;
		float f_CalcR = 0;
		int i_MeasPhase = 0;		
    	
    	//timing vars
        long l_StartTime = 0;
        long l_StopTime = 0;		
    	int i_Timeout = 0;
    	int i_Pause = 0;
    	boolean b_Receiving = true;
    	
    	//local AWT vars
    	String s_AwtText = "";
    	String s_AwtTitle = PB.getString("anweisung");	
    	int i_AwtTime = -1;
    	int i_AwtStyle = 0;
    	boolean b_AwtCancel = false;
    	boolean b_AwtBusy = false;
    	
    	//local fwt vars
    	String s_FwtText = "";
    	String s_FwtTitle = PB.getString("frage");
    	int i_FwtTime = -1;
    	boolean b_FwtBusy = false;
    	boolean b_FwtResult = false;
    	
    	//local qwt vars
    	String s_QwtText = "";
    	String s_QwtTitle = PB.getString("meldung");
    	int i_QwtTime = -1;
    	int i_QwtStyle = 0;
    	boolean b_QwtBusy = false; 
    	
		//clear result list
		v_ErgListe.clear();
		//check args
		//checkArgs();
		//init logger
		c_Helper.logInit(i_Debug);	
		
		c_Helper.logDebug(2, "�berpr�fe Parameter", "check parameters", this.getClass());
		
    	if (checkArgs () == false)
    	{
    		c_Helper.logDebug(0, "Mindestens ein zwingender Parameter fehlt.", "At least one required parameter is not supplied.", this.getClass());    		
    		pruefstandLogger.logC( LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one required parameter is not supplied.");
    		if (isDE())    			
    			i_Status = setSystemError (MsgDefs.ERR_WRONG_ARGS, "Pr�fe Konfiguration Pr�fling", "Karte (Parameter) erforderlich", "Mindestens ein erforderlicher Parameter wurde nicht �bergeben.", v_ErgListe);	
    		else
    			i_Status = setSystemError (MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "Card (Parameter) required", "At least one required parameter is not supplied.", v_ErgListe);
    		setPPStatus (info, i_Status, v_ErgListe);
    		return;
    	}
		
    	while (en_Keys.hasMoreElements())
		{
			//get name and value from next parameter
			s_Arg = en_Keys.nextElement().toString();
			//s_Val = getDynamicAttribute(en_Args.nextElement().toString());
			s_Val = getDynamicAttribute(ht_Args.get(s_Arg).toString());//.nextElement().toString());
			
			c_Helper.logDebug(2, "Arg: " + s_Arg + " Val: " + s_Val, "Arg: " + s_Arg + "Val: " + s_Val, this.getClass());
		
			//check parameter "DEBUG"
			if (s_Arg.equalsIgnoreCase("DEBUG"))
			{
				//set to level 0, if no value supplied or set to "false"
				if ((s_Val == null) || (s_Val.equalsIgnoreCase("false")))
					i_Debug = 0;
				else
					//set to level 1, if supplied "true"
					if (s_Val.equalsIgnoreCase("true"))
						i_Debug = 1;
					else						
					{
						try
						{
							//try to set desired level
							i_Debug = Integer.parseInt(s_Val);
						}
						//catch numberformatexception
						catch (NumberFormatException e)
						{
							i_Debug = 0;
							pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"DEBUG\" has wrong parameter format. Accepts only integer.", e );
						}
					}				
			}
					
			//Get timeout, if supplied
			if (s_Arg.equalsIgnoreCase("TIMEOUT"))
			{
				try
				{
					//try to set desired level
					i_Timeout = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_Timeout = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"TIMEOUT\" has wrong parameter format. Accepts only integer.", e );
				}	
			}
			
			//Get pause, if supplied
			if (s_Arg.equalsIgnoreCase("PAUSE"))
			{
				try
				{
					//try to set desired level
					i_Pause = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_Pause = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"PAUSE\" has wrong parameter format. Accepts only integer.", e );
				}	
			}
			
			//get AWT vals
			if (s_Arg.equalsIgnoreCase("AWT_TEXT"))
				s_AwtText = s_Val;
			
			if (s_Arg.equalsIgnoreCase("AWT_TITLE"))
				s_AwtTitle = s_Val;			

			if (s_Arg.equalsIgnoreCase("AWT_STYLE"))
			{
				try
				{
					//try to set desired time
					i_AwtStyle = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_AwtStyle = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"AWT_STYLE\" has wrong parameter format. Accepts only integer.", e );
				}					
			}
			
			if (s_Arg.equalsIgnoreCase("AWT_CANCEL"))
			{
				if (s_Val.equalsIgnoreCase("T") || s_Val.equalsIgnoreCase("TRUE") || s_Val.equalsIgnoreCase("1"))
					b_AwtCancel = true;
			}
			
			if (s_Arg.equalsIgnoreCase("AWT_TIME"))
			{
				try
				{
					if (Integer.parseInt(s_Val) < 1000)
						i_AwtTime = -1;
					else
					//try to get awt timeout
						i_AwtTime = Integer.parseInt(s_Val) / 1000;
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_AwtTime = -1;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"AWT_TIME\" has wrong parameter format. Accepts only integer.", e );
				}				
			}
			
			//get FWT vals
			if (s_Arg.equalsIgnoreCase("FWT_TEXT"))
				s_FwtText = s_Val;
			
			if (s_Arg.equalsIgnoreCase("FWT_TITLE"))
				s_FwtTitle = s_Val;			

			if (s_Arg.equalsIgnoreCase("FWT_TIME"))
			{
				try
				{
					if (Integer.parseInt(s_Val) < 1000)
						i_FwtTime = -1;
					else
					//try to get awt timeout
						i_FwtTime = Integer.parseInt(s_Val) / 1000;
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_FwtTime = -1;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"FWT_TIME\" has wrong parameter format. Accepts only integer.", e );
				}				
			}
			
			//get QWT vals
			if (s_Arg.equalsIgnoreCase("QWT_TEXT"))
				s_QwtText = s_Val;
			
			if (s_Arg.equalsIgnoreCase("QWT_TITLE"))
				s_QwtTitle = s_Val;			

			if (s_Arg.equalsIgnoreCase("QWT_STYLE"))
			{
				try
				{
					//try to set desired time
					i_QwtStyle = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_QwtStyle = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"QWT_STYLE\" has wrong parameter format. Accepts only integer.", e );
				}					
			}
			
			if (s_Arg.equalsIgnoreCase("QWT_TIME"))
			{
				try
				{
					if (Integer.parseInt(s_Val) < 1000)
						i_QwtTime = -1;
					else
					//try to get awt timeout
						i_QwtTime = Integer.parseInt(s_Val) / 1000;
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_QwtTime = -1;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"QWT_TIME\" has wrong parameter format. Accepts only integer.", e );
				}				
			}
			
			//Add card to list, if supplied
			if (s_Arg.toUpperCase().startsWith("TAG"))
				s_Tag = s_Val;
			
			//String must be MKnm_Vx or MKnm_Vxx
			if (s_Arg.equalsIgnoreCase("MEAS_CHANNEL"))
			{
				boolean b_NoError = true;
				//
				b_NoError = s_Val.startsWith("MK");				

				if (b_NoError)
					if ((s_Val.charAt(2) == '1') || (s_Val.charAt(2) == '2') || (s_Val.charAt(2) == '3') || (s_Val.charAt(2) == '4'))
						b_NoError = true;
					else
						b_NoError = false;
				
				if (b_NoError)
					if (s_Val.charAt(3) == 'A')
						b_NoError = true;
					else
						b_NoError = false;
				
				if (b_NoError)
					b_NoError = s_Val.substring(4, 6).equals("_V");

				if (s_Val.length() == 7)				
					if (b_NoError)
						if ((s_Val.charAt(6) == '1') || (s_Val.charAt(6) == '2') || (s_Val.charAt(6) == '5'))
							b_NoError = true;
						else
							b_NoError = false;
				else
					if (b_NoError)
						if ((s_Val.charAt(6) == '1') || (s_Val.charAt(7) == '0'))
							b_NoError = true;
						else
							b_NoError = false;		
				
				if (b_NoError)
				{
					s_MeasChannel = s_Val;
				}
				else
				{
		    		c_Helper.logDebug(0, "\"MEAS_CHANNEL\" hat falsche Syntax: " + s_Val, "\"MEAS_CHANNEL\" has wrong syntax: " + s_Val, this.getClass());    		
		    		pruefstandLogger.logC( LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "\"MEAS_CHANNEL\" has wrong syntax: " + s_Val);
		    		if (isDE())
		    			i_Status = setSystemError (MsgDefs.ERR_WRONG_ARGS, "Pr�fe Konfiguration vom Pr�fling", "", "\"MEAS_CHANNEL\" hat falsche Syntax: " + s_Val, v_ErgListe);	
		    		else
		    			i_Status = setSystemError (MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "", "\"MEAS_CHANNEL\" has wrong syntax: " + s_Val, v_ErgListe);
		    		setPPStatus (info, i_Status, v_ErgListe);
		    		return;										
				}
			}
			
			if (s_Arg.equalsIgnoreCase("FREQUENZ"))
			{
				try
				{
					i_Frequenz = Integer.parseInt(s_Val);
					if (i_Frequenz < 500)
						i_Frequenz = 500;
					if (i_Frequenz > 20000)
						i_Frequenz = 20000;
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
		    		c_Helper.logDebug(0, "\"VOLTAGE\" hat falsche Syntax: " + s_Val, "\"VOLTAGE\" has wrong syntax: " + s_Val, this.getClass());    		
		    		pruefstandLogger.logC( LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "\"VOLTAGE\" has wrong syntax: " + s_Val);
		    		if (isDE())
		    			i_Status = setSystemError (MsgDefs.ERR_WRONG_ARGS, "Pr�fe Konfiguration vom Pr�fling", "", "\"VOLTAGE\" hat falsche Syntax: " + s_Val, v_ErgListe);
		    		else
		    			i_Status = setSystemError (MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "", "\"VOLTAGE\" has wrong syntax: " + s_Val, v_ErgListe);
		    		setPPStatus (info, i_Status, v_ErgListe);
		    		return;					
				}				
			}
						
			if (s_Arg.equalsIgnoreCase("DELAY"))
			{
				try
				{
					i_Delay = Integer.parseInt(s_Val);
					if (i_Delay < 0)
						i_Delay = 0;
					if (i_Delay > 1000)
						i_Delay = 1000;										
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_Delay = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"DELAY\" has wrong parameter format. Accepts only integer.", e );				
				}				
			}			

			if (s_Arg.equalsIgnoreCase("DURATION"))
			{
				try
				{
					i_Duration = Integer.parseInt(s_Val);
					if (i_Duration < 0)
						i_Duration = 0;
					if (i_Duration > 50000)
						i_Duration = 50000;										
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_Duration = 500;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"DURATION\" has wrong parameter format. Accepts only integer.", e );				
				}				
			}			

			if (s_Arg.equalsIgnoreCase("I_MIN"))
			{
				try
				{
					i_Imin = Integer.parseInt(s_Val);
					if (i_Imin < 0)
						i_Imin = 0;
					if (i_Imin > 25000)
						i_Imin = 25000;										
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_Imin = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"I_MIN\" has wrong parameter format. Accepts only integer.", e );				
				}				
			}			

			if (s_Arg.equalsIgnoreCase("I_MAX"))
			{
				try
				{
					i_Imax = Integer.parseInt(s_Val);
					if (i_Imax < 0)
						i_Imax = 0;
					if (i_Imax > 25000)
						i_Imax = 25000;										
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_Imax = 25000;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"I_MAX\" has wrong parameter format. Accepts only integer.", e );				
				}				
			}
			
			if (s_Arg.equalsIgnoreCase("R_MIN"))
			{
				try
				{
					f_Rmin = Float.parseFloat(s_Val);
					if (f_Rmin < 0)
						f_Rmin = 0;
					if (f_Rmin > 1000000)
						f_Rmin = 1000000;										
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					f_Rmin = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"R_MIN\" has wrong parameter format. Accepts only integer.", e );				
				}				
			}			

			if (s_Arg.equalsIgnoreCase("R_MAX"))
			{
				try
				{
					f_Rmax = Float.parseFloat(s_Val);
					if (f_Rmax < 0)
						f_Rmax = 0;
					if (f_Rmax > 1000000)
						f_Rmax = 1000000;										
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					f_Rmax = 1000000;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"R_MAX\" has wrong parameter format. Accepts only integer.", e );				
				}				
			}			
			
			if (s_Arg.equalsIgnoreCase("U_MIN"))
			{
				try
				{
					i_Umin = Integer.parseInt(s_Val);
					if (i_Umin < 0)
						i_Umin = 0;
					if (i_Umin > 15000)
						i_Umin = 15000;										
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_Umin = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"U_MIN\" has wrong parameter format. Accepts only integer.", e );				
				}				
			}
			
			if (s_Arg.equalsIgnoreCase("U_MAX"))
			{
				try
				{
					i_Umax = Integer.parseInt(s_Val);
					if (i_Umax < 0)
						i_Umax = 0;
					if (i_Umax > 15000)
						i_Umax = 15000;										
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_Umax = 15000;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"U_MAX\" has wrong parameter format. Accepts only integer.", e );				
				}				
			}	
			
			
			if (s_Arg.equalsIgnoreCase("PHASE_MIN"))
			{
				try
				{
					i_PhaseMin = Integer.parseInt(s_Val);
					if (i_PhaseMin < 0)
						i_PhaseMin = 0;
					if (i_PhaseMin > 360)
						i_PhaseMin = 360;										
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_PhaseMin = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"PHASE_MIN\" has wrong parameter format. Accepts only integer.", e );				
				}				
			}
			
			if (s_Arg.equalsIgnoreCase("PHASE_MAX"))
			{
				try
				{
					i_PhaseMax = Integer.parseInt(s_Val);
					if (i_PhaseMax < 0)
						i_PhaseMax = 0;
					if (i_PhaseMax > 360)
						i_PhaseMax = 360;										
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_PhaseMax = 360;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"PHASE_MAX\" has wrong parameter format. Accepts only integer.", e );				
				}				
			}
			
			if (s_Arg.equalsIgnoreCase("COMPONENT"))
				s_Component = s_Val + ": ";
			
			if (s_Arg.equalsIgnoreCase("AFFECTED_COMPONENT"))
				s_AffectedComponent = /*"Affected Component: " + */s_Val + ": ";
		}
    	
    	//Process Measuring-Channel
    	//Build HW-Identifier 
    	i_MeasPort = Integer.parseInt(s_MeasChannel.substring(2, 3));
    	switch (i_MeasPort)
    	{
    		case 1 :
    			i_MeasPort = c_DefIp.MK1;
    			i_MeasHwid = 0x1;
    			break;
    		case 2 :
    			i_MeasPort = c_DefIp.MK2;
    			i_MeasHwid = 0x4;    			
    			break;
    		case 3 :
    			i_MeasPort = c_DefIp.MK3;
    			i_MeasHwid = 0x7;    			
    			break;
    		case 4 :
    			i_MeasPort = c_DefIp.MK4;
    			i_MeasHwid = 0xA;
    			break;
    		default :
    			i_MeasPort = -1;
    			i_MeasHwid = -1;
    			b_NoExecError = false;
    			break;    			
    	}	
    	
		int i_Amp = 0;
		if (s_MeasChannel.length() <= 7)
			i_Amp = Integer.parseInt(s_MeasChannel.substring(6, 7));
		else			
			i_Amp = Integer.parseInt(s_MeasChannel.substring(6, 8));
    	switch (i_Amp)
    	{
    		case 1 :
    			i_MeasHwid += 0x0;
    			break;
    		case 2 :
    			i_MeasHwid += 0x100;    			
    			break;
    		case 5 :
    			i_MeasHwid += 0x200;    			
    			break;
    		case 10 :
    			i_MeasHwid += 0x300;    			
    			break;
    		default : 
    			i_Amp = -1;
    			b_NoExecError = false;
    			break;    			
    	}
    	
		if (b_NoExecError == false)
		{
    		c_Helper.logDebug(0, "\"MEAS_CHANNEL\" hat falsche Syntax.", "\"MEAS_CHANNEL\" has wrong syntax.", this.getClass());    		
    		pruefstandLogger.logC( LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "\"MEAS_CHANNEL\" has wrong syntax.");
    		if (isDE())
    			i_Status = setSystemError (MsgDefs.ERR_WRONG_ARGS, "Pr�fe Konfiguration vom Pr�fling", "", "\"MEAS_CHANNEL\" hat falsche Syntax.", v_ErgListe);	
    		else
    			i_Status = setSystemError (MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "", "\"MEAS_CHANNEL\" has wrong syntax.", v_ErgListe);
    		setPPStatus (info, i_Status, v_ErgListe);
    		return;									
		}	    	

		if ((this.getLastErgebnis() != null) && !this.getLastErgebnis().equals(Ergebnis.FT_IO))
		{
			c_Helper.timer(500);
			pruefstandLogger.logC( LogLevel.INFO, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "Repeat due to error -> wait 500ms");
		} 

    	try
    	{
        	//Connect to measuring channel
        	c_Card = (CardURI) c_DevMan.getMPS3Card(s_Tag);    	
    		c_Card.setDebug(i_Debug);
    		c_Card.connectChannel(Integer.toString(i_MeasPort), "5000");
    		c_MeasChannel = c_Card.getClient(Integer.toString(i_MeasPort));    		    		
		}
		catch (Exception e)
		{
			if (isDE())
				i_Status = setSystemError (MsgDefs.ERR_CHANNEL, "Pr�fe Konfiguration vom Pr�fstand, Karte neu starten", "", "Fehler beim �ffnen von Port: " + i_MeasPort + " auf Karte: " + s_Tag, v_ErgListe);
			else
				i_Status = setSystemError (MsgDefs.ERR_CHANNEL, "Check configuration of pr�fstand, restart card", "", "Error while opening port: " + i_MeasPort + " at card: " + s_Tag, v_ErgListe);
			pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_DEVICE, this.getClass().getName(), "Error while opening port: " + i_MeasPort + " at card: " + s_Tag, "Error while Opening.", e );
			setPPStatus (info, i_Status, v_ErgListe);	
			return;
		}			
		        
        //show AWT
        if (s_AwtText.length() > 0)
        {
        	c_Helper.logDebug(2, "Zeige AWT", "show AWT", this.getClass());
        	
            try 
            {
           		getPr�flingLaufzeitUmgebung().getUserDialog().setAllowCancel(b_AwtCancel);
           		
                if(i_AwtStyle == 99)
                	getPr�flingLaufzeitUmgebung().getUserDialog().displayMessage(s_AwtTitle, s_AwtText, i_AwtTime);
                else 
                	if(i_AwtStyle == 1)
                		getPr�flingLaufzeitUmgebung().getUserDialog().displayStatusMessage(s_AwtTitle, s_AwtText, i_AwtTime);
                else 
                	if(i_AwtStyle == 2)
                		getPr�flingLaufzeitUmgebung().getUserDialog().displayAlertMessage(s_AwtTitle, s_AwtText, i_AwtTime);
                else
                    getPr�flingLaufzeitUmgebung().getUserDialog().displayUserMessage(s_AwtTitle, s_AwtText, i_AwtTime);

                b_AwtBusy = true;            
            } 
            catch (Exception e) 
            {
            	c_Helper.logDebug(0, s_AwtText + " " + e, s_AwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while opening AWT-Dialog: " + s_AwtText, e );            	
            }   
        } 
        
        //show FWT
        int i_FwtTemp = 0;
        if (s_FwtText.length() > 0)
        {
        	c_Helper.logDebug(2, "Zeige FWT", "show FWT", this.getClass());
        	
            try 
            {            	
            	i_FwtTemp = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputDigital(s_FwtTitle, s_FwtText, i_FwtTime);
            	
                //Get FWT result
                if (i_FwtTemp == getPr�flingLaufzeitUmgebung().getUserDialog().YES_KEY)
                	b_FwtResult = true;
                else if (i_FwtTemp == getPr�flingLaufzeitUmgebung().getUserDialog().NO_KEY)
                	b_FwtResult = false;              	

                b_FwtBusy = true;            
            } 
            catch (Exception e) 
            {
            	c_Helper.logDebug(0, s_FwtText + " " + e, s_FwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while opening FWT-Dialog: " + s_FwtText, e );            	
            }   
        }
        
        //show QWT
        if (s_QwtText.length() > 0)
        {
        	c_Helper.logDebug(2, "Zeige QWT", "show QWT", this.getClass());
        	
            try 
            {
                if( i_QwtStyle == 1 )
                    getPr�flingLaufzeitUmgebung().getUserDialog().requestStatusMessage(s_QwtTitle, s_QwtText, i_QwtTime);
                else if( i_QwtStyle == 2 )
                    getPr�flingLaufzeitUmgebung().getUserDialog().requestAlertMessage(s_QwtTitle, s_QwtText, i_QwtTime);
                else
                    getPr�flingLaufzeitUmgebung().getUserDialog().requestUserMessage(s_QwtTitle, s_QwtText, i_QwtTime);            	

                b_QwtBusy = true;            
            } 
            catch (Exception e) 
            {
            	c_Helper.logDebug(0, s_QwtText + " " + e, s_QwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while opening QWT-Dialog: " + s_QwtText, e );            	
            }   
        }
        
        //timoeut surveyed delays within the execution code
        if (i_Timeout > 0)
        {
        	//Measure until result is "green" or timeout expired
        	c_Helper.logDebug(2, "Starte Timeout", "start timeout", this.getClass());        	
        	    		
            l_StopTime = System.currentTimeMillis() + i_Timeout;
            boolean b_Measure = true;
    		while ((l_StopTime > System.currentTimeMillis()) && b_Measure && b_NoCancel)
    		{            
        		c_Helper.logDebug(2, "Starte Messung", "Start Measure", this.getClass());	  

        		ctrlAcChannel (c_MeasChannel, c_Helper, c_DefMsg.ID_CTRL_AC, i_MeasHwid, c_DefMsg.ACT_START, i_Frequenz, i_Delay, i_Duration);
        		
        		b_Receiving = true;        		
        		long l_Timer = System.currentTimeMillis() + 8000 + i_Duration + i_Delay; 
        		while (b_Receiving && (System.currentTimeMillis() < l_Timer))
        		{
        			c_Receiver = c_MeasChannel.getRxMsgByID(c_DefMsg.ID_STAT_AC, true);
        			if (c_Receiver != null)
        			{
        				if ((c_Receiver.words[0] == i_MeasHwid) && (c_Receiver.words[1] == c_DefMsg.STATE_ACT_FINISHED))
   						{
        					i_MeasU = c_Receiver.words[2];
        					i_MeasI = c_Receiver.words[3];
        					if (i_MeasI > 0)
        					{
        						f_CalcR = i_MeasU;
        						f_CalcR /= i_MeasI;
        					}
        					else
        						f_CalcR = Float.POSITIVE_INFINITY;        					
        					i_MeasPhase = c_Receiver.words[4];
        					i_MeasErr = c_Receiver.words[5];
        					b_Receiving = false; 
   						}
        				else if ((c_Receiver.words[0] == i_MeasHwid) && (c_Receiver.words[1] == c_DefMsg.STATE_ACT_FAILED))
   						{
        					i_MeasErr = c_Receiver.words[5];
        					b_Receiving = false; 
   						}        				
        			}
        			if (b_Receiving)
        				c_Helper.timer(100);
        		}
        		
        		if (b_Measure)
        		{
        			if ((i_MeasU >= i_Umin) && (i_MeasU <= i_Umax))
            			if ((i_MeasI >= i_Imin) && (i_MeasI <= i_Imax))
            				if ((f_CalcR >= f_Rmin) && (f_CalcR <= f_Rmax))            				
            					if ((i_MeasPhase >= i_PhaseMin) && (i_MeasPhase <= i_PhaseMax))
            						b_Measure = false;
            					else
            						b_Measure = true;
            				else
            					b_Measure = true;
            			else
            				b_Measure = true;
        			else
        				b_Measure = true;
        		}
        		
                if (i_MeasErr > 0)
                	b_Measure = false;

            	if (b_Measure)
            	{
            		if (i_Pause > 250)
                	{
                    	c_Helper.logDebug(2, "Starte Pause " + i_Pause + " ms", "start pause "  + i_Pause + " ms", this.getClass());
                        try 
                        {
                        	Thread.sleep(i_Pause);
                        } 
                        catch( InterruptedException e ) 
                        {
                        }        	
                	}
                	else
                		c_Helper.timer(250);            		
            	}        		
            	
            	try
            	{
               		if (b_AwtBusy | b_FwtBusy | b_QwtBusy) {
               			b_NoCancel = !getPr�flingLaufzeitUmgebung().getUserDialog().isCancelled();
               		}
            	}
            	catch (Exception e)
            	{            	
            	}         	
    		}
    		
            //execute, if timeout occured
            if (b_Measure)
            	c_Helper.logDebug(2, "Timeout " + i_Timeout + " ms", "Timeout " + i_Timeout + " ms", this.getClass());            		
        }
        else
        {
    		c_Helper.logDebug(2, "Starte Messung", "Start Measure", this.getClass());	  
    		
    		ctrlAcChannel (c_MeasChannel, c_Helper, c_DefMsg.ID_CTRL_AC, i_MeasHwid, c_DefMsg.ACT_START, i_Frequenz, i_Delay, i_Duration);
    		
    		b_Receiving = true;        		
    		long l_Timer = System.currentTimeMillis() + 8000 + i_Duration + i_Delay;
    		while (b_Receiving && (System.currentTimeMillis() < l_Timer))
    		{
    			c_Receiver = c_MeasChannel.getRxMsgByID(c_DefMsg.ID_STAT_AC, true);
    			if (c_Receiver != null)
    			{
    				if ((c_Receiver.words[0] == i_MeasHwid) && (c_Receiver.words[1] == c_DefMsg.STATE_ACT_FINISHED))
					{
    					i_MeasU = c_Receiver.words[2];
    					i_MeasI = c_Receiver.words[3];
    					if (i_MeasI > 0)
    					{
    						f_CalcR = i_MeasU;
    						f_CalcR /= i_MeasI;
    					}
    					else
    						f_CalcR = Float.POSITIVE_INFINITY;
    					i_MeasPhase = c_Receiver.words[4];
    					i_MeasErr = c_Receiver.words[5];
    					b_Receiving = false; 
					}
    				else if ((c_Receiver.words[0] == i_MeasHwid) && (c_Receiver.words[1] == c_DefMsg.STATE_ACT_FAILED))
					{
    					i_MeasErr = c_Receiver.words[5];
    					b_Receiving = false; 
					}    				
    			}
    			if (b_Receiving)
    				c_Helper.timer(100);
    		} 
    		
        	if (i_Pause > 0)
        	{
            	c_Helper.logDebug(2, "Starte Pause " + i_Pause + " ms", "start pause "  + i_Pause + " ms", this.getClass());
                try 
                {
                	Thread.sleep(i_Pause);
                } 
                catch( InterruptedException e ) 
                {
                }        	
        	}
        }
        
        //close AWT
        if (b_AwtBusy)
        {
        	c_Helper.logDebug(2, "Schlie�e AWT", "close AWT", this.getClass());
        	
        	try 
        	{
        		getPr�flingLaufzeitUmgebung().releaseUserDialog();
            }
        	catch (Exception e) 
        	{
            	c_Helper.logDebug(0, s_AwtText + " " + e, s_AwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while closing AWT-Dialog: " + s_AwtText, e );            	        		
        	}        	
        	
        	b_AwtBusy = false;
        }		                     
		
        //close FWT
        if (b_FwtBusy)
        {
        	c_Helper.logDebug(2, "Schlie�e FWT", "close FWT", this.getClass());
        	
        	try 
        	{
        		getPr�flingLaufzeitUmgebung().releaseUserDialog();
            }
        	catch (Exception e) 
        	{
            	c_Helper.logDebug(0, s_FwtText + " " + e, s_FwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while closing FWT-Dialog: " + s_FwtText, e );            	        		
        	}        	
        	
        	b_FwtBusy = false;
        }       
        
        //close QWT
        if (b_QwtBusy)
        {
        	c_Helper.logDebug(2, "Schlie�e QWT", "close QWT", this.getClass());
        	
        	try 
        	{
        		getPr�flingLaufzeitUmgebung().releaseUserDialog();
            }
        	catch (Exception e) 
        	{
            	c_Helper.logDebug(0, s_QwtText + " " + e, s_QwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while closing QWT-Dialog: " + s_QwtText, e );            	        		
        	}        	
        	
        	b_QwtBusy = false;
        }                						              				        	

        if (!b_NoCancel)
        {
        	if (isDE())
        		i_Status = setMeasureError (0, 0, 0, 0, "", "", "User hat Pr�fung abgebrochen.", v_ErgListe);
        	else
        		i_Status = setMeasureError (0, 0, 0, 0, "", "", "User cancelled teststep.", v_ErgListe);
        }
        else
        {
    		if (b_NoExecError)
    		{
    			if (i_MeasErr != 0)
    			{
    				b_NoMeasError = false;

    				if ((i_MeasErr & MsgDefs.ERR_TIMEOUT) > 0)
    					if (isDE())
    						setSystemError(MsgDefs.ERR_TIMEOUT, "Karte neu starten", "Measuring channel was not shut down correctly", "Timeout (MEAS)", v_ErgListe);
    					else
    						setSystemError(MsgDefs.ERR_TIMEOUT, "Restart card", "Measuring channel was not shut down correctly", "Timeout (MEAS)", v_ErgListe);
    				if ((i_MeasErr & MsgDefs.ERR_TEMP) > 0)
    					if (isDE())
    						setSystemError(MsgDefs.ERR_TEMP, "NOT-Aus bet�tigen, System abschalten", "Messkarten sind zu warm geworden", "�bertemperatur (MEAS)", v_ErgListe);					
    					else
    						setSystemError(MsgDefs.ERR_TEMP, "Press E-Stop, Switch off system", "Temperature exceede limits", "Overtemperature (MEAS)", v_ErgListe);					
    				if ((i_MeasErr & MsgDefs.ERR_ILIM) > 0)
    					if (isDE())
    						setSystemError(MsgDefs.ERR_ILIM, "Pr�fe auf Kurzschluss", "Strom hat die eingestellten Grenzwerte �berschritten", "�berlast (MEAS)", v_ErgListe);
    					else
    						setSystemError(MsgDefs.ERR_ILIM, "Check for short circuit", "Current exceeded specified limit", "Overload (MEAS)", v_ErgListe);
    				if ((i_MeasErr & MsgDefs.ERR_IOVFL) > 0)
    					if (isDE())
    						setSystemError(MsgDefs.ERR_IOVFL, "Stelle Verst�rkung richtig ein", "Strom hat den Me�bereich �berschritten", "�berlauf Strom (MEAS)", v_ErgListe);
    					else
    						setSystemError(MsgDefs.ERR_IOVFL, "Adjust amplification", "Current exceeded input range", "Current overflow (MEAS)", v_ErgListe);
    				if ((i_MeasErr & MsgDefs.ERR_ISHORT) > 0)
    					if (isDE())
    						setSystemError(MsgDefs.ERR_ISHORT, "Pr�fe auf Kurzschluss", "Strom hat die eingestellten Grenzwerte �berschritten", "Kurzschluss (MEAS)", v_ErgListe);					
    					else
    						setSystemError(MsgDefs.ERR_ISHORT, "Check for short circuit", "Current exceeded maximum ratings", "Short circuit (MEAS)", v_ErgListe);					
    				if ((i_MeasErr & MsgDefs.ERR_HW) > 0)
    					if (isDE())
    						setSystemError(MsgDefs.ERR_HW, "Pr�fe Sicherungen, Netzteil und Verkabelung", "Allgemeiner Hardwarefehler", "Hardwarefehler (MEAS)", v_ErgListe);
    					else
    						setSystemError(MsgDefs.ERR_HW, "Check fuses, power-supply and wiring", "Unspecified hardware error", "Hardware error (MEAS)", v_ErgListe);
    				if ((i_MeasErr & MsgDefs.ERR_FH) > 0)
    					if (isDE())
    						setSystemError(MsgDefs.ERR_FH, "Starte Karte neu, pr�fe Hardware und Parameter", "Fensterheber hat eine Fehlfunktion", "Fehler Fensterheber (MEAS)", v_ErgListe);					
    					else
    						setSystemError(MsgDefs.ERR_FH, "restart card, check hardware and parameters", "Window lifter doesnt work correctly", "Window lifter error (MEAS)", v_ErgListe);					
    				if ((i_MeasErr & MsgDefs.ERR_NOMSG) > 0)
    					if (isDE())
    						setSystemError(MsgDefs.ERR_NOMSG, "Karte neu starten", "Keine Nachricht im Puffer", "Buffer underrun (MEAS)", v_ErgListe);
    					else
    						setSystemError(MsgDefs.ERR_NOMSG, "Restart card", "No message in buffer", "Buffer underrun (MEAS)", v_ErgListe);
    				if ((i_MeasErr & MsgDefs.ERR_CONFIG) > 0)
    					if (isDE())
    						setSystemError(MsgDefs.ERR_CONFIG, "Karte richtig konfigurieren", "Karte hat falsche Konfiguration", "Falsche Konfiguration (MEAS)", v_ErgListe);					
    					else
    						setSystemError(MsgDefs.ERR_CONFIG, "Configure card correctly", "Card has wrong configuration", "Wrong config (MEAS)", v_ErgListe);					
    				if ((i_MeasErr & MsgDefs.ERR_EMSTOP) > 0)
    					if (isDE())
    						setSystemError(MsgDefs.ERR_EMSTOP, "NOT-Aus freigeben und best�tigen", "Ein NOT-Aus ist aufgetreten", "NOT-Aus (MEAS)", v_ErgListe);					
    					else
    						setSystemError(MsgDefs.ERR_EMSTOP, "Release condition and acknowledge", "An Emergency stop occured", "Emergency stop (MEAS)", v_ErgListe);					
    				if ((i_MeasErr & 0x8000) > 0)
    					if (isDE())
    						setSystemError(0x8000, "Warten, bis Messung beendet", "Eine Messung l�uft noch", "Busy (MEAS)", v_ErgListe);					
    					else
    						setSystemError(0x8000, "Wait until measurement is finished", "A measurement is running", "Busy (MEAS)", v_ErgListe);					
    				
    				if (isDE())
    				{
    					setMeasureError (i_MeasErr, i_MeasU, i_Umin, i_Umax, "Gemessene Spannung (mV)", v_ErgListe);
    					setMeasureError (i_MeasErr, i_MeasI, i_Imin, i_Imax, "Gemesserner Strom (mA)", v_ErgListe);
    					setMeasureError (i_MeasErr, f_CalcR, f_Rmin, f_Rmax, "Gemessener Widerstand (R)", v_ErgListe);
    					setMeasureError (i_MeasErr, i_MeasPhase, i_PhaseMin, i_PhaseMax, "Gemessene Phase (�)", v_ErgListe);
    				}
    				else
    				{
    					setMeasureError (i_MeasErr, i_MeasU, i_Umin, i_Umax, "Measured voltage (mV)", v_ErgListe);
    					setMeasureError (i_MeasErr, i_MeasI, i_Imin, i_Imax, "Measured current (mA)", v_ErgListe);
    					setMeasureError (i_MeasErr, f_CalcR, f_Rmin, f_Rmax, "Measured resistance (R)", v_ErgListe);
    					setMeasureError (i_MeasErr, i_MeasPhase, i_PhaseMin, i_PhaseMax, "Measured Phase (�)", v_ErgListe);
    				}					
    				i_Status = setMeasureError (i_MeasErr, i_MeasPhase, i_PhaseMin, i_PhaseMax, "Measured phase", v_ErgListe);				
    			}
    			
    	        if (b_Receiving)
    	        {
    	        	if (isDE())
    	        		i_Status = setMeasureError (0, 0, 0, 0, "Messung wiederholen", "Kommunikationsfehler", "Fehler beim Nachrichtenempfang", v_ErgListe);
    	        	else
    	        		i_Status = setMeasureError (0, 0, 0, 0, "Repeat measurement", "Communication error", "Error while receiving message", v_ErgListe);
    	        	
    	        	c_Helper.logDebug(0, "Fehler beim Nachrichtenempfang", "Error while receiving message", this.getClass());
    	        	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while receiving message");            	        		        	
    	        }
    	        else if (b_NoMeasError)
    			{
    				boolean b_ResultVoltage = false;
    				boolean b_ResultCurrent = false;
    				boolean b_ResultResistance = false;
    				boolean b_ResultPhase = false;

    				if (i_MeasU < i_Umin)
    				{
    					b_NoResultError = false;
    					b_ResultVoltage = true;
    					if (isDE())
    						setMeasureError (MsgDefs.ERR_UMIN, i_MeasU, i_Umin, i_Umax, "Gemessene Spannung (mV)", "Pr�fe Sicherungen, Netzeil und Parameter", "Spannung zu niedrig", s_Component + "Spannung ausserhalb der Grenzen", v_ErgListe);
    					else
    						setMeasureError (MsgDefs.ERR_UMIN, i_MeasU, i_Umin, i_Umax, "Measured voltage (mV)", "Check fuses, power-supply or parameters", "Voltage too low", s_Component + "Voltage out of range", v_ErgListe);
    				}
    				if (i_MeasU > i_Umax)
    				{
    					b_NoResultError = false;
    					b_ResultVoltage = true;					
    					if (isDE())
    						setMeasureError (MsgDefs.ERR_UMAX, i_MeasU, i_Umin, i_Umax, "Gemessene Spannung (mV)", "Pr�fe Sicherungen und Parameter", "Spannung zu hoch", s_Component + "Spannung ausserhalb der Grenzen", v_ErgListe);
    					else
    						setMeasureError (MsgDefs.ERR_UMAX, i_MeasU, i_Umin, i_Umax, "Measured voltage (mV)", "Check power-supply or paramaters", "Voltage too high", s_Component + "Voltage out of range", v_ErgListe);					
    				}
    				if (i_MeasI < i_Imin)
    				{
    					b_NoResultError = false;
    					b_ResultCurrent = true;
    					if (isDE())
    						setMeasureError (MsgDefs.ERR_IMIN, i_MeasI, i_Imin, i_Imax, "Gemessener Strom (mA)", "Leerlauf (Nicht gesteckt)", "Strom zu niedrig", s_Component + "Strom ausserhalb der Grenzen", v_ErgListe);
    					else
    						setMeasureError (MsgDefs.ERR_IMIN, i_MeasI, i_Imin, i_Imax, "Measured current (mA)", "Open load (DUT failure)", "Current too low", s_Component + "Current out of range", v_ErgListe);
    				}
    				if (i_MeasI > i_Imax)
    				{
    					b_NoResultError = false;
    					b_ResultCurrent = true;
    					if (isDE())
    						setMeasureError (MsgDefs.ERR_IMIN, i_MeasI, i_Imin, i_Imax, "Gemessener Strom (mA)", "Kurzschlu� (Komponentenfehler)", "Strom zu hoch", s_Component + "Strom ausserhalb der Grenzen", v_ErgListe);
    					else
    						setMeasureError (MsgDefs.ERR_IMAX, i_MeasI, i_Imin, i_Imax, "Measured current (mA)", "Short Circuit (DUT failure)", "Current too high", s_Component + "Current out of range", v_ErgListe);
    				}
    				if (f_CalcR < f_Rmin)
    				{
    					b_NoResultError = false;
    					b_ResultResistance = true;
    					if (isDE())
    						setMeasureError (MsgDefs.ERR_RMAX, f_CalcR, f_Rmin, f_Rmax, "Gemessener Widerstand (R)", "Kurzschlu� (Komponentenfehler)", "Widerstand zu niedrig", s_Component + "Widerstand ausserhalb der Grenzen", v_ErgListe);
    					else
    						setMeasureError (MsgDefs.ERR_RMAX, f_CalcR, f_Rmin, f_Rmax, "Measured resistance (R)", "Short Circuit (DUT failure)", "Resistance too low", s_Component + "Resistance out of range", v_ErgListe);
    				}
    				if (f_CalcR > f_Rmax)
    				{
    					b_NoResultError = false;
    					b_ResultResistance = true;					
    					if (isDE())
    						setMeasureError (MsgDefs.ERR_RMAX, f_CalcR, f_Rmin, f_Rmax, "Gemessener Widerstand (R)", "Leerlauf (Komponentenfehler)", "Widerstand zu hoch", s_Component + "Widerstand ausserhalb der Grenzen", v_ErgListe);
    					else
    						setMeasureError (MsgDefs.ERR_RMIN, f_CalcR, f_Rmin, f_Rmax, "Measured resistance (R)", "Open load (DUT failure)", "Resistance too high", s_Component + "Resistance out of range", v_ErgListe);
    				}
    				if (i_MeasPhase < i_PhaseMin)
    				{
    					b_NoResultError = false;
    					b_ResultPhase = true;
    					if (isDE())
    						setMeasureError (MsgDefs.ERR_PHASE, i_MeasPhase, i_PhaseMin, i_PhaseMax, "Gemessene Phase (�)", "", "Phasenverschiebung zu gering", s_Component + "Phase ausserhalb der Grenzen", v_ErgListe);
    					else
    						setMeasureError (MsgDefs.ERR_PHASE, i_MeasPhase, i_PhaseMin, i_PhaseMax, "Measured phase (�)", "", "Phase too low", s_Component + "Phase out of range", v_ErgListe);
    				}				
    				if (i_MeasPhase > i_PhaseMax)
    				{
    					b_NoResultError = false;
    					b_ResultPhase = true;
    					if (isDE())
    						setMeasureError (MsgDefs.ERR_PHASE, i_MeasPhase, i_PhaseMin, i_PhaseMax, "Gemessene Phase (�)", "", "Phasenverschiebung zu hoch", s_Component + "Phase ausserhalb der Grenzen", v_ErgListe);
    					else
    						setMeasureError (MsgDefs.ERR_PHASE, i_MeasPhase, i_PhaseMin, i_PhaseMax, "Measured phase (�)", "", "Phase too high", s_Component + "Phase out of range", v_ErgListe);
    				}
    				
    				if (b_NoResultError)
    				{
    					c_Helper.logDebug(2, "Ohne Fehler beendet.", "Finished without errors.", this.getClass());
    					if (isDE())
    					{
    						setMeasureOK (i_MeasU, i_Umin, i_Umax, "Gemessene Spannung (mV)", v_ErgListe);
    						setMeasureOK (i_MeasI, i_Imin, i_Imax, "Gemessener Strom (mA)", v_ErgListe);
    						setMeasureOK (f_CalcR, f_Rmin, f_Rmax, "Gemessener Widerstand (R)", v_ErgListe);
    						setMeasureOK (i_MeasPhase, i_PhaseMin, i_PhaseMax, "Gemessene Phase (�)", v_ErgListe);						
    					}
    					else
    					{
    						setMeasureOK (i_MeasU, i_Umin, i_Umax, "Measured voltage (mV)", v_ErgListe);
    						setMeasureOK (i_MeasI, i_Imin, i_Imax, "Measured current (mA)", v_ErgListe);
    						setMeasureOK (f_CalcR, f_Rmin, f_Rmax, "Measured resistance (R)", v_ErgListe);
    						setMeasureOK (i_MeasPhase, i_PhaseMin, i_PhaseMax, "Measured phase (�)", v_ErgListe);						
    					}
    					i_Status = STATUS_EXECUTION_OK;
    				}
    				else
    				{
    					if (!b_ResultVoltage)
    						if (isDE())
    							setMeasureOK (i_MeasU, i_Umin, i_Umax, "Gemessene Spannung (mV)", v_ErgListe);
    						else
    							setMeasureOK (i_MeasU, i_Umin, i_Umax, "Measured voltage (mV)", v_ErgListe);

    					if (!b_ResultCurrent)
    						if (isDE())
    							setMeasureOK (i_MeasI, i_Imin, i_Imax, "Gemessener Strom (mA)", v_ErgListe);
    						else
    							setMeasureOK (i_MeasI, i_Imin, i_Imax, "Measured current (mA)", v_ErgListe);

    					if (!b_ResultResistance)
    						if (isDE())
    							setMeasureOK (f_CalcR, f_Rmin, f_Rmax, "Gemessener Widerstand (R)", v_ErgListe);
    						else
    							setMeasureOK (f_CalcR, f_Rmin, f_Rmax, "Measured resistance (R)", v_ErgListe);
    					
    					if (!b_ResultPhase)
    						if (isDE())
    							setMeasureOK (i_MeasPhase, i_PhaseMin, i_PhaseMax, "Gemessene Phase (�)", v_ErgListe);
    						else
    							setMeasureOK (i_MeasPhase, i_PhaseMin, i_PhaseMax, "Measured phase (�)", v_ErgListe);					
    					
    					c_Helper.logDebug(1, "Mit Toleranzfehler beendet.", "Finished with tolerance errors.", this.getClass());
    					i_Status = STATUS_EXECUTION_ERROR;					
    				}				
    			}
    			else
    			{
    				c_Helper.logDebug(1, "Mit Messfehler beendet.", "Finished with measuring errors.", this.getClass());
    				i_Status = STATUS_EXECUTION_ERROR;
    			}							
    		}
    		else
    		{
    			c_Helper.logDebug(0, "Mit Ausf�hrungsfehler beendet.", "Finished with execution errors.", this.getClass());
    			if (isDE())
    				i_Status = setSystemError(0, "Check parameters and card", "Karten- oder Kommunikationsfehler", "Ausf�hrungsfehler", v_ErgListe);
    			else
    				i_Status = setSystemError(0, "Check parameters and card", "Card or communication failed", "Execution error", v_ErgListe);
    		}		        	
        }
		                
		c_MeasChannel.isFlagSent();
		//Switch off channel
		ctrlAcChannel (c_MeasChannel, c_Helper, c_DefMsg.ID_CTRL_AC, i_MeasHwid, c_DefMsg.ACT_STOP, 0, i_Delay, i_Duration);
		long l_Stop = System.currentTimeMillis() + 8000;
		while (!c_MeasChannel.isFlagSent() && (l_Stop > System.currentTimeMillis()))
			c_Helper.timer(100);
		
		if (l_Stop < System.currentTimeMillis())
        {
			/*
        	if (isDE())
        		i_Status = setMeasureError (0, 0, 0, 0, "Karte neu starten", "Kommunikationsfehler", "Fehler beim Abschalten des Messkanals", v_ErgListe);
        	else
        		i_Status = setMeasureError (0, 0, 0, 0, "Restart card", "Communication error", "Error while turning off measuring channel", v_ErgListe);
        	*/
        	
        	c_Helper.logDebug(0, "Fehler beim Senden Messkanal aus", "Error while sending turn off channel", this.getClass());
        	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while sending turn off channel");            	        		        	
        }
		
		try
		{
			c_Card.closeChannel(Integer.toString(i_MeasPort), true);			
		}		
		catch (Exception e)
		{
        	c_Helper.logDebug(0, "Fehler beim Schlie�en des Kanals: " + e.getMessage(), "Error while closing channel: " + e.getMessage(), this.getClass());
        	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error ehile closing channel: " + e.getMessage());            	        		        				
		}		
		
		//Set Result
		setPPStatus (info, i_Status, v_ErgListe);
	}

	/**
	 * Setzt das Pr�fungsergebnis auf Systemfehler und generiert entsprechende Eintr�ge.
	 * 
	 * @param errCode 		spezifischer Fehlercode
	 * @param awt 			spezifischer Anweisungstext
	 * @param hwt 			spezifischer Hinweistext
	 * @param errt 			spezifischer Fehlertext
	 * @param ergebnisListe zu beschreibende ErgListe
	 * @return status		status als int
	 */
	private int setSystemError (int errCode, String awt, String hwt, String errt, Vector ergListe)
	{		
		if (s_AffectedComponent != null || s_AffectedComponent.length() > 0)
			ergListe.add (new  Ergebnis(Integer.toString(errCode), this.getPPName(), "", "", "", "", "", "", "", "", "", "", awt, s_AffectedComponent + errt, hwt, Ergebnis.FT_NIO));
		else
			ergListe.add (new  Ergebnis(Integer.toString(errCode), this.getPPName(), "", "", "", "", "", "", "", "", "", "", awt, errt, hwt, Ergebnis.FT_NIO));
		return STATUS_EXECUTION_ERROR;		
	}

	/**
	 * Setzt das Pr�fungsergebnis auf Messung IO und generiert entsprechende Eintr�ge.
	 * 
	 * @param value 		finale(r) Messwert(e)
	 * @param min			Minimalwert
	 * @param max			Maximalwert
	 * @param rest			spezifischer Ergebnistext
	 * @param ergebnisListe zu beschreibende ErgListe
	 * @return status		status als int 
	 */
	private int setMeasureOK (int value, int min, int max, String rest, Vector ergListe)
	{
		ergListe.add (new Ergebnis("0", this.getPPName(), "", "", "", rest, Integer.toString(value), Integer.toString(min), Integer.toString(max), "", "", "", "", "", "", Ergebnis.FT_IO));
		return STATUS_EXECUTION_OK;
	}
	
	/**
	 * Setzt das Pr�fungsergebnis auf Messung IO und generiert entsprechende Eintr�ge.
	 * 
	 * @param value 		finale(r) Messwert(e)
	 * @param min			Minimalwert
	 * @param max			Maximalwert
	 * @param rest			spezifischer Ergebnistext
	 * @param ergebnisListe zu beschreibende ErgListe
	 * @return status		status als int 
	 */
	private int setMeasureOK (float value, float min, float max, String rest, Vector ergListe)
	{		
		ergListe.add (new Ergebnis("0", this.getPPName(), "", "", "", rest, Float.toString(value), Float.toString(min), Float.toString(max), "", "", "", "", "", "", Ergebnis.FT_IO));
		return STATUS_EXECUTION_OK;
	}
	
	/**
	 * Setzt das Pr�fungsergebnis auf Messung NIO und generiert entsprechende Eintr�ge.
	 * 
	 * @param errCode 	spezifischer Fehlercode
	 * @param value 	finaler Messwert
	 * @param min			Minimalwert
	 * @param max			Maximalwert
	 * @param awt 		spezifischer Anweisungstext
	 * @param hwt 		spezifischer Hinweistext
	 * @param errt 		spezifischer Fehlertext
	 * @param ergebnisListe zu beschreibende ErgListe
	 * @return status		status als int 
	 */
	private int setMeasureError (int errCode, int value, int min, int max, String awt, String hwt, String errt, Vector ergListe)
	{
		if (isDE())			
			ergListe.add (new Ergebnis(Integer.toString(errCode), this.getPPName(), "", "", "", "Ergebnis NIO", Integer.toString(value), Integer.toString(min), Integer.toString(max), "", "", "", awt, errt, hwt, Ergebnis.FT_NIO));
		else
			ergListe.add (new Ergebnis(Integer.toString(errCode), this.getPPName(), "", "", "", "Result out of range", Integer.toString(value), Integer.toString(min), Integer.toString(max), "", "", "", awt, errt, hwt, Ergebnis.FT_NIO));
		return STATUS_EXECUTION_ERROR;
	}	

	/**
	 * Setzt das Pr�fungsergebnis auf Messung NIO und generiert entsprechende Eintr�ge.
	 * 
	 * @param errCode 	spezifischer Fehlercode
	 * @param value 	finaler Messwert
	 * @param min		Minimalwert
	 * @param max		Maximalwert
	 * @param awt 		Ergebnistest
	 * @param ergebnisListe zu beschreibende ErgListe
	 * @return status		status als int 
	 */
	private int setMeasureError (int errCode, int value, int min, int max, String rest, Vector ergListe)
	{
		ergListe.add (new Ergebnis(Integer.toString(errCode), this.getPPName(), "", "", "", rest, Integer.toString(value), Integer.toString(min), Integer.toString(max), "", "", "", "", "", "", Ergebnis.FT_NIO));
		return STATUS_EXECUTION_ERROR;
	}
	
	/**
	 * Setzt das Pr�fungsergebnis auf Messung NIO und generiert entsprechende Eintr�ge.
	 * 
	 * @param errCode 	spezifischer Fehlercode
	 * @param value 	finaler Messwert
	 * @param min		Minimalwert
	 * @param max		Maximalwert
	 * @param awt 		Ergebnistest
	 * @param ergebnisListe zu beschreibende ErgListe
	 * @return status		status als int 
	 */
	private int setMeasureError (int errCode, float value, float min, float max, String rest, Vector ergListe)
	{
		ergListe.add (new Ergebnis(Integer.toString(errCode), this.getPPName(), "", "", "", rest, Float.toString(value), Float.toString(min), Float.toString(max), "", "", "", "", "", "", Ergebnis.FT_NIO));
		return STATUS_EXECUTION_ERROR;
	}	
	
	/**
	 * Setzt das Pr�fungsergebnis auf Messung NIO und generiert entsprechende Eintr�ge.
	 * 
	 * @param errCode 	spezifischer Fehlercode
	 * @param value 	finaler Messwert
	 * @param min			Minimalwert
	 * @param max			Maximalwert
	 * @param rest 		spezifischer Ergebnistext
	 * @param awt 		spezifischer Anweisungstext
	 * @param hwt 		spezifischer Hinweistext
	 * @param errt 		spezifischer Fehlertext
	 * @param ergebnisListe zu beschreibende ErgListe
	 * @return status		status als int 
	 */
	private int setMeasureError (int errCode, int value, int min, int max, String rest, String awt, String hwt, String errt, Vector ergListe)
	{
		ergListe.add (new Ergebnis(Integer.toString(errCode), this.getPPName(), "", "", "", rest, Integer.toString(value), Integer.toString(min), Integer.toString(max), "", "", "", awt, errt, hwt, Ergebnis.FT_NIO));
		return STATUS_EXECUTION_ERROR;
	}
	
	/**
	 * Setzt das Pr�fungsergebnis auf Messung NIO und generiert entsprechende Eintr�ge.
	 * 
	 * @param errCode 	spezifischer Fehlercode
	 * @param value 	finaler Messwert
	 * @param min			Minimalwert
	 * @param max			Maximalwert
	 * @param rest 		spezifischer Ergebnistext
	 * @param awt 		spezifischer Anweisungstext
	 * @param hwt 		spezifischer Hinweistext
	 * @param errt 		spezifischer Fehlertext
	 * @param ergebnisListe zu beschreibende ErgListe
	 * @return status		status als int 
	 */
	private int setMeasureError (int errCode, float value, float min, float max, String rest, String awt, String hwt, String errt, Vector ergListe)
	{
		ergListe.add (new Ergebnis(Integer.toString(errCode), this.getPPName(), "", "", "", rest, Float.toString(value), Float.toString(min), Float.toString(max), "", "", "", awt, errt, hwt, Ergebnis.FT_NIO));
		return STATUS_EXECUTION_ERROR;
	}
	
	/**
	 * L�st in @-geklammerte Attribute dynamisch (zur Laufzeit) auf.
	 *  
	 * @param input	input-String
	 * @return		gefundener Wert. input ohne @-Klammerung, dynamisch geholte mit Klammerung.
	 */
	private String getDynamicAttribute (String input)
	{
		String result = input;
		Helper funcs = new Helper ();
		
		funcs.logInit(i_Debug);		
		
		funcs.logDebug(2, "Start getDynamicAttribute. . .", "Start getDynamicAttribute . . .", this.getClass());
		if (input.startsWith("@"))
		{
			funcs.logDebug(2, "Erstes Token gefunden", "Found first Token", this.getClass());
			if (input.endsWith("@"))
			{
				funcs.logDebug(2, "Zweites Token gefunden", "Found second Token", this.getClass());
				if (input.length() >= 3)
					try
					{
						funcs.logDebug(2, "L�nge g�ltig. Hole dynamisches Attribut. . .", "Length valid. fetch dynmaic attribute. . .", this.getClass());
						result = (String) getPr�fling().getAllAttributes().get(input.substring(1, input.length() - 1));
						funcs.logDebug(2, "Dynamisches Attribut aufgel�st: " + result, "Resolved dynamic attribute: " + result, this.getClass());
					}
					catch (Exception e)
					{
						//@todo: wenn ein fehler auftritt nicht versuchen mit einem beliebigen text weiterzuarbeiten. sondern exception!
						result = input.substring(0, input.length() - 1);						
						funcs.logDebug(0, "Fehler beim dynamischen Aufl�sen. Es wird zur�ck gegeben: " + result, "Error during dynamic lookup. It will be returned: " + result, this.getClass());						
					}					
				else
				{
					result = null;
					funcs.logDebug(0, "L�nge ung�ltig. Es wird \"null\" zur�ck gegeben.", "Length invalid. \"null\" is returned.", this.getClass());
				}
			}
			else
			{
				result = input.substring(1, input.length());
				funcs.logDebug(0, "Zweites \"@\" nicht gefunden. Es wird zur�ck gegeben: " + result, "Did not find second \"@\". It will be returned: " + result, this.getClass());
			}
		}
		else
			if (input.endsWith("@"))
			{
				result = input.substring(0, input.length() - 1);
				funcs.logDebug(0, "Erstes \"@\" nicht gefunden. Es wird zur�ck gegeben: " + result, "Did not find first \"@\". It will be returned: " + result, this.getClass());
			}				
			else
			{
				funcs.logDebug(2, "Kein \"@\" gefunden. Statisches Attribut. Value: " + result, "No \"@\" found. Static attribute. Value: " + result, this.getClass());
				result = input;
			}
		
		return result;
	}
	
	/**Steuert eine Gleichspannung auf einem Messkanal ein.
	 * 
	 * @param channel	Messkanal als TcpClient
	 * @param helper	Helper als Helper (f�r Logging)
	 * @param msgid		Identifier der Nachricht
	 * @param hwid		Identifier der Hardware
	 * @param action	Auszuf�hrende Aktion
	 * @param frequenz	Einzustellende Frequenz
	 * @param delay		Einschwingzeit in ms
	 * @param duration	Messdauer in ms
	 */
	private void ctrlAcChannel (TcpClient channel, Helper helper, int msgid, int hwid, int action, int frequenz, int delay, int duration)
	{
		int ia_data[] = new int[6];

		ia_data[0] = hwid;
		ia_data[1] = action;
		ia_data[2] = frequenz;
		ia_data[3] = delay;
		ia_data[4] = duration;
		ia_data[5] = 0;
		
		if (channel != null)
			channel.setTxMsg(msgid, ia_data, null, null);
	}	

    /**
     * Liefert true zur�ck, wenn deutsche Texte verwendet werden sollen.
     * @result True bei deutsche texte, false sonst.
     */
    private static boolean isDE()
    {
        try 
        {
            if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true ) return true;
            else return false;
        } 
        catch (Exception e) 
        {
            return false;   //default is english
        }
    }    
}

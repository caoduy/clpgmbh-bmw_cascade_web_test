/*
 * MPS3Init_Cards
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.sql.Timestamp;

import com.bmw.appframework.logging.*;
import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.pruefstand.devices.mps3.*;
import com.bmw.cascade.util.logging.*;
import com.bmw.cascade.pruefstand.*;

/**
 * Diese Pr�fprozedut initialisiert die Messkarten im Multifunktionspr�fstand. 
 * 
 * @author BMW TI-431 Burger
 * @version V_1_0 21.08.2009 AB Implementierung <br>
 * @version V_2_0 26.08.2009 AB Deutsche Texte implementiert <br>
 * @version V_3_0 18.09.2009 AB Paue gefixt <br>
 * @version V_4_0 29.09.2009 AB F-Version <br>
 * @version V_5_0 27.10.2009 AB Tag-Handling implementiert <br>
 * @version V_6_0 27.10.2009 AB F-Version <br>
 * @version V_7_0 29.10.2009 AB Tags LEFT, RIGHT entfernt, Syntaxpr�fung TAG eingebaut <br>
 * @version V_8_0 07.05.2015 TM Changed attribute TAG_N to TAG_[0..N]
 * @version V9_0_T 04.08.2015 TM T version
 * @version V10_0_F 07.08.2015 TM F version
 */
public class MPS3ResetCards_10_0_F_Pruefprozedur extends AbstractBMWPruefprozedur 
{	
	static final long serialVersionUID = 1L;
	
	int i_Debug = 0;	

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public MPS3ResetCards_10_0_F_Pruefprozedur() 
	{
	}

	private Logger pruefstandLogger = CascadeLogging.getLogger( "PruefstandLogger" );
	
	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public MPS3ResetCards_10_0_F_Pruefprozedur ( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) 
	{
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() 
	{		
		super.attributeInit();	
	}

	/**
	 * liefert die optionalen Argumente
	 * 
     * TAG_N: Bezeichnung der r�ckzusetzenden Karte(n): Card_n, "ALL". Wenn nicht angegeben, "ALL"
     * DEBUG: Debug-Level 0, 1, 2, 3 (0 = off)
     * TIMEOUT: Timeout, bis Operation beendet
     * PAUSE: Wartezeit nach der Ausf�hrung
     * AWT_TEXT: Text f�r AWT
     * AWT_TITLE: Titel f�r AWT
     * AWT_TIME: Timeout f�r AWT in ms 
     * AWT_STYLE: 1 = Status, 2 = Error, 99 = Alarm, Rest = Standard
     * FWT_TEXT: Text f�r FWT
     * FWT_TITLE: Titel f�r FWT
     * FWT_TIME: Timeout f�r FWT in ms 
     * QWT_TEXT: Text f�r QWT
     * QWT_TITLE: Titel f�r QWT
     * QWT_TIME: Timeout f�r QWT in ms 
     * QWT_STYLE: 1 = Status, 2 = Error, 99 = Alarm, Rest = Standard 
	 */
	public String[] getOptionalArgs() 
	{
		String[] args = 
		{
				/*"TAG_N",*/
				"TAG_[1..N]",
				"DEBUG",
				//TIMEOUT,
				"PAUSE",
				"AWT_TEXT",
				"AWT_TITLE",
				"AWT_STYLE",
				"AWT_TIME",
				"AWT_CANCEL",
				"FWT_TEXT",
				"FWT_TITLE",
				"FWT_TIME",
				"QWT_TEXT",
				"QWT_TITLE",
				"QWT_STYLE",
				"QWT_TIME"			
	    };
		
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() 
	{
		String[] args = 
		{           
	    };
		
		return args;
	}

	/**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die parent-Methode aufgrund
     * der offenen Anzahl an Results
	 */	
	public boolean checkArgs() 
	{		
		return true;
    }
	
	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) 
	{	
		//Result vars
		Vector v_ErgListe = new Vector();	
		int i_Status = STATUS_EXECUTION_ERROR;
		
		//Hardware vars
		Helper c_Helper = new Helper ();
    	    	
    	//System vars
		Hashtable ht_Args = getArgs();
		//Enumeration en_Args = ht_Args.elements();
		Enumeration en_Keys = ht_Args.keys();		
		String s_Arg = null;        
		String s_Val = null;
		boolean b_NoExecError = true;	    	
    	
    	//timing vars
    	int i_Pause = 0;
    	
    	//local AWT vars
    	String s_AwtText = "";
    	String s_AwtTitle = PB.getString("anweisung");	
    	int i_AwtTime = -1;
    	int i_AwtStyle = 0;
    	boolean b_AwtCancel = false;
    	boolean b_AwtBusy = false;
    	
    	//local fwt vars
    	String s_FwtText = "";
    	String s_FwtTitle = PB.getString("frage");
    	int i_FwtTime = -1;
    	boolean b_FwtBusy = false;
    	boolean b_FwtResult = false;
    	
    	//local qwt vars
    	String s_QwtText = "";
    	String s_QwtTitle = PB.getString("meldung");
    	int i_QwtTime = -1;
    	int i_QwtStyle = 0;
    	boolean b_QwtBusy = false; 
    	
    	LinkedList ll_Tags = new LinkedList();
    	boolean b_ResetAll = false;
    	
		//clear result list
		v_ErgListe.clear();
		//check args
		//checkArgs();
		//init logger
		c_Helper.logInit(i_Debug);	
		
		c_Helper.logDebug(2, "�berpr�fe Parameter", "check parameters", this.getClass());
    	    	    			
		//loop over all supplied parameters
		//for (int i = 0; i < ht_Args.size(); i++)
    	while (en_Keys.hasMoreElements())
		{
			//get name and value from next parameter
			s_Arg = en_Keys.nextElement().toString();
			//s_Val = getDynamicAttribute(en_Args.nextElement().toString());
			s_Val = getDynamicAttribute(ht_Args.get(s_Arg).toString());//.nextElement().toString());
			
			c_Helper.logDebug(2, "Arg: " + s_Arg + " Val: " + s_Val, "Arg: " + s_Arg + "Val: " + s_Val, this.getClass());

			//Add card to list, if supplied
			if (s_Arg.toUpperCase().startsWith("TAG"))
			{
				System.out.println ("Found: " + s_Val);
				if (s_Val.equalsIgnoreCase("ALL"))
					b_ResetAll = true;
				else
					if (s_Val.toUpperCase().startsWith("192.168.130.".toUpperCase()) || s_Val.toUpperCase().startsWith("card_".toUpperCase()))
						ll_Tags.add(s_Val);
					else
						pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"TAG\" has wrong parameter format. Accepts only card address.");						
			}
				
			
			//check parameter "DEBUG"
			if (s_Arg.equalsIgnoreCase("DEBUG"))
			{
				//set to level 0, if no value supplied or set to "false"
				if ((s_Val == null) || (s_Val.equalsIgnoreCase("false")))
					i_Debug = 0;
				else
					//set to level 1, if supplied "true"
					if (s_Val.equalsIgnoreCase("true"))
						i_Debug = 1;
					else						
					{
						try
						{
							//try to set desired level
							i_Debug = Integer.parseInt(s_Val);
						}
						//catch numberformatexception
						catch (NumberFormatException e)
						{
							i_Debug = 0;
							pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"DEBUG\" has wrong parameter format. Accepts only integer.", e );
						}
					}				
			}		
			
			//Get pause, if supplied
			if (s_Arg.equalsIgnoreCase("PAUSE"))
			{
				try
				{
					//try to set desired level
					i_Pause = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_Pause = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"PAUSE\" has wrong parameter format. Accepts only integer.", e );
				}	
			}
			
			//get AWT vals
			if (s_Arg.equalsIgnoreCase("AWT_TEXT"))
				s_AwtText = s_Val;
			
			if (s_Arg.equalsIgnoreCase("AWT_TITLE"))
				s_AwtTitle = s_Val;			

			if (s_Arg.equalsIgnoreCase("AWT_STYLE"))
			{
				try
				{
					//try to set desired time
					i_AwtStyle = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_AwtStyle = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"AWT_STYLE\" has wrong parameter format. Accepts only integer.", e );
				}					
			}
			
			if (s_Arg.equalsIgnoreCase("AWT_CANCEL"))
			{
				if (s_Val.equalsIgnoreCase("T") || s_Val.equalsIgnoreCase("TRUE") || s_Val.equalsIgnoreCase("1"))
					b_AwtCancel = true;
			}
			
			if (s_Arg.equalsIgnoreCase("AWT_TIME"))
			{
				try
				{
					if (Integer.parseInt(s_Val) < 1000)
						i_AwtTime = -1;
					else
					//try to get awt timeout
						i_AwtTime = Integer.parseInt(s_Val) / 1000;
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_AwtTime = -1;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"AWT_TIME\" has wrong parameter format. Accepts only integer.", e );
				}				
			}
			
			//get FWT vals
			if (s_Arg.equalsIgnoreCase("FWT_TEXT"))
				s_FwtText = s_Val;
			
			if (s_Arg.equalsIgnoreCase("FWT_TITLE"))
				s_FwtTitle = s_Val;			

			if (s_Arg.equalsIgnoreCase("FWT_TIME"))
			{
				try
				{
					if (Integer.parseInt(s_Val) < 1000)
						i_FwtTime = -1;
					else
					//try to get awt timeout
						i_FwtTime = Integer.parseInt(s_Val) / 1000;
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_FwtTime = -1;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"FWT_TIME\" has wrong parameter format. Accepts only integer.", e );
				}				
			}
			
			//get QWT vals
			if (s_Arg.equalsIgnoreCase("QWT_TEXT"))
				s_QwtText = s_Val;
			
			if (s_Arg.equalsIgnoreCase("QWT_TITLE"))
				s_QwtTitle = s_Val;			

			if (s_Arg.equalsIgnoreCase("QWT_STYLE"))
			{
				try
				{
					//try to set desired time
					i_QwtStyle = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_QwtStyle = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"QWT_STYLE\" has wrong parameter format. Accepts only integer.", e );
				}					
			}
			
			if (s_Arg.equalsIgnoreCase("QWT_TIME"))
			{
				try
				{
					if (Integer.parseInt(s_Val) < 1000)
						i_QwtTime = -1;
					else
					//try to get awt timeout
						i_QwtTime = Integer.parseInt(s_Val) / 1000;
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_QwtTime = -1;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"QWT_TIME\" has wrong parameter format. Accepts only integer.", e );
				}				
			}			
		}		                	            
        
        //show AWT
        if (s_AwtText.length() > 0)
        {
        	c_Helper.logDebug(2, "Zeige AWT", "show AWT", this.getClass());
        	
            try 
            {
           		getPr�flingLaufzeitUmgebung().getUserDialog().setAllowCancel(b_AwtCancel);
           		
                if(i_AwtStyle == 99)
                	getPr�flingLaufzeitUmgebung().getUserDialog().displayMessage(s_AwtTitle, s_AwtText, i_AwtTime);
                else 
                	if(i_AwtStyle == 1)
                		getPr�flingLaufzeitUmgebung().getUserDialog().displayStatusMessage(s_AwtTitle, s_AwtText, i_AwtTime);
                else 
                	if(i_AwtStyle == 2)
                		getPr�flingLaufzeitUmgebung().getUserDialog().displayAlertMessage(s_AwtTitle, s_AwtText, i_AwtTime);
                else
                    getPr�flingLaufzeitUmgebung().getUserDialog().displayUserMessage(s_AwtTitle, s_AwtText, i_AwtTime);

                b_AwtBusy = true;            
            } 
            catch (Exception e) 
            {
            	c_Helper.logDebug(0, s_AwtText + " " + e, s_AwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while opening AWT-Dialog: " + s_AwtText, e );            	
            }   
        }              
		
        //show FWT
        int i_FwtTemp = 0;
        if (s_FwtText.length() > 0)
        {
        	c_Helper.logDebug(2, "Zeige FWT", "show FWT", this.getClass());
        	
            try 
            {            	
            	i_FwtTemp = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputDigital(s_FwtTitle, s_FwtText, i_FwtTime);
            	
                //Get FWT result
                if (i_FwtTemp == getPr�flingLaufzeitUmgebung().getUserDialog().YES_KEY)
                	b_FwtResult = true;
                else if (i_FwtTemp == getPr�flingLaufzeitUmgebung().getUserDialog().NO_KEY)
                	b_FwtResult = false;              	

                b_FwtBusy = true;            
            } 
            catch (Exception e) 
            {
            	c_Helper.logDebug(0, s_FwtText + " " + e, s_FwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while opening FWT-Dialog: " + s_FwtText, e );            	
            }   
        }                     	
						              				        
        //show QWT
        if (s_QwtText.length() > 0)
        {
        	c_Helper.logDebug(2, "Zeige QWT", "show QWT", this.getClass());
        	
            try 
            {
                if( i_QwtStyle == 1 )
                    getPr�flingLaufzeitUmgebung().getUserDialog().requestStatusMessage(s_QwtTitle, s_QwtText, i_QwtTime);
                else if( i_QwtStyle == 2 )
                    getPr�flingLaufzeitUmgebung().getUserDialog().requestAlertMessage(s_QwtTitle, s_QwtText, i_QwtTime);
                else
                    getPr�flingLaufzeitUmgebung().getUserDialog().requestUserMessage(s_QwtTitle, s_QwtText, i_QwtTime);            	

                b_QwtBusy = true;            
            } 
            catch (Exception e) 
            {
            	c_Helper.logDebug(0, s_QwtText + " " + e, s_QwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while opening QWT-Dialog: " + s_QwtText, e );            	
            }   
        }
        
        try
        {
            while (getPr�flingLaufzeitUmgebung().getUserDialog().isShowing())
            	c_Helper.timer(100);
        }
        catch (Exception e)
        {
        	;
        }
        
        try
        {
    		if ((b_ResetAll == true) || ll_Tags.isEmpty())
    		{
    			sendReset("192.168.130.127", true);
    			sendReset("192.168.130.255", true);
    			c_Helper.timer(8000);
    			ll_Tags.clear();
    		}
        }
        catch (Exception e)
        {
        	c_Helper.logDebug(0, "Fehler beim Reset aller Karten: " + e, "Error while resetting all cards: "+ e, this.getClass());
        	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while resetting all cards", e );            	        		        	
        }
		
		if (!ll_Tags.isEmpty())
		{
			while (!ll_Tags.isEmpty())
			{
				String s_Card = null;
				
		        try
		        {
					s_Card = (String) ll_Tags.getFirst();
					ll_Tags.removeFirst();
						
					sendReset(s_Card, false);				
		        }
		        catch (Exception e)
		        {
		        	c_Helper.logDebug(0, "Fehler beim Reset von Karte " + s_Card + ": " + e, "Error while resetting all cards " + s_Card + ": " + e, this.getClass());
		        	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while resetting " + s_Card + ": ", e );            	        		        	
		        }				
			}
			c_Helper.timer(8000);			
		}
		
		if (b_NoExecError)
		{
			i_Status = setMeasureOK (0, 0, 0, v_ErgListe);	
			c_Helper.logDebug(2, "Ohne Fehler beendet.", "Finished without errors.", this.getClass());
		}		
		    	
    	if (i_Pause > 0)
    	{
        	c_Helper.logDebug(2, "Starte Pause " + i_Pause + " ms", "start pause "  + i_Pause + " ms", this.getClass());
            try 
            {
            	Thread.sleep(i_Pause);
            } 
            catch( InterruptedException e ) 
            {
            }        	
    	}    	
    	
        //close FWT
        if (b_FwtBusy)
        {
        	c_Helper.logDebug(2, "Schlie�e FWT", "close FWT", this.getClass());
        	
        	try 
        	{
        		getPr�flingLaufzeitUmgebung().releaseUserDialog();
            }
        	catch (Exception e) 
        	{
            	c_Helper.logDebug(0, s_FwtText + " " + e, s_FwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while closing FWT-Dialog: " + s_FwtText, e );            	        		
        	}        	
        	
        	b_FwtBusy = false;
        }     
        
        //close QWT
        if (b_QwtBusy)
        {
        	c_Helper.logDebug(2, "Schlie�e QWT", "close QWT", this.getClass());
        	
        	try 
        	{
        		getPr�flingLaufzeitUmgebung().releaseUserDialog();
            }
        	catch (Exception e) 
        	{
            	c_Helper.logDebug(0, s_QwtText + " " + e, s_QwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while closing QWT-Dialog: " + s_QwtText, e );            	        		
        	}        	
        	
        	b_QwtBusy = false;
        }    	
        
        //close AWT
        if (b_AwtBusy)
        {
        	c_Helper.logDebug(2, "Schlie�e AWT", "close AWT", this.getClass());
        	
        	try 
        	{
        		getPr�flingLaufzeitUmgebung().releaseUserDialog();
            }
        	catch (Exception e) 
        	{
            	c_Helper.logDebug(0, s_AwtText + " " + e, s_AwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while closing AWT-Dialog: " + s_AwtText, e );            	        		
        	}        	
        	
        	b_AwtBusy = false;
        }        

		setPPStatus (info, i_Status, v_ErgListe);
	}

	/**
	 * Setzt das Pr�fungsergebnis auf Messung IO und generiert entsprechende Eintr�ge.
	 * 
	 * @param value 		finale(r) Messwert(e)
	 * @param min			Minimalwert
	 * @param max			Maximalwert
	 * @param ergebnisListe zu beschreibende ErgListe
	 * @return status		status als int 
	 */
	private int setMeasureOK (int value, int min, int max, Vector ergListe)
	{
		if (isDE())
			ergListe.add (new Ergebnis("0", this.getPPName(), "", "", "", "Ergebnis IO", Integer.toString(value), Integer.toString(min), Integer.toString(max), "", "", "", "", "", "", Ergebnis.FT_IO));
		else
			ergListe.add (new Ergebnis("0", this.getPPName(), "", "", "", "Result ok", Integer.toString(value), Integer.toString(min), Integer.toString(max), "", "", "", "", "", "", Ergebnis.FT_IO));
		return STATUS_EXECUTION_OK;
	}
	
	/**
	 * L�st in @-geklammerte Attribute dynamisch (zur Laufzeit) auf.
	 *  
	 * @param input	input-String
	 * @return		gefundener Wert. input ohne @-Klammerung, dynamisch geholte mit Klammerung.
	 */
	private String getDynamicAttribute (String input)
	{
		String result = input;
		Helper funcs = new Helper ();
		
		funcs.logInit(i_Debug);		
		
		funcs.logDebug(2, "Start getDynamicAttribute. . .", "Start getDynamicAttribute . . .", this.getClass());
		if (input.startsWith("@"))
		{
			funcs.logDebug(2, "Erstes Token gefunden", "Found first Token", this.getClass());
			if (input.endsWith("@"))
			{
				funcs.logDebug(2, "Zweites Token gefunden", "Found second Token", this.getClass());
				if (input.length() >= 3)
					try
					{
						funcs.logDebug(2, "L�nge g�ltig. Hole dynamisches Attribut. . .", "Length valid. fetch dynmaic attribute. . .", this.getClass());
						result = (String) getPr�fling().getAllAttributes().get(input.substring(1, input.length() - 1));
						funcs.logDebug(2, "Dynamisches Attribut aufgel�st: " + result, "Resolved dynamic attribute: " + result, this.getClass());
					}
					catch (Exception e)
					{
						result = input.substring(0, input.length() - 1);						
						funcs.logDebug(0, "Fehler beim dynamischen Aufl�sen. Es wird zur�ck gegeben: " + result, "Error during dynamic lookup. It will be returned: " + result, this.getClass());						
					}					
				else
				{
					result = null;
					funcs.logDebug(0, "L�nge ung�ltig. Es wird \"null\" zur�ck gegeben.", "Length invalid. \"null\" is returned.", this.getClass());
				}
			}
			else
			{
				result = input.substring(1, input.length());
				funcs.logDebug(0, "Zweites \"@\" nicht gefunden. Es wird zur�ck gegeben: " + result, "Did not find second \"@\". It will be returned: " + result, this.getClass());
			}
		}
		else
			if (input.endsWith("@"))
			{
				result = input.substring(0, input.length() - 1);
				funcs.logDebug(0, "Erstes \"@\" nicht gefunden. Es wird zur�ck gegeben: " + result, "Did not find first \"@\". It will be returned: " + result, this.getClass());
			}				
			else
			{
				funcs.logDebug(2, "Kein \"@\" gefunden. Statisches Attribut. Value: " + result, "No \"@\" found. Static attribute. Value: " + result, this.getClass());
				result = input;
			}
		
		return result;
	}

    /**
     * Liefert true zur�ck, wenn deutsche Texte verwendet werden sollen.
     * @result True bei deutsche texte, false sonst.
     */
    private static boolean isDE()
    {
        try 
        {
            if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true ) return true;
            else return false;
        } 
        catch (Exception e) 
        {
            return false;   //default is english
        }
    }

    /**
     * Sendet ein Resetsignal an eine / mehrere Karten.
     * @param s_Ip 			IP-Adresse des Ziels
     * @param b_Broadcast	Broadcast ja / nein
     */
    private void sendReset (String s_Ip, boolean b_Broadcast)
    {    	
    	try
    	{
        	Helper c_Helper = new Helper ();
			DatagramSocket c_socket;
			
			c_socket = new DatagramSocket (IPDefs.RESET);	
			c_socket.setBroadcast(b_Broadcast);
			c_socket.setSoTimeout(2000);  
			
			//Send Reset signal
			if ((c_socket != null) && (c_socket.isBound()))
			{
				try    				
				{
					byte[] buffer = new byte[4096];
					byte[] data = {'1'};
					
					DatagramPacket c_Packet = new DatagramPacket (buffer, buffer.length);
					c_Packet.setData(data);
					c_Packet.setLength(data.length);
					c_Packet.setAddress (InetAddress.getByName(s_Ip));					    					
					c_Packet.setPort(IPDefs.RESET);
				
					c_socket.send(c_Packet);
					c_Helper.timer(500);
					c_socket.send(c_Packet);
					c_Helper.timer(500);
					c_socket.send(c_Packet);
					c_Helper.timer(500);
				}
				catch (Exception e)
				{
					;
				}
				c_socket.close();
			}    			
    	}
    	catch (Exception e)
    	{
    		;
    	}    	
    }
}

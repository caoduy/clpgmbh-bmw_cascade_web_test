/*
 * Calc_11_0_F_Pruefprozedur.java Created on 10.03.03 Created: 10.03.03 Last modified: 20.04.2004
 * Author: Peter Winklhofer, BMW TI-430 / Andreas Mair, BMW TI-432
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import com.bmw.appframework.logging.LogLevel;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.util.logging.CascadeLogging;

/**
 * Diese Pr�fprozedur f�hrt eine der folgenden Rechenoperationen
 * ADD,SUB,MUL,DIV,AVERAGE,TERM,POLYNOMIAL,GREATER,GREATER_OR_EQUAL,LESS,LESS_OR_EQUAL. auf
 * einem/zwei Parametern (PARA1,PARA2=Konstante oder RESULT-ID@PP.P) durch und stellt das Ergebnis
 * als neues CASCADE-Result anderen Pr�fschritten zur Verf�gung.
 * 
 * @author Peter Winklhofer, TI-431
 * @author Andreas Mair, TI-432
 * @author Peter Rettig, Gefasoft GmbH
 * @author Peter Voelz, GEFASOFT Engineering GmbH
 * @version Implementierung
 * @version 0_0_1_FA PW 10.03.03 Erstellung (Winklhofer) <BR>
 * @version 0_0_2_FA AM Erweiterung f�r mehr Parameter, die in der Rechenoperation verkn�pft werden<BR>
 * @version 0_0_3_FA AM Optionale Argumente MIN, MAX. Zus�tzliche Operation AVERAGE (Mair)<BR>
 * @version 0_0_4_FA AM Ergebnisname = Pr�fschrittname, oder mit optionalem Argument parametrierbar
 *          (Wiederverwendbarkeit)<BR>
 * @version 0_0_5_FA AM Ergebnis-ID = Pr�fschrittname, oder mit optionalem Argument parametrierbar
 *          (Wiederverwendbarkeit)<BR>
 *          AM Es kommt bei dem Verweis auf ein anderes Ergebnis auf die ID an, nicht auf den Namen!<BR>
 * @version 0_0_6_FA AM Kein Test mehr, ob �berfl�ssige Argumente gesetzt! Keine Probleme bei
 *          Erweiterung um optionales Argument<BR>
 *          AM mit Kompatibilit�t. �ber Pr�flingsattribut bzw. PP-Argument aktivierbare
 *          Debugausgaben.<BR>
 * @version 0_0_7_FA AM M�glichkeit einen Hinweistext anzugeben, der angezeigt wird, falls der
 *          Schritt mit NIO bewertet wird.<BR>
 * @version 0_0_8_FA AM Zwei weitere Operationen TERM und POLYNOMIAL.<BR>
 * @version 0_0_9_FA AM Debug bzgl. Ergebnisablage im virtuellen Fahrzeug.<BR>
 * @version 0_1_0_FA AM Neue Operationen GREATER, GREATER_OR_EQUAL, LESS, LESS_OR_EQUAL .<BR>
 *          AM Neues optionales Argument DECIMAL_PLACES, mit dem man bei Zahlenergebnissen die
 *          maximale Anzahl von Nachkommastellen vorgeben kann. <BR>
 * @version 0_1_1_FA AM Neuer optionaler Parameter: ABSOLUTE_VALUE. Wenn vorhanden und == TRUE, dann
 *          wird vom Ergebnis <BR>
 *          AM der Berechnung noch der Absolutwert gebildet. Hat nur bei Rechenoperationen eine
 *          Auswirkung. <BR>
 *          AM Erweiterung der Fehlerausgabe, wenn referenzierte Ergebnisse aus dem virtuellen FZG
 *          nicht verf�gbar. <BR>
 * @version 12_0_F PR Bugfix: Vector mit Ergebnissen wird nun pro Ausf�hrung neu angelegt
 *          (andernfalls referenzieren die pro Ausf�hrung erzeugten Ergebnis-Objekte immer das
 *          gleiche Vector-Objekt). <BR>
 * @version 15_0_F CS APDM Ergebnisformat angepasst, deprecated CascdeLogManager durch
 *          CascadeLogging abgel�st <BR>
 * @version 16_0_F CS SGBD, wenn vorhanden, als APDM Ergebnis hinzugef�gt. <BR>
 * @version 18_0_F CS Freigabe 16_0_F. <BR>
 * @version 19_0_F FG Ausf�hrungszeit in kleiner 1ms --> Fehleinordnung in APDM; k�nstliche Pause eingef�gt <BR>
 * @version 20_0_T PV @-Pr�fergebnisse werden auch verwendet wenn diese n.i.O. sind <BR>
 * @version 21_0_F PV F-Version <BR>
 * @version 22_0_F UP 04.06.2014 Bei einer Division ist es jetzt erlaubt, dass der erste Parameter gleich Null ist<BR>
 * @version 23_0_T MS Bugfix, bei Division durch 0 wird der Pr�fschritt NIO
 * @version 24_0_F Freigabe 23_0_T
 * @version 25_0_T CW 21.07.2017: Zugriff mit Pr�fstands�bergreifenden @-Operator funktioniert nicht beim SMART-Server <BR>
 * @version 26_0_F CW 21.07.2017: Freigabe 25_0_T <BR>
 */
public class Calc_26_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public Calc_26_0_F_Pruefprozedur() {
	}

	// immer notwendig
	Ergebnis i_oResult;
	int i_iStatus = STATUS_EXECUTION_OK;

	// Strings for the i_oResult objects in execute
	String i_sID = ""; // ID
	String i_sWerkzeug = ""; // werkzeug
	String i_sParameter_1 = ""; // parameter_1
	String i_sParameter_2 = ""; // parameter_2
	String i_sParameter_3 = ""; // parameter_3
	String i_sErgebnis = ""; // ergebnis
	String i_sErgebniswert = ""; // ergebniswert
	String i_sMinWert = ""; // minWert
	String i_sMaxWert = ""; // maxWert
	String i_sWiederholungen = ""; // wiederholungen
	String i_sFrageText = ""; // frageText
	String i_sAntwortText = ""; // antwortText
	String i_sAnweisText = ""; // anweisText
	String i_sFehlerText = ""; // fehlertext
	String i_sHinweisText = ""; // hinweistext
	String i_sFehlertyp = ""; // fehlertyp

	// Debug flags
	boolean i_bCalcDebug_Standard = false; // CALC
	boolean i_bCalcDebug_CheckArgs = false;
	boolean i_bCalcDebug_DebugMode = false;

	/**
	 * erzeugt eine neue Pruefprozedur, die auf Einhaltung bzw. auf Ausschlu� der zu analysierenden
	 * Werte pr�ft.
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public Calc_26_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "PARA2..N", "BASIS", "MIN", "MAX", "RES_NAME", "HWT", "DECIMAL_PLACES", "ABSOLUTE_VALUE", "DEBUG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "PARA1", "OP" };
		return args;
	}

	/**
	 * Checks the validity of the arguments.
	 */
	public boolean checkArgs() {

		// Method variables
		boolean ok = true; // return value
		int i, j;
		boolean required = false, optional = false;
		String strCurrentKey, strCurrentArg;

		// Debug output on begin of checkArgs()
		if( i_bCalcDebug_CheckArgs ) {
			Enumeration enu = getArgs().keys();

			CascadeLogging.getLogger().log( LogLevel.INFO, "All Arguments from this PP: " + this.getName() );
			while( enu.hasMoreElements() ) {
				// The current argument
				strCurrentKey = (String) enu.nextElement();
				strCurrentArg = getArg( strCurrentKey );
				CascadeLogging.getLogger().log( LogLevel.INFO, strCurrentKey + " = " + strCurrentArg );
			}
		}

		// Checks
		// -------
		try {
			// 1. Check: Are present all requiredArgs from the list
			String[] requiredArgs = getRequiredArgs();
			for( i = 0; i < requiredArgs.length; i++ ) {
				if( getArg( "PARA1" ) == null || getArg( "PARA1" ).equals( "" ) )
					return false;
				else if( getArg( "OP" ) == null || getArg( "OP" ).equals( "" ) )
					return false;
			}

			// 2. Check: The required parameter OP must be valid
			// (ADD,SUB,MUL,DIV,AVERAGE,TERM,POLYNOMIAL,GREATER,GREATER_OR_EQUAL,LESS or
			// LESS_OR_EQUAL)
			String op = getArg( "OP" );
			if( !validOperation( op ) )
				return false;

			// 3. Check: Required or optional and valid
			Enumeration enu = getArgs().keys();
			// For all arguments
			while( enu.hasMoreElements() ) {
				strCurrentKey = (String) enu.nextElement();
				strCurrentArg = getArg( strCurrentKey );
				required = false;
				optional = false;

				// Test, if required argument
				j = 0;
				while( (required == false) && (j < requiredArgs.length) ) {
					if( strCurrentKey.equals( requiredArgs[j] ) == true )
						required = true;
					j++;
				}

				// Test, if optional argument and valid
				// -------------------------------------

				// PARA...
				if( strCurrentKey.startsWith( "PARA" ) ) {
					String sParaLine = null;
					sParaLine = strCurrentArg;

					if( sParaLine != null ) {

						// Checks if the index after PARA... is an integer value (is valid)
						String strIndex = strCurrentKey.substring( 4 );
						int iIndex = 0;
						try {
							iIndex = (int) Integer.parseInt( strIndex );
							if( iIndex >= 2 )
								optional = true;
						} catch( NumberFormatException e ) {
							if( i_bCalcDebug_CheckArgs ) {
								CascadeLogging.getLogger().log( LogLevel.WARNING, "Error on parsing the index of PARA..." );
							}
							return false;
						}

						// Checks the parameter details (of PARA...) for the operation TERM and
						// POLYNOMIAL
						// -number of parameter details
						// -type of parameter details (double)
						if( op.equals( "TERM" ) || op.equals( "POLYNOMIAL" ) ) {
							String[] summand = null;
							summand = extractValuesRedesigned( sParaLine );
							int iParaLen = 0;
							if( op.equals( "TERM" ) )
								iParaLen = 3;
							if( op.equals( "POLYNOMIAL" ) )
								iParaLen = 2;

							if( summand != null ) {
								// If the number of parameter details is correct
								if( summand.length == iParaLen ) {
									for( int s = 0; s < iParaLen; s++ ) {
										try {
											// If the argument detail is a constant value
											if( summand[s].indexOf( '@' ) == -1 ) {
												Double.parseDouble( summand[s] );
											}
											// If the argument detail is a reference on a result in
											// the virtual vehicle
											else {
												Double.parseDouble( extractValuesRedesigned( sParaLine )[0] );
											}
										} catch( NumberFormatException e ) {
											if( i_bCalcDebug_CheckArgs ) {
												CascadeLogging.getLogger().log( LogLevel.WARNING, "Parameter[n] isn't valid" );
											}
											return false;
										}
									}
								}
								// Wrong number of parameter details
								else {
									if( i_bCalcDebug_CheckArgs ) {
										CascadeLogging.getLogger().log( LogLevel.WARNING, "length != 3, ength != 2" );
									}
									return false;
								}
							} else {
								if( i_bCalcDebug_CheckArgs ) {
									CascadeLogging.getLogger().log( LogLevel.WARNING, "summand == null" );
								}
								return false;
							}
						}
						// Checks the parameter details (of PARA...) for all other operations
						// -one parameter detail
						// -type of parameter detail (double)
						else {
							try {
								// If the argument detail is a constant value
								if( sParaLine.indexOf( '@' ) == -1 ) {
									Double.parseDouble( sParaLine );
								}
								// If the argument detail is a reference on a result in the virtual
								// vehicle
								else {
									Double.parseDouble( extractValuesRedesigned( sParaLine )[0] );
								}
							} catch( NumberFormatException e ) {
								if( i_bCalcDebug_CheckArgs ) {
									CascadeLogging.getLogger().log( LogLevel.WARNING, "Parameter[0] isn't valid" );
								}
								return false;
							}
						}
						if( i_bCalcDebug_CheckArgs ) {
							CascadeLogging.getLogger().log( LogLevel.WARNING, "Check of parameter line: " + sParaLine + " is ok" );
						}
					} else {
						if( i_bCalcDebug_CheckArgs ) {
							CascadeLogging.getLogger().log( LogLevel.WARNING, "sParaLine == null" );
						}
						return false;
					}
				} // End if PARA...

				// BASIS
				else if( strCurrentKey.equals( "BASIS" ) ) {
					double limitVal = 0.0;
					optional = true;

					try {
						// If the argument detail is a constant value
						if( strCurrentArg.indexOf( '@' ) == -1 ) {
							Double.parseDouble( strCurrentArg );
						}
						// If the argument detail is a reference on a result in the virtual vehicle
						else {
							Double.parseDouble( extractValuesRedesigned( strCurrentArg )[0] );
						}
					} catch( NumberFormatException e ) {
						if( i_bCalcDebug_CheckArgs ) {
							CascadeLogging.getLogger().log( LogLevel.WARNING, "The value for argument BASIS isn't valid" );
						}
						return false;
					}
				} else if( strCurrentKey.equals( "MIN" ) || strCurrentKey.equals( "MAX" ) ) { // limits
					double limitVal = 0.0;
					try {
						limitVal = (double) Double.parseDouble( strCurrentArg );
					} catch( NumberFormatException e ) {
						return false;
					}
					optional = true;
				} else if( strCurrentKey.equals( "RES_NAME" ) ) {
					optional = true;
				} else if( strCurrentKey.equals( "HWT" ) ) {
					optional = true;
				} else if( strCurrentKey.equals( "DECIMAL_PLACES" ) ) {
					int iDecPlaces = 0;
					try {
						iDecPlaces = (int) Integer.parseInt( strCurrentArg );
					} catch( NumberFormatException e ) {
						return false;
					}
					optional = true;
				} else if( strCurrentKey.equals( "ABSOLUTE_VALUE" ) ) {
					optional = true;
				} else if( strCurrentKey.equals( "DEBUG" ) ) {
					optional = true;
				}

				// When neither required nor optional
				// This check has only an effect on the log entries in order to have compatibility
				// when a new argument
				// will be implemented.
				if( !(required || optional) ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "The argument " + strCurrentKey + " is neither required nor optional" );
					CascadeLogging.getLogger().log( LogLevel.INFO, "This should happen only in the transit time when new optional arguments are introdueced" );
				}
			}

			// 4 Check: If the Operation is POLYNOMIAL the Argument BASIS must exist
			if( op.equalsIgnoreCase( "POLYNOMIAL" ) ) {
				if( getArg( "BASIS" ) == null ) {
					if( i_bCalcDebug_CheckArgs ) {
						CascadeLogging.getLogger().log( LogLevel.INFO, "No BASIS argument which is necessary with the operation POLYNOMIAL!" );
					}
					return false;
				}
			}

			// 6 Check: If the Operation isn't TERM or POLYNOMIAL there must be present at least 2
			// PARA... arguments
			if( !(op.equalsIgnoreCase( "TERM" ) || op.equalsIgnoreCase( "POLYNOMIAL" )) ) {
				if( paraNumber() < 2 ) {
					if( i_bCalcDebug_CheckArgs ) {
						CascadeLogging.getLogger().log( LogLevel.INFO, "To less PARA... arguments!" );
					}
					return false;
				}
			}

			// 7 Check: If the operation is a logical one, no MIN-,MAX- evaluation is possible or it
			// is senseless
			if( logicalOperation( op ) ) {
				if( getArg( "MIN" ) != null || getArg( "MAX" ) != null ) {
					if( i_bCalcDebug_CheckArgs ) {
						CascadeLogging.getLogger().log( LogLevel.INFO, "No MIN-, MAX evalutaion for a logical operation!" );
					}
					return false;
				}
			}

			if( i_bCalcDebug_CheckArgs ) {
				CascadeLogging.getLogger().log( LogLevel.INFO, "ok = " + ok );
				CascadeLogging.getLogger().log( LogLevel.INFO, "End checkArgs() from Calc." );
			}

			return ok;
		} catch( Exception e ) {
			if( i_bCalcDebug_CheckArgs ) {
				CascadeLogging.getLogger().log( LogLevel.WARNING, "Exception in CheckArgs", e );
			}
			return false;
		} catch( Throwable e ) {
			if( i_bCalcDebug_CheckArgs ) {
				CascadeLogging.getLogger().log( LogLevel.WARNING, "Throwable in CheckArgs", e );
			}
			return false;
		}
	}

	/**
	 * Splittet den �bergebenen Argumente-/Attribute-String in die diversen Alternativen auf und
	 * ersetzt abschlie�end eventuell vorhandene Links durch die entsprechenden Werte
	 * 
	 * @throws PPExecutionException falls Werte nicht verf�gbar bzw. als nicht verwertbar angesehen
	 *             wird.
	 * @param arg der zu bearbeitende Argument- oder Attributstring
	 * @return Array mit den Werten
	 */
	private String[] extractValuesRedesigned( String arg ) throws PPExecutionException {
		String[] args;

		args = splitArg( arg );
		for( int i = 0; i < args.length; i++ ) {
			if( args[i].indexOf( '@' ) != -1 ) {
				args[i] = getResultRedesigned( args[i].toUpperCase() );
			}
		}
		return args;
	}

	/**
	 * Liefert den Ergebniswert einer Pr�fprozedur eines Pr�flings Originalmethode in der
	 * AbstractPruefprozedur funktioniert nicht, wenn der Ergebnisstatus nicht STATUS_EXECUTION_OK
	 * war
	 * 
	 * @return der Wert des Ergebnisses
	 * @param resultName Name des Ergebnisses entsprechend der Form
	 *            'ergname@pruefprozedur(.pruefling)'.
	 * @throws PPExecutionException falls Result nicht verf�gbar bzw. nicht verwertbar ist.
	 */
	private String getResultRedesigned( String resultName ) throws PPExecutionException {

		Pruefprozedur pp;
		String temp;
		String resultValue = null;
		int idx;

		resultName = resultName.toUpperCase();

		// Formatcheck
		if( (idx = resultName.indexOf( '@' )) < 0 ) {
			throw new PPExecutionException( "Token '@' in " + resultName + " nicht enthalten" );
		}
		temp = resultName.substring( idx + 1 );
		if( temp.indexOf( "." ) < 0 ) {
			// Kein Punkt somit Suche in eigenen Pr�fling ...
			pp = getPr�fling().getPr�fprozedur( temp );
		} else if( temp.indexOf( "." ) == temp.lastIndexOf( "." ) ) {
			// Ein Punkt enthalten, Suche in einem anderen Pr�fling �ber den Auftrag...
			pp = getPr�fling().getAuftrag().getPr�fprozedur( temp );
		} else {
			// 2 oder mehr Punkte, somit Fehler
			throw new PPExecutionException( "Punktnotation in " + resultName + " fehlerhaft" );
		}
		if( pp == null ) {
			throw new PPExecutionException( "Pr�fprozedur " + temp + " nicht vorhanden" );
		}
		if( pp.isValid() == false )
			throw new PPExecutionException( "Status der Pr�fprozedur " + temp + " nicht verwendbar" );

		// Jetzt das Ergebnis holen
		idx = resultName.indexOf( '@' );
		temp = resultName.substring( 0, idx ).trim();

		try {
			//resultValue = pp.getHistoryListe().getLatestErgebnis( temp ).getErgebnisWert();		
			resultValue = ((AbstractBMWPruefprozedur) pp).getPPResult( resultName );
		} catch( NullPointerException e ) {
			throw new PPExecutionException( "Result " + resultName + " nicht vorhanden" );
		} catch( InformationNotAvailableException e ) {
			e.printStackTrace();
		} catch( Exception e ) {
			e.printStackTrace();
		}

		if( resultValue == null ) {
			throw new PPExecutionException( "Result " + resultName + " nicht vorhanden" );
		}
		return resultValue;
	}

	/**
	 * Executes the test-procedure
	 * 
	 * @param info information for the execution
	 */
	public void execute( ExecutionInfo info ) {
		initInstanceVariables();

		// Method variables
		Vector i_oErgListe = new Vector();

		String strBase = "0";
		Vector strCoefficients = new Vector( 2 );
		Vector strOperands = new Vector( 2 );
		Vector strPowers = new Vector( 2 );

		String op;
		double dResOperation = 0.0;
		boolean bResOperation = false;
		boolean bAbsoluteValue = false;
		double dBase = 0.0;
		double dMin = 0.0, dMax = 0.0;
		boolean bMin = false, bMax = false; // A min-, max- evaluation is requested
		int iMaxDecimalPlaces = 2;

		//timestamp
		long timestamp = System.currentTimeMillis();

		/*
		 * Sets the debug flags depending on the optional debug argument in the PP or from the debug
		 * attribute in the P.
		 */
		String debugString = null;
		if( getArg( "DEBUG" ) != null ) {
			debugString = getArg( "DEBUG" ); // debug argument in PP (preference)
		} else if( getPr�fling().getAttribut( "DEBUG" ) != null ) {
			debugString = getPr�fling().getAttribut( "DEBUG" ); // debug attribute in P
		}
		if( debugString != null ) {
			String[] debugFlags = splitArg( debugString );
			int n = debugFlags.length;
			if( n >= 1 ) {
				for( int i = 0; i < n; i++ ) {
					if( debugFlags[i].equalsIgnoreCase( "CALC" ) ) {
						i_bCalcDebug_Standard = true;
					} else if( debugFlags[i].equalsIgnoreCase( "CALC_CHECK_ARGS" ) ) {
						i_bCalcDebug_CheckArgs = true;
					} else if( debugFlags[i].equalsIgnoreCase( "CALC_DEBUG_MODE" ) ) {
						i_bCalcDebug_DebugMode = true;
					} else if( debugFlags[i].equalsIgnoreCase( "ALL" ) ) {
						i_bCalcDebug_Standard = true;
						i_bCalcDebug_CheckArgs = true;
						i_bCalcDebug_DebugMode = true;
					}
				}
			}
		}

		// Initial debug lines
		if( i_bCalcDebug_Standard ) {
			CascadeLogging.getLogger().log( LogLevel.INFO, "---" );
			CascadeLogging.getLogger().log( LogLevel.INFO, "Start execute() from  Calc." );
			CascadeLogging.getLogger().log( LogLevel.INFO, "Test step: " + this.getName() );
			CascadeLogging.getLogger().log( LogLevel.INFO, "PP-version: 10j" );
			CascadeLogging.getLogger().log( LogLevel.INFO, "---" );
		}
		if( debugString != null && i_bCalcDebug_DebugMode ) {
			CascadeLogging.getLogger().log( LogLevel.INFO, "The debug string is: " + debugString );
			CascadeLogging.getLogger().log( LogLevel.INFO, "As a i_oResult of debug arguments/attributes the debug flags are: " );
			CascadeLogging.getLogger().log( LogLevel.INFO, "i_bCalcDebug_Standard = " + i_bCalcDebug_Standard );
			CascadeLogging.getLogger().log( LogLevel.INFO, "i_bCalcDebug_CheckArgs = " + i_bCalcDebug_CheckArgs );
			CascadeLogging.getLogger().log( LogLevel.INFO, "i_bCalcDebug_DebugMode = " + i_bCalcDebug_DebugMode );
		}

		try {
			// Parameter check
			// ----------------
			if( !checkArgs() ) {
				if( i_bCalcDebug_CheckArgs ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Argument error on using checkArgs() in execute()" );
				}
				resetResultStrings();
				i_sID = "ARGUMENT_ERROR";
				i_sWerkzeug = "CHECK PARAMETERS IN EXECUTE";
				i_sFehlerText = PB.getString( "parametrierfehler" );

				String[] missingResults = getMissingReferencesInAllArguments();
				if( missingResults != null ) {
					i_sHinweisText = "Invalid CASCASDE result(s): ";
					for( int i = 0; i < missingResults.length; i++ ) {
						i_sHinweisText += missingResults[i];
					}
				} else {
					i_sHinweisText = "CHECK ARGS IN CALC NOT SUCCESSFUL!";
				}

				i_sFehlertyp = Ergebnis.FT_NIO_SYS;
				// Pass a new exception upwards (PPExecutionException -> error in parameters!)
				throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			} else {
				if( i_bCalcDebug_CheckArgs ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Arguments ok on using checkArgs() in execute()" );
				}

				try {
					// Einlesen der Operanden
					// -----------------------
					op = getArg( "OP" );

					// Size of vectors
					int iPara = paraNumber();
					if( op.equals( "TERM" ) ) {
						strCoefficients.setSize( iPara );
						strOperands.setSize( iPara );
						strPowers.setSize( iPara );
					} else if( op.equals( "POLYNOMIAL" ) ) {
						strCoefficients.setSize( iPara );
						strPowers.setSize( iPara );
					} else {
						strOperands.setSize( iPara );
					}

					String strCurrentKey;
					Enumeration enu = getArgs().keys();
					String[] sElementsInParaLine = null;
					while( enu.hasMoreElements() ) {
						strCurrentKey = (String) enu.nextElement();
						if( strCurrentKey.startsWith( "PARA" ) ) {
							String strIndex = strCurrentKey.substring( 4 );
							int iIndex = 0; // 1 based!
							int insertIndex = 0; // 0 based!
							iIndex = Integer.parseInt( strIndex );
							insertIndex = iIndex - 1;
							sElementsInParaLine = extractValuesRedesigned( getArg( strCurrentKey ) );

							if( i_bCalcDebug_CheckArgs ) {
								CascadeLogging.getLogger().log( LogLevel.INFO, "The parameter PARA" + iIndex + " read in:" );
							}
							if( i_bCalcDebug_CheckArgs ) {
								CascadeLogging.getLogger().log( LogLevel.INFO, "Capacity of strOperands: " + strOperands.capacity() );
								CascadeLogging.getLogger().log( LogLevel.INFO, "Size of strOperands: " + strOperands.size() );
							}

							if( op.equals( "TERM" ) ) {
								strCoefficients.set( insertIndex, sElementsInParaLine[0] );
								strOperands.set( insertIndex, sElementsInParaLine[1] );
								strPowers.set( insertIndex, sElementsInParaLine[2] );
								if( i_bCalcDebug_CheckArgs ) {
									CascadeLogging.getLogger().log( LogLevel.INFO, "sElementsInParaLine[0] = " + sElementsInParaLine[0] );
									CascadeLogging.getLogger().log( LogLevel.INFO, "sElementsInParaLine[1] = " + sElementsInParaLine[1] );
									CascadeLogging.getLogger().log( LogLevel.INFO, "sElementsInParaLine[2] = " + sElementsInParaLine[2] );
								}
							} else if( op.equals( "POLYNOMIAL" ) ) {
								strCoefficients.set( insertIndex, sElementsInParaLine[0] );
								strPowers.set( insertIndex, sElementsInParaLine[1] );
								if( i_bCalcDebug_CheckArgs ) {
									CascadeLogging.getLogger().log( LogLevel.INFO, "sElementsInParaLine[0] = " + sElementsInParaLine[0] );
									CascadeLogging.getLogger().log( LogLevel.INFO, "sElementsInParaLine[1] = " + sElementsInParaLine[1] );
								}
							} else {
								strOperands.set( insertIndex, sElementsInParaLine[0] );
								if( i_bCalcDebug_CheckArgs ) {
									CascadeLogging.getLogger().log( LogLevel.INFO, "sElementsInParaLine[0] = " + sElementsInParaLine[0] );
								}
							}
						} else if( strCurrentKey.equals( "BASIS" ) ) {
							strBase = getArg( strCurrentKey );
							// Wenn kein Verweis auf ein anderes Ergebnis sondern Konstante
							if( strBase.indexOf( '@' ) == -1 ) {
								dBase = Double.parseDouble( strBase );
							}
							// Wenn Verweis auf ein Ergebnis im virtuellen Fahrzeug
							else {
								strBase = extractValuesRedesigned( strBase )[0];
								dBase = Double.parseDouble( strBase );
							}
						} else if( strCurrentKey.equals( "MIN" ) ) {
							bMin = true;
							dMin = (double) Double.parseDouble( getArg( strCurrentKey ) );
						} else if( strCurrentKey.equals( "MAX" ) ) {
							bMax = true;
							dMax = (double) Double.parseDouble( getArg( strCurrentKey ) );
						} else if( strCurrentKey.equals( "DECIMAL_PLACES" ) ) {
							iMaxDecimalPlaces = Integer.parseInt( getArg( strCurrentKey ) );
						} else if( strCurrentKey.equals( "ABSOLUTE_VALUE" ) ) {
							if( getArg( "ABSOLUTE_VALUE" ).equals( "TRUE" ) )
								bAbsoluteValue = true;
						}
					}
					// Delete the void elements in the vectors
				} catch( Exception e ) {
					resetResultStrings();
					i_sID = "ARGUMENT_ERROR";
					i_sWerkzeug = "CHECK PARAMETERS IN EXECUTE";
					i_sFehlerText = PB.getString( "parametrierfehler" );
					i_sHinweisText = "READ PARAMETERS IN CALC NOT SUCCESSFUL! -> " + e.toString();
					i_sFehlertyp = Ergebnis.FT_NIO_SYS;
					// Pass a new exception upwards (PPExecutionException -> error in parameters!)
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				}

				if( i_bCalcDebug_CheckArgs ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Arguments ok after reading in the parameters in execute()" );
				}
			}

			// Numbervectors as debug print
			if( i_bCalcDebug_Standard ) {
				if( op.equals( "TERM" ) ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: strCoefficients:" );
					debugPrintVector( strCoefficients );
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: Basis: " + strBase );
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: strPowers:" );
					debugPrintVector( strPowers );

				} else if( op.equals( "POLYNOMIAL" ) ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: strCoefficients:" );
					debugPrintVector( strCoefficients );
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: strOperands:" );
					debugPrintVector( strOperands );
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: strPowers:" );
					debugPrintVector( strPowers );
				} else {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: strOperands:" );
					debugPrintVector( strOperands );
				}
			}

			// Elements for the result object
			// -------------------------------
			// default strings
			resetResultStrings();
			i_sFehlertyp = Ergebnis.FT_IO;

			if( getArg( "RES_NAME" ) != null ) { // ID
				i_sID = getArg( "RES_NAME" );
			} else {
				i_sID = this.getName();
			}
			i_sWerkzeug = "CALC"; // werkzeug
			if( i_bCalcDebug_Standard ) {
				CascadeLogging.getLogger().log( LogLevel.INFO, "i_sParameter_1=" + i_sParameter_1 );
			}

			i_sParameter_1 = getArgumentDokuString( "PARA1" ); // parameter_1
			i_sParameter_2 = getArg( "OP" ); // parameter_2
			if( getArg( "PARA2" ) != null ) {
				i_sParameter_3 = getArgumentDokuString( "PARA2" ); // parameter_3
			}
			if( strOperands.size() > 2 ) {
				i_sParameter_3 = i_sParameter_3 + ", ...";
			}
			// ergebnis
			if( op.equalsIgnoreCase( "ADD" ) ) {
				i_sErgebnis = "SUMME";
				dResOperation = add( strOperands );
				if( i_bCalcDebug_Standard ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: execute(): ADD" );
				}
			} else if( op.equalsIgnoreCase( "SUB" ) ) {
				i_sErgebnis = "DIFFERENZ";
				dResOperation = sub( strOperands );
				if( i_bCalcDebug_Standard ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: execute(): SUB" );
				}
			} else if( op.equalsIgnoreCase( "MUL" ) ) {
				i_sErgebnis = "PRODUKT";
				dResOperation = mul( strOperands );
				if( i_bCalcDebug_Standard ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: execute(): MUL" );
				}
			} else if( op.equalsIgnoreCase( "DIV" ) ) {
				i_sErgebnis = "QUOTIENT";
				if( readyForDiv( strOperands ) ) {
					dResOperation = div( strOperands );
				} else {
					i_sID = "ParaFehler";
					i_iStatus = STATUS_EXECUTION_ERROR;
					i_sFehlerText = PB.getString( "parametrierfehler" );
					i_sFehlertyp = Ergebnis.FT_NIO_SYS;
					i_sFehlerText = "Division by zero";
				}
				if( i_bCalcDebug_Standard ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: execute(): DIV" );
				}
			} else if( op.equalsIgnoreCase( "AVERAGE" ) ) {
				i_sErgebnis = "AVERAGE";
				double opNumbers = (double) strOperands.size();
				dResOperation = add( strOperands ) / opNumbers;
				if( i_bCalcDebug_Standard ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: execute(): AVERAGE" );
				}
			} else if( op.equalsIgnoreCase( "TERM" ) ) {
				i_sErgebnis = "TERM";
				dResOperation = term( strCoefficients, strOperands, strPowers );
				if( i_bCalcDebug_Standard ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: execute(): TERM" );
				}
			} else if( op.equalsIgnoreCase( "POLYNOMIAL" ) ) {
				i_sErgebnis = "POLYNOMIAL";
				String b = " with basis ";
				b = b.concat( strBase );
				i_sParameter_2 = i_sParameter_2.concat( b );
				dResOperation = poynomial( strCoefficients, dBase, strPowers );
				if( i_bCalcDebug_Standard ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: execute(): POLYNOMIAL" );
				}
			} else if( op.equalsIgnoreCase( "GREATER" ) ) {
				i_sErgebnis = "GREATER THAN";
				bResOperation = greater_than( strOperands );
				if( i_bCalcDebug_Standard ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: execute(): GREATER" );
				}
			} else if( op.equalsIgnoreCase( "GREATER_OR_EQUAL" ) ) {
				i_sErgebnis = "GREATER THAN OR EQUAL";
				bResOperation = greater_than_or_equal( strOperands );
				if( i_bCalcDebug_Standard ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: execute(): GREATER_OR_EQUAL" );
				}
			} else if( op.equalsIgnoreCase( "LESS" ) ) {
				i_sErgebnis = "LESS THAN";
				bResOperation = less_than( strOperands );
				if( i_bCalcDebug_Standard ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: execute(): LESS" );
				}
			} else if( op.equalsIgnoreCase( "LESS_OR_EQUAL" ) ) {
				i_sErgebnis = "LESS THAN OR EQUAL";
				bResOperation = less_than_or_equal( strOperands );
				if( i_bCalcDebug_Standard ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: execute(): LESS_OR_EQUAL" );
				}
			} else {
				resetResultStrings();
				i_sID = "ARGUMENT_ERROR";
				i_sWerkzeug = "CALCULATE OPERATION IN EXECUTE";
				i_sFehlerText = PB.getString( "parametrierfehler" );
				i_sHinweisText = "UNKNOWN OPERATION:" + op;
				i_sFehlertyp = Ergebnis.FT_NIO_SYS;
				// Pass a new exception upwards (PPExecutionException -> error in parameters!)
				throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			}
			// Ergebniswert
			if( !logicalOperation( op ) ) {
				// Runden
				if( iMaxDecimalPlaces <= 0 ) {
					dResOperation = Math.round( dResOperation );
				} else {
					dResOperation = Math.round( dResOperation * Math.pow( 10d, (double) iMaxDecimalPlaces ) ) / Math.pow( 10d, (double) iMaxDecimalPlaces );
				}
				// Absolutwert
				if( bAbsoluteValue )
					dResOperation = Math.abs( dResOperation );
				// Ergebnisstring
				i_sErgebniswert = String.valueOf( dResOperation );
				// Nullen nach dem Komma (vorhanden wegen Umwandlung in String) entfernen, falls
				// vorhanden
				int pointIndex = i_sErgebniswert.indexOf( '.' );
				String sDecimalPlaces = i_sErgebniswert.substring( pointIndex + 1 );
				int iDecimalPlaces = sDecimalPlaces.length();
				for( int s = 0; s < (iDecimalPlaces + 1); s++ ) {
					if( i_sErgebniswert.endsWith( "0" ) || i_sErgebniswert.endsWith( "." ) ) {
						i_sErgebniswert = i_sErgebniswert.substring( 0, i_sErgebniswert.length() - 1 );
					} else {
						s = iDecimalPlaces + 1;
					}
				}
			} else
				i_sErgebniswert = String.valueOf( bResOperation );
			if( i_bCalcDebug_Standard ) {
				CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: execute(): Result = " + i_sErgebniswert );
			}
			// minWert, maxWert
			String strMinWert = "", strMaxWert = "";
			if( bMin && bMax ) {
				strMinWert = getArg( "MIN" );
				strMaxWert = getArg( "MAX" );
				i_sMinWert = strMinWert;
				i_sMaxWert = strMaxWert;

				if( i_bCalcDebug_Standard ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: execute(): min = " + dMin );
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: execute(): max = " + dMax );
				}

				if( !(dMin <= dResOperation && dResOperation <= dMax) ) {
					i_iStatus = STATUS_EXECUTION_ERROR; // ausserhalb Toleranz
					i_sFehlerText = "The result is out of tolerance!";
					i_sFehlertyp = Ergebnis.FT_NIO;
					if( i_bCalcDebug_Standard ) {
						CascadeLogging.getLogger().log( LogLevel.INFO, "Numerical result not between min, max!" );
					}
				}
			} else if( bMin && !bMax ) {
				strMinWert = getArg( "MIN" );
				i_sMinWert = strMinWert;

				if( i_bCalcDebug_Standard ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Calc: execute(): min = " + i_sMinWert );
				}
				if( dResOperation != dMin ) {
					i_iStatus = STATUS_EXECUTION_ERROR; // verschieden MIN
					i_sFehlerText = "The result is different from target value (min)!";
					i_sFehlertyp = Ergebnis.FT_NIO;
					if( i_bCalcDebug_Standard ) {
						CascadeLogging.getLogger().log( LogLevel.INFO, "Numerical result not equal to min!" );
					}
				}
			}
			// Status und Fehlertyp, f�r boolsche Operationen
			if( logicalOperation( op ) ) {
				if( !bResOperation ) {
					i_iStatus = STATUS_EXECUTION_ERROR; // logische Bewertung "false"
					i_sFehlerText = "The logical expression is false.";
					i_sFehlertyp = Ergebnis.FT_NIO;
					if( i_bCalcDebug_Standard ) {
						CascadeLogging.getLogger().log( LogLevel.INFO, "Locigal result equal to false!" );
					}
				}
			}
			// wiederholungen
			i_sWiederholungen = "1";
			// hinweistext
			if( !i_sFehlertyp.equals( Ergebnis.FT_IO ) ) {
				if( getArg( "HWT" ) != null ) {
					i_sHinweisText = getArg( "HWT" );
				}
			}

			// in Param3 werden alle Argumente als String zusammengefasst
			i_sParameter_3 = i_sParameter_1 + "; " + i_sParameter_2 + "; " + i_sParameter_3;

			// laut APDM-Cascade SST Kontrakt muss Param1 SGBD und Param2 APIJOB sein
			// deswegen hier auf SGBD bzw. "" setzen
			if( getPr�fling().getAttribut( "SGBD" ) != null ) {
				i_sParameter_1 = getPr�fling().getAttribut( "SGBD" );
			} else {
				i_sParameter_1 = "null";
			}
			i_sParameter_2 = "";

			// Add result
			i_oResult = new Ergebnis( i_sID, // ID
			i_sWerkzeug, // werkzeug
			i_sParameter_1, // parameter_1
			i_sParameter_2, // parameter_2
			i_sParameter_3, // parameter_3
			i_sErgebnis, // ergebnis
			i_sErgebniswert, // ergebniswert
			i_sMinWert, // minWert
			i_sMaxWert, // maxWert
			i_sWiederholungen, // wiederholungen
			i_sFrageText, // frageText
			i_sAntwortText, // antwortText
			i_sAnweisText, // anweisText
			i_sFehlerText, // fehlertext
			i_sHinweisText, // hinweistext
			i_sFehlertyp // fehlertyp
			);
			i_oErgListe.add( i_oResult );
		} catch( PPExecutionException ppee ) {
			i_iStatus = STATUS_EXECUTION_ERROR;
			ppee.printStackTrace();

			i_oResult = new Ergebnis( i_sID, // ID
			i_sWerkzeug, // werkzeug
			i_sParameter_1, // parameter_1
			i_sParameter_2, // parameter_2
			i_sParameter_3, // parameter_3
			i_sErgebnis, // ergebnis
			i_sErgebniswert, // ergebniswert
			i_sMinWert, // minWert
			i_sMaxWert, // maxWert
			i_sWiederholungen, // wiederholungen
			i_sFrageText, // frageText
			i_sAntwortText, // antwortText
			i_sAnweisText, // anweisText
			i_sFehlerText, // fehlertext
			i_sHinweisText, // hinweistext
			i_sFehlertyp // fehlertyp
			);
			i_oErgListe.add( i_oResult );
		} catch( Exception ex ) {
			i_iStatus = STATUS_EXECUTION_ERROR;
			ex.printStackTrace();
			resetResultStrings();
			i_sID = "EXCEPTION";
			i_sFehlerText = "EXCEPTION";
			i_sHinweisText = PB.getString( "unerwarteterLaufzeitfehler" );
			i_sFehlertyp = Ergebnis.FT_NIO_SYS;

			i_oResult = new Ergebnis( i_sID, // ID
			i_sWerkzeug, // werkzeug
			i_sParameter_1, // parameter_1
			i_sParameter_2, // parameter_2
			i_sParameter_3, // parameter_3
			i_sErgebnis, // ergebnis
			i_sErgebniswert, // ergebniswert
			i_sMinWert, // minWert
			i_sMaxWert, // maxWert
			i_sWiederholungen, // wiederholungen
			i_sFrageText, // frageText
			i_sAntwortText, // antwortText
			i_sAnweisText, // anweisText
			i_sFehlerText, // fehlertext
			i_sHinweisText, // hinweistext
			i_sFehlertyp // fehlertyp
			);
			i_oErgListe.add( i_oResult );
		} catch( Throwable t ) {
			i_iStatus = STATUS_EXECUTION_ERROR;
			t.printStackTrace();
			resetResultStrings();
			i_sID = "THROWABLE";
			i_sFehlerText = "THROWABLE";
			i_sHinweisText = PB.getString( "unerwarteterLaufzeitfehler" );
			i_sFehlertyp = Ergebnis.FT_NIO_SYS;

			i_oResult = new Ergebnis( i_sID, // ID
			i_sWerkzeug, // werkzeug
			i_sParameter_1, // parameter_1
			i_sParameter_2, // parameter_2
			i_sParameter_3, // parameter_3
			i_sErgebnis, // ergebnis
			i_sErgebniswert, // ergebniswert
			i_sMinWert, // minWert
			i_sMaxWert, // maxWert
			i_sWiederholungen, // wiederholungen
			i_sFrageText, // frageText
			i_sAntwortText, // antwortText
			i_sAnweisText, // anweisText
			i_sFehlerText, // fehlertext
			i_sHinweisText, // hinweistext
			i_sFehlertyp // fehlertyp
			);
			i_oErgListe.add( i_oResult );
		}

		// f�r richtige einordnung in der Auftragshistory muss die Executed time > 1ms sein
		if( (System.currentTimeMillis() - timestamp) < 1 ) {
			try {
				Thread.sleep( 10 );
			} catch( Exception e ) {

			}
		}

		// Status setzen
		setPPStatus( info, i_iStatus, i_oErgListe );

		if( this.getExecStatus() != STATUS_EXECUTION_OK )
			CascadeLogging.getLogger().log( LogLevel.INFO, "Exec status NIO!" );
		else
			CascadeLogging.getLogger().log( LogLevel.INFO, "Exec status IO!" );
		if( !this.isValid() )
			CascadeLogging.getLogger().log( LogLevel.INFO, "PP invalid!" );
		else
			CascadeLogging.getLogger().log( LogLevel.INFO, "PP valid!" );
	}

	/**
	 * Berechnet die Summe aller double-Werte, welche in einem Vector als Strings abgelegt sind.
	 * Wenn ein Objekt im Vektor sich nicht in einen double-Wert umwandeln l�sst, dann wird der
	 * Eintrag nicht ber�cksichtigt!
	 * 
	 * @param v Vector, welcher die double-Werte enth�lt.
	 * @return Der Summenwert.
	 */
	private double add( Vector v ) {
		double sum = 0.0;
		double summand = 0.0;
		int vSize = v.size();
		int n = 0;

		for( n = 0; n < vSize; n++ ) {
			try {
				summand = Double.parseDouble( (String) v.elementAt( n ) );
				sum += summand;
			} catch( NumberFormatException e ) {
				// Kein Objekt im Vector, das in einen double-Wert umgewandelt werden kann
			}
		}
		return sum;
	}

	/**
	 * Berechnet die Differenz "aller double-Werte", welche in einem Vector als Strings abgelegt
	 * sind. Op1-Op2-...-OpN Wenn ein Objekt im Vektor sich nicht in einen double-Wert umwandeln
	 * l�sst, dann wird der Eintrag nicht ber�cksichtigt!
	 * 
	 * @param v Vector, welcher die double-Werte enth�lt.
	 * @return Die Differenz.
	 */
	private double sub( Vector v ) {
		double minuend = 0.0;
		double subtrahend = 0.0;
		double diff = 0.0;
		int vSize = v.size();
		int n = 0;

		for( n = 0; n < vSize; n++ ) {
			try {
				subtrahend = Double.parseDouble( (String) v.elementAt( n ) );
				if( n == 0 ) {
					diff = subtrahend;
				} else {
					diff -= subtrahend;
				}
			} catch( NumberFormatException e ) {
				// Kein Objekt im Vector, das in einen double-Wert umgewandelt werden kann
			}
		}
		return diff;
	}

	/**
	 * Berechnet das Produkt aller double-Werte, welche in einem Vector als Strings abgelegt sind.
	 * Wenn ein Objekt im Vektor sich nicht in einen double-Wert umwandeln l�sst, dann wird der
	 * Eintrag nicht ber�cksichtigt!
	 * 
	 * @param v Vector, welcher die double-Werte enth�lt.
	 * @return Das Produkt.
	 */
	private double mul( Vector v ) {
		double prod = 1.0;
		double factor = 0.0;
		int vSize = v.size();
		int n = 0;

		for( n = 0; n < vSize; n++ ) {
			try {
				factor = Double.parseDouble( (String) v.elementAt( n ) );
				prod *= factor;
			} catch( NumberFormatException e ) {
				// Kein Objekt im Vector, das in einen double-Wert umgewandelt werden kann
			}
		}
		return prod;
	}

	/**
	 * Berechnet den Quotienten "aller double-Werte", welche in einem Vector als Strings abgelegt
	 * sind. Op1/Op2/.../OpN Wenn ein Objekt im Vektor sich nicht in einen double-Wert umwandeln
	 * l�sst, bzw. die Umwandlung "0" ergibt (au�er im ersten Element), dann wird der Eintrag
	 * nicht ber�cksichtigt!
	 * 
	 * @param v Vector, welcher die double-Werte enth�lt.
	 * @return Der Quotient.
	 */
	private double div( Vector v ) {
		double quot = 0.0;
		double divisor = 1.0;
		int vSize = v.size();
		int n = 0;

		for( n = 0; n < vSize; n++ ) {
			try {
				divisor = Double.parseDouble( (String) v.elementAt( n ) );
				if( n == 0 ) {
					quot = divisor;
				} else {
					if( divisor != 0.0 ) {
						quot /= divisor;
					}
				}
			} catch( NumberFormatException e ) {
				// Kein Objekt im Vector, das in einen double-Wert umgewandelt werden kann
			}
		}
		return quot;
	}

	/**
	 * Berechnet das Ergebnis eines allgemeinen Terms, wobei die in den Parametern angegebenen
	 * Summanden alle aufsummiert werden. Jeder Summand hat die Form F*(B)^E. D.h., dass jeder
	 * Parameter die drei Elemente multiplikativer Faktor (F), Basis (B) und Exponent (E) enthalten
	 * muss. Diese Elemente sind als Strings in drei Vektoren abgelegt (factor, basis, exponent).
	 */
	private double term( Vector factor, Vector basis, Vector exponent ) {
		double dTermResult = 0.0;
		double dF = 0.0, dB = 0.0, dE = 0.0;
		int nF = 0, nB = 0, nE = 0, n = 0;
		nF = factor.size();
		nB = basis.size();
		nE = exponent.size();
		if( nF == nB && nB == nE ) {
			n = nB;
			for( int m = 0; m < n; m++ ) {
				try {
					dF = Double.parseDouble( (String) factor.elementAt( m ) );
					dB = Double.parseDouble( (String) basis.elementAt( m ) );
					dE = Double.parseDouble( (String) exponent.elementAt( m ) );
					dTermResult = dTermResult + dF * Math.pow( dB, dE );
				} catch( NumberFormatException e ) {
					return dTermResult; // Error: String without double representation
				}
			}
		} else {
			return dTermResult; // Error: different length of Vectors
		}
		return dTermResult;
	}

	/**
	 * Berechnet das Ergebnis eines Polynoms, wobei die in den Parametern angegebenen Summanden alle
	 * aufsummiert werden. Jeder Summand hat die Form F*(B)^E. D.h., dass jeder Parameter die drei
	 * Elemente multiplikativer Faktor (F), Basis (B) und Exponent (E) enthalten muss. Die Basis
	 * wird �ber das optionale Argument BASIS �bergeben. Die Elemente der Faktoren und Exponenten
	 * sind als Strings in zwei Vektoren abgelegt (factor, exponent).
	 */
	private double poynomial( Vector factor, double basis, Vector exponent ) {
		double dPolynomialResult = 0.0;
		double dF = 0.0, dB = 0.0, dE = 0.0;
		int nF = 0, nE = 0, n = 0;
		nF = factor.size();
		nE = exponent.size();
		if( nF == nE ) {
			n = nF;
			dB = basis;
			for( int m = 0; m < n; m++ ) {
				try {
					dF = Double.parseDouble( (String) factor.elementAt( m ) );
					dE = Double.parseDouble( (String) exponent.elementAt( m ) );
					dPolynomialResult = dPolynomialResult + dF * Math.pow( dB, dE );
				} catch( NumberFormatException e ) {
					return dPolynomialResult; // Error: String without double representation
				}
			}
		} else {
			return dPolynomialResult; // Error: different length of Vectors
		}
		return dPolynomialResult;
	}

	/**
	 * Ermittelt den logischen Wert, ob f�r den Vektor, welcher mit "double-Werten" bef�llt sein
	 * mus, folgendes gilt: Op1>Op2>...>OpN Wenn eines von zwei aufeinanderfolgenden Objekten im
	 * Vektor sich nicht in einen double-Wert umwandeln l�sst, dann wird keine Bewertung
	 * vorgenommen!
	 * 
	 * @param v Vector, welcher die double-Werte enth�lt.
	 * @return Den logischen Wert als Ergebnis auf die logische Operation.
	 */
	private boolean greater_than( Vector v ) {
		double links = 0.0;
		double rechts = 0.0;
		int vSize = v.size();
		int n = 0;

		for( n = 0; n < vSize - 1; n++ ) {
			try {
				links = Double.parseDouble( (String) v.elementAt( n ) );
				rechts = Double.parseDouble( (String) v.elementAt( n + 1 ) );
				if( links <= rechts ) {
					return false;
				}
			} catch( NumberFormatException e ) {
				// Mindestens ein Objekt in diesem Verglich, welches nicht in einen double-Wert
				// umgewandelt werden kann
			}
		}
		return true;
	}

	/**
	 * Ermittelt den logischen Wert, ob f�r den Vektor, welcher mit "double-Werten" bef�llt sein
	 * mus, folgendes gilt: Op1>=Op2>=...>=OpN Wenn eines von zwei aufeinanderfolgenden Objekten im
	 * Vektor sich nicht in einen double-Wert umwandeln l�sst, dann wird keine Bewertung
	 * vorgenommen!
	 * 
	 * @param v Vector, welcher die double-Werte enth�lt.
	 * @return Den logischen Wert als Ergebnis auf die logische Operation.
	 */
	private boolean greater_than_or_equal( Vector v ) {
		double links = 0.0;
		double rechts = 0.0;
		int vSize = v.size();
		int n = 0;

		for( n = 0; n < vSize - 1; n++ ) {
			try {
				links = Double.parseDouble( (String) v.elementAt( n ) );
				rechts = Double.parseDouble( (String) v.elementAt( n + 1 ) );
				if( links < rechts ) {
					return false;
				}
			} catch( NumberFormatException e ) {
				// Mindestens ein Objekt in diesem Verglich, welches nicht in einen double-Wert
				// umgewandelt werden kann
			}
		}
		return true;
	}

	/**
	 * Ermittelt den logischen Wert, ob f�r den Vektor, welcher mit "double-Werten" bef�llt sein
	 * mus, folgendes gilt: Op1<Op2<...<OpN Wenn eines von zwei aufeinanderfolgenden Objekten im
	 * Vektor sich nicht in einen double-Wert umwandeln l�sst, dann wird keine Bewertung
	 * vorgenommen!
	 * 
	 * @param v Vector, welcher die double-Werte enth�lt.
	 * @return Den logischen Wert als Ergebnis auf die logische Operation.
	 */
	private boolean less_than( Vector v ) {
		double links = 0.0;
		double rechts = 0.0;
		int vSize = v.size();
		int n = 0;

		for( n = 0; n < vSize - 1; n++ ) {
			try {
				links = Double.parseDouble( (String) v.elementAt( n ) );
				rechts = Double.parseDouble( (String) v.elementAt( n + 1 ) );
				if( links >= rechts ) {
					return false;
				}
			} catch( NumberFormatException e ) {
				// Mindestens ein Objekt in diesem Verglich, welches nicht in einen double-Wert
				// umgewandelt werden kann
			}
		}
		return true;
	}

	/**
	 * Ermittelt den logischen Wert, ob f�r den Vektor, welcher mit "double-Werten" bef�llt sein
	 * mus, folgendes gilt: Op1<=Op2<=...<=OpN Wenn eines von zwei aufeinanderfolgenden Objekten im
	 * Vektor sich nicht in einen double-Wert umwandeln l�sst, dann wird keine Bewertung
	 * vorgenommen!
	 * 
	 * @param v Vector, welcher die double-Werte enth�lt.
	 * @return Den logischen Wert als Ergebnis auf die logische Operation.
	 */
	private boolean less_than_or_equal( Vector v ) {
		double links = 0.0;
		double rechts = 0.0;
		int vSize = v.size();
		int n = 0;

		for( n = 0; n < vSize - 1; n++ ) {
			try {
				links = Double.parseDouble( (String) v.elementAt( n ) );
				rechts = Double.parseDouble( (String) v.elementAt( n + 1 ) );
				if( links > rechts ) {
					return false;
				}
			} catch( NumberFormatException e ) {
				// Mindestens ein Objekt in diesem Vergleich, welches nicht in einen double-Wert
				// umgewandelt werden kann
			}
		}
		return true;
	}

	/**
	 * Schaut nach, ob die Elemente in einem Vector alle String-Objekte sind, und diese sich in
	 * doble-Werte umwandeln lassen.
	 * 
	 * @param v Vector, welcher untersucht wird
	 * @i_oResult true, wenn in dem Vector nur Strings enthalten sind und diese in double-Werte
	 *            gewandelt werden k�nnen.
	 */
	/*
	 * private boolean vectorWithNumbers( Vector v ) { boolean withNumbers = true; Object
	 * oVectorElement; String strVectorElement=""; Iterator opIterator = v.iterator(); while(
	 * opIterator.hasNext() ) { oVectorElement = opIterator.next(); if( oVectorElement instanceof
	 * String ) { try { strVectorElement = (String) oVectorElement; Double.parseDouble(
	 * strVectorElement ); } catch (NumberFormatException e) { withNumbers = false; } } else {
	 * withNumbers = false; } } return withNumbers; }
	 */

	/**
	 * Schaut nach, ob die Elemente in einem Vector bereit sind f�r die Division, wie sie in dieser
	 * Pr�fprozedur verstanden wird.
	 * 
	 * @param v Vector, welcher untersucht wird
	 * @i_oResult true, wenn bereit f�r Division.
	 */
	private boolean readyForDiv( Vector v ) {
		boolean ready = true;
		Object oVectorElement;
		String strVectorElement = "";
		Iterator opIterator = v.iterator();
		int iteratorCount = 0;
		while( opIterator.hasNext() ) {
			oVectorElement = opIterator.next();
			if( oVectorElement instanceof String ) {
				try {
					strVectorElement = (String) oVectorElement;
					double d = Double.parseDouble( strVectorElement );
					if( d == 0 && iteratorCount > 0 ) {
						// Null nur bei PARA1 akzeptieren 
						ready = false;
					}
				} catch( NumberFormatException e ) {
					ready = false;
				}
			} else {
				ready = false;
			}
			iteratorCount++;
		}
		return ready;
	}

	/**
	 * Determines the number of arguments beginnig with PARA...
	 */
	private int paraNumber() {
		Enumeration oAllArgsInEnumeration = getArgs().keys();
		String sArgKey;
		String sArgVal;
		int iParaNum = 0;

		while( oAllArgsInEnumeration.hasMoreElements() ) {
			sArgKey = (String) oAllArgsInEnumeration.nextElement();
			sArgVal = getArg( sArgKey );

			if( sArgKey.startsWith( "PARA" ) ) { // PARA...
				iParaNum++;
			}
		}
		return iParaNum;
	}

	/**
	 * Prints the parameters on the debug.log
	 */
	private void debugPrintVector( Vector v ) {
		String p = null;
		int vSize = v.size();
		int n = 0;
		if( vSize >= 1 ) {
			for( n = 0; n < vSize; n++ ) {
				p = (String) v.elementAt( n );
				if( p != null ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "PARA" + (n + 1) + " = " + p );
				} else {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Void element in the Vector on index " + n );
				}
			}
		}
	}

	/**
	 * Checks if the String is a valid operation.
	 */
	private boolean validOperation( String operation ) {
		if( operation.equalsIgnoreCase( "ADD" ) || operation.equalsIgnoreCase( "SUB" ) || operation.equalsIgnoreCase( "MUL" ) || operation.equalsIgnoreCase( "DIV" ) || operation.equalsIgnoreCase( "AVERAGE" ) || operation.equalsIgnoreCase( "TERM" ) || operation.equalsIgnoreCase( "POLYNOMIAL" ) || operation.equalsIgnoreCase( "GREATER" ) || operation.equalsIgnoreCase( "POLYNOMIAL" ) || operation.equalsIgnoreCase( "GREATER" ) || operation.equalsIgnoreCase( "GREATER_OR_EQUAL" ) || operation.equalsIgnoreCase( "LESS" ) || operation.equalsIgnoreCase( "LESS_OR_EQUAL" ) ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Gets the value for an argument as one String.In this string, references to other results in
	 * the virtual vehicle are resolved with the respective values. To see (e.g. in the error
	 * report) a reference, the used representation is reference=value. The resulting line is a
	 * ";"-separated list of parameter-details of this argument. (e.g.:
	 * [value1];[value2];[reference=value3], ... ). If a value indicated by a reference isn't
	 * available, the String-value contains the relevant error message.
	 * 
	 * @param argName the argument name
	 * @return a String containing the argument list
	 */
	private String getArgumentDokuString( String argName ) {
		String sRet = "";

		if( getArg( argName ) == null ) {
			sRet = "No argument " + argName + " available!";
		} else {
			String[] sArgsAsReferences = null;
			String[] sArgsAsValues = null;
			sArgsAsReferences = splitArg( getArg( argName ) );
			sArgsAsValues = new String[sArgsAsReferences.length];

			for( int i = 0; i < sArgsAsReferences.length; i++ ) {
				// with reference
				if( sArgsAsReferences[i].indexOf( '@' ) != -1 ) {
					try {
						sArgsAsValues[i] = sArgsAsReferences[i] + "=" + getResultRedesigned( sArgsAsReferences[i].toUpperCase() );
					} catch( PPExecutionException e ) {
						sArgsAsValues[i] = "Result " + sArgsAsReferences[i] + " not available: " + e.getMessage();
					}
				}
				// without reference
				else {
					sArgsAsValues[i] = sArgsAsReferences[i];
				}

				if( sRet.equals( "" ) ) {
					sRet = sArgsAsValues[i];
				} else {
					sRet = sRet + ";" + sArgsAsValues[i];
				}
			}
		}
		return sRet;
	}

	/**
	 * Gets the number of invalid references in an argument line. If "0" is returned, all referenced
	 * results are available in the virtual vehicle. "sArgLine" is the ";"-separated String
	 * containing all parameter details of an argument.
	 * 
	 * @param sArgLine the argument line as one String
	 * @return the number of invalid references
	 */
	private int checkReferencesInArgumentLine( String sArgLine ) {
		int iInvalidReferences = 0;
		String[] sArgsAsReferences = null;
		sArgsAsReferences = splitArg( sArgLine );
		for( int i = 0; i < sArgsAsReferences.length; i++ ) {
			// if argument contains a reference
			if( sArgsAsReferences[i].indexOf( '@' ) != -1 ) {
				try {
					getResultRedesigned( sArgsAsReferences[i].toUpperCase() );
				} catch( PPExecutionException e ) {
					iInvalidReferences++;
				}
			}
		}
		return iInvalidReferences;
	}

	/**
	 * Gets a String vector containig the missing references in an argument line. If all referenced
	 * results from the virtual vehicle are available a null-vector is returned. "sArgLine" is the
	 * ";"-separated String containing all parameter details of an argument.
	 * 
	 * @param sArgLine the argument line as one String
	 * @return the missing references, if any
	 */
	private String[] getMissingReferencesInArgumentLine( String sArgLine ) {
		String[] sArgsAsReferences = null;
		sArgsAsReferences = splitArg( sArgLine );
		String[] sMissingReferences = null;
		int iInvalidReferences = checkReferencesInArgumentLine( sArgLine );

		if( iInvalidReferences > 0 ) {
			sMissingReferences = new String[iInvalidReferences];
			int j = 0;
			for( int i = 0; i < sArgsAsReferences.length; i++ ) {
				// if argument contains a reference
				if( sArgsAsReferences[i].indexOf( '@' ) != -1 ) {
					try {
						getResultRedesigned( sArgsAsReferences[i].toUpperCase() );
					} catch( PPExecutionException e ) {
						sMissingReferences[j] = sArgsAsReferences[i];
						j++;
					}
				}
			}
		}

		return sMissingReferences;
	}

	/**
	 * Gets the number of invalid references in all arguments. If "0" is returned, all referenced
	 * results are available in the virtual vehicle.
	 * 
	 * @return the number of invalid references
	 */
	private int checkReferencesInAllArguments() {
		int iTotalInvalidRef = 0, iCurrentInvalidRef = 0;
		String currentKey, currentArg;
		Enumeration enu = getArgs().keys();
		while( enu.hasMoreElements() ) {
			currentKey = (String) enu.nextElement();
			currentArg = getArg( currentKey );
			iCurrentInvalidRef = checkReferencesInArgumentLine( currentArg );
			iTotalInvalidRef += iCurrentInvalidRef;
		}
		return iTotalInvalidRef;
	}

	/**
	 * Gets a String vector containig the missing references in all arguments. If all referenced
	 * results from the virtual vehicle are available a null-vector is returned.
	 * 
	 * @param notAvailableArgs the String array where not available arguments (the references) are
	 *            filled in
	 * @return the missing references, if any
	 */
	private String[] getMissingReferencesInAllArguments() {
		int iTotalInvalidRef = 0, iCurrentInvalidRef = 0;
		String currentKey, currentArg;
		String[] currentMissingArgs = null;
		String[] notAvailableArgs = null;
		iTotalInvalidRef = checkReferencesInAllArguments();

		if( iTotalInvalidRef > 0 ) {
			int insertIndex = 0;
			notAvailableArgs = new String[iTotalInvalidRef];
			Enumeration enu = getArgs().keys();
			while( enu.hasMoreElements() ) {
				currentKey = (String) enu.nextElement();
				currentArg = getArg( currentKey );
				iCurrentInvalidRef = checkReferencesInArgumentLine( currentArg );
				currentMissingArgs = getMissingReferencesInArgumentLine( currentArg );
				if( currentMissingArgs != null ) {
					for( int i = 0; i < iCurrentInvalidRef; i++ ) {
						notAvailableArgs[insertIndex + i] = currentMissingArgs[i];
					}
					insertIndex += iCurrentInvalidRef;
				}
			}
		}

		return notAvailableArgs;
	}

	/**
	 * Checks if the String stands for a logical operation.
	 */
	private boolean logicalOperation( String operation ) {
		if( operation.equals( "GREATER" ) || operation.equals( "GREATER_OR_EQUAL" ) || operation.equals( "LESS" ) || operation.equals( "LESS_OR_EQUAL" ) ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Resets the i_oResult strings
	 */
	private void resetResultStrings() {
		i_sID = ""; // ID
		i_sWerkzeug = ""; // werkzeug
		i_sParameter_1 = ""; // parameter_1
		i_sParameter_2 = ""; // parameter_2
		i_sParameter_3 = ""; // parameter_3
		i_sErgebnis = ""; // ergebnis
		i_sErgebniswert = ""; // ergebniswert
		i_sMinWert = ""; // minWert
		i_sMaxWert = ""; // maxWert
		i_sWiederholungen = ""; // wiederholungen
		i_sFrageText = ""; // frageText
		i_sAntwortText = ""; // antwortText
		i_sAnweisText = ""; // anweisText
		i_sFehlerText = ""; // fehlertext
		i_sHinweisText = ""; // hinweistext
		i_sFehlertyp = ""; // fehlertyp
	}

	/**
	 * Resets all instance variables
	 */
	private void initInstanceVariables() {
		i_oResult = null;
		i_iStatus = STATUS_EXECUTION_OK;
		resetResultStrings();
		i_bCalcDebug_Standard = false;
		i_bCalcDebug_CheckArgs = false;
		i_bCalcDebug_DebugMode = false;
	}
}

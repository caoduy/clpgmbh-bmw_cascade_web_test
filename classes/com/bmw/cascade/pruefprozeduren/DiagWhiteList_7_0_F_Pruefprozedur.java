package com.bmw.cascade.pruefprozeduren;

import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;

/**
 * Implementierung der Pr�fprozedur, die die Fehler in der Whitelist �berpr�ft. 
 * Wird SGBD und Whitelist parametriert und Detail=False, so wird ein FS_LESEN durchgef�hrt und das
 * Ergebnis wird mit der WhiteList abgeglichen. Tritt ein Fehler auf, der auch in der Whitelist steht, so wird
 * die PP mit niO bewertet und der Fehler mit niO dokumentiert, alle anderen auftretenden Fehler werden als ausgeblendet
 * dokumentiert.
 * 
 * Wird Detail auf true gesetzt so werden Fehler die sich in der Whitelist befinden mit Hilfe des
 * Jobs FS_LESEN_DETAIL ausgewertet. Dies f�hrt zu einer Erfassung des Fehlers auch f�r den Fall, dass er anliegt, 
 * aber aufgrund Priorisierung und Fehlerspeichergr��e nicht im Fehlerspeicher zu finden w�re. Ist der parametrierte 
 * Fehlecode via FS_LESEN_DETAIL zu finden und die F_Vorhanden_Nr!=0, so wird dieser Fehler mit niO dokumentiert und 
 * der Status der PP auf fehlerhaft gesetzt.
 * 
 * Sind die Testbedingungen des Fehlers nicht erf�llt (!=16) so wird dies in APDM mit �a� ausgeblendet dokumentiert, 
 * f�hrt aber nicht zu einer Fehlerausgabe mit �f/NOK�
 * 
 * Ist das Flag DTC_TRIGGER_EVALUATION auf true gesetzt, so wird der Fehler zus�tzlich als niO dokumentiert, wenn
 * die Testbedingungen nicht erf�llt sind (!=16), d.h. der Fehlererkennung im SG wurde f�r diesen DTC nicht 
 * durchlaufen/getriggert. 
 * Ist das Flag DTC_TRIGGER_EVALUATION auf true gesetzt, so wird der Fehler zus�tzlich als 
 * niO dokumentiert, wenn die Testbedingungen erf�llt sind (=16) und die F_Vorhanden_Nr!=0 ist.
 * Ist die Testbedingung des Fehlers erf�llt und die F_Vorhanden_Nr=0, so wird der Fehler zus�tzlich mit iO dok.
 * Das Attribut DTC_TRIGGER_EVALUATION ist nur unter der Pr�misse detail=true anwendbar, da hierf�r ein 
 * FS_LESEN_DETAIL verwendet werden muss.
 * 
 * Wird der DTC_KIND=Info gesetzt, so wird anstatt FS_LESEN/FS_LESEN_DETAIL der Job IS_LESEN/IS_LESEN_DETAIL hergenommen.
 * 
 * @author Wolf, Buboltz
 * @version 1_0_F 06.06.12 CW Ersterstellung <br>
 * @version 2_0_F 24.08.12 CW �berarbeitung und BugFixing gem�� Beschreibung oben <br>
 * @version 3_0_T 01.07.14 TB Anforderung seitens Analyse via R. Wurm, den EDIABAS Job mit zu dokumentieren <br>
 * @version 4_0_T 22.08.16 TB Erweiterungen f�r Paralleldiagnose analog zur DiagToleranzPP (LOP 2015), Generics eingebaut + Compiler Warnungen behoben <br>
 * @version 5_0_F 25.01.17 MKe <br>
 * @version 6_0_T 18.09.17 TB LOP 2252: Optionales Argument IGNORE_ERRORS eingebaut, wenn TRUE werden Diagnose Fehler WRONG_UBATT, IFH009 etc. ignoriert <br>
 * @version 7_0_F 28.09.17 TB F-Version <br>
 */
public class DiagWhiteList_7_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagWhiteList_7_0_F_Pruefprozedur() {
	}

	/**
	   * Erzeugt eine neue Pruefprozedur.
	   * @param pruefling Klasse des zugeh. Pr�flings
	   * @param pruefprozName Name der Pr�fprozedur
	   * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	   */
	public DiagWhiteList_7_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente
	 * @return eine Liste sie optionale Argurmenten
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "DTC_KIND", "DETAIL", "DTC_TRIGGER_EVALUATION", "TAG", "IGNORE_ERRORS" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente
	 * @return eine Liste sie zwingende Argurmenten
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "SGBD", "WHITELIST" };
		return args;
	}

	/**
	 * Pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 * @return true wenn Argumente iO sind sonst false
	 */
	@Override
	public boolean checkArgs() {
		return super.checkArgs();
	}

	/**
	 * F�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String sgbd = null;
		String dtcKind = null;
		String whitelist = null;
		boolean detail = false;
		String temp = null;
		boolean dtc_trigger_evaluation = false;
		EdiabasProxyThread ediabas = null;
		boolean ignore_errors = false; //

		//Paralleldiagnose
		boolean parallel = false;
		String tag = null;
		DeviceManager devMan = null;

		/***********************************************
		 * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
		 * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		 ***********************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		try {
			// Parameter holen / initialisieren
			try {
				// Parameter existenz
				if( checkArgs() == false )
					throw new PPExecutionException( "Parameterexistenz" );
				// SGBD
				sgbd = extractValues( getArg( getRequiredArgs()[0] ) )[0];

				// Whitelist
				if( getArg( getRequiredArgs()[1] ) != null ) {
					whitelist = getArg( getRequiredArgs()[1] );
				}
				if( whitelist != null ) {
					whitelist = extractErrorNumbers( whitelist );
				} else {
					whitelist = "";
				}
				// DTC-Kind
				if( getArg( getOptionalArgs()[0] ) != null ) {
					dtcKind = getArg( getOptionalArgs()[0] );
				} else {
					dtcKind = "";
				}
				// Detailauswertung
				if( getArg( getOptionalArgs()[1] ) != null ) {
					detail = new Boolean( getArg( getOptionalArgs()[1] ) ).booleanValue();
				}
				// DTC_Trigger_Evaluation
				if( getArg( getOptionalArgs()[2] ) != null ) {
					dtc_trigger_evaluation = new Boolean( getArg( getOptionalArgs()[2] ) ).booleanValue();
				}
				// IGNORE_ERRORS
				if( getArg( "IGNORE_ERRORS" ) != null ) {
					ignore_errors = new Boolean( getArg( "IGNORE_ERRORS" ) ).booleanValue();
				}
			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Ausf�hrung
			try {
				// Paralleldiagnose, Pr�fstandvariabel auslesen, ob Paralleldiagnose 
				try {
					Object pr_var;

					pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
					if( pr_var != null ) {
						if( pr_var instanceof String ) {
							if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
								parallel = true;
							}
						} else if( pr_var instanceof Boolean ) {
							if( ((Boolean) pr_var).booleanValue() ) {
								parallel = true;
							}
						}
					}
				} catch( VariablesException e ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				}

				// Devicemanager
				devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();

				if( getArg( "TAG" ) != null )
					tag = getArg( "TAG" );

				if( parallel ) // Ediabas holen
				{
					try {
						ediabas = devMan.getEdiabasParallel( tag, sgbd );
					} catch( Exception ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Wrong Cascade configuration for parallel diagnosys", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();
					} catch( Throwable ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();
					}
				} else {
					try {
						ediabas = devMan.getEdiabas( tag );
					} catch( Exception ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();

					}
				}
				// <<<<< Paralleldiagnose            

				// Jobauf�hrung
				if( !detail ) {
					String job = "";
					if( dtcKind.equalsIgnoreCase( "Info" ) ) {
						job = "IS_LESEN";
					} else {
						job = "FS_LESEN";
					}

					// EDIABAS Job ausf�hren
					temp = ediabas.executeDiagJob( sgbd, job, "", "" );
					if( temp.equals( "OKAY" ) == false ) {
						if( ignore_errors ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, dtcKind, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_IGNORE );
							ergListe.add( result );
						} else {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, dtcKind, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						}
					} else {
						result = new Ergebnis( "Status", "EDIABAS", sgbd, job, dtcKind, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
					// Alle Fehlerspeicherinfos (tempor�r) speichern
					for( int i = 1; i < (ediabas.getDiagJobSaetze() - 1); i++ ) {
						String fOrtNr = ediabas.getDiagResultValue( i, "F_ORT_NR" );
						String fOrtText = ediabas.getDiagResultValue( i, "F_ORT_TEXT" );
						String fHexCode = null;
						try {
							fHexCode = ediabas.getDiagResultValue( i, "F_HEX_CODE" );

						} catch( Exception ernfe ) {
							// Nicht alle Steuerger�te unterst�tzen diese Zusatzinformation, also Fehler ignorieren
							fHexCode = null;
						}

						//�berpr�fen: ist der Fehler in der WhiteList
						if( whitelist.indexOf( fOrtNr ) >= 0 ) {
							//wenn der aufgetretene Fehler in der WhiteList ist, so muss der Pr�fschritt und das Ergebnis mit n.i.O. bewertet werden
							status = STATUS_EXECUTION_ERROR;
							if( fHexCode != null ) {
								result = new Ergebnis( "FS", "EDIABAS", sgbd, job, dtcKind, "F_ORT_NR", fOrtNr + " (HEX " + fHexCode + ")", "", "", "0", "", "", "", fOrtText, "", Ergebnis.FT_NIO );
							} else {
								result = new Ergebnis( "FS", "EDIABAS", sgbd, job, dtcKind, "F_ORT_NR", fOrtNr, "", "", "0", "", "", "", fOrtText, "", Ergebnis.FT_NIO );
							}
						} else {
							//Fehler mit "A" in APDM dokumentieren
							if( fHexCode != null ) {
								result = new Ergebnis( "FS", "EDIABAS", sgbd, job, dtcKind, "F_ORT_NR", fOrtNr + " (HEX " + fHexCode + ")", "", "", "0", "", "", "", fOrtText, "", Ergebnis.FT_IGNORE );
							} else {
								result = new Ergebnis( "FS", "EDIABAS", sgbd, job, dtcKind, "F_ORT_NR", fOrtNr, "", "", "0", "", "", "", fOrtText, "", Ergebnis.FT_IGNORE );
							}
						}
						ergListe.add( result );
					}

				}
				//wenn detail true ist, wird ein FS_LESEN_DETAIL auf jeden Fehlerspeicher in der Whitelist gemacht
				//es werden in den UW-Bedingungen F_Ready_Text und F_Ready_Nr und F_Vorhanden_Nr ausgewertet
				if( detail ) {
					String[] whitelistArray = whitelist.split( ";" );
					String currentDTC = "";
					String fReadyText = "";
					String fReadyNr = "";
					String fVorhandenNr = "";
					String fHexCode = "";
					String fOrtText = "";
					for( int i = 0; i < whitelistArray.length; i++ ) {
						currentDTC = whitelistArray[i];

						String job = "";
						if( dtcKind.equalsIgnoreCase( "Info" ) ) {
							job = "IS_LESEN_DETAIL";
						} else {
							job = "FS_LESEN_DETAIL";
						}

						// EDIABAS Job ausf�hren
						temp = ediabas.executeDiagJob( sgbd, job, currentDTC, "" );
						if( temp.equals( "OKAY" ) == false ) {
							if( ignore_errors ) {
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, dtcKind, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_IGNORE );
								ergListe.add( result );
							} else {
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, dtcKind, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								throw new PPExecutionException();
							}
						} else {
							result = new Ergebnis( "Status", "EDIABAS", sgbd, job, dtcKind, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );

						}
						try {
							//Fehler dokumentieren als n.i.o. Fehler
							fHexCode = ediabas.getDiagResultValue( "F_HEX_CODE" );
							fOrtText = ediabas.getDiagResultValue( "F_ORT_TEXT" );
							try {
								fVorhandenNr = ediabas.getDiagResultValue( "F_VORHANDEN_NR" );
							} catch( Exception ernfe ) {
								// Nicht alle Steuerger�te unterst�tzen diese Zusatzinformation, also Fehler ignorieren
								throw new PPExecutionException();
							}
							if( !fVorhandenNr.equalsIgnoreCase( "0" ) ) {
								//Fehler ist vorhanden und FVorhandenNr!=0 (d.h. irgendeine Fehlerart ist verf�gbar
								//deswegen Status_Execution_Error setzen
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( "FS", "EDIABAS", sgbd, job, dtcKind, "F_ORT_NR", currentDTC + " (HEX " + fHexCode + "), " + "F_VORHANDEN_NR: " + fVorhandenNr, "", "", "0", "", "", "", fOrtText, "", Ergebnis.FT_NIO );
								ergListe.add( result );
							}
						} catch( PPExecutionException ppe ) {
							if( !ignore_errors ) {
								throw new PPExecutionException( "The ECU does not support the information F_VORHANDEN_NR" );
							}
						} catch( Exception e1 ) {
							if( !ignore_errors ) {
								throw new PPExecutionException();
							}
						}

						try {
							fReadyText = ediabas.getDiagResultValue( "F_READY_TEXT" );
						} catch( Exception ernfe ) {
							// Nicht alle Steuerger�te unterst�tzen diese Zusatzinformation, also Fehler ignorieren
							fReadyText = "";
						}

						try {
							fReadyNr = ediabas.getDiagResultValue( "F_READY_NR" );
						} catch( Exception ernfe ) {
							// Nicht alle Steuerger�te unterst�tzen diese Zusatzinformation, also Fehler ignorieren
							fReadyNr = "";
						}

						//wenn die Testbedingungen nicht erf�llt sind, dann soll der Fehler generell als n.i.o. dok. werden
						if( !dtc_trigger_evaluation && !fReadyNr.equalsIgnoreCase( "16" ) ) {
							result = new Ergebnis( "Status", "EDIABAS", sgbd, "FS_LESEN_DETAIL", dtcKind, currentDTC, "F_READY_NR: " + fReadyNr + " " + fReadyText, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE );
							ergListe.add( result );
						}

						//wenn das Flag dtc_trigger_evaluation gesetzt ist
						if( dtc_trigger_evaluation ) {
							//wenn die Testbedingungen erf�llt sind
							if( fReadyNr.equalsIgnoreCase( "16" ) ) {
								//wenn die F_Vorhanden_Nr ungleich 0 ist, d.h. irgendeine Fehlerart ist vorhanden
								if( !fVorhandenNr.equalsIgnoreCase( "0" ) ) {
									//Fehler als n.i.O. dok.
									result = new Ergebnis( "Status", "EDIABAS", sgbd, "FS_LESEN_DETAIL", dtcKind, currentDTC, "F_READY_NR: " + fReadyNr + " " + fReadyText + ", F_VORHANDEN_NR: " + fVorhandenNr, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
									ergListe.add( result );
								} else {//es ist keine Fehlerart vorhanden
									result = new Ergebnis( "Status", "EDIABAS", sgbd, "FS_LESEN_DETAIL", dtcKind, currentDTC, "F_READY_NR: " + fReadyNr + " " + fReadyText + ", F_VORHANDEN_NR: " + fVorhandenNr, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
									ergListe.add( result );
								}
							}
							//wenn die Testbedingungen nicht erf�llt sind
							if( !fReadyNr.equalsIgnoreCase( "16" ) ) {
								//Fehler als n.i.O. dok.
								result = new Ergebnis( "Status", "EDIABAS", sgbd, "FS_LESEN_DETAIL", dtcKind, currentDTC, "F_READY_NR: " + fReadyNr + " " + fReadyText + ", F_VORHANDEN_NR: " + fVorhandenNr, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
								ergListe.add( result );
							}
						}
					}
				}
			} catch( ApiCallFailedException e ) {
				if( ignore_errors ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, dtcKind, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_IGNORE );
					ergListe.add( result );
				} else {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, dtcKind, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				}
			} catch( EdiabasResultNotFoundException e ) {
				if( ignore_errors ) {
					if( e.getMessage() != null )
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, dtcKind, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_IGNORE );
					else
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, dtcKind, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_IGNORE );
					ergListe.add( result );
				} else {
					if( e.getMessage() != null )
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, dtcKind, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, dtcKind, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}
			}
		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception (" + e.getMessage() + ")", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable (" + e.getMessage() + ")", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Paralleldiagnose
		if( parallel ) {
			try {
				devMan.releaseEdiabasParallel( ediabas, tag, sgbd );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable t ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		setPPStatus( info, status, ergListe );

	}

	/**
	 * Hilfsmethode f�r Argument IGNORE: L�st den �bergebenen String auf
	 * @param in der Argument IGNORE
	 * @return vollst�ndige String (zB "1-3" wird als "1;2;3" zur�ckgeliefert)
	 */
	private String extractErrorNumbers( String in ) {
		String sub, subSub;
		int[] posArray;
		int counter = 1;
		int min, max;
		int i, j;

		for( i = 0; i < in.length(); i++ ) {
			if( in.charAt( i ) == ';' )
				counter++;
		}

		posArray = new int[counter + 1];
		j = 0;
		posArray[j++] = -1;
		for( i = 0; i < in.length(); i++ ) {
			if( in.charAt( i ) == ';' )
				posArray[j++] = i;
		}
		posArray[j++] = in.length();

		StringBuffer temp = new StringBuffer();
		//temp.append( ";" );
		for( i = 0; i < counter; i++ ) {
			sub = (in.substring( posArray[i] + 1, posArray[i + 1] )).trim();
			if( sub.compareTo( "" ) != 0 ) {
				j = sub.indexOf( "-" );
				if( (j < 1) || (j == (sub.length() - 1)) ) {
					temp.append( sub );
					temp.append( ";" );
				} else {
					subSub = sub.substring( 0, j );
					min = Integer.parseInt( subSub );
					subSub = sub.substring( j + 1, sub.length() );
					max = Integer.parseInt( subSub );
					for( j = min; j <= max; j++ ) {
						temp.append( j );
						temp.append( ";" );
					}
				}
			}
		}
		return temp.toString();
	}
}

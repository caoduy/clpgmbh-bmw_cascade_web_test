/**
 * UiInit
 *
 * Created on 14.09.00
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.uianalyse.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.*;

/**
 * Pr�fprozedur, die die Initialisierung des UIAnalysers vornimmt (Abklemmen, Anklemmen, Offsetabgleich, Umschliesen der Batt.-Leitung).
 * @author Martijn Peter Atell
 * @author Z. Wen, ESG Elektroniksystem- und Logistik-GmbH
 * @author <br>
 *    Atell (BMW AG / TI-430)<br>
 *    Wen (ESG Elektroniksystem- und Logistik-GmbH / TI-431)<br>
 * @version <br>
 *    V0_4_9_TA   We  Optionale Parameter PROBE_MESSAGE wurde hinzugef�gt<br>
 *    V0_5_0_FA   Me  FA-Version<br>
 *    V0_5_1_TA   We  AWT wurde ge�ndert<br>
 *    V0_5_2_FA  11.10.2004  We  CurrentProbeException wurde im Pr�fprozedur aufgefangen <br>
 *    V0_5_3_FA  11.10.2004  We  DetermineIOOffset wird beim Start benutzt (vorhandene Daten mussen gel�scht werden) <br>
 *    V0_5_4_FA  15.10.2004  We  CurrentProbeException wurde in jeder Messung behandelt wenn sie passiert <br>
 *    V0_5_5_TA  19.10.2004  We  TA-Version <br>
 *    V0_5_6_FA  22.10.2004  We  Mess- und Triggerfilter wurde am Anfang ausgeschaltet <br>
 *    V0_5_7_TA  25.10.2004  We  Optionale Parameter LOGGER_ENABLE wurde hinzugef�gt <br>
 *    V0_5_8_FA  26.10.2004  We  FA-Version <br>
 */
public class UiInit_68_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    	static final long serialVersionUID = 1L;
 

    	private boolean DEBUG = false;
  	
    	
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public UiInit_68_0_F_Pruefprozedur() {}

  /**
     * erzeugt eine Pruefprozedur mit obigem Verhalten.
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public UiInit_68_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    /**
     * initialsiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /**
     * liefert die optionalen Argumente
     * @return Stringvektor der optionalen Argumente
     */
    public String[] getOptionalArgs() {
        String[] args = {"POLARITY", "PROBE_MESSAGE", "LOGGER_ENABLE"};
        return args;
    }
    
    /**
     * liefert die zwingend erforderlichen Argumente
     * @return Stringvektor der ben�tigten Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = {"MIN_I","MAX_I", "MIN_U","MAX_U" };
        return args;
    }
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
     * @return true, wenn die Ueberpruefung der zwingenden Argumente ok ist
     */
    public boolean checkArgs() {
        int i, j;
        boolean ok;
        String temp;
        
        try{
            ok = super.checkArgs();
            if( ok == true ) {
                //Parameterpr�fung, ob alle zwingenden Parameter belegt sind.
                for( i=0; i<getRequiredArgs().length; i++ ) {
                    try {
                        //G�ltigkeitspr�fung der Belegung aller zwingenden Parameter. Alle Parameterwerte sollen von Typ Integer sein.
                        j = Integer.parseInt( getArg(getRequiredArgs()[i]) );
                    } catch (NumberFormatException e) {
                        return false;
                    }
                }
                //G�ltigkeitspr�fung der Belegung der optionalen Parameter. Der Parameterwert soll von Typ Integer sein.
                //POLARITY: Ob es relevant ist, in welcher Richtung die Zange um die Leitung umschlossen wird
                //POLARITY = 0 (DEFAULT): irrelevant
                //POLARITY = 1: relevant
                temp = getArg(getOptionalArgs()[0]);
                if(temp != null){
                    if( (temp.equals("0") == false) && (temp.equals("1") == false)) return false;
                }
/*                //!G�ltigkeitspr�fung der Belegung der optionalen Parameter. Der Parameterwert soll von Typ Integer sein.
                //!PROBE_MESSAGE: ob es notwendig ist, um die Meldung "Zange umschliessen" zu quittieren
                //!PROBE_MESSAGE = 0 (DEFAULT): nicht notwendig
                //!PROBE_MESSAGE = 1: notwendig
                temp = getArg(getOptionalArgs()[1]);
                if(temp != null){
                    if( (temp.equals("0") == false) && (temp.equals("1") == false)) return false;
                }
*/                //G�ltigkeitspr�fung der Belegung der optionalen Parameter. Der Parameterwert soll von Typ Integer sein.
                //LOGGER_ENABLE: Aktivieren bzw. deaktivieren des Daten-Loggers f�r die Stromaufzeichnung 
                //LOGGER_ENABLE = 1 (DEFAULT): aktivieren
                //LOGGER_ENABLE = 0: deaktivieren
                temp = getArg(getOptionalArgs()[2]);
                if(temp != null){
                    if( (temp.equals("0") == false) && (temp.equals("1") == false)) return false;
                }
            }
            return ok;
        } catch (Exception e) {
            return false;
        } catch (Throwable e) {
            return false;
        }
    }
    
    /**
     * f�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        // spezifische Variablen
        UIAnalyserEcos myUI = null;                             //Variable f�r UIAnalyser
        BoundInfo measure;                                  //das Ergebnis der Spannungsmessung
        String awt = null;                                  //Anweisungstext
        boolean udInUse = false;                            //Ob eine Meldung auf dem Handterminal dargestellt wird
        boolean isException = false;                        //Ob eine Ausnahme bekommt
        UserDialog myDialog = null;                         //Variable f�r Dialogbox
        String exceptionName = null;                        //Exception-Name f�r eine konkrete Exception
        int istValue=0, minValue=0, maxValue=0;             //Minimal-, Maximal- und Sollwert von Strom
        int minValueV = 0, maxValueV = 0, istValueV = 0;    //Minimal-, Maximal- und Sollwert von Spannung
        int intZange = 0;                                   //Variable f�r Parameter POLARITY
        //int intMessage = 0;                               //Variable f�r Parameter PROBE_MESSAGE
        String prbMessage = null;                           //Variable f�r Parameter PROBE_MESSAGE
        int loggerEnable = 1;                               //LOGGER_ENABLE
        boolean ECOS_TRACE_TEST_STEP=false;					//bestimmt ob ein einzelnes Trace augfezeichnet wird
        boolean ECOS_TRACE_CONTINUOUS=false;
        int oldOffsetStatus = UIMultizet.OFFSET_NOTREADY, newOffsetStatus = UIMultizet.OFFSET_NOTREADY;
        boolean batAnschliessen = true;
        
        try {
            try {
                //�berpr�fung der Parameter
                if( checkArgs() == false ) throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
            } catch (PPExecutionException e) {
                //Parametrierfehler
                e.printStackTrace();
                if (e.getMessage() != null)
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "", "", "", "", e.getMessage(), PB.getString( "parametrierfehler" ), Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "", "", "", "", e.getMessage(), PB.getString( "parametrierfehler" ), Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
			try {//Holen der Pr�fstandsvariable ECOS_PPS_DEBUG
				DEBUG = ((Boolean)getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PS_CONFIG,"ECOS_PPS_DEBUG")).booleanValue();
			} catch (Exception e) {
				if (e instanceof VariablesException) {
					//Falls keine Pr�fstandsvariable angegeben ist, oder es tritt beim Abholen ein Fehler auf
					// TransientenModus inaktiv
					DEBUG = false;
				} else {
					throw new PPExecutionException();
				}
			}//Ende Holen der Pr�fstandsvariablen ECOS_PPS_DEBUG             
 
            //Holen der Werte von Offset-Toleranzbereich
            minValue = Integer.parseInt( getArg(getRequiredArgs()[0]));
            maxValue = Integer.parseInt( getArg(getRequiredArgs()[1]));
            
            //Holen der Werte von Spannungstoleranzbereich
            minValueV = Integer.parseInt( getArg(getRequiredArgs()[2]));
            maxValueV = Integer.parseInt( getArg(getRequiredArgs()[3]));
            
            //Holen des Wertes des Parameters POLARITY
            if( getArg( getOptionalArgs()[0] ) != null ){
                intZange = Integer.parseInt( getArg( getOptionalArgs()[0]));
            }
            
            //Holen des Wertes des Parameters PROBE_MESSAGE
            if( getArg( getOptionalArgs()[1] ) != null ){

				prbMessage = PB.getString(getArg(getOptionalArgs()[1]));
                //intMessage = Integer.parseInt( getArg( getOptionalArgs()[1]));
            }
            if( DEBUG ) {
            	if( prbMessage != null ){
            		System.out.println( "prbMessage: " + prbMessage );
            		//System.out.println( "PROBE_MESSAGE: " + getArg(getOptionalArgs()[1]) );
            	}	
            	else 
            		System.out.println( "prbMessage: null" );
    		}
            
            //LOGGER_ENABLE
            if ( getArg( getOptionalArgs()[2] ) != null ){
                loggerEnable = Integer.parseInt( getArg( getOptionalArgs()[2]));
            }

            
			try {//Holen der Pr�fstandsvariablen ECOS_TRACE_TEST_STEP
				ECOS_TRACE_TEST_STEP = ((Boolean)getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PS_CONFIG,"ECOS_TRACE_TEST_STEP")).booleanValue();
			} catch (Exception e) {
				if (e instanceof VariablesException) {
					//Falls keine Pr�fstandsvariable angegeben ist, oder es tritt beim Abholen ein Fehler auf
					// TransientenModus inaktiv
					ECOS_TRACE_TEST_STEP = false;
				} else {
					throw new PPExecutionException();
				}
			}//Ende Holen der Pr�fstandsvariablen ECOS_TRACE_TEST_STEP             
            
			try {//Holen der Pr�fstandsvariablen ECOS_TRACE_CONTINUOUS
				ECOS_TRACE_CONTINUOUS =((Boolean)getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PS_CONFIG,"ECOS_TRACE_CONTINUOUS")).booleanValue();				
			} catch (Exception e) {
				if (e instanceof VariablesException) {
					//Falls keine Pr�fstandsvariable angegeben ist, oder es tritt beim Abholen ein Fehler auf
					// TransientenModus inaktiv
					ECOS_TRACE_CONTINUOUS = false;
				} else {
					throw new PPExecutionException();
				}
			}//Ende Holen der Pr�fstandsvariablen ECOS_TRACE_CONTINUOUS
			
			//ECOS_TRACE_CONTINUOUS ist dominant
			if(ECOS_TRACE_CONTINUOUS){
				ECOS_TRACE_TEST_STEP=false;
			}
            
            try {
                //Holen des Devices UIAnalyser
                myUI = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
//                myUI.triggerFilter.setFilter(0, 2);
                //Lable in Transientdatei rein schreiben
                myUI.LogSetTestStepName(this.getName());
                myUI.messFilter.setFilter(0, 2);
				if(!(ECOS_TRACE_TEST_STEP)){//Pr�fschrittlog ausschalten falls ECOS_TRACE_TEST_STEP=false
					myUI.setLog(false);
					loggerEnable=0;
				}                
                if (myUI.getLog() == true || loggerEnable == 1){ //wenn die Funktion f�r die Transientenaufnahme eingeschaltet ist
                    if(loggerEnable == 1){
                        myUI.setLog(true);
                        myUI.setVerbraucher(this.getName());   //Verbrauchername wird �bergegeben
                    }
				    Date date = new Date();
			        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
			        //SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
					String FileName = CascadeProperties.getCascadeHome()+File.separator+
						"trace"+File.separator+
						"ecos"+File.separator+dateFormat.format(date)+File.separator+//Tag/Ordner
						getPr�fling().getAuftrag().getFahrgestellnummer7();
						//+ "_" + dateTimeFormat.format(date);
                    myUI.prepareLog(FileName, false);
                }
            } catch (Exception e) {
                //Systemfehler, muss man genau analysieren
                e.printStackTrace();
                if (e instanceof DeviceLockedException){
                    exceptionName = "DeviceLockedException";
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "", "", "", "", e.getMessage(), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
                }
                else if (e instanceof DeviceNotAvailableException){
                    exceptionName = "DeviceNotAvailableException";
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "", "", "", "", e.getMessage(), "DeviceNotAvailableException", Ergebnis.FT_NIO_SYS );
                }
                else {
                    exceptionName = "Exception";
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "", "", "", "", e.getMessage(), "Exception", Ergebnis.FT_NIO_SYS );
                }
                try {
                    getPr�flingLaufzeitUmgebung().releaseUserDialog();
                    getPr�flingLaufzeitUmgebung().getUserDialog().requestAlertMessage(PB.getString( "meldung" ), exceptionName + ";" + e.getMessage(), 0);
                } catch (Exception se) {
                    se.printStackTrace();
                    ergListe = reportException( ergListe, se );
                    throw new PPExecutionException();
                }
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            //Offset-Ermittlung

            if( DEBUG ) {
    			System.out.println( "UIInit begin." + "\n" );
    		}
			String title = PB.getString( "zange" );
       		if (myUI.IOffsetRequired() == true) {
       			boolean newStatus = true;
       			newOffsetStatus = myUI.getActUI().oStatus;
       			oldOffsetStatus = myUI.getActUI().oStatus;
                getPr�flingLaufzeitUmgebung().releaseUserDialog();  //Device Dialgobox wird freigegeben
                //udInUse = false;
        		boolean isconnected = false;
        		batAnschliessen = true;
                while((newOffsetStatus != UIMultizet.OFFSET_IO) || !isconnected) {
                	while(newOffsetStatus != UIMultizet.OFFSET_IO) {
                		if (newOffsetStatus == UIMultizet.OFFSET_ERR_FLUCT ||
                		newOffsetStatus == UIMultizet.OFFSET_ERR_HIGH ||
						newOffsetStatus == UIMultizet.OFFSET_ERR_LOW ) {
                			awt = PB.getString( "OffsetNIO" ) + " " + PB.getString( "BatterieadaprerErneutAnschliessen" );
	                    }
                		else if (newOffsetStatus == UIMultizet.OFFSET_NOTREADY) {
                			awt = PB.getString( "batterieAdapter" );
                			//awt = PB.getString( "KeinOffsetOffsetabgleichDurchfuehren" );
                		}
	                    else if (newOffsetStatus == UIMultizet.OFFSET_RUNNING) {
	                        awt = PB.getString( "offsetAbgleichLaeuft" );
	                    }	
	                	if (newStatus){
	    					try {
				                myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();           //Holen der Dialogbox
				                myDialog.setAllowCancel(true);                                      //Abbruchtaste definiert f�r Dialogbox
				                myDialog.displayUserMessage( title, awt, -1 );    //AWT wird auf dem Bildschirm dargestellt
				            } catch (Exception ex) {
				                //Systemfehler, muss man genau analysieren
			                	ex.printStackTrace();
			                    ergListe = reportException( ergListe, ex );
			                    throw new PPExecutionException("Get UserDialog");
				            }        			
	                    }
	                    if (myDialog.isCancelled() == true) { //Wenn Wecker Abbruchtaste dr�ckt
	                        result = new Ergebnis( PB.getString("messung"), PB.getString("Werker"), "", "", "", "", "", "", "", "", "", PB.getString("werkerAbbruch"), "", "", "", Ergebnis.FT_NIO );
	                        ergListe.add(result);
	                        throw new PPExecutionException();
	                    }
	                	oldOffsetStatus = newOffsetStatus;
	                	Thread.sleep(250); //Warte immer 250 Millisekunden
	           			newOffsetStatus = myUI.getActUI().oStatus;
	           			if (newOffsetStatus == oldOffsetStatus) {
	           				newStatus = false;
	           			} else {
	           				newStatus = true;
	           			}
	                	if (newStatus && (newOffsetStatus != UIMultizet.OFFSET_RUNNING) && (newOffsetStatus != UIMultizet.OFFSET_IO)){
		                    int minOffsetToleranz = 0 - myUI.getMaxOffsetToleranz();
		                    result = new Ergebnis( PB.getString("messung"), "UIAnalyser", "", "", "", "Offset (mA)", ""+ myUI.getActUI().oValue, "" + minOffsetToleranz, ""+ myUI.getMaxOffsetToleranz(), "", "", "", "", "Offsetstatus " + newOffsetStatus, "", Ergebnis.FT_IO );
		                    ergListe.add(result);
		                    result = new Ergebnis( PB.getString("messung"), "UIAnalyser", "", "", "", "Max. Off. Diff. (mA)", ""+ myUI.getActUI().oDifferenz, "0", ""+ myUI.getMaxOffsetFluct(), "", "", "", "", "Offsetstatus " + newOffsetStatus, "", Ergebnis.FT_IO );
		                    ergListe.add(result);
	                	}	
	                }
	                if( DEBUG ) {
	        			System.out.println( "Offset IO. newOffsetStatus = " + newOffsetStatus);
	        		}
	                if( DEBUG ) {
	        			System.out.println( "Offset Status = " + myUI.getActUI().oStatus + " Offsetval = " + myUI.getActUI().oValue + " OffsetDif = " + myUI.getActUI().oDifferenz);
	        		}
					while(!isconnected) {
						Thread.sleep(250);
						if ( !myUI.getLastReadError()) {
							isconnected = true;
				            if( DEBUG ) {
				    			System.out.println( "ECOS Zange ist angeschlossen." + "\n" );
				    		}						
						}
						else {
							if(batAnschliessen) {
								batAnschliessen = false;
								if( DEBUG ) {
				        			System.out.println( "ECOS Zange ist nicht angeschlossen oder Kommunikationsprobleme." + "\n");
				        		}
		                        awt = PB.getString( "batterieAdapter" );
		    					try {
		    		                getPr�flingLaufzeitUmgebung().releaseUserDialog();  //Device Dialgobox wird freigegeben
					                myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();           //Holen der Dialogbox
					                myDialog.setAllowCancel(true);                                      //Abbruchtaste definiert f�r Dialogbox
					                myDialog.displayUserMessage( title, awt, -1 );    //AWT wird auf dem Bildschirm dargestellt
					            } catch (Exception ex) {
					                //Systemfehler, muss man genau analysieren
				                	ex.printStackTrace();
				                    ergListe = reportException( ergListe, ex );
				                    throw new PPExecutionException("Get UserDialog");
					            }        			
							}
							if (myDialog.isCancelled() == true) { //Wenn Wecker Abbruchtaste dr�ckt
		                        result = new Ergebnis( PB.getString("messung"), PB.getString("Werker"), "", "", "", "", "", "", "", "", "", PB.getString("werkerAbbruch"), "", "", "", Ergebnis.FT_NIO );
		                        ergListe.add(result);
		                        throw new PPExecutionException();
		                    }
						}
					}
           			newOffsetStatus = myUI.getActUI().oStatus;
                }
        	}
       		if ( prbMessage != null ){
			//awt = PB.getString("ZangeUmgeschlossen");
				try {
					getPr�flingLaufzeitUmgebung().releaseUserDialog();
					getPr�flingLaufzeitUmgebung().getUserDialog()
							.requestUserMessage(PB.getString("zange"), prbMessage, 0);
				} catch (Exception e) {
					//Systemfehler, muss man genau analysieren
					e.printStackTrace();
					result = new Ergebnis("ExecFehler", "Userdialog", "", "",
							"", "", "", "", "", "0", "", "", "",
							e.getMessage(), PB
									.getString("unerwarteterLaufzeitfehler"),
							Ergebnis.FT_NIO_SYS);
					ergListe.add(result);
					throw new PPExecutionException("Get UserDialog");
				}
       		}
			/**Auf Best�tigung warten end**/
			//int maxOffset = ((Boolean)getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PS_CONFIG,"ECOS_TRACE_TEST_STEP")).booleanValue();
			//int maxOffsetDiff = ((Integer)getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PS_CONFIG,"ECOS_TRACE_TEST_STEP")).booleanValue();
            if( DEBUG ) {
    			System.out.println( "Offset Status = " + myUI.getActUI().oStatus + " Offsetval = " + myUI.getActUI().oValue + " OffsetDif = " + myUI.getActUI().oDifferenz);
    		}
            result = new Ergebnis( PB.getString("messung"), "UIAnalyser", "", "", "", PB.getString( "batterieSpannung" ) + "(mV)", ""+ myUI.getActUI().uValue, "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
            ergListe.add(result);
            int minOffsetToleranz = 0 - myUI.getMaxOffsetToleranz();
            result = new Ergebnis( PB.getString("messung"), "UIAnalyser", "", "", "", "Offset (mA)", ""+ myUI.getActUI().oValue, "" + minOffsetToleranz, ""+ myUI.getMaxOffsetToleranz(), "", "", "", "", "", "", Ergebnis.FT_IO );
            ergListe.add(result);
            result = new Ergebnis( PB.getString("messung"), "UIAnalyser", "", "", "", "Max. Off. Diff. (mA)", ""+ myUI.getActUI().oDifferenz, "0", ""+ myUI.getMaxOffsetFluct(), "", "", "", "", "", "", Ergebnis.FT_IO );
            ergListe.add(result);

            /*result = new Ergebnis( PB.getString("messung"), "UIAnalyser", "", "", "", PB.getString( "batterieSpannung" ) + "(mV)", "" + istValueV, "" + minValueV, "" + maxValueV, "", "", "", "", "", "", Ergebnis.FT_IO );
            ergListe.add(result);*/
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            //Systemfehler, muss man genau analysieren
            e.printStackTrace();
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
            //Systemfehler, muss man genau analysieren
            e.printStackTrace();
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
        //Freigabe der verwendeten Devices
        if( myUI != null ) {
            try{
                if (myUI.getLog() == true){     //Schliesst Log-Datei
                    if(loggerEnable == 1){
                        myUI.setLog(false);
                    }
                    myUI.exitLog();
                }
            } catch (DeviceIOException e){
                //Systemfehler, muss man genau analysieren
                e.printStackTrace();
                result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "DeviceIOException", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                status = STATUS_EXECUTION_ERROR;
            }
            //Device UIAnalyser wird freigegeben
            try {
                getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
            } catch (Exception e) {
                //Systemfehler, muss man genau analysieren
                e.printStackTrace();
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "release", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                status = STATUS_EXECUTION_ERROR;
            }
            myUI = null;
        }
        try {
            //Device Dialgobox wird freigegeben
            getPr�flingLaufzeitUmgebung().releaseUserDialog();
        } catch (Exception e) {
            //Systemfehler, muss man genau analysieren
            e.printStackTrace();
            ergListe = reportException( ergListe, e );
            status = STATUS_EXECUTION_ERROR;
        }
        //Status setzen
        setPPStatus( info, status, ergListe );
    }
    
    //Report der Probleme mit UserDialog
    private Vector reportException( Vector ergListe, Exception e ) {
        Ergebnis result;
        if (e instanceof DeviceLockedException)
            result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
        else if (e instanceof DeviceNotAvailableException)
            result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "DeviceNotAvailableException", Ergebnis.FT_NIO_SYS );
        else
            result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "Exception", Ergebnis.FT_NIO_SYS );
        ergListe.add(result);
        return ergListe;
    }
}

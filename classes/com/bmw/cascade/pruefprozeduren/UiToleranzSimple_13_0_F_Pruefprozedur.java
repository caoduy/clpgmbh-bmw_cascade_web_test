/*
 * UiToleranzSimple_V10_0_0_FA_Pruefprozedur.java
 *
 * Created on 03.12.03
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;


import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.uianalyse.*;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;


/** Pr�fprozedur, die auf eine Grenzwerteinhaltung (MIN,MAX) des Strom- (I) oder Spannungswertes (U) innerhalb eines
 * Timeouts wartet. Wie lange die Grenzwerteinhaltung erf�llt sein soll, ist mit OK_TIME parametrierbar. Ebenfalls kann ein
 * Referenzwert REF �bergeben werden. Parallel kann eine Benutzeranweisung AWT ausgegeben werden.
 * PAUSE legt die Zeit in ms zwischen zwei Messungen fest. CHANNEL legt den Kanal bei der Verwendung von ICP-Modulen fest.
 * HWT wird implements Fehlerfall als Hinweistext ausgegeben.
 * @author Winklhofer
 * @version Implementierung
 * @version 03.12.2003 V0.0.1 FA Peter Winklhofer: Erste Testversion
 * @version 15.12.2003 V0.0.2 TA Peter Winklhofer: Referenzwert ber�cksichtigen
 * @version 19.01.2004 V0.0.3 TA Peter Winklhofer: Einige Texte/Ausgaben ge�ndert
 * @version 02.03.2004 V0.0.4 TA HWT hinzugef�gt
 * @version 10.03.2004 V0.0.5 FA Produktivversion
 * @version 30.05.2006 V6_0_T DEBUG als optionaler Parameter, default FALSE
 * @version 30.05.2006 V7_0_F Produktivversion
 * @version 24.07.2007 V9_0_T DEVICE als optionaler Parameter
 * @version 12.02.2008 V10_0_F MM Produktivversion
 * @version 03.06.2008 V11_0_F CS APDM Ergebnis-Format Anpassung
 * @version 04.05.2016 V12_0_T MK Wenn PAUSE l�nger als OK_TIME dann wird nur die erste Ausf�hrung ausgewertet
 * @version 04.05.2016 V13_0_F MK F-Version
 */
public class UiToleranzSimple_13_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    	static final long serialVersionUID = 1L;
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
        public UiToleranzSimple_13_0_F_Pruefprozedur() {}

  /**
     * erzeugt eine neue Pruefprozedur mit obigem Verhalten
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
        public UiToleranzSimple_13_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    /**
     * initialsiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /**
     * liefert die optionalen Argumente
     */
    public String[] getOptionalArgs() {
        String[] args = {"MIN","MAX","REF","OK_TIME","AWT","CHANNEL","TIMEOUT","PAUSE","HWT","DEBUG","DEVICE"};
        return args;
    }
    
    /**
     * liefert die zwingend erforderlichen Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = {"RESULT"};
        return args;
    }
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
     */
    public boolean checkArgs() {
        String temp;
        float i, j;
        long k,l,m;
        int n;
        boolean ok;
        
        try{
            ok = super.checkArgs();
            if( ok == true ) {
                //RESULT muss I oder U sein
                temp = getArg("RESULT").toUpperCase();
                if( (temp.equalsIgnoreCase("I") == false) && (temp.equalsIgnoreCase("U") == false) ) return false;
                //TIMEOUT muss numerisch und positiv sein
                temp = getArg("TIMEOUT");
                k=0;
                try {
                    if(temp != null) {
                        k =Long.parseLong( temp);
                        if (k < 0) return false;
                    }
                } catch (NumberFormatException e) {
                    return false;
                }
                //OK_TIME pr�fen
                temp = getArg("OK_TIME");
                l=0;
                try {
                    if(temp != null) {
                        l =Long.parseLong( temp);
                        if (l < 0) return false;
                    }
                } catch (NumberFormatException e) {
                    return false;
                }
                //OK_TIME <= TIMEOUT
                if (l > k) return false;
                //PAUSE pr�fen
                temp = getArg("PAUSE");
                m=0;
                try {
                    if(temp != null) {
                        m =Long.parseLong( temp);
                        if (m < 0) return false;
                    }
                } catch (NumberFormatException e) {
                    return false;
                }
                //MIN pr�fen
                temp = getArg("MIN");
                i = Float.MIN_VALUE;
                try {
                    if ((temp != null) && (temp.indexOf('@') == -1)) {
                        i = Float.parseFloat( temp );
                    }
                } catch (NumberFormatException e) {
                    return false;
                }
                //MAX pr�fen
                temp = getArg("MAX");
                j = Float.MAX_VALUE;
                try {
                    if ((temp != null) && (temp.indexOf('@') == -1)) {
                        j = Float.parseFloat( temp );
                    }
                } catch (NumberFormatException e) {
                    return false;
                }
                //MIN <= MAX
                if (i > j) return false;
                //REF pr�fen
                temp = getArg("REF");
                try {
                    if ((temp != null) && (temp.indexOf('@') == -1)) {
                        j = Float.parseFloat( temp );
                    }
                } catch (NumberFormatException e) {
                    return false;
                }
                //AWT pr�fen
                //�ber checkArgs gepr�ft
                
                //CHANNEL pr�fen: Auszulesender A/D-Kanal (ICP-Modul oder ADDI-DATA)
                temp = getArg("CHANNEL");
                n=0;
                try {
                    if(temp != null) {
                        n =Integer.parseInt( temp);
                        //M�glicher A/D-Kanal: z.B. 1 bis 8 (Hinweis: Das Modul arbeitet aber mit 0 bis 7)
                        if (n <= 0) return false;
                    }
                } catch (NumberFormatException e) {
                    return false;
                }
            }
            return ok;
        } catch (Exception e) {
            return false;
        } catch (Throwable e) {
            return false;
        }
    }
    
    
    /**
     * f�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        // spezifische Variablen
        UIAnalyser myUI = null;
        BoundInfo measure;
        
        //Legt fest, ob U oder I ausgewertet werden soll (U=>false, I=>true)
        boolean measure_I = false;
        
        String temp;
        String minText,maxText;         //F�r Resultausgabe
        boolean minPresent = false;     //true, falls min vorhanden
        boolean maxPresent = false;     //true, falls max vorhanden
        String awt=null;
        String hwt=null;
        int channel = 0;
        long startTime = System.currentTimeMillis();
        long currentTime = 0;
        long firstOkayTime = 0;
        long maxTime = 0;
        float ref=0, min=Float.MIN_VALUE, max=Float.MAX_VALUE;
        float currentValue = 0;
        long oktime=0, timeout=0, pause=0;
        boolean udInUse = false;
        boolean goon = true;
        boolean debug = false;
        UserDialog myDialog = null;
        String device=null; 
        
        //APDM Ergebnis Format Anpassung
        String param3="";
        
        try {
            try {
                //Parameter checken und holen f�r 1-ten Teil (Referenzwert)
                if( checkArgs() == false ) throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
                
                if (getArg("RESULT").equalsIgnoreCase("I")) {
                    measure_I = true;
                    //debug
                    if (debug) System.out.println("Measure I");
                } else {
                    measure_I = false;
                    //debug
                    if (debug) System.out.println("Measure U");
                }
                
                //TIMEOUT holen
                temp = getArg("TIMEOUT");
                if(temp != null) {
                    timeout =Long.parseLong( temp);
                } else {
                    timeout = 0;
                }
                
                //OK_TIME holen
                temp = getArg("OK_TIME");
                if(temp != null) {
                    oktime =Long.parseLong( temp);
                } else {
                    oktime = 0;
                }
                
                //MIN holen
                temp = getArg("MIN");
                if (temp != null) {
                    temp = extractValues(temp)[0];
                    min = Float.parseFloat( temp );
                    minPresent = true;
                } else {
                    min = Float.MIN_VALUE;
                    minPresent = false;
                }
                
                //MAX holen
                temp = getArg("MAX");
                if (temp != null) {
                    temp = extractValues(temp)[0];
                    max = Float.parseFloat( temp );
                    maxPresent = true;
                } else {
                    max = Float.MAX_VALUE;
                    maxPresent = false;
                }
                
                //REF holen
                temp = getArg("REF");
                if (temp != null) {
                    temp = extractValues(temp)[0];
                    ref = Float.parseFloat( temp );
                } else {
                    ref = 0;
                }
                
                //AWT holen
                awt = getArg("AWT");
                if (awt == null) awt = "";
                
                //CHANNEL pr�fen: Auszulesender A/D-Kanal (ICP-Modul oder ADDI-DATA)
                temp = getArg("CHANNEL");
                if(temp != null) {
                    //M�glicher A/D-Kanal: z.B. 1 bis 8 (Hinweis: Das Modul arbeitet aber mit 0 bis 7)
                    channel =Integer.parseInt( temp);
                } else {
                    channel = 0;    //0 ist ung�ltiger Kanal
                }

                //PAUSE holen
                temp = getArg("PAUSE");
                if(temp != null) {
                    pause =Long.parseLong( temp);
                } else {
                    pause = 0;
                }

                //HWT holen
                hwt = getArg("HWT");
                if (hwt == null) hwt = "";
                
                //DEBUG holen
                temp = getArg("DEBUG");
                if ((temp != null) && temp.equalsIgnoreCase("TRUE")) {
                    debug = true;
                } else {
                    debug = false;
                }
                
                //DEVICE holen
                device = getArg("DEVICE");
                
            } catch (PPExecutionException e) {
                if (e.getMessage() != null)
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            } catch (NumberFormatException n) {
                if (n.getMessage() != null)
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), n.getMessage(), Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            //Ausf�hrung
            //"CHANNEL" Setze zu lesenden A/D-Kanal, wenn CHANNEL angegeben.
            if( channel >= 1) {
                try {
                    myUI = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyser(device);
                    myUI.setKanal(channel);
                } catch (DeviceLockedException e) {
                    e.printStackTrace();
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "deviceLockedException" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                    ergListe.add(result);
                    throw new PPExecutionException();
                } catch (DeviceNotAvailableException e) {
                    e.printStackTrace();
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "deviceNotAvailableException" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                    ergListe.add(result);
                    throw new PPExecutionException();
                } catch (DeviceParameterException e) {
                    e.printStackTrace();
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "deviceParameterException" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                    ergListe.add(result);
                    throw new PPExecutionException();
                } catch (DeviceIOException e) {
                    e.printStackTrace();
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                    ergListe.add(result);
                    throw new PPExecutionException();
                }
            }
            
            //startTime/maxTime setzen
            startTime = System.currentTimeMillis();
            currentTime = startTime;            //Falls kein Timeout, da sonst lazy evaluation in while-Bedingung
            maxTime=startTime + timeout;
            firstOkayTime = 0;
            
            //status per default auf NIO
            status = this.STATUS_EXECUTION_ERROR;
            
            //UI-Analyser holen
            if (myUI == null) myUI = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyser(device);
            
            //HauptSchleife (Solange goon == true und maxTime noch nicht erreicht, falls timeout > 0
            while (goon && ((timeout <= 0) || ((currentTime = System.currentTimeMillis()) <= maxTime)) ) {
                
                //debug
                if (debug) System.out.println("Entering new loop");
                
                //status per default auf NIO
                status = this.STATUS_EXECUTION_ERROR;
                
                //Wert holen
                try {
                    
                    if (measure_I) {
                        currentValue = myUI.recordUIPair().iValue - ref;
                    } else {
                        currentValue = myUI.recordUIPair().uValue - ref;
                    }
                    
                    //debug
                    if (debug) System.out.println("Value: " + currentValue);
                    
                } catch (DeviceParameterException e) {
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                    ergListe.add(result);
                    throw new PPExecutionException();
                } catch (DeviceIOException e) {
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                    ergListe.add(result);
                    throw new PPExecutionException();
                } catch (IOException e) {
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                    ergListe.add(result);
                    throw new PPExecutionException();
                }
                
                //Bewerten
                if ( (!minPresent || (min <= currentValue)) 
                    && (!maxPresent || (max >= currentValue))) {
                    //Wir haben ein IO-Sample
                    status = this.STATUS_EXECUTION_OK;
                    //debug
                    if (debug) System.out.println("Value Okay: true");
                    //Wenn noch kein firstOkay gesetzt, seten
                    if ( (firstOkayTime == 0) || (firstOkayTime > currentTime)) {
                        firstOkayTime = currentTime;
                    }
                } else {
                    //NIO
                    firstOkayTime = 0;
                    //debug
                    if (debug) System.out.println("Value Okay: false");
                }
                
                //debug
                if (debug) System.out.println("CurrentTime: " + currentTime);
                if (debug) System.out.println("firstOkay: " + firstOkayTime);
                if (debug) System.out.println("OkayTime: " + oktime);
                
                //Wenn Wert IO und OK_TIME vorhanden
                if ((status == this.STATUS_EXECUTION_OK) && (oktime > 0)) {
                    //Pr�fe OK_TIME erreicht
                    if ( (currentTime - firstOkayTime) < oktime) {
                        status = this.STATUS_EXECUTION_ERROR;
                        //debug
                        if (debug) System.out.println("OkayTime not reached");
                    } else {
                        //Fertig: OK_TIME erreicht
                        goon = false;
                        //debug
                        if (debug) System.out.println("OkayTime reached!!!");
                    }
                    if (oktime<=pause){
                    	status = this.STATUS_EXECUTION_OK;
                    	 //Fertig: OK_TIME<=pause -> ein Mal IO ist genug
                        goon = false;
                        //debug
                        if (debug) System.out.println("IO because OK_TIME<=PAUSE -> single IO is enough");
                    }
                } else {
                    //keine OK_TIME und Wert IO -> Schleife verlassen
                    if (status == this.STATUS_EXECUTION_OK) goon = false;
                }
                
                //kein TIMEOUT, also keine Schleife
                if (timeout <= 0) goon = false;
                
                //AWT auswerten
                if (udInUse && (myDialog != null)) {
                    //Teste ob Cancel gedr�ckt wurde
                    if (myDialog.isCancelled()) {
                        goon = false;
                        status = this.STATUS_EXECUTION_ERROR;
                        //nur Param3 darf bef�llt werden, Param1 sollte SGBD beinhalten,
                        //Param2 sollte APIJOB beinhalten
                        param3 = ""+(currentTime - startTime)+ "; "+oktime+"; "+timeout;
                        //debug
                        if (debug) System.out.println("Cancelled!");
                        result = new Ergebnis( "ABBRUCH", "UIAnalyser", "", "", param3, "ABBRUCHVALUE", ""+currentValue, ""+min, ""+max, "0", "", "", awt, PB.getString("abbruchDurchWerker"), hwt, Ergebnis.FT_NIO );
                        ergListe.add(result);
                    }
                }
                
                //AWT anzeigen
                if ((status == this.STATUS_EXECUTION_ERROR) && (goon == true) && !udInUse && (awt != null)) {
                    awt = PB.getString( awt );
                    try { //Message setzen
                        myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
                        udInUse = true;
                        myDialog.setAllowCancel(true);
                        myDialog.displayMessage( PB.getString( "anweisung" ), awt, -1 );
                        //debug
                        if (debug) System.out.println("AWT displayed");
                    } catch (Exception e) {
                        if (e instanceof DeviceLockedException)
                            result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
                        else if (e instanceof DeviceNotAvailableException)
                            result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
                        else
                            result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
                        ergListe.add(result);
                        throw new PPExecutionException();
                    }
                }
                
                //Pause abwarten, falls definiert
                if (pause > 0) {
              try {
                Thread.sleep(pause);
              } catch( InterruptedException e ) {
              }
            }
                    
                
            } //Ende While (Hauptschleife)
            
            //minText und maxText erzeugen
            if (minPresent) {
                minText = ""+min;
            } else {
                minText = "-INF";
            }
            if (maxPresent) {
                maxText = ""+max;
            } else {
                maxText = "+INF";
            }
            
            //nur Param3 darf bef�llt werden, Param1 sollte SGBD beinhalten,
            //Param2 sollte APIJOB beinhalten
            param3 = ""+(currentTime - startTime)+ "; "+oktime+"; "+timeout;
            //Result f�r Wert erzeugen
            if (status == this.STATUS_EXECUTION_OK) {
                //IO-Result
                result = new Ergebnis( "VALUE", "UIAnalyser", "", "", param3, "VALUE", ""+currentValue, minText, maxText, "", "", "", awt, "", "", Ergebnis.FT_IO );
                ergListe.add(result);
            } else {
                //NIO-Result
                result = new Ergebnis( "VALUE", "UIAnalyser", "", "", param3, "VALUE", ""+currentValue, minText, maxText, "", "", "", awt, "", hwt, Ergebnis.FT_NIO );
                ergListe.add(result);
            }
            
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
        //Freigabe der benutzten Devices
        if( myUI != null ) {
            try {
                getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyser();
            } catch (Exception e) {
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                status = STATUS_EXECUTION_ERROR;
            }
            myUI = null;
        }
        if( udInUse == true ) {
            try {
                getPr�flingLaufzeitUmgebung().releaseUserDialog();
                myDialog = null;
                udInUse = false;
            } catch (Exception e) {
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                status = STATUS_EXECUTION_ERROR;
            }
        }
        
        //Status setzen
        setPPStatus( info, status, ergListe );
    }
    
    
}


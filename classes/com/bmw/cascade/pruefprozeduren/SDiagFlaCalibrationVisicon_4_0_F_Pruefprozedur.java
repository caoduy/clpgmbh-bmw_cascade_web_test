package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.ediabas.*;
import com.bmw.cascade.pruefstand.devices.serialcontrol.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.PrueflingLaufzeitUmgebung;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * SDiagFlaCalibrationVisicon_4_0_F_Pruefprozedur.java<BR>
 * bei Aenderung Hr. Pichler TI-432 Tel. 68609 informieren!<BR>
 * <BR>
 * Implementierung FLA Kalibrierung am Visicon Scheinwerferpruefstand<BR>
 * <BR>
 * @author Thomas Buboltz; BMW AG<BR>
 *
 * @version V0_0_1 26.01.05  SP   Ersterstellung<BR>
 * @version V0_0_2 11.02.05  SP   Methodenruempfe entfernt f�r eine Integration in Dgf<BR>
 * @version V0_0_3 14.02.05  SP   Methodenruempfe hinzugefuegt nach der IBN in Dgf.<BR>
 * @version V0_0_4 17.05.05  WM   all class variables set to null                                                  Jobs POSITION_TARGET_HOME und OPEN_CENTRATION durch RESET ersetzt.<BR>
 *
 */
public class SDiagFlaCalibrationVisicon_4_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    static final long serialVersionUID = 1L;
    /**
     * DefaultKonstruktor, nur fuer die Deserialisierung
     */
    public SDiagFlaCalibrationVisicon_4_0_F_Pruefprozedur() {}
    
    /** Attribut fuer serialControl */
    private  SerialControl mySerial = null;
    
    /** Attribut fuer den Auftrag */
    private String sAuftrag = null;
    
    /**
     * erzeugt eine neue Pruefprozedur mit obigem Verhalten
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public SDiagFlaCalibrationVisicon_4_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    /**
     * initialisiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /**
     * liefert die optionalen Argumente
     * @return Stringvektor der optionalen Argumente
     */
    public String[] getOptionalArgs() {
        String[] args = {"MESSAGE", "MAX_WAITINGTIME_S"};
        return args;
    }
    
    /**
     * liefert die zwingend erforderlichen Argumente
     * @return Stringvektor der ben�tigten Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = {"JOB"};
        return args;
    }
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
     * @return true, wenn die Ueberpruefung der zwingenden Argumente ok ist
     */
    public boolean checkArgs() {
        String[] requiredArgs = getRequiredArgs();
        String[] optionalArgs = getOptionalArgs();
        boolean answer = true;
        try {
            // JOB muss existieren
            if ( getArg(requiredArgs[0]) == null ) answer = false;
            // Wartezeit kann exisiteren, wenn dann Integer
            if ( getArg(optionalArgs[1]) != null ) {
                try {
                    Integer.parseInt(getArg(optionalArgs[1]));
                } catch(NumberFormatException e) {
                    answer = false;
                }
            }
        } catch (Exception e) {
            answer = false;
        }
        return(answer);
    }
    
    /**
     * f�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        // spezifische Variablen
        UserDialog myDialog = null;
        boolean udInUse = false;
        boolean cancelled = false;
        int trials = 20;
        mySerial = null;
        
        try {
            
            // Ueberpruefung der Argumente
            if(this.checkArgs() == false) {
                result = new Ergebnis("EXECFEHLER", "PRUEFPROZEDUR", "JOB: " + getArg(getRequiredArgs()[0]), "", "", "PARAMETER", "NOK", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            // Holen des SerialControl Devices
            try {
                mySerial = getPr�flingLaufzeitUmgebung().getDeviceManager().getSerialControl();
            } catch (Exception e) {
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis("EXECFEHLER", "SERIALCONTROL", "JOB: " + getArg(getRequiredArgs()[0]), "", "", "GETSERIALCONTROL", "DEVICELOCKED", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                else
                    result = new Ergebnis("EXECFEHLER", "SERIALCONTROL", "JOB: " + getArg(getRequiredArgs()[0]), "", "", "GETSERIALCONTROL", "EXCEPTION", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                throw new PPExecutionException();
            }
            
            // User Dialog ausgeben, sofern vorhanden
            if(getArg(getOptionalArgs()[0]) != null) {
                try {
                    udInUse = true;
                    myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
                    myDialog.setVisible(false);
                    myDialog.setAllowCancel(true);
                    if(System.getProperty("user.language").equalsIgnoreCase("de"))
                        myDialog.displayUserMessage("FLA-Kalibrierung", getArg(getOptionalArgs()[0]), -1);
                    else
                        myDialog.displayUserMessage("FLA-Calibration", getArg(getOptionalArgs()[0]), -1);
                    myDialog.setAllowCancel(true);
                    myDialog.setVisible(true);
                } catch (Exception e) {
                    if (e instanceof DeviceLockedException)
                        result = new Ergebnis("EXECFEHLER", "USERDIALOG", "JOB: " + getArg(getRequiredArgs()[0]), "", "", "GETUSERDIALOG", "DEVICELOCKED", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                    else
                        result = new Ergebnis("EXECFEHLER", "USERDIALOG", "JOB: " + getArg(getRequiredArgs()[0]), "", "", "GETUSERDIALOG", "EXCEPTION", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                    ergListe.add(result);
                    throw new PPExecutionException();
                }
            }
            
            // Ueberpruefen, ob Anlage bereit ist (Targetposition & Zentrierung)
            // jeweils 20 Sekunden warten (20*2*500ms)
            // --------------------------------------------------
            if(getArg(getRequiredArgs()[0]).equalsIgnoreCase("CHECK_MACHINE_READY")) {
                boolean ok = false;
                
                if(getArg(getOptionalArgs()[1]) != null) {
                    try {
                        trials = Integer.parseInt(getArg(getOptionalArgs()[1]));
                    } catch(NumberFormatException e) {
                        trials = 20;
                    }
                }
                int maxVersuche = trials;
                status = STATUS_EXECUTION_OK;
                while(!ok && trials >= 0 && !cancelled) {
                    try {
                        if(udInUse)
                            if (myDialog.isCancelled())
                                cancelled = true;
                        mySerial.transferData("CPOSSTAT;");
                        if(mySerial.receiveData(2).equalsIgnoreCase("DPH;"))
                            ok = true;
                        else
                            trials--;
                    } catch(DeviceIOException e) {}
                }
                if (!ok) {
                    result = new Ergebnis("CHECKMACHINEREADY", "SERIALCONTROL", "", "", "", "TARGETPOSITION", "NOTINHOME", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                    status = STATUS_EXECUTION_ERROR;
                    ergListe.add(result);
                }
                else {
                    result = new Ergebnis("CHECKMACHINEREADY", "SERIALCONTROL", "", "", "", "TARGETPOSITION", "HOME", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_IO);
                    ergListe.add(result);
                    ok = false;
                    while(!ok && trials >= 0) {
                        try {
                            mySerial.transferData("CCENTSTAT;");
                            if(mySerial.receiveData(2).equalsIgnoreCase("DCO;"))
                                ok = true;
                            else
                                trials--;
                        } catch(DeviceIOException e) {}
                    }
                    if (!ok) {
                        result = new Ergebnis("CHECKMACHINEREADY", "SERIALCONTROL", "", "", "", "CENTRATION", "CLOSED", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_IGNORE);
                        ergListe.add(result);
                    }
                    else {
                        result = new Ergebnis("CHECKMACHINEREADY", "SERIALCONTROL", "", "", "", "CENTRATION", "OPEN", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_IO);
                        ergListe.add(result);
                    }
                }
            }
            
            // Scheinwerfer-Applikation inaktiv setzen
            // 20 Sekunden warten (20*2*500ms)
            // --------------------------------------------------
            else if(getArg(getRequiredArgs()[0]).equalsIgnoreCase("SET_SW_IDLE")) {
                boolean ok = false;
                if(getArg(getOptionalArgs()[1]) != null) {
                    try {
                        trials = Integer.parseInt(getArg(getOptionalArgs()[1]));
                    } catch(NumberFormatException e) {
                        trials = 20;
                    }
                }
                int maxVersuche = trials;
                while(!ok && trials >= 0 && !cancelled) {
                    try {
                        if(udInUse)
                            if (myDialog.isCancelled())
                                cancelled = true;
                        mySerial.transferData("CSWIDLE;");
                        if(mySerial.receiveData(2).equalsIgnoreCase("RSWIDLE;"))
                            ok = true;
                        else
                            trials--;
                    } catch(DeviceIOException e) {}
                }
                if(!ok) {
                    result = new Ergebnis("SETSWIDLE", "SERIALCONTROL", "", "", "", "IDLESW", "NOK", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                    status = STATUS_EXECUTION_ERROR;
                }
                else {
                    result = new Ergebnis("SETSWIDLE", "SERIALCONTROL", "", "", "", "IDLESW", "OK", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_IO);
                    status = STATUS_EXECUTION_OK;
                }
                ergListe.add(result);
            }
            
            // Fahrzeugzentrierung schlie�en
            // 20 Sekunden warten (20*2*500ms)
            // --------------------------------------------------
            else if(getArg(getRequiredArgs()[0]).equalsIgnoreCase("CLOSE_CENTRATION")) {
                boolean ok = false;
                if(getArg(getOptionalArgs()[1]) != null) {
                    try {
                        trials = Integer.parseInt(getArg(getOptionalArgs()[1]));
                    } catch(NumberFormatException e) {
                        trials = 20;
                    }
                }
                int maxVersuche = trials;
                String sAuftrag = this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getSteuerschl�ssel();
                while(!ok && trials >= 0 && !cancelled) {
                    try {
                        if(udInUse)
                            if (myDialog.isCancelled())
                                cancelled = true;
                        mySerial.transferData("D" + sAuftrag + ";");
                        if(mySerial.receiveData(2).equalsIgnoreCase("R" + sAuftrag + ";"))
                            ok = true;
                        else
                            trials--;
                    } catch(DeviceIOException e) {}
                }
                if(!ok) {
                    result = new Ergebnis("SENDFGN", "SERIALCONTROL", "", "", "", "FZS", "NOK", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                    status = STATUS_EXECUTION_ERROR;
                }
                else {
                    result = new Ergebnis("SENDFGN", "SERIALCONTROL", "", "", "", "FZS", sAuftrag, "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_IO);
                    status = STATUS_EXECUTION_OK;
                    ok = false;
                    String received = new String("");
                    while(!ok && trials >= 0 && !cancelled) {
                        try {
                            if(udInUse)
                                if (myDialog.isCancelled())
                                    cancelled = true;
                            mySerial.transferData("CCENTSTAT;");
                            received = mySerial.receiveData(2);
                            if(received.equalsIgnoreCase("DCC;"))
                                ok = true;
                            else
                                trials--;
                        } catch(DeviceIOException e) {}
                    }
                    if(!ok) {
                        result = new Ergebnis("CLOSECENTRATION", "SERIALCONTROL", "", "", "", "CENTRATION", "NOTCLOSED", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                        status = STATUS_EXECUTION_ERROR;
                    }
                    else {
                        result = new Ergebnis("CLOSECENTRATION", "SERIALCONTROL", "", "", "", "CENTRATION", "CLOSED", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_IO);
                        status = STATUS_EXECUTION_OK;
                    }
                    ergListe.add(result);
                }
            }
            
            // FLA Target auf Mittelposition verfahren
            // 20 Sekunden warten (20*2*500ms)
            // --------------------------------------------------
            else if(getArg(getRequiredArgs()[0]).equalsIgnoreCase("POSITION_TARGET_CENTER")) {
                boolean ok = false;
                if(getArg(getOptionalArgs()[1]) != null) {
                    try {
                        trials = Integer.parseInt(getArg(getOptionalArgs()[1]));
                    } catch(NumberFormatException e) {
                        trials = 20;
                    }
                }
                int maxVersuche = trials;
                while(!ok && trials >= 0 && !cancelled) {
                    try {
                        if(udInUse)
                            if (myDialog.isCancelled())
                                cancelled = true;
                        mySerial.transferData("CPOSCENT;");
                        if(mySerial.receiveData(2).equalsIgnoreCase("RPOSCENT;"))
                            ok = true;
                        else
                            trials--;
                    } catch(DeviceIOException e) {}
                }
                if(!ok) {
                    result = new Ergebnis("TARGETTOCENTER", "SERIALCONTROL", "", "", "", "ORDERRECEIVED", "NOK", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                    status = STATUS_EXECUTION_ERROR;
                    ergListe.add(result);
                }
                else {
                    result = new Ergebnis("TARGETTOCENTER", "SERIALCONTROL", "", "", "", "ORDERRECEIVED", "OK", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_IO);
                    ergListe.add(result);
                    ok = false;
                    String received = new String("");
                    while(!ok && trials >= 0 && !cancelled) {
                        try {
                            if(udInUse)
                                if (myDialog.isCancelled())
                                    cancelled = true;
                            mySerial.transferData("CPOSSTAT;");
                            received = mySerial.receiveData(2);
                            if(received.equalsIgnoreCase("DPC;"))
                                ok = true;
                            else
                                trials--;
                        } catch(DeviceIOException e) {}
                    }
                    if(!ok) {
                        result = new Ergebnis("TARGETTOCENTER", "SERIALCONTROL", "", "", "", "TARGETCENTERED", "NOK", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                        status = STATUS_EXECUTION_ERROR;
                    }
                    else {
                        result = new Ergebnis("TARGETTOCENTER", "SERIALCONTROL", "", "", "", "TARGETCENTERED", "OK", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_IO);
                        status = STATUS_EXECUTION_OK;
                    }
                }
            }
            
            // Reset durchfuehren --> Zentrierung oeffnen und Target in Home Position verfahren
            // 20 Sekunden warten (20*2*500ms)
            // --------------------------------------------------
            else if(getArg(getRequiredArgs()[0]).equalsIgnoreCase("RESET")) {
                boolean ok = false;
                if(getArg(getOptionalArgs()[1]) != null) {
                    try {
                        trials = Integer.parseInt(getArg(getOptionalArgs()[1]));
                    } catch(NumberFormatException e) {
                        trials = 20;
                    }
                }
                int maxVersuche = trials;
                while(!ok && trials >= 0 && !cancelled) {
                    try {
                        if(udInUse)
                            if (myDialog.isCancelled())
                                cancelled = true;
                        mySerial.transferData("CCENTOPEN;");
                        if(mySerial.receiveData(2).equalsIgnoreCase("RCENTOPEN;"))
                            ok = true;
                        else
                            trials--;
                    } catch(DeviceIOException e) {}
                }
                if(!ok) {
                    result = new Ergebnis("RESET", "SERIALCONTROL", "", "", "", "ORDERRECEIVED", "NOK", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                    status = STATUS_EXECUTION_ERROR;
                    ergListe.add(result);
                }
                else {
                    result = new Ergebnis("RESET", "SERIALCONTROL", "", "", "", "ORDERRECEIVED", "OK", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_IO);
                    ergListe.add(result);
                    ok = false;
                    String received = new String("");
                    while(!ok && trials >= 0 && !cancelled) {
                        try {
                            if(udInUse)
                                if (myDialog.isCancelled())
                                    cancelled = true;
                            mySerial.transferData("CCENTSTAT;");
                            received = mySerial.receiveData(2);
                            if(received.equalsIgnoreCase("DCO;"))
                                ok = true;
                            else
                                trials--;
                        } catch(DeviceIOException e) {}
                    }
                    if(!ok) {
                        result = new Ergebnis("RESET", "SERIALCONTROL", "", "", "", "CENTRATION", "NOTOPEN", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                        status = STATUS_EXECUTION_ERROR;
                    }
                    else {
                        result = new Ergebnis("RESET", "SERIALCONTROL", "", "", "", "CENTRATION", "OPEN", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_IO);
                        status = STATUS_EXECUTION_OK;
                        ok = false;
                        received = new String("");
                        while(!ok && trials >= 0 && !cancelled) {
                            try {
                                if(udInUse)
                                    if (myDialog.isCancelled())
                                        cancelled = true;
                                mySerial.transferData("CPOSSTAT;");
                                received = mySerial.receiveData(2);
                                if(received.equalsIgnoreCase("DPH;"))
                                    ok = true;
                                else
                                    trials--;
                            } catch(DeviceIOException e) {}
                        }
                        if(!ok) {
                            result = new Ergebnis("RESET", "SERIALCONTROL", "", "", "", "TARGETPOSITION", "NOTHOME", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                            status = STATUS_EXECUTION_ERROR;
                        }
                        else {
                            result = new Ergebnis("RESET", "SERIALCONTROL", "", "", "", "TARGETPOSITION", "HOME", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_IO);
                            status = STATUS_EXECUTION_OK;
                        }
                    }
                }
            }
            
            // Scheinwerfer-Applikation aktiv setzen
            // 20 Sekunden warten (20*2*500ms)
            // --------------------------------------------------
            else if(getArg(getRequiredArgs()[0]).equalsIgnoreCase("SET_SW_ACTIVE")) {
                boolean ok = false;
                if(getArg(getOptionalArgs()[1]) != null) {
                    try {
                        trials = Integer.parseInt(getArg(getOptionalArgs()[1]));
                    } catch(NumberFormatException e) {
                        trials = 20;
                    }
                }
                int maxVersuche = trials;
                while(!ok && trials >= 0 && !cancelled) {
                    try {
                        if(udInUse)
                            if (myDialog.isCancelled())
                                cancelled = true;
                        mySerial.transferData("CSWACTIVE;");
                        if(mySerial.receiveData(2).equalsIgnoreCase("RSWACTIVE;"))
                            ok = true;
                        else
                            trials--;
                    } catch(DeviceIOException e) {}
                }
                if(!ok) {
                    result = new Ergebnis("SETSWACTIVE", "SERIALCONTROL", "", "", "", "SWACTIVE", "NOK", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                    status = STATUS_EXECUTION_ERROR;
                }
                else {
                    result = new Ergebnis("SETSWACTIVE", "SERIALCONTROL", "", "", "", "SWACTIVE", "OK", "", "", new Integer(maxVersuche-trials).toString(), "", "", "", "", "", Ergebnis.FT_IO);
                    status = STATUS_EXECUTION_OK;
                }
                ergListe.add(result);
            }
            
            else {
                result = new Ergebnis("EXECFEHLER", "PRUEFPROZEDUR", "JOB: " + getArg(getRequiredArgs()[0]), "", "", "JOB", "NOTFOUND", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                throw new PPExecutionException();
            }
            
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            result = new Ergebnis("EXECFEHLER", "PRUEFPROZEDUR", "", "", "", "EXCEPTION", "", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS);
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
            result = new Ergebnis("EXECFEHLER", "PRUEFPROZEDUR", "", "", "", "THROWABLE", "", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS);
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
        // Wurde der Prozess ueber den User Dialog abgebrochen
        if(cancelled) {
            result = new Ergebnis("USERABORT", "USERDIALOG", "JOB: " + getArg(getRequiredArgs()[0]), "", "", "EXECUTION", "ABORTED", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS);
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
        // Abbruch aufgrund von Zeit�berschreitung
        if(trials < 0) {
            result = new Ergebnis("SYSTEMABORT", "SERIALCONTROL", "JOB: " + getArg(getRequiredArgs()[0]), "", "", "TIME", "EXPIRED", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS);
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
        // Freigabe der benutzten Devices
        if( udInUse == true ) {
            try {
                myDialog.setVisible(false);
                myDialog.close();
                getPr�flingLaufzeitUmgebung().releaseUserDialog();
            } catch (Exception e) {
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis("EXECFEHLER", "USERDIALOG", "JOB: " + getArg(getRequiredArgs()[0]), "", "", "RELEASEUSERDIALOG", "DEVICELOCKED", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                else
                    result = new Ergebnis("EXECFEHLER", "USERDIALOG", "JOB: " + getArg(getRequiredArgs()[0]), "", "", "RELEASEUSERDIALOG", "EXCEPTION", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                ergListe.add(result);
                status = STATUS_EXECUTION_ERROR;
            }
        }
        if( mySerial != null ) {
            try {
                getPr�flingLaufzeitUmgebung().getDeviceManager().releaseSerialControl();
            } catch (Exception e) {
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis("EXECFEHLER", "SERIALCONTROL", "JOB: " + getArg(getRequiredArgs()[0]), "", "", "RELEASESERIALCONTROL", "DEVICELOCKED", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                else
                    result = new Ergebnis("EXECFEHLER", "SERIALCONTROL", "JOB: " + getArg(getRequiredArgs()[0]), "", "", "RELEASESERIALCONTROL", "EXCEPTION", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS);
                ergListe.add(result);
                status = STATUS_EXECUTION_ERROR;
            }
            mySerial = null;
        }
        
        /** Ergebnisstatus setzen
         */
        setPPStatus( info, status, ergListe );
        /** Attribut fuer serialControl */
        mySerial = null;
        
        /** Attribut fuer den Auftrag */
        sAuftrag = null;
        
    } // End of execute()
    
} // End of Class

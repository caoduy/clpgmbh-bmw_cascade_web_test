package com.bmw.cascade.pruefprozeduren;

import java.util.Arrays;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceIOException;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementierung des Video VideoTest E65
 * 
 * @author Mueller, Rainer Merz (rm), Frank Weber(fw), Michael Baum(mb)
 * @version 19_0_F fw live version of 18_0_T
 * @version 18_0_F ib added information text (AWT) for video test
 * @version 17_0_F mb bugfix for L6: results may be double, not integer
 * @version 16_0_F fw productive version of version 13
 * @version 15_0_T 02.11.06 fw parameter HYBRID removed, added parameters for setting jobnames, parameters and results
 * @version 14_0_F fw productive version of version 13
 * @version 13_0_T 12.12.05 fw switch to ediabas parallel device, parameter HYBRID added
 * @version 12_0_F fw productive version of version 11
 * @version 11_0_T fw smaller variable fixes
 * @version 0.1.0 fw FA vesrion for 0.0.9
 * @version 0.0.9Test fw optionale Parameter hinzu, um Antenne zu benennen,Kanal und Land zu waehlen ;
 *          check auf "VIDHIBUS" entfernt, Diagnose_Ende hinzu
 * @version 0.0.8 fw Korrektur VM-Hybrid Pr�fung Ergebnis 3.Antenne
 * @version 0.0.7 rm Korrektur VM-Hybrid Pr�fung
 * @version 0.0.6 fw Einfuegen des VM-Hybrid inkl. opt.Parameter fuer dritte Ant. (Drive)
 *          Zusaetzlicher Parameter fuer Anzahl der Messungen
 */
public class SDiagVideoTest_19_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	// language
	private final boolean isEnglish = System.getProperty( "user.language" ).equalsIgnoreCase( "EN" );

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public SDiagVideoTest_19_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public SDiagVideoTest_19_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName,
			Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialisiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "SOLL_MIN_H", "ANZAHL_MESSUNGEN", 
						  "NAME_ANTENNE_L", "NAME_ANTENNE_R", "NAME_ANTENNE_H",
						  "TVNORM", "TVCHANNEL", "PAUSES_TIME",
						  "JOBNAME_ANTENNE_L","JOBNAME_ANTENNE_R","JOBNAME_ANTENNE_H",
						  "JOBPAR_ANTENNE_L","JOBPAR_ANTENNE_R","JOBPAR_ANTENNE_H",
						  "JOBRESULT_ANTENNE_L","JOBRESULT_ANTENNE_R","JOBRESULT_ANTENNE_H","AWT"};
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "SGBD", "SOLL_MIN_L", "SOLL_MIN_R" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	public boolean checkArgs() {
		return super.checkArgs();
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		EdiabasProxyThread ed = null;

		// spezifische Variablen
		String sgbd = "";
		String job = "";
		String jobpar = "";
		String temp = "";

		String sArg = "";
		int anzahlAntennen;
		//boolean isVmHybrid;
		int anzahlMessungen = 4;
		
		int sollMin[] = {0,0,0};
		int Max[] = {0,0,0};
		int Value[][] = {new int[4],new int[4],new int[4]};
		
		boolean udInUse = false;

		/*int sollMinLeft, sollMinRight, leftMax, rightMax;
		int sollMinRear = 0;
		int rearMax = 0;

		int leftValue[];
		int rightValue[];
		int rearValue[];*/
		
        UserDialog myDialog = null;
		
        boolean useAWT = true;
		String myAWT = "";
		if (isEnglish) myAWT = "Video field strength test running";
		else myAWT = "Video Feldst�rkemessung l�uft";

		String nameAntenne[] = {"MindestWert1",
								"MindestWert2",
								"MindestWert3"};
		
		String jobAntenne[] = {"STEUERN_ANTENNE1_FELDSTAERKE",
							   "STEUERN_ANTENNE2_FELDSTAERKE",
							   "STEUERN_ANTENNE_FELDSTAERKE"};
		
		String jobParAntenne[] = {"ARG;STEUERN_ANTENNE_FELDSTAERKE;RCTECU;1",
								  "ARG;STEUERN_ANTENNE_FELDSTAERKE;RCTECU;2",
								  "ARG;STEUERN_ANTENNE_FELDSTAERKE;RCTECU;3"};
		
		String jobResultAntenne[] = {"STAT_FELDSTAERKE_ANTENNE_WERT",
									 "STAT_FELDSTAERKE_ANTENNE_WERT",
									 "STAT_FELDSTAERKE_ANTENNE_WERT"};

		String tvChannel = "";
		String tvNorm = "";
		long pauseTime = 1000;

		try {

			/*
			 * Pr�fen ob Argumente stimmen
			 */

			if( checkArgs() == false ) {
				result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			try {
				// Argumente Auswerten
				sgbd = getArg( "SGBD" );

				// standardeinstellungen f�r altes Hybridmodul
				if( sgbd.equalsIgnoreCase( "VID_65_2" ) ) {
					jobAntenne[0] = "STEUERN_ANTENNE_FELDSTAERKE";
					jobAntenne[1] = "STEUERN_ANTENNE_FELDSTAERKE";
					jobAntenne[2] = "STEUERN_ANTENNE_FELDSTAERKE";
					jobParAntenne[0] = "1";
					jobParAntenne[1] = "2";
					jobParAntenne[2] = "3";
					jobResultAntenne[0] = "STAT_FELDSTAERKE_ANTENNE_WERT";
					jobResultAntenne[1] = "STAT_FELDSTAERKE_ANTENNE_WERT";
					jobResultAntenne[2] = "STAT_FELDSTAERKE_ANTENNE_WERT";
				}

				// standardeinstellungen f�r altes analogmodul
				if( sgbd.equalsIgnoreCase( "VID_E65" ) ) {
					jobAntenne[0] = "STEUERN_ANTENNE1_FELDSTAERKE";
					jobAntenne[1] = "STEUERN_ANTENNE2_FELDSTAERKE";
					jobAntenne[2] = "STEUERN_ANTENNE_FELDSTAERKE";
					jobParAntenne[0] = "";
					jobParAntenne[1] = "";
					jobParAntenne[2] = "";
					jobResultAntenne[0] = "STAT_FELDSTAERKE_ANTENNE1_WERT";
					jobResultAntenne[1] = "STAT_FELDSTAERKE_ANTENNE2_WERT";
					jobResultAntenne[2] = "STAT_FELDSTAERKE_ANTENNE_WERT";
				}

				

				sollMin[0] = Integer.parseInt( getArg( "SOLL_MIN_L" ) );
				sollMin[1] = Integer.parseInt( getArg( "SOLL_MIN_R" ) );

				// optionales Argument fuer Drive pruefen
				sArg = getArg( "SOLL_MIN_H" );
				if( (!(sArg == null || sArg.equalsIgnoreCase( "" ) || sArg.equalsIgnoreCase( "null" ))) && 
					 !(sgbd.equalsIgnoreCase( "VID_E65" )) ) {
					anzahlAntennen = 3;
					sollMin[2] = Integer.parseInt( sArg );
				} else
					anzahlAntennen = 2;

				// anzahl der messungen festlegen (default:4)
				sArg = getArg( "ANZAHL_MESSUNGEN" );
				if( !(sArg == null || sArg.equalsIgnoreCase( "" ) || sArg.equalsIgnoreCase( "null" )) ) {
					int iMessungen = Integer.parseInt( sArg );
					if( iMessungen > 0 )
						anzahlMessungen = iMessungen;
				}

				// country norm
				sArg = getArg( "TVNORM" );
				if( !(sArg == null || sArg.equalsIgnoreCase( "" ) || sArg.equalsIgnoreCase( "null" )) ) {
					tvNorm = sArg;
				}

				// tv channel
				sArg = getArg( "TVCHANNEL" );
				if( !(sArg == null || sArg.equalsIgnoreCase( "" ) || sArg.equalsIgnoreCase( "null" )) ) {
					int iParse = Integer.parseInt( sArg );
					if( iParse > 0 )
						tvChannel = sArg;
				}

				// pause time
				sArg = getArg( "PAUSES_TIME" );
				if( !(sArg == null || sArg.equalsIgnoreCase( "" ) || sArg.equalsIgnoreCase( "null" )) ) {
					int iParse = Integer.parseInt( sArg );
					if( iParse > 0 )
						pauseTime = iParse;
				}
				
				// jobname first antenna
				sArg = getArg( "JOBNAME_ANTENNE_L" );
				if( !(sArg == null || sArg.equalsIgnoreCase( "" ) || sArg.equalsIgnoreCase( "null" )) ) {
					jobAntenne[0] = sArg;
				}
				// jobname second antenna
				sArg = getArg( "JOBNAME_ANTENNE_R" );
				if( !(sArg == null || sArg.equalsIgnoreCase( "" ) || sArg.equalsIgnoreCase( "null" )) ) {
					jobAntenne[1] = sArg;
				}
				// jobname third antenna
				sArg = getArg( "JOBNAME_ANTENNE_H" );
				if( !(sArg == null || sArg.equalsIgnoreCase( "" ) || sArg.equalsIgnoreCase( "null" )) ) {
					jobAntenne[2] = sArg;
				}
				
				// jobpar first antenna
				sArg = getArg( "JOBPAR_ANTENNE_L" );
				if( !(sArg == null || sArg.equalsIgnoreCase( "" ) || sArg.equalsIgnoreCase( "null" )) ) {
					jobParAntenne[0] = sArg;
				}
				// jobpar second antenna
				sArg = getArg( "JOBPAR_ANTENNE_R" );
				if( !(sArg == null || sArg.equalsIgnoreCase( "" ) || sArg.equalsIgnoreCase( "null" )) ) {
					jobParAntenne[1] = sArg;
				}
				// jobpar third antenna
				sArg = getArg( "JOBPAR_ANTENNE_H" );
				if( !(sArg == null || sArg.equalsIgnoreCase( "" ) || sArg.equalsIgnoreCase( "null" )) ) {
					jobParAntenne[2] = sArg;
				}
				
				// jobResult first antenna
				sArg = getArg( "JOBRESULT_ANTENNE_L" );
				if( !(sArg == null || sArg.equalsIgnoreCase( "" ) || sArg.equalsIgnoreCase( "null" )) ) {
					jobResultAntenne[0] = sArg;
				}
				// jobResult second antenna
				sArg = getArg( "JOBRESULT_ANTENNE_R" );
				if( !(sArg == null || sArg.equalsIgnoreCase( "" ) || sArg.equalsIgnoreCase( "null" )) ) {
					jobResultAntenne[1] = sArg;
				}
				// jobResult third antenna
				sArg = getArg( "JOBRESULT_ANTENNE_H" );
				if( !(sArg == null || sArg.equalsIgnoreCase( "" ) || sArg.equalsIgnoreCase( "null" )) ) {
					jobResultAntenne[2] = sArg;
				}
				
				sArg = getArg( "AWT" );
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					myAWT=sArg;
					if (myAWT.equalsIgnoreCase("IGNORE")) useAWT=false;
				}

				// Felder erstellen
				Value[0] = new int[anzahlMessungen];
				Value[1] = new int[anzahlMessungen];
				Value[2] = new int[anzahlMessungen];
				
			} catch( Exception e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			if (useAWT) {
				myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
				myDialog.displayMessage(PB.getString( "anweisung" ),myAWT, -1 );
				udInUse = true;
			}

			try {
				ed = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();// new
			} catch( Exception e1 ) {
				if( isEnglish ) {
					throw new DeviceIOException( "error getting EDIABAS device: " + e1.getMessage() );
				} else {
					throw new DeviceIOException( "Fehler beim Holen des Device EDIABAS: " + e1.getMessage() );
				}
			}

			/*
			 * Einstellung Kanal und Land
			 */
			try {
				if( tvNorm.length() > 0 ) {
					job = "STEUERN_TVSETCOUNTRY";
					jobpar = tvNorm;
					temp = ed.executeDiagJob( sgbd, job, jobpar, "" );
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					} else {
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
					// "Kleine Pause notwendig"
					try {
						Thread.sleep( pauseTime );
					} catch( Exception e ) {
					}
				}
				if( tvChannel.length() > 0 ) {
					job = "STEUERN_TVSETCHANNEL";
					jobpar = tvChannel;
					temp = ed.executeDiagJob( sgbd, job, jobpar, "" );
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					} else {
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
					// "Kleine Pause notwendig"
					try {
						Thread.sleep( pauseTime );
					} catch( Exception e ) {
					}
				}

			} catch( ApiCallFailedException e ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			/*
			 * Messung der Feldst�rken auf beiden Antennen mit 4 Wiederholungen
			 */
			for( int i = 0; i < anzahlMessungen; i++ ) {
				for( int j = 0; j < anzahlAntennen; j++ ) {
					job = jobAntenne[j];
					jobpar = jobParAntenne[j];
					try {
						temp = ed.executeDiagJob( sgbd, job, jobpar, "" );
						if( temp.equals( "OKAY" ) == false ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						} else {
							//--> mba 2008-01-09: Fehler in der L6: Das Ergebnis ist double!
							//Integer ist falsch!
							//Value[j][i] = Integer.parseInt( ed.getDiagResultValue( jobResultAntenne[j] ) );
							Value[j][i] = (int) Double.parseDouble( ed.getDiagResultValue( jobResultAntenne[j] ) );
							//<-- mba
							result = new Ergebnis( "Diagnose Messung Antenne "+j+" - Nr. "+i, "EDIABAS", sgbd, job, jobpar, jobResultAntenne[j], "" + Value[j][i], "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						}
					} catch( ApiCallFailedException e ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					} catch( EdiabasResultNotFoundException e ) {
						if( e.getMessage() != null )
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
						else
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						throw new PPExecutionException();
					}
				}
				// diagnose_ende
				if( tvChannel.length() > 0 || tvNorm.length() > 0 ) {
					job = "DIAGNOSE_ENDE";
					jobpar = "";
					try {
						temp = ed.executeDiagJob( sgbd, job, jobpar, "" );
						if( temp.equals( "OKAY" ) == false ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						} else {
							result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						}
					} catch( ApiCallFailedException e ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					} catch( EdiabasResultNotFoundException e ) {
						if( e.getMessage() != null )
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
						else
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						throw new PPExecutionException();
					}
				}
			}

			/*
			 * Analyse von Ergebnissen: Sort array into ascending numerical order. take highest and
			 * check against Soll_Mindest_Max
			 */
			try {
				Arrays.sort( Value[0] );
				Arrays.sort( Value[1] );
				Max[0] = Value[0][anzahlMessungen - 1];
				Max[1] = Value[1][anzahlMessungen - 1];
				if( anzahlAntennen > 2 ) {
					Arrays.sort( Value[2] );
					Max[2] = Value[2][anzahlMessungen - 1];
				}
			} catch( Exception e ) {
				result = new Ergebnis( "AnalyseFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Ergebnisbewertung
			for( int j = 0; j < anzahlAntennen; j++ ) {
				if( Max[j] >= sollMin[j] )
					result = new Ergebnis( "Analyse", nameAntenne[j], "", "", "", nameAntenne[j], Integer.toString( Max[j] ), Integer.toString( sollMin[j]), "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				else {
					result = new Ergebnis( "Analyse", nameAntenne[j], "", "", "", nameAntenne[j], Integer.toString( Max[j] ), Integer.toString( sollMin[j] ), "", "0", "", "", "", PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
					status = STATUS_EXECUTION_ERROR;
				}
				ergListe.add( result );
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			e.printStackTrace();
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}
		
		//Freigabe der benutzten Devices
        if( udInUse == true ) {
            try {
                getPr�flingLaufzeitUmgebung().releaseUserDialog();
            } catch (Exception e) {
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Exception: "+e.getMessage(), Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                status = STATUS_EXECUTION_ERROR;
            }
        }

		setPPStatus( info, status, ergListe );
	} // End of execute()

} // End of Class


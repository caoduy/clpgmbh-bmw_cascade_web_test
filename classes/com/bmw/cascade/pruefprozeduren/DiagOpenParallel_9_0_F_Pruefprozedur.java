/*
 * DiagOpenParallel_x_y_F_Pruefprozedur.java Created on 10.01.08
*/
package com.bmw.cascade.pruefprozeduren;

import java.io.File;
import java.util.Vector;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceInfo;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.appframework.logging.*;


/**
 * Holt sich EDIABAS und baut in Abh�ngigkeit vom verwendeten Interface die Diagnoseanbindung auf.
 * 
 * @author Baum, Wolf, Buboltz
 * @version <br>
 *          16.09.2008 MB  1_0_F �bernahme von DiagOpen_41_0_F_Pruefprozedur und Erweiterung um Paralleldiagnose		
 *          29.07.2014 CW  6_0_T Parametergewinnnung angepasst, sodass Parameter nur �ber CASCADE Standard-Methoden geholt werden
 *          29.07.2014 CW  7_0_F Freigabe 6_0_T	
 *          24.08.2016 TB  8_0_F Logausgabe korrigiert, Generics + Compilerwarnungen <BR>	
 *          24.08.2016 TB  9_0_F F-Version <BR>	
 */
public class DiagOpenParallel_9_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	/** <code>serialVersionUID</code> */
	static final long serialVersionUID = 1L;
	
	//static final String PARALLEL = "PARALLEL";
	//static final String TAG = "TAG";

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagOpenParallel_9_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DiagOpenParallel_9_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 * 
	 * @see com.bmw.cascade.pruefprozeduren.Pruefprozedur#getOptionalArgs()
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "EXTERN", "INIT", "TIMEOUT", "AUTOMODE", "DIAGMODE", "MDAEXT", "DEBUG", "STOP_ON_MARRIAGE_FAILURE", "FENSTERNAME", "TIMEOUT2", "TAG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 * 
	 * @see com.bmw.cascade.pruefprozeduren.Pruefprozedur#getRequiredArgs()
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "FZS","PARALLEL" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 * 
	 * @see com.bmw.cascade.pruefprozeduren.Pruefprozedur#checkArgs()
	 */
	@Override
	public boolean checkArgs() {

		try {
			return super.checkArgs();
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @see com.bmw.cascade.pruefprozeduren.Pruefprozedur#execute(com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo)
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
        String strFensterName = "";
		String mdaNummer = null;
		String temp = null;
		String mdaExt = null;          // initialisiere Hilfsvariable fuer den externen Parameter fuer MDA Nummer
		boolean stopOnMarriageFailure = false;  // sollte die Verheiratung nicht klappen, wird bei true sofort abgebrochen
		boolean leaveWhile = false;  // Hilfsvariable f�r das Verlassen der do - while Schleife 
		boolean udInUse = false;
		boolean initOK, startOK;
		boolean autoInitTried = false; // Automatische Bestimmung ob P2P- oder Linienmodus
		boolean autoInitSuccessful = false;
		boolean extern = false;
		boolean marriage = false;
		boolean parallel = false;
		int key;
		int messageTimeout = 0;
		int messageTimeout2 = 0;
		int automode = 0;
		int diagmode = 1;
		
		/** Flag; Sollen Debug-Infos geschrieben werden				*/
		boolean Debug = false;
		
		EdiabasProxyThread ediabas = null;
		EdiabasProxyThread ept[] = null;
		boolean simulation_confirmed = false;
		String tag = null;
		
        /***********************************************
         * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
         * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
         ***********************************************/
        try {
            UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
            myAnalyser.LogSetTestStepName(this.getName());
            getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
        } catch (Exception e) {
            try {
                getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
            }
            catch (Exception e1) {}
        }  

		try {
			try {
				// Parameter holen
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				
				//Parallelmode an/aus
				parallel = Boolean.valueOf(getArg("PARALLEL")).booleanValue();
				
				//Tag
                if ( getArg("TAG") != null ) {
                    tag = getArg("TAG");
                }
				
				// DEBUG ON ?
				if( getArg( getOptionalArgs()[6] ) != null && getArg( getOptionalArgs()[6]).equalsIgnoreCase( "DEBUGON" )) {
					Debug = true;
					if(Debug) System.out.println("\nDiagOpenParallel PP Debug = ON");
				}
				if( getArg( getOptionalArgs()[0] ) != null ) {
					if( getArg( getOptionalArgs()[0] ).equalsIgnoreCase( "TRUE" ) )
						extern = true;
					if(Debug) System.out.println("DiagOpenParallel PP EXTERN = " + extern);
				}
				if( getArg( getOptionalArgs()[1] ) != null ) {
					if( getArg( getOptionalArgs()[1] ).equalsIgnoreCase( "TRUE" ) )
						marriage = true;
					if(Debug) System.out.println("DiagOpenParallel PP INIT: " + marriage);
				}
				if( getArg( getOptionalArgs()[2] ) != null ) {
					messageTimeout = Integer.parseInt( getArg( getOptionalArgs()[2] ) );
					if(Debug) System.out.println("DiagOpenParallel PP TIMEOUT: " + messageTimeout);
				}
				if( getArg( getOptionalArgs()[3] ) != null ) {
					automode = Integer.parseInt( getArg( getOptionalArgs()[3] ) );
					if(Debug) System.out.println("DiagOpenParallel PP AUTOMODE: " + automode);
				}
				if( getArg( getOptionalArgs()[4] ) != null ) {
					diagmode = Integer.parseInt( getArg( getOptionalArgs()[4] ) );
					if(Debug) System.out.println("DiagOpenParallel PP DIAGMODE: " + diagmode);
				}
				if( getArg( getOptionalArgs()[5] ) != null ) {
					mdaExt = extractValues( getArg( getOptionalArgs()[5] ) )[0];
					if(Debug) System.out.println("DiagOpenParallel PP MDAEXT: " + mdaExt);
				}
				if( getArg( getOptionalArgs()[7] ) != null ) {
 					if( getArg( getOptionalArgs()[7] ).equalsIgnoreCase( "TRUE" ) )
 						stopOnMarriageFailure = true;
 					if(Debug) System.out.println("DiagOpenParallel PP STOP_ON_MARRIAGE_FAILURE: " + stopOnMarriageFailure);
				}
				if( getArg( getOptionalArgs()[8] ) != null ) {
					//Neu: Fenster-Name als optionales Argument
   				    strFensterName = getArg(getOptionalArgs()[8] );
					if(Debug) System.out.println("DiagOpenParallel PP FENSTERNAME: " + strFensterName);
				}
	            if( getArg( getOptionalArgs()[9] ) != null ) {
	            	//Neu: 2.Timeout-Parameter
					messageTimeout2 = Integer.parseInt( getArg( getOptionalArgs()[9] ) );
					if(Debug) System.out.println("DiagOpenParallel PP TIMEOUT2: " + messageTimeout2);
				}				
			} catch( Exception e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			
			
			
			try {
				// Auf Nummer sicher: erst mal Freigabe
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseEdiabasParallel_ALL();
				if(Debug) System.out.println("DiagOpenParallel PP releaseEdiabasParallel_ALL()");
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseEdiabas();
				if(Debug) System.out.println("DiagOpenParallel PP releaseEdiabas()");
				
				if (parallel) {
				
					//Pr�fstand auf Parallelbetrieb schalten
					getPr�flingLaufzeitUmgebung().setPruefstandVariable(VariableModes.TEMPORARY, "DIAG_PARALLEL", Boolean.TRUE );
					
					ept = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabasParallel_ALL();
					if (ept==null || ept.length < 1){
					   // kein Device f�r Paralleldiagnose gefunden
						result = new Ergebnis( "ExecFehler", "EDIABAS_PARALLEL", "", "", "", "", "", "", "", "0", "", "", "", "Exception: No EdiabasParallel device available", "", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						throw (new Exception ("No EdiabasParallel device available!"));
					}
				}
				else{
					//Pr�fstand auf Sequentiellbetrieb schalten
					getPr�flingLaufzeitUmgebung().setPruefstandVariable(VariableModes.TEMPORARY, "DIAG_PARALLEL", Boolean.FALSE );
					ept = new EdiabasProxyThread[]{getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas(tag)};
				}
				
				for (int i = 0; i < ept.length; i++){
					ediabas = ept[i];
					try {
						// Auf Nummer sicher: erst mal Freigabe
						//ediabas.apiEnd();
						//if(Debug) System.out.println("DiagOpenParallel PP apiEnd()");
						// Initialisierung
						ediabas.apiInitAuto();
						if(Debug) System.out.println("DiagOpenParallel PP apiInit");
					} catch( NoClassDefFoundError e ) {
						result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Exception: EDIABAS DLL (apijav32.dll)", "", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
					}

					// Simulationsmodus???
					if( (ediabas.apiGetConfig( "Simulation" )).equals( "0" ) == false && simulation_confirmed == false ) {
						if(Debug) System.out.println("DiagOpenParallel PP Simulationsmodus aktiviert");
						if (strFensterName == null)
						{
							key = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputDigital( PB.getString( "Diagnose" ), PB.getString( "diagnoseInSimulation" ) + "! " + PB.getString( "abbruchDurchWerker" ), messageTimeout );
						}  
						else
						{
							key = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputDigital( strFensterName , PB.getString( "diagnoseInSimulation" ) + "! " + PB.getString( "abbruchDurchWerker" ), messageTimeout );
						}  
						udInUse = true;
						if( key != UserDialog.NO_KEY ) {
							result = new Ergebnis( "Simulation", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnoseInSimulation" ), PB.getString( "abbruchDurchWerker" ), Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						}
						simulation_confirmed = true;
					}

					// M�ssen Traces aktiviert werden?
					try {
						// API-Trace?
						DeviceInfo apiTrace = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabasApiTrace();
						try {
							ediabas.apiSetConfig( "ApiTrace", apiTrace.getPort() );
							if( extern == false ) {
								File traceFile = new File( ediabas.apiGetConfig( "TracePath" ), "api.trc" );
								if( traceFile.exists() )
									traceFile.delete();
								if(Debug) System.out.println("DiagOpenParallel PP ApiTrace aktiviert");
							}
						} catch( Exception acfe ) {
							LogManager.getLogger().log(LogLevel.WARNING, getPr�fling().getAuftrag().getFahrgestellnummer7() + " Exception API-Trace Level " + apiTrace.getPort() + ": ", acfe );
						}
					} catch( Exception e ) {
						// keine Fehlerbehandlung
					} catch( Throwable t ) {
						// keine Fehlerbehandlung
					}
					try {
						// IFH-Trace?
						DeviceInfo ifhTrace = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabasIfhTrace();
						try {
							ediabas.apiSetConfig( "IfhTrace", ifhTrace.getPort() );
							if( extern == false ) {
								File traceFile = new File( ediabas.apiGetConfig( "TracePath" ), "ifh.trc" );
								if( traceFile.exists() )
									traceFile.delete();
								if(Debug) System.out.println("DiagOpenParallel PP IfhTrace aktiviert");
							}
						} catch( Exception acfe ) {
							LogManager.getLogger().log(LogLevel.WARNING, getPr�fling().getAuftrag().getFahrgestellnummer7() + " Exception IFH-Trace Level " + ifhTrace.getPort() + ": ", acfe );
						}
					} catch( Exception e ) {
						// keine Fehlerbehandlung
					} catch( Throwable t ) {
						// keine Fehlerbehandlung
					}
				}

				// Interface-Bestimmung
				temp = ediabas.executeDiagJob( "UTILITY", "INTERFACE", "", "TYP" );
			
				if(Debug) System.out.println("DiagOpenParallel PP Interface-Bestimmung JobStatus: " + temp);
				
				if( temp.equals( "OKAY" ) == false ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", "UTILITY", "INTERFACE", "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );

					ergListe.add( result );
					throw new PPExecutionException();
				}
				temp = ediabas.getDiagResultValue( "TYP" ).toUpperCase();
				
				if(Debug) System.out.println("DiagOpenParallel PP Interface: " + temp);
				
				result = new Ergebnis( "Interface", "EDIABAS", "UTILITY", "INTERFACE", "", "TYP", temp, temp, "", "0", "", "", "", "", "", "K" );
				ergListe.add( result );
			} catch( ApiCallFailedException e ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", "UTILITY", "INTERFACE", "", "TYP", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", "UTILITY", "INTERFACE", "", "TYP", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", "UTILITY", "INTERFACE", "", "TYP", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			if( (temp.equals( "FUNK" ) == true) && (extern == false) ) {
				// Statistik Zaehler im Master l�schen
				try {
					temp = ediabas.executeDiagJob( "IFR", "LOESCHEN_STATISTIK_MASTER", "", "" );
					
					if(Debug) System.out.println("DiagOpenParallel PP JOB LOESCHEN_STATISTIK_MASTER: " + temp);
					
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "LOESCHEN_STATISTIK_MASTER", "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_IGNORE );
						ergListe.add( result );
					}
				} catch( ApiCallFailedException e ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "LOESCHEN_STATISTIK_MASTER", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_IGNORE );
					ergListe.add( result );
				} catch( Exception e ) {
					LogManager.getLogger().log(LogLevel.WARNING, "Funkverhau nicht in Ordnung!!! / Wrong version for radio adaptor software!!!", e );
				} catch( Throwable t ) {
					LogManager.getLogger().log(LogLevel.WARNING, "Funkverhau nicht in Ordnung!!! / Wrong version for radio adaptor software!!!", t );
				}

				// Wenn eine Verheiratung der Funkadapter gew�nscht wird, dann kann auf die
				// Initialisierung verzichtet werden, es wird direkt zur Eingabemaske f�r die
				// MDA-Nummer gesprungen
				if( marriage == false )
					initOK = true;
				else
					initOK = false;
				
				startOK = false;

				if(Debug) System.out.println("DiagOpenParallel PP (Initialisierung) initOK: " + initOK);
				
				do {
					if( initOK == true ) {
						// Aufbau der Funkverbindung
						try {
							temp = ediabas.executeDiagJob( "IFR", "START_PRUEFUNG", getArg( getRequiredArgs()[0] ), "JOB_STATUS" );
							if(Debug) System.out.println("DiagOpenParallel PP if( initOK == true ) --> JOB START_PRUEFUNG: " + temp + " FGNR: " + getArg( getRequiredArgs()[0]));
							
							if( temp.equals( "OKAY" ) == true )
								startOK = true;
							
							if(Debug) System.out.println("DiagOpenParallel PP if( initOK == true ) --> SET startOK: " + startOK);
							
						} catch( ApiCallFailedException e ) {
							startOK = false;
						} catch( EdiabasResultNotFoundException e ) {
							startOK = false;
						}
					}
					if( startOK == false ) {
						initOK = false;
						// Versuche erstmal, die Verbindungsart automatisch zu bestimmen -
						// allerdings nur einmal!
						// Wenn Verheiratung gew�nscht wird, ist das �berfl�ssig
						if( (marriage == false) && (autoInitTried == false) ) {
							autoInitTried = true;
							int repeatCounter = 0;
							try {
								do {
									temp = ediabas.executeDiagJob( "IFR", "STATUS_SLAVE_ZIELNUMMER", "", "" );
									if(Debug) System.out.println("DiagOpenParallel PP if( startOK == false ) --> JOB STATUS_SLAVE_ZIELNUMMER: " + temp);
									repeatCounter++;
									if( temp.equals( "OKAY" ) == true ) {
										mdaNummer = ediabas.getDiagResultValue( "STAT_SLAVE_ZIELNUMMER" );
										if(Debug) System.out.println("DiagOpenParallel PP if( startOK == false ) --> STAT_SLAVE_ZIELNUMMER: " + mdaNummer);
										result = new Ergebnis( "Diagnose", "EDIABAS", "IFR", "STATUS_SLAVE_ZIELNUMMER", "", "STAT_SLAVE_ZIELNUMMER", mdaNummer, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
										ergListe.add( result );
										if( mdaNummer.equalsIgnoreCase( "0" ) == false ) {
											// Punkt-zu-Punkt verbindung, somit OK da IFR-Jobs
											// komplett �berfl�ssig, aber Funk reagiert sich nicht
											// zum ersten Diagnose Aufruf...
											try {
												temp = ediabas.executeDiagJob( "UTILITY", "STATUS_ZUENDUNG", "", "JOB_STATUS" );
											} catch( Exception e ) {
												// Fehler ignorieren beim ersten Job!
											}
											startOK = true;
											autoInitSuccessful = true;
										} 
										if (mdaNummer.equalsIgnoreCase( "0" ) == true){
											if(Debug) System.out.println("DiagOpenParallel PP if( startOK == false ) --> STAT_SLAVE_ZIELNUMMER = 0 --> Abbruch Schleife");
											break;
											}
									} else {
										if( (repeatCounter < 2) && (startOK == false) )
											if(Debug) System.out.println("DiagOpenParallel PP if( startOK == false ) --> JOB STATUS_SLAVE_ZIELNUMMER trys: " + repeatCounter);
											Thread.sleep( 200 );
									}
								} while( (repeatCounter < 2) && (startOK == false));
							} catch( Exception e ) {
								result = new Ergebnis( "Exception", "EDIABAS", "IFR", "STATUS_SLAVE_ZIELNUMMER", "", "STAT_SLAVE_ZIELNUMMER", "", "", "", "0", "", "", "", e.getMessage(), "", Ergebnis.FT_IO );
								ergListe.add( result );
								e.printStackTrace();
							}
						}
						if( autoInitSuccessful == false ) {
							// Manueller Ablauf mit Eingabe durch den Benutzer
							try {
								// wenn MDA-Nummer von extern kommt, dann nutze diese ansonsten nehmen wir den alten - manuellen Weg �ber die UserEingabe
								if (strFensterName == null)
                                {
								   mdaNummer = (mdaExt != null) ? mdaExt : getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputNumber( PB.getString( "Diagnose" ), PB.getString( "eingabeDerMdaNummer" ), messageTimeout );
								}
								else
								{
								   mdaNummer = (mdaExt != null) ? mdaExt : getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputNumber( strFensterName , PB.getString( "eingabeDerMdaNummer" ), messageTimeout );
								}   
								if(Debug) System.out.println("DiagOpenParallel PP if( autoInitSuccessful == false ) --> Used MDA-Nummer for assignment: " + mdaNummer);
								result = new Ergebnis( "MDA_NR_INPUT", "EDIABAS", "", "", "", "MDA_NR_INPUT", mdaNummer, "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
								udInUse = true;
								if( mdaNummer == null ) {
									result = new Ergebnis( "MDA", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Autostart Timeout = " + messageTimeout + "s", PB.getString( "abbruchDurchWerker" ), Ergebnis.FT_NIO );
									ergListe.add( result );
									throw new PPExecutionException();
								}
								if( mdaNummer.equals( "" ) == true ) {
									// Abbruch?
		                            if (strFensterName == null)
                                    {
										key = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputDigital( PB.getString( "Diagnose" ), PB.getString( "abbruchDurchWerker" ), messageTimeout );
								    }
								    else 
								    {
										key = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputDigital( strFensterName , PB.getString( "abbruchDurchWerker" ), messageTimeout );
								    } 			
									if( key != UserDialog.NO_KEY ) {
										result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "INIT_IFR", getArg( getRequiredArgs()[0] ) + ";" + mdaNummer, "", "", "", "", "0", "", "", "", PB.getString( "funkKommunikation" ), PB.getString( "abbruchDurchWerker" ), Ergebnis.FT_NIO );
										ergListe.add( result );
										throw new PPExecutionException();
									}
								} else if( mdaNummer.equals( "0" ) == true ) {
									// Punkt-zu-Punkt verbindung, somit OK da IFR-Jobs komplett
									// �berfl�ssig, aber Funk reagiert sich nicht zum ersten
									// Diagnose Aufruf...
									try {
										temp = ediabas.executeDiagJob( "UTILITY", "STATUS_ZUENDUNG", "", "JOB_STATUS" );
									} catch( Exception e ) {
										// Fehler ignorieren beim ersten Job!
									}
									startOK = true;

								} else {
									// Verheiratung MDA mit FZS?
									try {
										temp = ediabas.executeDiagJob( "IFR", "INIT_IFR", getArg( getRequiredArgs()[0] ) + ";" + mdaNummer + ";" + automode + ";" + diagmode, "JOB_STATUS" );
										if(Debug) System.out.println("DiagOpenParallel PP if( autoInitSuccessful == false ) --> JOB INIT_IFR: " + temp + " MDA: " + mdaNummer + " FGNR: " +  getArg( getRequiredArgs()[0]));
										if( temp.equals( "OKAY" ) == true ) {
											initOK = true;
											if(Debug) System.out.println("DiagOpenParallel PP if( autoInitSuccessful == false ) --> SET initOK: " + initOK );
										} else if( mdaExt != null ) {
											if(Debug) System.out.println("DiagOpenParallel PP if( autoInitSuccessful == false ) --> Fehler bei externen �bergabe MDA-Nummer: " + mdaExt );
											// falls die externe MDA Nummer nicht funktioniert, dann dokumentieren wir das und werfen eine Exception
											result = new Ergebnis( "MDA", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "MDA Extern = " + mdaExt, PB.getString( "mda" ) + ", " + PB.getString( "parametrierfehler" ), Ergebnis.FT_NIO );
											ergListe.add( result );
											throw new PPExecutionException();
 										}else if (stopOnMarriageFailure == true){
 											if(Debug) System.out.println("keine Verbindung mit dem MDA m�glich, STOP_ON_MARRIAGE_FAILURE = true -> Abbruch " );
 											// falls die externe MDA Nummer nicht funktioniert, dann dokumentieren wir das und werfen eine Exception
 											leaveWhile=true;
 											status = STATUS_EXECUTION_ERROR;
 											result = new Ergebnis( "MDA", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "keine Verbindung zu MDA "+mdaNummer, "MDA nicht korrekt gesteckt bzw. falscher MDA", Ergebnis.FT_NIO );
 											ergListe.add( result );
										}
									} catch( ApiCallFailedException e ) {
										// Ignore
									} catch( EdiabasResultNotFoundException e ) {
										// Ignore
									}
								}
							} catch( Exception e ) {
								if( e instanceof PPExecutionException )
									throw new PPExecutionException();
								else if( e instanceof DeviceLockedException )
									result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
								else if( e instanceof DeviceNotAvailableException )
									result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								throw new PPExecutionException();
							}
						}
					}
					
					if (parallel) {
					
						//immer confirmed communication
						try{
							temp = ediabas.executeDiagJob( "IFR", "SET_CONFIRMED_COM", "1", "JOB_STATUS" );
							if( temp.equals( "OKAY" ) == true ) {
								if(Debug) System.out.println("DiagOpenParallel PP if( SET_CONFIRMED_COM = 1 ) --> OK!");
							}
							//else throw new PPExecutionException("SET_CONFIRMED_COM(1) failed!");
						}
						catch (Exception ex){
							if(Debug) System.out.println("DiagOpenParallel PP SET_CONFIRMED_COM(1) --> FAILED!");
							//result = new Ergebnis( "MDA", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "","PP SET_CONFIRMED_COM(1) --> FAILED!", Ergebnis.FT_NIO );
							LogManager.getLogger().log(LogLevel.WARNING, "PP SET_CONFIRMED_COM(1) --> FAILED!");
							//ergListe.add( result );
							//throw new PPExecutionException();
						}
					}
					
					
 					if(Debug) System.out.println("startOK: "+startOK+" leaveWhile: "+leaveWhile );
 				} while( startOK == false && leaveWhile == false);
			} // Ende Funk

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Alarmmeldung
		if( status != STATUS_EXECUTION_OK ) {
			String awt = PB.getString( "Diagnose" ) + " " + PB.getString( "nichtVerfuegbar" );
			try {
				if( udInUse == true )
					getPr�flingLaufzeitUmgebung().releaseUserDialog();
				udInUse = false;
				if (strFensterName == null)
                {
	   				getPr�flingLaufzeitUmgebung().getUserDialog().requestAlertMessage( PB.getString( "Diagnose" ), awt, messageTimeout2 );
	            }  
                else
                {
    				getPr�flingLaufzeitUmgebung().getUserDialog().requestAlertMessage( strFensterName, awt, messageTimeout2 );
                }  
				udInUse = true;
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
				udInUse = false;
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
			}
		}

		// Freigabe der benutzten Devices
		if( udInUse == true ) {
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		// Status setzen
		setPPStatus( info, status, ergListe );
	}

}

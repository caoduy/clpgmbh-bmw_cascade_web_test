package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;

/**
 * Diese Pr�fprozedur setzt die Pr�fstandsvariable auf einen
 * bestimmten Wert.
 *
 * @author BMW TP-430 C. Schumann; Gefasoft Engineering, TI-538, Peter Rettig
 * @version 1_0_F  CS	27.05.2009  Implementierung <br>
 * @version 2_0_F  CS	12.08.2009  Ablage der Variablen von PersistentPruefstandVariable in String ge�ndert <br>
 * @version 3_0_F  ??	10.09.2010  Erweiterung, sodass ohne Definition einer Pr�fstandsvariablen am Frontend die PP funktioniert <br>
 * @version 4_0_F  PR	31.08.2012  Zur�ckgehen auf den Stand der Version 2_0_F, da 3_0_F fehlerhaft (Reaktivierung der 2_0_F auf zentralen Server wg. vorheriger Bereinigung nicht m�glich gewesen, daher noch mal neue Version) <br>
 * @version 5_0_F  TB   17.03.2014  Erlaube bei den Parametern den Referenzoperator <br>
 * @version 6_0_F  TB   17.03.2014  F-Version <br>
 * @version 7_0_T  TB   03.02.2015  Variable selbst noch im virtuellen Fahrzeug speichern f�r weitere @-Operator Zugriffe (LOP 1891)<br>
 * @version 8_0_F  TB   04.02.2015  F-Version <br>
 * @version 9_0_T  MK   26.09.2017  Nicht benutzter Parameter Datatype entfernt <br>
 * @version 10_0_F  MK  26.09.2017  F-Version <br>
 * @version 11_0_T  MK  04.10.2017  Parametrierung des Parameter Datatype f�hrt nicht zu Fehlern w�hrend der Laufzeit -> kann nach der Umstellung des Pr�fwissens und der Aktivierung der Version 12 wieder entfernt werden <br>
 * @version 12_0_F  MK  04.10.2017  F-Version <br>  
 */
public class SetPSVariable_12_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public SetPSVariable_12_0_F_Pruefprozedur() {
	}

	/**
	   * erzeugt eine neue Pruefprozedur.
	   * @param pruefling Klasse des zugeh. Pr�flings
	   * @param pruefprozName Name der Pr�fprozedur
	   * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	   */
	public SetPSVariable_12_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();

	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "VARTYPE" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "NAME", "VALUE" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 */
	public boolean checkArgs() {
		boolean ok;
		try {
			ok = checkArgs_redesign();
			if( ok == true ) {
				//�berpr�fung requiredArgs schon in super.checkArgs()

				//varmode darf nur 1,2 oder 3 sein
				if( getArg( getOptionalArgs()[0] ) != null && (((Integer.parseInt( getArg( getOptionalArgs()[0] ) )) <= 0) || (Integer.parseInt( getArg( getOptionalArgs()[0] ) )) > 3) )
					return false;
			}
			return ok;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * Pr�ft soweit als m�glich, ob die gesetzten Argumente die Ausf�hrkriterien der Pruefprozedur
	 * erf�llen. An dieser Stelle nur �berpr�fung, ob alle zwingenden Argumente vorhanden sind und
	 * keine nicht zur Verwendung kommenden Argumente gesetzt wurden. Es findet keine Bewertung der
	 * Argumentwerte mit Ausnahme Leerstring statt.
	 * 
	 * @return true, falls die gesetzten Argumente die Ausf�hrkriterien der Pruefprozedur erf�llen,
	 *         ansonsten false
	 */
	public boolean checkArgs_redesign() {
		int i, j;
		boolean exist;
		// 1. Check: Sind alle requiredArgs gesetzt
		String[] requiredArgs = getRequiredArgs();
		for( i = 0; i < requiredArgs.length; i++ ) {
			if( getArg( requiredArgs[i].toUpperCase() ) == null )
				return false;
			if( getArg( requiredArgs[i].toUpperCase() ).equals( "" ) == true )
				return false;
		}
		// 2. Check: Sind �berfl�ssige Argumente gesetzt, d.h. Argumente, die weder required noch
		// optional sind
		Enumeration enu = getArgs().keys();
		String[] optionalArgs = getOptionalArgs();
		String givenkey;
		while( enu.hasMoreElements() ) {
			givenkey = (String) enu.nextElement();
			exist = false;
			j = 0;
			while( (exist == false) && (j < requiredArgs.length) ) {
				if( givenkey.equalsIgnoreCase( requiredArgs[j] ) == true )
					exist = true;
				j++;
			}
			
			if( givenkey.equalsIgnoreCase( "DATATYPE" ) == true ) //kann entfernt werden wenn Parameter in keinem PL mehr parametrisiert ist und jedes Werk auf min. Version 12 geupdatet hat.
				exist = true;									  //
			
			j = 0;
			while( (exist == false) && (j < optionalArgs.length) ) {
				if( givenkey.equalsIgnoreCase( optionalArgs[j] ) == true )
					exist = true;
				j++;
			}
			if( exist == false )
				return false;
			if( getArg( givenkey ).equals( "" ) == true )
				return false;
		}
		// Tests bestanden, somit ok
		return true;
	}
	
	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String name = "";
		String value = "";
		int varmode = 3; //Standard-Variablen-Typ: tempor�r, d.h. Variable ist nur w�hrend einer Pr�fung am PS g�ltig

		try {
			//Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				try {
					name = extractValues( getArg( getRequiredArgs()[0] ) )[0];
					value = extractValues( getArg( getRequiredArgs()[1] ) )[0];
				} catch( NumberFormatException e ) {
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				}
				if( getArg( getOptionalArgs()[0] ) != null ) {
					varmode = new Integer( getArg( getOptionalArgs()[0] ) ).intValue();
				}
			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//bestehenden Wert der Pr�fstandsvariable holen
			Object original_var = null;
			try {
				original_var = (Object) getPr�flingLaufzeitUmgebung().getPruefstandVariable( varmode, name );
			} catch( Exception var_e ) {
				original_var = "Variable not defined";
			}

			//Pr�fstandsvariable setzen
			getPr�flingLaufzeitUmgebung().setPruefstandVariable( varmode, name, /*var*/value );

			// Variable selbst noch speichern, f�r weitere Zugriffe z.B. �ber den @-Operator
			result = new Ergebnis( name, "SetPSVariable", name, "", varmode + "", "Ergebnis", value, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
			
			//Ergebnis speichern
			result = new Ergebnis( "SetPSVariable", "SetPSVariable", name, "", varmode + "", "Ergebnis", "changed from " + "(" + original_var + ")" + " to " + "(" + /*var*/value + ")", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );


		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		setPPStatus( info, status, ergListe );
	}
}

/*
 * DiagFunktional_x_y_Pruefprozedur.java
 *
 * Created on 03.09.04
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.Hashtable;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementierung einer Pr�fporozedur, die eine funktionalen Job ausf�hrt
 * @author Mueller, Rettig, K�ser, Sch�nert, Buboltz
 * @version           08.03.2012 TB LOP1488 Logik soll weiterlaufen, auch wenn ein SG EInzelfehler aufgetreten ist, Debugmodus eingebaut<BR>
 * @version           13.03.2012 PR Fehlendes "status = STATUS_EXECUTION_ERROR" an jenen Stellen erg�nzt, wo wir zwar NIO-Ergebnisse erzeugen, aber keine PPExecutionException werfen. Au�erdem wurde ein explizites "status = STATUS_EXECUTION_OK" entfernt, da dieses vorgelagerte Fehler aufhebt. Beides ist wichtig, wenn die PP in OK_ON_ERROR-Bl�cken enthalten ist, da die F-nach-A-Modifikation der Results in AbstractBMWPruefprozedur.setPPStatus nur dann vorgenomen wird, wenn die PP nicht mit STATUS_EXECUTION_OK endet.
 * @version		      26.03.2012 PR Aktivschaltung als F-Version
 * @version 18_0_T    05.02.2016 MKe Parameter TIMEOUT als globalen PP Timeout in Sekunden hinzugef�gt.
 * @version 19_0_F    10.05.2016 FS  Produktive Freigabe Version 18_0_T.  
 * @version 20_0_T    11.07.2016 TB  weiterer opt. Parameter: RESULTS_LIST, Results die im Resultset enthalten sind, werden unbewertet nach APDM dokumentiert (sofern dabei kein Fehler auftrat).<BR>              
 * @version 21_0_F    14.07.2016 TB  F-Version. <BR>
 * @version	22_0_T	  22.08.2016 TB  Erweiterungen f�r Paralleldiagnose analog zur DiagToleranzPP (LOP 2015), Generics eingebaut + Compiler Warnungen behoben<BR>
* @version	23_0_F	  25.01.2017 MKe F-Version. <BR>
 */
public class DiagFunktional_23_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagFunktional_23_0_F_Pruefprozedur() {
	}

	/**
	   * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	   * @param pruefling Klasse des zugeh. Pr�flings
	   * @param pruefprozName Name der Pr�fprozedur
	   * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	   */
	public DiagFunktional_23_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "JOBPAR", "AWT", "EXCLUDE_LIST", "INCLUDE_LIST", "NO_RETRY", "TIMEOUT", "RESULTS_LIST", "TAG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "SGBD", "JOB" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	@Override
	public boolean checkArgs() {
		return super.checkArgs();
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		Pruefling prueflinge[] = null; // alle Prueflinge im virtuellen Fahrzeug

		String temp = "";
		String sgbd = "";
		String job = "";

		String jobargs = null;
		String awt = null;
		String[] excludeList = null;
		String[] includeList = null;
		String[] resultsList = null;

		Hashtable<String, String> group2address = new Hashtable<String, String>();
		Hashtable<String, String> address2group = new Hashtable<String, String>();

		Vector<String> requestedGroups = new Vector<String>();
		Vector<String> requestedAddresses = new Vector<String>();
		Vector<String> responseAddresses = new Vector<String>();
		Vector<String> noResponseAddresses = new Vector<String>();

		// Retry-Comm Settings
		boolean noRetry = false;
		String currentRetryValue = null;

		int resultAnz = 0;
		int Timeout = 0;

		DialogThread dialogThread = null;

		EdiabasProxyThread ediabas = null;

		//Paralleldiagnose
		boolean parallel = false;
		String tag = null;
		DeviceManager devMan = null;

		boolean bDebug = false;

		// Debug Schalter �ber Pr�fstandsvariable abfragen
		try {
			Object pr_var_debug;

			pr_var_debug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
			if( pr_var_debug != null ) {
				if( pr_var_debug instanceof String ) {
					if( "TRUE".equalsIgnoreCase( (String) pr_var_debug ) ) {
						bDebug = true;
					} else {
						bDebug = false;
					}
				} else if( pr_var_debug instanceof Boolean ) {
					if( ((Boolean) pr_var_debug).booleanValue() ) {
						bDebug = true;
					} else {
						bDebug = false;
					}
				}
			} else {
				bDebug = false;
			}

		} catch( VariablesException e ) {
			bDebug = false;
		}

		if( bDebug ) {
			System.out.println( "PP DiagFunktional, 'BEGIN'" );
		}

		/***********************************************
		 * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
		 * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		 ***********************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		try {
			//Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				else {
					// zwingende Parameter
					sgbd = getArg( getRequiredArgs()[0] );
					job = getArg( getRequiredArgs()[1] );
					// optionale Parameter
					jobargs = getArg( getOptionalArgs()[0] );
					if( jobargs == null )
						jobargs = "";
					jobargs = jobargs.toUpperCase();
					awt = getArg( getOptionalArgs()[1] );
					if( getArg( getOptionalArgs()[2] ) != null )
						excludeList = splitArg( getArg( getOptionalArgs()[2] ) );
					if( getArg( getOptionalArgs()[3] ) != null )
						includeList = splitArg( getArg( getOptionalArgs()[3] ) );

					// Sollen die Ediabas-Retry-Einstellungen ge�ndert werden?
					if( getArg( getOptionalArgs()[4] ) != null ) {
						if( getArg( getOptionalArgs()[4] ).equalsIgnoreCase( "TRUE" ) )
							noRetry = true;
					}

					//Timeout for single request of all not replied ECU�s
					if( getArg( getOptionalArgs()[5] ) != null ) {
						Timeout = Integer.parseInt( getArg( getOptionalArgs()[7] ) ) * 1000; //for milliseconds
					}

					// Zu dokumentierende Resultnamen einlesen
					if( getArg( "RESULTS_LIST" ) != null ) {
						resultsList = splitArg( getArg( "RESULTS_LIST" ).toUpperCase() );
					}

				}
			} catch( Exception e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Nachricht auf Bildschirm
			if( awt != null ) {
				try {
					dialogThread = new DialogThread( awt );
					dialogThread.run();
				} catch( Exception e ) {
				}
			}

			// Alle Pr�flinge aus dem virtuellen Fahrzeug holen
			try {
				prueflinge = getPr�fling().getAuftrag().getPr�flinge(); // alle verf�gbaren Pr�flinge holen
				for( int i = 0; i < prueflinge.length; i++ ) {
					temp = prueflinge[i].getAttribut( "GRUPPE" );
					// handelt es sich um einen Diagnosepr�fling?
					if( (temp != null) && (temp.equals( "" ) == false) ) {
						// Wurde diese Gruppe bereits aufgenommen?
						if( requestedGroups.contains( temp ) == false ) {
							// Die CARB-Sgbd ist vom OBD-Pr�fling und wird generell ausgeblendet
							if( temp.equals( "CARB" ) == false ) {
								if( (includeList == null) && (excludeList == null) ) {
									requestedGroups.add( temp );
								} else if( (includeList != null) && (excludeList == null) ) {
									for( int j = 0; j < includeList.length; j++ )
										if( includeList[j].equals( temp ) ) {
											requestedGroups.add( temp );
											break;
										}
								} else if( (includeList == null) && (excludeList != null) ) {
									boolean found = false;
									for( int j = 0; j < excludeList.length; j++ )
										if( excludeList[j].equals( temp ) )
											found = true;
									if( !found )
										requestedGroups.add( temp );
								} else if( (includeList != null) && (excludeList != null) ) {
									boolean found = false;
									for( int j = 0; j < excludeList.length; j++ )
										if( excludeList[j].equals( temp ) )
											found = true;
									for( int j = 0; j < includeList.length; j++ )
										if( (!found) && (includeList[j].equals( temp )) )
											requestedGroups.add( temp );
								}
							} // if (temp.equals("CARB")==false)
						} // if ( requestedGroups.contains(temp)==false )                        // Gruppendateiname in Vector speichern, um zu verhindern, dass ein Pr�fling doppelt auftaucht,
					} // if( (temp != null) && (temp.equals("")==false) )
				} // for
			} catch( Exception e ) {
				result = new Ergebnis( "ExecFehler", "GetPrueflinge", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Paralleldiagnose, Pr�fstandvariabel auslesen, ob Paralleldiagnose 
			try {
				Object pr_var;

				pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
				if( pr_var != null ) {
					if( pr_var instanceof String ) {
						if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
							parallel = true;
						}
					} else if( pr_var instanceof Boolean ) {
						if( ((Boolean) pr_var).booleanValue() ) {
							parallel = true;
						}
					}
				}
			} catch( VariablesException e ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				throw new PPExecutionException();
			}

			// Devicemanager
			devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();

			if( getArg( "TAG" ) != null )
				tag = getArg( "TAG" );

			if( parallel ) // Ediabas holen
			{
				try {
					ediabas = devMan.getEdiabasParallel( tag, sgbd );
				} catch( Exception ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Wrong Cascade configuration for parallel diagnosys", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				} catch( Throwable ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				}
			} else {
				try {
					ediabas = devMan.getEdiabas( tag );
				} catch( Exception ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();

				}
			}
			// <<<<< Paralleldiagnose            

			// Auslesen/Setzen der Retry-Comm Parameter, falls gew�nscht 
			if( noRetry ) {
				try {
					currentRetryValue = ediabas.apiGetConfig( "RetryComm" );
					if( currentRetryValue == null )
						currentRetryValue = "1";
					if( currentRetryValue.equalsIgnoreCase( "1" ) ) {
						ediabas.apiSetConfig( "RetryComm", "0" );
					}
				} catch( Exception e ) {
					e.printStackTrace();
					result = new Ergebnis( "DiagFehler", "EDIABAS", "apiSetConfig", "RetryComm", "0", "", "", "", "", "0", "", "", "", "Exception", e.getMessage(), Ergebnis.FT_IGNORE );
					ergListe.add( result );
				} finally {
					if( bDebug ) {
						System.out.println( "PP DiagFunktional, Step: 'check -> set RetryComm in EDIABAS.ini'" );
					}
				}
			}

			// Auslesen der Steuerger�teadressen
			for( int i = 0; i < requestedGroups.size(); i++ ) {
				try {
					temp = ediabas.executeDiagJob( sgbd, "GRP2SGADR", requestedGroups.elementAt( i ).toString(), "" );
					if( temp.equalsIgnoreCase( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, "GRP2SGADR", requestedGroups.elementAt( i ).toString(), "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					} else {
						temp = ediabas.getDiagResultValue( "SG_ADR" );
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, "GRP2SGADR", requestedGroups.elementAt( i ).toString(), "SG_ADR", temp, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
						requestedAddresses.add( temp );
						group2address.put( requestedGroups.elementAt( i ), temp );
						address2group.put( temp, requestedGroups.elementAt( i ) );
					}

				} catch( ApiCallFailedException acfe ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, "GRP2SGADR", requestedGroups.elementAt( i ).toString(), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				} catch( Exception e ) {
					result = new Ergebnis( "Exception", "EDIABAS", sgbd, "GRP2SGADR", requestedGroups.elementAt( i ).toString(), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO );
					ergListe.add( result );
					e.printStackTrace();
					status = STATUS_EXECUTION_ERROR;
				} finally {
					if( bDebug ) {
						System.out.println( "PP DiagFunktional, Step: 'detectECUaddresses', LOOP: <" + i + ", GRUPPE: <" + requestedGroups.elementAt( i ).toString() + ">" );
					}
				}
			}

			// Ausf�hrung des Ediabas-Jobs
			try {
				temp = ediabas.executeDiagJob( sgbd, job, jobargs, "" );

				if( temp.equals( "OKAY" ) == false ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobargs, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				} else {
					// Anzahl der Resultsets lesen
					resultAnz = ediabas.getDiagJobSaetze() - 1;
					result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobargs, "JOB_STATUS", "OKAY (" + resultAnz + " Resultsets)", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
					//status = STATUS_EXECUTION_OK; [RETTIG] Dadurch, dass wir verglichen mit Version 13 oben keine PPExecutionExceptions mehr werfen, sondern nur den Status auf STATUS_EXECUTION_ERROR setzen und weiterlaufen, ist dieser Status hier zu erhalten

				}
			} catch( ApiCallFailedException acfe ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobargs, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Exception e ) {
				result = new Ergebnis( "Exception", "Ediabas", sgbd, job, "", "", "", "", "", "0", "", "", "", "", e.getMessage(), Ergebnis.FT_NIO );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} finally {
				if( bDebug ) {
					System.out.println( "PP DiagFunktional, Step: 'executeDiagJob', SGBD: <" + sgbd + ">, Job: <" + job + ">, Jobargs: <" + jobargs + ">" );
				}
			}

			// Alle Resultsets lesen
			for( int i = 1; i < (resultAnz); i++ ) {
				try {
					// ist das Resultset gueltig?
					if( ediabas.getDiagResultValue( i, "JOB_STATUS" ).equals( "OKAY" ) == true ) {
						// Adresse auslesen und in Vector schreiben
						temp = ediabas.getDiagResultValue( i, "ID_SG_ADR" );
						responseAddresses.add( temp );
					}

					// Sind Results zu dokumentieren? Wenn diese vorhanden sind und kein Fehler auftrat, dann mache dies.
					try {
						for( String diagresult : resultsList ) {
							temp = ediabas.getDiagResultValue( i, diagresult );
							result = new Ergebnis( diagresult + "_" + i, "EDIABAS", sgbd, job, jobargs, diagresult + "_" + i, temp, temp, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						}
					} catch( Exception ex ) {
						// Nothing
					}
				} catch( ApiCallFailedException acfe ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobargs, "Resultset " + i, "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ) + " " + ediabas.apiErrorCode(), ediabas.apiErrorText(), Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR; //[RETTIG]
				} catch( Exception e ) {
					e.printStackTrace();
					result = new Ergebnis( "Exception", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR; //[RETTIG]
				} finally {
					if( bDebug ) {
						System.out.println( "PP DiagFunktional, Step: 'read all Resultsets', LOOP: <" + i + ">, SGBD: <" + sgbd + ">, Job: <" + job + ">, Jobargs: <" + jobargs + ">" );
					}
				}
			}

			// Vergleich der Listen und Bef�llen der noResponseAddresses-Liste
			for( int i = 0; i < requestedAddresses.size(); i++ ) {
				if( responseAddresses.contains( requestedAddresses.elementAt( i ) ) == false ) {
					noResponseAddresses.add( requestedAddresses.elementAt( i ) );
				}
			}

			// Adresseninfo aus den Jobargs rausschneiden, da diese durch phys. Adresse ersetzt wird
			if( jobargs.startsWith( "ALL;" ) || jobargs.startsWith( "PERSONAL;" ) || jobargs.startsWith( "CBS;" ) || jobargs.startsWith( "BOS;" ) || jobargs.startsWith( "MOST;" ) || jobargs.startsWith( "SI;" ) || jobargs.startsWith( "PT-CAN;" ) || jobargs.startsWith( "K-CAN;" ) || jobargs.startsWith( "VD-FLEXRAY;" ) || jobargs.startsWith( "SWT;" ) || jobargs.startsWith( "LIN;" ) || jobargs.startsWith( ";" ) ) {
				jobargs = jobargs.substring( jobargs.indexOf( ";" ) );
			} else {
				jobargs = "";
			}

			long t_start = System.currentTimeMillis();
			long t_temp;

			// Nachfordern der Jobs mit physikalischer Adressierung
			for( int i = 0; i < noResponseAddresses.size(); i++ ) {
				String physJobArgs = null;

				t_temp = System.currentTimeMillis();

				if( ((t_temp - t_start) > Timeout) && (Timeout != 0) ) {
					result = new Ergebnis( "Exception", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "Timeout" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				}

				try {
					physJobArgs = (String) (noResponseAddresses.elementAt( i )) + jobargs;
					temp = ediabas.executeDiagJob( sgbd, job, physJobArgs, "" );
					if( temp.equals( "OKAY" ) == true ) {
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, physJobArgs + "(" + (String) (address2group.get( noResponseAddresses.elementAt( i ) )) + ")", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					} else {
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, physJobArgs + "(" + (String) (address2group.get( noResponseAddresses.elementAt( i ) )) + ")", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				} catch( ApiCallFailedException acfe ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, physJobArgs + "(" + (String) (address2group.get( noResponseAddresses.elementAt( i ) )) + ")", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				} catch( Exception e ) {
					e.printStackTrace();
					result = new Ergebnis( "Exception", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				} finally {
					if( bDebug ) {
						System.out.println( "PP DiagFunktional, Step: 'executeDiagJob direct-physical', LOOP: <" + i + ">, SGBD: <" + sgbd + ">, Job: <" + job + ">, phys.JobArgs: <" + physJobArgs + ">" );
					}
				}
			}
		} catch( PPExecutionException ppee ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "DiagFunktional", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			e.printStackTrace();
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			result = new Ergebnis( "ExecFehler", "DiagFunktional", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			t.printStackTrace();
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// R�cksetzen der Retry-Comm Parameter, falls n�tig
		if( currentRetryValue != null ) {
			if( noRetry && (currentRetryValue.equalsIgnoreCase( "1" )) ) {
				try {
					ediabas.apiSetConfig( "RetryComm", "1" );
				} catch( Exception e ) {
					e.printStackTrace();
					result = new Ergebnis( "DiagFehler", "EDIABAS", "apiSetConfig", "RetryComm", "1", "", "", "", "", "0", "", "", "", "Exception", e.getMessage(), Ergebnis.FT_IGNORE );
					ergListe.add( result );
				} finally {
					if( bDebug ) {
						System.out.println( "PP DiagFunktional, Step: 'set RetryComm in EDIABAS.ini'" );
					}
				}
			}
		}

		//Freigabe/Beenden des Dialog-Threads
		if( dialogThread != null ) {
			dialogThread.interrupt();
		}

		// Paralleldiagnose
		if( parallel ) {
			try {
				devMan.releaseEdiabasParallel( ediabas, tag, sgbd );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable t ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		setPPStatus( info, status, ergListe );

		if( bDebug ) {
			System.out.println( "PP DiagFunktional, 'END'" );
		}
	}

	/** Dieser Thread macht der DialogBox.
	 */
	private class DialogThread implements Runnable {
		private boolean dialogRunning = false;
		private UserDialog myDialog = null;
		private String message;

		public DialogThread( String awt ) throws Exception {
			dialogRunning = true;
			message = awt;
			myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
		}

		@Override
		public void run() {
			try {
				myDialog.displayMessage( "Functional", message, -1 );
			} catch( Exception e ) {
			}
			dialogRunning = false;
		}

		public void interrupt() {
			while( this.dialogRunning() ) {
				try {
					Thread.sleep( 10 );
				} catch( Exception e ) {
				}
			}
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( Exception e ) {
			}
		}

		public boolean dialogRunning() {
			return dialogRunning;
		}

	}

}

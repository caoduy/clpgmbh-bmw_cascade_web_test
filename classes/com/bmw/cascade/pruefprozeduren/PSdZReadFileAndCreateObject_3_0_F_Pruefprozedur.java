package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import java.io.*;

import com.bmw.appframework.logging.LogLevel;
import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.psdz.PSdZ;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.util.logging.CascadeLogging;

/**
 * Implementierung einer Pr�fprozedur, die aus einer externen Datei ein PSdZ Objekt (SVTIST, SVTSOLL, SOLLVERBAUUNG, TAL) generieren kann. <BR>
 * 
 * Die externe Datei muss vollqualifiziert (FQN) �ber den �bergabeparameter "FILE_NAME" angegeben werden. Achtung! Die Datei muss lokal vorhanden sein!<BR>
 * Die Qualifizierung was die Datei beinhaltet, wird �ber den �bergabeparameter "FILE_TYPE" vorgenommen. M�gliche Werte sind dabei:<BR>
 * ---> "SVTIST" == Dateiinhalt wird als PSDZ SVTist XML Inhalt interpretiert.<BR>
 * ---> "SVTSOLL" == Dateiinhalt wird als PSDZ SVTsoll XML Inhalt interpretiert.<BR>
 * ---> "SOLLVERBAUUNG" == Dateiinhalt wird als PSDZ SVTist XML Inhalt interpretiert.<BR>
 * ---> "TAL" == Dateiinhalt wird als PSDZ SVTist XML Inhalt interpretiert.<BR>
 * 
 * Optional kann das Encoding der externen Datei mittels "ENCODING" angegeben werden. Falls der Parameter nicht gesetzt ist, wird das default Encoding bei Windows i.d.R. CP1252 angenommen.<BR>
 * 
 * Alle �bergabeparameter mit Ausnahme von "DEBUG" und "DEBUG_PERFORM" unterst�tzen die Benutzung des "@" Operators zum Zugriff auf Ergebnisse aus dem virtuellen Fahrzeug.<BR>
 * 
 * 
 * @author Thomas Buboltz, BMW AG, Martin Gampl, BMW AG <BR>
 * 
 *         12.04.2013 TB F-Version (1_0), Mindestversionsanforderung CASCADE 6.0.0 / PSdZ 4.8.1 <BR>
 *         ---------- -- Ersterstellung <BR>
 *         08.12.2015 MG T-Version (2_0), Mindestversionsanforderung CASCADE 6.0.0 / PSdZ 4.8.1 <BR>
 *         ---------- -- Speichern der XML-Dateien f�r folgenden Methoden auf PP verlagert, um Dateinamen pro PP-Aufruf individuell festlegen zu k�nnen <BR>
 *         ---------- -- - public void setPSdZSVTist( String SVTISTasXML ) <BR>
 *         ---------- -- - public void setPSdZSVTsoll( String SVTSOLLasXML ) <BR>
 *         ---------- -- - public void setPSdZSollverbauung( String SollverbauungAsXML ) <BR>
 *         ---------- -- - public void setPSdZTAL( String TALasXML ) <BR>
 *         ---------- -- LOP 1853: Ausgabe (in Log-Datei Pr�fstandScreen) der Pr�fprozedurversion sowie s�mtlicher optionaler und nicht optionaler Argumente, die an die Pr�fprozedur �bergeben werden zum Zweck der Nachvollziehbarkeit im Fehlerfall <BR>
 *         ---------- -- LOP 1899: Einheitliche Darstellung der Fehlerausgaben in PSdZ-Pr�fprozeduren <BR>
 *         04.02.2016 MG F-Version (3_0), Mindestversionsanforderung CASCADE 6.0.0 / PSdZ 4.8.1 <BR>
 */
public class PSdZReadFileAndCreateObject_3_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * Enum mit den PSdZ Objekten
	 */
	private static enum PSdZObjectType {
		SVTIST, SVTSOLL, SOLLVERBAUUNG, TAL;
	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSdZReadFileAndCreateObject_3_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSdZReadFileAndCreateObject_3_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG", "DEBUG_PERFORM", "ENCODING" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = { "FILE_NAME", "FILE_TYPE" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		if( !super.checkArgs() )
			return false;
		return true;
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {
		boolean DE = checkDE(); // Systemsprache DE wenn true
		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
		boolean bDebugPerform = true; // gibt an, ob eine Performencemessung erfolgen sollen
		int status = STATUS_EXECUTION_OK;

		Ergebnis result = null;
		PSdZ psdz = null;
		String[] optionalArgumentsNames = getOptionalArgs();
		String[] optionalArgumentsValues = null;
		String[] requiredArgumentsNames = getRequiredArgs();
		String[] requiredArgumentsValues = null;
		String optionalArgumentName = null;
		String optionalArgumentValue = null;
		String requiredArgumentName = null;
		String requiredArgumentValue = null;
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";
		String strEncoding = null; // optional Name des Encodings (UTF-8,...), wenn nicht vorhanden wird das default Windows Encoding CP1252 angenommen
		String strFile = null; // vollqualifizierter Dateiname der externen PSdZ Datei 
		String strFileType = null; // Bezeichnung des PSdZ Objektes (SVTIST, SVTSOLL etc.)
		String testScreenID = this.getPr�flingLaufzeitUmgebung().getName(); // TestScreenID (aus Pr�fstandsinstanz)
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();

		try {

			// Argumente loggen, einlesen und checken
			try {

				// Ausgabe der Pr�fprozedurversion sowie s�mtlicher an die Pr�fprozedur �bergebener Argumente (required/optional) f�r sp�tere Analysezwecke
				System.out.println( "PSdZ ID:" + testScreenID + ", Initialize " + this.getClassName() );

				for( int i = 0; i < requiredArgumentsNames.length; i++ ) {
					requiredArgumentName = requiredArgumentsNames[i];
					requiredArgumentValue = "";
					// gibt es nicht optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( requiredArgumentName ) != null ) {
						requiredArgumentsValues = extractValues( getArg( requiredArgumentName ) );
						for( int k = 0; k < requiredArgumentsValues.length; k++ ) {
							// Listen von nicht optionalen Argumenten werden durch ";" getrennt ausgegeben
							requiredArgumentValue = requiredArgumentValue + requiredArgumentsValues[k] + ((requiredArgumentsValues.length > 1 && k < (requiredArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						requiredArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZReadFileAndCreateObject with required argument: " + requiredArgumentName + " = " + requiredArgumentValue );
				}

				for( int i = 0; i < optionalArgumentsNames.length; i++ ) {
					optionalArgumentName = optionalArgumentsNames[i];
					optionalArgumentValue = "";
					// gibt es optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( optionalArgumentName ) != null ) {
						optionalArgumentsValues = extractValues( getArg( optionalArgumentName ) );
						for( int k = 0; k < optionalArgumentsValues.length; k++ ) {
							// Listen von optionalen Argumenten werden durch ";" getrennt ausgegeben
							optionalArgumentValue = optionalArgumentValue + optionalArgumentsValues[k] + ((optionalArgumentsValues.length > 1 && k < (optionalArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						optionalArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZReadFileAndCreateObject with optional argument: " + optionalArgumentName + " = " + optionalArgumentValue );
				}

				// Obligatorische Argumentpr�fung auf Vorhandensein von nicht optionalen sowie �berz�hligen Argumenten
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
					else
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				} else {
					// alternativ DEBUG �ber Pr�fstandsvariable noch auswerten
					try {
						Object objDebug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
						if( objDebug != null ) {
							if( objDebug instanceof String && "TRUE".equalsIgnoreCase( (String) objDebug ) )
								bDebug = true;
							else if( objDebug instanceof Boolean && ((Boolean) objDebug).booleanValue() )
								bDebug = true;
						}
					} catch( VariablesException e ) {
						// Nothing, bDebug ist false vorbelegt
					}
				}

				// DEBUG_PERFORM
				if( getArg( "DEBUG_PERFORM" ) != null ) {
					if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "TRUE" ) )
						bDebugPerform = true;
					else if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "FALSE" ) )
						bDebugPerform = false;
					else
						throw new PPExecutionException( "DEBUG_PERFORM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// ENCODING
				if( getArg( "ENCODING" ) != null ) {
					strEncoding = extractValues( getArg( "ENCODING" ) )[0];
				}

				// FILE_NAME
				if( getArg( "FILE_NAME" ) != null ) {
					strFile = extractValues( getArg( "FILE_NAME" ) )[0];
				}

				// FILE_TYPE
				if( getArg( "FILE_TYPE" ) != null ) {
					strFileType = extractValues( getArg( "FILE_TYPE" ) )[0];
				}

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Devices holen
			try {
				psdz = getPr�flingLaufzeitUmgebung().getDeviceManager().getPSdZ();
				if( bDebug )
					CascadeLogging.getLogger().log( LogLevel.INFO, "PSdZReadSVT: Get PSdZ Device finished." );
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Performancemessung Start
			if( bDebugPerform )
				System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZReadFileAndCreateObject PP START" );

			// externe Datei einlesen
			String txtDatei = DE ? "Dateiname: " : "File name: ";
			File fileToRead = new File( strFile );
			StringBuffer sbContent = new StringBuffer();
			if( fileToRead.exists() && fileToRead.canRead() ) {
				BufferedReader in = new BufferedReader( strEncoding != null ? new InputStreamReader( new FileInputStream( fileToRead ), strEncoding ) : new InputStreamReader( new FileInputStream( fileToRead ) ) );
				try {
					String strLine = "";
					while( (strLine = in.readLine()) != null ) {
						sbContent.append( strLine );
					}
				} finally {
					in.close();
				}
			} else {
				String txtLeseFehler = DE ? "Datei konnte nicht gelesen werden." : "File can not be read.";
				result = new Ergebnis( "PSdZReadFileAndCreateObject", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", txtLeseFehler + " " + txtDatei + strFile, "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// PSdZ Objekt erzeugen
			PSdZObjectType fileType = PSdZObjectType.valueOf( strFileType.toUpperCase().trim() ); // Typ �ber enum parsen...

			String txtPSdZType = DE ? "PSdZ Objekt Typ" : "PSdZ Object type";
			String[] psdzDeviceParams = psdz.getDeviceParams();
			boolean bEnableSVTTraces = (psdzDeviceParams != null && psdzDeviceParams.length > 3) ? Boolean.valueOf( psdzDeviceParams[3] ).booleanValue() : false;

			switch( fileType ) {
				case SVTIST:
					psdz.setPSdZSVTist( sbContent.toString() );
					if( bEnableSVTTraces ) {
						psdz.logPSdZContent( PSdZ.SVTIST + "_FROM_FILE", psdz.getPSdZSVTistAsXML() ); // XML dokumentieren
					}
					result = new Ergebnis( "PSdZReadFileAndCreateObject", "PSdZ", txtDatei + strFile, "", "", txtPSdZType, PSdZObjectType.SVTIST.name(), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
					break;
				case SVTSOLL:
					psdz.setPSdZSVTsoll( sbContent.toString() );
					if( bEnableSVTTraces ) {
						psdz.logPSdZContent( PSdZ.SVTSOLL + "_FROM_FILE", psdz.getPSdZSVTsollAsXML() ); // XML dokumentieren
					}
					result = new Ergebnis( "PSdZReadFileAndCreateObject", "PSdZ", txtDatei + strFile, "", "", txtPSdZType, PSdZObjectType.SVTSOLL.name(), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
					break;
				case SOLLVERBAUUNG:
					psdz.setPSdZSollverbauung( sbContent.toString() );
					if( bEnableSVTTraces ) {
						psdz.logPSdZContent( PSdZ.SOLLVERBAUUNG + "_FROM_FILE", psdz.getPSdZSollverbauungAsXML() ); // XML dokumentieren
					}
					result = new Ergebnis( "PSdZReadFileAndCreateObject", "PSdZ", txtDatei + strFile, "", "", txtPSdZType, PSdZObjectType.SOLLVERBAUUNG.name(), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
					break;
				case TAL:
					psdz.setPSdZTAL( sbContent.toString() );
					if( bEnableSVTTraces ) {
						psdz.logPSdZContent( PSdZ.TAL + "_FROM_FILE", psdz.getPSdZTALasXML() ); // XML dokumentieren
					}
					result = new Ergebnis( "PSdZReadFileAndCreateObject", "PSdZ", txtDatei + strFile, "", "", txtPSdZType, PSdZObjectType.TAL.name(), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
					break;
				default:
					String txtFehler = DE ? "PSdZ Objekt konnte nicht instanziiert werden, da �bergebener FILE_TYPE unbekannt." : "PSdZ object could not be created as given FILE_TYPE unknown.";
					result = new Ergebnis( "PSdZReadFileAndCreateObject", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", txtFehler + " FILE_TYPE: " + strFileType, "", Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					break;
			}

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Freigabe der benutzten Devices
		if( psdz != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releasePSdZ();
				if( bDebug )
					CascadeLogging.getLogger().log( LogLevel.INFO, "PSdZReadFileAndCreateObject: Release PSdZ Device finished." );
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "PSdZReadFileAndCreateObject", "PSdZ", "", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}
		setPPStatus( info, status, ergListe );

		// Performancemessung Ende
		if( bDebugPerform )
			System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZReadFileAndCreateObject PP ENDE" );

	}

}

/*
 * Liest die RDC IDs aus dem Handterminal
 *
 * Created on 20.06.2008
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.DeviceIOException;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.DeviceParameterException;
import com.bmw.cascade.pruefstand.devices.serial.SerialGeneric;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/** Liest die RDC IDs aus dem Handterminal
 *
 * @author Daniel Frey(FD), Frank Weber(FW)
 * @version V1_0_F  27.06.2008 FD Initialversion
 * @version V2_0_F  05.10.2008 FW Erweiterung Generationsfilter
 * @version V3_0_T  02.03.2009 FD Anpassung an neue Terminals (zusatzspalte "RLRot" bei Kommando "last"
 *                                Default f�r Generationsfilter: 0
 * @version V4_0_T  01.04.2009 FD Results gro� geschrieben und ID=Ergebnisname, da sonst nicht von DiagTolerenz auffindbar
 * @version V5_0_F  01.04.2009 FD F-Version
 * @version V6_0_T  25.05.2009 FD Baudrate konfigurierbar
 * @version V7_0_F  25.05.2009 FD F-Version 
 * @version V8_0_T  28.07.2009 FD Anpassungen f�r MiniAnalyser
 *                                Neuer Parameter: HANDHELDTYPE (Default: Altes Handheld)
 * @version V9_0_F  28.07.2009 FD F-Version 
 * @version V10_0_T  03.08.2009 FD Bugfix bei last, ack: Auch nach ack schickt der MA ein "USER1>" zur�ck.
 * @version V11_0_F  03.08.2009 FD F-Version 
 * @version V12_0_T  26.08.2009 FD MiniAnalyzer last Zeilen beginnen mit 1: 2: 3: statt 0: 1: 2:
 * @version V13_0_F  26.08.2009 FD F-Version 
 * @version V14_0_T  28.08.2009 Workaround aus V1213 wieder entfernt
 *                              Wenn gefilterte Anzahl != ungefilterte Anzahl: ungefilterte Anzahl in Klammern mit anzeigen
 * @version V15_0_F  26.08.2009 FD F-Version 
 * @version V16_0_T  09.04.2010 FD Wenn Handheld nicht antwortet: Kein Abbruch, sondern Warteschleife bis Timeout.
 *                                 Meldung: "Handheld eingeschaltet?"
 * @version V17_0_F  09.04.2010 FD F-Version                                  
 * @version V18_0_T  27.06.2012 FD Parameter GENFILTER akzeptiert beliebige Integerzahlen                                  
 * @version V19_0_F  27.06.2012 FD F-Version   
 * @version V20_0_F  25.07.2012 FD Kommado zum GENFILTER setzen ge�ndert von cpedit auf gencfg
 * @version V21_0_F  25.07.2012 FD F-Version                               
 */
public class ReadRDCTerminal_21_0_F_Pruefprozedur extends AbstractBMWPruefprozedur 
{
	/** added serial parameters to circumvent problem with importing javax.comm */
	private final int FLOWCONTROL_XONXOFF_IN =4;
    /** */
    private final int FLOWCONTROL_XONXOFF_OUT = 8;
    /** */
    private final int DATABITS_8 = 8;
    /** */
    private final int STOPBITS_1 = 1;
    /** */
    private final int PARITY_NONE = 0;
    /** */
    private final int TIMEOUT_SERIAL = 500;
	
	/** */
    static final long serialVersionUID = 1L;    
    
    
    /** Debug Level: 0 keine Ausgaben 
     *               20 maximale Ausgaben
     */
    private int m_debug_level;
    
    /** Handheld Typ:
     * 0 altes F1 Handheld
     * 1 MiniAnalyzer
     */
    private int m_handheld_type; 
    
    /** Dialog im Cyclic Fall: Zeigt Anweisung, Anzahl gelesener IDs und
     * Abbruch Button */
    private UserDialog m_myDialog = null;
    
    /**
     * DefaultKonstruktor, nur fuer die Deserialisierung
     */
    public ReadRDCTerminal_21_0_F_Pruefprozedur() {}
    
    /** Erzeugt eine neue Pruefprozedur.
     * 
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public ReadRDCTerminal_21_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) 
    {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    /** Nur Test f�r CRC 
     * 
     * @param args not used
     */
    public static void main(String[] args)
    {
        
        //new RDC_Entry(0, " 0:  --           0 10000  -40    0   0 0x00  0x00         0", 1);
        
        System.out.println("Test CRC");
        /*
        String nl="\r\n";
        String tst=" 0:   -- 1402246943  1000   25   50 237 0x41  0x00   667580"+nl+
                   " 0:   -- 1402246937  1025   24   50 233 0x51  0x00   669960"+nl+
                   " 0:   --          0 10000    0    0   0 0x00  0x00        0"+nl+
                   " 0:   --          0 10000    0    0   0 0x00  0x00        0"+nl;
        */
        String nl="\n";
        String tst=" 0:  --  1402246943  1000   25   50 237 0x41  0x00    667580"+nl+
                   " 0:  --  1402246937  1025   24   50 233 0x51  0x00    669960"+nl+
                   " 0:  --           0 10000    0    0   0 0x00  0x00         0"+nl+
                   " 0:  --           0 10000    0    0   0 0x00  0x00         0"+nl;
        
               tst=" 0:  --           0 10000  -40    0   0 0x00  0x00         0"+nl+
                   " 0:  --           0 10000  -40    0   0 0x00  0x00         0"+nl+
                   " 0:  --           0 10000  -40    0   0 0x00  0x00         0"+nl+
                   " 0:  --           0 10000  -40    0   0 0x00  0x00         0"+nl;   
        
        
        int erg=crc16(tst.getBytes());
        if (erg<0) erg+=65536;
        System.out.println(tst.length()+"  "+Integer.toHexString(erg));
    }
    
    /**
     * initialsiert die Argumente
     */
    protected void attributeInit() 
    {
        super.attributeInit();
    }
           
    /**
     * liefert die zwingend erforderlichen Argumente
     */
    public String[] getRequiredArgs() 
    {
        String[] args = { "MODE", "BAUDRATE" };
        return args;
    }
    
    
    /**
     * liefert die optionalen Argumente
     */
    public String[] getOptionalArgs() 
    {
        String[] args = 
        { "DEBUG",
          "AWT",
          "TIMEOUT",
          "RSSIFILTER_MINIMUM",
          "MODEFILTER_ACCEPT",
          "MODEFILTER_NOERROR",
          "IDFILTER",
          "GENFILTER",
          "HANDHELDTYPE"
        };
        return args;
    }
    
    public boolean checkArgs() 
    {
        Vector errors = new Vector();
        return checkArgs(errors);
    }
    
    /** �berpr�ft die Argumente f�r diese PP
     * 
     * @param errors Fehlerliste
     * @return false: Argumente fehlerhaft, true: Argumente IO
     */
    public boolean checkArgs(Vector errors) 
    {
    Ergebnis result;
    String param, paramname;
    int h;
    int i;
    char c;
    boolean accept;
    String[] ranges;
    String[] range;
        
        try 
        {
            paramname=getRequiredArgs()[0];
            param=getArg(paramname);
            if (param==null) param="";
            param=param.toUpperCase();
            
            if (!param.equals("CYCLIC") && !param.equals("ONCE") && !param.equals("PREPARE"))
            {
                if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterfehler", paramname+" mu� PREPARE, ONCE oder CYCLIC sein. Ist \""+param+"\"", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", paramname+" must be PREPARE, ONCE or CYCLIC. Is \""+param+"\"", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }
            
            paramname=getRequiredArgs()[1];
            param=getArg(paramname);
            if (param==null) param="";
            try
            {
                Integer.parseInt(param);
            }
            catch (NumberFormatException ex)
            {
                if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterfehler", paramname+" mu� ganze Zahl sein. Ist \""+param+"\"", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", paramname+" must be integer. Is \""+param+"\"", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }
            

            paramname=getOptionalArgs()[2]; //TIMEOUT
            param=getArg(paramname);
            if (param==null) param="";

            if (param.length()>0)
            {
                try
                {
                    h=Integer.parseInt(param);
                }
                catch (NumberFormatException ex)
                {
                    h=-1;
                }
                if (h<=0)
                {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterfehler", paramname+" mu� eine positive Zahl sein. Ist \""+param+"\"", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", paramname+" must be a positive number. Is \""+param+"\"", Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    return false;
                }
            }
            
            paramname=getOptionalArgs()[3]; // RSSIFILTER_MINIMUM
            param=getArg(paramname);
            if (param==null) param="";

            if (param.length()>0)
            {
                try
                {
                    h=Integer.parseInt(param);
                }
                catch (NumberFormatException ex)
                {
                    h=1;
                }
                if (h>=0)
                {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterfehler", paramname+" mu� eine negative Zahl sein. Ist \""+param+"\"", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", paramname+" must be a negative number. Is \""+param+"\"", Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    return false;
                }
            }

            for (h=4; h<=5; h++)
            {
                paramname=getOptionalArgs()[h]; // MODEFILTER_
                param=getArg(paramname);
                if (param==null) param="";

                if (param.length()>0)
                {
                    accept=true;
                    if (param.length()!=8)
                    {
                        accept=false;
                    }
                    else
                    {
                        param=param.toUpperCase();
                        for (i=0; i<param.length(); i++)
                        {
                            c=param.charAt(i);
                            if (c!='0' && c!='1' && c!='X')
                            {
                                accept=false;
                                break;
                            }
                        }
                    }
                    if (!accept)
                    {
                        if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterfehler", paramname+" mu� ein Filter der Form 8 mal [01X] sein, z.B. 10XX1100. Ist \""+param+"\"", Ergebnis.FT_NIO_SYS );
                        else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", paramname+" must be a filter of type 8 times [01X], e.g. 10XX1100. Is \""+param+"\"", Ergebnis.FT_NIO_SYS );
                        errors.add(result);
                        return false;
                    }
                }
            }
            
            paramname=getOptionalArgs()[6]; // IDFILTER
            param=getArg(paramname);
            if (param==null) param="";

            if (param.length()>0)
            {
                ranges=param.split(";");
                for (i=0; i<ranges.length; i++)
                {
                    param=ranges[i].trim();
                    range=param.split("-");
                    accept=false;
                    if (range.length==2)
                    {
                        try
                        {
                            long b1, b2;
                            b1=Long.parseLong(range[0]);
                            b2=Long.parseLong(range[1]);
                            accept=b2>b1;
                        }
                        catch (NumberFormatException ex)
                        {
                        }
                    }
                    if (!accept)
                    {
                        if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterfehler", paramname+" hat ung�ltigen Bereich \""+param+"\"", Ergebnis.FT_NIO_SYS );
                        else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", paramname+" has illegal range \""+param+"\"", Ergebnis.FT_NIO_SYS );
                        errors.add(result);
                        return false;
                    }
                }
            }
            
            paramname=getOptionalArgs()[7]; // GENFILTER
            param=getArg(paramname);
            if (param==null) param="";

            if (param.length()>0)
            {
                try
                {
                    h=Integer.parseInt(param);
                }
                catch (NumberFormatException ex)
                {
                    h=-1;
                }
                if (h<0)
                {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterfehler", paramname+" mu� >=0 sein. Ist \""+param+"\"", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", paramname+" must be >=0. Is \""+param+"\"", Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    return false;
                }
            }
            
            if (!super.checkArgs()) 
            {
                if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }

            return true;
        } 
        catch (Exception e) 
        {
            return false;
        } 
        catch (Throwable e) 
        {
            return false;
        }
    }
    
   
    /** f�hrt die Pr�fprozedur aus
     * 
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) 
    {
    int status = STATUS_EXECUTION_OK;
    Vector ergListe = new Vector();
    String    debuglevel_str;
    SerialGeneric mySerialDevice;
    DeviceManager devMan;
    Exception error;
    String mode, awt, timeout, handheldtype;
    int    Baudrate;
    RDC_Entry[] entries;
    String RSSI_Filter_str;
    int RSSI_Filter;
    String ModeAcceptFilter;
    String ModeNoErrorFilter;
    String sGenFilter;
    String param;
    long[][] IDFilter;

        m_myDialog=null;
        try
        {
            debuglevel_str = getArg("DEBUG");
            if (debuglevel_str == null) 
                m_debug_level = 0;
            else if (debuglevel_str.length()==0)
                m_debug_level = 0;
            else if ("TRUE".equalsIgnoreCase(debuglevel_str)) 
                m_debug_level = 20;
            else if ("FALSE".equalsIgnoreCase(debuglevel_str)) 
                m_debug_level = 0;
            else 
            {
                try 
                {
                    m_debug_level = Integer.parseInt(debuglevel_str);
                    if (m_debug_level < 0) m_debug_level = 0;
                } 
                catch (NumberFormatException nfe) 
                {
                    debuglevel_str="DEBUG (="+debuglevel_str+")";
                    if (isDE()) System.out.println("ReadRDCTerminal.execute: "+debuglevel_str+" ung�ltig");
                    else System.out.println("ReadRDCTerminal.execute: "+debuglevel_str+" invalid");
                    if (isDE()) throw new CError("ParameterFehler", "Parameter "+debuglevel_str+" ung�ltig", "Erlaubt: TRUE, FALSE, 0-20");
                    else throw new CError("ParameterError", "parameter "+debuglevel_str+" invalid", "Allowed: TRUE, FALSE, 0-20");
                }
            }
            
            if (!checkArgs(ergListe))  
            {
                setPPStatus( info, STATUS_EXECUTION_ERROR, ergListe );
                return;
            }
            
            if (m_debug_level>=1)
            {
                System.out.println("### DEBUG LEVEL: "+m_debug_level+" ###");
            }
            
            mode=getArg(getRequiredArgs()[0]);
            
            Baudrate=Integer.parseInt(getArg(getRequiredArgs()[1]));
            // z.B. 57600, checkArgs sollte sichergestellt haben, da� parsbar
            
            m_handheld_type=0;
            handheldtype=getArg(getOptionalArgs()[8]);
            if ("MA".equalsIgnoreCase(handheldtype) || 
                "MINIANALYZER".equalsIgnoreCase(handheldtype) ||
                "MINIANALYSER".equalsIgnoreCase(handheldtype))
            {
                m_handheld_type=1;
            }
            if (m_debug_level>5) System.out.println("Handheld type: "+handheldtype+"/"+m_handheld_type);
            
            RSSI_Filter_str=getArg(getOptionalArgs()[3]);
            if (RSSI_Filter_str==null) RSSI_Filter_str="";
            try
            {
                RSSI_Filter=Integer.parseInt(RSSI_Filter_str);
            }
            catch (NumberFormatException ex)
            {
                RSSI_Filter=Integer.MIN_VALUE;
            }
            
            ModeAcceptFilter=getArg(getOptionalArgs()[4]);
            ModeNoErrorFilter=getArg(getOptionalArgs()[5]);
            
            IDFilter=null;
            param=getArg(getOptionalArgs()[6]);
            if (param==null) param="";
            if (param.length()>0)
            {
                int i;
                String ranges[];
                String range[];
                ranges=param.split(";");
                IDFilter=new long[ranges.length][2];
                for (i=0; i<ranges.length; i++)
                {
                    range=ranges[i].trim().split("-");
                    IDFilter[i][0]=Long.parseLong(range[0]);
                    IDFilter[i][1]=Long.parseLong(range[1]);
                }
            }
            
            sGenFilter=getArg(getOptionalArgs()[7]);
            if (sGenFilter==null) sGenFilter="";
            sGenFilter=sGenFilter.trim();
            if (sGenFilter.length()==0) sGenFilter="0";
            
            try 
            {
                devMan=getPr�flingLaufzeitUmgebung().getDeviceManager();
                mySerialDevice = devMan.getSerialGeneric();
            } 
            catch( Exception e ) 
            {
                if( e instanceof DeviceLockedException )
                    throw new CError( "ExecFehler", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
                else if( e instanceof DeviceNotAvailableException )
                    throw new CError( "ExecFehler", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
                else if( e instanceof DeviceParameterException )
                    throw new CError( "ExecFehler", "DeviceParameterException", e.getMessage(), Ergebnis.FT_NIO_SYS );
                else
                    throw new CError( "ExecFehler", "Exception", e.getMessage(), Ergebnis.FT_NIO_SYS );
            }
            
            try
            {
                if (m_handheld_type==0)
                    mySerialDevice.open(Baudrate, this.FLOWCONTROL_XONXOFF_IN, this.FLOWCONTROL_XONXOFF_OUT, this.DATABITS_8, this.STOPBITS_1, this.PARITY_NONE);
                else
                    mySerialDevice.open(Baudrate, 0, 0, this.DATABITS_8, this.STOPBITS_1, this.PARITY_NONE);
            }
            catch (DeviceNotAvailableException ex)
            {
                devMan.releaseSerialGeneric();
                throw new CError( "ExecFehler", "SerialOpen failed", ex.getMessage(), Ergebnis.FT_NIO); 
            }
            
            error=null;
            try
            {
            	
            	//mode prepare
                if (mode.equalsIgnoreCase("PREPARE"))
                {
                    if (sGenFilter.equalsIgnoreCase("0")==false) setGenfilter(mySerialDevice, sGenFilter, 10000);
                    prepare(mySerialDevice);
                }
                // mode once or cyclic
                else
                {
                    if (mode.equalsIgnoreCase("ONCE")) entries=readIDsOnce(mySerialDevice, RSSI_Filter, ModeAcceptFilter, ModeNoErrorFilter, IDFilter);
                    // cyclic mode
                    else
                    {
                        initDialog();

                        
                        int to;
                        awt=getArg(getOptionalArgs()[1]);
                        if (awt==null) awt="";
                        
                        timeout=getArg(getOptionalArgs()[2]);
                        if (timeout==null) timeout="";
                        
                        if (m_debug_level>=3) System.out.println(getOptionalArgs()[2]+"="+timeout);
                        
                        try
                        {
                            to=Integer.parseInt(timeout);
                            to*=1000;
                        }
                        catch (NumberFormatException ex)
                        {
                            to=60*1000;
                        }
                        
                        if (sGenFilter.equalsIgnoreCase("0")==false) setGenfilter(mySerialDevice, sGenFilter, to);
                        
                        entries=readIDsCyclic(mySerialDevice, awt, to, RSSI_Filter, ModeAcceptFilter, ModeNoErrorFilter, IDFilter);
                    }
                    storeEntries(entries, ergListe);
                }
            }
            catch (Exception e)
            {
                error=e;
            }
            
            mySerialDevice.close();
            devMan.releaseSerialGeneric();
            
            releaseDialog();
            
            if (error!=null) throw error;
        } 
        catch (CError err) 
        {
            if (m_debug_level>0) System.out.println("Error: "+err.getMessage());
            ergListe.add(err.getEntry());
            status = STATUS_EXECUTION_ERROR;
        } 
        catch (Exception e) 
        {
            System.out.println(e.getMessage());
            e.printStackTrace(); 
            ergListe.add(new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS ));
            status = STATUS_EXECUTION_ERROR;
        } 
        catch (Throwable e) 
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
            ergListe.add(new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS ));
            status = STATUS_EXECUTION_ERROR;
        }
      
        setPPStatus( info, status, ergListe );
    }
    
    /** Setzt GenFilter
     * 
     * @param mySerialDevice Serielle Schnittstelle
     * @param sGenFilter GenFilter "2" oder "3"
     * @param FinalTimeout Timeout nachdem die PP mit Fehlerabbricht
     * @throws CError Fehlerobjekt
     */
    private void setGenfilter(SerialGeneric mySerialDevice, String sGenFilter, int FinalTimeout) throws CError
    {
    //send gen filter commands
    byte[] buffer;
    String str;
    long starttime;
    
        starttime=System.currentTimeMillis();
        buffer = command_execute_withRetry(mySerialDevice, "gencfg "+sGenFilter, TIMEOUT_SERIAL, starttime, FinalTimeout, true);
        str=new String(buffer);
        if (str.indexOf("Sensor gen configuration")<0) 
        {
            if (isDE()) 
                throw new CError("DEV_ERROR", "Fehler beim Setzen des RDC Gen Filters", "Handheld Antwort: '"+str+"'", Ergebnis.FT_NIO_SYS );
            else 
                throw new CError("DEV_ERROR", "Error setting RDC Gen Filter", "Handheld response: '"+str+"'", Ergebnis.FT_NIO_SYS );
        }
        if(m_debug_level>=3) System.out.println("Set gen filter successful");
    }
    
    /** Vorbeireitung bei drahtgebundenem Einlesen:
     * Es wird wird ermittelt, welche IDs schon im Terminal stehen.
     * Diese werden in den dyn. Attributen hinterlegt um sp�ter zu wissen
     * welche Eintr�ge neu und welche alt sind.
     * 
     * Ablauf:
     * Prepare
     * Handterminal abstecken
     * RDC IDs einlesen
     * Handterminal anstecken
     * IDs auslesen (readIDsOnce)
     * 
     * @param mySerialDevice serielle Schittstelle
     * @throws CError Fehlerobjekt
     */
    private void prepare(SerialGeneric mySerialDevice) throws CError
    {
    Set oldentries;
    Iterator it;
    String   key, att_entry;
    
        oldentries=getOldEntries(mySerialDevice, System.currentTimeMillis(), 10*1000);
        if (oldentries.size()==0) oldentries.add("NONE");
        it=oldentries.iterator();
        att_entry="";
        while (it.hasNext())
        {
            key=(String)it.next();
            if (att_entry.length()>0) att_entry+=";";
            att_entry+=key;
        }
        setDynamicAttribute("RDC_OLDENTRIES", att_entry, null);
    }
    
    /** Auslesen der letzten 4 RDC IDs aus dem Handterminal. Prepare mu� im PU vorher ausgef�hrt werden.
     * 
     * Ablauf:
     * Prepare
     * Handterminal abstecken
     * RDC IDs einlesen
     * Handterminal anstecken
     * IDs auslesen (readIDsOnce)
     * 
     * @param mySerialDevice serielle Schnittstelle
     * @param RSSI_Filter Minimalwert f�r RSSI
     * @param ModeAcceptFilter Filter f�r Mode 
     * @param ModeNoErrorFilter Filter f�r Mode
     * @param IDFilter IDFilter f�r RDC ID (null=alle IDs)
     * @return Eingelesene RDC IDs
     * @throws CError Fehlerobjekt
     */
    private RDC_Entry[] readIDsOnce(SerialGeneric mySerialDevice, int RSSI_Filter, String ModeAcceptFilter, String ModeNoErrorFilter, long[][] IDFilter) throws CError
    {
    RDC_Entry[] allentries;
    RDC_Entry[] last4entries;
    Set oldentries;
    
        oldentries=getOldEntriesFromAtt();
        if (oldentries==null || oldentries.isEmpty())
        {
            if (isDE()) throw new CError("ExecFehler", "MODE=PREPARE wurde nicht vorher ausgef�hert", "", Ergebnis.FT_NIO);
            else        throw new CError("ExecFehler", "MODE=PREPARE was not executed before", "", Ergebnis.FT_NIO);
        }
        allentries=readLastIDs(mySerialDevice, System.currentTimeMillis(), 10*1000);
        last4entries=extractLast4ValidEntries(oldentries, allentries, RSSI_Filter, ModeAcceptFilter, ModeNoErrorFilter, IDFilter);
        
        if (m_debug_level>2) System.out.println("Read "+last4entries.length+" valid entries.");
        
        if (last4entries.length!=4)
        {
            if (isDE()) throw new CError("ExecFehler", "Nur "+last4entries.length+" Eintr�ge gefunden.", "", Ergebnis.FT_NIO);
            else        throw new CError("ExecFehler", "Found only "+last4entries.length+" entries.", "", Ergebnis.FT_NIO);
        }
        return last4entries;
    }

    /** Zyklisches auslesen der RDC IDs aus dem Handterminal.
     * Sobald 4 (neue) IDs vorhanden sind, ist die PP fertig.
     * 
     * Ansonsten Abbruch nach Timeout oder auf Benutzerwunsch.
     * 
     * @param mySerialDevice serielle Schnittstelle
     * @param awt Anweisung
     * @param FinalTimeout Timeout
     * @param RSSI_Filter Minimalwert f�r RSSI
     * @param ModeAcceptFilter Filter f�r Mode 
     * @param ModeNoErrorFilter Filter f�r Mode
     * @param IDFilter IDFilter f�r RDC ID (null=alle IDs)
     * @return 4 RDC Eintr�ge
     * @throws CError Fehlerobjekt, falls Timeout, Benutzerabbruch oder Lesefehler der seriellen Schnittstelle
     */
    private RDC_Entry[] readIDsCyclic(SerialGeneric mySerialDevice, String awt, int FinalTimeout, int RSSI_Filter, String ModeAcceptFilter, String ModeNoErrorFilter, long[][] IDFilter) throws CError
    {
    long starttime, nowtime;
    RDC_Entry[] allentries;
    RDC_Entry[] last4entries;
    RDC_Entry[] last4entriesNoFilter;
    int entriesread, entriesreadNoFilter;
    CError readerror;
    
        starttime=System.currentTimeMillis();

        if (m_handheld_type==1)
        {
            clearentries_withRetry(mySerialDevice, starttime, FinalTimeout);
        }
    
        Set oldentries=getOldEntries(mySerialDevice, starttime, FinalTimeout);
    
        showText("RDC", awt);
        
        entriesread=0;
        entriesreadNoFilter=0;
        while (true)
        {
            readerror=null;
            try
            {
                allentries=readLastIDs(mySerialDevice, starttime, FinalTimeout);
            }
            catch (CError err)
            {
                allentries=null;
                showText( "RDC", awt+"\r\n>> Error <<");
                readerror=err;
            }
            
            if (allentries!=null)
            {
                last4entries=extractLast4ValidEntries(oldentries, allentries, RSSI_Filter, ModeAcceptFilter, ModeNoErrorFilter, IDFilter);
                last4entriesNoFilter=extractLast4ValidEntriesNoFilter(oldentries, allentries);
                entriesread=last4entries.length;
                entriesreadNoFilter=last4entriesNoFilter.length;

                if (m_debug_level>2)
                {
                    System.out.println("Read "+entriesread+" valid entries up to now.");
                    System.out.println(""+entriesreadNoFilter+" Entries when ignoring filters.");
                }
            
                String str=""+entriesread;
                if (entriesread!=entriesreadNoFilter) str+=" ("+entriesreadNoFilter+")";
                showText( "RDC", awt+"\r\n>> "+str+" <<");
            
                if (last4entries.length==4) return last4entries;
            }
            
            try
            {
                Thread.sleep(2000);
            }
            catch (InterruptedException e) { }
            
            if (isCancelled())
            {
                if (readerror!=null) throw readerror;
                if (isDE()) throw new CError("ExecFehler", "Benutzerabbruch beim Einlesen der RDCs.", "Bis jetzt "+entriesread+" eingelesen (die Filter passieren). "+entriesreadNoFilter+" ohne Filter.", Ergebnis.FT_NIO);
                else        throw new CError("ExecFehler", "User abort during reading of RDCs.", "Read "+entriesread+" up to now (passing filter). "+entriesreadNoFilter+" without filter.", Ergebnis.FT_NIO);
            }
            
            nowtime=System.currentTimeMillis();
            if (nowtime-starttime>FinalTimeout) 
            {
                if (readerror!=null) throw readerror;
                if (isDE()) throw new CError("ExecFehler", "Timeout beim Einlesen der RDCs.", "Bis jetzt "+entriesread+" eingelesen (die Filter passieren). "+entriesreadNoFilter+" ohne Filter.", Ergebnis.FT_NIO);
                else        throw new CError("ExecFehler", "Timeout during reading of RDCs.", "Read "+entriesread+" up to now (passing filter). "+entriesreadNoFilter+" without filter.", Ergebnis.FT_NIO);
            }
        }
    }

    /** Holt die bei PREPARE ermittelten Eintr�ge des Handterminals aus den dyn. Attributen
     * 
     * @return alte Eintr�ge im Handterminal (SerNum_Timestamp)
     */
    private Set getOldEntriesFromAtt()
    {
    Pruefling pl;
    String   att_entry;
    Set  erg=new HashSet();
    String[] keys;
    int i;
    
        pl = getPr�fling();
        Map ht = pl.getAllAttributes();
        
        att_entry=(String)ht.get("RDC_OLDENTRIES");

        if (m_debug_level > 5) System.out.println("getDynamicAttribute(RDC_OLDENTRIES,"+att_entry+")");
        
        if (att_entry==null) return erg;
        att_entry=att_entry.trim();
        if (att_entry.length()==0) return erg;
        
        keys=att_entry.split(";");
        for (i=0; i<keys.length; i++)
        {
            erg.add(keys[i].trim());
        }
        return erg;
    }
    
    /** Liest alle Eintrage des Handterminals aus und 
     * liefert sie als String-Set mit Eintr�gen der Form
     * SerNum_Timestamp.
     * Sinn: Sp�ter kann ermittelt werden, welche eintr�ge im Handterminal neu dazu gekommen sind.
     * (Und nur die neu hinzugekommenen interessieren)
     * 
     * @param mySerialDevice serielle Schnittstelle
     * @param starttime Startzeitpunkt der PP
     * @param FinalTimeout Timeout nachdem die PP mit Fehler abbricht
     * @return Eintrage des Handterminals
     * @throws CError Fehlerobjekt-
     */
    private Set getOldEntries(SerialGeneric mySerialDevice, long starttime, int FinalTimeout) throws CError
    {
    int i;
    Set oldentries=new HashSet();
    String key;
    RDC_Entry[] allentries;
        
        i=0;
        while (true)
        {
            try
            {
                allentries=readLastIDs(mySerialDevice, starttime, FinalTimeout);
                break;
            }
            catch (CError err)
            {
                i++;
                if (i>3) throw err;
            }
        }
        
        for (i=0; i<allentries.length; i++)
        {
            if (!allentries[i].isEmpty())
            {
                key=allentries[i].getSerialNo()+"_"+allentries[i].getTimeStamp();
                oldentries.add(key);
            }
        }
        
        if (m_debug_level>6) System.out.println("Found "+oldentries.size()+" old entries.");
        
        return oldentries;
    }
    
    /** Liest die letzten 10 Eintr�ge des Handterminals 
     * 
     * @param mySerialDevice serielle Schnittstelle
     * @param starttime Startzeitpunkt der PP
     * @param FinalTimeout Timeout nachdem die PP mit Fehler abbricht
     * @return letzten 10 RDC Eintr�ge
     * @throws CError Fehlerobjekt
     */
    private RDC_Entry[] readLastIDs(SerialGeneric mySerialDevice, long starttime, int FinalTimeout) throws CError
    {
    byte[] buffer;
    String str;
    int i;
    int pos0;
    int pos;
    int endpos1, endpos2, endpos;
    String line;
    Vector lines;
    RDC_Entry[] erg;
    int Format;
    String crcstring;
    
        buffer=command_execute_withRetry(mySerialDevice, "last", TIMEOUT_SERIAL, starttime, FinalTimeout, false);
        str=new String(buffer);
        Format=0;
        if (str.indexOf("RLRot")>=0) Format=1;
        lines=new Vector();
        
        crcstring="";
        pos0=str.indexOf(" 0:");
        //pos1=str.indexOf(" 1:"); // Workaround: Die neuen Handhelds liefern 1: 2: 3: 4: statt 0: 1: 2: 3:
        if (pos0<0)
        {
            if (isDE()) throw new CError("ExecFehler", "Kommando 'last' fehlgeschlagen", "Zeile 0 nicht gefunden.", Ergebnis.FT_NIO);                
            else        throw new CError("ExecFehler", "Command 'last' failed", "Line 0 not found.", Ergebnis.FT_NIO);
        }
        //if (pos0<0) pos0=Integer.MAX_VALUE;
        //if (pos1<0) pos1=Integer.MAX_VALUE;
        pos=pos0;
        //if (pos1<pos0) pos=pos1;
                
        i=0;
        while (true)
        {
            if (pos+2>=str.length()) break;
            else if (str.charAt(pos)!=' ' || str.charAt(pos+2)!=':') break;

            endpos1=str.indexOf("\r", pos);
            endpos2=str.indexOf("\n", pos);
            if (endpos1<0) endpos1=Integer.MAX_VALUE;
            if (endpos2<0) endpos2=Integer.MAX_VALUE;
            if (endpos1==Integer.MAX_VALUE && endpos2==Integer.MAX_VALUE)
            {
                if (isDE()) throw new CError("ExecFehler", "Kommando 'last' fehlgeschlagen", "Zeile "+i+" hat keine Terminierung.", Ergebnis.FT_NIO);
                else        throw new CError("ExecFehler", "Command 'last' failed", "Line "+i+" has no termination.", Ergebnis.FT_NIO);
            }
            if (endpos1<=endpos2) endpos=endpos1;
            else                  endpos=endpos2;
            line=str.substring(pos, endpos);
            if (m_debug_level>5) System.out.println("line "+i+": "+line);
            crcstring+=line+"\n";
            lines.add(new RDC_Entry(i, line, Format));
            pos=endpos;
            while (true)
            {
                if (pos>=str.length()) break;
                if (str.charAt(pos)!='\r' && str.charAt(pos)!='\n') break;
                pos++;
            }
            i++;
        }

        erg=new RDC_Entry[lines.size()];
        for (i=0; i<lines.size(); i++)
        {
            erg[i]=(RDC_Entry)lines.get(i);
            if (m_debug_level>10) System.out.println("Parsed RDC Entry: "+lines.get(i).toString());
        }

        if (str.indexOf("0x", pos)>=0 && pos+6<str.length())
        {
            boolean crc_ok;
            String crc_ma_str=str.substring(pos+2, pos+6);
            int crc_ma;
            int crc_own=crc16(crcstring.getBytes());
            if (crc_own<0) crc_own+=65536;
            if (m_debug_level>10)
            {
                System.out.println("CRC des MA: "+crc_ma_str);
                System.out.println("CRC selbst: "+Integer.toHexString(crc_own));
            }
            
            crc_ok=false;
            try
            {
                crc_ma=Integer.parseInt(crc_ma_str, 16);
                if (crc_ma==crc_own)
                {
                    send_ack(mySerialDevice, TIMEOUT_SERIAL, starttime, FinalTimeout);
                    crc_ok=true;
                }
            }
            catch (NumberFormatException ex)
            {
            }
            if (!crc_ok) 
            {
                if (isDE()) throw new CError("ExecFehler", "Kommando 'last' fehlgeschlagen", "Falsche checksumme", Ergebnis.FT_NIO);
                else        throw new CError("ExecFehler", "Command 'last' failed", "Wrong checksum", Ergebnis.FT_NIO);
            }
        }
        

        return erg;
    }

    /** Schickt das "setclear" Kommando an den MiniAnalyzer
     * und erwartet ein "ack" als Antwort
     * 
     * @param mySerialDevice Serial Device
     * @param starttime Startzeitpunkt der PP
     * @param FinalTimeout Timeout nachdem die PP mit Fehler abbricht
     * @throws CError Fehlerobjekt
     */
    private void clearentries_withRetry(SerialGeneric mySerialDevice, long starttime, int FinalTimeout) throws CError
    {
    int counter;
    
        counter=0;
        while (true)
        {
            try
            {
                clearentries(mySerialDevice, starttime, FinalTimeout);
                return;
            }
            catch (CError err)
            {
                counter++;
                if (counter>=3) throw err;
                if (isCancelled()) throw err;

                if (m_debug_level>2)
                {
                    System.out.println("clearentries failed: "+err.getMessage()+"\r\n=> Retry");
                }
            }
            
            try
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException e) { }
        }
    }
    
    /** Schickt das "setclear" Kommando an den MiniAnalyzer
     * und erwartet ein "ack" als Antwort
     * 
     * @param mySerialDevice Serial Device
     * @param starttime Startzeitpunkt der PP
     * @param FinalTimeout Timeout nachdem die PP mit Fehler abbricht
     * @throws CError Fehlerobjekt
     */
    private void clearentries(SerialGeneric mySerialDevice, long starttime, int FinalTimeout) throws CError
    {
    byte[] buffer;
    String str;
    
        buffer=command_execute_withRetry(mySerialDevice, "setclear", TIMEOUT_SERIAL, starttime, FinalTimeout, true);
        
        str=new String(buffer);
        
        if (str.toUpperCase().indexOf("ACK")<0)
        {
            if (isDE()) throw new CError("ExecFehler", "Kommando 'setclear' fehlgeschlagen", "Kein ACK", Ergebnis.FT_NIO);
            else        throw new CError("ExecFehler", "Command 'setclear' failed", "No ACK", Ergebnis.FT_NIO);
        }
    }

    /** F�hrt ein Kommando des Handterminals aus.
     * Falls Fehler und WerkerDialog vorhanden:
     * Fehler anzeigen und wiederholen bis Timeout erreicht
     * 
     * @param mySerialDevice serielle Schnittstelle
     * @param command Kommandoname
     * @param SerialTimeout Timeout 
     * @param starttime Startzeitpunkt der PP
     * @param FinalTimeout Timeout nachdem die PP mit Fehler abbricht
     * @param AtBeginning true: Zusatzhinweis bei Fehler: Handheld eingschaltet?
     * @return Eingelesene Bytes
     * @throws CError Fehlerobjekt
     */
    private byte[] command_execute_withRetry(SerialGeneric mySerialDevice, String command, int SerialTimeout, long starttime, int FinalTimeout, boolean AtBeginning) throws CError
    {
    long nowtime;
    int counter=0;
    
        while (true)
        {
            try
            {
                return command_execute_once(mySerialDevice, command, SerialTimeout, AtBeginning);
            }
            catch (CError err)
            {
                if (m_myDialog==null) throw err;
                showText("RDC", err.getMessage());
                if (isCancelled()) throw err;
                nowtime=System.currentTimeMillis();
                if (nowtime-starttime>FinalTimeout) throw err;
                if (counter%10==0)
                {
                    if (m_debug_level>2)
                    {
                        System.out.println("command_execute_once: "+err.getMessage()+"\r\n=> Retry");
                    }
                }
            }
            
            counter++;
            
            try
            {
                Thread.sleep(200);
            }
            catch (InterruptedException e) { }
        }
    }
    
    /** F�hrt ein Kommando des Handterminals aus
     * 
     * @param mySerialDevice serielle Schnittstelle
     * @param command Kommandoname
     * @param Timeout Timeout 
     * @param AtBeginning true: Zusatzhinweis bei Fehler: Handheld eingschaltet?
     * @return Eingelesene Bytes
     * @throws CError Fehlerobjekt
     */
    private byte[] command_execute_once(SerialGeneric mySerialDevice, String command, int Timeout, boolean AtBeginning) throws CError
    {
    int i;
    byte[] buffer;
    byte[] tmpbuffer;
    byte[] completebuffer; 
    long starttime1, starttime2, nowtime;
         
        if (m_debug_level>5) System.out.println("COMMAND \""+command+"\"");
        
        try
        {
            mySerialDevice.writeData((command+"\r").getBytes());
        }
        catch (DeviceIOException ex)
        {
            if (isDE()) throw new CError( "ExecFehler", "SerialWrite fehlgeschlagen", ex.getMessage(), Ergebnis.FT_NIO);
            else        throw new CError( "ExecFehler", "SerialWrite failed", ex.getMessage(), Ergebnis.FT_NIO); 
        }
        
        if (m_debug_level>7) System.out.println("COMMAND sent, now reading");
    
        
        starttime1=System.currentTimeMillis();
        starttime2=starttime1;
        completebuffer=new byte[0];
        while (true)
        {
            try
            {
                buffer=mySerialDevice.readData();
            }
            catch (DeviceIOException ex)
            {
                if (isDE()) throw new CError( "ExecFehler", "SerialRead fehlgeschlagen", ex.getMessage(), Ergebnis.FT_NIO);
                else        throw new CError( "ExecFehler", "SerialRead failed", ex.getMessage(), Ergebnis.FT_NIO); 
            }
            
            nowtime=System.currentTimeMillis();
            
            if (buffer!=null)
            {
    
                if (m_debug_level>12)
                {
                    System.out.println("Bufferpart:");
                    System.out.println(new String(buffer));
                    for (i=buffer.length-5; i<buffer.length; i++)
                    {
                        if (i<0) continue;
                        System.out.print(" "+Integer.toHexString((buffer[i]+256)%256));
                    }
                    System.out.println();
                }
    
                //completebuffer=completebuffer+buffer
                tmpbuffer=new byte[buffer.length+completebuffer.length];
                for (i=0; i<completebuffer.length; i++)
                {
                    tmpbuffer[i]=completebuffer[i];
                }
                for (i=0; i<buffer.length; i++)
                {
                    tmpbuffer[i+completebuffer.length]=buffer[i];
                }
                completebuffer=tmpbuffer;
                
                if (completebuffer.length>5)
                {
                    if (completebuffer[completebuffer.length-2]=='>' &&
                        completebuffer[completebuffer.length-1]==' ') break;
                    
                    if (completebuffer[completebuffer.length-3]=='>' &&
                        completebuffer[completebuffer.length-2]==' ') break;
                    
                    if (completebuffer[completebuffer.length-4]=='>' &&
                        completebuffer[completebuffer.length-3]==' ') break;
                }
                
                starttime2=System.currentTimeMillis();
            }
            else
            {
                if (nowtime-starttime2>Timeout)
                {
                    String txt1, txt2;
                        
                    if (isDE()) 
                    {
                        txt1="SerialRead fehlgeschlagen";
                        txt2="COM-Timeout";
                    }
                    else        
                    {
                        txt1="SerialRead failed";
                        txt2="COM-Timeout";
                    }
                    
                    if (AtBeginning)
                    {
                        txt2+="\r\n";
                        if (isDE()) txt2+="Handheld eingeschaltet?";
                        else        txt2+="Handheld power on?";
                    }
                    throw new CError("ExecFehler", txt1, txt2, Ergebnis.FT_NIO); 
                }
                try
                {
                    Thread.sleep(100);
                }
                catch (InterruptedException e) { }
            }
            if (nowtime-starttime1>(Timeout*10) ||
                completebuffer.length>1024*1024)
            {
                if (isDE()) throw new CError("ExecFehler", "SerialRead fehlgeschlagen", "Zu viele Daten", Ergebnis.FT_NIO);
                else        throw new CError("ExecFehler", "SerialRead failed", "Too much data", Ergebnis.FT_NIO);
            }
        }
        
        if (m_debug_level>7) System.out.println("COMMAND read finished");
        if (m_debug_level>12)
        {
            System.out.println("Complete Buffer:");
            System.out.println(new String(completebuffer));
        }
        return completebuffer;
    }
    
    /** Schickt ACK an das Handheld 
     * 
     * @param mySerialDevice serielle Schnittstelle
     * @param starttime Startzeitpunkt der PP
     * @param FinalTimeout Timeout nachdem die PP mit Fehler abbricht
     * @param timeout Timeout 
     */
    protected void send_ack(SerialGeneric mySerialDevice, int timeout, long starttime, int FinalTimeout) 
    {
        try
        {
            command_execute_withRetry(mySerialDevice, "ack", timeout, starttime, FinalTimeout, false);
        }
        catch (CError err) {}

        try
        {
            Thread.sleep(100);
        }
        catch (InterruptedException e) { }
    }

    /** Ermittelt aus den letzten 10 Eintr�gen des Handterminals die max. 4 neuen.
     * Ignoriert werden
     * - leere Eintr�ge 
     * - alte Eintr�ge; stehen in OldSerNums in der Form SerienNummer_Timestamp
     * - doppelte neue Eintr�ge mit gleicher Seriennummer
     * 
     * 
     * @param OldSerNums alte Eintr�ge, die ignoriert werden sollen
     * @param allentries Array mit allen Eintr�gen
     * @return Liste der neuen Eintr�ge (max. 4)
     * @throws CError Fehlerobjekt
     */
    private RDC_Entry[] extractLast4ValidEntriesNoFilter(Set OldSerNums, RDC_Entry[] allentries) throws CError
    {
        HashSet KnownIDs=new HashSet();
        Vector validentries=new Vector();
        RDC_Entry[] erg;
        int i;
        RDC_Entry entry;
        String id, key;
            
            for (i=0; i<allentries.length; i++)
            {
                entry=allentries[i];
                if (entry.isEmpty()) continue;
                
                id=entry.getSerialNo();
                if (OldSerNums!=null)
                {
                    key=id+"_"+entry.getTimeStamp();
                    if (OldSerNums.contains(key)) continue;
                }
                
                if (KnownIDs.contains(id)) continue;
                
                KnownIDs.add(id);
                validentries.add(entry);
                if (validentries.size()==4) break;
            }
        
            erg=new RDC_Entry[validentries.size()];
            for (i=0; i<validentries.size(); i++)
            {
                erg[i]=(RDC_Entry)validentries.get(i);
            }
            
            return erg;        
    }
    
    /** Ermittelt aus den letzten 10 Eintr�gen des Handterminals die max. 4 neuen.
     * Ignoriert werden
     * - leere Eintr�ge 
     * - alte Eintr�ge; stehen in OldSerNums in der Form SerienNummer_Timestamp
     * - doppelte neue Eintr�ge mit gleicher Seriennummer
     * 
     * 
     * @param OldSerNums alte Eintr�ge, die ignoriert werden sollen
     * @param allentries Array mit allen Eintr�gen
     * @param RSSI_Filter Minimalwert f�r RSSI
     * @param ModeAcceptFilter Filter f�r Mode 
     * @param ModeNoErrorFilter Filter f�r Mode
     * @param IDFilter IDFilter f�r RDC ID (null=alle IDs)
     * @return Liste der neuen Eintr�ge (max. 4)
     * @throws CError Fehlerobjekt
     */
    private RDC_Entry[] extractLast4ValidEntries(Set OldSerNums, RDC_Entry[] allentries, int RSSI_Filter, String ModeAcceptFilter, String ModeNoErrorFilter, long[][] IDFilter) throws CError
    {
    HashSet KnownIDs=new HashSet();
    Vector validentries=new Vector();
    RDC_Entry[] erg;
    int i;
    RDC_Entry entry;
    String id, key;
        
        for (i=0; i<allentries.length; i++)
        {
            entry=allentries[i];
            if (entry.isEmpty()) continue;
            
            id=entry.getSerialNo();
            if (OldSerNums!=null)
            {
                key=id+"_"+entry.getTimeStamp();
                if (OldSerNums.contains(key)) continue;
            }

            if (entry.getRSSI()<RSSI_Filter) continue;
            if (!passIDFilter(id, IDFilter)) continue;
            if (!passFilter(entry.getMode(), ModeAcceptFilter)) continue;
            if (!passFilter(entry.getMode(), ModeNoErrorFilter))
            {
                String modebin=Integer.toBinaryString(entry.getMode());
                while (modebin.length()<8) modebin="0"+modebin;
                if (isDE()) throw new CError("ExecFehler", "Mode 0x"+Integer.toHexString(entry.getMode())+"="+modebin+" wird von Filter "+ModeNoErrorFilter+" abgelehnt.", "", Ergebnis.FT_NIO);
                else        throw new CError("ExecFehler", "Mode 0x"+Integer.toHexString(entry.getMode())+"="+modebin+" is rejected by filter "+ModeNoErrorFilter+".", "", Ergebnis.FT_NIO);
            }
            
            if (KnownIDs.contains(id)) continue;
            
            KnownIDs.add(id);
            validentries.add(entry);
            if (validentries.size()==4) break;
        }
    
        erg=new RDC_Entry[validentries.size()];
        for (i=0; i<validentries.size(); i++)
        {
            erg[i]=(RDC_Entry)validentries.get(i);
        }
        
        return erg;
    }
    
    /** Pr�ft, ob id innerhalb eines der angegeben Bereiche liegt
     * 
     * @param id ID als Zahlstring (wenn keine Zahl -> Result true)
     * @param IDFilter Zahlenbereiche: inneres Array hat gr��e 2: [j][0] ist untere Grenze, [j][1] ist obere Grenze
     *                 null: -> Result true
     * @return true: id liegt in einem der Bereiche
     */
    private boolean passIDFilter(String id, long[][] IDFilter)
    {
    long idnum;
    int j;
    boolean erg;

        erg=true;
        try
        {
            idnum=Long.parseLong(id);
            if (IDFilter!=null)
            {
                erg=false;
                for (j=0; j<IDFilter.length; j++)
                {
                    if (idnum>=IDFilter[j][0] && idnum<=IDFilter[j][1])
                    {
                        erg=true;
                        break;
                    }
                }
            }
        }
        catch (NumberFormatException ex) {}
        return erg;
    }
    
    /** Pr�ft, ob ein byte einem 01X Filter entspricht
     * 
     * @param TheByte zu pr�fendes Byte
     * @param Filter Filter der Form [01X] mal 8, z.B. 010X000X
     * @return true: Byte passt zu Filter
     */
    private boolean passFilter(int TheByte, String Filter)
    {
    String ByteAsBin;
    int i;
    char c1, c2;
    
        if (Filter==null) return true;
        if (Filter.length()!=8) return true;
        
        ByteAsBin=Integer.toBinaryString(TheByte);
        if (ByteAsBin.length()>8) ByteAsBin=ByteAsBin.substring(ByteAsBin.length()-8);
        while (ByteAsBin.length()<8) ByteAsBin="0"+ByteAsBin;
        
        for (i=0; i<8; i++)
        {
            c1=ByteAsBin.charAt(i);
            c2=Filter.charAt(i);
            if (c1!=c2)
            {
                if (c2!='X' && c2!='x') return false;
            }
        }
        
        return true;
    }

    /** Speichert die RDC Eintr�ge (Seriennummern) in CascadeResults und dyn. Attributen.
     * Jeweils unter dem Namen RDC_SerialNoX
     * 
     * @param entries 4 RDC Eintr�ge 
     * @param ergListe Ergebnis Liste (CascadeResults)
     * @throws CError Fehlerobjekt
     */
    private void storeEntries(RDC_Entry[] entries, Vector ergListe) throws CError
    {
    int i;
    RDC_Entry entry;
    Ergebnis result;
    
        for (i=0; i<entries.length; i++)
        {
            entry=entries[i];
            result=entry.makeCascadeResult(i);
            ergListe.add(result);
            setDynamicAttribute(result.getErgebnis(), result.getErgebnisWert(), null);
        }
    }
    
    /** Erstellt WerkerDialog mit Abburch Button
     * 
     * @throws CError Fehlerobjekt
     */
    private void initDialog() throws CError
    {
        if (m_myDialog == null) 
        {
            try
            {
                m_myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
                m_myDialog.setAllowCancel(true);
            }
            catch (Exception e) 
            {
                System.out.println("ERROR in readIDsCyclic: getUserDialog "+e.getMessage());
                e.printStackTrace(System.out);
                m_myDialog = null;
                throw new CError("ExecFehler", "getUserDialog failed", e.getMessage(), Ergebnis.FT_NIO_SYS);
            }
        }
    }
    
    /** Text in Werkerdialog anzeigen
     * 
     * @param Title Titel
     * @param awt Text
     */
    private void showText(String Title, String awt)
    {
        if (m_myDialog != null) 
        {
            m_myDialog.displayStatusMessage( Title, awt, -1 );
        }
    }
    
    /** Wurde Abbrechen gedr�ckt?
     * 
     * @return true: Abbrechen wurde gedr�ckt
     */
    private boolean isCancelled()
    {
        if (m_myDialog!=null)
        {
            return m_myDialog.isCancelled();
        }
        return false;
    }

    /** Gibt jedweden WerkerDialog frei.
     * (Entfernt ihn vom Bildschirm 
     * @throws CError Fehlerobjekt 
     */
    private void releaseDialog() throws CError
    {
        if (m_myDialog != null) 
        {
            try 
            {
                getPr�flingLaufzeitUmgebung().releaseUserDialog();
            } 
            catch (Exception e) 
            {
                m_myDialog = null;
                System.out.println("ERROR: releaseDialog");
                e.printStackTrace(System.out);
                throw new CError("ExecFehler", "Release Dialog failed", e.getMessage(), Ergebnis.FT_NIO_SYS);
            }
            m_myDialog = null;
        }
    }

    /**
     * Assigns a given value to a specified dynamic attribute
     * @param label the name of the dynamic attribute
     * @param value the value of the dynamic attribute
     * @param storage defines the PL that will be used for storing dynamic attributes
     * @throws CError Error Object
     */
    private void setDynamicAttribute(String label, String value, String storage) throws CError 
    {
        if (m_debug_level > 5) System.out.println("setDynamicAttribute("+label+","+value+","+storage+")");
        
        Pruefling pl;   //reference to the storing PL
        
        if (storage != null) 
        {
            pl = getPr�fling().getAuftrag().getPr�fling(storage);   //get the reference to the storing PL
        } 
        else 
        {
            pl = getPr�fling();   //get the reference to the storing PL (use current PL)
        }
        if (pl == null) 
        {
            if (isDE()) throw new CError("ExecFehler", "Storage PL nicht gefunden", "", Ergebnis.FT_NIO_SYS);
            else        throw new CError("ExecError", "Storage-PL not found", "", Ergebnis.FT_NIO_SYS);
        }
        //get the hashtable of the PL where the attributes are stored
        Map ht = pl.getAllAttributes();
        if (ht == null) 
        {
            if (isDE()) throw new CError("ExecFehler", "Hashtable nicht vorhanden", "", Ergebnis.FT_NIO_SYS);
            else        throw new CError("ExecError", "Hashtable not found", "", Ergebnis.FT_NIO_SYS);
        }
        ht.put(label, value);   //put the pair (label/value) to the hashtable
    }

    /** CRC Checksummenberechnung 
     * 
     * @param data Inputdata
     * @return calculated checksum
     */
    static private short crc16(byte[] data)
    {
        int j, i;
        byte c;
        short crc = (short) 0x0000;       // initial contents of LFBSR
        for (j = 0; j < data.length; j++)
        {
            c = data[j];
            for (i = 0; i < 8; i++)
            {
                boolean c15 = ((crc >> 15      & 1) == 1);
                boolean bit = ((c   >> (7-i) & 1) == 1);
                crc <<= 1;
                if (c15 ^ bit) crc ^= 0x1021;   // 0001 0000 0010 0001  (0, 5, 12)
            }
        }
        return crc;
    }
    
    /* The same as crc16 ported from code from Beru
    static private short crc16_ (byte[] data)
    {
    byte temp;              // Temporary byte
    byte dbyte;             // Next data byte to look at
    byte b0;
    short CRCReg;           // CRC register
    int i, j;

        CRCReg = 0x0000;      // force 0x0000 seed for WIRELESS mode

        for (i = 0; i < data.length;  i++)                // For each byte in packet
        {
            dbyte  = data[i];             // Load next data byte to look at
            for (j = 0; j < 8; j++)       //     For each bit in byte
            {
                temp          = (byte)(((dbyte>>7)&1) ^ ((CRCReg>>15)&1));    // XOR top bit of data and top bit of CRC register
                CRCReg      <<= 1; //  Shift CRC Register up one bit
                dbyte       <<= 1; //  Shift data up one bit
                CRCReg = (short)(CRCReg | temp);  //   Write XOR result to bottom of CRC register
                b0=(byte)(CRCReg&1);
                CRCReg^=(b0<<5);
                CRCReg^=(b0<<12);
            }
        }

        return CRCReg;
    }
    */
    
    /** Ermittelt Sprache
     * 
     * @return true: Deutsch, false: Englisch
     */
    private static boolean isDE() 
    {
        try 
        {
            if (CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true ) return true;
            else return false;
        }
        catch (Exception e) 
        {
            return false;   //default is english
        }
    }
    
    /** Hilfsklasse, speichert alle Daten eines RDC Sensors 
     */
    private class RDC_Entry
    {
        /** Position 0-9 in der Liste im Handterminal */
        private int m_Pos=-1;
        /** Channel */
        private String m_Chan=null;
        /** Seriennummer */
        private String m_SerialNo=null;
        /** Druck */
        private int m_Pact=-1;
        /** Temperatur */
        private int m_Tact=-1;
        /** RSSI */
        private int m_Rssi=-1;
        /** RBL */
        private int m_RBL=-1;
        /** Mode */
        private int m_Mode=-1;
        /** RLRot */
        private int m_RLRot=-1;
        /** Zeitstempel */
        private long m_Timestamp=-1;
        
        
        /** Konstruktor
         * 
         * @param linenr Zeilennummer (0-9)
         * @param line Zeile mit Daten so wie aus dem Handterminal ausgelesen, z.B.:
         *  
         * Format 0:
         * 0:   -- 4200017114 14406   28   83 246 0x40  15766593 
         * 
         * Format 1 (mit RLRot):
         * 0:   -- 4200017114 14406   28   83 246 0x40  0x08  15766593
         * 
         * @param Format  0: altes Format ohne Spalte RLRot
         *                1: neues Format mit Spalte RLRot
         * 
         */
        public RDC_Entry(int linenr, String line, int Format)
        {
        int i, pos;
        String Element;
        
            if (line==null) return;
            String[] Elements=line.split(" ");
            if (m_debug_level>16) 
            {
                System.out.println("Format: "+Format);
                System.out.println("NrOfElements: "+Elements.length);
                for (i=0; i<Elements.length; i++) System.out.println("  >>"+Elements[i]+"<<");
            }
            
            pos=0;
            for (i=0; i<Elements.length; i++)
            {
                Element=Elements[i].trim();
                if (Element.length()==0) continue;
                if (m_debug_level>15)
                {
                    System.out.println(""+i+"/"+pos+": ||"+Element+"||");
                }

                
                switch (pos)
                {
                case 0:
                    try
                    {
                        Element=Element.replaceAll(":", "");
                        m_Pos=linenr;
                    }
                    catch (NumberFormatException ex)
                    {
                    }
                    break;

                case 1:
                    m_Chan=Element;
                    break;
                    
                case 2:
                    m_SerialNo=Element;
                    break;
                
                case 3:
                    try
                    {
                        m_Pact=Integer.parseInt(Element);
                    }
                    catch (NumberFormatException ex)
                    {
                    }
                    break;
                
                case 4:
                    try
                    {
                        m_Tact=Integer.parseInt(Element);
                    }
                    catch (NumberFormatException ex)
                    {
                    }
                    break;

                case 5:
                    try
                    {
                        m_Rssi=Integer.parseInt(Element);
                    }
                    catch (NumberFormatException ex)
                    {
                    }
                    break;

                case 6:
                    try
                    {
                        m_RBL=Integer.parseInt(Element);
                    }
                    catch (NumberFormatException ex)
                    {
                    }
                    break;

                case 7:
                    try
                    {
                        Element=Element.toLowerCase();
                        if (Element.startsWith("0x"))
                        {
                            Element=Element.substring(2);
                            m_Mode=Integer.parseInt(Element, 16);
                        }
                        else
                        {
                            m_Mode=Integer.parseInt(Element);
                        }
                    }
                    catch (NumberFormatException ex)
                    {
                    }
                    break;
                    
                default:
                    if ((Format==0 && pos==8) ||
                        (Format==1 && pos==9) )
                    {
                        try
                        {
                            m_Timestamp=Long.parseLong(Element);
                        }
                        catch (NumberFormatException ex)
                        {
                        }
                        return;
                    }
                    else if (Format==1 && pos==8) 
                    {
                        try
                        {
                            Element=Element.toLowerCase();
                            if (Element.startsWith("0x"))
                            {
                                Element=Element.substring(2);
                                m_RLRot=Integer.parseInt(Element, 16);
                            }
                            else
                            {
                                m_RLRot=Integer.parseInt(Element);
                            }
                        }
                        catch (NumberFormatException ex)
                        {
                        }
                        break;
                    }
                    else return;
                }
                pos++;
            }
        }
        
        /** ist Eintrag leer (keine Serienummer oder kein Zeitstempel) 
         * 
         * @return true: Eintrag ist leer
         */
        public boolean isEmpty()
        {
            if (m_SerialNo==null) return true;
            if (m_SerialNo.length()==0) return true;
            if (m_Timestamp<=0) return true;
            return false;
        }
        
        /** Liefert Seriennummer
         * 
         * @return Seriennummer
         */
        public String getSerialNo()
        {
            return m_SerialNo;
        }
        
        /** Liefert Zeitstempel
         * 
         * @return Zeitstempel
         */
        public long getTimeStamp()
        {
            return m_Timestamp;
        }
        
        /** Liefert RSSI (Empfangst�rke, negativ) 
         * 
         * @return RSSI
         */
        public int getRSSI()
        {
            return -m_Rssi;
        }
        
        /** Liefert Mode
         * 
         * @return Mode
         */
        public int getMode()
        {
            return m_Mode;
        }
        
        public String toString()
        {
        String erg="";
        String str;
        
            erg=""+m_Pos+": ";
            
            str=m_Chan;
            if (str==null) str="";
            while (str.length()<3) str=str+" ";
            erg+=str;
            
            erg+="|";
            
            str=m_SerialNo;
            if (str==null) str="";
            while (str.length()<11) str=" "+str;
            erg+=str;

            erg+="|";

            str=""+m_Pact;
            while (str.length()<5) str=" "+str;
            erg+=str;
            
            erg+="|";

            str=""+m_Tact;
            while (str.length()<5) str=" "+str;
            erg+=str;
            
            erg+="|";

            str=""+m_Rssi;
            while (str.length()<3) str=" "+str;
            erg+=str;

            erg+="|";

            str=""+m_RBL;
            while (str.length()<3) str=" "+str;
            erg+=str;

            erg+="|";

            if (m_Mode<0) str="----";
            else
            {
                str="0x"+Integer.toHexString(m_Mode);
                while (str.length()<4) str=" "+str;
            }
            erg+=str;

            erg+="|";
            
            if (m_RLRot<0) str="----";
            else
            {
                str="0x"+Integer.toHexString(m_RLRot);
                while (str.length()<4) str=" "+str;
            }
            erg+=str;

            erg+="|";

            str=""+m_Timestamp;
            while (str.length()<10) str=" "+str;
            erg+=str;
            
            return erg;
        }
        
        /** Erzeugt aus dem RDC Eintrag ein CascadeResult
         * Wertname=RDC_SerialNoX
         * Wertvalue=<m_SerialNo>
         * 
         * @param pos Position des Eintrags (0-3)
         * @return CascadeResult
         */
        public Ergebnis makeCascadeResult(int pos)
        {
        Ergebnis result;
        
            result=new Ergebnis("RDC_SERIALNO"+pos, "RDC_TERMINAL", "", "", "", "RDC_SERIALNO"+pos, m_SerialNo, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
            return result;
        }
    }
    
    /** Fehlerobjekt  */
    private class CError extends Exception
    {
    /** */
    private static final long serialVersionUID=-5654634010303049207L;
    /** Cascadeergebnis */
    Ergebnis m_entry;

        /** Konstruktor (Fehlertyp ist FT_NIO)
         * 
         * @param ID FehlerID
         * @param Fehlertext Fehlertext
         * @param Hinweistext Hinweistext
         */
        public CError(String ID, String Fehlertext, String Hinweistext)
        {
            m_entry=new Ergebnis(ID, "", "", "", "", "", "", "", "", "0", "", "", "", Fehlertext, Hinweistext, Ergebnis.FT_NIO );                      
        }

        /** Konstruktor 
         * 
         * @param ID FehlerID
         * @param Fehlertext Fehlertext
         * @param Hinweistext Hinweistext
         * @param ErrorType FehlerTyp (z.B. Ergebnis.FT_NIO)
         */
        public CError(String ID, String Fehlertext, String Hinweistext, String ErrorType)
        {
            m_entry=new Ergebnis(ID, "", "", "", "", "", "", "", "", "0", "", "", "", Fehlertext, Hinweistext, ErrorType );
            
        }
        
        /** Liefert das CascadeErgebnis 
         * 
         * @return Cascadeergebnis
         */
        public Ergebnis getEntry()
        {
            return m_entry;
        }
        
        public String getMessage()
        {
            return m_entry.getFehlerText()+"\r\n"+m_entry.getHinweisText();
        }
    }
}



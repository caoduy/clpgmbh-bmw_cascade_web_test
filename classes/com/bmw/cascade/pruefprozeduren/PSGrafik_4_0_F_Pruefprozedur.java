/*
 * PSGrafik_V0_0_1_FU_Pruefprozedur.java
 *
 * Created:          29.03.2004
 * Last modified:    25.10.2005
 *
 * Author:           Andreas Mair, BMW TI-432
 * Contact:          Tel.: +49/89/382-11992
 */

package com.bmw.cascade.pruefprozeduren;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.*;
import java.util.*;

/**
 * Testprocedure in the Cascade-System that gives access to common
 * grafical functions.
 * @author Andreas Mair, BMW TI-432
 * @version 0_0_1_FA 27.05.2004 AM  Ersterstellung.<BR>
 * @version 3_0_F 25.10.2005 WB Erweiterung der PP um Statusprotokollierung zu APDM und virtuellem Fahrzeug
 * @version 4_0_F 03.06.2008 CS APDM Ergebnis Format Anpassung
 */
public class PSGrafik_4_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    	static final long serialVersionUID = 1L;
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public PSGrafik_4_0_F_Pruefprozedur() {}

  /**
     * Creates a new instance of PSGrafik.
     * @param pruefling "Test-item"-class containing this concrete test-procedure
     * @param pruefprozName name of the test-procedure
     * @param hasToBeExecuted logical condition if the test-procedure schould be executed
     */
    public PSGrafik_4_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
    }
    
    /**
     * Initializes the arguments.
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /**
     * Gets the names of the optional arguments.
     * @return string-array containing the optional arguments
     */
    public String[] getOptionalArgs() {
        String[] args = { "PARAM1" };
        return args;
    }
    
    /**
     * Gets the names of the mandatory arguments.
     * @return string-array containing the required arguments
     */
    public String[] getRequiredArgs() {
        String[] args = {"GRAPHIC_FUNCTION"};
        return args;
    }
    
    /**
     * Checks if
     * - all mandatory arguments are present
     * - no other arguments than the mandatory and optional arguments are used
     * @return true if the check was successful.
     */
    public boolean checkArgs() {
        try {
            return super.checkArgs();
        } catch (Exception e) {
            return false;
        } catch (Throwable e) {
            return false;
        }
    }
    
    /**
     * Executes the test-procedure
     * @param info Information about the execution.
     */
    public void execute(ExecutionInfo info) {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_ERROR;
 
        String sFunction = null;
        
        //APDM Ergebnis Format->nur in param3 d�rfen variable Werte stehen
        String param3="";
        
        try {
            if( checkArgs() == false ) throw new PPExecutionException();
            
            sFunction = getArg("GRAPHIC_FUNCTION");
            
            // Change of graphic display mode
            if( sFunction.equalsIgnoreCase("MODE") ) {
                String sGraphicMode = getArg("PARAM1");
                if( sGraphicMode != null ) {
                    if ( sGraphicMode.equalsIgnoreCase("OLD") )
                        getPr�flingLaufzeitUmgebung().setDisplayMode( PruefstandConstants.OLD_DISPLAY_MODE );
                    else if ( sGraphicMode.equalsIgnoreCase("GRAFIK") )
                        getPr�flingLaufzeitUmgebung().setDisplayMode( PruefstandConstants.GRAFIK_DISPLAY_MODE );
                    else if ( sGraphicMode.equalsIgnoreCase("LARGE") )
                        getPr�flingLaufzeitUmgebung().setDisplayMode( PruefstandConstants.LARGE_DISPLAY_MODE );
                    else if ( sGraphicMode.equalsIgnoreCase("VERY_LARGE") )
                        getPr�flingLaufzeitUmgebung().setDisplayMode( PruefstandConstants.VERY_LARGE_DISPLAY_MODE );
                    else if ( sGraphicMode.equalsIgnoreCase("FULL_SCREEN") )
                        getPr�flingLaufzeitUmgebung().setDisplayMode( PruefstandConstants.FULL_SCREEN_DISPLAY_MODE );
                    else if ( sGraphicMode.equalsIgnoreCase("DEFAULT") )
                        getPr�flingLaufzeitUmgebung().setDisplayMode( PruefstandConstants.DEFAULT_DISPLAY_MODE );
                    else {
                        getPr�flingLaufzeitUmgebung().setDisplayMode( PruefstandConstants.DEFAULT_DISPLAY_MODE );
                    }
                }
                else {
                    getPr�flingLaufzeitUmgebung().setDisplayMode( PruefstandConstants.DEFAULT_DISPLAY_MODE );
                }
                param3=sFunction + "; " + sGraphicMode;
                result = new Ergebnis( "PSGraphic", "Graphic", "", "", param3, "", "", "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
                ergListe.add(result);
                
            }
            // Switch active a screen
            else if( sFunction.equalsIgnoreCase("SWITCH_SCREEN") ) {
                String sScreen = getArg("PARAM1");
                if( sScreen!=null ) {
                    getPr�flingLaufzeitUmgebung().setScreen(sScreen);
                }
                param3=sFunction + "; " + sScreen;
                result = new Ergebnis( "PSGraphic", "Screen", "", "", param3, "", "", "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
                ergListe.add(result);

            }
            else {
                throw new PPExecutionException("INVALID GRAPHIC_FUNCTION");
            }
             status = STATUS_EXECUTION_OK;
 
        } catch( Exception e ) {
            result = new Ergebnis( "ExecFehler", "Screen", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "parameterexistenz" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
        }
//      Status setzen
        setPPStatus( info, status, ergListe );
    }
    
}

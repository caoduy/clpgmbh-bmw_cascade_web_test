/* 
* SDiagSNRCheck_1_0_F.java 
* 
* Created on 01.09.2015 
*/

package com.bmw.cascade.pruefprozeduren;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.util.XmlUtil;
import com.bmw.cascade.auftrag.Auftrag;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResult;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultSet;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.pruefumfang.Pruefumfang;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.util.CascadeDateFormat;

/**
 * Test procedure to read out the BMW part numbers of the vehicle sensors specified in the include-parameters 
 * via EDIBAS-job and build an IPS-Q-format XML PU-result-file for each sensor. 
 * The result files are send to APDM/IPSQ afterwards..<BR>
 * <BR>
 * @author BMW TI-545 Martin Saller (MS)<BR>
 * @author BMW TI-545 Achim Reckel (AR)<BR>
 * @version 1_0_F 01.09.2015 MS First Implementation<BR>
 *          	  18.02.2016 AR Changed name from "IPSQSachnr" to "SDiagSNRCheck".<BR>
 *          					Create separate XML result file for each sensor. <BR>
 *          					Cleaned up deprecated methods/classes.<BR>
 *          					Changed optional parameters to one input array (Sensor Nr, Kennung Einbauort).<BR>
 *          					Removed optional F_DEFAULT parameter.<BR>
 *          					Removed optional TESTSTAND parameter.<BR>
 *          					Added support for passing through sensor result via "@" parameter.<BR>
 * @version 2_0_T 19.05.2016 AR	Changed attribute �pruefumfangName� to constant value �SNR_CHECK�<BR>  
 * @version 3_0_F 19.05.2016 AR	Final Version of 2_0_T<BR>        
 * @version 4_0_T 20.06.2016 AR make file access to target directory more robust (NAS-issue)<BR>
 * 								changed functional EDIABAS job to physical jobs (minimizing runtime)<BR>
 * @version 5_0_T 05.07.2016 AR reduce EDIABAS-Jobs to one per physical ECU address<BR>
 * @version 6_0_F 07.07.2016 AR Final Version of 5_0_T<BR>
 * 								do not create result file if sensor result is "--"<BR>
 * @version 7_0_T 17.08.2016 AR "resValue = OK" and "errorCount = 0" for all tests, not depending on the current execution status of the PU anymore<BR>
 * 								improved APDM formatting, show part number for every sensor<BR>
 * @version 8_0_F 24.08.2016 AR Final Version of 7_0_T<BR>
 */
public class SDiagSNRCheck_8_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	long timeout = 10; // internal timeout for accessing target directory (in seconds)

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public SDiagSNRCheck_8_0_F_Pruefprozedur() {
	}

	/**
	   * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	   * @param pruefling Klasse des zugeh. Pr�flings
	   * @param pruefprozName Name der Pr�fprozedur
	   * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public SDiagSNRCheck_8_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 * 
	 * @return String Array der optionalen PP Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "SENSOR_PARA", "DEBUG", "APDM_DB_URI" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 * 
	 * @return String Array der zwingenden PP Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = {};
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert!!!
	 * 
	 * @return true, wenn Parameter Check i.O.
	 */
	@Override
	public boolean checkArgs() {
		// Obligatorisch.
		return super.checkArgs();
	}

	/** Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * Debug Flag
	 */
	private boolean debug = false;

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {

		// immer notwendig
		Ergebnis result = null;
		Vector<Ergebnis> resList = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// �bergabeparameter
		Map<String, String[]> sensorMap = null; // input list "SENSOR_PARA"
		String apdmPath = ""; // "APDM_DB_URI"

		// spezifische Variablen
		DeviceManager devMan = null; // Devicemanager
		EdiabasProxyThread ediabas = null;
		boolean executeEdiabas = false;
		String temp = "", sgbd = "", job = "", para = "", jobres = ""; // for EDIABAS-job

		boolean pingStatus = false; // status if APDM path is reachable
		String logPLName = "";
		Properties prop = new Properties();
		int itmp = 0;
		UserDialog myDialog = null;

		String resValue = "", errorCount = "", ppName = "", plName = "", errorType = "", xmlPath = "", resName = "", xmlName = "";
		Auftrag auftrag = null;
		Pruefumfang pruefumfang = null;
		String testStand = "";

		final boolean DE = checkDE(); // Systemsprache DE wenn true

		try {

			//Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG Parameter
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						debug = true;
					else if( !getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						throw new PPExecutionException( "DEBUG " + PB.getString( "parameter.ungueltigerWert" ) );
				} else {
					// alternativ DEBUG �ber Pr�fstandsvariable noch auswerten
					try {
						Object objDebug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
						if( objDebug != null ) {
							if( objDebug instanceof String && "TRUE".equalsIgnoreCase( (String) objDebug ) )
								debug = true;
							else if( objDebug instanceof Boolean && ((Boolean) objDebug).booleanValue() )
								debug = true;
						}
					} catch( VariablesException e ) {
						// nix, debug ist false vorbelegt
					}
				}

				// resolve input string from parameter "SENSOR_PARA" to sensorMap 
				// (with "SensorNR" as string key, "Kennung Einbauort", physical ECU address and possibly an external result as string values)
				sensorMap = new HashMap<String, String[]>();
				String[] sensorArray = null;
				if( getArg( "SENSOR_PARA" ) != null ) {
					sensorArray = getArg( "SENSOR_PARA" ).split( ";" );
					String[] sensorTemp = null;
					for( String s : sensorArray ) {
						sensorTemp = s.split( "," );
						if( sensorTemp.length == 3 ) { // expect exactly three elements
							String sensorKey = sensorTemp[0].trim(); // first argument = SensorNR = key
							String[] sensorValues = { sensorTemp[1].trim(), sensorTemp[2].trim() }; // KK and ECU_adr = values
							if( sensorMap.containsKey( sensorKey ) ) { // check for unique sensorKey
								log( debug, "Error", "Parameter Error: (" + s + ") - SensorNR must be unique!" );
								throw new PPExecutionException( "SENSOR_PARA " + PB.getString( "parameter.ungueltigerWert" ) );
							} else if( !sensorValues[1].startsWith( "0x" ) || sensorValues[1].length() != 4 ) { // check for formatting oh physical ECU address
								log( debug, "Error", "Parameter Error: (" + s + ") - ECU_adr must be formatted as hexadecimal value!" );
								throw new PPExecutionException( "SENSOR_PARA " + PB.getString( "parameter.ungueltigerWert" ) );
							} else {
								sensorMap.put( sensorKey, sensorValues );
								executeEdiabas = true;
							}
						} else if( sensorTemp.length == 4 && sensorTemp[3].contains( "@" ) ) { // result via @param as forth value
							String sensorKey = sensorTemp[0].trim(); // first argument = SensorNR = key
							// add "RESULT_"-prefix to SenorNR, result is already available so it should not be (accidentally) found in EDIABAS-ResultSets later
							sensorKey = "RESULT_" + sensorKey;
							// @-Operator, get result string with Sachnummer
							try {
								sensorTemp[3] = extractValues( sensorTemp[3].trim() )[0];
							} catch( PPExecutionException e ) {
								log( debug, "Error", "Parameter Error: couldn't resolve value with @-parameter" );
								e.printStackTrace();
								throw new PPExecutionException( "SENSOR_PARA " + PB.getString( "parameter.ungueltigerWert" ) );
							}
							String[] sensorValues = { sensorTemp[1].trim(), sensorTemp[2].trim(), sensorTemp[3] }; // KK, ECU_adr and result = values
							if( sensorMap.containsKey( sensorKey ) ) { // check for unique sensorKey
								log( debug, "Error", "Parameter Error: (" + s + ") - SensorNR must be unique!" );
								throw new PPExecutionException( "SENSOR_PARA " + PB.getString( "parameter.ungueltigerWert" ) );
							} else if( !sensorValues[1].startsWith( "0x" ) || sensorValues[1].length() != 4 ) { // check for formatting oh physical ECU address
								log( debug, "Error", "Parameter Error: (" + s + ") - ECU_adr must be formatted as hexadecimal value!" );
								throw new PPExecutionException( "SENSOR_PARA " + PB.getString( "parameter.ungueltigerWert" ) );
							} else
								sensorMap.put( sensorKey, sensorValues );
						} else {
							log( debug, "Error", "Parameter Error: (" + s + ") - wrong formatting" );
							throw new PPExecutionException( "SENSOR_PARA " + PB.getString( "parameter.ungueltigerWert" ) );
						}
					}
				} else {
					log( debug, "Error", "Parameter Error: include-list 'SENSOR_PARA' is empty" );
					throw new PPExecutionException( "SENSOR_PARA " + PB.getString( "parameter.ungueltigerWert" ) );
				}

				// set up APDM path	
				if( getArg( "APDM_DB_URI" ) != null && getArg( "APDM_DB_URI" ).length() >= 1 ) {
					apdmPath = getArg( "APDM_DB_URI" );
				} else {
					// get APDM URI Path from system properties
					apdmPath = com.bmw.cascade.server.PB.getString( "cascade.server.auftrag.reportxmlpath" ).replaceAll( "\\\\", "/" );
				}
				if( apdmPath.endsWith( "/" ) && (!apdmPath.endsWith( ":/" )) ) // da wir sp�ter etwas anh�ngen, muss ein abschlie�endes "/" entfernt werden (au�er bei C:/ etc.)
					apdmPath = apdmPath.substring( 0, apdmPath.length() - 1 );

				for( Map.Entry<String, String[]> entry : sensorMap.entrySet() ) {
					if( entry.getValue().length == 2 )
						log( debug, "Check parameter", "Parameter: SENSOR_PARA=<SensorNR: " + entry.getKey() + ", KK: " + entry.getValue()[0] + ", ECU_adr: " + entry.getValue()[1] + ">" );
					if( entry.getValue().length == 3 )
						log( debug, "Check parameter", "Parameter: SENSOR_PARA=<SensorNR: " + entry.getKey() + ", KK: " + entry.getValue()[0] + ", ECU_adr: " + entry.getValue()[1] + ", result SNR: " + entry.getValue()[2] + ">" );

				}
				log( debug, "Check parameter", "Parameter: APDM_DB_URI=<" + apdmPath + ">" );
				log( debug, "Check parameter", "Parameter: DEBUG=<" + debug + ">" );

			} catch( PPExecutionException e ) {
				if( debug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				resList.add( result );
				throw new PPExecutionException();
			}

			// get EDIABAS
			if( executeEdiabas ) {
				devMan = getPr�flingLaufzeitUmgebung().getDeviceManager(); // Devicemanager
				try {
					ediabas = devMan.getEdiabas();
				} catch( Exception e ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
					resList.add( result );
					status = STATUS_EXECUTION_ERROR;
					if( debug )
						e.printStackTrace();
					throw new PPExecutionException();
				}
			}

			log( debug, "Info", "VIN: " + this.getPr�fling().getAuftrag().getFahrgestellnummer7() );

			// create/check local dir
			String localDirPath = CascadeProperties.getCascadeHome() + "/database/Pruefstand/IPSQ"; // set IPSQ path
			log( debug, "Ok", "creating/checking local dir..." );
			createLocalDir( debug, localDirPath );

			pingStatus = checkDirectory( apdmPath ); // check if APDM path is reachable			

			// prepare IPSQ data
			log( debug, "Ok", "preparing IPSQ data..." );
			String xmlns = "http://bmw.com/standardResultData";
			String xmlnsXsi = "http://www.w3.org/2001/XMLSchema-instance";
			String xsiSchemaLocation = "http://bmw.com/standardResultData \\\\europe.bmw.corp\\winfs\\ti-proj\\EFA\\51_APDM\\a_System\\a_Systembeschreibung\\Standard-Ergebnisformat\\standardResultDataV0123.xsd";
			String xsiVersion = "01.23.00";

			pruefumfang = getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang();
			auftrag = getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag();
			testStand = getSubString( this.getPr�flingLaufzeitUmgebung().getName(), "@", null );

//			// wenn die Pr�fung i.O. ist oder der Fehler ignoriert werden kann, dann resValue = OK.
//			resValue = ((pruefumfang.getLastExecStatus() == Pruefumfang.STATUS_EXECUTION_OK) || (pruefumfang.getLastExecStatus() == Pruefumfang.STATUS_EXECUTION_ERROR_IGNORE)) ? "OK" : "NOK";
			// resValue always OK, not depending on the current execution status of the PU
			resValue = "OK";
			log( debug, "resValue", resValue );

			errorCount = (resValue.equalsIgnoreCase( "OK" )) ? "0" : "1";
			errorType = (resValue.equalsIgnoreCase( "OK" )) ? "K" : "F";

			ppName = getSubString( this.getClassName(), ".", "_Pruefprozedur" );
			plName = getSubString( this.getPr�fling().getClassName(), ".", "_Pruefling" );
			logPLName = getPr�fling().getName(); // get logical PL name

			// testResult node
			// --------------------------------------------------------------------------
			prop.setProperty( "testResult.xmlns", xmlns );
			prop.setProperty( "testResult.xmlns_xsi", xmlnsXsi );
			prop.setProperty( "testResult.xsi_schemaLocation", xsiSchemaLocation );

			// fileInfo node
			// ----------------------------------------------------------------------------
			prop.setProperty( "fileInfo.systemName", "Cascade" ); // req
			prop.setProperty( "fileInfo.softwareVersion", CascadeProperties.getCascadeVersion() );
			prop.setProperty( "fileInfo.resultFormatVersion", xsiVersion );
			// prop.setProperty("fileInfo.orderID", auftrag.getOrderId());

			// vehicleInfo node
			// -------------------------------------------------------------------------
			prop.setProperty( "vehicleInfo.shortVIN", auftrag.getFahrgestellnummer7() ); // req
			// prop.setProperty("vehicleInfo.FZS", auftrag.getSteuerschluessel());
			prop.setProperty( "vehicleInfo.typeKey", auftrag.getTypschluessel() );

			// testInfo node
			// ----------------------------------------------------------------------------
			prop.setProperty( "testInfo.testStand", testStand ); // req
			prop.setProperty( "testInfo.errorCount", errorCount ); // req
			prop.setProperty( "testInfo.complete", "1" ); // req
			prop.setProperty( "testInfo.testTime", CascadeDateFormat.formatResultDate( new Date() ) ); // req
			prop.setProperty( "testInfo.physicalPruefumfangName", pruefumfang.getName() );
			prop.setProperty( "testInfo.testVersion", pruefumfang.getVersion() );
			// prop.setProperty( "testInfo.optParam1", job ); // not needed, also must not be > 20 characters
			prop.setProperty( "testInfo.testDuration", "0" );

			// EdiabasResultMap to store EDIABAS results in case there are multiple sensors with
			// Key = physical ECU address
			Map<String, EdiabasResult> EdiabasResultMap = new HashMap<String, EdiabasResult>();
			EdiabasResult er = null; // EDIABAS-result

			// run through Map to create IPSQ files (via catching EDIABAS results if necessary)
			for( Map.Entry<String, String[]> entry : sensorMap.entrySet() ) {
				// create IPSQ files for the sensors that need to execute an EDIABAS job to determine the result
				if( !entry.getKey().startsWith( "RESULT_" ) && (entry.getValue().length == 2) ) {
					// Info:
					// entry.getValue()[0] // KK
					// entry.getValue()[1] // phy_Adr

					if( !EdiabasResultMap.containsKey( entry.getValue()[1] ) ) { // no EDIABAS-execution if result is already available						    
						// specify EDIABAS-job
						sgbd = "F01";
						job = "sensoren_ident_lesen_funktional";
						para = entry.getValue()[1]; // get physical address from Map, e.g. "0x63"
						// execute EDIABAS-job
						log( debug, "Ok", "executing EDIABAS-job..." );
						// show user dialog
						myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
						if( DE )
							myDialog.displayMessage( "EDIABAS-Job ausf�hren...", "lese Sensor " + entry.getKey() + " @:" + para, -1 );
						else
							myDialog.displayMessage( "executing EDIABAS-job...", "read sensor " + entry.getKey() + " @:" + para, -1 );
						try {
							temp = ediabas.executeDiagJob( sgbd, job, para, jobres );

							// close user dialog
							try {
								getPr�flingLaufzeitUmgebung().releaseUserDialog();
							} catch( DeviceLockedException e ) {
								e.printStackTrace();
							}

							if( temp.equals( "OKAY" ) == false ) {
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, para, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), temp, Ergebnis.FT_NIO );
								resList.add( result );
								log( debug, "Error", "DiagFehler: EDIABAS " + PB.getString( "diagnosefehler" ) );
								status = STATUS_EXECUTION_ERROR;
								continue;
							}
						} catch( ApiCallFailedException e ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, para, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
							resList.add( result );
							log( debug, "Error", "DiagFehler: EDIABAS " + PB.getString( "diagnosefehler" ) + " " + ediabas.apiErrorCode() + ": " + ediabas.apiErrorText() );
							if( debug )
								e.printStackTrace();
							status = STATUS_EXECUTION_ERROR;
							continue;
						} catch( EdiabasResultNotFoundException e ) {
							if( e.getMessage() != null ) {
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, para, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
								log( debug, "Error", "DiagFehler: EDIABAS " + PB.getString( "diagnosefehler" ) + " " + e.getMessage() );
							} else {
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, para, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
								log( debug, "Error", "DiagFehler: EDIABAS " + PB.getString( "diagnosefehler" ) );
							}
							resList.add( result );
							if( debug )
								e.printStackTrace();
							status = STATUS_EXECUTION_ERROR;
							continue;
						} catch( Exception e ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, para, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
							resList.add( result );
							log( debug, "Error", "DiagFehler: EDIABAS " + PB.getString( "diagnosefehler" ) + " " + ediabas.apiErrorCode() + ": " + ediabas.apiErrorText() );
							if( debug )
								e.printStackTrace();
							status = STATUS_EXECUTION_ERROR;
							continue;
						}
						er = ediabas.getResult();
						EdiabasResultMap.put( entry.getValue()[1], er ); // store results for later use

					} else {
						er = EdiabasResultMap.get( entry.getValue()[1] ); // take over results from already executed EDIABAS-job
					}

					// search EDIABAS-job result-sets for matches with the "SENSOR_VERBAUORT_NR" in include list "SENSOR_PARA"
					EdiabasResultSet[] ers = er.getResultSets();
					for( int m = 0; m < er.getNumberOfResultSets(); m++ ) {
						String sensor_verbauort = null, sensor_verbauort_nr = null, sensor_bmw_nr = null;
						Set<?> testSet = ers[m].keySet();
						Object[] resultNames = (Object[]) testSet.toArray();
						for( int n = 0; n < resultNames.length; n++ ) {
							temp = ers[m].get( resultNames[n].toString() ).stringValue();
							if( resultNames[n].equals( "SENSOR_VERBAUORT" ) )
								sensor_verbauort = temp;
							if( resultNames[n].equals( "SENSOR_VERBAUORT_NR" ) )
								sensor_verbauort_nr = temp;
							if( resultNames[n].equals( "SENSOR_BMW_NR" ) )
								sensor_bmw_nr = temp;
						}

						// if this result-set contains the "SENSOR_VERBAUORT_NR" from this Map item and the "SENSOR_BMW_NR" is not "--" (= empty)
						if( entry.getKey().equals( sensor_verbauort_nr ) && !sensor_bmw_nr.equals( "--" ) ) {
							resName = entry.getValue()[0] + "System" + resValue;
							xmlName = "X" + auftrag.getFahrgestellnummer7() + "_!SENSOR_" + sensor_verbauort_nr + "_KK_" + entry.getValue()[0] + "_" + String.valueOf( System.currentTimeMillis() ) + ".xml";

							xmlPath = localDirPath + File.separator + xmlName;

							// testInfo node
							// ----------------------------------------------------------------------------
							// prop.setProperty( "testInfo.pruefumfangName", resName ); // req
							prop.setProperty( "testInfo.pruefumfangName", "SNR_CHECK" ); // change for 2_0_T

							// result node
							// ------------------------------------------------------------------------------
							prop.setProperty( "result" + ".testStepName", logPLName ); // req
							prop.setProperty( "result" + ".testStepResult", XmlUtil.replaceXmlCharsAndTruncate( resValue, 4000 ) ); // req
							prop.setProperty( "result" + ".resultName", "Sachnummer" );
							prop.setProperty( "result" + ".lineRelevant", errorCount );
							prop.setProperty( "result" + ".description", sensor_verbauort );
							// IPSQ Sachnummer = KK + SENSOR_BMW_NO + 00
							prop.setProperty( "result" + ".resultValueStr", XmlUtil.replaceXmlCharsAndTruncate( entry.getValue()[0] + sensor_bmw_nr + "00", 4000 ) );
							prop.setProperty( "result" + ".minValueStr", "OK" );
							prop.setProperty( "result" + ".maxValueStr", "OK" );
							prop.setProperty( "result" + ".prueflingName", plName );
							prop.setProperty( "result" + ".pruefprozedurName", ppName ); // IPSQXml_x_0_F
							prop.setProperty( "result" + ".errorType", errorType );
							prop.setProperty( "result" + ".resultType", "diagnostic" ); // ??
							prop.setProperty( "result" + ".errorText", XmlUtil.replaceXmlCharsAndTruncate( "", 4000 ) ); // FehlerText
							prop.setProperty( "result" + ".adviseText", XmlUtil.replaceXmlCharsAndTruncate( "", 500 ) ); // HinweiseText
							prop.setProperty( "result" + ".instructionText", XmlUtil.replaceXmlCharsAndTruncate( "", 500 ) ); // AnweiseText

							// create XML for sensor
							// create IPSQ file
							// ----------------
							log( debug, "Ok", "creating IPSQ file ..." );
							if( createIPSQFile( debug, prop, xmlPath ) ) {
								// IO
								//resList.add( new Ergebnis( "StatusIPSQPuResult", "IPSQXml", job, entry.getValue()[0], resName, "IPSQPuResultOk", "Ok", "Ok", "Ok", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
								resList.add( new Ergebnis( "StatusIPSQPuResult", "IPSQXml", sgbd, job, entry.getValue()[1], sensor_verbauort, entry.getValue()[0] + sensor_bmw_nr + "00", "Ok", "Ok", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
								log( debug, "Ok", "IPSQ file created. (" + resName + ")" );
							} else {
								// NIO
								status = STATUS_EXECUTION_ERROR;
								//resList.add( new Ergebnis( "StatusIPSQPuResult", "IPSQXml", job, entry.getValue()[0], resName, "IPSQPuResultNOk", "NOk", "Ok", "Ok", "0", "", "", "", "XmlFileCreationError", "", Ergebnis.FT_NIO ) );
								resList.add( new Ergebnis( "StatusIPSQPuResult", "IPSQXml", sgbd, job, entry.getValue()[1], sensor_verbauort, entry.getValue()[0] + sensor_bmw_nr + "00", "Ok", "Ok", "0", "", "", "", "XmlFileCreationError", "", Ergebnis.FT_NIO ) );
								log( debug, "Error", "Failed to create IPSQ file. (" + xmlPath + ")" );
							}
						}
					}
				}

				// create IPSQ files for the sensors that already have a determined result
				if( entry.getKey().startsWith( "RESULT_" ) && (entry.getValue().length == 3) ) {
					// Info:
					// entry.getValue()[0] // KK
					// entry.getValue()[1] // phy_Adr
					// entry.getValue()[2] // result

					resName = entry.getValue()[0] + "System" + resValue;
					xmlName = "X" + auftrag.getFahrgestellnummer7() + "_!SENSOR_" + entry.getKey().substring( 7 ) + "_KK_" + entry.getValue()[0] + "_" + String.valueOf( System.currentTimeMillis() ) + ".xml";

					xmlPath = localDirPath + File.separator + xmlName;

					// testInfo node
					// ----------------------------------------------------------------------------
					prop.setProperty( "testInfo.pruefumfangName", resName ); // req

					// result node
					// ------------------------------------------------------------------------------
					prop.setProperty( "result" + ".testStepName", logPLName ); // req
					prop.setProperty( "result" + ".testStepResult", XmlUtil.replaceXmlCharsAndTruncate( resValue, 4000 ) ); // req
					prop.setProperty( "result" + ".resultName", "Sachnummer" );
					prop.setProperty( "result" + ".lineRelevant", errorCount );
					prop.setProperty( "result" + ".description", "result via @parameter" );
					// IPSQ Sachnummer = KK + SENSOR_BMW_NO + 00
					prop.setProperty( "result" + ".resultValueStr", XmlUtil.replaceXmlCharsAndTruncate( entry.getValue()[0] + entry.getValue()[2] + "00", 4000 ) );
					prop.setProperty( "result" + ".minValueStr", "OK" );
					prop.setProperty( "result" + ".maxValueStr", "OK" );
					prop.setProperty( "result" + ".prueflingName", plName );
					prop.setProperty( "result" + ".pruefprozedurName", ppName ); // IPSQXml_x_0_F
					prop.setProperty( "result" + ".errorType", errorType );
					prop.setProperty( "result" + ".resultType", "diagnostic" ); // ??
					prop.setProperty( "result" + ".errorText", XmlUtil.replaceXmlCharsAndTruncate( "", 4000 ) ); // FehlerText
					prop.setProperty( "result" + ".adviseText", XmlUtil.replaceXmlCharsAndTruncate( "", 500 ) ); // HinweiseText
					prop.setProperty( "result" + ".instructionText", XmlUtil.replaceXmlCharsAndTruncate( "", 500 ) ); // AnweiseText

					// create XML for sensor
					// create IPSQ file
					// ----------------
					log( debug, "Ok", "creating IPSQ file ..." );
					if( createIPSQFile( debug, prop, xmlPath ) ) {
						// IO
						//resList.add( new Ergebnis( "StatusIPSQPuResult", "IPSQXml", job, entry.getValue()[0], resName, "IPSQPuResultOk", "Ok", "Ok", "Ok", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
						resList.add( new Ergebnis( "StatusIPSQPuResult", "IPSQXml", sgbd, job, entry.getValue()[1], "result via @parameter", entry.getValue()[0] + entry.getValue()[2] + "00", "Ok", "Ok", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
						log( debug, "Ok", "IPSQ file created. (" + resName + ")" );
					} else {
						// NIO
						status = STATUS_EXECUTION_ERROR;
						//resList.add( new Ergebnis( "StatusIPSQPuResult", "IPSQXml", job, entry.getValue()[0], resName, "IPSQPuResultNOk", "NOk", "Ok", "Ok", "0", "", "", "", "XmlFileCreationError", "", Ergebnis.FT_NIO ) );
						resList.add( new Ergebnis( "StatusIPSQPuResult", "IPSQXml", sgbd, job, entry.getValue()[1], "result via @parameter", entry.getValue()[0] + entry.getValue()[2] + "00", "Ok", "Ok", "0", "", "", "", "XmlFileCreationError", "", Ergebnis.FT_NIO ) );
						log( debug, "Error", "Failed to create IPSQ file. (" + xmlPath + ")" );
					}
				}
			}

			// send IPSQ files
			// ---------------
			log( debug, "Ok", "start Timestamp for vin: " + auftrag.getFahrgestellnummer7() );
			if( timeStamp( debug, auftrag.getFahrgestellnummer7(), resName ) ) {
				log( debug, "Ok", "ende Timestamp for vin: " + auftrag.getFahrgestellnummer7() + " , check ok" );
				if( pingStatus ) {
					log( debug, "Ok", "sending IPSQ files... from " + localDirPath + " to " + apdmPath );
					itmp = sendIPSQFiles( debug, localDirPath, apdmPath );
					resList.add( new Ergebnis( "StatusSendIPSQPuResults", "IPSQXml", job, apdmPath, "", "SendIPSQPuResultsOk", String.valueOf( itmp ), "0", "", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
					log( debug, "Ok", "sent " + String.valueOf( itmp ) + " files to the APDM DB server." );
				} else {
					// sending not possible
					resList.add( new Ergebnis( "StatusIPSQPingResult", "IPSQXml", job, apdmPath, "", "IPSQPingResultNOk", "NOk", "Ok", "Ok", "0", "", "", "", "APDMPathPingError", "", Ergebnis.FT_IGNORE ) );
					log( debug, "Error", "Failed to connect to apdmpath! (" + apdmPath + ")" );

					myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();

					if( DE )
						myDialog.displayAlertMessage( "IPSQXml", "APDM-Pfad nicht erreichbar - Netzwerkfehler!", 5 );
					else
						myDialog.displayAlertMessage( "IPSQXml", "APDM-path not reachable - network problem!", 5 );

					String message = DE ? "Server nicht erreichbar - IPSQ-Result nur lokal erzeugt!" : "Server not reachable - IPSQ-result only local";
					log( debug, "Error", message );
				}
			} else
				log( debug, "Ok", "ende Timestamp for vin: " + auftrag.getFahrgestellnummer7() + " , check nok" );
		} catch( PPExecutionException e ) {
			if( debug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			resList.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			if( debug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			resList.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			if( debug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			resList.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		if( myDialog != null )
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( DeviceLockedException e ) {
				e.printStackTrace();
			}

		setPPStatus( info, status, resList );
	}

	/**
	 * <b>log</b> <br>
	 * Logs a message if the given debug flag is active or if it is an 'Error'
	 * message. <br>
	 * 
	 * @param debug - the debug flag
	 * @param type - the type of message to display
	 * @param text - the message text
	 */
	private void log( boolean debug, String type, String text ) {
		if( type.equalsIgnoreCase( "Error" ) || debug )
			System.out.println( "[SDiagSNRCheck PP] [" + type + "] " + text );
	}

	/**
	 * <b>createLocalDir</b> <BR>
	 * Creates the local IPSQ directory and verifies validity. <BR>
	 * 
	 * @param debug - debug flag
	 * @param localDirPath
	 * @return true if ok, false if not
	 */
	private boolean createLocalDir( boolean debug, String localDirPath ) throws Exception {
		File dir = new File( localDirPath );
		// check existence/create
		if( !dir.exists() ) {
			if( !dir.mkdir() ) {
				throw new Exception( "unable to create local output dir " + localDirPath );
			}
		}
		// check validity
		if( !validDir( debug, dir ) ) {
			throw new Exception( "invalid local output dir " + localDirPath );
		}
		return true;
	}

	/**
	 * <b>validDir</b> <BR>
	 * Checks if the given directory exists, is readable, and is writtable. <BR>
	 * 
	 * @param dir - the directoy to evaluate
	 * @return validation result
	 */
	private boolean validDir( boolean debug, File dir ) {
		try {
			if( !dir.exists() || !dir.isDirectory() || !dir.canRead() || !dir.canWrite() ) {
				log( debug, "Error", "invalid directory (criteria: Existence/Directory/Readable/Writable) " + dir.getAbsolutePath() );
				return false;
			}
			log( debug, "Ok", "valid directory " + dir.getAbsolutePath() );
			return true;

		} catch( SecurityException e ) {
			log( debug, "Error", "checking directory security problems, SecurityException: " + e.getMessage() );
			return false;
		} catch( NullPointerException e ) {
			log( debug, "Error", "checking directory null argument, NullPointerException: " + e.getMessage() );
			return false;
		} catch( Exception e ) {
			log( debug, "Error", "checking directory, Exception: " + e.getMessage() );
			return false;
		}
	}

	/**
	 * <b>getSubString</b> <br>
	 * Gets the substring between the given start string (last occurrence) and
	 * the end string (last occurrence). <br>
	 * 
	 * @param str - the string where to extract the substring from
	 * @param startStr - the string (last occurrence) where to start the extraction or NULL to start from the beginning
	 * @param endStr - the string (last occurrence) where to end the extraction or NULL to end in the end
	 * @return the substring or an empty string
	 */
	private String getSubString( String str, String startStr, String endStr ) {
		int startIndex = (startStr != null && str.lastIndexOf( startStr ) != -1) ? str.lastIndexOf( startStr ) + startStr.length() : 0;
		int endIndex = (endStr != null && str.lastIndexOf( endStr ) != -1) ? str.lastIndexOf( endStr ) : str.length();
		return (startIndex >= 0 && startIndex < endIndex && endIndex <= str.length()) ? str.substring( startIndex, endIndex ) : "";
	}

	/**
	 * <b>createIPSQFile</b> <BR>
	 * Creates an IPSQ result xml file with the specified properties in the given path. <BR>
	 * 
	 * @param debug - debug flag
	 * @param prop - properties object containing the data to insert
	 * @param xmlPath - output xml file path
	 * @return state - IPSQ file creation result
	 * @throws Exception - if something goes wrong
	 */
	private boolean createIPSQFile( boolean debug, Properties prop, String xmlPath ) throws Exception {
		try {
			log( debug, "Ok", "creating IPSQ XML file..." );
			log( debug, "Var", "XML properties: " + prop.toString() );

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			log( debug, "Ok", "DOM document built." );
			// build nodes
			Element root = (Element) buildNode( debug, doc, prop, "testResult" );
			Node fileInfoNode = buildNode( debug, doc, prop, "fileInfo" );
			Node vehicleInfoNode = buildNode( debug, doc, prop, "vehicleInfo" );
			Node testInfoNode = buildNode( debug, doc, prop, "testInfo" );

			Node resultNode = buildNode( debug, doc, prop, "result" );

			// template for two nodes
			// Node resultNode1 = buildNode( debug, doc, prop, "result1" );
			// Node resultNode2 = buildNode( debug, doc, prop, "result2" );

			// template for multiple nodes 
			//	Node[] resultNodeArray = new Node[zaehler];
			//	for( int i = 0; i < zaehler; i++ ) {
			//		resultNodeArray[i] = buildNode( debug, doc, prop, "result" + (i + 1) );
			//	}

			log( debug, "Ok", "XML DOM nodes created." );
			// build structure
			doc.appendChild( root );
			root.appendChild( fileInfoNode );
			root.appendChild( vehicleInfoNode );

			testInfoNode.appendChild( resultNode );

			// template for two nodes
			// doc.renameNode( resultNode1, null, "result" );
			// testInfoNode.appendChild( resultNode1 );
			// doc.renameNode( resultNode2, null, "result" );
			// testInfoNode.appendChild( resultNode2 );

			// template for multiple nodes
			//	for( int j = 0; j < resultNodeArray.length; j++ ) {
			//		Node temp = resultNodeArray[j];
			//		doc.renameNode( temp, null, "result" );
			//		testInfoNode.appendChild( temp );
			//	}

			root.appendChild( testInfoNode );
			log( debug, "Ok", "XML DOM structure created." );
			doc.normalize();
			// write to xml file
			TransformerFactory transfFactory = TransformerFactory.newInstance();
			Transformer transf = transfFactory.newTransformer();
			log( debug, "Ok", "got DOM transformer." );
			DOMSource source = new DOMSource( doc );
			log( debug, "Ok", "got DOM source." );
			File xmlFile = new File( xmlPath );

			StreamResult result = new StreamResult( xmlFile );

			transf.transform( source, result );
			updateRegistry( debug, xmlFile.getParent(), "Ok", "CREATED : " + xmlFile.getName() + " (" + prop.getProperty( "result.resultName" ) + ")" );
			log( debug, "Ok", "XML document " + xmlFile.getName() + " created." );
			return true;

		} catch( FactoryConfigurationError e ) {
			throw new Exception( "[FactoryConfigurationException] " + e.getMessage() );
		} catch( ParserConfigurationException e ) {
			throw new Exception( "[ParserConfigurationException] " + e.getMessage() );
		} catch( DOMException e ) {
			throw new Exception( "[DOMException] " + e.getMessage() );
		} catch( TransformerFactoryConfigurationError e ) {
			throw new Exception( "[TransformerFactoryConfigurationException] " + e.getMessage() );
		} catch( TransformerConfigurationException e ) {
			throw new Exception( "[TransformerConfigurationException] " + e.getMessage() );
		} catch( TransformerException e ) {
			throw new Exception( "[TransformerException] " + e.getMessage() );
		} catch( NullPointerException e ) {
			throw new Exception( "[NullPointerException] " + e.getMessage() );
		} catch( Exception e ) {
			throw new Exception( "[Exception] " + e.getMessage() );
		}
	}

	/**
	 * <b>buildNode</b> <BR>
	 * Builds a DOM node with the specified name and with the properties
	 * specified attributes. <BR>
	 * 
	 * @param debug - debug flag
	 * @param doc - DOM document
	 * @param prop - properties object to with the data to include
	 * @param name - name of the node to be built
	 * @return the created node ready to be added
	 * @throws Exception - if something goes wrong
	 */
	private Node buildNode( boolean debug, Document doc, Properties prop, String name ) throws Exception {
		Element node = doc.createElement( name );
		log( debug, "Ok", "building node " + name + " ..." );
		Enumeration<?> keys = prop.keys();
		String propName = null;
		String attribName = null;
		String attribValue = null;
		while( keys.hasMoreElements() ) {
			propName = (String) keys.nextElement();
			if( propName.startsWith( name ) ) {
				attribName = propName.substring( propName.indexOf( "." ) + 1 );
				if( name.equalsIgnoreCase( "testResult" ) && attribName.indexOf( "_" ) != 1 ) {
					attribName = attribName.replaceFirst( "_", ":" );
				}
				attribValue = prop.getProperty( propName );
				node.setAttribute( attribName, attribValue );
			}
		}
		return node;
	}

	/**
	 * <b>updateRegistry</b> <BR>
	 * Updates the local directory's input/output registry. <BR>
	 * 
	 * @param debug the debug flag
	 * @param type the type of message to display
	 * @param text the message text
	 */
	private synchronized void updateRegistry( boolean debug, String localDirPath, String type, String text ) {
		final String LOG_NAME = "IORegistry.log";
		final long LOG_MAX_SIZE = 1000000; // (Bytes) 1 MB

		String regFilePath = localDirPath + File.separator + LOG_NAME;
		File regFile = null;
		PrintWriter outWriter = null;

		try {
			regFile = new File( regFilePath );
			// check existence and size
			if( regFile.exists() ) {
				// check file size
				if( regFile.length() >= LOG_MAX_SIZE ) {
					if( regFile.delete() ) {
						log( debug, "Ok", "deleted local registry log (>" + String.valueOf( LOG_MAX_SIZE ) + " Bytes)." );
					} else {
						log( debug, "Error", "deleting local registry log (>" + String.valueOf( LOG_MAX_SIZE ) + " Bytes)." );
					}
				}
			} else {
				// create file
				if( regFile.createNewFile() ) {
					log( debug, "Ok", "created local registry log." );
				} else {
					log( debug, "Error", "creating local registry log." );
				}
			}
			// append text
			outWriter = new PrintWriter( new BufferedWriter( new FileWriter( regFile, true ) ) );
			outWriter.println( "[" + new Date().toString() + "] " + "[" + type + "] " + text );
			outWriter.close();
			log( debug, "Ok", "updated local registry log." );

		} catch( Exception e ) {
			log( debug, "Error", "updating local registry log " + regFilePath + ", Exception: " + e.getMessage() );
		}
	}

	/**
	 * <b>timeStamp</b> <br>
	 * Sets the current timestamp for the current vin and ipsq mark and writes
	 * it in a (temporary) pruefstandvariable. Also checks if there is already a
	 * timestamp and the same ipsq mark for the vin and if the delta is bigger
	 * than 14 seconds, because than it is possible to send another IPSQ-Result
	 * for the same vin. If the delta is smaller than the IPSQ-result will only
	 * be saved locally and sent with the next vin. <br>
	 * 
	 * @param debug
	 * @param vin - string with the current vin
	 * @param kkParas - string with the ipsq mark
	 * @return send - boolean, if IPSQ-result should be sent
	 */
	private boolean timeStamp( boolean debug, String vin, String kkParas ) {

		HashMap<String, IpsqMerkmal> ipsqMerkmalMap = new HashMap<String, IpsqMerkmal>();
		long currentTime = System.currentTimeMillis();
		boolean send = true;
		String lastPuName;
		IpsqMerkmal ipsq;
		try {
			try {
				ipsqMerkmalMap = (HashMap<String, IpsqMerkmal>) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "TEMP_IPSQXML" );
			} catch( VariablesException e1 ) {

				log( debug, "Warning", "Fehler getPruefstandVariable" );
			}

			if( !ipsqMerkmalMap.isEmpty() && ipsqMerkmalMap.containsKey( vin ) ) {
				log( debug, "Ok", "Pruefstandsvariable ist nicht leer und enth�lt vin: " + vin );
				Set<String> keys = ipsqMerkmalMap.keySet();
				Iterator<String> iterkeys = keys.iterator();

				while( iterkeys.hasNext() ) {
					String key = iterkeys.next().toString();
					ipsq = (IpsqMerkmal) ipsqMerkmalMap.get( key );
					long keyTime = ipsq.getIpsqTime();
					lastPuName = ipsq.getIpsqName();
					boolean timeDelta = (currentTime - keyTime) > 14000;
					log( debug, "Ok", "Delta: " + (currentTime - keyTime) + " f�r VIN: " + key );
					boolean kkParasCompare = lastPuName.equalsIgnoreCase( kkParas );

					if( timeDelta ) {
						ipsqMerkmalMap.remove( key );
						log( debug, "Ok", "VIN " + key + " wird aus Pruefstandvariable gel�scht, da Delta: " + timeDelta );
					}
					if( !timeDelta && !kkParasCompare ) {
						log( debug, "Ok", "VIN " + key + " wird gesendet, da Delta: " + timeDelta + " zwar zu klein, jedoch ein anderes Merkmal " + kkParas + " gesendet wird!" );
					}
					if( !timeDelta && key.equalsIgnoreCase( vin ) && kkParasCompare ) {
						send = false;
						log( debug, "Ok", "VIN " + key + " wird nicht gesendet, da Delta: " + timeDelta + " und gleiches Merkmal" );
					}
				}

				try {
					getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "TEMP_IPSQXML", ipsqMerkmalMap );
				} catch( VariablesException e ) {
					System.out.println( "Kann Pruefstandvariable IPSQXML nicht setzen!" );
					e.printStackTrace();
				}

			} else if( !ipsqMerkmalMap.isEmpty() && !ipsqMerkmalMap.containsKey( vin ) ) {

				log( debug, "Ok", "Pruefstandsvariable ist nicht leer aber enth�lt nicht vin: " + vin );
				Set<String> keys = ipsqMerkmalMap.keySet();
				Iterator<String> iterkeys = keys.iterator();

				while( iterkeys.hasNext() ) {
					String key = iterkeys.next().toString();
					ipsq = (IpsqMerkmal) ipsqMerkmalMap.get( key );
					long keyTime = ipsq.getIpsqTime();
					lastPuName = ipsq.getIpsqName();
					boolean timeDelta = (currentTime - keyTime) > 14000;
					log( debug, "Ok", "Delta: " + (currentTime - keyTime) + " f�r VIN: " + key );

					if( timeDelta ) {
						ipsqMerkmalMap.remove( key );
						log( debug, "Ok", "VIN " + key + " wird aus Pruefstandvariable gel�scht, da Delta: " + timeDelta );
					}
					log( debug, "Ok", "vin " + vin + " wird der Pr�fstandsvariable hinzugef�gt!" );
					ipsq = new IpsqMerkmal( kkParas, Long.valueOf( currentTime ) );
					ipsqMerkmalMap.put( vin, ipsq );
					try {
						getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "TEMP_IPSQXML", ipsqMerkmalMap );
					} catch( VariablesException e ) {
						System.out.println( "Kann Pruefstandvariable IPSQXML nicht setzen!" );
						e.printStackTrace();
					}

				}

			} else {
				log( debug, "Ok", "Pruefstandsvariable leer, vin wird geschrieben " + vin );
				ipsq = new IpsqMerkmal( kkParas, Long.valueOf( currentTime ) );
				ipsqMerkmalMap.put( vin, ipsq );
				try {
					getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "TEMP_IPSQXML", ipsqMerkmalMap );
				} catch( VariablesException e ) {
					System.out.println( "Kann Pruefstandvariable IPSQXML nicht setzen!" );
					e.printStackTrace();
				}
			}
		} catch( Exception e1 ) {
			e1.printStackTrace();
			System.out.println( "exception e1 " + e1.getMessage() );
		}

		return send;

	}

	private class IpsqMerkmal {

		long ipsqtime;
		String ipsqname;

		private IpsqMerkmal( String name, long time ) {
			this.ipsqname = name;
			this.ipsqtime = time;
		}

		private long getIpsqTime() {
			return ipsqtime;
		}

		private String getIpsqName() {
			return ipsqname;
		}

	}

	/**
	 * <b>checkDirectory</b> <br>
	 * Checks if the host is available (ping). <br>
	 * 
	 * @param host - The host which should be checked
	 * @return returns true or false
	 */
	private boolean checkDirectory( String host ) {
		boolean isReachable = false;

		if( host.startsWith( "//" ) ) {
			do {
				host = host.replaceFirst( "//", "" );
			} while( host.startsWith( "//" ) );
		}

		if( host.startsWith( "\\" ) ) {
			do {
				host = host.replaceFirst( "\\", "" );
			} while( host.startsWith( "\\" ) );
		}

		StringTokenizer tokenizer = new StringTokenizer( host, "/" );
		host = tokenizer.nextToken();

		try {

			// Als externen Prozess ausf�hren
			Runtime runtime = Runtime.getRuntime();
			Process process = runtime.exec( "ping -n 1 -w 1000 " + host );
			int exitValue = process.waitFor();

			if( exitValue == 0 )
				isReachable = true;

		} catch( Exception e ) {
			if( ((getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ))) ? true : false ) {
				System.out.println( "Ping PP: Exception begin!" );
				e.printStackTrace();
				System.out.println( "Ping PP: Exception end!" );
			}
		}
		return isReachable;
	}

	/**
	 * <b>sendIPSQFiles</b> <BR>
	 * Tests the APDM DB connection and if exists then sends all IPSQ result xml
	 * files in the given directory to the given APDM DB Path. Returns -1 if no
	 * APDM connection is possible. <BR>
	 * 
	 * @param debug - debug flag
	 * @param prop - properties object containing the data to insert
	 * @param xmlPath - output xml file path
	 * @return filesSent - number of files sent or -1 for APDM connection error
	 * @throws Exception - if something goes wrong
	 */
	private synchronized int sendIPSQFiles( boolean debug, String localDirPath, String apdmPath ) throws Exception {
		File outputDir = new File( localDirPath );
		File[] srcFiles = outputDir.listFiles();
		File destFile = null;
		int filesSent = 0;

		// check accessibility of required directory via extra thread
		ExecutorService exSrv = Executors.newSingleThreadExecutor();
		ExecutorCompletionService<Boolean> completionService = new ExecutorCompletionService<Boolean>( exSrv );
		Future<Boolean> pathExists;
		try {
			completionService.submit( new CheckPathExists( apdmPath ) );
			pathExists = completionService.poll( timeout, TimeUnit.SECONDS );
			if( pathExists == null || pathExists.get() != true )
				throw new Exception();
			exSrv.shutdown();
		} catch( Exception e ) {
			exSrv.shutdown();
			e.printStackTrace();
			throw new PPExecutionException( "APDM ACCESS ERROR" );
		}

		// Send all existing .xml files that start with X from the local folder.
		for( int i = 0; i < srcFiles.length; i++ ) {
			if( srcFiles[i].isFile() && srcFiles[i].getName().endsWith( ".xml" ) && srcFiles[i].getName().startsWith( "X" ) ) {
				destFile = new File( apdmPath + "/" + srcFiles[i].getName() );

				log( debug, "OK", "exists-Method before renaming: destFile.exists()=" + destFile.exists() );

				boolean renameSuccessfull = false;
				for( int k = 0; k < 5 && !renameSuccessfull; k++ ) {
					renameSuccessfull = srcFiles[i].renameTo( destFile );
					log( debug, "OK", "renameTo-Method: renameSuccessfull=" + renameSuccessfull );
					if( destFile.exists() ) {
						renameSuccessfull = true;
						log( debug, "OK", "exists-Method: destFile.exists()=" + destFile.exists() );
					}
				}

				if( renameSuccessfull ) {
					filesSent++;
					updateRegistry( debug, localDirPath, "Ok", "SENT    : " + srcFiles[i].getName() );
					log( debug, "Ok", "sent file " + destFile.getAbsolutePath() );
				} else {
					// inperformanteres kopieren versuchen
					if( copy( debug, srcFiles[i].getPath(), destFile.getPath() ) ) {
						// nach copy unbedingt sourcefile l�schen
						if( srcFiles[i].exists() ) {
							srcFiles[i].delete();
							log( debug, "Ok", "source file deleted: " + srcFiles[i].getPath() );
						}
						filesSent++;
						updateRegistry( debug, localDirPath, "Ok", "SENT    : " + srcFiles[i].getName() );
						log( debug, "Ok", "sent file " + destFile.getAbsolutePath() );
					} else {
						updateRegistry( debug, localDirPath, "Error", "SENDING : " + srcFiles[i].getName() );
						log( debug, "Error", "sending file " + destFile.getAbsolutePath() );
					}
				}
			}
		}
		// ---------------------------------------
		return filesSent;
	}

	/**
	 * Kopiert das fromFile zum toFile �ber ByteStreams -> kostet viel
	 * Performance, wird also nur aufgerufen falls die Java Methode "renameTo"
	 * nicht funktioniert.
	 * 
	 * @param debug
	 * @param fromFileName
	 * @param toFileName
	 * @throws IOException
	 */
	private boolean copy( boolean debug, String fromFileName, String toFileName ) throws IOException {
		File fromFile = new File( fromFileName );
		File toFile = new File( toFileName );

		if( !fromFile.exists() ) {
			log( debug, "Error", "FileCopy: " + "no such source file: " + fromFileName );
			return false;
		}
		if( !fromFile.isFile() ) {
			log( debug, "Error", "FileCopy: " + "can't copy directory: " + fromFileName );
			return false;
		}
		if( !fromFile.canRead() ) {
			log( debug, "Error", "FileCopy: " + "source file is unreadable: " + fromFileName );
			return false;
		}

		if( toFile.isDirectory() )
			toFile = new File( toFile, fromFile.getName() );

		if( toFile.exists() ) {
			if( !toFile.canWrite() ) {
				log( debug, "Error", "FileCopy: " + "destination file is unwriteable: " + toFileName );
				return false;
			}
		}
		FileInputStream from = null;
		FileOutputStream to = null;
		try {
			from = new FileInputStream( fromFile );
			to = new FileOutputStream( toFile );
			byte[] buffer = new byte[4096];
			int bytesRead;

			while( (bytesRead = from.read( buffer )) != -1 )
				to.write( buffer, 0, bytesRead ); // write
		} finally {
			if( from != null )
				try {
					from.close();
				} catch( IOException e ) {
					e.printStackTrace();
					return false;
				}
			if( to != null )
				try {
					to.close();
				} catch( IOException e ) {
					e.printStackTrace();
					return false;
				}
		}
		log( debug, "OK", fromFileName + "was copied to: " + toFileName );
		return true;
	}

	/**
	 *  Die Klasse CheckPathExists erm�glicht ein threadbasiertes �berpr�fen auf die Existenz eines Pfades.
	 * 
	 * @author Sch�nert Fabian, TI-545 BMW AG
	 *
	 */
	class CheckPathExists implements Callable<Boolean> {

		private File pathFile;

		public CheckPathExists( String path ) {
			this.pathFile = new File( path );
		}

		@Override
		public Boolean call() throws Exception {
			return pathFile.exists();
		}
	}

}

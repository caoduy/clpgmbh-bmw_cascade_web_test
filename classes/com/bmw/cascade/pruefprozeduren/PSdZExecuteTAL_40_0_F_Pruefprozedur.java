package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import java.util.Map.Entry;

import com.bmw.appframework.logging.LogLevel;
import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.psdz.PSdZ;
import com.bmw.cascade.pruefstand.devices.psdz.data.*;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.*;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.util.logging.CascadeLogging;

/**
 * Implementierung einer Pr�fprozedur, die eine TAL ausf�hrt. <BR>
 * <BR>
 * W�hrend der TAL Ausf�hrung wird ein Benutzerdialog eingeblendet, der die aktuell <BR>
 * behandelten SG und die noch zu behandelnden SG anzeigt. <BR>
 * Der Benutzer hat die M�glichkeit die Behandlung der TAL abzubrechen. <BR>
 * Die Behandlung wird zum n�chsten m�glichen Zeitpunkt abgebrochen. (PSdZ Verantwortung)<BR>
 * <BR>
 * 
 * @author Thomas Buboltz, BMW AG; Dieter Busetti, GEFASOFT AG; Martin Gampl, BMW AG <BR>
 * <BR>
 * @version 14.09.2006 TB Ersterstellung <BR>
 *          19.10.2006 TB Parameter ignoreErrors eingebaut [CR329] <BR>
 *          28.01.2007 TB Ausf�hrung einer TAL �ber Vorgabe als TAL XML File mittels Parameter "TAL_XML_FILE" <BR>
 *          07.02.2007 TB Stop bei Fehlern bei einem SG, diesen Parameter nach au�en gelegt und die maximale Anzahl der Wdhlg eingegrenzt <BR>
 *          15.02.2007 TB Events eingebaut <BR>
 *          25.03.2007 TB sauberer PSdZ Exception catch eingebaut <BR>
 *          12.04.2007 TB DEU / ENG Texte eingebaut <BR>
 *          16.04.2007 TB Zaehlen der SG Anzahl korrigiert (SG und nicht Aktionen) <BR>
 *          23.04.2007 TB Sch�nheitsfehler bei der SG Z�hlung in der TAL Abarbeitung korrigiert <BR>
 *          06.06.2007 TB �berarbeitung SG Zaehlung (Hintergund: Wir bekommen jede PSdZ TAL mit, SGBM IDs k�nnen unterschiedlich sein) <BR>
 *          06.06.2007 TB �berfl�ssigen Code f�r die Auswertung auskommentiert <BR>
 *          26.10.2007 TB IO / NIO Steuerger�te in APDM eintragen und ECU Z�hlung BugFix <BR>
 *          03.01.2008 DB Einbau "testScreenID" in Debug-Ausgaben <BR>
 *          13.01.2008 TB Einbau Erkennung KIS Doppeltreffer, bei weicher KIS -> diese f�hren zu PP NIO <BR>
 *          17.02.2008 TB KIS Doppeltreffer immer als APDM Fehler ausgeben, ECU IO / NIO Daten f�r APDM erweitert um Prozessklassen Info <BR>
 *          22.02.2008 TB NotExecutable Teile sind auch NIO <BR>
 *          23.04.2008 TB Neueste Fehlertexte <BR>
 *          18.05.2008 TB ECU Statistik dazu <BR>
 *          27.05.2008 TB antwortet immer und unbekannt ausgeblendet (Wunsch DGF) <BR>
 *          29.05.2008 DB Fehlertexte aus der TAL <BR>
 *          18.06.2008 DB Performanceoptimierung durch Verlagerung von Code <BR>
 *          24.06.2008 DB �nderung holen des "testScreenID" <BR>
 *          13.07.2009 TB Logging der finalen TAL als "TALSTATUS" XML File im LogVerzeichnis <BR>
 *          20.10.2009 TB Konfigurierbare Modusumschaltung eingebaut 2 neue optionale Args ["MODE_SWITCH_GATEWAY_LIST", "MODE_SWITCH"]<BR>
 *          14.12.2009 MB opt. Arg "DYNAMIC_RETRIES" hinzu: macht eine interne wiederholung der fehlerhaften tal-lines <BR>
 *          28.06.2011 TB Ausgabe der TAIS Teilenummer analog wie bei der Verify PP <BR>
 *          13.09.2011 AN APDM Meldung mit TAL-Status und kleine Korrektur bei Ausgabe TAIS Teilenummer <BR>
 *          05.02.2012 TB LOP Punkt 1436: Neben der Elektroniksachnummer zus�tzlich wenn m�glich noch die bestellbare Sachnummer anzeigen. // DE/ENG Sprachermittlung hinzu<BR>
 *          13.11.2012 TB Opt. Argument f�r den CODING_TYPE eingebaut (Modi: default FA: Codierung auf Basis orderXML, SHIPMENT: Codierung auf Anlieferzustand, FDL: Modus FDL), Debug �ber Pr�fstandsvariable<BR>
 *          14.11.2012 TB F-Version<BR>
 *          24.11.2012 MK Dokumentation der einzelnen SG Zeiten <BR>
 *          07.01.2013 MK F-Version<BR>
 *          07.02.2014 MG T-Version (26_0) <BR>
 *          ---------- -- - NotExecutable Transaktionen in TAL werden zu ECU_NIO's gez�hlt, TAL-Auswertung in doDokuAPDMTimestamps auf neues TAL-Schema ab PSdZ 4.9 erweitert, Generics hinzu <BR>
 *          05.03.2014 MG T-Version (27_0) <BR>
 *          ---------- -- - APDM-Ausgaben strukturiert. Es erfolgt immer (d.h. f�r IO-TAL , NIO-TAL ggf. mit Wiederholung, Abbruch durch Werker) zun�chst die Ausgabe der TAL-Zeiten, danach die IO-Steuerger�te, danach die NIO-Steuerger�te und danach der Gesamtstatus der TAL. <BR>
 *          06.03.2014 MG F-Version (28_0) <BR>
 *          08.04.2014 MG T-Version (29_0) <BR>
 *          ---------- -- - Das Schreiben der SVT-Soll in das VCM im Rahmen der TAL-Ausf�hrung zur Aufl�sung des uneindeutigen Diagnoserounings im ZSG wird unterbunden, da das vorherige Schreiben der SVT-Soll in das VCM durch den Pr�fablauf sichergestellt werden kann. <BR>
 *          ---------- -- - Neuer optionaler Parameter WRITE_SVT_SOLL_TO_VCM (default: false) <BR>
 *          ---------- -- - Optionale Parameter DIFFERENTIAL_EXECUTION (nicht mehr genutzt seit CASCADE 5.1.0/PSdZ 4.6.1) und STOP_ON_ERROR (nicht mehr genutzt seit CASCADE 4.2.0/PSdZ 4.4.2) wurden entfernt <BR>
 *          ---------- -- - Optionaler Parameter IGNORE_ERRORS wird in PP noch aus Gr�nden der Abw�rtskompatibilit�t mit aktuellem PW unterst�tzt, jedoch ab CASCADE 6.1.0/PSdZ 4.9.1 nicht mehr genutzt <BR>
 *          ---------- -- - Ausbau der deprecated Methoden/deprecated TAL-Status <BR>
 *          17.04.2014 MG F-Version (30_0) <BR>
 *          12.06.2014 MG T-Version (31_0) <BR>
 *          ---------- -- - Fehlerausgaben in Pr�fungsprotokoll verbessert. Durch Nutzung des neuen TAL-Schemas ab PSdZ 4.9 erfolgt nun zus�tzlich die Ausgabe �ber die genaue Fehlerstelle im Ablauf (Aktion). <BR>
 *          ---------- -- - Fehler, die in der TAL-Line-Nachbereitung auftreten, werden nun ebenfalls sicher erfasst. <BR>
 *          17.06.2014 MG F-Version (32_0) <BR>
 *          18.08.2014 MG T-Version (33_0) <BR>
 *          ---------- -- - LOP 1819: Code-Erweiterung zur korrekten Ausgabe der TAL-Ausf�hrungszeiten (in PSdZ 4.9.1 und 5.00.00) ab Nutzung des neuen TAL-Schemas in PSdZ 5.00.00 <BR>
 *          12.09.2014 MG F-Version (34_0) <BR>
 *          20.08.2015 MG T-Version (35_0), Mindestversionsanforderung CASCADE 7.0.1 / PSdZ 5.00.01 <BR>
 *          ---------- -- - Umstellung des Datenbezugs f�r die Ausgabe der TAL-Ausf�hrungszeiten von XML-Datei (TAL) auf CASCADE-Wrapperklassen (CascadeTAL/CascadeTALLine) und somit wieder saubere Kapselung von CASCADE um PSdZ. Somit zuk�nftig keine neuen PP-Versionen mehr notwendig, falls TAL-Schema in PSdZ erneut ver�ndert wird <BR>
 *          ---------- -- - Ausbau des Workarounds zur Lokalisierung der Fehler-Kontexte in der CRE (TALLINE, PREPARATION, AUTHENTICATION, FINALIZATION), da ab CASCADE 7.0 Fehler-Kontexte vollst�ndig lokalisiert sind. CASCADE-Mindestversionsanforderung CASCADE 7.0.1 <BR>
 *          ---------- -- - Optionaler Parameter IGNORE_ERRORS entfernt, da ab CASCADE 6.1.0/PSdZ 4.9.1 nicht mehr genutzt. PP-Nutzung erst m�glich, wenn Parameter fl�chig aus PW ausgebaut ist! <BR>
 *          ---------- -- - LOP 1853: Ausgabe (in Log-Datei Pr�fstandScreen) der Pr�fprozedurversion sowie s�mtlicher optionaler und nicht optionaler Argumente, die an die Pr�fprozedur �bergeben werden zum Zweck der Nachvollziehbarkeit im Fehlerfall. <BR>
 *          ---------- -- - LOP 1899: Einheitliche Darstellung der Fehlerausgaben in PSdZ-Pr�fprozeduren <BR>
 *          ---------- -- - NPE in doDokuTALTimesToAPDM() bei Werkerabbruch w�hrend der TAL-Abarbeitung entfernt. Hintergrund: NPE bei Versuch Start-/Endtime-Elemente aus TAL-Line auszulesen, die jedoch vor vollst�ndiger TAL-Abarbeitung noch nicht f�r alle TAL-Lines vorhanden sind. <BR>
 *          27.08.2015 MG F-Version (36_0), Mindestversionsanforderung CASCADE 7.0.1 / PSdZ 5.00.01 <BR>
 *          23.09.2015 MG T-Version (37_0), Mindestversionsanforderung CASCADE 7.1.0 / PSdZ 5.01.00 <BR>
 *          ---------- -- - LOP 1926: Nutzung des neuen TAL-Elements talGenerationReport (verf�gbar ab PSdZ 5.01) zur Erkennung von Fehlern, die bereits bei der TAL-Generierung aufgetreten sind. <BR>
 *          ---------- -- - LOP 1990: Fehler innerhalb von TA-Kategorien werden bei mehr als einer Transaktion pro TAL-Line (z.B. mehr als eine fscDeployTA) ebenfalls sicher erkannt und im Fehlerprotokoll ausgegeben. <BR>
 *          04.02.2016 MG F-Version (38_0), Mindestversionsanforderung CASCADE 7.1.0 / PSdZ 5.01.00 <BR>
 *          05.08.2016 MG T-Version (39_T) <BR>
 *          ---------- -- - Allgemeine (nicht SG spezifische) Fehler in talGenerationReport werden nun ebenfalls erfasst <BR>
 *          ---------- -- - LOP 2107: Im Fall einer fehlerhaften TAL wird allg. NIO-TAL-Gesamtstatus in Ergebnisliste nur noch dann ausgegeben, wenn keine weiteren allg. oder SG spezifischen TAL-Fehler erfasst wurden <BR>
 *          19.09.2016 MG F-Version (40_F), Mindestversionsanforderung CASCADE 7.1.0 / PSdZ 5.01.00 <BR>
 */
public class PSdZExecuteTAL_40_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * PSdZ als globale Variable
	 */
	private PSdZ psdz = null;

	/**
	 * DE fuer Sprachermittlung
	 */
	private final boolean DE = checkDE(); // Systemsprache DE wenn true

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSdZExecuteTAL_40_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSdZExecuteTAL_40_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "CODING_TYPE", "DEBUG", "DEBUG_PERFORM", "DYNAMIC_RETRIES", "MAX_ERROR_REPEAT", "MODE_SWITCH", "MODE_SWITCH_GATEWAY_LIST", "PARALLEL_EXECUTION", "SHOW_DURATION", "SHOW_TAL", "TAL_XML_FILE", "WRITE_SVT_SOLL_TO_VCM" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = {};
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		if( !super.checkArgs() )
			return false;
		return true;
	}

	/**
	 * Tr�gt ECUs in die Ergebnisliste (APDM) ein
	 * 
	 * @param mErgListe Ergebnisliste
	 * @param mMap Map mit einzutragenden ECU's
	 * @param mTransact einzutragende Transaktion
	 * @param mResult einzutragendes Ergebnis
	 * @param mPsdzErrors zugeh�rige einzutragende Fehler
	 * @param mECUStatistics Map mit SG Antwortverhalten
	 * @return failureLoggedToErgList gibt an, ob ein oder mehrere Fehler in Ergebnisliste eingetragen wurden
	 * @throws CascadePSdZRuntimeException
	 */
	private boolean entryECUsDetails( Vector<Ergebnis> mErgListe, TreeMap<String, HashSet<String>> mMap, String mTransact, String mSResult, HashMap<String, List<Exception>> mPsdzErrors, HashMap<String, String> mECUStatistics ) throws CascadePSdZRuntimeException, CascadePSdZConditionsNotCorrectException {
		boolean failureLoggedToErgList = false;
		Ergebnis mResult;
		String key, fehlertext, antwortVerhalten;
		CascadePSdZRuntimeException cscRE;
		Exception ex;
		String action = DE ? "Aktion: " : "Action: ";
		String actionDescriptionTalGenerationReport = DE ? "Auswertung des TAL-Generierungsberichts im Rahmen der TAL-Ausf�hrung" : "Evaluation of tal generation report during tal execution";
		String errorDescription = DE ? "Fehlerbeschreibung: " : "Error description: ";
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";
		String titleBestellTeilTAIS = DE ? "BestellNr: " : "OrderPartNo: "; // TAIS Nr. Bestellteil

		// Schleife �ber alle Eintr�ge der �bergebenen Map
		for( Iterator<Entry<String, HashSet<String>>> e = mMap.entrySet().iterator(); e.hasNext(); ) {
			Map.Entry<String, HashSet<String>> entry = e.next(); // z.B. SG:JBBF 0x00=[cdDeploy]
			cscRE = null;
			ex = null;

			String tempKey = entry.getKey();

			// etwas aufw�ndigerer Code (anstatt einfacher Suche nach ":"), stellt aber sicher, dass Basisvariante sicher ermittelt wird, auch dann wenn "psdz.SG" zuk�nftig ":" enthalten sollte(!)
			String sgBaseVariantName = tempKey.substring( tempKey.indexOf( PB.getString( "psdz.SG" ) ) + PB.getString( "psdz.SG" ).length() + 1, tempKey.indexOf( "0x" ) - 1 ); // z.B. "JBBF"
			String sgDiagAddress = tempKey.substring( tempKey.indexOf( "0x" ) ); // z.B. "0x00"

			HashSet<String> myContent = new HashSet<String>();
			myContent = ((HashSet<String>) entry.getValue()); // z.B. [cdDeploy]

			Iterator<String> i = myContent.iterator();
			StringBuffer text = new StringBuffer();
			boolean hasNext = i.hasNext();
			Object o = null;

			while( hasNext ) {
				o = i.next();
				if( String.valueOf( o ).toLowerCase().trim().equalsIgnoreCase( "talgeneration" ) ) {
					// behalte Textfeld unver�ndert bei
				} else {
					text.append( PB.getString( "psdz.tal." + String.valueOf( o ).toLowerCase().trim() ) );
				}
				hasNext = i.hasNext();
				if( hasNext )
					text.append( ", " );
			}

			if( !mSResult.equalsIgnoreCase( Ergebnis.FT_IO ) ) {
				for( Iterator<String> j = myContent.iterator(); j.hasNext(); ) {

					String sgCatOrTalGenAction = j.next().toLowerCase().trim(); // z.B. "cddeploy"
					List<Exception> psdZErrorsExceptionList = mPsdzErrors.get( sgDiagAddress + "_" + sgCatOrTalGenAction );

					// extrahiere alle Exceptions (falls vorhanden) aus dem aktuell selektierten Key von mPsdzErrors (z.B. "0x00_cddeploy")
					if( psdZErrorsExceptionList != null ) {
						for( int excep = 0; excep < psdZErrorsExceptionList.size(); excep++ ) {
							cscRE = null;
							ex = psdZErrorsExceptionList.get( excep );
							if( ex instanceof CascadePSdZRuntimeException ) {
								cscRE = (CascadePSdZRuntimeException) ex;
							}

							// den anderen Key (-> Diagnose-Adresse ohne 0x und ohne f�hrende Nullen) f�r die ECUStatistics holen (etwas aufwendig...)
							key = sgDiagAddress.charAt( 2 ) == '0' ? sgDiagAddress.substring( 3, 4 ) : sgDiagAddress.substring( 2, 4 );

							// der Fehlertext
							if( cscRE != null ) {
								fehlertext = action + (sgCatOrTalGenAction.equalsIgnoreCase( "talgeneration" ) ? actionDescriptionTalGenerationReport : cscRE.getStrErrorContextTranslated()) + ", " + errorDescription + cscRE.getMessage() + ", (PSdZ ID: " + cscRE.getMessageId() + ")";
							} else {
								fehlertext = PB.getString( mTransact );
							}

							// SG Antwortverhalten nur eintragen, wenn SG nie geantwortet hat oder sporadisch
							if( mECUStatistics.get( key ).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.0" ) ) || mECUStatistics.get( key ).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.1" ) ) )
								antwortVerhalten = " (" + PB.getString( "psdz.SG" ) + " " + PB.getString( "psdz.antwortet" ) + " " + mECUStatistics.get( key ) + ")";
							else
								antwortVerhalten = "";

							// LOP 1436: Ausgabe um TAIS Teilenummer anreichern und zus�tzlich, wenn m�glich noch die TAIS Nummer des bestellbaren Teils mit ausgeben. Unterdr�cke die Bestellnummer, wenn diese gleich der Nummer des logTeils ist

							String logTeil = "";
							String bestellTeilTAIS = "";
							StringBuffer strBufNummernTAIS = new StringBuffer();

							if( psdz.getLogistischesTeil( key ) != null ) {
								logTeil = psdz.getLogistischesTeil( key ).getSachNrTAIS();
								strBufNummernTAIS.append( ", " + PB.getString( "psdz.text.TaisNummer" ) + logTeil );

								bestellTeilTAIS = psdz.getLogistischesTeil( key ).getSachNrBestellbaresTeilTAIS();
								strBufNummernTAIS.append( logTeil.equals( bestellTeilTAIS ) ? "" : " (" + titleBestellTeilTAIS + bestellTeilTAIS + ")" );
							}

							mResult = new Ergebnis( "PSdZExecuteTAL", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress + strBufNummernTAIS.toString(), text.toString(), "", "STATUS", statusNotOkay, "", "", "0", "", "", "", fehlertext + antwortVerhalten, "", mSResult );
							mErgListe.add( mResult );
							failureLoggedToErgList = true;
						}
					}
				}
			} else {
				mResult = new Ergebnis( "PSdZExecuteTAL", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, text.toString(), "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", mSResult );
				mErgListe.add( mResult );
			}
		}
		return failureLoggedToErgList;
	}

	/**
	 * Eintrag der allgemeinen Fehler, der TalGenFailure-ECUs, der NIO-ECUs und der NotExecutable-ECUs ins APDM
	 * 
	 * @param gErgListe Ergebnisliste
	 * @param gMapTalGenFailure Map mit TalGenFailure-ECU's
	 * @param gMapNIO Map mit NIO-ECU's
	 * @param gMapNotExec Map mit NotExecutable-ECU's
	 * @param gPsdzErrors zugeh�rige einzutragende Fehler
	 * @param gECUStatistics Map mit SG Antwortverhalten
	 * @return failureLoggedToErgList gibt an, ob ein oder mehrere Fehler in Ergebnisliste eingetragen wurden
	 * @throws CascadePSdZRuntimeException
	 */
	private boolean entryECUsFT_NIO( Vector<Ergebnis> gErgListe, TreeMap<String, HashSet<String>> gMapTalGenFailure, TreeMap<String, HashSet<String>> gMapNIO, TreeMap<String, HashSet<String>> gMapNotExec, HashMap<String, List<Exception>> gPsdzErrors, HashMap<String, String> gECUStatistics ) throws CascadePSdZRuntimeException, CascadePSdZConditionsNotCorrectException {
		List<Boolean> failureLoggedToErgList = new ArrayList<Boolean>();
		CascadePSdZRuntimeException cscRE;
		Ergebnis gResult;
		String action = DE ? "Aktion: " : "Action: ";
		String actionDescriptionTalGenerationReport = DE ? "Auswertung des TAL-Generierungsberichts im Rahmen der TAL-Ausf�hrung" : "Evaluation of tal generation report during tal execution";
		String errorDescription = DE ? "Fehlerbeschreibung: " : "Error description: ";
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";

		// Haben wir irgendeinen allgemeinen Fehler (TAL-Generierungsbericht) ohne SG Adresse?
		if( gPsdzErrors.containsKey( "generalTALgenerationFailure" ) ) {
			// Liste der allgemeinen TAL-Generierungsfehler durchgehen
			for( Exception ex : gPsdzErrors.get( "generalTALgenerationFailure" ) ) {
				cscRE = null;
				if( ex instanceof CascadePSdZRuntimeException ) {
					cscRE = (CascadePSdZRuntimeException) ex;
				}

				gResult = new Ergebnis( "PSdZExecuteTAL", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", cscRE != null ? action + actionDescriptionTalGenerationReport + ", " + errorDescription + cscRE.getMessage() + ", (PSdZ ID: " + cscRE.getMessageId() + ")" : ex.getMessage(), "", Ergebnis.FT_NIO );
				gErgListe.add( gResult );
				failureLoggedToErgList.add( Boolean.TRUE );
			}
		}
		// Haben wir irgendeinen allgemeinen Fehler (TAL) ohne SG Adresse?
		if( gPsdzErrors.containsKey( "generalTALFailure" ) ) {
			// Liste der allgemeinen TAL-Fehler durchgehen
			for( Exception ex : gPsdzErrors.get( "generalTALFailure" ) ) {
				cscRE = null;
				if( ex instanceof CascadePSdZRuntimeException ) {
					cscRE = (CascadePSdZRuntimeException) ex;
				}

				gResult = new Ergebnis( "PSdZExecuteTAL", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", cscRE != null ? action + cscRE.getStrErrorContextTranslated() + ", " + errorDescription + cscRE.getMessage() + ", (PSdZ ID: " + cscRE.getMessageId() + ")" : ex.getMessage(), "", Ergebnis.FT_NIO );
				gErgListe.add( gResult );
				failureLoggedToErgList.add( Boolean.TRUE );
			}
		}

		// TalGenFailure ECUs in Ergebnisliste (APDM) eintragen
		failureLoggedToErgList.add( entryECUsDetails( gErgListe, gMapTalGenFailure, "psdz.tal.talgenfailure", Ergebnis.FT_NIO, gPsdzErrors, gECUStatistics ) );

		// NIO ECUs in Ergebnisliste (APDM) eintragen
		failureLoggedToErgList.add( entryECUsDetails( gErgListe, gMapNIO, "psdz.tal.transactionNIO", Ergebnis.FT_NIO, gPsdzErrors, gECUStatistics ) );

		// NIO ECUs NotExecutable in Ergebnisliste (APDM) eintragen
		failureLoggedToErgList.add( entryECUsDetails( gErgListe, gMapNotExec, "psdz.tal.transactionNOTExecutable", Ergebnis.FT_NIO, gPsdzErrors, gECUStatistics ) );

		return failureLoggedToErgList.contains( Boolean.TRUE );
	}

	/**
	 * Aufbau einer HashMap aus den Exceptions der TAL (Aufbau: key = Steuerger�teadresse in hex + _ + TACategorie z.B. '0x00_cddeploy', wert = 'Exceptions', oder key = "generalTALFailure" oder = "generalTALgenerationFailure", wert = 'Exceptions')
	 * 
	 * @param mTALResult zu analysierende CascadeTAL
	 * @return HashMap aus input-CascadeTAL erzeugte HashMap
	 */
	private HashMap<String, List<Exception>> buildExcHashMap( CascadeTAL mTALResult ) {
		HashMap<String, List<Exception>> ret_val = new HashMap<String, List<Exception>>();

		// Exceptions in der TAL?
		if( mTALResult.getExceptions().size() > 0 ) {
			ret_val.put( "generalTALFailure", mTALResult.getExceptions() );
		}

		CascadeTALLine[] myLines = mTALResult.getTalLines();
		for( int i = 0; i < myLines.length; i++ ) {
			String sgAddressTemp = "0" + Integer.toHexString( myLines[i].getDiagnosticAddress().intValue() ).toUpperCase(); // z.B. "012"
			String sgDiagAddress = "0x" + sgAddressTemp.substring( sgAddressTemp.length() - 2, sgAddressTemp.length() ); // z.B. "0x12"
			String sgCat = "_" + myLines[i].getTALTACategories()[0].getCategory().toString().toLowerCase().trim(); // z.B. "_swdeploy"
			boolean foundException = false;

			// Exceptions in der TALLine?
			if( myLines[i].getExceptions().size() > 0 ) {
				ret_val.put( sgDiagAddress + sgCat, myLines[i].getExceptions() );
				foundException = true;
			}

			if( !foundException ) {
				for( int j = 0; j < myLines[i].getTALTACategories().length; j++ ) {
					CascadeTALTACategory myTALTACategory = myLines[i].getTALTACategories()[j];
					sgCat = "_" + myTALTACategory.getCategory().toString().toLowerCase().trim();
					// Pr�fung der TAs
					for( int k = 0; k < myTALTACategory.getTAs().length; k++ ) {
						CascadeTA myTA = myTALTACategory.getTAs()[k];
						// Exceptions in der Transaktion?
						if( !myTA.getExceptions().isEmpty() ) {
							// erste gefundene Exception wird gespeichert und weitere Suche abgebrochen, da PSdZ in diesem Fall auch keine Folge-Transaktionen innerhalb der TA-Kategorie mehr anst��t 
							// und bei einem erneuten put-Aufruf auf das ret_val-Objekt f�r den gleichen key der zuerst gespeicherte value (-> erste Exception) �berschrieben w�rde
							ret_val.put( sgDiagAddress + sgCat, myTA.getExceptions() );
							break;
						}
					}
					// Pr�fung der FscTAs					
					for( int k = 0; k < myTALTACategory.getFscTAs().length; k++ ) {
						CascadeFscTA myFscTA = myTALTACategory.getFscTAs()[k];
						// Exceptions in der FSC Transaktion?
						if( !myFscTA.getExceptions().isEmpty() ) {
							// erste gefundene Exception wird gespeichert und weitere Suche abgebrochen, da PSdZ in diesem Fall auch keine Folge-Transaktionen innerhalb der TA-Kategorie mehr anst��t 
							// und bei einem erneuten put-Aufruf auf das ret_val-Objekt f�r den gleichen key der zuerst gespeicherte value (-> erste Exception) �berschrieben w�rde
							ret_val.put( sgDiagAddress + sgCat, myFscTA.getExceptions() );
							break;
						}
					}
				}
			}
		}

		// Exception in talGenerationReport ?

		// LOP 1926: Hier soll gepr�ft werden, ob Elemente im talGenerationReport vorhanden sind. Falls ja, werden die Fehler in die Hashmap ret_val aufgenommen entweder SG spezifisch
		// (key -> Diag-Adresse) oder nicht SG spezifisch (key -> "generalTALgenerationFailure")

		CascadeTALGenerationReport talGenerationReportRootElement = mTALResult.getCascadeTalGenerationReport();
		List<Exception> talGenerationReportEcuSpecificExceptions = new ArrayList<Exception>();
		List<Exception> talGenerationReportGeneralExceptions = new ArrayList<Exception>();

		for( int j = 0; j < talGenerationReportRootElement.getExceptions().size(); j++ ) {
			CascadePSdZRuntimeException myCascadePsdzRuntimeExecption = (CascadePSdZRuntimeException) talGenerationReportRootElement.getExceptions().get( j );

			String sgDiagAddr = myCascadePsdzRuntimeExecption.getDiagnosticAdressHex();
			String sgAction = "_" + "talgeneration";

			// Fehler mit ECU-Bezug?
			if( !sgDiagAddr.equalsIgnoreCase( "-1" ) ) {
				// Key von ret_val z.B. "0x12_talgeneration"
				// enth�lt ret_val bereits Value zu Key wird Exception-Liste um weitere Exception angereichert, sonst wird Exception-Liste erstmalig mit Exception bef�llt
				talGenerationReportEcuSpecificExceptions = ret_val.get( sgDiagAddr + sgAction );
				if( talGenerationReportEcuSpecificExceptions == null ) {
					talGenerationReportEcuSpecificExceptions = new ArrayList<Exception>();
				}

				talGenerationReportEcuSpecificExceptions.add( talGenerationReportRootElement.getExceptions().get( j ) );

				ret_val.put( sgDiagAddr + sgAction, talGenerationReportEcuSpecificExceptions );
			} else
				// Fehler ohne ECU-Bezug?
				// addiere allg. talGen-Exception ohne ECU-Bezug in Liste
				talGenerationReportGeneralExceptions.add( talGenerationReportRootElement.getExceptions().get( j ) );
		}
		if( !talGenerationReportGeneralExceptions.isEmpty() ) {
			ret_val.put( "generalTALgenerationFailure", talGenerationReportGeneralExceptions );
		}

		return ret_val;
	}

	/**
	 * Aufbau von drei TreeMaps aus den Ergebnissen der TAL (Aufbau: key = 'SG:' + Basisvariantenname + ' ' + Steuerger�teadresse in hex, wert = TALCategorie z.B. 'SG:JBBF 0x00', wert = 'cdDeploy'
	 * 
	 * @param mTALResult zu analysierende CascadeTAL
	 * @param mEcuTalGenFailure aus input-CascadeTAL erzeugte TreeMapp mit Fehler-Eintr�gen aus CascadeTalGenerationReport
	 * @param mEcuIO aus input-CascadeTAL erzeugte TreeMap mit CascadeExcecutionStatus = PROCESSED
	 * @param mEcuNIO aus input-CascadeTAL erzeugte TreeMap mit CascadeExcecutionStatus = PROCESSEDWITHERROR
	 * @param mEcuNotExecutable aus input-CascadeTAL erzeugte TreeMap mit CascadeExcecutionStatus = NOTEXECUTABLE
	 */
	private void buildMaps( CascadeTAL mTALResult, TreeMap<String, HashSet<String>> mEcuTalGenFailure, TreeMap<String, HashSet<String>> mEcuIO, TreeMap<String, HashSet<String>> mEcuNIO, TreeMap<String, HashSet<String>> mEcuNotExecutable ) {
		// Leeren der Maps
		mEcuTalGenFailure.clear();
		mEcuIO.clear();
		mEcuNIO.clear();
		mEcuNotExecutable.clear();

		// Ergebnisanalyse

		// LOP 1926: Hier soll gepr�ft werden, ob Elemente im talGenerationReport vorhanden sind. Falls ja, werden die betroffenen Steuerger�te in die Map mEcuTalGenFailure aufgenommen, sofern es
		// sich bei dem Fehler um einen SG spezifischen Fehler handelt.
		// Hintergrund: F�r SGe, die bereits im talGenerationReport auffallen, wird entweder eine TAL-Line mit Status NOTEXECUTABLE, oder �berhaupt keine TAL-Line, jedoch in jedem Fall ein Eintrag in den talGenerationReport erzeugt. 
		// Durch einen Eintrag in der Map mEcuTalGenFailure wird dokumentiert, dass das SG zwar behandelt werden sollte, jedoch bereits bei der TAL-Generierung "aussortiert" wurde.

		for( int j = 0; j < mTALResult.getCascadeTalGenerationReport().getExceptions().size(); j++ ) {
			CascadePSdZRuntimeException myCascadePsdzRuntimeExecption = (CascadePSdZRuntimeException) mTALResult.getCascadeTalGenerationReport().getExceptions().get( j );
			// Eintrag in Map nur bei Fehlern mit ECU-Bezug, d.h. bei Diagnoseadresse != "-1"!
			if( !myCascadePsdzRuntimeExecption.getDiagnosticAdressHex().equalsIgnoreCase( "-1" ) ) {
				String sgIDName = PB.getString( "psdz.SG" ) + ":" + myCascadePsdzRuntimeExecption.getStrEcuBV() + " " + myCascadePsdzRuntimeExecption.getDiagnosticAdressHex();

				HashSet<String> setTalGenerationAction = new HashSet<String>();
				// pr�fe ob schon eingetragen
				if( mEcuTalGenFailure.containsKey( sgIDName ) )
					setTalGenerationAction = mEcuTalGenFailure.get( sgIDName );

				setTalGenerationAction.add( "talGeneration" );
				mEcuTalGenFailure.put( sgIDName, setTalGenerationAction );
			}
		}

		for( int i = 0; i < mTALResult.getTalLines().length; i++ ) {
			// BasisName des SG ermitteln ("ECU:'Basisname'" + " 0x'hexcode'")
			String sgAddressTemp = "0" + Integer.toHexString( mTALResult.getTalLines()[i].getDiagnosticAddress().intValue() ).toUpperCase(); // z.B. "012"
			String sgIDName = PB.getString( "psdz.SG" ) + ":" + mTALResult.getTalLines()[i].getBaseVariantName() + " " + "0x" + sgAddressTemp.substring( sgAddressTemp.length() - 2, sgAddressTemp.length() ); // z.B. "SG:DME 0x12"

			// Status der einzelnen TAs und SWTs holen und merken in IO-, NIO- und NotExecutable-Listen
			for( int j = 0; j < mTALResult.getTalLines()[i].getTALTACategories().length; j++ ) {
				// IO TAL-Lines analysieren und TalLine-Kategorien merken
				// Es wird der TAL-Line Status analysiert und nicht der Status der TALTACategories, damit Fehler, die nach der Transaktion auftreten (-> FINALIZATION) auch erfasst werden 
				if( mTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.PROCESSED ) || mTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.INACTIVE ) ) {
					HashSet<String> setTalTaCategoriesIO = new HashSet<String>();
					// pr�fe ob schon eingetragen
					if( mEcuIO.containsKey( sgIDName ) )
						setTalTaCategoriesIO = mEcuIO.get( sgIDName );

					setTalTaCategoriesIO.add( mTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString() );
					mEcuIO.put( sgIDName, setTalTaCategoriesIO );
				}

				// NIO TAL-Lines analysieren und TalLine-Kategorien merken (z.B. blFlash, swDeploy)
				if( mTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.PROCESSEDWITHERROR ) ) {
					HashSet<String> setTalTaCategoriesNIO = new HashSet<String>();
					// pr�fe ob schon eingetragen
					if( mEcuNIO.containsKey( sgIDName ) )
						setTalTaCategoriesNIO = mEcuNIO.get( sgIDName );

					setTalTaCategoriesNIO.add( mTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString() );
					mEcuNIO.put( sgIDName, setTalTaCategoriesNIO );
				}

				// NotExcecutable (auch NIO !)
				if( mTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.NOTEXECUTABLE ) ) {
					HashSet<String> setTalTaCategoriesNotExecutable = new HashSet<String>();
					// pr�fe ob schon eingetragen
					if( mEcuNotExecutable.containsKey( sgIDName ) )
						setTalTaCategoriesNotExecutable = mEcuNotExecutable.get( sgIDName );

					setTalTaCategoriesNotExecutable.add( mTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString() );
					// Key von mEcuNotExecutable z.B. "SG:DME 0x12", value z.B. "cdDeploy" 
					mEcuNotExecutable.put( sgIDName, setTalTaCategoriesNotExecutable );
				}
			}
		}
	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {

		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
		boolean bDebugPerform = true; // gibt an, ob eine Performencemessung erfolgen sollen
		boolean bDoRepeat = false;
		boolean bFailureLoggedToErgList = false; // gib an, ob ein Fehler in Ergebnisliste eingetragen wurde
		boolean bModusSwitch = false; // per default keine Modusumschaltung
		boolean bParallelExecution = true; // Voreinstellung: parallele Ausf�hrung erlauben
		boolean bShowTAL = false; // keine Anzeige der TAL als default
		boolean bWriteSvtSollToVCM = false; // gibt an, ob eine SVT-Soll im Rahmen der TAL-Abarbeitung ins VCM geschrieben werden soll
		final int I_TAL_POLLTIME = 1000; // alle I_TAL_POLLTIME in ms TAL Status �berpr�fen
		int iDynamicRetries = 0; // Die maximale Anzahl der dynamischen Retries: Nach der Ausf�hrung werden die fehlerhaften actions in eine neue TAL integriert und diese TAL nochmals ausgef�hrt
		int iMaxErrorRepeat = 1; // Voreinstellung: maximal 1 Wdhlg im Fehlerfall
		int iRuns = 0; // Anzahl der bereits durchgef�hrten l�ufe
		int iShowDuration = 3; // Anzeige per default 3 Sekunden
		int status = STATUS_EXECUTION_OK;

		Ergebnis result = null;
		Set<String> setEcuIO = new HashSet<String>(), setEcuNIO = new HashSet<String>(), setEcuOpen = new HashSet<String>(); // Verwaltung der verarbeiteten SG [IO, NIO, OFFEN]
		String codingType = "FA"; // Modi: default FA: Codierung auf Basis orderXML, SHIPMENT: Codierung auf Anlieferzustand, FDL: Modus FDL
		String optionalArgumentName = null;
		String optionalArgumentValue = null;
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";
		String strTAL_XML_File = null; // TAL XML File mit Pfad von extern
		String testScreenID = this.getPr�flingLaufzeitUmgebung().getName(); // TestScreenID (aus Pr�fstandsinstanz)
		// Liste der umzuschaltenden Gateways [default aus Template-Bedatung typischerweise leer, umzuschaltenden Gateways k�nnen hier�ber eingegrenzt werden]
		String[] modeSwitchGatewayList = (String[]) Collections.emptyList().toArray( new String[Collections.emptyList().size()] );
		String[] optionalArgumentsNames = getOptionalArgs();
		String[] optionalArgumentsValues = null;
		// Verwaltung der verarbeiteten SG [TALGENFAILURE, IO, NIO, NOTEXECUTABLE] in sortierten Listen mit Variable=ECU und Wert=TA, SWT Kategories als HashSet; Bsp.: Variable=ECU:DSC 0x29, Wert=cdDeploy, fscDeploy als HashSet
		TreeMap<String, HashSet<String>> mapEcuTalGenFailure = new TreeMap<String, HashSet<String>>();
		TreeMap<String, HashSet<String>> mapEcuIO = new TreeMap<String, HashSet<String>>();
		TreeMap<String, HashSet<String>> mapEcuNIO = new TreeMap<String, HashSet<String>>();
		TreeMap<String, HashSet<String>> mapEcuNotExecutable = new TreeMap<String, HashSet<String>>();
		UserDialog userDialog = null;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();

		try {

			// Argumente loggen, einlesen und checken
			try {

				// Ausgabe der Pr�fprozedurversion sowie s�mtlicher an die Pr�fprozedur �bergebener Argumente (required/optional) f�r sp�tere Analysezwecke
				System.out.println( "PSdZ ID:" + testScreenID + ", Initialize " + this.getClassName() );

				// aktuell keine zwingenden Argumente parametrierbar, deshalb nur Ausgabe optionaler Argumente
				for( int i = 0; i < optionalArgumentsNames.length; i++ ) {
					optionalArgumentName = optionalArgumentsNames[i];
					optionalArgumentValue = "";
					// gibt es optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( optionalArgumentName ) != null ) {
						optionalArgumentsValues = extractValues( getArg( optionalArgumentName ) );
						for( int k = 0; k < optionalArgumentsValues.length; k++ ) {
							// Listen von optionalen Argumenten werden durch ";" getrennt ausgegeben
							optionalArgumentValue = optionalArgumentValue + optionalArgumentsValues[k] + ((optionalArgumentsValues.length > 1 && k < (optionalArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						optionalArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZExecuteTAL with optional argument: " + optionalArgumentName + " = " + optionalArgumentValue );
				}

				// Obligatorische Argumentpr�fung auf Vorhandensein von nicht optionalen sowie �berz�hligen Argumenten
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// CODING_TYPE
				if( getArg( "CODING_TYPE" ) != null ) {
					codingType = extractValues( getArg( "CODING_TYPE" ) )[0];
				}

				// DEBUG
				// DEBUG �ber Parameter
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
					else
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				} else {
					// alternativ DEBUG �ber Pr�fstandsvariable noch auswerten
					try {
						Object objDebug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
						if( objDebug != null ) {
							if( objDebug instanceof String && "TRUE".equalsIgnoreCase( (String) objDebug ) )
								bDebug = true;
							else if( objDebug instanceof Boolean && ((Boolean) objDebug).booleanValue() )
								bDebug = true;
						}
					} catch( VariablesException e ) {
						// Nothing, bDebug ist false vorbelegt
					}
				}

				// DEBUG_PERFORM
				if( getArg( "DEBUG_PERFORM" ) != null ) {
					if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "TRUE" ) )
						bDebugPerform = true;
					else if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "FALSE" ) )
						bDebugPerform = false;
					else
						throw new PPExecutionException( "DEBUG_PERFORM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DYNAMIC_RETRIES
				if( getArg( "DYNAMIC_RETRIES" ) != null ) {
					try {
						iDynamicRetries = new Integer( extractValues( getArg( "DYNAMIC_RETRIES" ) )[0] ).intValue();
					} catch( NumberFormatException e ) {
						throw new PPExecutionException( "DYNAMIC_RETRIES " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
					}
				}

				// MAX_ERROR_REPEAT
				if( getArg( "MAX_ERROR_REPEAT" ) != null ) {
					try {
						iMaxErrorRepeat = Integer.parseInt( extractValues( getArg( "MAX_ERROR_REPEAT" ) )[0] );
						if( iMaxErrorRepeat > 10 || iMaxErrorRepeat < 1 )
							throw new NumberFormatException( "Fehler: Wert muss 1 <= x <= 10 sein!" );
					} catch( NumberFormatException nfe ) {
						throw new PPExecutionException( PB.getString( "psdz.parameter.ParameterfehlerMaxErrorRepeat" ) );
					}
				}

				// MODE_SWITCH
				if( getArg( "MODE_SWITCH" ) != null ) {
					if( getArg( "MODE_SWITCH" ).equalsIgnoreCase( "TRUE" ) )
						bModusSwitch = true;
					else if( getArg( "MODE_SWITCH" ).equalsIgnoreCase( "FALSE" ) )
						bModusSwitch = false;
					else
						throw new PPExecutionException( "MODE_SWITCH " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// MODE_SWITCH_GATEWAY_LIST
				if( getArg( "MODE_SWITCH_GATEWAY_LIST" ) != null ) {
					modeSwitchGatewayList = extractValues( getArg( "MODE_SWITCH_GATEWAY_LIST" ) );
				}

				// PARALLEL_EXECUTION
				if( getArg( "PARALLEL_EXECUTION" ) != null ) {
					if( getArg( "PARALLEL_EXECUTION" ).equalsIgnoreCase( "TRUE" ) )
						bParallelExecution = true;
					else if( getArg( "PARALLEL_EXECUTION" ).equalsIgnoreCase( "FALSE" ) )
						bParallelExecution = false;
					else
						throw new PPExecutionException( "PARALLEL_EXECUTION " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// SHOW_DURATION
				if( getArg( "SHOW_DURATION" ) != null ) {
					try {
						iShowDuration = new Integer( extractValues( getArg( "SHOW_DURATION" ) )[0] ).intValue();
					} catch( NumberFormatException e ) {
						throw new PPExecutionException( "SHOW_DURATION " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
					}
				}

				// SHOW_TAL
				if( getArg( "SHOW_TAL" ) != null ) {
					if( getArg( "SHOW_TAL" ).equalsIgnoreCase( "TRUE" ) )
						bShowTAL = true;
					else if( getArg( "SHOW_TAL" ).equalsIgnoreCase( "FALSE" ) )
						bShowTAL = false;
					else
						throw new PPExecutionException( "SHOW_TAL " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// TAL_XML_FILE
				if( getArg( "TAL_XML_FILE" ) != null ) {
					strTAL_XML_File = extractValues( getArg( "TAL_XML_FILE" ) )[0];
				}

				// WRITE_SVT_SOLL_TO_VCM
				if( getArg( "WRITE_SVT_SOLL_TO_VCM" ) != null ) {
					if( getArg( "WRITE_SVT_SOLL_TO_VCM" ).equalsIgnoreCase( "TRUE" ) )
						bWriteSvtSollToVCM = true;
					else if( getArg( "WRITE_SVT_SOLL_TO_VCM" ).equalsIgnoreCase( "FALSE" ) )
						bWriteSvtSollToVCM = false;
					else
						throw new PPExecutionException( "WRITE_SVT_SOLL_TO_VCM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// setze tempor�re Variablen
			try {
				// setze tempor�re Variablen f�r die Modusumschaltung
				this.getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_MODE_SWITCH", new Boolean( bModusSwitch ) );
				this.getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_MODE_SWITCH_GATEWAY_LIST", new ArrayList<String>( Arrays.asList( modeSwitchGatewayList ) ) );
				// setze tempor�re Variablen f�r den CodingType
				this.getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_CODING_TYPE", codingType );
			} catch( Exception e ) {
				// Nothing
			}

			// Performancemessung Start
			if( bDebugPerform )
				System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL PP START" );

			// Devices holen
			try {
				psdz = getPr�flingLaufzeitUmgebung().getDeviceManager().getPSdZ();
				if( bDebug )
					CascadeLogging.getLogger().log( LogLevel.INFO, "PSdZExecuteTAL: Get PSdZ Device finished." );
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			try {
				userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
				// Informiere den Benutzer
				userDialog.setDisplayProgress( true );
				userDialog.getProgressbar().setIndeterminate( true );
				userDialog.displayMessage( PB.getString( "psdz.ud.TALAusfuehrung" ), ";" + PB.getString( "psdz.ud.TALAusfuehrunglaeuft" ), -1 );
				if( bDebug )
					CascadeLogging.getLogger().log( LogLevel.INFO, "PSdZExecuteTAL: Get UserDialog Device finished." );
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			setEcuIO.clear();
			setEcuNIO.clear();
			setEcuOpen.clear();

			//Schleif f�r automatische Wiederholungen im Fehlerfall
			while( iRuns == 0 || bDoRepeat ) {
				iRuns++;
				bDoRepeat = false;

				try {

					// asynchrone Ausf�hrung einer TAL starten
					if( strTAL_XML_File == null ) {
						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.startTALExecution' START" );
						}

						// Normalfall: Die TAL wird �ber SVTist / SVTsoll und TAL Generierung erzeugt
						// Das Schreiben der SVT-Soll in das VCM im Rahmen der TAL-Ausf�hrung wird unterbunden, da das vorherige Schreiben der SVT-Soll in das 
						// VCM durch den Pr�fablauf sichergestellt wird
						psdz.startTALExecution( bParallelExecution, iMaxErrorRepeat, bWriteSvtSollToVCM );

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.startTALExecution' ENDE" );
						}

					} else {
						// Spezialfall: Notfall wir m�ssen die TAL als XML File einlesen und �bergeben
						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.readAndStoreTALfromFile' START" );
						}

						// Lies das XML-File und speichere die TAL im PSdZ Device
						psdz.readAndStoreTALfromFile( strTAL_XML_File, true );

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.readAndStoreTALfromFile' ENDE" );
						}

						// TAL anzeigen und best�tigen?
						if( bShowTAL ) {
							// UserDialog zur�cksetzen
							userDialog.reset();

							StringBuffer sbTAL = new StringBuffer();
							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.getTAL' START" );
							}

							CascadeTAL casTAL = psdz.getTAL();

							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.getTAL' ENDE" );
							}

							// sbTAL.append( "Status: " + casTAL.getStatus() + ";" );
							// sbTAL.append( "Actual End: " + casTAL.getTime().getActualEndTime() + ";" );
							// sbTAL.append( "Planned End: " + casTAL.getTime().getPlannedEndTime() + ";" );
							// sbTAL.append( "Actual Start: " + casTAL.getTime().getActualStartTime() + ";" );
							// sbTAL.append( "Planned Start: " + casTAL.getTime().getPlannedStartTime() + ";;"
							// );
							CascadeTALLine[] myLines = casTAL.getTalLines();
							for( int i = 0; i < myLines.length; i++ ) {
								// sbTAL.append( "LINE " + i + ";" );
								sbTAL.append( PB.getString( "psdz.text.Steuergeraet" ) + myLines[i].getBaseVariantName() + ";" );
								sbTAL.append( PB.getString( "psdz.text.DiagAdresse" ) + Integer.toHexString( myLines[i].getDiagnosticAddress().intValue() ).toUpperCase() + PB.getString( "psdz.text.Dez" ) + myLines[i].getDiagnosticAddress() + ");" );
								// sbTAL.append( " Status: " + myLines[i].getStatus() + ";;" );
								for( int j = 0; j < myLines[i].getTALTACategories().length; j++ ) {
									CascadeTALTACategory myTALTACategory = myLines[i].getTALTACategories()[j];
									// sbTAL.append( " KATEGORIE " + j + ";" );
									sbTAL.append( PB.getString( "psdz.text.Aktion" ) + myTALTACategory.getCategory().toString() + " Status: " + myTALTACategory.getStatus() + ";" );
									// sbTAL.append( " Status: " + myTALTACategory.getStatus() + ";;" );
									for( int k = 0; k < myTALTACategory.getTAs().length; k++ ) {
										CascadeTA myTA = myTALTACategory.getTAs()[k];
										// sbTAL.append( " TRANSAKTION " + k + ";" );
										sbTAL.append( PB.getString( "psdz.text.Prozessklasse" ) + myTA.getProcessClass() + " Version: (" + myTA.getMainVersion() + "." + myTA.getSubVersion() + "." + myTA.getPatchVersion() + ");" );
										// sbTAL.append( " Status: " + myTA.getStatus() + ";" );
										// sbTAL.append( " Main: " + myTA.getMainVersion() + ";" );
										// sbTAL.append( " Sub: " + myTA.getSubVersion() + ";" );
										// sbTAL.append( " Patch: " + myTA.getPatchVersion() + ";" );

									}
								}
								sbTAL.append( ";" );
							}
							// sbTAL.append( ";TAL FERTIG!" );
							// Drucken und Abbruch eingebaut
							userDialog.setLineWrap( false );
							userDialog.setNoButtonText( PB.getString( "psdz.ud.ButtonCancel" ) );
							userDialog.setAuxiliaryButtonVisibilityMask( UserDialog.PRINT_BUTTON_MASK );
							int iSelection = userDialog.requestUserInputDigital( PB.getString( "psdz.ud.TALOK" ), sbTAL.toString(), iShowDuration );

							// Wurde die TAL-Anzeige durch den Benutzter abgebrochen? (Nein Button)
							if( iSelection == 2 ) {

								// Performancemessung
								if( bDebugPerform ) {
									System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.cancelTALExecution' START" );
								}

								psdz.cancelTALExecution();

								// Performancemessung
								if( bDebugPerform ) {
									System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.cancelTALExecution' ENDE" );
								}

								result = new Ergebnis( "PSdZExecuteTAL", "Userdialog", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", PB.getString( "werkerAbbruch" ), "", PB.getString( "pollingAbbruch" ), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								throw new PPExecutionException();
							}
						}

						// Starte TAL-Ausf�hrung ohne Fahrzeugprofil (FP)
						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.startTALExecutionWithoutFP' START" );
						}

						// Das Schreiben der SVT-Soll in das VCM im Rahmen der TAL-Ausf�hrung wird unterbunden, da das vorherige Schreiben der SVT-Soll in das 
						// VCM immer durch den Pr�fablauf sichergestellt wird
						psdz.startTALExecutionWithoutFP( bParallelExecution, iMaxErrorRepeat, bWriteSvtSollToVCM );

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.startTALExecutionWithoutFP' ENDE" );
						}

					}

					// UserDialog Reset
					userDialog.reset();

					// Vorbereitung der Statusanzeige, w�hrend der Ausf�hrung
					userDialog.setAllowCancel( true );
					userDialog.setLineWrap( false );
					userDialog.setDisplayProgress( true );
					userDialog.getProgressbar().setIndeterminate( false );

					// �berpr�fe pollend die Ausf�hrung der TAL
					while( true ) {

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.getTALExecutionStatus' START" );
						}
						// lese den Status + hier wird f�r I_TAL_POLLTIME in ms gewartet
						CascadeTAL myTALResult = psdz.getTALExecutionStatus( I_TAL_POLLTIME, null );

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.getTALExecutionStatus' ENDE" );
						}

						// Ergebnisanalyse
						int iECU_IO = 0, iECU_NIO = 0, iECU_OPEN = 0;
						setEcuIO.clear();
						setEcuNIO.clear();
						setEcuOpen.clear();

						// eigentliche Ergebnisanalyse
						for( int i = 0; i < myTALResult.getTalLines().length; i++ ) {
							// BasisName des SG ermitteln ("ECU:'Basisname'" + " 0x'hexcode'")							
							String sgAddressTemp = "0" + Integer.toHexString( myTALResult.getTalLines()[i].getDiagnosticAddress().intValue() ).toUpperCase();
							String sgIDName = PB.getString( "psdz.SG" ) + ":" + myTALResult.getTalLines()[i].getBaseVariantName() + " " + "0x" + sgAddressTemp.substring( sgAddressTemp.length() - 2, sgAddressTemp.length() );

							// falls schon vorhanden, dieses ECU erst mal aus den anderen Listen entfernen // Executable ist dominant
							setEcuIO.remove( sgIDName );
							setEcuNIO.remove( sgIDName );

							// Anzahl ECU_OPEN's
							// Es wird der TAL-Line Status analysiert und nicht der Status der TALTACategories, damit Fehler, die nach der Transaktion auftreten (-> FINALIZATION) auch erfasst werden 
							if( !myTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.PROCESSED ) && !myTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.PROCESSEDWITHERROR ) && !myTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.INACTIVE ) && !myTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.NOTEXECUTABLE ) )
								setEcuOpen.add( sgIDName );

							// Anzahl ECU_NIO's holen
							if( myTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.PROCESSEDWITHERROR ) || myTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.NOTEXECUTABLE ) ) {
								if( !setEcuIO.contains( sgIDName ) && !setEcuOpen.contains( sgIDName ) )
									setEcuNIO.add( sgIDName );
							}

							// Anzahl ECU_IO's holen
							if( myTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.PROCESSED ) ) {
								if( !setEcuNIO.contains( sgIDName ) && !setEcuOpen.contains( sgIDName ) )
									setEcuIO.add( sgIDName );
							}

							// Werte setzen
							iECU_OPEN = setEcuOpen.size();
							iECU_NIO = setEcuNIO.size();
							iECU_IO = setEcuIO.size();
						}

						// Anzeige des Ergebnisses
						userDialog.displayMessage( (iRuns > 1 ? iRuns + ". " : "") + PB.getString( "psdz.ud.TALAusfuehrung" ), PB.getString( "psdz.ud.SGBehandlunglaeuft" ) + psdz.getIStufeSoll() + "\n" + PB.getString( "psdz.ud.VIN" ) + this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer() + ";" + iECU_IO + " " + PB.getString( "psdz.ud.ECUok" ) + iECU_NIO + " " + PB.getString( "psdz.ud.ECUnok" ) + iECU_OPEN + " " + PB.getString( "psdz.ud.ECUopen" ), -1 );
						userDialog.getProgressbar().setMaximum( iECU_IO + iECU_NIO + iECU_OPEN );
						userDialog.getProgressbar().setValue( iECU_IO + iECU_NIO );
						userDialog.getProgressbar().setString( (iECU_IO + iECU_NIO) + " / " + (iECU_IO + iECU_NIO + iECU_OPEN) );

						// CHECK TAL EXECUTION STATUS
						// fertig IO?
						// CascadeTALExecutionStatus.FINISHED_WITH_WARNINGS, CascadeTALExecutionStatus.FINISHED_FOR_HW_TRANSACTIONS_WITH_WARNINGS und CascadeTALExecutionStatus.FINISHED_FOR_HW_TRANSACTIONS werden auf CascadeTALExecutionStatus.FINISHED durch CASCADE-Kern-SW gemappt
						if( myTALResult.getStatus().getName().equals( CascadeTALExecutionStatus.FINISHED.getName() ) ) {

							//Ergebnis TAL auf PLatte ablegen
							psdz.getTALExecutionStatus( 0, "STATUS" );

							// Erzeugen der TreeMaps TalGenFailure, IO, NIO und NotExecutable
							buildMaps( myTALResult, mapEcuTalGenFailure, mapEcuIO, mapEcuNIO, mapEcuNotExecutable );

							// Timestamps aus TAL auswerten
							doDokuTALTimesToAPDM( ergListe, myTALResult );

							// IO - ECUs in Ergebnisliste (APDM) eintragen
							entryECUsDetails( ergListe, mapEcuIO, "psdz.tal.transactionIO", Ergebnis.FT_IO, null, null );

							// LOP 1926: Falls TAL-Status i.O., jedoch Eintr�ge in mapEcuTalGenFailure oder allg. TALgenerationFailure vorhanden, gab es Fehler bereits bei der TAL-Generierung, die hier behandelt werden

							// hole die aufgelaufenen Fehler schon mal vorab (-> psdzErrors), f�r den Fall, dass es nicht SG spezifische Fehler im TAL-Generation Report gibt, die sich ja nicht auf den TAL-Status auswirken und auch nicht in der mapEcuTalGenFailure enthalten sind
							HashMap<String, List<Exception>> psdzErrors = new HashMap<String, List<Exception>>( buildExcHashMap( myTALResult ) );
							// wenn es ECU-spezifische oder nicht ECU-spezifische Fehler in TAL-Generationreport gibt, dann Fehlerbehandlung durchf�hren 
							if( !mapEcuTalGenFailure.isEmpty() || psdzErrors.containsKey( "generalTALgenerationFailure" ) ) {
								HashMap<String, String> psdzECUStatisticsHexStr = new HashMap<String, String>( psdz.getEcuStatisticsHexString() );

								// Eintrag der TalGenFailure - ECUs ins APDM
								entryECUsFT_NIO( ergListe, mapEcuTalGenFailure, mapEcuNIO, mapEcuNotExecutable, psdzErrors, psdzECUStatisticsHexStr );

								// IO (!) Ergebnis in (APDM) eintragen, da zwar TalGenFailure vorliegt, jedoch die TAL ansonst fehlerfrei abgearbeitet wurde
								result = new Ergebnis( "PSdZExecuteTAL", "PSdZ", "", "", "", DE ? "Ausf�hrungsstatus der TAL" : "TAL execution status", myTALResult.getStatus().getName(), myTALResult.getStatus().getName(), "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );

								throw new PPExecutionException();
							} else {
								// IO Ergebnis in (APDM) eintragen
								result = new Ergebnis( "PSdZExecuteTAL", "PSdZ", "", "", "", DE ? "Ausf�hrungsstatus der TAL" : "TAL execution status", myTALResult.getStatus().getName(), myTALResult.getStatus().getName(), "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );

								// Wiederholschleife umgehen
								iRuns = iDynamicRetries + 1;
								bDoRepeat = false;
								break;
							}
						}

						// fertig NIO? // neue Stati
						// CascadeTALExecutionStatus.FINISHED_WITH_ERROR, CascadeTALExecutionStatus.ABORTED_BY_ERROR, CascadeTALExecutionStatus.ABORTED_BY_USER und CascadeTALExecutionStatus.FINISHED_FOR_HW_TRANSACTIONS_WITH_ERROR werden auf CascadeTALExecutionStatus.FINISHED_WITH_ERROR_IN_TRANSACTION durch CASCADE-Kern-SW gemappt
						if( myTALResult.getStatus().getName().equals( CascadeTALExecutionStatus.FINISHED_WITH_ERROR_IN_TRANSACTION.getName() ) ) {
							// mba: Nur wenn die maximale Anzahl �berschritten ist wird abgebrochen.
							if( iRuns > iDynamicRetries ) {

								//Ergebnis TAL auf PLatte ablegen
								psdz.getTALExecutionStatus( 0, "STATUS" );

								// Performancemessung
								if( bDebugPerform ) {
									System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.getEcuStatisticsHexString' START" );
								}
								// Erzeugen der TreeMaps TalGenFailure, IO, NIO und NotExecutable
								buildMaps( myTALResult, mapEcuTalGenFailure, mapEcuIO, mapEcuNIO, mapEcuNotExecutable );
								// PSdZ hole die aufgelaufenen Fehler
								HashMap<String, List<Exception>> psdzErrors = new HashMap<String, List<Exception>>( buildExcHashMap( myTALResult ) );
								HashMap<String, String> psdzECUStatisticsHexStr = new HashMap<String, String>( psdz.getEcuStatisticsHexString() );

								// Performancemessung
								if( bDebugPerform ) {
									System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.getEcuStatisticsHexString' ENDE" );
								}

								// Timestamps aus TAL auswerten
								doDokuTALTimesToAPDM( ergListe, myTALResult );

								// IO - ECUs in Ergebnisliste (APDM) eintragen
								entryECUsDetails( ergListe, mapEcuIO, "psdz.tal.transactionIO", Ergebnis.FT_IO, null, null );

								// Eintrag der TalGenFailure - / NIO - / NotExecutable - ECUs ins APDM
								bFailureLoggedToErgList = entryECUsFT_NIO( ergListe, mapEcuTalGenFailure, mapEcuNIO, mapEcuNotExecutable, psdzErrors, psdzECUStatisticsHexStr );

								// NIO Ergebnis (allg.) in APDM eintragen, jedoch nur, wenn bisher noch kein anderer allgemeiner oder SG spezifischer Fehler in Ergebnisliste erfasst wurde, jedoch der TAL-Status dennoch fehlerhaft war (sh. LOP 2107)
								if( !bFailureLoggedToErgList ) {
									result = new Ergebnis( "PSdZExecuteTAL", "PSdZ", "", "", "", DE ? "Ausf�hrungsstatus der TAL" : "TAL execution status", myTALResult.getStatus().getName(), CascadeTALExecutionStatus.FINISHED.toString(), "", "0", "", "", "", DE ? "Ausgef�hrte TAL enth�lt Fehler" : "Executed TAL contains errors", "", Ergebnis.FT_NIO );
									ergListe.add( result );
								}

								throw new PPExecutionException();
							}

							//dokumentiere TAL auf PLatte
							psdz.getTALExecutionStatus( 0, "REPEAT_" + iRuns ); // �nderungen

							// Timestamps aus TAL auswerten
							doDokuTALTimesToAPDM( ergListe, myTALResult );

							// Wiederhol-Flag setzen und Endlosschleife unterbrechen damit es weiter gehen kann
							bDoRepeat = true;
							if( bDebug )
								System.out.println( "Wiederhole TAL" );

							CascadeTALLine line = null;

							// �ber alle ausgef�hrten TAL-Lines gehen. Was vorher schon i.o. (processed) war, wird auf
							// INACTIVE gesetzt. Das wird nicht mehr ausgef�hrt und sp�ter als i.o. interpretiert
							for( int i = 0; i < myTALResult.getTalLines().length; i++ ) {
								line = myTALResult.getTalLines()[i];
								if( bDebug )
									System.out.println( "TalLine: " + line.getBaseVariantName() + ", " + line.getTALTACategories()[0].toString() );

								if( line.getStatus().equals( CascadeExecutionStatus.PROCESSED ) ) {
									// manipuliere die TAL lines
									psdz.manipulateTALLinePSdZ( line );
								} else {
									// TAL-Line fehlerhaft. Dokumentation f�r Wiederholung der TAL-Line
									String sgAddressTemp = "0" + Integer.toHexString( line.getDiagnosticAddress().intValue() ).toUpperCase(); // z.B. "012"
									String sgDiagAddress = "0x" + sgAddressTemp.substring( sgAddressTemp.length() - 2, sgAddressTemp.length() ); // z.B. "0x12"

									result = new Ergebnis( "PSdZExecuteTAL", "PSdZ", line.getBaseVariantName() + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, (DE ? "Wiederhole Ausf�hrung der TAL-Line" : "Retry execution of TAL line"), "", "", "", "", "", "0", "", "", "", "", "", Ergebnis.FT_RETRY );
									ergListe.add( result );
								}
							}

							// dokumentieren, dass TAL wiederholt wird
							result = new Ergebnis( "PSdZExecuteTAL", "PSdZ", "", "", "", (DE ? "TAL-Wiederholung n�tig. Bisherige TAL-Ausf�hrungen: " : "Retry TAL execution. Previous TAL executions: ") + iRuns + (DE ? ", max. Anzahl an TAL-Ausf�hrungen: " : ", max. number of TAL executions: ") + (iDynamicRetries + 1), "", "", "", "0", "", "", "", "", "", Ergebnis.FT_RETRY );
							ergListe.add( result );

							break;
						}
						// Abbruch durch Werker???
						if( userDialog.isCancelled() == true ) {
							userDialog.reset();

							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.cancelTALExecution' START" );
							}
							psdz.cancelTALExecution();
							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.cancelTALExecution' ENDE" );
							}

							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.getEcuStatisticsHexString' START" );
							}
							// Erzeugen der TreeMaps TalGenFailure, IO, NIO und NotExecutable
							buildMaps( myTALResult, mapEcuTalGenFailure, mapEcuIO, mapEcuNIO, mapEcuNotExecutable );
							// PSdZ hole die aufgelaufenen Events 
							HashMap<String, List<Exception>> psdzErrors = new HashMap<String, List<Exception>>( buildExcHashMap( myTALResult ) );
							HashMap<String, String> psdzECUStatisticsHexStr = new HashMap<String, String>( psdz.getEcuStatisticsHexString() );
							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL 'psdz.getEcuStatisticsHexString' ENDE" );
							}

							// Timestamps aus TAL auswerten
							doDokuTALTimesToAPDM( ergListe, myTALResult );

							// IO - ECUs in Ergebnisliste (APDM) eintragen
							entryECUsDetails( ergListe, mapEcuIO, "psdz.tal.transactionIO", Ergebnis.FT_IO, null, null );

							// Eintrag der TalGenFailure - / NIO - / NotExecutable - ECUs ins APDM
							entryECUsFT_NIO( ergListe, mapEcuTalGenFailure, mapEcuNIO, mapEcuNotExecutable, psdzErrors, psdzECUStatisticsHexStr );

							// Werkerabbruch in Ergebnisliste (APDM) eintragen
							result = new Ergebnis( "PSdZExecuteTAL", "Userdialog", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", PB.getString( "werkerAbbruch" ), "", PB.getString( "pollingAbbruch" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );

							throw new PPExecutionException();
						}
					}

				} catch( CascadePSdZConditionsNotCorrectException e ) {
					// Fehler abfangen und dokumentiern
					if( bDebug )
						e.printStackTrace();
					result = new Ergebnis( "PSdZExecuteTAL", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Bedingungen" ), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				} catch( CascadePSdZRuntimeException e ) {
					// Fehler abfangen und dokumentiern
					if( bDebug )
						e.printStackTrace();
					result = new Ergebnis( "PSdZExecuteTAL", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Laufzeit" ), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}
			} // Ende While-Schleife (iRuns)

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// *************************************************************************************************
		// * Check auf Fehler bei fehlertoleranter (weicher) KIS // z.B.: KIS Doppeltreffer -> PP dann NIO *
		// *************************************************************************************************
		try {
			List<CascadeConfigError> svbConfigErrors = psdz.getSollverbauungConfigErrors();

			CascadeConfigError myCascadeConfigError = null;

			// jetzt die ConfigError Liste durchgehen und Fehler eintragen
			for( int i = 0; i < svbConfigErrors.size(); i++ ) {
				myCascadeConfigError = svbConfigErrors.get( i );
				// Fehler eintragen und ausgeben
				result = new Ergebnis( "PSdZExecuteTAL", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Pr�fung der SVT-Soll bei Nutzung von fehlertoleranter KIS fehlgeschlagen! " : "SVT-Soll check failed when using error tolerant KIS! ") + (DE ? "KIS Fehler-ID: " : "KIS error ID: ") + myCascadeConfigError.getConfigErrorCode() + (DE ? ", SG: 0x" : ", ECU: 0x") + Integer.toHexString( myCascadeConfigError.getDiagAddress().intValue() ) + (DE ? ", KIS Fehlerbeschreibung: " : ", KIS error text: ") + myCascadeConfigError.getConfigErrorText(), "", Ergebnis.FT_NIO );
				ergListe.add( result );
			}

			// weiche KIS Fehler -> PP NIO 
			if( svbConfigErrors.size() > 0 )
				throw new PPExecutionException();
		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			status = STATUS_EXECUTION_ERROR;
		}

		// R�cksetzen tempor�rer Variablen
		try {
			// R�cksetzen der Variablen f�r die Modusumschaltung
			this.getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_MODE_SWITCH", new Boolean( false ) );
			this.getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_MODE_SWITCH_GATEWAY_LIST", new ArrayList<Object>() );
			// R�cksetzen der Variablen f�r den CodingType
			this.getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_CODING_TYPE", "FA" );
		} catch( Exception e ) {
			// Nothing
		}

		// Freigabe der benutzten Devices
		if( psdz != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releasePSdZ();
				if( bDebug )
					CascadeLogging.getLogger().log( LogLevel.INFO, "PSdZExecuteTAL: Release PSdZ Device finished." );
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
		try {
			getPr�flingLaufzeitUmgebung().releaseUserDialog();
			if( bDebug )
				CascadeLogging.getLogger().log( LogLevel.INFO, "PSdZExecuteTAL: Release UserDialog Device finished." );
		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			if( e instanceof DeviceLockedException )
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "PSdZExecuteTAL", "PSdZ", "", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}
		setPPStatus( info, status, ergListe );

		// globale Variablen zur�cksetzen
		psdz = null;

		// Performancemessung Ende
		if( bDebugPerform )
			System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteTAL PP ENDE" );
	}

	/**
	 * Zeitstempel aus TAL auswerten und in APDM dokumentieren
	 * 
	 * @param ergListe, Vektor der Ergebnisliste
	 * @param TALResult, Ergebnis-TAL
	 * @throws CascadePSdZRuntimeException
	 */
	private void doDokuTALTimesToAPDM( Vector<Ergebnis> ergListe, CascadeTAL TALResult ) throws CascadePSdZRuntimeException {

		Calendar startTime = Calendar.getInstance();
		String startTimeInTalTimeFormat;
		Calendar endTime = Calendar.getInstance();
		long diffTime; // Differenz, Starttime - Endtime
		String baseVariant;
		String hexAddress;
		Ergebnis Result;

		// gehe �ber jede TAL-Line
		for( int i = 0; i < TALResult.getTalLines().length; i++ ) {

			// falls doDokuTALTimesToAPDM aufgerufen wird, wenn die TAL-Abarbeitung noch nicht beendet ist, z.B. bei Abbruch durch Werker, muss beim Einlesen der TAL-Zeiten unterschieden werden zwischen TAL-Lines mit und TAL-Lines ohne Ausf�hrungszeiten
			if( (TALResult.getTalLines()[i].getStartTime() != null) && (TALResult.getTalLines()[i].getEndTime() != null) ) {
				// nur wenn eine Start- und Endzeit vorhanden ist werden Ergebnisse geschrieben
				startTime.setTime( TALResult.getTalLines()[i].getStartTime() );
				startTimeInTalTimeFormat = Integer.toString( startTime.get( Calendar.YEAR ) ) + String.format( "%02d", startTime.get( Calendar.MONTH ) + 1 ) // Z�hlung der Monate beginnt bei 0 // 
						+ String.format( "%02d", startTime.get( Calendar.DAY_OF_MONTH ) ) + "-" + String.format( "%02d", startTime.get( Calendar.HOUR_OF_DAY ) ) + String.format( "%02d", startTime.get( Calendar.MINUTE ) ) + String.format( "%02d", startTime.get( Calendar.SECOND ) ) + "." + String.format( "%03d", startTime.get( Calendar.MILLISECOND ) );
				endTime.setTime( TALResult.getTalLines()[i].getEndTime() );
				diffTime = endTime.getTimeInMillis() - startTime.getTimeInMillis(); // Differenzzeit
				baseVariant = TALResult.getTalLines()[i].getBaseVariantName();
				hexAddress = String.format( "%02x", TALResult.getTalLines()[i].getDiagnosticAddress() ).toUpperCase();

				Result = new Ergebnis( "PSdZExecuteTAL", "PSdZ", "", "", "", "ECU processing time analysis", "ECU: " + baseVariant + "; Hex Addr.: 0x" + hexAddress + "; Starttime: " + startTimeInTalTimeFormat + "; Deltatime: " + diffTime + "ms", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( Result );

			} else {
				// tue nichts
			}
		}
	}

}

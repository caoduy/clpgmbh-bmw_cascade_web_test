package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.*;

/** 
 * Implementierung der Pr�fprozedur, die beliebige Zeilen persistent in eine 
 * Datei im Filesystem schreibt.<BR>
 * <BR>
 * Created on 16.03.04<BR>
 * <BR>
 * Bei �nderung Hr. Buboltz TI-430 Tel. 35789 informieren!<BR>
 * <BR> 
 * @author 	BMW TI-430 Thomas Buboltz (TB)<BR>
 * 			Fa. GEFASOFT Peter Rettig (PR)<BR>
 * 			BMW TI-545 Fabian Sch�nert (FS)<BR>
 * @version V0_0_1  16.03.2004  TB  Implementierung<BR>
 * 			V0_0_2  07.04.2005  PR  Hinzugf�gen der optionalen Argumente zur 
 * 									Spezifizierung von Trennzeichen (DELIMITER)
 * 									und zur Angabe, ob an eine bestehende Datei
 * 									angehangen werden soll (APPEND).<BR>
 * 			V0_0_3  18.04.2005  PR  Hinzuf�gen eines optionalen Arguments,
 * 									welches bestimmt, ob nach dem Schreiben
 * 									aller Lines ein Wagenr�cklauf + 
 * 									Zeilenvorschub angehangen werden soll.<BR>
 * 			V4_0_0  26.08.2005  PR  Hinzuf�gen eines optionalen Arguments,
 * 									welches den Namen einer Pr�fstandsvariablen
 * 								    spezifiziert, die eine Liste von 
 * 									Pr�fstandsnamen enth�lt, bei denen die Datei 
 * 									tats�chlich geschrieben werden soll (bei 
 * 									allen anderen Pr�fst�nden erfolgt dann kein 
 * 									Schreiben).<BR>
 * 			V5_0_0  03.06.2008  CS  APDM ErgebnisFormat angepasst.<BR>
 * 			V_6_0T  19.05.2014  TB  Beim Dateinamen k�nnen Tokens f�r Computername und Datum enthalten sein, diese werden mit den Pr�fstanddaten ersetzt, Generics<BR>
 *                                  - das Token -COMPUTERNAME in Gro�buchstaben wird mit dem hostname des Rechner (Pr�fstand) ersetzt.<BR>
 *                                  - das Token -DATE in Gro�buchstaben wird mit dem Datum (YYYYMMDD) ersetzt.<BR>
 *          V_7_F   19.05.2014  TB  F Version.<BR>
 * 			V_8_0T  09.04.2015  FS  Zus�tzlicher optionaler Parameter Overwrite. Wenn gesetzt werden nur die spezifizierten Zeilen in der urspr�nflichen Datei �berschrieben,<BR>
 * 									die urspr�ngliche Datei bleibt bestehen.<BR>
 * 									Erweiterung der PP, sodass auch ein Write in einer beliebigen Zeile erfolgen kann.<BR>  
 * 			V_9_0F  08.05.2014 FS  F Version.<BR>            
 *   		V_10_0F 12.05.2016 FS  Optionaler ACCESS_TIMEOUT f�r Dateizugriff hinzugef�gt. Default: 10 Sekunden.<BR>  
 *     		V_11_T	25.05.2016	MK	Bugfix bez�glich des optionalen Parameters ACCESS_TIMEOUT.<BR>   
 *          V_12_T	30.05.2016	MK	Bugfix bez�glich des optionalen Parameters ACCESS_TIMEOUT.<BR>  
 *          V_13_T	07.06.2016	MK	Default Timeout auf 5 Sekunden angepasst.<BR>    
 *          V_14_F	07.06.2016	MK	Produktive Freigabe Version V_13_0_T.<BR>  
 *          V_15_T  19.08.2016  TB  Tokens f�r die XML Zeichen Verarbeitung bei den Textzeilen eingebaut. (-LT == <, -GT == >, ...) <BR>
 *          V_16_F  19.08.2016  TB  F Version <BR>
 *          V_17_F  19.08.2016  TB  @-Operator im String ersetzen <BR>
 *          V_18_F  19.08.2016  TB  F Version <BR>
 *          V_19_T  23.08.2016  KW  Tokens f�r Delimiter eingebaut <BR>
 *          V_20_F  24.08.2016  KW  F Version <BR>
 *          V_21_T  25.08.2016  KW  Bugfix bez�glich des optionalen Parameters DELIMITER.<BR> 
 *          V_22_F  26.08.2016  KW  F Version <BR>
 *          V_23_T  30.06.2017  KW  Bugfix bez�glich des optionalen Parameters OVERWRITE. <BR>
 *          V_24_F  24.07.2017  KW  F Version <BR>
 */

public class DateiWrite_24_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	static final long DEFAULT_TIMEOUT = 5;
	private int maxIndex;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DateiWrite_24_0_F_Pruefprozedur() {
	}

	/**
	   * erzeugt eine neue Pruefprozedur.
	   * @param pruefling Klasse des zugeh. Prueflings
	   * @param pruefprozName Name der Pruefprozedur
	   * @param hasToBeExecuted Ausfuehrbedingung f�r Fehlerfreiheit
	   */
	public DateiWrite_24_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit(); 
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "LINE[1..N]", "DELIMITER", "APPEND", "NEW_LINE_ON_CLOSE", "VAR_FOR_PS_NAMES", "OVERWRITE", "ACCESS_TIMEOUT" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "DIR", "FILENAME", "EXTENSION" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die parent-Methode aufgrund
	 * der offenen Anzahl an Results
	 */
	@Override
	public boolean checkArgs() {
		int i, j;
		boolean exist;
		maxIndex = 0;

		try {
			// 1. Check: Sind alle requiredArgs gesetzt
			String[] requiredArgs = getRequiredArgs();
			for( i = 0; i < requiredArgs.length; i++ ) {
				if( getArg( requiredArgs[i] ) == null )
					return false;
				if( getArg( requiredArgs[i] ).equals( "" ) == true )
					return false;
			}

			// 2. Check: Sind �berfl�ssige Argumente gesetzt, d.h. Argumente, die weder required noch optional sind
			// Ein gesetztes Argument, das mit LINE wird nicht analysiert, es wird aber der maximale Index festgehalten.
			// Die detaillierte Analyse erfolgt im 3-ten Check.
			@SuppressWarnings("unchecked")
			Enumeration<String> enu = getArgs().keys();
			String[] optionalArgs = getOptionalArgs();
			String givenkey;
			String temp;
			while( enu.hasMoreElements() ) {
				givenkey = enu.nextElement();
				if( (givenkey.startsWith( "DIR" ) == false) && (givenkey.startsWith( "FILENAME" ) == false) && (givenkey.startsWith( "EXTENSION" ) == false) && (givenkey.startsWith( "LINE" ) == false) && (givenkey.startsWith( "DELIMITER" ) == false) && (givenkey.startsWith( "APPEND" ) == false) && (givenkey.startsWith( "NEW_LINE_ON_CLOSE" ) == false) && (givenkey.startsWith( "VAR_FOR_PS_NAMES" ) == false) && (givenkey.startsWith( "OVERWRITE" ) == false) ) {
					exist = false;
					j = 0;
					while( (exist == false) && (j < requiredArgs.length) ) {
						if( givenkey.equals( requiredArgs[j] ) == true )
							exist = true;
						j++;
					}
					j = 0;
					while( (exist == false) && (j < optionalArgs.length) ) {
						if( givenkey.equals( optionalArgs[j] ) == true )
							exist = true;
						j++;
					}
					if( exist == false )
						return false;
					if( getArg( givenkey ).equals( "" ) == true )
						return false;
				} else {
					if( givenkey.startsWith( "LINE" ) == true ) {
						temp = givenkey.substring( 4 );
						try {
							j = Integer.parseInt( temp );
							if( j < 1 )
								return false;
							if( j > maxIndex )
								maxIndex = j;
						} catch( NumberFormatException e ) {
							e.printStackTrace();
							return false;
						}
					}
				}
			}

			// Tests bestanden, somit ok
			return true;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * fuehrt die Pruefprozedur aus
	 * @param info Information zur Ausfuehrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String filename, extension, dir = null;
		String[] lines = null;
		String delimiter = null; //Trennzeichen zum Separieren der Zeilen
		boolean append = false;  //Angabe, ob die Daten anzuh�ngen sind, falls die Datei bereits existiert
		boolean overwrite = false; //Angabe, ob existierende Zeilen �berschrieben werden sollen
		boolean newLineOnClose = false; //Angabe, ob ganz am Ende ein CR + LF angehangen werden soll
		boolean saveForThisPs = false; //Angabe, ob f�r diesen Pr�fstand das Speichern tats�chlich erfolgen soll
		long timeout = DEFAULT_TIMEOUT;

		try {
			//Parameter holen
			try {
				//Parameterpruefung
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				//Zwingenden singulaeren Parameter DIR holen
				if( getArg( getRequiredArgs()[0] ).indexOf( '@' ) != -1 )
					dir = getPPResult( getArg( getRequiredArgs()[0] ) ).trim();
				else
					dir = getArg( getRequiredArgs()[0] ).trim();

				//Zwingenden singulaeren Parameter NAME holen
				filename = extractValues( getArg( "FILENAME" ) )[0].trim();
				//ggf. den Hostname ersetzen
				if( filename.contains( "-COMPUTERNAME" ) ) {
					filename = filename.replaceAll( "-COMPUTERNAME", System.getenv( "COMPUTERNAME" ) );
				}
				//ggf. das Datum ersetzen
				if( filename.contains( "-DATE" ) ) {
					SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyyMMdd" );
					filename = filename.replaceAll( "-DATE", dateFormat.format( new Date() ) );
				}

				//Zwingenden singulaeren Parameter EXTENSION holen
				if( getArg( getRequiredArgs()[2] ).indexOf( '@' ) != -1 )
					extension = getPPResult( getArg( getRequiredArgs()[2] ) ).trim();
				else
					extension = getArg( getRequiredArgs()[2] ).trim();

				// Optionales Array LINE holen
				String temper = null;
				String line = "";
				lines = new String[maxIndex];
				for( int index = 0; index < maxIndex; ++index ) {
					if( getArg( "LINE" + (index + 1) ) != null )
					{
						if( (temper = replaceLineTokens( getArg( "LINE" + (index + 1) ) )) != null ) {
							line = line + ";" + replaceVariablesInString( temper );
						} 
					}else {
						line = line + ";";
					}
				}
				if( maxIndex > 0 ) {
					line = line.substring( 1 );
					lines = splitArg( line );
				}

				//Optionalen Parameter DELIMITER holen
				delimiter = getArg( "DELIMITER" );   
				
				// Wenn delimiter Sondertoken enth�lt, dann soll dieses ersetzt werden
				if( delimiter != null)
				    delimiter = replaceLineTokens(delimiter);
				
				if( delimiter != null && delimiter.indexOf( '@' ) != -1 ) //Parameter vorhanden und mit Verweis auf ein Ergebnis (ERGEBNIS@PR�FSCHRITT oder ERGEBNIS@PR�FLING.PR�FSCHRITT)?
					delimiter = getPPResult( delimiter ); //Ja. Dann Ergebnis-Verweis aufl�sen.
				//Optionalen Parameter APPEND holen
				String appendAsString;
				appendAsString = getArg( "APPEND" );
				if( appendAsString != null && appendAsString.indexOf( '@' ) != -1 ) //Parameter vorhanden und mit Verweis auf ein Ergebnis (ERGEBNIS@PR�FSCHRITT oder ERGEBNIS@PR�FLING.PR�FSCHRITT)?
					appendAsString = getPPResult( appendAsString ).trim(); //Ja. Dann Ergebnis-Verweis aufl�sen.
				append = "TRUE".equalsIgnoreCase( appendAsString ); //An bestehende Datei anh�ngen?                                
				//Optionalen Parameter NEW_LINE_ON_CLOSE holen
				String newLineOnCloseAsString;
				newLineOnCloseAsString = getArg( "NEW_LINE_ON_CLOSE" );
				if( newLineOnCloseAsString != null && newLineOnCloseAsString.indexOf( '@' ) != -1 ) //Parameter vorhanden und mit Verweis auf ein Ergebnis (ERGEBNIS@PR�FSCHRITT oder ERGEBNIS@PR�FLING.PR�FSCHRITT)?
					newLineOnCloseAsString = getPPResult( newLineOnCloseAsString ).trim(); //Ja. Dann Ergebnis-Verweis aufl�sen.
				newLineOnClose = "TRUE".equalsIgnoreCase( newLineOnCloseAsString ); //Ganz am Ende ein CR + LF anh�ngen?
				//Optionalen Parameter APPEND holen
				String overwriteAsString;
				overwriteAsString = getArg( "OVERWRITE" );
				if( overwriteAsString != null && overwriteAsString.indexOf( '@' ) != -1 ) //Parameter vorhanden und mit Verweis auf ein Ergebnis (ERGEBNIS@PR�FSCHRITT oder ERGEBNIS@PR�FLING.PR�FSCHRITT)?
					overwriteAsString = getPPResult( overwriteAsString ).trim(); //Ja. Dann Ergebnis-Verweis aufl�sen.
				overwrite = "TRUE".equalsIgnoreCase( overwriteAsString ); //bestehende Zeile �berschreiben?      
				//Optionalen Parameter VAR_FOR_PS_NAMES holen
				String varForPsNames;
				varForPsNames = getArg( "VAR_FOR_PS_NAMES" );
				if( varForPsNames != null ) { //Parameter vorhanden?
					//  Parameter mit Verweis auf ein Ergebnis (ERGEBNIS@-
					//  PR�FSCHRITT oder ERGEBNIS@PR�FLING.PR�FSCHRITT)?
					if( varForPsNames.indexOf( '@' ) != -1 )
						varForPsNames = getPPResult( varForPsNames ).trim(); //Ja. Dann Ergebnis-Verweis aufl�sen.

					//  Beziehe aus der im Parameter spezifizierten Pr�fstands-
					//  variablen die Liste derjenigen Pr�fst�nde, bei denen die
					//  Datei (tats�chlich) geschrieben werden soll.
					String psNamesString = (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, varForPsNames ); //beziehe Inhalt der Pr�fstandsvariablen
					String[] psNames = null; //Array, welches die Namen derjenigen Pr�fst�nde enth�lt, bei denen die Datei zu schreiben ist (bei den anderen wird nichts geschrieben)
					if( psNamesString != null )
						psNames = splitArg( psNamesString ); //separiere die Namen der Pr�fst�nde

					//  Stelle fest, ob das Speichern f�r den aktuellen Pr�f-
					//  stand erfolgen soll.
					String currentPsName = getPr�flingLaufzeitUmgebung().getName(); //Name des aktuellen Pr�fstandes
					if( psNames != null )
						for( int i = 0; i < psNames.length; i++ )
							if( currentPsName.equalsIgnoreCase( psNames[i] ) ) {
								saveForThisPs = true;
								break;
							}

				} else
					saveForThisPs = true; //Datei f�r diesen Pr�fstand speichern

				//Optionalen Parameter TIMEOUT holen
				if( getArg( "ACCESS_TIMEOUT" ) != null ) {
					timeout = Long.parseLong( getArg( "ACCESS_TIMEOUT" ).trim() );
				}

			} catch( Exception e ) {
				e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Speichern
			if( saveForThisPs ) // Soll auf diesem Pr�fstand das Speichern
									// durchgef�hrt werden?
				try {
					// Separator am Ende kuerzen
					if( dir.endsWith( "\\" ) || (dir.endsWith( "/" )) )
						dir = dir.substring( 0, dir.length() - 1 );
					if( dir.endsWith( "\\\\" ) )
						dir = dir.substring( 0, dir.length() - 2 );

					// Fileobjekte generieren
					File f = new File( dir + System.getProperty( "file.separator" ) + filename + "." + extension ); // Zu �ndernde (alte) Datei
					File f_temp = new File( dir + System.getProperty( "file.separator" ) + "~" + filename + "." + extension ); // tempor�re Datei

					// DIR eventuell erzeugen inklusive SubDIRs
					// Pr�fe angegebenes Verzeichnis in Executorthread um einen Timeout zu erm�glichen
					ExecutorService exSrv = Executors.newSingleThreadExecutor();
					ExecutorCompletionService completionService = new ExecutorCompletionService( exSrv );
					completionService.submit( new CreateDirectory( dir ) );
					Future<Boolean> createDirectorySuccess = completionService.poll( timeout, TimeUnit.SECONDS );
					// Fehler bei Zugriff auf das Verzeichnis
					if( createDirectorySuccess == null || createDirectorySuccess.get() != true ) {
						// Ergebnis Doc
						result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "ACCESS DIRECTORY EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						exSrv.shutdown();
						throw new PPExecutionException();
					}
					exSrv.shutdown();

					// Daten rausschreiben bei bereits erfolgreichen Zugriff auf das Verzeichnis
					if( !f.exists() || (f.exists() && f.canWrite()) ) {

						BufferedWriter out = null;
						if( delimiter == null ) // new line wenn kein Trennzeichen angegeben
							delimiter = String.format( "%n" );

						// Write
						if( overwrite && !append ) {
							// Existierende Zeilen werden Zeilenweise �berschrieben
							out = new BufferedWriter( new FileWriter( f_temp ) );
							Scanner scanner = new Scanner( f );
							int index;
							String line = "";
							for( index = 0; index < lines.length; index++ ) {
								// Hole Zeile aus bestehender Datei
								if( scanner.useDelimiter( delimiter ).hasNext() )
									line = scanner.useDelimiter( delimiter ).next();
								else
									line = "";
								// Schreibe Zeile in tempor�re Datei
								if( lines[index].equals( "" ) )
									out.write( line + delimiter ); // Kopiere Zeile aus bestehender Datei
								else
									out.write( lines[index] + delimiter ); // Schreibe neu hinzugef�gte Zeile
							}
							// Kopiere verbleibenden Rest der Datei
							if( scanner.hasNext() ) {
								scanner.skip( delimiter ); // �berspringe einen bereits geschriebenes Delimiter
								line = scanner.useDelimiter( "\\Z" ).next(); // \\Z f�r das Ende der Datei
								out.write( line );
							}
							scanner.close();
							if( newLineOnClose ) // Ganz am Ende der Datei eine neue Zeile einleiten?
								out.newLine();
							out.close();
							f.delete(); // L�sche tempor�re Datei nach Fertigstellung
							f_temp.renameTo( f ); // Nenne tempor�re Datei um nach altem Dateinamen

						} else {
							// Es wird der neue Inhalt an die Datei angehangen bzw. eine neue Datei erzeugt
							out = new BufferedWriter( new FileWriter( f, append ) );
							int index;
							for( index = 0; index < lines.length; index++ ) {
								out.write( lines[index] + delimiter );
							}
							if( newLineOnClose ) // Ganz am Ende der Datei eine neue Zeile einleiten?
								out.newLine();
							out.close();
						}

						// Ergebnis Doc
						result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE WRITTEN", "TRUE", "TRUE", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					} else {
						// Ergebnis Doc
						result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE WRITE EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					}

				} catch( CancellationException e ) {
					result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "CANCELLATION_EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} catch( InterruptedException e ) {
					result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "INTERRUPTED_EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} catch( ExecutionException e ) {
					// An execution exception of CheckFileExists can only be due to a security exception in file.exists()
					result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "SECURITY_EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} catch( IOException e ) {
					e.printStackTrace();
					result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE IO EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} catch( SecurityException e ) {
					e.printStackTrace();
					result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE WRITE SECURITY EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				}
			else { //Wenn f�r diesen Pr�fstand nicht gespeichert werden soll, dann trotzdem ein Ergebnis generieren.
				result = new Ergebnis( "Status", "System", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "", "", "FILE   N O T   WRITTEN", "TRUE", "TRUE", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		setPPStatus( info, status, ergListe );
	}

	/**
	 * Tokens in der Line wieder aufl�sen, z.B. XML
	 * @param str, input Sting with Tokens
	 * @return replaced String
	 */
	private String replaceLineTokens( String str ) {
		str = str.replaceAll( "-LT", "<" );
		str = str.replaceAll( "-GT", ">" );
		str = str.replaceAll( "-COMMA", "," );
		str = str.replaceAll( "-AMP", "&" );
		return str;
	}

	/**
	 * Ersetzt einen vorkommenden @-Operatoren in einem String durch dessen Werte
	 * Der Ausdruck muss dabei durch Leerzeichen abgetrennt sein
	 * @param   input - String mit zu ersetzenden Variablen
	 * @return	Ergbenis mit den aus dem @-Operator bezogenen Werten
	 */
	private String replaceVariablesInString( String input ) throws PPExecutionException {
		if( input != null ) {
			if( input.indexOf( '@' ) != -1 ) { // Ersetzen notwendig?
				String[] variablenArray = null;
				StringBuilder output = new StringBuilder();
				// Finde Indizes f�r @-Operator Ausdruck
				int atIndex = input.indexOf( '@' );
				int beginIndex = input.lastIndexOf( ' ', atIndex ) + 1;
				int endIndex = input.indexOf( ' ', atIndex );
				if( endIndex == -1 )
					endIndex = input.length();
				// Werte @-Operatore aus
				try {
					variablenArray = extractValues( input.substring( beginIndex, endIndex ) );
				} catch( PPExecutionException exception ) {
					throw exception; // leite exception weiter
				}
				// Baue neuen String mit ausgewertetem Ausdruck
				output.append( input.substring( 0, beginIndex ) );
				for( int index = 0; index < variablenArray.length; ++index ) {
					output.append( variablenArray[index] );
				}
				output.append( input.substring( endIndex, input.length() ) );
				return output.toString();
			}
		}
		return input;
	}

	/**
	 *  Die Klasse CreateDirectory erm�glicht ein threadbasiertes �berpr�fen/Erzeugen des Zielordners.
	 * 
	 * @author Sch�nert Fabian, TI-545 BMW AG
	 *
	 */
	class CreateDirectory implements Callable {

		private File pathFile;

		public CreateDirectory( String path ) {
			this.pathFile = new File( path );
		}

		@Override
		public Boolean call() {
			if( pathFile.exists() ) {
				return pathFile.canWrite();
			} else {
				return pathFile.mkdirs();
			}
		}
	}

}

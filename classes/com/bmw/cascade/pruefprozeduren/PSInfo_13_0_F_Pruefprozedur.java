/*
 * PSInfo_5_0_F_Pruefprozedur.java
 *
 * Created:          01.06.2004
 * Last modified:    02.03.2006
 *
 * Author:           Andreas Mair, BMW TI-432
 * Contact:          Tel.: +49/89/382-11992
 */

package com.bmw.cascade.pruefprozeduren;

import com.bmw.appframework.logging.LogLevel;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.util.logging.CascadeLogging;

import java.util.*; // Vector

/**
 * Testprocedure in the Cascade-System that gives access to test-stand-specific information.
 * @author Andreas Mair, BMW TI-432; Werner Braunstorer, BMW TI-430; Peter Rettig, GEFASOFT GmbH; C. Schumann, BMW TI-430; F. Sch�nert, BMW TI-545
 * @version 0_0_1_FA 03.06.2004 AM  Ersterstellung.<BR>
 * @version 0_0_2_FA 08.06.2004 AM  Erweiterung f�r mehere EVAL_PARAM's.<BR>
 * @version 0_0_3_FA 20.07.2004 AM  Debug bzgl. Ergebnisablage bei mehrfachem Aufruf.<BR>
 *                              SP  WerkerFrage, wenn IS_ON_LIST n.I.O. und QUESTION ausgef�llt. Wenn 
 *                                  WerkerFrage mit Ja best�tigt wird, dann gehts weiter<BR>.
 * @version 0_0_4_FA 02.03.2006 WB  Bei Vergleich von zwei Parametern wird nicht mehr zwischen Gross- Kleinschreibung unterschieden.<BR>
 * @version 5_0_F    19.04.2006 PR  Aufnahme des zus�tzlichen Wertes VAR_VALUE f�r das Argument INFO_NAME zum Abfragen von Pr�fstandsvariablen.<BR>
 * @version 10_0_T   12.06.2008 CS  deprecated CascadeLogManager abgel�st durch CascadeLogging, APDM Ergebnis angepasst.<BR>
 * @version 11_0_F   06.10.2008 CS  Freigabe 10_0_T.<BR>
 * @version 12_0_T   23.11.2015 FS  Es ist jetzt auch m�glich PS Variablen der Typen Temporary und Persistent zu beziehen.<BR> 
 * @version 13_0_F   23.11.2015 FS  Freigabe 12_0_T.<BR>
 */
public class PSInfo_13_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSInfo_13_0_F_Pruefprozedur() {
	}

	// Needed for every test-procedure:
	//---------------------------------
	private Vector i_oErgListe = null; // result vector
	private int i_iStatus = STATUS_UNDEFINED; // execution status

	// Actual Cascade-result
	private Ergebnis i_oResult;
	// Strings for the result objects in execute
	private String i_sID = ""; // ID
	private String i_sWerkzeug = ""; // werkzeug
	private String i_sParameter_1 = ""; // parameter_1
	private String i_sParameter_2 = ""; // parameter_2
	private String i_sParameter_3 = ""; // parameter_3
	private String i_sErgebnis = ""; // ergebnis
	private String i_sErgebniswert = ""; // ergebniswert
	private String i_sMinWert = ""; // minWert
	private String i_sMaxWert = ""; // maxWert
	private String i_sWiederholungen = ""; // wiederholungen
	private String i_sFrageText = ""; // frageText
	private String i_sAntwortText = ""; // antwortText
	private String i_sAnweisText = ""; // anweisText
	private String i_sFehlerText = ""; // fehlertext
	private String i_sHinweisText = ""; // hinweistext
	private String i_sFehlertyp = ""; // fehlertyp

	// Debug flags
	private boolean i_bPSInfo_Standard = false;

	/**
	 * Creates a new instance of PSInfo.
	 * @param pruefling "Test-item"-class containing this concrete test-procedure
	 * @param pruefprozName name of the test-procedure
	 * @param hasToBeExecuted logical condition if the test-procedure should be executed
	 */
	public PSInfo_13_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
	}

	/**
	 * Initializes the arguments.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Gets the names of the mandatory arguments.
	 * @return string-array containing the required arguments
	 */
	public String[] getRequiredArgs() {
		String[] args = { "INFO_NAME" };
		return args;
	}

	/**
	 * Gets the names of the optional arguments.
	 * @return string-array containing the optional arguments
	 */
	public String[] getOptionalArgs() {
		String[] args = { "EVALUATION", "EVAL_PARAM", "RES_ID", "DEBUG", "QUESTION" };
		return args;
	}

	/**
	 * Checks if
	 * - all mandatory arguments are present
	 * - no other arguments than the mandatory and optional arguments are used
	 * @return true if the check was successful.
	 */
	public boolean checkArgs() {
		// Debug print of all arguments
		if( i_bPSInfo_Standard ) {
			CascadeLogging.getLogger().log( LogLevel.INFO, "Start checkArgs() from PSInfo." );
			Enumeration enu = getArgs().keys();
			String givenkey, strActualArg;
			CascadeLogging.getLogger().log( LogLevel.INFO, "All Arguments from this PP: " + this.getName() );
			while( enu.hasMoreElements() ) {
				// The actual argument
				givenkey = (String) enu.nextElement();
				strActualArg = getArg( givenkey );
				CascadeLogging.getLogger().log( LogLevel.INFO, givenkey + " = " + strActualArg );
			}
		}
		try {
			return super.checkArgs();
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * Executes the test-procedure
	 * @param info Information about the execution.
	 */
	public void execute( ExecutionInfo info ) {
		// Resets the relevant instance variables
		i_oErgListe = new Vector(); // create a new Vector
		i_iStatus = STATUS_UNDEFINED;
		i_oResult = null;
		resetResultStrings();

		// Object variables
		String sInfoName = null;
		String sInfoValue = null;
		String sResId = null;

		// Sets the default result strings
		i_sID = "TEST STAND INFO";
		i_sWerkzeug = "TEST STAND INFO";
		i_sErgebnis = "NO RESULT";
		i_sErgebniswert = "NIO";

		/***********************************************
		 * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
		 * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		 ***********************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		/* Sets debug flags as a result from the optional debug argument in the PP or as a result from
		 * the debug attribute in the Pr�fling. */
		String debugString = null;
		if( getArg( "DEBUG" ) != null ) {
			debugString = getArg( "DEBUG" ); // debug argument in PP (preference)
		} else if( getPr�fling().getAttribut( "DEBUG" ) != null ) {
			debugString = getPr�fling().getAttribut( "DEBUG" ); // debug attribute in P
		}
		if( debugString != null ) {
			String[] debugFlags = splitArg( debugString );
			int n = debugFlags.length;
			if( n >= 1 ) {
				for( int i = 0; i < n; i++ ) {
					if( debugFlags[i].equalsIgnoreCase( "PS_INFO" ) ) {
						i_bPSInfo_Standard = true;
					} else if( debugFlags[i].equalsIgnoreCase( "ALL" ) ) {
						i_bPSInfo_Standard = true;
					}
				}
			}
		}

		// Actual execution
		try {
			if( !checkArgs() ) {
				if( i_bPSInfo_Standard ) {
					CascadeLogging.getLogger().log( LogLevel.WARNING, "Argument error on using checkArgs() in execute()" );
				}
				resetResultStrings();
				i_sID = "ARGUMENT_ERROR";
				i_sWerkzeug = "CHECK PARAMETERS IN EXECUTE";
				i_sFehlerText = PB.getString( "parametrierfehler" );
				i_sHinweisText = "CHECK ARGS IN PS_INFO NOT SUCCESSFUL!";
				i_sFehlertyp = Ergebnis.FT_NIO_SYS;
				// Pass a new exception upwards (PPExecutionException -> error in parameters!)
				throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			} else {
				if( i_bPSInfo_Standard ) {
					CascadeLogging.getLogger().log( LogLevel.INFO, "Arguments ok on using checkArgs() in execute()" );
				}
			}

			// Mandatory argument
			sInfoName = getArg( "INFO_NAME" );

			// Reads the optional arguments
			String sInfoEvaluation = null;
			if( getArg( "EVALUATION" ) != null ) {
				sInfoEvaluation = getArg( "EVALUATION" );
			}
			String sEvalParam = null;
			String[] asEvalParam = null;
			if( getArg( "EVAL_PARAM" ) != null ) {
				sEvalParam = getArg( "EVAL_PARAM" );
				asEvalParam = extractValues( sEvalParam );
			}
			if( getArg( "RES_ID" ) != null ) {
				sResId = getArg( "RES_ID" );
			}

			// Further argument check
			boolean bFurtherCheck = true;
			if( sInfoEvaluation != null ) {
				if( !(asEvalParam != null && validEvaluation( sInfoEvaluation )) ) {
					bFurtherCheck = false;
					if( i_bPSInfo_Standard ) {
						CascadeLogging.getLogger().log( LogLevel.INFO, "PSInfo:further args check: false" );
					}
				}
			}
			if( !bFurtherCheck ) {
				resetResultStrings();
				i_sID = "ARGUMENT_ERROR";
				i_sWerkzeug = "CHECK PARAMETERS IN EXECUTE";
				i_sFehlerText = PB.getString( "parametrierfehler" );
				i_sHinweisText = "ERROR ON FURTHER ARGS CHECK IN PS_INFO!";
				i_sFehlertyp = Ergebnis.FT_NIO_SYS;
				// Pass a new exception upwards (PPExecutionException -> error in parameters!)
				throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			}

			// Evaluation
			sInfoValue = getPSInfo( sInfoName );
			i_sErgebnis = sInfoName;
			i_sErgebniswert = sInfoValue;

			if( sInfoValue == null ) { //Kein zu testender Wert gefunden?

				i_sFehlerText = "DESIRED TEST STAND INFORMATION DOES NOT EXIST OR IS EMPTY";
				i_sHinweisText = "CHECK IF THIS IS PLAUSIBLE";
				i_sFehlertyp = Ergebnis.FT_NIO;
				i_iStatus = STATUS_EXECUTION_ERROR;
				if( i_bPSInfo_Standard )
					CascadeLogging.getLogger().log( LogLevel.INFO, "PSInfo:desired test stand information (" + sInfoName + ") does not exist or is empty ->NIO!" );

			} else if( sInfoEvaluation != null ) { //Wurde der Evaluierungstyp festgelegt?

				i_sParameter_3 = i_sParameter_3 + "WITH EVALUATION ";

				// Check, if on list
				if( sInfoEvaluation.equalsIgnoreCase( "IS_ON_LIST" ) ) {
					i_sMinWert = sEvalParam;
					int iListLen = asEvalParam.length;
					boolean bOnList = false;
					for( int l = 0; l < iListLen; l++ ) {
						if( sInfoValue.equalsIgnoreCase( asEvalParam[l] ) ) {
							bOnList = true;
						}
					}
					if( bOnList ) {
						i_sParameter_3 = i_sParameter_3 + "ON_LIST ";
						i_sFehlertyp = Ergebnis.FT_IO;
						i_iStatus = STATUS_EXECUTION_OK;
						if( i_bPSInfo_Standard ) {
							CascadeLogging.getLogger().log( LogLevel.INFO, "PSInfo:evaluation:" + sInfoValue + " is on list: " + sEvalParam + " ->IO!" );
						}
					} else {
						// If QUESTION exists
						if( getArg( getOptionalArgs()[4] ) != null ) {
							int iAnswer = 0;
							iAnswer = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputDigital( "Fortfahren/Continue ?", getArg( getOptionalArgs()[4] ), 0 );
							if( iAnswer == UserDialog.YES_KEY ) {
								i_iStatus = STATUS_EXECUTION_OK;
								i_sWerkzeug = "UserInput";
								i_sErgebnis = "Continue";
								i_sErgebniswert = "OK";
								i_sFrageText = getArg( getOptionalArgs()[4] );
								i_sAntwortText = "YES";
								i_sFehlertyp = Ergebnis.FT_IO;
							}
							//Answer is NO
							else {
								i_iStatus = STATUS_EXECUTION_ERROR;
								i_sWerkzeug = "UserInput";
								i_sErgebnis = "Continue";
								i_sErgebniswert = "NOK";
								i_sFrageText = getArg( getOptionalArgs()[4] );
								i_sAntwortText = "NO";
								i_sFehlertyp = Ergebnis.FT_NIO;
							}
							getPr�flingLaufzeitUmgebung().releaseUserDialog();
						} else {
							i_sFehlerText = "PS-INFO NOT ON LIST";
							i_sHinweisText = "CHECK IF THIS IS PLAUSIBLE";
							i_sFehlertyp = Ergebnis.FT_NIO;
							i_iStatus = STATUS_EXECUTION_ERROR;
							if( i_bPSInfo_Standard ) {
								CascadeLogging.getLogger().log( LogLevel.INFO, "PSInfo:evaluation:" + sInfoValue + " is not on list: " + sEvalParam + " ->NIO!" );
							}
						}
					}
				}
				// Check, if not on list
				else if( sInfoEvaluation.equalsIgnoreCase( "IS_NOT_ON_LIST" ) ) {
					i_sMinWert = sEvalParam;
					int iListLen = asEvalParam.length;
					boolean bOnList = false;
					for( int l = 0; l < iListLen; l++ ) {
						if( sInfoValue.equalsIgnoreCase( asEvalParam[l] ) ) {
							bOnList = true;
						}
					}
					if( !bOnList ) {
						i_sParameter_3 = i_sParameter_3 + "NOT_ON_LIST ";
						i_sFehlertyp = Ergebnis.FT_IO;
						i_iStatus = STATUS_EXECUTION_OK;
						if( i_bPSInfo_Standard ) {
							CascadeLogging.getLogger().log( LogLevel.INFO, "PSInfo:evaluation:" + sInfoValue + " is not on list: " + sEvalParam + " ->IO!" );
						}
					} else {
						i_sFehlerText = "PS-INFO ON LIST";
						i_sHinweisText = "CHECK IF THIS IS PLAUSIBLE";
						i_sFehlertyp = Ergebnis.FT_NIO;
						i_iStatus = STATUS_EXECUTION_ERROR;
						if( i_bPSInfo_Standard ) {
							CascadeLogging.getLogger().log( LogLevel.INFO, "PSInfo:evaluation:" + sInfoValue + " is on list: " + sEvalParam + " ->NIO!" );
						}
					}
				}

			} else { //kein Evaluierungstyp festgelegt
				i_sParameter_3 = i_sParameter_3 + "NO EVALUATION ";
				i_sFehlertyp = Ergebnis.FT_IO;
				i_iStatus = STATUS_EXECUTION_OK;
			}

			// Regular result
			if( sResId != null ) {
				i_sID = sResId;
			} else {
				i_sID = "PS_INFO";
			}
			i_sWiederholungen = "1";
			addResultFromInstanceVariables();

		} // End: Try of execute
		catch( PPExecutionException ppee ) { // Error in arguments!
			i_iStatus = STATUS_EXECUTION_ERROR;
			ppee.printStackTrace();
			addResultFromInstanceVariables();
		} catch( Exception ex ) {
			i_iStatus = STATUS_EXECUTION_ERROR;
			ex.printStackTrace();
			resetResultStrings();
			i_sID = "EXCEPTION";
			i_sFehlerText = "EXCEPTION";
			i_sHinweisText = PB.getString( "unerwarteterLaufzeitfehler" );
			i_sFehlertyp = Ergebnis.FT_NIO_SYS;
			addResultFromInstanceVariables();

		} catch( Throwable t ) {
			i_iStatus = STATUS_EXECUTION_ERROR;
			t.printStackTrace();
			resetResultStrings();
			i_sID = "THROWABLE";
			i_sFehlerText = "THROWABLE";
			i_sHinweisText = PB.getString( "unerwarteterLaufzeitfehler" );
			i_sFehlertyp = Ergebnis.FT_NIO_SYS;
			addResultFromInstanceVariables();
		}

		//i_iStatus setzen
		setPPStatus( info, i_iStatus, i_oErgListe );
	} // End execute

	/**
	 * Resets the result strings
	 */
	private void resetResultStrings() {
		i_sID = ""; // ID
		i_sWerkzeug = ""; // werkzeug
		i_sParameter_1 = ""; // parameter_1
		i_sParameter_2 = ""; // parameter_2
		i_sParameter_3 = ""; // parameter_3
		i_sErgebnis = ""; // ergebnis
		i_sErgebniswert = ""; // ergebniswert
		i_sMinWert = ""; // minWert
		i_sMaxWert = ""; // maxWert
		i_sWiederholungen = ""; // wiederholungen
		i_sFrageText = ""; // frageText
		i_sAntwortText = ""; // antwortText
		i_sAnweisText = ""; // anweisText
		i_sFehlerText = ""; // fehlertext
		i_sHinweisText = ""; // hinweistext
		i_sFehlertyp = ""; // fehlertyp
	}

	private void addResultFromInstanceVariables() {
		Ergebnis result = new Ergebnis( i_sID, // ID
		i_sWerkzeug, // werkzeug
		i_sParameter_1, // parameter_1
		i_sParameter_2, // parameter_2
		i_sParameter_3, // parameter_3
		i_sErgebnis, // ergebnis
		i_sErgebniswert, // ergebniswert
		i_sMinWert, // minWert
		i_sMaxWert, // maxWert
		i_sWiederholungen, // wiederholungen
		i_sFrageText, // frageText
		i_sAntwortText, // antwortText
		i_sAnweisText, // anweisText
		i_sFehlerText, // fehlertext
		i_sHinweisText, // hinweistext
		i_sFehlertyp // fehlertyp
		);
		i_oErgListe.add( result );
	}

	/**
	 * Checks a string for a valid evaluation
	 */
	private boolean validEvaluation( String e ) {
		boolean check = false;
		if( e.equals( "IS_ON_LIST" ) || e.equals( "IS_NOT_ON_LIST" ) )
			check = true;
		if( i_bPSInfo_Standard ) {
			CascadeLogging.getLogger().log( LogLevel.INFO, "PSInfo:validEvaluation:evaluation " + e + " valid: " + check );
		}
		return check;
	}

	/**
	 * Gets the value for a requested test-stand-information.
	 */
	private String getPSInfo( String infoName ) {
		final String TS_NAME_INFO = "TS_NAME"; //INFO-Typ: Pr�fstandname
		final String VAR_VALUE_INFO = "VAR_VALUE:"; //INFO-Typ: Wert einer Pr�fstandsvariablen    	
		String infoVal = null;

		if( infoName.equals( TS_NAME_INFO ) ) { //Pr�fstandsnamen beziehen?
			infoVal = getPr�flingLaufzeitUmgebung().getName();
			infoVal = infoVal.substring( 10 );

		} else if( infoName.startsWith( VAR_VALUE_INFO ) ) { //Wert einer Pr�fstandsvariablen beziehen?        	
			if( infoName.length() > VAR_VALUE_INFO.length() ) { //Wurde �berhaupt der Name einer Pr�fstandsvariablen angegeben?        		
				String varName = infoName.substring( VAR_VALUE_INFO.length() ); //ermittle den Namen der Pr�fstandsvariablen
				//beziehe den Inhalt der Pr�fstandsvariablen der Typen TEMPORARY,PERSISTENT und PS_CONFIG
				Object obj = null;
				try {
					obj = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, varName );
				} catch( Exception x ) {
					// Nichts
				}
				try {
					obj = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, varName );
				} catch( Exception x ) {
					// Nichts
				}
				try {
					obj = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PERSISTENT, varName );
				} catch( Exception x ) {
					// Nichts
				}
				if( obj != null )
					infoVal = obj.toString(); //universell f�r alle Typen von Pr�fstandsvariablen (STRING, INTEGER etc.) vorgehen
				// sonst bleibt infoVal null
			}
		} else
			return "NO TS-INFO AVAILABLE";

		return infoVal;
	}
}

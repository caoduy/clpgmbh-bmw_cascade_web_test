package com.bmw.cascade.pruefprozeduren;

import java.util.LinkedList;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;

/**
 * Implementierung einer Wartezeit zur Sitzinitialisierung (parallel f�r alle Sitzmodule)
 * Die Sgbden werden den Pr�flingen entnommen, sofern diese im Auftrag vorhanden sind. Diagnose-
 * fehler werden nur einmal, nicht f�r jeden Fall ins Log eingetragen.
 * Positive Ergebnisse werden auch nur einmal ins Log eingetragen.
 * 
 * @author Burger, Andreas
 * 
 * @version 1_0_F  AB Neue Version wegen Inkompatibilit�t <br>
 * @version 2_0_F  AB Major Bugfixes <br>
 * @version 3_0_F  AB Test im Werk <br>
 * @version 4_0_F  AB Iterator durch LL-Zugriffe ersetzt, Fehlerbehandlung korrigiert <br>
 * @version 5_0_F  AB SGBD-Aufl�sung korrigiert <br> 
 */
public class SDiagSitzComplex_5_0_F_Pruefprozedur extends AbstractBMWPruefprozedur 
{

	static final long serialVersionUID = 1L;

	// language
	private final boolean isEnglish = System.getProperty( "user.language" ).equalsIgnoreCase( "EN" );

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public SDiagSitzComplex_5_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public SDiagSitzComplex_5_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName,
			Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialisiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() 
	{
		String[] args = {	"START_TRIES",
							"START_DELAY"};
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() 
	{
		String[] args = { 	"REF_PL_NAME_FAS", 
							"REF_PL_NAME_BFS", 
							"REF_PL_NAME_FAH", 
							"REF_PL_NAME_BFH",
							"JOB",
							"JOB_POLLPAR", 
							"JOB_POLLRESULT",
							"JOB_POLLVALUE",
							"JOB_STARTPAR", 
							"JOB_STARTRESULT",
							"JOB_STARTVALUE",							
							"TIMEOUT",
							"PAUSE"	};
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	public boolean checkArgs() 
	{
		return super.checkArgs();
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) 
	{
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		EdiabasProxyThread c_Ediabas = null;
		boolean b_EdiabasParallel = false;

		//spezifische Variablen
		String s_Job = "";
		String s_PollPar = "";
		String s_StartPar = "";
		String s_PollResult = "";
		String s_StartResult = "";
		String s_PollValue = "";
		String s_StartValue = "";
		int i_Pause;
		int i_Timeout;
		int i_StartTries;
		int i_StartDelay;
		String s_Temp = null;
		LinkedList c_OpenEcus = new LinkedList();
		LinkedList c_RunningEcus = new LinkedList();

		if( checkArgs() == false ) 
		{
			if (this.isEnglish)
				result = new Ergebnis( this.name, "Wrong parameters", "", "", "", "", "", "", "", "0", "", "", "", "Wrong parameters", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( this.name, "Parametrierfehler", "", "", "", "", "", "", "", "0", "", "", "", "Parametrierfehler", "", Ergebnis.FT_NIO_SYS );

			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;			
			setPPStatus( info, status, ergListe );
			return;
		}
			
		try 
		{
			s_Job = getArg ("JOB");
			s_PollPar = getArg ("JOB_POLLPAR");
			s_StartPar = getArg ("JOB_STARTPAR");
			s_PollResult = getArg ("JOB_POLLRESULT");
			s_StartResult = getArg ("JOB_STARTRESULT");
			s_PollValue = getArg ("JOB_POLLVALUE");
			s_StartValue = getArg ("JOB_STARTVALUE");
			i_Pause = Integer.parseInt(getArg("PAUSE"));
			i_Timeout = Integer.parseInt(getArg("TIMEOUT"));

			String temp = getArg ("START_TRIES"); 
			if ((temp != null) && (temp.length() > 0))
				i_StartTries = Integer.parseInt(temp);
			else
				i_StartTries = 1;
			
			temp = null;
			temp = getArg ("START_DELAY"); 
			if ((temp != null) && (temp.length() > 0))
				i_StartDelay = Integer.parseInt(temp);
			else
				i_StartDelay = 500;	
		} 
		catch (Exception e) 
		{
			if(e.getMessage() != null)
			{
				if (this.isEnglish)
					result = new Ergebnis( this.name, "Wrong parameters", "", "", "", "", "", "", "", "0", "", "", "", "Wrong parameters", e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( this.name, "Parametrierfehler", "", "", "", "", "", "", "", "0", "", "", "", "Parametrierfehler", e.getMessage(), Ergebnis.FT_NIO_SYS );
			}
			else
			{
				if (this.isEnglish)
					result = new Ergebnis( this.name, "Wrong parameters", "", "", "", "", "", "", "", "0", "", "", "", "Wrong parameters", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( this.name, "Parametrierfehler", "", "", "", "", "", "", "", "0", "", "", "", "Parametrierfehler", "", Ergebnis.FT_NIO_SYS );
			}

			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			setPPStatus( info, status, ergListe );
			return;
		}
		
		try 
		{
			// welche sgbdn m�ssen gepr�ft werden?
			//Ist ein Pr�fling vorhanden --> Hole SGBD aus Pr�fling
			if (getPr�fling().getAuftrag().getPr�fling(getArg("REF_PL_NAME_FAS")) != null)
			{
				s_Temp = getPr�fling().getAuftrag().getPr�fling(getArg("REF_PL_NAME_FAS")).getAttribut("SGBD");
				if ((s_Temp != null) && (s_Temp.trim().length() > 0)) 
					c_OpenEcus.add(s_Temp);
			}			
			//Wenn kein Pr�fling vorhanden, dann setze Name == SGBD
			/*
			else if (getArg("REF_PL_NAME_FAS") != null)
			{
				s_Temp = getArg("REF_PL_NAME_FAS");
				s_Temp = s_Temp.trim().toUpperCase();
				if ((s_Temp != null) && (s_Temp.length() > 0) && !s_Temp.equalsIgnoreCase("NULL")) 
					c_OpenEcus.add(s_Temp);				
			}
			*/
				
		} 
		catch (Exception e) 
		{
		}		

		try 
		{
			//Ist ein Pr�fling vorhanden --> Hole SGBD aus Pr�fling			
			if (getPr�fling().getAuftrag().getPr�fling(getArg("REF_PL_NAME_BFS")) != null)
			{
				s_Temp = getPr�fling().getAuftrag().getPr�fling(getArg("REF_PL_NAME_BFS")).getAttribut("SGBD");
				if ((s_Temp != null) && (s_Temp.trim().length() > 0)) 
					c_OpenEcus.add(s_Temp);
			}
			//Wenn kein Pr�fling vorhanden, dann setze Name == SGBD
			/*
			else if (getArg("REF_PL_NAME_BFS") != null)
			{
				s_Temp = getArg("REF_PL_NAME_BFS");
				s_Temp = s_Temp.trim().toUpperCase();
				if ((s_Temp != null) && (s_Temp.length() > 0) && !s_Temp.equalsIgnoreCase("NULL")) 
					c_OpenEcus.add(s_Temp);								
			}
			*/			
		} 
		catch (Exception e) 
		{
		}

		try 
		{	
			//Ist ein Pr�fling vorhanden --> Hole SGBD aus Pr�fling			
			if (getPr�fling().getAuftrag().getPr�fling(getArg("REF_PL_NAME_FAH")) != null)
			{
				s_Temp = getPr�fling().getAuftrag().getPr�fling(getArg("REF_PL_NAME_FAH")).getAttribut("SGBD");
				if ((s_Temp != null) && (s_Temp.trim().length() > 0)) 
					c_OpenEcus.add(s_Temp);
			}
			//Wenn kein Pr�fling vorhanden, dann setze Name == SGBD
			/*
			else if (getArg("REF_PL_NAME_FAH") != null)
			{
				s_Temp = getArg("REF_PL_NAME_FAH");
				s_Temp = s_Temp.trim().toUpperCase();
				if ((s_Temp != null) && (s_Temp.length() > 0) && !s_Temp.equalsIgnoreCase("NULL")) 
					c_OpenEcus.add(s_Temp);								
			}
			*/			
		} 
		catch (Exception e) 
		{
		}
		
		try 
		{
			//Ist ein Pr�fling vorhanden --> Hole SGBD aus Pr�fling			
			if (getPr�fling().getAuftrag().getPr�fling(getArg("REF_PL_NAME_BFH")) != null)
			{
				s_Temp = getPr�fling().getAuftrag().getPr�fling(getArg("REF_PL_NAME_BFH")).getAttribut("SGBD");
				if ((s_Temp != null) && (s_Temp.trim().length() > 0)) 
					c_OpenEcus.add(s_Temp);	
			}
			//Wenn kein Pr�fling vorhanden, dann setze Name == SGBD
			/*
			else if (getArg("REF_PL_NAME_BFH") != null)
			{
				s_Temp = getArg("REF_PL_NAME_BFH");
				s_Temp = s_Temp.trim().toUpperCase();
				if ((s_Temp != null) && (s_Temp.length() > 0) && !s_Temp.equalsIgnoreCase("NULL")) 
					c_OpenEcus.add(s_Temp);								
			}	
			*/		
		} 
		catch (Exception e) 
		{
		}		
				
		SDiagResult resStart = new SDiagResult();
		SDiagResult resPoll = new SDiagResult();
		//Launch Init-sequences
		if ((c_OpenEcus != null) && (c_OpenEcus.size() > 0) && (i_StartTries > 0))
			resStart = startEcus (b_EdiabasParallel, c_Ediabas, c_OpenEcus, c_RunningEcus, i_StartTries, i_StartDelay, s_Job, s_StartPar, s_StartResult, s_StartValue, s_PollPar, s_PollResult, s_PollValue, ergListe);
		
		long l_StopTime = System.currentTimeMillis() + i_Timeout;
		while ((c_RunningEcus != null) && (c_RunningEcus.size() > 0) && (l_StopTime > System.currentTimeMillis()))
		{
			resPoll = pollEcus (b_EdiabasParallel, c_Ediabas, c_RunningEcus, s_Job, s_PollPar, s_PollResult, s_PollValue, ergListe);
			wait (i_Pause);
		}
		
		//Report all not-finished-ecus
		while (c_RunningEcus.size() > 0)
		{			
			String s_Sgbd = (String) c_RunningEcus.getFirst();

			if (isEnglish)
				result = new Ergebnis( this.name, "Diagnose", s_Sgbd, "", "", "", "", "", "", "0", "", "", "", "Time too long", "Move seat to home position manually", Ergebnis.FT_NIO );
			else
				result = new Ergebnis( this.name, "Diagnose", s_Sgbd, "", "", "", "", "", "", "0", "", "", "", "Wartezeit zu lange", "Fahre Sitz manuell in Grundstellung", Ergebnis.FT_NIO );
			
			c_RunningEcus.remove(s_Sgbd);
			
			ergListe.add(result);
			status = STATUS_EXECUTION_ERROR;	
		}
		
		if ((resStart.status == STATUS_EXECUTION_ERROR) || (resPoll.status == STATUS_EXECUTION_ERROR))
			status = STATUS_EXECUTION_ERROR;

		if ((resStart.success != true) || (resPoll.success != true))
			status = STATUS_EXECUTION_ERROR;
		
		//Temporary Result-output
		/*
		for (int i = 0; i < ergListe.size(); i++)
			System.out.println (ergListe.get(i));
		*/
		
		setPPStatus( info, status, ergListe );
	} // End of execute()
	
	/**
	 * Poll all open ECUs one time
	 * 
	 * @param isEdiabasParallel
	 * @param ediabas
	 * @param openEcus
	 * @param job
	 * @param askArg
	 * @param askResult
	 * @param askValue
	 * @param ergListe
	 * @return
	 */
	private SDiagResult pollEcus (boolean isEdiabasParallel, EdiabasProxyThread ediabas, LinkedList ecusToPoll, String job, String askArg, String askResult, String askValue, Vector ergListe)
	{	
		boolean noError = true;
		boolean resTemp = false;
		SDiagResult result = new SDiagResult();		
		LinkedList openEcus = new LinkedList();
		
		//Copy ECU's to local list, if value doesn't matter --> dummy call
		if ((askResult == null) || (askValue == null))
		{
			for (int i = 0; i < ecusToPoll.size(); i++)
				openEcus.add(ecusToPoll.get(i));
		}
		else
			openEcus = ecusToPoll;
		
		//Do one poll-cycle over all available ECUs
		for (int i = 0; i < ecusToPoll.size(); i++)
		{
			String sgbd = (String) ecusToPoll.get(i);			
			
			result = executeJob(isEdiabasParallel, true, ediabas, sgbd, job, askArg, askResult, askValue, ergListe);	
			ediabas = result.ebas;
			resTemp = result.success;
			if (resTemp)
			{
				//Report OK, if value given and reached
				if ((askResult != null) && (askValue != null) && result.remove)
				{
					if (this.isEnglish)
						result.result = new Ergebnis( this.name, "Diagnose", sgbd, "", "", "", "", "", "", "0", "", "", "", "Wait sequence IO", "", Ergebnis.FT_IO );
					else
						result.result = new Ergebnis( this.name, "Diagnose", sgbd, "", "", "", "", "", "", "0", "", "", "", "Wartezeit IO", "", Ergebnis.FT_IO );

					openEcus.remove(sgbd);
					ergListe.add(result.result);
				}				
			}
			else
			{
				//Report NIO, if there was a communication error
				if (this.isEnglish)
					result.result = new Ergebnis( this.name, "Diagnose", sgbd, "", "", "", "", "", "", "0", "", "", "", "Communication failure", "ECU is not responding", Ergebnis.FT_NIO_SYS );
				else
					result.result = new Ergebnis( this.name, "Diagnose", sgbd, "", "", "", "", "", "", "0", "", "", "", "Kommunikationsfehler", "Steuerger�t antwortet nicht", Ergebnis.FT_NIO_SYS );
				noError = false;
				
				openEcus.remove(sgbd);
				ergListe.add(result.result);
			}
			
			//Remove entry from list, if value reached
			if (result.remove)
				openEcus.remove(sgbd);
		}		
		
		if (!noError)
		{
			result.success = false;
			result.status = STATUS_EXECUTION_ERROR;
		}
		else
		{
			result.success = true;
			result.status = STATUS_EXECUTION_OK;			
		}	
		
		return result;
	}

	/**
	 * Starts all given ECUs with polling and a delayed start-time
	 * 
	 * @param isEdiabasParallel
	 * @param ediabas
	 * @param startEcus
	 * @param openEcus
	 * @param startTries
	 * @param startDelay
	 * @param job
	 * @param startArg
	 * @param startResult
	 * @param startValue
	 * @param pollArg
	 * @param pollResult
	 * @param pollValue
	 * @param ergListe
	 * @return
	 */
	private SDiagResult startEcus (boolean isEdiabasParallel, EdiabasProxyThread ediabas, LinkedList startEcus, LinkedList openEcus, int startTries, int startDelay, String job, String startArg, String startResult, String startValue, String pollArg, String pollResult, String pollValue, Vector ergListe)
	{		
		boolean noError = true;
		//boolean resTemp = false;
		SDiagResult result = new SDiagResult();
		
		//Start-cycle over all given ECUs
		while (startEcus.size() > 0)
		{
			int tries = startTries;
			String sgbd = (String) startEcus.getFirst();
			for (int i = 1; i <= tries; i++)				
			{
				//Start Routine
				result = executeJob (isEdiabasParallel, false, ediabas, sgbd, job, startArg, null, null, ergListe);					
				//Execution successful?
				if (result.success == true)
				{
					//Check, if routine is running
					result = executeJob (isEdiabasParallel, true, ediabas, sgbd, job, pollArg, startResult, startValue, ergListe);
					ediabas = result.ebas;
				}
				
				//Running?
				if(result.success && result.remove)
				{
					Ergebnis res;
					
					openEcus.add(sgbd);						
					startEcus.remove(sgbd);
					tries = 0;

					if (isEnglish)
						res = new Ergebnis( this.name, "Diagnose", sgbd, job, startArg, "", "", "", "", "0", "", "", "", PB.getString("Routine for " + sgbd + " started at try no " + i), "", Ergebnis.FT_IO );
					else
						res = new Ergebnis( this.name, "Diagnose", sgbd, job, startArg, "", "", "", "", "0", "", "", "", PB.getString("Routine f�r " + sgbd + " gestartet bei Versuch No " + i), "", Ergebnis.FT_IO );
										
					ergListe.add(res);					
				}
				else
				{
					Ergebnis res;
					
					if (i >= tries)
					{
						if (isEnglish)
							res = new Ergebnis( this.name, "Diagnose", sgbd, job, startArg, "", "", "", "", "0", "", "", "", PB.getString("Launch no. " + i + " NIO. Give up."), "" + "", Ergebnis.FT_NIO_SYS );
						else
							res = new Ergebnis( this.name, "Diagnose", sgbd, job, startArg, "", "", "", "", "0", "", "", "", PB.getString("Startversuch No. " + i + " NIO. Breche ab."), "" + "", Ergebnis.FT_NIO_SYS );
						
						startEcus.remove(sgbd);
						noError = false;
					}
					else
					{
						if (isEnglish)
							res = new Ergebnis( this.name, "Diagnose", sgbd, job, startArg, "", "", "", "", "0", "", "", "", PB.getString("Launch no. " + i + " NIO. Try again. . ."), "" + "", Ergebnis.FT_NIO_SYS );
						else
							res = new Ergebnis( this.name, "Diagnose", sgbd, job, startArg, "", "", "", "", "0", "", "", "", PB.getString("Startversuch No. " + i + " NIO. Wiederhole. . ."), "" + "", Ergebnis.FT_NIO_SYS );
					}						
					
					ergListe.add(res);						
				}
				
				if (i <= tries)
				{
					result = pollEcus (isEdiabasParallel, ediabas, openEcus, job, pollArg, null, null, ergListe);
					if (!result.success)
						noError = false;					
					wait (250);						
				}					
			}				
			
			if (startEcus.size() > 0)//c_It.hasNext())
			{
				//Wait startDelay
				for (int j = 0; j < startDelay; j += 250)
				{
					result = pollEcus (isEdiabasParallel, ediabas, openEcus, job, pollArg, null, null, ergListe);
					if (!result.success)
						noError = false;					
					wait (250);						
				}				
			}
		}
				
		if (!noError)
		{
			result.success = false;
			result.status = STATUS_EXECUTION_ERROR;
		}
		else
		{
			result.success = true;
			result.status = STATUS_EXECUTION_OK;			
		}
		
		return result;
	}

	/**
	 * Execute a Diag-job and catch the results
	 * 
	 * @param isEdiabasParallel
	 * @param errorOnError
	 * @param ediabas
	 * @param sgbd
	 * @param job
	 * @param arg
	 * @param resultPar
	 * @param resultVal
	 * @param ergListe
	 * @return
	 */
	private SDiagResult executeJob (boolean isEdiabasParallel, boolean errorOnError, EdiabasProxyThread ediabas, String sgbd, String job, String arg, String resultPar, String resultVal, Vector ergListe)
	{
		SDiagResult result = new SDiagResult();
		String s_JobResult = null;
		
		result.success = false;
		result.remove = false;

		try
		{
			if (isEdiabasParallel)
				ediabas = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabasParallel("", sgbd);
			else
				ediabas = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();
			
			result.ebas = ediabas;
			
			s_JobResult = ediabas.executeDiagJob(sgbd, job, arg, "");
			
			//Diag-error
			if(!s_JobResult.equalsIgnoreCase("OKAY")) 
			{   		
				//System.out.println (sgbd + ": " + job + ", " + arg);
				result.result = new Ergebnis( this.name, "Diagnose", sgbd, job, arg, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add(result.result);				
				
				if (errorOnError)
				{
					result.status = STATUS_EXECUTION_ERROR;
					result.remove = true;
					result.success = true;
				}
				else
				{
					result.status = STATUS_EXECUTION_ERROR_IGNORE;
					result.remove = false;	
					result.success = false;
				}
			} 
			//No Diag error an no result-check --> success
			else if ((resultPar == null) || (resultVal == null))
			{
				//Keine Ergebnisabfrage, und Ausf�hrung IO --> Ergebnis IO
				result.remove = true; 
				result.success = true;
			}
			//No Diag error and result check --> check result
			else
			{
				String s_Erg = ediabas.getDiagResultValue(resultPar);
					
				if (s_Erg.equalsIgnoreCase(resultVal))
				{							
					//ergListe.add(result.result);
					//--> sg hat bedingung erf�llt, entfernen!
					result.remove = true; 
					//Abfrage / Ergebnis IO
					result.success = true;
				}
				else
				{
					//--> sg hat bedingung nicht erf�llt, nicht entfernen!
					result.remove = false; 
					//Abfrage / Ergebnis IO
					result.success = true;
				}
			}
				
			if (isEdiabasParallel)
				this.getPr�flingLaufzeitUmgebung().getDeviceManager().releaseEdiabasParallel(ediabas, "", sgbd);											
		} 
		catch (ApiCallFailedException e) 
		{
			result.result = new Ergebnis( this.name, "Diagnose", sgbd, job, arg, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );

			ergListe.add(result.result);
			result.status = STATUS_EXECUTION_ERROR;					
			result.remove = true;
		} 
		catch (EdiabasResultNotFoundException e) 
		{
			if (e.getMessage() != null)
				result.result = new Ergebnis( this.name, "Diagnose", sgbd, job, arg, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
			else
				result.result = new Ergebnis( this.name, "Diagnose", sgbd, job, arg, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
			
			ergListe.add (result.result);
			result.status = STATUS_EXECUTION_ERROR;					
			result.remove = true;
		}	
		catch (Throwable ex)
		{
			result.result = new Ergebnis( this.name, "Diagnose", sgbd, job, arg, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
			
			ergListe.add(result.result);
			result.status = STATUS_EXECUTION_ERROR;					
			result.remove = true;
		}
		
		return result;
	}
	
	/**
	 * Wait
	 * 
	 * @param pause
	 */
	private void wait (int pause)
	{
		long stopTime = System.currentTimeMillis() + pause;
		while (System.currentTimeMillis() < stopTime)
		{
			try
			{
				Thread.sleep(10);
			}
			catch (InterruptedException e)
			{
				;
			}			
		}
	}
	
	/**
	 * Represents a Diagnosis-result
	 * 
	 * @author q245803
	 *
	 */
	private class SDiagResult
	{
		public Ergebnis result = new Ergebnis("", "undef", "", "", "", "", "", "", "", "0", "", "", "", "", "" + "", Ergebnis.FT_NIO );;
		public int status;
		public boolean success;
		public boolean remove;		
		public EdiabasProxyThread ebas;
	}
	
} // End of Class


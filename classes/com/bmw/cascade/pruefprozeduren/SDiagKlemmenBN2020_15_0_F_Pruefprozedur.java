/*
* SDiagKlemmenBN2020_13_0_F_Pruefprozedur.java
*
* Created on 2010/11
*/

// Import-Abschnitt
package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.*;

// Erweiterte Imports f�r diese spezielle Pr�fprozedur
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;

/**
* Sonderpr�fprozedur zum Schalten von Klemmen im Fertigungsablauf incl. aller Spezialit�ten.
*
* @author	BMW TI-533	Andreas Burger, Achim Reckel
* 
* @version	1_0_F	2010/11/??	AB		Implementierung
* @version	2_0_F	2010/11/??	AB		Kommentar und Aufgabe bei reportallresult() erg�nzt
* @version	3_0_F	2011/04/07	AR		Anpassung an das aktuell abgesicherte Schaltschema
* @version	4_0_F	2011/07/18	AR		Debugging hinzugef�gt
* @version	5_0_F	2011/07/20	AR		Methode zum Aufwecken des Fahrzeugs entfernt
* @version	6_0_F	2011/07/27	AR		ELV-Workaround optimiert
* @version	7_0_F	2011/08/25	AR		"Repeat Once On Error" hinzugef�gt
* @version	8_0_F	2011/09/07	AR		Ablaufkorrektur (Timeout vor STEUERN_KL15)
* @version	9_0_F	2011/09/07	AR		neuen Timeout als optionalen Parameter hinzugef�gt, STEUERN_KL15 wird im Fehlerfall einmalig wiederholt
* @version 10_0_F	2011/09/09	AR		Klemmen: keine Statusabfragen nach Steuern-Job NIO; APDM A statt F bei erstem Versuch
* @version 11_0_F	2012/06/05	AR/TS 	Traces: Trace-Parameter nur noch f�r Zusatzinfos
* @version 12_0_T	2012/06/05	AR/TS 	T-Version: Traces: Trace-Parameter nur noch f�r Zusatzinfos
* @version 13_0_F	2012/06/05	AR/TS 	F-Version
* @version 14_0_T	2017/05/02	MKe 	F�higkeit der Parallel Diagnose hinzugef�gt.
* @version 15_0_F	2017/05/02	MKe 	F-Version.
*/

// TODO Timeout/Pause f�r Klemmen-Steuer-Jobs (optional, default 0) [der aktuelle ist f�r Klemmen-Lese_Jobs]
// TODO �berfl�ssige Wartezeit: Eventuell sogar noch "if(System.currentTimeMillis() + i_pause <= stopTime)"

// Deklaration der Pr�fprozedur
public class SDiagKlemmenBN2020_15_0_F_Pruefprozedur extends AbstractBMWPruefprozedur
{
	// definiere Konstanten
	static final long serialVersionUID = 1L;

	// language
	private final boolean isEnglish = System.getProperty("user.language").equalsIgnoreCase("EN");
	
	// Statuswerte f�r verschiedene Zust�nde
	static final String CONST_STATUS_KL15 = "10"; 			// Status f�r Klemme 15
	static final String CONST_STATUS_KL30B = "6"; 			// Status f�r Klemme 30B
	static final String CONST_STATUS_CAS_MM_2 = "2"; 		// Status f�r CAS_MONTAGEMODE_2
	static final String CONST_STATUS_ELV_ENTR_1 = "0"; 		// Status f�r ELV_ENTRIEGELT, M�glichkeit 1, Entriegelt
	static final String CONST_STATUS_ELV_ENTR_2 = "16"; 	// Status f�r ELV_ENTRIEGELT, M�glichkeit 2, Entriegelt (nicht initialisiert)
	static final String CONST_STATUS_ELV_ENTR_3 = "32"; 	// Status f�r ELV_ENTRIEGELT, M�glichkeit 3, Entriegelt (falsche Initialisierung)
	static final String CONST_STATUS_ELV_VER_1 = "1"; 		// Status f�r ELV_VERRIEGELT, M�glichkeit 1, Verriegelt
	static final String CONST_STATUS_ELV_VER_2 = "17"; 		// Status f�r ELV_VERRIEGELT, M�glichkeit 2, Verriegelt (nicht initialisiert)
	static final String CONST_STATUS_ELV_VER_3 = "33"; 		// Status f�r ELV_VERRIEGELT, M�glichkeit 3, Verriegelt (falsche Initialisierung)
	static final String CONST_STATUS_ELV_UNBK_1 = "2";		// Status f�r ELV_UNBEKANNT, M�glichkeit 1
	static final String CONST_STATUS_ELV_UNBK_2 = "255";	// Status f�r ELV_UNBEKANNT, M�glichkeit 2
	static final int 	CONST_KEY_NR_MIN = 0;				// untere Bereichsgrenze f�r g�ltigen Schl�ssel
	static final int 	CONST_KEY_NR_MAX = 9;				// obere Bereichsgrenze f�r g�ltigen Schl�ssel
	
	// Definition der Jobs
	static final String CONST_JOB_IDENT = "IDENT";
//	static final String CONST_JOB_ICOM_WUP = "STEUERN_SEND_WAKE_UP"; // kein Fahrzeugwecken in dieser Pr�fprozedur
	static final String CONST_JOB_STAT_LESEN = "STATUS_LESEN";
	static final String CONST_JOB_STAT_ELV = "STATUS_ELV";
	static final String CONST_JOB_STAT_ELV_HIST = "status_elv_historie";
	static final String CONST_JOB_STR_ROUTINE = "STEUERN_ROUTINE";
	static final String CONST_JOB_STR_KL = "STEUERN_KLEMMEN";
	
	// Definition der Parameter
	static final String CONST_PAR_STAT_KL = "ARG;STATUS_KLEMMEN";
	static final String CONST_PAR_STAT_MM = "ARG;CAS_MONTAGEMODUS";
	static final String CONST_PAR_ELV_AKT_4 = "ARG;ELV_AKTION;STR;4";
	static final String CONST_PAR_ELV_AKT_0 = "ARG;ELV_AKTION;STR;0";
	static final String CONST_PAR_STR_CA_BROADCAST = "ARG;STEUERN_CA_BROADCAST;STR";
	static final String CONST_PAR_STR_KL15 = "KL15_EIN";
	static final String CONST_PAR_STR_KL30B = "KL30B_EIN";
	
	// Definition der Results
	static final String CONST_RESULT_STD = "JOB_STATUS";
	static final String CONST_RESULT_STAT_KL = "STAT_KLEMMENSTATUS";
	static final String CONST_RESULT_STAT_MM = "STAT_CAS_MONTAGEMODUS";
	static final String CONST_RESULT_STAT_ELV = "STAT_ELV_ZUSTAND";
	static final String CONST_RESULT_STAT_ELV_AS = "STAT_ELV_AUSFUEHRUNGSSTATUS";
	
	// Std-Trace Variable
	static final boolean stdTrace = true;

	/**
	* DefaultKonstruktor, nur fuer die Deserialisierung
	*/
	public SDiagKlemmenBN2020_15_0_F_Pruefprozedur(){}
	
	/**
	* erzeugt eine neue Pruefprozedur mit obigem Verhalten
	* 
	* @param pruefling Klasse des zugeh. Pr�flings
	* @param pruefprozName Name der Pr�fprozedur
	* @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	*/
	
	// Implementierung des Konstruktors
	public SDiagKlemmenBN2020_15_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted)
	{
		super(pruefling, pruefprozName, hasToBeExecuted);
		attributeInit();
	}

	/**
	* initialisiert die Argumente
	*/
	protected void attributeInit()
	{
		super.attributeInit();
	}

	/**
	* liefert die optionalen Argumente
	*/
	public String[] getOptionalArgs()
	{
		String[] args = {	// "TIMEOUT_WUP", 			// Wartezeit f�r den Ident-Job // kein Fahrzeugwecken in dieser Pr�fprozedur
							// "PAUSE_WUP", 			// Pause f�r den Ident-Job // kein Fahrzeugwecken in dieser Pr�fprozedur
				
							"TIMEOUT_CLAMPS", 		// Wartezeit f�r die Klemmen-Jobs
							"PAUSE_CLAMPS",			// Pause f�r die Klemmen-Jobs
							
							"TIMEOUT_BROADCAST",	// Wartezeit f�r die Schl�sselsuche
							"PAUSE_BROADCAST",		// Pause f�r die Schl�sselsuche (auch Wartezeit zwischen CA_Broadcast und ELV-Stausabfrage; mind. 1000ms!)
							
							"TIMEOUT_ELV",			// Wartezeit f�r die ELV-Status-Abfrage
							"PAUSE_ELV",			// Pause f�r die ELV-Status-Abfrage

							"TRACE",				// wenn "TRUE" werden zus�tzliche Informationen aufgezeichnet (siehe 'doTrace')
													// TRACE = Ablauf-Debugging, alle Parameter in APDM schreiben
							
							"ELV_STEUERN_EXTRA",	// wenn "TRUE", f�hrt den ELV_STEUERN-Job immer nach dem CA_Broadcast aus (10sec Problem)		
							"ELV_LESEN_EXTRA",		// wenn "TRUE", f�hrt bei unbekannter ELV den Job ELV_AUSFUEHRUNGSSTATUS_LESEN aus 
													// und fragt dann erneut die ELV ab (ELV noch unbekannt Problem)
							
							"KL_SHORTCUT",			// KL30B schalten einsparen, wenn vorher schon geschaltet
							
							"TIMEOUT_PRE_KL15",		// Neuer Timeout, warten bis die Klstr. das entriegeln der ELV mitbekommen hat
							
							"tag"
						};
		return args;
	}

	/**
	* liefert die zwingend erforderlichen Argumente
	*/
	public String[] getRequiredArgs()
	{
		String[] args = {	"REF_CLAMP_ECU",	// SGBD des Steuerger�ts f�r Klemmensteuerung (aktuell FEM ("FEM_20")) oder Pr�flingsname
							// "REF_ICOM",		// SGBD des ICOM ("ICOM_P") oder Pr�flingsname // kein Fahrzeugwecken in dieser Pr�fprozedur
							"KL15",				// KL15 ein- ("EIN", "ON", "1", "TRUE", "T") oder aussschalten ("AUS", "OFF", "0", "FALSE", "F")
							"ELV_FZG"			// ELV-Fzg. ("EIN", "ON", "1", "TRUE", "T") oder kein ELV-Fzg. ("AUS", "OFF", "0", "FALSE", "F")
						};
		return args;
	}

	/**
	* pr�ft - soweit wie m�glich - die Argumente auf Existenz und Wert
	*/
	public boolean checkArgs()
	{
		// Existenzpr�fung
		boolean ok = super.checkArgs();

		return ok;
	}

	/**
	* f�hrt die Pr�fprozedur aus
	* 
	* @param info Information zur Ausf�hrung
	*/
	public void execute(ExecutionInfo info)
	{
		// zwingend notwendige Variablen
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		EdiabasProxyThread c_Ediabas = null;
		//boolean b_EdiabasParallel = false; // potError L�sungsansatz bei Problemen an verschieden Pr�fst�nden

		// spezifische Variablen
		String s_sgbd_clamp_ecu = "";		// SGBD des Steuerger�ts f�r Klemmensteuerung (aktuell FEM) als String
		// String s_sgbd_ICOM = "";			// SGBD des ICOM als String // kein Fahrzeugwecken in dieser Pr�fprozedur
		boolean kl15 = false;				// KL15 ein- ('true') oder aussschalten ('false')
		boolean elv_fzg = true;				// Information ob ELV-Fzg. ('true') oder Automatik ('false')
		
		boolean elv_steuern_extra = true;	// F�hrt den ELV_STEUERN-Job immer nach dem CA_Broadcast aus (10sec Problem)
		boolean elv_lesen_extra = true;		// F�hrt bei unbekannter ELV den Job ELV_AUSFUEHRUNGSSTATUS_LESEN aus und fragt dann erneut die ELV ab
											// (ELV noch unbekannt Problem)
		
		boolean kl_shortcut = true;			// KL30B schalten einsparen, wenn vorher schon geschaltet (bei "TRUE")
		
		int i_timeout_pre_kl15 = -1;		// Timeout vor dem Steuern der KL15, Klstr. muss das entrieglen der ELV erst mitbekommen

		// int i_timeout_wup = -1;			// Wartezeit f�r den Ident-Job als Integer // kein Fahrzeugwecken in dieser Pr�fprozedur
		// int i_pause_wup = -1;			// Pause f�r den Ident-Job als Integer // kein Fahrzeugwecken in dieser Pr�fprozedur
		
		int i_timeout_clamps = -1; 			// Wartezeit f�r die Klemmen-Jobs als Integer
		int i_pause_clamps = -1;			// Pause f�r die Klemmen-Jobs als Integer
		
		int i_timeout_broadcast = -1;		// Wartezeit f�r die Schl�sselsuche als Integer
		int i_pause_broadcast = -1;			// Pause f�r die Schl�sselsuche als Integer
		
		int i_timeout_elv = -1;				// Wartezeit f�r die ELV-Status-Abfrage als Integer
		int i_pause_elv = -1;				// Pause f�r die ELV-Status-Abfrage als Integer

		boolean doTrace = false;			// wenn 'true' werden zus�tzliche Informationen in APDM gespeichert
		
		int step = 0;						// Z�hlt die Anzahl der Durchl�ufe
		int repeat_max = 2;					// Wie oft im Fehlerfall wiederholen (2 = Repeat Once On Error) 
		boolean abort = false;				// Abbruchbedingung
		
		boolean parallelDiagnose = false;
		DeviceManager devMan = null;
		String tag = null;
		
		do
		{
			status = STATUS_EXECUTION_OK;	// auch bei zweitem Durchlauf nach NIO mit richtiger Startbedingung beginnen
			step++; // Durchlauf hochz�hlen
			
			// �bergeordneter try/catch-Block
			try
			{
				if(step==1) // nur im ersten Durchlauf pr�fen
				{
					// Parameter �bergeben
					try
					{
						if(checkArgs() == false)
							throw new PPExecutionException(PB.getString("parameterexistenz"));
					
						doTrace = resolveBoolean(ergListe, info, "TRACE", false, false, doTrace);
						addComment(ergListe, "Parameter", "parameter", "TRACE", doTrace, Ergebnis.FT_IO, doTrace);
						
						kl_shortcut = resolveBoolean(ergListe, info, "KL_SHORTCUT", true, false, doTrace);
						addComment(ergListe, "Parameter", "parameter", "KL_SHORTCUT", kl_shortcut, Ergebnis.FT_IO, doTrace);
						
						s_sgbd_clamp_ecu = resolveSgbd(ergListe, info, "REF_CLAMP_ECU");
						addComment(ergListe, "Parameter", "parameter", "REF_CLAMP_ECU", s_sgbd_clamp_ecu, Ergebnis.FT_IO, doTrace);
						
						// s_sgbd_ICOM = resolveSgbd(ergListe, info, "REF_ICOM"); // kein Fahrzeugwecken in dieser Pr�fprozedur
						// addComment(ergListe, "Parameter", "parameter", "REF_ICOM", s_sgbd_ICOM, Ergebnis.FT_IO, doTrace); // kein Fahrzeugwecken in dieser Pr�fprozedur
		
						kl15 = resolveBoolean(ergListe, info, "KL15", false, true, doTrace);
						addComment(ergListe, "Parameter", "parameter", "KL15", kl15, Ergebnis.FT_IO, doTrace);
						
						elv_fzg = resolveBoolean(ergListe, info, "ELV_FZG", true, true, doTrace);
						addComment(ergListe, "Parameter", "parameter", "ELV_FZG", elv_fzg, Ergebnis.FT_IO, doTrace);
						
						elv_steuern_extra = resolveBoolean(ergListe, info, "ELV_STEUERN_EXTRA", true, false, doTrace);
						addComment(ergListe, "Parameter", "parameter", "ELV_STEUERN_EXTRA", elv_steuern_extra, Ergebnis.FT_IO, doTrace);
						
						elv_lesen_extra = resolveBoolean(ergListe, info, "ELV_LESEN_EXTRA", true, false, doTrace);
						addComment(ergListe, "Parameter", "parameter", "ELV_LESEN_EXTRA", elv_lesen_extra, Ergebnis.FT_IO, doTrace);
						
						// i_timeout_wup = resolveInt_timeout(ergListe, info, "TIMEOUT_WUP", 2000, false, doTrace); // kein Fahrzeugwecken in dieser Pr�fprozedur
						// addComment(ergListe, "Parameter", "parameter", "TIMEOUT_WUP", i_timeout_wup, Ergebnis.FT_IO, doTrace); // kein Fahrzeugwecken in dieser Pr�fprozedur
						// i_pause_wup = resolveInt_timeout(ergListe, info, "PAUSE_WUP", 100, false, doTrace); // kein Fahrzeugwecken in dieser Pr�fprozedur
						// addComment(ergListe, "Parameter", "parameter", "PAUSE_WUP", i_pause_wup, Ergebnis.FT_IO, doTrace); // kein Fahrzeugwecken in dieser Pr�fprozedur
		
						i_timeout_clamps = resolveInt_timeout(ergListe, info, "TIMEOUT_CLAMPS", 5000, false, doTrace);
						addComment(ergListe, "Parameter", "parameter", "TIMEOUT_CLAMPS", i_timeout_clamps, Ergebnis.FT_IO, doTrace);
						i_pause_clamps = resolveInt_timeout(ergListe, info, "PAUSE_CLAMPS", 500, false, doTrace);
						addComment(ergListe, "Parameter", "parameter", "PAUSE_CLAMPS", i_pause_clamps, Ergebnis.FT_IO, doTrace);
		
						i_timeout_broadcast = resolveInt_timeout(ergListe, info, "TIMEOUT_BROADCAST", 10000, false, doTrace);
						addComment(ergListe, "Parameter", "parameter", "TIMEOUT_BROADCAST", i_timeout_broadcast, Ergebnis.FT_IO, doTrace);
						i_pause_broadcast = resolveInt_timeout(ergListe, info, "PAUSE_BROADCAST", 1000, false, doTrace);
						addComment(ergListe, "Parameter", "parameter", "PAUSE_BROADCAST", i_pause_broadcast, Ergebnis.FT_IO, doTrace);
						
						i_timeout_elv = resolveInt_timeout(ergListe, info, "TIMEOUT_ELV", 2000, false, doTrace);
						addComment(ergListe, "Parameter", "parameter", "TIMEOUT_ELV", i_timeout_elv, Ergebnis.FT_IO, doTrace);
						i_pause_elv = resolveInt_timeout(ergListe, info, "PAUSE_ELV", 100, false, doTrace);
						addComment(ergListe, "Parameter", "parameter", "PAUSE_ELV", i_pause_elv, Ergebnis.FT_IO, doTrace);
						
						i_timeout_pre_kl15 = resolveInt_timeout(ergListe, info, "TIMEOUT_PRE_KL15", 300, false, doTrace);
						addComment(ergListe, "Parameter", "parameter", "TIMEOUT_PRE_KL15", i_timeout_pre_kl15, Ergebnis.FT_IO, doTrace);
						
						if( getArg( "TAG" ) != null ){
							tag = getArg( "TAG" );
						}
					}
					catch(PPExecutionException e)
					{
						addDiagResult(ergListe, "ParaFehler", "paraError", "Pr�fung der Parameter", "validating the parameters", "", "", "", "", "", "", PB.getString("parametrierfehler"), PB.getString("parameterError"), Ergebnis.FT_NIO_SYS, true);
						if(stdTrace)
							e.printStackTrace(); // Details im Pr�fstand Screenlog ausgeben
						
						abort = true; // Bei Parameterfehler Pr�fung nicht wiederholen 
						
						throw new PPExecutionException();
					}
				}
				
				
				try // Pr�fstandvariabel auslesen, ob Paralleldiagnose
				{
					Object pr_var;
					pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
					if( pr_var != null ) {
						if( pr_var instanceof String ) {
							if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
								parallelDiagnose = true;
							}
						} else if( pr_var instanceof Boolean ) {
							if( ((Boolean) pr_var).booleanValue() ) {
								parallelDiagnose = true;
							}
						}
					}
				} catch( VariablesException ex ) {	
				}
				
				// Devicemanager
				devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();
				
				// Diagnosejobs ausf�hren
				if( parallelDiagnose ) // Ediabas holen
				{	
					c_Ediabas = devMan.getEdiabasParallel( tag, s_sgbd_clamp_ecu );		
				} else {
					c_Ediabas = devMan.getEdiabas( tag );
				}
				
				// Diagnose_START passiert im Pr�fumfang
	
				// Ist Fahrzeug ansprechbar?
				simpleIdent(ergListe, s_sgbd_clamp_ecu, c_Ediabas, step, repeat_max, stdTrace); // einfaches Fragen, ob Fzg. wach ist
				// wakeUpIdent(ergListe, s_sgbd_clamp_ecu, s_sgbd_ICOM, i_timeout_wup, i_pause_wup, c_Ediabas, doTrace); // kein Fahrzeugwecken in dieser Pr�fprozedur
	
				// Fahrzeug wach und ansprechbar
	
				// Klemme 15 ein- oder ausschalten
				if(kl15 == true)
					main_clamp15On(ergListe, s_sgbd_clamp_ecu, i_timeout_clamps, i_pause_clamps, i_timeout_broadcast, i_pause_broadcast, i_timeout_elv, i_pause_elv, c_Ediabas, elv_fzg, elv_lesen_extra, elv_steuern_extra, kl_shortcut, i_timeout_pre_kl15, step, repeat_max, stdTrace);
				else
					main_clamp15Off(ergListe, s_sgbd_clamp_ecu, i_timeout_clamps, i_pause_clamps, c_Ediabas, step, repeat_max, stdTrace);
	
				// Diagnose_ENDE passiert im Pr�fumfang
				// "Diagnosejobs ausf�hren" beendet
			}//  Ende �bergeordneter try-Block
			catch(PPExecutionException e)
			{
				status = STATUS_EXECUTION_ERROR;
				if(stdTrace)
					e.printStackTrace(); // Details im Pr�fstand Screenlog ausgeben
			}
			catch(Exception e)		
			{
				if(e instanceof DeviceLockedException)
					addDiagResult(ergListe, "ExecFehler", "ExecError", "", "", "", "", "", "", "", "", "DeviceLockedException (" + e.getMessage() + ")" + PB.getString("ausfuehrungsfehler"), "DeviceLockedException (" + e.getMessage() + ")" + PB.getString("executionError"), Ergebnis.FT_NIO_SYS, true);
				else if(e instanceof DeviceNotAvailableException)
					addDiagResult(ergListe, "ExecFehler", "ExecError", "", "", "", "", "", "", "", "", "DeviceNotAvailableException (" + e.getMessage() + ")" + PB.getString("ausfuehrungsfehler"), "DeviceNotAvailableException (" + e.getMessage() + ")" + PB.getString("executionError"), Ergebnis.FT_NIO_SYS, true);
				else
					addDiagResult(ergListe, "ExecFehler", "ExecError", "", "", "", "", "", "", "", "", "Exception (" + e.getMessage() + ")" + PB.getString("unerwarteterLaufzeitfehler"), "Exception (" + e.getMessage() + ")" + PB.getString("unexpectedRuntimeError"), Ergebnis.FT_NIO_SYS, true);
	
				status = STATUS_EXECUTION_ERROR;
				if(stdTrace)
					e.printStackTrace(); // Details im Pr�fstand Screenlog ausgeben
			}
			catch(Throwable e)
			{
				addDiagResult(ergListe, "ExecFehler", "ExecError", "", "", "", "", "", "", "", "", "Throwable (" + e.getMessage() + ")" + PB.getString("unerwarteterLaufzeitfehler"), "Throwable (" + e.getMessage() + ")" + PB.getString("unexpectedRuntimeError"), Ergebnis.FT_NIO_SYS, true);
				status = STATUS_EXECUTION_ERROR;
				if(stdTrace)
					e.printStackTrace(); // Details im Pr�fstand Screenlog ausgeben
			}
			
			if((status == STATUS_EXECUTION_OK) || (step >= repeat_max))
				abort = true;
			
		} while(!abort);
		
		// MBa: Paralleldiagnose
		if( parallelDiagnose ) //&& ept!=null)
		{
			try {
				devMan.releaseEdiabasParallel( c_Ediabas, tag, s_sgbd_clamp_ecu );
			} catch( DeviceLockedException ex ) {
				addDiagResult(ergListe, "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "", "DeviceLockedException", ex.getMessage(), Ergebnis.FT_NIO_SYS, true);
				status = STATUS_EXECUTION_ERROR;
			} catch( Exception ex ) {
				addDiagResult(ergListe, "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "", "Exception ", "Unexpected runtime error in releasing the parallel diagnosis", Ergebnis.FT_NIO_SYS, true);
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable ex ) {
				addDiagResult(ergListe, "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "", "Throwable ", "Unexpected runtime error in releasing the parallel diagnosis", Ergebnis.FT_NIO_SYS, true);
				status = STATUS_EXECUTION_ERROR;
			}
		}

		setPPStatus(info, status, ergListe); // finalen Status setzen			
	} // Ende von execute()

	/**
	 * simple Identification
	 * 
	 * @param list
	 * @param s_sgbd_clamp_ecu
	 * @param c_Ediabas
	 * @param step
	 * @param repeat_max
	 * @param doTrace
	 * @throws PPExecutionException 
	 */
	private void simpleIdent(Vector list, String s_sgbd_clamp_ecu, EdiabasProxyThread c_Ediabas, int step, int repeat_max, boolean doTrace) throws PPExecutionException
	{
		String s_ediabas_result = null;
		
		// Ident: Fahrzeug wach?
		s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_IDENT, "", CONST_RESULT_STD, c_Ediabas, doTrace);
		if(s_ediabas_result.equalsIgnoreCase("OKAY"))
			addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_IDENT, "", CONST_RESULT_STD, s_ediabas_result, "OKAY", "", "", Ergebnis.FT_IO, doTrace);
		else // "Fzg. nicht weckbar"
		{			
			if(step >= repeat_max) // Wenn wir schon im letzten Durchlauf sind -> NIO ; sonst IGNORE, da es noch einen weiteren Versuch gibt
				addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_IDENT, "", CONST_RESULT_STD, s_ediabas_result, "OKAY", "Fzg. ist nicht wach/ansprechbar", "car not awake", Ergebnis.FT_NIO, doTrace);
			else
				addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_IDENT, "", CONST_RESULT_STD, s_ediabas_result, "OKAY", "Fzg. ist nicht wach/ansprechbar", "car not awake", Ergebnis.FT_IGNORE, doTrace);
				
			throw new PPExecutionException();
		}
	}
	
/* kein Fahrzeugwecken in dieser Pr�fprozedur	
	private void wakeUpIdent(Vector list, String s_sgbd_clamp_ecu, String s_sgbd_ICOM, int i_timeout, int i_pause, EdiabasProxyThread c_Ediabas, boolean doTrace) throws PPExecutionException
	{
		String s_ediabas_result = null;
		
		long stopTime = -1;
		long timeout = i_timeout;

		boolean ident_okay = false; // Pr�fvariable f�r Timeout-Schleife

		// Ident: Fahrzeug wach?
		s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_IDENT, "", CONST_RESULT_STD, c_Ediabas, doTrace);
		if(s_ediabas_result.equalsIgnoreCase("OKAY"))
			addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_IDENT, "", CONST_RESULT_STD, s_ediabas_result, "OKAY", "", "", Ergebnis.FT_IO, doTrace);
		else
		{
			addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_IDENT, "", CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_IGNORE, doTrace);
			// Fahrzeug wecken (ICOM)
			s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_ICOM, CONST_JOB_ICOM_WUP, "", CONST_RESULT_STD, c_Ediabas, doTrace);
			if(s_ediabas_result.equalsIgnoreCase("OKAY"))
				addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_ICOM, CONST_JOB_ICOM_WUP, "", CONST_RESULT_STD, s_ediabas_result, "OKAY", "", "", Ergebnis.FT_IO, doTrace);
			else // ICOM WakeUp fehlgeschlagen
				addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_ICOM, CONST_JOB_ICOM_WUP, "", CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosesError"), Ergebnis.FT_IGNORE, doTrace);
			// Ident: Fahrzeug wach? (der zweite Versuch)
			stopTime = System.currentTimeMillis() + timeout;
			do
			{
				// Exception soll nicht zum Abbruch f�hren, wenn innerhalb des Timeouts kein "OKAY" -> throw Exception
				s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_IDENT, "", CONST_RESULT_STD, c_Ediabas, doTrace);	
				
				if(s_ediabas_result.equalsIgnoreCase("OKAY"))
				{
					addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_IDENT, "", CONST_RESULT_STD, s_ediabas_result, "OKAY", "", "", Ergebnis.FT_IO, doTrace);
					ident_okay = true;
					break;
				}
				else
				{
					addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_IDENT, "", CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_IGNORE, doTrace);
					if(System.currentTimeMillis() <= stopTime)
						wait(i_pause);
				}
			} while(System.currentTimeMillis() <= stopTime);
			if(ident_okay == false)
			{
				// "Fzg. nicht weckbar"
				addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_IDENT, "", CONST_RESULT_STD, s_ediabas_result, "OKAY", "Fzg. nicht weckbar", "unable to wake up car", Ergebnis.FT_NIO, doTrace);
				throw new PPExecutionException();
			}
		}
	}
*/
	
	/**
	 * main procedure for setting clamp 15 Off
	 * 
	 * @param list
	 * @param s_sgbd_clamp_ecu
	 * @param i_timeout
	 * @param i_pause
	 * @param c_Ediabas
	 * @param step
	 * @param repeat_max
	 * @param doTrace
	 * @throws PPExecutionException 
	 */
	private void main_clamp15Off(Vector list, String s_sgbd_clamp_ecu, int i_timeout, int i_pause, EdiabasProxyThread c_Ediabas, int step, int repeat_max, boolean doTrace) throws PPExecutionException
	{
		String s_ediabas_result = null;
		String s_result_status = null;

		// Status KL30B abfragen
		s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, c_Ediabas, doTrace);
		// Klemmenstatus auslesen, CONST_STATUS_KL30B entspricht KL30B
		s_result_status = getDiagResultValueHandleExceptions(CONST_RESULT_STAT_KL, c_Ediabas, doTrace);
		if(s_ediabas_result.equalsIgnoreCase("OKAY") && s_result_status.equalsIgnoreCase(CONST_STATUS_KL30B))
			addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, s_result_status, CONST_STATUS_KL30B, "", "", Ergebnis.FT_IO, doTrace);
		else
		{
			if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result CONST_RESULT_STAT_KL, da Job-Ausf�hrung "OKAY"
				addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, s_result_status, CONST_STATUS_KL30B, "", "", Ergebnis.FT_IGNORE, doTrace);
			else // Fehler bei der Job-Ausf�hrung
				addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_IGNORE, doTrace);

			// Schalte Klemme 30B
			setClamp30B(list, s_sgbd_clamp_ecu, i_timeout, i_pause, c_Ediabas, "EDIABAS", "EDIABAS", step, repeat_max, doTrace);
		}
	}
	
	/**
	 * main procedure for setting clamp 15 On
	 * 
	 * @param list
	 * @param s_sgbd_clamp_ecu
	 * @param i_clamp_timeout
	 * @param i_clamp_pause
	 * @param i_cas_timeout
	 * @param i_cas_pause
	 * @param i_elv_timeout
	 * @param i_elv_pause
	 * @param c_Ediabas
	 * @param elv_fzg
	 * @param elv_lesen_extra
	 * @param elv_steuern_extra
	 * @param kl_shortcut
	 * @param i_timeout_pre_kl15
	 * @param step
	 * @param repeat_max
	 * @param doTrace
	 * @throws PPExecutionException 
	 */
	private void main_clamp15On(Vector list, String s_sgbd_clamp_ecu, int i_clamp_timeout, int i_clamp_pause, int i_cas_timeout, int i_cas_pause, int i_elv_timeout, int i_elv_pause, EdiabasProxyThread c_Ediabas, boolean elv_fzg, boolean elv_lesen_extra, boolean elv_steuern_extra, boolean kl_shortcut, int i_timeout_pre_kl15, int step, int repeat_max, boolean doTrace) throws PPExecutionException
	{
		String s_ediabas_result = null;
		String s_result_status = null;
		
		try
		{
			// Status 15 abfragen
			s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, c_Ediabas, doTrace);
			// Klemmenstatus auslesen, CONST_STATUS_KL15 entspricht KL15
			s_result_status = getDiagResultValueHandleExceptions(CONST_RESULT_STAT_KL, c_Ediabas, doTrace);
			if(s_ediabas_result.equalsIgnoreCase("OKAY") && s_result_status.equalsIgnoreCase(CONST_STATUS_KL15))
				addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, s_result_status, CONST_STATUS_KL15, "", "", Ergebnis.FT_IO, doTrace);
			else
			{
				if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result CONST_RESULT_STAT_KL, da Job-Ausf�hrung "OKAY"
					addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, s_result_status, CONST_STATUS_KL15, "", "", Ergebnis.FT_IGNORE, doTrace);
				else // Fehler bei der Job-Ausf�hrung
					addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_IGNORE, doTrace);
				// weiter je nach Fzg.-Typ
				if(elv_fzg == true) // ELV-Fahrzeug
					main_clamp15On_elv(list, s_sgbd_clamp_ecu, i_clamp_timeout, i_clamp_pause, i_cas_timeout, i_cas_pause, i_elv_timeout, i_elv_pause, c_Ediabas, elv_lesen_extra, elv_steuern_extra, kl_shortcut, s_result_status, i_timeout_pre_kl15, step, repeat_max, doTrace);
				else // Automatik-Fahrzeug
					setClamp15(list, s_sgbd_clamp_ecu, i_clamp_timeout, i_clamp_pause, c_Ediabas, i_timeout_pre_kl15, step, repeat_max, doTrace); // Schalte Klemme 15
			}
		}
		catch(PPExecutionException e)
		{
			if(doTrace)
				e.printStackTrace(); // Details im Pr�fstand Screenlog ausgeben
			
			// Fehler in "setClamp15" oder "unlockELV"
			throw new PPExecutionException();
		}
	}

	/**
	 * sub-procedure for vehicles with electronic steering column lock
	 * 
	 * @param list
	 * @param s_sgbd_clamp_ecu
	 * @param i_clamp_timeout
	 * @param i_clamp_pause
	 * @param i_cas_timeout
	 * @param i_cas_pause
	 * @param i_elv_timeout
	 * @param i_elv_pause
	 * @param c_Ediabas
	 * @param elv_lesen_extra
	 * @param elv_steuern_extra
	 * @param kl_shortcut
	 * @param s_kl_shortcut_status
	 * @param i_timeout_pre_kl15
	 * @param step
	 * @param repeat_max
	 * @param doTrace
	 * @throws PPExecutionException
	 */
	private void main_clamp15On_elv(Vector list, String s_sgbd_clamp_ecu, int i_clamp_timeout, int i_clamp_pause, int i_cas_timeout, int i_cas_pause, int i_elv_timeout, int i_elv_pause, EdiabasProxyThread c_Ediabas, boolean elv_lesen_extra, boolean elv_steuern_extra, boolean kl_shortcut, String s_kl_shortcut_status, int i_timeout_pre_kl15, int step, int repeat_max, boolean doTrace) throws PPExecutionException
	{
		String s_ediabas_result = null;
		String s_result_status = null;
		String s_result_status1 = null;
		
		long stopTime = -1;
		
		boolean b_temp_elv = true; // Hilfsvariable zum ELV-unbekannt-Workaround (false = ELV muss noch entriegelt werden)
		
		// String erstellen der alle g�ltigen IO-Werte enth�lt, um das Ganze nachher mit addDiagResult vern�nftig in APDM schreiben zu k�nnen
		String s_elv_values = CONST_STATUS_ELV_ENTR_1 + ";" + CONST_STATUS_ELV_ENTR_2 + ";" + CONST_STATUS_ELV_ENTR_3;
		
		try
		{
			// Status CAS_MONTAGEMODUS abfragen
			s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_MM, CONST_RESULT_STAT_MM, c_Ediabas, doTrace);
			// Montagemodus auslesen, CONST_STATUS_CAS_MM_2 = MONTAGEMODUS_2 aktiv
			s_result_status = getDiagResultValueHandleExceptions(CONST_RESULT_STAT_MM, c_Ediabas, doTrace);
			if(s_ediabas_result.equalsIgnoreCase("OKAY") && s_result_status.equalsIgnoreCase(CONST_STATUS_CAS_MM_2))
			{					
				addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_MM, CONST_RESULT_STAT_MM, s_result_status, CONST_STATUS_CAS_MM_2, "", "", Ergebnis.FT_IO, doTrace);
				
				// Schalte Klemme 15
				setClamp15(list, s_sgbd_clamp_ecu, i_clamp_timeout, i_clamp_pause, c_Ediabas, i_timeout_pre_kl15, step, repeat_max, doTrace);
			}
			else
			{				
				if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result CONST_RESULT_STAT_MM, da Job-Ausf�hrung "OKAY"
					addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_MM, CONST_RESULT_STAT_MM, s_result_status, CONST_STATUS_CAS_MM_2, "", "", Ergebnis.FT_IGNORE, doTrace);
				else // Fehler bei der Job-Ausf�hrung
					addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_MM, CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_IGNORE, doTrace);
				
				// Status ELV_ENTRIEGELT abfragen
				s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV, "", CONST_RESULT_STAT_ELV, c_Ediabas, doTrace);
				// ELV-Status auslesen, CONST_STATUS_ELV_ENTR_1/2/3 = ELV entriegelt
				s_result_status = getDiagResultValueHandleExceptions(CONST_RESULT_STAT_ELV, c_Ediabas, doTrace);
				if(s_ediabas_result.equalsIgnoreCase("OKAY") && (s_result_status.equalsIgnoreCase(CONST_STATUS_ELV_ENTR_1) || s_result_status.equalsIgnoreCase(CONST_STATUS_ELV_ENTR_2) || s_result_status.equalsIgnoreCase(CONST_STATUS_ELV_ENTR_3)))
				{
					addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV, "", CONST_RESULT_STAT_ELV, s_result_status, s_elv_values, "", "", Ergebnis.FT_IO, doTrace);
					
					// Schalte Klemme 15
					setClamp15(list, s_sgbd_clamp_ecu, i_clamp_timeout, i_clamp_pause, c_Ediabas, i_timeout_pre_kl15, step, repeat_max, doTrace);
				}
				else
				{
					if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result CONST_RESULT_STAT_ELV, da Job-Ausf�hrung "OKAY"
					{
						addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV, "", CONST_RESULT_STAT_ELV, s_result_status, s_elv_values, "", "", Ergebnis.FT_IGNORE, doTrace);
						
						try // Exception im Workaround darf auf keinen Fall die Pr�fung unterbrechen
						{
							// ELV-unbekannt-Workaround
							if((s_result_status.equalsIgnoreCase(CONST_STATUS_ELV_UNBK_1) || s_result_status.equalsIgnoreCase(CONST_STATUS_ELV_UNBK_2)) && elv_lesen_extra)
							{
								// Klemme 30B schalten							
								if(kl_shortcut && s_kl_shortcut_status.equalsIgnoreCase(CONST_STATUS_KL30B))
									; // wir sind bereits in KL30B
								else
								{
									// TODO: unsch�n umgesetzt, zeitnah korriegieren!!!
									// wir �bergeben feste Werte f�r step und repeat_max, somit wird setClamp30B niemals NIO werfen - nur IGNORE im NIO-Fall
									// (weil er denkt er sei im ersten durchgang)
									// wir wollen niemals ein NIO-APDM-Eintrag durch den Workaround haben
									setClamp30B(list, s_sgbd_clamp_ecu, i_clamp_timeout, i_clamp_pause, c_Ediabas, "Vorbereitung zum ELV-Unbekannt-Workaround", "preparation for ELV-unknown-workaround", 1, 2, doTrace);
									
									// Wenn hier keine Exception geflogen ist, sind wir in KL30B, Shortcut-Param �berschreiben (nur wenn Workaround an ist)
									if(kl_shortcut)
										s_kl_shortcut_status = CONST_STATUS_KL30B;
								}
								
								// STATUS_ELV_HERSTELLERDATEN_AUSFUEHRUNGSSTATUS-Job ausf�hren (OKAY_ON_ERROR)
								s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_ELV_AKT_4, CONST_RESULT_STAT_ELV_AS, c_Ediabas, doTrace);
								// Ergebnis-Wert auslesen
								s_result_status1 = getDiagResultValueHandleExceptions("STAT_ELV_AUSFUEHRUNGSSTATUS", c_Ediabas, doTrace);
								if(s_ediabas_result.equalsIgnoreCase("OKAY") && s_result_status1.equalsIgnoreCase("0"))
									addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_ELV_AKT_4, CONST_RESULT_STAT_ELV_AS, s_result_status1, "0", "", "", Ergebnis.FT_IO, doTrace);
								else
								{
									if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result "STAT_ELV_AUSFUEHRUNGSSTATUS", da Job-Ausf�hrung "OKAY"
										addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_ELV_AKT_4, CONST_RESULT_STAT_ELV_AS, s_result_status1, "0", "", "", Ergebnis.FT_IGNORE, doTrace);
									else // Fehler bei der Job-Ausf�hrung
										addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_ELV_AKT_4, CONST_RESULT_STD, s_ediabas_result, "OKAY", "", "", Ergebnis.FT_IGNORE, doTrace);
								}
								
								// Status ELV_ENTRIEGELT abfragen
								stopTime = System.currentTimeMillis() + i_elv_timeout;
								do
								{
									s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV, "", CONST_RESULT_STAT_ELV, c_Ediabas, doTrace);
									// ELV-Status auslesen, CONST_STATUS_ELV_ENTR_1/2/3 = ELV entriegelt
									s_result_status = getDiagResultValueHandleExceptions(CONST_RESULT_STAT_ELV, c_Ediabas, doTrace);
									if(s_ediabas_result.equalsIgnoreCase("OKAY") && (s_result_status.equalsIgnoreCase(CONST_STATUS_ELV_ENTR_1) || s_result_status.equalsIgnoreCase(CONST_STATUS_ELV_ENTR_2) || s_result_status.equalsIgnoreCase(CONST_STATUS_ELV_ENTR_3)))
									{
										addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV, "", CONST_RESULT_STAT_ELV, s_result_status, s_elv_values, "", "", Ergebnis.FT_IO, doTrace);
	
										// im IO-Fall warten, da sonst die Klstr. noch von einer verriegelten ELV ausgeht
										wait(i_timeout_pre_kl15);
										
										// ELV braucht nicht mehr entriegelt werden, da Workaround funktioniert hat
										b_temp_elv = false;		
										
										break;
									}
									else if(s_ediabas_result.equalsIgnoreCase("OKAY") && (s_result_status.equalsIgnoreCase(CONST_STATUS_ELV_VER_1) || s_result_status.equalsIgnoreCase(CONST_STATUS_ELV_VER_2) || s_result_status.equalsIgnoreCase(CONST_STATUS_ELV_VER_3)))
									{
										addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV, "", CONST_RESULT_STAT_ELV, s_result_status, s_elv_values, "", "", Ergebnis.FT_IGNORE, doTrace);	
										break;
									}
									else
									{
										if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result CONST_RESULT_STAT_ELV, da Job-Ausf�hrung "OKAY"
											addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV, "", CONST_RESULT_STAT_ELV, s_result_status, s_elv_values, "", "", Ergebnis.FT_IGNORE, doTrace);
										else // Fehler bei der Job-Ausf�hrung
											addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV, "", CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_IGNORE, doTrace);
										
										if(System.currentTimeMillis() <= stopTime)
											wait(i_elv_pause);
									}
								} while(System.currentTimeMillis() <= stopTime);
							}
						}
						catch(Exception e) // wahrscheinlich bei setClamp30B aufgetreten
						{
							// TODO �berdenken
							addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, "ELV-ungueltig-Workaround", "", "", "Fehler im ELV-ungueltig-Workaround", "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_IGNORE, doTrace);
							
							if(doTrace)
								e.printStackTrace(); // Details im Pr�fstand Screenlog ausgeben
						}
					}
					else // Fehler bei der Job-Ausf�hrung
						addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV, "", CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_IGNORE, doTrace);
					
					// ELV per CAS_BROADCAST entriegeln (if-Anweisung wegen ELV-unbekannt-Workaround)
					if(b_temp_elv)
						unlockELV(list, s_sgbd_clamp_ecu, i_clamp_timeout, i_clamp_pause, i_cas_timeout, i_cas_pause, i_elv_timeout, i_elv_pause, c_Ediabas, elv_steuern_extra, kl_shortcut, s_kl_shortcut_status, i_timeout_pre_kl15, step, repeat_max, doTrace);
					
					// Schalte Klemme 15
					setClamp15(list, s_sgbd_clamp_ecu, i_clamp_timeout, i_clamp_pause, c_Ediabas, i_timeout_pre_kl15, step, repeat_max, doTrace);
				}
			}
		}
		catch(PPExecutionException e)
		{
			if(doTrace)
				e.printStackTrace(); // Details im Pr�fstand Screenlog ausgeben
			
			// Fehler in "setClamp15" oder "unlockELV"
			throw new PPExecutionException();
		}
	}
	
	/**
	 * procedure for setting clamp 30B
	 * throws PPExecutionException if clamp 30B can't be set
	 * 
	 * @param list
	 * @param s_sgbd_clamp_ecu
	 * @param i_timeout
	 * @param i_pause
	 * @param c_Ediabas
	 * @param s_nio_tool_de
	 * @param s_nio_tool_en
	 * @param step
	 * @param repeat_max
	 * @param doTrace
	 * @throws PPExecutionException 
	 */
	private void setClamp30B(Vector list, String s_sgbd_clamp_ecu, int i_timeout, int i_pause, EdiabasProxyThread c_Ediabas, String s_nio_tool_de , String s_nio_tool_en, int step, int repeat_max, boolean doTrace) throws PPExecutionException
	{
		String s_ediabas_result = null;
		String s_result_status = null;

		long stopTime = -1;
		long timeout = i_timeout;

		boolean read_clamp_okay = false; // Pr�fvariable f�r Timeout-Schleife

		// in Klemme 30B schalten
		s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_STR_KL, CONST_PAR_STR_KL30B, CONST_RESULT_STD, c_Ediabas, doTrace);
		if(s_ediabas_result.equalsIgnoreCase("OKAY"))
		{
			addDiagResult(list, "Steuern", "actuate", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_KL, CONST_PAR_STR_KL30B, CONST_RESULT_STD, s_ediabas_result, "OKAY", "", "", Ergebnis.FT_IO, doTrace);
			
			// Funktion KL30B_EIN (mit Timeout); nur bei Steuern-IO
			stopTime = System.currentTimeMillis() + timeout;
			do
			{
				// Status KL30B abfragen
				s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, c_Ediabas, doTrace);
				// Klemmenstatus auslesen, CONST_STATUS_KL30B entspricht KL30B
				s_result_status = getDiagResultValueHandleExceptions(CONST_RESULT_STAT_KL, c_Ediabas, doTrace);
				if(s_ediabas_result.equalsIgnoreCase("OKAY") && s_result_status.equalsIgnoreCase(CONST_STATUS_KL30B))
				{
					addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, s_result_status, CONST_STATUS_KL30B, "", "", Ergebnis.FT_IO, doTrace);
					read_clamp_okay = true;
					break;
				}
				else
				{
					// Fehler bei doTrace in APDM ablegen
					if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result CONST_RESULT_STAT_KL, da Job-Ausf�hrung "OKAY"
						addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, s_result_status, CONST_STATUS_KL30B, "", "", Ergebnis.FT_IGNORE, doTrace);
					else // Fehler bei der Job-Ausf�hrung
						addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_IGNORE, doTrace);
					
					if(System.currentTimeMillis() <= stopTime)
						wait(i_pause);
				}
			} while(System.currentTimeMillis() <= stopTime);
			if(read_clamp_okay == false)
			{
				// NIO-Fehler in APDM ablegen
				if(step >= repeat_max) // Wenn wir schon im letzten Durchlauf sind -> NIO ; sonst IGNORE, da es noch einen weiteren Versuch gibt
				{
					if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result CONST_RESULT_STAT_KL, da Job-Ausf�hrung "OKAY"
						addDiagResult(list, "Status", "status", s_nio_tool_de, s_nio_tool_en, s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, s_result_status, CONST_STATUS_KL30B, "KL30B nicht schaltbar", "unable to set clamp 30B", Ergebnis.FT_NIO, doTrace);
					else // Fehler bei der Job-Ausf�hrung
						addDiagResult(list, "DiagFehler", "DiagError", s_nio_tool_de, s_nio_tool_en, s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STD, s_ediabas_result, "OKAY", "KL30B nicht schaltbar", "unable to set clamp 30B", Ergebnis.FT_NIO, doTrace);
				}
				else
				{
					if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result CONST_RESULT_STAT_KL, da Job-Ausf�hrung "OKAY"
						addDiagResult(list, "Status", "status", s_nio_tool_de, s_nio_tool_en, s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, s_result_status, CONST_STATUS_KL30B, "KL30B nicht schaltbar", "unable to set clamp 30B", Ergebnis.FT_IGNORE, doTrace);
					else // Fehler bei der Job-Ausf�hrung
						addDiagResult(list, "DiagFehler", "DiagError", s_nio_tool_de, s_nio_tool_en, s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STD, s_ediabas_result, "OKAY", "KL30B nicht schaltbar", "unable to set clamp 30B", Ergebnis.FT_IGNORE, doTrace);
				}
			
				// Exception werfen = KL30B schalten fehlgeschlagen, 
				throw new PPExecutionException();
			}
		}
		else // Steuern-Job fehlgeschlagen, NIO
		{
			if(step >= repeat_max) // Wenn wir schon im letzten Durchlauf sind -> NIO ; sonst IGNORE, da es noch einen weiteren Versuch gibt
				addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_KL, CONST_PAR_STR_KL30B, CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_NIO, doTrace);
			else
				addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_KL, CONST_PAR_STR_KL30B, CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_IGNORE, doTrace);
			
			// Exception werfen = KL30B schalten fehlgeschlagen, 
			throw new PPExecutionException();
		}
	}
	
	/**
	 * procedure for setting clamp 15
	 * throws PPExecutionException if clamp 15 can't be set
	 * 
	 * @param list
	 * @param s_sgbd_clamp_ecu
	 * @param i_timeout
	 * @param i_pause
	 * @param c_Ediabas
	 * @param i_timeout_pre_kl15
	 * @param step
	 * @param repeat_max
	 * @param doTrace
	 * @throws PPExecutionException 
	 */
	private void setClamp15(Vector list, String s_sgbd_clamp_ecu, int i_timeout, int i_pause, EdiabasProxyThread c_Ediabas, int i_timeout_pre_kl15, int step, int repeat_max, boolean doTrace) throws PPExecutionException
	{
		String s_ediabas_result = null;
		String s_result_status = null;
					
		long stopTime = -1;
		long timeout = i_timeout;

		boolean read_clamp_okay = false; // Pr�fvariable f�r Timeout-Schleife
		
		// in Klemme 15 schalten
		s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_STR_KL, CONST_PAR_STR_KL15, CONST_RESULT_STD, c_Ediabas, doTrace);
		if(s_ediabas_result.equalsIgnoreCase("OKAY"))
		{
			addDiagResult(list, "Steuern", "actuate", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_KL, CONST_PAR_STR_KL15, CONST_RESULT_STD, s_ediabas_result, "OKAY", "", "", Ergebnis.FT_IO, doTrace);
			
			// Funktion KL15_EIN (mit Timeout); nur bei IO-Steuern
			stopTime = System.currentTimeMillis() + timeout;
			do
			{
				// Status KL15 abfragen
				s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, c_Ediabas, doTrace);
				// Klemmenstatus auslesen, CONST_STATUS_KL15 entspricht KL15
				s_result_status = getDiagResultValueHandleExceptions(CONST_RESULT_STAT_KL, c_Ediabas, doTrace);
				if(s_ediabas_result.equalsIgnoreCase("OKAY") && s_result_status.equalsIgnoreCase(CONST_STATUS_KL15))
				{
					addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, s_result_status, CONST_STATUS_KL15, "", "", Ergebnis.FT_IO, doTrace);
					read_clamp_okay = true;
					break;
				}
				else
				{
					// Fehler bei doTrace in APDM ablegen
					if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result CONST_RESULT_STAT_KL, da Job-Ausf�hrung "OKAY"
						addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, s_result_status, CONST_STATUS_KL15, "", "", Ergebnis.FT_IGNORE, doTrace);
					else // Fehler bei der Job-Ausf�hrung
						addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_IGNORE, doTrace);
					
					if(System.currentTimeMillis() <= stopTime)
						wait(i_pause);
				}
			} while(System.currentTimeMillis() <= stopTime);
			if(read_clamp_okay == false)
			{
				if(step >= repeat_max) // Wenn wir schon im letzten Durchlauf sind -> NIO ; sonst IGNORE, da es noch einen weiteren Versuch gibt
				{
					// NIO-Fehler in APDM ablegen
					if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result CONST_RESULT_STAT_KL, da Job-Ausf�hrung "OKAY"
						addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, s_result_status, CONST_STATUS_KL15, "KL15 nicht schaltbar", "unable to set clamp 15", Ergebnis.FT_NIO, doTrace);
					else // Fehler bei der Job-Ausf�hrung
						addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STD, s_ediabas_result, "OKAY", "KL15 nicht schaltbar", "unable to set clamp 15", Ergebnis.FT_NIO, doTrace);
				}
				else
				{
					// IGNORE-Fehler in APDM ablegen
					if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result CONST_RESULT_STAT_KL, da Job-Ausf�hrung "OKAY"
						addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, s_result_status, CONST_STATUS_KL15, "KL15 nicht schaltbar", "unable to set clamp 15", Ergebnis.FT_IGNORE, doTrace);
					else // Fehler bei der Job-Ausf�hrung
						addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STD, s_ediabas_result, "OKAY", "KL15 nicht schaltbar", "unable to set clamp 15", Ergebnis.FT_IGNORE, doTrace);
				}
				
				// Exception werfen = KL15 schalten fehlgeschlagen, 
				throw new PPExecutionException();
			}
		}
		else // Steuern-Job fehlgeschlagen
		{
			addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_KL, CONST_PAR_STR_KL15, CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_IGNORE, doTrace);
			
			// einmalig nach entsprechendem timeout wiederholen
			wait(i_timeout_pre_kl15);
			
			// in Klemme 15 schalten
			s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_STR_KL, CONST_PAR_STR_KL15, CONST_RESULT_STD, c_Ediabas, doTrace);
			if(s_ediabas_result.equalsIgnoreCase("OKAY"))
			{
				addDiagResult(list, "Steuern", "actuate", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_KL, CONST_PAR_STR_KL15, CONST_RESULT_STD, s_ediabas_result, "OKAY", "", "", Ergebnis.FT_IO, doTrace);
				
				// Funktion KL15_EIN (mit Timeout); nur bei IO-Steuern
				stopTime = System.currentTimeMillis() + timeout;
				do
				{
					// Status KL15 abfragen
					s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, c_Ediabas, doTrace);
					// Klemmenstatus auslesen, CONST_STATUS_KL15 entspricht KL15
					s_result_status = getDiagResultValueHandleExceptions(CONST_RESULT_STAT_KL, c_Ediabas, doTrace);
					if(s_ediabas_result.equalsIgnoreCase("OKAY") && s_result_status.equalsIgnoreCase(CONST_STATUS_KL15))
					{
						addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, s_result_status, CONST_STATUS_KL15, "", "", Ergebnis.FT_IO, doTrace);
						read_clamp_okay = true;
						break;
					}
					else
					{
						// Fehler bei doTrace in APDM ablegen
						if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result CONST_RESULT_STAT_KL, da Job-Ausf�hrung "OKAY"
							addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, s_result_status, CONST_STATUS_KL15, "", "", Ergebnis.FT_IGNORE, doTrace);
						else // Fehler bei der Job-Ausf�hrung
							addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_IGNORE, doTrace);
						
						if(System.currentTimeMillis() <= stopTime)
							wait(i_pause);
					}
				} while(System.currentTimeMillis() <= stopTime);
				if(read_clamp_okay == false)
				{
					if(step >= repeat_max) // Wenn wir schon im letzten Durchlauf sind -> NIO ; sonst IGNORE, da es noch einen weiteren Versuch gibt
					{
						// NIO-Fehler in APDM ablegen
						if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result CONST_RESULT_STAT_KL, da Job-Ausf�hrung "OKAY"
							addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, s_result_status, CONST_STATUS_KL15, "KL15 nicht schaltbar", "unable to set clamp 15", Ergebnis.FT_NIO, doTrace);
						else // Fehler bei der Job-Ausf�hrung
							addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STD, s_ediabas_result, "OKAY", "KL15 nicht schaltbar", "unable to set clamp 15", Ergebnis.FT_NIO, doTrace);
					}
					else
					{
						// IGNORE-Fehler in APDM ablegen
						if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result CONST_RESULT_STAT_KL, da Job-Ausf�hrung "OKAY"
							addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STAT_KL, s_result_status, CONST_STATUS_KL15, "KL15 nicht schaltbar", "unable to set clamp 15", Ergebnis.FT_IGNORE, doTrace);
						else // Fehler bei der Job-Ausf�hrung
							addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_LESEN, CONST_PAR_STAT_KL, CONST_RESULT_STD, s_ediabas_result, "OKAY", "KL15 nicht schaltbar", "unable to set clamp 15", Ergebnis.FT_IGNORE, doTrace);
					}
					
					// Exception werfen = KL15 schalten fehlgeschlagen, 
					throw new PPExecutionException();
				}
			}
			else // Steuern-Job fehlgeschlagen NIO
			{
				if(step >= repeat_max) // Wenn wir schon im letzten Durchlauf sind -> NIO ; sonst IGNORE, da es noch einen weiteren Versuch gibt
					addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_KL, CONST_PAR_STR_KL15, CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_NIO, doTrace);
				else
					addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_KL, CONST_PAR_STR_KL15, CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_IGNORE, doTrace);
				
				// Exception werfen = KL15 schalten fehlgeschlagen, 
				throw new PPExecutionException();
			}
		}		
	}

	/**
	 * procedure to unlock the electronic steering column lock
	 * 
	 * @param list
	 * @param s_sgbd_clamp_ecu
	 * @param i_clamp_timeout
	 * @param i_clamp_pause
	 * @param i_cas_timeout
	 * @param i_cas_pause
	 * @param i_elv_timeout
	 * @param i_elv_pause
	 * @param c_Ediabas
	 * @param elv_steuern_extra
	 * @param kl_shortcut
	 * @param s_kl_shortcut_status
	 * @param i_timeout_pre_kl15
	 * @param step
	 * @param repeat_max
	 * @param doTrace
	 * @throws PPExecutionException
	 */
	private void unlockELV(Vector list, String s_sgbd_clamp_ecu, int i_clamp_timeout, int i_clamp_pause, int i_cas_timeout, int i_cas_pause, int i_elv_timeout, int i_elv_pause, EdiabasProxyThread c_Ediabas, boolean elv_steuern_extra, boolean kl_shortcut, String s_kl_shortcut_status, int i_timeout_pre_kl15, int step, int repeat_max, boolean doTrace) throws PPExecutionException
	{
		String s_ediabas_result = null;
		String s_result_status1 = null; // Schl�ssel #1 (und allgemeines Einzelergebnis)
		String s_result_status2 = null; // Schl�ssel #2
		
		int i_result_status1 = 0;
		int i_result_status2 = 0;
		
		//  String erstellen der alle g�ltigen IO-Werte enth�lt, um das Ganze nachher mit addDiagResult vern�nftig in APDM schreiben zu k�nnen
		String s_elv_values = CONST_STATUS_ELV_ENTR_1 + ";" + CONST_STATUS_ELV_ENTR_2 + ";" + CONST_STATUS_ELV_ENTR_3;
		
		// String erstellen der alle g�ltigen IO-Werte enth�lt, um das Ganze nachher mit addDiagResult vern�nftig in APDM schreiben zu k�nnen
		String s_key_values = "0...9";
		
		long stopTime = -1;
		long timeout = i_cas_timeout;

		boolean read_elv_okay = false; // Pr�fvariable f�r Timeout-Schleife (ELV)

		// Klemme 30B schalten
		if(kl_shortcut && s_kl_shortcut_status.equalsIgnoreCase(CONST_STATUS_KL30B))
			; // wir sind bereits in KL30B
		else
			setClamp30B(list, s_sgbd_clamp_ecu, i_clamp_timeout, i_clamp_pause, c_Ediabas, "Vorbereitung zur ELV-Entriegelung", "preparation for unlocking the steering column lock", step, repeat_max, doTrace);
		
		// Hier sind wir in Klemme 30B
		// Steuern CA_Broadcast um ELV zu entriegeln (mit Timeout)
		stopTime = System.currentTimeMillis() + timeout;
		do
		{
			// potError CA_Broadcast-Job mit RRR Ergebnisse abholen (aktueller Job hat nur STR, kann sich in Zukunft �ndern)
			
			// CAS_Broadcast ausf�hren und Ergebnisse pr�fen
			s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_STR_CA_BROADCAST, "", c_Ediabas, doTrace);
			// Werte f�r Schl�ssel #1 und #2 auslesen
			s_result_status1 = getDiagResultValueHandleExceptions("STAT_CA_SCHL_NUM1", c_Ediabas, doTrace);
			s_result_status2 = getDiagResultValueHandleExceptions("STAT_CA_SCHL_NUM2", c_Ediabas, doTrace);
			// Schl�ssel IDs in Integer umwandeln
			try
			{
				i_result_status1 = Integer.parseInt(s_result_status1);
				i_result_status2 = Integer.parseInt(s_result_status2);
			}
			catch(NumberFormatException e)
			{
				addDiagResult(list, "JAVA", "JAVA", "Exception", "Exception", "unlockELV()", "Integer.parseInt()", "", "Key ID1", s_result_status1, s_key_values, "Key ID nicht als Int aufzul�sen", "Key ID cannot be resolved as Int", Ergebnis.FT_IGNORE, doTrace);
				addDiagResult(list, "JAVA", "JAVA", "Exception", "Exception", "unlockELV()", "Integer.parseInt()", "", "Key ID2", s_result_status2, s_key_values, "Key ID nicht als Int aufzul�sen", "Key ID cannot be resolved as Int", Ergebnis.FT_IGNORE, doTrace);
				
				if(doTrace)
					e.printStackTrace(); // Details im Pr�fstand Screenlog ausgeben
				
				// f�hrt zu NIO in folgender Abfrage
				i_result_status1 = -1;
				i_result_status2 = -1;
			}
			
			if(s_ediabas_result.equalsIgnoreCase("OKAY") && (((CONST_KEY_NR_MIN <= i_result_status1) && (i_result_status1 <= CONST_KEY_NR_MAX)) || ((CONST_KEY_NR_MIN <= i_result_status2) && (i_result_status2 <= CONST_KEY_NR_MAX))))
			{
				// nur IO in APDM f�r den Schl�ssel der auch gefunden wurde 
				if(((CONST_KEY_NR_MIN <= i_result_status1) && (i_result_status1 <= CONST_KEY_NR_MAX)))
					addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_STR_CA_BROADCAST, "STAT_CA_SCHL_NUM1", s_result_status1, s_key_values, "", "", Ergebnis.FT_IO, doTrace);
				else
					addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_STR_CA_BROADCAST, "STAT_CA_SCHL_NUM1", s_result_status1, s_key_values, "", "", Ergebnis.FT_IGNORE, doTrace);
				
				if(((CONST_KEY_NR_MIN <= i_result_status2) && (i_result_status2 <= CONST_KEY_NR_MAX)))
					addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_STR_CA_BROADCAST, "STAT_CA_SCHL_NUM2", s_result_status2, s_key_values, "", "", Ergebnis.FT_IO, doTrace);
				else
					addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_STR_CA_BROADCAST, "STAT_CA_SCHL_NUM2", s_result_status2, s_key_values, "", "", Ergebnis.FT_IGNORE, doTrace);
				
				break;
			}
			else
			{
				// Fehler bei doTrace in APDM ablegen
				if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result "STAT_CA_SCHL_NUM#", da Job-Ausf�hrung "OKAY"
				{
					addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_STR_CA_BROADCAST, "STAT_CA_SCHL_NUM1", s_result_status1, s_key_values, "", "", Ergebnis.FT_IGNORE, doTrace);
					addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_STR_CA_BROADCAST, "STAT_CA_SCHL_NUM2", s_result_status2, s_key_values, "", "", Ergebnis.FT_IGNORE, doTrace);
				}
				else // Fehler bei der Job-Ausf�hrung
					addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_STR_CA_BROADCAST, CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_IGNORE, doTrace);
				
				if(System.currentTimeMillis() <= stopTime)
					wait(i_cas_pause);
			}
		} while(System.currentTimeMillis() <= stopTime);
		
		// manuellen ELV-Entriegeln-Job ausf�hren (10sec-Workaround)
		if(elv_steuern_extra)
		{
			// STEUERN_ELV_ENTRIEGELN-Job ausf�hren (OKAY_ON_ERROR)
			s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_ELV_AKT_0, CONST_RESULT_STD, c_Ediabas, doTrace);
			if(s_ediabas_result.equalsIgnoreCase("OKAY"))
				addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_ELV_AKT_0, CONST_RESULT_STD, s_ediabas_result, "OKAY", "", "", Ergebnis.FT_IO, doTrace);
			else // Fehler bei der Job-Ausf�hrung
				addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_ELV_AKT_0, CONST_RESULT_STD, s_ediabas_result, "OKAY", "", "", Ergebnis.FT_IGNORE, doTrace);
		}
		
		// den ELV-Status abfragen, auch wenn Broadcast fehlerhaft
		stopTime = System.currentTimeMillis() + i_elv_timeout;
		do
		{
			s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV, "", CONST_RESULT_STAT_ELV, c_Ediabas, doTrace);
			// ELV-Status auslesen, CONST_STATUS_ELV_ENTR_1/2/3 = ELV entriegelt
			s_result_status1 = getDiagResultValueHandleExceptions(CONST_RESULT_STAT_ELV, c_Ediabas, doTrace);
			if(s_ediabas_result.equalsIgnoreCase("OKAY") && (s_result_status1.equalsIgnoreCase(CONST_STATUS_ELV_ENTR_1) || s_result_status1.equalsIgnoreCase(CONST_STATUS_ELV_ENTR_2) || s_result_status1.equalsIgnoreCase(CONST_STATUS_ELV_ENTR_3)))
			{
				addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV, "", CONST_RESULT_STAT_ELV, s_result_status1, s_elv_values, "", "", Ergebnis.FT_IO, doTrace);
				read_elv_okay = true;
				
				// im IO-Fall warten, da sonst die Klstr. noch von einer verriegelten ELV ausgeht
				wait(i_timeout_pre_kl15);
				
				break;
			}
			else
			{
				// Fehler bei doTrace in APDM ablegen
				if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result CONST_RESULT_STAT_ELV, da Job-Ausf�hrung "OKAY"
					addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV, "", CONST_RESULT_STAT_ELV, s_result_status1, s_elv_values, "", "", Ergebnis.FT_IGNORE, doTrace);
				else // Fehler bei der Job-Ausf�hrung
					addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV, "", CONST_RESULT_STD, s_ediabas_result, "OKAY", PB.getString("diagnosefehler"), PB.getString("diagnosisError"), Ergebnis.FT_IGNORE, doTrace);
				
				if(System.currentTimeMillis() <= stopTime)
					wait(i_elv_pause);
			}
		} while(System.currentTimeMillis() <= stopTime);
		if(read_elv_okay == false)
		{
			// NIO-Ergebnis schreiben
			if(step >= repeat_max) // Wenn wir schon im letzten Durchlauf sind -> NIO ; sonst IGNORE, da es noch einen weiteren Versuch gibt
			{
				if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result CONST_RESULT_STAT_ELV, da Job-Ausf�hrung "OKAY"
					addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV, "", CONST_RESULT_STAT_ELV, s_result_status1, s_elv_values, "Kann ELV nicht entriegeln", "cannot unlock the electronic steering column lock", Ergebnis.FT_NIO, doTrace);
				else // Fehler bei der Job-Ausf�hrung
					addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV, "", CONST_RESULT_STD, s_ediabas_result, "OKAY", "Kann ELV nicht entriegeln", "cannot unlock the electronic steering column lock", Ergebnis.FT_NIO, doTrace);
				
				// Steuern: ErrorLog = STATUS_ELV_HISTORIE_ANALYSE + STATUS_ELV_HERSTELLERDATEN_AUSFUEHRUNGSSTATUS
				// STATUS_ELV_HISTORIE_ANALYSE-Job ausf�hren
				s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV_HIST, "", "", c_Ediabas, doTrace);
				if(s_ediabas_result.equalsIgnoreCase("OKAY"))
					reportAllResults(list, "Analyse", "analysis", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV_HIST, "", "", "", Ergebnis.FT_IO, c_Ediabas, doTrace);
				else // Fehler bei der Job-Ausf�hrung
					addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV_HIST, "", CONST_RESULT_STD, s_ediabas_result, "OKAY", "", "", Ergebnis.FT_IGNORE, doTrace);
				// STATUS_ELV_HERSTELLERDATEN_AUSFUEHRUNGSSTATUS-Job ausf�hren
				s_ediabas_result = executeDiagJobHandleExceptions(s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_ELV_AKT_4, CONST_RESULT_STAT_ELV_AS, c_Ediabas, doTrace);
				// Ergebnis-Wert auslesen
				s_result_status1 = getDiagResultValueHandleExceptions("STAT_ELV_AUSFUEHRUNGSSTATUS", c_Ediabas, doTrace);
				if(s_ediabas_result.equalsIgnoreCase("OKAY") && s_result_status1.equalsIgnoreCase("0"))
					addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_ELV_AKT_4, CONST_RESULT_STAT_ELV_AS, s_result_status1, "0", "", "", Ergebnis.FT_IO, doTrace);
				else
				{
					if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result "STAT_ELV_AUSFUEHRUNGSSTATUS", da Job-Ausf�hrung "OKAY"
						addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_ELV_AKT_4, CONST_RESULT_STAT_ELV_AS, s_result_status1, "0", "", "", Ergebnis.FT_IGNORE, doTrace);
					else // Fehler bei der Job-Ausf�hrung
						addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STR_ROUTINE, CONST_PAR_ELV_AKT_4, CONST_RESULT_STD, s_ediabas_result, "OKAY", "", "", Ergebnis.FT_IGNORE, doTrace);
				}
			}
			else
			{
				if(s_ediabas_result.equalsIgnoreCase("OKAY")) // Fehler bei dem Result CONST_RESULT_STAT_ELV, da Job-Ausf�hrung "OKAY"
					addDiagResult(list, "Status", "status", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV, "", CONST_RESULT_STAT_ELV, s_result_status1, s_elv_values, "Kann ELV nicht entriegeln", "cannot unlock the electronic steering column lock", Ergebnis.FT_IGNORE, doTrace);
				else // Fehler bei der Job-Ausf�hrung
					addDiagResult(list, "DiagFehler", "DiagError", "EDIABAS", "EDIABAS", s_sgbd_clamp_ecu, CONST_JOB_STAT_ELV, "", CONST_RESULT_STD, s_ediabas_result, "OKAY", "Kann ELV nicht entriegeln", "cannot unlock the electronic steering column lock", Ergebnis.FT_IGNORE, doTrace);
			}

			// Exception werfen = ELV kann nicht entriegelt werden 
			throw new PPExecutionException();
		}
	}
	
	/**
	 * procedure to execute an ok_on_error_job and handle ediabas-exceptions
	 * 
	 * @param s_sgbd
	 * @param s_job
	 * @param s_par
	 * @param s_result
	 * @param c_Ediabas
	 * @param doTrace
	 * @return
	 */
	private String executeDiagJobHandleExceptions(String s_sgbd, String s_job, String s_par, String s_result, EdiabasProxyThread c_Ediabas, boolean doTrace)
	{
		String s_ediabas_result = null;
		try
		{
			s_ediabas_result = c_Ediabas.executeDiagJob(s_sgbd, s_job, s_par, s_result);
		}
		catch(ApiCallFailedException e)
		{
			// OK_ON_ERROR, definiertes NIO Ergebnis schreiben
			s_ediabas_result = "ApiCallFailedException";
			if(doTrace)
				e.printStackTrace(); // Details im Pr�fstand Screenlog ausgeben
		}
		catch(EdiabasResultNotFoundException e)
		{
			// OK_ON_ERROR, definiertes NIO Ergebnis schreiben
			s_ediabas_result = "EdiabasResultNotFoundException";
			if(doTrace)
				e.printStackTrace(); // Details im Pr�fstand Screenlog ausgeben
		}
		
		return s_ediabas_result;
	}
	
	/**
	 * procedure to execute an "getDiagResultValue" and handle ediabas-exceptions
	 * 
	 * @param s_result
	 * @param c_Ediabas
	 * @param doTrace
	 * @return
	 */
	private String getDiagResultValueHandleExceptions(String s_result, EdiabasProxyThread c_Ediabas, boolean doTrace)
	{
		String s_result_status = null;
		try
		{
			s_result_status = c_Ediabas.getDiagResultValue(s_result);	
		}
		catch(ApiCallFailedException e)
		{
			// OK_ON_ERROR, definiertes NIO Ergebnis schreiben
			s_result_status = "ApiCallFailedException";
			if(doTrace)
				e.printStackTrace(); // Details im Pr�fstand Screenlog ausgeben
		}
		catch(EdiabasResultNotFoundException e)
		{
			// OK_ON_ERROR, definiertes NIO Ergebnis schreiben
			s_result_status = "EdiabasResultNotFoundException";
			if(doTrace)
				e.printStackTrace(); // Details im Pr�fstand Screenlog ausgeben
		}
		
		return s_result_status;
	}
	
	/**
	 * Waits a period of mseconds
	 * 
	 * @param pause
	 */
	void wait(int pause)
	{
		synchronized(this)
		{
			try
			{
				Thread.sleep(pause);
			}
			catch(InterruptedException e)
			{
				;
			}
		}
	}
	
	/**
	 * 
	 * @param list
	 * @param IdDe
	 * @param IdEn
	 * @param toolDe
	 * @param toolEn
	 * @param s_sgbd
	 * @param s_job
	 * @param s_par
	 * @param s_result
	 * @param s_resultVal
	 * @param s_resultValMin
	 * @param s_errMsgDe
	 * @param s_errMsgEN
	 * @param s_errTyp
	 * @param doTrace
	 */
	private void addDiagResult(Vector list, String IdDe, String IdEn, String toolDe, String toolEn, String s_sgbd, String s_job, String s_par, String s_result, String s_resultVal, String s_resultValMin, String s_errMsgDe, String s_errMsgEn, String s_errTyp, boolean doTrace)	
	{		
		// Ergebnis in APDM schreiben wenn trace gewollt oder kritischer Fehlertyp
		if(doTrace || s_errTyp.equalsIgnoreCase(Ergebnis.FT_NIO) || s_errTyp.equalsIgnoreCase(Ergebnis.FT_NIO_SYS))
		{			
			if(this.isEnglish)
			{
				list.add(new Ergebnis(IdEn, toolEn, s_sgbd, s_job, s_par, s_result, s_resultVal, s_resultValMin, "", "0", "", "", "", s_errMsgEn, "", s_errTyp)); 
			}
			else
			{
				list.add(new Ergebnis(IdDe, toolDe, s_sgbd, s_job, s_par, s_result, s_resultVal, s_resultValMin, "", "0", "", "", "", s_errMsgDe, "", s_errTyp));
			}
		}		
	}
	
	/**
	 * Adds a comment / state to the ErgListe
	 * 
	 * @param list
	 * @param s_de
	 * @param s_en
	 * @param s_result
	 * @param b_resultVal
	 * @param s_errTyp
	 * @param doTrace
	 */
	private void addComment(Vector list, String s_de, String s_en, String s_result, boolean b_resultVal, String s_errTyp, boolean doTrace)	
	{
		if(doTrace)
		{
			String s_resultVal = new Boolean(b_resultVal).toString();
			if(this.isEnglish)
			{
				list.add(new Ergebnis(s_en, "process", "", "", "", s_result, s_resultVal, "", "", "", "", "", "", "", "", s_errTyp)); 
			}
			else
			{
				list.add(new Ergebnis(s_de, "Ablauf", "", "", "", s_result, s_resultVal, "", "", "", "", "", "", "", "", s_errTyp));
			}
		}		
	}

	/**
	 * Adds a comment / state to the ErgListe
	 * 
	 * @param list
	 * @param s_de
	 * @param s_en
	 * @param s_result
	 * @param i_resultVal
	 * @param s_errTyp
	 * @param doTrace
	 */
	private void addComment(Vector list, String s_de, String s_en, String s_result, int i_resultVal, String s_errTyp, boolean doTrace)	
	{
		if(doTrace)
		{
			if(this.isEnglish)
			{
				list.add(new Ergebnis(s_en, "process", "", "", "", s_result, Integer.toString(i_resultVal), "", "", "", "", "", "", "", "", s_errTyp)); 
			}
			else
			{
				list.add(new Ergebnis(s_de, "Ablauf", "", "", "", s_result, Integer.toString(i_resultVal), "", "", "", "", "", "", "", "", s_errTyp));
			}
		}		
	}
	
	/**
	 * Adds a comment / state to the ErgListe
	 * 
	 * @param list
	 * @param s_de
	 * @param s_en
	 * @param s_result
	 * @param s_resultVal
	 * @param s_errTyp
	 * @param doTrace
	 */
	private void addComment(Vector list, String s_de, String s_en, String s_result, String s_resultVal, String s_errTyp, boolean doTrace)	
	{
		if(doTrace)
		{
			if(this.isEnglish)
			{
				list.add(new Ergebnis(s_en, "process", "", "", "", s_result, s_resultVal, "", "", "", "", "", "", "", "", s_errTyp)); 
			}
			else
			{
				list.add(new Ergebnis(s_de, "Ablauf", "", "", "", s_result, s_resultVal, "", "", "", "", "", "", "", "", s_errTyp));
			}
		}		
	}

	/**
	 * Adds a parameter Error to the ErgListe
	 * 
	 * @param list
	 * @param s_para
	 * @param s_content
	 */
	private void addParaError(Vector list, String s_para, String s_content)
	{
		Ergebnis result;

		if(this.isEnglish)	
			result = new Ergebnis("parameterError", "prepare test", "", "", "", "", "", "", "", "", "", "", "", "parameterError", "content of parameter " + "\"" + s_para + "\": " + "\"" + s_content + "\"" + " invalid", Ergebnis.FT_NIO_SYS);
		else
			result = new Ergebnis("Parametrierfehler", "Vorbereitung der Pr�fung", "", "", "", "", "", "", "", "", "", "", "", "Parametrierfehler", "Inhalt von Parameter " + "\"" + s_para + "\": " + "\"" + s_content + "\"" + " ist ung�ltig", Ergebnis.FT_NIO_SYS);
		
		list.add(result);
	}
		
	/**
	 * Resolves an Integer-Argument. An error is added to the ergList (if occured).
	 * 
	 * @param list
	 * @param info
	 * @param s_arg
	 * @param i_defaultVal
	 * @param isRequired
	 * @param doTrace
	 * @return
	 * @throws PPExecutionException 
	 */
	private int resolveInt_timeout(Vector list, ExecutionInfo info, String s_arg, int i_defaultVal, boolean isRequired, boolean doTrace) throws PPExecutionException
	{
		String s_Temp = getArg(s_arg);
		int i_Temp = -1;
		
		try 
		{
			i_Temp = Integer.parseInt(s_Temp);
		} 
		catch(NumberFormatException e) 
		{		
			if(isRequired)
			{
				e.printStackTrace(); // Details im Pr�fstand Screenlog ausgeben
				
				addParaError(list, s_arg, s_Temp);
				throw new PPExecutionException(PB.getString("parameterexistenz"));			
			}
			else
			{
				if(this.isEnglish)
					addComment(list, "Parameter", "parameter", s_arg + " has been set to default-value " + Integer.toString(i_defaultVal), s_Temp, Ergebnis.FT_IO, doTrace);
				else
					addComment(list, "Parameter", "parameter", s_arg + " wurde auf Defaultwert " + Integer.toString(i_defaultVal) + " gesetzt", s_Temp, Ergebnis.FT_IO, doTrace);
				
				i_Temp = i_defaultVal;
			}
		}
	
		return i_Temp;
	}
	
	/**
	 * Resolves a boolean Parameter. An error is added to the ergList (if occured).
	 * 
	 * @param list
	 * @param info
	 * @param s_arg
	 * @param b_defaultVal
	 * @param isRequired
	 * @param doTrace
	 * @return
	 * @throws PPExecutionException 
	 */
	private boolean resolveBoolean(Vector list, ExecutionInfo info, String s_arg, boolean b_defaultVal, boolean isRequired, boolean doTrace) throws PPExecutionException
	{
		String s_Temp = null;
		boolean Erg = false;
		
		if(getArg(s_arg) != null)
			s_Temp = getArg(s_arg).trim();
						
		if((s_Temp != null) && (s_Temp.startsWith("1") || s_Temp.startsWith("ON") || s_Temp.startsWith("EIN") || s_Temp.startsWith("TRUE") || s_Temp.equalsIgnoreCase("T")))
			Erg = true;
		else if((s_Temp != null) && (s_Temp.startsWith("0") || s_Temp.startsWith("OFF") || s_Temp.startsWith("AUS") || s_Temp.startsWith("FALSE") || s_Temp.equalsIgnoreCase("F")))
			Erg = false;
		else if(isRequired)
		{
			addParaError(list, s_arg, s_Temp);
			throw new PPExecutionException(PB.getString("parameterexistenz"));			
		}
		else
		{
			String s_defaultVal = new Boolean(b_defaultVal).toString();
			
			if(this.isEnglish)
				addComment(list, "Parameter", "parameter", s_arg + " has been set to default-value " + s_defaultVal, s_Temp, Ergebnis.FT_IO, doTrace);
			else
				addComment(list, "Parameter", "parameter", s_arg + " wurde auf Defaultwert " + s_defaultVal + " gesetzt", s_Temp, Ergebnis.FT_IO, doTrace);
				
			Erg = b_defaultVal;
		}

		return Erg;
	}
	
	/**
	 * Resolves a Sgbd (String) Parameter. An error is added to the ergList (if occured).
	 * 
	 * @param list
	 * @param info
	 * @param s_arg
	 * @return
	 * @throws PPExecutionException 
	 */
	private String resolveSgbd(Vector list, ExecutionInfo info, String s_arg) throws PPExecutionException
	{
		String s_Temp = null;
		String s_Erg = null;
		
		if(getArg(s_arg) != null)
			s_Temp = getArg(s_arg).trim();

		if(s_Temp != null)
		{
			//Identifiziere Pr�fling
			Pruefling c_Pl = getPr�fling().getAuftrag().getPr�fling(s_Temp);						
			if(c_Pl != null)
			{
				//Hole SGBD aus Pr�fling
				s_Erg = c_Pl.getAttribut("SGBD").trim();
			}
			// Kein g�ltiger Pr�fling. Dann eventuell direkter SGBD-Name.
			else
				s_Erg = s_Temp;
		}
		else
		{
			addParaError(list, s_arg, "NULL");
			throw new PPExecutionException(PB.getString("parameterexistenz"));
		}			

		return s_Erg;
	}
	
	// scrollt �ber alle Ergebniss�tze und schreibt diese in APDM (zB f�r HistorieAnalyse)
	/**
	 * records the results of a Job with multiple record-sets
	 * 
	 * @param list
	 * @param IdDe
	 * @param IdEn
	 * @param toolDe
	 * @param toolEn
	 * @param s_sgbd
	 * @param s_job
	 * @param s_par
	 * @param s_errMsgDe
	 * @param s_errMsgEn
	 * @param s_errTyp
	 * @param c_Ediabas
	 * @param doTrace
	 * 
	 */
	private void reportAllResults(Vector list, String IdDe, String IdEn, String toolDe, String toolEn, String s_sgbd, String s_job, String s_par, String s_errMsgDe, String s_errMsgEn, String s_errTyp, EdiabasProxyThread c_Ediabas, boolean doTrace)
	{
		try
		{
			int res_sets = c_Ediabas.apiResultSets(); // Anzahl Ergebniss�tze

			for(int i = 0; i <= res_sets; i++)
			{
				int res_count = c_Ediabas.apiResultNumber(i); // Anzahl der Ergebniss in diesem Satz
				
				for(int j = 1; j <= res_count; j++)
				{
					String s_resName = c_Ediabas.apiResultName(j, i); // Name des aktuellen Ergebnisses
					String s_resValue = c_Ediabas.getDiagResultValue(i, s_resName); // Wert des aktuellen Ergebnisses

					// Ergebnis in APDM schreiben
					addDiagResult(list, IdDe, IdEn, toolDe, toolEn, s_sgbd, s_job, s_par, s_resName, s_resValue, "", s_errMsgDe, s_errMsgEn, s_errTyp, doTrace);
				}
			}
		}
		catch(ApiCallFailedException e)
		{
			addDiagResult(list, "ExecFehler", "ExecError", "", "", "", "", "", "", "", "", "ApiCallFailedException (" + e.getMessage() + ")" + PB.getString("ausfuehrungsfehler"), "ApiCallFailedException (" + e.getMessage() + ")" + PB.getString("executionError"), Ergebnis.FT_IGNORE, doTrace);
			if(doTrace)
				e.printStackTrace(); // Details im Pr�fstand Screenlog ausgeben
		}
		catch(EdiabasResultNotFoundException e)
		{
			addDiagResult(list, "ExecFehler", "ExecError", "", "", "", "", "", "", "", "", "EdiabasResultNotFoundException (" + e.getMessage() + ")" + PB.getString("ausfuehrungsfehler"), "EdiabasResultNotFoundException (" + e.getMessage() + ")" + PB.getString("executionError"), Ergebnis.FT_IGNORE, doTrace);
			if(doTrace)
				e.printStackTrace(); // Details im Pr�fstand Screenlog ausgeben
		}		
	}
} // End of Class

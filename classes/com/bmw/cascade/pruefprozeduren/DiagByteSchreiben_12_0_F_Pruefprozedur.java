/*
 * DiagByteSchreiben_x_y_F_Pruefprozedur.java
 *
 * Created on 09.08.04 
 * changes 01.04.05  system.out for exceptions. 
 * fg 06.02.13 ausf�hrungsfehler nicht bei io job!! gel�scht
 * 
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;

/**
 * Implementierung der Pr�fprozedur, die Einzelbits in den Pr�fbytes setzen kann.
 * Es werden 3 Bytes pro Steuerger�t f�r die Montage zur Verf�gung gestellt. Somit k�nnen 24 "Stempel" permanent 
 * lokal hinterlegt bzw. abgefragt werden. 
 * Zwingende Argumente : SGBD und mindestens 1 Byte (Pr�ftempel_byte1-3 )(8x 0, 1 oder M)
 * optional weiter Pr�fstempel-Bytes
 *
 * @author Gliss, Buboltz
 * @version Implementierung
 * @version	  6_0_T	  22.08.2016 TB  Erweiterungen f�r Paralleldiagnose analog zur DiagToleranzPP (LOP 2015), Generics eingebaut + Compiler Warnungen behoben<BR>
 * @version 7_0_F 25.01.2017 MKe F-Version
 * @version 8_0_T 10.08.2017 CW optionaler Parameter INITIAL_VALUE hinzugef�gt. Ist dieser Wert gesetzt und das Steuerger�t antwortet mit
 * 								"ERROR_ECU_CONDITIONS_NOT_CORRECT" so wird der Pr�fstempel nochmal auf den Initwert gesetzt bevor der eigentliche
 * 								 Wert nochmal probiert wird zu setzten.
 * @version 9_0_F  11.08.2017 CW BugFix 8_0_T
 * @version 10_0_F 06.09.2017 MKe F-Version
 * @version 11_0_T 05.10.2017 CW Fehlerbewertung angepasst wenn Parameter INITAL_VALUE parametriert ist und der Job Pr�fstempel_Lesen mit
 * 								"ERROR_ECU_CONDITIONS_NOT_CORRECT" fehlschl�gt (Ergebnis wird ausgeblendet)
 * @version 12_0_F 10.10.2017 CW Freigabe 11_0_T
 */
public class DiagByteSchreiben_12_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagByteSchreiben_12_0_F_Pruefprozedur() {
	}

	// Debug flags
	private boolean byteDebug = false; // Byte
	public static final boolean simulationDebug = false; //falls simulation mu� noch gewartet werden bis daten per hand ge�ndert sind

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DiagByteSchreiben_12_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
		super(pruefling, pruefprozName, hasToBeExecuted);
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "BYTE1", "BYTE2", "BYTE3", "DEBUG", "TAG", "INITIAL_VALUE" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "SGBD" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 * Pr�ft ob implements Pr�fstempel nur 1,0,M  enth�lt und mindestens 1 byte gesetzt (byte1, byte2, byte3)
	 **ohne exceptions
	 */

	@Override
	public boolean checkArgs() {

		try {
			if( !checkArgsInternal() )
				return false;
		} catch( Exception e ) {
			if( byteDebug )
				e.printStackTrace( System.out );
			return false;
		}
		return true;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 * Pr�ft ob implements Pr�fstempel nur 1,0,M  enth�lt und mindestens 1 byte gesetzt (byte1, byte2, byte3)
	 */
	private boolean checkArgsInternal() throws PPExecutionException {
		boolean ok;
		boolean byteOk = false;
		String strValue;
		int i;
		int optionalArgLengthNoDebugAndTag = getOptionalArgs().length;

		try {
			ok = super.checkArgs();
			if( ok == true ) {
				//pr�fung ob ein Byte angegeben ist und dessen Inhalt (0,1,M).
				//debugflag abfragen, nach bytes das letzte byte somit length-1
				strValue = getArg( "DEBUG" );
				if( strValue != null ) {
					//Debug is present
					try {
						//sp�ter optionale argumente ohne debugfeld und tag durchsuchen
						optionalArgLengthNoDebugAndTag = getOptionalArgs().length - 3;
						//test debugflag okay
						if( (strValue.toLowerCase().compareTo( "true" ) == 0) || (strValue.toLowerCase().compareTo( "false" ) == 0) ) {
							if( strValue.toLowerCase().compareTo( "true" ) == 0 )
								byteDebug = true;
							else
								byteDebug = false;
							writeDebug( "DiagByteSchreiben.checkArgs.myargs  DEBUG present" );
						} else {
							writeDebug( "DiagByteSchreiben.checkArgs.myargs contains wrong DEBUG (false/true): " + strValue );
							throw new PPExecutionException( "PPExecutionException: " + getPr�fling().getName() + "." + getName() + " :DiagByteSchreiben.checkArgs.myargs contains wrong DEBUG (false/true): " + strValue );
						}
					} catch( PPExecutionException e ) {
						throw new PPExecutionException( "" + e.getMessage() );
					} catch( Exception e ) {
						throw new PPExecutionException( "PPExecutionException: " + getPr�fling().getName() + "." + getName() + " :DiagByteSchreiben.checkArgs:" + e.getMessage() );
					}
				}

				//debug schon abgefragt, nur noch optionale abfrage f�r bytes length -2
				for( i = 0; i < optionalArgLengthNoDebugAndTag; i++ ) {
					try {
						if( getArg( getOptionalArgs()[i] ) != null ) {
							//sobald ein byte geschrieben okay
							byteOk = true;
							//byte �berpr�fen und auf uppercase setzten
							checkMask( getArg( getOptionalArgs()[i] ) );
						}
					} catch( PPExecutionException e ) {
						throw new PPExecutionException( "" + e.getMessage() );
					} catch( Exception e ) {
						throw new PPExecutionException( "PPExecutionException: " + getPr�fling().getName() + "." + getName() + " :DiagByteSchreiben.checkArgs: Exception " + e.getMessage() );
					}
				}
				if( !byteOk ) {
					throw new PPExecutionException( "PPExecutionException: " + getPr�fling().getName() + "." + getName() + " :DiagByteSchreiben.checkArgs: at least 1 Byte have to be written!" );
				}
			}
			if( !ok )
				throw new PPExecutionException( "PPExecutionException: " + getPr�fling().getName() + "." + getName() + " :DiagByteSchreiben.checkArgs: went wrong" );
		} catch( Exception e ) {
			throw new PPExecutionException( "" + e.getMessage() );
		} catch( Throwable e ) {
			throw new PPExecutionException( "" + e.getMessage() );
		}
		return ok;
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String[] pruefbyte = { "BYTE1", "BYTE2", "BYTE3" }; //bytes aus dem Steuerger�t (ist-zustand)
		String[] writeStringPruefbyte = new String[pruefbyte.length]; //bytes aus dem Steuerger�t (neuer-zustand)
		String[] mask = new String[pruefbyte.length]; //�bergabe aus dem pr�fling (soll-zustand)
		String[] pruefbyteAgain = new String[pruefbyte.length]; //zum testen des gegenlesens, und nicht auf alte variable zum sichern soll zustand        
		String pruefbyteName;
		String temp;
		String sgbd = null;
		String job = null;
		String jobpara = null;
		String pruefstempel = "";
		String initialValue = "";

		EdiabasProxyThread ediabas = null;

		//Paralleldiagnose
		boolean parallel = false;
		String tag = null;
		DeviceManager devMan = null;

		//f�r debug simulation
		boolean udInUse = false;

		try {
			//felder initialisieren
			try {
				for( int i = 0; i < mask.length; i++ ) {
					mask[i] = "MMMMMMMM";
				}
			} catch( Exception e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//Parameter p�rfen und variablen setzen
			try {
				checkArgsInternal();
				//               if( checkArgs() == false ) throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				//sgbd einlesen
				sgbd = extractValues( getArg( getRequiredArgs()[0] ) )[0];
				for( int i = 0; i < mask.length; i++ ) {
					if( getArg( getOptionalArgs()[i] ) != null )
						mask[i] = getArg( getOptionalArgs()[i] );
					writeDebug( "DiagByteSchreiben.execute : optionalArgs[" + i + "] = " + mask[i] );
				}
			} catch( Exception e ) {
				System.out.println( " " + e.getMessage() );
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), PB.getString( "parameterexistenz" ), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Paralleldiagnose, Pr�fstandvariabel auslesen, ob Paralleldiagnose 
			try {
				Object pr_var;

				pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
				if( pr_var != null ) {
					if( pr_var instanceof String ) {
						if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
							parallel = true;
						}
					} else if( pr_var instanceof Boolean ) {
						if( ((Boolean) pr_var).booleanValue() ) {
							parallel = true;
						}
					}
				}
			} catch( VariablesException e ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				throw new PPExecutionException();
			}

			// Devicemanager
			devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();

			if( getArg( "TAG" ) != null )
				tag = getArg( "TAG" );

			if( parallel ) // Ediabas holen
			{
				try {
					ediabas = devMan.getEdiabasParallel( tag, sgbd );
				} catch( Exception ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Wrong Cascade configuration for parallel diagnosys", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				} catch( Throwable ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				}
			} else {
				try {
					ediabas = devMan.getEdiabas( tag );
				} catch( Exception ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();

				}
			}
			// <<<<< Paralleldiagnose            

			//lesen aus steuerger�t.
			//Pruefstempel muss gelesen werden
			job = "PRUEFSTEMPEL_LESEN";
			jobpara = "";

			try {
				temp = ediabas.executeDiagJob( sgbd, job, jobpara, "" );
				if( temp.equals( "OKAY" ) == false ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				}
			} catch( Exception e ) {
				if( byteDebug )
					e.printStackTrace( System.out );
				throw e;
			}
			//auslesen der bytes 
			try {
				for( int i = 0; i < pruefbyte.length; i++ ) {
					pruefbyteName = "BYTE" + (i + 1);
					pruefbyte[i] = ediabas.getDiagResultValue( pruefbyteName );
					result = new Ergebnis( "BYTE" + (i + 1), "EDIABAS", sgbd, job, jobpara, "BYTE" + (i + 1), pruefbyte[i], "0x00", "0xFF", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
					writeDebug( "DiagByteSchreiben.execute: reading testbytes from ECU " + pruefbyte[i] );
				}
			} catch( Exception e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException( "" + e.getMessage() );
			}

			//maskieren der bytes
			try {
				writeStringPruefbyte = maskPruefbyte( mask, pruefbyte );
				for( int i = 0; i < writeStringPruefbyte.length; i++ )
					writeDebug( "DiagByteSchreiben.execute:new Bytes: " + writeStringPruefbyte[i] + " old TestBytes: " + pruefbyte[i] + " mask: " + mask[i] );
			} catch( Exception e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//Bytes d�rfen nicht kleiner 0 und gr��er 255 sein sonst fehler!
			try {
				for( int i = 0; i < writeStringPruefbyte.length; i++ )
					if( (Integer.parseInt( writeStringPruefbyte[i] ) < 0) || (Integer.parseInt( writeStringPruefbyte[i] ) > 256) ) {
						writeDebug( "DiagByteSchreiben.execute:Variable <0 bzw. >256 : " + writeStringPruefbyte[i] );
						throw new PPExecutionException( "DiagByteSchreiben.execute:0< " + writeStringPruefbyte[i] + " <256" );

					}
			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//Bytes schreiben in steuerger�t
			job = "PRUEFSTEMPEL_SCHREIBEN";
			jobpara = "";
			for( int i = 0; i < writeStringPruefbyte.length; i++ )
				jobpara = jobpara + writeStringPruefbyte[i] + ";";

			pruefstempel = jobpara; //zwischenspeichern, da sp�ter evtl. nochmal verwendet wird
			writeDebug( "DiagByteSchreiben.execute: jobpara  for testbyte writing to ECU: " + jobpara );
			try {
				temp = ediabas.executeDiagJob( sgbd, job, jobpara, "JOB_STATUS" );
				if( temp.equals( "OKAY" ) == false ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} else {
					result = new Ergebnis( "PS_Setzen", "EDIABAS", sgbd, job, jobpara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO ); //fg 06.02.13 ausf�hrungsfehler nicht bei io job!! gel�scht
					ergListe.add( result );
				}
			} catch( Exception e ) {
				if( byteDebug )
					e.printStackTrace( System.out );
				throw e;
			}

			//test pause zum eintragen des stempels
			if( simulationDebug ) {
				getPr�flingLaufzeitUmgebung().getUserDialog().requestUserMessage( "Simulation : DiagByteSchreiben", "bitte daten in simulationsdatei �ndern ", 0 );
				udInUse = true;
			}

			//gegenlesen, ob richtig geschrieben!
			job = "PRUEFSTEMPEL_LESEN";
			jobpara = "";
			
			//ist ein Initalwert gesetzt?
			if( getArg( "INITIAL_VALUE" ) != null ) {
				initialValue = getArg( "INITIAL_VALUE" );
			}
			
			try {
				temp = ediabas.executeDiagJob( sgbd, job, jobpara, "" );
				if( temp.equals( "OKAY" ) == false ) {
					//bei einem tempor�ren Fehler im Steuerger�t ist der Job_STATUS="ERROR_ECU_CONDITIONS_NOT_CORRECT"
					//ist ein initial-Wert gesetzt und Job_STATUS="ERROR_ECU_CONDITIONS_NOT_CORRECT" wird das Ergebnis mit "Ausgeblendet" bewertet
					if(!initialValue.equalsIgnoreCase("") && temp.equalsIgnoreCase("ERROR_ECU_CONDITIONS_NOT_CORRECT")) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_IGNORE );
					//sonst wird das Ergebnis wie bisher als "Fehler" bewertet
					}else {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					}
					ergListe.add( result );
				
					//bei einem tempor�ren Fehler im Steuerger�t ist der Job_STATUS="ERROR_ECU_CONDITIONS_NOT_CORRECT"
					//ist ein initial-Wert gesetzt und Job_STATUS="ERROR_ECU_CONDITIONS_NOT_CORRECT"
					if(!initialValue.equalsIgnoreCase("") && temp.equalsIgnoreCase("ERROR_ECU_CONDITIONS_NOT_CORRECT")) {
						//Lesen war nicht korrekt
						//Pruefstempel initial bef�llen
						//Initalwert ein byte z.B. "255" - f�r alle 3 bytes benutzt "255;255;255"
						job="PRUEFSTEMPEL_SCHREIBEN";
						jobpara = initialValue + ";" + initialValue + ";" + initialValue;
						temp = ediabas.executeDiagJob( sgbd, job , jobpara , "JOB_STATUS" );
						if( temp.equals( "OKAY" ) == false ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						} else {
							result = new Ergebnis( "PS_Setzen", "EDIABAS", sgbd, job, jobpara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						}
						//konfigurierter Pr�fstempel nochmal schreiben
						temp = ediabas.executeDiagJob( sgbd, job, pruefstempel, "JOB_STATUS" );
						if( temp.equals( "OKAY" ) == false ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, pruefstempel, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						} else {
							result = new Ergebnis( "PS_Setzen", "EDIABAS", sgbd, job, pruefstempel, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO ); 
							ergListe.add( result );
						}
						
						//Nochmal Pr�fstempel lesen
						job = "PRUEFSTEMPEL_LESEN";
						jobpara = "";
						temp = ediabas.executeDiagJob( sgbd, job, jobpara, "" );
						if( temp.equals( "OKAY" ) == false ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						}
					
					} else {
						throw new PPExecutionException();
					}
				}
			} catch( Exception e ) {
				if( byteDebug )
					e.printStackTrace( System.out );
				throw e;
			}

			try {
				//auslesen der bytes 
				for( int i = 0; i < pruefbyteAgain.length; i++ ) {
					pruefbyteName = "BYTE" + (i + 1);
					pruefbyteAgain[i] = ediabas.getDiagResultValue( pruefbyteName );
					result = new Ergebnis( "BYTE" + (i + 1), "EDIABAS", sgbd, job, jobpara, "BYTE" + (i + 1), pruefbyteAgain[i], "0x00", "0xFF", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
					writeDebug( "DiagByteSchreiben.execute:reading the testbytes " + pruefbyteAgain[i] );

					//vergleichen von geschriebenen daten und welche geschrieben werden sollten
					if( pruefbyteAgain[i] != null ) {
						if( (pruefbyteAgain[i]).compareTo( writeStringPruefbyte[i] ) != 0 )
							throw new PPExecutionException( "write or read after write to ecu wrong!: Byte " + (i + 1) + " : read value " + pruefbyteAgain[i] + " write value: " + writeStringPruefbyte[i] );
					} else {
						throw new PPExecutionException( "Pruefbyte read again: PruefByteAgain[ " + i + "] is null!!" );
					}
				}
			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "PS_Setzen", "", "", "", "", "", "", "", "", "0", "", "", PB.getString( "toleranzFehler1" ), "", e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PS_Setzen", "", sgbd, "", "", "", "", "", "", "0", "", "", "", PB.getString( "toleranzFehler1" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( Exception e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "PS_Setzen", "", "", "", "", "", "", "", "", "0", "", "", PB.getString( "toleranzFehler1" ), "", e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PS_Setzen", "", sgbd, "", "", "", "", "", "", "0", "", "", "", PB.getString( "toleranzFehler1" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new Exception();
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
			if( byteDebug )
				e.printStackTrace( System.out );
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			if( byteDebug )
				e.printStackTrace( System.out );
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			if( byteDebug )
				e.printStackTrace( System.out );
		}

		//Devices freigeben
		if( udInUse == true ) {
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				if( byteDebug )
					e.printStackTrace( System.out );
			}
		}

		// Paralleldiagnose
		if( parallel ) {
			try {
				devMan.releaseEdiabasParallel( ediabas, tag, sgbd );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable t ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		//Status setzen
		setPPStatus( info, status, ergListe );

	}

	/**debug funktion Ausgabewerte in PruefstandScreenLog**/
	private void writeDebug( String info ) {
		if( byteDebug )
			System.out.println( "DiagByteSchreiben: " + getPr�fling().getName() + "." + getName() + " " + info );
	}

	/**
	 *maskieren der Bytes aus dem Pr�fling und Ausgabe der neuen byte-felder 
	 *@param Eingabe der zu �bernehmenden Daten (string[]) �bernahme der ECU-Werte (Byte [])
	 *return neue werte f�r die Bytes im ECU (Byte [])   
	 *@throws  PPExecutionException bei einem fehler
	 **/
	private String[] maskPruefbyte( String[] mask, String[] pruefbyte ) throws PPExecutionException {

		int[] intPruefbyte = new int[pruefbyte.length]; //Die stringfelder auf intfelder ge�ndert 
		int[] writeIntPruefbyte = new int[pruefbyte.length]; // neuen werte speichern
		String[] writeStringPruefbyte = new String[pruefbyte.length]; //r�ckgabe der neuen werte als String
		int[] maskBit = { 128, 64, 32, 16, 8, 4, 2, 1 }; //maskierungsfelder f�r die einzelnen bits
		int[] tempBit = new int[maskBit.length]; //neuer tempbyte nach maskierung

		try {
			//Pruefbytes aus ECU in int konvertieren 
			try {
				for( int i = 0; i < pruefbyte.length; i++ ) {
					intPruefbyte[i] = Integer.parseInt( pruefbyte[i] );
					writeDebug( "DiagByteSchreiben.maskArgs: konvertierung von string: " + pruefbyte[i] + " nach int: " + intPruefbyte[i] );
				}
			} catch( NumberFormatException e ) {
				e.printStackTrace( System.out );
				throw new PPExecutionException( e.getMessage() );
			}

			//Bytes einlesen und die Bits der Bytes entsprechend �ndern
			//einlesen der Bytes
			try {
				for( int i = 0; i < pruefbyte.length; i++ ) {
					//belegen des tempbits stelle null ist der h�chste wert !!
					for( int x = 0; x < maskBit.length; x++ ) {
						tempBit[x] = ((intPruefbyte[i] & maskBit[x]) >> ((maskBit.length - 1) - x));
					}
					if( mask[i] != null ) {
						//lesen �ber die Bits des Bytes
						for( int x = 0; x < maskBit.length; x++ ) {
							switch( mask[i].charAt( x ) ) {
								case '0':
									tempBit[x] = 0;
									break;
								case '1':
									tempBit[x] = 1;
									break;
								default:
									tempBit[x] = ((intPruefbyte[i] & maskBit[x]) >> (maskBit.length - 1 - x));
									break;
							}
						}
					}
					//neuen String aufbauen
					for( int x = 0; x < maskBit.length; x++ ) {
						(writeIntPruefbyte[i]) = writeIntPruefbyte[i] + ((tempBit[x]) << (maskBit.length - 1 - x));
					}
					writeDebug( "DiagByteSchreiben.maskArgs: bit schreiben: writeIntEcuArg" + i + " = " + writeIntPruefbyte[i] + " writeStringPruefbyte" + i + " = " + writeIntPruefbyte[i] + " mask" + i + " = " + mask[i] + "intPruefbyte" + i + " = " + intPruefbyte[i] );

					//erworbene integer in string umwandeln
					writeStringPruefbyte[i] = String.valueOf( writeIntPruefbyte[i] );
					writeDebug( "DiagByteSchreiben.maskArgs: konvertierung von int: " + writeIntPruefbyte[i] + " nach String: " + writeStringPruefbyte[i] );

				}
			} catch( Exception e ) {
				e.printStackTrace( System.out );
				throw new PPExecutionException( e.getMessage() );
			}
		} catch( PPExecutionException e ) {
			throw new PPExecutionException( e.getMessage() );
		}

		return writeStringPruefbyte;
	}

	/**
	 * �berpr�ft den �bergebenen sting auf 0,1,M bzw. L�nge 8
	 * @param zu �berpr�fender Byte.
	 * @return false / true.
	 * @throws PPExecutionException bei einem nicht konfromen Byte
	*/
	private boolean checkMask( String mask ) throws PPExecutionException {

		writeDebug( "checkMask: Eingansvariable: " + mask );
		try {
			if( mask.length() == 8 ) {
				for( int i = 0; i < mask.length(); i++ ) {
					if( ((mask.charAt( i ) != '0') && (mask.charAt( i ) != '1') && (mask.charAt( i ) != 'm') && (mask.charAt( i ) != 'M')) ) {
						writeDebug( "DiagByteSchreiben.checkMask: wrong value(0,1,m, M): " + mask );
						throw new PPExecutionException( "PPExecutionException: " + getPr�fling().getName() + "." + getName() + " :DiagByteSchreiben.checkMask:Wrong Mask value(0,1,m, M): " + mask );
					}
				}
			} else {
				writeDebug( "DiagByteSchreiben.checkMask: wrong length (8) : Length is " + mask.length() + ", variable: " + mask );
				throw new PPExecutionException( "PPExecutionException: " + getPr�fling().getName() + "." + getName() + " :DiagByteSchreiben.checkMask:Wrong mask length (8) : Length is " + mask.length() + ", variable: " + mask );
			}
		} catch( PPExecutionException e ) {
			throw new PPExecutionException( "" + e.getMessage() );
		} catch( Exception e ) {
			if( byteDebug )
				e.printStackTrace( System.out );
			throw new PPExecutionException( "" + e.getMessage() );
		}

		return true;

	}

}

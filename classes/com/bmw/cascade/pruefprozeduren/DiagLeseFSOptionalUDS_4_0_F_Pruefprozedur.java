/*
 * DiagLeseFSOptionalUDS_VX_X_X_YY_Pruefprozedur.java
 *
 * Created on 11.09.2017
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import java.util.Map.Entry;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;

/**
 * Implementierung der Pr�fprozedur, die per Diagnose den Fehlerspeicher ausliest und
 * dabei Fehler auf Basis des Fehlerortes ignoriert bzw. weitere Analysen durch Ausf�hrung der
 * Analyse-PPs vornimmt. Sollten nur zu ignorierende Fehler vorliegen, wird mit der �bergebenen
 * Clear-PP das L�schen vorgenommen.
 * @author Borutta, Schumann, Gebauer, K�ser, Buboltz, Wolf, Kemp
 * @version 1_0_F		11.09.17  Mke  Uebernahme der PP DiagLeseFSEinzelUDS_11_0_F_Pruefprozedur 
 * @version 3_0_T		23.01.18  Mke  Bugfix bei der Freigabe der Paralleldiagnose 
 * @version 4_0_F		23.01.18  Mke  F-Version 
 */
public class DiagLeseFSOptionalUDS_4_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	private Vector<Ergebnis> ergListe;
	private DeviceManager devMan;
	private String diagnosetag, sgbd; // MBa, Paralleldiagnose
	private EdiabasProxyThread ediabas;
	private boolean debug;
	private int status;
	private boolean parallelDiagnose = false;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagLeseFSOptionalUDS_4_0_F_Pruefprozedur() {
	}

	/**
	  * erzeugt eine neue Pruefprozedur.
	  * @param pruefling Klasse des zugeh. Pr�flings
	  * @param pruefprozName Name der Pr�fprozedur
	  * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	  */
	public DiagLeseFSOptionalUDS_4_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "JOB", "IGNORE", "ANALYSE_PP", "CLEAR_PP", "FCODE[1..N]", "HWT[1..N]", "DEBUG", "TAG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "SGBD" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	@Override
	public boolean checkArgs() {
		Pruefprozedur p;
		String temp;
		debug = (getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
		int i;

		try {
			//SGBD
			temp = getArg( getRequiredArgs()[0] );
			if( temp.indexOf( ';' ) != -1 )
				return false;
			//Job
			temp = getArg( getOptionalArgs()[0] );
			if( temp != null ) {
				if( temp.indexOf( ';' ) != -1 )
					return false;
			}
			//Ignore
			temp = getArg( getOptionalArgs()[1] );
			try {
				if( temp != null )
					temp = extractErrorNumbers( temp );
			} catch( NumberFormatException e ) {
				return false;
			}
			//Analyse-PPs
			temp = getArg( getOptionalArgs()[2] );
			if( temp != null ) {
				String[] tempArray = splitArg( temp );
				for( i = 0; i < tempArray.length; i++ ) {
					p = getPr�fling().getPr�fprozedur( tempArray[i] );
					if( p == null )
						return false;
					if( p.checkArgs() == false )
						return false;
				}
			}
			//Clear-PP
			temp = getArg( getOptionalArgs()[3] );
			if( temp != null ) {
				p = getPr�fling().getPr�fprozedur( temp );
				if( p == null )
					return false;
				if( p.checkArgs() == false )
					return false;
			}
			return true;
		} catch( Exception e ) {
			if( debug )
				System.out.println( e.getMessage() );
			return false;
		} catch( Throwable e ) {
			if( debug )
				System.out.println( e.getMessage() );
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		ergListe = new Vector<Ergebnis>();
		status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		Pruefprozedur p;
		String temp = null;
		sgbd = null;
		String job;
		String[] jobResults;
		String ignore;
		String analyse;
		String clear;
		int ergS�tze = 0;
		int i, j, k;
		boolean hexCode = false;
		boolean alreadyReleased = false;

		//* Variablen eingef�gt analog zu DiagLeseFSUDS
		String[] analyseNames = new String[0];
		debug = (getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
		ediabas = null;

		//Paralleldiagnose
		diagnosetag = null;
		devMan = null;
		parallelDiagnose = false;

		int FCodeAnzahl; //  Variable fuer die Anzahl angegeben FCodes
		String hwt;
		Hashtable<String, String> allArgs = getArgs();
		List<List<String>> fCodeList = new ArrayList<List<String>>();

		/***********************************************
		  * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
		  * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		 ***********************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		try {
			//Parameter holen  
			try {
				// Parameter Existenz
				if( checkArgs() == false )
					throw new PPExecutionException( "Parameterexistenz" );
				// SGBD
				sgbd = extractValues( getArg( getRequiredArgs()[0] ) )[0];
				if( debug ) {
					System.out.println( "Aktuelle SGBD " + String.valueOf( sgbd ) );
				}
				// Jobname
				job = getArg( getOptionalArgs()[0] );
				if( debug ) {
					System.out.println( "Aktueller Job " + String.valueOf( job ) );
				}
				if( job == null ) {
					job = "FS_LESEN";
					if( debug ) {
						System.out.println( "job wurde FS_LESEN zugewiesen" );
					}
				}

				// Fehler die auszublenden sind
				ignore = getArg( getOptionalArgs()[1] ); //IGNORE
				if( ignore != null ) {
					ignore = extractErrorNumbers( ignore );
					if( debug ) {
						System.out.println( "IGNORE " + String.valueOf( ignore ) );
					}
				}

				// Analyse Pr�fprozedur(en)
				analyse = getArg( getOptionalArgs()[2] );
				if( analyse != null ) {
					analyseNames = splitArg( analyse );
				}

				//alternatives Einlesen der Fehlercodes
				Set<Entry<String, String>> argSet = allArgs.entrySet();
				for( Iterator<Map.Entry<String, String>> it = argSet.iterator(); it.hasNext(); ) {
					Entry<String, String> singleItem = it.next();
					if( singleItem.getKey().startsWith( "FCODE" ) ) {
						fCodeList.add( new ArrayList<String>( Arrays.asList( singleItem.getKey(), singleItem.getValue() ) ) );
						if( debug ) {
							System.out.println( "Aktueller FCode " + singleItem.getKey() );
							System.out.println( "Aktueller FCode Wert " + singleItem.getValue() );
						}
					}
				}
				FCodeAnzahl = fCodeList.size();
				// Einlesen der Anzahl der angegeben Fehler-Codes und Ablage aller Fehler-Codes
				if( debug ) {
					System.out.println( "Anzahl FCODES " + String.valueOf( FCodeAnzahl ) );
				}

				// Job um Fehlerspeicher zu l�schen (falls vorhanden)
				clear = getArg( getOptionalArgs()[3] );

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				if( debug )
					System.out.println( e.getMessage() );
				throw new PPExecutionException();
			}

			try {
				if( debug ) {
					System.out.println( "Ausf�hrung" );
				}

				initEdiabas();

				// EDIABAS Job ausf�hren
				temp = ediabas.executeDiagJob( sgbd, job, "", "" );
				if( temp.equals( "OKAY" ) == false ) { // Es sind Fehler bei der Ausf�hrung aufgetreten
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} else {
					result = new Ergebnis( "Status", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				}

				//Alle Fehlerspeicherinfos speichern mit Status Fehler (F)
				jobResults = new String[2];
				jobResults[0] = "F_ORT_NR";
				jobResults[1] = "F_ORT_TEXT";
				ergS�tze = ediabas.getDiagJobSaetze(); // Anzahl der gefunden Fehler
				String[][] errors = new String[ergS�tze - 2][jobResults.length + 1];

				// Aufsplittung der Ergebnisse  in ein Array
				for( i = 0; i < ergS�tze - 2; i++ ) {
					for( j = 0; j < jobResults.length; j++ ) {
						errors[i][j] = ediabas.getDiagResultValue( i + 1, jobResults[j] );
					}
					errors[i][jobResults.length] = Ergebnis.FT_NIO;
				}

				for( k = 0; k < analyseNames.length; k++ ) {
					p = getPr�fling().getPr�fprozedur( analyseNames[k] );
					//DiagAna greift auf die Attribute zu wenn SGBD/Job im jeweiligen Pr�fschritt nicht parametriert ist.
					p.setAttribut( "ERROR_SGBD", sgbd );
					p.setAttribut( "ERROR_JOB", job );
				}

				// MBa: Paralleldiagnose
				if( parallelDiagnose && ediabas != null && sgbd != null ) {
					try {
						devMan.releaseEdiabasParallel( ediabas, diagnosetag, sgbd );
						alreadyReleased = true;
						if( debug ) {
							System.out.println( "Release SGBD: " + sgbd + " ediabas: " + ediabas.getName() );
						}
					} catch( DeviceLockedException ex ) {
						result = new Ergebnis( "ExecFehler", "EDIABAS", sgbd, "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", ex.getMessage(), Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					} catch( Exception ex ) {
						result = new Ergebnis( "ExecFehler", "EDIABAS", sgbd, "", "", "", "", "", "", "0", "", "", "", "Exception", "Unexpected runtime error at releasing the parallel diagnosis", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					} catch( Throwable ex ) {
						result = new Ergebnis( "ExecFehler", "EDIABAS", sgbd, "", "", "", "", "", "", "0", "", "", "", "Throwable", "Unexpected runtime error at releasing the parallel diagnosis", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				}

				boolean[] errorTags;
				if( ergS�tze > 2 ) {
					// Grunds�tzlich auszublendende Fehler ausblenden
					if( ignore != null ) {
						for( i = 0; i < ergS�tze - 2; i++ ) {
							if( ignore.indexOf( ";" + errors[i][0] + ";" ) >= 0 ) {
								errors[i][jobResults.length] = Ergebnis.FT_IGNORE;
							}
						}
					}

					// Auswertung der parametrierten FCODEs
					for( i = 0; i < ergS�tze - 2; i++ ) {

						String fHexCode = null;
						try {
							fHexCode = ediabas.getDiagResultValue( i + 1, "F_HEX_CODE" );
						} catch( Exception ernfe ) {
							// Nicht alle Steuerger�te unterst�tzen diese Zusatzinformation, also Fehler ignorieren
							fHexCode = null;
						}

						for( j = 0; j < FCodeAnzahl; j++ ) {
							if( errors[i][0].equals( fCodeList.get( j ).get( 1 ).toString() ) ) {
								errorTags = new boolean[errors.length * analyseNames.length];
								for( k = 0; k < analyseNames.length; k++ ) {
									p = getPr�fling().getPr�fprozedur( analyseNames[k] );
									String analyseFort = extractErrorNumbers( p.getArg( "FORT" ) );
									if( analyseFort.contains( fCodeList.get( j ).get( 1 ).toString() ) ) {
										p.setPr�flingLaufzeitUmgebung( getPr�flingLaufzeitUmgebung() ); // Crichton
										p.execute( info );
										if( p.getExecStatus() != STATUS_EXECUTION_OK ) {
											errors[i][jobResults.length] = Ergebnis.FT_NIO;
											errorTags[i] = true;
										} else {
											if( errorTags[i] != true ) {
												errors[i][jobResults.length] = Ergebnis.FT_IGNORE;
											}
										}
									}
								}
								//wenn Analyse PP NIO wird die PP mit NIO bewertet
								if( errors[i][jobResults.length].equalsIgnoreCase( Ergebnis.FT_NIO ) ) {
									//ist ein zugeh�riger HWT zum FCODE vorhanden?
									String singleHWT = "";
									if( allArgs.get( "HWT" + fCodeList.get( j ).get( 0 ).toString().substring( 5 ) ) != null ) {
										singleHWT = allArgs.get( "HWT" + fCodeList.get( j ).get( 0 ).toString().substring( 5 ) ).toString();
									}

									// Existiert Zusatzinformation �ber einen Hex-Fehlercode?
									if( fHexCode != null ) {
										result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", errors[i][0] + " (HEX " + fHexCode + ")", "", "", "0", "", "", "", errors[i][1], singleHWT, errors[i][jobResults.length] );
									} else {
										result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", errors[i][0], "", "", "0", "", "", "", errors[i][1], singleHWT, errors[i][jobResults.length] );
									}
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR; // Job ist fehlgeschlagen
								}
							} else {
								// alle Fehlerspeicher ignorieren die nicht in FCODE parametriert sind
								errors[i][jobResults.length] = Ergebnis.FT_IGNORE;
							}
						}
					}

					//Auswertung aller Fehlercodes im Fehlerspeicher wenn keine FCODEs  parametiert sind
					if( (analyse != null) && (FCodeAnzahl < 1) ) {
						errorTags = new boolean[errors.length * analyseNames.length];
						i = 0;
						while( (i < analyseNames.length) ) {
							p = getPr�fling().getPr�fprozedur( analyseNames[i] );
							ignore = extractErrorNumbers( p.getArg( "FORT" ) );
							boolean codeExistiert = false;
							for( j = 0; j < ergS�tze - 2; j++ ) {
								if( (ignore.indexOf( ";" + errors[j][0] + ";" ) >= 0) )
									codeExistiert = true;
							}
							if( codeExistiert == true ) {
								p.setPr�flingLaufzeitUmgebung( getPr�flingLaufzeitUmgebung() ); // Crichton
								p.execute( info );
								if( p.getExecStatus() != STATUS_EXECUTION_OK ) {
									for( j = 0; j < ergS�tze - 2; j++ ) {
										if( ignore.indexOf( ";" + errors[j][0] + ";" ) >= 0 ) {
											errors[j][jobResults.length] = Ergebnis.FT_NIO;
											errorTags[j] = true;
										}
									}
								} else {
									for( j = 0; j < ergS�tze - 2; j++ ) {
										if( ignore.indexOf( ";" + errors[j][0] + ";" ) >= 0 ) {
											if( errorTags[j] != true ) {
												errors[j][jobResults.length] = Ergebnis.FT_IGNORE;
											}
										}
									}
								}
							}
							i++;
						} //Ende while
					}

					//Doku und Status
					for( i = 0; i < ergS�tze - 2; i++ ) {
						hwt = "";
						result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", errors[i][0], "", "", "0", "", "", "", errors[i][1], hwt, errors[i][jobResults.length] );
						ergListe.add( result );
						//beim FT_NIO  Pruefprozedur status auf NIO setzen und ggf. Hex Code als Ergebnis speichern
						if( errors[i][jobResults.length].equals( Ergebnis.FT_NIO ) == true ) {
							status = STATUS_EXECUTION_ERROR;
							if( hexCode == true ) {
								result = new Ergebnis( "FORT " + errors[i][0], "EDIABAS", sgbd, job, "", "F_HEX_CODE", errors[i][2], "", "", "0", "", "", "", "", "", errors[i][jobResults.length] );
								ergListe.add( result );
							}
						}
					}

					if( (status == STATUS_EXECUTION_OK) && (clear != null) ) {
						p = getPr�fling().getPr�fprozedur( clear );
						p.setPr�flingLaufzeitUmgebung( getPr�flingLaufzeitUmgebung() ); // Crichton
						p.execute( info );
						status = p.getExecStatus(); // Crichton
					}

				} // Ende if (ergS�tze > 2)

			} catch(

			ApiCallFailedException e ) {
				e.printStackTrace();
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// MBa: Paralleldiagnose
		if( parallelDiagnose && ediabas != null && sgbd != null && alreadyReleased == false ) {
			try {
				devMan.releaseEdiabasParallel( ediabas, diagnosetag, sgbd );
				if( debug ) {
					System.out.println( "Release SGBD: " + sgbd + " ediabas: " + ediabas.getName() );
				}
			} catch( DeviceLockedException ex ) {
				result = new Ergebnis( "ExecFehler", "EDIABAS", sgbd, "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", ex.getMessage(), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "EDIABAS", sgbd, "", "", "", "", "", "", "0", "", "", "", "Exception", "Unexpected runtime error at releasing the parallel diagnosis", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable ex ) {
				result = new Ergebnis( "ExecFehler", "EDIABAS", sgbd, "", "", "", "", "", "", "0", "", "", "", "Throwable", "Unexpected runtime error at releasing the parallel diagnosis", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		setPPStatus( info, status, ergListe );
	}

	/**
	 * Hilfsmethode f�r Argument IGNORE: L�st den �bergebenen String auf
	 * @param in der Argument IGNORE
	 * @return vollst�ndige String (zB "1-3" wird als "1;2;3" zur�ckgeliefert)
	 */
	private String extractErrorNumbers( String in ) {
		String sub, subSub;
		String out;
		int[] posArray;
		int counter = 1;
		int min, max;
		int i, j;

		for( i = 0; i < in.length(); i++ ) {
			if( in.charAt( i ) == ';' )
				counter++;
		}

		posArray = new int[counter + 1];
		j = 0;
		posArray[j++] = -1;
		for( i = 0; i < in.length(); i++ ) {
			if( in.charAt( i ) == ';' )
				posArray[j++] = i;
		}
		posArray[j++] = in.length();

		out = ";";
		for( i = 0; i < counter; i++ ) {
			sub = (in.substring( posArray[i] + 1, posArray[i + 1] )).trim();
			if( sub.compareTo( "" ) != 0 ) {
				j = sub.indexOf( "-" );
				if( (j < 1) || (j == (sub.length() - 1)) ) {
					out = out + sub + ";";
				} else {
					subSub = sub.substring( 0, j );
					min = Integer.parseInt( subSub );
					subSub = sub.substring( j + 1, sub.length() );
					max = Integer.parseInt( subSub );
					for( j = min; j <= max; j++ ) {
						out = out + j + ";";
					}
				}
			}
		}
		return out;
	}

	/**
	 * Initialisiert die Ediabas Anbindung f�r das Ausf�hren von Diagnose Jobs
	 * @throws PPExecutionException 
	 */
	private void initEdiabas() throws PPExecutionException {

		Ergebnis result;

		// Devicemanager
		devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();
		if( getArg( "TAG" ) != null )
			diagnosetag = getArg( "TAG" );

		// MBa: Paralleldiagnose >>>>>
		try {
			// Pr�fstandvariabel auslesen, ob Paralleldiagnose
			Object pr_var;
			pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
			if( pr_var != null ) {
				if( pr_var instanceof String ) {
					if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
						parallelDiagnose = true;
					}
				} else if( pr_var instanceof Boolean ) {
					if( ((Boolean) pr_var).booleanValue() ) {
						parallelDiagnose = true;
					}
				}
			}
		} catch( VariablesException ex ) {
			if( debug ) {
				System.out.println( "Variable DIAG_PARALLEL nicht vorhanden: " + ex.getMessage() );
			}
			//diese Exception f�hrt dazu, dass die DiagToleranz mit einer PPExecution Exception abbricht
			//dies ist so gewollt, da wenn in der DiagOpen die Variable "DIAG_PARALLEL" nicht initialisiert wurde
			//die Diagnose nicht korrekt aufgebaut werden konnte und damit eine Diagnose keinen Sinn macht
			throw new PPExecutionException( "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL" );
		}

		try {
			if( parallelDiagnose ) { // Ediabas holen	
				ediabas = devMan.getEdiabasParallel( diagnosetag, sgbd );
			} else {
				ediabas = devMan.getEdiabas( diagnosetag );
			}
			if( debug ) {
				System.out.println( "Init EDIABAS, SGBD: " + sgbd + " ediabas: " + ediabas.getName() );
			}

		} catch( DeviceLockedException ex ) {
			result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", ex.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			throw new PPExecutionException();
		} catch( DeviceNotAvailableException ex ) {
			result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", ex.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			throw new PPExecutionException();
		} catch( Exception ex ) {
			result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			throw new PPExecutionException();
		} catch( Throwable ex ) {
			result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			throw new PPExecutionException();
		}
	}
}

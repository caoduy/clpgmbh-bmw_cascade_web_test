package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.Pruefstand;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.psdz.PSdZ;
import com.bmw.cascade.pruefstand.devices.psdz.PSdZ.KIS_CALCULATION_MODES;
import com.bmw.cascade.pruefstand.devices.psdz.data.*;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.CascadePSdZConditionsNotCorrectException;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.CascadePSdZRuntimeException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementierung einer Pr�fprozedur, die eine Fahrzeugverifikation auf Basis einer TAL durchf�hrt <BR>
 * F�r die Eingrenzung der Ergebnisse kann eine SG-Liste �bergeben werden.<BR>
 * Per default sind alle Aktionen deaktiviert.<BR>
 * <BR>
 * 
 * @author Thomas Buboltz, BMW AG; Dieter Busetti, GEFASOFT AG; Martin Gampl, BMW AG <BR>
 *         <BR>
 * @version 11.03.2007 TB Ersterstellung <BR>
 *          03.04.2007 TB leere Liste als blackList <BR>
 *          25.04.2007 TB zusaetzliche Loggausgaben <BR>
 *          04.05.2007 TB zusaetzlicher TAL Filter eingebaut <BR>
 *          05.05.2007 TB PSdZ 2.1.2.x Auswertung der neuen TA Categorien <BR>
 *          03.01.2008 DB Einbau "testScreenID" in Debug-Ausgaben <BR>
 *          23.01.2008 TB neue optionale Parameter "USE_SVT_IST", "USE_SVT_SOLL" <BR>
 *          03.06.2008 DB �nd. der Anzeige von 'Ist :' zu 'Soll :', Ausblenden der 'TAIS-Nummer' falls null <BR>
 *          24.06.2008 DB �nderung holen des "testScreenID" <BR>
 *          25.06.2008 DB �nd. der Anzeige von 'Soll :' zu 'Ist :' bei Ausbau (UNMOUNT), �nd. Text 'HW falsch' entf�llt <BR>
 *          25.06.2008 DB neue optionale Parameter "VIN_CHECK", "CPS_CHECK", Funktion vorerst noch auskommentiert <BR>
 *          02.07.2008 DB neuer optionaler Parameter "HWT" als Hinweistext <BR>
 *          21.01.2011 AN neuer optionaler Parameter "RESULT_FILTER" zur Filterung der Ergebnisse<BR>
 *          03.05.2011 AN Zweimaliges SVT Generierung bei CPS_CHECK = TRUE unterdrueckt<BR>
 *          24.05.2011 TB Feature f�r weiche CAFD Pr�fung �ber optionales Argument "SOFTCHECK_CAFD" <BR>
 *          05.02.2012 TB LOP Punkt 1436: Neben der Elektroniksachnummer zus�tzlich wenn m�glich noch die bestellbare Sachnummer anzeigen. // DE/ENG Sprachermittlung hinzu<BR>
 *          25.07.2012 TB Testversion: R�ckbau Zweimaliges SVT Generierung bei CPS_CHECK = TRUE unterdrueckt<BR>
 *          31.07.2012 TB F-Version<BR>
 *          13.11.2012 TB Opt. Argument f�r den SAB_CHECK eingebaut (d.h. ob die �berpr�fung nach Systemabnahmeprozess erfolgen soll oder nicht (z.B.bei Vorserienfahrzeugen)), Debug �ber Pr�fstandsvariable<BR>
 *          14.11.2012 TB F-Version (23_0) <BR>
 *          18.07.2013 MG T-Version (25_0) <BR>
 *          ---------- -- - Pr�fung auf falsche Gatewaytabelle eingef�gt <BR>
 *          31.07.2013 MG F-Version (26_0) <BR>
 *          23.09.2013 -- T-Version (27_0) <BR>
 *          ---------- TB LOP 1610 Anforderung: requestSWT Status nur dann ausf�hren, wenn FSC Aktivit�ten �ber den TAL-Filter drin sind od. der Filter leer ist, ACHTUNG: CASCADE Version ab 6.0.0 wird ben�tigt <BR>
 *          ---------- PR Ber�cksichtigung Action-Includes und -Excludes bei der TAL-Filterung <BR>
 *          ---------- TB BEGU Sub-Kategorien beim swDeploy unterstuetzen, Generics <BR>
 *          08.11.2013 MG F-Version (28_0) <BR>
 *          08.04.2014 MG T-Version (29_0) <BR>
 *          ---------- -- - Funktionalit�t zu neuen optionalen Parametern FA_CHECK__VCM und FA_CHECK__VCM_BACKUP eingebaut, Ausbau der deprecated Methoden <BR>
 *          17.04.2014 MG F-Version (30_0) <BR>
 *          11.06.2014 MG T-Version (31_0) <BR>
 *          ---------- -- - LOP Punkt 1789: Als Kurzfristma�nahme wird eine Mehrfachangabe einer Diagnoseadresse in einer Include-/Exclude-Liste bereits in der PP entfernt <BR>
 *          ---------- -- - Codeoptimierungen (Berechung von Negativliste bei Include-Liste und aktivem ForceExecution-Flag entfernt) <BR>
 *          ---------- -- - Debugausgaben zu Actions und Sub-Actions erweitert <BR>
 *          17.06.2014 MG F-Version (32_0) <BR>
 *          19.11.2014 MG T-Version (33_0, 34_0) <BR>
 *          ---------- -- - Funktionalit�t zu neuem optionalen Parameter SOFTCHECK_FSC eingebaut. Hierdurch schl�gt die FSC-Pr�fung nicht fehl, wenn lediglich die VIN im SG nicht zur VIN aus dem Auftrag passt, die erst mit CASCADE 7.0 durchg�ngig in alle FSC-SGs geschrieben wird. <BR>
 *          ---------- -- - Fehlerausgabe zu FSC-Fehlern verbessert. Im Fehlerfall erfolgt nun zus�tzlich die Ausgabe der betroffenen ApplikationIDs. <BR>
 *          ---------- -- - LOP 1853: Ausgabe (in Log-Datei Pr�fstandScreen) der Pr�fprozedurversion sowie s�mtlicher optionaler und nicht optionaler Argumente, die an die Pr�fprozedur �bergeben werden zum Zweck der Nachvollziehbarkeit im Fehlerfall. <BR>
 *          04.12.2014 MG F-Version (35_0) <BR>
 *          25.01.2016 MG T-Version (36_0), Mindestversionsanforderung CASCADE 7.1.0 / PSdZ 5.01.00 <BR>
 *          ---------- -- - Kurzfristma�nhame zu LOP Punkt 1789 wieder entfernt, da Fehlerbehebung in CASCADE-Kern verf�gbar. Mindestversion f�r PP ist CASCADE 7.0.0 <BR>
 *          ---------- -- - LOP 1848: TAIS-Nummer SOLL sowie Bestellnummer f�r falsch verbaute Steuerger�te in Fehlerprotokoll anzeigen, die verl�ngert g�ltig sind (-> HW o.k.) jedoch nicht im Bandablauf programmiert werden. CASCADE-Mindestversionsanforderung CASCADE 7.1.0 <BR>
 *          ---------- -- - LOP 1899: Einheitliche Darstellung der Fehlerausgaben in PSdZ-Pr�fprozeduren <BR>
 *          ---------- -- - LOP 1914: Erweiterung der Fehlerausgabe bei FSC-Fehlern um die Information ob FSCs fehlen, ueberzaelig sind, oder ob die Steuerger�te-VIN nicht zum Auftrag passt. Mindestversion f�r PP ist CASCADE 7.1.0 <BR>
 *          ---------- -- - LOP 1926: Nutzung des neuen TAL-Elements talGenerationReport (verf�gbar ab PSdZ 5.01) zur Erkennung von Fehlern, die bereits bei der TAL-Generierung aufgetreten sind. <BR>
 *          ---------- -- - LOP 1941: FSC-Konsistenzpr�fung bei Unterschieden zwischen KIS und Auftragsdaten. Mindestversion f�r PP ist CASCADE 7.1.0 <BR>
 *          29.01.2016 MG F-Version (37_0), Mindestversionsanforderung CASCADE 7.1.0 / PSdZ 5.01.00 <BR>
 *          14.07.2016 MG T-Version (38_0, 39_0) <BR>
 *          ---------- -- - Ausgabe von fehlenden/�berz�hligen FSC Applikations-Nrn. in Hex- anstatt Dez-Format <BR>
 *          ---------- -- - Ausgabe der Diagnoseadresse in dez-Format nicht notwendig (Aussage R. Kolbeck im FT FzgKonfig vom 02.03.2016); Vereinheitlichung wie bei PP PSdZExecuteTAL <BR>
 *          ---------- -- - LOP 2093: Ausleitung der KIS-Laufzeiten aus PSdZ-Pr�fprozeduren zur KPI-Bestimmung f�r EE-23 Zielvorgabe <BR>
 *          ---------- -- - Verbesserung FSC CHECK (KIS vs. Orderdata): Logausgaben hinzu zur Ausgabe des Inhalts der eingelesenen FSC application ID(s) (KIS/Auftragsdaten) <BR>
 *          25.07.2016 MG F-Version (40_0), Mindestversionsanforderung CASCADE 7.1.0 / PSdZ 5.01.00 <BR>
 *          09.05.2017 MG T-Version (41_0), Mindestversionsanforderung CASCADE 8.1.0 / PSdZ 5.03.00 / sonst ohne LOP 2195 -> CASCADE 8.0.0 / PSdZ 5.02.00 <BR>
 *          ---------- -- - Nachbehandlung zu LOP 2093: <BR>
 *          ---------- -- - - Nutzung des neuen ENUMs KIS_CALCULATION_MODES sowie der neuen Methoden verifyVehicleOnTAL und verifyCPS <BR>
 *          ---------- -- - - PS-Variable PSDZ__KIS_CALCULATION_LEVEL wird vor Methodenaufruf einmalig eingelesen und DIREKT an neue Methoden weitergegeben. -> Prozesssicherheit im Multiinstanzbetr., <BR>
 *          ---------- -- - - da einmal eingelesener Wert so direkt unver�ndert weiterverwendet wird (-> Methodenaufruf, APDM-Ausgabe), auch wenn PS-Variable in der Zwischenzeit durch andere Instanz ver�ndert wurde. <BR>
 *          ---------- -- - LOP 2138: Die TAL-Filterung erfolgt nun bereits im Rahmen der TAL-Generierung und nicht wie bisher erst durch eine nachgelagerte TAL-Filterung nach TAL-Erzeugung mit leerem TAL-Filter <BR>
 *          ---------- -- - LOP 2195: Einf�hrung eines optionalen Parameters START_COLLECT_PIA_META_DATA zur Umsetzung von CR2871/CR2975 (Umstellung des Einsammelns der PIA Daten inittiert durch Tester anstatt wie bisher durch VCM) <BR>
 *          14.09.2017 MG F-Version (42_0), Mindestversionsanforderung CASCADE 8.1.0 / PSdZ 5.03.00 / sonst ohne LOP 2195 -> CASCADE 8.0.0 / PSdZ 5.02.00 <BR>
 */
public class PSdZVerifyVehicleOnTAL_42_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSdZVerifyVehicleOnTAL_42_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSdZVerifyVehicleOnTAL_42_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	// ArrayList f�r den Ergebnisfilter anlegen
	static ArrayList<Integer> ecuResultList = new ArrayList<Integer>();

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "ACTION_LIST", "CHECK_VCM", "CPS_CHECK", "DEBUG", "DEBUG_PERFORM", "EXCLUDE_LIST", "FA_CHECK__VCM", "FA_CHECK__VCM_BACKUP", "FSC_CHECK__KIS_VS_ORDERDATA", "HWT", "INCLUDE_LIST", "RESULT_FILTER", "SAB_CHECK", "SOFTCHECK_CAFD", "SOFTCHECK_FSC", "START_COLLECT_PIA_META_DATA", "USE_SVT_IST", "USE_SVT_SOLL", "VIN_CHECK" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = {};
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		if( !super.checkArgs() )
			return false;
		return true;
	}

	/**
	 * �berpr�ft ob eine SG Adresse in der ecuResultList vorhanden ist
	 * 
	 * @param ECU Adresse.
	 */

	public static boolean EcuAddressInList( int ecuAddress ) {

		boolean erfolgreicheECUSuche = false;
		for( int i = 0; i < ecuResultList.size(); i++ ) {
			if( ecuResultList.get( i ).equals( new Integer( ecuAddress ) ) ) {
				erfolgreicheECUSuche = true;
			}
		}
		if( !erfolgreicheECUSuche ) {
			erfolgreicheECUSuche = false;
		}
		return erfolgreicheECUSuche;
	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * [RETTIG] Pr�fe und modifiziere ein Eingangssarray aus ECU Hex Adressen.<BR>
	 * Ignoriere den '*' als m�glichen Platzhalter, der ebenfalls in der Liste auftauchen kann<BR>
	 * 
	 * @param strInputArray mit ECU SG Adressen in hexadezimaler Form
	 * @return modifiziertes Eingangsarray aus ECU Hex Adressen mit Kleinbuchstaben
	 */
	private String[] checkAndModifyEcuHexArray( String[] strInputArray ) {
		for( int i = 0; i < strInputArray.length; i++ ) {
			if( !strInputArray[i].equalsIgnoreCase( "*" ) )
				strInputArray[i] = Integer.toHexString( new Integer( Integer.parseInt( strInputArray[i], 16 ) ).intValue() ).toLowerCase();
		}

		return strInputArray;
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {

		boolean activeResultfilter = false; // Ergebnisfilterung mit Vorbelegung false (alle Ergebnisse werden ausgegeben)
		// Action array mit 5 Eintr�gen
		// 0 te (hwInstall), 1 te (hwDeinstall), 2 te (blFlash oder swDeploy)
		// 3 te (cdDeploy), 4 te (idBackup, idRestore, idDelete), 5 te (fscDeploy oder fscBackup)
		// 6 te ibaDeploy, 7 te hddUpdate, 8 te gwtbDeploy
		boolean bAction[] = new boolean[9];
		boolean bAddEcuToEcuResultList = false;
		boolean bFullOrderListAlreadyGenerated = false; // vollst�ndige Bestellliste wird im Bedarfsfall nur ein Mal pro Pr�fschrittdurchlauf ermittelt 
		boolean bCheckSWTStatus = false; // gibt an, ob ein requestSWTStatus durchgef�hrt werden soll, oder nicht
		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
		boolean bDebugPerform = true; // gibt an, ob eine Performencemessung erfolgen sollen
		boolean bReportError = false;
		boolean bSABCheck = false; // gibt an, ob die �berpr�fung nach Systemabnahmeprozess erfolgen soll oder nicht (z.B.bei Vorserienfahrzeugen)
		boolean bSoftCheckCAFD = false; // gibt an, ob die CAFD Patch Level Version �berpr�ft wird o. nicht 
		boolean bSoftCheckFSC = false; // gibt an, ob bei Vorliegen einer aussschlie�lich falschen VIN im Steuerger�t bei sonst korrekten FSCs (IST=SOLL) die FSC-Pr�fung fehlschl�gt (false) oder nicht fehlschl�gt (true)
		boolean bStartCollectPiaMetaData = false; // gibt an, ob der PIA-Portierungsmaster nach einer Programmierung, oder Codierung des Fahrzeuges die PIA-Meta-Daten einsammeln soll oder nicht
		boolean checkVCM = true;
		boolean cpsCheck = false;
		boolean faCheckVCM = false; // gibt an, ob der FA aus dem VCM gelesen werden soll oder nicht
		boolean faCheckVCMBackup = false; // gibt an, ob der FA aus dem VCM_Backup gelesen werden soll oder nicht
		boolean faFromVCMEqualsFAFromOrderXML = false; // speichert das Ergebnis des Vergleichs zwischen FA aus VCM und FA aus OrderXML
		boolean faFromVCMBackupEqualsFAFromOrderXML = false; // speichert das Ergebnis des Vergleichs zwischen FA aus VCM_Backup und FA aus OrderXML
		boolean fscCheckKisVsOrderData = false; // gibt an, ob eine Pr�fung auf �bereinstimmung aller FSCs aus der KIS (-> FSC-Tabelle aus Sollverbauung) mit allen FSCs aus dem Auftrag durchgef�hrt werden soll
		boolean useSVTist = false; // SVTIst wird standardmaessig neu ermittelt
		boolean useSVTsoll = false; // SVTSoll wird standardmaessig neu ermittelt
		boolean vinCheck = false;
		char cPlatzhalter = 'n'; // Verwaltung des '*' Platzhalters: 'n'-nichts oder ohne Platzhalter, 'i'-include Liste hat Platzhalter, 'e'-exclude Liste hat Platzhalter
		ecuResultList.clear(); // Liste l�schen
		final boolean DE = checkDE(); // Systemsprache DE wenn true
		// df- funkt. Adr neues Bordnetz; ef-funkt. Adr altes Bordnetz; f1, f4, f5 Testeradressen
		final String[] STAR_IGNORE_LIST = { "df", "ef", "f1", "f4", "f5" }; // IGNORE Liste f�r die Platzhalter-Verwendung, hier k�nnen z.B. die funktionalen ECU Adressen stehen, ACHTUNG: hier z.B. nur "ef" und nicht "EF" eintragen
		final List<String> STAR_IGNORE_LIST_AS_LIST = Arrays.asList( STAR_IGNORE_LIST );
		int status = STATUS_EXECUTION_OK;
		long startTimeStamp = 0;

		// Mittels eines '*' beim ersten Element in der IncludeList oder ExcludeList
		// werden in der jeweils anderen Liste alle ECUs eingetragen ohne die ECUs in der List wo kein Platzhalter steht und ohne die ECUs
		// aus der IGNORE_LIST.
		ArrayList<String> maskList = new ArrayList<String>();
		CascadeFA FA = null;
		Ergebnis result;
		HashMap<CascadeECU, String> piaCollectionResult = null;
		Hashtable<String, String[]> actionIncludeList = new Hashtable<String, String[]>(); //[RETTIG]
		Hashtable<String, String[]> actionExcludeList = new Hashtable<String, String[]>(); //[RETTIG]
		KIS_CALCULATION_MODES enumKISCalculationMode = null;
		List<CascadeLogistischesTeil> listCascadeLogistischesTeil = null;
		List<String> actions = new ArrayList<String>(); // Liste fuer die Actions "swDeploy" etc.
		List<String> subActions = new ArrayList<String>(); // Liste der SubActions "swDeploy | swDeleteTA" etc.
		Map<?, ?> cpsResults = null;
		PSdZ psdz = null;
		String[] actionAndSubActionList = null;
		String[] excludeList = null;
		String[] includeList = null;
		String[] optionalArgumentsNames = getOptionalArgs();
		String[] optionalArgumentsValues = null;
		String[] requiredArgumentsNames = getRequiredArgs();
		String[] requiredArgumentsValues = null;
		String actionText = "";
		String errorCauseFaCheckVCMText = null;
		String errorCauseFaCheckVCMBackupText = null;
		String errorDescription = DE ? " Fehlerbeschreibung: " : " Error Description: ";
		String faFromVCMText = null;
		String faFromVCMBackupText = null;
		String faFromOrderXMLText = null;
		String optionalArgumentName = null;
		String optionalArgumentValue = null;
		String requiredArgumentName = null;
		String requiredArgumentValue = null;
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";
		String strKISCalculationLevel = null;
		String textHWT = "";
		String testScreenID = this.getPr�flingLaufzeitUmgebung().getName(); // TestScreenID (aus Pr�fstandsinstanz)
		String titleBestellTeilTAIS = DE ? "BestellNr: " : "OrderPartNo: "; // TAIS Nr BestellTeil
		String vinResult = null;
		UserDialog userDialog = null;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();

		try {

			// Argumente loggen, einlesen und checken
			try {

				// Ausgabe der Pr�fprozedurversion sowie s�mtlicher an die Pr�fprozedur �bergebener Argumente (required/optional) f�r sp�tere Analysezwecke
				System.out.println( "PSdZ ID:" + testScreenID + ", Initialize " + this.getClassName() );

				for( int i = 0; i < requiredArgumentsNames.length; i++ ) {
					requiredArgumentName = requiredArgumentsNames[i];
					requiredArgumentValue = "";
					// gibt es nicht optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( requiredArgumentName ) != null ) {
						requiredArgumentsValues = extractValues( getArg( requiredArgumentName ) );
						for( int k = 0; k < requiredArgumentsValues.length; k++ ) {
							// Listen von nicht optionalen Argumenten werden durch ";" getrennt ausgegeben
							requiredArgumentValue = requiredArgumentValue + requiredArgumentsValues[k] + ((requiredArgumentsValues.length > 1 && k < (requiredArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						requiredArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZVerifyVehicleOnTAL with required argument: " + requiredArgumentName + " = " + requiredArgumentValue );
				}

				for( int i = 0; i < optionalArgumentsNames.length; i++ ) {
					optionalArgumentName = optionalArgumentsNames[i];
					optionalArgumentValue = "";
					// gibt es optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( optionalArgumentName ) != null ) {
						optionalArgumentsValues = extractValues( getArg( optionalArgumentName ) );
						for( int k = 0; k < optionalArgumentsValues.length; k++ ) {
							// Listen von optionalen Argumenten werden durch ";" getrennt ausgegeben
							optionalArgumentValue = optionalArgumentValue + optionalArgumentsValues[k] + ((optionalArgumentsValues.length > 1 && k < (optionalArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						optionalArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZVerifyVehicleOnTAL with optional argument: " + optionalArgumentName + " = " + optionalArgumentValue );
				}

				// Obligatorische Argumentpr�fung auf Vorhandensein von nicht optionalen sowie �berz�hligen Argumenten
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// ACTION_LIST
				if( getArg( "ACTION_LIST" ) != null ) {
					actionAndSubActionList = extractValues( getArg( "ACTION_LIST" ) );

					// ermittle die Actions und SubActions Listen
					for( String str : Arrays.asList( actionAndSubActionList ) ) {
						int indexOfPipe = str.indexOf( "|" );

						if( indexOfPipe == -1 ) {
							// Die ohne "|" sind die Actions
							actions.add( str.trim() );
						} else {
							// Die mit "|" sind die SubActions im Format "Actions | SubActions"
							subActions.add( str.trim() );
						}
					}
				}

				if( actionAndSubActionList != null ) {
					// Include, Exclude der Actions
					for( int i = 0; i < actionAndSubActionList.length; i++ ) {
						if( getArg( "INCLUDE_LIST" ) != null ) {
							actionIncludeList.put( actionAndSubActionList[i], checkAndModifyEcuHexArray( extractValues( getArg( "INCLUDE_LIST" ) ) ) );
						}
						if( getArg( "EXCLUDE_LIST" ) != null )
							if( getArg( "INCLUDE_LIST" ) != null )
								throw new PPExecutionException( "EXCLUDE_LIST" + (i + 1) + " + INCLUDE_LIST" + (i + 1) + PB.getString( "psdz.parameter.ParameterfehlerGegenseitigerAusschluss" ) );
							else {
								actionExcludeList.put( actionAndSubActionList[i], checkAndModifyEcuHexArray( extractValues( getArg( "EXCLUDE_LIST" ) ) ) );
							}
					}
				}

				// CHECK_VCM
				if( getArg( "CHECK_VCM" ) != null ) {
					if( getArg( "CHECK_VCM" ).equalsIgnoreCase( "TRUE" ) )
						checkVCM = true;
					else if( getArg( "CHECK_VCM" ).equalsIgnoreCase( "FALSE" ) )
						checkVCM = false;
					else
						throw new PPExecutionException( "CHECK_VCM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// CPS_CHECK
				if( getArg( "CPS_CHECK" ) != null ) {
					if( getArg( "CPS_CHECK" ).equalsIgnoreCase( "TRUE" ) )
						cpsCheck = true;
					else if( getArg( "CPS_CHECK" ).equalsIgnoreCase( "FALSE" ) )
						cpsCheck = false;
					else
						throw new PPExecutionException( "CPS_CHECK " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DEBUG
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
					else
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				} else {
					// alternativ DEBUG �ber Pr�fstandsvariable noch auswerten
					try {
						Object objDebug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
						if( objDebug != null ) {
							if( objDebug instanceof String && "TRUE".equalsIgnoreCase( (String) objDebug ) )
								bDebug = true;
							else if( objDebug instanceof Boolean && ((Boolean) objDebug).booleanValue() )
								bDebug = true;
						}
					} catch( VariablesException e ) {
						// Nothing, bDebug ist false vorbelegt
					}
				}

				// DEBUG_PERFORM
				if( getArg( "DEBUG_PERFORM" ) != null ) {
					if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "TRUE" ) )
						bDebugPerform = true;
					else if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "FALSE" ) )
						bDebugPerform = false;
					else
						throw new PPExecutionException( "DEBUG_PERFORM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// FA_CHECK__VCM
				if( getArg( "FA_CHECK__VCM" ) != null ) {
					if( getArg( "FA_CHECK__VCM" ).equalsIgnoreCase( "TRUE" ) )
						faCheckVCM = true;
					else if( getArg( "FA_CHECK__VCM" ).equalsIgnoreCase( "FALSE" ) )
						faCheckVCM = false;
					else
						throw new PPExecutionException( "FA_CHECK__VCM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// FA_CHECK__VCM_BACKUP
				if( getArg( "FA_CHECK__VCM_BACKUP" ) != null ) {
					if( getArg( "FA_CHECK__VCM_BACKUP" ).equalsIgnoreCase( "TRUE" ) )
						faCheckVCMBackup = true;
					else if( getArg( "FA_CHECK__VCM_BACKUP" ).equalsIgnoreCase( "FALSE" ) )
						faCheckVCMBackup = false;
					else
						throw new PPExecutionException( "FA_CHECK__VCM_BACKUP " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// FSC_CHECK__KIS_VS_ORDERDATA
				if( getArg( "FSC_CHECK__KIS_VS_ORDERDATA" ) != null ) {
					if( getArg( "FSC_CHECK__KIS_VS_ORDERDATA" ).equalsIgnoreCase( "TRUE" ) )
						fscCheckKisVsOrderData = true;
					else if( getArg( "FSC_CHECK__KIS_VS_ORDERDATA" ).equalsIgnoreCase( "FALSE" ) )
						fscCheckKisVsOrderData = false;
					else
						throw new PPExecutionException( "FSC_CHECK__KIS_VS_ORDERDATA " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// Hinweistext
				if( getArg( "HWT" ) != null ) {
					textHWT = getArg( "HWT" );
				}

				// INCLUDE_LIST
				if( getArg( "INCLUDE_LIST" ) != null ) {
					includeList = extractValues( getArg( "INCLUDE_LIST" ) );
				}

				// RESULT_FILTER
				if( getArg( "RESULT_FILTER" ) != null ) {
					if( getArg( "RESULT_FILTER" ).equalsIgnoreCase( "TRUE" ) )
						activeResultfilter = true;
					else if( getArg( "RESULT_FILTER" ).equalsIgnoreCase( "FALSE" ) )
						activeResultfilter = false;
					else
						throw new PPExecutionException( "RESULT_FILTER " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// SAB_CHECK
				if( getArg( "SAB_CHECK" ) != null ) {
					if( getArg( "SAB_CHECK" ).equalsIgnoreCase( "TRUE" ) )
						bSABCheck = true;
					else if( getArg( "SAB_CHECK" ).equalsIgnoreCase( "FALSE" ) )
						bSABCheck = false;
					else
						throw new PPExecutionException( "SAB_CHECK " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// SOFTCHECK_CAFD
				if( getArg( "SOFTCHECK_CAFD" ) != null ) {
					if( getArg( "SOFTCHECK_CAFD" ).equalsIgnoreCase( "TRUE" ) )
						bSoftCheckCAFD = true;
					else if( getArg( "SOFTCHECK_CAFD" ).equalsIgnoreCase( "FALSE" ) )
						bSoftCheckCAFD = false;
					else
						throw new PPExecutionException( "SOFTCHECK_CAFD " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// SOFTCHECK_FSC
				if( getArg( "SOFTCHECK_FSC" ) != null ) {
					if( getArg( "SOFTCHECK_FSC" ).equalsIgnoreCase( "TRUE" ) )
						bSoftCheckFSC = true;
					else if( getArg( "SOFTCHECK_FSC" ).equalsIgnoreCase( "FALSE" ) )
						bSoftCheckFSC = false;
					else
						throw new PPExecutionException( "SOFTCHECK_FSC " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// START_COLLECT_PIA_META_DATA
				if( getArg( "START_COLLECT_PIA_META_DATA" ) != null ) {
					if( getArg( "START_COLLECT_PIA_META_DATA" ).equalsIgnoreCase( "TRUE" ) )
						bStartCollectPiaMetaData = true;
					else if( getArg( "START_COLLECT_PIA_META_DATA" ).equalsIgnoreCase( "FALSE" ) )
						bStartCollectPiaMetaData = false;
					else
						throw new PPExecutionException( "SOFTCHECK_FSC " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// USE_SVT_IST
				if( getArg( "USE_SVT_IST" ) != null ) {
					if( getArg( "USE_SVT_IST" ).equalsIgnoreCase( "TRUE" ) )
						useSVTist = true;
					else if( getArg( "USE_SVT_IST" ).equalsIgnoreCase( "FALSE" ) )
						useSVTist = false;
					else
						throw new PPExecutionException( "USE_SVT_IST " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// USE_SVT_SOLL
				if( getArg( "USE_SVT_SOLL" ) != null ) {
					if( getArg( "USE_SVT_SOLL" ).equalsIgnoreCase( "TRUE" ) )
						useSVTsoll = true;
					else if( getArg( "USE_SVT_SOLL" ).equalsIgnoreCase( "FALSE" ) )
						useSVTsoll = false;
					else
						throw new PPExecutionException( "USE_SVT_SOLL " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// EXCLUDE_LIST
				if( getArg( "EXCLUDE_LIST" ) != null ) {
					excludeList = extractValues( getArg( "EXCLUDE_LIST" ) );

					// Darf nur zusammen mit der IncludeList definiert werden, wenn ein '*' Platzhalter in einer Liste ist
					if( getArg( "INCLUDE_LIST" ) != null ) {
						// Kleiner Fehlercheck?
						if( (includeList.length > 1 && excludeList.length > 1) || (includeList[0].equalsIgnoreCase( "*" ) && excludeList[0].equalsIgnoreCase( "*" )) )
							throw new PPExecutionException( PB.getString( "psdz.parameter.ParameterfehlerIncludeExcludeList" ) );

						// includeList mit '*' ?
						if( includeList[0].equalsIgnoreCase( "*" ) )
							cPlatzhalter = 'i';

						// excludeList mit '*' ?
						if( excludeList[0].equalsIgnoreCase( "*" ) )
							cPlatzhalter = 'e';
					}
				}

				// VIN_CHECK
				if( getArg( "VIN_CHECK" ) != null ) {
					if( getArg( "VIN_CHECK" ).equalsIgnoreCase( "TRUE" ) )
						vinCheck = true;
					else if( getArg( "VIN_CHECK" ).equalsIgnoreCase( "FALSE" ) )
						vinCheck = false;
					else
						throw new PPExecutionException( "VIN_CHECK " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Performancemessung Start
			if( bDebugPerform )
				System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL PP START" );

			// Devices holen
			try {
				psdz = getPr�flingLaufzeitUmgebung().getDeviceManager().getPSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			try {
				userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// requestSWTStatus soll dann belegt werden, wenn FSC Aktivit�ten bef�llt sind oder wenn keine Actions gesetzt sind
			if( actionAndSubActionList != null && actionAndSubActionList.length != 0 ) {
				for( String strAction : actionAndSubActionList ) {
					if( strAction.equalsIgnoreCase( CascadeTACategories.FSC_BACKUP.toString() ) || strAction.equalsIgnoreCase( CascadeTACategories.FSC_DEPLOY.toString() ) ) {
						bCheckSWTStatus = true;
						break;
					}
				}
			} else {
				bCheckSWTStatus = true;
			}

			// setzen von tempor�ren Variablen zur Feinsteuerung des PSdZ
			try {
				// setze tempor�re Variable f�r das Ignorieren der CAFD Patch Level Pr�fung
				this.getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_IGNORE_CAFD_PATCH_LEVEL", new Boolean( bSoftCheckCAFD ) );
				// setze tempor�re Variable f�r das zus�tzliche Ausf�hren des SAB Checks
				this.getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_SAB_CHECK", new Boolean( bSABCheck ) );
			} catch( Exception e ) {
				// Nothing
			}

			// Verifiziere das Gesamtfahrzeug auf Basis der TAL
			userDialog.setDisplayProgress( true );
			userDialog.getProgressbar().setIndeterminate( true );
			userDialog.displayMessage( PB.getString( "psdz.ud.VerifikationTal" ), "VIN-Check: " + vinCheck + ";CPS-Check: " + cpsCheck + ";FA-Check: " + (faCheckVCM || faCheckVCMBackup) + ";Check VCM: " + checkVCM + ";Result Filter: " + activeResultfilter + ";" + PB.getString( "psdz.ud.VerifikationLaeuft" ), -1 );

			// Berechnung der ECU Maske, bei Verwendung eines Platzhalters '*' bei der IncludeList oder der ExcludeList
			switch( cPlatzhalter ) {
				// include Liste mit '*'
				case 'i':
					List<String> excludeListAsList = Arrays.asList( excludeList );

					for( int i = 0; i < 256; i++ ) {
						if( !excludeListAsList.contains( Integer.toHexString( i ) ) && !excludeListAsList.contains( Integer.toHexString( i ).toUpperCase() ) && !STAR_IGNORE_LIST_AS_LIST.contains( Integer.toHexString( i ) ) )
							maskList.add( Integer.toHexString( i ) );
					}
					includeList = (String[]) maskList.toArray( new String[maskList.size()] );
					excludeList = null;

					if( bDebug )
						System.out.println( "PSdZ ID:" + testScreenID + ", MaskList bei IncludeList: " + maskList.toString() );

					break;

				// exclude Liste mit '*'
				case 'e':
					List<String> includeListAsList = Arrays.asList( includeList );

					for( int i = 0; i < 256; i++ ) {
						if( !includeListAsList.contains( Integer.toHexString( i ) ) && !includeListAsList.contains( Integer.toHexString( i ).toUpperCase() ) && !STAR_IGNORE_LIST_AS_LIST.contains( Integer.toHexString( i ) ) )
							maskList.add( Integer.toHexString( i ) );
					}
					excludeList = (String[]) maskList.toArray( new String[maskList.size()] );
					includeList = null;

					if( bDebug )
						System.out.println( "PSdZ ID:" + testScreenID + ", MaskList bei ExcludeList: " + maskList.toString() );

					break;

				// tue nichts...
				default:
			}

			try {

				// Ermitteln des zuletzt gesetzen KIS-Modus wie er auch im PSdZ-Device genutzt wird
				// 'PSDZ__KIS_CALCULATION_LEVEL' Typ: string; Werte: MONTAGEFORTSCHRITT, EINZELFLASH, GESAMTFLASH oder VORSCHAUberechnung; default: MONTAGEFORTSCHRITT; 
				try {
					strKISCalculationLevel = (String) Pruefstand.getInstance().getPruefstandVariable( VariableModes.PS_CONFIG, "PSDZ__KIS_CALCULATION_LEVEL" );

					switch( strKISCalculationLevel.toUpperCase().charAt( 0 ) ) {
						// M == MONTAGEFORTSCHRITT
						case 'M':
							enumKISCalculationMode = KIS_CALCULATION_MODES.MONTAGEFORTSCHRITT;
							break;
						// E == EINZELFLASH
						case 'E':
							enumKISCalculationMode = KIS_CALCULATION_MODES.EINZELFLASH;
							break;
						// G == GESAMTFLASH
						case 'G':
							enumKISCalculationMode = KIS_CALCULATION_MODES.GESAMTFLASH;
							break;
						// V == VORSCHAU
						case 'V':
							enumKISCalculationMode = KIS_CALCULATION_MODES.VORSCHAU;
							break;

						// default ist MONTAGEFORTSCHRITT
						default:
							enumKISCalculationMode = KIS_CALCULATION_MODES.MONTAGEFORTSCHRITT;
					}
				} catch( Exception e ) {
					enumKISCalculationMode = KIS_CALCULATION_MODES.MONTAGEFORTSCHRITT;
				}

				// LOP 2138: Die TAL-Filterung erfolgt nun bereits im Rahmen der TAL-Generierung und nicht wie bisher erst durch eine zus�tzliche TAL-Filterung nach TAL-Erzeugung mit leerem TAL-Filter. 
				// Hintergrund: Sh. LOP 2133 (Ausblenden der PSdZ Fehler-IDs 1727 und 2030 aus TALGenerationReport)
				CascadeTALFilter myTALFilter = null;

				// Zusammenbauen des TAL-Filters, nat�rlich nur, wenn es was zum filtern gibt ;-)
				if( actionAndSubActionList != null && (includeList != null || excludeList != null) ) {
					myTALFilter = new CascadeTALFilter();
					Object[] allCategories = CascadeTACategories.getAllCategories().toArray();

					// nur wenn ACTIONS und/oder SUBACTIONS �bergeben werden...
					// Hintergrund: Wird nur eine SUBACTION �ber die PP �bergeben, m�ssen s�mtliche Kategorien dennoch durchlaufen werden, damit der TAL-Filter korrekt bef�llt wird. Andernfalls w�rden f�r den Fall, dass nur SUBACTIONS �bergeben werden
					// f�r die restlichen ACTIONS die TA-Kategorien im TAL-Filter nicht bef�llt werden (-> empty) und so diese ACTIONS erlaubt werden, obwohl diese ausgefiltert werden m�ssen. 
					if( actions.size() > 0 || subActions.size() > 0 ) {
						for( int i = 0; i < allCategories.length; i++ ) {

							if( bDebug )
								System.out.println( "PSdZ ID:" + testScreenID + ", PSdZVerifyVehicleOnTAL: Categories Loop Counter: " + i + ", Category: " + ((CascadeTACategories) allCategories[i]).toString() );
							CascadeFilteredTACategory myCategory = new CascadeFilteredTACategory( (CascadeTACategories) allCategories[i] );
							// Treffer in der Liste der Actions?
							boolean filteredActionFound = false;

							for( int j = 0; j < actionAndSubActionList.length; j++ ) {
								if( allCategories[i].toString().equalsIgnoreCase( actionAndSubActionList[j] ) ) {
									filteredActionFound = true;
									// Gibt es zu der Category eine Include-Liste?
									if( actionIncludeList.get( actionAndSubActionList[j] ) != null ) {
										// Manual Parameter hier entfernt, weil es gibt die zus�tzlichen PP f�r codieren / programmieren
										if( bDebug ) {
											StringBuffer sbECUAdresses = new StringBuffer();
											for( int iCount = 0; iCount < ((String[]) actionIncludeList.get( actionAndSubActionList[j] )).length; iCount++ ) {
												sbECUAdresses.append( ((String[]) actionIncludeList.get( actionAndSubActionList[j] ))[iCount] + " " );
											}
											System.out.println( "PSdZ ID:" + testScreenID + ", PSdZVerifyVehicleOnTAL: Action: " + actionAndSubActionList[j] + ", ECU count: " + ((String[]) actionIncludeList.get( actionAndSubActionList[j] )).length + ", ECU HEX addresses included ( 0x " + sbECUAdresses.toString().toUpperCase() + "), ForceExecution-Flag: " + myCategory.isForceExecution() );
										}
										myCategory.addEcusToList( (String[]) actionIncludeList.get( actionAndSubActionList[j] ) );
										myCategory.setBlacklist( false );

									} else if( actionExcludeList.get( actionAndSubActionList[j] ) != null ) {
										if( bDebug ) {
											StringBuffer sbECUAdresses = new StringBuffer();
											for( int iCount = 0; iCount < ((String[]) actionExcludeList.get( actionAndSubActionList[j] )).length; iCount++ ) {
												sbECUAdresses.append( ((String[]) actionExcludeList.get( actionAndSubActionList[j] ))[iCount] + " " );
											}
											System.out.println( "PSdZ ID:" + testScreenID + ", PSdZVerifyVehicleOnTAL: Action: " + actionAndSubActionList[j] + ", ECU count: " + ((String[]) actionExcludeList.get( actionAndSubActionList[j] )).length + ", ECU HEX addresses excluded ( 0x " + sbECUAdresses.toString().toUpperCase() + "), ForceExecution-Flag: " + myCategory.isForceExecution() );
										}
										myCategory.addEcusToList( (String[]) actionExcludeList.get( actionAndSubActionList[j] ) );
										myCategory.setBlacklist( true );
									} else {
										if( bDebug ) {
											System.out.println( "PSdZ ID:" + testScreenID + ", PSdZVerifyVehicleOnTAL: Action: " + actionAndSubActionList[j] + ", ECU count: 256" + ", all possible ECU HEX addresses are included, ForceExecution-Flag: " + myCategory.isForceExecution() );
										}
										myCategory.setAffectAllEcus( true );
										myCategory.setBlacklist( false );
									}
								}
							}
							if( !filteredActionFound ) {
								myCategory.setAffectAllEcus( true );
								myCategory.setBlacklist( true );
							}

							myTALFilter.addFilteredTACategory( myCategory );

						}
					}

					// SUB-ACTION "swDeploy" Behandlung. Aktuell nur relevant bzgl. BEGU, d.h. "swDeploy" kennt Subkategorien z.B. in der Form "swDeploy | swDeleteTA"
					for( int i = 0; i < subActions.size(); i++ ) {
						if( bDebug )
							System.out.println( "PSdZ ID:" + testScreenID + ", PSdZVerifyVehicleOnTAL: Sub-Categories Loop Counter: " + i + ", Sub-Category: " + subActions.get( i ) );

						// Die Sub-Action hat z.B. das Format: "swDeploy | swDeleteTA", ermittle parentName und subName und erzeuge eine neue CascadeFilteredTASubCategory
						int indexOfPipe = subActions.get( i ).indexOf( "|" );
						//String subActionParentName = subActions.get( i ).substring( 0, indexOfPipe ).trim();
						String subActionSubName = subActions.get( i ).substring( indexOfPipe + 1 ).trim();

						CascadeFilteredTASubCategory mySubCategory = new CascadeFilteredTASubCategory( CascadeTACategories.SW_DEPLOY, subActionSubName );

						// Treffer in der Liste der SubActions?
						boolean filteredSubActionFound = false;

						// die Liste der gesamten Actions durchgehen und die Teildaten des CascadeFilteredTASubCategory Objektes bei Treffern bef�llen
						for( int j = 0; j < actionAndSubActionList.length; j++ ) {
							// Die Action beinhaltet keine Subaction z.B. "swDeploy | swDeleteTA", dann mache mit der n�chsten Action weiter
							if( actionAndSubActionList[j].indexOf( "|" ) == -1 ) {
								continue;
							}

							// analysiere die Actions
							if( subActions.get( i ).equalsIgnoreCase( actionAndSubActionList[j] ) ) {
								filteredSubActionFound = true;

								// Gibt es zu der Sub-Category eine Include-Liste?
								if( actionIncludeList.get( actionAndSubActionList[j] ) != null ) {
									if( bDebug ) {
										StringBuffer sbECUAdresses = new StringBuffer();
										for( int iCount = 0; iCount < ((String[]) actionIncludeList.get( actionAndSubActionList[j] )).length; iCount++ ) {
											sbECUAdresses.append( ((String[]) actionIncludeList.get( actionAndSubActionList[j] ))[iCount] + " " );
										}
										System.out.println( "PSdZ ID:" + testScreenID + ", PSdZVerifyVehicleOnTAL: Sub-Action: " + actionAndSubActionList[j] + ", ECU count: " + ((String[]) actionIncludeList.get( actionAndSubActionList[j] )).length + ", ECU HEX addresses included ( 0x " + sbECUAdresses.toString().toUpperCase() + "), ForceExecution-Flag: " + mySubCategory.isForceExecution() );
									}
									mySubCategory.addEcusToList( (String[]) actionIncludeList.get( actionAndSubActionList[j] ) );
									mySubCategory.setBlacklist( false );

								} else if( actionExcludeList.get( actionAndSubActionList[j] ) != null ) {
									if( bDebug ) {
										StringBuffer sbECUAdresses = new StringBuffer();
										for( int iCount = 0; iCount < ((String[]) actionExcludeList.get( actionAndSubActionList[j] )).length; iCount++ ) {
											sbECUAdresses.append( ((String[]) actionExcludeList.get( actionAndSubActionList[j] ))[iCount] + " " );
										}
										System.out.println( "PSdZ ID:" + testScreenID + ", PSdZVerifyVehicleOnTAL: Sub-Action: " + actionAndSubActionList[j] + ", ECU count: " + ((String[]) actionExcludeList.get( actionAndSubActionList[j] )).length + ", ECU HEX addresses excluded ( 0x " + sbECUAdresses.toString().toUpperCase() + "), ForceExecution-Flag: " + mySubCategory.isForceExecution() );
									}
									mySubCategory.addEcusToList( (String[]) actionExcludeList.get( actionAndSubActionList[j] ) );
									mySubCategory.setBlacklist( true );
								} else {
									if( bDebug ) {
										System.out.println( "PSdZ ID:" + testScreenID + ", PSdZVerifyVehicleOnTAL: Sub-Action: " + actionAndSubActionList[j] + ", ECU count: 256" + ", all possible ECU HEX addresses are included, ForceExecution-Flag: " + mySubCategory.isForceExecution() );
									}
									mySubCategory.setAffectAllEcus( true );
									mySubCategory.setBlacklist( false );
								}
							}
						}
						if( !filteredSubActionFound ) {
							mySubCategory.setAffectAllEcus( true );
							mySubCategory.setBlacklist( true );
						}

						myTALFilter.addFilteredTASwDeploySubCategory( mySubCategory );
					}
				}

				CascadeTAL myTALResult = null;

				// Startzeit
				startTimeStamp = System.currentTimeMillis();

				if( includeList != null ) {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.verifyVehicleOnTAL' START" );
					}

					myTALResult = psdz.verifyVehicleOnTAL( useSVTist, useSVTsoll, myTALFilter, checkVCM, includeList, false, bCheckSWTStatus, enumKISCalculationMode ).getCascadeTAL();

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.verifyVehicleOnTAL' ENDE" );
					}

					// Nur dann Zeiten loggen, wenn auch tats�chlich eine KIS-Abfrage erfolgt
					if( !useSVTsoll ) {
						result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", "Test procedure method name for KIS call:=verifyVehicleOnTAL", "KIS calculation mode set by CASCADE:=" + enumKISCalculationMode, "", "Duration of method call [msec]", Long.toString( System.currentTimeMillis() - startTimeStamp ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}

				} else if( excludeList != null ) {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.verifyVehicleOnTAL' START" );
					}

					myTALResult = psdz.verifyVehicleOnTAL( useSVTist, useSVTsoll, myTALFilter, checkVCM, excludeList, true, bCheckSWTStatus, enumKISCalculationMode ).getCascadeTAL();

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.verifyVehicleOnTAL' ENDE" );
					}

					// Nur dann Zeiten loggen, wenn auch tats�chlich eine KIS-Abfrage erfolgt
					if( !useSVTsoll ) {
						result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", "Test procedure method name for KIS call:=verifyVehicleOnTAL", "KIS calculation mode set by CASCADE:=" + enumKISCalculationMode, "", "Duration of method call [msec]", Long.toString( System.currentTimeMillis() - startTimeStamp ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}

				} else {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.verifyVehicleOnTAL' START" );
					}

					// �bergebe leere Liste -> damit wir alles verifizieren k�nnen
					String[] emptyList = {};
					myTALResult = psdz.verifyVehicleOnTAL( useSVTist, useSVTsoll, myTALFilter, checkVCM, emptyList, true, bCheckSWTStatus, enumKISCalculationMode ).getCascadeTAL();

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.verifyVehicleOnTAL' ENDE" );
					}

					// Nur dann Zeiten loggen, wenn auch tats�chlich eine KIS-Abfrage erfolgt
					if( !useSVTsoll ) {
						result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", "Test procedure method name for KIS call:=verifyVehicleOnTAL", "KIS calculation mode set by CASCADE:=" + enumKISCalculationMode, "", "Duration of method call [msec]", Long.toString( System.currentTimeMillis() - startTimeStamp ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}

				}

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.getTAL' START" );
				}

				// Jetzt die neue gefilterte TAL weiterverwenden
				myTALResult = psdz.getTAL();

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.getTAL' ENDE" );
				}

				// Analyse der Actions
				// KIS 'keep it simple' Bef�llung des bAction Arrays
				if( actionAndSubActionList != null ) {
					for( int i = 0; i < actionAndSubActionList.length; i++ ) {
						if( actionAndSubActionList[i].equalsIgnoreCase( CascadeTACategories.HW_INSTALL.toString() ) )
							bAction[0] = true;
						if( actionAndSubActionList[i].equalsIgnoreCase( CascadeTACategories.HW_DEINSTALL.toString() ) )
							bAction[1] = true;
						if( actionAndSubActionList[i].equalsIgnoreCase( CascadeTACategories.BL_FLASH.toString() ) || actionAndSubActionList[i].equalsIgnoreCase( CascadeTACategories.SW_DEPLOY.toString() ) || actionAndSubActionList[i].equalsIgnoreCase( CascadeTACategories.SW_DEPLOY.toString() + " | " + CascadeTASubCategories.SWDeployTA.toString() ) || actionAndSubActionList[i].equalsIgnoreCase( CascadeTACategories.SW_DEPLOY.toString() + " | " + CascadeTASubCategories.SWDeleteTA.toString() ) )
							bAction[2] = true;
						if( actionAndSubActionList[i].equalsIgnoreCase( CascadeTACategories.CD_DEPLOY.toString() ) )
							bAction[3] = true;
						if( actionAndSubActionList[i].equalsIgnoreCase( CascadeTACategories.ID_BACKUP.toString() ) || actionAndSubActionList[i].equalsIgnoreCase( CascadeTACategories.ID_DELETE.toString() ) || actionAndSubActionList[i].equalsIgnoreCase( CascadeTACategories.ID_RESTORE.toString() ) )
							bAction[4] = true;
						if( actionAndSubActionList[i].equalsIgnoreCase( CascadeTACategories.FSC_BACKUP.toString() ) || actionAndSubActionList[i].equalsIgnoreCase( CascadeTACategories.FSC_DEPLOY.toString() ) )
							bAction[5] = true;
						if( actionAndSubActionList[i].equalsIgnoreCase( CascadeTACategories.IBA_DEPLOY.toString() ) )
							bAction[6] = true;
						if( actionAndSubActionList[i].equalsIgnoreCase( CascadeTACategories.HDD_UPDATE.toString() ) )
							bAction[7] = true;
						if( actionAndSubActionList[i].equalsIgnoreCase( CascadeTACategories.GWTB_DEPLOY.toString() ) )
							bAction[8] = true;
					}
				}
				if( bDebug )
					System.out.println( "PSdZ ID:" + testScreenID + ", CASCADE VerifyVehicleOnTAL: Actions: (HWINSTALL:" + bAction[0] + ", HWDEINSTALL:" + bAction[1] + ", [BLFLASH, SWDEPLOY, SWDEPLOY | SWDeployTA, SWDeploy | SWDeleteTA]:" + bAction[2] + ", CDDEPLOY:" + bAction[3] + ", [IDBACKUP, IDDELETE, IDRESTORE]:" + bAction[4] + ", [FSCBACKUP, FSCDEPLOY]:" + bAction[5] + ", IBADEPLOY:" + bAction[6] + ", HDDUPDATE:" + bAction[7] + ", GWTBDEPLOY:" + bAction[8] + "); " + "VIN-Check: " + vinCheck + ", CPS-Check: " + cpsCheck + ", FA-Check: " + (faCheckVCM || faCheckVCMBackup) + ", Check VCM: " + checkVCM + ", HWT: " + (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : "") );

				// Analyse der TAL - Auswertung des TAL-Generierungsberichts
				// LOP 1926: Hier soll gepr�ft werden, ob Elemente im talGenerationReport vorhanden sind. 
				for( int j = 0; j < myTALResult.getCascadeTalGenerationReport().getExceptions().size(); j++ ) {
					CascadePSdZRuntimeException myCascadePsdzRuntimeExecption = (CascadePSdZRuntimeException) myTALResult.getCascadeTalGenerationReport().getExceptions().get( j );
					String action = DE ? "Aktion: " : "Action: ";
					String actionDescriptionTalGenerationReport = DE ? "Auswertung des TAL-Generierungsberichts" : "Evaluation of tal generation report";
					String errortext = action + actionDescriptionTalGenerationReport + ", " + errorDescription + myCascadePsdzRuntimeExecption.getMessage() + ", (PSdZ ID: " + myCascadePsdzRuntimeExecption.getMessageId() + ")";
					result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myCascadePsdzRuntimeExecption.getStrEcuBV() + ", " + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myCascadePsdzRuntimeExecption.getDiagnosticAdress() ).toUpperCase(), "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errortext, (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}

				// Analyse der TAL - Auswertung der TAL-Lines
				for( int i = 0; i < myTALResult.getTalLines().length; i++ ) {
					CascadeTALTACategory[] myTALTACategory = myTALResult.getTalLines()[i].getTALTACategories();
					CascadeTALLine myTALLine = myTALResult.getTalLines()[i];

					if( bDebug )
						System.out.println( "PSdZ ID:" + testScreenID + ", CASCADE VerifyVehicleOnTAL TAL Lines: " + myTALResult.getTalLines().length + ", Durchlauf: " + i + ", Basisname SG: " + myTALLine.getBaseVariantName() );

					// LOP 1436: Ausgabe um TAIS Teilenummer anreichern und zus�tzlich, wenn m�glich noch die TAIS Nummer des bestellbaren Teils mit ausgeben (Unterdr�cke die Bestellnummer, wenn diese gleich der Nummer des logTeils ist)						

					String logTeil = "";
					String bestellTeilTAIS = "";
					StringBuffer strBufNummernTAIS = new StringBuffer();

					if( bDebug )
						System.out.println( "PSdZ ID:" + testScreenID + ", CASCADE VerifyVehicleOnTAL Logistisches Teil fuer ECU: 0x" + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase() );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.getLogistischesTeil' START" );
					}

					if( psdz.getLogistischesTeil( Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ) ) != null ) {
						logTeil = psdz.getLogistischesTeil( Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ) ).getSachNrTAIS();
						strBufNummernTAIS.append( ", " + PB.getString( "psdz.text.TaisNummer" ) + logTeil );

						bestellTeilTAIS = psdz.getLogistischesTeil( Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ) ).getSachNrBestellbaresTeilTAIS();
						strBufNummernTAIS.append( logTeil.equals( bestellTeilTAIS ) ? "" : " (" + titleBestellTeilTAIS + bestellTeilTAIS + ")" );
					}

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.getLogistischesTeil' ENDE" );
					}

					// LOP 1848: TAIS-Nummer SOLL sowie Bestellnummer f�r falsch verbaute Steuerger�te in Fehlerprotokoll anzeigen, die verl�ngert g�ltig sind (-> HW o.k.) jedoch nicht im Bandablauf programmiert werden

					// wenn SachNrTAIS in Bestellliste der aktuellen Sollverbauung nicht gefunden wird ...
					if( psdz.getLogistischesTeil( Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ) ) == null ) {
						boolean bTaisNumbersAlreadyDetermined = false;
						for( int j = 0; j < myTALTACategory.length; j++ ) {
							// ... und es sich bei der TA-Kategorie um eine ein Programmierung handelt (bei fehlerhaften Codierungen beispielsweise oder unvollst�ndigem FSC-Stand im Bandablauf ist kein HW-Tausch notwendig 
							// und hier soll dann auch keine TAIS-Nummer SOLL im Fehlerprotokoll ausgegeben werden) ...
							if( myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.BL_FLASH.toString() ) || myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.SW_DEPLOY.toString() ) ) {
								// ... soll die Sollverbauung mit vollst�ndiger(!) Bestellliste erneut erzeugt werden jedoch nur einmal pro Pr�fschrittdurchlauf
								if( !bFullOrderListAlreadyGenerated ) {
									// Startzeit
									startTimeStamp = System.currentTimeMillis();

									// Performancemessung
									if( bDebugPerform ) {
										System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.generateFullOrderList' START" );
									}
									listCascadeLogistischesTeil = psdz.generateFullOrderList();
									bFullOrderListAlreadyGenerated = true;

									// Performancemessung
									if( bDebugPerform ) {
										System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.generateFullOrderList' ENDE" );
									}

									result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", "Test procedure method name for KIS call:=generateFullOrderList", "KIS calculation mode set by CASCADE:=" + KIS_CALCULATION_MODES.MONTAGEFORTSCHRITT, "", "Duration of method call [msec]", Long.toString( System.currentTimeMillis() - startTimeStamp ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
									ergListe.add( result );
								}
								// hole entsprechende Dezimaladresse des Steuerger�tes
								int diagAddr = myTALLine.getDiagnosticAddress().intValue();

								// jetzt die Liste s�mtlicher logistischer Teile anhand der Diagnoseadresse durchgehen und den ersten Treffer ermittlen...
								for( int k = 0; k < listCascadeLogistischesTeil.size(); k++ ) {
									if( listCascadeLogistischesTeil.get( k ).getDiagnosticAddress().intValue() == diagAddr ) {
										// Bef�llung von strBufNummernTAIS soll nur einmal pro TAL-Line erfolgen, damit es nicht zu Doppelausgaben der TAIS-Nummern im Fehlerprotokoll kommt, 
										// wenn die TA-Kategorien BL_FLASH UND SW_DEPLOY gleichzeitig in einer TAL-Line vorkommen 
										if( !bTaisNumbersAlreadyDetermined ) {
											logTeil = listCascadeLogistischesTeil.get( k ).getSachNrTAIS();
											strBufNummernTAIS.append( ", " + PB.getString( "psdz.text.TaisNummer" ) + logTeil );

											bestellTeilTAIS = listCascadeLogistischesTeil.get( k ).getSachNrBestellbaresTeilTAIS();
											strBufNummernTAIS.append( logTeil.equals( bestellTeilTAIS ) ? "" : " (" + titleBestellTeilTAIS + bestellTeilTAIS + ")" );
											bTaisNumbersAlreadyDetermined = true;
										}
									}
								}
							}
						}
					}

					if( bDebug )
						System.out.println( "PSdZ ID:" + testScreenID + ", CASCADE VerifyVehicleOnTAL Logistisches Teil Nummer (TAIS): " + logTeil + ",  BestellTeil TAIS: " + bestellTeilTAIS + ", fuer ECU: 0x" + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase() );

					for( int j = 0; j < myTALTACategory.length; j++ ) {
						if( bDebug )
							System.out.println( "CASCADE VerifyVehicleOnTAL TAL Category: " + myTALTACategory[j].getCategory().toString() + ", Durchlauf: " + j );

						// Wenn keine Individualdaten, kein FSC, kein IBADelpoy und kein HDD Update
						StringBuffer id = new StringBuffer();
						if( !myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.FSC_DEPLOY.toString() ) && !myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.FSC_BACKUP.toString() ) && !myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.ID_BACKUP.toString() ) && !myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.ID_DELETE.toString() ) && !myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.ID_RESTORE.toString() ) && !myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.IBA_DEPLOY.toString() ) && !myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.HDD_UPDATE.toString() ) ) {
							CascadeTA[] cascadeTAs = myTALTACategory[j].getTAs();
							id = new StringBuffer( "SGBM IDs [ " );
							for( int k = 0; k < cascadeTAs.length; k++ ) {
								id.append( cascadeTAs[k].getProcessClass() + cascadeTAs[k].getId() + "(" + cascadeTAs[k].getMainVersion() + "." + cascadeTAs[k].getSubVersion() + "." + cascadeTAs[k].getPatchVersion() + ") " );
							}
							id.append( "]" );

							if( bDebug )
								System.out.println( "PSdZ ID:" + testScreenID + ", CASCADE VerifyVehicleOnTAL: " + id.toString() );
						}

						// TODO Eine gemeinsame Ausgabe bei notwendigem HW-Tausch (Job: HW UNMOUT / HW MOUNT)!!!
						// ECU's Check Hardware fehlt					
						if( !activeResultfilter ) {

							if( bAction[0] && !myTALTACategory[j].getStatus().toString().equalsIgnoreCase( CascadeExecutionStatus.NOTEXECUTABLE.toString() ) && myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.HW_INSTALL.toString() ) ) {
								result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myTALLine.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase() + strBufNummernTAIS.toString(), "HW MOUNT", (DE ? "SOLL " : "Nom ") + id.toString(), "STATUS", statusNotOkay, "", "", "0", "", "", "", DE ? "Steuerger�t fehlt, Steuerger�t einbauen!" : "ECU is missing, install ECU!", (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO ); // pruefprozedur.psdz.tal.hwdeinstall = Steuerger�t ausbauen    pruefprozedur.psdz.tal.hwinstall = Steuerger�t einbauen
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						} else {
							//  Result Filter aktiv			
							if( bAction[0] && !myTALTACategory[j].getStatus().toString().equalsIgnoreCase( CascadeExecutionStatus.NOTEXECUTABLE.toString() ) && myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.HW_INSTALL.toString() ) ) {
								result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myTALLine.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase() + strBufNummernTAIS.toString(), "HW MOUNT", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", DE ? "Steuerger�t fehlt, Steuerger�t einbauen!" : "ECU is missing, install ECU!", (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
								ecuResultList.add( myTALLine.getDiagnosticAddress() );
							}
						}

						// ECU's Check Hardware muss raus
						if( !activeResultfilter ) {
							if( bAction[1] && !myTALTACategory[j].getStatus().toString().equalsIgnoreCase( CascadeExecutionStatus.NOTEXECUTABLE.toString() ) && myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.HW_DEINSTALL.toString() ) ) {

								result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myTALLine.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase(), "HW UNMOUNT", (DE ? "IST " : "Act ") + id.toString(), "STATUS", statusNotOkay, "", "", "0", "", "", "", DE ? "Steuerger�t verbaut obwohl nicht erwartet, Steuerger�t ausbauen!" : "ECU is mounted although not expect, remove ECU!", (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						} else {
							if( bAction[1] && !myTALTACategory[j].getStatus().toString().equalsIgnoreCase( CascadeExecutionStatus.NOTEXECUTABLE.toString() ) && myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.HW_DEINSTALL.toString() ) ) {

								result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myTALLine.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase(), "HW UNMOUNT", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", DE ? "Steuerger�t verbaut obwohl nicht erwartet, Steuerger�t ausbauen!" : "ECU is mounted although not expect, remove ECU!", (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
								ecuResultList.add( myTALLine.getDiagnosticAddress() );
							}
						}

						// ECU's, die noch ge'flasht' werden m�ssen
						if( !activeResultfilter ) {
							if( bAction[2] && !myTALTACategory[j].getStatus().toString().equalsIgnoreCase( CascadeExecutionStatus.NOTEXECUTABLE.toString() ) && (myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.BL_FLASH.toString() ) || myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.SW_DEPLOY.toString() )) ) {

								result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myTALLine.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase() + strBufNummernTAIS.toString(), "SVK FLASH", (DE ? "SOLL " : "Nom ") + id.toString(), "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.falscheSvk" ), (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						} else {
							// �berarbeitung Result Filter
							if( bAction[2] && !myTALTACategory[j].getStatus().toString().equalsIgnoreCase( CascadeExecutionStatus.NOTEXECUTABLE.toString() ) && (myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.BL_FLASH.toString() ) || myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.SW_DEPLOY.toString() )) && (!EcuAddressInList( myTALLine.getDiagnosticAddress().intValue() )) ) {

								result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myTALLine.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase() + strBufNummernTAIS.toString(), "SVK FLASH", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.falscheSvk" ), (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
								ecuResultList.add( myTALLine.getDiagnosticAddress() );
							}
						}

						// ECU's, die noch codiert werden m�ssen
						if( !activeResultfilter ) {
							if( bAction[3] && !myTALTACategory[j].getStatus().toString().equalsIgnoreCase( CascadeExecutionStatus.NOTEXECUTABLE.toString() ) && myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.CD_DEPLOY.toString() ) ) {

								result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myTALLine.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase() + strBufNummernTAIS.toString(), "CD DEPLOY", (DE ? "SOLL " : "Nom ") + id.toString(), "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.falscheCD" ), (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						} else {
							if( bAction[3] && !myTALTACategory[j].getStatus().toString().equalsIgnoreCase( CascadeExecutionStatus.NOTEXECUTABLE.toString() ) && myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.CD_DEPLOY.toString() ) && (!EcuAddressInList( myTALLine.getDiagnosticAddress().intValue() )) ) {

								result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myTALLine.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase() + strBufNummernTAIS.toString(), "CD DEPLOY", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.falscheCD" ), (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
								ecuResultList.add( myTALLine.getDiagnosticAddress() );
							}
						}

						// ECU's, bei denen was mit den Individualdaten passiert
						if( !activeResultfilter ) {
							if( bAction[4] && !myTALTACategory[j].getStatus().toString().equalsIgnoreCase( CascadeExecutionStatus.NOTEXECUTABLE.toString() ) && (myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.ID_BACKUP.toString() ) || myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.ID_DELETE.toString() ) || myTALTACategory[j].getCategory().equals( CascadeTACategories.ID_RESTORE.toString() )) ) {

								result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myTALLine.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase() + strBufNummernTAIS.toString(), "Process ID data", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.IdProblem" ), (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						} else {
							if( bAction[4] && !myTALTACategory[j].getStatus().toString().equalsIgnoreCase( CascadeExecutionStatus.NOTEXECUTABLE.toString() ) && (myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.ID_BACKUP.toString() ) || myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.ID_DELETE.toString() ) || myTALTACategory[j].getCategory().equals( CascadeTACategories.ID_RESTORE.toString() )) && (!EcuAddressInList( myTALLine.getDiagnosticAddress().intValue() )) ) {

								result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myTALLine.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase() + strBufNummernTAIS.toString(), "Process ID data", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.IdProblem" ), (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
								ecuResultList.add( myTALLine.getDiagnosticAddress() );
							}
						}

						// ECU's, bei denen was mit den FSCs passiert
						if( bAction[5] && !myTALTACategory[j].getStatus().toString().equalsIgnoreCase( CascadeExecutionStatus.NOTEXECUTABLE.toString() ) && (myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.FSC_BACKUP.toString() ) || myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.FSC_DEPLOY.toString() )) ) {
							bAddEcuToEcuResultList = false;
							bReportError = false;

							if( !activeResultfilter ) {
								bReportError = true;
							} else {
								if( !EcuAddressInList( myTALLine.getDiagnosticAddress().intValue() ) ) {
									bReportError = true;
									bAddEcuToEcuResultList = true;
								}
							}
							if( bReportError ) {
								String errorText = "AppID(s): ";
								boolean bApplicationIDFound = false;

								for( int k = 0; k < myTALTACategory[j].getFscTAs().length; k++ ) {
									CascadeSwtActionType action = myTALTACategory[j].getFscTAs()[k].getAction();
									if( action.equals( CascadeSwtActionType.TYPE_ACTIVATE_STORE ) || action.equals( CascadeSwtActionType.TYPE_ACTIVATE_UPDATE ) || action.equals( CascadeSwtActionType.TYPE_ACTIVATE_UPGRADE ) )
										actionText = DE ? "(fehlt)" : "(missing)";
									else if( action.equals( CascadeSwtActionType.TYPE_DEACTIVATE ) )
										actionText = DE ? "(�berz�hlig)" : "(surplus)";
									else if( action.equals( CascadeSwtActionType.TYPE_WRITE_VIN ) )
										actionText = DE ? "(VIN-IST ungleich VIN-SOLL)" : "(actual VIN unequal to target VIN)";
									else
										actionText = "";

									// CR1987 - f�r action="6" (VIN-Schreiben) wird keine applicationID (applicationNo sowie updateIndex) in TAL generiert
									// weitere Fehler-Pr�fung erfolgt also nur f�r FSC-Fehler, bei denen etwas mit AppIDs passieren muss
									if( myTALTACategory[j].getFscTAs()[k].isApplicationIDSet() ) {
										errorText = errorText + "0x" + Integer.toHexString( myTALTACategory[j].getFscTAs()[k].getApplicationIDVersion() ).toUpperCase() + "/" + myTALTACategory[j].getFscTAs()[k].getUpgradeIndex() + " " + actionText;
										bApplicationIDFound = true;
									} else {
										errorText = errorText + "-" + " " + actionText;
									}
									errorText = errorText + ((myTALTACategory[j].getFscTAs().length > 1) && (k < (myTALTACategory[j].getFscTAs().length - 1)) ? ", " : "");
								}

								// Sonderbehandlung bzgl. CR1987 f�r Mischbetrieb von CASCADE 6.1 und CASCADE 7.0 w�hrend Rollout-Phase
								// bei scharfer FSC-Pr�fung (bSoftCheckFSC=false) wird Pr�fungsstatus an dieser Stelle immer n.i.O, andernfalls schl�gt Pr�fung nur dann fehl, wenn die TAL-Line mindestens eine AppID enth�lt und bleibt somit i.O. wenn die TAL-Line nur TAs mit action="6" (keine AppID) enth�lt
								if( !bSoftCheckFSC || bApplicationIDFound ) {
									result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myTALLine.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase() + strBufNummernTAIS.toString(), "FSC DEPLOY", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.FscProblem" ) + " " + errorText, (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
									if( bAddEcuToEcuResultList )
										ecuResultList.add( myTALLine.getDiagnosticAddress() );
								}
							}
						}

						// ECU's, mit Problemen beim IBA Deploy
						if( !activeResultfilter ) {
							if( bAction[6] && !myTALTACategory[j].getStatus().toString().equalsIgnoreCase( CascadeExecutionStatus.NOTEXECUTABLE.toString() ) && myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.IBA_DEPLOY.toString() ) ) {

								result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myTALLine.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase() + strBufNummernTAIS.toString(), "IBA DEPLOY", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.IbaProblem" ), (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						} else {
							if( bAction[6] && !myTALTACategory[j].getStatus().toString().equalsIgnoreCase( CascadeExecutionStatus.NOTEXECUTABLE.toString() ) && myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.IBA_DEPLOY.toString() ) && (!EcuAddressInList( myTALLine.getDiagnosticAddress().intValue() )) ) {

								result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myTALLine.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase() + strBufNummernTAIS.toString(), "IBA DEPLOY", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.IbaProblem" ), (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
								ecuResultList.add( myTALLine.getDiagnosticAddress() );
							}
						}

						// ECU's, mit Problemen beim HDD Update
						if( !activeResultfilter ) {
							if( bAction[7] && !myTALTACategory[j].getStatus().toString().equalsIgnoreCase( CascadeExecutionStatus.NOTEXECUTABLE.toString() ) && myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.HDD_UPDATE.toString() ) ) {

								result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myTALLine.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase() + strBufNummernTAIS.toString(), "HDD UPDATE", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.HddProblem" ), (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						} else {
							if( bAction[7] && !myTALTACategory[j].getStatus().toString().equalsIgnoreCase( CascadeExecutionStatus.NOTEXECUTABLE.toString() ) && myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.HDD_UPDATE.toString() ) && (!EcuAddressInList( myTALLine.getDiagnosticAddress().intValue() )) ) {

								result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myTALLine.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase() + strBufNummernTAIS.toString(), "HDD UPDATE", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.HddProblem" ), (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
								ecuResultList.add( myTALLine.getDiagnosticAddress() );
							}
						}

						// ECU's, mit Problemen bei der Gatewaytabelle
						if( !activeResultfilter ) {
							if( bAction[8] && !myTALTACategory[j].getStatus().toString().equalsIgnoreCase( CascadeExecutionStatus.NOTEXECUTABLE.toString() ) && myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.GWTB_DEPLOY.toString() ) ) {

								result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myTALLine.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase() + strBufNummernTAIS.toString(), "GWTB DEPLOY", (DE ? "SOLL " : "Nom ") + id.toString(), "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.falscheGWTB" ), (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						} else {
							// �berarbeitung Result Filter
							if( bAction[8] && !myTALTACategory[j].getStatus().toString().equalsIgnoreCase( CascadeExecutionStatus.NOTEXECUTABLE.toString() ) && (myTALTACategory[j].getCategory().toString().equalsIgnoreCase( CascadeTACategories.GWTB_DEPLOY.toString() )) && (!EcuAddressInList( myTALLine.getDiagnosticAddress().intValue() )) ) {

								result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", myTALLine.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( myTALLine.getDiagnosticAddress().intValue() ).toUpperCase() + strBufNummernTAIS.toString(), "GWTB DEPLOY", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.falscheGWTB" ), (textHWT != null && !textHWT.equalsIgnoreCase( "" ) ? textHWT : ""), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
								ecuResultList.add( myTALLine.getDiagnosticAddress() );
							}
						}
					}
				}

				// Anweisung an PIA-Portierungsmaster zum Einsammeln der PIA-Meta-Daten
				if( bStartCollectPiaMetaData ) {

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.startCollectPiaMetaData' START" );
					}
					piaCollectionResult = psdz.startCollectPiaMetaData();
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.startCollectPiaMetaData' ENDE" );
					}

					if( !piaCollectionResult.isEmpty() ) {
						// Ergebnis im Fehlerfall ausgeben
						String piaMetaDataCollectionFailed = DE ? "Prozess zum Einsammeln der PIA-Metadaten konnte nicht gestartet werden! " : "Process to collect pia meta data could not be started! ";
						CascadeECU piaMaster = piaCollectionResult.keySet().iterator().next();
						String baseVariantName = piaMaster.getBaseVariantName();
						int diagAddress = piaMaster.getDiagnosticAddress();
						String errorText = piaCollectionResult.get( piaMaster );

						result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", baseVariantName + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( diagAddress ).toUpperCase(), "START COLLECT PIA META DATA", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", piaMetaDataCollectionFailed + errorDescription + errorText, "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				}

				// VIN Check
				if( vinCheck ) {
					String vinCheckFailed = DE ? "VIN-Pr�fung (Fahrzeug gegen Auftragsdaten) fehlgeschlagen. " : "VIN CHECK (vehicle versus order data) failed. ";

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.verifyVin' START" );
					}
					vinResult = psdz.verifyVin();
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.verifyVin' ENDE" );
					}
					if( !vinResult.equalsIgnoreCase( getPr�fling().getAuftrag().getFahrgestellnummer() ) ) {
						if( bDebug ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", CASCADE VerifyVehicleOnTAL: VIN-Check: IST >" + vinResult + "<, SOLL >" + getPr�fling().getAuftrag().getFahrgestellnummer() + "<" );
						}
						// VIN ausgeben
						result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", "", "", "", "VIN", vinResult, getPr�fling().getAuftrag().getFahrgestellnummer(), "", "0", "", "", "", vinCheckFailed + PB.getString( "psdz.error.falscheVin" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				}

				// CPS Check
				if( cpsCheck ) {

					// Startzeit
					startTimeStamp = System.currentTimeMillis();

					if( includeList != null ) {
						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.verifyCPS' START" );
						}
						cpsResults = psdz.verifyCPS( useSVTist, useSVTsoll, checkVCM, includeList, false, enumKISCalculationMode );
						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.verifyCPS' ENDE" );
						}

						// Nur dann Zeiten loggen, wenn auch tats�chlich eine KIS-Abfrage erfolgt
						if( !useSVTsoll ) {
							result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", "Test procedure method name for KIS call:=verifyCPS", "KIS calculation mode set by CASCADE:=" + enumKISCalculationMode, "", "Duration of method call [msec]", Long.toString( System.currentTimeMillis() - startTimeStamp ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						}
					} else if( excludeList != null ) {
						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.verifyCPS' START" );
						}
						cpsResults = psdz.verifyCPS( useSVTist, useSVTsoll, checkVCM, excludeList, true, enumKISCalculationMode );
						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.verifyCPS' ENDE" );
						}

						// Nur dann Zeiten loggen, wenn auch tats�chlich eine KIS-Abfrage erfolgt
						if( !useSVTsoll ) {
							result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", "Test procedure method name for KIS call:=verifyCPS", "KIS calculation mode set by CASCADE:=" + enumKISCalculationMode, "", "Duration of method call [msec]", Long.toString( System.currentTimeMillis() - startTimeStamp ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						}
					} else {
						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.verifyCPS' START" );
						}
						// �bergebe leere Liste -> damit wir alles verifizieren k�nnen
						String[] emptyList = {};
						cpsResults = psdz.verifyCPS( useSVTist, useSVTsoll, checkVCM, emptyList, true, enumKISCalculationMode );
						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.verifyCPS' ENDE" );
						}

						// Nur dann Zeiten loggen, wenn auch tats�chlich eine KIS-Abfrage erfolgt
						if( !useSVTsoll ) {
							result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", "Test procedure method name for KIS call:=verifyCPS", "KIS calculation mode set by CASCADE:=" + enumKISCalculationMode, "", "Duration of method call [msec]", Long.toString( System.currentTimeMillis() - startTimeStamp ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						}
					}
					if( cpsResults != null ) {
						for( Iterator<?> e = cpsResults.keySet().iterator(); e.hasNext(); ) {
							CascadeECU key = (CascadeECU) e.next();
							if( bDebug ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", CASCADE VerifyVehicleOnTAL: CPS-Check: IST >" + cpsResults.get( key ).toString() + "<, SOLL >" + getPr�fling().getAuftrag().getFahrgestellnummer7() + "<" );
							}
							// CPS ausgeben
							result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", key.getBaseVariantName() + (DE ? ", Adr.: 0x" : ", Addr.: 0x") + Integer.toHexString( key.getDiagnosticAddress().intValue() ).toUpperCase(), "CPS CHECK", "", "CPS", cpsResults.get( key ).toString(), getPr�fling().getAuftrag().getFahrgestellnummer7(), "", "0", "", "", "", PB.getString( "psdz.error.falschesCps" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}
				}

				// FA Check (VCM und/oder VCM_Backup)
				if( faCheckVCM || faCheckVCMBackup ) {

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.getFAFromVehicle' START" );
					}

					if( faCheckVCM ) {
						// Vergleiche FA aus VCM mit FA aus OrderXML
						FA = null;
						try {
							FA = psdz.getFAFromVehicle( false, false );
						} catch( Exception e ) {
							errorCauseFaCheckVCMText = e.getLocalizedMessage();
						}

						if( FA != null ) {
							faFromVCMText = FA.getAsString();
							faFromVCMEqualsFAFromOrderXML = psdz.compareWithFAFromOrderXML( FA );
						} else {
							faFromVCMText = PB.getString( "psdz.error.LeseFAausVCM" );
							faFromVCMEqualsFAFromOrderXML = false;
						}
					}

					if( faCheckVCMBackup ) {
						// Vergleiche FA aus VCM_Backup mit FA aus OrderXML
						FA = null;
						try {
							FA = psdz.getFAFromVehicle( true, false );
						} catch( Exception e ) {
							errorCauseFaCheckVCMBackupText = e.getLocalizedMessage();
						}

						if( FA != null ) {
							faFromVCMBackupText = FA.getAsString();
							faFromVCMBackupEqualsFAFromOrderXML = psdz.compareWithFAFromOrderXML( FA );
						} else {
							faFromVCMBackupText = PB.getString( "psdz.error.LeseFAausVCMBackup" );
							faFromVCMBackupEqualsFAFromOrderXML = false;
						}
					}

					faFromOrderXMLText = psdz.getVehicleFA().getAsString();

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.getFAFromVehicle' ENDE" );
					}

					String faCheckFailed = DE ? "FA-Pr�fung fehlgeschlagen. " : "FA CHECK failed. ";

					if( !faFromVCMEqualsFAFromOrderXML ) {
						result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", "", "", "", "FA", faFromVCMText, faFromOrderXMLText, "", "0", "", "", "", faCheckFailed + PB.getString( "psdz.error.VergleicheFAausVCMmitFAausOrderXML" ) + (errorCauseFaCheckVCMText != null ? errorDescription + errorCauseFaCheckVCMText : ""), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
					if( !faFromVCMBackupEqualsFAFromOrderXML ) {
						result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", "", "", "", "FA", faFromVCMBackupText, faFromOrderXMLText, "", "0", "", "", "", faCheckFailed + PB.getString( "psdz.error.VergleicheFAausVCMBackupmitFAausOrderXML" ) + (errorCauseFaCheckVCMBackupText != null ? errorDescription + errorCauseFaCheckVCMBackupText : ""), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}

				}

				// FSC Check (KIS versus OrderData)
				if( fscCheckKisVsOrderData ) {

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.retrieveSwtApplicationIdListFromVehicleSollverbauung' START" );
					}

					List<CascadeSwtApplicationId> swtApplicationIdListFromKIS;
					swtApplicationIdListFromKIS = psdz.retrieveSwtApplicationIdListFromVehicleSollverbauung();

					if( bDebug ) {
						List<String> kisList = new ArrayList<String>();
						for( CascadeSwtApplicationId swtAppIdKis : swtApplicationIdListFromKIS ) {
							kisList.add( "0x" + Integer.toHexString( swtAppIdKis.getApplicationNumber() ).toUpperCase() + "/" + swtAppIdKis.getUpgradeIndex() );
						}
						System.out.println( "PSdZ ID:" + testScreenID + ", CASCADE VerifyVehicleOnTAL FSC application ID(s) from KIS: " + kisList.toString() );
					}

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.retrieveSwtApplicationIdListFromVehicleSollverbauung' ENDE" );
					}

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.retrieveSwtApplicationIdListFromOrderFile' START" );
					}

					List<CascadeSwtApplicationId> swtApplicationIdListFromOrderFile;
					swtApplicationIdListFromOrderFile = psdz.retrieveSwtApplicationIdListFromOrderFile();

					if( bDebug ) {
						List<String> orderFileList = new ArrayList<String>();
						for( CascadeSwtApplicationId swtAppIdOrderFile : swtApplicationIdListFromOrderFile ) {
							orderFileList.add( "0x" + Integer.toHexString( swtAppIdOrderFile.getApplicationNumber() ).toUpperCase() + "/" + swtAppIdOrderFile.getUpgradeIndex() );
						}
						System.out.println( "PSdZ ID:" + testScreenID + ", CASCADE VerifyVehicleOnTAL FSC application ID(s) from order file: " + orderFileList.toString() );
					}

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL 'psdz.retrieveSwtApplicationIdListFromOrderFile' ENDE" );
					}

					// Vergleich der beiden Listen durchf�hren

					Set<String> multipleEntiresSetFromSwtApplicationIdListFromKIS = new HashSet<String>();
					Set<String> multipleEntiresSetFromSwtApplicationIdListFromOrderFile = new HashSet<String>();

					// Sind alle Eintr�ge aus KIS-Liste in Auftrags-Liste enthalten?
					List<String> applicationIds = new ArrayList<String>();
					String errorText = "";
					String fscCheckKisVsOrderDataFailed = DE ? "FSC-Pr�fung (KIS gegen Auftragsdaten) fehlgeschlagen. " : "FSC CHECK ( KIS VERSUS ORDER DATA ) failed. ";

					for( CascadeSwtApplicationId swtAppIdKis : swtApplicationIdListFromKIS ) {
						boolean matchFound = false;
						for( CascadeSwtApplicationId swtAppIdOrderFile : swtApplicationIdListFromOrderFile ) {
							if( swtAppIdKis.equals( swtAppIdOrderFile ) ) {
								// �bereinstimmung (auch mehrfach) gefunden
								// Falls matchFound bereits True, handelt es sich um erneute �bereinstimmung und damit um Mehrfacheintrag in Auftrags-Liste!
								if( matchFound ) {
									multipleEntiresSetFromSwtApplicationIdListFromOrderFile.add( "0x" + Integer.toHexString( swtAppIdOrderFile.getApplicationNumber() ).toUpperCase() + "/" + swtAppIdOrderFile.getUpgradeIndex() );
								}
								matchFound = true;
							}
						}
						if( !matchFound )
							// Eintrag in KIS-Liste ohne passenden Eintrag in Auftrags-Liste
							applicationIds.add( "0x" + Integer.toHexString( swtAppIdKis.getApplicationNumber() ).toUpperCase() + "/" + swtAppIdKis.getUpgradeIndex() );
					}
					if( !applicationIds.isEmpty() ) {

						errorText = DE ? "FSC Applikations-Id(s): " + applicationIds.toString() + " in FSC-Tabelle der Sollverbauung enthalten, jedoch in Auftragsdaten nicht enthalten!" : "FSC application ID(s): " + applicationIds.toString() + " part of fsc table in Sollverbauung, but missing in order data!";
						result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", fscCheckKisVsOrderDataFailed + errorText, "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}

					// Sind alle Eintr�ge aus Auftrags-Liste in KIS-Liste enthalten?
					applicationIds.clear();
					for( CascadeSwtApplicationId swtAppIdOrderFile : swtApplicationIdListFromOrderFile ) {
						boolean matchFound = false;
						for( CascadeSwtApplicationId swtAppIdKis : swtApplicationIdListFromKIS ) {
							if( swtAppIdOrderFile.equals( swtAppIdKis ) ) {
								// �bereinstimmung (auch mehrfach) gefunden
								// Falls matchFound bereits True, handelt es sich um erneute �bereinstimmung und damit um Mehrfacheintrag in KIS-Liste!
								if( matchFound ) {
									multipleEntiresSetFromSwtApplicationIdListFromKIS.add( "0x" + Integer.toHexString( swtAppIdKis.getApplicationNumber() ).toUpperCase() + "/" + swtAppIdKis.getUpgradeIndex() );
								}
								matchFound = true;
							}
						}
						if( !matchFound )
							// Eintrag in Auftrags-Liste ohne passenden Eintrag in KIS-Liste
							applicationIds.add( "0x" + Integer.toHexString( swtAppIdOrderFile.getApplicationNumber() ).toUpperCase() + "/" + swtAppIdOrderFile.getUpgradeIndex() );
					}

					if( !applicationIds.isEmpty() ) {

						errorText = DE ? "FSC Applikations-Id(s): " + applicationIds.toString() + " in Auftragsdaten enthalten, jedoch in FSC-Tabelle der Sollverbauung nicht enthalten!" : "FSC application ID(s): " + applicationIds.toString() + " part of order data, but missing in fsc table of Sollverbauung!";
						result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", fscCheckKisVsOrderDataFailed + errorText, "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}

					// Sind gleichnamige Eintr�ge mehrfach in KIS-Set oder OrderFile-Set vorhanden? Wenn ja, werden diese explizit ausgewiesen
					// Hinweis: Mehrfacheintr�ge in z.B. KIS-Set werden nur aufgenommen, wenn ein passender Eintrag in OrderFile-Set enthalten ist. Andernfalls werden Doppeleintr�ge �ber obigen Algorithmus nicht erkannt.
					// Diese F�lle (Mehrfacheintrag in einer Liste, kein entsprechender Eintrag in jeweils anderer Liste) werden dennoch sicher erkannt. In diesem Fall wird der Fehler dar�ber abgefangen, dass die Mehrfacheintr�ge in einer Liste jeweils einzeln als �berz�hlige Elemente gg�. der anderen Liste erkannt werden.  
					if( !multipleEntiresSetFromSwtApplicationIdListFromKIS.isEmpty() ) {
						errorText = DE ? "FSC Applikations-Id(s): " + multipleEntiresSetFromSwtApplicationIdListFromKIS.toString() + " in FSC-Tabelle der Sollverbauung mehrfach enthalten!" : "FSC application ID(s): " + multipleEntiresSetFromSwtApplicationIdListFromKIS.toString() + " contained multiple times in fsc table of Sollverbauung!";
						result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", fscCheckKisVsOrderDataFailed + errorText, "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
					if( !multipleEntiresSetFromSwtApplicationIdListFromOrderFile.isEmpty() ) {
						errorText = DE ? "FSC Applikations-Id(s): " + multipleEntiresSetFromSwtApplicationIdListFromOrderFile.toString() + " in Auftragsdaten mehrfach enthalten!" : "FSC application ID(s): " + multipleEntiresSetFromSwtApplicationIdListFromOrderFile.toString() + " contained multiple times in order data!";
						result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", fscCheckKisVsOrderDataFailed + errorText, "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				}

				// reset UserDialog
				userDialog.reset();

			} catch( CascadePSdZConditionsNotCorrectException e ) {
				// Fehler abfangen und dokumentiern
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), "conditions not correct exception", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( CascadePSdZRuntimeException e ) {
				// Fehler abfangen und dokumentiern
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), "runtime exception", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();

			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;

		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();

			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// tempor�re Variablen zur�cksetzen
		try {
			// setze tempor�re Variable f�r das Ignorieren der CAFD Patch Level Pr�fung
			this.getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_IGNORE_CAFD_PATCH_LEVEL", new Boolean( false ) );
			// setze tempor�re Variable f�r das zus�tzliche Ausf�hren des SAB Checks
			this.getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_SAB_CHECK", new Boolean( false ) );
		} catch( Exception e ) {
			// Nothing
		}

		// Freigabe der benutzten Devices
		if( psdz != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releasePSdZ();
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();

				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
		try {
			getPr�flingLaufzeitUmgebung().releaseUserDialog();
		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();

			if( e instanceof DeviceLockedException )
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "PSdZVerifyVehicleOnTAL", "PSdZ", "", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}
		setPPStatus( info, status, ergListe );
		// Performancemessung Ende
		if( bDebugPerform )
			System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVerifyVehicleOnTAL PP ENDE" );

	}
}

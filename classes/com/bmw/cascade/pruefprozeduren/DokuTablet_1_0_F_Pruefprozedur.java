package com.bmw.cascade.pruefprozeduren;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.util.dom.DomPruefstand;

/**
 * Dokumentation von E/E Daten u.a. der Seriennummer zum optional mit ausgelieferten Samsung Tablet. (Ab 35up neuer 7er G11 und ff. Baureihen) <BR>
 * <BR>
 * Funktion:<BR>
 *    - Die E/E Daten Daten zum Samsung Tablet werden mit einem externen Pre-Provisioning Tool generiert und in eine Datei: '7-stellige ShortVIN'_Tablet.txt abgelegt.<BR>
 *    - Das Pre-Provisioning Tool legt diese Dateien in seinem Arbeitsverzeichnis -> Unterordner: SerialData ab. (Ablagezeitraum 30 Tage)<BR>
 *    - Diese PP liest den Inhalt dieser Datei im definierten Format: "Modell:'alphanumerisch', SN:'alphanumerisch' (UTF-8-encoding) ein.<BR>
 *    - Als Parameter der PP: 'WDIR' m�ssen das obige Arbeitsverzeichnis und die Sachnummer des Tablets 'TABLET_ASSEMBLYPARTNUMBER' zwingend angegeben werden.<BR>
 *    - �ber den optionalen Parameter: 'DEBUG' kann ein erweitertes Logging der PP aktiviert werden. Alle Parameter unterst�tzen den @-Operator Referenzmechanismus.<BR>
 *    - Beim Vorhandensein der E/E Daten werden diese in Form eines DOM-Sensor-Files: 'ShortVIN'J.xml gespeichert und an DOM bei F2 �bertragen.<BR>
 *    - Falls die Daten nicht vorhanden sind wird die PP NIO.<BR>
 *    - Erg�nzend werden die Informationen auch im virtuellen Fahrzeug gespeichert.<BR>    
 * <BR>
 * @author BMW TI-545 Thomas Buboltz (TB)<BR>
 * @version 1_0_F 24.10.2015 TB Implementierung<BR>
 */
public class DokuTablet_1_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DokuTablet_1_0_F_Pruefprozedur() {
	}

	/**
	   * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	   * @param pruefling Klasse des zugeh. Pr�flings
	   * @param pruefprozName Name der Pr�fprozedur
	   * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	   */
	public DokuTablet_1_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 * 
	 * @return String Array der optionalen PP Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 * 
	 * @return String Array der zwingenden PP Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "WDIR", "TABLET_ASSEMBLYPARTNUMBER" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert!!!
	 * 
	 * @return true, wenn Parameter Check i.O.
	 */
	@Override
	public boolean checkArgs() {
		// Obligatorisch.
		return super.checkArgs();
	}

	/** Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * Debug Flag
	 */
	private boolean debug = false;

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// �bergabeparameter
		String wdir = null;
		String tabletAssemblyPartNumber = null;

		// spezifische Variablen
		final String TABLET_SENSOR_ID = "7900"; // Tablet wird als quasi als SG-Sensor (mit der hex ID: 7900) behandelt, ID muss mit der in der 'STD_ISEN.b2s' Datei vgl. EDIABAS �bereinstimmen

		String shortVIN = null;
		Map<String, String> tabletData = null;

		final boolean DE = checkDE(); // Systemsprache DE wenn true

		try {
			//Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG Parameter
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						debug = true;
					else if( !getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				} else {
					// alternativ DEBUG �ber Pr�fstandsvariable noch auswerten
					try {
						Object objDebug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
						if( objDebug != null ) {
							if( objDebug instanceof String && "TRUE".equalsIgnoreCase( (String) objDebug ) )
								debug = true;
							else if( objDebug instanceof Boolean && ((Boolean) objDebug).booleanValue() )
								debug = true;
						}
					} catch( VariablesException e ) {
						// Nothing, debug ist false vorbelegt
					}
				}
				if( debug ) {
					System.out.println( "PP DokuTablet: PARAMETERCHECK, Parameter: DEBUG=<" + debug + ">" );
				}

				// WDIR Parameter
				if( getArg( "WDIR" ) != null ) {
					wdir = extractValues( getArg( "WDIR" ) )[0];
				}

				// TABLET_ASSEMBLYPARTNUMBER
				if( getArg( "TABLET_ASSEMBLYPARTNUMBER" ) != null ) {
					tabletAssemblyPartNumber = extractValues( getArg( "TABLET_ASSEMBLYPARTNUMBER" ) )[0];
				}

				if( debug )
					System.out.println( "PP DokuTablet: PARAMETERCHECK, Parameter: WDIR=<" + wdir + ">, TabletAssemblyPartNumber=<" + tabletAssemblyPartNumber + ">" );

			} catch( PPExecutionException e ) {
				if( debug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// 7-stellige VIN vom akt. Fahrzeug bestimmen 
			shortVIN = getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7();

			// Daten vom Tablet als Datei einlesen
			tabletData = readTabletFile( wdir, shortVIN );

			// Dokumentiere, wenn Daten vorhanden sind...
			if( tabletData != null && tabletData.containsKey( "SN" ) ) {
				StringBuffer sbXMLModified = null;

				// Gibt es bereits eine bestehende " '7-stellige-VIN'J.xml " Datei, die aktualisiert werden muss?
				try {
					StringBuffer sbXMLExistent = readXMLFromCascadeServer( shortVIN, "J" );

					// Tablet-Sensor Daten Fragment bef�llen
					StringBuffer sbXMLSensorFragment = new StringBuffer();
					sbXMLSensorFragment.append( "\t<electronicComponent>\r\n" );
					sbXMLSensorFragment.append( "\t\t<sensor sensorID=\"" + TABLET_SENSOR_ID + "\" />\r\n" );
					sbXMLSensorFragment.append( "\t\t<serialNo>" + tabletData.get( "SN" ) + "</serialNo>\r\n" );
					sbXMLSensorFragment.append( "\t\t<assemblyPartNo>" + tabletAssemblyPartNumber + "</assemblyPartNo>\r\n" );
					sbXMLSensorFragment.append( "\t</electronicComponent>\r\n" );

					// sind schon die Tablet Daten drin od. m�ssen diese einfach angeh�ngt werden
					int indexSearchToken = sbXMLExistent.indexOf( "\t<electronicComponent>\r\n\t\t<sensor sensorID=\"" + TABLET_SENSOR_ID );
					if( indexSearchToken >= 0 ) {
						// Sensordaten mit aktuellen Sensor Fragment Daten aktualisieren
						sbXMLModified = new StringBuffer( sbXMLExistent.substring( 0, indexSearchToken ) );
						sbXMLModified.append( sbXMLSensorFragment );
						String footer = sbXMLExistent.substring( indexSearchToken );
						sbXMLModified.append( footer.substring( footer.indexOf( "\t</electronicComponent>\r\n" ) + "\t</electronicComponent>\r\n".length() ) );
					} else {
						// neues Sensor Fragment anh�ngen
						sbXMLModified = sbXMLExistent;
						sbXMLModified.insert( sbXMLModified.indexOf( "</electronicComponents>\r\n" ), sbXMLSensorFragment.toString() );
					}
				} catch( Exception e ) {
					//nur Debug, kann ja sein das keine Datei vorhanden ist od. diese nicht �ber das NW gelesen werden konnte
					if( debug ) {
						System.out.println( "PP DokuTablet: OK (just no Server File available)." );
						e.printStackTrace();
					}
				}

				// DOM Daten schreiben
				try {
					DomPruefstand.writeDomData( shortVIN, "J", sbXMLModified != null ? sbXMLModified.toString().getBytes() : buildXMLContent( shortVIN, TABLET_SENSOR_ID, tabletData, tabletAssemblyPartNumber ).toString().getBytes() );
				} catch( Exception e ) {
					result = new Ergebnis( "ExecFehler", "DomTransfer", "", "", "", "", "", "", "", "0", "", "", "", "FileWrite", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}

				//Tablet Daten im virtuellen Fzg. dokumentieren
				for( Map.Entry<String, String> entry : tabletData.entrySet() ) {
					result = new Ergebnis( entry.getKey(), "DokuTablet", "", "", "", entry.getKey(), entry.getValue(), entry.getValue(), "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				}

				//IO-Ablauf als Status festhalten
				result = new Ergebnis( "DokuTabletResult", "DomTransfer", "", "", "", "STATUS", "OK", "OK", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );
			} else {
				String hinweisText = DE ? "Es wurde keine DOM Datei: " + shortVIN + "J.xml, erzeugt!" : "No DOM file: " + shortVIN + "J.xml, created!";

				//Es wurde keine DOM Datei geschrieben! NIO-Ablauf als Status festhalten
				result = new Ergebnis( "DokuTabletResult", "DomTransfer", "", "", "", "STATUS", "OK", "NOK", "", "0", "", "", "", "ERROR", hinweisText, Ergebnis.FT_NIO );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}

		} catch( PPExecutionException e ) {
			if( debug ) {
				e.printStackTrace();
			}
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			if( debug ) {
				e.printStackTrace();
			}
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			if( debug ) {
				e.printStackTrace();
			}
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		setPPStatus( info, status, ergListe );
	}

	/** Holt potentiell bereits vorhandenen Datensatz vom CASCADE Server
	 * 
	 * @param shortVIN
	 * @param suffix
	 * @return Eingelesener Datensatz
	 * @throws Exception Fehler beim Laden
	 */
	private StringBuffer readXMLFromCascadeServer( final String shortVIN, final String suffix ) throws Exception {
		try {
			// RMI Aufruf zum Server
			byte[] rawData = DomPruefstand.readDomData( shortVIN, suffix );

			// Roh-Daten verarbeiten
			if( (rawData != null) && (rawData.length > 1) ) {
				String strData = new String( rawData );
				if( (strData != null) && (strData.length() > 1) ) {
					return new StringBuffer( strData.trim() );
				}
			}
		} catch( Exception e ) {
			throw e;
		}

		throw new Exception( "Dataset is empty" );
	}

	/**
	 * DOM XML Inhalt neu erzeugen
	 * 
	 * @param shortVIN
	 * @param TABLET_SENSOR_ID
	 * @param tabletData
	 * @param tabletAssemblyPartNumber
	 * @return DOM XML Inhalt
	 */
	private StringBuffer buildXMLContent( final String shortVIN, final String TABLET_SENSOR_ID, final Map<String, String> tabletData, final String tabletAssemblyPartNumber ) {
		StringBuffer sbXml = new StringBuffer();

		// XML-Output-Buffer zusammenbauen
		sbXml.append( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" );
		sbXml.append( "<vehicleElectronicComponentsJ\r\n" );
		sbXml.append( "xmlns=\"http://bmw.com/vehicleElectronicComponents\"\r\n" );
		sbXml.append( "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\r\n" );
		sbXml.append( "xsi:schemaLocation=\"http://bmw.com/vehicleElectronicComponents vehicleElectronicComponentsJ.xsd\"\r\n" );
		sbXml.append( "version=\"01.00\" refSchema=\"vehicleElectronicComponentsJ.xsd\">\r\n" );
		sbXml.append( "<vehicle vinShort=\"" );
		sbXml.append( shortVIN );
		sbXml.append( "\">\r\n" );
		sbXml.append( "<electronicComponents>\r\n" );

		// Tablet-Sensor Daten bef�llen
		sbXml.append( "\t<electronicComponent>\r\n" );
		sbXml.append( "\t\t<sensor sensorID=\"" + TABLET_SENSOR_ID + "\" />\r\n" );
		sbXml.append( "\t\t<serialNo>" + tabletData.get( "SN" ) + "</serialNo>\r\n" );
		sbXml.append( "\t\t<assemblyPartNo>" + tabletAssemblyPartNumber + "</assemblyPartNo>\r\n" );
		sbXml.append( "\t</electronicComponent>\r\n" );

		// XML-Footer anh�ngen
		sbXml.append( "</electronicComponents>\r\n" );
		sbXml.append( "</vehicle>\r\n" );
		sbXml.append( "</vehicleElectronicComponentsJ>\r\n" );

		return sbXml;
	}

	/**
	 * Lese die lokale und auf die kurze Fahrgestellnummer basierte Tablet-Datei: 'ShortVIN'_Tablet.txt ein und trage sofern die Informationen vorhanden sind, diese in eine Map: Key / Value ein
	 * 
	 * @param Arbeitsverzeichnis des Pre-Provision Tools
	 * @param kurze VIN des aktuellen Fahrzeuges
	 * @return Map mit den Tablet-Daten
	 */
	private Map<String, String> readTabletFile( String workingDir, String shortVIN ) {
		File tabletFile = new File( workingDir, shortVIN + "_Tablet.txt" );
		BufferedReader bufferedReader = null;
		Map<String, String> tabletData = new HashMap<String, String>();

		try {
			bufferedReader = new BufferedReader( new InputStreamReader( new FileInputStream( tabletFile ), "UTF-8" ) );
			String[] lineElements = bufferedReader.readLine().split( "," );

			for( String lineElement : lineElements ) {
				String[] keyValues = lineElement.split( ":" );
				tabletData.put( keyValues[0].trim(), keyValues[1].trim() );
			}
		} catch( Exception ex ) {
			if( debug )
				ex.printStackTrace();
		} finally {
			if( bufferedReader != null ) {
				try {
					bufferedReader.close();
				} catch( IOException e ) {
					// Nothing
				}
			}
		}

		return tabletData;
	}

	/**
	 * Testmethode
	 * @param args
	 */
	public static void main( String args[] ) {
	}

}

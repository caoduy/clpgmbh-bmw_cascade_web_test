/*
 * StatisticDecide
 *
 * Created on 08.05.2008
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;


import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.variables.VariablesExceptionTypes;



/** Pr�ft, ob Datens�tze vorhanden sind
 *
 * @author Daniel Frey
 * @version V1_0_F  30.04.2008 FD Initialversion
 * @version V2_0_T  06.04.2009 FD Im Fehlerfall -> Alle Pr�fst�nde am 100% Test beteiligen (erfordert Cascade 3.0.0)
 * @version V3_0_F  06.04.2009 FD F-Version 
 * @version V4_0_T  02.02.2010 FD Optionaler Parameter DEBUG (wenn TRUE: DebugAusgaben in PruefstandScreen.log)
 * @version V5_0_F  02.02.2010 FD F-Version 
 * @version V6_0_T  06.05.2010 FD Optionaler Paremeter AFTER_ERROR_ONLY_LOCAL. Wenn true: 100% Tests nur an dem Pr�fstand durchf�hren, an dem der Fehler aufgetreten ist (keine Servervariable n�tig)
 * @version V7_0_F  11.05.2010 FD F-Version
 *  */
public class StatisticDecide_7_0_F_Pruefprozedur extends AbstractBMWPruefprozedur 
{
    /** */
    static final long serialVersionUID = 1L;    
    
    /**
     * DefaultKonstruktor, nur fuer die Deserialisierung
     */
    public StatisticDecide_7_0_F_Pruefprozedur() {}
    
    /** Erzeugt eine neue Pruefprozedur.
     * 
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public StatisticDecide_7_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) 
    {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    /**
     * initialsiert die Argumente
     */
    protected void attributeInit() 
    {
        super.attributeInit();
    }
           
    /**
     * liefert die zwingend erforderlichen Argumente
     */
    public String[] getRequiredArgs() 
    {
        String[] args = { "MODE", "ID" };
        return args;
    }
    
    
    /**
     * liefert die optionalen Argumente
     */
    public String[] getOptionalArgs() 
    {
        String[] args = 
        {
            "QUOTE",
            "TESTS_AFTER_ERROR",
            "DEBUG",
            "AFTER_ERROR_ONLY_LOCAL"
        };
        return args;
    }
    
    public boolean checkArgs() 
    {
        Vector errors = new Vector();
        return checkArgs(errors);
    }
    
    /** true: Debug Ausgaben */
    private boolean m_Debug;
    
    /** �berpr�ft die Argumente f�r diese PP
     * 
     * @param errors Fehlerliste
     * @return false: Argumente fehlerhaft, true: Argumente IO
     */
    public boolean checkArgs(Vector errors) 
    {
        Ergebnis result;
        String param_str;
        int    quote;
        
        try 
        {
            if (!super.checkArgs()) 
            {
                if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }

            param_str=getArg(getRequiredArgs()[1]);
            if (param_str==null) param_str="";
            if (param_str.length()==0)
            {
                if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", getRequiredArgs()[1]+" fehlt", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", getRequiredArgs()[0]+" is missing", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }
            
            param_str=getArg(getRequiredArgs()[0]);
            if (param_str==null) param_str="";
            if (param_str.equalsIgnoreCase("") || param_str.equalsIgnoreCase("DECIDE") || param_str.equalsIgnoreCase("TEST_IO"))
            {
                    // Parameter Ok;
            }
            else
            {
                if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterfehler", getRequiredArgs()[0]+" mu� 'DECIDE' oder 'TEST_IO' sein", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", getRequiredArgs()[0]+" must be 'DECIDE' or 'TEST_IO'", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }
            
            if (param_str.equalsIgnoreCase("TEST_IO")) 
            {
                int i;
                for (i=0; i<getOptionalArgs().length; i++)
                {
                    param_str=getArg(getOptionalArgs()[i]);
                    if (param_str==null) param_str="";
                    if (param_str.length()>0)
                    {
                        if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", getOptionalArgs()[i]+" nicht erlaubt bei MODE=TEST_IO", Ergebnis.FT_NIO_SYS );
                        else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", getOptionalArgs()[i]+" not allowed for MODE=TEST_IO", Ergebnis.FT_NIO_SYS );
                        errors.add(result);
                        return false;
                    }
                }
                return true;
            }
            
            param_str=getArg(getOptionalArgs()[0]);
            if (param_str==null) param_str="";
            if (param_str.endsWith("%")) param_str=param_str.substring(0, param_str.length()-1);
            if (param_str.length()<1)
            {
                if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", getOptionalArgs()[0]+" fehlt", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", getOptionalArgs()[0]+" is missing", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }
            
            try
            {
                quote=Integer.parseInt(param_str);
            }
            catch (NumberFormatException ex) 
            {
                if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterfehler", getOptionalArgs()[0]+"(="+param_str+") ist keine Zahl", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", getOptionalArgs()[0]+"(="+param_str+") is not a number", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }
            
            if (quote<1 || quote>100)
            {
                if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterfehler", getOptionalArgs()[0]+"(="+param_str+") mu� zwischen 1 und 100 liegen", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", getOptionalArgs()[0]+"(="+param_str+") must be a value between 1 and 100", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }

            
            param_str=getArg(getOptionalArgs()[1]);
            if (param_str==null) param_str="";
            if (param_str.length()<1)
            {
                if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", getOptionalArgs()[1]+" fehlt", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", getOptionalArgs()[1]+" is missing", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }
            
            try
            {
                Integer.parseInt(param_str);
            }
            catch (NumberFormatException ex) 
            {
                if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterfehler", getOptionalArgs()[1]+"(="+param_str+") ist keine Zahl", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", getOptionalArgs()[1]+"(="+param_str+") is not a number", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }
            
            param_str=getArg(getOptionalArgs()[3]);
            if (param_str==null) param_str="";
            if (!param_str.equalsIgnoreCase("") &&
                !param_str.equalsIgnoreCase("TRUE") &&
                !param_str.equalsIgnoreCase("FALSE"))
            {
                if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterfehler", getOptionalArgs()[3]+"(="+param_str+") muss TRUE, FALSE oder leer sein", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", getOptionalArgs()[3]+"(="+param_str+") must be TRUE, FALSE or empty", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }
            
            return true;
        } 
        catch (Exception e) 
        {
            return false;
        } 
        catch (Throwable e) 
        {
            return false;
        }
    }
    
    
    /** f�hrt die Pr�fprozedur aus
     * 
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) 
    {
    int status = STATUS_EXECUTION_OK;
    Vector ergListe = new Vector();
    String ID;
    String param_str, debug_str;
    boolean local_only;
        
        m_Debug=false;
        try
        {
            if (!checkArgs(ergListe))
            {
                setPPStatus( info, STATUS_EXECUTION_ERROR, ergListe );
                return;
            }

            debug_str=getArg(getOptionalArgs()[2]);
            if (debug_str!=null)
            {
                if (debug_str.equalsIgnoreCase("TRUE"))
                {
                    m_Debug=true;
                }
            }
            
            ID=getArg(getRequiredArgs()[1]);
            if (ID==null) ID="";
            if (ID.length()==0) ID="def";
            else
            {
                try
                {
                    ID=resolve(ID);
                }
                catch (PPExecutionException ex)
                {
                    Ergebnis result;
                    
                    if (isDE()) result = new Ergebnis( "ExecFehler", "Parameter", "", "", "", "", "", "", "", "0", "", "", "", ID, ex.getMessage(), Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "Parameter", "", "", "", "", "", "", "", "0", "", "", "", ID, ex.getMessage(), Ergebnis.FT_NIO_SYS );
                    
                    ergListe.add(result);
                    setPPStatus( info, STATUS_EXECUTION_ERROR, ergListe );
                    return;
                }
            }
            
            param_str=getArg("MEMORY_FILE_PATH");
            if (param_str==null) param_str="";
            if (param_str.length()>0)
            {
                if (isDE()) ergListe.add(new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "�berfl�ssiger Parameter", "MEMORY_FILE_PATH="+param_str, Ergebnis.FT_IGNORE ));
                else ergListe.add(new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Obsolete Parameter", "MEMORY_FILE_PATH="+param_str, Ergebnis.FT_IGNORE ));
            }
            
            param_str=getArg(getOptionalArgs()[3]);
            if (param_str==null) param_str="";
            local_only=param_str.equalsIgnoreCase("TRUE");
            
            /*
            if (param_str.length()==0)
            {
                int pos;
                MemoryFileName=com.bmw.cascade.distribution.DistributionConstants.PRUEFSTAND_DIRECTORY;
                if (MemoryFileName.endsWith(File.separator)) MemoryFileName=MemoryFileName.substring(0, MemoryFileName.length()-1);
                pos=MemoryFileName.lastIndexOf(File.separator);
                if (pos<0) MemoryFileName="C:";
                else MemoryFileName=MemoryFileName.substring(0, pos);
                MemoryFileName+=File.separator;
            }
            else
            {
                MemoryFileName=param_str;
                if (!MemoryFileName.endsWith(File.separator)) MemoryFileName+=File.separator;
            }
            MemoryFileName+="StatisticMemory_"+ID+".dat";
            */

            param_str=getArg(getRequiredArgs()[0]);
            if (param_str==null) param_str="";
            
            if (param_str.equalsIgnoreCase("TEST_IO"))
            {
                status=TestIO(ID, ergListe);
            }
            else
            {
                status=TestDecide(ID, local_only, ergListe);
            }
        } 
        catch (Exception e) 
        {
            System.out.println("StatisticDecide: "+e.getMessage());
            e.printStackTrace(); 
            ergListe.add(new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS ));
            status = STATUS_EXECUTION_ERROR;
        } 
        catch (Throwable e) 
        {
            System.out.println("StatisticDecide: "+e.getMessage());
            e.printStackTrace();
            ergListe.add(new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS ));
            status = STATUS_EXECUTION_ERROR;
        }
      
        setPPStatus( info, status, ergListe );
    }

    /** Statistic vor dem Testschritt: Pr�ft, ob Test notwendig
     * 
     * @param ID Statistic ID
     * @param ergListe CascadeResults
     * @param local_only true: 100% Countdown nur lokal z�hlen
     * @return Cascade Status
     */
    private int TestDecide(String ID, boolean local_only, Vector ergListe)
    {
    int status = STATUS_EXECUTION_OK;
    String param_str;
    int    quote, tests_after_error;
    TestHistory tHist;

        debugPrint("test decide");

        param_str=getArg(getOptionalArgs()[0]);
        if (param_str==null) param_str="";
        param_str=param_str.trim();
        if (param_str.endsWith("%")) param_str=param_str.substring(0, param_str.length()-1);
        param_str=param_str.trim();
        if (param_str.length()==0) param_str="10";
        
        try
        {
            quote=Integer.parseInt(param_str);
        }
        catch (NumberFormatException ex) 
        {
            Ergebnis result;
            if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", getOptionalArgs()[0]+" ist keine Zahl, nehme 100%", Ergebnis.FT_IGNORE );
            else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", getOptionalArgs()[0]+" is not a number, default to 100%", Ergebnis.FT_IGNORE );
            ergListe.add(result);
            quote=100;
        }
    
        param_str=getArg(getOptionalArgs()[1]);
        if (param_str==null) param_str="";
        param_str=param_str.trim();
        try
        {
            tests_after_error=Integer.parseInt(param_str);
        }
        catch (NumberFormatException ex) 
        {
            tests_after_error=10;
        }
        
        debugPrint("ID="+ID);
        debugPrint("Quote="+quote);
        debugPrint("Tests after error="+tests_after_error);
        debugPrint("LocalOnly="+local_only);
        
        tHist=new TestHistory();
    
        if (!tHist.canSkipTest(ID, quote, tests_after_error, local_only, ergListe))
        {
            status = STATUS_EXECUTION_ERROR;
        }

        return status;
    }
    
    /** Statistic nach dem durchgef�hrten Testschritt: Setzt Test auf IO
     * 
     * @param ID Statistic ID
     * @param ergListe CascadeResults
     * @return Cascade Status
     */
    private int TestIO(String ID, Vector ergListe)
    {
    int status = STATUS_EXECUTION_OK;
    TestHistory tHist;
    
        debugPrint("test IO");

        tHist=new TestHistory();
        if (!tHist.setLastTestIO(ID, ergListe))
        {
            status = STATUS_EXECUTION_ERROR;
        }
        return status;
    }
    
    /** Klasse, welche die "Vergangenheit" logt und analysiert und entscheided ob ein Test notwendig ist */
    private class TestHistory
    {
        /** Entscheidung ob ein Test notwendig ist
         * 
         * @param ID Test-ID
         * @param quote Statistische Quote in Prozent
         * @param tests_after_error Anzahl an Tests nach Fehler
         * @param local_only true: 100% Countdown nur lokal z�hlen
         * @param ergListe CascadeResults
         * @return true: Test kann �bersprungen werden, false: Test mu� gemacht werden (ergListe enth�lt mind. einen NIO Eintrag)
         */
        public boolean canSkipTest(String ID, int quote, int tests_after_error, boolean local_only, Vector ergListe)
        {
        boolean lastTestOk;
        int nr;
        String hist;
        double quote_d, quote_nt, quote_t, diff_t, diff_nt;
        int currentCarsDone, currentTestsDone;
        
            lastTestOk=wasLastTestOk(ID, ergListe);
        
            if (!lastTestOk)
            {
                startNew100PercentSequence(ID, tests_after_error, local_only, ergListe);
                return false;
            }
            
            nr=getAndDecrementNrOf100PercentTests(ID, tests_after_error, local_only, ergListe);
            
            if (nr>0)
            {
                start100PercenTest(ID, nr, tests_after_error, ergListe);
                return false;
            }
            
            hist=getTestHist(ID);
            debugPrint("Hist="+hist);
            
            currentCarsDone=getCurrentCarsDone(hist);
            currentTestsDone=getCurrentIOTestsDone(hist);
            
            //Soll quote
            quote_d=(double)quote/100.0;
            
            // Quote falls kein Test
            quote_nt=(double)currentTestsDone/(double)(currentCarsDone+1.0);
            debugPrint("Quote without Test="+quote_nt);
            
            // Quote falls Test
            quote_t= (double)(currentTestsDone+1.0)/(double)(currentCarsDone+1.0);
            debugPrint("Quote with Test="+quote_t);
            
            // Differenzen der Quote mit und ohne Test zur SollQuote berechnen.
            diff_nt=quote_d-quote_nt;
            if (diff_nt<0) diff_nt=-diff_nt;

            diff_t=quote_d-quote_t;
            if (diff_t<0) diff_t=-diff_t;
            
            // History auf die max. letzten 100 Zyklen beschr�nken
            while (hist.length()>=100) hist=hist.substring(1);
            
            // Wie kann die SollQuote besser erf�llt werden?
            if (diff_t<diff_nt)
            {
                // Die SollQuote wird besser erf�llt, wenn als n�chstes ein Test gemacht wird.
                startStatisticTest(hist, ID, ergListe);
                return false;
            }
            else
            {
                // Die SollQuote wirde besser erf�llt, wenn als n�chstes kein Test gemacht wird.
                return skipTest(hist, ID, ergListe);
            }
        }
        
        /** Der letzte Test ist schief gegangen, es mu� eine neue Volltestsequenz gestartet werden
         * 
         * @param ID Test-ID 
         * @param tests_after_error Anzahl an Tests die ab jetzt gemacht werden m�ssen
         * @param local_only true: 100% Countdown nur lokal z�hlen
         * @param ergListe CascadeResults (mind. ein NIO Eintrag wird gemacht) 
         */
        private void startNew100PercentSequence(String ID, int tests_after_error, boolean local_only, Vector ergListe)
        {
        boolean erg1, erg2, erg3;
        
            debugPrint("startNew100PercentSequence");

            erg1=setStatisticServerVar(ID, tests_after_error-1, local_only, ergListe);
            debugPrint("setServerVar="+erg1);
            
            erg2=setCurrentTestPending(ID, ergListe);
            debugPrint("setCurrentTestPending="+erg2);
            
            erg3=setStatisticLocalVar(ID, "H", "", ergListe);
            debugPrint("setStatisticLocalVar="+erg3);
            
            if (erg1 && erg2 && erg3)
            {
                if (isDE()) ergListe.add(new Ergebnis( "NeedTest", "", "", "", "", "", "", "", "", "0", "", "", "", "Starte neue Volltestsequenz mit "+tests_after_error+" Tests", "", Ergebnis.FT_NIO ));
                else        ergListe.add(new Ergebnis( "NeedTest", "", "", "", "", "", "", "", "", "0", "", "", "", "Starting new full test sequence with "+tests_after_error+" tests", "", Ergebnis.FT_NIO ));
            }
        }
        
        /** Wir befinden uns noch in einer Volltestsequenz, weil k�rzlich ein Test schief gegangen ist.
         * 
         * @param ID Test-ID 
         * @param counter Nummer des Tests in der Volltestsequenz
         * @param max Anzahl an Tests der Volltestsequenz
         * @param ergListe CascadeResults (mind. ein NIO Eintrag wird gemacht) 
         */
        private void start100PercenTest(String ID, int counter, int max, Vector ergListe)
        {
        boolean erg2, erg3;
        
            debugPrint("start100PercenTest");

            erg2=setCurrentTestPending(ID, ergListe);
            erg3=setStatisticLocalVar(ID, "H", "", ergListe);            
            
            if (erg2 && erg3)
            {
                if (isDE()) ergListe.add(new Ergebnis( "NeedTest", "", "", "", "", "", "", "", "", "0", "", "", "", "Starte Volltest "+counter+"/"+max, "", Ergebnis.FT_NIO ));
                else        ergListe.add(new Ergebnis( "NeedTest", "", "", "", "", "", "", "", "", "0", "", "", "", "Starting full test "+counter+"/"+max, "", Ergebnis.FT_NIO ));
            }
            
        }
        
        /** Vermerkt lokal am Pr�fstand, da� ein Test l�uft.
         * Wenn er IO ist, wird setLastTestIO im selben PU aufgerufen
         * 
         * @param ID Test-ID
         * @param ergListe Cascade-Result
         * @return true: Pr�fstandsvariable konnte gesetzt werden, false: Fehler beim Setzen der Pr�fstandsvariable  
         */
        private boolean setCurrentTestPending(String ID, Vector ergListe)
        {
            return setStatisticLocalVar(ID, "P", "P", ergListe);
        }
        
        /** Vermerkt lokal am Pr�fstand, da� ein Test �bersprungen wird.
         * 
         * @param ID Test-ID
         * @param ergListe Cascade-Result
         * @return true: Pr�fstandsvariable konnte gesetzt werden, false: Fehler beim Setzen der Pr�fstandsvariable  
         */
        private boolean setCurrentTestSkipped(String ID, Vector ergListe)
        {
            return setStatisticLocalVar(ID, "P", "S", ergListe);
        }
        
        /** Vermerkt lokal am Pr�fstand, da� ein Test IO bewertet wurde.
         * 
         * @param ID Test-ID
         * @param ergListe Cascade-Result
         * @return true: Pr�fstandsvariable konnte gesetzt werden, false: Fehler beim Setzen der Pr�fstandsvariable  
         */
        public boolean setLastTestIO(String ID, Vector ergListe)
        {
            debugPrint("setLastTestIO");
            
            return setStatisticLocalVar(ID, "P", "O", ergListe);
        }
        
        /** Pr�ft, ob der zuletzt ausgef�hrte Test IO (O) oder Skipped (S) war
         * 
         * @param ID Test-ID
         * @param ergListe Cascade-Result, falls Pr�fstandsvariable nicht gelesen werden konnte
         * @return true: Test war IO oder Skipped, false: Test war NIO 
         */
        private boolean wasLastTestOk(String ID, Vector ergListe)
        {
        String erg;
        
            erg=getStatisticLocalVar(ID, "P", ergListe);

            if (erg==null)
            {
                if (isDE()) ergListe.add(new Ergebnis( "Variables", "", "", "", "", "Letzer Test", "Kein Ergebnis", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ));
                else        ergListe.add(new Ergebnis( "Variables", "", "", "", "", "Last Test", "No Result", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ));
                return false;
            }

            if (erg.equalsIgnoreCase("O"))
            {
                if (isDE()) ergListe.add(new Ergebnis( "Variables", "", "", "", "", "Letzer Test", "IO", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ));
                else        ergListe.add(new Ergebnis( "Variables", "", "", "", "", "Last Test", "IO", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ));
            }
            else if (erg.equalsIgnoreCase("S"))
            {
                if (isDE()) ergListe.add(new Ergebnis( "Variables", "", "", "", "", "Letzer Test", "�bersprungen", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ));
                else        ergListe.add(new Ergebnis( "Variables", "", "", "", "", "Last Test", "Skipped", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ));
            }
            else
            {
                if (isDE()) ergListe.add(new Ergebnis( "Variables", "", "", "", "", "Letzer Test", "NIO", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ));
                else        ergListe.add(new Ergebnis( "Variables", "", "", "", "", "Last Test", "NIO", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ));
            }
            
            return erg.equalsIgnoreCase("O") || erg.equalsIgnoreCase("S"); // O=Letzter Test IO, S=Letzter Test �bersprungen
        }
        
        /** Holt die Liste der vergangenen statistischen Tests (+ durchgef�hrt, - skipped)
         * 
         * @param ID Test-ID
         * @return Testhistorie (String aus + (durchgef�hrt) und - (skipped))
         */
        private String getTestHist(String ID)
        {
        String erg;
        
            erg=getStatisticLocalVar(ID, "H", new Vector());
            if (erg==null) erg="";
            return erg;
        }
        
        /** Ermittelt, wieviele PUs durchgelaufen sind (in der statistic Phase)
         * 
         * @param hist Testhistorie (String aus + (durchgef�hrt) und - (skipped))
         * @return Anzahl der PU durchl�ufe insgesammt
         */
        private int getCurrentCarsDone(String hist)
        {
            if (hist==null) return 0;
            return hist.length();
        }
        
        /** Ermittelt, wie oft Tests gemacht wurden (in der statistic Phase)
         * 
         * @param hist Testhistorie (String aus + (durchgef�hrt) und - (skipped))
         * @return Anzahl der PU Durchl�ufe mit Test
         */
        private int getCurrentIOTestsDone(String hist)
        {
        int i, Tests;
        
            if (hist==null) return 0;
            
            Tests=0;
            
            for (i=0; i<hist.length(); i++)
            {
                if (hist.charAt(i)=='+') Tests++;
            }
            return Tests;
        }

        /** Es wird ein Test in der statistic Phase begonnen
         * 
         * @param hist Bisherige Testhistorie (String aus + (durchgef�hrt) und - (skipped)), wird um ein + erweitert und gespeichert
         * @param ID Test-ID
         * @param ergListe CascadeResults (mind. ein NIO Eintrag wird gemacht)
         */
        private void startStatisticTest(String hist, String ID, Vector ergListe)
        {
        boolean erg1, erg2;
        
            debugPrint("startStatisticTest");
            erg1=setStatisticLocalVar(ID, "H", hist+"+", ergListe);
            erg2=setCurrentTestPending(ID, ergListe);
            
            if (erg1 && erg2)
            {
                if (isDE()) ergListe.add(new Ergebnis( "NeedTest", "", "", "", "", "", "", "", "", "0", "", "", "", "Statistikentscheidung: Test ben�tigt", "", Ergebnis.FT_NIO ));
                else        ergListe.add(new Ergebnis( "NeedTest", "", "", "", "", "", "", "", "", "0", "", "", "", "Statistic decision: Test needed", "", Ergebnis.FT_NIO ));
            }
        }
        
        /** Es wird ein Test in der statistic Phase �bersprungen
         * 
         * @param hist Bisherige Testhistorie (String aus + (durchgef�hrt) und - (skipped)), wird um ein - erweitert und gespeichert
         * @param ID Test-ID
         * @param ergListe CascadeResults
         * @return true: Pr�fstandsvariable konnte gesetzt werden, false: Fehler beim Setzen der Pr�fstandsvariable
         */
        private boolean skipTest(String hist, String ID, Vector ergListe)
        {
        boolean erg1, erg2;
            
            debugPrint("skipTest");
            erg1=setStatisticLocalVar(ID, "H", hist+"-", ergListe);
            erg2=setCurrentTestSkipped(ID, ergListe);
            
            if (erg1 && erg2)
            {
                if (isDE()) ergListe.add(new Ergebnis( "NeedNoTest", "", "", "", "", "", "", "", "", "0", "", "", "", "", "Statistikentscheidung: Kein Test ben�tigt", Ergebnis.FT_IO ));
                else        ergListe.add(new Ergebnis( "NeedNoTest", "", "", "", "", "", "", "", "", "0", "", "", "", "", "Statistic decision: No test needed", Ergebnis.FT_IO ));                
            }
            
            return erg1 && erg2;
        }

        /** Speichert etwas lokal, persistent am Pr�fstand
         * 
         * @param ID Test-ID
         * @param SubID UnterId 
         *        pro Test-ID m�ssen wir uns 3 Werte merken:
         *        SubID=F: Countdown der 100% Tests 
         *        SubID=P: Den Status des letzten Tests IO (O), PENDING=NIO (P), SKIPPED (S)
         *        SubID=H: Die Historie der durchgef�hrten (+) und skipped Tests (-)
         * @param val Variablenwert
         * @param ergListe Cascade-Results
         * @return true: Pr�fstandsvariable konnte gesetzt werden, false: Fehler beim Setzen der Pr�fstandsvariable
         */
        private boolean setStatisticLocalVar(String ID, String SubID, String val, Vector ergListe)
        {
        String varname="STATISTIC_"+ID+"_"+SubID;

            try
            {
                getPr�flingLaufzeitUmgebung().setPruefstandVariable(VariableModes.PERSISTENT, varname, val);
                return true;
            }
            catch (VariablesException ex)
            {
                if (isDE()) ergListe.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Konnte lokale Variable "+varname+" nicht setzen", ex.getMessage(), Ergebnis.FT_NIO ));
                else        ergListe.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not set local Variable "+varname, ex.getMessage(), Ergebnis.FT_NIO ));
            }
            return false;
        }
        
        /** Holt eine Variable lokal vom Pr�fstand
         * 
         * @param ID Test-ID
         * @param SubID UnterID
         * @param ergListe Cascade-Results
         * @return VariablenInhalt, null: Wenn Variable nicht gefunden
         */
        private String getStatisticLocalVar(String ID, String SubID, Vector ergListe)
        {
            Object pr_var;
            String varname="STATISTIC_"+ID+"_"+SubID;
            
            try
            {
                pr_var=getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PERSISTENT, varname);
                if (pr_var!=null) 
                {
                    if (pr_var instanceof String) 
                    {
                        return (String)pr_var;
                    }
                }
                if (isDE()) ergListe.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Lokale variable "+varname+" nicht gefunden", "", Ergebnis.FT_NIO ));
                else        ergListe.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not find local variable "+varname, "", Ergebnis.FT_NIO ));
            }
            catch (VariablesException ex)
            {
                if (isDE()) ergListe.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Lokale variable "+varname+" nicht gefunden", ex.getMessage(), Ergebnis.FT_NIO ));
                else ergListe.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not get local variable "+varname, ex.getMessage(), Ergebnis.FT_NIO ));
            }
            return null;
        }
        
        /** Versuche f�r 5sec eine Servervariable zu locken
         * 
         * @param VarName Variablenname
         * @return true: Lock erhalten, false: Lock nach 5sec nicht erhalten
         * @throws VariablesException Fehler bei Zugriff auf Server
         */
        private boolean lockStatisticServerVar(String VarName) throws VariablesException
        {
        boolean locked;
        long    starttime, nowtime;
            
            starttime=System.currentTimeMillis();
            while (true)
            {
                locked=getPr�flingLaufzeitUmgebung().lockServerVariable(VarName);
                if (locked)
                {
                    debugPrint(VarName +" got lock");
                    return true;
                }

                debugPrint(VarName+" got NO lock");
                try
                {
                    Thread.sleep(50);
                }
                catch (InterruptedException e) { }
                nowtime=System.currentTimeMillis();
                if (nowtime-starttime>5000)
                {
                    debugPrint(VarName+" timeout (5 sec) at get lock");
                    return false;
                }
            }
        }
        
        /** Setzt einen Wert in einer Variable auf dem Server und zur Sicherheit auch noch lokal
         * 
         * @param VarName Name der Variable
         * @param Val Integer Wert
         * @param local_only true: Nur lokale Pr�fstandsvariable setzen, false: Server und lokale Variable setzen
         * @param ergList CascadeResults
         * @return true: Variablen konnte gesetzt, false: Fehler beim Setzen
         */
        private boolean setStatisticServerVar(String VarName, int Val, boolean local_only, Vector ergList)
        {
        boolean erg1, erg2;
        boolean locked;
        boolean setsuccessfull;
        String str;
        
            if (local_only)
            {
                setsuccessfull=true;
                erg1=true;
            }
            else
            {
                setsuccessfull=false;
                try
                {
                    locked=lockStatisticServerVar(VarName);
                    if (!locked)
                    {
                        debugPrint("Could not lock server variable "+VarName);
                        if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Servervariable "+VarName+" konnte nicht gelockt werden", "Timeout", Ergebnis.FT_NIO ));
                        else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not lock server variable "+VarName, "Timeout", Ergebnis.FT_NIO ));
                    }
                }
                catch (VariablesException ex)
                {
                    debugPrint("VariablesException: "+ex.getClass()+getName()+" "+ex.getMessage()+"\r\nex.iExeptionType="+ex.iExeptionType);
                    String m=ex.getMessage();
                    if (m==null) m="";
                    if (ex.iExeptionType==VariablesExceptionTypes.NO_SUCH_VARIABLE || m.indexOf("UnknownVariableException")>=0)
                    {
                        debugPrint("Could not find server variable "+VarName);
                        if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Servervariable "+VarName+" nicht gefunden", ex.getMessage(), Ergebnis.FT_NIO_SYS ));
                        else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not find server variable "+VarName, ex.getMessage(), Ergebnis.FT_NIO_SYS ));
                        return false;
                    }
                    locked=false;
                    debugPrint("Could not lock server variable "+VarName+". ");
                    if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Servervariable "+VarName+" konnte nicht gelockt werden", ex.getMessage(), Ergebnis.FT_NIO ));
                    else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not lock server variable "+VarName, ex.getMessage(), Ergebnis.FT_NIO ));
                }
                catch (NoSuchMethodError ex2)
                {
                    locked=false;
                    debugPrint("Could not lock server variable "+VarName);
                    if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Servervariable "+VarName+" konnte nicht gelockt werden", "NoSuchMethodError: "+ex2.getMessage(), Ergebnis.FT_NIO_SYS ));
                    else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not lock server variable "+VarName, "NoSuchMethodError: "+ex2.getMessage(), Ergebnis.FT_NIO_SYS ));
                }
                erg1=locked;
                
                if (locked)
                {
                    try
                    {
                        getPr�flingLaufzeitUmgebung().setServerVariable(VarName, ""+Val);
                        setsuccessfull=true;
                    }
                    catch (VariablesException ex)
                    {
                        erg1=false;
                        debugPrint("Could not set server variable "+VarName);
                        if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Servervariable "+VarName+" konnte nicht gesetzt werden", ex.getMessage(), Ergebnis.FT_NIO ));
                        else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not set server variable "+VarName, ex.getMessage(), Ergebnis.FT_NIO ));
                    }
                    
                    try
                    {
                        if (!getPr�flingLaufzeitUmgebung().unlockServerVariable(VarName))
                        {
                            erg1=false;
                            debugPrint("Could not unlock server variable "+VarName);
                            if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Konnte lock nicht aufheben bei Servervariable "+VarName, "Lock war nicht vorhanden", Ergebnis.FT_NIO ));
                            else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not unlock server variable "+VarName, "Had not lock", Ergebnis.FT_NIO ));
                        }
                        else
                        {
                            debugPrint("VarName unlocked");
                        }
                    }
                    catch (VariablesException ex)
                    {
                        erg1=false;
                        debugPrint("Could not unlock server variable "+VarName);
                        if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Konnte lock nicht aufheben bei Servervariable "+VarName, ex.getMessage(), Ergebnis.FT_NIO ));
                        else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not unlock server variable "+VarName, ex.getMessage(), Ergebnis.FT_NIO ));
                    }
                }
            }
            
            str=""+Val;
            if (!setsuccessfull) str+="_";
            erg2=setStatisticLocalVar(VarName, "F", str, ergList);

            debugPrint("setStatisticServerVar erg1="+erg1+",  erg2="+erg2);

            return erg1 && erg2;
        }
        
        /** Holt eine Variable vom Server. Wenn >0: zieht 1 ab, und speichert sie auf dem Server
         * 
         * @param VarName Name der Variable 
         * @param tests_after_error Anzahl an Tests nach Fehler. Wenn eine leere Server Variable gefunden wird, so wird auch die 100% Testsequenz gestartet
         * @param local_only true: Nur lokale Pr�fstandsvariable, false: Server und lokale Variable
         * @param ergList CascadeResults 
         * @return Wert der Variable vor dem abziehen
         */
        private int getAndDecrementNrOf100PercentTests(String VarName, int tests_after_error, boolean local_only, Vector ergList)
        {
        String val;
        int val_int_local, val_int_server;
        int val_old, val_new;
        boolean val_local_io, val_local_override, val_server_io;
        boolean setsuccessfull;
        boolean locked;
        String str;
        
        
            // Erst mal schauen, was in der lokalen Variable steht
            // Fehler werden in APDM gespeichert und ignoriert
            val_local_io=false;
            val_local_override=false;
            val_int_local=tests_after_error;
            val=getStatisticLocalVar(VarName, "F", ergList);
            ergList.add(new Ergebnis( "Variables", "", "", "", "", "Local variable "+VarName+"_F", ""+val, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ));
        
            if (val==null)
            {
                if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Leere lokale Variable "+VarName, "", Ergebnis.FT_IGNORE ));
                else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Empty local variable "+VarName, "", Ergebnis.FT_IGNORE ));
            }
            else
            {
                try
                {
                    if (val.endsWith("_"))
                    {
                        val=val.substring(0, val.length()-1);
                        val_local_override=true;
                    }
                    val_int_local=Integer.parseInt(val);
                    val_local_io=true;
                }
                catch (NumberFormatException ex)
                {
                    if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Lokale Variable "+VarName+"="+val+" ist keine Zahl", "", Ergebnis.FT_IGNORE ));
                    else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Local variable "+VarName+"="+val+" is not a number", "", Ergebnis.FT_IGNORE ));
                }
            }
        

            if (local_only)
            {
                if (val_local_io) val_old=val_int_local;
                else              val_old=tests_after_error;
                
                if (val_old>0)
                {
                    val_new=val_old-1;
                    str=""+val_new;
                    
                    setStatisticLocalVar(VarName, "F", str, new Vector());
                }
                
                return val_old;
            }
            
            
            // Jetzt schauen, was in der Servervariable steht
            val_server_io=false;
            val_int_server=tests_after_error;
            try
            {
                locked=lockStatisticServerVar(VarName);
                if (!locked)
                {
                    if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Servervariable "+VarName+" konnte nicht gelockt werden", "Timeout", Ergebnis.FT_IGNORE ));
                    else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not lock server variable "+VarName, "Timeout", Ergebnis.FT_IGNORE ));
                }
                
                /*
                try
                {
                    int ToDo_OnlyForLockTest_RemoveMe;
                    Thread.sleep(2000);
                }
                catch (InterruptedException e) { }
                */
            }
            catch (VariablesException ex)
            {
                debugPrint("VariablesException: "+ex.getClass()+getName()+" "+ex.getMessage()+"\r\nex.iExeptionType="+ex.iExeptionType);
                String m=ex.getMessage();
                if (m==null) m="";
                if (ex.iExeptionType==VariablesExceptionTypes.NO_SUCH_VARIABLE || m.indexOf("UnknownVariableException")>=0)
                {
                    if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Servervariable "+VarName+" nicht gefunden", ex.getMessage(), Ergebnis.FT_NIO_SYS ));
                    else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not find server variable "+VarName, ex.getMessage(), Ergebnis.FT_NIO_SYS ));
                    return tests_after_error; // Server ist, aber Variable nicht angelegt -> Immer Fehler, d.h. TestNeeded. Werksbetreuer mu� Variable anlegen. 
                }
                
                locked=false;
                if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Servervariable "+VarName+" konnte nicht gelockt werden", ex.getMessage(), Ergebnis.FT_IGNORE ));
                else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not lock server variable "+VarName, ex.getMessage(), Ergebnis.FT_IGNORE ));
            }
            catch (NoSuchMethodError ex2)
            {
                locked=false;
                if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Servervariable "+VarName+" konnte nicht gelockt werden", "NoSuchMethodError: "+ex2.getMessage(), Ergebnis.FT_NIO ));
                else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not lock server variable "+VarName, "NoSuchMethodError: "+ex2.getMessage(), Ergebnis.FT_NIO ));
            }
            
            if (locked)
            {
                try
                {
                    val=getPr�flingLaufzeitUmgebung().getServerVariable(VarName);
                    ergList.add(new Ergebnis( "Variables", "", "", "", "", "Server variable "+VarName, ""+val, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ));
                    
                    if (val==null)
                    {
                        if (isDE()) ergList.add(new Ergebnis( "Variables", "", "", "", "", "", "", "", "", "0", "", "", "", "Leere Servervariable "+VarName, "", Ergebnis.FT_NIO ));
                        else        ergList.add(new Ergebnis( "Variables", "", "", "", "", "", "", "", "", "0", "", "", "", "Empty server variable "+VarName, "", Ergebnis.FT_NIO ));
                        // Leere Server Variable -> Volltestsequenz
                        val_int_server=tests_after_error;
                        val_server_io=true;
                    }
                    else
                    {
                        try
                        {
                            val_int_server=Integer.parseInt(val);
                            val_server_io=true;
                        }
                        catch (NumberFormatException ex)
                        {
                            if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Servervariable "+VarName+"="+val+" ist keine Zahl", "", Ergebnis.FT_NIO ));
                            else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Server variable "+VarName+"="+val+" is not a number", "", Ergebnis.FT_NIO ));
                            // Fehlerhafte Server Variable -> Volltestsequenz
                            val_int_server=tests_after_error;
                            val_server_io=true;
                        }
                    }
                }
                catch (VariablesException ex)
                {
                    if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Konnte Servervariable "+VarName+" nicht holen", ex.getMessage(), Ergebnis.FT_NIO ));
                    else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not get server variable "+VarName, ex.getMessage(), Ergebnis.FT_NIO ));
                    // Server Variable konte gelockt werden aber nicht gelesen??? -> Volltestsequenz
                    val_int_server=tests_after_error;
                    val_server_io=true;
                }
            }
            
            if (val_local_io && val_local_override && val_server_io)
            {
                // val_local gewinnt wenn das Override Flag (_) gesetzt ist (und es gr��er ist als die Servervariable)
                // Dies ist der Fall, wenn ein Testfehler aufgetreten ist und die Servervariable nicht gesetzt werden konnte
                val_old=val_int_local>val_int_server ? val_int_local : val_int_server; 
            }
            else if (val_server_io)
            {
                // Normalerweise gewinnt die Servervariable
                val_old=val_int_server;
            }
            else if (val_local_io)
            {
                // Wenn die Servervariable nicht geholt werden konnte, gewinnt die lokale Variable
                val_old=val_int_local;
            }
            else 
            {
                // Weder Lokal noch auf dem Server etwas gefunden: Volltest
                val_old=tests_after_error;
            }
            
            if (val_old>0)
            {
                val_new=val_old-1;
                str=""+val_new;
                
                setsuccessfull=false;
                if (locked)
                {
                    try
                    {
                        getPr�flingLaufzeitUmgebung().setServerVariable(VarName, str);
                        setsuccessfull=true;
                    }
                    catch (VariablesException ex)
                    {
                        if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Konnte Servervariable "+VarName+" nicht setzen", ex.getMessage(), Ergebnis.FT_IGNORE ));
                        else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not set server variable "+VarName, ex.getMessage(), Ergebnis.FT_IGNORE ));
                    }
                }
                
                if (!setsuccessfull && val_local_override && val_new>0) str+="_"; // Solange die Servervariable nicht gesetzt werden kann, bleibt das Override Flag bestehen
                
                // Variable sicherheitshalber auch lokal speichern
                setStatisticLocalVar(VarName, "F", str, new Vector());
            }
            
            if (locked)
            {
                try
                {
                    if (!getPr�flingLaufzeitUmgebung().unlockServerVariable(VarName))
                    {
                        if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Konnte lock nicht aufheben bei Servervariable "+VarName, "Lock war nicht vorhanden", Ergebnis.FT_IGNORE ));
                        else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not unlock server variable "+VarName, "Had not lock", Ergebnis.FT_IGNORE ));
                    }
                    else
                    {
                        debugPrint("VarName unlocked");
                    }
                }
                catch (VariablesException ex)
                {
                    if (isDE()) ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Konnte lock nicht aufheben bei Servervariable "+VarName, ex.getMessage(), Ergebnis.FT_IGNORE ));
                    else        ergList.add(new Ergebnis( "VariablesException", "", "", "", "", "", "", "", "", "0", "", "", "", "Could not unlock server variable "+VarName, ex.getMessage(), Ergebnis.FT_IGNORE ));
                }
            }

            return val_old;
        }
    }
    
    /** Falls ein Parameter in @ angegebn ist, so versucht diese Funktion den Wert aufzul�sen,
     * indem sie nach einem Pr�flingsattribut oder einer Pr�fstandsvariable diese Names sucht und den Inhalt zur�ckgibt.
     * Ist 'value' nicht in @, so wird der Wert unver�ndert zur�ckgegeben.
     * Existiert sowohl ein Pr�flingsattribut als auch eine Pr�fstandsvariable so wird das Pr�flingsattribut genommen.
     * 
     * @param value auzul�sender Parameterwert
     * @return aufgel�ster Parameterwert
     * @throws PPExecutionException Fehler
     */
    private String resolve(String value) throws PPExecutionException
    {
    Pruefling pl;
    String    erg;
    
        erg=value;
        if (value.startsWith("@"))
        {
            value=value.substring(1);
            if (value.endsWith("@")) value=value.substring(0, value.length()-1);
        
            pl = getPr�fling();   //get the reference to the storing PL (use current PL)
            
            if (pl == null) 
            {
                System.out.println("StatisticDecide.resolve: PL not found");
                //throw an exeception if storing PL is null
                if (isDE()) throw new PPExecutionException("StatisticDecide.resolve: PL nicht vorhanden");
                else throw new PPExecutionException("StatisticDecide.resolve: PL not found");
            }

            try 
            {
                //get the hashtable of the pp where the arguments are stored
                Map ht = pl.getAllAttributes();
                if (ht == null) 
                {
                    //thow an exception if ht is null
                    if (isDE()) throw new PPExecutionException("StatisticDecide.resolve: Hashtable nicht vorhanden");
                    else throw new PPExecutionException("StatisticDecide.resolve: Hashtable not found");
                }
                Object temp = ht.get(value);    //get the value assigned to label (in the hashtable)
                if (temp == null) temp="";  
                else if (!(temp instanceof String)) temp=""; 
                erg=(String)temp;
                
                if (erg.length()==0)
                {
                    try 
                    {
                        Object pr_var;
     
                        pr_var=getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PS_CONFIG, value);
                        if (pr_var!=null) 
                        {
                            if (pr_var instanceof String) 
                            {
                                erg=(String)pr_var;
                            }
                        }
                    }
                    catch (VariablesException e) { }
                }
            } 
            catch (PPExecutionException ppe) 
            {
                throw ppe;
            } 
            catch (Exception e) 
            {
                //throw an exception if something unknown/unexpected went wrong
                if (isDE()) throw new PPExecutionException("StatisticDecide.resolve: Unerwarteter Fehler");
                else throw new PPExecutionException("StatisticDecide.resolve: Unknown Exception");
            }
        }
        return erg;
    }
    
    
    /** Debugausgabe
     * 
     * @param str Debugstring
     */
    private void debugPrint(String str)
    {
        if (m_Debug) System.out.println(str);
    }
    
    
    /** Ermittelt Sprache
     * 
     * @return true: Deutsch, false: Englisch
     */
    private static boolean isDE() 
    {
        try 
        {
            if (CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true ) return true;
            else return false;
        }
        catch (Exception e) 
        {
            return false;   //default is english
        }
    }
}


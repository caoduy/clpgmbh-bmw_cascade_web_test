/*
 * AndiConsole_1_0_F_Pruefprozedur.java
 *
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;
import com.bmw.cascade.util.dynamicdevices.PropertyObject;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.devices.DynamicDevice;
import com.bmw.cascade.devices.DynamicDeviceManager;
import com.bmw.cascade.devices.NoSuchDeviceException;

/**
 * PP zum starten/stoppen der Andi Console<BR>
 * <BR>
 * 
 * @author Fabian Sch�nert<BR>
 * @version 1_0_F 22.04.2015 FS Initial PP <BR>
 *          2_0_F 30.11.2015 TB AndiConsole.exe hat akt. noch die Usch�nheit, dass kein korrekter R�ckgabewert kommt. Deshalb es auch als i.O. bewertet, wenn keine Fehlerr�ckgabeobjekte kommen. <BR>
 *			3_0_T 03.06.2015 MK neue Aktion QUICK hinzugef�gt.<BR>
 *			4_0_F 15.06.2015 MK F-Version<BR>
 *			5_0_T 23.02.2017 MK Parameter f�r Debug Ausgabe hinzugef�gt.<BR>
 *			6_0_F 29.06.2017 MK Debug Ausgaben entfernt<BR>
 **/
public class ANDiConsole_6_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	boolean debug = false;
	boolean additional_args = false;
	boolean watchdog_limit_enable = false;
	boolean xml_output_enable = false;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public ANDiConsole_6_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling
	 *            Klasse des zugeh. Pr�flings
	 * @param pruefprozName
	 *            Name der Pr�fprozedur
	 * @param hasToBeExecuted
	 *            Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public ANDiConsole_6_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "CONFIG_FILE", // 0
		"GATEWAY", // 1
		"SINK_IP", // 2
		"WATCHDOG_LIMIT", // 3
		"XML_OUTPUT", // 4
		"XML_TEMPLATE", // 5
		"DEBUG" // 6
		};
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "ACTION" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	@Override
	public boolean checkArgs() {
		int i;
		String tmp;
		attributeInit();
		try {
			String[] requiredArgs = getRequiredArgs();
			String[] optionalArgs = getOptionalArgs();

			//
			// 0. Check: Debug?
			//
			debug = getArg( optionalArgs[6] ).equalsIgnoreCase( "TRUE" ) ? true : false;

			//
			// 1. Check: Required arguments
			//

			// All required Args set?

			for( i = 0; i < requiredArgs.length; i++ ) {
				if( getArg( requiredArgs[i] ) == null )
					return false;
				if( getArg( requiredArgs[i] ).equals( "" ) )
					return false;
			}
			if( debug )
				System.out.println( "1. Check done" );

			// For Stopping AndiConsole additionalArgs are optional
			if( getArg( optionalArgs[0] ) != null )
				additional_args = true;
			if( getArg( "ACTION" ).equalsIgnoreCase( "STOP" ) ) {
				if( !additional_args ) {
					// -> No Further Arguments needed
					if( debug )
						System.out.println( "checkargs success" );
					return true;
				}
			} else if( !getArg( "ACTION" ).equalsIgnoreCase( "STOPXCP" ) && !getArg( "ACTION" ).equalsIgnoreCase( "START" ) && !getArg( "ACTION" ).equalsIgnoreCase( "QUICK" ) ) {
				// Action can only be START, STOP or STOPXCP
				if( debug )
					System.out.println( "Checkargs Error: ACTION can only be START, QUICK, STOP or STOPXCP" );
				return false;
			}

			//
			// 2. Check: Verpflichtende Argumente f�r Start gesetzt?
			//

			// Check Config File
			tmp = getArg( optionalArgs[0] );
			if( tmp != "" && tmp != null ) {
				File f = new File( tmp );
				if( !f.exists() || f.isDirectory() ) {
					return false;
				}
			} else {
				return false;
			}

			// Check Gateway
			tmp = getArg( optionalArgs[1] );
			if( tmp != "" && tmp != null ) {
				tmp = tmp.substring( 0, tmp.lastIndexOf( ':' ) ); // Cut off port
				if( (tmp.length() - tmp.replace( ".", "" ).length()) == 3 ) { // contains exactly 3 dots -> IP address
					// Gateway is IP address, no further checks
				} else {
					// Check if Gateway File exists
					File f = new File( tmp );
					if( !f.exists() || f.isDirectory() ) {
						return false;
					}
				}

			} else {
				return false;
			}

			// Check Sink IP
			tmp = getArg( optionalArgs[2] );
			if( tmp == "" || tmp == null ) {
				return false;
			}

			if( debug )
				System.out.println( "2. Check done" );

			//
			// 3. Check: Watchdog Limit gesetzt?
			//
			tmp = getArg( optionalArgs[3] );
			if( tmp != "" && tmp != null ) {
				Integer.parseInt( getArg( optionalArgs[3] ) );
				watchdog_limit_enable = true;
			}

			if( debug )
				System.out.println( "3. Check done" );

			//
			// 4. Check: Argumente f�r XML Output gesetzt?
			//

			//Setzen von XML Output verpflichtet zu einem g�ltigen XML Template
			tmp = getArg( optionalArgs[4] );
			if( tmp != "" && tmp != null ) {
				// XML Datei soll geschrieben werden
				tmp = getArg( optionalArgs[5] );
				if( tmp != "" && tmp != null ) {
					// Template existiert?
					File f = new File( tmp );
					if( !f.exists() || f.isDirectory() ) {
						return false;
					}
					xml_output_enable = true;
				} else {
					return false;
				}
			}
			if( debug )
				System.out.println( "4. Check done" );

			//
			// Tests bestanden, somit ok
			//

			return true;

		} catch( Exception e ) {
			System.out.println( "checkArgs() exception " + e.getMessage() );
			return false;
		} catch( Throwable e ) {
			System.out.println( "checkArgs() exception " + e.getMessage() );
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info
	 *            Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;
		String configFile, gateway, sinkIP;
		try {
			// check args
			if( checkArgs() == false )
				throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

			// get dynamic Device
			DynamicDeviceManager deviceMgr = this.getPr�flingLaufzeitUmgebung().getDynamicDeviceManager();
			DynamicDevice Andi = deviceMgr.requestDevice( "ANDi" );

			// fill execution arguments
			List<PropertyObject> arguments = new ArrayList<PropertyObject>();
			if( getArg( "ACTION" ).equalsIgnoreCase( "STOP" ) ) {
				// Stop ANDi Console
				arguments.add( new PropertyObject( "--stop" ) );
			}
			if( getArg( "ACTION" ).equalsIgnoreCase( "START" ) ) {
				//START ANDi Console
				arguments.add( new PropertyObject( "--start" ) );
			} else if( getArg( "ACTION" ).equalsIgnoreCase( "STOPXCP" ) ) {
				// STOP XCP Session only
				arguments.add( new PropertyObject( "--stopxcp" ) );
			} else if( getArg( "ACTION" ).equalsIgnoreCase( "QUICK" ) ) {
				// QUICK ANDi Console
				arguments.add( new PropertyObject( "--quick" ) );
			}

			// Additional arg optional for STOP and required for other actions
			if( additional_args ) {
				configFile = getArg( getOptionalArgs()[0] );
				arguments.add( new PropertyObject( "-c" ) );
				arguments.add( new PropertyObject( configFile ) );
				gateway = getArg( getOptionalArgs()[1] );
				arguments.add( new PropertyObject( "-t" ) );
				arguments.add( new PropertyObject( gateway ) );
				sinkIP = getArg( getOptionalArgs()[2] );
				arguments.add( new PropertyObject( "-s" ) );
				arguments.add( new PropertyObject( sinkIP ) );
			}

			// Optional Arguments
			if( watchdog_limit_enable ) {
				arguments.add( new PropertyObject( "-w" ) );
				arguments.add( new PropertyObject( getArg( getOptionalArgs()[3] ) ) );
			}
			if( xml_output_enable ) {
				arguments.add( new PropertyObject( "-XML" + getArg( getOptionalArgs()[4] ) ) );
				arguments.add( new PropertyObject( "-DTD" + getArg( getOptionalArgs()[5] ) ) );
				arguments.add( new PropertyObject( "-FGNR" + this.getPr�fling().getAuftrag().getFahrgestellnummer7() ) );
			}

			// additionial output
			if( debug )
				arguments.add( new PropertyObject( "-v" ) ); // verbose

			// execution
			List<PropertyObject> output = Andi.execute( "andiWithConsoleOutput", arguments );

			if( debug ) {
				// ganze Konsolenausgabe ausgeben
				for( PropertyObject element : output )
					System.out.println( element.getValue() );
			}

			int returnValue = Integer.parseInt( output.get( 0 ).getValue() ); // return Wert von ANDiConsole
			if( returnValue == 0 || output.size() == 0 ) { // Fehlerfrei (returnValue == 0 oder keine Fehler beim return)
				result = new Ergebnis( "Output", "ANDiConsole", "", "", "", "OK", "OK", "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );
			} else { // Fehler 
				status = STATUS_EXECUTION_ERROR;
				for( PropertyObject element : output ) {
					String outputString = element.getValue();
					if( outputString.contains( "ERRORS:" ) ) {
						String errorString = outputString.substring( outputString.indexOf( "ERRORS:" ), outputString.length() );
						result = new Ergebnis( "Output", "ANDiConsole", "", "", "", "NOK", "OK", "", "", "", "", "", "", errorString, "", Ergebnis.FT_NIO );
						ergListe.add( result );
					}
				}
			}
		} catch( NoSuchDeviceException e ) {
			if( debug ) {
				e.printStackTrace();
			}
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( PPExecutionException e ) {
			if( debug ) {
				e.printStackTrace();
			}
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			if( debug ) {
				e.printStackTrace();
			}
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			if( debug ) {
				e.printStackTrace();
			}
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}
		setPPStatus( info, status, ergListe );
	}

}

/*
 * SDiagVisualCheck Pruefprozedur.java
 *
 * Created on 28.07.13
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Spezifische PP f�r das FT Energie. <BR>
 * Implementierung einer neuer Funktionalit�t zur Aktivierung und Deaktivierung der Ambiente Leuchten. <BR> * 
 * <BR>
 * Anforder: FT Energie, 28.09.2015, LOP 1993
 * <BR>
 * @author Saller <BR>
 * @version 1_0_F	29.09.2015  MS Erstimplementierung <BR>
 * @version 2_0_F	29.09.2015  MS Bugfix <BR>
 * @version 3_0_F	14.01.2016  MS Added APDM Result <BR>
 * @version 5_0_T	04.03.2016  MS Bugfix UserDialog <BR>
 * @version 6_0_F	04.03.2016  MS F-Version <BR>
 * @version 7_0_F	04.03.2016  MKe Bugfix <BR>
 * @version 8_0_F	13.06.2016  MKe optionalen Titel hinzugef�gt <BR>
 * @version 9_0_T	02.08.2016  MKe 50ms Pausen wegen sporadisch auftretenden Laufzeitfehler eingebaut<BR>
 * @version 10_0_F	04.08.2016  MKe F-Version <BR>
 * @version 11_0_F	04.08.2016  MKe F-Version <BR>
 * @version 12_0_T	25.01.2017  MKe FT Parameter wird nun an APDM �bertragen. <BR>
 * @version 13_0_F	10.02.2017  MKe F-Version <BR>
 * @version 14_0_T	05.10.2017  MKe Die Verarbeitung des FT wurde ge�ndert; Bugfix bei Zeilenumbr�che(";") und AT-Operator  <BR>
 * @version 15_0_F	16.10.2017  MKe F-Version  <BR>
 * @version 16_0_T	06.12.2017  PR  Bugfix. Jede Ausf�hrung der PP f�hrte dazu, dass workerQuestion zwar mit FT initialisiert wird, doch da es sich um eine gobale Variable handelt, beh�lt sie ihren alten Wert und w�chst mit jeder Ausf�hrung.<BR>
 * @version 17_0_F	16.10.2017  PR  F-Version  <BR>
 */
public class SDiagVisualCheck_17_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
	UserDialog ud = null; //UserDialog
	String workerQuestion = "", workerAnswer = "", titel;
	int timeout;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public SDiagVisualCheck_17_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public SDiagVisualCheck_17_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "TITEL", "JOBPAR_ON_1", "JOBPAR_OFF_1", "DIAGMODEJOB", "DIAGMODEPAR", "DEBUG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "SGBD", "FT", "TIMEOUT", "WAIT", "JOB_ON_1", "JOB_OFF_1" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 *
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt
	 *         hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		return super.checkArgs();
	}

	/** Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String sgbd = null, wait = null, diagmode = null, diagModePar = null;
		String[] jobOn = null;
		String[] jobOff = null;
		String[] jobParOn = null;
		String[] jobParOff = null;
		int max = 0;
		DialogThread thread = null;
		// EDIABAS
		EdiabasProxyThread myEdiabas = null;
		String temp;
		workerAnswer = "";
		workerQuestion = "";

		/***********************************************
		 * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
		 * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		 ***********************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		try {
			//Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG abfragen
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
				} else {
					// noch Pr�fstandvariable pr�fen, ob Debug
					try {
						Object pr_var_debug;

						pr_var_debug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
						if( pr_var_debug != null ) {
							if( pr_var_debug instanceof String ) {
								if( "TRUE".equalsIgnoreCase( (String) pr_var_debug ) ) {
									bDebug = true;
								} else {
									bDebug = false;
								}
							} else if( pr_var_debug instanceof Boolean ) {
								if( ((Boolean) pr_var_debug).booleanValue() ) {
									bDebug = true;
								} else {
									bDebug = false;
								}
							}
						} else {
							bDebug = false;
						}

					} catch( VariablesException e ) {
						bDebug = false;
					}
				}

				// SGBD
				sgbd = extractValues( getArg( "SGBD" ) )[0];

				// WAIT
				wait = extractValues( getArg( "WAIT" ) )[0];

				// TIMEOUT
				timeout = Integer.parseInt( extractValues( getArg( "TIMEOUT" ) )[0] );

				// WORKERQUESTION
				for( String str : extractValuesModified( getArg( "FT" ) ) ) {
					workerQuestion = workerQuestion + str;
				}

				// Titel
				try {
					titel = extractValues( getArg( "TITEL" ) )[0];
				} catch( Exception e ) {
					titel = "frage";
				}
				//Diagmode
				try {
					if( extractValues( getArg( "DIAGMODEJOB" ) ) != null ) {
						diagmode = extractValues( getArg( "DIAGMODEJOB" ) )[0];
					}
				} catch( Exception e ) {
					diagmode = "";
				}

				//DiagModePar
				try {
					if( extractValues( getArg( "DIAGMODEPAR" ) ) != null ) {
						diagModePar = extractValues( getArg( "DIAGMODEPAR" ) )[0];
					}
				} catch( Exception e ) {
					diagmode = "";
				}

				//Userdialog holen

				ud = getPr�flingLaufzeitUmgebung().getUserDialog();

				// Jobs
				//Anzahl der Jobs z�hlen
				Enumeration enu = getArgs().keys();
				String para;
				while( enu.hasMoreElements() ) {
					para = enu.nextElement().toString();
					if( para.startsWith( "JOB_ON_" ) ) {
						max++;
					}

				}
				//Pr�fen ob f�r jeden JOB_ON auch ein JOB_OFF existiert

				for( int i = 0; i < max; i++ ) {
					boolean check = false;
					enu = getArgs().keys();
					while( enu.hasMoreElements() ) {
						if( enu.nextElement().toString().equalsIgnoreCase( "JOB_OFF_" + (i + 1) ) ) {
							check = true;
							break;
						}
					}
					if( !check ) {
						throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

					}
				}

				//JOB_ON und JOB_OFF

				jobOn = new String[max];
				jobOff = new String[max];
				jobParOn = new String[max];
				jobParOff = new String[max];

				for( int j = 0; j < max; j++ ) {
					jobOn[j] = extractValues( getArg( "JOB_ON_" + (j + 1) ) )[0];
					jobOff[j] = extractValues( getArg( "JOB_OFF_" + (j + 1) ) )[0];
					try {
						if( extractValues( getArg( "JOBPAR_ON_" + (j + 1) ) ) != null ) {
							jobParOn[j] = getArg( "JOBPAR_ON_" + (j + 1) );
						}
					} catch( Exception e ) {
						jobParOn[j] = "";
					}
					try {
						if( extractValues( getArg( "JOBPAR_ON_" + (j + 1) ) ) != null ) {
							jobParOff[j] = getArg( "JOBPAR_OFF_" + (j + 1) );
						}
					} catch( Exception e ) {
						jobParOff[j] = "";
					}

				}

				// debug
				if( bDebug ) {
					System.out.println( "SDiagVisualCheck: Parameter processing Check is done. (SGBD: <" + sgbd + ">, WAIT: <" + wait + ">, TIMEOUT: <" + timeout + ">, WORKERQUESTION: <" + workerQuestion + "> )" );
				}

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );

				if( bDebug ) {
					e.printStackTrace();
				}

				throw new PPExecutionException();
			}

			// Ausf�hrung EDIABAS Job

			// EDIABAS laden
			if( bDebug ) {
				System.out.println( "SDiagVisualCheck, Ediabas anfang" );
			}
			myEdiabas = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();
			Long starttime = System.currentTimeMillis();
			Long endtime = starttime + timeout;

			//Starten des UserDialogthread und starten der Ediabas Ausf�hrung
			thread = new DialogThread( ud );
			thread.start();
			int error_count = 0;
			if( bDebug ) {
				System.out.println( "SDiagVisualCheck, Schleifenanfang" );
			}
			Thread.sleep( 50 );
			do {
				// EDIABAS Jobs ausf�hren

				error_count = error_count + 1;
				if( bDebug ) {
					System.out.println( "SDiagVisualCheck, Schleifenindex " + error_count );
				}
				Thread.sleep( 50 );
				//Extended DiagMode
				if( diagmode != "" ) {
					if( bDebug ) {
						System.out.println( "SDiagVisualCheck, erster Job" );
					}
					Thread.sleep( 50 );
					temp = myEdiabas.executeDiagJob( sgbd, diagmode, diagModePar, "" );
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, diagmode, "", "JOB_STATUS", temp, "OKAY", "", "0", workerQuestion, "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					}
				}

				//Einschalten
				if( bDebug ) {
					System.out.println( "SDiagVisualCheck, zweiter Job" );
				}
				Thread.sleep( 50 );
				for( int i = 0; i < max; i++ ) {
					temp = myEdiabas.executeDiagJob( sgbd, jobOn[i], jobParOn[i], "" );
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, jobOn[i], "", "JOB_STATUS", temp, "OKAY", "", "0", workerQuestion, "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					}
				}
				//Wartezeit				
				Thread.sleep( Long.parseLong( wait ) );

				//Extended DiagMode
				if( diagmode != "" ) {
					if( bDebug ) {
						System.out.println( "SDiagVisualCheck, dritter Job" );
					}
					Thread.sleep( 50 );
					temp = myEdiabas.executeDiagJob( sgbd, diagmode, diagModePar, "" );
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, diagmode, "", "JOB_STATUS", temp, "OKAY", "", "0", workerQuestion, "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					}
				}

				//Ausschalten
				if( bDebug ) {
					System.out.println( "SDiagVisualCheck, vierter Job" );
				}
				Thread.sleep( 50 );
				for( int i = 0; i < max; i++ ) {
					temp = myEdiabas.executeDiagJob( sgbd, jobOff[i], jobParOff[i], "" );
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, jobOff[i], "", "JOB_STATUS", temp, "OKAY", "", "0", workerQuestion, "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					}
				}
				//Wartezeit
				Thread.sleep( Long.parseLong( wait ) );
				if( bDebug ) {
					System.out.println( "SDiagVisualCheck, workerAnswer: " + workerAnswer );
				}
				if( workerAnswer.equals( Integer.toString( UserDialog.NO_KEY ) ) || workerAnswer.equals( Integer.toString( UserDialog.YES_KEY ) ) ) {
					break;
				}
				if( bDebug ) {
					System.out.println( "SDiagVisualCheck, Systemzeit: " + System.currentTimeMillis() );
					System.out.println( "SDiagVisualCheck, Endzeit: " + endtime );
				}
			} while( System.currentTimeMillis() < endtime );

			Thread.sleep( 50 );
			if( bDebug ) {
				System.out.println( "SDiagVisualCheck, Ende Schleife" );
			}
			if( !workerAnswer.equals( Integer.toString( UserDialog.YES_KEY ) ) ) {
				if( workerAnswer.equals( Integer.toString( UserDialog.NO_KEY ) ) ) {
					workerAnswer = "Nein";
				} else {
					workerAnswer = "Timeout";
				}
				result = new Ergebnis( "Werkereingabe", "", "", "", "", "Werkerbest�tigung", workerAnswer, "", "", "", workerQuestion, "", "", "", "", Ergebnis.FT_NIO );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} else {
				result = new Ergebnis( "Status", "VisualCheck", "", "", "", "IO", "", "", "", "0", workerQuestion, "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );
			}

		} catch( PPExecutionException e ) {
			result = new Ergebnis( "DiagFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), myEdiabas.apiErrorCode() + ": " + myEdiabas.apiErrorText(), Ergebnis.FT_NIO );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;

			if( bDebug ) {
				e.printStackTrace();
			}
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ) + myEdiabas.apiErrorCode() + ": " + myEdiabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			e.printStackTrace();
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			e.printStackTrace();
		}
		if( ud != null ) {
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
				ud = null;
			} catch( DeviceLockedException e ) {
				e.printStackTrace();
			}
		}
		//Status setzen
		setPPStatus( info, status, ergListe );
	}

	protected String[] extractValuesModified( String arg ) throws PPExecutionException {
		String[] args;

		args = splitArg( arg );
		for( int i = 0; i < args.length; i++ ) {
			if( args[i].indexOf( '@' ) != -1 ) {
				try {
					args[i] = getPPResult( args[i].toUpperCase() );
				} catch( InformationNotAvailableException e ) {
					throw new PPExecutionException( e.getMessage() );
				}
			} else {
				if( i < args.length - 1 ) {
					if( i + 1 < args.length ) {
						if( args[i + 1].indexOf( '@' ) == -1 ) {
							args[i] = args[i] + ";";
						}
					}
				}
			}
		}
		return args;
	}

	private class DialogThread extends Thread {
		UserDialog userd;

		public DialogThread( UserDialog ud ) {
			userd = ud;
		}

		@Override
		public void run() {
			workerAnswer = Integer.toString( userd.requestUserInputDigital( PB.getString( titel ), workerQuestion, timeout / 1000 ) );
		}

	}

}

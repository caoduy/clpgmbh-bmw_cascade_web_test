/*
 * ReadWifiRDCHandheldCorghi_x_y_F_Pruefprozedur.java
 *
 * Created on 11.09.16
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.framework.udnext.UDNButtonSection.UDNButton;
import com.bmw.cascade.pruefstand.framework.udnext.UDNHandle;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.UDNState;
import com.bmw.cascade.pruefstand.framework.udnext.UserDialogNextRuntimeEnvironment;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementierung einer Pr�fprozedur zum Auslesen und Speichern der RDC
 * Transponderdaten, die �ber ein Wifi (WLAN) basiertes RDC CORGHI Handger�t
 * erfasst wurden.<BR>
 * <BR>
 * Info von Fa. Corghi: Unter Nutzung von HTTP-Get/Post �ber Aufruf der URL :
 * http://'HOSTNAME':'PORT'/tpms_triggertool.php zu arbeiten, funktioniert
 * nicht.<BR>
 * <BR>
 * Der Trick ist, einen eigenen Server zu bauen und dar�ber, die Daten
 * abzuholen. Weil die Daten vom WLAN TPM Client laut Wireshark einfach direkt
 * an den ServerHost geschickt werden. Weiteres Problem ist, dass der
 * zur�ckgeschickte HTTP Post Content Stream nicht klar terminiert wird.
 * Deswegen muss zuerst durch Auswertung des Keys: Content-Length die Anzahl der
 * noch folgenden Zeichen ermittelt werden. Nachfolgend werden diese einfach auf
 * Basis der L�ngeninfo zeichenweise eingelesen. <BR>
 * TODO's: - RDC Handterminal meldet bei IO: Procedure failed. Error:0; - RDC
 * Handterminal generiert teilweise, irref�hrende Meldungen obwohl alles IO war;
 * - RDC Handterminal verliert manchmal die WLAN Verbindung; - Fehlermeldungen
 * von Corghi Handheld sollten noch dokumentiert werden
 * 
 * @author Buboltz <BR>
 * @version Implementierung <BR>
 * @version 1_0_F 11.09.2016 TB Erstimplementierung <BR>
 * @version 1_1_F 19.10.2016 KW Anpassung und UserDialog ausgeschaltet <BR>
 * @version 2_0_T 31.03.2017 KW Nach Daten Tranfer muss Handheld best�tigt werden <BR>
 * @version 3_0_F 18.04.2017 KW F-Version <BR>
 */
@SuppressWarnings({ "deprecation", "unused" })
public class ReadWifiRDCHandheldCorghi_3_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	// gibt an, ob Debug-Ausgaben erfolgen sollen
	boolean debug = false;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public ReadWifiRDCHandheldCorghi_3_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling
	 *            Klasse des zugeh. Pr�flings
	 * @param pruefprozName
	 *            Name der Pr�fprozedur
	 * @param hasToBeExecuted
	 *            Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public ReadWifiRDCHandheldCorghi_3_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName,
			Boolean hasToBeExecuted) {
		super(pruefling, pruefprozName, hasToBeExecuted);
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 * 
	 * @return Optionale Argumente als String Array
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG", "TPM_TCP_PORT" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 * 
	 * @return Zwingende Argumente als String Array
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = {};
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 *
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt
	 *         hat, ansonsten <code>true</code>.
	 */
	@Override
	public boolean checkArgs() {
		// Obligatorisch.
		return super.checkArgs();
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info
	 *            Information zur Ausf�hrung
	 */
	@Override
	public void execute(ExecutionInfo info) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		int port = 1150; // default TCP-Port: 1150
		UserDialogNextRuntimeEnvironment udSingle = null;
		UDNHandle udSingleHandle = null;
		UserDialog udMulti = null;
		boolean singleMode = !this.isMultiInstance();

		// f�r die �bersetzung
		ResourceBundle bundle = ResourceBundle.getBundle(Resources.class.getName());

		try {
			// Parameter holen
			try {
				if (checkArgs() == false)
					throw new PPExecutionException(PB.getString("parameterexistenz"));

				// DEBUG abfragen
				if (getArg("DEBUG") != null) {
					if (getArg("DEBUG").equalsIgnoreCase("TRUE"))
						debug = true;
					else if (getArg("DEBUG").equalsIgnoreCase("FALSE"))
						debug = false;
				} else {
					// noch Pr�fstandvariable pr�fen, ob Debug
					try {
						Object pr_var_debug;

						pr_var_debug = getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PS_CONFIG,
								"DEBUG");
						if (pr_var_debug != null) {
							if (pr_var_debug instanceof String) {
								if ("TRUE".equalsIgnoreCase((String) pr_var_debug)) {
									debug = true;
								} else {
									debug = false;
								}
							} else if (pr_var_debug instanceof Boolean) {
								if (((Boolean) pr_var_debug).booleanValue()) {
									debug = true;
								} else {
									debug = false;
								}
							}
						} else {
							debug = false;
						}

					} catch (VariablesException e) {
						debug = false;
					}
				}

				// opt. Parameter 'TPM_TCP_PORT'
				final String TPM_TCP_PORT = "TPM_TCP_PORT";
				if (getArg(TPM_TCP_PORT) != null) {
					try {
						port = Integer.parseInt(extractValues(getArg(TPM_TCP_PORT))[0]);
					} catch (NumberFormatException nbfe) {
						throw new PPExecutionException("Parameter 'TPM_TCP_PORT' is not a number");
					}
				}

				// debug
				if (debug) {
					System.out.println("ReadWifiRDCHandheldCorghi PP: Parameter Check is done.");
					System.out.println("ReadWifiRDCHandheldCorghi PP: TCP-Port of TPM device is: " + port);
				}

			} catch (PPExecutionException e) {
				if (e.getMessage() != null)
					result = new Ergebnis("ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "",
							PB.getString("parametrierfehler"), e.getMessage(), Ergebnis.FT_NIO_SYS);
				else
					result = new Ergebnis("ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "",
							PB.getString("parametrierfehler"), "", Ergebnis.FT_NIO_SYS);
				ergListe.add(result);
				throw new PPExecutionException();
			}

			// UserDialog holen
			if (singleMode) {
				udSingle = getPr�flingLaufzeitUmgebung().getUserDialogNext();
				udSingleHandle = udSingle.allocateUserDialogNext();
			} else {
				udMulti = getPr�flingLaufzeitUmgebung().getUserDialog();
			}

			// DER EIGENTLICHE PP-CODE...

			// Benutzerausgabe, warte auf Kommunikation
			UserInteractionThread uiThread = new UserInteractionThread(udSingle, udSingleHandle, udMulti, singleMode,
					bundle.getString("AWT1_Title"), bundle.getString("AWT1_Message"));
			uiThread.start();

			// ServerSocket instanzieren und zugeh�rigen Thread starten
			ServerSocket serverSocket = null;
			try {
				serverSocket = new ServerSocket(port);
			} catch (IOException e1) {
				throw new IOException("TPM TCP Port:" + port + " allocation error!");
			}
			TPMServerThread tpmThread = new TPMServerThread(serverSocket);
			tpmThread.start();

			// warte auf TPM Daten und checke Cancel im UserDialog
			String tpmData = null;
			while (true) {
				// Wurden Daten vom TPM Handterminal empfangen?
				if (tpmThread != null && !tpmThread.isAlive()) {
					tpmData = tpmThread.getTpmResponse();
					break;
				}

				// checke Benutzerabbruch im parallelen UserDialog
				if (uiThread != null && !uiThread.isAlive()) {
					// terminiere paralleln Server Thread
					if (serverSocket != null) {
						try {
							serverSocket.close();
						} catch (IOException e) {
						}
					}
					break;
				}

				// pausieren
				try {
					Thread.sleep(100);
				} catch (InterruptedException ie) {
					// Nothing
				}

			}

			// Debug
			if (debug && tpmData != null) {
				System.out.println("ReadWifiRDCHandheldCorghi PP: Received data <" + tpmData + ">");
			}

			// Test only
			// tpmData = "POST /tpms_triggertool.php HTTP/1.0 User-Agent: TPMS
			// Trigger Tool Content-Type: application/x-www-form-urlencoded
			// Content-Length: 330 sn=030259999&make=BMW&model=2
			// SER.(F22)&year=04/14-&id1=0992349683&id2=0958030412&id3=0992878408&id4=0992691376&p1=0.00&p2=0.00&p3=0.00&p4=0.00&t1=23.00&t2=13.00&t3=14.00&t4=15.00&b1=Bat:116m&b2=Bat:99m&b3=Bat:116m&b4=Bat:116m&s1=STORAGE&s2=PARKING&s3=STORAGE&s4=STORAGE&m1=CONTINENTAL&m2=CONTINENTAL&m3=CONTINENTAL&m4=CONTINENTAL";

			// UserInfo und dokumentieren
			List<String> tokens = Arrays.asList("id1=", "id2=", "id3=", "id4=");
			StringBuilder udTransponderContent = new StringBuilder(bundle.getString("AWT2_Message"));

			if (tpmData != null) {
				for (String token : tokens) {
					Scanner scanner = new Scanner(tpmData.substring(tpmData.indexOf(token) + token.length()));
					scanner.useDelimiter("&");
					String transponder_id = scanner.next();
					String transponder_name = token.substring(0, 3).toUpperCase();
					scanner.close();

					udTransponderContent.append(transponder_name + " ID: " + transponder_id + "\n");

					// Doku virtuelles Fahrzeug
					result = new Ergebnis(transponder_name, "ReadWifiRDCHandheldCorghi", "", "", "", transponder_name,
							transponder_id, transponder_id, "", "0", "", "", "", "", "", Ergebnis.FT_IO);
					ergListe.add(result);

					// Debug
					if (debug) {
						System.out.println("ReadWifiRDCHandheldCorghi PP: Transponder Name <" + transponder_name
								+ ">, ID <" + transponder_id + ">");
					}
				}

				// IO Doku virtuelles Fahrzeug
				result = new Ergebnis("TRANSPONDER_DATA", "ReadWifiRDCHandheldCorghi", "", "", "", "TRANSPONDER_DATA",
						"true", "true", "", "0", "", "", "", "", "", Ergebnis.FT_IO);
				ergListe.add(result);

				// Debug
				if (debug) {
					System.out.println("ReadWifiRDCHandheldCorghi PP: Data received ok. -> PP Status is IO");
				}

				// UserInfo
				// if( singleMode ) {
				// udSingle.displayMessage( udSingleHandle, UDNState.MESSAGE,
				// bundle.getString( "AWT1_Title" ),
				// udTransponderContent.toString(), 10, UDNButton.CANCEL_BUTTON
				// );
				// } else {
				// udMulti.setAllowCancel( true );
				// udMulti.displayMessage( bundle.getString( "AWT1_Title" ),
				// udTransponderContent.toString(), 10 );
				// }
			} else {
				// NIO Doku virtuelles Fahrzeug
				result = new Ergebnis("TRANSPONDER_DATA", "ReadWifiRDCHandheldCorghi", "", "", "", "TRANSPONDER_DATA",
						"false", "true", "", "0", "", "", "", "", "", Ergebnis.FT_NIO);
				ergListe.add(result);

				status = STATUS_EXECUTION_ERROR;

				// Debug
				if (debug) {
					System.out.println("ReadWifiRDCHandheldCorghi PP: NO data received -> PP Status is NIO");
				}
			}

		} catch (PPExecutionException e) {
			if (debug) {
				e.printStackTrace();
			}
			status = STATUS_EXECUTION_ERROR;
		} catch (Exception e) {
			if (debug) {
				e.printStackTrace();
			}
			result = new Ergebnis("ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception",
					PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
			ergListe.add(result);
			status = STATUS_EXECUTION_ERROR;
		} catch (Throwable e) {
			if (debug) {
				e.printStackTrace();
			}
			result = new Ergebnis("ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable",
					PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
			ergListe.add(result);
			status = STATUS_EXECUTION_ERROR;
		}

		if (singleMode && udSingle != null) {
			udSingle.releaseUserDialogNext(udSingleHandle);
		}

		if (!singleMode && udMulti != null) {
			try {
				this.getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch (DeviceLockedException e) {
			}
		}

		// Status setzen
		setPPStatus(info, status, ergListe);
	}

	/**
	 * Pr�ft mit Hilfe eines Http Requests auf den 2. Pr�fstandscreen, ob der
	 * Pr�fstand im Multiinstanzmodus l�uft.
	 * 
	 * @return <code>True</code>, wenn Pr�fstand im Multiinstanzmodus l�uft,
	 *         <code>false</code> wenn nicht.
	 * @author Fabian Schoenert, BMW AG TI-545
	 */
	private boolean isMultiInstance() {
		final String URL = "http://localhost:8086/testscreen-2";
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) new URL(URL).openConnection();
			con.setConnectTimeout(500); // in ms
			con.setRequestMethod("GET"); // ist eigentlich Default
			con.getInputStream(); // throws IOException wenn Adresse nicht
									// erreichbar
		} catch (final IOException e) {
			// Nicht im MI Modus
			return false;
		} catch (Exception e) {
			// Wenn Fehler bei Http Abfrage kein MI Modus
			return false;
		} finally {
			if (con != null) {
				con.disconnect();
			}
		}
		return true;
	}

	/**
	 * Eigener ServerThread zum Empfangen der Daten vom RDC WLAN Handterminal
	 * 
	 * @author Buboltz
	 */
	private class TPMServerThread extends Thread {
		private ServerSocket serverSocket = null;
		private static final String TOKEN_CONTENT_LENGTH = "Content-Length: ";
		private StringBuilder tpmResponse = null;

		/**
		 * Konstruktor
		 * 
		 * @param serverSocket
		 */
		public TPMServerThread(ServerSocket serverSocket) {
			this.serverSocket = serverSocket;
		}

		/**
		 * Getter fuer die TPM Nutzinformation
		 * 
		 * @return the tpmResponse, null oder tpmData
		 */
		public String getTpmResponse() {
			return tpmResponse != null ? tpmResponse.toString() : null;
		}

		/**
		 * run Methode
		 */
		@Override
		public void run() {
			Socket socket = null;
			BufferedReader reader = null;
			OutputStream os = null;
			// acknowledgment for Conghi handheld "HTTP/1.1 200"
			String ack = "HTTP/1.1 200";

			try {
				socket = serverSocket.accept();
				reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

				// lese die Header Infos des HTTP-Post vom TPM ein
				ArrayList<String> headerInfos = new ArrayList<String>();
				tpmResponse = new StringBuilder();
				String str;
				while ((str = reader.readLine()) != null) {
					headerInfos.add(str);
					tpmResponse.append(str).append("\n");
					if (str.startsWith(TOKEN_CONTENT_LENGTH)) {
						break;
					}
				}

				// Content L�nge bestimmen
				int contentLength = Integer
						.parseInt(headerInfos.get(headerInfos.size() - 1).substring(TOKEN_CONTENT_LENGTH.length()));

				// restlichen Content einlesen
				for (int i = 0; i < contentLength; i++) {
					tpmResponse.append((char) reader.read());
				}

				// sende Bestaetigung "HTTP/1.1 200"
				os = socket.getOutputStream();
				os.write(ack.getBytes());
				os.flush();

			} catch (Exception e) {
				if (debug) {
					e.printStackTrace();
				}
			} finally {
				if (reader != null) {
					try {
						reader.close();
					} catch (IOException e) {
					}
				}
				if (os != null) {
					try {
						os.close();
					} catch (IOException e) {
					}
				}
				if (socket != null) {
					try {
						socket.close();
					} catch (IOException e) {
					}
				}
				if (serverSocket != null) {
					try {
						serverSocket.close();
					} catch (IOException e) {
					}
				}
			}

		}
	}

	/**
	 * Eigener Thread zur Benutzerinteraktion
	 * 
	 * @author Buboltz
	 */
	private class UserInteractionThread extends Thread {
		private UserDialogNextRuntimeEnvironment udSingle = null;
		private UDNHandle udSingleHandle = null;
		private UserDialog udMulti = null;
		private final boolean singleMode;
		private final String TITLE, MESSAGE;

		/**
		 * Freigabe des UserDialoges
		 */
		public void releaseUserDialog() {
			if (singleMode) {
				udSingle.releaseUserDialogNext(udSingleHandle);
			} else {
				try {
					getPr�flingLaufzeitUmgebung().releaseUserDialog();
				} catch (DeviceLockedException e) {
				}
			}
		}

		/**
		 * Konstruktor
		 * 
		 * @param ud
		 * @param TITLE
		 * @param MESSAGE
		 */
		public UserInteractionThread(UserDialogNextRuntimeEnvironment udSingle, UDNHandle udSingleHandle,
				UserDialog udMulti, boolean singleMode, final String TITLE, final String MESSAGE) {
			this.udSingle = udSingle;
			this.udSingleHandle = udSingleHandle;
			this.udMulti = udMulti;
			this.singleMode = singleMode;
			this.TITLE = TITLE;
			this.MESSAGE = MESSAGE;
		}

		/**
		 * run Methode
		 */
		@Override
		public void run() {
			if (singleMode) {
				udSingle.displayMessage(udSingleHandle, UDNState.STATUS, TITLE, MESSAGE, 0, UDNButton.CANCEL_BUTTON);
			} else {
				udMulti.setAllowCancel(true);
				udMulti.displayMessage(TITLE, MESSAGE, 0);
			}
		}
	}

	/**
	 * RessourceBundle default en / English
	 * 
	 * @author Buboltz
	 */
	public static class Resources extends ListResourceBundle {
		@Override
		protected Object[][] getContents() {
			return new Object[][] {
					// LOCALIZE THIS
					{ "AWT1_Title", "WIFI-TPM-Corghi handheld" }, // <BR>
					{ "AWT1_Message", "\n\rWaiting on data..." }, // <BR>
					{ "AWT2_Message", "Read Transponder ID's:\n\n" } // <BR>
					// END OF MATERIAL TO LOCALIZE
			};
		}
	}

	/**
	 * RessourceBundle de / Deutsch
	 * 
	 * @author Buboltz
	 */
	public static class Resources_de extends ListResourceBundle {
		@Override
		protected Object[][] getContents() {
			return new Object[][] {
					// LOCALIZE THIS
					{ "AWT1_Title", "WIFI-RDC-Corghi Handterminal" }, // <BR>
					{ "AWT1_Message", "\n\rWarte auf Daten..." }, // <BR>
					{ "AWT2_Message", "Gelesene Transponder ID's:\n\n" }
					// END OF MATERIAL TO LOCALIZE
			};
		}

	}

	/**
	 * Testmethode
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		System.out.println("MAIN testing PP ReadWifiRDCHandheldCorghi...\n");
		// ReadWifiRDCHandheldCorghi_1_0_F_Pruefprozedur pp = new
		// ReadWifiRDCHandheldCorghi_1_0_F_Pruefprozedur();

		// Ressources Test:
		System.out.println("DO RESSOURCES TEST...");
		System.out.println(Resources.class.getName());
		System.out.println(Locale.getDefault());
		ResourceBundle bundle = ResourceBundle.getBundle(Resources.class.getName());
		System.out.println("AWT1_Title=" + bundle.getString("AWT1_Title"));
		System.out.println();

		// Test Commucication
		// Info von Corghi �ber HTTP-Get/Post: so geht's aber nicht:
		// http://192.168.7.111:888/tpms_triggertool.php
		// String urlTPMHandheld = "http://" + "172.20.10.7" + ":" + 1150 +
		// "/tpms_triggertool.php";
		//
		// Der Trick ist einen eigenen Server zu bauen und dar�ber die Daten
		// abzuholen.
		// Weil die Daten vom WLAN TPM Client einfach direkt an den ServerHost
		// geschickt werden.
		// Weiteres Problem ist, dass der zur�ckgeschickte HTTP Post Content
		// nicht klar terminiert wird.
		// Deswegen muss zuerst durch Auswertung des Keys: Content-Length die
		// Anzahl der noch folgenden Zeichen ermittelt werden.
		// Nachfolgend werden diese einfach auf Basis der L�ngeninfo
		// zeichenweise eingelesen.
		System.out.println("DO TPM HANDHELD WIFI TEST...");
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(1150);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		Socket socket = null;
		BufferedReader reader = null;

		try {
			System.out.println("before accept");
			socket = serverSocket.accept();
			System.out.println("before read");
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			System.out.println("before check");
			StringBuilder tpmResponse = new StringBuilder();
			ArrayList<String> headerInfos = new ArrayList<String>();
			String str;
			while ((str = reader.readLine()) != null) {
				headerInfos.add(str);
				tpmResponse.append(str).append("\n");
				if (str.startsWith("Content-Length: ")) {
					break;
				}
			}

			int contentLength = Integer
					.parseInt(headerInfos.get(headerInfos.size() - 1).substring("Content-Length: ".length()));

			for (int i = 0; i < contentLength; i++) {
				tpmResponse.append((char) reader.read());
			}

			System.out.println("Received TPM WIFI Data...");
			System.out.println(tpmResponse.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (serverSocket != null) {
				try {
					serverSocket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		// End testing
		System.out.println("\nEND TESTING!!!");
	}
}

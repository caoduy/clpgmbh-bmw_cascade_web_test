package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;
import java.text.*;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.uianalyse.*;
import com.bmw.cascade.pruefstand.visualisierung.*;


/** UIToleranzGetLWMAngle_13_0_F_Pruefprozedur.java<BR>
 * <BR>
 * Created on 04.09.2009<BR>
 * 
 * <BR>
 * <b>Funktions�bersicht</b>
 * <ul><li>Kalibrieren<p>
 * Die Spannungs-Messwerte werden bei einer Lage von -5�, 0� und +5� Grad aufgenommen. Aus diesen Werten wird eine
 * Kenngerade errechnet, deren Faktoren A und B (y=Ax+B) in einer Datei gespeichert werden. Nach Werksanforderungen
 * ist die Kenngerade die Regressionsgerade der drei Messwerte, die auf den Nullpunkt verschoben wird.</li>
 * <li>Bewerten<p>
 * Der Winkel der Lenkradmesswaage wird ausgelesen und anhand der �bergebenen Toleranzen als ok oder nicht ok bewertet.
 * Erreicht der Winkel innerhalb der vorgegebenen Zeit nicht das Toleranzfenster, wird ein Fehler an CASCADE und APDM gemeldet.</li>
 * <li>Anzeigen<p>
 * Der Winkel der Lenkradmesswaage wir zum Funktionstest eine gewisse Zeit angezeigt.</li></ul>
 *
 * @author Klaus Kraski, ZSI<BR>
 * 		   Pedro Ferreira-Jorge, Industriehansa GmbH<BR>
 * 		   Ulf Plath, TI-534<BR>
 *
 * @version 0_0_1  02.04.2003 KK Grundfunktionalitaet und FA Freigabe<BR>
 * @version 0_0_2  27.04.2003 KK �nderung der Spannungs-/Winkelumrechnung<BR>
 * @version 0_0_3  12.05.2003 KK keine neue Instanz des UIAnalyzers<BR>
 * @version 0_0_4  14.05.2003 KK am Ende von getMeasuredValue UIAnalyser nicht freigeben<BR>
 *                               ge�nderter Variablenname ist_Value in getMeasuredValue<BR>
 *                               Kalibrierung entfernt => neue Pr�fprozedur<BR>
 *                               Umrechnungsfaktoren aus Datei lesen<BR>
 * @version 0_0_5  02.07.2007 PJ Corrected the no-result bug when OK<BR>
 * @version 0_0_6  11.06.2008 UP Default-Wert f�r Status-Variable auf ERROR gesetzt, Anzeige des Lenkwinkels auf
 * 								 max. f�nf Zeichen begrenzt (kein Zeilenumbruch im Fenster), Toleranzfenster muss
 * 					      		 definierte Zeit eingehalten werden, Zeit wird �ber neues Argument LMW_OK_TIME
 * 					 			 festgelegt
 * @version 0_0_7  05.09.2008 UP Werkerbest�tigung nach OK-Bewertung des Lenkwinkels eingef�gt.
 * @version 0_0_8  10.10.2008 UP Ausleseprozedur f�r Kalibrierwerte ge�ndert
 * @version 0_0_9  16.10.2008 UP Englische Fehler- und Anweisungstexte hinzugef�gt, zus�tzliches optionales Argument
 *                               CONFIRM hinzugef�gt (steuert die Werkerbest�tigung nach Einlesen des Winkels)
 * @version 0_0_10 24.03.2009 UP Integration der Anzeige- und Kalibrierfunktionen aus CalibrateLWM
 * 								 und UIToleranzShowLWMAngle, Argument SKALIEREN in BEWERTEN umbenannt, Anzeigefarbe bei
 * 								 (Nicht-)Erreichen des Toleranzfensters ge�ndert, opt. Argument OFFSET zur �bergabe eines
 * 								 Lenkwinkeloffsets hinzugef�gt, Kalibriervorgang auf drei Messpunkte erweitert
 * @version 0_0_11 23.04.2009 UP Kleine Fehlerkorrekturen, Berechnungsmethode f�r Kennlinie angepasst
 * @version 0_0_12 05.06.2009 UP Wert�berpr�fung f�r Kalibrierung (0, NaN, Unendlich) beim Speichern und Auslesen hinzugef�gt
 * @version 0_0_13 04.09.2009 UP Aufruf des UI-Analyzers mit Tag umgesetzt (notwendig f�r ECOS-Stand mit zwei ICP-Modulen),
 *                               deshalb auch Einf�hrung des opt. Arg. DEVICE_TAG
 * @version 0_0_14 15.03.2012 UP Neues optionales Argument LARGE_DIGITS hinzugef�gt, dass die Anzeigegroesse der Winkelwerte beeinflusst.
 */
public class UIToleranzGetLWMAngle_16_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    	static final long serialVersionUID = 1L;
    	
    	// Name der Datei, die die Kalibrierwerte enth�lt
    	private static final String fileName = "faktoren.txt";
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public UIToleranzGetLWMAngle_16_0_F_Pruefprozedur() {}

  /**
     * erzeugt eine neue Pruefprozedur mit obigem Verhalten
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public UIToleranzGetLWMAngle_16_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    /**
     * initialisiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /**
     * liefert die optionalen Argumente<br>
     * @return Stringvektor der optionalen Argumente
     */
    public String[] getOptionalArgs() {
        String[] args = {"KALIBRIEREN", "BEWERTEN", "LMW_OK_TIME", "CONFIRM", "OFFSET", "DEVICE_TAG", "LARGE_DIGITS", "DEBUG"};
        return args;
    }
    
    /**
     * liefert die zwingend erforderlichen Argumente
     * @return Stringvektor der ben�tigten Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = {"RESULT", "UNTERE_TOLERANZ", "OBERE_TOLERANZ", "KANAL", "MESSDAUER"};
        return args;
    }
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
     * @return true, wenn die Ueberpruefung der zwingenden Argumente ok ist
     */
    public boolean checkArgs() {
        String strtemp = "";
        float floatCheckA = 0;
        float floatCheckB = 0;
        int intCheck = 0;
        long longCheck = 0;
        int IOChannel;
        boolean ok;
        boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;

		try {
            ok = super.checkArgs();
            if (ok == true) {
                // Result in V oder A
                strtemp = getArg(getRequiredArgs()[0]);
                log(debug, "Info", "RESULT: " + strtemp);
                if (strtemp != null) {
                    if (!(strtemp.startsWith("I") || strtemp.startsWith("U"))) {
                    	log(debug, "Error", "Konnte RESULT-Parameter nicht verwerten. \"U\" oder \"I\" verwenden.");
                    	return false;
                    }
                } else {
                	log(debug, "Error", "Konnte RESULT-Parameter nicht finden.");
                    return false;
                }
                
                // Min-Wert
                strtemp=getArg(getRequiredArgs()[1]);
                log(debug, "Info", "MINWERT: " + strtemp);
                if (strtemp != null) {
                    try {
                        floatCheckA = Float.parseFloat(strtemp);
                    } catch (NumberFormatException e) {
                    	log(debug, "Error", "NumberFormatException aufgetreten. UNTERE_TOLERANZ-Parameter pr�fen.");
                        return false;
                    }
                } else {
                	log(debug, "Error", "Konnte UNTERE_TOLERANZ-Parameter nicht finden.");
                	return false;
                }
                
                // Max-Wert
                strtemp=getArg(getRequiredArgs()[2]);
                log(debug, "Info", "MAXWERT: " + strtemp);
                if (strtemp != null) {
                    try {
                    	floatCheckB = Float.parseFloat(strtemp);
                    } catch (NumberFormatException e) {
                    	log(debug, "Error", "NumberFormatException aufgetreten. OBERE_TOLERANZ-Parameter pr�fen.");
                        return false;
                    }
                } else {
                	log(debug, "Error", "Konnte OBERE_TOLERANZ-Parameter nicht finden.");
                	return false;
                }
                
                if (floatCheckA > floatCheckB) {
                	log(debug, "Error", "UNTERE_TOLERANZ muss kleiner als OBERE_TOLERANZ sein.");
                	return false;
                }
                // Kan�le
                strtemp=getArg(getRequiredArgs()[3]);
                log(debug, "Info", "KANAL: " + strtemp);
                if (strtemp != null) {
                    try {
                        IOChannel = Integer.parseInt(strtemp);
                        if (IOChannel < 1 || IOChannel > 8) {
                            log(debug, "Error", "KANAL-Parameter muss eine Zahl aus 1 bis 8 sein.");
                        	return false;
                        }
                    } catch (NumberFormatException e) {
                    	log(debug, "Error", "Argument KANAL falsch parametriert! " + e.getMessage());
                        return false;
                    }
                }
                // Messdauer
                strtemp=getArg(getRequiredArgs()[4]);
                log(debug, "Info", "MESSDAUER: " + strtemp);
                if (strtemp != null) {
                    try {
                        longCheck = Long.parseLong(strtemp);
                        if (longCheck <= 0) {
                        	log(debug, "Error", "MESSDAUER-Parameter muss gr��er Null sein.");
                        	return false;
                        }
                    } catch (NumberFormatException e) {
                    	log(debug, "Error", "Argument MESSDAUER falsch parametriert! " + e.getMessage());
                        return false;
                    }
                }
                // Kalibrieren
                strtemp=getArg(getOptionalArgs()[0]);
                log(debug, "Info", "KALIBRIEREN: " + strtemp);
                if (strtemp != null) {
                    try {
                        intCheck = Integer.parseInt(strtemp);
                        if (intCheck < 0) {
                        	log(debug, "Error", "KALIBRIEREN-Parameter muss gr��er oder gleich Null sein.");
                        	return false;
                        }
                    } catch (NumberFormatException e) {
                    	log(debug, "Error", "Argument KALIBRIEREN falsch parametriert! " + e.getMessage());
                        return false;
                    }
                }
                // Bewerten
                strtemp=getArg(getOptionalArgs()[1]);
                log(debug, "Info", "BEWERTEN: " + strtemp);
                if (strtemp != null) {
                    try {
                        intCheck = Integer.parseInt(strtemp);
                        if (intCheck < 0) {
                        	log(debug, "Error", "BEWERTEN-Parameter muss gr��er oder gleich Null sein.");
                        	return false;
                        }
                    } catch (NumberFormatException e) {
                    	log(debug, "Error", "Argument BEWERTEN falsch parametriert! " + e.getMessage());
                        return false;
                    }
                }
                // LMW-in-Toleranz-Zeit
                strtemp=getArg(getOptionalArgs()[2]);
                log(debug, "Info", "LMW_OK_ZEIT: " + strtemp);
                if (strtemp != null) {
                    try {
                        intCheck = Integer.parseInt(strtemp);
                        if (intCheck < 0) {
                        	log(debug, "Error", "LMW_OK_TIME-Parameter muss gr��er oder gleich Null sein.");
                        	return false;
                        }
                    } catch (NumberFormatException e) {
                    	log(debug, "Error", "Argument LMW_OK_TIME falsch parametriert! " + e.getMessage());
                        return false;
                    }
                }
                // Best�tigung
                strtemp=getArg(getOptionalArgs()[3]);
                log(debug, "Info", "CONFIRM: " + strtemp);
                if (strtemp != null) {
                    try {
                        intCheck = Integer.parseInt(strtemp);
                        if (intCheck < 0) {
                        	log(debug, "Error", "CONFIRM-Parameter muss gr��er oder gleich Null sein.");
                        	return false;
                        }
                    } catch (NumberFormatException e) {
                    	log(debug, "Error", "Argument CONFIRM falsch parametriert! " + e.getMessage());
                        return false;
                    }
                }
             	// Verrechnung eines Offset-Werts
                strtemp=getArg(getOptionalArgs()[4]);
                log(debug, "Info", "OFFSET: " + strtemp);
                if (strtemp != null) {
                    try {
                        floatCheckA = Float.parseFloat(strtemp);
                    } catch (NumberFormatException e) {
                    	log(debug, "Error", "Argument LMW_OK_TIME falsch parametriert! " + e.getMessage());
                        return false;
                    }
                }
            }
            return ok;
        } catch (Exception e) {
        	e.printStackTrace();
            return false;
        } catch (Throwable e) {
        	e.printStackTrace();
            return false;
        }
    }   // end of checkArgs
    
    /**
     * f�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_ERROR;
        
        // spezifische Variablen
        UserDialog ud = null;           // f�r Instanz auf UserDialog - nur eine Instanz holen
        UIAnalyser analyzerUI = null;   // f�r Instanz auf UIAnalyzer
        String devTag = (getArg("DEVICE_TAG") != null) ? (getArg("DEVICE_TAG")) : null;
        int channel = 1;
        int angleCounter = 0;			// Z�hlvariable f�r das mehrmalige Messen der LMW
        int funktion = 2;               // Funktion der PP (Bewerten, Anzeigen, Kalibrieren)
        
        // Anzahl der 200ms-Schleifendurchl�ufe, bevor LMW in Toleranz akzeptiert wird, Standard 25 = 5sec
		int okLoops = (getArg("LMW_OK_TIME") != null) ? (Integer.parseInt(getArg("LMW_OK_TIME")) * 5) : 25;
        
        String temp = null;              // tempor�re Variable
        String result_str = null;        // Zwischenspeichern V oder A
        String istWinkelStr = "";		 // Ist-Winkel der LMW als String
        
        float abValue[]		  = {0f, 0f};// Array zur Berechnung der Regressionsgeraden
        float aValue		  =  0.0f;   // Wert A aus der Kennliniendatei
        float bValue          =  0.0f;	 // Wert B aus der Kennliniendatei
        float istValue		  =  0.0f;   // Messwert in [mV]
        float istWinkel       =  0.0f;   // umgerechneter Messwert in [Grad]
        float untere_Toleranz = -0.2f;   // unterer Toleranz-Wert in [Grad] 
        float obere_Toleranz  =  0.2f;   // oberer Toleranz-Wert in [Grad]
        float min_Spannung    =  0.0f;   // minimaler Spannunswert in einem Messzyklus
        float max_Spannung    =  0.0f;   // maximaler Spannunswert in einem Messzyklus
        
        final float fWinkel[] =  {-5.0f, 0f, 5.0f};   // Konstanten zum Kalibrieren der Lenkradmesswaage
        float fVoltage[]       = {0f, 0f, 0f}; 		  // Spannungen beim Kalibrieren der Lenkradmesswaage
        float offset = (getArg("OFFSET") != null) ? Float.parseFloat(getArg("OFFSET")) : 0.0f; // wurde in CheckArgs schon gepr�ft
        
        long dauer            =  0;      // Messdauer (kommt vom Pr�fling)
        long starttime        =  0;      // Startzeit einer Messung

        // interne Variablen
        boolean udInUse = false;
        boolean Winkel_OK = false;		// Winkel in Toleranz?
        boolean noTimeout = true;		// kein Timeout aufgetreten?
        
        boolean angleConfirm = (getArg("CONFIRM") != null && getArg("CONFIRM").equalsIgnoreCase("0")) ? false : true;
        boolean lang = System.getProperty("user.language").equalsIgnoreCase("de") ? true : false;
        boolean largeFont = (getArg("LARGE_DIGITS") != null && getArg("LARGE_DIGITS").equalsIgnoreCase("TRUE")) ? true : false;

        boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
        boolean calib = (getArg("KALIBRIEREN") == null || getArg("KALIBRIEREN").equals("0")) ? false : true;
        boolean eval = (getArg("BEWERTEN") == null || getArg("BEWERTEN").equals("0")) ? false : true;

        log(debug, "Info", "This is PP " + this.getPPName() + " " + this.getVersion());
        
        if (calib) {
        	funktion = 0; // Kalibrieren
        } else if (eval) {
        	funktion = 1; // Bewerten
        } else {
        	funktion = 2; // Anzeigen (Default)
        }
        
        log(debug, "Info", "Gew�hlte Funktion (0 = Kalibrieren, 1 = Bewerten, 2 = Anzeigen): " + funktion);
        
        try {
            try {
                if( checkArgs() == false ) throw new PPExecutionException(PB.getString("Parameterexistenz"));
                //
                // Messgr��e und untere, obere Toleranz einlesen
                //
                result_str = getArg(getRequiredArgs()[0]);
                untere_Toleranz = Float.parseFloat(getArg(getRequiredArgs()[1]));
                obere_Toleranz = Float.parseFloat(getArg(getRequiredArgs()[2]));
                
                //
                // Kanal setzen
                //
                temp = getArg(getRequiredArgs()[3]);
                if (temp != null) {
                    try {
                        analyzerUI = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyser(devTag);
                        log(debug, "Info", "UIAnalyser wurde geholt.");
                        channel=Integer.parseInt(temp);
                        analyzerUI.setKanal(channel); // Kanal von 1-8, intern 0-7
                    } catch (DeviceLockedException e) {
                        e.printStackTrace();
                        result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "deviceLockedException" ), e.getMessage(), Ergebnis.FT_NIO_SYS);
                        ergListe.add(result);
                        throw new PPExecutionException();
                    } catch (DeviceNotAvailableException e) {
                        e.printStackTrace();
                        result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "deviceNotAvailableException" ), e.getMessage() + " Das UIAnalyzer-Device ist nicht vorhanden oder falsch parametriert.", Ergebnis.FT_NIO_SYS);
                        ergListe.add(result);
                        throw new PPExecutionException();
                    } catch (DeviceParameterException e) {
                        e.printStackTrace();
                        result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "deviceParameterException" ), e.getMessage(), Ergebnis.FT_NIO_SYS);
                        ergListe.add(result);
                        throw new PPExecutionException();
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                        result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unzulaessigerADKanal" ), e.getMessage(), Ergebnis.FT_NIO_SYS);
                        ergListe.add(result);
                        throw new PPExecutionException();
                    } catch (Exception e) {
                        e.printStackTrace();
                        result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS);
                        ergListe.add(result);
                        throw new PPExecutionException();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (e.getMessage() != null) {
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                } else {
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
                }
                ergListe.add(result);
                throw new PPExecutionException(e.getMessage());
            }

            //
            // Werkeranweisung ausgeben
            //
            
            // Wenn noch kein Infofenster, dann hole eine Instanz und poste das Infofeld
            if (udInUse != true) {
                udInUse = true;
                try {
                    ud = getPr�flingLaufzeitUmgebung().getUserDialog();
                    log(debug, "Info", "UserDialog wurde geholt.");
                } catch (Exception e) {
                    if (e instanceof DeviceLockedException) {
                        result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
                    } else if (e instanceof DeviceNotAvailableException) {
                        result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
                    } else {
                        result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
                	}
                    ergListe.add(result);
                    throw new PPExecutionException();
                }
            }
            
            // ----------------------------------------------------------
            // Unterscheidung der Funktionen
            // ----------------------------------------------------------
            
            switch (funktion) {
            	
            	case 0:
		            	//
		            	// Lenkradmesswaage kalibrieren
		                //
            			log(debug, "Info", "SWITCH: Case 0, Kalibrieren");
            			
		                for (int i = 0; i < fWinkel.length; i++) {
		                	if (lang) {
	            				ud.requestUserMessage("Kennlinie bestimmen:", "Bitte Messwaage in Position " + (int) fWinkel[i] + "� bringen.", 0);
				            } else {
				               	ud.requestUserMessage("Determine Scaling:", "Please put balancer in " + (int) fWinkel[i] + "� position.", 0);
				            }           
	            			fVoltage[i] = getMeasuredValue(analyzerUI, result_str, min_Spannung, max_Spannung);
	            			log(debug, "Info", "Spannung bei Winkel " + fWinkel[i] + "= " + fVoltage[i]);
		                }
		                
		                // Faktoren abspeichern                   
		                abValue = getRegressionThroughZero(fVoltage, fWinkel);
		                
		                if (Float.isNaN(abValue[0]) || abValue[0] == 0 || Float.isInfinite(abValue[0]) || Float.isNaN(abValue[1]) || abValue[1] == 0 || Float.isInfinite(abValue[1])) {
		                	throw new PPExecutionException("Die Berechnung der Kennlinie ist fehlgeschlagen! Bitte die korrekte Funktion der LMW pr�fen.");
		                }
		                aValue = abValue[0];
		                bValue = abValue[1];

		                writeValuesToFile(channel, abValue);
		                
		                log(debug, "Info", "Kennlinien-Faktoren (A = " + aValue + ", B = " + bValue + ")wurden in Datei " + fileName + " geschrieben.");
	                	result = new Ergebnis( "Kalibrierung_A", "UIToleranzGetLWMAngle", "", "", "", "Kalibrierung der Lenkwinkelmesswaage", "" + aValue, "", "", "0", "", "", "", "Wert A (Steigung der Kenngeraden)", "", Ergebnis.FT_IO);
	                	ergListe.add(result);
	                	result = new Ergebnis( "Kalibrierung_B", "UIToleranzGetLWMAngle", "", "", "", "Kalibrierung der Lenkwinkelmesswaage", "" + bValue, "", "", "0", "", "", "", "Wert B (Schnittpunkt der Kenngeraden mit der y-Achse)", "", Ergebnis.FT_IO);
	                	ergListe.add(result);
		                status = STATUS_EXECUTION_OK;
		                break;
	                    
            	case 1:
            			//
            			// gemessenen Winkel anzeigen und bewerten
            			//
            			log(debug, "Info", "SWITCH: Case 1, Bewerten");
            			starttime = System.currentTimeMillis();
            			dauer = Long.parseLong(getArg(getRequiredArgs()[4])); //Messdauer, wurde bei CheckArgs schon gepr�ft
	            				
                        //
                        // Faktoren aus Datei auslesen
                        //
                        aValue = getCalibValue(channel, "A");
                        bValue = getCalibValue(channel, "B");
		                
                        if (Float.isNaN(aValue) || aValue == 0 || Float.isInfinite(aValue) || Float.isNaN(bValue) || bValue == 0 || Float.isInfinite(bValue)) {
		                	throw new PPExecutionException("Das Auslesen der Kennlinie ist fehlgeschlagen! Bitte die LMW neu kalibrieren.");
		                }
                        
                        log(debug, "Info", "Kennlinien-Faktoren wurden aus Datei " + fileName + " ausgelesen. A = " + aValue + "; B = " + bValue);
                        
                        //
			            // DO-Schleife �ber gesamte Messdauer oder bis Winkel im Toleranzbereich
			            //
		                do {
		                	istValue = getMeasuredValue(analyzerUI, result_str, min_Spannung, max_Spannung);
		                    istWinkel = aValue * istValue + bValue + offset;
		                    
		                    // auf zwei Nachkommastellen formatieren
		                    istWinkel *= 100;
		                    istWinkel = (float) Math.round(istWinkel);
		                    istWinkel /= 100;
		                    istWinkelStr = String.valueOf(istWinkel);
		                    
		                    if (istWinkel >= 0) istWinkelStr = "+" + istWinkelStr; // positives Vorzeichen anf�gen
		                    //if (istWinkelStr.length() >= 6) istWinkelStr = istWinkelStr.substring(0, 5); // �bersch�ssige Nachkommastellen abschneiden
			                    
		                    if ((istWinkel > untere_Toleranz) && (istWinkel < obere_Toleranz)) {
		                    	if (largeFont) {
		                    		ud.setMessageFontSize(72);
		                    	}
		                    	
		                    	if (lang) {  	
			                    		ud.displayUserMessage("Auswertung", "min: " + Float.toString(untere_Toleranz) + "�;Istwinkel:   " + istWinkelStr + "�;max: " + Float.toString(obere_Toleranz) + "�", -1);
			                    	} else {
			                    		ud.displayUserMessage("Evaluation", "min: " + Float.toString(untere_Toleranz) + "�;actual angle:   " + istWinkelStr + "�;max: " + Float.toString(obere_Toleranz) + "�", -1);                    	
			                    	}
			                    	Winkel_OK = true;
			                        if (angleCounter >= okLoops - 1) { // Winkelwert nur beim letzten Durchlauf in APDM schreiben
			                        	result= new Ergebnis(result_str, "UIAnalyzer", "", "", "", "", istWinkelStr, "" + untere_Toleranz, "" + obere_Toleranz, "0", "", "", "", "", "Lenkwinkel in Toleranz", Ergebnis.FT_IO);
			                        	ergListe.add(result);
			                            status = STATUS_EXECUTION_OK;
			                        }
			                } else if (lang) {            
			                    	ud.displayAlertMessage("Auswertung", "min: " + Float.toString(untere_Toleranz) + "�;Istwinkel:   " + istWinkelStr + "�;max: " + Float.toString(obere_Toleranz) + "�" + "\n\n" + "Winkel ist nicht in Toleranz!", -1);
			                } else {
			                    	ud.displayAlertMessage("Evaluation", "min: " + Float.toString(untere_Toleranz) + "�;actual angle:   " + istWinkelStr + "�;max: " + Float.toString(obere_Toleranz) + "�" + "\n\n" + "Angle is not in tolerance!", -1);
			                }
			                    
			                angleCounter = Winkel_OK ? (angleCounter + 1) : 0; // Durchl�ufe z�hlen, w�hrend derer die LMW in Toleranz ist                
			                Winkel_OK = false; // reset for next cycle
		                 
		                    try {
		        				Thread.sleep(200);
		        			} catch (InterruptedException e) {}
		        			
		        			noTimeout = (System.currentTimeMillis() < starttime + dauer) ? true : false;
		                    
		                } while	(angleCounter <= okLoops && noTimeout);
		                	                
		                if (!noTimeout) {
		                	log(debug, "Error", "Timeout ist w�hrend der Lenkwinkelmessung aufgetreten.");
		                	result = new Ergebnis( "Timeout", "UIToleranzGetLWMAngle", "", "", "", "Lenkwinkel nach " + dauer + "s (Timeout)", istWinkelStr, "" + untere_Toleranz, "" + obere_Toleranz, "0", "", "", "", "Lenkwinkel war innerhalb des Zeitfensters nicht in Toleranz", "", Ergebnis.FT_NIO);
		                	ergListe.add(result);
		                } else if (angleConfirm) {
		                	// Best�tigung durch Benutzer
		                	if (largeFont) {
		                		ud.setMessageFontSize(36);
		                	}
		                	if (lang) {
		                		ud.requestUserMessage("Best�tigung", "Lenkwinkel (" + istWinkelStr + "�) ist innerhalb der Toleranz.", 0);
		                	} else {
		                		ud.requestUserMessage("Confirmation", "Steering angle (" + istWinkelStr + "�) is within tolerance.", 0);
		                	}
		                	// letztmalige Pr�fung des Winkels
		                	istValue = getMeasuredValue(analyzerUI, result_str, min_Spannung, max_Spannung);
		                	istWinkel = aValue * istValue + bValue + offset;
		                    
		                	// Nachkommastellen abschneiden, damit Messung = Anzeige
		                	istWinkel *= 100;
		                    istWinkel = (float) Math.round(istWinkel);
		                    istWinkel /= 100;
		                	if ((istWinkel > untere_Toleranz) && (istWinkel < obere_Toleranz))
		                		status = STATUS_EXECUTION_OK;
		                	else {
		                		status = STATUS_EXECUTION_ERROR;
			                	log(debug, "Error", "Nach der Werkerbest�tigung war der Winkel nicht mehr in Toleranz.");
			                	result = new Ergebnis( "Error", "UIToleranzGetLWMAngle", "", "", "", "Lenkwinkel", "" + istWinkel, "" + untere_Toleranz, "" + obere_Toleranz, "0", "", "", "", "Lenkwinkel war nach Werker-Best�tigung nicht mehr in Toleranz", "", Ergebnis.FT_NIO_SYS );
		                    	ergListe.add(result);
		                	}
		                }
		                break; // Ende case 1
			            
            	case 2:
	        			//
	        			// gemessenen Winkel anzeigen
	        			//
	            		log(debug, "Info", "SWITCH: Case 2, Anzeigen");
	            		dauer = Long.parseLong(getArg(getRequiredArgs()[4]));
	                	starttime = System.currentTimeMillis();
	                    
	                	//
	                    // Faktoren aus Datei auslesen
	                    //
	                    aValue = getCalibValue(channel, "A");
	                    bValue = getCalibValue(channel, "B");
                        log(debug, "Info", "Kennlinien-Faktoren wurden aus Datei " + fileName + " ausgelesen. A = " + aValue + "; B = " + bValue);
	                	
                        do {
	                        istValue = getMeasuredValue(analyzerUI, result_str, min_Spannung, max_Spannung);
	                        istWinkel = aValue * istValue + bValue + offset;
	                        
	                        // auf zwei Nachkommastellen formatieren
		                    istWinkel *= 100;
		                    istWinkel = (float) Math.round(istWinkel);
		                    istWinkel /= 100;
		                    istWinkelStr = String.valueOf(istWinkel);
		                    if (istWinkel >= 0) istWinkelStr = "+" + istWinkelStr; // positives Vorzeichen anf�gen
		                    //if (istWinkelStr.length() >= 6) istWinkelStr = istWinkelStr.substring(0, 5); // �bersch�ssige Nachkommastellen abschneiden
		                    
		                    if (largeFont) {
		                    	ud.setMessageFontSize(72);
		                    }
		                    
	                        if (lang) {
	                        	ud.displayMessage("Ausgabe", "Istwinkel:  " + istWinkelStr + "�", -1);
	                        } else {
	                        	ud.displayMessage("Output", "Actual Angle:  " + istWinkelStr + "�", -1);
	                        }

	                    } while  (System.currentTimeMillis() < starttime + dauer);
	                    status = STATUS_EXECUTION_OK;
	                    break; // Ende case 2
            } // Ende switch;
        } catch (PPExecutionException e) {
        	log(debug, "Error", "Ausf�hrungsfehler " + e.getMessage());
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", e.getMessage(), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
            e.printStackTrace();
        } catch (Exception e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
            e.printStackTrace();
        } catch (Throwable e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
            e.printStackTrace();
        }
        
//Freigabe der benutzten Devices
// 1. UIAnalyser
        if( analyzerUI != null ) {
            try {
                getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyser(devTag);
                log(debug, "Info", "UIAnalyser wurde freigegeben.");
            } catch (Exception  e) {
                if (e instanceof DeviceLockedException) {
                    result = new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "" , Ergebnis.FT_NIO_SYS );
                } else {
                    result = new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "" , Ergebnis.FT_NIO_SYS );
                }
                ergListe.add(result);
                status= STATUS_EXECUTION_ERROR;
                analyzerUI=null;
            }
        }
//
// 2. UserDialog
        if( ud != null ) {
            try {
                getPr�flingLaufzeitUmgebung().releaseUserDialog();
                log(debug, "Info", "UserDialog wurde freigegeben.");
            } catch (Exception e) {
                if (e instanceof DeviceLockedException) {
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
                } else {
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
                }
                ergListe.add(result);
                status = STATUS_EXECUTION_ERROR;
                ud = null;
            }
            udInUse = false;
        }        
// Ergebnisstatus setzen
        setPPStatus( info, status, ergListe );
        
    } // End of execute()
    
    
	/**
	 * Einlesen des gemessenen Werts
	 * 
	 * @param anUI UIAnalyser-Objekt
	 * @param r_str Art der Messung (U oder I)
	 * @param minVal unterer Grenzwert
	 * @param maxVal oberer Grenzwert
	 * @return Mittelwert
	 */
    private float getMeasuredValue(UIAnalyser anUI, String r_str, float minVal, float maxVal) { 
        Ergebnis result;
        Vector ergListe = new Vector();
        BoundInfo messung;         // Struktur mit Messinfos (Mittelwert, untere-, obere Grenze)
        float ist_Value=0.0f;
        
        try {
            if (r_str.equals("U") == true) {
                messung = anUI.getVoltageBoundInfo(false, 100);
            } else {
                messung = anUI.getCurrentBoundInfo(false, 100);
            }
            
            ist_Value=messung.getAverageValue()/1000.0f;
            minVal=messung.getMinValue()/1000.0f;
            maxVal=messung.getMaxValue()/1000.0f;

        } catch (DeviceParameterException e) {
            e.printStackTrace();
            result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "deviceParameterException" ), e.getMessage(), Ergebnis.FT_NIO_SYS);
            ergListe.add(result);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unzulaessigerADKanal" ), e.getMessage(), Ergebnis.FT_NIO_SYS);
            ergListe.add(result);
        } catch (DeviceIOException e) {
            e.printStackTrace();
            result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS);
            ergListe.add(result);
        } catch (Exception e) {
            e.printStackTrace();
            result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS);
            ergListe.add(result);
        }

        return ist_Value;
    }
    
    /**
     * Liest die Kalibrierfaktoren aus der entsprechenden Datei aus.
     * 
     * @param channel - der gew�nschte Kanal (1-8)
     * @param prefix - Pr�fix des gew�nschten Werts (A oder B)
     * @return den gelesenen Wert
     * @throws PPExecutionException
     */
    private float getCalibValue(int channel, String prefix) throws PPExecutionException {
    	float calibValue = -9999.9f;
    	StringBuffer temp = new StringBuffer();
    	String ioText ="";
    	int[] infoIndex = {-1, -1};
    	 
    	try {
    		BufferedReader fis = new BufferedReader( new FileReader(fileName));
			while ((ioText = fis.readLine()) != null) {
				temp.append(ioText);
			}
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PPExecutionException("Exception occurred during reading from file " + fileName + ".");
		}
		
		infoIndex[0] = temp.indexOf(prefix + Integer.toString(channel));
		
		if (infoIndex[0] >= 0) {
			infoIndex[1] = temp.indexOf(";", infoIndex[0]);
			calibValue = Float.parseFloat(temp.substring(infoIndex[0] + 5, infoIndex[1]));
		} else {
			throw new PPExecutionException("Calibration value for desired channel could not be found in " + fileName + ".");
		}
   	
    	return calibValue;
    }
    
	/**
	 * <b>getRegression</b>
	 * <br>
	 * Berechnet aus einer Menge von Messpunkten (x|y) die Regressionsgerade nach der
	 * Methode der kleinsten Quadrate, die durch den Nullwert (0�) geht.
	 * <br>
	 * @param voltage - array of measured voltages
	 * @param angle - array of measured angles
	 * @return line data {SLOPE, CROSSING}
	 */
	private float[] getRegressionThroughZero(float[] voltage, float[] angle)
	{
		int n = voltage.length;
		float sumX = 0.0f, sumY = 0.0f;	
		float sumXX = 0.0f, sumXY = 0.0f;
		float[] lineData = {0.0f, 0.0f}; 
		
		for (int i = 0; i < n; i++) {
			sumX  += voltage[i];			  // generating the sum of x-values
			sumY  += angle[i]; 		          // generating the sum of y-values
			sumXX += voltage[i]*voltage[i];   // generating the sum of product x*x
			sumXY += voltage[i]*angle[i];     // generating the sum of product x*y
		}
		// line slope:  m = (n[Exy] - [Ex][Ey]) / (n[Exx] - [Ex][Ex])
		lineData[0] = (n*sumXY - sumX*sumY) / (n*sumXX - sumX*sumX);
		// y-axis intersection:  b = ([Ey] - m[Ex]) / n
		//lineData[1] = (sumY - lineData[0]*sumX) / n;
		lineData[1] = -1 * voltage[1] * lineData[0]; // perform a little cheating to match the line to the zero point

		return lineData; 
	}

	/**
	 * <b>log</b>
	 * <br>
	 * Logs a message if the given debug flag is active or if it is an 'Error' message.
	 * <br>
	 * @param debug - the debug flag
	 * @param type - the type of message to display
	 * @param text - the message text
	 */
	private void log(boolean debug, String type, String text)
	{
		if (type.equalsIgnoreCase("Error") || debug) { 
			System.out.println("["+ getLocalName() +"] ["+ type +"] "+ text);
		}
	}
	
	/**
	 * Formatiert die gegebenen Zahl als String
	 * 
	 * @param Zahl - die zu formatierende Zahl
	 * @param nkStellen - die Anzahl an Nachkommastellen
	 */
	public String round(double Zahl, int nkStellen) 
	{ 
		NumberFormat nf = NumberFormat.getInstance(); 
		nf.setMinimumFractionDigits(nkStellen); 
		nf.setMaximumFractionDigits(nkStellen); 

		return nf.format(Zahl); 
	}
	
	/**
	 * Schreibt die Kalibrierfaktoren in die Datei faktoren.txt. Sind die Werte schon vorhanden, werden
	 * sie �berschrieben. Existiert die Datei noch nicht, wird sie angelegt.
	 * 
	 * @param channel - der Kanal, auf den sich die Faktoren beziehen (1-8)
	 * @param abValue - die beiden Werte f�r die Kennlinie
	 * @return true bei Erfolg, sonst false
	 * @throws PPExecutionException 
	 */
	
	private void writeValuesToFile(int channel, float [] abValue) throws PPExecutionException {
		String ioText = "";
		StringBuffer temp = new StringBuffer();
		int[] idx = {0,0};
		
		try {
			File file = new File(fileName);
			if (file.createNewFile()) log(true, "Info", "Created new file " + fileName + ".");		
		} catch (Exception e) {
			e.printStackTrace();
			throw new PPExecutionException("General exception occurred during reading from file" + fileName + ".");
		}
	
		try {
			BufferedReader fis = new BufferedReader( new FileReader(fileName));
			while ((ioText = fis.readLine()) != null) {
				temp.append(ioText);
				temp.append(System.getProperty("line.separator"));
			}
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PPExecutionException("General exception occurred during reading from file" + fileName + ".");
		}
	
		idx[0] = temp.indexOf("A" + channel);
		if (idx[0] == -1) {
			temp.append("A" + channel + " = " + abValue[0] + ";" + System.getProperty("line.separator"));
		} else {
			idx[1] = temp.indexOf(";", idx[0]);
			temp.replace(idx[0], idx[1], "A" + channel + " = " + abValue[0]);
		}
		
		idx[0] = temp.indexOf("B" + channel);
		if (idx[0] == -1) {
			temp.append("B" + channel + " = " + abValue[1] + ";" + System.getProperty("line.separator"));
		} else {
			idx[1] = temp.indexOf(";", idx[0]);
			temp.replace(idx[0], idx[1], "B" + channel + " = " + abValue[1]);
		}
		
		ioText = temp.toString();
		
		try {
			FileOutputStream fos = new FileOutputStream(fileName);
			fos.write(ioText.getBytes());
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PPExecutionException("General exception occurred during writing to file" + fileName + ".");
		}
	}

} // End of Class

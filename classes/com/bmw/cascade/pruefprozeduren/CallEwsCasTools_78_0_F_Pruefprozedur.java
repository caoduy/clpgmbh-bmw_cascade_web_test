/*
 * CallEwsCasTools_VX_X_X_ZZ_Pruefprozedur.java
 *
 * Created on 14.12.2006
 */
package com.bmw.cascade.pruefprozeduren;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.devices.relaycontrol.RelayControl;
import com.bmw.cascade.pruefstand.devices.serial.SerialGeneric;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.util.dom.DomPruefstand;

/**
 * Ruft das Jar File EwsCasTools_Cascade.jar auf.
 * Damit werden diverse CAS Abl�ufe durchgef�hrt.
 *
 * @author Daniel Frey
 * @version V1_0_F  16.03.2007 FD Initialversion
 * @version V2_0_F  16.03.2007 FD Innere Klasse EdiabasResult umbenannt in ECT_EdiabasResult (ggf. Namenskonflikt mit Cascadeklasse)
 * @version V3_0_F  16.03.2007 FD Klasse ECT_EdiabasResult eine Ebene nach oben gezogen, da sie sonst vermutlich beim Import auf den zentralen Server nicht mit erzeugt wird
 * @version V4_0_F  16.03.2007 FD Bei Aufl�sung von InputParametern (mit @) auch in Pr�fstandsvariablen schauen
 * @version V5_0_F  26.03.2007 FD InputParameter LogLevel
 * @version V6_0_F  05.04.2007 FD Besseren SelectDialog genommen 
 *                                Abort mit 2 Buttons m�glich 
 * @version V7_0_F  25.04.2007 FD Reihenfolge erhalten bei Results
 * @version V8_0_F  14.05.2007 FD Zwei Parameter f�r Motor SGBD: F�r DME 1 und DME2
 * @version V9_0_F  12.06.2007 FD Fensterfarbe   
 * @version V10_0_F 18.06.2007 FD InitEdiabas: Info ob 1 oder mehrere Interfaces
 *                                Bugfix in DetermineSGBD                             
 * @version V11_0_T 06.08.2007 FD Datensatz auch im lokalen Pr�fstand Distribution Verzeichnis suchen
 *                                2 Distributionsverzeichnisse: casXml und casAPDMXml                               
 * @version V12_0_F 06.08.2007 FD F-Version  
 * @version V13_0_T 01.10.2007 FD Bugfix bei Datensatz aus Distribution holen
 * @version V14_0_F 01.10.2007 FD F-Version
 * @version V15_0_T 19.12.2007 FD Neue opt. Parameter: LogFileSize, QuickResetDummyEcu, QuickResetRealEcu
 *                                EdiabasInterface erweitert: Mehrere Sets abholen
 *                                EcuPlace Auto: Immer InCar, wenn EdiabasDevice!=OMITEC    
 * @version V16_0_F 19.12.2007 FD F-Version
 * @version V17_0_T 08.02.2008 FD Weitere Cascade-Quelle: CASCADE_STATION = database\pruefstand\dom
 *                                Bessere Fehlermeldung, wenn Call in ewscastool_cascade.jar fehlschl�gt
 * @version V18_0_F 08.02.2008 FD F-Version
 * @version V19_0_T 28.03.2008 FD Wenn Teststand configuriert && Remotebreakpoint && !Interface==Omitec: Trotzdem Dummy Ecu
 *                                Cascade Result Parameter1
 * @version V20_0_F 28.03.2008 FD F-Version
 * @version V21_0_T 11.06.2008 FD Neuer optionaler Parameter History Split Size
 * @version V22_0_F 11.06.2008 FD F-Version
 * @version V23_0_T 10.07.2008 FD Bugfix: AbortDialog mit 2 Buttons + sich �nderndem Text ging nicht
 * @version V24_0_F 10.07.2008 FD F-Version
 * @version V25_0_T            FD Interface zu generischer serieller Schnittstelle
 *                                Weiterer Result Typ <repetition> 
 *                                Neuer optionaler Parameter ParallelKeyInit
 * @version V26_0_F            FD F-Version
 * @version V27_0_T 30.10.2008 FD Neuer optionaler Parameter ForceIgnition
 * @version V28_0_F 30.10.2008 FD F-Version
 * @version V29_0_T            FD UserDialog gr��er
 *                                Zus�tzliche Ausgaben bei userDecideYesNo (f�r F-Version wieder entfernen!)
 *                                Neue Parameter: SHOWINSERTMESSAGE, ALLOWLOCKSETNROVERRIDE
 * @version V30_0_T            FD UserDialoggr��e abh�ngig von Bildschirmgr��e                                                                
 * @version V31_0_F            FD F-Version    
 * @version V32_0_T            FD UserDialog etwas verkleinert (damit FGNR sichtbar)
 *                                CasVariants optional
 *                                Check des Parametertyps (Integer, Boolean)
 * @version V33_0_F            FD F-Version   
 * @version V34_0_T 07.05.2009 FD EcuPlace=Auto auch f�r CAS3
 *                                F6, F7 auf Buttons Wiederholen/Abbrechen mappen
 *                                Bei Ja/Nein Buttons '_' hinzuf�gen 
 * @version V35_0_F 07.05.2009 FD F-Version  
 * @version V37_0_T 24.06.2009 FD Dialog vergr��ern �ber invokeLater
 *                                Neuer optionaler Parameter EMERGENCYSTRATEGYATAPPL
 *                                und AFTERKEYLEARNTIME
 * @version V38_0_F 28.07.2009 FD F-Version
 * @version V39_0_T 15.09.2009 FD Cascade Bug Workaround: Repaint User Dialog verz�gert (durch eigenen Thread)
 * @version V40_0_F 15.09.2009 FD F-Version
 * @version V41_0_T 10.12.2009 FD Vorbereitung f�r Umstellung Pr�fstandskonfiguration
 * @version V42_0_F 10.12.2009 FD F-Version
 * @version V43_0_T            FD Umstellung auf Pr�fstandskonfiguration
 *                                Ab dieser Version mu� unbedingt EwsCasTools Version 0.95.000 oder h�her benutzt werden
 * @version V44_0_F 10.12.2009 FD F-Version
 * @version V45_0_T 01.03.2010 FD optionaler Parameter DOEWS5 hinzu 
 * @version V46_0_F 01.03.2010 FD F-Version
 * @version V47_0_T 01.03.2010 FD SGBD in Ediabas Result Objects speichern
 * @version V48_0_F 01.03.2010 FD F-Version
 * @version V49_0_T 15.06.2011 FD Ein DiagOpen unter Cascade �ffnet nur das erste EdiabasInterface mit apiInitExt
 *                                An einem Pr�fstand mit 2 (oder mehr) Interfaces, macht diese PP selber das 
 *                                apiInitExt und apiEnd f�r alle weiteren Interfaces.
 * @version V50_0_F 15.06.2011 FD F-Version
 * @version V51_0_T 11.04.2012 FD Neuer optionaler Parameter HASCARSHARINGMODULE
 * @version V52_0_F 11.04.2012 FD F-Version
 * @version V53_0_T 16.04.2012 FD Bugfix f�r HASCARSHARINGMODULE
 * @version V54_0_F 16.04.2012 FD F-Version
 * @version V55_0_T 30.07.2012 FD promptForString needed for BarcodeInput
 * @version V56_0_F 30.07.2012 FD F-Version
 * @version V57_0_T 14.01.2014 FD F-Version Neue optionale Parameter TESTSTANDECU1-3: Damit k�nnen die ECUs f�r generische Pr�fstandskonfigurationen gesetzt werden
 * @version V58_0_F 14.01.2014 FD F-Version
 * @version V59_0_F 27.08.2014 SBS Implemented apiSetConfig and apiGetConfig
 * @version V60_0_F 16.09.2014 SBS Added the new optional parameter MechcodeMode
 * @version V61_0_F 14.10.2014 SBS Neuer Parameter "NoBlockingDialogs" hinzugef�gt
 * @version V62_0_F 25.11.2014 SBS Anpassung der Dialoge zur Unterst�tzung von nicht blockierenden Dialogen. Neuer Parameter "NoBlockingDialogs_Timeout" hinzugef�gt.
 * @version V63_0_F 25.11.2014 SBS Neuer Parameter "NUM_AUTO_RETRIES" hinzugef�gt
 * @version V64_0_F 26.11.2014 SBS Compiler Warnungen entfernt
 * @version V65_0_T 09.12.2014 SBS T-Version f�r den Test im Werk
 * @version V66_0_F 09.12.2014 SBS F-Version
 * @version V67_0_T 07.01.2015 SBS Keytype wird jetzt als String �bergeben damit dieser auch z.B. auch "N" sein kann.
 * @version V68_0_F 07.01.2015 SBS F-Version
 * @version V69_0_T 28.01.2015 SBS Bei KEYINFO gibt es jetzt einen dritten optionalen Parameter f�r die Schl�sselposition (KEYPOSITION).
 * @version V70_0_F 28.01.2015 SBS F-Version
 * @version V71_0_T 05.08.2015 SBS Es wurde ein neuer optionaler Parameter "TOOLVERSION_MIN" hinzugef�gt. Dieser Parameter kann eine mindestens ben�tigte EwsCasTool Versionsnummer beinhalten.
 * @version V72_0_F 05.08.2015 SBS F-Version
 * @version V73_0_T 09.09.2015 PW Bug-Fix f�r Kompatibilit�t zur Version 1.07.021 des EWSCASTool (�ltere und neuere Versionen sind zur Version 72 kompatibel).
 * @version V74_0_F 09.09.2015 SBS F-Version
 * @version V75_0_T 11.01.2016 ANR LOP 2015_087: Umsetzung Sachnnummernpr�fung der Schl�ssel.
 * @version V76_0_F 04.03.2016 ANR F-Version
 * @version V77_0_T 28.11.2016 ANR Erweiterung DGV: Neuer optionaler Paremeter CM_MODE. Default: MODE_NONE
 *                                 Neuer optionaler Parameter TraceSecretKeys, um SKs und CryptoManager Aufrufe zu tracen
 * @version V78_0_F 21.02.2017 ANR F-Version
 * 
 */
public class CallEwsCasTools_78_0_F_Pruefprozedur extends AbstractBMWPruefprozedur 
{
   /** */
   static final long serialVersionUID = 1L;    
    
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public CallEwsCasTools_78_0_F_Pruefprozedur() {}

  /**
    * erzeugt eine neue Pruefprozedur.
    * @param pruefling Klasse des zugeh. Pr�flings
    * @param pruefprozName Name der Pr�fprozedur
    * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
    */
   public CallEwsCasTools_78_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) 
   {
      super( pruefling, pruefprozName, hasToBeExecuted );
      attributeInit();
   }
   
   /** Pr�fling in dem nach Attributen gesucht wird */
   private String m_Storage;

   
   /** initialsiert die Argumente */
   protected void attributeInit() 
   {
      super.attributeInit();
   }
   
   /** Parameter, die den Ablauf direkt betreffen (ergeben sich
    * aus PU und FA) 
    * 1. Spalte: S String, I Integer, B Boolean
    * 2. Spalte: Parametername
    * 3. Spalte: R Required, O Optional
    * */
   private String[] m_OrderData={ 
                         "S", "PROCESS_ID", "R",
                         "S", "VIN17", "R",
                         "S", "ECUEWSTYPE", "R",  
                         "S", "CASVARIANTS", "O",
                         "S", "TLAYOUT", "O",
                         "I", "FREQUENCY", "O",
           
                         "S", "CASPARTNUMBER", "O",
                         "S", "CASPARTNUMBER2", "O",
                         "S", "LOCKSETNUMBER", "O",
                         "S", "LOCKSETNUMBER2", "O",

                         "B", "USEDEFAULTISN", "O",
                         "B", "USELONGISN", "O",
                         "B", "USEDEFAULTEWS4SK", "O",
                         "B", "DOEWS4SYNC", "O",
                         "I", "DOEWS5", "O",
                         "B", "HASCARSHARINGMODULE", "O",
                         
                         "S", "MechcodeMode", "O"
                         };

   /** Paramter, die den Optionen von EwsCasTools entsprechen (ergeben sich aus dem PL, bzw. Pr�fstandvariablen) */
   private String[] m_EnvironmentData={ "I", "LogLevel", 
                   "S", "Station",
                   "S", "Location",
                   "S", "URL_CAS",
                   "S", "URL_EWS4",
                   "I", "DOMTimeout",
                   "B", "OldURLForCAS",
                   "S", "ModifiedPath",
                   "S", "HistoryPath",
                   "S", "ToDOMPath",
                   "S", "SaveDatPath",
                   "S", "DatasetSources",
                   "I", "KeyInsertTimeout",
                   "I", "KeyLearnTimeout",
                   "I", "IgnitionOnTimeout",
                   "B", "NotLocking",
                   "B", "KeyNotLockingCAS4",
                   "S", "KeepAlive",
                   "S", "EcuPlace",
                   "B", "WithEngaging", 
                   "S", "CAS_sgbd",
                   "S", "MOTOR1_sgbd",
                   "S", "MOTOR2_sgbd",
                   "B", "RemoteBreakpoints",
                   "I", "LogFileSize",
                   "B", "QuickResetDummyEcu",
                   "B", "QuickResetRealEcu",
                   "B", "DoKeyCheck",
                   "I", "HistorySplitSize",
                   "B", "ParallelKeyInit",
                   "B", "ForceIgnition",
                   "B", "ShowInsertMessage",
                   "B", "AllowLocksetNrOverride",
                   "B", "EmergencyStrategyAtAPPL",
                   "B", "KeyInitDSPresentIO",
                   "I", "AfterKeyLearnTime",
                   "S", "TeststandConfig",
                   "S", "TeststandEcu1",
                   "S", "TeststandEcu2",
                   "S", "TeststandEcu3",
                   "B", "NoBlockingDialogs",
                   "I", "NoBlockingDialogs_Timeout",
                   "I", "NUM_AUTO_RETRIES",
                   "S", "TOOLVERSION_MIN",
                   "S", "CM_MODE",
                   "B", "TraceSecretKeys"
                   };

           
   /** liefert die zwingend erforderlichen Argumente */
   public String[] getRequiredArgs() 
   {
      String[] args = { "PROCESS_ID",
                        "VIN17",
                        "ECUEWSTYPE"
                      };
      return args;
   }

   
   /** liefert die optionalen Argumente */
   public String[] getOptionalArgs() 
   {
      String[] args = {"STORAGE", 
                       "KEYINFO1", "KEYINFO2", "KEYINFO3",
                       "TLAYOUT",
                       "CASVARIANTS",
                       "CASPARTNUMBER",
                       "CASPARTNUMBER2",
                       "LOCKSETNUMBER",
                       "LOCKSETNUMBER2",
                       "USEDEFAULTISN",
                       "USELONGISN",
                       "USEDEFAULTEWS4SK",
                       "DOEWS4SYNC",
                       "DOEWS5",
                       "FREQUENCY",
                       "LOGLEVEL", 
                       "STATION",
                       "LOCATION",
                       "URL_CAS",
                       "URL_EWS4",
                       "DOMTIMEOUT",
                       "OLDURLFORCAS",
                       "HISTORYPATH",
                       "TODOMPATH",
                       "DATASETSOURCES",
                       "KEYINSERTTIMEOUT",
                       "KEYLEARNTIMEOUT",
                       "IGNITIONONTIMEOUT",
                       "NOTLOCKING",
                       "KEYNOTLOCKINGCAS4",
                       "KEEPALIVE",
                       "ECUPLACE",
                       "WITHENGAGING", 
                       "CAS_SGBD",
                       "MOTOR1_SGBD",
                       "MOTOR2_SGBD",
                       "REMOTEBREAKPOINTS",
                       "LOGFILESIZE",
                       "QUICKRESETDUMMYECU",
                       "QUICKRESETREALECU",
                       "DOKEYCHECK",
                       "HISTORYSPLITSIZE",
                       "PARALLELKEYINIT",
                       "FORCEIGNITION",
                       "SHOWINSERTMESSAGE",
                       "ALLOWLOCKSETNROVERRIDE",
                       "EMERGENCYSTRATEGYATAPPL",
                       "KEYINITDSPRESENTIO",
                       "AFTERKEYLEARNTIME",
                       "TESTSTANDCONFIG",
                       "HASCARSHARINGMODULE",
                       "TESTSTANDECU1",
                       "TESTSTANDECU2",
                       "TESTSTANDECU3",
                       "MECHCODEMODE",
                       "NOBLOCKINGDIALOGS",
                       "NOBLOCKINGDIALOGS_TIMEOUT",
                       "NUM_AUTO_RETRIES",
                       "TOOLVERSION_MIN",
                       "CM_MODE",
                       "KEY1_SNR", "KEY2_SNR","KEY3_SNR",
                       "KEY1_SNR_ALT", "KEY2_SNR_ALT", "KEY3_SNR_ALT"
                       };
      return args;
   }
   
   /** */
   static final String TXT_CONTINUE_DE="Weiter";
   /** */
   static final String TXT_CONTINUE_EN="Continue";
   /** */
   static final String TXT_RETRY_DE="Wiederholen";
   /** */
   static final String TXT_RETRY_EN="Retry";
   /** */
   static final String TXT_ABORT_DE="Abbrechen";
   /** */
   static final String TXT_ABORT_EN="Abort";
   
   /** �berpr�fung der Parameter f�r diese PP 
    * @return mindestens ein Parameterfehler gefunden
    */
   public boolean checkArgs() 
   {
       Vector<Ergebnis> errors = new Vector<Ergebnis>();
       return checkArgs(errors);
   }

   /** �berpr�fung der Parameter f�r diese PP 
    * 
    * @param errors Cascaderesults mit gefundenen Fehlern
    * @return mindestens ein Parameterfehler gefunden
    */
   public boolean checkArgs(Vector<Ergebnis> errors) 
   {
   Ergebnis result;
   
      try 
      {
          if (!super.checkArgs()) 
          {
              if (isDE()) result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler", Ergebnis.FT_NIO_SYS );
              else result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error", Ergebnis.FT_NIO_SYS );
              errors.add(result);
              return false;
          }
         
          return true;
      } 
      catch (Exception e) 
      {
          return false;
      } 
      catch (Throwable e) 
      {
          return false;
      }
   }
   
   
   /** f�hrt die Pr�fprozedur aus
    * 
    * @param info Information zur Ausf�hrung
    */
   public void execute( ExecutionInfo info ) 
   {
      int status = STATUS_EXECUTION_OK;
      int i;
      String ewscastools_result_xml;
      boolean Required;
      String EwsCasToolsVersion;
      String remoteBreakpointsStr;
      String CasType;
      
      String VerStrs[];
      int MajVersion=0;
      int MinVersion=0;
      int PatchVersion=0;
      int SollMajVersion=0;
      int SollMinVersion=95;

      Ergebnis result;
      Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
      try 
      {
    	  // Parameter TRACESECRETKEYS verstecken
    	  Hashtable parametersHashTable = getArgs();
    	  String valueTraceSecretKeys = null;
    	  
    	  if (parametersHashTable.containsKey("TRACESECRETKEYS")) {
    		  valueTraceSecretKeys = (String)parametersHashTable.get("TRACESECRETKEYS");
    		  parametersHashTable.remove("TRACESECRETKEYS");
    	  }
    	  
         //Parameter holen
         if ( checkArgs(ergListe) == false ) throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
         
   	  	if (valueTraceSecretKeys != null)
   	  		parametersHashTable.put("TRACESECRETKEYS", valueTraceSecretKeys);
         
         m_Storage=getArg("STORAGE");
         if (m_Storage==null) m_Storage="";

         remoteBreakpointsStr=getValue("REMOTEBREAKPOINTS", false);
         if (remoteBreakpointsStr==null) remoteBreakpointsStr="FALSE";
         
         // Aufruf XML-String f�r EwsCasTools erzeugen
         StringBuffer ewscastools_input_parameter_xml = new StringBuffer();
         ewscastools_input_parameter_xml.append("<?xml version=\"1.0\"?>\r\n");
         ewscastools_input_parameter_xml.append("<batch>\r\n");
         ewscastools_input_parameter_xml.append("\t<PPVersion>"+getClass().getName()+"</PPVersion>\r\n");

         for (i=0; i<m_OrderData.length; i+=3)
         {
             try
             {
                 Required=m_OrderData[i+2].equalsIgnoreCase("R");
                 switch (m_OrderData[i].charAt(0))
                 {
                 case 'I': addIntItem(ewscastools_input_parameter_xml, m_OrderData[i+1], Required); break;
                 case 'B': addBoolItem(ewscastools_input_parameter_xml, m_OrderData[i+1], Required); break;
                 default:  addStringItem(ewscastools_input_parameter_xml, m_OrderData[i+1], Required); break;
                 }
             }
             catch (PPExecutionException ex)
             {
                 if (isDE()) result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Parameterfehler: "+ex.getMessage(), "", Ergebnis.FT_NIO_SYS );
                 else result = new Ergebnis( "ExecError", "", "", "", "", "", "", "", "", "0", "", "", "", "Parametererror: "+ex.getMessage(), "", Ergebnis.FT_NIO_SYS );
                 ergListe.add(result);
                 throw ex;
             }
         }

         CasType=getValue(getRequiredArgs()[2], true);
         
         ewscastools_input_parameter_xml.append("\t<KEYINFOS>\r\n");
         
         i=0;
         while (true)
         {
             String values;
             String[] valuesArr;
             values=getArg("KEYINFO"+((char)('1'+i)));
             if (values==null) break;
             if (values.length()==0) break;
             ewscastools_input_parameter_xml.append("\t\t<KEYINFO>\r\n");
             
             valuesArr=values.split(";");
             
             ewscastools_input_parameter_xml.append("\t\t\t<KEYTYPE type=\"int\">"+resolve(valuesArr[0])+"</KEYTYPE>\r\n");//wird zur Kompabilit�t mit alten Tool Versionen ben�tigt
             ewscastools_input_parameter_xml.append("\t\t\t<KEYTYPE_STRING type=\"string\">"+resolve(valuesArr[0])+"</KEYTYPE_STRING>\r\n");

             ewscastools_input_parameter_xml.append("\t\t\t<KEYVARIANTS type=\"string\">");
             if (valuesArr.length>=2) ewscastools_input_parameter_xml.append(resolve(valuesArr[1]));
             ewscastools_input_parameter_xml.append("</KEYVARIANTS>\r\n");

             if (valuesArr.length>=3) {
	             ewscastools_input_parameter_xml.append("\t\t\t<KEYPOSITION type=\"string\">");
	             ewscastools_input_parameter_xml.append(resolve(valuesArr[2]));
	             ewscastools_input_parameter_xml.append("</KEYPOSITION>\r\n");
             }
             
             // LOP 2015_087: Umsetzung Sachnnummernpr�fung der Schl�ssel
             String sachnummer = getArg("KEY"+ (i+1) +"_SNR");
             if (sachnummer != null) {
	             ewscastools_input_parameter_xml.append("\t\t\t<KEY_SNR type=\"string\">");
	             ewscastools_input_parameter_xml.append(resolve(sachnummer));
	             ewscastools_input_parameter_xml.append("</KEY_SNR>\r\n");
             }
             sachnummer = getArg("KEY"+ (i+1) +"_SNR_ALT");
             if (sachnummer != null) {
	             ewscastools_input_parameter_xml.append("\t\t\t<KEY_SNR_ALT type=\"string\">");
	             ewscastools_input_parameter_xml.append(resolve(sachnummer));
	             ewscastools_input_parameter_xml.append("</KEY_SNR_ALT>\r\n");
             }
             
             ewscastools_input_parameter_xml.append("\t\t</KEYINFO>\r\n");
             i++;
         }
         
         ewscastools_input_parameter_xml.append("\t</KEYINFOS>\r\n");
         
         ewscastools_input_parameter_xml.append("\t<configuration>\r\n");
         
         for (i=0; i<m_EnvironmentData.length; i+=2)
         {
             if (m_EnvironmentData[i+1].equals("EcuPlace"))
             {
                 String EcuPlace;
                 EcuPlace=getValue(m_EnvironmentData[i+1], false);
                 if (EcuPlace!=null)
                 {
                     // Bei EcuPlace "Auto":
                     // -> Incar, wenn kein Relay-Device gefunden wird oder (EdiabasDevice!=Omitec und nicht RemoteBreakpoints)
                     // -> NotIncar, wenn ein Relay-Device gefunden wird, aber kein Locker konfiguriert ist
                     if (EcuPlace.equalsIgnoreCase("Auto"))
                     {
                         RelayControl RelayCtrl;
                         EdiabasProxyThread ept;
                         boolean InterfaceIsOmitec;
                         boolean InterfaceIsFunk;
                         String if_str;
                         
                         if (isDE()) result = new Ergebnis( "EcuPlaceAuto", "", "", "", "", "", "", "", "", "0", "", "", "", "EcuPlace ist 'Auto'", "", Ergebnis.FT_IO );
                         else result = new Ergebnis( "EcuPlaceAuto", "", "", "", "", "", "", "", "", "0", "", "", "", "EcuPlace is 'Auto'", "", Ergebnis.FT_IO );
                         ergListe.add(result);
                         
                         InterfaceIsOmitec=true;
                         InterfaceIsFunk=false;
                         try 
                         {
                             RelayCtrl = getPr�flingLaufzeitUmgebung().getDeviceManager().getRelayControl();
                             try
                             {
                                 ept = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();
                                 ept.apiJob("UTILITY", "INTERFACE", "", "");

                                 if_str=ept.apiResultText("TYP", 1, "");
                                 if (if_str!=null)
                                 {
                                     if_str=if_str.toUpperCase();
                                     InterfaceIsOmitec=(if_str.indexOf("OMITEC")>=0) || (if_str.indexOf("OMIC01")>=0);
                                     InterfaceIsFunk=(if_str.indexOf("FUNK")>=0);
                                 }
                             }
                             catch ( Exception e1 ) 
                             {
                             }
                         } 
                         catch( Exception e ) 
                         {
                             try
                             {
                                 getPr�flingLaufzeitUmgebung().getDeviceManager().releaseRelayControl();
                             }
                             catch( Exception ex ) {} catch(Throwable th) {}
                             RelayCtrl = null;
                         }
                         
                         if (isDE()) result = new Ergebnis( "EcuPlaceAuto", "", "", "", "", "", "", "", "", "0", "", "", "", 
                                 "RelayControl ist "+ (RelayCtrl==null ? "nicht " : "")+"vorhanden\r\n"+
                                 "Interface ist "+ (!InterfaceIsOmitec ? "nicht " : "")+"Omitec",
                                 "", Ergebnis.FT_IO );
                         else result = new Ergebnis( "EcuPlaceAuto", "", "", "", "", "", "", "", "", "0", "", "", "", 
                                 "RelayControl is "+ (RelayCtrl==null ? "not " : "")+"present\r\n"+
                                 "Interface is "+ (!InterfaceIsOmitec ? "not " : "")+"Omitec",
                                 "", Ergebnis.FT_IO );
                         ergListe.add(result);
                         
                         
                         if (RelayCtrl==null)
                         {
                             addItemToXml(ewscastools_input_parameter_xml, m_EnvironmentData[i+1], "string", "InCar");
                         }
                         else if (CasType.equalsIgnoreCase("CAS4") && !remoteBreakpointsStr.equalsIgnoreCase("TRUE") && !InterfaceIsOmitec)
                         {
                             // Bei CAS4 haben alle Schl�sselinitstationen OMITEC:
                             // D.h. wenn kein Omitec, dann keine Schl�sselinitstationen (->Incar)
                             // Ausnahme: Automatische Tests mit Simulation (Remotebreakpoints) haben als Interface OBD eingestellt
                             addItemToXml(ewscastools_input_parameter_xml, m_EnvironmentData[i+1], "string", "InCar");
                         }
                         else if (CasType.equalsIgnoreCase("CAS3") && !remoteBreakpointsStr.equalsIgnoreCase("TRUE") && InterfaceIsFunk)
                         {
                             // Bei CAS3: Schl�sselinitstationen haben sicher kein FUNK interface
                             // D.h. wenn FUNK, dann keine Schl�sselinitstationen (->Incar)
                             addItemToXml(ewscastools_input_parameter_xml, m_EnvironmentData[i+1], "string", "InCar");
                         }
                         else
                         {
                             addItemToXml(ewscastools_input_parameter_xml, m_EnvironmentData[i+1], "string", "NotInCar");
                             
                         }
                         
                         if (RelayCtrl!=null)
                         {
                             try
                             {
                                 getPr�flingLaufzeitUmgebung().getDeviceManager().releaseRelayControl();
                             }
                             catch( Exception ex ) {} catch(Throwable th) {}
                             RelayCtrl = null;
                         }
                         
                         continue;
                     } // if Ecuplace == "Auto"
                 } // if Ecuplace != null
             } // if Item == EcuPlace
             
             try
             {
                 switch (m_EnvironmentData[i].charAt(0))
                 {
                 case 'I': addIntItem(ewscastools_input_parameter_xml, m_EnvironmentData[i+1], false); break;
                 case 'B': addBoolItem(ewscastools_input_parameter_xml, m_EnvironmentData[i+1], false); break;
                 default:  addStringItem(ewscastools_input_parameter_xml, m_EnvironmentData[i+1], false); break;
                 }
             }
             catch (PPExecutionException ex)
             {
                 if (isDE()) result = new Ergebnis( "ExecFehler", "Parameterfehler", "", "", "", "", "", "", "", "0", "", "", "", ex.getMessage(), "", Ergebnis.FT_NIO_SYS );
                 else result = new Ergebnis( "ExecError", "Parametererror", "", "", "", "", "", "", "", "0", "", "", "", ex.getMessage(), "", Ergebnis.FT_NIO_SYS );
                 ergListe.add(result);
                 throw ex;
             }
         }
         
         ewscastools_input_parameter_xml.append("\t</configuration>\r\n");
         
         ewscastools_input_parameter_xml.append("</batch>\r\n");         
         
         // EwsCasTools Aufruf
         try
         {
             EwsCasToolsVersion=(new ExtCall()).getVersion();
             VerStrs=EwsCasToolsVersion.split("\\.");
             MajVersion=0;
             MinVersion=0;
             PatchVersion=0;
             if (VerStrs.length>=2)
             {
                 try
                 {
                     MajVersion=Integer.parseInt(VerStrs[0]);
                 }
                 catch (NumberFormatException ex) {}
                 try
                 {
                     MinVersion=Integer.parseInt(VerStrs[1]);
                 }
                 catch (NumberFormatException ex) {}
             }
             if(VerStrs.length>=3)
             {
            	 try
                 {
                     PatchVersion=Integer.parseInt(VerStrs[2]);
                 }
                 catch (NumberFormatException ex) {}
             }
         }
         catch (Throwable e)
         {
             e.printStackTrace();
             
             if (isDE()) result = new Ergebnis( "ExecFehler", "EwsCasTools", "", "", "", "", "", "", "", "0", "", "", "", "Klasse ewscastools.CascadeCall konnte nicht erzeugt werden", "Vermutlich ewscastools_cascade.jar in lib/ext nicht gefunden oder nicht im Classpath,\r\n"+e.getClass().getName()+" \r\n"+e.getMessage(), Ergebnis.FT_NIO_SYS );
             else result = new Ergebnis( "ExecFehler", "EwsCasTools", "", "", "", "", "", "", "", "0", "", "", "", "Class ewscastools.CascadeCall could not be created", "Probably ewscastools_cascade.jar in lib/ext not found or not in classpath,\r\n"+e.getClass().getName()+" \r\n"+e.getMessage(), Ergebnis.FT_NIO_SYS ); 
             ergListe.add(result);
             throw new PPExecutionException();             
         }
         
         //Pr�fe ob die Tool Version mindestens so hoch ist wie im Parameter TOOLVERSION_MIN angegeben (falls vorhanden).
         String version_min=getArg("TOOLVERSION_MIN");
         if (version_min==null) 
         {
        	 //optionaler Parameter ist nicht vorhanden
        	 version_min="";

        	 if (MajVersion<SollMajVersion || (MajVersion==SollMajVersion && MinVersion<SollMinVersion))
        	 {
        		 if (isDE()) result = new Ergebnis( "ExecFehler", "EwsCasTools", "", "", "", "", "", "", "", "0", "", "", "", "EwsCasTools Version zu niedrig", 
                                                	"Mind. "+SollMajVersion+"."+SollMinVersion+".000 erforderlich. "+EwsCasToolsVersion+" gefunden.", Ergebnis.FT_NIO_SYS );
        		 else result = new Ergebnis( "ExecFehler", "EwsCasTools", "", "", "", "", "", "", "", "0", "", "", "", "EwsCasTools version too low", 
                                         	"Min. "+SollMajVersion+"."+SollMinVersion+".000 needed. Found "+EwsCasToolsVersion+".", Ergebnis.FT_NIO_SYS ); 
        		 ergListe.add(result);
        		 throw new PPExecutionException();             
        	 }
         }
         else
         {
        	 //Versionsnummer in die Bestandteile zerlegen und �berpr�fen
        	 String [] version_min_array = version_min.split("\\.");
        	 if(version_min_array.length == 3)
        	 {
        		 int version_min_major;
        		 int version_min_minor;
        		 int version_min_patch;
        		 try
        		 {
        			version_min_major = Integer.parseInt(version_min_array[0]);
        		 	version_min_minor = Integer.parseInt(version_min_array[1]);
        		 	version_min_patch = Integer.parseInt(version_min_array[2]);
        		 }
        		 catch(NumberFormatException e)
        		 {
        			 //Fehler beim parsen der angegebenen Versionsnummer
        			 if (isDE()) result = new Ergebnis( "ExecFehler", "EXECUTE", "", "", "", "", "", "", "", "0", "", "", "", "Parameterformat: TOOLVERSION_MIN hat das falsche Format", "Parametrierfehler", Ergebnis.FT_NIO_SYS );
                     else result = new Ergebnis( "ExecError", "EXECUTE", "", "", "", "", "", "", "", "0", "", "", "", "parameter format error: TOOLVERSION_MIN has an invalid format", "parameter error", Ergebnis.FT_NIO_SYS );
        			 ergListe.add(result);
        			 throw new PPExecutionException(); 
        		 }
        		 
        		 if(MajVersion<version_min_major || (MajVersion==version_min_major && MinVersion<version_min_minor) || (MajVersion==version_min_major && MinVersion==version_min_minor && PatchVersion<version_min_patch))
        		 {
        			 if (isDE()) result = new Ergebnis( "ExecFehler", "EwsCasTools", "", "", "", "", "", "", "", "0", "", "", "", "EwsCasTools Version zu niedrig", 
                         	"Mind. "+version_min_major+"."+version_min_minor+"."+ version_min_patch +" erforderlich. "+EwsCasToolsVersion+" gefunden.", Ergebnis.FT_NIO_SYS );
        			 else result = new Ergebnis( "ExecFehler", "EwsCasTools", "", "", "", "", "", "", "", "0", "", "", "", "EwsCasTools version too low", 
        					 "Min. "+version_min_major+"."+version_min_minor+"." + version_min_patch +" needed. Found "+EwsCasToolsVersion+".", Ergebnis.FT_NIO_SYS ); 
        			 ergListe.add(result);
        			 throw new PPExecutionException(); 			 
        		 }
        		 	 
        	 }
        	 else
        	 {
        		//Fehler beim parsen der angegebenen Versionsnummer
    			 if (isDE()) result = new Ergebnis( "ExecFehler", "EXECUTE", "", "", "", "", "", "", "", "0", "", "", "", "Parameterformat: TOOLVERSION_MIN hat die falsche L�nge.", "Parametrierfehler", Ergebnis.FT_NIO_SYS );
                 else result = new Ergebnis( "ExecError", "EXECUTE", "", "", "", "", "", "", "", "0", "", "", "", "parameter format error", "parameter error: TOOLVERSION_MIN has an incorrect length", Ergebnis.FT_NIO_SYS );
    			 ergListe.add(result);
    			 throw new PPExecutionException(); 
        	 }
         }
         
         try
         {
             ewscastools_result_xml=(new ExtCall()).doCall(ewscastools_input_parameter_xml);
         }
         catch (Throwable e)
         {
             e.printStackTrace();
             if (isDE()) result = new Ergebnis( "ExecFehler", "EwsCasTools "+EwsCasToolsVersion, "", "", "", "", "", "", "", "0", "", "", "", "Fehler bei Aufruf von Klasse lib\\ext\\ewscastools_cascade.jar, ewscastools.CascadeCall", e.getClass().getName()+" \r\n"+e.getMessage(), Ergebnis.FT_NIO_SYS );
             else result = new Ergebnis( "ExecFehler", "EwsCasTools "+EwsCasToolsVersion, "", "", "", "", "", "", "", "0", "", "", "", "Error on call of class lib\\ext\\ewscastools_cascade.jar, ewscastools.CascadeCall", e.getClass().getName()+" \r\n"+e.getMessage(), Ergebnis.FT_NIO_SYS ); 
             ergListe.add(result);
             throw new PPExecutionException();             
         }
         
         // Auswertung des EwsCasTools Aufrufs (Auswertung des zur�ckgegeben XML-Results)
         
         if (ewscastools_result_xml==null)
         {
             if (isDE())
                 result = new Ergebnis( "EwsCasToolsError", "EwsCasTools "+(new ExtCall()).getVersion(), "", "", "", "", "", "", "", "0", "", "", "", "No result returned", "", Ergebnis.FT_NIO );
             else
                 result = new Ergebnis( "EwsCasToolsError", "EwsCasTools "+(new ExtCall()).getVersion(), "", "", "", "", "", "", "", "0", "", "", "", "Kein Ergebnis zur�ckgegeben", "", Ergebnis.FT_NIO );
             ergListe.add(result);
             status = STATUS_EXECUTION_ERROR;
         }
         else 
         {
             int ergtype;
             int pos, pos0, pos1, pos2, pos3;
             int minpos, endpos1, endpos9, endpos10;
             String startpattern0, startpattern1, startpattern2, startpattern3, endpattern;
             String Code, Text, Parameter1;
             String attributes;
             Map<String, String> att_map;
             
             startpattern0="<error code=\"";
             startpattern1="<warning code=\"";
             startpattern2="<info name=\"";
             startpattern3="<repetition code=\"";
             pos=0;
             while (true)
             {
                 pos0=ewscastools_result_xml.indexOf(startpattern0, pos);
                 pos1=ewscastools_result_xml.indexOf(startpattern1, pos);
                 pos2=ewscastools_result_xml.indexOf(startpattern2, pos);
                 pos3=ewscastools_result_xml.indexOf(startpattern3, pos);
                 
                 minpos=-1;
                 endpattern=null;
                 ergtype=-1;
                 if (pos0>=0)
                 {
                     minpos=pos0+startpattern0.length(); ergtype=0; endpattern="</error>";
                 }
                 if (pos1>=0)
                 {
                     if (minpos<0 || pos1<minpos) { minpos=pos1+startpattern1.length(); ergtype=1; endpattern="</warning>"; }
                 }
                 if (pos2>=0)
                 {
                     if (minpos<0 || pos2<minpos) { minpos=pos2+startpattern2.length(); ergtype=2; endpattern="</info>"; }
                 }
                 if (pos3>=0)
                 {
                     if (minpos<0 || pos3<minpos) { minpos=pos3+startpattern3.length(); ergtype=3; endpattern="</repetition>"; }
                 }
                 if (minpos<0) break;
                 
                 endpos1=ewscastools_result_xml.indexOf("\"", minpos);
                 endpos9=-1;
                 endpos10=-1;
                 if (endpos1>=0)
                 {
                     endpos9=ewscastools_result_xml.indexOf(">", endpos1);
                 }
                 if (endpos9>=0)
                 {
                     endpos10=ewscastools_result_xml.indexOf(endpattern, endpos9);
                 }
                 if (endpos10>=0)
                 {
                     Code=ewscastools_result_xml.substring(minpos, endpos1);
                     Text=ewscastools_result_xml.substring(endpos9+1, endpos10);
                     Parameter1="";

                     attributes=ewscastools_result_xml.substring(endpos1+1, endpos9);
                     att_map=getAttributes(attributes);
                     
                     Parameter1=(String)(att_map.get("parameter1"));
                     if (Parameter1==null) Parameter1="";

                     Code.replaceAll("&lw", "<");
                     Code.replaceAll("&gt", ">");
                     Text.replaceAll("&lw", "<");
                     Text.replaceAll("&gt", ">");

                     // XML-Results in Cascaderesults verpacken
                     if (ergtype==0)
                     {
                         result = new Ergebnis( "EwsCasToolsError", "EwsCasTools "+EwsCasToolsVersion, "", "", "", "", "", "", "", "0", "", "", "", Text, "ErrorCode: "+Code, Ergebnis.FT_NIO );
                         ergListe.add(result);
                         status = STATUS_EXECUTION_ERROR;
                     }
                     else if (ergtype==1)
                     {
                         result = new Ergebnis( "EwsCasToolsWarning", "EwsCasTools "+EwsCasToolsVersion, "", "", "", "", "", "", "", "0", "", "", "", Text, "WarningCode: "+Code, Ergebnis.FT_IGNORE );
                         ergListe.add(result);
                     }
                     else if (ergtype==3)
                     {
                         int h, k;
                         String Anzahl;
                         
                         // Anzahl der Wiederholungen sollte in Text stehen in der Form " 3x "
                         Anzahl="1";
                         h=Text.toLowerCase().indexOf("x ");
                         if (h>=1)
                         {
                             k=h;
                             while (k>0 && h-k<10)
                             {
                                 if (Text.charAt(k)<=' ')
                                 {
                                     k++;
                                     break;
                                 }
                                 k--;
                             }

                             if (h>k && h-k<=3)
                             {
                                 Anzahl=Text.substring(k, h);
                                 
                                 try
                                 {
                                     Integer.parseInt(Anzahl);
                                 }
                                 catch (NumberFormatException x)
                                 {
                                     Anzahl="1";
                                 }
                             }
                         }
                         
                         result = new Ergebnis( "EwsCasToolsRepetition", "EwsCasTools "+EwsCasToolsVersion, "", "", "", "", "", "", "", Anzahl, "", "", "", Text, "WarningCode: "+Code, Ergebnis.FT_RETRY );
                         ergListe.add(result);
                     }
                     else
                     {
                         // Code ist bei Info ein textueller Bezeichner
                         result = new Ergebnis( "EwsCasToolsInfo", "EwsCasTools "+EwsCasToolsVersion, Parameter1, "", "", Code, Text, "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
                         ergListe.add(result);
                     }
                 }
                 
                 pos=minpos+1;
             }
         }
      } 
      catch (PPExecutionException e) 
      {
         status = STATUS_EXECUTION_ERROR;
      } 
      catch (Exception e) 
      {
         System.out.println(e.getMessage());
         e.printStackTrace(); 
         result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
         ergListe.add(result);
         status = STATUS_EXECUTION_ERROR;
      } 
      catch (Throwable e) 
      {
         System.out.println(e.getMessage());
         e.printStackTrace();
         result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
         ergListe.add(result);
         status = STATUS_EXECUTION_ERROR;
      }
      
      setPPStatus( info, status, ergListe );
   }

   /** Ediabasergebnisse f�r EwsCasTools_Cascade.jar */
   private class ECT_EdiabasResult
   {
       /** Name of the used SGBD */
       private String m_SGBD;
       /** Name des ausgef�hren Jobs */
       private String m_JobName="";
       /** Job-Status, z.b: "OKAY" */
       private String m_Status="";
       /** Anzahl an Result Sets */
       private int m_NrOfSets;
       /** Ergebnisse */
       private HashMap<String, Object> m_Results=new HashMap<String, Object>();
   }
   
   /** Cascade Dialog Objekt*/
   public UserDialog m_myDialog = null;
   /** Semaphore f�r Zugriff auf m_myDialog */
   public Object     m_myDialogSema=new Object();
   /** 0: Kein verz�gertes Repaint aktiv 
    *  1: Ein verz�gertes Repaint aktiv
    *  2: W�hrend auf ein verz�gertes Repaint gewartet wurde, wurde mind. ein zweites angefordert.
    *     -> Nachdem das erste fertig ist, noch ein weieteres absetzen.
    */
   public int        m_RepaintRunning=0;
   /** Semaphore f�r Zugriff auf m_RepaintRunning */
   public Object     m_RepaintRunningSema=new Object();

   /** Klasse zum Aufruf des EwsCasTools jar file (lib/ext/ewscastools_cascade.jar)
    *  Durch Verwendung dieser inneren Klasse mu� das jar-File nur am Pr�fstand vorhanden sein, nicht
    *  bei Verwendung des Frontends 
    */
   private class ExtCall
   {
       /** Liefert Version von EwsCasTools 
        * 
        * @return Version von EwsCasTools */
       private String getVersion()
       {
           return ewscastools.MainFrame.getVersion();
       }
       
       /** Ruft EwsCasTools auf
        * 
        * @param ewscastools_input_parameter_xml Inputparameter f�r Aufruf als XML-String
        * @return Result des Aufrufs als xml-String
        * @throws Exception Fehlerobjekt
        */
       private String doCall(StringBuffer ewscastools_input_parameter_xml) throws Exception
       {
           String erg;
           
           ewscastools.CascadeCall cc=new ewscastools.CascadeCall()
           {
               //  GUI Interface: ==================================================================
               
               
               /** Hinweistext (z.B. aktuelle Aktion) an der Cascadeoberfl�che darstellen,
                * mit Hilfe von WerkerMeldung.
                * 
                * @param txt  Hinweistext
                * @throws Exception Fehlerobjekt
                */
               public void showTextCascade(int WindowColor, String txt) throws Exception
               {
               String str;
               
                   if (txt==null)
                   {
                       releaseDialog();
                       return;
                   }
                   
                   if (txt.length()==0)
                   {
                       releaseDialog();
                       return;
                   }
                   
                   synchronized(m_myDialogSema)
                   {

                       if (m_myDialog == null) 
                       {
                           try
                           {
                               m_myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
                               m_myDialog.setAllowCancel(false);
                           }
                           catch (Exception e) 
                           {
                               System.out.println("ERROR in showText: getUserDialog "+e.getMessage());
                               e.printStackTrace(System.out);
                               
                               m_myDialog = null;
                               throw new Exception("getUserDialog failed ("+e.getMessage()+")");
                           }
                       }
                       
                       if (isDE()) str="Info";
                       else        str="Info";
                       if (WindowColor==1)
                           m_myDialog.displayStatusMessage( str, txt, -1 );
                       else if (WindowColor==2)
                           m_myDialog.displayAlertMessage( str, txt, -1) ;
                       else
                           m_myDialog.displayUserMessage( str, txt, -1);
                       
                       enlargeDialog();
                   } // synchronized
                   
               }
               
               /** Benutzereingabe
                * 
                * @param txt Text
                * @param MaxLength ignored unter Cascade 
                * @param Type ignored unter Cascade
                * @return Eingegebener String
                */
               public String promptForStringCascade(String txt, int MaxLength, int Type) throws Exception
               {
               String Input;
               
                   synchronized(m_myDialogSema)
                   {

                       releaseDialog();
                       try
                       {
                           m_myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
                       }
                       catch (Exception e) 
                       {
                           System.out.println("ERROR in promptForString: getUserDialog "+e.getMessage());
                           e.printStackTrace(System.out);
                          
                           m_myDialog = null;
                           throw new Exception("getUserDialog failed ("+e.getMessage()+")");
                       }
                       
                       Input=m_myDialog.requestUserInput(txt, "", 0);
    
                       if (Input==null) return null;
                       if (Input.length()==0) return null;
                       
                       releaseDialog();
                   } // synchronized

                   return Input;
                   
               }
               
               
               /** Benutzereingabe von Login und Passwort in Cascade 
                * 
                * @return Vector der 2 Strings enth�lt. 1. Element: Login, 2. Element: Passwort
                * Bei Abbruch null.
                * @throws Exception Fehlerobjekt
                */
               public Vector<String> promptForLoginAndPwCascade() throws Exception
               {
               Vector<String> erg;
               String Login, PW;
               String str;
               
                   synchronized(m_myDialogSema)
                   {

                       releaseDialog();
                       try
                       {
                           m_myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
                       }
                       catch (Exception e) 
                       {
                           System.out.println("ERROR in promptForLoginAndPw: getUserDialog "+e.getMessage());
                           e.printStackTrace(System.out);
                          
                           m_myDialog = null;
                           throw new Exception("getUserDialog failed ("+e.getMessage()+")");
                       }
                       
                       if (isDE()) str="Login eingeben";
                       else        str="Enter login";
                       Login=m_myDialog.requestUserInput(str, "", 0);
    
                       if (Login==null) return null;
                       if (Login.length()==0) return null;
                       
                       try
                       {
                           m_myDialog = getPr�flingLaufzeitUmgebung().getUserDialog(); 
                       }
                       catch (Exception e) 
                       {
                           System.out.println("ERROR in promptForLoginAndPw: getUserDialog "+e.getMessage());
                           e.printStackTrace(System.out);
                           m_myDialog = null;
                           throw new Exception("getUserDialog failed ("+e.getMessage()+")");
                       }
                       
                       if (isDE()) str="Passwort eingeben";
                       else        str="Enter password";
                       
                       PW=m_myDialog.requestUserInputPassword(str, "", 0);
                       releaseDialog();
                   } // synchronized

                   if (PW==null) return null;
                   if (PW.length()==0) return null;
                   
                   erg=new Vector<String>();
                   erg.add(Login);
                   erg.add(PW);
                   return erg;
               }
               
               /** WerkerQuittung in Cascade darstellen, mit einer beliebigen Kombination folgenden Buttons:
                * "Weiter", "Wiederholen", "Abbrechen". Je nach Buttonkombination wird
                * es auf eine WerkerDialog mit einem Button, einen Ja/Nein Dialog oder eine Auswahldialog
                * abgebildet.
                * 
                * @param txt Werkermeldung
                * @param showContinue Button "Weiter" vorhanden
                * @param showRetry Button "Wiederholen" vorhanden
                * @param showAbort Button "Abbrechen" vorhanden
                * @return Vom Benutzer gew�hlte Option: 1=Weiter, 2=Wiederholen, 3=Abbrechen
                * @throws Exception
                */
               public int userDecideCascade(int WindowColor, String txt, boolean showContinue, boolean showRetry, boolean showAbort) throws Exception
               {
               String str1, str2, str3;
               
                   if (isDE())
                   {
                       str1=TXT_CONTINUE_DE;
                       str2=TXT_RETRY_DE;
                       str3=TXT_ABORT_DE;
                   }
                   else
                   {
                       str1=TXT_CONTINUE_EN;
                       str2=TXT_RETRY_EN;
                       str3=TXT_ABORT_EN;
                   }
                   
                   return userDecideCascade(WindowColor, txt, showContinue ? str1 : null, 
                                          showRetry ? str2 : null, 
                                          showAbort ? str3 : null);
               }
               
               /** Erstellt einen WerkerDialog mit den Buttons Ja und Nein
                * 
                * @param txt Werkermeldung
                * @param ResYes Returnwert, falls Ja gedr�ckt wird
                * @param ResNo Returnwert, fall Nein gedr�ckt wird
                * @return Gedr�ckter Button, 'ResYes' oder 'ResNo'
                * @throws Exception Fehlerobjekt
                */
               private int userDecideYesNo(int WindowColor, String txt, String YesText, int ResYes, String NoText, int ResNo) throws Exception
               {
               String titel;
               int key;
               MyKeyEventDispatcher keyDispatcher;
               HashSet<Integer> mappedKeys;
               
                   synchronized(m_myDialogSema)
                   {

                       releaseDialog();
                       try
                       {
                           if (isDE()) titel="Frage";
                           else        titel="Question";
                           m_myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
                       }
                       catch (Exception e) 
                       {
                           System.out.println("ERROR in userDecideYesNo: getUserDialog "+e.getMessage());
                           e.printStackTrace(System.out);
                           m_myDialog = null;
                           throw new Exception("getUserDialog failed ("+e.getMessage()+")");
                       }
                       
                       enlargeDialog();
                   
                       try
                       {
                           if (WindowColor==1)
                               m_myDialog.requestUserInputDigital( PB.getString( titel ), txt, -1, UserDialog.STATUS_MESSAGE );
                           else if (WindowColor==2)
                               m_myDialog.requestUserInputDigital( PB.getString( titel ), txt, -1, UserDialog.ALERT_MESSAGE );
                           else
                               m_myDialog.requestUserInputDigital( PB.getString( titel ), txt, -1);
                       }
                       catch (Exception e) 
                       {
                           System.out.println("ERROR in userDecideYesNo: requestUserInputDigital "+e.getMessage());
                           e.printStackTrace(System.out);
                           m_myDialog = null;
                           throw new Exception("requestUserInputDigital failed ("+e.getMessage()+")");
                       }

                       mappedKeys=new HashSet<Integer>();
                       if (YesText.equalsIgnoreCase(TXT_RETRY_DE) || YesText.equalsIgnoreCase(TXT_RETRY_EN) ||
                           NoText.equalsIgnoreCase(TXT_RETRY_DE) || NoText.equalsIgnoreCase(TXT_RETRY_EN))
                       {
                           mappedKeys.add(new Integer(KeyEvent.VK_F6));
                       }
                   
                       if (YesText.equalsIgnoreCase(TXT_ABORT_DE) || YesText.equalsIgnoreCase(TXT_ABORT_EN) ||
                           NoText.equalsIgnoreCase(TXT_ABORT_DE) || NoText.equalsIgnoreCase(TXT_ABORT_EN))
                       {
                           mappedKeys.add(new Integer(KeyEvent.VK_F7));
                       }
                   
                       keyDispatcher=new MyKeyEventDispatcher(mappedKeys);
                       KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(keyDispatcher);
                   
                   
                       if (YesText!=null) m_myDialog.setYesButtonText(YesText);
                       if (NoText!=null)  m_myDialog.setNoButtonText(NoText);

                       enlargeDialog();
                   } // synchronized
                   
                   while (true)
                   {
                       // Am Pr�fstand sind Buttons f�r F6, F7 (Wiederholen, Abbrechen) herausgef�hrt.
                       // Diese auf YES NO Buttons mappen, wenn diese den Text "Wiederholen", "Abbrechen" haben
                       key=keyDispatcher.getLastKey();
                       if (key==KeyEvent.VK_F6)
                       {
                           if (YesText.equalsIgnoreCase(TXT_RETRY_DE) || YesText.equalsIgnoreCase(TXT_RETRY_EN))
                           {
                               key=UserDialog.YES_KEY;
                               break;
                           }
                           if (NoText.equalsIgnoreCase(TXT_RETRY_DE) || NoText.equalsIgnoreCase(TXT_RETRY_EN))
                           {
                               key=UserDialog.NO_KEY;
                               break;
                           }
                       }
                       if (key==KeyEvent.VK_F7)
                       {
                           if (YesText.equalsIgnoreCase(TXT_ABORT_DE) || YesText.equalsIgnoreCase(TXT_ABORT_EN))
                           {
                               key=UserDialog.YES_KEY;
                               break;
                           }
                           if (NoText.equalsIgnoreCase(TXT_ABORT_DE) || NoText.equalsIgnoreCase(TXT_ABORT_EN))
                           {
                               key=UserDialog.NO_KEY;
                               break;
                           }
                       }
                       
                       synchronized(m_myDialogSema)
                       {
                           key=m_myDialog.getLastKey();
                       } // synchronized
                       if (key==UserDialog.YES_KEY)  
                       {
                           break;
                       }
                       else if (key==UserDialog.NO_KEY) 
                       {
                           break;
                       }
                       try
                       {
                           Thread.sleep(50);
                       }
                       catch (InterruptedException ex) {}
                   }
                   
                   KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(keyDispatcher);
                   
                   releaseDialog();
                   
                   if (key==UserDialog.YES_KEY) return ResYes;
                   else return ResNo;
               }
               
               /** WerkerQuittung in Cascade darstellen, mit bis zu 3 Wahlm�glichkeiten.
                * Je nach Anzahl und Inhalt der Wahltm�glichkeiten wird
                * es auf eine WerkerDialog mit einem Button, einen Ja/Nein Dialog oder eine Auswahldialog
                * abgebildet.
                * 
                * @param txt Werkermeldung
                * @param choice1 Text f�r Option 1 oder null
                * @param choice2 Text f�r Option 2 oder null
                * @param choice3 Text f�r Option 3 oder null
                * @return Vom Benutzer gew�hlte Option 1-3
                * @throws Exception Fehlerobjekt
                */
               public int userDecideCascade(int WindowColor, String txt, String choice1, String choice2, String choice3) throws Exception
               {
               int Count;
               int FirstNr, SecondNr;
               String FirstTxt, SecondTxt;
               
                   if (choice1!=null)
                   {
                       if (choice1.equalsIgnoreCase("Ja") || choice1.equalsIgnoreCase("Yes") ||
                           choice1.equalsIgnoreCase("Nein") || choice1.equalsIgnoreCase("No")) choice1="&"+choice1;
                   }
                   if (choice2!=null)
                   {
                       if (choice2.equalsIgnoreCase("Ja") || choice2.equalsIgnoreCase("Yes") ||
                           choice2.equalsIgnoreCase("Nein") || choice2.equalsIgnoreCase("No")) choice2="&"+choice2;
                   }
                   if (choice3!=null)
                   {
                       if (choice3.equalsIgnoreCase("Ja") || choice3.equalsIgnoreCase("Yes") ||
                           choice3.equalsIgnoreCase("Nein") || choice3.equalsIgnoreCase("No")) choice3="&"+choice3;
                   }
               
                   Count=0;
                   FirstNr=-1;
                   SecondNr=-1;
                   FirstTxt="";
                   SecondTxt="";
                   if (choice1!=null)
                   {
                       Count++;
                       FirstNr=1;
                       FirstTxt=choice1;
                   }
                   if (choice2!=null)
                   {
                       if (Count==0) { FirstNr=2; FirstTxt=choice2; } 
                       else          { SecondNr=2; SecondTxt=choice2; }
                       Count++;
                   }
                   if (choice3!=null)
                   {
                       if (Count==0) { FirstNr=3; FirstTxt=choice3; }
                       else          { SecondNr=3; SecondTxt=choice3; }
                       Count++;
                   }
                   
                   if (Count==0) return 0;
                   if (Count==1) return showConfirmationDialog(WindowColor, txt, FirstTxt, FirstNr);
                   if (Count==2)
                   {
                       int erg=userDecideYesNo(WindowColor, txt, FirstTxt, FirstNr, SecondTxt, SecondNr);
                       return erg;
                   }
                   
                   return showSelectionDialog(WindowColor, txt, choice1, choice2, choice3);
               }

               /** Bei showTextAbortCascade Resultwert, falls entsprechender Button gedr�ckt */
               private int m_AbortResult=0;
               private int m_YesResult=0;
               private int m_NoResult=0;
               /** Bei showTextAbortCascade tats�chlicher gedr�ckter Button (bzw. dessen Resultwert, 0=kein Abortbutton gedr�ckt) */
               private int m_FinalResult=0;
               
               private String m_AbortTxt=null;
               private String m_AbortTitel=null;
               private int    m_AbortWindowColor=0;
               private String m_AbortButton1;
               private String m_AbortButton2;
               private MyKeyEventDispatcher m_KeyEventDispatcher=null;

               /** Text darstellen und bis zu 3 Abbruchoptionen anbieten. Der Aufruf kehrt sofort zur�ck.
                * Sinnvoll, wenn Ablauf eine l�ngere Aktion durchf�hrt, die der Benutzer abbrechen k�nnen soll.
                * Benutzer mu� aber nicht abbrechen.
                * Der Aufrufer kann mit didAbortCascade feststellen, ob zwischenzeitlich einer der Abbruchbuttons
                * gedr�ckt wurde. Der Abbruchdialog mu� in jedem Fall mit stopWaitingForAbortCascade() geschlossen werden. 
                * 
                * @param txt Werkermeldung.
                * @param choice1 Abbruchoption 1 oder null
                * @param choice2 Abbruchoption 2 oder null
                * @param choice3 Abbruchoption 3 oder null
                * @throws Exception Fehlerobjekt
                */
               public void showTextAbortCascade(int WindowColor, String txt, String choice1, String choice2, String choice3) throws Exception
               {
               int NrOfChoices;
               String titel;
               String choices[];
               int    mapChoiceResult[];
               HashSet<Integer> mappedKeys=new HashSet<Integer>();
               
                   if (m_KeyEventDispatcher!=null)
                   {
                       KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(m_KeyEventDispatcher);
                       m_KeyEventDispatcher=null;
                   }
               
                   releaseDialog();

                   if (choice1!=null)
                   {
                       if (choice1.equalsIgnoreCase("Ja") || choice1.equalsIgnoreCase("Yes") ||
                           choice1.equalsIgnoreCase("Nein") || choice1.equalsIgnoreCase("No")) choice1="&"+choice1;
                   }
                   if (choice2!=null)
                   {
                       if (choice2.equalsIgnoreCase("Ja") || choice2.equalsIgnoreCase("Yes") ||
                           choice2.equalsIgnoreCase("Nein") || choice2.equalsIgnoreCase("No")) choice2="&"+choice2;
                   }
                   if (choice3!=null)
                   {
                       if (choice3.equalsIgnoreCase("Ja") || choice3.equalsIgnoreCase("Yes") ||
                           choice3.equalsIgnoreCase("Nein") || choice3.equalsIgnoreCase("No")) choice3="&"+choice3;
                   }
                   
                   if (isDE()) titel="Info";
                   else        titel="Info";

                   m_AbortResult=0;
                   m_YesResult=0;
                   m_NoResult=0;
                   m_FinalResult=0;

                   choices=new String[3];
                   mapChoiceResult=new int[3];
                   NrOfChoices=0;
                   if (choice1!=null) { choices[NrOfChoices]=choice1; mapChoiceResult[NrOfChoices]=1; NrOfChoices++; }
                   if (choice2!=null) { choices[NrOfChoices]=choice2; mapChoiceResult[NrOfChoices]=2; NrOfChoices++; }
                   if (choice3!=null) { choices[NrOfChoices]=choice3; mapChoiceResult[NrOfChoices]=3; NrOfChoices++; }
                   
                   if (NrOfChoices==1)
                   {
                       synchronized(m_myDialogSema)
                       {
                           try
                           {
                               m_myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
                               m_myDialog.setAllowCancel(true);
                               m_AbortResult=mapChoiceResult[0];
                           }
                           catch (Exception e) 
                           {
                               System.out.println("ERROR in showTextAbort: getUserDialog "+e.getMessage());
                               e.printStackTrace(System.out);
                               m_myDialog = null;
                               throw new Exception("getUserDialog failed ("+e.getMessage()+")");
                           }
                           
                           m_AbortTxt=txt;
                           m_AbortWindowColor=WindowColor;
                           
                           if(WindowColor==2)
                             m_myDialog.displayAlertMessage(titel, txt, -1 );
                           else
                             m_myDialog.displayStatusMessage( titel, txt, -1 );
                           
                           m_myDialog.setCancelButtonText(choices[0]);
                           
                           if (choices[0].equalsIgnoreCase(TXT_ABORT_DE) || choices[0].equalsIgnoreCase(TXT_ABORT_DE)) mappedKeys.add(new Integer(KeyEvent.VK_F7));
                           
                           enlargeDialog();
                       } // synchronized
                   }
                   else
                   {
                       synchronized(m_myDialogSema)
                       {
                           try
                           {
                               m_myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
                               m_YesResult=mapChoiceResult[0];
                               m_NoResult=mapChoiceResult[1];
                           }
                           catch (Exception e) 
                           {
                               System.out.println("ERROR in showTextAbort: getUserDialog "+e.getMessage());
                               e.printStackTrace(System.out);
                               m_myDialog = null;
                               throw new Exception("getUserDialog failed ("+e.getMessage()+")");
                           }
                       } // synchronized
                       
                       m_AbortTxt=txt;
                       m_AbortTitel=titel;
                       m_AbortWindowColor=WindowColor;
                       m_AbortButton1=choices[0];
                       m_AbortButton2=choices[1];
                       
                       if (choices[0].equalsIgnoreCase(TXT_ABORT_DE) || choices[0].equalsIgnoreCase(TXT_ABORT_DE) ||
                           choices[1].equalsIgnoreCase(TXT_ABORT_DE) || choices[1].equalsIgnoreCase(TXT_ABORT_DE)) mappedKeys.add(new Integer(KeyEvent.VK_F7));
                       
                       if (choices[0].equalsIgnoreCase(TXT_RETRY_DE) || choices[0].equalsIgnoreCase(TXT_RETRY_DE) ||
                           choices[1].equalsIgnoreCase(TXT_RETRY_DE) || choices[1].equalsIgnoreCase(TXT_RETRY_DE)) mappedKeys.add(new Integer(KeyEvent.VK_F6));
                       
                       makeAbortDialogWith2Buttons();
                   }
                   
                   m_KeyEventDispatcher=new MyKeyEventDispatcher(mappedKeys);
                   KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(m_KeyEventDispatcher);
               }
               
               private void makeAbortDialogWith2Buttons()
               {
                   synchronized(m_myDialogSema)
                   {
                       if (m_AbortWindowColor==1)
                           m_myDialog.requestUserInputDigital( m_AbortTitel, m_AbortTxt, -1, UserDialog.STATUS_MESSAGE );
                       else if (m_AbortWindowColor==2)
                           m_myDialog.requestUserInputDigital( m_AbortTitel, m_AbortTxt, -1, UserDialog.ALERT_MESSAGE );
                       else
                           m_myDialog.requestUserInputDigital( m_AbortTitel, m_AbortTxt, -1);
                       
                       m_myDialog.setYesButtonText(m_AbortButton1);
                       m_myDialog.setNoButtonText(m_AbortButton2);
                       
                       enlargeDialog();
                   } // synchronized
               }

               /** Erneuert den Text einer Meldung die mit showTextAbortCascade erzeugt wurde.
                * Diese Funktion wird z.B. beim Schl�sselanlernen benutzt um den aktuellen Anlernstatus
                * anzuzeigen.
                * 
                * @param txt Neuer Text 
                * @throws Exception
                */
               public void updateTextAbortCascade(String txt) throws Exception
               {
               String str;
               
                   if (isDE()) str="Info";
                   else        str="Info";
                   
                   synchronized(m_myDialogSema)
                   {
                       if (m_myDialog==null) return;
                       if (txt==null) txt="";
                       if (txt.equals(m_AbortTxt)) return;
                       
                       m_AbortTxt=txt;
                       
                       if (m_AbortResult>0)
                       {
                           // Wir haben einen Dialog mit Cancel Button
                    	   
                    	   if(m_AbortWindowColor == 2)
                    		 m_myDialog.displayAlertMessage( str, txt, -1 );
                    	   else
                             m_myDialog.displayStatusMessage( str, txt, -1 );
                    	   
                           enlargeDialog();
                       }
                       else
                       {
                           // Wir haben einen Dialog mit Yes/No Button (angefordert �ber requestUserInputDigital)
                           // Hier funktioniert displayStatusMessage nicht, sonst verschwinden die Buttons
                           
                           makeAbortDialogWith2Buttons();
                       }
                   } // synchronized
               }

               
               /** Pr�fen, ob eine Option bei showTextAbortCascade gew�hlt wurde 
                * 
                * @return 0: Keine Benutzereingabe, 1-3: Entsprechende Benutzereingabe
                * @throws Exception Fehlerobjekt
                */
               public int didAbortCascade() throws Exception
               {
               
                   if (m_FinalResult!=0) return m_FinalResult;
                   
                   synchronized(m_myDialogSema)
                   {
                       if (m_myDialog!=null)
                       {
                           if (m_KeyEventDispatcher!=null)
                           {
                               int key;
                               key=m_KeyEventDispatcher.getLastKey();
                               if (key==KeyEvent.VK_F7 && m_AbortResult>0) m_FinalResult=m_AbortResult;
                               else if (key==KeyEvent.VK_F7 && m_YesResult>0 && (m_AbortButton1.equalsIgnoreCase(TXT_ABORT_DE) || m_AbortButton1.equalsIgnoreCase(TXT_ABORT_EN))) m_FinalResult=m_YesResult;
                               else if (key==KeyEvent.VK_F7 && m_NoResult>0 && (m_AbortButton2.equalsIgnoreCase(TXT_ABORT_DE) || m_AbortButton2.equalsIgnoreCase(TXT_ABORT_EN))) m_FinalResult=m_NoResult;
                               else if (key==KeyEvent.VK_F6 && m_YesResult>0 && (m_AbortButton1.equalsIgnoreCase(TXT_RETRY_DE) || m_AbortButton1.equalsIgnoreCase(TXT_RETRY_EN))) m_FinalResult=m_YesResult;
                               else if (key==KeyEvent.VK_F6 && m_NoResult>0 && (m_AbortButton2.equalsIgnoreCase(TXT_RETRY_DE) || m_AbortButton2.equalsIgnoreCase(TXT_RETRY_EN))) m_FinalResult=m_NoResult;
                           }
                           
                           if (m_FinalResult==0)
                           {
                               if (m_AbortResult>0)
                               {
                                   if (m_myDialog.isCancelled())
                                   {
                                       m_FinalResult=m_AbortResult;
                                   }
                               }
                               else if (m_YesResult>0)
                               {
                                   int key=m_myDialog.getLastKey();
                                   if (key==UserDialog.YES_KEY)  
                                   {
                                       m_FinalResult=m_YesResult;
                                   }
                                   else if (key==UserDialog.NO_KEY) 
                                   {
                                       m_FinalResult=m_NoResult;
                                   }
                               }
                           }
                       }
                   } // synchronized
                   
                   if (m_FinalResult>0)
                   {
                       if (m_KeyEventDispatcher!=null)
                       {
                           KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(m_KeyEventDispatcher);
                           m_KeyEventDispatcher=null;
                       }
                       releaseDialog();
                   }
                   
                   return m_FinalResult;
               }
               
               /** Beendet showTextAbortCascade. Der Ablauf ist mit seiner Aktion fertig. 
                * Diese Methode mu� in jedem Fall aufgerufen werden, egal, ob
                * die Aktion fertig ist, weil sie "wirklich" fertig ist, oder weil
                * sie abgebrochen wurde.
                */
               public void stopWaitingForAbortCascade() throws Exception
               {
                   if (m_KeyEventDispatcher!=null)
                   {
                       KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(m_KeyEventDispatcher);
                       m_KeyEventDispatcher=null;
                   }
                   releaseDialog();
               }
               
               /** Erstellt eine WerkerQuittung mit einem Button
                * 
                * @param txt Werkermeldung
                * @param ButtonTxt Text f�r Button
                * @param Result Returnwert wenn Button gedr�ckt wird
                * @return Wert der in 'Result' �bergeben wurde
                * @throws Exception Fehlerobjekt
                */
               private int showConfirmationDialog(int WindowColor, String txt, String ButtonTxt, int Result) throws Exception
               {
                   synchronized(m_myDialogSema)
                   {
                       releaseDialog();
                       try
                       {
                           m_myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
                       }
                       catch (Exception e) 
                       {
                           System.out.println("ERROR in showConfirmationDialog: getUserDialog "+e.getMessage());
                           e.printStackTrace(System.out);
                           m_myDialog = null;
                           throw new Exception("getUserDialog failed ("+e.getMessage()+")");
                       }
                       
                       enlargeDialog();
                       
                       m_myDialog.setOkButtonText(ButtonTxt);
                       
                       if (txt.length()<=10)
                       {
                           if (WindowColor==1)
                               m_myDialog.requestStatusMessage( txt, "", 0 );
                           else if (WindowColor==2)
                               m_myDialog.requestAlertMessage( txt, "", 0 );
                           else
                               m_myDialog.requestUserMessage( txt, "", 0 );
                       }
                       else
                       {
                           if (WindowColor==1)
                               m_myDialog.requestStatusMessage( null, txt, 0 );
                           else if (WindowColor==2)
                               m_myDialog.requestAlertMessage( null, txt, 0 );
                           else
                               m_myDialog.requestUserMessage( null, txt, 0 );
                       }
    
                       releaseDialog();
                   } // synchronized
                   
                   return Result;
               }
               
               /** WerkerDialog (ListBox) in Cascade darstellen, mit bis zu 3 Wahlm�glichkeiten:
                * 
                * @param txt Werkermeldung
                * @param sel1 Option 1 oder null
                * @param sel2 Option 1 oder null
                * @param sel3 Option 1 oder null
                * @return gew�hlte Option 1-3 
                * @throws Exception Fehlerobjekt
                */
               private int showSelectionDialog(int WindowColor, String txt, String sel1, String sel2, String sel3) throws Exception
               {
               int Count;
               String choices[];
               int map[];
               int Result;

                   Count=0;
                   if (sel1!=null) Count++;
                   if (sel2!=null) Count++;
                   if (sel3!=null) Count++;
                   
                   choices=new String[Count];
                   map=new int[Count];
                   Count=0;
                   if (sel1!=null) { choices[Count]=sel1; map[Count]=1; Count++; } 
                   if (sel2!=null) { choices[Count]=sel2; map[Count]=2; Count++; }
                   if (sel3!=null) { choices[Count]=sel3; map[Count]=3; Count++; }

                   Result=userSelectCascade(WindowColor, txt, choices);
                   return map[Result];
               }
               
               /** WerkerQuittung in Cascade darstellen, mit n Wahlm�glichkeiten:
                * 
                * @param txt Werkermeldung
                * @param choices Texte f�r Optionen
                * @return gew�hlte Option 0 - (choices.length-1)
                * @throws Exception Fehlerobjekt
                */
               public int userSelectCascade(int WindowColor, String txt, String[] choices) throws Exception
               {
               int Result[];
                   
                   synchronized(m_myDialogSema)
                   {
                       releaseDialog();
                       try
                       {
                           m_myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
                       }
                       catch (Exception e) 
                       {
                           System.out.println("ERROR in showSelectionDialog: getUserDialog "+e.getMessage());
                           e.printStackTrace(System.out);
                           m_myDialog = null;
                           throw new Exception("getUserDialog failed ("+e.getMessage()+")");
                       }
               
                       Result=m_myDialog.requestUserInputSingleChoice(null, txt, 0, choices);
                       
                       releaseDialog();
                   }
    
                   if (Result==null)
                   {
                       return 0;
                   }
                   else if (Result.length==0)
                   {
                       return 0;
                   }
                   
                   return Result[0];
               }
               
               /** Gibt jedweden WerkerDialog frei.
                * (Entfernt ihn vom Bildschirm */
               public void releaseDialog() throws Exception
               {
                   synchronized(m_myDialogSema)
                   {
                       if (m_myDialog != null) 
                       {
                           try 
                           {
                               getPr�flingLaufzeitUmgebung().releaseUserDialog();
                           } 
                           catch (Exception e) 
                           {
                               m_myDialog = null;
                               System.out.println("ERROR: releaseDialog");
                               e.printStackTrace(System.out);
                               throw new Exception("Release Dialog failed ("+e.getMessage()+")");
                           }
                           m_myDialog = null;
                       }
                   } // synchronized
               }
               
               /** Vergr��ert den CascacadeDialog.
                * 
                * Wird f�r alle Dialoge au�er Multiselect aufgerufen.
                * Bei EwsCas-Abl�ufen kann durchaus mehr Text anfallen, da ist
                * die Standard-Cascade-Box zu klein.
                */
               private void enlargeDialog()
               {
               RepaintDelay endiTh;
               int cols;
               int rows;
               
                   synchronized(m_myDialogSema)
                   {
                       if (m_myDialog == null) return;
    
                       Dimension screenSize;
                       screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    
                       /*
                       int width, height;
                       Rectangle r;
                       screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                       System.out.println("Screensize "+screenSize.width+" "+screenSize.height);
                       
                       width=(int)(screenSize.width*0.8);
                       height=(int)(screenSize.height*0.8);
                       
                       r=m_myDialog.getBounds();
                       if (r.width<width)   r.width=width;
                       if (r.height<height) r.height=height;
                       
                       r.x=(screenSize.width-width)/2;
                       if (r.x<0) r.x=0;
                       r.y=(screenSize.height-height)/2;
                       if (r.y<0) r.y=0;
                       */
                       
                       if (screenSize.height<=700)
                           cols=screenSize.height/26;
                       else if (screenSize.height<=800)
                           cols=screenSize.height/31; // Bei dieser Aufl�sung soll die FGNR links noch lesbar sein
                       else
                           cols=screenSize.height/28;
                       rows=screenSize.height/100;
                       
                       m_myDialog.setColumns(cols);
                       m_myDialog.setRows(rows);
                       
                       //debugPrint("## Enlarge Dlg");
                       
                       synchronized(m_RepaintRunningSema)
                       {
                           if (m_RepaintRunning==0)
                           {
                               //debugPrint("## Enlarge Dlg 0");
                               m_RepaintRunning=1;
                               endiTh=new RepaintDelay(100);
                               endiTh.start();
                           }
                           else if (m_RepaintRunning==1)
                           {
                               //debugPrint("## Enlarge Dlg 1");
                               m_RepaintRunning=2;
                           }
                       }
                       //debugPrint("## Enlarge Dlg done");
                   }
               } // synchronized
               
               // Ediabas Interface: ==================================================================

               
               private HashMap<Integer, EdiabasProxyThread> m_EdiabasInstances=new HashMap<Integer, EdiabasProxyThread>();
               private int    m_FreeHandle=0; // Bei jedem Aufruf der PP wird m_FreeHandle wieder auf 0 gesetzt.
                                              // Das ist wichtig, weil das erste ge�ffnete Interface (mit Handle==0)
                                              // anders behandelt wird, als alle anderen: Kein apiInitExt/apiEnd

               /** Wiederholungen, falls SG nicht antwortet */
               private int m_Retries;
               
               /** Ediabas wird initialisiert
                * Bei allen Instanzen, au�er der ersten, wird ein ApiInitExt gemacht.
                * Grund: DiagOpen macht nur ein ApiInit(Ext) auf das erste Device
                * Sauber w�re, wenn DiagOpen und DiagClose alle konfigurierten EdiabasDevices �ffnet. Da aber DiagOpen nicht angepasst wird, muss es diese PP erledigen 
                * 
                * 
                * @param InstanceNr Ediabasinstanz. Wichtig bei Multiinstanzbetrieb. Sonst immer 0
                *        Im Multiinstanzbetrieb z�hlt InstanceNr ab 0. Achtung: Ediabas z�hlt ab 1
                * @param Retries Wiederholversuche, wenn Ausf�hrung fehlschl�gt.
                * @param ForceTrace true: Ediabas Trace einschalten, auch wenn er in der Ediabas.ini nicht aktiviert ist.
                * @returns Handle, f�r Aufrufe von closeEdiabas, determineSGBD, executeJob, getxxxResult
                * @throws Exception Fehlerobjekt
                */
               public int initEdiabas(String InterfaceName, int InstanceNr,  boolean MultipleInterfaces, int Retries, boolean ForceTrace) throws Exception
               {
               EdiabasProxyThread ept;
               int erg;
               
                   m_Retries=Retries;
                   try
                   {
                       if (MultipleInterfaces)
                       {
                           ept = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas("IF_"+InterfaceName+"_"+(InstanceNr+1));
                       }
                       else
                       {
                           ept = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();
                       }
                   }
                   catch ( Exception e1 ) 
                   {
                       throw new Exception("error getting EDIABAS device: " + e1.getMessage());
                   }
                   
                   m_EdiabasInstances.put(new Integer(m_FreeHandle), ept);
                   erg=m_FreeHandle;
                   System.out.println("=== initEdiabas "+InterfaceName+(InstanceNr+1)+"="+erg);
                   
                   if (m_FreeHandle!=0)
                   { 
                       // Das erste Interface (Handle==0) wurde durch DiagOpen ge�ffnet
                       // Um alle weiteren Interfaces k�mmert sich das EwsCasTools selbst
                       // (sauber w�re, DiagOpen so zu erweitern, dass es alle Interfaces �ffnen kann)
                       ept.apiInitExt(InterfaceName, ""+(InstanceNr+1), "");
                   }
                   
                   m_FreeHandle++;
                   if (m_FreeHandle>=0x7FFFFFFF)
                   {
                       m_FreeHandle=1;
                       while (m_EdiabasInstances.containsKey(new Integer(m_FreeHandle))) m_FreeHandle++;
                   }
                   return erg;
               }

               /** Ediabas wird geschlossen
                * 
                * @param EdiabasHandle Ediabasinstanz. Hier gleiche Nummer verwenden, wie bei InitEdiabas.
                *        -1: Alle EdiabasInterfaces schlie�en
                * @throws Exception Fehlerobjekt
                */ 
               public void closeEdiabas(int EdiabasHandle) throws Exception
               {
               Iterator<Integer> it;
               Integer h;
               EdiabasProxyThread ept;
               
                   if (EdiabasHandle>0)
                   {
                       ept=(EdiabasProxyThread)m_EdiabasInstances.get(new Integer(EdiabasHandle));
                       if (ept!=null) ept.apiEnd();
                   }
                   else if (EdiabasHandle<0)
                   {
                       it=m_EdiabasInstances.keySet().iterator();
                       while (it.hasNext())
                       {
                           h=(Integer)it.next();
                           if (h.intValue()!=0)
                           {
                               // Das erste Interface (Handle==0) wird durch DiagOpen und DiagClose ge�ffnet und geschlosse
                               // Um alle weiteren Interfaces k�mmert sich das EwsCasTools selbst
                               // (sauber w�re, DiagOpen/DiagClose so zu erweitern, dass es alle Interfaces �ffnen/schlie�en kann)
                               ept=(EdiabasProxyThread)m_EdiabasInstances.get(h);                       
                               if (ept!=null) ept.apiEnd();
                           }
                       }
                   }
                   
                   if (EdiabasHandle<0) m_EdiabasInstances.clear();
                   else m_EdiabasInstances.put(new Integer(EdiabasHandle), null);
               }
               
               /** �ndert eine Einstellung in der Ediabas Konfiguration.
                * 
                * @param EdiabasHandle Ediabasinstanz
                * @param Name Name der zu �ndernden Einstellung 
                * @param Value Neuer Wert der Einstellung
                * @throws Exception Fehlerobjekt
                */
               public void apiSetConfig(int EdiabasHandle, String Name, String Value) throws Exception
               {
            	   EdiabasProxyThread ept=getEPT(EdiabasHandle);
            	   
            	   ept.apiSetConfig(Name, Value);
               }
               
               /** Holt den Wert einer Einstellung in Ediabas
                * 
                * @param EdiabasHandle Ediabasinstanz
                * @param Name Name der zu holenden Einstellung 
                * @return Wert der Einstellung (oder null, wenn nicht unterst�tzt)
                * @throws Exception Fehlerobjekt
                */
               public String apiGetConfig(int EdiabasHandle, String Name) throws Exception
               {
            	   EdiabasProxyThread ept=getEPT(EdiabasHandle);
            	   
            	   return ept.apiGetConfig(Name);       	   
               }

               /** Holt einen EdiabasProxyThread
                * 
                * @param EdiabasHandle EdiabasHandle (-1: Das erste/einzige Interface)
                * @return EdiabasProxyThread Objekt
                * @throws Exception Fehlerobjekt
                */
               private EdiabasProxyThread getEPT(int EdiabasHandle) throws Exception
               {
               EdiabasProxyThread ept;
               Iterator<Integer> it;

                   if (EdiabasHandle<0)
                   {
                       it=m_EdiabasInstances.keySet().iterator();
                       if (!it.hasNext())  throw new Exception("EdiabasIntance is not initialised");
                       ept=(EdiabasProxyThread)m_EdiabasInstances.get(it.next());
                   }
                   else
                   {
                       ept=(EdiabasProxyThread)m_EdiabasInstances.get(new Integer(EdiabasHandle));
                   }
                   
                   if (ept==null) throw new Exception("EdiabasIntance "+EdiabasHandle+" is not initialised");
                   
                   return ept;
               }

               /** SGBD �ber Gruppendatei ermitteln.
                * 
                * @param EdiabasHandle Ediabasinstanz. Hier gleiche Nummer verwenden, wie bei InitEdiabas.
                * @param GRP Name der Gruppendatei.
                * @return Name der ermittelten SGBD
                * @throws Exception Fehlerobjekt
                */
               public String determineSGBD(int EdiabasHandle, String GRP) throws Exception
               {
                   String erg;
                   EdiabasProxyThread ept=getEPT(EdiabasHandle);

                   ept.apiJob(GRP, "INITIALISIERUNG", "", "VARIANTE");

                   erg=ept.apiResultText("VARIANTE", 0, "");
                   if (erg==null) throw new Exception("Result VARIANTE is null");

                   return erg;
               }

               /** F�hrt einen Ediabas Job aus.
                * 
                * @param EdiabasHandle Ediabasinstanz. Hier gleiche Nummer verwenden, wie bei InitEdiabas.
                * @param SGBD Name der SGBD
                * @param JobName Name des EdiabasJobs
                * @param ReqParams Requestparameter mit ";" getrennt
                * @param RespParams Gew�nschte Responseparameter mit ";" getrennt
                * @return Objekt, welches alle Ergebnisse des EdiabasJobs enth�lt. Der Aufrufer kann mit diesem Objekt
                * nicht direkt etwas anfangen, sondern nur in andere Interfacefunktionen wie getJobStatus, getStringResult
                * hineinstecken.
                *  
                * @throws Exception Fehlerobjekt
                */
               public Object executeJob(int EdiabasHandle, String SGBD, String JobName, String ReqParams, String RespParams) throws Exception
               {
                   int i, j;
                   HashSet<String> WantedResults;
                   String[] ResultArray;
                   int numResultsInSet;
                   String resultName;
                   String keyname;
                   int resultType;
                   int counter;
                   
                   EdiabasProxyThread ept=getEPT(EdiabasHandle);
                   
                   ECT_EdiabasResult res=new ECT_EdiabasResult();
                   if (RespParams==null) RespParams="";
                   
                   if (RespParams.length()==0)
                   {
                       WantedResults=null;
                   }
                   else
                   {
                       WantedResults=new HashSet<String>();
                       ResultArray=RespParams.split(";");
                       for (i=0; i<ResultArray.length; i++)
                       {
                           WantedResults.add(ResultArray[i]);
                       }
                   }

                   res.m_SGBD=SGBD;
                   res.m_JobName=JobName;
                   counter=0;
                   while (true)
                   {
                       try
                       {
                           res.m_Status = ept.executeDiagJob(SGBD, JobName, ReqParams, RespParams);
                           break;
                       }
                       catch (ApiCallFailedException ex)
                       {
                           String err=ept.apiErrorText();
                           if (!(err.startsWith("IFH-0009") || err.startsWith("IFH-0010")) || counter>=m_Retries) throw new Exception (err);
                       }
                       try
                       {
                           Thread.sleep(400);
                       }
                       catch (InterruptedException e1) {}
                       counter++;
                   }
                   
                   res.m_NrOfSets=ept.apiResultSets();
                   
                   for (i = 1; i <= res.m_NrOfSets; i++)
                   {
                       numResultsInSet = ept.apiResultNumber(i);
                       
                       for (j = 1; j <= numResultsInSet; j++)
                       {
                           resultName = ept.apiResultName(j, i);

                           if (WantedResults==null) {}
                           else if (WantedResults.contains(resultName)) {}
                           else continue;

                           resultType = ept.apiResultFormat(resultName, i);
                           keyname=resultName+"@@"+i;
                           switch (resultType)
                           {
                           case EdiabasProxyThread.APIFORMAT_CHAR:
                               res.m_Results.put(keyname, new Integer(ept.apiResultChar(resultName, i))); 
                               break;
                           case EdiabasProxyThread.APIFORMAT_BYTE:
                               res.m_Results.put(keyname, new Integer(ept.apiResultByte(resultName, i)));
                               break;
                           case EdiabasProxyThread.APIFORMAT_INTEGER:
                               res.m_Results.put(keyname, new Integer(ept.apiResultInt(resultName, i)));
                               break;
                           case EdiabasProxyThread.APIFORMAT_LONG:
                               res.m_Results.put(keyname, new Integer(ept.apiResultLong(resultName, i)));
                               break;
                           case EdiabasProxyThread.APIFORMAT_WORD:
                               res.m_Results.put(keyname, new Integer(ept.apiResultWord(resultName, i)));
                               break;
                           case EdiabasProxyThread.APIFORMAT_DWORD:
                               res.m_Results.put(keyname, new Long(ept.apiResultDWord(resultName, i)));
                               break;
                           case EdiabasProxyThread.APIFORMAT_TEXT:
                               res.m_Results.put(keyname, ept.apiResultText(resultName, i, ""));
                               break;
                           case EdiabasProxyThread.APIFORMAT_BINARY:
                               short[] sTemp = ept.apiResultBinary(resultName, i);
                               byte[]  bTemp=new byte[sTemp.length];
                               for (int k = 0; k < sTemp.length; k++)
                               {
                                   bTemp[k] = (byte)sTemp[k];
                               }
                               res.m_Results.put(keyname, bTemp);
                               break;
                           case EdiabasProxyThread.APIFORMAT_REAL:
                               res.m_Results.put(keyname, new Double(ept.apiResultReal(resultName, i)));
                               break;
                           default:
                               throw new Exception("Unknown API_FORMAT");
                           }
                       }
                   }

                   return res;
               }

               /** Ermittelt nach Ausf�hrung eines EdiabasJobs mit executeJob die SGBD, die aufgerufen wurde
                * 
                * @param Result Ergebnis von executeJob
                * @return SGBD
                */
               public String getSGBDFromResult(Object Result)
               {
                   return ((ECT_EdiabasResult)Result).m_SGBD;
               }
               
               /** Ermittelt nach Ausf�hrung eines EdiabasJobs mit executeJob den Job Status, etwa "OKAY".
                * 
                * @param Result Ergebnis von executeJob
                * @return JobStatus, z.B. "OKAY"
                */
               public String getJobStatus(Object Result)
               {
                   return ((ECT_EdiabasResult)Result).m_Status;
               }

               /** Ermittelt den Name eines Jobs aus dem Result von executeJob. Es wird der String geliefert, der bei
                * executeJob in 'JobName' angegeben wurde.
                * 
                * @param Result Ergebnis von executeJob
                * @return Name des Jobs
                */
               public String getJobName(Object Result)
               {
                   return ((ECT_EdiabasResult)Result).m_JobName;
               }

               /** Extrahiert ein Stringresult aus dem Result von executeJob
                * 
                * @param Result Ergebnis von executeJob
                * @param ResponseParam Name des Stringresults
                * @return Inhalt des Results
                * @throws Exception Fehlerobjekt
                */
               public String getStringResult(Object Result, String ResponseParam) throws Exception
               {
                   return getStringResult(Result, ResponseParam, 1);
               }

               /** Extrahiert ein Intresult aus dem Result von executeJob
                * 
                * @param Result Ergebnis von executeJob
                * @param ResponseParam Name des Intresults
                * @return Inhalt des Results
                * @throws Exception Fehlerobjekt
                */
               public int getIntResult(Object Result, String ResponseParam) throws Exception
               {
                   ECT_EdiabasResult res=(ECT_EdiabasResult)Result;
                   Object erg;
                   
                   erg=res.m_Results.get(ResponseParam+"@@1");
                   
                   if (erg==null) throw new Exception("Result "+ResponseParam+" not found");
                   
                   if (erg instanceof Integer)
                   {
                       return ((Integer)erg).intValue();
                   }
                   else if (erg instanceof Long)
                   {
                       return (int)(((Long)erg).longValue());
                   }
                       
                   throw new Exception("Result "+ResponseParam+" is not integer");
               }

               /** Extrahiert ein Byterrayresult aus dem Result von executeJob
                * 
                * @param Result Ergebnis von executeJob
                * @param ResponseParam Name des Byterrayresults
                * @return Inhalt des Results
                * @throws Exception Fehlerobjekt
                */
               public byte[] getByteArrayResult(Object Result, String ResponseParam) throws Exception
               {
                   ECT_EdiabasResult res=(ECT_EdiabasResult)Result;
                   Object erg;
                   
                   erg=res.m_Results.get(ResponseParam+"@@1");
                   
                   if (erg==null) throw new Exception("Result "+ResponseParam+" not found");

                   if (!(erg instanceof byte[])) throw new Exception("Result "+ResponseParam+" is not byte array");
               
                   return (byte[])erg;
               }

               public int getNrOfResultSets(Object Result)
               {
                   ECT_EdiabasResult res=(ECT_EdiabasResult)Result;
                   return res.m_NrOfSets;
               }

               public String getStringResult(Object Result, String ResponseParam, int set) throws Exception
               {
                   ECT_EdiabasResult res=(ECT_EdiabasResult)Result;
                   Object erg;
                   
                   erg=res.m_Results.get(ResponseParam+"@@"+set);
                   
                   if (erg==null) throw new Exception("Result "+ResponseParam+" not found");
                   
                   if (erg instanceof byte[])
                   {
                       int i;
                       String b, str="";
                       byte[] ergArray=(byte[])erg;
                       for (i=0; i<ergArray.length; i++)
                       {
                           if (i>0) str+=" ";
                           b=Integer.toHexString((ergArray[i]+256)%256);
                           while (b.length()<2) b="0"+b;
                           str+=b;
                       }
                       return str;
                   }
                   
                   if (erg instanceof Integer)
                   {
                       int i;
                       i=((Integer)erg).intValue();
                       return Integer.toString(i);
                   }
                       
                   if (erg instanceof Long)
                   {
                       long i;
                       i=((Long)erg).longValue();
                       return Long.toString(i);
                   }

                   if (erg instanceof Double)
                   {
                       double i;
                       i=((Double)erg).doubleValue();
                       return Double.toString(i);
                   }

                   if (!(erg instanceof String)) throw new Exception("Result "+ResponseParam+" is not string");
                       
                   return (String)erg;
               }
               
               
               // DigitalIO Interface: ==================================================================
               
               
               /** Initialisierung
                */
               public void initDigitalIO() 
               { 
                   //Nothing to do: We get and release the RelayControl at every access  
               }
               

               /** Freigabe
                * 
                */
               public void uninitDigitalIO() throws Exception
               {
                   //Nothing to do: We get and release the RelayControl at every access  
               }    
               
               public boolean[] read(int Addr, int[] inputs) throws Exception
               {
               RelayControl rct;
               boolean[] erg;
               
                   rct = getPr�flingLaufzeitUmgebung().getDeviceManager().getRelayControl();
                   try
                   {
                       erg=rct.read(inputs);
                   }
                   catch (Exception err)
                   {
                       err.printStackTrace();
                       getPr�flingLaufzeitUmgebung().getDeviceManager().releaseRelayControl();
                       throw err;
                   }
                   
                   getPr�flingLaufzeitUmgebung().getDeviceManager().releaseRelayControl();
                   
                   return erg;
               }

               public double readAnalog(int Addr, int Channel) throws Exception
               {
                   System.out.println("CallEwsCasTools: Cascade Device does not support Analog Input");
                   return 0;
               }

               public void switchRelayOnOff(boolean On, int Addr, int[] RelaysArray) throws Exception
               {
               RelayControl rct;
               
                   rct = getPr�flingLaufzeitUmgebung().getDeviceManager().getRelayControl();
                   try
                   {
                       if (On) 
                           rct.setOn( RelaysArray );
                       else
                           rct.setOff( RelaysArray );
                   }
                   catch (Exception err)
                   {
                       err.printStackTrace();
                       getPr�flingLaufzeitUmgebung().getDeviceManager().releaseRelayControl();
                       throw err;
                   }
                   
                   getPr�flingLaufzeitUmgebung().getDeviceManager().releaseRelayControl();
               }

               
               // CascadeServerTransfer Interface: ==================================================================
               
               /** Holt Datensatz vom CASCADE Server, Pr�ftsand oder Distribution Verzeichnis
                * 
                * @param Source "CASCADE_SERVER" (vom Server) oder "CASCADE_DISTRIBUTION" (lokale Distribution)
                *               "CASCADE_STATION" (vom lokalen Verzeichnis database\pruefstand\dom)
                * @param Suffix Endebuchstabe f�r Datensatz, z.B. "C" bei CAS-Datensatz, "E" bei EWS4-Sync-Datensatz
                * @return Eingelesener Datensatz 
                * @throws Exception Fehler beim Laden
                */
               public StringBuffer readDatasetFromCascade(String Source, String Suffix) throws Exception
               {
                   if (Source.equals("CASCADE_STATION")) return readFromCascade_Pruefstand(Suffix);
                   else if (Source.equals("CASCADE_SERVER")) return readFromCascadeServer_Server(Suffix);
                   else if (Source.equals("CASCADE_DISTRIBUTION")) return readFromCascadeServer_Distribution(Suffix);
                   throw new Exception("Unknown dataset source \""+Source+"\"");
               }

               /** Holt Datensatz vom lokalen Pr�fstandsverzeichnis database\pruefstand\dom
                * 
                * @param Suffix Endebuchstabe f�r Datensatz, z.B. "C" bei CAS-Datensatz, "E" bei EWS4-Sync-Datensatz
                * @return Eingelesener Datensatz 
                * @throws Exception
                */
               private StringBuffer readFromCascade_Pruefstand(String Suffix) throws Exception
               {
               StringBuffer buffer;
               String str;
               
                   String pathName=DomPruefstand.PRUEFSTAND_DIRECTORY+File.separator;
                   buffer=readFromCascade_File(pathName, Suffix);
                   str=buffer.toString();
                   if (!str.startsWith("Error:")) return buffer;
                   throw new Exception(str);
               }
               
               /** Holt Datensatz vom CASCADE Server
                * 
                * @param Suffix Endebuchstabe f�r Datensatz, z.B. "C" bei CAS-Datensatz, "E" bei EWS4-Sync-Datensatz
                * @return Eingelesener Datensatz
                * @throws Exception Fehler beim Laden
                */
               private StringBuffer readFromCascadeServer_Server(String Suffix) throws Exception
               {
                   byte rawData[];

                   try 
                   {
                       //call the rmi-method
                       rawData= DomPruefstand.readDomData(getPr�fling().getAuftrag().getFahrgestellnummer7(), Suffix);
                       if ((rawData != null) && (rawData.length > 1)) 
                       {
                           String strData = new String(rawData);
                           if ((strData != null) && (strData.length() > 1)) 
                           {
                               return new StringBuffer(strData.trim());
                           }
                       }
                   } 
                   catch(Exception e) 
                   {
                       throw e;
                   }
                   throw new Exception("Dataset is empty");
               }
               
               /** Holt Datensatz aus dem Distributionsverzeichniss
                * 
                * @param Suffix Endebuchstabe f�r Datensatz, z.B. "C" bei CAS-Datensatz, "E" bei EWS4-Sync-Datensatz
                * @return Eingelesener Datensatz
                * @throws Exception Fehler beim Laden
                */
               private StringBuffer readFromCascadeServer_Distribution(String Suffix) throws Exception 
               {
               int pos;
               int i;
               StringBuffer buffer;
               String str;
               String final_errtxt;
               String localPaths[]=new String[4];
                   
                   //try to read local copy in distribution
                   
                   localPaths[2] = com.bmw.cascade.distribution.DistributionConstants.PRUEFSTAND_DIRECTORY+File.separator+com.bmw.cascade.distribution.DistributionConstants.XML_CAS_FILE;
                   // Eventuellen Slash am Ende entfernen
                   if (localPaths[2].endsWith(File.separator)) localPaths[2]=localPaths[2].substring(0, localPaths[2].length()-1);
                   
                   localPaths[0] = localPaths[2];
                   // letzte Pfadebene abschneiden...
                   pos=localPaths[0].lastIndexOf(File.separator);
                   //...und stattdessen casAPDMXml anh�ngen
                   if (pos>=0) localPaths[0]=localPaths[0].substring(0, pos+1)+"casAPDMXml";

                   
                   localPaths[1]=localPaths[0]+File.separator+getPr�fling().getAuftrag().getFahrgestellnummer7();
                   localPaths[3]=localPaths[2]+File.separator+getPr�fling().getAuftrag().getFahrgestellnummer7();

                   
                   final_errtxt="";
                   for (i=0; i<localPaths.length; i++)
                   {
                       buffer=readFromCascade_File(localPaths[i], Suffix);
                       str=buffer.toString();
                       if (!str.startsWith("Error:")) return buffer;
                       str="Distrubition:"+str.substring(6);
                       
                       if (final_errtxt.length()==0) final_errtxt=str;
                       else final_errtxt+="\r\n"+str;
                   }
                   throw new Exception(final_errtxt);
               }
               
               /** Liest Datensatz aus definiertem Verzeichnis <DatasetPath>
                * 
                * @param DatasetPath Verzeichnis mit Datensatz
                * @param Suffix Endebuchstabe f�r Datensatz, z.B. "C" bei CAS-Datensatz, "E" bei EWS4-Sync-Datensatz
                * @return eingelesener Datensatz
                */
               protected StringBuffer readFromCascade_File(String DatasetPath, String Suffix)
               {
               File localDir;
               File localFile;
               FileReader in = null;
               StringBuffer buffer=new StringBuffer();
                   
                   localDir = new File(DatasetPath);
                   if (localDir.exists()) 
                   {
                       //distribution directory exists
                       localFile = new File(localDir, getPr�fling().getAuftrag().getFahrgestellnummer7()+Suffix+".xml");
                       if (localFile.exists() && localFile.canRead()) 
                       {
                           //datasetfile exists so try to read it
                           in=null;
                           try
                           {
                               in = new FileReader(localFile);
                               int size = (int)localFile.length();
                               if (size<=1)
                               {
                                   throw new Exception("Dataset is empty");
                               }
                               char[] cbuff = new char[size];
                               in.read(cbuff);
                               buffer.append(cbuff);
                               return buffer;
                           }
                           catch (Exception err)
                           {
                               buffer.append("Error: ");
                               buffer.append(err.getMessage());
                               return buffer;
                           }
                           finally
                           {
                               if (in!=null)
                               {
                                   try
                                   {
                                        in.close();
                                   }
                                   catch (IOException e) { }
                               }
                           }
                       }
                       else
                       {
                           buffer.append("Error: Dataset \""+localFile.getAbsolutePath()+"\" does not exist or is not readable");
                           return buffer;
                       }
                   }
                   else
                   {
                       buffer.append("Error: Path \""+DatasetPath+"\" does not exist");
                       return buffer;
                   }
               }
               
               /** Schreibt Datensatz auf den Cascade-Server (am Ende des PUs bei IO)
                * 
                * @param Target Future use
                * @param Suffix ndebuchstabe f�r Datensatz, z.B. "C" bei CAS-Datensatz, "E" bei EWS4-Sync-Datensatz
                * @param Data Datensatz
                * @throws Exception Fehler beim Schreiben
                */
               public void writeDatasetToCascade(String Target, String Suffix, StringBuffer Data) throws Exception
               {
                   // Target should always be "SERVER", maybe different Targets in future
                   try 
                   {
                       DomPruefstand.writeDomData(getPr�fling().getAuftrag().getFahrgestellnummer7(), Suffix, Data.toString().getBytes());
                   } 
                   catch(Exception e) 
                   {
                       throw e;
                   }
               }
               
               // Serial Interface: ==================================================================

               private SerialGeneric m_mySerialDevice=null;
               
               
               
               /** Schittstelle �ffnen
                * 
                * @param r_iBaudRate        Baudrate
                * @param r_iFlowControlIn   Flowcontrol
                * @param r_iFlowControlOut  Flowcontrol
                * @param r_iDataBits        Anzahl Datenbits (7 oder 8)
                * @param r_iStopBits        Anzahl Stopbits (0, 1, oder 2)
                * @param r_iParity          Parity (Even, Odd, None)
                * @throws Exception         Fehler
                */
               public void open(int r_iBaudRate, int r_iFlowControlIn, int r_iFlowControlOut, int r_iDataBits, int r_iStopBits, int r_iParity) throws Exception
               {
               DeviceManager devMan;
               
                   devMan=getPr�flingLaufzeitUmgebung().getDeviceManager();
                   
                   try 
                   {
                       m_mySerialDevice = devMan.getSerialGeneric();
                   }
                   catch (Exception e)
                   {
                       try
                       {
                           devMan.releaseSerialGeneric();
                       }
                       catch( Exception ex ) {} catch(Throwable th) {}
                       m_mySerialDevice = null;
                       throw e;
                   }
                   
                   // LOP 2015_088: PP SerialGeneric.open() deprecated
                   // Laut Peter Rettig wird die deprecated Methode nicht entfernt und kann weiter verwendet werden.
                   // Die Umrechnung der beiden FlowControl Werte in den neuen Wert in SerialGeneric ist etwas dubios.
                   // -> Alles bleibt beim Alten.
                   m_mySerialDevice.open(r_iBaudRate, r_iFlowControlIn, r_iFlowControlOut, r_iDataBits, r_iStopBits, r_iParity);
               }
               
               /** Schnittstelle schlie�en
                *  
                * @throws Exception Fehler
                */
               public void close() throws Exception
               {
               DeviceManager devMan;
               Exception ex1;
               ex1=null;
               
                   try
                   {
                       m_mySerialDevice.close();
                   }
                   catch( Exception ex ) 
                   {
                       ex1=ex;
                   }
                   catch(Throwable th) {}
               
                   devMan=getPr�flingLaufzeitUmgebung().getDeviceManager();
                   try
                   {
                       devMan.releaseSerialGeneric();
                   }
                   catch( Exception ex ) 
                   {
                       if (ex1==null) ex1=ex;
                   }
                   catch(Throwable th) {}
               
                   m_mySerialDevice=null;
                   
                   if (ex1!=null) throw ex1;
               }

               /** Daten von serieller Schnittstelle empfangen lesen 
                * Wenn keine Daten bereit: Array der l�nge 0 
                * @return Empfangene Bytes
                * 
                * @throws Exception Fehler
                */
               public byte[] readData() throws Exception
               {
               byte[] erg;
               
                   //System.out.println("ser read...");
                   erg=m_mySerialDevice.readData();
                   //System.out.println("..."+ba2s(erg));
                   return erg;
               }

               /** Daten auf serieller Schnittstelle schreiben
                * 
                * @param buffer zu sendende Bytes
                * @throws Exception Fehler
                */
               public void writeData(byte[] buffer) throws Exception
               {
                   //System.out.println("ser write "+ba2s(buffer)+"...");
                   m_mySerialDevice.writeData(buffer);
                   //System.out.println("...written");
               }
               
               /** Puffer der seriellen Schnittstelle leeren
                * 
                * @throws Exception Fehler
                */
               public void cancel() throws Exception
               {
                   m_mySerialDevice.cancel();
               }
               
               /*
               private String ba2s(byte[] ba)
               {
                   String erg="[";
                   if (ba==null) return "[null]";
                   for (int i=0; i<ba.length; i++)
                   {
                       if (i>0) erg+=",";
                       erg+="0x";
                       erg+=Integer.toHexString((ba[i]+256)%256);
                   }
                   return erg+"]";
               }
               */
               
           };
           
           
           // Der eigentliche aufruf des EwsCasTools: ==================================================================
           
           try
           {
               erg=cc.executeProcess(ewscastools_input_parameter_xml.toString());
               cc.releaseDialog();
               return erg;
           }
           catch (Exception ex)
           {
               ex.printStackTrace(System.out);
               cc.releaseDialog();
               throw ex;
           }
       }
   }
   
   /** Falls ein Parameter in @ angegebn ist, so versucht diese Funktion den Wert aufzul�sen,
    * indem sie nach einem Pr�flingsattribut oder einer Pr�fstandsvariable diese Names sucht und den Inhalt zur�ckgibt.
    * Ist 'value' nicht in @, so wird der Wert unver�ndert zur�ckgegeben.
    * Existiert sowohl ein Pr�flingsattribut als auch eine Pr�fstandsvariable so wird das Pr�flingsattribut genommen.
    * 
    * @param value auzul�sender Parameterwert
    * @return aufgel�ster Parameterwert
    * @throws PPExecutionException Fehler
    */
   private String resolve(String value) throws PPExecutionException
   {
   Pruefling pl;
   String    erg;
   
       erg=value;
       if (value.startsWith("@"))
       {
           value=value.substring(1);
           if (value.endsWith("@")) value=value.substring(0, value.length()-1);
       
           if (m_Storage != null && m_Storage.length()>0) 
           {
               pl = getPr�fling().getAuftrag().getPr�fling(m_Storage);   //get the reference to the storing PL
           } 
           else 
           {
               pl = getPr�fling();   //get the reference to the storing PL (use current PL)
           }
           
           if (pl == null) 
           {
               System.out.println("CallEwsCasTools.getValue: Storage-PL not found");
               System.out.println("Storage is: "+m_Storage);
               //throw an exeception if storing PL is null
               if (isDE()) throw new PPExecutionException("CallEwsCasTools.getValue: Storage-PL nicht vorhanden");
               else throw new PPExecutionException("CallEwsCasTools.getValue: Storage-PL not found");
           }

           try 
           {
               //get the hashtable of the pp where the arguments are stored
               @SuppressWarnings("rawtypes")
               Map ht = pl.getAllAttributes();
               if (ht == null) 
               {
                   //thow an exception if ht is null
                   if (isDE()) throw new PPExecutionException("CallEwsCasTools.getValue: Hashtable nicht vorhanden");
                   else throw new PPExecutionException("CallEwsCasTools.getValue: Hashtable not found");
               }
               Object temp = ht.get(value);    //get the value assigned to label (in the hashtable)
               if (temp == null) temp="";  
               else if (!(temp instanceof String)) temp=""; 
               erg=(String)temp;
               
               if (erg.length()==0)
               {
                   try 
                   {
                       Object pr_var;
    
                       pr_var=getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PS_CONFIG, value);
                       if (pr_var!=null) 
                       {
                           if (pr_var instanceof String) 
                           {
                               erg=(String)pr_var;
                           }
                       }
                   }
                   catch (VariablesException e) { }
               }
           } 
           catch (PPExecutionException ppe) 
           {
               throw ppe;
           } 
           catch (Exception e) 
           {
               //throw an exception if something unknown/unexpected went wrong
               if (isDE()) throw new PPExecutionException("CallEwsCasTools.getValue: Unerwarteter Fehler");
               else throw new PPExecutionException("CallEwsCasTools.getValue: Unknown Exception");
           }
       }
       return erg;
   }
   
   /** F�gt dem xml Aufruf String f�r das EwsCasTool einen Int-Parameter hinzu,
    * der als Inputparameter dieser PP �bergeben wird.
    * 
    * @param xml xml Aufruf String
    * @param ItemName Names des Parameters
    * @param ExceptionIfParameterMissing Exception generieren, falls der PP kein Inputparameter mit Namen 'ItemName' �bergeben wurde
    * @throws PPExecutionException Fehler
    */
   private void addIntItem(StringBuffer xml, String ItemName, boolean ExceptionIfParameterMissing) throws PPExecutionException
   {
   String value;
       value=getValue(ItemName, ExceptionIfParameterMissing);
       if (value==null) return;
       value=value.trim();
       if (value.length()==0) return;
       
       try
       {
           Integer.parseInt(value);
       }
       catch (NumberFormatException ex)
       {
           throw new PPExecutionException("CallEwsCasTools.addIntItem Argument "+ItemName+"="+value+" is not a number");
       }
       
       addItemToXml(xml, ItemName, "int", value);
   }
   
   /** F�gt dem xml Aufruf String f�r das EwsCasTool einen String-Parameter hinzu,
    * der als Inputparameter dieser PP �bergeben wird.
    * 
    * @param xml xml Aufruf String
    * @param ItemName Names des Parameters
    * @param ExceptionIfParameterMissing Exception generieren, falls der PP kein Inputparameter mit Namen 'ItemName' �bergeben wurde
    * @throws PPExecutionException Fehler
    */
   private void addStringItem(StringBuffer xml, String ItemName, boolean ExceptionIfParameterMissing) throws PPExecutionException
   {
   String value;
       value=getValue(ItemName, ExceptionIfParameterMissing);
       if (value==null) return;
       addItemToXml(xml, ItemName, "string", value);
   }
   
   /** F�gt dem xml Aufruf String f�r das EwsCasTool einen Bool-Parameter hinzu,
    * der als Inputparameter dieser PP �bergeben wird.
    * 
    * @param xml xml Aufruf String
    * @param ItemName Names des Parameters
    * @param ExceptionIfParameterMissing true: Exception generieren, falls der PP kein Inputparameter mit Namen 'ItemName' �bergeben wurde
    * @throws PPExecutionException Fehler
    */
   private void addBoolItem(StringBuffer xml, String ItemName, boolean ExceptionIfParameterMissing) throws PPExecutionException
   {
   String value;
       value=getValue(ItemName, ExceptionIfParameterMissing);
       if (value==null) return;
       value=value.trim();
       if (value.length()==0) return;

       if (!value.equalsIgnoreCase("TRUE") &&
           !value.equalsIgnoreCase("FALSE") &&
           !value.equalsIgnoreCase("0") &&
           !value.equalsIgnoreCase("1"))
       {
           throw new PPExecutionException("CallEwsCasTools.addIntItem Argument "+ItemName+"="+value+" is not TRUE or FALSE");
       }
       
       addItemToXml(xml, ItemName, "boolean", value);
   }


   /** Holt einen Inputparameter f�r diese PP und l�st ihn auf (Pr�flingsattribut, Pr�fstandsvariable) falls
    * er in @ angegebn ist
    * 
    * @param ValueName Parametername
    * @param ExceptionIfParameterMissing true: Exception generieren, falls der PP kein Inputparameter mit Namen 'ValueName' �bergeben wurde 
    * @return Inhalt des Parameters (ggf. aufgel�st)
    * @throws PPExecutionException Fehler
    */
   private String getValue(String ValueName, boolean ExceptionIfParameterMissing) throws PPExecutionException
   {
       String value;

       value=getArg(ValueName.toUpperCase());
       if (value==null)
       {
           if (ExceptionIfParameterMissing)
               throw new PPExecutionException("CallEwsCasTools.getValue: Argument "+ValueName+" is missing");
           else 
               return null;
       }
   
       return resolve(value);
   }

   /** F�gt dem xml Aufruf String f�r das EwsCasTool einen Parameter hinzu,
    * der als Inputparameter dieser PP �bergeben wird.
    * 
    * @param xml  xml Aufruf String
    * @param ItemName Parametername
    * @param Type Datentyp (int, string, boolean)
    * @param value Parameterwert (als String)
    */
   private void addItemToXml(StringBuffer xml, String ItemName, String Type, String value)
   {
       xml.append("\t<"+ItemName+" type=\""+Type+"\">"+value+"</"+ItemName+">\r\n");
   }

   /** Extrahiert XML-attribute der Form    
    * att1="wert1"
    * und packt sie in eine Map
    * 
    * @param attributes Attribut-String
    * @return Attribute als Map
    */
   private Map<String, String> getAttributes(String attributes)
   {
   HashMap<String, String> erg=new HashMap<String, String>();
   String str=attributes.trim();
   int pos;
   String name;
   String value;
   
       while (str.length()>0)
       {
           pos=str.indexOf("=");
           if (pos<0) break;
           
           name=str.substring(0, pos);
           name=name.trim();
           
           str=str.substring(pos+1);
           pos=str.indexOf("\"");
           if (pos<0) break;
           str=str.substring(pos+1);
           pos=str.indexOf("\"");
           if (pos<0) break;
           
           value=str.substring(0, pos);
           erg.put(name, value);
           
           str=str.substring(pos+1);
           str=str.trim();
       }
              
       return erg;
   }
   
   /** Ermittelt, ob die Sprache in Cascade deutsch oder englisch ist
    * 
    * @return true: Deutsch, false: nicht Deutsch (-> Englisch)
    */
   private static boolean isDE() 
   {
       try 
       {
           if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true ) return true;
           else return false;
       }
       catch (Exception e) 
       {
           return false;   //default is english
       }
   }
   
   /** Key Listener f�r Werkerdialoge, um Pr�fstandskn�pfe (insb. F6, F7) zu mappen */
   protected class MyKeyEventDispatcher implements KeyEventDispatcher
   {
       /** The Last key entered, that is in set m_ExpectedKeys */
       private int m_LastKey;
       /** Set of Keycodes to listen for */
       private Set<Integer> m_ExpectedKeys;
       /** Semaphor to sync m_LastKey */
       private Object m_Semaphor=new Object();
       
       /** Constructor 
        * 
        * @param ExpectedKey Set of Keycodes to listen for 
        */
       public MyKeyEventDispatcher(Set<Integer> ExpectedKey)
       {
           synchronized(m_Semaphor)
           {
               m_ExpectedKeys=ExpectedKey;
               m_LastKey=0;
           }
       }
       
       public boolean dispatchKeyEvent(KeyEvent e)
       {
       int keycode;
       
           if (e.getID()!=KeyEvent.KEY_PRESSED) return false;
       
           keycode=e.getKeyCode();
           if (m_ExpectedKeys.contains(new Integer(keycode)))
           {
               synchronized(m_Semaphor)
               {
                   m_LastKey=keycode;
               }
               return true;
           }
           return false;
       }
       
       /** The last key pressed an is element of m_ExpectedKeys
        * 0 if no key pressed yet
        * 
        * @return last key pressed
        */
       public int getLastKey()
       {
           synchronized(m_Semaphor)
           {
               return m_LastKey;
           }
       }
   }
   
   /** Wartethread, um Cascadefenster verz�gert neu zu zeichnen (Workaround um Cascade repaint bug  */
   protected class RepaintDelay extends Thread
   {
       /** Wartezeit bis Repaint */
       private int m_Time;
       
       /** Konstruktor
        * 
        * @param Time Wartezeit bis Repaint
        */
       public RepaintDelay(int Time)
       {
           m_Time=Time;
       }
       
       public final void run()
       {
           //debugPrint("$$ Start RepaintDelay");
           while (true)
           {

               try
               {
                   Thread.sleep(m_Time);
               }
               catch (InterruptedException e) { }
    
               //debugPrint("$$ After wait");
               
               synchronized(m_myDialogSema)
               {
                   if (m_myDialog!=null)
                   {
                       try
                       {
                           EventQueue.invokeAndWait(new RepaintThread());
                       }
                       catch (InterruptedException e)
                       {
                       }
                       catch (InvocationTargetException e)
                       {
                       }
                   }
               }

               //debugPrint("$$ After repaint invoke");
               
               synchronized(m_RepaintRunningSema)
               {
                   //debugPrint("$$ RepRunning"+m_RepaintRunning);
                   
                   if (m_RepaintRunning<=1)
                   {
                       //debugPrint("$$ End thread");
                       m_RepaintRunning=0;
                       return;
                   }
                   m_RepaintRunning--;
                   //debugPrint("$$ Restart loop");
               }
           }
       }
   }
   
   /** Thread f�r invokeAndWait Ablauf zum Repaint des Fensters */
   protected class RepaintThread extends Thread
   {
       /** Vergr��ert m_Dialog */
       public void doRepaint()
       {
           //debugPrint("** Repaint B");
           
           if (m_myDialog==null) return; // D�rfte nie auftreten
           
           //debugPrint("** Repaint C");
          m_myDialog.invalidate();
          m_myDialog.validate();
          m_myDialog.repaint();

       }
       
       public final void run()
       {
           doRepaint();
       }
   }
   
   /** Debugausgaben 
    * 
    * @param txt DebugText
    */
   /*
   synchronized private void debugPrint(String txt)
   {
       System.out.println(txt);
   }
   */
}




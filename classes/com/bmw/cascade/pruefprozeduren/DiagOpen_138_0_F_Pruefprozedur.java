/*
 * DiagOpen_129_0_F_Pruefprozedur.java Created on 12.11.14
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.bmw.appframework.logging.LogLevel;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.Pruefstand;
import com.bmw.cascade.pruefstand.devices.DeviceInfo;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.b2v.*;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
//import com.bmw.cascade.pruefstand.ediabas.Ediabas;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
//import com.bmw.cascade.util.logging.CascadeLogManager;
import com.bmw.cascade.util.logging.CascadeLogging;

/**
 * Holt sich EDIABAS und baut in Abh�ngigkeit vom verwendeten Interface die Diagnoseanbindung auf.
 * 
 * !!! ACHTUNG: VERWENDUNG VON ICOM oder B2V bedingt EDIABAS 7.2 oder h�her und
 * CASCADE 2.2.9 oder h�her!!! -> Grund: Ediabas Methode "apiInitExtExt" wurde
 * in Ediabas DLL und in CASCADE Ediabas Klasse eingebaut
 * 
 * @author Winkler, M�ller, Crichton, Gelfert, Haase, Eghbaly, Pflugbeil,
 *         Schumann, Drexel, Rothenberger, Rettig
 * @version <br>
 *          30.07.2004 NC 17_0_T Benutzermeldungen ggfs. mit n.i.O. Timeout von Autostart <br>
 *          30.07.2004 NC 18_0_F Freigabe 0_1_7_TA <br>
 *          30.07.2004 NC 19_0_F Zur�ckgesetzt implements Werk 2.4 <br>
 *          30.07.2004 NC 20_0_T Timeoutfehlertext (Autostart verbessert), Defaultantworten umgekehrt <br>
 *          22.09.2004 NC 21_0_T Timeoutzeit wird nicht mehr von Autostart �bernommen, Stattdessen Opt. Parameter "TIMEOUT" <br>
 *          23.09.2004 NC 22_0_T Schreibfehler im 0_2_1_TA korrigiert <br>
 *          29.09.2004 NC 23_0_T "TIMEOUT" korrigiert <br>
 *          11.10.2004 NC 24_0_F Freigabe 0_2_3_TA <br>
 *          14.04.2005 RG 25_0_F Fuer die eingegebene MDA-Nummer wird ein Ergebnis angelegt. <br>
 *          14.04.2005 MM 26_0_F ErgebnisID f�r �nderung 2.5 hinzugef�gt <br>
 *          18.05.2005 DH 28_0_F Neue optionale Parameter "AUTOMODE" und "DIAGMODE" hinzu (MDA K-Line D-CAN Einf�hrung ab L3 / L4). <br>
 *          21.02.2006 DH 29_0_F Neukompelierung <br>
 *          24.04.2006 ME 30_0_T Erweiterung f�r ECOS; Name des Pr�fschritts wird in Transientendatei (Test-Step-Label) geschrieben, erst ab Cascade Version 2.1.0 oder h�her einsetzbar!
 *          15.06.2006 DH 31_0_F Generierung aus Version 30_0_T ohne �nderungen <br>
 *          22.06.2006 DH 32_0_T Opt. Parameter fuer die Uebergabe der MDA Slave Nummer mit der Moeglichkeit den Referenzparameter @ zu verwenden. Neue Zugriffsmethode auf EDIABAS eingf�gt.<br>
 *          31.07.2006 DH 33_0_F Generierung aus Version 32_0_T ohen �nderungen <br>
 *          ...09.2006 TP 34_0_T Verbesserung ECOS-Anpassung f�r Transientendatei <br>
 *          16.10.2006 DH 35_0_T EDIABAS Instanz (neuer Weg) wieder freigeben <br>
 *          19.10.2006 DH 36_0_T R�ckbau EDIABAS (neuer Weg) zur�ck zum alten Weg <br>
 *          26.10.2006 DH 37_0_F Generierung aus Version 36_0_T ohne �nderung <br>
 *          23.10.2006 DH 38_0_F �nderungen der Automatikparameter (AUTOMODE = 1, DIAGMODE = 1)<br>
 *          18.09.2007 RG 39_0_T Anpassungen f�r optionalen Abbruch der PP bei Falscheingabe MDA-Nummer<br>
 *          21.10.2007 FM 40_0_T Opt. Parameter "FENSTERNAME" und "TIMEOUT2" implementiert<br>
 *          10.01.2008 DH 41_0_F ReleaseVersion von Version 40_0_F<br>
 *          04.11.2008 MB 42_0_T Erweiterung Pr�fstandsvariabele DIAG_PARA = false - Notwendig f�r Paralleldiagnose
 *          27.11.2008 CS 43_0_T Erweiterung, sodass das Trace-Level bei neuerer IFH/API-Trace-Konfiguration gelesen werden kann.
 *          21.01.2009 CS 44_0_F Release-Version von Version 43_0_T
 *          21.01.2009 TB 45_0_F Einbau Reaktion auf (neues Device) B2V und ENET (Aufruf apiInitExt(...))
 *          27.02.2009 TB 46_0_T Einbau holen einer IP Adresse aus einem File beim ICOM Interface <BR>
 *          10.03.2009 DH 47_0_T Verheiratung prototypisch ohne Fehlerh�ndling ICOM eingebaut
 *          19.03.2009 MB 48_0_T Merge B2V-�nderung von Thomas. Schreiben der IP-Adresse f�r ICOM
 *          14.04.2009 JB 50_0_F Neuer Parameter "TIMEOUT3" gibt die Anzahl der START_PRUEFUNGS-Versuche an (5 Sekunden Intervalle) wichtig f�r automatisierte Linienfunkpr�fst�nde in Regensburg
 *          05.05.2009 DH 50_0_F ICOM-Interface Treiber von REMOTE auf RPLUS umgestellt. (EDIABAS 7.2 ist zwingend notwendig!!!)
 *          25.05.2009 DH 51_0_T Analog 50_F nur T-Version
 *          26.05.2009 DH 52_0_F Analog 50_F nur F-Version, nach Abschluss Test W2.4
 *          08.06.2009 DH 53_0_T neuen apiInitExt-Aufruf f�r ICOM P eingebaut
 *          15.06.2009 DH 54_0_T Ablauf Autoverheiratung umgebaut
 *          15.06.2009 DH 54_0_T Ablauf Autoverheiratung
 *          15.06.2009 DH 55_0_F F Version von 54_0_T
 *          16.06.2009 DH 56_0_T Timeout3 f�r ICOM P verf�gbargemacht, Anpassung Ablauf Autoassign
 *          16.06.2009 DH 57_0_F Timeout3 f�r ICOM P verf�gbargemacht, Anpassung Ablauf Autoassign, Auslesen WLAN_IP aus ICOM P
 *          23.07.2009 DH 58_0_F Abbruchbutton f�r "STATUS ICOM P"-Meldung eingebaut
 *          24.08.2009 DH 59_0_F Stack-Trace f�r Debugschalter eingebaut
 *          18.09.2009 MB 61_0_T timeout fuer normales apiinit.
 *          24.09.2009 MB 62_0_T Fehlerbehandlung beim API-Init: es wird kein Fehlerresult mehr erzeugt!
 *          24.10.2009 DH 63_0_T CancelButton eingebaut
 *          25.10.2009 DH 64_0_T driverIcomP = "noIcomP" eingef�gt
 *          02.11.2009 DH 65_0_T Gleiche wie 64_0_T; Bugfix im Ablauf.
 *          07.11.2009 DH 66_0_F ReleaseVersion von 64_0_T und 65_0_T.
 *          16.11.2009 DH 67_0_T PU_NAME als Ergebnis ins virtuelle Fahrzeug eintragen
 *          07.12.2009 DH 68_0_F ReleaseVersion von 67_0_T
 *          08.12.2009 DH 69_0_T Bugfixes bei Ergebnisr�ckgabe an APDM durchgef�hrt
 *          09.12.2009 DH 70_0_T Bugfix: Fehlereintrag trotz IO behoben
 *          17.12.2009 DH 71_0_F ReleaseVersion von 70_0_T
 *          12.01.2010 DH 72_0_F hinzugef�gt  driverIcomP == null || autoAssignICOM == false als Bedingung f�r apiInit
 *          11.04.2010 DH 73_0_T BUGFIX in Write_Remotehost in EDIABAS.INI Block / UserDialog entladen durch reset()
 *          27.04.2010 DH 74_0_T IP-Adresseingabe �ber messageTimeout;
 *          03.05.2010 DH 75_0_T WriteRemoteHost auch f�r PROXY-Betrieb des ICOM P
 *          03.05.2010 DH 76_0_T Auf Anforderung aus M�nchen marriage (Direkt zur Eingabe IP-Adresse) auch f�r ICOM P verwendbargemacht
 *          04.05.2010 DH 77_0_T getRequiredArgs und getOptionalArgs nur noch �ber STATIC Variable
 *          05.05.2010 DH 78_0_T Durch PSdZ Problem mit EmbeddedEthernet sIcomEnetPort hinzugef�gt und im weiteren auch noch die Lesebarkeit der PP erh�ht.
 *          07.05.2010 DH 79_0_T Wiederholung bei IP-Adresseingabe abh�ngig von messageTimeout3, bei marriage = true, nach IP-Adresseingabe automatische Wiederholung bis messageTimeout3 abgelaufen.
 *          10.05.2010 DH 80_0_F ReleaseVersion von 79_0_F
 *          11.05.2010 DH 81_0_F Bugfix 80_0_F: L�schen IP trotz IO Verbindungsaufbau
 *          24.06.2010 DH 82_0_T Bugfix 80_0_F: <= bei Wiederholung API_INIT L�schen IP trotz IO Verbindungsaufbau
 *          28.06.2010 DH 82_0_T Schreiben IP in EDIBAS.INI/RPLUS.INI nur mehr mit zus�tzlichem String nach IP-Name --> Namens�ufl�sung funktioniert sonst nicht.
 *          29.07.2010 DH 83_0_F VerifyVin und iIcomErrTimeout einbebaut.
 *          01.09.2010 DH 84_0_T Testversion 83_0_F f�r die Werke, Einbau apiInitExt (nul) f�r neunladen RPLUS.INI bei neuer IP vor apiInit (P2P)
 *          01.09.2010 DH 85_0_T VerfiyVin nur m�glich wenn auch ICOM_IP_LOOKUP_PATH gesetzt, sonst ohne VerfiyVin
 *          16.09.2010 DH 86_0_F ReleaseVersion 85_0_T
 *          16.09.2010 DH 87_0_T Zwei Server f�r VerifyVin und Fehlerbehandlung angepa�t, ICOM P-Notstratgie neben IP auch ICOM P
 *          20.10.2010 DH 88_0_T BUGFIX: repeatCounter wurde bei NET-0009 nicht hochgez�hlt
 *          18.08.2011 RD 90_0_T IP-Adresse in RPLUS.INI jetzt korrekt, wenn ICOM mit Kabel-Adresse "192.168.68.40" betrieben wird
 *          19.08.2011 RD 91_0_F ReleaseVersion 90_0_T
 *          26.07.2011 TB 92_0_T Timeout Parameter und bisherigen PS Variablen Parameter durchg�ngig mit neuer PS Variablen Methodik "DIAGOPEN_" + Parametername belegt // Entscheidung Workshop W2.4 am 26.7 <BR> 
 *          15.09.2011 RD 94_0_T ICOM VerifyVin Fehlermeldung
 *          15.09.2011 RD 95_0_F ReleaseVersion 94_0_T
 *          24.10.2011 MR 96_0_T Diverse �nderungen 
 *                               - Robustheit: Autoassign: kein "apiInitExt" Aufrufe, wenn die IP Adresse noch nicht bekannt ist (Parameter 'ICOM_IP_LOOKUP_PATH' muss gesetzt sein)  
 *                               - Schleifendurchlauf von 10ms auf 100ms, 
 *                               - bei Submethode "String getIcomIPFromFiles( String strFilename, String strLookUpPath )" werden keine lokalen Pfade mehr durchsucht,
 *                               - bei Submethode "String getIcomIPFromFile( File f )" Verbesserung, dass close bei der Datei auf jeden Fall versucht wird 
 *                               - keine NullPointerException wenn keine IP-Adresse durch Anwender eingegeben wurde
 *          26.10.2011 MR 97_0_F Convenience Version speziell f�r Gehard Karl 
 *          30.11.2011 RD 98_0_T Abbruchm�glichkeit bei Fehleingabe mit ICOM, Methode isValidIP neu (Test auf IPv4 Format)
 *          05.12.2011 RD 99_0_F F-version
 *          17.01.2012 MR 100_0_T Bugfix leere ICOM Datei beim Zugriff
 *          30.01.2012 MR 101_0_F Freigabe 100_0_T f�r Werk DGF
 *          29.03.2012 MR 103_0_F Debugausgaben hinzugef�gt f�r Werk DGF
 *          29.03.2012 MR 104_0_T Testversion von 103_0_F
 *          02.08.2012 MR 105_0_T Testversion f�r Werk Muenchen
 *          					  - PP bleibt in der Schleife falls VIN_DIFFERENT oder NO_VIN
 *          06.08.2012 MR 106_0_T Testversion f�r DGF und RGB
 *          					  -Statusmeldung VIN_DIFFERENT bzw. NO_VIN wird angezeigt. Die Schleife l�uft im Hintergrund weiter. Der Dialog und damit die Schleife lassen sich abbrechen.
 *          10.08.2012 MR 107_0_F produktive Freigabe 106_0_T 
 *          29.08.2012 MR 108_0_T Einlesen aller relevanter ICOMP-Parameter �ber Pr�fstandsvariablen m�glich
 *          					  -Bugfix: Schleife wird richtig durchgef�hrt, wenn Timeout3 �ber PSV gesetzt
 *          30.08.2012 MR 109_0_T Ausgabe des Autoassign-Ediabas_Error Codes, wenn IP-Adress Eingabe leer oder abgeborchen.
 *          03.09.2012 MR 110_0_T Ausgabe des Autoassig um die Fehler NO_ICOMP_XMLs, NO_VIN /keine FGNR und DIFFERENT_VIN /unterschiedliche FGNR erweitert
 *          05.09.2012 MR 111_0_T Fehlerausgaben ver�nder zu DIFFERENT_VIN /unterschiedliche FGNR --> Please plug in ICOMP earlier! / Bitte ICOMP rechtzeitig anstecken! und NO_ICOMP_XMLs --> Please plug in ICOMP earlier! / Bitte ICOMP rechtzeitig anstecken!
 *          12.09.2012 MR 112_0_F produktive Freigabe 111_0_T 
 *          12.10.2012 MR 113_0_T kleine BugFixes im Screenlog und in der Fehlerausgabe:
 *                               - keine Ausgabe des ApiErrorCodes an APDM, wenn bei der IP-Eingabe null �bergeben wurde
 *                               - Ausgabe der �ber PS eingelsenen Variablen Im PS-Screenlog korrigiert
 *                               - Autoassign-Fehlerausgabe ohne Lookuppath korrigiert  
 *          25.10.2012 MR 114_0_F produktive Freigabe 113_0_T
 *          29.11.2012 MR 115_0_T Bugfix beim einlesen der XML: Es wird zun�chst geprueft ob der Server erreichbar und das share existent ist. Siehe private Methode 
 *          04.12.2012 MR 116_0_F produktive Freigabe 115_0_T	                                       			
 *          12.12.2012 PR 117_0_T Pr�fung, ob die am Test-Screen ausgew�hlte Interface-Konfiguration Einstellungen der Pr�fprozedurparameter/angezogener Pr�fstandvariablen 
 *                                �berlagert. Falls dem so ist, haben die Einstellungen des Test-Screens Vorrang. �ber den Test-Screen hinzugekommene Einstellungen (die 
 *                                weder in Pr�fprozedurparametern noch Pr�fstandvariablen gesetzt sind) finden ebenfalls Anwendung (reichern also die Interface-Konfiguration 
 *                                an). 
 *          06.03.2014 MR 118_0_F produktive Freigabe 117_0_T
 *          13.05.2014 MR 119_0_T isreachable durch private Methode is reachable2 ersetzt. Apijav32.dll aus APDM Fehlermeldung entfernt und durch allgemeine Formulierung ersetzt.  
 *          14.05.2014 MR 120_0_F produktive Freigabe 119_0_T   	
 *          26.06.2014 MR 121_0_T neues Ergebnis IP from XML wird im Autoassing NIO Fall erzeugt
 *          26.06.2014 MR 122_0_F produktive Freigabe 121_0_T  					                                    
 *          16.07.2014 CW 123_0_T Parametergewinnnung angepasst, sodass Parameter nur �ber CASCADE Standard-Methoden geholt werden 	
 *          29.07.2014 CW 124_0_F produktive Freigabe 123_0_T	
 *          02.10.2014 MR 125_0_T Parallelisierung des Einlesen des ICOM Shares<BR>	
 *          08.10.2014 MR 126_0_T Bugfix bei der Ausgabe des Pfades, aus dem die IP Adresse beim parallelem Zugriff ausgelesen wurde
 *          08.10.2014 MR 127_0_F produktive Freigabe 126_0_T
 *          12.11.2014 MR 128_0_T Bugfix in Methode getIcomIPFromFiles, es werden jetzt alle Exceptions abgefangen und das result wird vor der Verarbeitung auf null gepueft
 *          12.11.2014 MR 129_0_F produktive Freigabe 128_0_T
 *          07.02.2017 MKe 131_0_F selbe wie 129_0_F.
 *          07.02.2017 MKe 132_0_T deprecated Code ersetzt.
 *          02.05.2017 MKe 133_0_F F-Version.
 *          11.05.2017 MKe 135_0_T Bugfix apiInit durch apiInitAuto ersetzt.
 *          19.05.2017 MKe 136_0_T Bugfix Das Schreiben in die Ini files hat nicht mehr funktioniert.
 *          19.05.2017 MKe 137_0_T Das Setzen der Variable DIAG_PARALLEL wurde nach oben geschoben.
 *          07.06.2017 MKe 138_0_F F-Version.
 *          	                                    
 */
public class DiagOpen_138_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	/** <code>serialVersionUID</code> */
	static final long serialVersionUID = 1L;

	//Prefix DiagOpen PP
	static final String DIAGOPEN_ = "DIAGOPEN_";

	//required args
	//	static final String FZS = "FZS";
	//
	//	// allg. optional args
	//	static final String DEBUG = "DEBUG"; //DEBUG-Schalter
	//	static final String EXTERN = "EXTERN"; //
	//	static final String INIT = "INIT"; //Direkt zur Eingabe MDA-NR oder ICOM P IP-Adresse
	//	static final String "TIMEOUT" = ""TIMEOUT""; //Timeout f�r Eingabefenster
	//	static final String "TIMEOUT2" = "TIMEOUT2"; //Timeout f�r Fehlermeldungen
	//	static final String "TIMEOUT3" = "TIMEOUT3"; //Repeatcounter f�r Verbindungsaufbau zum Interface
	//
	//	// MDA optional args
	//	static final String AUTOMODE = "AUTOMODE"; //MDA: automatische Bestimmung ob DCAN/K-Line-Betrieb
	//	static final String DIAGMODE = "DIAGMODE"; //MDA: Einstellung des Kommunikationsweges DCAN/KLINE
	//	static final String MDAEXT = "MDAEXT"; //�bergabe MDA-Slave-Nummer aus externer Quelle z.B. durch @-Operator
	//	static final String STOP_ON_MARRIAGE_FAILURE = "STOP_ON_MARRIAGE_FAILURE"; //sollte die MDA-Verheiratung nicht klappen, wird bei true sofort abgebrochen
	//	static final String FENSTERNAME = "FENSTERNAME"; //Benennung Fenstername bei MDA-Warteschleife
	//
	//	// ICOM P optional args
	//	static final String ICOM_WRITE_REMOTEHOST = "ICOM_WRITE_REMOTEHOST"; //Schreiben der bestimmten IP in EDIABAS.INI oder RPLUS.INI
	//	static final String ICOM_IP_LOOKUP_PATH = "ICOM_IP_LOOKUP_PATH"; //Pfad f�r die VIN-XML zum Verify-VIN
	//	static final String ICOM_DRIVER = "ICOM_DRIVER"; //Treiber des ICOM P mit Section f�r RPLUS.INI
	//	static final String "ICOM_AUTO_ASSIGN" = ICOM_AUTO_ASSIGN; //Verstimmung IP aus ICOM P VIN-XML
	//	static final String "ICOM_VERIFY_VIN" = ""ICOM_VERIFY_VIN""; //Vergleich VIN aus Fahrzeugauftrag und VIN im ICOM
	//	static final String ICOM_ERR_TIMEOUT = "ICOM_ERR_TIMEOUT"; //Timeout f�r Fehler bei ICOM au�er NET-0009
	//	static final String ICOM_STATIC_IP_PART = "ICOM_STATIC_IP_PART"; // Statischer Teil der IP-Addresse/IP-Name zur Eingabe der ICOM P Nummer bei Notstrategie

	/** Flag; Sollen Debug-Infos geschrieben werden */
	boolean Debug = false;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagOpen_138_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling
	 *            Klasse des zugeh. Pr�flings
	 * @param pruefprozName
	 *            Name der Pr�fprozedur
	 * @param hasToBeExecuted
	 *            Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DiagOpen_138_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 * 
	 * @see com.bmw.cascade.pruefprozeduren.Pruefprozedur#getOptionalArgs()
	 */
	public String[] getOptionalArgs() {
		String[] args = { "EXTERN", "INIT", "TIMEOUT", "AUTOMODE", "DIAGMODE", "MDAEXT", "DEBUG", "STOP_ON_MARRIAGE_FAILURE", "FENSTERNAME", "TIMEOUT2", "TIMEOUT3", "ICOM_AUTO_ASSIGN", "ICOM_DRIVER", "ICOM_WRITE_REMOTEHOST", "ICOM_IP_LOOKUP_PATH", "ICOM_VERIFY_VIN", "ICOM_ERR_TIMEOUT" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 * 
	 * @see com.bmw.cascade.pruefprozeduren.Pruefprozedur#getRequiredArgs()
	 */
	public String[] getRequiredArgs() {
		String[] args = { "FZS" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 * 
	 * @see com.bmw.cascade.pruefprozeduren.Pruefprozedur#checkArgs()
	 */
	public boolean checkArgs() {

		try {
			return super.checkArgs();
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @see com.bmw.cascade.pruefprozeduren.Pruefprozedur#execute(com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo)
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;
		int key;
		String temp = null;
		String tempPSV = null;

		// beide Interfaces
		boolean extern = false;
		boolean marriage = false;
		int messageTimeout = 0;
		int messageTimeout2 = 0;
		int messageTimeout3 = 0;

		// MDA spezifische Variablen
		String strFensterName = "";
		String mdaNummer = null;
		String mdaExt = null; // initialisiere Hilfsvariable fuer den externen Parameter fuer MDA Nummer
		String fawt = "Bitte Zuendung einschalten und gegebenenfalls MDA adaptieren ! ! !";
		int automode = 1;
		int diagmode = 1;
		long timestamp, deltatime;
		boolean stopOnMarriageFailure = false; // sollte die Verheiratung nicht klappen, wird bei true sofort abgebrochen
		boolean leaveWhile = false; // Hilfsvariable f�r das Verlassen der do - while Schleife 
		boolean udInUse = false;
		boolean initOK, startOK;
		boolean autoInitTried = false; // Automatische Bestimmung ob P2P- oder Linienmodus
		boolean autoInitSuccessful = false;

		//ICOM P spezifische Variablen
		String sIcomDriver = null; //Treiber ICOM P EDIABAS (analog EDIABAS.INI)
		String sIcomIpLookupPath = null; //optinaler Suchpfad f�r Icom-Ip-Zuordnungsdatei
		String sIcomIpLookupPath2 = null; //optinaler Suchpfad f�r Icom-Ip-Zuordnungsdatei
		String sIcomEnetPort = null; //optionale Porteingabe f�r HSFZ-Verbindung mit ICOM P
		String sIcomIpFamily = null; // optionale Angabe eines statischen Teiles der IP-Adresse/IP-Name
		boolean bIcomAutoAssign = true; //Flag zum deaktivieren AutoAssign ICOM P in DiagOpen (Default: AutoAssign -> AKTIV)
		boolean bIcomWriteRemoteHost = false;
		boolean bIcomVerifyVin = false; //Flag zum vergleichen der VIN im Auto
		boolean bIcomXMLFound = false; //Flag welches kennzeichnet, ob die PP bereits ermittelt hat, dass keine ICOM.xml gefunden wurde (=> unn�tige Logausgaben vermeiden)
		int iIcomErrTimeout = 5000; // Pause zwischen den Wiederholversuchen [ms] bei// 166 -->	NET-0016: Host Error// 155 -->	NET-0005: No Support // 156 -->	NET-0006: Access Denied
		boolean novin = false; // wird auf true gesetzt wenn keine Vin im Fahrzeug

		// Device B2V, notwe. bei Fzg. TCP/IP-Anbindung zum Finden der dem Fahrzeug (i.d.R. dem ZGW) zugeordn. IP-Adr.
		B2V myB2VHandler = null;

		DeviceManager devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();
		EdiabasProxyThread Ediabas;

		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}
		
		try {
			//MBa f�r Paralleldiagnose:
			//Pr�fstand auf Parallelbetrieb=AUS schalten
			getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL", Boolean.FALSE );

		} catch( Exception e ) {
			CascadeLogging.getLogger().log( LogLevel.WARNING, "Ediabas Parallel Diagnostics was not disabled", e );
		}

		try {
			try {
				// Parameter holen
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG ON ?
				if( getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "DEBUGON" ) ) {
					Debug = true;
					if( Debug )
						System.out.println( "\n\n\n Aktuelle Pr�fprozedur: " + this.getClassName().substring( 32 ) );
					if( Debug )
						System.out.println( "DiagOpen PP Debug = ON" );
				}

				//Version 108_0_T_/ Einlesen von Debug �ber Pr�fstandsvariable

				else {
					try {
						try {
							tempPSV = overrideInterfaceParameter( "_ICOMP_DEBUG", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, "_ICOMP_DEBUG" ) );
						} catch( VariablesException f ) {
							tempPSV = overrideInterfaceParameter( DIAGOPEN_ + "ICOMP_DEBUG", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, DIAGOPEN_ + "ICOMP_DEBUG" ) );
						}

						if( tempPSV.equalsIgnoreCase( "DEBUGON" ) ) {
							Debug = true;
						}
						tempPSV = null;
						if( Debug ) {
							System.out.println( "\n\n\n Aktuelle Pr�fprozedur: " + this.getClassName().substring( 32 ) );
							System.out.println( "DiagOpen PP ICOM_P Parameter Debug via PS-Konfig = ON" );
						}
					} catch( Exception e ) {
					}
				}

				/***********************************************************************
				 * NACHFOLGENDEN CODE WIEDER L�SCHEN, WENN DER PR�FSTAND IN DER LAGE 
				 * IST, DIE �BERLAGERTEN EINSTELLUNGEN AUSZUGEBEN. SIEHE CASCADE-
				 * LOP-PUNKT 1583 (VEREINBARUNG VOM 12.12.2012 MIT MANUEL ROTHENBERGER 
				 * UND REINHOLD DREXEL). 
				 **********************************************************************/
				String cfgName = Pruefstand.instance.getInterfaceConfigurationSelection( Pruefstand.instance.getScreenNumber() ); //beziehe den Namen der ausgew�hlten Interface-Konfiguration
				if( cfgName != null ) {
					HashMap config = Pruefstand.instance.getInterfaceConfiguration( cfgName ); //beziehe die Parameter der Interface-Konfiguration
					if( config != null && Debug )
						System.out.println( "APPLIED INTERFACE CONFIGURATION SELECTION IS " + cfgName + " WITH " + config );
				}

				/***********************************************
				 * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
				 * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
				 ***********************************************/

				//Version 108_0_T_

				if( overrideInterfaceParameter( "EXTERN", getArg( "EXTERN" ) ) != null ) {
					if( overrideInterfaceParameter( "EXTERN", getArg( "EXTERN" ) ).equalsIgnoreCase( "TRUE" ) )
						extern = true;
					if( Debug )
						System.out.println( "DiagOpen PP EXTERN = " + extern );
				}
				if( overrideInterfaceParameter( "INIT", getArg( "INIT" ) ) != null ) {
					if( overrideInterfaceParameter( "INIT", getArg( "INIT" ) ).equalsIgnoreCase( "TRUE" ) )
						marriage = true;
					if( Debug )
						System.out.println( "DiagOpen PP INIT: " + marriage );
				}
				if( overrideInterfaceParameter( "TIMEOUT", getArg( "TIMEOUT" ) ) != null ) {
					messageTimeout = Integer.parseInt( overrideInterfaceParameter( "TIMEOUT", getArg( "TIMEOUT" ) ) );
					if( Debug )
						System.out.println( "DiagOpen PP TIMEOUT: " + messageTimeout );
				}
				if( overrideInterfaceParameter( "AUTOMODE", getArg( "AUTOMODE" ) ) != null ) {
					automode = Integer.parseInt( overrideInterfaceParameter( "AUTOMODE", getArg( "AUTOMODE" ) ) );
					if( Debug )
						System.out.println( "DiagOpen PP AUTOMODE: " + automode );
				}
				if( overrideInterfaceParameter( "DIAGMODE", getArg( "DIAGMODE" ) ) != null ) {
					diagmode = Integer.parseInt( overrideInterfaceParameter( "DIAGMODE", getArg( "DIAGMODE" ) ) );
					if( Debug )
						System.out.println( "DiagOpen PP DIAGMODE: " + diagmode );
				}
				if( overrideInterfaceParameter( "MDAEXT", getArg( "MDAEXT" ) ) != null ) {
					mdaExt = extractValues( overrideInterfaceParameter( "MDAEXT", getArg( "MDAEXT" ) ) )[0];
					if( Debug )
						System.out.println( "DiagOpen PP MDAEXT: " + mdaExt );
				}
				if( overrideInterfaceParameter( "STOP_ON_MARRIAGE_FAILURE", getArg( "STOP_ON_MARRIAGE_FAILURE" ) ) != null ) {
					if( overrideInterfaceParameter( "STOP_ON_MARRIAGE_FAILURE", getArg( "STOP_ON_MARRIAGE_FAILURE" ) ).equalsIgnoreCase( "TRUE" ) )
						stopOnMarriageFailure = true;
					if( Debug )
						System.out.println( "DiagOpen PP STOP_ON_MARRIAGE_FAILURE: " + stopOnMarriageFailure );
				}
				if( overrideInterfaceParameter( "FENSTERNAME", getArg( "FENSTERNAME" ) ) != null ) {
					//Neu: Fenster-Name als optionales Argument
					strFensterName = overrideInterfaceParameter( "FENSTERNAME", getArg( "FENSTERNAME" ) );
					if( Debug )
						System.out.println( "DiagOpen PP FENSTERNAME: " + strFensterName );
				}
				if( overrideInterfaceParameter( "TIMEOUT2", getArg( "TIMEOUT2" ) ) != null ) {
					//Neu: 2.Timeout-Parameter
					messageTimeout2 = Integer.parseInt( overrideInterfaceParameter( "TIMEOUT2", getArg( "TIMEOUT2" ) ) );
					if( Debug )
						System.out.println( "DiagOpen PP TIMEOUT2: " + messageTimeout2 );
				}
				if( overrideInterfaceParameter( "TIMEOUT3", getArg( "TIMEOUT3" ) ) != null ) {
					//Neu: 3.Timeout-Parameter
					messageTimeout3 = Integer.parseInt( overrideInterfaceParameter( "TIMEOUT3", getArg( "TIMEOUT3" ) ) );
					if( Debug )
						System.out.println( "DiagOpen PP TIMEOUT3: " + messageTimeout3 );
				}
				if( overrideInterfaceParameter( "ICOM_AUTO_ASSIGN", getArg( "ICOM_AUTO_ASSIGN" ) ) != null && overrideInterfaceParameter( "ICOM_AUTO_ASSIGN", getArg( "ICOM_AUTO_ASSIGN" ) ).equalsIgnoreCase( "FALSE" ) ) {
					bIcomAutoAssign = false;
					if( Debug )
						System.out.println( "DiagOpen PP ICOM_P Parameter ICOM_AUTO_ASSIGN = " + bIcomAutoAssign );
				}
				if( overrideInterfaceParameter( "ICOM_DRIVER", getArg( "ICOM_DRIVER" ) ) != null ) {
					sIcomDriver = overrideInterfaceParameter( "ICOM_DRIVER", getArg( "ICOM_DRIVER" ) );
					if( Debug )
						System.out.println( "DiagOpen PP ICOM_P Parameter ICOM_DRIVER = " + sIcomDriver );
				}
				if( overrideInterfaceParameter( "ICOM_WRITE_REMOTEHOST", getArg( "ICOM_WRITE_REMOTEHOST" ) ) != null ) {
					if( overrideInterfaceParameter( "ICOM_WRITE_REMOTEHOST", getArg( "ICOM_WRITE_REMOTEHOST" ) ).trim().equalsIgnoreCase( "TRUE" ) )
						bIcomWriteRemoteHost = true;
					if( Debug )
						System.out.println( "DiagOpen PP ICOM_P Parameter ICOM_WRITE_REMOTEHOST: " + bIcomWriteRemoteHost );
				}
				if( overrideInterfaceParameter( "ICOM_IP_LOOKUP_PATH", getArg( "ICOM_IP_LOOKUP_PATH" ) ) != null ) {

					String sTempIcomIpLookupPath = overrideInterfaceParameter( "ICOM_IP_LOOKUP_PATH", getArg( "ICOM_IP_LOOKUP_PATH" ) );

					int index;
					if( (index = sTempIcomIpLookupPath.indexOf( ";" )) != -1 ) {

						int strLength = sTempIcomIpLookupPath.length();
						if( Debug ) {
							System.out.println( "DiagOpen PP ICOM_P Parameter tempLookupPath = " + sTempIcomIpLookupPath );
						}
						sIcomIpLookupPath = sTempIcomIpLookupPath.substring( 0, index );
						sIcomIpLookupPath2 = sTempIcomIpLookupPath.substring( index + 1, strLength );

						if( Debug )
							System.out.println( "DiagOpen PP ICOM_P Parameter ICOM_IP_LOOKUP_PATH = " + sIcomIpLookupPath );
						if( Debug )
							System.out.println( "DiagOpen PP ICOM_P Parameter ICOM_IP_LOOKUP_PATH2 = " + sIcomIpLookupPath2 );
						// SIcomIpLookupPath auf beide Pafde zur�cksetzten f�r parallelen Zugriff auf die Pfade.
						sIcomIpLookupPath = sTempIcomIpLookupPath;

					} else {
						sIcomIpLookupPath = sTempIcomIpLookupPath;
						if( Debug )
							System.out.println( "DiagOpen PP ICOM_P Parameter ICOM_IP_LOOKUP_PATH = " + sIcomIpLookupPath );
					}

				}
				if( overrideInterfaceParameter( "ICOM_VERIFY_VIN", getArg( "ICOM_VERIFY_VIN" ) ) != null ) {
					if( overrideInterfaceParameter( "ICOM_VERIFY_VIN", getArg( "ICOM_VERIFY_VIN" ) ).equalsIgnoreCase( "TRUE" ) )
						bIcomVerifyVin = true;
					if( Debug )
						System.out.println( "DiagOpen PP ICOM_P Parameter ICOM_VERIFY_VIN = " + bIcomVerifyVin );
				}
				if( overrideInterfaceParameter( "ICOM_ERR_TIMEOUT", getArg( "ICOM_ERR_TIMEOUT" ) ) != null ) {
					iIcomErrTimeout = Integer.parseInt( overrideInterfaceParameter( "ICOM_ERR_TIMEOUT", getArg( "ICOM_ERR_TIMEOUT" ) ) );
					if( Debug )
						System.out.println( "DiagOpen PP ICOM_ERR_TIMEOUT: " + iIcomErrTimeout );
				}

			} catch( Exception e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//Ediabas holen
			Ediabas = devMan.getEdiabas();

			try {
				// PU-Name abgelegt in virtuellem Fahrzeug
				result = new Ergebnis( "PU_NAME", "", "", "", "", "PU_NAME", "" + getPr�flingLaufzeitUmgebung().getPr�fumfangName(), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );

				//INTERFACE Treiber �ber Pr�fstandsvariable
				// DH
				if( sIcomDriver == null ) {
					try {
						try {
							sIcomDriver = overrideInterfaceParameter( "_ICOMP_Driver", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, "_ICOMP_Driver" ) );
						} catch( VariablesException f ) {
							sIcomDriver = overrideInterfaceParameter( DIAGOPEN_ + "ICOMP_Driver", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, DIAGOPEN_ + "ICOMP_Driver" ) );
						}
						if( Debug )
							System.out.println( "DiagOpen PP ICOM_P Parameter ICOM_DRIVER via PS-Konfig = " + sIcomDriver );
					} catch( Exception e ) {
					}
				}

				//Autoassignmode f�r ICOMP
				// DH
				if( bIcomAutoAssign == true ) {
					try {
						String tempAA = null;
						try {
							tempAA = overrideInterfaceParameter( "_ICOMP_Autoassign", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, "_ICOMP_Autoassign" ) );
						} catch( VariablesException f ) {
							tempAA = overrideInterfaceParameter( DIAGOPEN_ + "ICOMP_Autoassign", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, DIAGOPEN_ + "ICOMP_Autoassign" ) );
						}

						if( tempAA.equalsIgnoreCase( "FALSE" ) ) {
							bIcomAutoAssign = false;
						}
						if( Debug )
							System.out.println( "DiagOpen PP ICOM_P Parameter ICOM_AUTO_ASSIGN via PS-Konfig = " + bIcomAutoAssign );
					} catch( Exception e ) {
					}
				}

				//EnetDiagPort Treiber �ber Pr�fstandsvariable
				// DH
				if( sIcomEnetPort == null ) {
					try {
						try {
							sIcomEnetPort = overrideInterfaceParameter( "_ICOMP_EnetPort", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, "_ICOMP_EnetPort" ) );
						} catch( VariablesException f ) {
							sIcomEnetPort = overrideInterfaceParameter( DIAGOPEN_ + "ICOMP_EnetPort", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, DIAGOPEN_ + "ICOMP_EnetPort" ) );
						}
						if( Debug )
							System.out.println( "DiagOpen PP ICOM_P Parameter sIcomEnetPort via PS-Konfig = " + sIcomEnetPort );
					} catch( Exception e ) {
					}
				}

				//statischer Teil der IP-Adresse �ber Pr�fstandsvariable zur M�glichkeit der Eingabe der ICOM Nr.: zum Verbindungsaufbau
				// DH
				if( sIcomIpFamily == null ) {
					try {
						try {
							sIcomIpFamily = overrideInterfaceParameter( "_ICOMP_IpFamily", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, "_ICOMP_IpFamily" ) );
						} catch( VariablesException f ) {
							sIcomIpFamily = overrideInterfaceParameter( DIAGOPEN_ + "ICOMP_IpFamily", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, DIAGOPEN_ + "ICOMP_IpFamily" ) );
						}
						if( Debug )
							System.out.println( "DiagOpen PP ICOM_P Parameter sIcomIpFamily via PS-Konfig = " + sIcomIpFamily );
					} catch( Exception e ) {
					}
				}

				//messageTimeout Pr�fstandvariable einlesen �ber _Timeout und DiagOpen_Timeout ab Version 108_T_
				if( overrideInterfaceParameter( "TIMEOUT", getArg( "TIMEOUT" ) ) == null ) {
					try {
						try {
							String str = overrideInterfaceParameter( "_TIMEOUT", null );
							if( str != null )
								messageTimeout = Integer.parseInt( str );
							else
								messageTimeout = ((Integer) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "_TIMEOUT" )).intValue();

						} catch( Exception e ) {
							String str = overrideInterfaceParameter( DIAGOPEN_ + "TIMEOUT", null );
							if( str != null )
								messageTimeout = Integer.parseInt( str );
							else
								messageTimeout = ((Integer) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, DIAGOPEN_ + "TIMEOUT" )).intValue();
						}
						if( Debug )
							System.out.println( "DiagOpen PP ICOM_P Parameter TIMEOUT via PS-Konfig = " + messageTimeout );
					} catch( Exception e ) {
					}
				}

				//messageTimeout2 Pr�fstandvariable einlesen �ber _Timeout2 und DiagOpen_Timeout2 ab Version 108_T_
				if( getArg( "TIMEOUT2" ) == null ) {
					try {
						try {
							String str = overrideInterfaceParameter( "_TIMEOUT2", null );
							if( str != null )
								messageTimeout2 = Integer.parseInt( str );
							else
								messageTimeout2 = ((Integer) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "_TIMEOUT2" )).intValue();

						} catch( Exception e ) {
							String str = overrideInterfaceParameter( DIAGOPEN_ + "TIMEOUT2", null );
							if( str != null )
								messageTimeout2 = Integer.parseInt( str );
							else
								messageTimeout2 = ((Integer) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, DIAGOPEN_ + "TIMEOUT2" )).intValue();
						}
						if( Debug )
							System.out.println( "DiagOpen PP ICOM_P Parameter TIMEOUT2 via PS-Konfig = " + messageTimeout2 );
					} catch( Exception e ) {
					}
				}

				//messageTimeout3 Pr�fstandvariable einlesen �ber _Timeout3 und DiagOpen_Timeout3 ab Version 108_T_
				if( getArg( "TIMEOUT3" ) == null ) {
					try {
						try {
							String str = overrideInterfaceParameter( "_TIMEOUT3", null );
							if( str != null )
								messageTimeout3 = Integer.parseInt( str );
							else
								messageTimeout3 = ((Integer) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "_TIMEOUT3" )).intValue();

						} catch( Exception e ) {
							String str = overrideInterfaceParameter( DIAGOPEN_ + "TIMEOUT3", null );
							if( str != null )
								messageTimeout3 = Integer.parseInt( str );
							else
								messageTimeout3 = ((Integer) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, DIAGOPEN_ + "TIMEOUT3" )).intValue();

						}
						if( Debug )
							System.out.println( "DiagOpen PP ICOM_P Parameter TIMEOUT3 via PS-Konfig = " + messageTimeout3 );
					} catch( Exception e ) {
					}
				}

				//Version 108_0_T einlesen der reslichen ICOMP-Varibalen �ber Pr�fsandsvariablen m�glich

				//Einlesen des _VerifyVIn �ber Pr�fstandsvariable				
				if( overrideInterfaceParameter( "ICOM_VERIFY_VIN", getArg( "ICOM_VERIFY_VIN" ) ) == null ) {
					try {
						try {
							tempPSV = overrideInterfaceParameter( "_ICOMP_VERIFY_VIN", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, "_ICOMP_VERIFY_VIN" ) );
						} catch( VariablesException f ) {
							tempPSV = overrideInterfaceParameter( DIAGOPEN_ + "ICOMP_VERIFY_VIN", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, DIAGOPEN_ + "ICOMP_VERIFY_VIN" ) );
						}

						if( tempPSV.equalsIgnoreCase( "true" ) ) {
							bIcomVerifyVin = true;
						}
						tempPSV = null;
						if( Debug ) {
							System.out.println( "DiagOpen PP ICOM_P Parameter ICOMP_VERIFY_VIN via PS-Konfig = " + bIcomVerifyVin );
						}
					} catch( Exception e ) {
					}
				}

				//Einlesen des Parameter ICOMP_Extern �ber die Pr�fstandsvariable
				if( overrideInterfaceParameter( "EXTERN", getArg( "EXTERN" ) ) == null ) {
					try {
						try {
							tempPSV = overrideInterfaceParameter( "_ICOMP_EXTERN", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, "_ICOMP_EXTERN" ) );
						} catch( VariablesException f ) {
							tempPSV = overrideInterfaceParameter( DIAGOPEN_ + "ICOMP_EXTERN", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, DIAGOPEN_ + "ICOMP_EXTERN" ) );
						}

						if( tempPSV.equalsIgnoreCase( "true" ) ) {
							extern = true;
						}
						tempPSV = null;
						if( Debug ) {
							System.out.println( "DiagOpen PP ICOM_P Parameter EXTERN via PS-Konfig = " + extern );
						}
					} catch( Exception e ) {
					}
				}

				//Einlesen des Parameters ICOMP_INIT �ber die Pr�fstandsvariable				
				if( overrideInterfaceParameter( "INIT", getArg( "INIT" ) ) == null ) {
					try {
						try {
							tempPSV = overrideInterfaceParameter( "_ICOMP_INIT", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, "_ICOMP_INIT" ) );
						} catch( VariablesException f ) {
							tempPSV = overrideInterfaceParameter( DIAGOPEN_ + "ICOMP_INIT", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, DIAGOPEN_ + "ICOMP_INIT" ) );
						}

						if( tempPSV.equalsIgnoreCase( "true" ) ) {
							marriage = true;
						}
						tempPSV = null;
						if( Debug ) {
							System.out.println( "DiagOpen PP ICOM_P Parameter INIT via PS-Konfig = " + marriage );
						}
					} catch( Exception e ) {
					}
				}

				//Einlesen des Parameters _ICOMP_WRITE_REMOTEHOST �ber die Pr�fstandsvariable
				if( overrideInterfaceParameter( "ICOM_WRITE_REMOTEHOST", getArg( "ICOM_WRITE_REMOTEHOST" ) ) == null ) {
					try {
						try {
							tempPSV = overrideInterfaceParameter( "_ICOMP_WRITE_REMOTEHOST", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, "_ICOMP_WRITE_REMOTEHOST" ) );
						} catch( VariablesException f ) {
							tempPSV = overrideInterfaceParameter( DIAGOPEN_ + "ICOMP_WRITE_REMOTEHOST", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, DIAGOPEN_ + "ICOMP_WRITE_REMOTEHOST" ) );
						}

						if( tempPSV.equalsIgnoreCase( "true" ) ) {
							bIcomWriteRemoteHost = true;
						}
						tempPSV = null;
						if( Debug ) {
							System.out.println( "DiagOpen PP ICOM_P Parameter ICOMP_WRITE_REMOTEHOST via PS-Konfig = " + bIcomWriteRemoteHost );
						}
					} catch( Exception e ) {
					}
				}

				//Einlesen des Parameters _ICOMP_ERR_TIMEOUT �ber die Pr�fstandsvariable
				if( overrideInterfaceParameter( "ICOM_ERR_TIMEOUT", getArg( "ICOM_ERR_TIMEOUT" ) ) == null ) {
					try {
						try {
							String str = overrideInterfaceParameter( "_ICOMP_ERR_TIMEOUT", null );
							if( str != null )
								iIcomErrTimeout = Integer.parseInt( str );
							else
								iIcomErrTimeout = ((Integer) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "_ICOMP_ERR_TIMEOUT" )).intValue();

						} catch( Exception e ) {
							String str = overrideInterfaceParameter( DIAGOPEN_ + "ICOMP_ERR_TIMEOUT", null );
							if( str != null )
								iIcomErrTimeout = Integer.parseInt( str );
							else
								iIcomErrTimeout = ((Integer) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, DIAGOPEN_ + "ICOMP_ERR_TIMEOUT" )).intValue();
						}

						if( Debug )
							System.out.println( "DiagOpen PP ICOM_P Parameter ICOMP_ERR_TIMEOUT via PS-Konfig = " + iIcomErrTimeout );

					} catch( Exception e ) {
					}
				}

				if( overrideInterfaceParameter( "ICOM_IP_LOOKUP_PATH", getArg( "ICOM_IP_LOOKUP_PATH" ) ) == null ) {
					try {
						try {
							tempPSV = overrideInterfaceParameter( "_ICOMP_IP_LOOKUP_PATH", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, "_ICOMP_IP_LOOKUP_PATH" ) );
						} catch( VariablesException f ) {
							tempPSV = overrideInterfaceParameter( DIAGOPEN_ + "ICOMP_IP_LOOKUP_PATH", (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, DIAGOPEN_ + "ICOMP_IP_LOOKUP_PATH" ) );
						}

						if( tempPSV != null ) {
							String sTempIcomIpLookupPath = tempPSV;

							tempPSV = null;
							if( Debug ) {
								System.out.println( "DiagOpen PP ICOM_P Parameter _ICOMP_IP_LOOKUP_PATH via PS-Konfig = " + sTempIcomIpLookupPath );
							}
							int index;
							if( (index = sTempIcomIpLookupPath.indexOf( ";" )) != -1 ) {

								int strLength = sTempIcomIpLookupPath.length();
								if( Debug ) {
									System.out.println( "DiagOpen PP ICOM_P Parameter tempLookupPath = " + sTempIcomIpLookupPath );
								}

								sIcomIpLookupPath = sTempIcomIpLookupPath.substring( 0, index );
								sIcomIpLookupPath2 = sTempIcomIpLookupPath.substring( index + 1, strLength );

								if( Debug )
									System.out.println( "DiagOpen PP ICOM_P Parameter ICOM_IP_LOOKUP_PATH = " + sIcomIpLookupPath );
								if( Debug )
									System.out.println( "DiagOpen PP ICOM_P Parameter ICOM_IP_LOOKUP_PATH2 = " + sIcomIpLookupPath2 );

								// beide Pfade getrennt mit ; zur�ck in sIcomIpLookupPath um parallelen Aufruf zu erm�glichen
								sIcomIpLookupPath = sTempIcomIpLookupPath;

							} else {
								sIcomIpLookupPath = sTempIcomIpLookupPath;
								if( Debug )
									System.out.println( "DiagOpen PP ICOM_P Parameter ICOM_IP_LOOKUP_PATH = " + sIcomIpLookupPath );
							}
						}
					} catch( Exception e ) {
					}

				}

				//Version 108_0_T Ende einlesen der restlichen ICOMP-Varibalen �ber Pr�fsandsvariablen m�glich

				// >>>> mba 18.09.2009 timeout fuer api init
				int repeatCounter = 1;

				getPr�flingLaufzeitUmgebung().getUserDialog().setAllowCancel( true );
				getPr�flingLaufzeitUmgebung().getUserDialog().displayStatusMessage( "Status INTERFACE", "Warten auf Verbindung\n Waiting for connection\n\n INTERFACE -> ON?\n Z�NDUNG / IGNITION -> ON?", -1 );

				//Wiederholschleife f�r mehr Verbindungsversuche, wenn Interface noch nicht erreichbar
				do {
					if( getPr�flingLaufzeitUmgebung().getUserDialog().isCancelled() ) {
						if( Debug )
							System.out.println( "DiagOpen PP pressCancel apiEnd/ApiInit" );
						break;
					}

					try {

						// Ediabas Initialisierung lt. Ediabas.INI
						if( sIcomDriver == null || bIcomAutoAssign == false ) {
							// Auf Nummer sicher: erst mal Freigabe EDIABAS-Device
							if( Debug )
								System.out.println( "DiagOpen PP apiEnd (vor) " );

							Ediabas.apiEnd();
							if( Debug )
								System.out.println( "DiagOpen PP apiEnd (nach)" );

							//Abfagen Fehler bei Umstellung, falls DLL nicht verf�gbar
							//Pr�fung muss dennoch ohne Fehler weiterlaufen
							try {
								// InterfaceWechsel f�r neuladen INI-Files
								if( Debug )
									System.out.println( "DiagOpen PP apiInitExt(nul) (vor) " );
								Ediabas.apiInitExt( "nul", "", "" );
								if( Debug )
									System.out.println( "DiagOpen PP apiInitExt(nul) (nach) " );
							} catch( Exception nul ) {
								if( Debug ) {
									System.out.println( "DiagOpen PP apiInitExt(nul) Exception nul --> apiError: " + Ediabas.apiErrorCode() + " --> " + Ediabas.apiErrorText() );
									nul.printStackTrace();
								}
							} catch( Throwable nul ) {
								if( Debug ) {
									System.out.println( "DiagOpen PP apiInitExt(nul) Throwable nul --> apiError: " + Ediabas.apiErrorCode() + " --> " + Ediabas.apiErrorText() );
									nul.printStackTrace();
								}
							}

							// Verbindungsaufbau �ber EDIABAS-Settings
							if( Debug )
								System.out.println( "DiagOpen PP apiInit (vor)" );
							Ediabas.apiInitAuto();
							if( Ediabas.apiErrorCode() == 0 ) {
								//Wenn Verbindungsaufau i.O. --> raus aus Schleife
								if( Debug )
									System.out.println( "DiagOpen PP apiInit " + repeatCounter + ". apiInit i.O. -> apiError: " + Ediabas.apiErrorCode() + " --> " + Ediabas.apiErrorText() );
								break;
							} else {
								//Wenn Verbindungsaufbau n.i.O. --> weiter in Schleife
								if( Debug )
									System.out.println( "DiagOpen PP apiInit " + repeatCounter + ". apiInit n.i.O. -> apiError: " + Ediabas.apiErrorCode() + " --> " + Ediabas.apiErrorText() );
							}
							if( Debug )
								System.out.println( "DiagOpen PP apiInit (nach)" );
						} else {
							if( Debug )
								System.out.println( "DiagOpen PP break nach apiInit: driverIcomP: " + sIcomDriver );
							break;
						}

					} catch( Exception e ) {

						//MBa: Hier darf kein Fehler-Result mehr erzeugt werden!
						/* 
						result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Exception: EDIABAS DLL", "", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						*/
						if( Debug ) {
							System.out.println( "DiagOpen PP apiInit Exception --> apiError: " + Ediabas.apiErrorCode() + " --> " + Ediabas.apiErrorText() );
							e.printStackTrace();
						}
					}
					try {
						Thread.sleep( 1000 );
					} catch( Exception ex ) {
					}
					repeatCounter++;
					//weiter in Schleife bis repeatCounter abgelaufen oder break
				} while( repeatCounter <= messageTimeout3 );

				//Entladen der Statusmeldung
				getPr�flingLaufzeitUmgebung().getUserDialog().reset();
				// <<<<< mba 18.09.2009 timeout fuer api init

				//Beginn ICOM P Block mit Autoassign
				if( sIcomDriver != null && bIcomAutoAssign == true ) {

					String ip = null; //Basis f�r Ergebnis IP_ADDRESS
					String ipVerifyVin = null; //Aus XML gelesene IP-Adresse f�r Vergleich mit ipVerifyVinNio
					String ipVerifyVin2 = null; //Aus XML gelesene IP-AdresseS
					String ipSave = null; //zur Fehlerbehandlung, nach zur�cksetzen ip
					String ipVerifyVinNio = null; //IP-Adresse, wenn Verbindungsaufbau bei VerifyVin n.i.O.
					boolean ipVerifyResult = true; //Vergleich VINs iO/niO
					boolean bCancel = false; //Abbruchbutton gedr�ckt???

					if( Debug )
						System.out.println( "DiagOpen PP ICOM_P Interface/Driver: " + sIcomDriver );

					try {
						repeatCounter = 1;

						getPr�flingLaufzeitUmgebung().getUserDialog().setAllowCancel( true );
						getPr�flingLaufzeitUmgebung().getUserDialog().displayStatusMessage( "Status ICOM P", "Warten auf ICOM P-Verbindung\n Waiting for ICOM P-connection\n\n ICOM P -> ON?\n Z�NDUNG / IGNITION -> ON?", -1 );

						do {
							bIcomXMLFound = true;
							if( marriage == true ) {
								//Abbruch Autoassign ICOM P und direkt zur Eingabe IP-Adresse
								if( Debug )
									System.out.println( "DiagOpen PP ICOM P IP-Adressen-Eingabe erzwungen = " + marriage );
								break;
							}

							if( Debug )
								System.out.println( "DiagOpen PP REPEAT ICOM_P bei Timeout3 = " + repeatCounter );

							if( getPr�flingLaufzeitUmgebung().getUserDialog().isCancelled() ) {
								if( Debug )
									System.out.println( "DiagOpen PP REPEAT ICOM_P pressCancel " );
								break;
							}

							try {

								if( bIcomVerifyVin == true && sIcomIpLookupPath != null ) {
									//Vergleich der VIN aus Pr�fauftrag und der im Fahrzeug wird ausgef�hrt
									//zur�cksetzen zur Sicherheit
									ipVerifyResult = true;
									ipVerifyVin = null;
									//Auslesen IP aus XML-Datei
									ipVerifyVin = this.getIcomIPFromFiles( "I" + this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7() + ".XML", sIcomIpLookupPath );
									//zweiter Pfad unn�tig, da paralleler Zugriff bereits erfolgt
									//if( ipVerifyVin == null && sIcomIpLookupPath2 != null ) {
									//	if( Debug )
									//		System.out.println( "DiagOpen PP ICOM_P VIN-XML-File 2ter Server (VerifyVin)" );
									//	ipVerifyVin = this.getIcomIPFromFiles( "I" + this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7() + ".XML", sIcomIpLookupPath2 );
									//}

									if( Debug )
										System.out.println( "DiagOpen PP ICOM_P VIN-XML-File (VerifyVin): I" + this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7() + ".XML -> IP: " + ipVerifyVin );

									//bei Fehlender XML sonst bei folgender Abfrage problem
									if( ipVerifyVin == null ) {
										ipVerifyVin = "0.0.0.0";
										tempPSV = "NO_ICOMP_XMLs --> Please plug in ICOMP earlier! / Bitte ICOMP rechtzeitig anstecken!";
									} else {

										if( tempPSV == "NO_ICOMP_XMLs --> Please plug in ICOMP earlier! / Bitte ICOMP rechtzeitig anstecken!" || tempPSV == "NO_VIN /keine FGNR" ) {
											tempPSV = null;
										}
									}

									// Vergleich IP Adressen aus XML und Verbindungsaufbau niO
									// Version 105_0_T
									if( ipVerifyVin.equalsIgnoreCase( ipVerifyVinNio ) && novin == false ) {
										// XML noch nicht wieder neu geschrieben, daher kein erneuter Verbindungsversuch
										Thread.sleep( iIcomErrTimeout );
										if( Debug )
											System.out.println( "DiagOpen PP ICOM_P : ipVerifyVin.equals(ipVerifyVinNio) :" + iIcomErrTimeout + "ms" );
										//Setzen auf False, da VIN und IP nicht zusammenpassen
										//dadurch wird die Schleife von vorn begonnen
										ipVerifyResult = false;

									} else {

										if( tempPSV == "DIFFERENT_VIN /unterschiedliche FGNR --> Please plug in ICOMP earlier! / Bitte ICOMP rechtzeitig anstecken!" ) //Variable f�r Fehlerausgabe zur�cksetzen
										{
											tempPSV = null;
										}
										//erster Verbindungsaufbau ODER
										//Vergleich niO--> IPs unterschiedlich --> Verbindungsaufbau
										String sIcomConf = sIcomDriver + ":RemoteHost=" + getArg( "FZS" ) + ";DNS=ICOM";

										if( !ipVerifyVin.equalsIgnoreCase( "0.0.0.0" ) ) {
											//Zur Sicherheit
											Ediabas.apiEnd();
											if( Debug )
												System.out.println( "DiagOpen PP (VerifyVin) apiInitExt (vor) : " + sIcomConf );
											Ediabas.apiInitExt( sIcomConf, "", "" );
											if( Debug )
												System.out.println( "DiagOpen PP (VerifyVin) apiInitExt (nach): " + sIcomConf );
										} else {
											Thread.sleep( iIcomErrTimeout );
											bIcomXMLFound = false;
										}
									}

								} else {

									if( sIcomIpLookupPath != null ) {
										//Auslesen IP aus XML-Datei
										//if( Debug )
										//	System.out.println( "DiagOpen PP ICOM_P VIN-XML-File 1ter Server" );
										ipVerifyVin2 = this.getIcomIPFromFiles( "I" + this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7() + ".XML", sIcomIpLookupPath );
										//zweiter Pfad unn�tig, da paralleler Zugriff auf die Shares bereits erfolgt
										//if( ipVerifyVin2 == null && sIcomIpLookupPath2 != null ) {
										//	if( Debug )
										//		System.out.println( "DiagOpen PP ICOM_P VIN-XML-File 2ter Server" );
										//	ipVerifyVin2 = this.getIcomIPFromFiles( "I" + this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7() + ".XML", sIcomIpLookupPath2 );
										//}
									}

									//bei Fehlender XML sonst bei folgender Abfrage problem
									if( ipVerifyVin2 == null ) {
										ipVerifyVin2 = "0.0.0.0";
										if( sIcomIpLookupPath != null ) {
											tempPSV = "NO_ICOMP_XMLs --> Please plug in ICOMP earlier! / Bitte ICOMP rechtzeitig anstecken!";
										}
									} else {

										if( tempPSV == "NO_ICOMP_XMLs --> Please plug in ICOMP earlier! / Bitte ICOMP rechtzeitig anstecken!" || tempPSV == "NO_VIN /keine FGNR" ) {
											tempPSV = null;
										}
									}

									//Kein Vergleich der VIN wird durchgef�hrt, direkter Verbindungsaufbau mit ICOM P �ber IP
									String sIcomConf = sIcomDriver + ":RemoteHost=" + getArg( "FZS" ) + ";DNS=ICOM";

									if( !ipVerifyVin2.equalsIgnoreCase( "0.0.0.0" ) || (sIcomIpLookupPath == null) ) {
										//Zur Sicherheit
										Ediabas.apiEnd();
										if( Debug )
											System.out.println( "DiagOpen PP apiInitExt (vor) : " + sIcomConf );
										Ediabas.apiInitExt( sIcomConf, "", "" );
										if( Debug )
											System.out.println( "DiagOpen PP apiInitExt (nach): " + sIcomConf );
									} else {
										Thread.sleep( iIcomErrTimeout );
										bIcomXMLFound = false;
									}
								}

								if( Ediabas.apiErrorCode() == 0 && ipVerifyResult == true && bIcomXMLFound == true ) {
									if( Debug )
										System.out.println( "DiagOpen PP ICOM_P " + repeatCounter + ". apiInitExt i.O. -> apiError: " + Ediabas.apiErrorCode() + " --> " + Ediabas.apiErrorText() );

									// Verify VIN nach erfolgreichem Verbindungsaufbau
									if( bIcomVerifyVin == true ) {
										try {
											temp = "false";
											// 0=VIN neu aus dem Fahrzeug auslesen und dann Vergleichen (default);
											// 1=VIN mit der bereits ausgelesen VIN Vergleichen;
											String jobPara = getArg( "FZS" ) + ";1";
											if( Debug )
												System.out.println( "DiagOpen PP ICOM_P : STEUERN_VERIFY_VIN --> jobPara <" + jobPara + ">" );

											temp = Ediabas.executeDiagJob( "ICOM_P", "STEUERN_VERIFY_VIN", jobPara, "JOB_STATUS" );

											if( Debug )
												System.out.println( "DiagOpen PP ICOM_P : STEUERN_VERIFY_VIN --> JOB_STATUS <" + temp + ">" );

											if( temp.equals( "OKAY" ) ) {
												try {
													temp = "false";
													temp = Ediabas.executeDiagJob( "ICOM_P", "STATUS_GET_CONFIG_DETAIL", "WLAN-IP", "" );

													if( Debug )
														System.out.println( "DiagOpen PP ICOM_P (VerifyVinIo) : STATUS_GET_CONFIG_DETAIL --> WLAN-IP <" + temp + ">" );

													if( temp.equals( "OKAY" ) ) {
														ip = Ediabas.getDiagResultValue( "STAT_VALUE" );
														if( Debug )
															System.out.println( "DiagOpen PP ICOM_P (VerifyVinIo): IP Adresse via EDIABAS <" + ip + ">" );
														// Version 91_0_F
														if( Debug )
															System.out.println( "DiagOpen PP ICOM_P (VerifyVinIo): IP Adresse from xml file <" + ipVerifyVin + ">" );
														if( ipVerifyVin.equals( "192.168.68.40" ) ) {
															ip = ipVerifyVin;
															if( Debug )
																System.out.println( "DiagOpen PP ICOM_P (VerifyVinIo): IP Adresse changed to LAN-Adresse <" + ip + ">" );
														}
														// Version 91_0_F

														//juhuu!!
														bCancel = true;
														ipVerifyResult = true;
													}

												} catch( Exception e ) {
													if( Debug ) {
														System.out.println( "DiagOpen PP ICOM_P Exception (VerifyVinIo): STATUS_GET_CONFIG_DETAIL --> WLAN-IP <" + temp + ">" );
														e.printStackTrace();
													}
													// Was passiert, wenn ICOM _P keine IP liefert?
													// Fehler ignorieren!
												}
												//Verbindung steht -- Raus hier
												break;
											} else {
												// Version 106_0_T
												if( temp.equals( "NO_VIN" ) ) {
													if( Debug )
														System.out.println( "DiagOpen PP ICOM_P (VerifyVinNio) : Fehlermeldung (NO_VIN)" );
													//getPr�flingLaufzeitUmgebung().releaseUserDialog();													
													//getPr�flingLaufzeitUmgebung().getUserDialog().requestAlertMessage( PB.getString( "ICOM_P_Pruefe_FGNR" ), PB.getString( "NO_VIN" ), -1 );
													getPr�flingLaufzeitUmgebung().getUserDialog().setAllowCancel( true );
													getPr�flingLaufzeitUmgebung().getUserDialog().displayStatusMessage( "Status Vin Vergleich", "Keine FGNR / no VIN \n  Bitte warten oder abbrechen und IP eingeben!!! \n  Please wait or cancel and enter IP!!! ", -1 );
													result = new Ergebnis( "DiagFehler", "ICOM", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "ICOM P VERIFY VIN NO_VIN", Ergebnis.FT_IGNORE );
													ergListe.add( result );
													novin = true;
													tempPSV = "NO_VIN /keine FGNR";

												}
												if( temp.equals( "VIN_DIFFERENT" ) ) {
													if( Debug )
														System.out.println( "DiagOpen PP ICOM_P (VerifyVinNio) : Fehlermeldung (VIN_DIFFERENT)" );
													//getPr�flingLaufzeitUmgebung().releaseUserDialog();													
													//getPr�flingLaufzeitUmgebung().getUserDialog().requestAlertMessage( PB.getString( "ICOM_P_Pruefe_FGNR" ), PB.getString( "VIN_DIFFERENT" ), -1 );
													getPr�flingLaufzeitUmgebung().getUserDialog().setAllowCancel( true );
													getPr�flingLaufzeitUmgebung().getUserDialog().displayStatusMessage( "Status Vin Vergleich", "Unterschiedliche FGNR / different VIN  \n Bitte warten oder abbrechen und IP eingeben!!! \n  Please wait or cancel and enter IP!!! ", -1 );
													result = new Ergebnis( "DiagFehler", "ICOM", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "ICOM P VERIFY VIN VIN_DIFFERENT", Ergebnis.FT_IGNORE );
													ergListe.add( result );
													novin = false;
													tempPSV = "DIFFERENT_VIN /unterschiedliche FGNR --> Please plug in ICOMP earlier! / Bitte ICOMP rechtzeitig anstecken!";
												}
												// Version 106_0_T

												try {
													temp = "false";
													temp = Ediabas.executeDiagJob( "ICOM_P", "STATUS_GET_CONFIG_DETAIL", "WLAN-IP", "" );

													if( Debug )
														System.out.println( "DiagOpen PP ICOM_P (VerifyVinNio) : STATUS_GET_CONFIG_DETAIL --> WLAN-IP <" + temp + ">" );

													if( temp.equals( "OKAY" ) ) {
														ipVerifyVinNio = Ediabas.getDiagResultValue( "STAT_VALUE" );
														if( Debug )
															System.out.println( "DiagOpen PP ICOM_P (VerifyVinNio) : IP Adresse via EDIABAS <" + ipVerifyVinNio + ">" );
														// Version 91_0_F
														if( Debug )
															System.out.println( "DiagOpen PP ICOM_P (VerifyVinNio): IP Adresse from xml file <" + ipVerifyVin + ">" );
														if( ipVerifyVin.equals( "192.168.68.40" ) ) {
															ip = ipVerifyVin;
															if( Debug )
																System.out.println( "DiagOpen PP ICOM_P (VerifyVinNio): IP Adresse changed to LAN-Adresse <" + ip + ">" );
														}
														// Version 91_0_F
													}

													ipVerifyResult = false;

												} catch( Exception e ) {
													if( Debug ) {
														System.out.println( "DiagOpen PP ICOM_P Exception (VerifyVinNio) : STATUS_GET_CONFIG_DETAIL --> WLAN-IP <" + temp + ">" );
														e.printStackTrace();
													}
													// Was passiert, wenn ICOM _P keine IP liefert?
													// Fehler ignorieren!
												}

												//Zur Sicherheit damit andere wieder zugreifen k�nnen.
												Ediabas.apiEnd();
												// Version 105_0_T
												if( novin == true ) {
													Thread.sleep( iIcomErrTimeout );
													if( Debug ) {
														System.out.println( "DiagOpen PP ICOM_P (VerifyVinNio): novin == true :" + iIcomErrTimeout + "ms" );

													}
													// Version 105_0_T
												}
											}

										} catch( Exception e ) {
											if( Debug ) {
												System.out.println( "DiagOpen PP ICOM_P Exception (VerifyVinNio) : STEUERN_VERIFY_VIN --> JOB_STATUS <" + temp + ">" );
												e.printStackTrace();
											}
											//Zur Sicherheit damit andere wieder zugreifen k�nnen.
											Ediabas.apiEnd();
										}

										repeatCounter++;

									} else {
										try {
											temp = "false";
											temp = Ediabas.executeDiagJob( "ICOM_P", "STATUS_GET_CONFIG_DETAIL", "WLAN-IP", "" );

											if( Debug )
												System.out.println( "DiagOpen PP ICOM_P : STATUS_GET_CONFIG_DETAIL --> WLAN-IP <" + temp + ">" );

											if( temp.equals( "OKAY" ) ) {
												ip = Ediabas.getDiagResultValue( "STAT_VALUE" );
												if( Debug )
													System.out.println( "DiagOpen PP ICOM_P : IP Adresse via EDIABAS <" + ip + ">" );

												//juhuu!!
												bCancel = true;
												ipVerifyResult = true;
											}

										} catch( Exception e ) {
											if( Debug ) {
												System.out.println( "DiagOpen PP ICOM_P Exception : STATUS_GET_CONFIG_DETAIL --> WLAN-IP <" + temp + ">" );
												e.printStackTrace();
											}
										}
										//Verbindung steht -- Raus hier
										break;
									}
								} else { // fehler bei apiInitExt
									if( ipVerifyResult == false ) {

										result = new Ergebnis( "DiagFehler", "ICOM", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "ICOM_P VERIFY VIN ERROR", Ergebnis.FT_IGNORE );
										ergListe.add( result );
										if( Debug && bIcomXMLFound )
											System.out.println( "DiagOpen PP ICOM_P ipVerifyResult == false" );

									} else {
										ipVerifyResult = true;
										result = new Ergebnis( "DiagFehler", "ICOM", "", "", "", "IP_ADDRESS", "" + ip, "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "ICOM_P WLAN-IP: " + ip + " apiError: " + Ediabas.apiErrorCode() + " --> " + Ediabas.apiErrorText(), Ergebnis.FT_IGNORE );
										ergListe.add( result );
										if( Debug && bIcomXMLFound )
											System.out.println( "DiagOpen PP ICOM_P " + repeatCounter + ". apiInitExt n.i.O. (else) -> apiError: " + Ediabas.apiErrorCode() + " --> " + Ediabas.apiErrorText() );
									}
									repeatCounter++;
								}

								try {// warte etwas
									Thread.sleep( 100 );
								} catch( InterruptedException iex ) {
								}
							} catch( Exception e ) {

								if( Debug ) {
									e.printStackTrace();
									System.out.println( "DiagOpen PP ICOM P  Exception at apiInitExt n.i.O. -> apiError:  " + Ediabas.apiErrorCode() + " --> " + (Ediabas.apiErrorText() != null ? Ediabas.apiErrorText() : "Ediabas.apiErrorText() == null!") );
								}

								//nur Verl�ngerung wenn auch messageTimeout3 gesetzt!!!
								if( Ediabas.apiErrorCode() != 159 && messageTimeout3 != 0 ) {
									//Sleep bei Fehler:
									if( Ediabas.apiErrorCode() == 166 || Ediabas.apiErrorCode() == 155 || Ediabas.apiErrorCode() == 156 ) {
										// Weiter mit normaler Wiederholung
										// 166 -->	NET-0016: Host Error
										// 155 -->	NET-0005: No Support
										// 156 -->	NET-0006: Access Denied

										Thread.sleep( iIcomErrTimeout );
										if( Debug )
											System.out.println( "DiagOpen PP ICOM_P: apiErrorCode = " + Ediabas.apiErrorCode() + " && messageTimeout3 != null: " + iIcomErrTimeout + "ms" );

										if( Debug )
											System.out.println( "DiagOpen PP ICOM P " + repeatCounter + ". apiInitExt n.i.O. (e) IF -> apiError: " + Ediabas.apiErrorCode() + " --> " + Ediabas.apiErrorText() );
									} else {
										//Abbruch und direkt zur IP-Eingabe
										// 39  -->  IFH-0029: Access Denied
										// 152 -->  NET-0002: Illegal Version
										// 153 -->	NET-0003: Initialization Error										
										// 157 -->	NET-0007: System Error
										// 165 -->	NET-0015: Host Not Found
										// 169 -->	NET-0019: Unknown Service
										// 170 -->	NET-0020: Unknown Host

										if( Debug )
											System.out.println( "DiagOpen PP ICOM P  apiInitExt n.i.O. (e) ELSE -> apiError: " + Ediabas.apiErrorCode() + " --> " + Ediabas.apiErrorText() );

										result = new Ergebnis( "DiagFehler", "ICOM P", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "DNS=ICOM --> " + Ediabas.apiErrorCode() + ": " + Ediabas.apiErrorText(), Ergebnis.FT_IGNORE );
										ergListe.add( result );
										//direkt zu IP-Adresseingabe
										marriage = true;
										break;
									}
								}

								//Hochz�hlen au�er bei Fehlern im Else-Zweig
								repeatCounter++;
								if( Debug )
									System.out.println( "DiagOpen PP ICOM P  apiInitExt n.i.O. (e) repeatCounter++" + repeatCounter );

							}

						} while( repeatCounter <= messageTimeout3 );

						//Version 109_0_T Merken des Autoassign Fehlers f�r sp�tere Ausgabe, wenn DiagOpen NIO						

						if( Ediabas.apiErrorCode() != 0 ) {
							if( tempPSV == null )

							{
								tempPSV = Ediabas.apiErrorCode() + ": " + Ediabas.apiErrorText();
							}

						}

						//Version 109_0_T Ende Merken des Autoassign Fehlers f�r sp�tere Ausgabe, wenn DiagOpen NIO	

						if( Ediabas.apiErrorCode() != 0 && marriage == false ) {
							result = new Ergebnis( "DiagFehler", "ICOM P", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "DNS=ICOM --> " + Ediabas.apiErrorCode() + ": " + Ediabas.apiErrorText(), Ergebnis.FT_IGNORE );
							ergListe.add( result );

						}
						//Entladen der Statusmeldung
						getPr�flingLaufzeitUmgebung().getUserDialog().reset();

						int iCntRetries = 0;

						//bisher keine IP �ber SGBD-Job
						if( !bCancel ) {
							// Fehlerbehandlung bisher keine IP bekommen
							while( iCntRetries++ <= messageTimeout3 && !bCancel ) {

								// Default: wenn IP noch nicht vorhanden oder Fehler: user fragen!
								if( ip == null || ip.equals( "" ) ) {

									boolean inputFlag = false;

									while( inputFlag == false ) {
										ip = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInput( "INPUT ICOM P", "IP-Adresse oder ICOM Nr.:\n IP-address or ICOM no.:", messageTimeout );
										if( Debug )
											System.out.println( "DiagOpen PP getUserDialog Input IP-Adresse oder ICOM Nr.: INPUT = '" + ip + "'" );

										// wenn auch der user nix eingegeben hat: Abbruch!
										if( ip == null || ip.equals( "" ) ) {
											bCancel = true;
											ip = null;
											break;
										}

										// Eingabe ICOM P Name (4 stellig nach Namenkonvention --> statischer Teil muss gesetzt sein!)				
										//if (ip.length() == 4 ){
										if( ip.length() > 2 && ip.length() <= 5 ) {

											if( sIcomIpFamily == null || sIcomIpFamily.equals( "" ) ) {
												//wenn keine IP-Familie angegeben weiter
												if( Debug )
													System.out.println( "DiagOpen PP ICOM Nr.: ERROR in _ICOM_IP_FAMILY == " + sIcomIpFamily );
											} else {

												String tempIp = null;
												String last3 = null;
												//erste Stelle hinter Ip-Familie einf�gen und anschlie�end den Punkt als Abschluss des Ip-Segments

												if( ip.length() > 3 ) {

													if( ip.length() == 4 ) {
														tempIp = sIcomIpFamily + ip.substring( 0, 1 );
														tempIp += ".";
														//letzeten 3 Stellen in Hilfvariable
														last3 = ip.substring( 1 );

													}

													if( ip.length() == 5 ) {
														tempIp = sIcomIpFamily + ip.substring( 0, 2 );
														tempIp += ".";
														//letzeten 3 Stellen in Hilfvariable
														last3 = ip.substring( 2 );
													}
												} else {
													tempIp = sIcomIpFamily + ".";
													//letzeten 3 Stellen in Hilfvariable
													last3 = ip.substring( 0 );
												}

												if( Debug )
													System.out.println( "DiagOpen PP tempIP = " + tempIp );

												if( Debug )
													System.out.println( "DiagOpen PP ICOM letzten 3 Stellen aus ICOM Nr.:" + last3 );
												//F�hrende Nullen entfernen
												last3 = Integer.toString( Integer.parseInt( last3 ) );
												//zusammensetzen f�r gesamte IP
												ip = tempIp + last3;

												if( Debug )
													System.out.println( "DiagOpen PP ICOM IP-Adresse via ICOM Nr.: " + ip + " aus " + tempIp + " + " + last3 );

												inputFlag = true;
											}
										}

										// Version 98_0_
										/*
										//Eingabe IP-Adresse
										if( ip.length() > 6 && ip.length() <= 15 ) {
										
											if( Debug )
												System.out.println( "DiagOpen PP ICOM P ICOM IP-Adresse via getUserDialog: " + ip );
											inputFlag = true;
										}
										
										//Fehlerhafte Eingabe, Wiederholung der Eingabe bis das Richtige, raus aus Schleife durch keine Eingabe
										//Eingabe von 1,2,6 oder gr��er 15 Zeichen ist nicht erlaubt
										if( ip.length() < 3 || ip.length() > 15 || ip.length() == 6 ) {
											getPr�flingLaufzeitUmgebung().getUserDialog().displayAlertMessage( "ERROR ICOM P", "Ung�ltige Eingabe: \n Invalid input: \n\n" + ip, 3 );
											ip = null;
										}
										*/
										// Version 98_0_

										// Version 98_0_
										if( isValidIP( ip ) ) {
											if( Debug )
												System.out.println( "DiagOpen PP ICOM P ICOM IP-Adresse via getUserDialog: " + ip );
											inputFlag = true;

										} else {
											getPr�flingLaufzeitUmgebung().getUserDialog().displayAlertMessage( "ERROR ICOM P", "Ung�ltige Eingabe: \n Invalid input: \n\n" + ip, 3 );
											ip = null;
										}
										// Version 98_0_
									}
								}

								try {
									//Zur Sicherheit
									Ediabas.apiEnd();
									if( Debug )
										System.out.println( "DiagOpen PP ICOM P: " + iCntRetries + ". apiInitExt (vor) IP-Eingabe --> ip: " + ip );
									if( ip != null ) {
										Ediabas.apiInitExt( sIcomDriver + ":RemoteHost=" + ip, "", "" );
									}
									if( Debug )
										System.out.println( "DiagOpen PP ICOM P: " + iCntRetries + ". apiInitExt (nach) IP-Eingabe -> apiError: " + Ediabas.apiErrorCode() + " --> " + Ediabas.apiErrorText() + " ip: " + ip );

									if( Ediabas.apiErrorCode() == 0 ) {

										//SET VIN durchf�hren damit XML angelegt um zu verhindern das ggf. wieder nach IP gefragt wird.
										//Weiter auch bei Fehler
										try {

											temp = "false";
											temp = Ediabas.executeDiagJob( "ICOM_P", "STEUERN_SET_VIN", getArg( "FZS" ), "" );

											if( Debug ) {
												System.out.println( "DiagOpen PP ICOM_P : STEUERN_SET_VIN --> JOB_STATUS <" + temp + ">" );
												if( temp.equals( "OKAY" ) )
													System.out.println( "DiagOpen PP ICOM_P : STEUERN_SET_VIN --> SERVER_STATUS <" + temp + ">" );
											}

											result = new Ergebnis( "DiagnoseFehler", "EDIABAS", "ICOM_P", "STEUERN_SET_VIN", "", "STEUERN_SET_VIN", Ediabas.getDiagResultValue( "SERVER_STATUS" ), "OKAY", "", "0", "", "", "", "ServerShareError", "", Ergebnis.FT_IGNORE );
											ergListe.add( result );

										} catch( Exception nul ) {
											if( Debug ) {
												System.out.println( "DiagOpen PP ICOM_P (Exception nul) : STEUERN_SET_VIN --> JOB_STATUS <" + temp + ">" );
												nul.printStackTrace();
											}
										} catch( Throwable nul ) {
											if( Debug ) {
												System.out.println( "DiagOpen PP ICOM_P (Throwable nul) : STEUERN_SET_VIN --> JOB_STATUS <" + temp + ">" );
												nul.printStackTrace();
											}
										}

										if( Debug )
											System.out.println( "DiagOpen PP ICOM_P " + iCntRetries + ". apiInitExt i.O. -> apiError: " + Ediabas.apiErrorCode() + " --> " + Ediabas.apiErrorText() );
										bCancel = true;
									} else { //Fehler im apiInitExt
										result = new Ergebnis( "DiagFehler", "ICOM", "", "", "", "IP_ADDRESS", "" + ip, "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "ICOM_P WLAN-IP: " + ip + " apiError: " + Ediabas.apiErrorCode() + " --> " + Ediabas.apiErrorText(), Ergebnis.FT_IGNORE );
										ergListe.add( result );
										if( Debug )
											System.out.println( "DiagOpen PP ICOM_P " + iCntRetries + ". apiInitExt n.i.O. (else) -> apiError: " + Ediabas.apiErrorCode() + " --> " + Ediabas.apiErrorText() );

										//rein hier nur wenn marriage == false; IP-Adresseingabe erzwingen
										if( marriage == false ) {
											//Sichern vorherige IP
											ipSave = ip;
											ip = null;
											if( Debug ) {
												System.out.println( "DiagOpen PP ICOM_P apiInitExt Aufruf fehlgeschlagen apiErrorCode !=0 : " + ipSave );
												System.out.println( "DiagOpen PP ICOM_P zur�cksetzen IP: " + ip );
											}
										}
									}
								} catch( Exception ex1 ) {
									result = new Ergebnis( "DiagFehler", "ICOM", "", "", "", "IP_ADDRESS", "" + ip, "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "ICOM_P WLAN-IP: " + ip + " apiError: " + Ediabas.apiErrorCode() + " --> " + Ediabas.apiErrorText(), Ergebnis.FT_IGNORE );
									ergListe.add( result );

									// Version 98_0_									
									if( Debug )
										System.out.println( "DiagOpen PP ICOM_P Abbruchfenster. Bis jetzt wars nix mit ip: " + ip );
									getPr�flingLaufzeitUmgebung().getUserDialog().setAllowCancel( true );
									getPr�flingLaufzeitUmgebung().getUserDialog().displayStatusMessage( "Warten auf ICOM P-Verbindung\n Waiting for ICOM P-connection\n\n ICOM P IP: " + ip, -1 );
									if( getPr�flingLaufzeitUmgebung().getUserDialog().isCancelled() ) {
										if( Debug )
											System.out.println( "DiagOpen PP pressCancel Warten auf ICOM P-Verbindung apiEnd/ApiInit" );
										bCancel = true;
										ip = null;
										break;
									}
									// Version 98_0_									

									if( Debug ) {
										System.out.println( "DiagOpen PP ICOM_P apiInitExt Aufruf fehlgeschlagen: " + sIcomDriver + ":RemoteHost=" + ip );
										System.out.println( "DiagOpen PP ICOM_P ex1.getMessage() = " + ex1.getMessage() );
										ex1.printStackTrace();
										System.out.println( "DiagOpen PP ICOM_P " + iCntRetries + ". apiInitExt n.i.O. (ex1) -> apiError: " + Ediabas.apiErrorCode() + " --> " + Ediabas.apiErrorText() );
									}

									//rein hier nur wenn marriage == false; IP-Adresseingabe erzwingen
									if( marriage == false ) {
										//Sichern vorherige IP
										ipSave = ip;
										ip = null;
										if( Debug ) {
											System.out.println( "DiagOpen PP ICOM_P apiInitExt Aufruf fehlgeschlagen Exception ex1: " + ipSave );
											System.out.println( "DiagOpen PP ICOM_P zur�cksetzen IP: " + ip );
										}
									}
								}
							}
						}
						// Version 98_0_									
						//Entladen der Statusmeldung
						getPr�flingLaufzeitUmgebung().getUserDialog().reset();
						// Version 98_0_									

					} catch( Exception ex ) {
						if( Debug ) {
							System.out.println( "DiagOpen PP ICOM_P fehlgeschlagen (ex): " + ex.getMessage() );
							ex.printStackTrace();
						}
					}
					// Warum siehe CASCADE-Doku INIT-Parameter	
					if( Ediabas.apiErrorCode() != 0 && marriage == true ) {
						//Sichern vorherige IP
						ipSave = ip;
						ip = null;
						if( Debug ) {
							System.out.println( "DiagOpen PP ICOM_P apiInitExt Aufruf fehlgeschlagen nach Eingabe IP - marriage == true: " + ipSave );
							System.out.println( "DiagOpen PP ICOM_P zur�cksetzen IP: " + ip );
						}
					}
					if( ip == null || ip.equals( "" ) ) {
						// keine IP vom ICOM raus hier..
						if( tempPSV != null ) // Version 109_0_T Ausgabe des AutoassignSchleifenfehlers, falls vorhanden.
						{
							result = new Ergebnis( "DiagFehler", "ICOM P", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), " AUTOASSIGN_LOOP_ERROR: " + tempPSV, Ergebnis.FT_NIO_SYS );
							ergListe.add( result );

							if( ipVerifyVin != null ) {
								result = new Ergebnis( "DiagFehler", "ICOM P", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "IP from XML: " + ipVerifyVin, Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
							} else {
								result = new Ergebnis( "DiagFehler", "ICOM P", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "IP from XML: " + ipVerifyVin2, Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
							}
							tempPSV = null;
						}
						if( ipSave != null ) // keine Ausgabe des Ediabasfehlers im Falle keiner IP-Adresseingabe
						{
							result = new Ergebnis( "DiagFehler", "ICOM P", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "IP via Dialog: " + ipSave + " --> " + Ediabas.apiErrorCode() + ": " + Ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
						} else {
							result = new Ergebnis( "DiagFehler", "ICOM P", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "IP via Dialog: " + ipSave, Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
						}
						throw new PPExecutionException();

					}

					if( ip != null && bIcomWriteRemoteHost == true ) {
						// JuHu, wir haben eine IP Adresse..
						// String ediabasConfig = Ediabas.apiGetConfig(
						// "Interface" ).trim();
						String ediabasConfig = sIcomDriver.toString();
						String ediabasBinPath = "";

						// ediabas pfad bestimmen
						ediabasBinPath = "C:\\EC-APPS\\Ediabas\\bin";
						if( !new File( ediabasBinPath ).exists() ) {
							ediabasBinPath = "C:\\Ediabas\\bin";
							if( !new File( ediabasBinPath ).exists() )
								throw new FileNotFoundException( "Ediabas-Pfad wurde nicht gefunden" );
						}

						if( ediabasConfig.equals( "RPLUS" ) || ediabasConfig.equals( "PROXY:RPLUS" ) ) {
							// ohne RPLUS.INI
							if( Debug )
								System.out.println( "DiagOpen PP ICOM_P EDIABAS.INI Section: [TCP]" );
							this.writeToIni( ediabasBinPath + File.separator + "Ediabas.ini", "", "REMOTEHOST", ip );
						} else { // mit RPLUS.INI

							if( ediabasConfig.indexOf( "PROXY" ) < 0 ) {
								// RPLUS:.....
								String sSection = ediabasConfig.substring( 6, ediabasConfig.length() );

								if( Debug )
									System.out.println( "DiagOpen PP ICOM_P Section RPLUS.INI: " + "[" + sSection + "]" );
								this.writeToIni( ediabasBinPath + File.separator + "rplus.ini", sSection, "REMOTEHOST", ip );
							} else {
								// PROXY:RPLUS:.....
								String sSection = ediabasConfig.substring( 12, ediabasConfig.length() );

								if( Debug )
									System.out.println( "DiagOpen PP ICOM_P Section (PROXY-Mode) RPLUS.INI: " + "[" + sSection + "]" );
								this.writeToIni( ediabasBinPath + File.separator + "rplus.ini", sSection, "REMOTEHOST", ip );
							}
						}
					}

					//EnetPort f�r ICOM P_Kommunikation mit PSdZ hinzugef�gt
					if( sIcomEnetPort != null && Ediabas.apiErrorCode() == 0 ) {
						ip = ip + ":" + sIcomEnetPort;
						if( Debug )
							System.out.println( "DiagOpen PP ICOM_P Section add sIcomEnetPort to IP: " + ip );
					}
					// IP Adresse des ICOMs merken
					result = new Ergebnis( "IP_ADDRESS", "ICOM", "", "", "", "IP_ADDRESS", "" + ip, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
					bCancel = true;
				}

				try { //B2V

					// Device B2V konfiguriert ?
					try {
						myB2VHandler = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getB2V();
						myB2VHandler.requestAvailableVehicles();
					} catch( Exception exp ) {
						// Wenn nicht konfiguriert oder Probleme, dann k�nnen wir mit B2V halt nicht's tun..
					}
					String ediabasConfig = Ediabas.apiGetConfig( "Interface" ).trim();
					if( myB2VHandler != null && (ediabasConfig.equalsIgnoreCase( "ENET" ) || ediabasConfig.equalsIgnoreCase( "PROXY:ENET" )) ) {
						// TODO Parameter wird auf "RemoteHost" mit dem n�chsten Ediabas ge�ndert 

						// Alternative 1, Umschaltung der IP-Adresse �ber apiInitExt(...)
						Ediabas.apiInitExtExt( ediabasConfig, "", "", "IfhRemoteHost=" + myB2VHandler.getMVinIdoIdVin().get( this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7() ) );
						if( Debug )
							System.out.println( "DiagOpen PP : (B2V) : apiInitExtExt( \"" + ediabasConfig + "\" , \"\" , \"\" , \"IfhRemoteHost=" + myB2VHandler.getMVinIdoIdVin().get( this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7() ) + "\" )" );

						// IP Adresse des Fahrzeuges i.d.R. die IP des ZGWs im virtuellen Fzg. merken
						result = new Ergebnis( "IP_ADDRESS", "B2V", "", "", "", "IP_ADDRESS", "" + myB2VHandler.getMVinIdoIdVin().get( this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7() ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}

				} catch( NoClassDefFoundError e ) {
					result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Exception: EDIABAS DLL", "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
				} catch( Exception eex ) {
					result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Exception: EDIABAS DLL", "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					if( Debug )
						eex.printStackTrace();
					if( Debug )
						System.out.println( "DiagOpen PP : Exception : ErrorCode = >" + Ediabas.apiErrorCode() + "< : ErrorText = >" + Ediabas.apiErrorText() + "<" );
				}

				// Simulationsmodus???
				if( (Ediabas.apiGetConfig( "Simulation" )).equals( "0" ) == false ) {
					if( Debug )
						System.out.println( "DiagOpen PP Simulationsmodus aktiviert" );
					if( strFensterName == null ) {
						key = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputDigital( PB.getString( "Diagnose" ), PB.getString( "diagnoseInSimulation" ) + "! " + PB.getString( "abbruchDurchWerker" ), messageTimeout );
					} else {
						key = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputDigital( strFensterName, PB.getString( "diagnoseInSimulation" ) + "! " + PB.getString( "abbruchDurchWerker" ), messageTimeout );
					}
					udInUse = true;
					if( key != UserDialog.NO_KEY ) {
						result = new Ergebnis( "Simulation", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnoseInSimulation" ), PB.getString( "abbruchDurchWerker" ), Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					}
				}

				// M�ssen Traces aktiviert werden?
				try {
					// API-Trace?
					DeviceInfo apiTrace = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabasApiTrace();
					try {
						//Port-Konfiguration auslesen, das erste Element ist immer das TraceLevel
						String traceLevel = "";
						String traceConfiguration = apiTrace.getPort();
						StringTokenizer st = new StringTokenizer( traceConfiguration, ";" );
						ArrayList configurationArray = new ArrayList( st.countTokens() );
						while( st.countTokens() > 0 ) {
							configurationArray.add( st.nextElement() );
						}
						if( !(configurationArray.size() == 0) ) {
							traceLevel = (String) configurationArray.get( 0 );
						}

						Ediabas.apiSetConfig( "ApiTrace", traceLevel );
						if( extern == false ) {
							File traceFile = new File( Ediabas.apiGetConfig( "TracePath" ), "api.trc" );
							if( traceFile.exists() )
								traceFile.delete();
							if( Debug )
								System.out.println( "DiagOpen PP ApiTrace aktiviert" );
						}
					} catch( Exception acfe ) {
						CascadeLogging.getLogger().log( LogLevel.WARNING, getPr�fling().getAuftrag().getFahrgestellnummer7() + " Exception API-Trace Level " + apiTrace.getPort() + ": ", acfe );
					}
				} catch( Exception e ) {
					// keine Fehlerbehandlung
				} catch( Throwable t ) {
					// keine Fehlerbehandlung
				}
				try {
					// IFH-Trace?
					DeviceInfo ifhTrace = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabasIfhTrace();
					try {
						//Port-Konfiguration auslesen, das erste Element ist immer das TraceLevel
						String traceLevel = "";
						String traceConfiguration = ifhTrace.getPort();
						StringTokenizer st = new StringTokenizer( traceConfiguration, ";" );
						ArrayList configurationArray = new ArrayList( st.countTokens() );
						while( st.countTokens() > 0 ) {
							configurationArray.add( st.nextElement() );
						}
						if( !(configurationArray.size() == 0) ) {
							traceLevel = (String) configurationArray.get( 0 );
						}
						Ediabas.apiSetConfig( "IfhTrace", traceLevel );
						if( extern == false ) {
							File traceFile = new File( Ediabas.apiGetConfig( "TracePath" ), "ifh.trc" );
							if( traceFile.exists() )
								traceFile.delete();
							if( Debug )
								System.out.println( "DiagOpen PP IfhTrace aktiviert" );
						}
					} catch( Exception acfe ) {
						CascadeLogging.getLogger().log( LogLevel.WARNING, getPr�fling().getAuftrag().getFahrgestellnummer7() + " Exception IFH-Trace Level " + ifhTrace.getPort() + ": ", acfe );
					}
				} catch( Exception e ) {
					// keine Fehlerbehandlung
				} catch( Throwable t ) {
					// keine Fehlerbehandlung
				}

				// Interface-Bestimmung
				temp = Ediabas.executeDiagJob( "UTILITY", "INTERFACE", "", "TYP" );

				if( Debug )
					System.out.println( "DiagOpen PP Interface-Bestimmung JobStatus: " + temp );

				if( temp.equals( "OKAY" ) == false ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", "UTILITY", "INTERFACE", "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );

					ergListe.add( result );
					throw new PPExecutionException();
				}
				temp = Ediabas.getDiagResultValue( "TYP" ).toUpperCase();

				if( Debug )
					System.out.println( "DiagOpen PP Interface: " + temp );

				result = new Ergebnis( "Interface", "EDIABAS", "UTILITY", "INTERFACE", "", "TYP", temp, temp, "", "0", "", "", "", "", "", "K" );
				ergListe.add( result );
			} catch( ApiCallFailedException e ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", "UTILITY", "INTERFACE", "", "TYP", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), Ediabas.apiErrorCode() + ": " + Ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", "UTILITY", "INTERFACE", "", "TYP", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", "UTILITY", "INTERFACE", "", "TYP", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Speichere den Wert des Parameters "LABEL" vom am Pr�fstand 
			// selektierten Interface als Ergebnis. Wenn kein Interface 
			// ausgew�hlt wurde, wird stattdessen der Inhalt der Pr�fstand-
			// variablen USE_INTERFACE benutzt. Falls diese am Pr�fstand nicht 
			// existiert, wird KEIN Ergebnis erzeugt. Grund f�r das Schreiben: 
			// Es soll m�glich sein, innerhalb von Pr�fumf�ngen eine Interface-
			// abh�ngige Sonderbehandlung durchzuf�hren, welche Multi-Instanz-
			// f�hig ist. Auf Grundlage des erzeugten Ergebnisses k�nnte z. B. 
			// mittels einer Pr�fprozedur (wie DiagData) ein Soll-Ist-Vergleich 
			// durchgef�hrt werden. 
			try { //try-catch n�tig, damit die PP auch mit altem CASCADE funktioniert
				String label = null; //Label der aktuell am Pr�fstand selektierten Interface-Konfiguration

				String configName = Pruefstand.instance.getInterfaceConfigurationSelection( Pruefstand.instance.getScreenNumber() ); //beziehe den Namen der ausgew�hlten Interface-Konfiguration
				if( configName != null ) {
					HashMap config = Pruefstand.instance.getInterfaceConfiguration( configName ); //beziehe die Parameter der Interface-Konfiguration
					if( config != null )
						label = (String) config.get( "LABEL" ); //suche nach dem Label, das die Interface-Konfiguration tr�gt
				}

				if( label == null ) //wenn kein Label gefunden, dann Pr�fstandvariable USE_INTERFACE heranziehen
					try {
						label = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "USE_INTERFACE" ).toString();
					} catch( VariablesException x ) {
					}

				if( label != null ) { //nur schreiben, wenn etwas gefunden wurde
					result = new Ergebnis( "USE_INTERFACE", "Test Screen", "", "", "", "USE_INTERFACE", label, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				}
			} catch( Throwable x ) {
			}

			if( (temp.equals( "FUNK" ) == true) && (extern == false) ) {
				// Statistik Zaehler im Master l�schen
				try {
					temp = Ediabas.executeDiagJob( "IFR", "LOESCHEN_STATISTIK_MASTER", "", "" );

					if( Debug )
						System.out.println( "DiagOpen PP JOB LOESCHEN_STATISTIK_MASTER: " + temp );

					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "LOESCHEN_STATISTIK_MASTER", "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_IGNORE );
						ergListe.add( result );
					}
				} catch( ApiCallFailedException e ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "LOESCHEN_STATISTIK_MASTER", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), Ediabas.apiErrorCode() + ": " + Ediabas.apiErrorText(), Ergebnis.FT_IGNORE );
					ergListe.add( result );
				} catch( Exception e ) {
					CascadeLogging.getLogger().log( LogLevel.WARNING, "Funkverhau nicht in Ordnung!!! / Wrong version for radio adaptor software!!!", e );
				} catch( Throwable t ) {
					CascadeLogging.getLogger().log( LogLevel.WARNING, "Funkverhau nicht in Ordnung!!! / Wrong version for radio adaptor software!!!", t );
				}

				// Wenn eine Verheiratung der Funkadapter gew�nscht wird, dann kann auf die
				// Initialisierung verzichtet werden, es wird direkt zur Eingabemaske f�r die
				// MDA-Nummer gesprungen
				if( marriage == false )
					initOK = true;
				else
					initOK = false;

				startOK = false;

				if( Debug )
					System.out.println( "DiagOpen PP (Initialisierung) initOK: " + initOK );

				do {
					if( initOK == true ) {
						// Aufbau der Funkverbindung
						try {
							// Alte Welt mit STARTE_PR�FUNG
							if( messageTimeout3 == 0 ) {
								temp = Ediabas.executeDiagJob( "IFR", "START_PRUEFUNG", getArg( "FZS" ), "JOB_STATUS" );
								if( Debug )
									System.out.println( "DiagOpen PP if( initOK == true ) --> JOB START_PRUEFUNG: " + temp + " FGNR: " + getArg( "FZS" ) );

								if( temp.equals( "OKAY" ) == true )
									startOK = true;

								if( Debug )
									System.out.println( "DiagOpen PP if( initOK == true ) --> SET startOK: " + startOK );
							}
							//Regensburg (STARTE_PRUEFUNG bis Abbruch)
							else {
								int repeatCounter = 0;

								getPr�flingLaufzeitUmgebung().getUserDialog().setAllowCancel( true );

								do {
									temp = Ediabas.executeDiagJob( "IFR", "START_PRUEFUNG", getArg( "FZS" ), "JOB_STATUS" );
									if( Debug )
										System.out.println( "DiagOpen PP if( initOK == true ) --> JOB START_PRUEFUNG: " + temp + " FGNR: " + getArg( "FZS" ) );

									if( temp.equals( "OKAY" ) == true ) {
										startOK = true;
										repeatCounter = messageTimeout3;
									} else {
										if( strFensterName == null ) {

											getPr�flingLaufzeitUmgebung().getUserDialog().displayAlertMessage( PB.getString( "anweisung" ), fawt, -1 );

										} else {
											getPr�flingLaufzeitUmgebung().getUserDialog().displayAlertMessage( strFensterName, fawt, -1 );

										}
										deltatime = 4000;
										timestamp = System.currentTimeMillis() + deltatime;
										while( deltatime > 0 ) {
											try {
												Thread.sleep( deltatime );
											} catch( InterruptedException e ) {
											}
											deltatime = timestamp - System.currentTimeMillis();
										}

										repeatCounter++;
									}
									if( Debug )
										System.out.println( "DiagOpen PP if( initOK == true ) --> SET startOK: " + startOK );

									if( getPr�flingLaufzeitUmgebung().getUserDialog().isCancelled() ) {
										if( Debug )
											System.out.println( "DiagOpen PP pressCancel FUNK-Verbindung" );
										break;
									}

								} while( repeatCounter < messageTimeout3 );

								//Entladen der Statusmeldung
								getPr�flingLaufzeitUmgebung().getUserDialog().dispose();

							} //ende else

						} catch( ApiCallFailedException e ) {
							startOK = false;
						} catch( EdiabasResultNotFoundException e ) {
							startOK = false;
						}
					}
					if( startOK == false ) {
						initOK = false;
						// Versuche erstmal, die Verbindungsart automatisch zu bestimmen -
						// allerdings nur einmal!
						// Wenn Verheiratung gew�nscht wird, ist das �berfl�ssig
						if( (marriage == false) && (autoInitTried == false) ) {
							autoInitTried = true;
							int repeatCounter = 0;
							try {
								do {
									temp = Ediabas.executeDiagJob( "IFR", "STATUS_SLAVE_ZIELNUMMER", "", "" );
									if( Debug )
										System.out.println( "DiagOpen PP if( startOK == false ) --> JOB STATUS_SLAVE_ZIELNUMMER: " + temp );
									repeatCounter++;
									if( temp.equals( "OKAY" ) == true ) {
										mdaNummer = Ediabas.getDiagResultValue( "STAT_SLAVE_ZIELNUMMER" );
										if( Debug )
											System.out.println( "DiagOpen PP if( startOK == false ) --> STAT_SLAVE_ZIELNUMMER: " + mdaNummer );
										result = new Ergebnis( "Diagnose", "EDIABAS", "IFR", "STATUS_SLAVE_ZIELNUMMER", "", "STAT_SLAVE_ZIELNUMMER", mdaNummer, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
										ergListe.add( result );
										if( mdaNummer.equalsIgnoreCase( "0" ) == false ) {
											// Punkt-zu-Punkt verbindung, somit OK da IFR-Jobs
											// komplett �berfl�ssig, aber Funk reagiert sich nicht
											// zum ersten Diagnose Aufruf...
											try {
												temp = Ediabas.executeDiagJob( "UTILITY", "STATUS_ZUENDUNG", "", "JOB_STATUS" );
											} catch( Exception e ) {
												// Fehler ignorieren beim ersten Job!
											}
											startOK = true;
											autoInitSuccessful = true;
										}
										if( mdaNummer.equalsIgnoreCase( "0" ) == true ) {
											if( Debug )
												System.out.println( "DiagOpen PP if( startOK == false ) --> STAT_SLAVE_ZIELNUMMER = 0 --> Abbruch Schleife" );
											break;
										}
									} else {
										if( (repeatCounter < 2) && (startOK == false) )
											if( Debug )
												System.out.println( "DiagOpen PP if( startOK == false ) --> JOB STATUS_SLAVE_ZIELNUMMER trys: " + repeatCounter );
										Thread.sleep( 200 );
									}
								} while( (repeatCounter < 2) && (startOK == false) );
							} catch( Exception e ) {
								result = new Ergebnis( "Exception", "EDIABAS", "IFR", "STATUS_SLAVE_ZIELNUMMER", "", "STAT_SLAVE_ZIELNUMMER", "", "", "", "0", "", "", "", e.getMessage(), "", Ergebnis.FT_IO );
								ergListe.add( result );
								e.printStackTrace();
							}
						}
						if( autoInitSuccessful == false ) {
							// Manueller Ablauf mit Eingabe durch den Benutzer
							try {
								// wenn MDA-Nummer von extern kommt, dann nutze diese ansonsten nehmen wir den alten - manuellen Weg �ber die UserEingabe
								if( strFensterName == null ) {
									mdaNummer = (mdaExt != null) ? mdaExt : getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputNumber( PB.getString( "Diagnose" ), PB.getString( "eingabeDerMdaNummer" ), messageTimeout );
								} else {
									mdaNummer = (mdaExt != null) ? mdaExt : getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputNumber( strFensterName, PB.getString( "eingabeDerMdaNummer" ), messageTimeout );
								}
								if( Debug )
									System.out.println( "DiagOpen PP if( autoInitSuccessful == false ) --> Used MDA-Nummer for assignment: " + mdaNummer );
								result = new Ergebnis( "MDA_NR_INPUT", "EDIABAS", "", "", "", "MDA_NR_INPUT", mdaNummer, "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
								udInUse = true;
								if( mdaNummer == null ) {
									result = new Ergebnis( "MDA", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Autostart Timeout = " + messageTimeout + "s", PB.getString( "abbruchDurchWerker" ), Ergebnis.FT_NIO );
									ergListe.add( result );
									throw new PPExecutionException();
								}
								if( mdaNummer.equals( "" ) == true ) {
									// Abbruch?
									if( strFensterName == null ) {
										key = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputDigital( PB.getString( "Diagnose" ), PB.getString( "abbruchDurchWerker" ), messageTimeout );
									} else {
										key = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputDigital( strFensterName, PB.getString( "abbruchDurchWerker" ), messageTimeout );
									}
									if( key != UserDialog.NO_KEY ) {
										result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "INIT_IFR", getArg( "FZS" ) + ";" + mdaNummer, "", "", "", "", "0", "", "", "", PB.getString( "funkKommunikation" ), PB.getString( "abbruchDurchWerker" ), Ergebnis.FT_NIO );
										ergListe.add( result );
										throw new PPExecutionException();
									}
								} else if( mdaNummer.equals( "0" ) == true ) {
									// Punkt-zu-Punkt verbindung, somit OK da IFR-Jobs komplett
									// �berfl�ssig, aber Funk reagiert sich nicht zum ersten
									// Diagnose Aufruf...
									try {
										temp = Ediabas.executeDiagJob( "UTILITY", "STATUS_ZUENDUNG", "", "JOB_STATUS" );
									} catch( Exception e ) {
										// Fehler ignorieren beim ersten Job!
									}
									startOK = true;

								} else {
									// Verheiratung MDA mit FZS?
									try {
										temp = Ediabas.executeDiagJob( "IFR", "INIT_IFR", getArg( "FZS" ) + ";" + mdaNummer + ";" + automode + ";" + diagmode, "JOB_STATUS" );
										if( Debug )
											System.out.println( "DiagOpen PP if( autoInitSuccessful == false ) --> JOB INIT_IFR: " + temp + " MDA: " + mdaNummer + " FGNR: " + getArg( "FZS" ) );
										if( temp.equals( "OKAY" ) == true ) {
											initOK = true;
											if( Debug )
												System.out.println( "DiagOpen PP if( autoInitSuccessful == false ) --> SET initOK: " + initOK );
										} else if( mdaExt != null ) {
											if( Debug )
												System.out.println( "DiagOpen PP if( autoInitSuccessful == false ) --> Fehler bei externen �bergabe MDA-Nummer: " + mdaExt );
											// falls die externe MDA Nummer nicht funktioniert, dann dokumentieren wir das und werfen eine Exception
											result = new Ergebnis( "MDA", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "MDA Extern = " + mdaExt, PB.getString( "mda" ) + ", " + PB.getString( "parametrierfehler" ), Ergebnis.FT_NIO );
											ergListe.add( result );
											throw new PPExecutionException();
										} else if( stopOnMarriageFailure == true ) {
											if( Debug )
												System.out.println( "keine Verbindung mit dem MDA m�glich, STOP_ON_MARRIAGE_FAILURE = true -> Abbruch " );
											// falls die externe MDA Nummer nicht funktioniert, dann dokumentieren wir das und werfen eine Exception
											leaveWhile = true;
											status = STATUS_EXECUTION_ERROR;
											result = new Ergebnis( "MDA", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "keine Verbindung zu MDA " + mdaNummer, "MDA nicht korrekt gesteckt bzw. falscher MDA", Ergebnis.FT_NIO );
											ergListe.add( result );
										}
									} catch( ApiCallFailedException e ) {
										// Ignore
									} catch( EdiabasResultNotFoundException e ) {
										// Ignore
									}
								}
							} catch( Exception e ) {
								if( e instanceof PPExecutionException )
									throw new PPExecutionException();
								else if( e instanceof DeviceLockedException )
									result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
								else if( e instanceof DeviceNotAvailableException )
									result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								throw new PPExecutionException();
							}
						}
					}
					if( Debug )
						System.out.println( "startOK: " + startOK + " leaveWhile: " + leaveWhile );
				} while( startOK == false && leaveWhile == false );
			} // Ende Funk

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
			if( Debug )
				e.printStackTrace();
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			if( Debug )
				e.printStackTrace();
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			if( Debug )
				e.printStackTrace();
		}

		// Alarmmeldung
		if( status != STATUS_EXECUTION_OK ) {
			String awt = PB.getString( "Diagnose" ) + " " + PB.getString( "nichtVerfuegbar" );
			try {
				if( udInUse == true )
					getPr�flingLaufzeitUmgebung().releaseUserDialog();
				udInUse = false;
				if( strFensterName == null ) {
					getPr�flingLaufzeitUmgebung().getUserDialog().requestAlertMessage( PB.getString( "Diagnose" ), awt, messageTimeout2 );
				} else {
					getPr�flingLaufzeitUmgebung().getUserDialog().requestAlertMessage( strFensterName, awt, messageTimeout2 );
				}
				udInUse = true;
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
				udInUse = false;
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
			}
		}

		// Freigabe der benutzten Devices
		if( udInUse == true ) {
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
		if( myB2VHandler != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseB2V();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "B2V", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "B2V", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		// Status setzen
		setPPStatus( info, status, ergListe );

	}

	private void sleep( int i ) {
		// TODO Auto-generated method stub

	}

	/******************************************************
	 * /* AB HIER PRIVATE METHODEN * /
	 ******************************************************/

	private String getIcomIPFromFiles( String strFilename ) {
		return getIcomIPFromFiles( strFilename, null );
	}

	/**
	 * Parallelisiert die Zugriffe auf mehrere Dateien und liefert den ersten g�ltigen Treffer. Die anderen werden ignoriert.
	 * 
	 * @param fileName der Dateiname
	 * @param semicolonSeparatedLookupPathes die per Semicolon separarierten Dateinamen
	 * @return die IP-Adresse als String oder null
	 */
	private String getIcomIPFromFiles( String fileName, String semicolonSeparatedLookupPathes ) {

		if( semicolonSeparatedLookupPathes != null ) {
			ArrayList<String> lookupPathes = new ArrayList<String>();
			StringTokenizer st = new StringTokenizer( semicolonSeparatedLookupPathes, ";" );
			while( st.hasMoreTokens() ) {
				lookupPathes.add( st.nextToken() );
			}
			ExecutorService exSrv = Executors.newFixedThreadPool( lookupPathes.size() );
			ExecutorCompletionService</*String*/HashMap> completionService = new ExecutorCompletionService</*String*/HashMap>( exSrv );
			// Wir schicken alle Anfragen parallel in den CompletionService
			for( String lookupPath : lookupPathes ) {
				completionService.submit( new CheckIPFromFileCallable( new File( lookupPath + File.separator + fileName ) ) );
			}

			// und nehmen jetzt das erste Nicht-Null-Ergebnis, wahlweise mit take() oder poll() mit Timeout
			String ipAdr = null;
			File appliedFile;

			for( int i = 0; i < lookupPathes.size(); i++ ) {
				try {
					Future</*String*/HashMap> result = completionService.poll( 5, TimeUnit.SECONDS );
					//ipAdr = result.get();
					if( result != null ) {
						ipAdr = (String) result.get().get( CheckIPFromFileCallable.KEY_FOR_IP );
						appliedFile = (File) result.get().get( CheckIPFromFileCallable.KEY_FOR_FILE );
						if( ipAdr != null ) {

							if( Debug ) {
								//System.out.println( "DiagOpen PP XML read out from " + (i+1) +". path" );
								System.out.println( "DiagOpen PP XML read out from " + appliedFile );
								System.out.println( "DiagOpen PP getIcomIPFromFile: ICOM IP Adress <" + ipAdr + ">" );
							}
							break;
						}
					}
				} catch( Exception e ) {
					// Sicherheitshalber alle Exceptions bearbeiten (NPE, InterruptedException, ...)
					if( Debug ) {
						e.printStackTrace();
					}
				}
			}
			exSrv.shutdown();
			return ipAdr;
		}
		return null;
	}

	/**
	 * Lese die IP-Adresse aus einem ICOM XML-File
	 * 
	 * @param f
	 *            , das Fileobjekt
	 * @return die IP Adresse aus dem XML-File, null wenn es Probleme gab
	 */
	private String getIcomIPFromFile( File f ) {
		String strIP = null;
		final String strToken = "ip>"; // Wir suchen nach dem Token "ip>", um
		// die IP-Adresse zu finden

		BufferedReader brDatei = null;
		try {
			brDatei = new BufferedReader( new InputStreamReader( new FileInputStream( f ) ) );

			String strTemp = "";
			while( (strTemp = brDatei.readLine()) != null ) {
				StringBuffer sbTemp = new StringBuffer( strTemp );

				// lese die ID, quick + dirty rein, einen XML Parser daf�r
				// herzunehemen ist zu komplex
				int index;

				// die Zeile mit der IP Adresse?
				if( (index = sbTemp.indexOf( strToken )) != -1 ) {
					strIP = sbTemp.substring( index + 3, sbTemp.lastIndexOf( strToken ) - 2 );
					break;
				}

			}
			//brDatei.close();
		} catch( IOException e ) {
			if( Debug )
				e.printStackTrace();
		} finally {
			if( brDatei != null )
				try {
					brDatei.close();
				} catch( IOException e ) {
					if( Debug )
						e.printStackTrace();
				}
		}

		// Debug?
		if( Debug )
			System.out.println( "DiagOpen PP getIcomIPFromFile: ICOM IP Adress <" + strIP + ">" );
		return strIP;

	}

	/**
	 * Schreibt eine Key/Value Kombination in eine Ini-Datei.
	 * 
	 * @param aFileName
	 * @param aSectionName
	 * @param aKeyName
	 * @param aValue
	 * @return Erfolgsmeldung
	 */
	@SuppressWarnings("resource")
	private boolean writeToIni( String aFileName, String aSectionName, String aKeyName, String aValue ) {
		boolean ret = false;
		String line = null;
		boolean bSectionFound = false;
		boolean bKeyFound = false;

		// StringBuilder sb = null;
		StringBuffer sb = null;
		java.io.BufferedReader dis = null;// .DataInputStream dis;

		try {
			// ini einlesen
			// dis = new DataInputStream(new FileInputStream(aFileName));
			// //fileinput
			FileReader fr = new FileReader( aFileName );
			dis = new BufferedReader( fr );
			// sb = new StringBuilder(); //buffer
			sb = new StringBuffer();

			// section vorbereiten
			// wenn keine Section angegeben, dann auch nicht erst suchen...
			if( aSectionName == null || aSectionName.equals( "" ) ) {
				bSectionFound = true;
			} else {
				aSectionName = aSectionName.trim();
				if( !aSectionName.startsWith( "[" ) )
					aSectionName = "[" + aSectionName + "]";
			}

			while( (line = dis.readLine()) != null ) {
				line = line.trim();

				// wenn schon mal die richtige section gefunden wurde und die
				// n�chste section kommt: fehler!
				if( !bKeyFound && bSectionFound && line.startsWith( "[" ) )
					throw new Exception( "nicht gefunden!" );

				if( !bKeyFound && !bSectionFound && line.equalsIgnoreCase( aSectionName ) ) {
					bSectionFound = true;
				}

				if( !bKeyFound && bSectionFound && line.toLowerCase().startsWith( aKeyName.toLowerCase() ) ) {
					bKeyFound = true;
					// gefunden: den wert gleich �ndern
					sb.append( aKeyName + " = " + aValue + "\r\n" );
				} else {
					sb.append( line );
					sb.append( "\r\n" );
				}
			}
			fr.close();
			dis.close();

			// wurde der eintrag nicht gefunden --> fehler!
			if( !bKeyFound )
				return false;

			// ini schreiben
			java.io.FileWriter fos = new FileWriter( aFileName );
			fos.write( sb.toString() );
			fos.flush();
			fos.close();

		} catch( Exception e ) {
			ret = false;
		}

		return ret;

	}

	/**
	 * �berpr�ft einen Textstring auf IP-Adressformat. <br>
	 * 'x.x.x.x' x='0'..'255' <br>
	 *
	 * @author Drexel
	 * @version 1_0_0 30.11.2011 Erstellung
	 */
	private boolean isValidIP( String checkIP ) {
		int i = 0;
		int n = 0;
		String[] parts = null;

		if( checkIP == null )
			return false;

		parts = checkIP.split( "\\." );
		if( parts.length != 4 )
			return false;
		if( checkIP.charAt( checkIP.length() - 1 ) == '.' )
			return false;

		for( i = 0; i < parts.length; i++ ) {
			try {
				n = Integer.parseInt( parts[i] );
				if( (n < 0) || (n > 255) )
					return false;
				if( !parts[i].equalsIgnoreCase( String.valueOf( n ) ) )
					return false;
			} catch( Exception e ) {
				return false;
			}
		}
		return true;
	}

	/** 
	 * Kapselt die gleichnamige Funktion der Klasse DeviceManager und erg�nzt
	 * eigene Debugausgaben.
	 */
	public String overrideInterfaceParameter( String paramName, String defaultValue ) {
		String result = defaultValue;

		try { //try n�tig, damit die PP auch mit altem CASCADE funktioniert
			result = getPr�flingLaufzeitUmgebung().getDeviceManager().overrideInterfaceParameter( paramName, defaultValue );

			if( Debug )
				if( result != null && !result.equals( defaultValue ) )
					System.out.println( "DiagOpen PP Bevorzuge Pr�fstandeinstellungen und �ndere Interface-Parameter " + paramName + " von " + defaultValue + " in " + result );
		} catch( Throwable e ) {
		}

		return result; //nichts gefunden, nehme Default
	}

	/**
	 * 
	 * Die Klasse CheckIPFromFileCallable. Erm�glicht die Parallelisierung von Dateizugriffen.
	 * 
	 * @author Volker Funke, BRAINBOW EDV-Beratung GmbH
	 *
	 */
	private class CheckIPFromFileCallable implements Callable</*String*/HashMap> {
		public final static String KEY_FOR_IP = "ip";
		public final static String KEY_FOR_FILE = "file";

		private File file;

		public CheckIPFromFileCallable( File file ) {
			this.file = file;
		}

		/*
		public String call() throws Exception {
			return getIcomIPFromFile();
		}
		*/
		public HashMap call() throws Exception {
			HashMap map = new HashMap();
			map.put( KEY_FOR_IP, getIcomIPFromFile() );
			map.put( KEY_FOR_FILE, file );
			return map;

		}

		/**
		 * Lese die IP-Adresse aus einem ICOM XML-File
		 * 
		 * @param f das Fileobjekt
		 * @return die IP Adresse aus dem XML-File, null wenn es Probleme gab
		 */
		private String getIcomIPFromFile() {
			final String token = "ip>"; // Wir suchen nach dem Token "ip>", um die IP-Adresse zu finden

			if( file.exists() && file.canRead() ) {
				String ipAddress = null;
				try {
					BufferedReader reader = new BufferedReader( new FileReader( file ) );

					String tmpString;
					while( (tmpString = reader.readLine()) != null ) {

						// lese die ID, quick + dirty rein, einen XML Parser daf�r
						// herzunehemen ist zu komplex
						int index;

						// die Zeile mit der IP Adresse?
						if( (index = tmpString.indexOf( token )) != -1 ) {
							ipAddress = tmpString.substring( index + 3, tmpString.lastIndexOf( token ) - 2 );
							break;
						}
					}
					reader.close();
				} catch( IOException e ) {
					if( Debug ) {
						e.printStackTrace();
					}
				}

				return ipAddress;
			} else {
				return null;
			}
		}
	}

}

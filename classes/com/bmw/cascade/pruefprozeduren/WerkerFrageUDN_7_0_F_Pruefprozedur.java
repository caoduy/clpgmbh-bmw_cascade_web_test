package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.framework.udnext.UDNButtonSection.UDNButton;
import com.bmw.cascade.pruefstand.framework.udnext.UDNHandle;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.ImageLocation;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.UDNButtonConfiguration;
import com.bmw.cascade.pruefstand.framework.udnext.UserDialogNextRuntimeEnvironment;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.UDNState;

/**
 * Implementierung der Pr�fprozedur, die f�r eine vorgegeben Zeitdauer eine Frage an den Werker ausgibt,
 * die dieser mit OK zu beantworten hat. Nach Ablauf der Zeit wird automatisch ein Nicht-OK angenommen.
 * @author Winkler
 * @version Implementierung
 * @version 0_0_3 27.01.2004  NM  Zusaetzlicher optionaler Parameter TITEL hinzu<BR>
 * @version 0_0_4 09.08.2004  AM  Eintrag der Antwort in das Ergebnisobjekt als String (YES/NO), (bisher nur als Zahl)<BR>
 * @version 0_0_5 04.02.2005  Konstante FehlerText hinzu, damit dem Ergebnis-Objekt im FT_NIO-Fall immer ein Fehlertext als Parameter uebergeben wird.<BR>
 * @version 0_2_5 21.02.2005  Gehring  Konstanten FehlerText durch PB.getString("werkerfrageAbbruch") ersetzt.<BR>
 * @version 0_2_6 24.03.2005  Gehring  FA - Version der V0_2_5_TA - Version <BR>
 *          2_0 T 27.05.2013  TB NullPointerException bei fehlenden HWT <BR>
 *          3_0 T 27.05.2013  TB F-Version <BR>
 * @version 5_0_F 24.03.2005  Erweiterung um oprionalen Parameter INVERT <BR>
 * @version 6_0_T 24.03.2016  TB LOP 2124: UserButton Auswahl im virt. Fzg. dokumentieren, Generics <BR>
 * @version 7_0_F 24.03.2016  TB F-Version <BR>
 */
public class WerkerFrageUDN_7_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * Default-Konstruktor, nur fuer die Deserialisierung.
	 */
	public WerkerFrageUDN_7_0_F_Pruefprozedur() {
	}

	/**
	 * Erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh�rigen Pr�flings.
	 * @param pruefprozName Name der Pr�fprozedur.
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit.
	 */
	public WerkerFrageUDN_7_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Argumente.
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den Arguementen.
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "TITEL", "HWT", "IMAGE", "IMAGE_LOCATION" , "INVERT" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den Arguementen.
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "DAUER", "FT" };
		return args;
	}

	/**
	 * Pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>True</code> bei positiver Pr�fung, andernfalls <code>false</code>.
	 */
	@Override
	public boolean checkArgs() {
		try {
			if( super.checkArgs() )
				return !((Long.parseLong( extractValues( getArg( getRequiredArgs()[0] ) )[0] )) < 0); //Werte kleiner 0 nicht zul�ssig (0 hingegen bedeutet kein Warten)
			else
				return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// Immer notwendig.
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// Spezifische Variablen.
		String title;
		String ft;
		String hwt;
		String image;
		String imageLocation;
		int timeout; //in Sekunden
		UserDialogNextRuntimeEnvironment dlu = null;
		UDNHandle handle = null;
		boolean invert = false;

		/***********************************************
		* Label in Transientdatei schreiben, die folgende Zeile ist auch notig
		* import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		***********************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			//			e.printStackTrace();
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		try {
			// Parameter holen.
			try {
				if( !checkArgs() )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// Timeout aus Kompatibilit�t in Millisekunden, welche intern in Sekunden konvertiert werden.
				try {
					timeout = Integer.parseInt( extractValues( getArg( getRequiredArgs()[0] ) )[0] ) / 1000; //bei 0 wird nicht gewartet (Timeout inaktiv)
				} catch( NumberFormatException e ) {
					//					e.printStackTrace();
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				}

				// Fragetext.
				ft = getArg( getRequiredArgs()[1] );
				if( ft.indexOf( '@' ) != -1 ) {
					String[] fts = extractValues( ft );
					ft = "";
					for( int i = 0; i < fts.length; i++ )
						ft = ft + fts[i] + " ";
				} else
					ft = PB.getString( ft );

				// Titel.
				title = getArg( getOptionalArgs()[0] );
				if( title == null )
					title = PB.getString( "frage" );

				// HWT.
				if( getArg( "HWT" ) != null ) {
					if( getArg( "HWT" ).contains( "@" ) ) {
						hwt = PB.getString( extractValues( "HWT" )[0] );
					} else {
						hwt = getArg( "HWT" );
					}
				} else {
					hwt = "";
				}
				
				//INVERT
				if( getArg( "INVERT" ) != null ) {
					if( getArg( "INVERT" ).equalsIgnoreCase( "TRUE" ) ) {
						invert = true;
					} else {
						invert = false;
					}
				} else {
					invert = false;
				}

				// Image. Dies ist das Bild, welches der Dialog anzeigen soll (falls gew�nscht).
				image = getArg( getOptionalArgs()[2] ); //kann auch null sein, dann kein Bild

				// Position des Bildes in Bezug zum Meldungstext.
				imageLocation = getArg( getOptionalArgs()[3] ); //kann auch null sein, dann existiert die Angabe nicht und der Default wird angenommen

			} catch( PPExecutionException e ) {
				//				e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Dialog anzeigen und Antwort auswerten.
			try {
				dlu = getPr�flingLaufzeitUmgebung().getUserDialogNext();
				handle = dlu.allocateUserDialogNext();

				if( handle != null ) {
					UDNButton answer;

					// Anzeigen.
					if( image != null )
						answer = dlu.displayMessage( handle, UDNState.MESSAGE, image, toImageLocation( imageLocation ), title, ft, timeout, UDNButtonConfiguration.YES_NO_BUTTON );
					else
						answer = dlu.displayMessage( handle, UDNState.MESSAGE, title, ft, timeout, UDNButtonConfiguration.YES_NO_BUTTON );

					// Antwort auswerten.
					if( (answer == UDNButton.YES_BUTTON) && ( invert == false ) ) {
						result = new Ergebnis( title, "Userdialog", "", "", "", "BUTTON", "YES", "YES", "", "0", ft, "YES", "", "", "", Ergebnis.FT_IO );
					}else if( (answer == UDNButton.NO_BUTTON) && ( invert == true ) ){
						result = new Ergebnis( title, "Userdialog", "", "", "", "BUTTON", "YES (INVERT)", "YES (INVERT)", "", "0", ft, "YES (INVERT)", "", "", "", Ergebnis.FT_IO );
					} else {
						result = new Ergebnis( title, "Userdialog", "", "", "", "BUTTON", "NO", "YES", "", "0", ft, "NO", "", PB.getString( "werkerfrageAbbruch" ), hwt, Ergebnis.FT_NIO );
						status = STATUS_EXECUTION_ERROR;
					}
					ergListe.add( result );

					// Freigeben.
					dlu.releaseUserDialogNext( handle ); //!!!																			
				} else
					throw new DeviceNotAvailableException();

			} catch( Exception e ) {
				//				e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException e ) {
			//			e.printStackTrace();
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			//			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			//			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		//Status setzen
		setPPStatus( info, status, ergListe );
	}

	/**
	 * Konvertiert den IMAGE_LOCATION-Parameter der Pr�fprozedur in die 
	 * korrespondierende UDN-ImageLocation Angabe. Bei �bergabe von 
	 * <code>null</code> wird ImageLocation.IMAGE_LEFT als Default zur�ck 
	 * geliefert.
	 *  
	 * @param code IMAGE_LOCATION-Parameter der Pr�fprozedur.  
	 * @return Korrespondierendes UDN-ImageLocation.
	 */
	private ImageLocation toImageLocation( String locationCode ) {
		ImageLocation location;

		if( "R".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_RIGHT;
		else if( "L".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_LEFT;
		else if( "T".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_TOP;
		else if( "B".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_BOTTOM;
		else
			location = ImageLocation.IMAGE_LEFT;

		return location;
	}

}

package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import com.bmw.appframework.logging.*;
import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.mps3.*;
import com.bmw.cascade.util.logging.*;
import com.bmw.cascade.pruefstand.*;


/**
 * Diese Pr�fprozedut initialisiert die Messkarten im Multifunktionspr�fstand. 
 * 
 * @author BMW TI-431 Burger
 * @version V_1_0 16.06.2009 AB Implementierung <br>
 * @version V_2_0 26.06.2009 AB Parameter "Start" hinzugef�gt" <br>
 * @version V_3_0 26.06.2009 AB static finals entfernt <br>
 * @version V_4_0 13.07.2009 AB releaseMPS3Card () ausgef�hrt <br>
 * @version V_5_0 21.07.2009 AB Nachricht senden gefixt <br>
 * @version V_6_0 26.08.2009 AB Deutsche Texte implementiert <br>
 * @version V_7_0 18.09.2009 AB Close card in Close channel ge�ndert
 * 								Parameter f�r Rahmenlose T�re implementiert <br>
 * @version V_8_0 29.09.2009 AB F-Version <br>
 * @version V_9_0 05.10.2009 AB T-Version f�r Oxford (unver�ndert) <br>
 * @version V_10_0 05.10.2009 AB F-Version nach T-Version (unver�ndert) <br>
 * @version V_11_0 16.10.2009 AB Timeouts erh�ht <br>
 * @version V_12_0 09.11.2009 AB Fehlerausgabe beim Socket schliessen eingebaut <br>
 * @version V_13_0 09.11.2009 AB F-Version <br>
 * @version V_14_0_F 19.11.2009 AB Revers-Time f�r Oxford eingebaut <br>
 * @version V_16_0_T 11.07.2011 TM ADD: Parameter-Werte per trim() bearbeitet; CHA: Fehlerausgabe (Messkanal --> Port) <br>
 * @version V_17_0_F 11.10.2011 TM F-Version <br>
 * @version V_18_0_F 24.10.2011 TM CHA: getTrimmedArg() -> Abfrage auf NULL vor trim()-Aufruf hinzugef�gt <br>
 */
public class MPS3WindowManual_18_0_F_Pruefprozedur extends AbstractBMWPruefprozedur 
{
	static final long serialVersionUID = 1L;
	int i_Debug = 0;	

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public MPS3WindowManual_18_0_F_Pruefprozedur() 
	{
		
	}

	private Logger pruefstandLogger = CascadeLogging.getLogger("PruefstandLogger");
	
	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public MPS3WindowManual_18_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) 
	{
		super(pruefling, pruefprozName, hasToBeExecuted);
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() 
	{		
		super.attributeInit();	
	}
	
	/**
	 * liefert die optionalen Argumente
	 *
	 * TIME_DOWN: Maximale Zeit zum Herunterfahren.
	 * TIME_PAUSE: Wartezeit zwischen runter- und hochfahren.
	 * TIME_BLOCK: Wartezeit zwischen Fenster geschlossen und m�glichem Neustart.
	 * TIME_REVERS: Zeit, um die das Fenster nach dem Schliessen wieder ge�ffnet wird (nur Frameless).
	 * FRAMELESS: Gibt an, ob die Routine eine Rahmenlose oder eine "normale" T�re ansteuert.
	 * DEBUG: gibt Debuglevel vor.
	 * 
	 */
	public String[] getOptionalArgs() 
	{
		String[] args = 
		{
			"TIME_DOWN",				
			"TIME_PAUSE",
			"TIME_BLOCK",
			"TIME_REVERS",
			"TIME_REVERS_ESTOP",
			"FRAMELESS",
			"DEBUG"				
	    };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 * 
     * TAG: Bezeichnung der anzusteuernden Messkarte wie in Pr�fstandskonfig.
     * PWM: Tastverh�ltnis zur Ansteuerung FH-Motore.
     * ILIM: Stromgrenze f�r I�T-�berwachung.
     * INVERS: Invertiere Drehrichtung bei True.
     * DIGIN: W�hle Digitaleingang f�r Taster�berwachung (1, 2).
     * TIME_UP: Maximale Zeit zum Hochfahren.
     * 
	 */
	public String[] getRequiredArgs() 
	{
		String[] args = 
		{
			"TAG",
			"PWM",
			"ILIM",
			"INVERS",
			"DIGIN",
			"TIME_UP"
	    };
		
		return args;
	}

	/**
	 * Entfernt �berfl�ssige Leerzeichen (nur am Anfang und am Ende) bei Parameter-Werten
	 */
	private String getTrimmedArg(String arg)
	{
		String tmp = getArg(arg);
		
		if (tmp != null)
			return tmp.trim();
		else
			return tmp;
	}
	
	/**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die parent-Methode aufgrund
     * der offenen Anzahl an Results
	 */	
	public boolean checkArgs() 
	{		
		if (getTrimmedArg("TAG").length() <= 0)
			return false;
		
		if (getTrimmedArg("PWM").length() <= 0)
			return false;

		if (getTrimmedArg("ILIM").length() <= 0)
			return false;

		if (getTrimmedArg("INVERS").length() <= 0)
			return false;

		if (getTrimmedArg("DIGIN").length() <= 0)
			return false;

		if (getTrimmedArg("TIME_UP").length() <= 0)
			return false;

		return true;
    }
	
	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) 
	{	
		//Result vars
		Vector v_ErgListe = new Vector();	
		int i_Status = STATUS_EXECUTION_ERROR;
		
		//Hardware vars
		DeviceManager c_DevMan = this.getPr�flingLaufzeitUmgebung().getDeviceManager();
		CardFensterheber c_Card = null;
		Helper c_Helper = new Helper();
		
		//Message vars
		MsgDefs c_DefMsg = new MsgDefs();	
		TcpClient c_Channel = null;
    	
    	//System vars
		Hashtable ht_Args = getArgs();
		Enumeration en_Keys = ht_Args.keys();		
		String s_Arg = null;        
		String s_Val = null;
		boolean b_NoExecError = true;
		
		//local measuring vars
		String s_Tag = "";
		int i_TimeDown = 0;
		int i_TimeUp = 0;
		int i_TimePause = 0;
		int i_TimeBlock = 0;
		int i_TimeRevers = 0;
		int i_TimeReversEstop = 250;
		int i_PWM = 0;
		int i_Current = 0;
		int i_Revers = 0;
		int i_Digin = 0;
		int i_Frameless = 0;
    	    	   	
		//clear result list
		v_ErgListe.clear();
		//check args
		//checkArgs();
		//init logger
		c_Helper.logInit(i_Debug);	
		
		c_Helper.logDebug(2, "�berpr�fe Parameter", "check parameters", this.getClass());
		
    	if (checkArgs() == false)
    	{
    		c_Helper.logDebug(0, "Mindestens ein zwingender Parameter fehlt.", "At least one required parameter is not supplied.", this.getClass());    		
    		pruefstandLogger.logC( LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one required parameter is not supplied.");
    		if (isDE())
    			i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Pr�fe Konfiguration vom Pr�fling", "Karte (Parameter) erforderlich", "Mindestens ein erforderlicher Parameter wurde nicht �bergeben.", v_ErgListe);	
    		else
    			i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "Card (Parameter) required", "At least one required parameter is not supplied.", v_ErgListe);
    		setPPStatus(info, i_Status, v_ErgListe);
    		return;
    	}
		
    	while (en_Keys.hasMoreElements())
		{
			//get name and value from next parameter
			s_Arg = en_Keys.nextElement().toString();
			//s_Val = getDynamicAttribute(en_Args.nextElement().toString());
			s_Val = getDynamicAttribute(ht_Args.get(s_Arg).toString().trim());//.nextElement().toString());
			
			c_Helper.logDebug(2, "Arg: " + s_Arg + " Val: " + s_Val, "Arg: " + s_Arg + "Val: " + s_Val, this.getClass());
		
			//check parameter "DEBUG"
			if (s_Arg.equalsIgnoreCase("DEBUG"))
			{
				//set to level 0, if no value supplied or set to "false"
				if ((s_Val == null) || (s_Val.equalsIgnoreCase("F")) || (s_Val.equalsIgnoreCase("False")))
					i_Debug = 0;
				else
					//set to level 1, if supplied "true"
					if ((s_Val.equalsIgnoreCase("T")) || (s_Val.equalsIgnoreCase("True")))
						i_Debug = 1;
					else						
					{
						try
						{
							//try to set desired level
							i_Debug = Integer.parseInt(s_Val);
						}
						//catch numberformatexception
						catch (NumberFormatException e)
						{
							i_Debug = 0;
							pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"DEBUG\" has wrong parameter format. Accepts only integer.", e );
						}
					}				
			}

			//Get timeout, if supplied
			if (s_Arg.equalsIgnoreCase("TIME_UP"))
			{
				try
				{
					//try to set desired level
					i_TimeUp = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_TimeUp = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"TIME_UP\" has wrong parameter format. Accepts only integer.", e );
				}	
			}
			
			//Get timeout, if supplied
			if (s_Arg.equalsIgnoreCase("TIME_DOWN"))
			{
				try
				{
					//try to set desired level
					i_TimeDown = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_TimeDown = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"TIME_DOWN\" has wrong parameter format. Accepts only integer.", e );
				}	
			}

			//Get timeout, if supplied
			if (s_Arg.equalsIgnoreCase("TIME_PAUSE"))
			{
				try
				{
					//try to set desired level
					i_TimePause = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_TimePause = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"TIME_PAUSE\" has wrong parameter format. Accepts only integer.", e );
				}	
			}	
			
			if (s_Arg.equalsIgnoreCase("TIME_BLOCK"))
			{
				try
				{
					//try to set desired level
					i_TimeBlock = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_TimePause = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"TIME_BLOCK\" has wrong parameter format. Accepts only integer.", e );
				}	
			}
			
			if (s_Arg.equalsIgnoreCase("TIME_REVERS"))
			{
				try
				{
					//try to set desired level
					i_TimeRevers = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_TimePause = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"TIME_REVERS\" has wrong parameter format. Accepts only integer.", e );
				}	
			}
			
			if (s_Arg.equalsIgnoreCase("TIME_REVERS_ESTOP"))
			{
				try
				{
					//try to set desired level
					i_TimeReversEstop = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_TimePause = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"TIME_REVERS\" has wrong parameter format. Accepts only integer.", e );
				}	
			}				
			
			//Add card to list, if supplied
			if (s_Arg.toUpperCase().startsWith("TAG"))
				s_Tag = s_Val;
						
			//Get timeout, if supplied
			if (s_Arg.equalsIgnoreCase("PWM"))
			{
				try
				{
					//try to set desired level
					i_PWM = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_PWM = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"PWM\" has wrong parameter format. Accepts only integer.", e );
				}	
			}
			
			//Get timeout, if supplied
			if (s_Arg.equalsIgnoreCase("DIGIN"))
			{
				try
				{
					//try to set desired level
					i_Digin = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_Digin = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"DIGIN\" has wrong parameter format. Accepts only integer.", e );
				}	
			}			
			
			//Get timeout, if supplied
			if (s_Arg.equalsIgnoreCase("ILIM"))
			{
				try
				{
					//try to set desired level
					i_Current = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_Current = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"CURRENT\" has wrong parameter format. Accepts only integer.", e );
				}	
			}			

			//String must be MKnm_Vx or MKnm_Vxx
			if (s_Arg.equalsIgnoreCase("INVERS"))
			{
				if ((s_Val.equalsIgnoreCase("T")) || (s_Val.equalsIgnoreCase("TRUE")))
					i_Revers = 1;
				if ((s_Val.equalsIgnoreCase("F")) || (s_Val.equalsIgnoreCase("FALSE")))
					i_Revers = 0;
			}	
			
			//String must be MKnm_Vx or MKnm_Vxx
			if (s_Arg.equalsIgnoreCase("FRAMELESS"))
			{
				if ((s_Val.equalsIgnoreCase("T")) || (s_Val.equalsIgnoreCase("TRUE")))
					i_Frameless = 1;
				if ((s_Val.equalsIgnoreCase("F")) || (s_Val.equalsIgnoreCase("FALSE")))
					i_Frameless = 0;
			}			
		}    	

    	try
    	{
        	//Connect to measuring channel
        	c_Card = (CardFensterheber) c_DevMan.getMPS3Card(s_Tag);    	
    		c_Card.setDebug(i_Debug);
    		c_Card.connectChannel(Integer.toString(IPDefs.FH), "5000");
    		c_Channel = c_Card.getClient(Integer.toString(IPDefs.FH));    		    		
		}
		catch (Exception e)
		{
			if (isDE())
				i_Status = setSystemError(MsgDefs.ERR_CHANNEL, "Pr�fe Konfiguration vom Pr�fstand", "", "Fehler beim �ffnen von Port: " + IPDefs.FH + " auf Karte: " + s_Tag, v_ErgListe);
			else
				i_Status = setSystemError(MsgDefs.ERR_CHANNEL, "Check configuration of pr�fstand", "", "Error while opening port: " + IPDefs.FH + " at card: " + s_Tag, v_ErgListe);
			pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_DEVICE, this.getClass().getName(), "Error while opening port: " + IPDefs.DIGIN + " at card: " + s_Val, "Error while Opening.", e );
			setPPStatus(info, i_Status, v_ErgListe);	
			return;
		}			 	
                
       	int i_Data[] = {MsgDefs.ACT_START, i_PWM, i_Current, i_Revers, i_Digin, i_TimeDown, i_TimeUp, i_TimePause, i_TimeBlock, i_TimeRevers, i_Frameless, i_TimeReversEstop};

       	c_Helper.logDebug(2, "Starte Messung", "Start Measure", this.getClass());
        	            	
       	if (c_Channel != null)
       	{
       		c_Channel.setTxMsg(c_DefMsg.ID_CTRL_FH_MAN, i_Data, null, null);
       		c_Helper.timer(250);
       	}
       	else
            b_NoExecError = false;
				
		if (b_NoExecError)
			i_Status = setMeasureOK(0, 0, 0, v_ErgListe);				
		else
			c_Helper.logDebug(0, "Mit Ausf�hrungsfehler beendet.", "Finished with execution errors.", this.getClass());

		try
		{
			c_Card.closeChannel(Integer.toString(IPDefs.FH), true);			
		}		
		catch (Exception e)
		{
        	c_Helper.logDebug(0, "Fehler beim Schlie�en des Kanals: " + e.getMessage(), "Error while closing channel: " + e.getMessage(), this.getClass());
        	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error ehile closing channel: " + e.getMessage());            	        		        				
		}
		
		//Set Result
		setPPStatus(info, i_Status, v_ErgListe);		
	}

	/**
	 * Setzt das Pr�fungsergebnis auf Systemfehler und generiert entsprechende Eintr�ge.
	 * 
	 * @param errCode 		spezifischer Fehlercode
	 * @param awt 			spezifischer Anweisungstext
	 * @param hwt 			spezifischer Hinweistext
	 * @param errt 			spezifischer Fehlertext
	 * @param ergebnisListe zu beschreibende ErgListe
	 * @return status		status als int
	 */
	private int setSystemError(int errCode, String awt, String hwt, String errt, Vector ergListe)
	{		
		ergListe.add(new  Ergebnis(Integer.toString(errCode), this.getPPName(), "", "", "", "", "", "", "", "", "", "", awt, errt, hwt, Ergebnis.FT_NIO));
		return STATUS_EXECUTION_ERROR;		
	}

	/**
	 * Setzt das Pr�fungsergebnis auf Messung IO und generiert entsprechende Eintr�ge.
	 * 
	 * @param value 		finale(r) Messwert(e)
	 * @param min			Minimalwert
	 * @param max			Maximalwert
	 * @param ergebnisListe zu beschreibende ErgListe
	 * @return status		status als int 
	 */
	private int setMeasureOK(int value, int min, int max, Vector ergListe)
	{
		if (isDE())
			ergListe.add(new Ergebnis("0", this.getPPName(), "", "", "", "Ergebnis IO", Integer.toString(value), Integer.toString(min), Integer.toString(max), "", "", "", "", "", "", Ergebnis.FT_IO));
		else
			ergListe.add(new Ergebnis("0", this.getPPName(), "", "", "", "Result ok", Integer.toString(value), Integer.toString(min), Integer.toString(max), "", "", "", "", "", "", Ergebnis.FT_IO));
		return STATUS_EXECUTION_OK;
	}	

	/**
	 * L�st in @-geklammerte Attribute dynamisch (zur Laufzeit) auf.
	 *  
	 * @param input	input-String
	 * @return		gefundener Wert. input ohne @-Klammerung, dynamisch geholte mit Klammerung.
	 */
	private String getDynamicAttribute(String input)
	{
		String result = input;
		Helper funcs = new Helper();
		
		funcs.logInit(i_Debug);		
		
		funcs.logDebug(2, "Start getDynamicAttribute. . .", "Start getDynamicAttribute . . .", this.getClass());
		if (input.startsWith("@"))
		{
			funcs.logDebug(2, "Erstes Token gefunden", "Found first Token", this.getClass());
			if (input.endsWith("@"))
			{
				funcs.logDebug(2, "Zweites Token gefunden", "Found second Token", this.getClass());
				if (input.length() >= 3)
					try
					{
						funcs.logDebug(2, "L�nge g�ltig. Hole dynamisches Attribut. . .", "Length valid. fetch dynmaic attribute. . .", this.getClass());
						result = (String) getPr�fling().getAllAttributes().get(input.substring(1, input.length() - 1));
						funcs.logDebug(2, "Dynamisches Attribut aufgel�st: " + result, "Resolved dynamic attribute: " + result, this.getClass());
					}
					catch (Exception e)
					{
						//@todo: wenn ein fehler auftritt nicht versuchen mit einem beliebigen text weiterzuarbeiten. sondern exception!
						result = input.substring(0, input.length() - 1);						
						funcs.logDebug(0, "Fehler beim dynamischen Aufl�sen. Es wird zur�ck gegeben: " + result, "Error during dynamic lookup. It will be returned: " + result, this.getClass());						
					}					
				else
				{
					result = null;
					funcs.logDebug(0, "L�nge ung�ltig. Es wird \"null\" zur�ck gegeben.", "Length invalid. \"null\" is returned.", this.getClass());
				}
			}
			else
			{
				result = input.substring(1, input.length());
				funcs.logDebug(0, "Zweites \"@\" nicht gefunden. Es wird zur�ck gegeben: " + result, "Did not find second \"@\". It will be returned: " + result, this.getClass());
			}
		}
		else
			if (input.endsWith("@"))
			{
				result = input.substring(0, input.length() - 1);
				funcs.logDebug(0, "Erstes \"@\" nicht gefunden. Es wird zur�ck gegeben: " + result, "Did not find first \"@\". It will be returned: " + result, this.getClass());
			}				
			else
			{
				funcs.logDebug(2, "Kein \"@\" gefunden. Statisches Attribut. Value: " + result, "No \"@\" found. Static attribute. Value: " + result, this.getClass());
				result = input;
			}
		
		return result;
	}


	/**
     * Liefert true zur�ck, wenn deutsche Texte verwendet werden sollen.
     * @result True bei deutsche texte, false sonst.
     */
    private static boolean isDE()
    {
        try 
        {
            if (CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true)
            	return true;
            else
            	return false;
        } 
        catch (Exception e) 
        {
            return false;   //default is english
        }
    }	
}

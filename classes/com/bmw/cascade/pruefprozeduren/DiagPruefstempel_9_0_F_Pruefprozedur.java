/*
 * DiagPruefstempel_1_0_F_Pruefprozedur.java
 *
 * Created on 19.11.01
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.ediabas.*;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;

/**
 * Implementierung der Pr�fprozedur, die �nderungen (inkrementell oder absolut) des 
 * Pruefstempels durchf�hrt.
 * @author Winkler<br>
 *         Plath, TI-544
 * @version 6_0_F 16.01.2014 UP Fehlerhafte Bezeichnung der Bytes in den APDM-Ergebnissen korrigiert (vorher 3x 'Byte1')
 *                              Ansteuerung des Ediabas-Device �ber DeviceManager eingebaut.
 * @version 7_0_F 10.11.2015 UP Nach einem unerwarteten Laufzeitfehler im Werk 0 wurde die Ausnahme-
 *                              behandlung �berarbeitet.
 * @version 8_0_T 23.01.2017 UP Testversion mit den Inhalten der Version 7_0_F
 * @version 9_0_F 24.01.2017 UP Freigegebene Version mit den Inhalten der Version 8_0_T
 */
public class DiagPruefstempel_9_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagPruefstempel_9_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur, die inkrementelle �nderungen am Pruefstempel vornimmt
	 * oder diesen auf einen absoluten Wert setzt.
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DiagPruefstempel_9_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "BYTE1", "BYTE2", "BYTE3" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "SGBD", "ABSOLUT" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	public boolean checkArgs() {
		int i, j;
		boolean ok;

		try {
			ok = super.checkArgs();
			if( ok == true ) {
				// Check, ob f�r den Parameter ABSOLUT nur true oder false angegeben wurde
				if( (!getArg( getRequiredArgs()[1] ).equalsIgnoreCase( "TRUE" )) && !getArg( getRequiredArgs()[1] ).equalsIgnoreCase( "FALSE" ) )
					return false;
				//Byte1, Byte2 und Byte 3 auf Integer checken (incl. Hex-Darstellung)
				for( i = 0; i < 3; i++ ) {
					try {
						if( getArg( getOptionalArgs()[i] ) != null ) {
							j = getValue( getArg( getOptionalArgs()[i] ) );
							if( (j < 0) || (j > 0xFF) )
								return false;
						}
					} catch( NumberFormatException e ) {
						e.printStackTrace();
						return false;
					}
				}
			}
			return ok;
		} catch( Exception e ) {
			e.printStackTrace();
			return false;
		} catch( Throwable e ) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;
		EdiabasProxyThread ediabas = null;

		// spezifische Variablen
		String temp;
		String sgbd;
		boolean absolut;
		String job = null;
		String jobpara = null;
		int byte1 = 0;
		int byte2 = 0;
		int byte3 = 0;

		try {
			//Parameter holen 
			try {
				if( !checkArgs() )
					throw new PPExecutionException( PB.getString( "parametrierfehler" ) );
				sgbd = extractValues( getArg( getRequiredArgs()[0] ) )[0];
				absolut = getArg( getRequiredArgs()[1] ).equalsIgnoreCase( "true" ) ? true : false;
				if( getArg( getOptionalArgs()[0] ) != null )
					byte1 = getValue( getArg( getOptionalArgs()[0] ) );
				if( getArg( getOptionalArgs()[1] ) != null )
					byte2 = getValue( getArg( getOptionalArgs()[1] ) );
				if( getArg( getOptionalArgs()[2] ) != null )
					byte3 = getValue( getArg( getOptionalArgs()[2] ) );

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			try {
				ediabas = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();
			} catch( DeviceNotAvailableException e ) {
				result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				e.printStackTrace();
				throw new PPExecutionException();
			}
			
			try {
				//Bytes werden abh�ngig von alten Wert geschrieben.
				if( !absolut ) {
					//Pruefstempel muss gelesen werden
					job = "PRUEFSTEMPEL_LESEN";
					jobpara = "";
					temp = ediabas.executeDiagJob( sgbd, job, jobpara, "" );
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					}
					temp = ediabas.getDiagResultValue( "BYTE1" );
					result = new Ergebnis( "Byte1", "EDIABAS", sgbd, job, jobpara, "BYTE1", temp, "0x00", "0xFF", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
					byte1 = byte1 + Integer.parseInt( temp );
					temp = ediabas.getDiagResultValue( "BYTE2" );
					result = new Ergebnis( "Byte2", "EDIABAS", sgbd, job, jobpara, "BYTE2", temp, "0x00", "0xFF", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
					byte2 = byte2 + Integer.parseInt( temp );
					temp = ediabas.getDiagResultValue( "BYTE3" );
					result = new Ergebnis( "Byte3", "EDIABAS", sgbd, job, jobpara, "BYTE3", temp, "0x00", "0xFF", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
					byte3 = byte3 + Integer.parseInt( temp );
				} else {
					//Bytes werden "Absolut" geschrieben, nur wenn die als Parameter existieren, sonst bleiben sie gleich.
					//Pruefstempel muss gelesen werden
					job = "PRUEFSTEMPEL_LESEN";
					jobpara = "";
					temp = ediabas.executeDiagJob( sgbd, job, jobpara, "" );
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					}
					if( getArg( getOptionalArgs()[0] ) == null ) {
						temp = ediabas.getDiagResultValue( "BYTE1" );
						result = new Ergebnis( "Byte1", "EDIABAS", sgbd, job, jobpara, "BYTE1", temp, "0x00", "0xFF", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
						byte1 = Integer.parseInt( temp );
					} else {
						// byte1 kommt von oben, bzw. wird nicht ge�ndert
					}
					if( getArg( getOptionalArgs()[1] ) == null ) {
						temp = ediabas.getDiagResultValue( "BYTE2" );
						result = new Ergebnis( "Byte2", "EDIABAS", sgbd, job, jobpara, "BYTE2", temp, "0x00", "0xFF", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
						byte2 = Integer.parseInt( temp );
					} else {
						// byte2 kommt von oben, bzw. wird nicht ge�ndert
					}
					if( getArg( getOptionalArgs()[2] ) == null ) {
						temp = ediabas.getDiagResultValue( "BYTE3" );
						result = new Ergebnis( "Byte3", "EDIABAS", sgbd, job, jobpara, "BYTE3", temp, "0x00", "0xFF", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
						byte3 = Integer.parseInt( temp );
					} else {
						// byte3 kommt von oben, bzw. wird nicht ge�ndert
					}
				}

				//Um Fehler zu vermeiden...
				if( byte1 < 0 )
					byte1 = 0;
				if( byte2 < 0 )
					byte2 = 0;
				if( byte3 < 0 )
					byte3 = 0;
				if( byte1 > 255 )
					byte1 = 255;
				if( byte2 > 255 )
					byte2 = 255;
				if( byte3 > 255 )
					byte3 = 255;

				//Pruefstempel (byte1, byte2, byte3) schreiben
				job = "PRUEFSTEMPEL_SCHREIBEN";
				jobpara = "" + byte1 + ";" + byte2 + ";" + byte3;
				temp = ediabas.executeDiagJob( sgbd, job, jobpara, "JOB_STATUS" );
				if( temp.equals( "OKAY" ) == false ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} else {
					result = new Ergebnis( "PS_Setzen", "EDIABAS", sgbd, job, jobpara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				}

			} catch( ApiCallFailedException e ) {
				e.printStackTrace();
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException e ) {
			e.printStackTrace();
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		setPPStatus( info, status, ergListe );
	}

	/**
	 * liefert den uebergebenen String als Integer unter Beruecksichtigung
	 * einer moeglichen Hexdarstellung zurueck.
	 * @param in der zu konvertierende String.
	 * @return die Integerzahl.
	 * @throws NumberFormatException bei einem nicht zu konvertierenden String.
	  */
	private int getValue( String in ) throws NumberFormatException {
		if( in.startsWith( "-0x" ) == true )
			return (-Integer.parseInt( in.substring( 3 ), 16 ));
		else if( in.startsWith( "0x" ) == true )
			return Integer.parseInt( in.substring( 2 ), 16 );
		else
			return Integer.parseInt( in );
	}

}

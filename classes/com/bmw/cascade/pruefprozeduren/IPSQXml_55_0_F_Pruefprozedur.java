package com.bmw.cascade.pruefprozeduren;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.bmw.appframework.logging.LogLevel;
import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.Auftrag;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.pruefumfang.PPBlockParallel;
import com.bmw.cascade.pruefstand.pruefumfang.PPBlockSequence;
import com.bmw.cascade.pruefstand.pruefumfang.PPStatement;
import com.bmw.cascade.pruefstand.pruefumfang.PPSwitchStatement;
import com.bmw.cascade.pruefstand.pruefumfang.Pruefumfang;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.util.CascadeDateFormat;
import com.bmw.cascade.util.XmlUtil;
import com.bmw.cascade.util.logging.CascadeLogging;

/**
 * Implementation of the function to write and send IPSQ PU results during a parent PU execution.<BR>
 * Created on 14.11.06 <BR>
 * <BR>
 * 
 * @author P. Jorge (PJ) [IndustrieHansa AG], TI-432, BMW AG<BR>
 * @author U. Plath, TP-432, BMW AG
 * @author C. Schumann (CS) TP-430, BMW AG<br>
 *  			
 * @version 1_0 PJ 17.01.2007 Test version. Builds a IPS-Q format xml PU result file and stores it locally on 
 * 							  the created Cascade IPSQ directory.<BR>
 * 			2_0 PJ 20.01.2007 Added the job SEND_IPSQ_PU_RESULTS to send all the locally stored files to the 
 * 							  specified APDM db URI if a connection is possible.<BR>  
 * 							  Added ADVISE_TEXT as required parameter for the job IPSQ_PU_RESULT.<BR>
 * 			3_0 PJ 10.02.2007 Added TESTSTAND as optional parameter.<BR>
 * 			4_0 PJ 10.02.2007 Added ERROR_TEXT as required parameter for the job IPSQ_PU_RESULT. 
 * 							  Added ADVISE_TEXT & INSTRUCTION_TEXT as optional parameters for the job IPSQ_PU_RESULT.
 * 							  All texts	supplied will be used in a warning message if the built PU result is NOK.<BR>
 * 			6_0 UP/CS 6.2.09  Changed method of detecting errors (get outer block error count), changed method for
 * 							  APDM connection test (now w/o deleting  the file), added optional Argument F_DEFAULT for setting
 * 							  the result to F as default result. Improved the copying of the result files to the APDM Share.
 * 			7_0 CS 10.02.2009 Release IPSQXml_6_0_T
 * 			8_0	CS 11.02.2009 Verify if after the rename-method independently of the return value the resultfile is copied
 * 			9_0 CS 18.02.2009 After copying manually the source file is now deleted.
 * 		   10_0 CS 12.03.2009 apdmTestFile Pfad ge�ndert.
 * 	  11_0+12_0 CS 17.03.2009 Probleme mit resValue Ergebnis: f�r DGF eingebaut, falls die Ergebnisse ungleich sind, wird der
 * 							  resValue vom alten Vorgehen benutzt
 * 		   13_0 CS 24.03.2009 Changed method of detecting errors (get last execution status)
 *         14_0 UP 22.02.2011 Implemented methods to match argument length to IPSQ scheme template (v0119)
 *         15_0 PR 30.05.2011 Parameter APDM_DB_URI is obsolete (the APDM path is read from server property) and will be irgnored from now on (!!)
 *         16_0 PR 21.10.2011 Set to F
 *         17_0 MS 14.03.2012 Changed Standardformat to version 1.23 . Removed FZS and OrderID parameter. Parameter APDM_DB_URI will be used from now on
 *         18_0 MS 20.03.2012 Changed Schemalocation
 *         19_0 CW 08.05.2012 Do not execute this testprocedure if the runmode is "Auto_Retry_on_Error" or "Repeat_Once_on_Error" and it is the first run
 *         					  (background: IPSQ cannot process the same result twice within a short duration) 
 *         20_0 CW 14.05.2012 Do not execute this testprocedure if the runmode is "Auto_Retry_on_Error" or "Repeat_Once_on_Error" and it is the first run
 *         					  (background: IPSQ cannot process the same result twice within a short duration) 
 *         21_0 MS 23.07.2012 Added method to ping the apdmpath
 *         22_0 MS 10.09.2012 Removed Error Status when Server is not reachable
 *         23_0 MS 12.09.2012 improvement of error handling when server not reachable
 *         25_0 MS 11.10.2012 Set and check of Timestamp, IPSQ-Result is only sent on repeat when there is a delta of 14 seconds (IPSQ needs the difference for the same vin)
 *         27_0 MS 11.10.2012 F-Version
 *         28_0 MS 15.04.2013 Delete methode that testprocedure is not executed on "Auto_Retry_on_Error" or "Repeat_Once_on_Error" and it is the first run
 *         29_0 MS 13.05.2013 F-Version
 *         30_0 MS 20.08.2014 Erweiterung das nur das Senden des IPSQ-Merkmal beim gleichen IPSQ-Merkmal unterdr�ckt wird
 *         31_0 MS 27.08.2014 Added new optional Parameter SHOW_MESSAGE, if UserDialog should be displayed
 *         32_0 MS 13.10.2014 F-Version
 *         33_0 MS 15.10.2014 Erweiterung Error Text um "!PU!", enth�lt Error Text dieses Wort, so wird dieses mit dem aktuellen PU-Name aufgel�st
 *         34_0 MS 15.10.2014 F-Version
 *         36_0 MS 11.02.2015 Erweiterung des XML-Results um Teststepname
 *         38_0 MS 21.07.2015 Erweiterung um zus�tzliche Debug Ausgaben
 *         V_39_0_T  FS  13.05.2016  Optionaler APDM_ACCESS_TIMEOUT f�r Dateizugriff hinzugef�gt. Default: 10 Sekunden.<BR>
 *         V_40_0_F  FS  25.05.2016  Produktive Freigabe Version V_39_0_T.<BR> 
 *         V_41_0_T  MK  07.06.2016  Default Timeout auf 5 Sekunden angepasst<BR> 
 *         V_42_0_F  MK  07.06.2016  Produktive Freigabe Version V_41_0_T.<BR> 
 *         V_43_0_T  MK  28.11.2016  Erweiterung der PP um optionale Parameter die Pr�fstandsvariablen im IPSQ Ergebnis anzeigen.<BR> 
 *         V_44_0_F  MK  29.11.2016  Produktive Freigabe Version V_43_0_T.<BR> 
 *         V_45_0_T  MKe 29.11.2016  @Konstrukt f�r errorText, adviseText und instructionText implementiert .<BR> 
 *         V_46_0_T  MKe 07.02.2017  Max. L�nge des Anweisungstextes auf 150 Chars erh�ht<BR> 
 *         V_47_0_F  MKe 07.02.2017  F-Version<BR> 
 *         V_48_0_T  MKe 07.02.2017  deprecated Code ausgetauscht<BR> 
 *         V_49_0_F  MKe 07.02.2017  F-Version<BR>
 *         V_50_0_T  CW  13.06.2017  Zugriff auf Parameter APDM_ACCESS_TIMEOUT angepasst, SoftwareVersion beim APDM Ergebnis abgeschnitten<BR>
 *         V_51_0_F  CW  26.06.2017  F-Version<BR>
 *         V_52_0_T  CW  28.06.2017  Parameter PARALLEL_BLOCKNAME and SEQUENTIAL_BLOCKNAME added to make it possible to use the IPSQXml testprocedure in parallel blocks<BR>
 *         V_53_0_F  CW  28.06.2017  F-Version<BR>
 *         V_54_0_T  CW  04.08.2017  Bugfix NullPointerException 53_0_F-Version<BR>
 *         V_55_0_F  CW  04.08.2017  F-Version<BR>

 */
public class IPSQXml_55_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	static final long DEFAULT_TIMEOUT = 5;
	long timeout = DEFAULT_TIMEOUT;

	// Specific for this procedure
	//----------------------------

	// Number of variables
	private int i_iMaxKeyIndex = 0; // Maximum Varaible_Key.. index found (must not be equal to the number of VARIABLE_KEY's)
	private int i_iMaxValueIndex = 0; // Maximum Varaible_Value.. index found (must not be equal to the number of VARIABLE_VALUES's)

	//parallel block element which has to be determined by the configured variable PARALLEL_BLOCKNAME
	private PPBlockParallel parallelBlock = null;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public IPSQXml_55_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling
	 *            Klasse des zugeh. Prueflings
	 * @param pruefprozName
	 *            Name der Pruefprozedur
	 * @param hasToBeExecuted
	 *            Ausfuehrbedingung f�r Fehlerfreiheit
	 */
	public IPSQXml_55_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "JOB" };
		return args;
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "VARIABLE_KEY1..N", "VARIABLE_VALUE1..N", "APDM_DB_URI", "IPSQ_PU_NAME", "ERROR_TEXT", "ADVISE_TEXT", // Default = "" max length 150
				"INSTRUCTION_TEXT", // Default = "" max length 150
				"TESTSTAND", // Default = Pruefstand IP name
				"F_DEFAULT", // Sets the PP's result to F in any case
				"DEBUG", // Default = FALSE
				"SHOW_MESSAGE", // Default = TRUE
				"APDM_ACCESS_TIMEOUT", // Default = 5 sec
				"PARALLEL_BLOCKNAME", "SEQUENTIAL_BLOCKNAME" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 * �berschreibt die parent-Methode aufgrund der offenen Anzahl an Results
	 * 
	 * @return true, wenn �berpruefung der Argumente erfolgreich
	 */
	public boolean checkArgs() {
		String job = getArg( "JOB" );
		boolean debug = (getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
		try {

			boolean exist = false;
			// 1. Check: Sind alle requiredArgs gesetzt
			String[] requiredArgs = getRequiredArgs();
			for( int i = 0; i < requiredArgs.length; i++ ) {
				if( getArg( requiredArgs[i].toUpperCase() ) == null )
					return false;
				if( getArg( requiredArgs[i].toUpperCase() ).equals( "" ) == true )
					return false;
			}

			// 2. Check: Sind �berfl�ssige Argumente gesetzt, d.h. Argumente, die weder required noch
			// optional sind
			Enumeration enu = getArgs().keys();
			String sGivenkey;
			while( enu.hasMoreElements() ) {
				sGivenkey = (String) enu.nextElement();
				if( sGivenkey.startsWith( "VARIABLE_KEY" ) ) {
					// Tests if the index is valid
					String sKeyIndex = sGivenkey.substring( 12 );
					int iKeyIndex;
					try {
						iKeyIndex = Integer.parseInt( sKeyIndex );
						if( iKeyIndex > i_iMaxKeyIndex )
							i_iMaxKeyIndex = iKeyIndex;
					} catch( NumberFormatException e ) {
						if( debug ) {
							CascadeLogging.getLogger().log( LogLevel.DEBUG, "Argument error in checkArgs()!" );
							CascadeLogging.getLogger().log( LogLevel.DEBUG, "Invalid index of VARIABLE_KEY in" + sGivenkey );
						}
						throw new Exception( "Invalid index of Variable_Key in" + sGivenkey );
					}
				}

				if( sGivenkey.startsWith( "VARIABLE_VALUE" ) ) {
					// Tests if the index is valid
					String sValueIndex = sGivenkey.substring( 14 );
					int iValueIndex;
					try {
						iValueIndex = Integer.parseInt( sValueIndex );
						if( iValueIndex > i_iMaxValueIndex )
							i_iMaxValueIndex = iValueIndex;
					} catch( NumberFormatException e ) {
						if( debug ) {
							CascadeLogging.getLogger().log( LogLevel.DEBUG, "Argument error in checkArgs()!" );
							CascadeLogging.getLogger().log( LogLevel.DEBUG, "Invalid index of VARIABLE_VALUE in" + sGivenkey );
						}
						throw new Exception( "Invalid index of Variable_Value in" + sGivenkey );
					}
				}
				int j = 0;
				while( (exist == false) && (j < requiredArgs.length) ) {
					if( sGivenkey.equalsIgnoreCase( requiredArgs[j] ) == true )
						exist = true;
					j++;
				}
			}

			int j = 0;

			for( int i = 1; i <= i_iMaxValueIndex; i++ ) {
				enu = getArgs().keys();
				while( enu.hasMoreElements() ) {
					sGivenkey = (String) enu.nextElement();
					if( sGivenkey.startsWith( "VARIABLE_VALUE" ) ) {
						// Tests if the index is valid
						String sValueIndex = sGivenkey.substring( 14 );
						int iValueIndex;
						iValueIndex = Integer.parseInt( sValueIndex );
						if( iValueIndex == j + 1 )
							j++;
					}
				}
			}
			if( j != i_iMaxValueIndex )
				throw new Exception( "Invalid index of Variable Value" );

			j = 0;
			for( int i = 1; i <= i_iMaxValueIndex; i++ ) {
				enu = getArgs().keys();
				while( enu.hasMoreElements() ) {
					sGivenkey = (String) enu.nextElement();
					if( sGivenkey.startsWith( "VARIABLE_KEY" ) ) {
						// Tests if the index is valid
						String sValueIndex = sGivenkey.substring( 12 );
						int iValueIndex;
						iValueIndex = Integer.parseInt( sValueIndex );
						if( iValueIndex == j + 1 )
							j++;
					}
				}
			}
			if( j != i_iMaxValueIndex )
				throw new Exception( "Invalid index of Key Value" );

			if( i_iMaxValueIndex != i_iMaxKeyIndex )
				throw new Exception( "Invalid index of Variable Key or Value" );

			if( exist == false )
				throw new Exception( "Required Argument is missing" );

			// Job IPSQ_PU_RESULT
			// ------------------
			if( job.equalsIgnoreCase( "IPSQ_PU_RESULT" ) ) {
				check_IPSQ_PU_RESULT();
			}
			// Job SEND_IPSQ_PU_RESULTS
			// ------------------
			else if( job.equalsIgnoreCase( "SEND_IPSQ_PU_RESULTS" ) ) {
				//nichts machen
			}
			// Job undefined
			// ------------------
			else {
				throw new Exception( "job " + job + " " + PB.getString( "nichtVerfuegbar" ) + "!" );
			}
			return true;

		} catch( Exception e ) {
			log( debug, "Error", "checking args, Exception: " + e.getMessage() );
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Ersetzt vorkommende Variablen durch dessen  Werte
	 * 
	 * @throws PPExecutionException falls Werte nicht verf�gbar bzw. als nicht verwertbar angesehen
	 *             wird.
	 * @param str der zu pr�fende String
	 * @return String mit evtl. erg�nzten Wert der Variable
	 */
	protected String replaceVariablesInString( String str ) throws PPExecutionException {
		String[] temp;

		if( str.indexOf( '@' ) != -1 ) {
			temp = extractValues( str );
			str = "";
			for( int k = 0; k < temp.length; k++ ) {
				str = str + temp[k] + " ";
			}
		} else {
			str = PB.getString( str );
		}

		return str;

	}

	/**
	 * Splittet den �bergebenen Argumente-/Attribute-String in die diversen Alternativen auf und
	 * ersetzt abschlie�end eventuell vorhandene Links durch die entsprechenden Werte
	 * 
	 * @throws PPExecutionException falls Werte nicht verf�gbar bzw. als nicht verwertbar angesehen
	 *             wird.
	 * @param arg der zu bearbeitende Argument- oder Attributstring
	 * @return Array mit den Werten
	 */
	protected String[] extractValues( String arg ) throws PPExecutionException {
		String[] args;

		args = splitArg( arg );
		for( int i = 0; i < args.length; i++ ) {
			if( args[i].indexOf( '@' ) != -1 ) {
				try {
					args[i] = getPPResult( args[i].toUpperCase() );
				} catch( InformationNotAvailableException e ) {
					throw new PPExecutionException( e.getMessage() );
				}
			}
		}
		return args;
	}

	/**
	 * fuehrt die Pruefprozedur aus
	 * 
	 * @param info - Information zur Ausfuehrung
	 */
	public void execute( ExecutionInfo info ) {
		String job = getArg( "JOB" );
		boolean debug = ((getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ))) ? true : false;
		// aux
		int status = STATUS_EXECUTION_OK;
		Vector resList = new Vector();
		String localDirPath = CascadeProperties.getCascadeHome() + "/database/Pruefstand/IPSQ";
		log( debug, "Info", "This is vehicle no. " + this.getPr�fling().getAuftrag().getFahrgestellnummer7() );

		try {
			// -------------------------------------------------------------------------------------------
			// Check args
			// -------------------------------------------------------------------------------------------
			log( debug, "Var", "---> " + getClassName() );
			if( !checkArgs() ) {
				throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			}
			log( debug, "Ok", "check args ok." );

			// -------------------------------------------------------------------------------------------
			// Job execution
			// -------------------------------------------------------------------------------------------
			log( debug, "Ok", "executing job... " + job );

			/*
			//�berpr�fung ob man einen Auto Retry on Error oder Repeat Once on Error Block hat und der erste Durchlauf fehlerhaft war
			//dann sollte kein Ergebnis erstellt werden
			boolean createResult = true;
			if( getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getOuterBlock().getErrorMode() == ExecutionConstants.ERRORMODE_AUTO_RETRY_ON_ERROR || getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getOuterBlock().getErrorMode() == ExecutionConstants.ERRORMODE_REPEAT_ONCE_ON_ERROR ) {
				if( (getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getOuterBlock().getExecutionCount() == 1) && (getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getLastExecStatus() == ExecutionConstants.STATUS_EXECUTION_ERROR) ) {
					createResult = false;
				}
			}
			*/

			//if( createResult ) {

			String apdmPath;
			if( getArg( "APDM_DB_URI" ) != null && getArg( "APDM_DB_URI" ).length() >= 1 ) {
				apdmPath = getArg( "APDM_DB_URI" );
			} else {
				// get APDM URI Path from system properties
				apdmPath = com.bmw.cascade.server.PB.getString( "cascade.server.auftrag.reportxmlpath" ).replaceAll( "\\\\", "/" );
			}
			if( apdmPath.endsWith( "/" ) && (!apdmPath.endsWith( ":/" )) ) //da wir sp�ter etwas anh�ngen, muss ein abschlie�endes "/" entfernt werden (au�er bei C:/ etc.)
				apdmPath = apdmPath.substring( 0, apdmPath.length() - 1 );

			// Job IPSQ_PU_RESULT
			// ---------------------
			// creates and send IPSQ PU result file
			if( job.equalsIgnoreCase( "IPSQ_PU_RESULT" ) ) {
				status = execute_IPSQ_PU_RESULT( resList, localDirPath );
			}

			// Job SEND_IPSQ_PU_RESULTS
			// ---------------------
			// only sends all IPSQ PU result files
			else if( job.equalsIgnoreCase( "SEND_IPSQ_PU_RESULTS" ) ) {

				status = execute_SEND_IPSQ_PU_RESULTS( resList, localDirPath );

			}
			//}

			//Optionalen Parameter TIMEOUT "checken"
			if( getArg( "APDM_ACCESS_TIMEOUT" ) != null ) {
				timeout = Long.parseLong( getArg( "APDM_ACCESS_TIMEOUT" ).trim() );
			}

		} catch( PPExecutionException e ) {
			// NIO
			status = STATUS_EXECUTION_ERROR;
			resList.add( new Ergebnis( "ExecError", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "", Ergebnis.FT_NIO ) );
			log( debug, "Error", "executing procedure, PPExecutionException: " + e.getMessage() );
		} catch( Exception e ) {
			// NIO_SYS
			status = STATUS_EXECUTION_ERROR;
			resList.add( new Ergebnis( "ExecError", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS ) );
			log( debug, "Error", "executing procedure, Exception: " + e.getMessage() );
		} catch( Throwable e ) {
			// NIO_SYS
			status = STATUS_EXECUTION_ERROR;
			resList.add( new Ergebnis( "ExecError", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS ) );
			log( debug, "Error", "executing procedure, Throwable: " + e.getMessage() );
		}

		// -------------------------------------------------------------------------------------------
		// Log APDM results
		// -------------------------------------------------------------------------------------------
		for( int i = 0; i < resList.size(); i++ ) {
			log( debug, "APDM" + i, resList.get( i ).toString() );
		}
		// -------------------------------------------------------------------------------------------
		// Set status
		// -------------------------------------------------------------------------------------------
		setPPStatus( info, status, resList );
		log( debug, "Var", "Execution status: " + (status == STATUS_EXECUTION_OK ? "Ok" : "Error") );
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//										CHECK JOBS 	
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * <b>check_IPSQ_PU_RESULT</b>
	 * <br>
	 * - req: IPSQ_PU_NAME, ERROR_TEXT<br>
	 * - opt: ADVISE_TEXT, INSTRUCTION_TEXT<br>
	 */
	private void check_IPSQ_PU_RESULT() throws Exception {
		try {
			// IPSQ_PU_NAME
			new String( getArg( "IPSQ_PU_NAME" ) );
			// ERROR_TEXT
			new String( getArg( "ERROR_TEXT" ) );
		} catch( NullPointerException e ) {
			throw new Exception( "[NullPointerException] " + e.getMessage() );
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//										EXECUTE JOBS 	
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * <b>execute_IPSQ_PU_RESULT</b>
	 * <br>
	 * @param resultList - APDM results list
	 * @param localDirPath - local dir path
	 * @return execution status
	 */
	private int execute_IPSQ_PU_RESULT( Vector resultList, String localDirPath ) throws PPExecutionException {
		// CASCADE parameters
		String job = getArg( "JOB" );

		String puName = getArg( "IPSQ_PU_NAME" ).trim().toUpperCase();
		String errorText = getArg( "ERROR_TEXT" );
		String testStand = (getArg( "TESTSTAND" ) != null) ? getArg( "TESTSTAND" ) : getSubString( this.getPr�flingLaufzeitUmgebung().getName(), "@", null );
		String adviseText = (getArg( "ADVISE_TEXT" ) != null) ? getArg( "ADVISE_TEXT" ) : ""; // maximum length for this argument is 150 characters
		String instructionText = (getArg( "INSTRUCTION_TEXT" ) != null) ? getArg( "INSTRUCTION_TEXT" ) : ""; // maximum length for this argument is 150 characters

		int maxLenAdvise = 150;
		int maxLenError = 150;
		//int maxLenInstruction = 30;
		int maxLenInstruction = 150;

		UserDialog myDialog = null;
		final boolean DE = checkDE(); // Systemsprache DE wenn true

		boolean debug = ((getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ))) ? true : false;
		boolean showMessage = ((getArg( "SHOW_MESSAGE" ) != null) && (getArg( "SHOW_MESSAGE" ).equalsIgnoreCase( "FALSE" ))) ? false : true;

		// aux
		int status = STATUS_EXECUTION_OK;
		Properties prop = new Properties();
		int itmp = 0;

		// Unterst�tzung @-Parameter
		errorText = replaceVariablesInString( errorText );
		adviseText = replaceVariablesInString( adviseText );
		instructionText = replaceVariablesInString( instructionText );

		//Wenn errorText "!PU!" enth�lt, dann soll dieses ersetzt werden durch "Pruefumfangname"
		if( errorText.contains( "!PU!" ) ) {
			errorText = errorText.replaceFirst( "!PU!", getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getLogicalName() );
		}

		String apdmPath;
		if( getArg( "APDM_DB_URI" ) != null && getArg( "APDM_DB_URI" ).length() >= 1 ) {
			apdmPath = getArg( "APDM_DB_URI" );
		} else {
			// get APDM URI Path from system properties
			apdmPath = com.bmw.cascade.server.PB.getString( "cascade.server.auftrag.reportxmlpath" ).replaceAll( "\\\\", "/" );
		}
		if( apdmPath.endsWith( "/" ) && (!apdmPath.endsWith( ":/" )) ) { //da wir sp�ter etwas anh�ngen, muss ein abschlie�endes "/" entfernt werden (au�er bei C:/ etc.)
			apdmPath = apdmPath.substring( 0, apdmPath.length() - 1 );
		}
		boolean pingStatus = checkDirectory( apdmPath );

		String parallelBlockname = null;
		String sequentialBlockname = null;
		try {
			parallelBlockname = getArg( "PARALLEL_BLOCKNAME" ).toUpperCase();
			sequentialBlockname = getArg( "SEQUENTIAL_BLOCKNAME" ).toUpperCase();
		} catch(NullPointerException npe) {
			//do nothing - optional args Parallel_Blockname and Sequential_Blockname are not configured.
		}
		

		try {
			// create/check local dir
			log( debug, "Ok", "creating/checking local dir..." );
			createLocalDir( debug, localDirPath );

			// prepare IPSQ data
			log( debug, "Ok", "preparing IPSQ data..." );
			String xmlns = "http://bmw.com/standardResultData";
			String xmlnsXsi = "http://www.w3.org/2001/XMLSchema-instance";
			String xsiSchemaLocation = "http://bmw.com/standardResultData \\\\europe.bmw.corp\\winfs\\ti-proj\\EFA\\51_APDM\\a_System\\a_Systembeschreibung\\Standard-Ergebnisformat\\standardResultDataV0123.xsd";
			String xsiVersion = "01.23.00";
			Pruefumfang pruefumfang = getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang();
			Auftrag auftrag = getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag();

			// cut text arguments to proper size
			errorText = (errorText.length() <= maxLenError) ? errorText : errorText.substring( 0, maxLenError );
			adviseText = (adviseText.length() <= maxLenAdvise) ? adviseText : adviseText.substring( 0, maxLenAdvise );
			instructionText = (instructionText.length() <= maxLenInstruction) ? instructionText : instructionText.substring( 0, maxLenInstruction );

			String resValue = "";
			//Differenzieren zwischen klassischer Fehlerbewertung oder Fehlerbewertung im parallelen Block
			//Fehlerbewertung im parallelen Block
			if( parallelBlockname != null && (!parallelBlockname.equalsIgnoreCase( "" )) ) {
				resValue = getResValueFromSquentialWithinParallelBlock( pruefumfang, parallelBlockname, sequentialBlockname, debug );
				//klassische Fehlerbewertung
			} else {
				//wenn die Pr�fung i.O. ist oder der Fehler ignoriert werden kann, dann resValue = OK.
				resValue = ((pruefumfang.getLastExecStatus() == Pruefumfang.STATUS_EXECUTION_OK) || (pruefumfang.getLastExecStatus() == Pruefumfang.STATUS_EXECUTION_ERROR_IGNORE)) ? "OK" : "NOK";
			}

			log( debug, "resValue", resValue );

			// set result to "F" for IPSQ if variable "F_DEFAULT" is set
			if( (getArg( "F_DEFAULT" ) != null) && (getArg( "F_DEFAULT" ).equalsIgnoreCase( "TRUE" )) )
				resValue = "NOK";

			String resName = puName + "System" + resValue;
			String errorCount = (resValue.equalsIgnoreCase( "OK" )) ? "0" : "1";
			String errorType = (resValue.equalsIgnoreCase( "OK" )) ? "K" : "F";
			if( resValue.equalsIgnoreCase( "OK" ) ) {
				// if OK clean texts ??
				errorText = "";
				adviseText = "";
				instructionText = "";
			}
			String xmlName = "X" + auftrag.getFahrgestellnummer7() + "_" + puName + "_" + String.valueOf( System.currentTimeMillis() ) + ".xml";
			String xmlPath = localDirPath + File.separator + xmlName;
			String ppName = getSubString( this.getClassName(), ".", "_Pruefprozedur" );
			String plName = getSubString( this.getPr�fling().getClassName(), ".", "_Pruefling" );
			String logPLName = getPr�fling().getName();

			// testResult node --------------------------------------------------------------------------
			prop.setProperty( "testResult.xmlns", xmlns );
			prop.setProperty( "testResult.xmlns_xsi", xmlnsXsi );
			prop.setProperty( "testResult.xsi_schemaLocation", xsiSchemaLocation );
			// fileInfo node ----------------------------------------------------------------------------
			prop.setProperty( "fileInfo.systemName", "Cascade" ); //req 
			prop.setProperty( "fileInfo.softwareVersion", XmlUtil.replaceXmlCharsAndTruncate( CascadeProperties.getCascadeVersion(), 16 ) );
			prop.setProperty( "fileInfo.resultFormatVersion", xsiVersion );
			//prop.setProperty("fileInfo.orderID", auftrag.getOrderId());
			// vehicleInfo node -------------------------------------------------------------------------
			prop.setProperty( "vehicleInfo.shortVIN", auftrag.getFahrgestellnummer7() ); //req
			//prop.setProperty("vehicleInfo.FZS", auftrag.getSteuerschl�ssel());
			prop.setProperty( "vehicleInfo.typeKey", auftrag.getTypschluessel() );
			// testInfo node ----------------------------------------------------------------------------
			prop.setProperty( "testInfo.pruefumfangName", puName ); //req
			prop.setProperty( "testInfo.testStand", testStand ); //req
			prop.setProperty( "testInfo.errorCount", errorCount ); //req
			prop.setProperty( "testInfo.complete", "1" ); //req
			prop.setProperty( "testInfo.testTime", CascadeDateFormat.formatResultDate( new Date() ) ); //req  
			prop.setProperty( "testInfo.physicalPruefumfangName", pruefumfang.getName() );
			prop.setProperty( "testInfo.testVersion", pruefumfang.getVersion() );
			prop.setProperty( "testInfo.optParam1", job );
			prop.setProperty( "testInfo.testDuration", "0" );
			// result node ------------------------------------------------------------------------------
			prop.setProperty( "result.testStepName", logPLName ); //req
			prop.setProperty( "result.testStepResult", XmlUtil.replaceXmlCharsAndTruncate( resValue, 4000 ) ); //req
			prop.setProperty( "result.resultName", XmlUtil.replaceXmlCharsAndTruncate( resName, 100 ) );
			prop.setProperty( "result.lineRelevant", errorCount );
			prop.setProperty( "result.description", this.getName() );
			prop.setProperty( "result.resultValueStr", XmlUtil.replaceXmlCharsAndTruncate( resValue, 4000 ) );
			prop.setProperty( "result.minValueStr", "OK" );
			prop.setProperty( "result.maxValueStr", "OK" );
			prop.setProperty( "result.prueflingName", plName ); //PJ_10_0_F
			prop.setProperty( "result.pruefprozedurName", ppName ); //IPSQXml_x_0_F
			prop.setProperty( "result.errorType", errorType );
			prop.setProperty( "result.resultType", "diagnostic" ); //??	  
			prop.setProperty( "result.errorText", XmlUtil.replaceXmlCharsAndTruncate( errorText, 4000 ) ); //FehlerText
			prop.setProperty( "result.adviseText", XmlUtil.replaceXmlCharsAndTruncate( adviseText, 500 ) ); //HinweiseText
			prop.setProperty( "result.instructionText", XmlUtil.replaceXmlCharsAndTruncate( instructionText, 500 ) ); //AnweiseText

			for( int i = 1; i <= i_iMaxValueIndex; i++ ) {
				prop.setProperty( "resultVariable" + i + ".variableKey", getArg( "VARIABLE_KEY" + i ) );
				prop.setProperty( "resultVariable" + i + ".variableValue", getArg( "VARIABLE_VALUE" + i ) );
			}

			// create IPSQ file
			// ----------------
			log( debug, "Ok", "creating IPSQ file..." );
			if( createIPSQFile( debug, prop, xmlPath ) ) {
				// IO
				resultList.add( new Ergebnis( "StatusIPSQPuResult", "IPSQXml", job, puName, resName, "IPSQPuResultOk", "Ok", "Ok", "Ok", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
				log( debug, "Ok", "IPSQ file created. (" + resName + ")" );

				// show advise Text
				// ---------------
				if( (!errorText.equalsIgnoreCase( "" ) || !adviseText.equalsIgnoreCase( "" ) || !instructionText.equalsIgnoreCase( "" )) && showMessage ) {
					try {
						UserDialog myUd = getPr�flingLaufzeitUmgebung().getUserDialog();
						myUd.setAllowCancel( false );
						myUd.requestAlertMessage( errorText, adviseText + "\n" + instructionText, 5 );
						getPr�flingLaufzeitUmgebung().releaseUserDialog();
						log( debug, "Ok", "showed & released user dialog." );

					} catch( Exception ex ) {
						log( debug, "Error", "showing/releasing user dialog! Exception: " + ex.getMessage() );
					}
				}

				// send IPSQ files
				// ---------------
				log( debug, "Ok", "start Timestamp for vin: " + auftrag.getFahrgestellnummer7() );
				if( timeStamp( debug, auftrag.getFahrgestellnummer7(), puName ) ) {
					log( debug, "Ok", "ende Timestamp for vin: " + auftrag.getFahrgestellnummer7() + " , check ok" );
					if( pingStatus ) {
						log( debug, "Ok", "sending IPSQ files... from " + localDirPath + " to " + apdmPath );
						itmp = sendIPSQFiles( debug, localDirPath, apdmPath );
						resultList.add( new Ergebnis( "StatusSendIPSQPuResults", "IPSQXml", job, apdmPath, "", "SendIPSQPuResultsOk", String.valueOf( itmp ), "0", "", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
						log( debug, "Ok", "sent " + String.valueOf( itmp ) + " files to the APDM DB server." );
					} else {
						// sending not possible
						resultList.add( new Ergebnis( "StatusIPSQPingResult", "IPSQXml", job, apdmPath, "", "IPSQPingResultNOk", "NOk", "Ok", "Ok", "0", "", "", "", "APDMPathPingError", "", Ergebnis.FT_IGNORE ) );
						log( debug, "Error", "Failed to connect to apdmpath! (" + apdmPath + ")" );
						try {
							myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
						} catch( Exception e ) {
							if( e instanceof DeviceLockedException ) {

							}
						}
						myDialog.displayAlertMessage( "IPSQXml", "APDM-Pfad nicht erreichbar - Netzwerkfehler!", 5 );
						String message = DE ? "Server nicht erreichbar - IPSQ-Result nur lokal erzeugt!" : "Server not reachable - IPSQ-result only local";
						System.out.println( message );
					}
				} else {
					log( debug, "Ok", "ende Timestamp for vin: " + auftrag.getFahrgestellnummer7() + " , check nok" );
				}
			} else {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resultList.add( new Ergebnis( "StatusIPSQPuResult", "IPSQXml", job, puName, resName, "IPSQPuResultNOk", "NOk", "Ok", "Ok", "0", "", "", "", "XmlFileCreationError", "", Ergebnis.FT_NIO ) );
				log( debug, "Error", "Failed to create IPSQ file. (" + xmlPath + ")" );
			}
			return status;

		} catch( Exception e ) {
			resultList.add( new Ergebnis( "StatusSendIPSQPuResults", "IPSQXml", job, apdmPath, "", "SendIPSQPuResultsNOk", String.valueOf( itmp ), "0", "", "0", "", "", "", "APDMConnectionFailed", "", Ergebnis.FT_NIO ) );
			if( debug ) {
				e.printStackTrace();
			}
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_SEND_IPSQ_PU_RESULTS</b>
	 * <br>
	 * @param resultList - APDM results list
	 * @param localDirPath - local dir path
	 * @return execution status
	 */
	private int execute_SEND_IPSQ_PU_RESULTS( Vector resultList, String localDirPath ) throws PPExecutionException {
		// CASCADE parameters
		String job = getArg( "JOB" );
		UserDialog myDialog = null;
		final boolean DE = checkDE(); // Systemsprache DE wenn true
		boolean debug = ((getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ))) ? true : false;
		// aux
		int status = STATUS_EXECUTION_OK;
		int itmp = 0;
		String apdmPath;
		if( getArg( "APDM_DB_URI" ) != null && getArg( "APDM_DB_URI" ).length() >= 1 ) {
			apdmPath = getArg( "APDM_DB_URI" );
		} else {
			// get APDM URI Path from system properties
			apdmPath = com.bmw.cascade.server.PB.getString( "cascade.server.auftrag.reportxmlpath" ).replaceAll( "\\\\", "/" );
		}
		if( apdmPath.endsWith( "/" ) && (!apdmPath.endsWith( ":/" )) ) //da wir sp�ter etwas anh�ngen, muss ein abschlie�endes "/" entfernt werden (au�er bei C:/ etc.)
			apdmPath = apdmPath.substring( 0, apdmPath.length() - 1 );

		boolean pingStatus = checkDirectory( apdmPath );
		try {
			// create/check local dir 
			log( debug, "Ok", "creating/checking local dir..." );
			createLocalDir( debug, localDirPath );

			// send IPSQ files

			if( pingStatus ) {
				log( debug, "Ok", "sending IPSQ files... from " + localDirPath + " to " + apdmPath );
				itmp = sendIPSQFiles( debug, localDirPath, apdmPath );
				resultList.add( new Ergebnis( "StatusSendIPSQPuResults", "IPSQXml", job, apdmPath, "", "SendIPSQPuResultsOk", String.valueOf( itmp ), "0", "", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
				log( debug, "Ok", "sent " + String.valueOf( itmp ) + " files to the APDM DB server." );
				return status;
			} else {
				// sending not possible
				resultList.add( new Ergebnis( "StatusIPSQPingResult", "IPSQXml", job, apdmPath, "", "IPSQPingResultNOk", "NOk", "Ok", "Ok", "0", "", "", "", "APDMPathPingError", "", Ergebnis.FT_IGNORE ) );
				log( debug, "Error", "Failed to connect to Apdmpath: (" + apdmPath + ")" );
				try {
					myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
				} catch( Exception e ) {
					if( e instanceof DeviceLockedException ) {

					}
				}
				myDialog.displayAlertMessage( "IPSQXml", "APDM-Pfad nicht erreichbar - Netzwerkfehler!", 5 );
				String message = DE ? "Server nicht erreichbar - IPSQ-Result nur lokal erzeugt!" : "Server not reachable - IPSQ-result only local";
				System.out.println( message );

				return status;
			}

		} catch( Exception e ) {
			resultList.add( new Ergebnis( "StatusSendIPSQPuResults", "IPSQXml", job, apdmPath, "", "SendIPSQPuResultsNOk", String.valueOf( itmp ), "0", "", "0", "", "", "", "APDMConnectionError", "", Ergebnis.FT_NIO ) );
			if( debug ) {
				e.printStackTrace();
			}
			throw new PPExecutionException( e.getMessage() );
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//											UTILS 	
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////	

	/**
	 * <b>createLocalDir</b> 
	 * <BR>
	 * Creates the local IPSQ directory and verifies validity.
	 * <BR>
	 * @param debug - debug flag
	 * @param localDirPath
	 * @return true if ok, false if not
	 */
	private boolean createLocalDir( boolean debug, String localDirPath ) throws Exception {
		File dir = new File( localDirPath );
		// check existence/create
		if( !dir.exists() ) {
			if( !dir.mkdir() ) {
				throw new Exception( "unable to create local output dir " + localDirPath );
			}
		}
		// check validity
		if( !validDir( debug, dir ) ) {
			throw new Exception( "invalid local output dir " + localDirPath );
		}
		return true;
	}

	/**
	 * <b>createIPSQFile</b>
	 * <BR>
	 * Creates an IPSQ result xml file with the specified properties in the given path.
	 * <BR>
	 * @param debug - debug flag
	 * @param prop - properties object containing the data to insert
	 * @param xmlPath - output xml file path
	 * @return state - IPSQ file creation result 
	 * @throws Exception - if something goes wrong
	 */
	private boolean createIPSQFile( boolean debug, Properties prop, String xmlPath ) throws Exception {
		try {
			log( debug, "Ok", "creating IPSQ XML file..." );
			log( debug, "Var", "XML properties: " + prop.toString() );

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			log( debug, "Ok", "DOM document built." );
			// build nodes
			Element root = (Element) buildNode( debug, doc, prop, "testResult" );
			Node fileInfoNode = buildNode( debug, doc, prop, "fileInfo" );
			Node vehicleInfoNode = buildNode( debug, doc, prop, "vehicleInfo" );
			Node testInfoNode = buildNode( debug, doc, prop, "testInfo" );
			Node resultNode = buildNode( debug, doc, prop, "result" );
			log( debug, "Ok", "XML DOM nodes created." );
			// build structure
			doc.appendChild( root );
			root.appendChild( fileInfoNode );
			root.appendChild( vehicleInfoNode );
			testInfoNode.appendChild( resultNode );
			for( int i = 1; i <= i_iMaxValueIndex; i++ ) {
				Node variableNode = buildNode( debug, doc, prop, "resultVariable" + i );
				testInfoNode.appendChild( variableNode );
			}
			root.appendChild( testInfoNode );
			log( debug, "Ok", "XML DOM structure created." );
			doc.normalize();
			// write to xml file
			TransformerFactory transfFactory = TransformerFactory.newInstance();
			Transformer transf = transfFactory.newTransformer();
			log( debug, "Ok", "got DOM transformer." );
			DOMSource source = new DOMSource( doc );
			log( debug, "Ok", "got DOM source." );
			if( xmlPath == null ) {
				return false;
			}
			File xmlFile = new File( xmlPath );
			StreamResult result = new StreamResult( xmlFile );

			transf.transform( source, result );
			updateRegistry( debug, xmlFile.getParent(), "Ok", "CREATED : " + xmlFile.getName() + " (" + prop.getProperty( "result.resultName" ) + ")" );
			log( debug, "Ok", "XML document " + xmlFile.getName() + " created." );
			return true;

		} catch( FactoryConfigurationError e ) {
			throw new Exception( "[FactoryConfigurationException] " + e.getMessage() );
		} catch( ParserConfigurationException e ) {
			throw new Exception( "[ParserConfigurationException] " + e.getMessage() );
		} catch( DOMException e ) {
			throw new Exception( "[DOMException] " + e.getMessage() );
		} catch( TransformerFactoryConfigurationError e ) {
			throw new Exception( "[TransformerFactoryConfigurationException] " + e.getMessage() );
		} catch( TransformerConfigurationException e ) {
			throw new Exception( "[TransformerConfigurationException] " + e.getMessage() );
		} catch( TransformerException e ) {
			throw new Exception( "[TransformerException] " + e.getMessage() );
		} catch( NullPointerException e ) {
			throw new Exception( "[NullPointerException] " + e.getMessage() );
		} catch( Exception e ) {
			throw new Exception( "[Exception] " + e.getMessage() );
		}
	}

	/**
	 * <b>sendIPSQFiles</b>
	 * <BR>
	 * Tests the APDM DB connection and if exists then sends all IPSQ result xml files in the 
	 * given directory to the given APDM DB Path. Returns -1 if no APDM connection is possible.
	 * <BR>
	 * @param debug - debug flag
	 * @param prop - properties object containing the data to insert
	 * @param xmlPath - output xml file path
	 * @return filesSent - number of files sent or -1 for APDM connection error
	 * @throws Exception - if something goes wrong
	 */
	private synchronized int sendIPSQFiles( boolean debug, String localDirPath, String apdmPath ) throws Exception {
		File outputDir = new File( localDirPath );
		File[] srcFiles = outputDir.listFiles();
		File destFile = null;
		int filesSent = 0;

		// Pr�fe Erreichbarkeit der APDM DB in Executorthread um einen Timeout zu erm�glichen
		ExecutorService exSrv = Executors.newSingleThreadExecutor();
		ExecutorCompletionService<Boolean> completionService = new ExecutorCompletionService<Boolean>( exSrv );
		completionService.submit( new CheckPathExists( apdmPath ) );
		Future<Boolean> pathExists = completionService.poll( timeout, TimeUnit.SECONDS );
		if( pathExists == null || pathExists.get() != true ) {
			exSrv.shutdown();
			throw new PPExecutionException( "APDM ACCESS ERROR" );
		}
		exSrv.shutdown();

		// Send all existing .xml files that start 
		// with X from the local folder.
		// --------------------------------------- 
		for( int i = 0; i < srcFiles.length; i++ ) {
			if( srcFiles[i].isFile() && srcFiles[i].getName().endsWith( ".xml" ) && srcFiles[i].getName().startsWith( "X" ) ) {
				destFile = new File( apdmPath + "/" + srcFiles[i].getName() );

				log( debug, "OK", "exists-Method before renaming: destFile.exists()=" + destFile.exists() );

				boolean renameSuccessfull = false;
				for( int k = 0; k < 5 && !renameSuccessfull; k++ ) {
					renameSuccessfull = srcFiles[i].renameTo( destFile );
					log( debug, "OK", "renameTo-Method: renameSuccessfull=" + renameSuccessfull );
					if( destFile.exists() ) {
						renameSuccessfull = true;
						log( debug, "OK", "exists-Method: destFile.exists()=" + destFile.exists() );
					}
				}

				if( renameSuccessfull ) {
					filesSent++;
					updateRegistry( debug, localDirPath, "Ok", "SENT    : " + srcFiles[i].getName() );
					log( debug, "Ok", "sent file " + destFile.getAbsolutePath() );
				} else {
					//inperformanteres kopieren versuchen
					if( copy( debug, srcFiles[i].getPath(), destFile.getPath() ) ) {
						//nach copy unbedingt sourcefile l�schen
						if( srcFiles[i].exists() ) {
							srcFiles[i].delete();
							log( debug, "Ok", "source file deleted: " + srcFiles[i].getPath() );
						}
						filesSent++;
						updateRegistry( debug, localDirPath, "Ok", "SENT    : " + srcFiles[i].getName() );
						log( debug, "Ok", "sent file " + destFile.getAbsolutePath() );
					} else {
						updateRegistry( debug, localDirPath, "Error", "SENDING : " + srcFiles[i].getName() );
						log( debug, "Error", "sending file " + destFile.getAbsolutePath() );
					}
				}
			}
		}
		// --------------------------------------- 
		return filesSent;
	}

	/**
	 * Kopiert das fromFile zum toFile �ber ByteStreams
	 * -> kostet viel Performance, wird also nur aufgerufen falls die Java Methode
	 * "renameTo" nicht funktioniert.
	 * @param debug
	 * @param fromFileName
	 * @param toFileName
	 * @throws IOException
	 */
	private boolean copy( boolean debug, String fromFileName, String toFileName ) throws IOException {
		File fromFile = new File( fromFileName );
		File toFile = new File( toFileName );

		if( !fromFile.exists() ) {
			log( debug, "Error", "FileCopy: " + "no such source file: " + fromFileName );
			return false;
		}
		if( !fromFile.isFile() ) {
			log( debug, "Error", "FileCopy: " + "can't copy directory: " + fromFileName );
			return false;
		}
		if( !fromFile.canRead() ) {
			log( debug, "Error", "FileCopy: " + "source file is unreadable: " + fromFileName );
			return false;
		}

		if( toFile.isDirectory() )
			toFile = new File( toFile, fromFile.getName() );

		if( toFile.exists() ) {
			if( !toFile.canWrite() ) {
				log( debug, "Error", "FileCopy: " + "destination file is unwriteable: " + toFileName );
				return false;
			}
		}
		FileInputStream from = null;
		FileOutputStream to = null;
		try {
			from = new FileInputStream( fromFile );
			to = new FileOutputStream( toFile );
			byte[] buffer = new byte[4096];
			int bytesRead;

			while( (bytesRead = from.read( buffer )) != -1 )
				to.write( buffer, 0, bytesRead ); // write
		} finally {
			if( from != null )
				try {
					from.close();
				} catch( IOException e ) {
					e.printStackTrace();
					return false;
				}
			if( to != null )
				try {
					to.close();
				} catch( IOException e ) {
					e.printStackTrace();
					return false;
				}
		}
		log( debug, "OK", fromFileName + "was copied to: " + toFileName );
		return true;
	}

	/**
	 * <b>buildNode</b>
	 * <BR>
	 * Builds a DOM node with the specified name and with the properties specified attributes. 
	 * <BR>
	 * @param debug - debug flag
	 * @param doc - DOM document
	 * @param prop - properties object to with the data to include
	 * @param name - name of the node to be built
	 * @return the created node ready to be added 	
	 * @throws Exception - if something goes wrong
	 */
	private Node buildNode( boolean debug, Document doc, Properties prop, String name ) throws Exception {
		Element node;
		if( name.startsWith( "resultVariable" ) )
			node = doc.createElement( "resultVariable" );
		else
			node = doc.createElement( name );

		log( debug, "Ok", "building node " + name + " ..." );
		Enumeration keys = prop.keys();
		String propName = null;
		String attribName = null;
		String attribValue = null;
		while( keys.hasMoreElements() ) {
			propName = (String) keys.nextElement();
			if( propName.startsWith( name ) ) {
				attribName = propName.substring( propName.indexOf( "." ) + 1 );
				if( name.equalsIgnoreCase( "testResult" ) && attribName.indexOf( "_" ) != 1 ) {
					attribName = attribName.replaceFirst( "_", ":" );
				}
				attribValue = prop.getProperty( propName );
				node.setAttribute( attribName, attribValue );
			}
		}
		return node;
	}

	/**
	 * <b>validDir</b> 
	 * <BR>
	 * Checks if the given directory exists, is readable, and is writtable.  
	 * <BR>
	 * @param dir - the directoy to evaluate
	 * @return validation result
	 */
	private boolean validDir( boolean debug, File dir ) {
		try {
			if( !dir.exists() || !dir.isDirectory() || !dir.canRead() || !dir.canWrite() ) {
				log( debug, "Error", "invalid directory (criteria: Existence/Directory/Readable/Writable) " + dir.getAbsolutePath() );
				return false;
			}
			log( debug, "Ok", "valid directory " + dir.getAbsolutePath() );
			return true;

		} catch( SecurityException e ) {
			log( debug, "Error", "checking directory security problems, SecurityException: " + e.getMessage() );
			return false;
		} catch( NullPointerException e ) {
			log( debug, "Error", "checking directory null argument, NullPointerException: " + e.getMessage() );
			return false;
		} catch( Exception e ) {
			log( debug, "Error", "checking directory, Exception: " + e.getMessage() );
			return false;
		}
	}

	/**
	 * <b>updateRegistry</b>
	 * <BR>
	 * Updates the local directory's input/output registry. 
	 * <BR>
	 * @param debug the debug flag
	 * @param type the type of message to display
	 * @param text the message text
	 */
	private synchronized void updateRegistry( boolean debug, String localDirPath, String type, String text ) {
		final String LOG_NAME = "IORegistry.log";
		final long LOG_MAX_SIZE = 1000000; // (Bytes) 1 MB 

		String regFilePath = localDirPath + File.separator + LOG_NAME;
		File regFile = null;
		PrintWriter outWriter = null;

		try {
			regFile = new File( regFilePath );
			// check existence and size 
			if( regFile.exists() ) {
				// check file size
				if( regFile.length() >= LOG_MAX_SIZE ) {
					if( regFile.delete() ) {
						log( debug, "Ok", "deleted local registry log (>" + String.valueOf( LOG_MAX_SIZE ) + " Bytes)." );
					} else {
						log( debug, "Error", "deleting local registry log (>" + String.valueOf( LOG_MAX_SIZE ) + " Bytes)." );
					}
				}
			} else {
				// create file
				if( regFile.createNewFile() ) {
					log( debug, "Ok", "created local registry log." );
				} else {
					log( debug, "Error", "creating local registry log." );
				}
			}
			// append text 
			outWriter = new PrintWriter( new BufferedWriter( new FileWriter( regFile, true ) ) );
			outWriter.println( "[" + new Date().toString() + "] " + "[" + type + "] " + text );
			outWriter.close();
			log( debug, "Ok", "updated local registry log." );

		} catch( Exception e ) {
			log( debug, "Error", "updating local registry log " + regFilePath + ", Exception: " + e.getMessage() );
		}
	}

	/**
	 * <b>trim</b>
	 * <br> 
	 * Trims n chars from the given string
	 * <br>
	 * @param nChars - number of chars to trim
	 * @return trimmed string
	 */
	private String getTrimmedString( String str, int nChars ) {
		try {
			return str.substring( 0, str.length() - nChars );

		} catch( Exception e ) {
			return "";
		}
	}

	/**
	 * <b>getSubString</b>
	 * <br> 
	 * Gets the substring between the given start string (last ocurrence) and the end string (last ocurrence). 
	 * <br> 
	 * @param str - the string where to extract the substring from
	 * @param startStr - the string (last ocurrence) where to start the extraction or NULL to start from the beginning
	 * @param endStr - the string (last ocurrence) where to end the extraction or NULL to end in the end
	 * @return the substring or an empty string
	 */
	private String getSubString( String str, String startStr, String endStr ) {
		int startIndex = (startStr != null && str.lastIndexOf( startStr ) != -1) ? str.lastIndexOf( startStr ) + startStr.length() : 0;
		int endIndex = (endStr != null && str.lastIndexOf( endStr ) != -1) ? str.lastIndexOf( endStr ) : str.length();
		return (startIndex >= 0 && startIndex < endIndex && endIndex <= str.length()) ? str.substring( startIndex, endIndex ) : "";
	}

	/**
	 * <b>timeStamp</b>
	 * <br> 
	 * Sets the current timestamp for the current vin and ipsq mark and writes it in a (temporary) pruefstandvariable. 
	 * Also checks if there is already a timestamp and the same ipsq mark for the vin and if the delta is bigger than 14 seconds,
	 * because than it is possible to send another IPSQ-Result for the same vin. If the delta is smaller than 
	 * the IPSQ-result will only be saved locally and sent with the next vin.  
	 * <br> 
	 * @param debug 
	 * @param vin - string with the current vin
	 * @param puName - string with the ipsq mark
	 * @return send - boolean, if IPSQ-result should be sent
	 */
	private boolean timeStamp( boolean debug, String vin, String puName ) {

		HashMap ipsqMerkmalMap = new HashMap();
		long currentTime = System.currentTimeMillis();
		boolean send = true;
		String lastPuName;
		IpsqMerkmal ipsq;
		try {
			try {
				ipsqMerkmalMap = (HashMap) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "TEMP_IPSQXML" );
			} catch( VariablesException e1 ) {

				log( debug, "Error", "Fehler getPruefstandVariable" );
			}

			if( !ipsqMerkmalMap.isEmpty() && ipsqMerkmalMap.containsKey( vin ) ) {
				log( debug, "Ok", "Pruefstandsvariable ist nicht leer und enth�lt vin: " + vin );
				Set keys = ipsqMerkmalMap.keySet();
				Iterator iterkeys = keys.iterator();

				while( iterkeys.hasNext() ) {
					String key = iterkeys.next().toString();
					ipsq = (IpsqMerkmal) ipsqMerkmalMap.get( key );
					long keyTime = ipsq.getIpsqTime();
					lastPuName = ipsq.getIpsqName();
					boolean timeDelta = (currentTime - keyTime) > 14000;
					log( debug, "Ok", "Delta: " + (currentTime - keyTime) + " f�r VIN: " + key );
					boolean puNameCompare = lastPuName.equalsIgnoreCase( puName );

					if( timeDelta ) {
						ipsqMerkmalMap.remove( key );
						log( debug, "Ok", "VIN " + key + " wird aus Pruefstandvariable gel�scht, da Delta: " + timeDelta );
					}
					if( !timeDelta && !puNameCompare ) {
						log( debug, "Ok", "VIN " + key + " wird gesendet, da Delta: " + timeDelta + " zwar zu klein, jedoch ein anderes Merkmal " + puName + " gesendet wird!" );
					}
					if( !timeDelta && key.equalsIgnoreCase( vin ) && puNameCompare ) {
						send = false;
						log( debug, "Ok", "VIN " + key + " wird nicht gesendet, da Delta: " + timeDelta + " und gleiches Merkmal" );
					}
				}

				try {
					getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "TEMP_IPSQXML", ipsqMerkmalMap );
				} catch( VariablesException e ) {
					System.out.println( "Kann Pruefstandvariable IPSQXML nicht setzen!" );
					e.printStackTrace();
				}

			} else if( !ipsqMerkmalMap.isEmpty() && !ipsqMerkmalMap.containsKey( vin ) ) {

				log( debug, "Ok", "Pruefstandsvariable ist nicht leer aber enth�lt nicht vin: " + vin );
				Set keys = ipsqMerkmalMap.keySet();
				Iterator iterkeys = keys.iterator();

				while( iterkeys.hasNext() ) {
					String key = iterkeys.next().toString();
					ipsq = (IpsqMerkmal) ipsqMerkmalMap.get( key );
					long keyTime = ipsq.getIpsqTime();
					lastPuName = ipsq.getIpsqName();
					boolean timeDelta = (currentTime - keyTime) > 14000;
					log( debug, "Ok", "Delta: " + (currentTime - keyTime) + " f�r VIN: " + key );

					if( timeDelta ) {
						ipsqMerkmalMap.remove( key );
						log( debug, "Ok", "VIN " + key + " wird aus Pruefstandvariable gel�scht, da Delta: " + timeDelta );
					}
					log( debug, "Ok", "vin " + vin + " wird der Pr�fstandsvariable hinzugef�gt!" );
					ipsq = new IpsqMerkmal( puName, Long.valueOf( currentTime ) );
					ipsqMerkmalMap.put( vin, ipsq );
					try {
						getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "TEMP_IPSQXML", ipsqMerkmalMap );
					} catch( VariablesException e ) {
						System.out.println( "Kann Pruefstandvariable IPSQXML nicht setzen!" );
						e.printStackTrace();
					}

				}

			} else {
				log( debug, "Ok", "Pruefstandsvariable leer, vin wird geschrieben " + vin );
				ipsq = new IpsqMerkmal( puName, Long.valueOf( currentTime ) );
				ipsqMerkmalMap.put( vin, ipsq );
				try {
					getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "TEMP_IPSQXML", ipsqMerkmalMap );
				} catch( VariablesException e ) {
					System.out.println( "Kann Pruefstandvariable IPSQXML nicht setzen!" );
					e.printStackTrace();
				}
			}
		} catch( Exception e1 ) {
			e1.printStackTrace();
			System.out.println( "exception e1 " + e1.getMessage() );
		}

		return send;

	}

	/**
	 * Represents an IPSQ characteristic
	 */
	private class IpsqMerkmal {

		long ipsqtime;
		String ipsqname;

		private IpsqMerkmal( String name, long time ) {
			this.ipsqname = name;
			this.ipsqtime = time;
		}

		private long getIpsqTime() {
			return ipsqtime;
		}

		private String getIpsqName() {
			return ipsqname;
		}

	}

	/**
	 * <b>checkDirectory</b>
	 * <br> 
	 * Checks if the host is available (ping). 
	 * <br> 
	 * @param host - The host which should be checked
	 * @return returns true or false
	 */
	private boolean checkDirectory( String host ) {
		boolean isReachable = false;

		if( host.startsWith( "//" ) ) {
			do {
				host = host.replaceFirst( "//", "" );
			} while( host.startsWith( "//" ) );
		}

		if( host.startsWith( "\\" ) ) {
			do {
				host = host.replaceFirst( "\\", "" );
			} while( host.startsWith( "\\" ) );
		}

		StringTokenizer tokenizer = new StringTokenizer( host, "/" );
		host = tokenizer.nextToken();

		try {

			// Als externen Prozess ausf�hren
			Runtime runtime = Runtime.getRuntime();
			Process process = runtime.exec( "ping -n 1 -w 1000 " + host );
			int exitValue = process.waitFor();

			if( exitValue == 0 )
				isReachable = true;

		} catch( Exception e ) {
			if( ((getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ))) ? true : false ) {
				System.out.println( "Ping PP: Exception begin!" );
				e.printStackTrace();
				System.out.println( "Ping PP: Exception end!" );
			}
		}
		return isReachable;
	}

	/** Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * <b>log</b>
	 * <br>
	 * Logs a message if the given debug flag is active or if it is an 'Error' message.
	 * <br>
	 * @param debug - the debug flag
	 * @param type - the type of message to display
	 * @param text - the message text
	 */

	private void log( boolean debug, String type, String text ) {
		if( text.equalsIgnoreCase( "Error" ) || debug ) {
			System.out.println( "[" + getLocalName() + "] [" + type + "] " + text );
		}
	}

	/**
	 *  Die Klasse CheckPathExists erm�glicht ein threadbasiertes �berpr�fen auf die Existenz eines Pfades.
	 * 
	 * @author Sch�nert Fabian, TI-545 BMW AG
	 *
	 */
	class CheckPathExists implements Callable {

		private File pathFile;

		public CheckPathExists( String path ) {
			this.pathFile = new File( path );
		}

		@Override
		public Boolean call() throws Exception {
			return pathFile.exists();
		}
	}

	/**
	 * Returns if the result of a sequential Block within a parallel Block is ok or nok.
	 * @param pruefumfang - the pruefumfang which is run through
	 * @param parallelBlockname - name of the parallel block
	 * @param sequentialBlockname - name of the sequential block within the parallel block
	 * @param debug - debug the messages
	 * @return OK oder NOK
	 */
	private String getResValueFromSquentialWithinParallelBlock( Pruefumfang pruefumfang, String parallelBlockname, String sequentialBlockname, boolean debug ) {
		String resValue = "";

		PPBlockSequence sequ = pruefumfang.getOuterBlock();

		//diese Methode findet rekursiv den gesuchten parallelen Block und speichert diesen in the globalen Variable parallelBlock
		findParallelBlock( sequ, parallelBlockname );

		PPBlockParallel pBlock = getParallelBlock();

		if( pBlock != null ) {
			if( pBlock.getName().equalsIgnoreCase( parallelBlockname ) ) {
				log( debug, "found parallel block", parallelBlockname );
				//finde den sequentiellen Block und bewerte dessen Fehler
				Enumeration parallelStatementEnumeration = pBlock.getStatements();
				for( ; parallelStatementEnumeration.hasMoreElements(); ) {
					Object parallelStatement = parallelStatementEnumeration.nextElement();
					if( parallelStatement instanceof PPBlockSequence ) {
						PPBlockSequence sequentialElement = (PPBlockSequence) parallelStatement;
						if( sequentialElement.getName().equalsIgnoreCase( sequentialBlockname ) ) {
							log( debug, "found sequential block", sequentialBlockname );
							log( debug, "number of errors", sequentialElement.getErrorCount() + "" );
							//Fehlerbewertung
							//errorCount z�hlt nur richtige Fehler keine die im "OK on Error" Block auftraten
							//errorCount entspricht der Anzahl der Fehler der unterhalb des sequentiellen Blocks befindlichen Sequenzen oder Statements
							//hat eine Sequenz unterhalb eines sequentiellen Blocks mehrere Fehler(z.B. errorCount=5), so wird dies f�r den dar�ber liegenden Block
							//als ein Fehler(errorCount=1) gewertet 
							if( sequentialElement.getErrorCount() > 0 ) {
								resValue = "NOK";
							} else {
								resValue = "OK";
							}
							log( debug, "resValue of Block " + sequentialBlockname + " is ", resValue );
						}
					}
				}
			}
		}

		return resValue;
	}

	/**
	 * This methods finds out the parallel Block within a Pruefumfang-Sequence.
	 * The parallel block sequence is saved in the global variable parallelBlock.
	 * @param sequ - Pruefumfang Sequence
	 * @param parallelBlockname - name of the parallel Block which is searched
	 */
	private void findParallelBlock( PPBlockSequence sequ, String parallelBlockname ) {
		Enumeration statementEnumeration = sequ.getStatements();
		Object localStatement = null;
		boolean interruptForLoop = false;

		//finde den parallelen Block
		for( ; statementEnumeration.hasMoreElements(); ) {
			if( interruptForLoop ) {
				break;
			} else {
				localStatement = (Object) statementEnumeration.nextElement();
				if( localStatement instanceof PPBlockParallel ) {
					PPBlockParallel localPBlock = (PPBlockParallel) localStatement;
					//paralleler Block entspricht gesuchten parallelen Blocknamen?
					if( localPBlock.getName().equalsIgnoreCase( parallelBlockname ) ) { 
						setParallelBlock( localPBlock );
						interruptForLoop = true;
					}
				} else {
					if( localStatement instanceof PPBlockSequence ) { //sequentieller Block
						PPBlockSequence sequElement = (PPBlockSequence) localStatement;
						findParallelBlock( sequElement, parallelBlockname );	//Rekursion
						if( getParallelBlock() != null ) {
							interruptForLoop = true;
						}
					} else if( localStatement instanceof PPSwitchStatement ) { //Wenn-Dann Block
						PPSwitchStatement sequElement = (PPSwitchStatement) localStatement;

						//niO Zweig
						PPStatement errorStatement = sequElement.getErrorBranch();
						if( errorStatement instanceof PPBlockSequence ) { //sequentieller Block
							PPBlockSequence errorSequ = (PPBlockSequence) errorStatement;
							findParallelBlock( errorSequ, parallelBlockname );	//Rekursion
							if( getParallelBlock() != null ) {
								interruptForLoop = true;
							}
						} else if( errorStatement instanceof PPBlockParallel ) {//paralleler Block
							PPBlockParallel localPBlock = (PPBlockParallel) errorStatement;
							//paralleler Block entspricht gesuchten parallelen Blocknamen?
							if( localPBlock.getName().equalsIgnoreCase( parallelBlockname ) ) {
								setParallelBlock( localPBlock );
								interruptForLoop = true;
							}
						}

						//iO Zweig
						PPStatement okStatement = sequElement.getOkBranch();
						if( okStatement instanceof PPBlockSequence ) { //sequentieller Block
							PPBlockSequence errorSequ = (PPBlockSequence) okStatement;
							findParallelBlock( errorSequ, parallelBlockname );	//Rekursion
							if( getParallelBlock() != null ) {
								interruptForLoop = true;
							}
						} else if( okStatement instanceof PPBlockParallel ) {//paralleler Block
							PPBlockParallel localPBlock = (PPBlockParallel) okStatement;
							//paralleler Block entspricht gesuchten parallelen Blocknamen?
							if( localPBlock.getName().equalsIgnoreCase( parallelBlockname ) ) {
								setParallelBlock( localPBlock );
								interruptForLoop = true;
							}
						}

					}

				}

			}
		}
	}

	/**
	 * Get the parallel Block.
	 * @return parallelBlock
	 */
	private PPBlockParallel getParallelBlock() {
		return parallelBlock;
	}

	/**
	 * Set the parallel Block.
	 * @param pBlock
	 */
	private void setParallelBlock( PPBlockParallel pBlock ) {
		this.parallelBlock = pBlock;
	}

}

/*
 * DiagWriteRead_2_0_F_Pruefprozedur.java
 *
 * Created on 14.09.00
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;


import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.ediabas.*;


/**
 * Implementierung der Pr�fprozedur, die einen schreibenden Diagnosejob ausf�hrt und mit einem 2-ten Diagnosejob
 * die mit dem ersten Job hinterlegten Informationen verifiziert. Die zu hinterlegenden Information kann hierbei Bestandteil
 * der Jobparameter des ersten Jobs sein, welcher Teil dies ist, ist unter dem Min-Wert des zweiten Jobs zu spezifizieren 
 * @author Winkler
 * @version Implementierung
 */
public class DiagWriteRead_2_0_F_Pruefprozedur extends AbstractBMWPruefprozedur 
{

  	static final long serialVersionUID = 1L;
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public DiagWriteRead_2_0_F_Pruefprozedur() {}

  /**
   * erzeugt eine neue Pruefprozedur
   * @param pruefling Klasse des zugeh. Pr�flings
   * @param pruefprozName Name der Pr�fprozedur
   * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
   */
  public DiagWriteRead_2_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) 
  {
    super( pruefling, pruefprozName, hasToBeExecuted );
    attributeInit();
  }  

  /**
   * initialsiert die Argumente
   */
  protected void attributeInit() {
    super.attributeInit();
  }

  /**
   * liefert die optionalen Argumente
   */
  public String[] getOptionalArgs() {
    String[] args = {"JOBPAR_W2", "JOBPAR_W3", "JOBPAR_R", "WAIT"};
    return args;
  }

  /**
   * liefert die zwingend erforderlichen Argumente
   */
  public String[] getRequiredArgs() {
    String[] args = {"SGBD", "JOB_W", "JOBPAR_W1", "JOB_R", "RESULT1", "MIN1"};
    return args;
  }

  /**
   * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
   */
   public boolean checkArgs() {
    String temp;
    int i, j;
    boolean ok;
    
    try{
      ok = super.checkArgs();
      if( ok == true ) {
        //SGBD
        if( getArg(getRequiredArgs()[0]).indexOf(';') != -1 ) return false;
        //JOB_W
        if( getArg(getRequiredArgs()[1]).indexOf(';') != -1 ) return false;
        //JOB_R
        if( getArg(getRequiredArgs()[3]).indexOf(';') != -1 ) return false;
        //Result1
        if( getArg(getRequiredArgs()[4]).indexOf(';') != -1 ) return false;
        //Min1
        temp = getArg( getRequiredArgs()[5] ).toUpperCase();
        //Min1 kann eines der Argumente JOBPAR_W[1..3] sein
        if((temp.equals(getRequiredArgs()[2]) == true ) || (temp.equals(getOptionalArgs()[0]) == true) ||
           (temp.equals(getOptionalArgs()[1]) == true)) {
          temp = getArg( temp );
          if (temp == null ) return false;
        }    
        
        //WAIT
        temp = getArg(getOptionalArgs()[3]);
        if (temp != null) {     
          try {
              if( Integer.parseInt( temp ) < 0 ) return false;
          } catch (NumberFormatException e) {
              return false;
          }                              
        }
      
      
      }
      return ok;
                                                                
    } catch (Exception e) {
      return false;
    } catch (Throwable e) {
      return false;
    }                     
  }


  /**
   * f�hrt die Pr�fprozedur aus
   * @param info Information zur Ausf�hrung
   */
  public void execute( ExecutionInfo info )
  {
    // immer notwendig
    Ergebnis result;
    Vector ergListe = new Vector();
    int status = STATUS_EXECUTION_OK;

    // spezifische Variablen
    String temp;
    String sgbd;
    String job=null;
    String jobpara=null;
    String jobres=null;
    String soll=null;
    long pause;
    
    try {
      //Parameter holen 
      try {
        if( checkArgs() == false ) throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
        sgbd    =  extractValues( getArg( getRequiredArgs()[0] ) )[0];
        job     =  getArg( getRequiredArgs()[1] );
        jobpara =  getArg( getRequiredArgs()[2] );
        temp = getArg( getOptionalArgs()[0] );
        if( temp != null ) jobpara = jobpara+";"+temp;
        temp = getArg( getOptionalArgs()[1] );
        if( temp != null ) jobpara = jobpara+";"+temp;
      } catch (PPExecutionException e) {
        if (e.getMessage() != null)
          result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
        else
          result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
        ergListe.add(result);
        throw new PPExecutionException();
      }

      try {
        //Schreiben
        temp = Ediabas.executeDiagJob(sgbd,job,jobpara,"JOB_STATUS");
        if( temp.equals("OKAY") == false) {
          result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
          ergListe.add(result);
          throw new PPExecutionException();
        } else {
          result = new Ergebnis( "Schreiben", "EDIABAS", sgbd, job, jobpara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
          ergListe.add(result);
        }
        
        temp = getArg(getOptionalArgs()[3]);
        if (temp != null) {
          pause = Long.parseLong( temp );
          try { Thread.sleep(pause); }
           catch (Exception e) {};                
        }
                                
        //Lesen
        job     =  getArg( getRequiredArgs()[3] );
        jobpara =  getArg( getOptionalArgs()[2] );
        if( jobpara == null ) jobpara = ""; 
        jobres  =  getArg( getRequiredArgs()[4] );
        temp = Ediabas.executeDiagJob(sgbd,job,jobpara,jobres);
        if( temp.equals("OKAY") == false) {
          result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
          ergListe.add(result);
          throw new PPExecutionException();
        }
        //Wert holen und checken
        temp = Ediabas.getDiagResultValue(jobres);
        soll = getArg( getRequiredArgs()[5] );
        if((soll.equalsIgnoreCase(getRequiredArgs()[2]) == true ) || (soll.equalsIgnoreCase(getOptionalArgs()[0]) == true) ||
           (soll.equalsIgnoreCase(getOptionalArgs()[1]) == true)) soll = getArg( soll.toUpperCase() );
        if( temp.equals(soll) == false) {
          result = new Ergebnis( "Lesen", "EDIABAS", sgbd, job, jobpara, jobres, temp, soll, "", "0", "", "", "", PB.getString( "toleranzFehler1" ), "", Ergebnis.FT_NIO );
          ergListe.add(result);
          throw new PPExecutionException();
        } else {
          result = new Ergebnis( "Lesen", "EDIABAS", sgbd, job, jobpara, jobres, temp, soll, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
          ergListe.add(result);
        }

      } catch( ApiCallFailedException e ) {
        result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), Ediabas.apiErrorCode()+": " + Ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
        ergListe.add(result);
        throw new PPExecutionException();
      } catch( EdiabasResultNotFoundException e ) {
        if (e.getMessage() != null)
          result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
        else
          result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
        ergListe.add(result);
        throw new PPExecutionException();
      }

    } catch (PPExecutionException e) {
      status = STATUS_EXECUTION_ERROR;
    } catch (Exception e) {
      result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
      ergListe.add(result);
      status = STATUS_EXECUTION_ERROR;
    } catch (Throwable e) {
      result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
      ergListe.add(result);
      status = STATUS_EXECUTION_ERROR;
    }                     

    setPPStatus( info, status, ergListe );
  }


}


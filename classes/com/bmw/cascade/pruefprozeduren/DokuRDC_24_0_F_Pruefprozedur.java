package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.DeviceIOException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.*;
import com.bmw.cascade.util.dom.DomPruefstand;

/**
 * Implementierung der Pr�fprozedur, die die RDC-Daten aus dem Steuerger�t abruft und
 * in einer Datei in XML-Form ablegt.
 * @author Staudinger, Weber
 * @version 0.0.1   SL  Implementierung <BR>
 *          0.0.9   TB  Bugfixing + Aenderungen <BR>
 *          0.1.0   TB  kleine Optimierung <BR>
 *          0.1.1   TB  Vergleich auf "0000000009" eingebaut <BR>
 *          0.1.2   LS  Jobanpassungen an RDC Gen2 US <BR>
 *          0.1.3   LS  FA Version von Version 0.1.2 <BR>
 *          0.1.4   LS  Job f�r Anzahl �berwachter R�der und Frequenz lesen <BR>
 *                      wird bei SA2VB nicht mehr ausgef�hrt <BR>
 *          0.1.5   LS  Debug implementiert und Fehler behoben<BR>
 *          0.1.6   NC  DOM Transfer korrigiert<BR>
 * 09.01.06 17.0.F  LS  Anpassung an RDC_53 --> Jobnamen und Parameter wurden
 *                      an RDC_60 angeglichen.
 * 10.01.06 17.1.F  LS  Entfernen der SA und Baureihenabh�ngigkeit durch 
 *                      Einf�hrung optionaler Parameter (KBUS und Gen1)
 * 23.01.06 18.0.F  LS  Nur Versionserh�hung, keine Inhaltliche �nderung
 *                      da 17.1 eine ung�ltige Zwischenversion w�re!
 * 24.03.07 19.0.T  FW  Neue Parameter f�r L6 hinzu, Jobnamen nun generisch zuweisbar
 * 31.03.07 20.0.F  FW  produktivversion von 19.0.T
 * 21.08.07 21.0.T  FW  corrected error tracing for jobs 
 * 21.08.07 22.0.F  FW  live version of 21.0.T
 * 23.08.08 23.0.T  FW  added new parameter to support RDC Gen3 and one to set frequency
 * 23.08.08 24.0.F  FW  live version of 23.0.T
 */
public class DokuRDC_24_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
	static final long serialVersionUID = 1L;
	
	//	language
	private final boolean isEnglish = System.getProperty( "user.language" ).equalsIgnoreCase( "EN" );
	  
	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
    public DokuRDC_24_0_F_Pruefprozedur() {}

      
    /**
     * erzeugt eine neue Pruefprozedur mit obigem Verhalten
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public DokuRDC_24_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    
    /**
     * initialsiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /**
     * liefert die zwingend erforderlichen Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = {"SGBD"};
        return args;
    }
    
    /**
     * liefert die optionalen Argumente
     */
    public String[] getOptionalArgs() {
        String[] args = {"DEBUG","KBUS","GEN1","UDS","GEN3",
        				 "JOB_SERIAL_NR","JOBPAR_SERIAL_NR","RES_SERIAL_NR",
        				 "JOB_FREQUENCY","JOBPAR_FREQUENCY","RES_FREQUENCY",
        				 "NUMBER_WHEELS",
        				 "JOB_ID_GENERIC","JOBPAR_ID_GENERIC","RES_ID",
        				 "USE_ID_CHECK","SET_FREQUENCY"};
        return args;
    }
    
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
     */
    public boolean checkArgs() {
        int i;
        
        try{
            // 1. Check: Sind alle requiredArgs gesetzt
            String[] requiredArgs = getRequiredArgs();
            for( i=0; i<requiredArgs.length; i++ ) {
                if( getArg(requiredArgs[i]) == null ) return false;
                if( getArg(requiredArgs[i]).equals("") == true ) return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        } catch (Throwable e) {
            return false;
        }
    }
    
    
    /**
     * f�hrt die Pr�fprozedur aus
     */
    public void execute( ExecutionInfo info ) {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        // Ediabas Device
        EdiabasProxyThread ed = null;
        
        // spezifische Variablen
        String sgbd                     ="";
        String temp                     ="";
        String job                      ="";
        String jobpar                     ="";
        String jobres                   ="";
        
        String JobFrequenzAuslesen;
        String JobRadIdAuslesen;
        String JobAnzUebwRaeder;
        String JobSeriennummerLesen;
        
        String jobParFrequenzAuslesen;
        String jobParRadIdAuslesen;
        String jobParSeriennummerLesen;
        
        String resultJobFrequenzAuslesen;
        String resultJobAnzUebwRaeder;
        String resultJobSeriennummer;
        String resultJobRadId;
        
        String resultFrequency="";
        String resultNumberRDCCodes;
        String resultSerialnumber;
        
        Vector RadIds = new Vector();        
               
        int  NumberRDCCodes = 4; //STANDARD immer 4, nur E67 muss ausgelesen werden
        
        boolean bDEBUG  = false;
        boolean bKBUS   = false;
        boolean bGEN1   = false;
        boolean bGEN3   = false;
        boolean bUDS   = false;
        boolean bFreqSet   = false;
        boolean useIdCheck = false;
        
               
        StringBuffer output = null;        
        
        int i;
      
        // Jobs- und Resultnamen in Abh�ngigkeit der optionalen Parameter festlegen
        // Allgemeing�ltige Jobs und Results f�r KBUS, CAN Gen2 und CAN Gen1 (E67) Steuerger�te
        
        JobRadIdAuslesen           ="STATUS_MESSDATEN_BLOCK%WHEELNUMBER%_LESEN";    //Job f�r Ergebnis-Rad-ID, generisch
        resultJobRadId              ="STAT_RE_IDENTIFIER";          //Ergebnis Rad-Id
        jobParRadIdAuslesen         ="";          					//jobpar
        
        JobFrequenzAuslesen         ="";                            //Bei RDC Gen2 immer 433 MHz
        resultJobFrequenzAuslesen   ="";                            //Job und Result nur bei Gen1 (RDC alt) vorhanden
        jobParFrequenzAuslesen      ="";          					//jobpar
            
        JobAnzUebwRaeder            ="";                            //Bei RDC Gen2 immer 4 �berwachte R�der
        resultJobAnzUebwRaeder      ="";                            //Job und Result nur bei Gen1 (RDC alt) vorhanden

        
        try { //Parameter holen und pruefen
            try {
                if( checkArgs() == false ) throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
                
                //sgbd holen
                sgbd  = getArg( "SGBD");
                
                // DEBUG
                String sArg = getArg("DEBUG");
                if( sArg != null && sArg.length() > 0) {
                    bDEBUG = sArg.equalsIgnoreCase("TRUE");
                }
                
                // KBUS
                sArg = getArg("KBUS");
                if( sArg != null && sArg.length() > 0) {
                    bKBUS = sArg.equalsIgnoreCase("TRUE");
                }
                
                // GEN1
                sArg = getArg("GEN1");
                if( sArg != null && sArg.length() > 0) {
                    bGEN1 = sArg.equalsIgnoreCase("TRUE");
                }
                
                // GEN3
                sArg = getArg("GEN3");
                if( sArg != null && sArg.length() > 0) {
                    bGEN3 = sArg.equalsIgnoreCase("TRUE");
                }
                
                // UDS
                sArg = getArg("UDS");
                if( sArg != null && sArg.length() > 0) {
                    bUDS = sArg.equalsIgnoreCase("TRUE");
                }
                
                // Frequency
                sArg = getArg("SET_FREQUENCY");
                if( sArg != null && sArg.length() > 0) {
                    bFreqSet = true;
                    resultFrequency = sArg;
                }
                
                // -----------------------------------------------------------------
                //set parameters dependent on general settings
                
                // Jobs- und Resultnamen in Abh�ngigkeit der optionalen Parameter festlegen
                if (bGEN1) { //RDC_E65.prg SGBD nur f�r RDC Generation 1 => Serie E67
                    
                    JobFrequenzAuslesen         ="STATUS_FREQUENZ_LESEN";   //Job f�r Auslesen der Frequenz
                    resultJobFrequenzAuslesen   ="STAT_FREQUENZ_WERT";      //Ergebnis f�r Frequenz verbautes RDC
                    
                    JobAnzUebwRaeder             ="STATUS_ANZ_RAD" ;         //Job f�r Anzahl �berwachter R�der mit RDC
                    resultJobAnzUebwRaeder       ="STAT_ANZ_VERB_RAD";       //Ergebnis f�r Anzahl �berwachter R�der mit RDC
                }
                if (bUDS) { // bei F03 muss Frequenz ausgelesen werden
                    
                	JobRadIdAuslesen               ="STATUS_LESEN";    			//Job f�r Ergebnis-Rad-ID, generisch
                    resultJobRadId                 ="STAT_RE_IDENTIFIER";          //Ergebnis Rad-Id
                    if (bGEN3) jobParRadIdAuslesen ="ARG;STATUS_MESSDATENBLOCK_GEN3_%WHEELNUMBER%"; //jobpar
                    else       jobParRadIdAuslesen ="ARG;STATUS_MESSDATENBLOCK%WHEELNUMBER%_LESEN"; //jobpar
                	
                	JobFrequenzAuslesen                ="STATUS_LESEN";   			//Job f�r Auslesen der Frequenz
                	resultJobFrequenzAuslesen          ="STAT_FREQUENZ_WERT";      //Ergebnis f�r Frequenz verbautes RDC
                	if (bGEN3) jobParFrequenzAuslesen  ="ARG;STATUS_RDC_GEN3_LESEN"; //JobPar f�r Auslesen der Frequenz
                    else  jobParFrequenzAuslesen       ="ARG;STATUS_IO_RDC_LESEN"; //JobPar f�r Auslesen der Frequenz     
                } 
                
                if (bKBUS) { //Bei K-Bus wird BMW_NR statt Seriennummer ausgelesen da Job SERIENNUMMER_LESEN nicht vorhanden
                    
                    JobSeriennummerLesen        ="IDENT";               //Seriennummer ist implements RDC-Steuerger�t nicht angelegt
                    resultJobSeriennummer       ="ID_BMW_NR";           //Seriennummer kann nicht ausgelesen werden, statt dessen die BMW-Nr.
                    jobParSeriennummerLesen     ="";					//jobPar
                } 
                else {                    
                    JobSeriennummerLesen        ="SERIENNUMMER_LESEN";      //Job Seriennummer aus RDC-Steuerger�t lesen
                    resultJobSeriennummer       ="SERIENNUMMER";            //Ergebnis Seriennummer aus RDC-Steuerger�t
                    jobParSeriennummerLesen     ="";						//jobPar
                    if (bGEN3) {
                        JobRadIdAuslesen          ="STATUS_MESSDATENBLOCK_GEN3_%WHEELNUMBER%";    //Job f�r Ergebnis-Rad-ID, generisch
                       	JobFrequenzAuslesen       ="STATUS_RDC_GEN3_LESEN";   			//Job f�r Auslesen der Frequenz
                    	resultJobFrequenzAuslesen ="STAT_FREQUENZ_WERT";      //Ergebnis f�r Frequenz verbautes RDC
                    }
                }
                
                // -----------------------------------------------------------------
                // define jobs individually - normally not needed 
                
                // JOB_SERIAL_NR
                sArg = getArg("JOB_SERIAL_NR");
                if( sArg != null && sArg.length() > 0) {
                	JobSeriennummerLesen = sArg;
                }
                // JOBPAR_SERIAL_NR
                sArg = getArg("JOBPAR_SERIAL_NR");
                if( sArg != null && sArg.length() > 0) {
                	jobParSeriennummerLesen = sArg;
                }
                // RES_SERIAL_NR
                sArg = getArg("RES_SERIAL_NR");
                if( sArg != null && sArg.length() > 0) {
                	resultJobSeriennummer = sArg;
                }
                
                // JOB_FREQUENCY
                sArg = getArg("JOB_FREQUENCY");
                if( sArg != null && sArg.length() > 0) {
                	JobFrequenzAuslesen = sArg;
                }
                // JOBPAR_FREQUENCY
                sArg = getArg("JOBPAR_FREQUENCY");
                if( sArg != null && sArg.length() > 0) {
                	jobParFrequenzAuslesen = sArg;
                }
                // RES_FREQUENCY
                sArg = getArg("RES_FREQUENCY");
                if( sArg != null && sArg.length() > 0) {
                	resultJobFrequenzAuslesen = sArg;
                }
                
                // JOB_ID_GENERIC
                sArg = getArg("JOB_ID_GENERIC");
                if( sArg != null && sArg.length() > 0) {
                	JobRadIdAuslesen = sArg;
                }
                // JOBPAR_ID_GENERIC
                sArg = getArg("JOBPAR_ID_GENERIC");
                if( sArg != null && sArg.length() > 0) {
                	jobParRadIdAuslesen = sArg;
                }
                // RES_ID
                sArg = getArg("RES_ID");
                if( sArg != null && sArg.length() > 0) {
                	resultJobRadId = sArg;
                }
                
                sArg = getArg( "NUMBER_WHEELS" );
				if( sArg != null ) {
					int tempInt = Integer.parseInt( sArg );
					if( tempInt >= 1 && tempInt <= 5 )
						NumberRDCCodes = tempInt;
				}
				// ID Check
                sArg = getArg("USE_ID_CHECK");
                if( sArg != null && sArg.length() > 0) {
                	useIdCheck = sArg.equalsIgnoreCase("TRUE");
                }
            } 
            catch (PPExecutionException e) {
                
                if (e.getMessage() != null) result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
                
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            // neue Methode, Ediabas aufzurufen
            try {
				ed = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();// new
			} catch( Exception e1 ) {
				if( isEnglish ) {
					throw new DeviceIOException( "error getting EDIABAS device: " + e1.getMessage() );
				} else {
					throw new DeviceIOException( "Fehler beim Holen des Device EDIABAS: " + e1.getMessage() );
				}
			}
            
            try { // Jobs rausballern
                
                // Seriennummer des verbauten RDC-Steuerger�tes auslesen
            	job=JobSeriennummerLesen;
                jobpar=jobParSeriennummerLesen;
                jobres=resultJobSeriennummer;
            	temp = ed.executeDiagJob(sgbd,job,jobpar,"");
                if( temp.equals("OKAY") == false ) {
                    result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
                    ergListe.add(result);
                    throw new PPExecutionException();
                }
                else {
                    resultSerialnumber = ed.getDiagResultValue(jobres); // Ergebnis intern
                }
                
                // Frequenz des verbauten RDC-Systems auslesen
                if (!bFreqSet) {
	                if (bGEN1) { // Nur wenn E67 mit Serienausstattung RDC Gen1 (RDC alt)
	                    if (bDEBUG) System.out.println("Frequenz lesen, RDC Gen1, E67 : " + JobFrequenzAuslesen);
	                    job=JobFrequenzAuslesen;
	                    jobpar="";
	                    jobres=resultJobFrequenzAuslesen;
	                    temp = ed.executeDiagJob(sgbd,job,jobpar,"");
	                    
	                    if( temp.equals("OKAY") == false ) {
	                        result = new Ergebnis( "DiagFehler", "EDIABAS", job,jobpar, "", "", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
	                        ergListe.add(result);
	                        throw new PPExecutionException();
	                    }
	                    else {
	                        resultFrequency = ed.getDiagResultValue(jobres); // Ergebnis intern
	                        if (bDEBUG) System.out.println("Frequenz RDC Gen1 : " + resultFrequency);
	                    }
	                }
	                else if (bUDS||bGEN3) { // Bei UDS Auslesen da F03 wohl mit 315MHz
	                    if (bDEBUG) System.out.println("Frequenz lesen, UDS : " + JobFrequenzAuslesen);
	                    job=JobFrequenzAuslesen;
	                    jobpar=jobParFrequenzAuslesen;
	                    jobres=resultJobFrequenzAuslesen;
	                    temp = ed.executeDiagJob(sgbd,job,jobpar,"");
	                    
	                    if( temp.equals("OKAY") == false ) {
	                        result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job,jobpar, "", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
	                        ergListe.add(result);
	                        throw new PPExecutionException();
	                    }
	                    else {
	                        resultFrequency = ed.getDiagResultValue(jobres); // Ergebnis intern
	                        int tempInt = resultFrequency.indexOf('.');
	                        if (tempInt>-1) resultFrequency = resultFrequency.substring(0, tempInt);
	                        if (bDEBUG) System.out.println("Frequenz RDC UDS : " + resultFrequency);
	                    }
	                }
	                else {
	                    resultFrequency = "433"; //Bei RDC Gen2 (SA2VB) ist Frequenz immer 433MHz
	                    if (bDEBUG) System.out.println("Frequenz SA2VB : " + resultFrequency);
	                }
                }
                // Anzahl der mit RDC verbauten R�der auslesen
                if (bGEN1) { // Nur wenn E67 mit Serienausstattung RDC Gen1 (RDC alt)
                    job=JobAnzUebwRaeder;
                    jobpar="";
                    jobres=resultJobAnzUebwRaeder;
                    temp = ed.executeDiagJob(sgbd,job,jobpar,"");
                    if( temp.equals("OKAY") == false ) {
                        
                        result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job,jobpar, "", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
                        ergListe.add(result);
                        throw new PPExecutionException();
                    }
                    else {
                        resultNumberRDCCodes = ed.getDiagResultValue(jobres); // Ergebnis intern
                        try {
                            NumberRDCCodes = Integer.parseInt(resultNumberRDCCodes);
                        }
                        catch (Exception e){
                            throw new PPExecutionException();
                        }
                    }
                }
                
                //immer 4 �berwachte R�der bei RDC Gen>1 (SA2VB)
             
                // Identifier der Radelektroniken auslesen (1-4 bzw. 1-5)
                if (bDEBUG) System.out.println("RadIDs lesen");
                if (bDEBUG) System.out.println("NumberRDCCodes : " + NumberRDCCodes);
                // Allgemeing�ltige Jobs und Results f�r KBUS, CAN Gen2 und CAN Gen1 (E67) Steuerger�te
                
                for (i=1;i<= NumberRDCCodes;i++) {

                    // replace placeholder %WHEELNUMBER% with wheel number
                    job = JobRadIdAuslesen;
                    if (job.indexOf( "%WHEELNUMBER%" )>=0) job = job.replaceAll( "%WHEELNUMBER%", ""+i );
                    jobpar = jobParRadIdAuslesen;
                    if (jobpar.indexOf( "%WHEELNUMBER%" )>=0) jobpar = jobpar.replaceAll( "%WHEELNUMBER%", ""+i );
                    jobres = resultJobRadId;
                    
                    if (bDEBUG) System.out.println("Case : " +i);
                    if (bDEBUG) System.out.println("ID Lesen Job : " +job);
                    temp = ed.executeDiagJob(sgbd,job,jobpar,"");
                    
                    if(temp.equals("OKAY") == false) {
                        result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
                        ergListe.add(result);
                        throw new PPExecutionException();
                    }
                    else {
                        temp = ed.getDiagResultValue(jobres); // Ergebnis intern
                        RadIds.add(temp);
                        // Rad ID Check
                        if (useIdCheck && (temp.matches( "[a-zA-Z0-9\\-]+" ) == false || temp.length()<1)) {
                        	status = STATUS_EXECUTION_ERROR;
                        	if (temp.length()<1) {
                              	if (isEnglish) result = new Ergebnis( "RadIDFehler"+i, "EDIABAS", sgbd, job, jobpar, "TPM Wheel ID"+i, temp, "", "", "0", "", "", "", "RadId contains no characters", "", Ergebnis.FT_NIO );
                            	else result = new Ergebnis( "RadIDFehler"+i, "EDIABAS", sgbd, job, jobpar, "RDC Rad ID"+i, temp, "", "", "0", "", "", "", "RadId enth�lt keine Zeichen", "", Ergebnis.FT_NIO );		
                         	}
                        	else {
                               	if (isEnglish) result = new Ergebnis( "RadIDFehler"+i, "EDIABAS", sgbd, job, jobpar, "TPM Wheel ID"+i, temp, "", "", "0", "", "", "", "RadId contains faulty characters", "", Ergebnis.FT_NIO );
                            	else result = new Ergebnis( "RadIDFehler"+i, "EDIABAS", sgbd, job, jobpar, "RDC Rad ID"+i, temp, "", "", "0", "", "", "", "RadId enth�lt fehlerhafte Zeichen", "", Ergebnis.FT_NIO );		
                        	}
                        }
                        // RAD ID Check ok or no check at all -> APDM result
                        else {
                          	if (isEnglish) result = new Ergebnis( "RadID"+i, "EDIABAS", sgbd, job, jobpar, "TPM Wheel ID"+i, temp, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                        	else result = new Ergebnis( "RadID"+i, "EDIABAS", sgbd, job, jobpar, "RDC Rad ID"+i, temp, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );		                   	
                        }
                        ergListe.add(result);
                    }
                }
            }
         
            catch( ApiCallFailedException e ) {
                result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, jobres, "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode()+": " + ed.apiErrorText(), Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            catch( EdiabasResultNotFoundException e ) {
                if( e.getMessage() != null )
                    result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, jobres, "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, jobres, "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            //Infos in StringBuffer �bernehmen
            String az = "\"";
            output = new StringBuffer();
            output = output.append("<?xml version="+az+"1.0"+az+" encoding="+az+"iso-8859-1"+az+"?>"+"\r\n");
            output = output.append("<!DOCTYPE vehicleRDC SYSTEM "+az+"vehicleRDC.dtd"+az+">"+"\r\n");
            output = output.append("<vehicleRDC>"+"\r\n");
            output = output.append("\t"+"<vehicle vinShort="+az+getPr�fling().getAuftrag().getFahrgestellnummer7()+az+">\r\n");
            output = output.append("\t\t"+"<frequency>"+resultFrequency+"</frequency>"+"\r\n");
            output = output.append("\t\t"+"<serialNoControlUnit>"+resultSerialnumber+"</serialNoControlUnit>"+"\r\n");
            output = output.append("\t\t"+"<rdcCodes>"+"\r\n");
            
            if (bDEBUG) System.out.println("Gr��e RadIDs Array: " + RadIds.size());
            
            for(i=0;i<RadIds.size();i++) {
                // gelesene RE-ID muss bis auf 10 Stellen mit fuehrenden Nullen aufgefuellt werden
                String idXML="0000000000";
                idXML = idXML + RadIds.elementAt(i);
                idXML = idXML.substring(idXML.length()-10, idXML.length());
                if (idXML.equalsIgnoreCase("0000000000")) continue; // fw: changed to be complete zeros
                output = output.append("\t\t\t"+"<rdcCode>"+idXML+"</rdcCode>"+"\r\n");
                
                if (bDEBUG) System.out.println("Index i : " + i);
                if (bDEBUG) System.out.println("idXML : " + idXML);
                //output = output.append("\t\t\t"+"<rdcCode>"+(String)(RadIds.elementAt(i))+"</rdcCode>"+"\r\n");
            }
            output = output.append("\t\t"+"</rdcCodes>"+"\r\n");
            output = output.append("\t"+"</vehicle>"+"\r\n");
            output = output.append("</vehicleRDC>"+"\r\n");
            
            
            // Schreiben an DOM Datenbank
            try {
            	DomPruefstand.writeDomData( getPr�fling().getAuftrag().getFahrgestellnummer7(), "R", output.toString().getBytes() );
            } catch(Exception e) {
                result = new Ergebnis( "ExecFehler", "DomTransfer", "", "", "", "", "", "", "", "0", "", "", "", "FileWrite", ""+e.getMessage(), Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            //IO-Ablauf als Status festhalten
            result = new Ergebnis("Status", "", "", "", "", "STATUS", ""+status, ""+STATUS_EXECUTION_OK, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
            ergListe.add(result);
            
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
        setPPStatus( info, status, ergListe );
    }
    
}


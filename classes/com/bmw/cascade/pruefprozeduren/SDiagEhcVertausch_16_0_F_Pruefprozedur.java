package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.ediabas.*;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;

/**
 * Implementierung von EHC (S_Vertau) INPA-Sonderskript. Diese Pr�fprozedur sichert den Anschluss
 * der Luftfederung gegen Vertauschung von Kabel und Schl�uchen ab und muss f�r jedes (luftgefederte)
 * Rad einmal aufgerufen werden.
 * <BR>
 * @author Crichton, Helmich, Schumann, Plath, Buboltz
 * 
 * @version  Implementierung
 * @version  1_0_F 29.11.2002  SH  Ersterstellung auf Basis SDiagEhcTest
 * @version  2_0_F 20.05.2003  TB  unn�tige Kommentare raus, Betrag bei der Differenzauswertung eingebaut (nach Absprache mit S.H.) 
 * @version  4_0_F 21.05.2003  TB  Betrag bei der Differenzauswertung entfernt -> wuerde nur zu NIO fuehren, wenn 0!
 * @version  5_0_F 11.04.2008  CS  Erweiterung auf 4 optionale Argumente PARAM2_JOB2, PARAM1_JOB1, DELAY1 und DELAY2
 * @version  7_0_F 23.09.2008  UP  Optionales Argument "PARAM2_JOB2" entfernt, da nicht mehr n�tig bei RR4 ab I300, Variablentyp f�r
 * 								   Fastfilterwerte von Integer auf Float ge�ndert
 * @version  8_0_F 14.01.2014  UP  Neue optionale Parameter zur Invertierung des Ergebnissen und zur Steuerung des Benutzerdialogs hinzugef�gt
 *                                 Benennung der Ergebnisse angepasst
 * @version  9_0_F 28.01.2014  UP  Ein neuer optionaler Parameter MIN_VALUE zur Steuerung der Differenz-Bewertung wurde hinzugef�gt.
 * @version 10_0_T 11.03.2015  UP  Ein neuer optinaler Parameter HWT wurde zur Anzeige von Fehlertexten hinzugef�gt.
 * @version 11_0_F 17.03.2015  UP  Produktivversion mit den �nderungen aus v10_0_T (Hinweistexte)
 * @version 12_0_T 14.07.2016  UP  �ber die Jobargumente kann jetzt auch eine SGBD �bergeben werden. Dies ist f�r die
 *                                 Vertauscherpr�fung mit dem CHC notwendig.
 * @version 13_0_F 19.07.2016  UP  Freigabe der Testversion 12_0_T, zus�tzliche Log-Ausgaben wurden entfernt.
 * @version 14_0_F 02.08.2016  UP  Die Jobs funktionieren jetzt auch ohne explizite Angabe einer SGBD im Job1/Job2-Parameter
 * @version 15_0_T 22.08.2016  TB  Erweiterungen f�r Paralleldiagnose analog zur DiagToleranzPP (LOP 2015), Generics eingebaut + Compiler Warnungen behoben <BR>
 * @version 16_0_F 25.01.2017  MKe F-Version
 */

public class SDiagEhcVertausch_16_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public SDiagEhcVertausch_16_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public SDiagEhcVertausch_16_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialisiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "PARAM1_JOB1", "DELAY1", "DELAY2", "INVERT", "MESSAGE", "MIN_VALUE", "HWT", "TAG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "SGBD", "JOB1", "ERG1_JOB1", "ERG2_JOB1", "JOB2", "PARAM1_JOB2" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	@Override
	public boolean checkArgs() {
		boolean ok;
		String strtemp;
		String sgdb;

		try {
			// SGBD, Jobs und Argumente muessen existieren
			ok = super.checkArgs();
			if( ok == true ) {

				//SGDB vorhanden?
				sgdb = getArg( getRequiredArgs()[0] );
				if( sgdb != null ) {

				} else {
					return false;
				}

				//Job1 vorhanden?
				strtemp = getArg( getRequiredArgs()[1] );
				if( strtemp != null ) {

				} else {
					return false;
				}

				// ERG1_JOB1 vorhanden?
				strtemp = getArg( getRequiredArgs()[2] );
				if( strtemp != null ) {

				} else {
					return false;
				}

				//JOB2 vorhanden?
				strtemp = getArg( getRequiredArgs()[3] );
				if( strtemp != null ) {

				} else {
					return false;
				}

				//PARAM1_JOB2 vorhanden?
				strtemp = getArg( getRequiredArgs()[4] );
				if( strtemp != null ) {

				} else {
					return false;
				}

				//wenn sgdb ungleich "EhcRR2" ist, muss der
				//optionale Parameter PARAM1_JOB1 vorhanden sein,
				//PARAM1_JOB1 muss f�r alle Baureihen, die das 
				//UDS Bordnetz verwenden bef�llt sein (z.B. RR4)
				if( !sgdb.equals( "EHC2RR" ) ) {
					strtemp = getArg( getOptionalArgs()[0] );
					if( strtemp != null ) {
					} else {
						return false;
					}
				}

				//MIN_VALUE muss eine Zahl sein
				if( getArg( getOptionalArgs()[5] ) != null ) {
					try {
						Double.parseDouble( getArg( getOptionalArgs()[5] ) );
					} catch( NumberFormatException e ) {
						System.out.println( "[SDiagEhcVertausch] Check of arguments failed on parameter MIN_VALUE!" );
						return false;
					}
				}
			}
			return ok;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {

		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String sgbd = null;
		String sgbdJob1 = "";
		String sgbdJob2 = "";
		String temp;
		String job1 = "";
		String erg1_job1 = "";
		String erg2_job1 = "";
		String job2 = "";
		String param1_job1 = "";
		String param1_job2 = "";
		String hwt = (getArg( "HWT" ) != null) ? getArg( "HWT" ) : "";

		String delay1 = "";
		String delay2 = "";
		boolean invert = false;
		String message = "";

		double fafi_hinten_links_1 = -999.9;
		double fafi_hinten_links_2 = -999.9;
		double fafi_hinten_rechts_1 = -999.9;
		double fafi_hinten_rechts_2 = -999.9;
		double difference;
		double minValue;

		//Ediabas
		EdiabasProxyThread ediabas = null;

		//Paralleldiagnose
		boolean parallel = false;
		String tag = null;
		DeviceManager devMan = null;

		//Sprachauswahl
		int lang = System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) ? 1 : 0; // 0: English, 1: German

		try {

			/* Pr�fen ob Argumente stimmen
			 */
			try {
				if( !checkArgs() ) {
					System.out.println( "Check of Arguments failed!" );
					throw new PPExecutionException();
				}
				sgbd = getArg( getRequiredArgs()[0] );

				// 'JOB1' auslesen und ggf. zerlegen in SGBD und Job
				job1 = getArg( getRequiredArgs()[1] );
				if( job1.indexOf( ';' ) != -1 ) {
					sgbdJob1 = splitArg( job1 )[0];
					job1 = splitArg( job1 )[1];
				}

				erg1_job1 = getArg( getRequiredArgs()[2] );
				erg2_job1 = getArg( getRequiredArgs()[3] );

				// 'JOB2' auslesen und ggf. zerlegen in SGBD und Job
				job2 = getArg( getRequiredArgs()[4] );
				if( job2.indexOf( ';' ) != -1 ) {
					sgbdJob2 = splitArg( job2 )[0];
					job2 = splitArg( job2 )[1];
				}

				param1_job2 = getArg( getRequiredArgs()[5] );

				try {
					param1_job1 = getArg( getOptionalArgs()[0] );
				} catch( Exception e ) {
					System.out.println( "Parameter PARAM1_JOB1 not found, replaced with \"\"" );
				}
				if( param1_job1 == null )
					param1_job1 = "";

				try {
					delay1 = getArg( getOptionalArgs()[1] );
				} catch( Exception e ) {
					System.out.println( "Parameter DELAY1 not found, replaced with 1000[ms]" );
				}
				if( delay1 == null )
					delay1 = "1000";

				try {
					delay2 = getArg( getOptionalArgs()[2] );
				} catch( Exception e ) {
					System.out.println( "Parameter DELAY2 not found, replaced with 10000[ms]" );
				}
				if( delay2 == null )
					delay2 = "10000";

				invert = (getArg( getOptionalArgs()[3] ) != null && getArg( getOptionalArgs()[3] ).equalsIgnoreCase( "true" )) ? true : false;
				message = (getArg( getOptionalArgs()[4] ) != null) ? getArg( getOptionalArgs()[4] ) : PB.getString( "ehcWirdAngesteuert" );
				minValue = (getArg( getOptionalArgs()[5] ) != null) ? Double.parseDouble( getArg( getOptionalArgs()[5] ) ) : 0.0;

			} catch( Exception e ) {
				e.printStackTrace();
				result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", hwt + PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Paralleldiagnose, Pr�fstandvariabel auslesen, ob Paralleldiagnose 
			try {
				Object pr_var;

				pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
				if( pr_var != null ) {
					if( pr_var instanceof String ) {
						if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
							parallel = true;
						}
					} else if( pr_var instanceof Boolean ) {
						if( ((Boolean) pr_var).booleanValue() ) {
							parallel = true;
						}
					}
				}
			} catch( VariablesException e ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				throw new PPExecutionException();
			}

			// Devicemanager
			devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();

			if( getArg( "TAG" ) != null )
				tag = getArg( "TAG" );

			if( parallel ) // Ediabas holen
			{
				try {
					ediabas = devMan.getEdiabasParallel( tag, sgbd );
				} catch( Exception ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Wrong Cascade configuration for parallel diagnosys", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				} catch( Throwable ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				}
			} else {
				try {
					ediabas = devMan.getEdiabas( tag );
				} catch( Exception ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();

				}
			}
			// <<<<< Paralleldiagnose            

			/* --------------------------------- */
			/* --- Filterwerte lesen (Job 1) --- */
			/* --------------------------------- */

			// SGBD-Namen bei Bedarf ersetzen
			sgbd = sgbdJob1.length() == 0 ? getArg( getRequiredArgs()[0] ) : sgbdJob1;

			try {
				temp = ediabas.executeDiagJob( sgbd, job1, param1_job1, "" );
				if( temp.equals( "OKAY" ) == false ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job1, param1_job1, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", hwt + PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} else {
					// Linke-Seite messen
					fafi_hinten_links_1 = Double.parseDouble( ediabas.getDiagResultValue( erg1_job1 ) );
					result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job1, param1_job1, erg1_job1, Double.toString( fafi_hinten_links_1 ), "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
					// Rechte-Seite messen          
					fafi_hinten_rechts_1 = Double.parseDouble( ediabas.getDiagResultValue( erg2_job1 ) );
					result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job1, param1_job1, erg2_job1, Double.toString( fafi_hinten_rechts_1 ), "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				}
			} catch( ApiCallFailedException e ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job1, param1_job1, "", "", "", "", "0", "", "", "", hwt + PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job1, param1_job1, "", "", "", "", "0", "", "", "", hwt + PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job1, param1_job1, "", "", "", "", "0", "", "", "", hwt + PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( NumberFormatException e ) {
				System.out.println( "NumberFormatException occurred during reading of fastfilter values" );
				result = new Ergebnis( "ExecError", "SDiagEHCVertausch", "", "", "", "", "", "", "", "0", "", "", "", hwt + "Exception: " + (e.getMessage() != null ? e.getMessage() : ""), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			}

			/* Pause (kopiert von INPA)
			 */
			try {
				Thread.sleep( new Integer( delay1 ).longValue() );
			} catch( Exception e ) {
			}

			/* ------------------------------------- */
			/* --- Kompressoransteuerung (Job 2) --- */
			/* ------------------------------------- */

			// SGBD-Namen bei Bedarf ersetzen
			sgbd = sgbdJob2.length() == 0 ? getArg( getRequiredArgs()[0] ) : sgbdJob2;

			try {
				temp = ediabas.executeDiagJob( sgbd, job2, param1_job2, "" );
				if( temp.equals( "OKAY" ) == false ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job2, param1_job2, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", hwt + PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} else {
					result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job2, param1_job2, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				}
			} catch( ApiCallFailedException e ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job2, param1_job2, "", "", "", "", "0", "", "", "", hwt + PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job2, param1_job2, "", "", "", "", "0", "", "", "", hwt + PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job2, param1_job2, "", "", "", "", "0", "", "", "", hwt + PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			/* Nachricht auf Bildschirm bringen
			 */
			try {
				getPr�flingLaufzeitUmgebung().getUserDialog().displayMessage( PB.getString( "meldung" ), message, -1 );
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", hwt + "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", hwt + "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", hwt + "Exception", "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				e.printStackTrace();
				throw new PPExecutionException();
			}

			/* Pause (kopiert von INPA)
			 * Warte fuer 10 sekunden wahrend der Ansteuerung...
			 */
			try {
				Thread.sleep( new Integer( delay2 ).longValue() );
			} catch( Exception e ) {
			}

			/* Nachricht von Bildschirm loeschen
			 */
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", hwt + "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", hwt + "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", hwt + "Exception", "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				e.printStackTrace();
				throw new PPExecutionException();
			}

			/* --------------------------------- */
			/* --- Filterwerte lesen (Job 1) --- */
			/* --------------------------------- */

			// SGBD-Namen bei Bedarf ersetzen
			sgbd = sgbdJob1.length() == 0 ? getArg( getRequiredArgs()[0] ) : sgbdJob1;

			try {
				temp = ediabas.executeDiagJob( sgbd, job1, param1_job1, "" );
				if( temp.equals( "OKAY" ) == false ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job1, param1_job1, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", hwt + PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} else {
					// Linke-Seite messen
					fafi_hinten_links_2 = Double.parseDouble( ediabas.getDiagResultValue( erg1_job1 ) );
					result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job1, param1_job1, erg1_job1, Double.toString( fafi_hinten_links_2 ), "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
					// Rechte-Seite messen          
					fafi_hinten_rechts_2 = Double.parseDouble( ediabas.getDiagResultValue( erg2_job1 ) );
					result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job1, param1_job1, erg2_job1, Double.toString( fafi_hinten_rechts_2 ), "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				}
			} catch( ApiCallFailedException e ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job1, param1_job1, "", "", "", "", "0", "", "", "", hwt + PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job1, param1_job1, "", "", "", "", "0", "", "", "", hwt + PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job1, param1_job1, "", "", "", "", "0", "", "", "", hwt + PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				e.printStackTrace();
				throw new PPExecutionException();
			}

			/* Analyse von Ergebnissen:
			 * Formel: Die Differenz der linken Werte minus der Differenz der rechten Werte, mu� > 0 sein!
			 */
			try {
				difference = (fafi_hinten_links_2 - fafi_hinten_links_1) - (fafi_hinten_rechts_2 - fafi_hinten_rechts_1);

				// berechneten Wert bei Bedarf invertieren
				if( invert ) {
					difference *= -1;
				}

				if( difference >= minValue ) {
					result = new Ergebnis( "Analyse", "SDiagEhcVertausch", "", "", "", lang == 0 ? "Regulation difference" : "Regeldifferenz", Double.toString( difference ), Double.toString( minValue ), "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				} else {
					result = new Ergebnis( "Analyse", "SDiagEhcVertausch", "", "", "", lang == 0 ? "Regulation difference" : "Regeldifferenz", Double.toString( difference ), Double.toString( minValue ), "", "0", "", "", "", hwt + PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
			} catch( Exception e ) {
				result = new Ergebnis( "AnalyseFehler", "EDIABAS", sgbd, job1, param1_job1, "", "", "", "", "0", "", "", "", hwt + PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				e.printStackTrace();
				throw new PPExecutionException();
			}

		} catch( PPExecutionException e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", hwt + PB.getString( "ausfuehrungsfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", hwt + PB.getString( "unerwarteterLaufzeitfehler" ), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			e.printStackTrace();
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", hwt + PB.getString( "unerwarteterLaufzeitfehler" ), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Paralleldiagnose
		if( parallel ) {
			try {
				devMan.releaseEdiabasParallel( ediabas, tag, sgbd );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable t ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		setPPStatus( info, status, ergListe );
	} // End of execute()
} // End of Class

package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.framework.udnext.UDNHandle;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.ImageLocation;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.UDNButtonConfiguration;
import com.bmw.cascade.pruefstand.framework.udnext.UserDialogNextRuntimeEnvironment;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.UDNState;

/**
 * Implementierung der Pr�fprozedur, die auf eine Eingabe durch den Benutzer wartet.
 * 
 * @author M�ller
 * @version Historie WerkerEingabe PP: 0_0_1 06.03.2003 MM Implementierung
 * @version Historie WerkerEingabe PP: 0_0_2 17.05.2005 RG Optionale Parameter MIN und MAX dazu. Wenn nur MIN angegeben ist
 *          wird ein Stringvergleich durchgefuehrt.
 * @version Historie WerkerEingabe PP: 3_0_T 01.03.2006 RG Angabe mehrerer Mins m�glich f�r Stringvergleich, �berpr�fung der
 *          Argumente jetzt in checkArgs() <br>
 * @version Historie WerkerEingabe PP: 4_0_T 29.05.2006 TB Erlaube bei allen Parametern den @-Referenzparameter <BR> 
 * @version Historie WerkerEingabe PP: 5_0_F 10.07.2007 RG, TB Merge beider T-Versionen und F-Release <BR>      
 * @version 2_0_T 13.02.2014 TB Anforderung W6, dass der Style (gr�n, rot, ..) ebenfalls parametriert werden kann, simple �bernahme aus der WerkerAnweisungUDN PP <BR>       
 * @version 3_0_F 13.02.2014 TB F-Version <BR> 
 */
public class WerkerEingabeUDN_3_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * Default-Konstruktor, nur fuer die Deserialisierung.
	 */
	public WerkerEingabeUDN_3_0_F_Pruefprozedur() {
	}

	/**
	 * Erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh�rigen Pr�flings.
	 * @param pruefprozName Name der Pr�fprozedur.
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit.
	 */
	public WerkerEingabeUDN_3_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den Arguementen.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "NUMBERS_ONLY", "MIN", "MAX", "IMAGE", "IMAGE_LOCATION", "STYLE" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den Arguementen.
	 */
	public String[] getRequiredArgs() {
		String[] args = { "AWT" };
		return args;
	}

	/**
	 * Pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>True</code> bei positiver Pr�fung, andernfalls <code>false</code>.
	 */
	public boolean checkArgs() {
		try {
			if( super.checkArgs() ) {
				// NUMBERS_ONLY muss TRUE oder FALSE sein (Gro�-/Kleinschreibung jedoch egal).
				if( getArg( getOptionalArgs()[0] ) != null )
					if( !getArg( getOptionalArgs()[0] ).equalsIgnoreCase( "TRUE" ) && !getArg( getOptionalArgs()[0] ).equalsIgnoreCase( "FALSE" ) )
						return false;

				// Wenn ein MAX vorhanden ist, muss auch ein MIN existieren. 
				// W�hrend beim Soll-Ist-Vergleich jedoch mehrere MINs erlaubt
				// sind, d�rfen bei Min-Max-Vergleich MIN und MAX nur einen Wert 
				// besitzen.
				if( getArg( "MAX" ) != null ) { //Wurde ein Min-Max-Vergleich gew�nscht?
					if( getArg( "MIN" ) == null ) //Fehlt MIN, obwohl MAX existiert?
						return false;
					else if( extractValues( getArg( "MIN" ) ).length != 1 ) //Hat MIN nur einen Wert? 
						return false;
					else if( extractValues( getArg( "MAX" ) ).length != 1 ) //Hat MAX nur einen Wert? 
						return false;
				}

				return true;
			} else
				return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {
		// Immer notwendig.
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// Spezifische Variablen.
		String awt;
		String image;
		String imageLocation;
		String min = null;
		String max = null;
		double minAsDouble = 0;
		double maxAsDouble = 0;
		boolean numbersOnly = false;
		UserDialogNextRuntimeEnvironment dlu = null;
		UDNHandle handle = null;
		String input;
		int style;

		try {
			// Parameter holen.
			try {
				if( !checkArgs() )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// Meldungstext.
				awt = getArg( getRequiredArgs()[0] );
				if( awt.indexOf( '@' ) != -1 ) {
					String[] awts = extractValues( awt );
					awt = "";
					for( int i = 0; i < awts.length; i++ )
						awt = awt + awts[i] + " ";
				} else
					awt = PB.getString( awt );

				// Nur Zahlen?
				numbersOnly = "TRUE".equalsIgnoreCase( getArg( getOptionalArgs()[0] ) );

				// Min-Wert.
				min = getArg( getOptionalArgs()[1] ); //kann auch null sein, dann kein MIN

				// Max-Wert.
				max = getArg( getOptionalArgs()[2] ); //kann auch null sein, dann kein MAX
				if( max != null ) {
					try {
						maxAsDouble = Double.parseDouble( max );
					} catch( NumberFormatException e ) {
						throw new PPExecutionException( "NumberFormatException (MAX): " + max );
					}
					try { //Achtung: Nur bei einem Min-Max-Vergleich auch MIN in double umwandeln!
						minAsDouble = Double.parseDouble( min );
					} catch( NumberFormatException e ) {
						throw new PPExecutionException( "NumberFormatException (MIN): " + min );
					}
				}

				// Image. Dies ist das Bild, welches der Dialog anzeigen soll (falls gew�nscht).
				image = getArg( getOptionalArgs()[3] ); //kann auch null sein, dann kein Bild

				// Position des Bildes in Bezug zum Meldungstext.
				imageLocation = getArg( getOptionalArgs()[4] ); //kann auch null sein, dann existiert die Angabe nicht und der Default wird angenommen

				// Style.
				if( getArg( "STYLE" ) != null )
					style = Integer.parseInt( getArg( "STYLE" ) );
				else
					style = -1;
			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Dialog anzeigen.
			try {
				dlu = getPr�flingLaufzeitUmgebung().getUserDialogNext();
				handle = dlu.allocateUserDialogNext();

				if( handle != null ) {
					if( numbersOnly ) {
						if( image != null )
							input = dlu.requestNumberInput( handle, toUDNState( style ), image, toImageLocation( imageLocation ), PB.getString( "Eingabe" ), awt, 0, UDNButtonConfiguration.OK_BUTTON );
						else
							input = dlu.requestNumberInput( handle, toUDNState( style ), PB.getString( "Eingabe" ), awt, 0, UDNButtonConfiguration.OK_BUTTON );
					} else {
						if( image != null )
							input = dlu.requestTextInput( handle, toUDNState( style ), image, toImageLocation( imageLocation ), PB.getString( "Eingabe" ), awt, 0, UDNButtonConfiguration.OK_BUTTON );
						else
							input = dlu.requestTextInput( handle, toUDNState( style ), PB.getString( "Eingabe" ), awt, 0, UDNButtonConfiguration.OK_BUTTON );
					}
					dlu.releaseUserDialogNext( handle ); //!!!																			
				} else
					throw new DeviceNotAvailableException();

			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Eingabe als Ergebnis ablegen und pr�fen, ob sie eventuell leer war.
			if( input.equalsIgnoreCase( "" ) ) {
				if( numbersOnly )
					result = new Ergebnis( "INPUT", "Userdialog", "numbers", "", "", "", "", "", "", "0", awt, "", "", "", "", Ergebnis.FT_NIO );
				else
					result = new Ergebnis( "INPUT", "Userdialog", "", "", "", "", "", "", "", "0", awt, "", "", "", "", Ergebnis.FT_NIO );
				status = STATUS_EXECUTION_ERROR;
			} else {
				if( numbersOnly )
					result = new Ergebnis( "INPUT", "Userdialog", "numbers", "", "", "INPUT", input, "", "", "0", awt, "", "", "", "", Ergebnis.FT_IO );
				else
					result = new Ergebnis( "INPUT", "Userdialog", "", "", "", "INPUT", input, "", "", "0", awt, "", "", "", "", Ergebnis.FT_IO );
			}
			ergListe.add( result );

			// Soll-Ist- bzw. Min-Max-Vergleich durchf�hren.
			if( min != null && max == null ) { //Soll-Ist-Vergleich?
				// Vergleichen. Hierbei alle Soll-Werte durchgehen.
				String[] mins = extractValues( getArg( "MIN" ) );
				boolean isEqual = false;
				for( int i = 0; i < mins.length; i++ )
					//Stringvergleich
					if( mins[i].equalsIgnoreCase( input ) ) {
						isEqual = true;
						break;
					}

				// MINs zum Speichern im Ergebnis aufl�sen und merken.
				String minString = mins[0];
				for( int i = 1; i < mins.length; i++ )
					minString = minString + ";" + mins[i];

				// Ergebnisse generieren.
				if( isEqual ) { //Gleichheit gefunden					
					result = new Ergebnis( "INPUT", "Userdialog", "", "", "", "INPUT", "" + input, minString, "", "0", awt, "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				} else { //keine Gleichheit gefunden					
					result = new Ergebnis( "INPUT", "Userdialog", "", "", "", "INPUT", "" + input, minString, "", "0", awt, "", "", "", PB.getString( "toleranzFehler1" ), Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}

			} else if( min != null && max != null ) { //Min-Max-Vergleich durchf�hren?				
				// Input in Double wandeln.
				double inputAsDouble;
				try {
					inputAsDouble = Double.parseDouble( input );
				} catch( NumberFormatException e ) {
					result = new Ergebnis( "INPUT", "Userdialog", "", "", "", "INPUT", input, "", "", "0", awt, "", "", "", PB.getString( "numberFormat" ), Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				}

				// Vergleich durchf�hren und Ergebnisse generieren.
				if( (inputAsDouble < minAsDouble) || (inputAsDouble > maxAsDouble) ) {
					result = new Ergebnis( "INPUT", "Userdialog", "", "", "", "INPUT", "" + inputAsDouble, "" + minAsDouble, "" + maxAsDouble, "0", awt, "", "", "", PB.getString( "toleranzFehler3" ), Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				} else {
					result = new Ergebnis( "INPUT", "Userdialog", "", "", "", "INPUT", "" + inputAsDouble, "" + minAsDouble, "" + maxAsDouble, "0", awt, "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				}
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		//Status setzen
		setPPStatus( info, status, ergListe );
	}

	/**
	 * Konvertiert den STYLE-Parameter der Pr�fprozedur in den 
	 * korrespondierenden UDN-State.
	 * 
	 * @param style STYLE-Angabe der Pr�fprozedur.
	 * @return Korrespondierender UDN-State.
	 */
	private UDNState toUDNState( int style ) {
		UDNState state;
		switch( style ) {
			case 1:
				state = UDNState.STATUS;
				break;
			case 2:
				state = UDNState.ALERT;
				break;
			case 3:
				state = UDNState.WARNING;
				break;
			case 4:
				state = UDNState.NONE;
				break;
			default:
				state = UDNState.MESSAGE;
		}
		return state;
	}

	/**
	 * Konvertiert den IMAGE_LOCATION-Parameter der Pr�fprozedur in die 
	 * korrespondierende UDN-ImageLocation Angabe. Bei �bergabe von 
	 * <code>null</code> wird ImageLocation.IMAGE_LEFT als Default zur�ck 
	 * geliefert.
	 *  
	 * @param code IMAGE_LOCATION-Parameter der Pr�fprozedur.  
	 * @return Korrespondierendes UDN-ImageLocation.
	 */
	private ImageLocation toImageLocation( String locationCode ) {
		ImageLocation location;

		if( "R".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_RIGHT;
		else if( "L".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_LEFT;
		else if( "T".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_TOP;
		else if( "B".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_BOTTOM;
		else
			location = ImageLocation.IMAGE_LEFT;

		return location;
	}

}

package com.bmw.cascade.pruefprozeduren;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.Pruefstand;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.psdz.PSdZ;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.CascadePSdZArgumentException;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.CascadePSdZConditionsNotCorrectException;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.CascadePSdZRuntimeException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementierung einer Pr�fprozedur, die alle SVTs ermittelt (i.d.R. die SVTist und die Sollverbauung) Wahlweise kann f�r beide das VCM benutzt werden. In diesem Fall wird nur die SVTsoll ermittelt.
 * 
 * @author Buboltz, BMW AG, Busetti, GEFASOFT AG , Saller, BMW AG, Gampl, BMW AG <BR>
 *         27.04.2007 TB Zwei neue optionale Parameter (SVTIST_ON und SVTSOLL_ON) default beide on <BR>
 *         04.05.2007 TB Tippfehler bei SVTSoll Parameter korrigiert <BR>
 *         03.01.2008 DB Einbau "testScreenID" in Debug-Ausgaben <BR>
 *         24.06.2008 DB �nderung holen des "testScreenID" <BR>
 *         17.08.2011 TB Feature f�r weiche CAFD Pr�fung �ber optionales Argument "SOFTCHECK_CAFD" <BR>
 *         23.01.2012 MS Methode zur �berpr�fung der SG-Antworten, falls 0 Antworten dann NIO <BR>
 *         29.02.2012 MS F-Version (12_0) <BR>
 *         ---------- -- - Bugfix: Neue Methode wird nur bei bSVTistUseVCM = false ausgef�hrt <BR>
 *         17.07.2012 BL T-Version (13_0) <BR>
 *         ---------- -- - Entfernen von psdz.generateSVTIstVCM() <BR>
 *         07.12.2012 MG T-Version (14_0) <BR>
 *         ---------- -- - Pr�fung auf nicht antwortende ECUs nur dann wenn bSVTistOn==true <BR>
 *         17.05.2013 TB F-Version (15_0) bzgl. Aenderung vom 7.12.2012 <BR>
 *         17.05.2013 TB T-Version (16_0) <BR>
 *         ---------- -- - Option f�r die KIS Vorschauberechnung soll als optionaler Parameter �bergeben werden // Achtung MI Verhalten wurde erst einmal bewu�t nicht vorgesehen, Generics <BR>
 *         17.05.2013 TB F-Version (17_0) bzgl. letzter �nderung, Mindestversionsanforderung CASCADE 6.0.0 / PSdZ 4.8.1 <BR>
 *         11.08.2015 MG T-Version (18_0), Mindestversionsanforderung CASCADE 6.0.0 / PSdZ 4.8.1 <BR>
 *         ---------- -- - LOP 1853: Ausgabe (in Log-Datei Pr�fstandScreen) der Pr�fprozedurversion sowie s�mtlicher optionaler und nicht optionaler Argumente, die an die Pr�fprozedur �bergeben werden zum Zweck der Nachvollziehbarkeit im Fehlerfall. <BR>
 *         ---------- -- - LOP 1899: Einheitliche Darstellung der Fehlerausgaben in PSdZ-Pr�fprozeduren <BR>
 *         27.08.2015 MG F-Version (19_0), Mindestversionsanforderung CASCADE 6.0.0 / PSdZ 4.8.1 <BR>
 *         14.07.2016 MG T-Version (20_0) <BR>
 *         ---------- -- - LOP 2093: Ausleitung der KIS-Laufzeiten aus PSdZ-Pr�fprozeduren zur KPI-Bestimmung f�r EE-23 Zielvorgabe <BR>
 *         25.07.2016 MG F-Version (21_0), Mindestversionsanforderung CASCADE 6.0.0 / PSdZ 4.8.1 <BR>
 */
public class PSdZGetSVTs_21_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSdZGetSVTs_21_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSdZGetSVTs_21_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG", "DEBUG_PERFORM", "KIS_CALCULATION_LEVEL", "SOFTCHECK_CAFD", "SVTIST_ON", "SVTIST_USE_VCM", "SVTSOLL_ON", "SVTSOLL_USE_VCM", "VEHICLE_STATE_EXCLUDE_LIST", "VEHICLE_STATE_INCLUDE_LIST" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = {};
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		if( !super.checkArgs() )
			return false;
		return true;
	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {
		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
		boolean bDebugPerform = true; // gibt an, ob eine Performencemessung erfolgen sollen
		boolean bSVTistOn = true;
		boolean bSVTistUseVCM = false;
		boolean bSVTsollOn = true;
		boolean bSVTsollUseVCM = false;
		boolean bSoftCheckCAFD = false; // gibt an, ob die CAFD Patch Level Version �berpr�ft wird o. nicht 
		char cPlatzhalter = 'n'; // Verwaltung des '*' Platzhalters: 'n'-nichts oder ohne Platzhalter, 'i'-include Liste hat Platzhalter, 'e'-exclude Liste hat Platzhalter 
		// df- funktionale Adresse neues Bordnetz; ef-funktionale Adresse altes Bordnetz; f1, f4, f5 Testeradressen
		final String[] STAR_IGNORE_LIST = { "df", "ef", "f1", "f4", "f5" }; // IGNORE Liste f�r die Platzhalter-Verwendung, hier k�nnen z.B. die funktionalen ECU Adressen stehen, ACHTUNG: hier z.B. nur "ef" und nicht "EF" eintragen
		final List<String> STAR_IGNORE_LIST_AS_LIST = Arrays.asList( STAR_IGNORE_LIST );
		final boolean DE = checkDE(); // Systemsprache DE wenn true		
		int status = STATUS_EXECUTION_OK;
		long startTimeStamp = 0;

		ArrayList<String> maskList = new ArrayList<String>(); // Mittels eines '*' beim ersten Element in der vehicleStateIncludeList oder vehicleStateExcludeList werden in der jeweils anderen Liste alle ECUs eingetragen ohne die ECUs in der List wo kein Platzhalter steht und ohne die ECUs aus der IGNORE_LIST.
		Ergebnis result;
		PSdZ psdz = null;
		String optionalArgumentName = null;
		String optionalArgumentValue = null;
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";
		// KIS_CALCULATION_LEVEL verwalten, wenn es als �bergabeparameter verwendet wird. Erlaubte Argumente sind analog denen der Variable aus der Pr�fstandskonfiguration 'PSDZ__KIS_CALCULATION_LEVEL' Typ: string; Werte: MONTAGEFORTSCHRITT, EINZELFLASH, GESAMTFLASH oder VORSCHAUberechnung; default: MONTAGEFORTSCHRITT; 
		String strKISCalculationLevel = null;
		String strKISCalculationLevelBefore = null;
		String strKISCalculationLevelFinal = null;
		String testScreenID = this.getPr�flingLaufzeitUmgebung().getName(); // TestScreenID (aus Pr�fstandsinstanz)
		String[] optionalArgumentsNames = getOptionalArgs();
		String[] optionalArgumentsValues = null;
		String[] vehicleStateExcludeList = null;
		String[] vehicleStateIncludeList = null;
		UserDialog userDialog = null;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();

		try {
			// Argumente loggen, einlesen und checken
			try {

				// Ausgabe der Pr�fprozedurversion sowie s�mtlicher an die Pr�fprozedur �bergebener Argumente (required/optional) f�r sp�tere Analysezwecke
				System.out.println( "PSdZ ID:" + testScreenID + ", Initialize " + this.getClassName() );

				// aktuell keine zwingenden Argumente parametrierbar, deshalb nur Ausgabe optionaler Argumente
				for( int i = 0; i < optionalArgumentsNames.length; i++ ) {
					optionalArgumentName = optionalArgumentsNames[i];
					optionalArgumentValue = "";
					// gibt es optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( optionalArgumentName ) != null ) {
						optionalArgumentsValues = extractValues( getArg( optionalArgumentName ) );
						for( int k = 0; k < optionalArgumentsValues.length; k++ ) {
							// Listen von optionalen Argumenten werden durch ";" getrennt ausgegeben
							optionalArgumentValue = optionalArgumentValue + optionalArgumentsValues[k] + ((optionalArgumentsValues.length > 1 && k < (optionalArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						optionalArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZGetSVTs with optional argument: " + optionalArgumentName + " = " + optionalArgumentValue );
				}

				// Obligatorische Argumentpr�fung auf Vorhandensein von nicht optionalen sowie �berz�hligen Argumenten
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
					else
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DEBUG_PERFORM
				if( getArg( "DEBUG_PERFORM" ) != null ) {
					if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "TRUE" ) )
						bDebugPerform = true;
					else if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "FALSE" ) )
						bDebugPerform = false;
					else
						throw new PPExecutionException( "DEBUG_PERFORM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// KIS_CALCULATION_LEVEL
				if( getArg( "KIS_CALCULATION_LEVEL" ) != null ) {
					strKISCalculationLevel = extractValues( getArg( "KIS_CALCULATION_LEVEL" ) )[0];
				}

				// SOFTCHECK_CAFD
				if( getArg( "SOFTCHECK_CAFD" ) != null ) {
					if( getArg( "SOFTCHECK_CAFD" ).equalsIgnoreCase( "TRUE" ) )
						bSoftCheckCAFD = true;
					else if( getArg( "SOFTCHECK_CAFD" ).equalsIgnoreCase( "FALSE" ) )
						bSoftCheckCAFD = false;
					else
						throw new PPExecutionException( "SOFTCHECK_CAFD " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// SVTIST_ON
				if( getArg( "SVTIST_ON" ) != null ) {
					if( getArg( "SVTIST_ON" ).equalsIgnoreCase( "TRUE" ) )
						bSVTistOn = true;
					else if( getArg( "SVTIST_ON" ).equalsIgnoreCase( "FALSE" ) )
						bSVTistOn = false;
					else
						throw new PPExecutionException( "SVTIST_ON " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// SVTIST_USE_VCM
				if( getArg( "SVTIST_USE_VCM" ) != null ) {
					if( getArg( "SVTIST_USE_VCM" ).equalsIgnoreCase( "TRUE" ) )
						bSVTistUseVCM = true;
					else if( getArg( "SVTIST_USE_VCM" ).equalsIgnoreCase( "FALSE" ) )
						bSVTistUseVCM = false;
					else
						throw new PPExecutionException( "SVTIST_USE_VCM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// SVTSOLL_ON
				if( getArg( "SVTSOLL_ON" ) != null ) {
					if( getArg( "SVTSOLL_ON" ).equalsIgnoreCase( "TRUE" ) )
						bSVTsollOn = true;
					else if( getArg( "SVTSOLL_ON" ).equalsIgnoreCase( "FALSE" ) )
						bSVTsollOn = false;
					else
						throw new PPExecutionException( "SVTSOLL_ON " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// SVTSOLL_USE_VCM
				if( getArg( "SVTSOLL_USE_VCM" ) != null ) {
					if( getArg( "SVTSOLL_USE_VCM" ).equalsIgnoreCase( "TRUE" ) )
						bSVTsollUseVCM = true;
					else if( getArg( "SVTSOLL_USE_VCM" ).equalsIgnoreCase( "FALSE" ) )
						bSVTsollUseVCM = false;
					else
						throw new PPExecutionException( "SVTSOLL_USE_VCM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// VEHICLE_STATE_INCLUDE_LIST
				if( getArg( "VEHICLE_STATE_INCLUDE_LIST" ) != null ) {
					vehicleStateIncludeList = extractValues( getArg( "VEHICLE_STATE_INCLUDE_LIST" ) );
				}

				// VEHICLE_STATE_EXCLUDE_LIST
				if( getArg( "VEHICLE_STATE_EXCLUDE_LIST" ) != null ) {
					vehicleStateExcludeList = extractValues( getArg( "VEHICLE_STATE_EXCLUDE_LIST" ) );

					// Darf nur zusammen mit der IncludeList definiert werden, wenn ein '*' Platzhalter in einer Liste ist
					if( getArg( "VEHICLE_STATE_INCLUDE_LIST" ) != null ) {
						// Kleiner Fehlercheck?
						if( (vehicleStateIncludeList.length > 1 && vehicleStateExcludeList.length > 1) || (vehicleStateIncludeList[0].equalsIgnoreCase( "*" ) && vehicleStateExcludeList[0].equalsIgnoreCase( "*" )) )
							throw new PPExecutionException( PB.getString( "psdz.parameter.ParameterfehlerVehicleStateList" ) );

						// includeList mit '*' ?
						if( vehicleStateIncludeList[0].equalsIgnoreCase( "*" ) )
							cPlatzhalter = 'i';

						// excludeList mit '*' ?
						if( vehicleStateExcludeList[0].equalsIgnoreCase( "*" ) )
							cPlatzhalter = 'e';
					}
				}

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Performancemessung Start
			if( bDebugPerform )
				System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetSVTs PP START" );

			// Devices holen
			try {
				psdz = getPr�flingLaufzeitUmgebung().getDeviceManager().getPSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			try {
				userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// setze tempor�re Variable f�r das Ignorieren der CAFD Patch Level Pr�fung, damit das ganze abw�rtskompatibel bleibt
			if( bSoftCheckCAFD ) {
				try {
					this.getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_IGNORE_CAFD_PATCH_LEVEL", new Boolean( true ) );
				} catch( Exception e ) {
					// Nothing
				}
			}

			// Steuerung des Montagefortschritts �ber die Pr�fstandsvariable PSDZ__KIS_CALCULATION_LEVEL, Achtung! Diese wird hier innerhalb der Pr�fprozedur potentiell 'overruled'
			if( strKISCalculationLevel != null ) {
				// Evtl. bestehende potentiell 'backupen' um diesen Stand nachtr�glich wieder rekonstruieren zu k�nnen
				try {
					strKISCalculationLevelBefore = (String) Pruefstand.getInstance().getPruefstandVariable( VariableModes.PS_CONFIG, "PSDZ__KIS_CALCULATION_LEVEL" );
				} catch( Exception e ) {
					// Nothing! Variable ist ja potentiell nicht vorhanden
				}

				// Schreibe die Pr�fstandsvariable mit dem entsprechenden Wert
				try {
					if( strKISCalculationLevel.toUpperCase().equalsIgnoreCase( "VORSCHAU" ) ) {
						Pruefstand.getInstance().setPruefstandVariable( VariableModes.PS_CONFIG, "PSDZ__KIS_CALCULATION_LEVEL", "VORSCHAU" );
					}
				} catch( Exception e ) {
					// Nothing! Sollte nicht auftreten.
				}
			}

			// Informiere den Benutzer
			userDialog.setDisplayProgress( true );
			userDialog.getProgressbar().setIndeterminate( true );
			userDialog.displayMessage( PB.getString( "psdz.ud.SVTErmittlung" ), ";" + PB.getString( "psdz.ud.SVTGenerierunglaeuft" ), -1 );

			// Berechnung der ECU Maske, bei Verwendung eines Platzhalters '*'
			// bei der vehicleStateIncludeList oder der vehicleStateExcludeList
			switch( cPlatzhalter ) {
			// include Liste mit '*'
				case 'i':
					List<String> vehicleStateExcludeListAsList = Arrays.asList( vehicleStateExcludeList );

					for( int i = 0; i < 256; i++ ) {
						if( !vehicleStateExcludeListAsList.contains( Integer.toHexString( i ) ) && !vehicleStateExcludeListAsList.contains( Integer.toHexString( i ).toUpperCase() ) && !STAR_IGNORE_LIST_AS_LIST.contains( Integer.toHexString( i ) ) )
							maskList.add( Integer.toHexString( i ) );
					}
					vehicleStateIncludeList = (String[]) maskList.toArray( new String[maskList.size()] );
					vehicleStateExcludeList = null;

					if( bDebug )
						System.out.println( "PSdZ ID:" + testScreenID + ", MaskList bei IncludeList: " + maskList.toString() );

					break;

				// exclude Liste mit '*'
				case 'e':
					List<String> vehicleStateIncludeListAsList = Arrays.asList( vehicleStateIncludeList );

					for( int i = 0; i < 256; i++ ) {
						if( !vehicleStateIncludeListAsList.contains( Integer.toHexString( i ) ) && !vehicleStateIncludeListAsList.contains( Integer.toHexString( i ).toUpperCase() ) && !STAR_IGNORE_LIST_AS_LIST.contains( Integer.toHexString( i ) ) )
							maskList.add( Integer.toHexString( i ) );
					}
					vehicleStateExcludeList = (String[]) maskList.toArray( new String[maskList.size()] );
					vehicleStateIncludeList = null;

					if( bDebug )
						System.out.println( "PSdZ ID:" + testScreenID + ", MaskList bei ExcludeList: " + maskList.toString() );

					break;

				// tue nichts...
				default:
			}

			// Ermitteln des final gesetzen KIS-Modus wie er auch im PSdZ-Device genutzt wird
			// 'PSDZ__KIS_CALCULATION_LEVEL' Typ: string; Werte: MONTAGEFORTSCHRITT, EINZELFLASH, GESAMTFLASH oder VORSCHAUberechnung; default: MONTAGEFORTSCHRITT; 
			try {
				strKISCalculationLevelFinal = (String) Pruefstand.getInstance().getPruefstandVariable( VariableModes.PS_CONFIG, "PSDZ__KIS_CALCULATION_LEVEL" );

				switch( strKISCalculationLevelFinal.toUpperCase().charAt( 0 ) ) {
				// M == MONTAGEFORTSCHRITT
					case 'M':
						strKISCalculationLevelFinal = "MONTAGEFORTSCHRITT";
						break;
					// E == EINZELFLASH
					case 'E':
						strKISCalculationLevelFinal = "EINZELFLASH";
						break;
					// G == GESAMTFLASH
					case 'G':
						strKISCalculationLevelFinal = "GESAMTFLASH";
						break;
					// V == VORSCHAU
					case 'V':
						strKISCalculationLevelFinal = "VORSCHAU";
						break;

					// default ist MONTAGEFORTSCHRITT
					default:
						strKISCalculationLevelFinal = "MONTAGEFORTSCHRITT";
				}
			} catch( Exception e ) {
				strKISCalculationLevelFinal = "MONTAGEFORTSCHRITT";
			}

			try {
				// SVTist ermitteln?
				if( bSVTistOn ) {

					// Startzeit
					startTimeStamp = System.currentTimeMillis();

					// SVT IST Ermittlung (direkt �bers Fahrzeug oder aus dem
					// VCM)
					if( !bSVTistUseVCM ) {
						// SVTIst muss vorher ermittelt werden
						if( vehicleStateIncludeList != null ) {
							// bedeutet wir haben eine Whitelist
							// alle in dieser Liste m�ssen antworten / nach
							// funktionaler Adressierung
							// werden
							// diese noch mal physikalisch angefragt

							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetSVTs 1 'psdz.generateAndStoreSVTIst' START" );
							}
							psdz.generateAndStoreSVTIst( vehicleStateIncludeList, false );

							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetSVTs 1 'psdz.generateAndStoreSVTIst' ENDE" );
							}

							result = new Ergebnis( "PSdZGetSVTs", "PSdZ", "Test procedure method name for KIS call:=generateAndStoreSVTIst", "KIS calculation mode set by CASCADE:=" + strKISCalculationLevelFinal, "", "Duration of method call [msec]", Long.toString( System.currentTimeMillis() - startTimeStamp ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );

						} else if( vehicleStateExcludeList != null ) {
							// bedeutet wir haben eine Blacklist
							// alle in dieser Liste m�ssen NICHT antworten /
							// alle nicht in dieser Liste
							// m�ssen antworten (aus SVTSoll)

							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetSVTs 2 'psdz.generateAndStoreSVTIst' START" );
							}

							psdz.generateAndStoreSVTIst( vehicleStateExcludeList, true );

							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetSVTs 2 'psdz.generateAndStoreSVTIst' ENDE" );
							}

							result = new Ergebnis( "PSdZGetSVTs", "PSdZ", "Test procedure method name for KIS call:=generateAndStoreSVTIst", "KIS calculation mode set by CASCADE:=" + strKISCalculationLevelFinal, "", "Duration of method call [msec]", Long.toString( System.currentTimeMillis() - startTimeStamp ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );

						} else {
							// �bergebe leere Liste -> damit wir �berhaupt ne
							// SVTIst bekommen
							String[] emptyList = {};

							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetSVTs 3 'psdz.generateAndStoreSVTIst' START" );
							}

							psdz.generateAndStoreSVTIst( emptyList, true );

							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetSVTs 3 'psdz.generateAndStoreSVTIst' ENDE" );
							}

							result = new Ergebnis( "PSdZGetSVTs", "PSdZ", "Test procedure method name for KIS call:=generateAndStoreSVTIst", "KIS calculation mode set by CASCADE:=" + strKISCalculationLevelFinal, "", "Duration of method call [msec]", Long.toString( System.currentTimeMillis() - startTimeStamp ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );

						}
					} else {
						// hole Daten aus VCM und speichere diese SVTist als
						// SVTist im PSdZ ab

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetSVTs 'psdz.getSVTfromVCM' START" );
						}

						psdz.getSVTfromVCM( "SVTIST", true );

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetSVTs 'psdz.getSVTfromVCM' ENDE" );
						}

					}

				}

				// Sollverbauung (o.SVTsoll) ermitteln?
				if( bSVTsollOn ) {

					// Startzeit
					startTimeStamp = System.currentTimeMillis();

					if( !bSVTistOn ) {
						// wenn kein SVTist, dann rufe eine andere PSdZ Funktion
						// auf, weil wir haben ja kein SVTist Objekt haben (PSdZ
						// Verhau!)

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetSVTs 'psdz.generateAndStoreSollverbauungRoh' START" );
						}

						psdz.generateAndStoreSollverbauungRoh();

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetSVTs 'psdz.generateAndStoreSollverbauungRoh' ENDE" );
						}

						result = new Ergebnis( "PSdZGetSVTs", "PSdZ", "Test procedure method name for KIS call:=generateAndStoreSollverbauungRoh", "KIS calculation mode set by CASCADE:=" + strKISCalculationLevelFinal, "", "Duration of method call [msec]", Long.toString( System.currentTimeMillis() - startTimeStamp ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}

					else {
						// SOLLVERBAUUNG ermitteln (direkt �ber KIS oder aus dem
						// VCM[dann nur SVT Soll])
						if( !bSVTsollUseVCM ) {

							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetSVTs 'psdz.generateAndStoreSollverbauung' START" );
							}

							psdz.generateAndStoreSollverbauung();

							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetSVTs 'psdz.generateAndStoreSollverbauung' ENDE" );
							}

							result = new Ergebnis( "PSdZGetSVTs", "PSdZ", "Test procedure method name for KIS call:=generateAndStoreSollverbauung", "KIS calculation mode set by CASCADE:=" + strKISCalculationLevelFinal, "", "Duration of method call [msec]", Long.toString( System.currentTimeMillis() - startTimeStamp ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );

						} else {
							// hole Daten aus VCM und speichere diese SVTSoll als
							// SVTSoll im PSdZ ab

							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetSVTs 'psdz.getSVTfromVCM' START" );
							}

							psdz.getSVTfromVCM( "SVTSOLL", true );

							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetSVTs 'psdz.getSVTfromVCM' END" );
							}
						}
					}
				}

				if( !bSVTistUseVCM && bSVTistOn ) {

					// Liste aller ECUs aus der SVT einlesen
					Map<String, String> ecus = psdz.getEcuStatisticsDec();
					Set<String> keys = ecus.keySet();
					Object[] key = keys.toArray();
					boolean noresponse = false;
					if( bDebugPerform ) {
						for( int i = 0; i < key.length; i++ ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", ECU: " + key[i].toString() + " , Antwort: " + ecus.get( key[i] ).toString() );
						}
					}
					// falls �ber getSVTs keine ECUs antworten, soll eine Fehlermeldung erzeugt werden
					for( int i = 0; i < key.length; i++ ) {
						noresponse = false;
						String ecuresponse = ecus.get( key[i] ).toString();
						if( ecuresponse.equalsIgnoreCase( "1" ) || ecuresponse.equalsIgnoreCase( "2" ) ) {
							break;
						} else {
							noresponse = true;
						}
					}
					if( noresponse ) {
						String errorText = DE ? "Kein Steuerger�t ansprechbar!" : "No response from any ECU!";
						result = new Ergebnis( "PSdZGetSVTs", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText, "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}

				}

			} catch( CascadePSdZArgumentException e ) {
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZGetSVTs", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Argument" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( CascadePSdZConditionsNotCorrectException e ) {
				// Fehler abfangen und dokumentiern
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZGetSVTs", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Bedingungen" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( CascadePSdZRuntimeException e ) {
				// Fehler abfangen und dokumentiern
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZGetSVTs", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Laufzeit" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// setze tempor�re Variable f�r das Ignorieren der CAFD Patch Level Pr�fung, damit das ganze abw�rtskompatibel bleibt
		if( bSoftCheckCAFD ) {
			try {
				this.getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_IGNORE_CAFD_PATCH_LEVEL", new Boolean( false ) );
			} catch( Exception e ) {
				// Nothing
			}
		}

		// Rekonstruktion der Pr�fstandsvariablen PSDZ__KIS_CALCULATION_LEVEL
		if( strKISCalculationLevelBefore != null ) {
			try {
				Pruefstand.getInstance().setPruefstandVariable( VariableModes.PS_CONFIG, "PSDZ__KIS_CALCULATION_LEVEL", strKISCalculationLevelBefore );
			} catch( Exception e ) {
				// Nothing! Sollte nicht auftreten.
			}

		} else if( strKISCalculationLevelBefore == null && strKISCalculationLevel != null ) {
			try {
				Pruefstand.getInstance().deletePruefstandVariable( VariableModes.PS_CONFIG, "PSDZ__KIS_CALCULATION_LEVEL" );
			} catch( Exception e ) {
				// Nothing! Sollte nicht auftreten.
			}
		}

		// Freigabe der benutzten Devices
		if( psdz != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releasePSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
		try {
			getPr�flingLaufzeitUmgebung().releaseUserDialog();
		} catch( Exception e ) {
			if( e instanceof DeviceLockedException )
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "PSdZGetSVTs", "", "", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}
		setPPStatus( info, status, ergListe );

		// Performancemessung Ende
		if( bDebugPerform )
			System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetSVTs PP ENDE" );
	}
}

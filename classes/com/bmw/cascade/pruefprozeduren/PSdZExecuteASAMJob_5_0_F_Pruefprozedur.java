package com.bmw.cascade.pruefprozeduren;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.psdz.PSdZ;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.*;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementierung einer Pr�fprozedur, die das Ausf�hren eines ASAM Diagnose Jobs direkt �ber PSdZ erlaubt. <BR>
 * INPUT_PARAMETER_DATATYPE'X' mu� eine voll qualifizierte Java Klasse sein: z.B.: 'java.lang.String' f�r den Datentyp String. <BR>
 * 
 * @author Buboltz, BMW AG; Gampl, BMW AG <BR>
 *         24.05.2011 TB Ersterstellung <BR>
 *         17.04.2013 TB UserDialog, Generics, Kosmetik, T-Version <BR>
 *         20.04.2013 TB F-Version <BR>
 *         09.10.2014 MG <BR>
 *         - Codebereinigung (Ausbau alter Methoden: getPSdZExceptions(), clearPSdZExceptions()); neue CASCADE Mindestversion 6.1.0 <BR>
 *         - Bestehende Debugausgaben korrigiert und neue Debugausgaben hinzu <BR>
 *         20.10.2014 MG F-Version <BR>
 */
public class PSdZExecuteASAMJob_5_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	// Debug flag
	boolean bCheckArgsOn = false;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSdZExecuteASAMJob_5_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSdZExecuteASAMJob_5_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "INPUT_PARAMETER_NAME[1..N]", "INPUT_PARAMETER_DATATYPE[1..N]", "INPUT_PARAMETER_VALUE[1..N]", "AWT", "HWT", "DEBUG", "DEBUG_PERFORM" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = { "ECU_ADDRESS_HEX", "ECU_BASE_VARIANT", "JOB_ID" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		int i = 0, j = 0;
		int maxIndex = 0;
		boolean bOptionalArg = false;

		// Debug output on begin of checkArgs()
		if( bCheckArgsOn ) {
			Enumeration<?> enu = getArgs().keys();

			System.out.println( "PSdZExecuteASAMJob: checkArgs" );
			System.out.println( "PSdZExecuteASAMJob: checkArgs in: " + this.getName() );
			System.out.println( "PSdZExecuteASAMJob: All Arguments from this PP:" );
			while( enu.hasMoreElements() ) {
				// The actual argument
				String strGivenkey = (String) enu.nextElement();
				String strActualArg = getArg( strGivenkey );
				System.out.println( "PSdZExecuteASAMJob: " + strGivenkey + " = " + strActualArg );
			}
		}

		try {
			// 1. Check: Sind alle requiredArgs gesetzt
			String[] requiredArgs = getRequiredArgs();
			for( i = 0; i < requiredArgs.length; i++ ) {
				if( getArg( requiredArgs[i] ) == null )
					return false;
				if( getArg( requiredArgs[i] ).equals( "" ) == true )
					return false;
			}

			if( bCheckArgsOn ) {
				System.out.println( "PSdZExecuteASAMJob: Done 1. Check" );
			}

			// 2. Check: Sind �berfl�ssige Argumente gesetzt, d.h. Argumente, die weder required
			// noch optional sind
			// Gesetzte Argumente, die mit ..[1..N] beginnen, werden hierbei nicht analysiert, es
			// wird
			// jedoch der maximale Index festgehalten.
			Enumeration<?> enu = getArgs().keys();
			String[] optionalArgs = getOptionalArgs();
			String givenkey = null;
			String temp = null;
			while( enu.hasMoreElements() ) {
				givenkey = (String) enu.nextElement();
				// Wenn Argument ohne Index
				if( (givenkey.startsWith( "INPUT_PARAMETER_NAME" ) == false) || (givenkey.startsWith( "INPUT_PARAMETER_DATATYPE" ) == false) || (givenkey.startsWith( "INPUT_PARAMETER_VALUE" ) == false) ) {

					j = 0;
					bOptionalArg = false;
					while( (bOptionalArg == false) && (j < optionalArgs.length) ) {
						if( givenkey.equals( optionalArgs[j] ) == true )
							bOptionalArg = true;
						j++;
					}
					if( getArg( givenkey ).equals( "" ) == true )
						return false;
				}
				// Wenn Argument mit Index
				else {
					if( givenkey.startsWith( "INPUT_PARAMETER_NAME" ) == true ) {
						temp = givenkey.substring( 20 );
					} else if( givenkey.startsWith( "INPUT_PARAMETER_DATATYPE" ) == true ) {
						temp = givenkey.substring( 24 );
					} else if( givenkey.startsWith( "INPUT_PARAMETER_VALUE" ) == true ) {
						temp = givenkey.substring( 21 );
					}
					try {
						j = Integer.parseInt( temp );
						if( j < 1 )
							return false;
						if( j > maxIndex )
							maxIndex = j;
					} catch( NumberFormatException e ) {
						e.printStackTrace();
						return false;
					}
				}
			}
			if( bCheckArgsOn ) {
				System.out.println( "PSdZExecuteASAMJob: Done 2. Check" );
			}

			// Tests bestanden, somit ok
			if( bCheckArgsOn ) {
				System.out.println( "PSdZExecuteASAMJob: ok = true" );
				System.out.println( "PSdZExecuteASAMJob: End checkArgs() from PSdZExecuteASAMJob." );
			}
			return true;
		} catch( Exception e ) {
			if( bCheckArgsOn )
				e.printStackTrace();
			return false;
		} catch( Throwable e ) {
			if( bCheckArgsOn )
				e.printStackTrace();
			return false;
		}
	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * Indizierte Argumente einlesen
	 * 
	 * @param parameterName
	 * @return
	 */
	private String[] readIndexedParameter( String parameterName ) {
		int resAnzahl = 0;
		while( (getArg( parameterName + (resAnzahl + 1) )) != null ) {
			resAnzahl++;
		}
		String[] resList = new String[resAnzahl];
		for( int i = 0; i < resAnzahl; i++ )
			resList[i] = getArg( parameterName + (i + 1) );
		return resList;
	}

	/**
	 * Decodiere PSdZ ByteArray
	 * 
	 * @param bytes Eingangsbytes im PSdZ Format, wobei 1:1 Zuordnung vorausgesetzt wird. Keine Formatierung und kein Encoding.
	 * @return decodierter String in der Form "0x01 0x1A ..."
	 */
	private String decodeByteArrayToString( byte[] bytes ) {
		StringBuffer sb = new StringBuffer( "" );

		for( int i = 0; i < bytes.length; i = i + 2 ) {
			String hi = "0" + Integer.toHexString( bytes[i] & 0xff ).toUpperCase();
			String lo = "0" + Integer.toHexString( bytes[i + 1] & 0xff ).toUpperCase();

			sb.append( " 0x" + hi.substring( hi.length() - 2, hi.length() ) + " 0x" + lo.substring( lo.length() - 2, lo.length() ) );

			//// unsigned Byte
			//		for( byte val : bytes ) {
			//			sb.append( (char) (val & 0x7F) + (val < 0 ? 128 : 0) );
		}

		return sb.toString();
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// �bergabeparameter
		String ecuAddressHex = null;
		String ecuBaseVariant = null;
		String jobID = null;
		String awt = "";
		String hwt = "";
		String[] inputParameterName = null;
		String[] inputParameterDatatype = null;
		String[] inputParameterValue = null;

		// Map der InputParameter, ErgebnisParameter
		Map<String, Object> inputParameters = new HashMap<String, Object>();
		Map<String, Object> resultParameters = new HashMap<String, Object>();

		// sekund�re Objekte
		PSdZ psdz = null;
		UserDialog userDialog = null;
		String testScreenID = this.getPr�flingLaufzeitUmgebung().getName(); // TestScreenID (aus Pr�fstandsinstanz)
		final boolean DE = checkDE(); // Systemsprache DE wenn true
		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
		boolean bDebugPerform = true; // gibt an, ob eine Performencemessung erfolgen sollen

		try {

			// Argumente einlesen und checken
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
					else if( !getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) && !getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DEBUG_PERFORM
				if( getArg( "DEBUG_PERFORM" ) != null ) {
					if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "TRUE" ) )
						bDebugPerform = true;
					else if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "FALSE" ) )
						bDebugPerform = false;
					else if( !getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "FALSE" ) && !getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "TRUE" ) )
						throw new PPExecutionException( "DEBUG_PERFORM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// ECU_ADDRESS_HEX
				ecuAddressHex = extractValues( getArg( "ECU_ADDRESS_HEX" ) )[0];

				// ECU_BASE_VARIANT
				ecuBaseVariant = extractValues( getArg( "ECU_BASE_VARIANT" ) )[0];

				// JOB_ID
				jobID = extractValues( getArg( "JOB_ID" ) )[0];

				// INPUT_PARAMETER_NAME
				inputParameterName = readIndexedParameter( "INPUT_PARAMETER_NAME" );

				// INPUT_PARAMETER_DATATYPE
				inputParameterDatatype = readIndexedParameter( "INPUT_PARAMETER_DATATYPE" );

				// INPUT_PARAMETER_VALUE
				inputParameterValue = readIndexedParameter( "INPUT_PARAMETER_VALUE" );

				// AWT
				if( getArg( "AWT" ) != null ) {
					awt = extractValues( getArg( "AWT" ) )[0];
				}

				// HWT
				if( getArg( "HWT" ) != null ) {
					hwt = extractValues( getArg( "HWT" ) )[0];
				}

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			// Performancemessung Start
			if( bDebugPerform )
				System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteASAMJob PP START" );

			// Devices holen
			try {
				psdz = getPr�flingLaufzeitUmgebung().getDeviceManager().getPSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			try {
				userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Info via UserDialog
			userDialog.setDisplayProgress( true );
			userDialog.getProgressbar().setIndeterminate( true );
			String message = DE ? "Job: " + jobID + ";SG-Adresse: 0x" + ecuAddressHex.toUpperCase() + ";Basisvariante: " + ecuBaseVariant + ";;Ausfuehrung laeuft..." : "Job: " + jobID + ";ECU: 0x" + ecuAddressHex.toUpperCase() + "; Basis variant: " + ecuBaseVariant + ";;Execution is running...";

			// Benutzerausgabe
			if( awt.equals( "" ) ) {
				userDialog.displayMessage( "PSdZ Execute ASAM Job", message, -1 );
			} else
				userDialog.displayMessage( "PSdZ Execute ASAM Job", awt, -1 );

			// Debug Jobname
			if( bDebug )
				System.out.println( "PSdZ ID:" + testScreenID + ", DEBUG PSdZExecuteASAMJob, ECU: 0x" + ecuAddressHex.toUpperCase() + ",  Basis variante: " + ecuBaseVariant + ", Job: " + jobID );

			// InputParameter aufbereiten
			for( int i = 0; i < inputParameterName.length; i++ ) {
				Class<?> parameterClass = Class.forName( inputParameterDatatype[i] );
				String[] params = new String[1];
				params[0] = inputParameterValue[i];
				Class<?>[] argTypes = new Class[1];
				argTypes[0] = String.class;
				Constructor<?> constructor = parameterClass.getConstructor( argTypes );
				inputParameters.put( inputParameterName[i], constructor.newInstance( (Object[]) params ) );
				result = new Ergebnis( inputParameterName[i], "PSdZExecuteASAMJob", "", "", "", "ECU: 0x" + ecuAddressHex.toUpperCase() + ", Variante: " + ecuBaseVariant + ", Job: " + jobID + ", Parameter: " + inputParameterName[i], "Parameter Value: " + inputParameterValue[i] + " (Datatype: " + inputParameterDatatype[i] + ")", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );

				if( bDebug )
					System.out.println( "PSdZ ID:" + testScreenID + ", DEBUG PSdZExecuteASAMJob, Parameter: " + inputParameterName[i] + ",  Value: " + inputParameterValue[i] + ", (Datatype: " + inputParameterDatatype[i] + ")" );
			}

			// F�hre den ASAM Job aus
			try {
				// teste vorher die Verbindung mit dem SG
				if( psdz.checkCommunication() ) {

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteASAMJob 'psdz.executeASAMJob' START" );
					}

					resultParameters = psdz.executeASAMJob( ecuAddressHex, ecuBaseVariant, jobID, inputParameters );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteASAMJob 'psdz.executeASAMJob' ENDE" );
					}

					// Dokumentiere den Job in APDM "IO" 
					result = new Ergebnis( "ASAM_JOB", "PSdZExecuteASAMJob", "ECU: 0x" + ecuAddressHex, "Variante: " + ecuBaseVariant, "Job: " + jobID, "ASAM_JOB", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );

					try {
						// Ergebnisparameter
						for( Iterator<?> it = resultParameters.entrySet().iterator(); it.hasNext(); ) {
							Map.Entry<?, ?> pairs = (Map.Entry<?, ?>) it.next();

							String paramName = (String) pairs.getKey();
							Object value = (Object) pairs.getValue();

							// PSdZ kennt nur die Datentypen String, float, double, int, long oder das byte[]
							String strValue = null;
							if( value instanceof String || value instanceof Float || value instanceof Double || value instanceof Integer || value instanceof Long ) {
								strValue = "" + value;
								result = new Ergebnis( paramName, "PSdZExecuteASAMJob", "", "", "", "Resultname: " + paramName, "Resultvalue: " + strValue, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
							} else {
								// jetzt kennt PSdZ nur noch das ByteArray, etwas kniffliger...
								// es wird vorausgesetzt, dass 1:1 die Bytes von PSdZ zur�ckgegeben werden, kein Encoding und keine Umformatierung
								ByteArrayOutputStream bos = null;
								ObjectOutputStream oos = null;

								try {
									bos = new ByteArrayOutputStream();
									oos = new ObjectOutputStream( bos );
									oos.writeObject( value );
									oos.flush();

									strValue = decodeByteArrayToString( bos.toByteArray() );
									result = new Ergebnis( paramName, "PSdZExecuteASAMJob", "", "", "", "Resultname: " + paramName, "Resultvalue: " + strValue, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
									ergListe.add( result );
								} finally {
									if( oos != null ) {
										oos.close();
									}
									if( bos != null ) {
										bos.close();
									}
								}
							}

							// noch loggen
							if( bDebug )
								System.out.println( "PSdZ ID:" + testScreenID + ", DEBUG PSdZExecuteASAMJob, Resultname: " + paramName + ",  Resultvalue: " + strValue );
						}
					} catch( Exception e ) {
						// Falls wir was gar nicht auswerten k�nnen, dann ignorieren // Job ist IO wir haben ja was ausf�hren k�nnen
						if( bDebug )
							e.printStackTrace();
					}
				} else {
					String fehlerText = DE ? "Keine Verbindung zum Fahrzeug vorhanden!" : "No connection with the vehicle available!";
					result = new Ergebnis( "VEHICLE_COMMUNICATION", "PSdZExecuteASAMJob", "", "", "", "VEHICLE_COMMUNICATION", "FALSE", "TRUE", "", "0", "", "", "", fehlerText, "", Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
			} catch( CascadePSdZConditionsNotCorrectException e ) {
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZ", "PSdZExecuteASAMJob", "", "", "", "", "", "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Argument" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( CascadePSdZRuntimeException e ) {
				if( bDebug )
					e.printStackTrace();
				// Dokumentiere den Job in APDM "NIO" 
				result = new Ergebnis( "ASAM_JOB", "PSdZExecuteASAMJob", ecuAddressHex, ecuBaseVariant, jobID, "ASAM_JOB", "NOT_OKAY", "OKAY", "", "0", "", "", "", "", hwt, Ergebnis.FT_NIO );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}

			// reset UserDialog
			userDialog.reset();

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Freigabe der benutzten Devices
		if( psdz != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releasePSdZ();
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
		try {
			getPr�flingLaufzeitUmgebung().releaseUserDialog();
		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			if( e instanceof DeviceLockedException )
				result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "PSdZExecuteASAMJob", "", "", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}

		// Status setzen
		setPPStatus( info, status, ergListe );

		// Performancemessung Ende
		if( bDebugPerform )
			System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZExecuteASAMJob PP ENDE" );
	}
}

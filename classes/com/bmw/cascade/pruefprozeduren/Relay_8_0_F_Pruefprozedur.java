package com.bmw.cascade.pruefprozeduren;

import java.util.StringTokenizer;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceIOException;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.relaycontrol.RelayControl;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;

/**
 * Pruefprozedur zur Ansteuerung von momentan 16 Ausg�ngen Liest 16 Eing�nge (08.08.2002: noch nicht
 * implementiert), setzt 16 Ausg�nge, unter Verwendnung von -ADDI-DATA 1500 oder getrennt �ber zwei
 * Module: -ICP I-7053(D) (16 Eing�nge), ICP I-7043(D) (16 Ausg�nge) ##ACHTUNG: jedem ICP-Modul mu�
 * vor dem Ersteinsatz die richtige Adresse zugewiesen werden!
 * 
 * @author Ruediger Gall, TI-430
 * @author Peter Winklhofer, TI-431
 * @author Peter Rettig, TI-431
 * @author Frank Weber, TI-431
 * @version 08.08.2002 V0.0.1 GA Ersterstellung
 * @version 06.10.2002 V0.0.2 GA e.getMessage() bei "Relais' schalten" in Ergebnis dazu, um am
 *          Pr�fstand den Fehler auszugeben
 * @version 14.08.2003 V0.0.3 PW Nutzung von OUTPUT_ON und OUTPUT_OFF jetzt gleichzeitig m�glich
 *          (OFF wird vor ON ausgef�hrt)
 * @version 07.10.2003 V0.0.4 PR Neues Argument INIT zum Aufruf von setOutputMemoryOn (ist
 *          notwendig, falls durch Device CasEcuLocker ein setOutputMemoryOff aufgerufen wurde)
 * @version 30.06.2004 V0.0.5 PW+FW Erweiterung auf mehr Kan�le (bis zu 80)
 * @version 08.07.2005 V0.0.6Test FW IO Ergebnis hinzu f�r APDM Dokumentation
 * @version 22.07.2005 7_0_T FW bugfxing IO result
 * @version 22.07.2005 8_0_F FW productive version of Version 7_0_T
 */
public class Relay_8_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public Relay_8_0_F_Pruefprozedur() {
	}

	/**
	 * Legt die maximale Anzahl �ber diese PP ansteuerbarer Ausg�nge fest. Diese Anzahl ist durch
	 * das verwendete Device vorgegeben. Momentan beherrschen die Devices maximal 16 Ausg�nge.
	 */
	public final static int MAX_NUMBER_OUTPUTS = 80;

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public Relay_8_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName,
			Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "OUTPUT_ON", "OUTPUT_OFF", "WAIT", "INIT" };//_On, _Off, kann Array mit
																	  // Werten zwischen 1 und 16
																	  // sein [RETTIG]
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {//Da nicht bekannt ist, ob _ON und oder _OFF benutzt wird,
									   // gibt es kein required Argument
		String[] args = new String[0];
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 */
	public boolean checkArgs() {
		if( (getArg( "OUTPUT_ON" ) == null) && (getArg( "OUTPUT_OFF" ) == null) && (getArg( "INIT" ) == null) ) //[RETTIG]
			return false;
		else
			return true;
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		RelayControl myRelay = null;
		int[] setOnNummern = null;
		int[] setOffNummern = null;
		boolean on = false;
		boolean off = false;
		String warte = null;
		long dauer = 0;
		boolean DEBUG = false;

		try {

			//Parameter
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				//int paramAnzahl = 0;Wird nirgends gelesen
				//int tokenAnzahl = 0; Wird nirgends gelesen
				//int onOff = 0; Wird nirgends gelesen

				Integer wert = null;
				String temp1 = null; //f�r On-Parameter
				String temp2 = null; //f�r Off-Parameter
				Vector relOnNr = new Vector(); //f�r On-Parameter
				Vector relOffNr = new Vector(); //f�r Off-Parameter

				if( (temp1 = getArg( "OUTPUT_ON" )) != null ) {
					on = true;
				}
				if( (temp2 = getArg( "OUTPUT_OFF" )) != null ) {
					off = true;
				}

				//***********************************************************************Beginn
				// �nderung
				if( on == true ) {
					//On-Parameter ermitteln
					if( DEBUG )
						System.out.println( "Relay setOn: " + temp1 + ", on= " + on + ", off= " + off );
					StringTokenizer myTokenizer = new StringTokenizer( temp1, ";" );
					for( int i = 0; myTokenizer.hasMoreTokens(); i++ ) {
						String token = myTokenizer.nextToken();
						if( DEBUG )
							System.out.println( "TOKEN " + i + ": " + token );

						//token-->integer
						try {
							wert = new Integer( Integer.parseInt( token ) );
						} catch( NumberFormatException nfe ) {
							throw new PPExecutionException( PB.getString( "pruefprozedur.klappencodierung.errortype" ) );
						}

						// Pr�fung Wertebereich
						if( (wert.intValue() < 1) || (wert.intValue() > MAX_NUMBER_OUTPUTS) ) {
							if( DEBUG )
								System.out.println( "Wert: " + wert );
							throw new PPExecutionException( PB.getString( "pruefprozedur.klappencodierung.errorrange" ) );
						}

						relOnNr.add( wert );
					}
				}
				if( off == true ) {
					//Off-Parameter ermitteln
					if( DEBUG )
						System.out.println( "Relay setOff: " + temp2 + ", on= " + on + ", off= " + off );
					StringTokenizer myTokenizer = new StringTokenizer( temp2, ";" );
					for( int i = 0; myTokenizer.hasMoreTokens(); i++ ) {
						String token = myTokenizer.nextToken();
						if( DEBUG )
							System.out.println( "TOKEN " + i + ": " + token );

						//token-->integer
						try {
							wert = new Integer( Integer.parseInt( token ) );
						} catch( NumberFormatException nfe ) {
							throw new PPExecutionException( PB.getString( "pruefprozedur.klappencodierung.errortype" ) );
						}

						// Pr�fung Wertebereich
						if( (wert.intValue() < 1) || (wert.intValue() > MAX_NUMBER_OUTPUTS) ) {
							if( DEBUG )
								System.out.println( "Wert: " + wert );
							throw new PPExecutionException( PB.getString( "pruefprozedur.klappencodierung.errorrange" ) );
						}

						relOffNr.add( wert );
					}
				}
				//***************************************************Ende �nderung

				// Umwandlung des Vectors in ein int-Array
				if( on == true ) {
					setOnNummern = new int[relOnNr.size()];
					for( int i = 0; i < relOnNr.size(); i++ )
						setOnNummern[i] = ((Integer) relOnNr.get( i )).intValue();
				}

				if( off == true ) {
					setOffNummern = new int[relOffNr.size()];
					for( int i = 0; i < relOffNr.size(); i++ )
						setOffNummern[i] = ((Integer) relOffNr.get( i )).intValue();
				}
			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//Device holen
			try {
				myRelay = getPr�flingLaufzeitUmgebung().getDeviceManager().getRelayControl();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Relay", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "ExecFehler", "Relay", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Relay", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//Relais' schalten
			try {
				if( "TRUE".equalsIgnoreCase( getArg( "INIT" ) ) )
					myRelay.init(); //[RETTIG] init ruft setOutputMemoryOn() auf (dies ist
									// notwendig, wenn zuvor, z. B. durch das Device CasEcuLocker,
									// setOutputMemoryOff aufgerufen wurde)
				if( off == true )
					myRelay.setOff( setOffNummern ); //Reihenfolge von on-off auf off-on umgestellt
				if( on == true )
					myRelay.setOn( setOnNummern );

				//Kontrollausgaben
				if( DEBUG ) {
					if( on == true )
						for( int i = 0; i < setOnNummern.length; i++ )
							System.out.println( "Relay einschalten,  laufende Nr.: " + (i + 1) + "   : " + setOnNummern[i] );
					if( off == true )
						for( int i = 0; i < setOffNummern.length; i++ )
							System.out.println( "Relay ausschalten,  laufende Nr.: " + (i + 1) + "   : " + setOffNummern[i] );
					System.out.println( "- - -" );
				}
				
				//APDM Result
				String successText = "Relais set ON: ";
				if( on == true && setOnNummern.length > 0 ) {
					for( int i = 0; i < setOnNummern.length - 1; i++ ) {
						successText += setOnNummern[i] + ",";
					}
					successText += setOnNummern[setOnNummern.length - 1];
				} else {
					successText += "-";
				}
				successText += "  OFF: ";
				if( off == true && setOffNummern.length > 0 ) {
					for( int i = 0; i < setOffNummern.length - 1; i++ ) {
						successText += setOffNummern[i] + ",";
					}
					successText += setOffNummern[setOffNummern.length - 1];
				} else {
					successText += "-";
				}
				result = new Ergebnis( "ExecSuccessful", "Relay", "", "", "", "", "", "", "", "0", "", "", "", "", successText, Ergebnis.FT_IO );
				ergListe.add( result );
				

			} catch( Exception e ) {
				e.printStackTrace();
				if( e instanceof DeviceIOException )
					result = new Ergebnis( "ExecFehler", "Relay", "", "", "", "", "", "", "", "0", "", "", "", "DeviceIOException", e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Relay", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "use", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//Z.B. Prellzeit nach Setzen eines Relais abwarten
			if( (warte = getArg( "WAIT" )) != null ) {
				try {
					dauer = Long.parseLong( warte );
				} catch( NumberFormatException e ) {
					e.printStackTrace();
					if( e.getMessage() != null )
						result = new Ergebnis( "ParaFehler", "Relay", "", "", "", "", "", "", "", "0", "", "", "", "parametrierfehler", e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "ParaFehler", "Relay", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}
				//Warten
				long timestamp = System.currentTimeMillis() + dauer;
				do {
					try {
						Thread.sleep( dauer );
					} catch( InterruptedException e ) {
					}
					dauer = timestamp - System.currentTimeMillis();
				} while( dauer > 0 );
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "Relay", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "Relay", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		//Freigabe der benutzten Devices
		if( myRelay != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseRelayControl();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Relay", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Relay", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
			myRelay = null;
		}

		//Status setzen
		setPPStatus( info, status, ergListe );
	}
}

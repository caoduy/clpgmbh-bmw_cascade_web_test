package com.bmw.cascade.pruefprozeduren;

import java.util.HashMap;
import java.util.List;
import java.util.ListResourceBundle;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.devices.DeviceExecutionException;
import com.bmw.cascade.devices.DynamicDevice;
import com.bmw.cascade.devices.DynamicDeviceManager;
import com.bmw.cascade.pruefprozeduren.parameter.InvalidParameterException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.util.dynamicdevices.PropertyObject;

/** 
 * CASCADE Job AscotIFE
 * 
 *	@author Michael Scholler, remes GmbH
 *
 *	@version 27_0_F	2018-02-12
 *	- MS: production release
 *	@version 26_0_T	2018-02-08
 *	- MS: ErrorMemory unexpected length handling changed
 *	- MS: replaced STATUS_EXECUTION_ABORTED_BY_USER result by STATUS_EXECUTION_ERROR when PP is canceled in user dialog
 *	@version 25_0_F	2017-12-04
 *	- MS: production release
 *	@version 24_0_T	2017-11-22
 *	- MS: fixed return on error: setPPStatus added
 *	- MS: inserted CancelJob to abort on Timeout and UserDialogCanceled
 *	@version 23_0_F	2017-1025
 *	- MS: rework ErrorMemory request
 *	@version 22_0_F	2017-10-17
 *	- MS: extended ErrorMemory request
 *	@version 21_0_F	2017-10-11
 *	- OR: Fix behavior when move relative routine returns different states than 4 or 5 to avoid unneeded delays and repetitions
 *	@version 20_0_F	2017-10-10
 *	- OR: Add delay for repetitions
 *  - OR: Make TesterPresent optional
 *  - OR: Wake up IFE using the request routine status command
 *	@version 19_0_F	2017-10-05
 *	- OR: Cleanup/optimize IO Error and ErrorMemory results
 *	@version 18_0_F	2017-09-29
 *	- OR: change moveRelative to behave similar to MPS3/4 test systems
 *        set new optional parameter EvaluateMSL=true to use old behavior
 *	@version 17_0_F	2017-07-31
 *	- MS: new timeout calculation (cf. ASCOTBase.java)
 *	@version 16_0_F	2017-05-05
 *	- MS: casting Long to Integer for PropertyObject encapsulation
 *	- MS: fixed MotorStopLogger UDS-ID to 0x5db2(BFT) and 0x5db3(BFH) 
 *	- MS: formating of measurement value using (Locale)null for dot as decimal separator 
 *	- MS: PowerOnDelay, UdsTimeout as Double in [s]
 *	@version 15_0_F	2017-04-20
 *	- MS: changed casing of PP internal parameter
 *	- MS: timeout and dialog_timeout in [s]
 *	- MS: formating of measurement value according to given precision(minimum=3)
 *	@version 14_0_F	2017-04-10
 *	- MS: moveRelative stopped successfully message in ergebnisWert
 *	- MS: formating of measurement value according to given precision(default=3)
 *	- MS: new parameter UdsTimeout
 *	@version 13_0_F	2017-04-03
 *	- MS: production release
 *	- MS: JobValue[~] will be added to results
 *	@version 12_0_T 2017-03-27
 *	- MS: dialog can be canceled and will abort execute()
 *	- MS: fixed condition to output parameter COMPONENT
 *	- MS: changed result handling
 *	- MS: changed data handling in ErrorMemory and MotorStopLogger
 *	@version 11_0_T	2017-03-16
 *	- MS: fixed ArrayOutOfBoundException while reading ErrorMemory
 *	- MS: adapted to template ASCOTBase
 *	@version 10_0_T	2017-03-02
 *	- MS: fixed error in result handling
 *	@version 9_0_F 2017-03-02
 *	- BMW: rollback from 7_0_F
 *	@version 8_0_F 2017-03-01
 *	- MS: new Parameter PowerSource, Voltage, CurrentLimit, MaxRepetitions, PowerOnDelay
 *	- MS: extended MotorStopLogger, ErrorMemory handling
 *	- MS: UserDialog now not closed at each subcommand
 *	- MS: changed result handling
 *	@version 7_0_F 2017-02-14 
 *	- MS: NAD for rear doors changed to 7
 *	- MS: getErrorMemory() implemented
 *	- MS: userdialog in getMotorStopLogger() disabled
 *	- MS: moveRelative() return value 5 will write only an ignore result
 *	@version 6_0_F 2017-02-03
 *	- MS: new Parameter Stroke, GetErrorMemory, GetMotorStopLogger
 *	- MS: added lin paramter for BFT, FHT, BHT
 *	@version 5_0_F	
 *	- MS: introduced Parameter TIMEOUT
 *	@version 4_0_F 2016-12-08
 *	- MS: replaced TranslatedText by ResourceBundle
 *	- MS: move Parameter initialization from constructor to execute()
 *	@version 3_0_F 2016-xx-xx
 *	- MS: longer delay in JobResult polling
 *	@version 2_0_F 2016-11-28
 *	- MS: restructured to remove external dependencies	
 *	@version 1_0_F 2016-11-11
 *	- MS: initial release	
 */

public class AscotIFE_27_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	static final long serialVersionUID = 0x08122972300000L;
	private String Werkzeug = "ASCOT";
	private String jobName	= "AscotIFE";
	private Boolean checkAscotParameters = false;
	private Double      jobCancelWaitTime = 1.0;

	private UserDialog	userDialog	= null;
	private Boolean		debug		= false;
	private Double		timeout		= /*<*CascadeTimeoutDefault*>*/30.000;
	private String		component   = "";
	private String 		originalComponent = "";
	
	private String linInterface;
	private int nad;
	private int udsRoutineId_MoveRelative;
	private int udsDataId_ErrorMemory;
	private int udsDataId_MotorStopLogger;
	private double udsTimeout;
	private boolean evaluateMsl;
	private boolean sendTesterPresent;
	private double repetitionDelay;

	/**
	 * Default constructor, only for de-seriaslisation
	 */
	public AscotIFE_27_0_F_Pruefprozedur() {
	}

	/**
	 * Creates a new test procedure
	 * 
	 * @param pruefling			Class of related Pruefling
	 * @param pruefprozName		Name of test procedure
	 * @param hasToBeExecuted	Execution condition for absence of errors
	 */
	public AscotIFE_27_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialises arguments
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

   /**
 	* Provides mandatory arguments
 	*/
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "PwrPin", "GndPin", "LinPin", "DOOR" };
		return args;
	}
	/** 
	 * Provides optional arguments
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG", "DELAY", "PAUSE", "TIMEOUT", "COMPONENT", 
				"DIALOG_TITLE", "DIALOG_TEXT", "DIALOG_CANCEL", "DIALOG_STYLE", "DIALOG_TIME",
				"PowerSource", "Voltage", "CurrentLimit", "Interface",
				"STROKE", "MAXREPETITIONS", "POWERONDELAY", 
				"GETERRORMEMORY", "GETMOTORSTOPLOGGER",
				"UdsTimeout", "EvaluateMSL", "SendTesterPresent", "RepetitionDelay", "LinGndPin" };
		return args;
	}

	/**
     *	Pr�ft die Argumente auf Existenz und Wert.
     *
	 * @return <code>false</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
     */
	@Override
	public boolean checkArgs() {
	   boolean ok = super.checkArgs();   	// obligatorische Existenzpr�fung 
	   return ok;
	}

	private void exportArgs() {
		declareParameter( "DEBUG",					"",						ParameterType.Boolean,	false );
		declareParameter( "DELAY",					"",						ParameterType.Double,	0.0 );
		declareParameter( "PAUSE",					"",						ParameterType.Double,	0.0 );
		declareParameter( "TIMEOUT",				"",						ParameterType.Double,	timeout );
		declareParameter( "COMPONENT",				"",						ParameterType.String,	"" );

		declareParameter( "DIALOG_TITLE",			"",						ParameterType.String,	"" );
		declareParameter( "DIALOG_TEXT",			"",						ParameterType.String,	"" );
		declareParameter( "DIALOG_CANCEL",			"",						ParameterType.Boolean,	false );
		declareParameter( "DIALOG_STYLE",			"",						ParameterType.Long,		0 );
		declareParameter( "DIALOG_TIME",			"",						ParameterType.Double,	-1.0 );

		// job specific parameter
		declareParameter("PwrPin",					"PwrPin",				ParameterType.Long, null);
		declareParameter("GndPin",					"GndPin",				ParameterType.Long, null);
		declareParameter("LinPin",					"LinPin",				ParameterType.Long, null);
		
		declareParameter("PowerSource",				"PowerSource",			ParameterType.String,	"40A Pwr");
		declareParameter("Voltage",					"Voltage",				ParameterType.Double,	13.5);
		declareParameter("CurrentLimit",			"CurrentLimit", 		ParameterType.Double,	40.0);

		declareParameter("Interface",				"Interface",			ParameterType.String,	"LIN2");

		declareParameter("DOOR",					"",						ParameterType.String, null);
		
		declareParameter("STROKE", 	 				"",						ParameterType.Long, 0);
		declareParameter("MAXREPETITIONS", 	 		"",						ParameterType.Long, 2);
		
		declareParameter("GETERRORMEMORY",			"",						ParameterType.Boolean, false);
		declareParameter("GETMOTORSTOPLOGGER",		"",						ParameterType.Boolean, false);

		declareParameter("POWERONDELAY",			"",				 		ParameterType.Double, 	0.0);

		declareParameter("UdsTimeout",				"Timeout",				ParameterType.Double, 	0.500);
		
		declareParameter("EvaluateMSL",				"",						ParameterType.Boolean, 	false);
		declareParameter("SendTesterPresent",		"",						ParameterType.Boolean, 	false);
		declareParameter("RepetitionDelay",			"",						ParameterType.Double, 	0.3);
		declareParameter("LinGndPin",				"",						ParameterType.Long, 	0);	
	}

	/**
	 * Executes test procedure
	 * 
	 * @param info	Information for execution
	 */
	public void execute(ExecutionInfo info) {
		Vector<Ergebnis> ergebnisListe = new Vector<Ergebnis>();
		HashMap<String, Object> jobParameter = new HashMap<String, Object>();
		JobResult jobResult;
		XREF<Integer> status = new XREF<Integer>(STATUS_EXECUTION_ERROR);
		boolean copyResultOnSuccess = false;

		Integer pwrPin;
		Integer gndPin;
		Integer linPin;
		Integer linGndPin;
		String door;

		String powerSource;
		Double voltage;
		Double currentLimit;
		Double powerOnDelay;

		Integer stroke;
		String strokeParam;
		Integer targetPosition;
		
		Integer maxRepetitions;

		Boolean execGetErrorMemory;
		Boolean execGetMotorStopLogger;

		Double delay;
		Double pause;

		String  dialogTitle;
		String  dialogText;
		Boolean dialogCancel;
		Integer dialogStyle;
		Double	dialogTime;

		if(jobName.isEmpty()) {
			addResultsException(ergebnisListe, new Exception(bundle.getString("NoJobName")));
			setPPStatus(info, STATUS_EXECUTION_ERROR, ergebnisListe);
			return;
		}

		try {  // special handling for debug flag
			String DEBUG = getArg("DEBUG").toUpperCase();
			if(DEBUG.startsWith("T") || DEBUG.startsWith("Y") || DEBUG.startsWith("J")) {
				debug=true;
			} else if(Double.parseDouble(DEBUG)>0)	{
				debug=true;
			}
		} catch (Exception e) {
			debug=false;
		}

		if(debug) {
			System.out.println(String.format("<%s> %s.%s.%s[%s] -> %s v%s [%s] => %s()", 
					this.getPr�fling().getAuftrag().getFahrgestellnummer7(),
					this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getName(), this.getName(),this.getPr�fling().getName(),this.getPr�fling().getClassName(), 
					this.getPPName(), this.getVersion(), this.getClassName(), jobName
			));
		}

		//Aufruf der Argumentpr�fung  
		try {
			exportArgs();
			if (checkArgs() == false) throw new PPExecutionException( PB.getString("parameterexistenz" ));
			parseArgs();

			delay     	  			= (Double)  getParameter("DELAY");
			pause      				= (Double)  getParameter("PAUSE");
//			timeout 				= (Double)  getParameter("TIMEOUT");
			timeout 				= (Double)  /*<*CascadeTimeout*>*/timeout;
			component    			= (String)  getParameter("COMPONENT");
			originalComponent = component;
			
			dialogTitle    			= (String)  getParameter("DIALOG_TITLE");
			dialogText     			= (String)  getParameter("DIALOG_TEXT");
			dialogCancel   			= (Boolean) getParameter("DIALOG_CANCEL");
			dialogStyle    			= (Integer) getParameter("DIALOG_STYLE");
			dialogTime     			= (Double)  getParameter("DIALOG_TIME");

			pwrPin 					= (Integer) getParameter("PwrPin");
			gndPin 					= (Integer) getParameter("GndPin");
			linPin 					= (Integer) getParameter("LinPin");
			linGndPin 				= (Integer) getParameter("LinGndPin");
			door    				= (String)  getParameter("Door");

			linInterface 			= (String) 	getParameter("Interface");
			powerSource  			= (String) 	getParameter("PowerSource");
			voltage  				= (Double)  getParameter("Voltage");
			currentLimit			= (Double)  getParameter("CurrentLimit");
			powerOnDelay			= (Double)  getParameter("PowerOnDelay");
			udsTimeout				= (Double)  getParameter("UdsTimeout");
			evaluateMsl				= (Boolean) getParameter("EvaluateMSL");
			sendTesterPresent		= (Boolean) getParameter("SendTesterPresent");
			repetitionDelay			= (Double)  getParameter("RepetitionDelay");
	
			stroke					= (Integer) getParameter("Stroke");
			// test code to test slightly window open
			/*
			if (stroke != 0) {
				System.out.println("IFE: call stroke=" + stroke);
				if (stroke < 2000) {
					stroke = 200;
				} else if (stroke > 2000) {
					stroke = -2500;
				}
			}*/

			strokeParam  			= strokeToParam(stroke);//"ff ff 09 c4";//+2500
			targetPosition 			= getTargetPosition(stroke);
			
			maxRepetitions  		= (Integer) getParameter("MaxRepetitions");
	
			execGetErrorMemory      = (Boolean) getParameter("GetErrorMemory");
			execGetMotorStopLogger  = (Boolean) getParameter("GetMotorStopLogger");

		} catch (PPExecutionException e) {
			addResultsException(ergebnisListe, e);
			setPPStatus(info, STATUS_EXECUTION_ERROR, ergebnisListe);
			return;
		}

		try {
			if((!dialogTitle.isEmpty()) || (!dialogText.isEmpty())) {
				if (getPr�flingLaufzeitUmgebung() != null) {
					userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
					if (userDialog != null) {
						userDialog.setAllowCancel(dialogCancel);

						switch( dialogStyle ) {
							case 1:
								userDialog.displayStatusMessage(dialogTitle, dialogText, (int)Math.round(dialogTime*1000));
								break;
							case 2:
								userDialog.displayAlertMessage(dialogTitle, dialogText, (int)Math.round(dialogTime*1000));
								break;
							case 99:
								userDialog.displayMessage(dialogTitle, dialogText, (int)Math.round(dialogTime*1000));
								break;
							default:
								userDialog.displayUserMessage(dialogTitle, dialogText, (int)Math.round(dialogTime*1000));
								break;
						}
					}
				}
			}
		} catch (Exception e) {
			addResultsException(ergebnisListe, new Exception(bundle.getString("UserDialogOpenFail")+": "+e.getMessage()));
			setPPStatus(info, STATUS_EXECUTION_ERROR, ergebnisListe);
			return;
		}

		if (delay > 0) {
			try {
				Thread.sleep(Math.round(delay));
			}
			catch( InterruptedException e ) {
			}        	
		}

		/* run command sequence */
		Boolean switchOffPowerSource = false;
		Boolean switchOffPwrPin = false;
		Boolean switchOffGndPin = false;
		Boolean switchOffLinPin = false;
		Boolean switchOffLinGndPin = false;

		try {
			evaluateDoorType(door);
			
			// set power on
			jobParameter.clear();
			jobParameter.put("PowerSource",powerSource);
			jobParameter.put("Voltage", voltage);
			jobParameter.put("CurrentLimit", currentLimit);
			jobParameter.put("PwrPin", pwrPin);
			jobParameter.put("GndPin", gndPin);		
			jobResult = executeJob("AscotSetPowerSourceOn", jobParameter, ergebnisListe, copyResultOnSuccess, status);
			if (!jobResult.getExecutionOk()) {
				String msg = bundle.getString("UnableToPowerOn");
				System.out.println(msg);
				throw new PPExecutionException(msg);
			}
			switchOffPowerSource = jobResult.getReturnParameterAsBoolean("SwitchOffPowerSource");
			switchOffPwrPin = jobResult.getReturnParameterAsBoolean("SwitchOffPwrPin");
			switchOffGndPin = jobResult.getReturnParameterAsBoolean("SwitchOffGndPin");
			
			//switch on LIN Pin
			jobParameter.clear();
			jobParameter.put("FunctionName",linInterface);
			jobParameter.put("Pin", linPin);		
			jobParameter.put("State", true);		
			jobResult = executeJob("AscotSetFunctionPin", jobParameter, ergebnisListe, copyResultOnSuccess, status);
			if (!jobResult.getExecutionOk()) {
				String msg = bundle.getString("UnableToConnectLIN");
				System.out.println(msg);
				throw new PPExecutionException(msg);
			}
			switchOffLinPin = jobResult.getReturnValueAsBoolean();
			
			//switch on LIN Gnd Pin
			if (linGndPin > 0) {
				jobParameter.clear();
				jobParameter.put("FunctionName", "Gnd");
				jobParameter.put("Pin", linGndPin);		
				jobParameter.put("State", true);		
				jobResult = executeJob("AscotSetFunctionPin", jobParameter, ergebnisListe, copyResultOnSuccess, status);
				if (!jobResult.getExecutionOk()) {
					String msg = bundle.getString("UnableToConnectLIN");
					System.out.println(msg);
					throw new PPExecutionException(msg);
				}
				switchOffLinGndPin = jobResult.getReturnValueAsBoolean();
			}
			Thread.sleep(Math.round(powerOnDelay*1000));
			
			// start cyclic TesterPresent
			if (sendTesterPresent) {
				jobParameter.clear();
				jobParameter.put("Interface",linInterface);
				jobParameter.put("Nad",nad);
				jobParameter.put("SuppressPositiveResponse",true);
				component = "IFE StartCyclciTesterPresent";
				jobResult = executeJob("AscotLinUdsStartCyclicTesterPresent", jobParameter, ergebnisListe, copyResultOnSuccess, status);
				component = originalComponent;
				if(!jobResult.getExecutionOk())	{
					setPPStatus(info, STATUS_EXECUTION_ERROR, ergebnisListe);
					return;
				}
			}
			
			// wakeup IFE
			if (!wakeupIfe(ergebnisListe, status)) {
				setPPStatus(info, STATUS_EXECUTION_ERROR, ergebnisListe);				
				return;
			}
				
			boolean overAllResult = true;
			if(stroke!=0) {
				if (evaluateMsl) {
					if(!moveRelativeMsl(bundle.getString("Stroke"), strokeParam, maxRepetitions, ergebnisListe, status))
						overAllResult &= false;
				} else {
					if(!moveRelativeNoMsl(bundle.getString("Stroke"), strokeParam, targetPosition, maxRepetitions, ergebnisListe, status))
						overAllResult &= false;
				}
			}

			if(execGetErrorMemory) {
				ErrorMemory em = new ErrorMemory(ergebnisListe, status);
				boolean button = em.isButtonOK();
				if (!button) {
					ergebnisListe.add(new Ergebnis(
						"",									// ID
						Werkzeug,							// werkzeug
						"",									// parameter_1 
						"",									// parameter_2
						"",									// parameter_3
						"IFE ErrorMemory: Button",			// ergebnis
						em.getButtonValue(),				// ergebnisWert
						"",									// minWert
						"",									// maxWert
						"",									// wiederholungen
						"",									// frageText
						"",									// antwortText
						"",									// anweisText
						em.getButtonErrorMessage(),			// fehlerText
						"",									// hinweisText
						Ergebnis.FT_NIO						// fehlerTyp
					));
				}
				overAllResult &= button;
			}

			if(execGetMotorStopLogger) {
				MotorStopLogger msl = new MotorStopLogger(ergebnisListe, status);
				boolean ok = msl.isOK();
				addResultsErgebnis(ergebnisListe, "MotorStopLogger", msl.raw, ok?Ergebnis.FT_IO:Ergebnis.FT_NIO);

				overAllResult &= ok;
			}
			
			if(overAllResult) {
//				ergebnisListe.add(new Ergebnis("", Werkzeug, "", "", "", "", "", "", "", "", "", "", "", bundle.getString("Success"), "", Ergebnis.FT_IO));
				status.setValue(STATUS_EXECUTION_OK);
			}

		} catch(Exception e) {
			addResultsException(ergebnisListe, e);
			status.setValue(STATUS_EXECUTION_ERROR);
		} finally {
			try {
				// stop Cyclic TesterPresent
				if(sendTesterPresent) {
					jobParameter.clear();
					jobParameter.put("Interface",linInterface);
					component = "IFE StopCyclicTesterPresent";
					jobResult = executeJob("AscotLinUdsStopCyclicTesterPresent", jobParameter, ergebnisListe, copyResultOnSuccess, status);
				    component = originalComponent;
				}
				
				// Power off
				jobParameter.clear();
				jobParameter.put("PowerSource",powerSource);
				jobParameter.put("PwrPin",pwrPin);
				jobParameter.put("GndPin",gndPin);
				jobParameter.put("SwitchOffPowerSource",switchOffPowerSource);
				jobParameter.put("SwitchOffPwrPin",switchOffPwrPin);
				jobParameter.put("SwitchOffGndPin",switchOffGndPin);
				jobResult = executeJob("AscotSetPowerSourceOff", jobParameter, ergebnisListe, copyResultOnSuccess, status);
				
			    if(switchOffLinPin) {
					jobParameter.clear();
					jobParameter.put("FunctionName",linInterface);
					jobParameter.put("Pin",linPin);
					jobParameter.put("State",false);
					jobResult = executeJob("AscotSetFunctionPin", jobParameter, ergebnisListe, copyResultOnSuccess, status);
			    }
				
			    if(switchOffLinGndPin && linGndPin > 0) {
					jobParameter.clear();
					jobParameter.put("FunctionName","Gnd");
					jobParameter.put("Pin",linGndPin);
					jobParameter.put("State",false);
					jobResult = executeJob("AscotSetFunctionPin", jobParameter, ergebnisListe, copyResultOnSuccess, status);
			    }
			} catch(Exception e) {
				addResultsException(ergebnisListe, e);
				status.setValue(STATUS_EXECUTION_ERROR);
			}
		}

		if (pause > 0) {
            try {
            	Thread.sleep(Math.round(pause));
            }
            catch( InterruptedException e ) {
            }        	
    	}

		//cleanup
		try {
	        //close dialog
			if ((userDialog != null) && (userDialog.isShowing())) {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			}
		} catch (Exception e) {
			addResultsException(ergebnisListe, e);
		}

		if(ergebnisListe.size()==0) {
			addResultsErgebnis(ergebnisListe, bundle.getString("Result"),
				status.getValue()==STATUS_EXECUTION_OK?bundle.getString("Success"):bundle.getString("Fail"), 
				status.getValue()==STATUS_EXECUTION_OK?Ergebnis.FT_IO:Ergebnis.FT_NIO );
		}
		setPPStatus(info, status.getValue(), ergebnisListe);
		if(debug) {
			String results = "\n";
			for(Ergebnis e : ergebnisListe) {
				results += String.format("\t%s %-8.8s %1.1s  %-40.40s=%-30.30s [%-8.8s,%-8.8s] frage=\"%s\" antwort=\"%s\" anweisung=\"%s\" fehler=\"%s\" hinweis=\"%s\" p1=%s p2=%s p3=%s\n", 
						e.getID(), e.getWerkzeug(), e.getFehlerTyp(),
						e.getErgebnis(), e.getErgebnisWert(), e.getMinWert(), e.getMaxWert(),
						e.getFrageText(), e.getAntwortText(), e.getAnweisText(), e.getFehlerText(), e.getHinweisText(),
						e.getParameter1(), e.getParameter2(), e.getParameter3()
				);
			}
			System.out.println(String.format("%s.%s Results = {%s} finished [%d=%s]",	this.getPr�fling().getName(), this.getName(), results, status.getValue(), statusAsString(status.getValue())));
		}
	}

	private String strokeToParam(int stroke) {
		String hex = String.format("%04x", stroke);
		int n = hex.length();
		hex="ffff" + hex.substring(n-4,n-0);
		String out="";
		for(int l=0;l<8;l++) {
			out += hex.charAt(l);
			if(l%2 == 1) out += " ";
		}
		return out.trim();
	}
	
	private Integer getTargetPosition(int stroke) {
		if (stroke < -2000) // close window
			return 0; // window fully open
		if (stroke > 2000) // open window
			return 2; // window fully closed
		return 1; // windows in middle position
	}

	private void evaluateDoorType(String door) throws InvalidParameterException {
		if((door.compareToIgnoreCase("FA")==0) || (door.compareToIgnoreCase("FAT")==0)) {
			nad=6;
			udsRoutineId_MoveRelative		= 0x9996;
			udsDataId_ErrorMemory     		= 0x9986;
			udsDataId_MotorStopLogger 		= 0x6042;
		} else if((door.compareToIgnoreCase("FAH")==0) || (door.compareToIgnoreCase("FHT")==0)) {
			nad=7;
			udsRoutineId_MoveRelative		= 0x9997;
			udsDataId_ErrorMemory     		= 0x9987;
			udsDataId_MotorStopLogger		= 0x6043;
		} else if((door.compareToIgnoreCase("BF")==0) || (door.compareToIgnoreCase("BFT")==0)) {
			nad=6;
			udsRoutineId_MoveRelative		= 0x9998;
			udsDataId_ErrorMemory			= 0x9988;
			udsDataId_MotorStopLogger		= 0x5db2;
		} else if((door.compareToIgnoreCase("BFH")==0) || (door.compareToIgnoreCase("BHT")==0)) {
			nad=7;
			udsRoutineId_MoveRelative		= 0x9999;
			udsDataId_ErrorMemory			= 0x9989;
			udsDataId_MotorStopLogger		= 0x5db3;
		} else {
			throw new InvalidParameterException("Unknown Door Type ["+ door +"]", door);
		}
	}
	
	private Boolean wakeupIfe(Vector<Ergebnis> ergebnisListe, XREF<Integer> status) {
		HashMap<String, Object> parameter = new HashMap<String, Object>();
		JobResult result;
		boolean copyResultOnSuccess = false;
		Vector<Ergebnis> localResults = new Vector<Ergebnis>();

		parameter.clear();
		parameter.put("Interface",linInterface);
		parameter.put("Nad",nad);
		parameter.put("RoutineId",udsRoutineId_MoveRelative);
		parameter.put("Data", "");
		parameter.put("Timeout", Math.round(udsTimeout*1000));
		Integer repetition = 0;
		Integer okCount = 0;
		while (true) {
			++repetition;
			
			localResults.clear();
			component = "IFE Read initial status (wakeup)";
			result = executeJob("AscotLinUdsRequestRoutineStatusGeneric", parameter, localResults, copyResultOnSuccess, status);
			component = originalComponent;
			if(result.getExecutionOk()) {
				--repetition;
				++okCount;
				if(okCount >= 2)
					return true;
			}
			if (repetition >= 5)
				break;
			try {Thread.sleep(100); } catch (Exception e) { }
		}
		// we only get here on errors
		for (int n = 0; n < localResults.size(); ++n) {
			ergebnisListe.add(localResults.get(n));
		}
		return false;
	}
	
	private Boolean moveRelativeNoMsl(String text, String routineParams, Integer targetPosition, int maxRepetition, Vector<Ergebnis> ergebnisListe, XREF<Integer> status) {
		HashMap<String, Object> parameter = new HashMap<String, Object>();
		JobResult result;
		boolean copyResultOnSuccess = false;
		Vector<Ergebnis> localResults = new Vector<Ergebnis>();
		MotorStopLogger msl;
		
		try {
			// outer repeat loop for general repetitions
			for(int i=0;i<maxRepetition;i++) {
				System.out.println("MoveRelative: Outer repetition " + (i + 1));
				
				// inner repeat loop for up to 5 tries to move the window
				int moveRepetition = 0;
				do {
					if (i > 0 || moveRepetition > 0) {
						try {
							Thread.sleep((long)(repetitionDelay * 1000));
						}
						catch(Exception e) { }
					}
						
					++moveRepetition;
					if (moveRepetition > 5) {
						System.out.println("MoveRelative: max inner repetitions reached");
						break;
					}
					System.out.println("MoveRelative: inner repetition " + moveRepetition);
					
					localResults.clear();
					parameter.clear();
					parameter.put("Interface",linInterface);
					parameter.put("Nad",nad);
					parameter.put("RoutineId",udsRoutineId_MoveRelative);
					parameter.put("Data",routineParams);
					parameter.put("Timeout", Math.round(udsTimeout*1000));
					component = "IFE StartRoutine MoveRelative";
					result = executeJob("AscotLinUdsStartRoutine", parameter, localResults, copyResultOnSuccess, status);
					component = originalComponent;
					if(!result.getExecutionOk()) {
						continue;
					}
			
					//Integer routineReturn = -1;
					Integer routineStatus = 0;
					Integer routineResult = 0;
					
					System.out.println("MoveRelative: target=" + targetPosition);
		
					parameter.clear();
					parameter.put("Interface",linInterface);
					parameter.put("Nad",nad);
					parameter.put("RoutineId",udsRoutineId_MoveRelative);
					parameter.put("PollTimeout",15);
					parameter.put("Interval",500);
					parameter.put("ExpectedStatus","0 2 4 5 6 7"); // only 1 and 3 means routine is still running
					parameter.put("ExpectedResult","");
					component = "IFE PollRoutineStatus MoveRelative";
					result = executeJob("AscotLinUdsPollRoutineStatusGeneric", parameter, localResults, copyResultOnSuccess, status);
					component = originalComponent;
					
					if(result==null || !result.getExecutionOk()){
						System.out.println("MoveRelative: Timeout");
						continue;
					}
					
					//routineReturn = result.getReturnValueAsInteger();
					routineStatus = result.getReturnParameterAsInteger("Status");
					routineResult = result.getReturnParameterAsInteger("Result");

					if(routineStatus==null) {
						addResultsError(localResults, "Status=null");
						continue;
					}
					msl = new MotorStopLogger(localResults, status);
					System.out.println("MoveRelative: stat=" + routineStatus + ", res=" + routineResult + " " + msl.translateEntry(msl.getLastEntry()));
					// check routine results
					if(debug) {
						localResults.add(new Ergebnis("", Werkzeug, "", "", "", text+": Status", (routineStatus!=null)?routineStatus.toString():"null", "", "", "", "", "", "", "", bundle.getString("LIN_UDS_MoveRelative__"+routineStatus), Ergebnis.FT_IO));
						localResults.add(new Ergebnis("", Werkzeug, "", "", "", text+": Result", (routineResult!=null)?routineResult.toString():"null", "", "", "", "", "", "", "", "", Ergebnis.FT_IO));
					}
					switch(routineStatus) {
					case 0x01: //pending
					case 0x03: //routine is executing
						localResults.add(new Ergebnis("", Werkzeug, "", "", "", text, "", "", "", "", "", "", "", bundle.getString("LIN_UDS_MoveRelative__"+routineStatus), "", Ergebnis.FT_NIO));
						break;
					case 0x00: //service still not required
					case 0x02: //routine cannot be executed
					case 0x07: //invalid
						// cancel inner repetitions
						moveRepetition = 5;
						localResults.add(new Ergebnis("", Werkzeug, "", "", "", text, "", "", "", "", "", "", "", bundle.getString("LIN_UDS_MoveRelative__"+routineStatus), "", Ergebnis.FT_NIO));
						break;
					case 0x06: //routine interrupted
						// cancel inner repetitions
						moveRepetition = 5;
						localResults.add(new Ergebnis("", Werkzeug, "", "", "", text, "", "", "", "", "", "", "", bundle.getString("MoveInterrupted") + " " + msl.translateEntry(msl.getLastEntry()), "", Ergebnis.FT_NIO));
						break;
					case 0x05: //routine stopped with errors
						if (targetPosition != 1) {
							if (routineResult == 1) { 
								// windows is somewhere in the middle
								// cancel inner repetitions
								moveRepetition = 5;
								if (msl.getLastEntry() == 1) {
									localResults.add(new Ergebnis("", Werkzeug, "", "", "", text, "", "", "", "", "", "", "", bundle.getString("LIN_UDS_MoveRelative__"+routineStatus), "", Ergebnis.FT_NIO));
								} else if (msl.getLastEntry() == 6) {
									localResults.add(new Ergebnis("", Werkzeug, "", "", "", text, "", "", "", "", "", "", "", bundle.getString("WindowReversed"), "", Ergebnis.FT_NIO));
								} else {
									localResults.add(new Ergebnis("", Werkzeug, "", "", "", text, "", "", "", "", "", "", "", msl.translateEntry(msl.getLastEntry()), "", Ergebnis.FT_NIO));
								}
							} else if (routineResult >= 3) {
								localResults.add(new Ergebnis("", Werkzeug, "", "", "", text, "", "", "", "", "", "", "", bundle.getString("Fail") + ": " + msl.translateEntry(msl.getLastEntry()), "", Ergebnis.FT_NIO));
							} else {
								// reset repetitions and try again
								System.out.println("MoveRelative: reset inner repetitions count");
								moveRepetition = 0;
							}
						}
						break;
					case 0x04: //routine stopped successfully
						if (routineResult != targetPosition) {
							// window not at target position
							System.out.println("MoveRelative: not at target pos");
							localResults.add(new Ergebnis("", Werkzeug, "", "", "", text, bundle.getString("LIN_UDS_MoveRelative__"+routineStatus), "", "", "", "", "", "", bundle.getString("PositionNotReached"), "", Ergebnis.FT_NIO));
						} else {							
							localResults.add(new Ergebnis("", Werkzeug, "", "", "", text, "", "", "", "", "", "", "", "", "", Ergebnis.FT_IO));
							System.out.println("MoveRelative: OK");
							return true;
						}
					default:
						localResults.add(new Ergebnis("", Werkzeug, "", "", "", text, "", "", "", "", "", "", "", bundle.getString("LIN_UDS_MoveRelative__N"), "", Ergebnis.FT_NIO));
						break;
					}
					try {
						Thread.sleep(50);
					} catch(Exception e) {
						
					}
				} while (true);
			}
			System.out.println("MoveRelative: max outer repetions reached");
			return false;
		}
		finally	{
			// copy local results to external list
			for(int n = 0; n < localResults.size(); ++n) {
				ergebnisListe.add(localResults.get(n));
			}
		}
	}
	
	private Boolean moveRelativeMsl(String text, String routineParams, int maxRepetition, Vector<Ergebnis> ergebnisListe, XREF<Integer> status) {
		HashMap<String, Object> parameter = new HashMap<String, Object>();
		JobResult result;
		boolean copyResultOnSuccess = false;

//		Screen.SetCommandText(Text="IFE Test\nFenster �ffnen ...")
//		userdialog.displayMessage("IFE Test", text+" ...", -1);

		for(int i=0;i<maxRepetition;i++) {
//		Uds.StartRoutine(Interface=LIN2;BusParams=busParams;RoutineId=0x9996;Data=paramsDown)
			parameter.clear();
			parameter.put("Interface",linInterface);
			parameter.put("Nad",nad);
			parameter.put("RoutineId",udsRoutineId_MoveRelative);
			parameter.put("Data",routineParams);
			parameter.put("Timeout", Math.round(udsTimeout*1000));
			component = "IFE StartRoutine MoveRelative";
			result = executeJob("AscotLinUdsStartRoutine", parameter, ergebnisListe, copyResultOnSuccess, status);
			component = originalComponent;
			
			if(!result.getExecutionOk())				
				return false;
	
			Integer routineReturn = -1;
			Integer routineStatus = 0;
			Integer routineResult = 0;

//		Number finalResult = Uds.PollRoutineStatusGeneric(
//			Interface=LIN2;BusParams=busParams;RoutineId=0x9996;PollTimeout=15;
//			Interval=500;
//			ExpectedStatus=Number[0x04]; ExpectedResult=Number[];
//			ref Status=status; ref Result=result)
			parameter.clear();
			parameter.put("Interface",linInterface);
			parameter.put("Nad",nad);
			parameter.put("RoutineId",udsRoutineId_MoveRelative);
			parameter.put("PollTimeout",15);
			parameter.put("Interval",500);
			parameter.put("ExpectedStatus","4 5");
			parameter.put("ExpectedResult","");
			component = "IFE PollRoutineStatus MoveRelative";
			result = executeJob("AscotLinUdsPollRoutineStatusGeneric", parameter, ergebnisListe, copyResultOnSuccess, status);
			component = originalComponent;
			
			if(result==null || !result.getExecutionOk())				
				return false;
			
			routineReturn = result.getReturnValueAsInteger();
			routineStatus = result.getReturnParameterAsInteger("Status");
			routineResult = result.getReturnParameterAsInteger("Result");

			if(routineStatus==null) {
				addResultsError(ergebnisListe, "Status=null");
				return false;
			}
			// Ergebnisauswertung
			if(debug) {
				ergebnisListe.add(new Ergebnis("", Werkzeug, "", "", "", text+": Status", (routineStatus!=null)?routineStatus.toString():"null", "", "", "", "", "", "", "", bundle.getString("LIN_UDS_MoveRelative__"+routineStatus), Ergebnis.FT_IO));
				ergebnisListe.add(new Ergebnis("", Werkzeug, "", "", "", text+": Result", (routineResult!=null)?routineResult.toString():"null", "", "", "", "", "", "", "", "", Ergebnis.FT_IO));
			}
			switch(routineStatus) {
			case 0x00: //service still not required
			case 0x02: //routine cannot be executed
			case 0x06: //routine interrupted
			case 0x07: //invalid
				ergebnisListe.add(new Ergebnis("", Werkzeug, "", "", "", text, "", "", "", ((Integer)i).toString(), "", "", "", bundle.getString("LIN_UDS_MoveRelative__"+routineStatus), "", Ergebnis.FT_NIO));
				return false;
			case 0x01: //pending
			case 0x03: //routine is executing
				ergebnisListe.add(new Ergebnis("", Werkzeug, "", "", "", text, "", "", "", ((Integer)i).toString(), "", "", "", bundle.getString("LIN_UDS_MoveRelative__"+routineStatus), "", Ergebnis.FT_IGNORE));
				break;
			case 0x05: //routine stopped with errrors
				MotorStopLogger msl = new MotorStopLogger(ergebnisListe, status);
				if(msl.getLastEntry() == 1) {
					ergebnisListe.add(new Ergebnis("", Werkzeug, "", "", "", text, ((Integer)i).toString(), "", "", "", bundle.getString("LIN_UDS_MoveRelative__"+routineStatus), "", "", "", "", ((msl.getLastEntry()==1)?Ergebnis.FT_IO:Ergebnis.FT_IGNORE)));
					return true;
				} else {
					ergebnisListe.add(new Ergebnis("", Werkzeug, "", "", "", text, "", "", "", ((Integer)i).toString(), "", "", "", bundle.getString("LIN_UDS_MoveRelative__"+routineStatus), "", ((msl.getLastEntry()==1)?Ergebnis.FT_IO:Ergebnis.FT_IGNORE)));
				}
				break;
			case 0x04: //routine stopped succesfully
				ergebnisListe.add(new Ergebnis("", Werkzeug, "", "", "", text, bundle.getString("LIN_UDS_MoveRelative__"+routineStatus), "", "", ((Integer)i).toString(), "", "", "", "", "", Ergebnis.FT_IO));
				return true;
			default:
				ergebnisListe.add(new Ergebnis("", Werkzeug, "", "", "", text, "", "", "", ((Integer)i).toString(), "", "", "", bundle.getString("LIN_UDS_MoveRelative__N"), "", Ergebnis.FT_NIO));
				return false;
			}
			try {
				Thread.sleep(50);
			} catch(Exception e) {
				
			}
		}
		ergebnisListe.add(new Ergebnis("", Werkzeug, "", "", "", text, "", "", "", ((Integer)maxRepetition).toString(), "", "", "", bundle.getString("MaxCountReached"), "", Ergebnis.FT_NIO));
		return false;
	}

	protected class ErrorMemory {
		protected String raw;
		protected Vector<Integer> entries = new Vector<Integer>();

		
		public ErrorMemory(String entryCodes) {
			entries = parseEntries(entryCodes);
		}
		public ErrorMemory(Vector<Ergebnis> ergebnisListe, XREF<Integer> status) {
			entries = readEntriesFromECU(ergebnisListe, status);
		}
		private int expectedEntries = 8;
		private int offset=3; // data offset vs. offset in spec
		private Vector<Integer> parseEntries(Vector<Ergebnis> ergebnisListe) {
			if(ergebnisListe!=null) { 
				ergebnisListe.add(new Ergebnis("", Werkzeug, "", "", "", "IFE ErrorMemory", toString(), "", "", "", "", "", "", "", "", Ergebnis.FT_IGNORE));
			}

			entries.clear();
			String[] r = raw.split(", ");

			if(r.length==expectedEntries) {
				int n = 0;
				for(String s : r) {
					int entry = -1;
					n++;
					String message = bundle.getString("LIN_UDS_ErrorMemory__0xNN");
					try {
						entry = parseEntry(s);
						entries.add(entry);
						String values = "";
						String key = "";
						if(entry==0) {
							key = "LIN_UDS_ErrorMemory__Result"+(n+offset)+"__0x00";
							values = bundle.getString(key);
						} else if(entry==0xff) {
							key = "LIN_UDS_ErrorMemory__Result"+(n+offset)+"__0xff";
							values = bundle.getString(key);
						} else {
							for(int i=0; i<8; i++) {
								if((entry & (1<<i)) == (1<<i)) { 
									key = String.format("LIN_UDS_ErrorMemory__Result%d__0x%02x",n+offset,(1<<i));
									values += bundle.getString(key) + ",";
								}
							}
						}
						if(values.endsWith(",")) values = values.substring(0, values.length()-1);
						message = String.format("%-50.50s = [0x%02x] {%s}",bundle.getString("LIN_UDS_ErrorMemory__Result"+(n+offset)), entry, values);
					} catch (Exception e) {
						message = String.format("Error %-50.50s = [  ] %s",s,message);
					}
					System.out.println(message);
					if(ergebnisListe!=null) { 
						ergebnisListe.add(new Ergebnis("", Werkzeug, "", "", "", "IFE ErrorMemory", message, "", "", "", "", "", "", "", "", Ergebnis.FT_IGNORE));
					}
				}
			} else {
				ergebnisListe.add(new Ergebnis("", Werkzeug, "", "", "", "IFE ErrorMemory", "LIN_UDS_ErrorMemory__unexpected", "", "", "", "", "", "", "", "", Ergebnis.FT_NIO));				
			}
			return entries;

		}
		private Vector<Integer> parseEntries(String entryCodes) {
			raw = entryCodes;
			return parseEntries(new Vector<Ergebnis>());	//ergebnisList will be destroyed hereafter!			
		}
		private Integer parseEntry(String entryCode) throws Exception {
			return Integer.parseInt(entryCode,16); // parse as hex
		}

		public Vector<Integer> getEntries() {
			return entries;
		}

		public Boolean isOK() {
			return isButtonOK(); // && isOtherThingOK();
		}
			
		public Boolean isButtonOK() {
			//141013_IFE_System.pdf	5.3.2.1.1.1.14  _FH_FEHLER_DIAGNOSE (Fertigung)
			boolean err;
			err = (entries==null);
			if(err) {
				buttonErrorMessage = bundle.getString("LIN_UDS_ErrorMemory__invalid")+": [] "+toString();
				return !err;
			}
			err = (entries.size()<(12-offset-1));
			if(err) {
				buttonErrorMessage = bundle.getString("LIN_UDS_ErrorMemory__invalid")+": ["+entries.size()+"] "+toString();
				return !err;
			}

			int res10 = entries.get(10-offset-1);
			int res11 = entries.get(11-offset-1);
			buttonValue = ""+res10+" ,"+res11;
			if(res10 == 1) { // BUTTON_ERROR_ACTIVE
				if (res11 == 0) {
					if (!err) {
						err = true;
					}
					
					buttonErrorMessage = bundle.getString("IFE_ButtonError__0x00");
				} else if (res11 == 1) {
					if (!err) {
						err = true;
					}
					
					buttonErrorMessage = bundle.getString("IFE_ButtonError__0x01");
				} else if (res11 == 2) {
					if (!err) {
						err = true;
					}
					
					buttonErrorMessage = bundle.getString("IFE_ButtonError__0x02");
				} else if (res11 == 3) {
					if (!err) {
						err = true;
					}
					
					buttonErrorMessage = bundle.getString("IFE_ButtonError__0x03");
				} else { //if (res11 == 0xff) {
					if (!err) {
						err = true;
					}
					
					buttonErrorMessage = bundle.getString("IFE_ButtonError__0xff");
				}				
			}
			return !err;
		}

		String buttonValue = "";
		public String getButtonValue() {
			return buttonValue;
		}
		private String buttonErrorMessage="";
		public String getButtonErrorMessage() {
			if(buttonErrorMessage=="") isOK();
			return buttonErrorMessage;
		}
		
		public Vector<Integer> readEntriesFromECU(Vector<Ergebnis> ergebnisListe, XREF<Integer> status){
			HashMap<String, Object> parameter = new HashMap<String, Object>();
			JobResult result;
			boolean copyResultOnSuccess = false;
			entries.clear();
			buttonValue="";
			buttonErrorMessage="";
			
//			Uds.ReadDataByIdentifier(Interface=LIN2;BusParams=busParams;RoutineId=0x9986)
			parameter.clear();
			parameter.put("Interface",linInterface);
			parameter.put("Nad",nad);
			parameter.put("DataId",udsDataId_ErrorMemory);
			component = "IFE ReadDataByIdentifier ErrorMemory";
			result = executeJob("AscotLinUdsReadDataByIdentifier", parameter, ergebnisListe, copyResultOnSuccess, status);
			component = originalComponent;
			
			if(result.getExecutionOk()) {		
				String  routineReturn = result.getReturnValueAsString();
//				Integer routineStatus = result.getReturnParameterAsInteger("Status");
//				Integer routineResult = result.getReturnParameterAsInteger("Result");		
				System.out.println("IFE ErrorMemory: Return="+routineReturn);
//				System.out.println("IFE ErrorMemory: Status="+routineStatus);
//				System.out.println("IFE ErrorMemory: Result="+routineResult);

				raw = routineReturn;
				entries = parseEntries(ergebnisListe);
				
//				addResultsErgebnis(ergebnisListe, "ErrorMemory", linMessage, Ergebnis.FT_IO);
			}
			return entries;
		}
		
		public String toString() {
			return raw;
		}
	}
	
	protected class MotorStopLogger {	//wdIFE{666ff::GetMotorstopLogger()}
		private Integer defaultEntry = -1;
		protected String raw;
		protected Vector<Integer> entries = new Vector<Integer>();
		
		public MotorStopLogger(String entryCodes) {
			entries = parseEntries(entryCodes);
		}
		public MotorStopLogger(Vector<Ergebnis> ergebnisListe, XREF<Integer> status) {
			entries = readEntriesFromECU(ergebnisListe, status);
		}

		private Vector<Integer> parseEntries() {
			entries.clear();
			String[] r = raw.split(", ");
			if(r.length>0) {
				for(String s : r) {
					int entry = -1;
					String message = translateEntry(entry);
					try {
						entry = parseEntry(s);
						entries.add(entry);
						message = translateEntry(entry);
						System.out.println(String.format("MotorStopLogger: Reason %s = [0x%02x] %s",s,entry,message));
					} catch (Exception e) {
						System.out.println(String.format("MotorStopLogger: Reason %s = [  ] %s",s,message));
					}
				}
			}
			System.out.println(String.format("MotorStopLogger: Reasons #%d", entries.size()));
			return entries;
		}
		private Vector<Integer> parseEntries(String reasonCodes) {
			raw = reasonCodes;
			return parseEntries();
		}
		private Integer parseEntry(String reasonCode) throws Exception {
			return Integer.parseInt(reasonCode,16); // parse as hex
		}

		public Vector<Integer> getEntries() {
			return entries;
		}
		public Integer getLastEntry() {
			Integer entry = defaultEntry;
			if(isOK()) {
				entry = entries.firstElement();
			}
			System.out.println("MotorStopLogger: getLastEntry()="+entry);
			return entry;
		}
		public String translateLastEntry() {
			return translateEntry(getLastEntry());
		}
		public String translateEntry(Integer reason) {
			String stopReason="";
			if(reason<0) {
				stopReason = bundle.getString("LIN_UDS_MotorStopLogger__invalid");
			} else
			if((reason >= 0x00) && (reason <= 0x1f)) {
				stopReason = bundle.getString(String.format("LIN_UDS_MotorStopLogger__0x%02x", reason));
			} else
			if((reason >= 0x60) && (reason <= 0x6d)) { // customer specific
				stopReason = bundle.getString(String.format("LIN_UDS_MotorStopLogger__0x%02x", reason));
			} else {
				stopReason = bundle.getString("LIN_UDS_MotorStopLogger__0xNN");
				stopReason.replace("??", Integer.toHexString(reason));
			}
			return stopReason;		
		}
		
		public Boolean isOK() {
			return entries!=null && entries.size()>0;
		}

		public Vector<Integer> readEntriesFromECU(Vector<Ergebnis> ergebnisListe, XREF<Integer> status) {
			HashMap<String, Object> parameter = new HashMap<String, Object>();
			JobResult result;
			boolean copyResultOnSuccess = false;
			entries.clear();
			
//			Uds.ReadDataByIdentifier(Interface=LIN2;BusParams=busParams;RoutineId=0x6042)
			parameter.clear();
			parameter.put("Interface",linInterface);
			parameter.put("Nad",nad);
			parameter.put("DataId",udsDataId_MotorStopLogger);
			component = "IFE ReadDataByIdentiifer MotorStopLogger";
			result = executeJob("AscotLinUdsReadDataByIdentifier", parameter, ergebnisListe, copyResultOnSuccess, status);
			component = originalComponent;
			
			if(result.getExecutionOk()) {
				String  routineReturn = result.getReturnValueAsString();
//				Integer routineStatus = result.getReturnParameterAsInteger("Status");
//				Integer routineResult = result.getReturnParameterAsInteger("Result");			
				System.out.println("MotorStopLogger: Return="+routineReturn);
//				System.out.println("MotorStopLogger: Status="+routineStatus);
//				System.out.println("MotorStopLogger: Result="+routineResult);
				
				entries = parseEntries(routineReturn);

//				addResultsErgebnis(ergebnisListe, "MotorStopLogger", linMessage, Ergebnis.FT_IO);
			}
			return entries;
		}

		public String toString() {
			return raw;
		}
	}

	/**
	 * Executes test procedure
	 * 
	 * @param jobName	job to execute
	 * @param parameter	job parameter
	 * @return		ASCOTJobResult
	 */
	private JobResult executeJob( String jobName, HashMap<String, Object> parameter ) {
		DynamicDeviceManager ddm = null;
		DynamicDevice ascotDevice = null;
		
		JobResult result = new JobResult();
		result.status = STATUS_EXECUTION_ERROR;

		if(debug) {
			addResultsErgebnis(result.ergebnisse, "JobName", jobName, Ergebnis.FT_IO);
		}

		HashMap<String, PropertyObject> jobParameter = new HashMap<String, PropertyObject>();
		for( String s : parameter.keySet() ) {
			if(s=="") continue;
			if(debug) {
				addResultsErgebnis(result.ergebnisse, "Parameter["+s+"]", parameter.get(s).toString(), Ergebnis.FT_IO);
			}
			try {
				if(checkAscotParameters) {
					Parameter par = null; 
					if(parameters.containsKey(s.toUpperCase())) {
						par = parameters.get(s.toUpperCase());
					}
					if(par==null)
						throw new Exception(String.format(bundle.getString("ParameterUnknown__sName"), s));
					if(par._ascotName=="")
						continue;
					jobParameter.put( par._ascotName, getObjectAsPropertyObject(parameter.get(s)) );
				} else {
					jobParameter.put( s, getObjectAsPropertyObject(parameter.get(s)) );
				}
			} catch ( Exception e ) {
				addResultsException( result.ergebnisse, e );
				return result;
			}
		}
		
		try {
			ddm = getPr�flingLaufzeitUmgebung().getDynamicDeviceManager();
			ascotDevice = ddm.requestDevice( Werkzeug, Werkzeug );
			if ( ascotDevice == null ) {
				throw new DeviceExecutionException ( bundle.getString("RequestDeviceFail") );
			}
		} catch (Exception e) {
			addResultsException( result.ergebnisse, e );
			return result;
		}

		Set<String> methods = ascotDevice.getMethodNames();
		if(!methods.contains("StartJob") 
		|| !methods.contains("GetJobStatus") 
		|| !methods.contains("GetJobResults") 
		|| !methods.contains("GetJobValues")
		|| !methods.contains("CancelJob")) {
			addResultsError( result.ergebnisse, bundle.getString("GetMethodNamesFail") );
			return result;
		}
		
		try {
			int jobToken = 0;
			int jobStatus = 0;
			List<PropertyObject> jobResult;

			// StartJob
			long stopTime = System.currentTimeMillis() + Math.round(timeout*1000);
			jobResult  = ascotDevice.execute( "StartJob", new PropertyObject(jobName), new PropertyObject(jobParameter) );
			jobToken = jobResult.get(0).asInt();

			do { // GetJobStatus
				Thread.sleep(1);
				jobResult = ascotDevice.execute( "GetJobStatus", new PropertyObject(jobToken) );
				jobStatus = jobResult.get(0).asInt();

				if(userDialog!=null && userDialog.isCancelled()) {
					jobStatus = JobStatus.Canceled;
					ascotDevice.execute("CancelJob", new PropertyObject(jobToken), new PropertyObject(jobCancelWaitTime));
					break;
				}
				if( (timeout > 0) && (System.currentTimeMillis() > stopTime) ) {
					jobStatus = JobStatus.Timeout;
					ascotDevice.execute("CancelJob", new PropertyObject(jobToken), new PropertyObject(jobCancelWaitTime));
					break;
				}
			} while( jobStatus == JobStatus.Running );
			
			switch( jobStatus ) {
				case JobStatus.Succeeded:
				case JobStatus.Failed:
//						addResultsErgebnis( result.ergebnisse, bundle.getString("Result"), 
//								jobStatus==JobStatus.Succeeded?bundle.getString("Success"):bundle.getString("Fail"), 
//								jobStatus==JobStatus.Succeeded?Ergebnis.FT_IO:Ergebnis.FT_NIO );

						// GetJobResult
						jobResult = ascotDevice.execute( "GetJobResults", new PropertyObject(jobToken) );
						if( jobResult != null ) {
							for( PropertyObject po : jobResult ) {
								switch( po.getType() ) {
								case BOOLEAN:
									addResultsErgebnis( result.ergebnisse, bundle.getString("Result"), ((Boolean)(po.asBoolean())).toString(), Ergebnis.FT_IO );
									break;
								case INTEGER:
									addResultsErgebnis( result.ergebnisse, bundle.getString("Result"), ((Integer)(po.asInt())).toString(), Ergebnis.FT_IO );
									break;
								case DOUBLE:
									addResultsErgebnis( result.ergebnisse, bundle.getString("Result"), ((Double)(po.asDouble())).toString(), Ergebnis.FT_IO );
									break;
								case STRING:
									addResultsErgebnis( result.ergebnisse, bundle.getString("Result"), ((String)(po.asString())).toString(), Ergebnis.FT_IO );
									break;
								case MAP:
									Map<String, PropertyObject> map = po.asMap();
									Ergebnis ergebnis = new Ergebnis("", Werkzeug, "", "", "", "", "", "", "", "", "", "", "", "", "", Ergebnis.FT_IO ); 
		
									String unit = "";
									if(map.containsKey("Unit")) 			unit			=" ["+map.get("Unit").asString()+"]";
									Integer precision = 3;
									if(map.containsKey("Precision"))		precision		= Math.max(map.get("Precision").asInt(), precision);
									String desiredValue = "";
									if(map.containsKey("DesiredValue"))		desiredValue	= map.get("DesiredValue").asString();
		
									if(map.containsKey("Name"))				ergebnis.setErgebnis(map.get("Name").asString()+unit);
									if(map.containsKey("Repetitions"))		ergebnis.setAnzWiederholungen(((Integer)map.get("Repetitions").asInt()).toString());
									if(map.containsKey("InfoText"))			ergebnis.setHinweisText(map.get("InfoText").asString());
									if(map.containsKey("Value")) {
										PropertyObject value = map.get("Value");
										if(value.isString()) {
											ergebnis.setErgebnisWert(map.get("Value").asString());
										} else
										if(value.getType() == PropertyObject.PropertyDataType.DOUBLE) {
											//https://www.tutorials.de/threads/ersatz-fuer-cultureinfo-invariantculture-aus-net-in-java.345956/
											//(Locale)null ^~= CultureInfo.InvariantCulture
											Double v = value.asDouble(); 
											ergebnis.setErgebnisWert(String.format((Locale)null, "%."+precision+"f", v));
										}
									}
									if(map.containsKey("MinValue"))			ergebnis.setMinWert(((Double)map.get("MinValue").asDouble()).toString());
									if(map.containsKey("MaxValue"))			ergebnis.setMaxWert(((Double)map.get("MaxValue").asDouble()).toString());
									if(map.containsKey("ResultType"))		ergebnis.setFehlerTyp(map.get("ResultType").asString());
									else									ergebnis.setFehlerTyp(jobStatus==JobStatus.Succeeded?Ergebnis.FT_IO:Ergebnis.FT_NIO);
									if(map.containsKey("ErrorMessage"))		ergebnis.setFehlerText(map.get("ErrorMessage").asString()
																			+(((ergebnis.getFehlerTyp()!=Ergebnis.FT_IO) && (component!=""))?(" : "+component):("")));
									result.ergebnisse.add(ergebnis);
//									if(debug) {
//										System.out.println(String.format("%s.%s Return: %s=%s [%s,%s] %s %s -> %s (%s)",
//												this.getPr�fling().getName(), this.getPPName(),
//												ergebnis.getErgebnis(),
//												ergebnis.getErgebnisWert(),
//												ergebnis.getMinWert(), ergebnis.getMaxWert(),
//												ergebnis.getFehlerTyp(),
//												ergebnis.getAnzWiederholungen(),
//												ergebnis.getFehlerText(), ergebnis.getHinweisText()
//										));
//									}
									break;
								case LIST:
									List<PropertyObject> list = po.asList();
									for(PropertyObject lpo : list) {
										addResultsErgebnis(result.ergebnisse, bundle.getString("Result")+"{}", lpo.getValue(), Ergebnis.FT_IO);
									}
									break;
								default:
									throw new Exception("ResultsValue: unknown data type "+po.getType().toString());
								}//switch(Type)
							}//for(jobResults)
						}//if(jobResult)
					
						// GetJobValues
						jobResult = ascotDevice.execute("GetJobValues", new PropertyObject(jobToken));
						if(jobResult != null) {
							for(PropertyObject po : jobResult) {
								Object jobValues = getPropertyObjectAsObject(po);
		
								if (jobValues instanceof Map) {
									Map<String, PropertyObject> map = po.asMap();
									for(String key : map.keySet()) {
										PropertyObject mpo = map.get(key);  
										if(debug) {
											addResultsErgebnis(result.ergebnisse, bundle.getString("Result")+"["+key+"]", mpo.getValue(), Ergebnis.FT_IO);
										}
										if(key=="~") {
											addResultsErgebnis(result.ergebnisse, bundle.getString("Result"), mpo.getValue(), Ergebnis.FT_IO);
										}
										result.jobResults.put(key, getPropertyObjectAsObject(mpo));
									}
								}
								else if (jobValues instanceof List) {
									List<PropertyObject> list = po.asList();
									for(PropertyObject lpo : list) {
										if(debug) {
											addResultsErgebnis(result.ergebnisse, bundle.getString("Result")+"{}", lpo.getValue(), Ergebnis.FT_IO);
										}
									}
								}
								else {
									if(debug) {
										addResultsErgebnis(result.ergebnisse, bundle.getString("Result"), jobValues.toString(), Ergebnis.FT_IO);
									}
								}//if(Type)
							}//for(jobResult)
						}//if(jobResult)
		
						result.status = ((jobStatus==JobStatus.Succeeded)?(STATUS_EXECUTION_OK):(STATUS_EXECUTION_ERROR));
					break;
				case JobStatus.Canceled:
						addResultsErgebnis(result.ergebnisse, bundle.getString("Fail"), bundle.getString("UserDialogCancel"), Ergebnis.FT_NIO);
						result.status = STATUS_EXECUTION_ERROR;
					break;
				case JobStatus.Timeout:
						addResultsError(result.ergebnisse, bundle.getString("Timeout")+" ["+timeout+" s]" );
						result.status = STATUS_EXECUTION_ERROR;
					break;
				default:
						addResultsError(result.ergebnisse, bundle.getString("UnknownStatus")+" ["+jobStatus+"]" );
						result.status = STATUS_EXECUTION_ERROR;
					break;
			} //switch(jobStatus)
		} catch (Exception e) {
			addResultsException(result.ergebnisse, e);
			result.status = STATUS_EXECUTION_ERROR;
			return result;
		}
		return result;
	}

	/**
	 * Executes test procedure
	 * 
	 * @param jobName				job to execute
	 * @param parameter				job parameter
	 * @param ergebnisListe			reference to result set to add results
	 * @param copyResultOnSuccess	should results be added if job succeeds
	 * @param status				reference to status
	 * @return					ASCOTJobResult
	 */
	private JobResult executeJob(String jobName, HashMap<String, Object> parameter, Vector<Ergebnis> ergebnisListe, Boolean copyResultOnSuccess, XREF<Integer> status) {
		JobResult result = executeJob(jobName, parameter);

		if((!result.getExecutionOk()) || copyResultOnSuccess) {
			result.copyJobErgebnisse(ergebnisListe);
		}
		if((!result.getExecutionOk())) {
			status.setValue(result.status);
		}
		return result;
	}

	/** 
     * export parameter
     * 
     * @param cascadeName  Name of parameter in CASCADE context
     * @param ascotName  Name of parameter in ASCOT context (@note this parameter is not use anymore!)
     * @param type  Type of parameter cf. AbstractBMWPruefprozedur$ParameterType
     * @param value Value of parameter if null parameter is required else given value is default value and can be overridden by CASCADE
     */
	private HashMap<String, Parameter> parameters = new HashMap<String, Parameter>();
	private void declareParameter(String cascadeName, String ascotName, byte type, Object value) {
		parameters.put(cascadeName.toUpperCase(), new Parameter(cascadeName, ascotName, new ParameterType(type), value));
	}
	private void exportParameter(String cascadeName, String ascotName, byte type, Object value) {
		parameters.put(cascadeName.toUpperCase(), new Parameter(cascadeName, cascadeName, new ParameterType(type), value));
	}
	private Object getParameter(String name) throws PPExecutionException {
		String arg = name.toUpperCase();
		if(parameters.containsKey(arg)) {
			Parameter par = parameters.get(arg);
			return par.getValue();
		}
		throw new PPExecutionException(PB.getString("parametrierfehler")+": "+String.format(bundle.getString("ParameterUnknown__sName"), name));
	}

	/**
	 * Checks if parameters can be parses as their given type 
	 * @return	true if all parameters can be parsed else false
	 * @throws PPExecutionException
	 */
	private boolean parseArgs() throws PPExecutionException {
		boolean ok=true;

		for(Object o : getArgs().keySet()) { // all given parameters
			if (debug) {
				System.out.println("\t" + o + "=" + getArgs().get(o));
			}
		}

		for (String param : getRequiredArgs()) {
			if(!parameters.containsKey(param.toUpperCase()))
				throw new PPExecutionException(PB.getString("parametrierfehler") + ": "
						+ String.format(bundle.getString("ParameterIsNotDeclared__sName"), param));
		}
		for (String param : getOptionalArgs()) {
			if(!parameters.containsKey(param.toUpperCase()))
				throw new PPExecutionException(PB.getString("parametrierfehler") + ": "
						+ String.format(bundle.getString("ParameterIsNotDeclared__sName"), param));			
		}

		for(String param : parameters.keySet()) {
			boolean found = false;
			for(String s : getRequiredArgs()) {
				if(param.toUpperCase().compareTo(s.toUpperCase())==0) {
					found = true;
					break;
				}
			}
			for(String s : getOptionalArgs()) {
				if(param.toUpperCase().compareTo(s.toUpperCase())==0) {
					found = true;
					break;
				}
			}
			if(found==false) {
				throw new PPExecutionException(PB.getString("parametrierfehler") + ": "
						+ String.format(bundle.getString("ParameterIsNotListedAsCascadeArg__sName"), param));				
			}
		}
		
		String dump = String.format("%s.%s Parameter = {\n", this.getPr�fling().getName(), this.getName());

		int req = 0;
		int opt = 0;
		int def = 0;

		for(String param : parameters.keySet()) {
			Parameter par = parameters.get(param.toUpperCase());
			par.parseArgument();
			boolean io=par.checkValue();
			if (io == false) {
				throw new PPExecutionException(PB.getString("parametrierfehler") + ": " 
						+ String.format(bundle.getString("ParameterCannotBeParsed__sName_sType_sValue"), param, par.getType(), par.getArgument()));
			}
			dump += String.format("\t%s\n", par);
			ok &= io;
			switch(par.getFlag()) {
			case 'r':req++; break;
			case 'o':opt++; break;
			case 'd':def++; break;
			}
		}
		dump += "}";
		if (debug) {
			System.out.println(dump + "\trequired[" + req + "], optional[" + opt + "/" + (opt + def) + "]");
		}
		return ok;
	}

	/**
	 * converts PropertyObject to String
	 * 
	 * @param po				PropertyObject
	 * @return					String representation of PropertyObject
	 */
	private String getPropertyObjectAsString(PropertyObject po) {
		try {
			return getPropertyObjectAsObject(po).toString();
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	/**
	 * converts PropertyObject to Object of its type
	 * 
	 * @param po				PropertyObject
	 * @return					Object of PropertyObject
	 * @throws Exception
	 */
	private Object getPropertyObjectAsObject(PropertyObject po) throws Exception {
		switch(po.getType()) {
		case BOOLEAN:
			return po.asBoolean();
		case INTEGER:
			return po.asInt();
		case DOUBLE:
			return po.asDouble();
		case STRING:
			return po.asString();
		case MAP:
			return po.asMap();
		case LIST:
			return po.asList();
		default:
			throw new Exception("getPropertyObjectAsObject: unknown data type "+po.getType().toString());
		}
	}
	/**
	 * converts PropertyObject to Object of its type
	 * 
	 * @param 	o				Object
	 * @return					PropertyObject
	 * @throws Exception
	 */
	private PropertyObject getObjectAsPropertyObject(Object o) throws Exception {
		if(o==null) throw new Exception("getObjectAsPropertyObject: Object is null");
		if(o instanceof Boolean)
			return new PropertyObject((Boolean)o);
		if(o instanceof Long)
			return new PropertyObject(((Long)o).intValue());
		if(o instanceof Integer)
			return new PropertyObject((Integer)o);
		if(o instanceof Double)
			return new PropertyObject((Double)o);
		if(o instanceof Float)
			return new PropertyObject((Double)o);
		if(o instanceof String)
			return new PropertyObject((String)o);
		throw new Exception("getObjectAsPropertyObject: unknown data type "+o.getClass().getName()+"="+o.toString());
	}
	
	/**
	 * add result to result set
	 * 
	 * @param	ergebnisListe		reference to result set
	 * @param	name				result name
	 * @param	value				result value
	 * @param	typ					result type
	 */
	private void addResultsErgebnis(Vector<Ergebnis> ergebnisListe, String name, String value, String typ) {
		addResultsErgebnis(ergebnisListe, name, value, "", typ);
	}
	/**
	 * add result to result set
	 * 
	 * @param	ergebnisListe		reference to result set
	 * @param	name				result name
	 * @param	value				result value
	 * @param	typ					result type
	 */
	private void addResultsErgebnis(Vector<Ergebnis> ergebnisListe, String name, String value, String hinweisText, String typ) {
		ergebnisListe.add(new Ergebnis(
				"",									// ID
				Werkzeug,							// werkzeug
				"",									// parameter_1 
				"",									// parameter_2
				"",									// parameter_3
				name,								// ergebnis
				value,								// ergebnisWert
				"",									// minWert
				"",									// maxWert
				"",									// wiederholungen
				"",									// frageText
				"",									// antwortText
				"",									// anweisText
				"",									// fehlerText
				hinweisText,						// hinweisText
				typ									// fehlerTyp
			)
		);
	}
	/**
	 * add debug info of an object to result set
	 * 
	 * @param	ergebnisListe		reference to result set
	 * @param	name				local name of object
	 * @param	object				reference to object
	 */
	private void addResultsDebugInfo(Vector<Ergebnis> ergebnisListe, String name, Object object) {
		addResultsDebugInfo(ergebnisListe, name+"["+object.getClass().getName()+"]="+object.toString());
	}
	/**
	 * add debug info string to result set
	 * 
	 * @param	ergebnisListe		reference to result set
	 * @param	message				text message
	 */
	private void addResultsDebugInfo(Vector<Ergebnis> ergebnisListe, String message) {
		Exception e = new Exception();
		String pos = "";
		StackTraceElement[] stack = e.getStackTrace();
		for(int i=0; i<stack.length; i++) {
			String entry =stack[i].toString();
			if(entry.contains("addResultsDebugInfo(")) continue;
			
			pos = entry;
			break;
		}
		
		ergebnisListe.add(new Ergebnis(
				"",						// ID
				Werkzeug,				// werkzeug
				"",						// parameter_1 
				"",						// parameter_2
				"",						// parameter_3
				"DEBUG INFO",			// ergebnis
				message,				// ergebnisWert
				"",						// minWert
				"",						// maxWert
				"",						// wiederholungen
				"",						// frageText
				"",						// antwortText
				"",						// anweisText
				"",						// fehlerText
				pos,					// hinweisText
				Ergebnis.FT_IO			// fehlerTyp
			)
		);
	}
	/**
	 * add error message to result set
	 * 
	 * @param	ergebnisListe		reference to result set
	 * @param	message				text message
	 */
	private void addResultsError(Vector<Ergebnis> ergebnisListe, String message) {
		Exception e = new Exception();
		String pos = "";
		StackTraceElement[] stack = e.getStackTrace();
		for(int i=0; i<stack.length; i++) {
			String entry =stack[i].toString();
			if(entry.contains("addResultsError(")) continue;
			
			pos = entry;
			break;
		}

		ergebnisListe.add(new Ergebnis(
				"",									// ID
				Werkzeug,							// werkzeug
				"",									// parameter_1 
				"",									// parameter_2
				"",									// parameter_3
				"ERROR",							// ergebnis
				"",									// ergebnisWert
				"",									// minWert
				"",									// maxWert
				"",									// wiederholungen
				"",									// frageText
				"",									// antwortText
				"",									// anweisText
				message,							// fehlerText
				pos,								// hinweisText
				Ergebnis.FT_NIO						// fehlerTyp
			)
		);
	}
	private void addResultsError(Vector<Ergebnis> ergebnisListe, String ergebnis, String message) {
		Exception e = new Exception();
		String pos = "";
		StackTraceElement[] stack = e.getStackTrace();
		for(int i=0; i<stack.length; i++) {
			String entry =stack[i].toString();
			if(entry.contains("addResultsError(")) continue;
			
			pos = entry;
			break;
		}

		ergebnisListe.add(new Ergebnis(
				"",									// ID
				Werkzeug,							// werkzeug
				"",									// parameter_1 
				"",									// parameter_2
				"",									// parameter_3
				ergebnis,							// ergebnis
				"",									// ergebnisWert
				"",									// minWert
				"",									// maxWert
				"",									// wiederholungen
				"",									// frageText
				"",									// antwortText
				"",									// anweisText
				message,							// fehlerText
				pos,								// hinweisText
				Ergebnis.FT_NIO						// fehlerTyp
			)
		);
	}
	private void addResultsSystemError(Vector<Ergebnis> ergebnisListe, String message) {
		Exception e = new Exception();
		String pos = "";
		StackTraceElement[] stack = e.getStackTrace();
		for(int i=0; i<stack.length; i++) {
			String entry =stack[i].toString();
			if(entry.contains("addResultsError(")) continue;
			
			pos = entry;
			break;
		}

		ergebnisListe.add(new Ergebnis(
				"",									// ID
				Werkzeug,							// werkzeug
				"",									// parameter_1 
				"",									// parameter_2
				"",									// parameter_3
				"ERROR",							// ergebnis
				"",									// ergebnisWert
				"",									// minWert
				"",									// maxWert
				"",									// wiederholungen
				"",									// frageText
				"",									// antwortText
				"",									// anweisText
				message,							// fehlerText
				pos,								// hinweisText
				Ergebnis.FT_NIO_SYS					// fehlerTyp
			)
		);
	}
	/**
	 * add debug info of exception to result set
	 * 
	 * @param	ergebnisListe		reference to result set
	 * @param	exception			reference to exception
	 */
	private void addResultsException(Vector<Ergebnis> ergebnisListe, Throwable exception) {
		ergebnisListe.add(new Ergebnis(
				"",									// ID
				Werkzeug,							// werkzeug
				"",									// parameter_1 
				"",									// parameter_2
				"",									// parameter_3
				"Exception",						// ergebnis
				exception.getClass().getName(),		// ergebnisWert
				"",									// minWert
				"",									// maxWert
				"",									// wiederholungen
				"",									// frageText
				"",									// antwortText
				"",									// anweisText
				exception.getMessage(),				// fehlerText
				getStackTrace(exception),			// hinweisText
				Ergebnis.FT_NIO_SYS					// fehlerTyp
			)
		);
	}

	/**
	 * get stack trace as string
	 * 
	 * @param	exception
	 * @return	string representation of stack trace
	 */
	private static String getStackTrace(Throwable exception) {
		StringBuilder sb = new StringBuilder();
		for (StackTraceElement element : exception.getStackTrace()) {
			sb.append("\n");
			sb.append(element.toString());
		}
		sb.append("\n");
		return sb.toString();
	}
	/**
	 * get line number of calling line
	 *
	 * @return string with class name calling function name and line number
	 */
	private static String getCurrentLine () {
        StackTraceElement ste = Thread.currentThread().getStackTrace()[2];
        String where = ste.getClassName() + "::" + ste.getMethodName() + "(" + ste.getLineNumber() + ")";
        return where;
    }

	private String statusAsString(int status) {
		switch(status) {
		case -1:
			/** The element is undefined */
			return "STATUS_UNDEFINED";// = -1;
		case 0:
			/** The element has not been executed */
			return "STATUS_NOT_EXECUTED";// = 0;
		case 1:
			/** The element is being executed */
			return "STATUS_RUNNING";// = 1;
		case 2:
			/** The element has been executed and was OK */
			return "STATUS_EXECUTION_OK";// = 2;
		case 3:
			/** The element has been executed and was not OK */
			return "STATUS_EXECUTION_ERROR";// = 3;
		case 4:
			/** The element execution was cancelled by a user */
			return "STATUS_EXECUTION_ABORTED_BY_USER";// = 4;
		case 5:
			/** The element has been executed more than once (retried) */
			return "STATUS_EXECUTION_RETRY";// = 5;
		case 6:
			/** The element has been executed and was not OK but the error will be ignored (eg. OK_ON_ERROR) */
			return "STATUS_EXECUTION_ERROR_IGNORE";// = 6;
		case 100:
			/** The element is disabled */
			return "STATUS_DISABLED";// = 100;
		default:
			return "unkown";
		}
	}

	private Boolean False = false;
	private Boolean True  = true;

	protected class ParameterType {
		static final long serialVersionUID = 1L;

		public static final byte String = 0;
		public static final byte Boolean = 1;
		public static final byte Long = 2;
		public static final byte Double = 3;
		public static final byte None = (byte)0xff;
		

		public byte value;
		public ParameterType() {
			value = None;
		}
		public ParameterType(byte b) {
			this();
			value = b;
		}
		
		public String toString() {
			switch(value) {
			case ParameterType.Boolean:
				return "Boolean";
			case ParameterType.Long:
				return "Integer";
			case ParameterType.Double:
				return "Double";
			case ParameterType.String:
				return "String";
			case ParameterType.None:
				return "None";
			default:
				return "unknown";
			}
		}
	}
	protected class Parameter {
		static final long serialVersionUID = 1L;

		private String _cascadeName;
		private String _ascotName;
		private ParameterType _type;
		private Object _default;
		private Object _value;
		private String _argument;
		private Boolean _parsed;
		
		public Parameter(String cascadeName, String ascotName, ParameterType type, Object defaultValue) {
			_cascadeName	= cascadeName;
			_ascotName 		= ascotName;
			_type 			= type;
			_default		= defaultValue;
			_value			= _default;
			_argument		= "";
			_parsed			= false;
		}
		
		public String			getName() 		{ return _cascadeName; }
		public String			getArgument() 	{ return _argument; }
		public String 			getAscotName() 	{ return _ascotName; }
		public ParameterType	getType() 		{ return _type; }
		public Object 			getDefault() 	{ return _default; }
		public Object getValue() throws PPExecutionException {
			if(_parsed) return _value;
			parseArgument();
			return _value;
		}
		
		private Object getStringAsObject(String s) throws PPExecutionException {
			try {
				switch(_type.value) {
				case ParameterType.Boolean:
					try {
						if(s.startsWith("T") || s.startsWith("Y") || s.startsWith("J")) {
							_value=true;
						} else
						if(Boolean.parseBoolean(s)==true) {
							_value=true;
						} else
						if(Double.parseDouble(s)>0)	{
							_value=true;
						}
					} catch (Exception e) {
						_value=false;
					}
					return _value;
				case ParameterType.Long:
					return Integer.parseInt(s);
				case ParameterType.Double:
					return Double.parseDouble(s);
				case ParameterType.String:
					return s.trim();
				default:
					throw new PPExecutionException(PB.getString("parametrierfehler")+": "+String.format(bundle.getString("ParameterTypeUnknown__sName_sType"), _cascadeName, _type));
				}
			} catch(Exception e) {
				throw new PPExecutionException(PB.getString("parametrierfehler")+": "+String.format(bundle.getString("ParameterCannotBeParsed__sName_sType_sValue"), _cascadeName, _type, s));
			}
		}
		
		private String readArgument() {
			_argument = getArg(_cascadeName.toUpperCase());
			if(_argument!=null) { 
				_argument = _argument.trim();
			}
			return _argument;
		}
		public boolean parseArgument() throws PPExecutionException {
			if(_parsed) return _parsed; // already done;
			
			if(readArgument()!=null) {
				_value = getStringAsObject(_argument);
			} else {
				_value = _default;
			}
			_parsed=true;
			return _parsed;
		}

		public boolean checkValue() throws PPExecutionException {
			return parseArgument();
		}
		
		public boolean hasDefault() {
			return _default!=null;
		}
		
		public boolean isRequired() {
			return !isOptional();
		}
		public boolean isOptional() {
			return hasDefault();
		}
		public boolean isGiven() {
			Object arg = getArg(_cascadeName.toUpperCase());
			return arg!=null;
		}
		
		private char getFlag() {
			if(isRequired())	return 'r';
			if(isGiven())		return 'o';
			return 'd';
		}
		
		public String toString() {
			try {
//				return String.format("[%c]%-40.40s = %-40.40s [%-7.7s](%-40.40s)-> %-40.40s  [%2.2s]", getFlag(), _cascadeName, _argument, _type, _default, _value, (checkValue() ? "OK" : "!!"));
				return String.format("[%c]%-40.40s = %-40.40s [%-7.7s]-> %-40.40s  [%2.2s]", getFlag(), _cascadeName, _argument, _type, _value, (checkValue() ? "OK" : "!!"));
			} catch (Exception e) {
				return String.format("%s[%s]=%s", _cascadeName, _type, _default);
			}
		}
	}
	
	protected class JobStatus {
		public static final byte Running = 0;
		public static final byte Succeeded = 1;
		public static final byte Failed = 2;
		public static final byte Unknown = 3;
		public static final byte Canceled = (byte)0xfe;
		public static final byte Timeout = (byte)0xff;
		
		public byte status;
		public JobStatus() {
			status = 3;
		}
		public JobStatus(byte s) {
			status = s;
		}
		
		public String toString() {
			switch(status) {
			case Running:
				return "Running";
			case Succeeded:
				return "Succeeded";
			case Failed:
				return "Failed";
			default:
			case Unknown:
				return "Unknown";
			case Canceled:
				return "Canceled";
			case Timeout:
				return "Timeout";
			}
		}
	}
	protected class JobResult {
		static final long serialVersionUID = 1L;
		public JobResult(){
			ergebnisse = new Vector<Ergebnis>();
			jobResults = new HashMap<String, Object>();
		}
		
		public Vector<Ergebnis> ergebnisse;
		public JobStatus jobStatus;
		public HashMap<String, Object> jobResults;
		public int status;

		public String toString() {
			String sErgebnisse = "{\n";
			for(Ergebnis erg : ergebnisse) {
				sErgebnisse += erg+"\n\n";
			}
			sErgebnisse += "}";
			String sJobResults = "{\n";
			for(String key : jobResults.keySet()) {
				sJobResults += "\t"+key+"="+jobResults.get(key)+"\n";
			}
			sJobResults += "}";
			return String.format("{ jobStatus=%s, jobResults=%s,\nergebnisse=%s,\n status=%d }",
					jobStatus, sJobResults, sErgebnisse, status);
		}
		
		public Boolean getExecutionOk() {
			return status == AbstractBMWPruefprozedur.STATUS_EXECUTION_OK;
		}
		
		public void copyJobErgebnisse(Vector<Ergebnis> target)
		{
			for(int e = 0; e < ergebnisse.size(); ++e)
				target.add(ergebnisse.get(e));
		}
		
		public Boolean getReturnParameterAsBoolean(String name) {
			return (Boolean) this.jobResults.get(name);
		}
		public Long getReturnParameterAsLong(String name) {
			return (Long) this.jobResults.get(name);
		}
		public Integer getReturnParameterAsInteger(String name) {
			//return getReturnParameterAsLong(name).intValue();
			return (Integer) this.jobResults.get(name);
		}
		public Double getReturnParameterAsDouble(String name) {
			return (Double) this.jobResults.get(name);
		}
		public String getReturnParameterAsString(String name) {
			return (String) this.jobResults.get(name);
		}

		public Boolean getReturnValueAsBoolean() {
			return getReturnParameterAsBoolean("~");
		}
		public Long getReturnValueAsLong() {
			return getReturnParameterAsLong("~");
		}
		public Integer getReturnValueAsInteger() {
			return getReturnParameterAsInteger("~");
		}
		public Double getReturnValueAsDouble() {
			return getReturnParameterAsDouble("~");
		}
		public String getReturnValueAsString() {
			return getReturnParameterAsString("~");
		}
	}

	protected class XREF<T> {
	       public XOBJ<T> Obj = null;
	       public XREF(T value) 
	       {
	           Obj = new XOBJ<T>(value);
	       }
	       public XREF() 
	       {
	         Obj = new XOBJ<T>();
	       }  
	       public XOUT<T> Out()
	       {
	           return(Obj.Out());       
	       }
	       public XREF<T> Ref()
	       {
	           return(this);
	       }
	       
	       public T getValue() {
	    	   return Obj.Value;
	       }
	       public void setValue(T value) {
	    	   Obj.Value = value;
	       }
	};
	protected class XOUT<T> {
	     public XOBJ<T> Obj = null;
	     public XOUT(T value) 
	     {
	         Obj = new XOBJ<T>(value);
	     }
	     public XOUT() 
	     {
	       Obj = new XOBJ<T>();
	     }  
	     public XOUT<T> Out()
	     {
	         return(this);           
	     }
	     public XREF<T> Ref()
	     {
	         return(Obj.Ref());       
	     }

	     public T getValue() {
	  	   return Obj.Value;
	     }
	     public void setValue(T value) {
	  	   Obj.Value = value;
	     }
	};
	protected class XOBJ<T> {

		  public T Value;

		  public  XOBJ() {

		  }    
		  public XOBJ(T value) {
		      this.Value = value;
		  }
		  //
		  // Method: Ref()
		  // Purpose: returns a Reference Parameter object using the XOBJ value
		  //    
		  public XREF<T> Ref()
		  {
		      XREF<T> ref = new XREF<T>();
		      ref.Obj = this;
		      return(ref);
		  }
		  //
		  // Method: Out()
		  // Purpose: returns an Out Parameter Object using the XOBJ value
		  //
		  public XOUT<T> Out()
		  {
		      XOUT<T> out = new XOUT<T>();
		      out.Obj = this;
		      return(out);
		  }    
		  //
		  // Method get()
		  // Purpose: returns the value 
		  // Note: Because this is combersome to edit in the code,
		  // the Value object has been made public
		  //    
		  public T get() {
		      return Value;
		  }
		  //
		  // Method get()
		  // Purpose: sets the value
		  // Note: Because this is combersome to edit in the code,
		  // the Value object has been made public
		  //
		  public void set(T anotherValue) {
		      Value = anotherValue;
		  }


		  @Override
		  public String toString() {
		      return Value.toString();
		  }

		  @Override
		  public boolean equals(Object obj) {
		      return Value.equals(obj);
		  }

		  @Override
		  public int hashCode() {
		      return Value.hashCode();
		  }
		}	

	// f�r die �bersetzung
	private ResourceBundle bundle = ResourceBundle.getBundle( Resources.class.getName() );

	/**
	 * RessourceBundle default en / English
	 * @author Michael Scholler, remes GmbH
	 */
	public static class Resources extends ListResourceBundle {
		@Override
		protected Object[][] getContents() {
			return new Object[][] {
				// LOCALIZE THIS
				{ "Timeout", "Action takes to long"},
				{ "Exception", "Exception: "},
				{ "RequestDeviceFail", "Cannot get Device"},
				{ "RequestDeviceException", "Exception while getting ASCOT DynamicDevice: "},
				{ "GetMethodNamesFail", "Device doesn't contain all required methods"},
				{ "Result", "Result"},
				{ "Success", "Success"},
				{ "Fail", "Fail"},
				{ "UnknownStatus", "Unknown status"},
				{ "NoJobName", "You must privide a job name!" },
				{ "UserDialogOpenFail","Cannot show userdialog"},
				{ "UserDialogCloseFail","Cannot close userdialog"},
				{ "UserDialogCancel","test step canceled by user"},
				{ "WaitingForUserAction", "ASCOT: Waiting for action..."},
				{ "ParameterCannotBeParsed__sName_sType_sValue", "%s[%s] = \"%s\" cannot be parsed!"},
				{ "ParameterIsNotDeclared__sName", "%s is not declared as parameter!"},
				{ "ParameterIsNotListedAsCascadeArg__sName", "%s is not listed as required or optional argument!"},
				{ "ParameterUnknown__sName", "unknown parameter: %s"},
				{ "ParameterTypeUnknown__sName_sType", "unknown type for parameter: %s[%s]"},
				{ "UnableToPowerOn", "unable to power on"},
				{ "UnableToConnectLIN", "unable to connect to lin"},
				{ "Stroke", "Window is moving..."},
				{ "WindowFullUp", "Window is closing..."},
				{ "WindowFullDown", "Window is opening..."},
				{ "WindowShortUp", "Window is closing (short stroke)..."},
				{ "WindowShortDown", "Window is opening (short stroke)..."},
				{ "MaxCountReached", "maximum number of trails reached"},
				{ "GetErrormemory", "read error memory data"},
				{ "GetMotorStopLogger", "read motor stop logger data"},
				{ "PositionNotReached", "Target position not reached"},
				{ "WindowReversed", "Window reversed"},
				{ "MoveInterrupted", "Move interrupted"},				
				{ "IFE_ButtonError__0x00", 					"IFE error memory active, no error - switch not connected or defect."},
				{ "IFE_ButtonError__0x01", 					"IFE error memory active, error button - switch not connected or defect."},
				{ "IFE_ButtonError__0x02", 					"IFE error memory active, error illumination - switch not connected or defect."},
				{ "IFE_ButtonError__0x03", 					"IFE error memory active, error button and illumination - switch not connected or defect."},
				{ "IFE_ButtonError__0xff", 					"IFE error memory active, error invalid - switch not connected or defect."},
				{ "LIN_UDS_MoveRelative__0",				"LIN UDS answer: service still not required"},
				{ "LIN_UDS_MoveRelative__1",				"LIN UDS answer: operation pending"},
				{ "LIN_UDS_MoveRelative__2",				"LIN UDS answer: routine cannot be executed"},
				{ "LIN_UDS_MoveRelative__3",				"LIN UDS answer: routine is executing"},
				{ "LIN_UDS_MoveRelative__4",				"LIN UDS answer: routine stopped succesfully"},
				{ "LIN_UDS_MoveRelative__5",				"LIN UDS answer: routine stopped with errrors"},
				{ "LIN_UDS_MoveRelative__6",				"LIN UDS answer: routine interrupted"},
				{ "LIN_UDS_MoveRelative__7",				"LIN UDS answer: invalid"},
				{ "LIN_UDS_MoveRelative__N", 				"LIN UDS answer: unknown LIN UDS answer"},
				{ "LIN_UDS_ErrorMemory__0xNN",				"LIN UDS ErrorMemory: 0x?? unknown" },
				{ "LIN_UDS_ErrorMemory__unexpected",		"LIN UDS ErrorMemory: unexpected answer" },
				{ "LIN_UDS_ErrorMemory__invalid",			"LIN UDS ErrorMemory: invalid" },
				{ "LIN_UDS_ErrorMemory__Result1",			"LIN UDS ErrorMemory: result of execution" },
				{ "LIN_UDS_ErrorMemory__Result2",			"LIN UDS ErrorMemory: Order" },
				{ "LIN_UDS_ErrorMemory__Result3",			"LIN UDS ErrorMemory: answer" },
				{ "LIN_UDS_ErrorMemory__Result4",			"LIN UDS ErrorMemory: INIT_ERROR" },
				{ "LIN_UDS_ErrorMemory__Result4__0x00",		"DTC_INIT_INACTIVE" },
				{ "LIN_UDS_ErrorMemory__Result4__0x01",		"DTC_INIT_ACTIVE" },
				{ "LIN_UDS_ErrorMemory__Result5",			"LIN UDS ErrorMemory: INIT_ERROR_CODE" },
				{ "LIN_UDS_ErrorMemory__Result5__0x00",		"DTC_INIT_OK" },
				{ "LIN_UDS_ErrorMemory__Result5__0x01",		"DTC_INIT_CODING_INVALID" },
				{ "LIN_UDS_ErrorMemory__Result5__0x02",		"DTC_INIT_CODE_VERS_INVALID" },
				{ "LIN_UDS_ErrorMemory__Result5__0x04",		"DTC_INIT_HW:VERS_INVALID" },
				{ "LIN_UDS_ErrorMemory__Result5__0x08",		"DTC_INIT_NOT_POSSIBLE" },
				{ "LIN_UDS_ErrorMemory__Result5__0x10",		"DTC_INIT_ADAPT_INVALID" },
				{ "LIN_UDS_ErrorMemory__Result5__0x20",		"DTC_INIT_NORM_INVALID" },
				{ "LIN_UDS_ErrorMemory__Result5__0xff",		"DTC_INIT_INVALID" },
				{ "LIN_UDS_ErrorMemory__Result6",			"LIN UDS ErrorMemory: OPR_ERROR" },
				{ "LIN_UDS_ErrorMemory__Result6__0x00",		"DTC_OPR_INACTIVE" },
				{ "LIN_UDS_ErrorMemory__Result6__0x01",		"DTC_OPR_ACTIVE" },
				{ "LIN_UDS_ErrorMemory__Result7",			"LIN UDS ErrorMemory: OPR_ERROR_CODE" },
				{ "LIN_UDS_ErrorMemory__Result7__0x00",		"DTC_RUN_OK" },
				{ "LIN_UDS_ErrorMemory__Result7__0x01",		"DTC_RUN_THERMO_100" },
				{ "LIN_UDS_ErrorMemory__Result7__0x02",		"DTC_RUN_THERMO_90" },
				{ "LIN_UDS_ErrorMemory__Result7__0x04",		"DTC_RUN_CONTROL" },
				{ "LIN_UDS_ErrorMemory__Result7__0xff",		"DTC_RUN_INVALID" },
				{ "LIN_UDS_ErrorMemory__Result8",			"LIN UDS ErrorMemory: HW_ERROR" },
				{ "LIN_UDS_ErrorMemory__Result8__0x00",		"DTC_HW_INACTIVE" },
				{ "LIN_UDS_ErrorMemory__Result8__0x01",		"DTC_HW_ACTIVE" },
				{ "LIN_UDS_ErrorMemory__Result9",			"LIN UDS ErrorMemory: HW_ERROR_CODE" },
				{ "LIN_UDS_ErrorMemory__Result9__0x00",		"DTC_HW_OK" },
				{ "LIN_UDS_ErrorMemory__Result9__0x01",		"DTC_HW_REL_LOW_STUCK" },
				{ "LIN_UDS_ErrorMemory__Result9__0x02",		"DTC_HW_REL_HIGH_STUCK" },
				{ "LIN_UDS_ErrorMemory__Result9__0x04",		"DTC_HW_COMMON" },
				{ "LIN_UDS_ErrorMemory__Result9__0x08",		"DTC_HW_HAL" },
				{ "LIN_UDS_ErrorMemory__Result9__0xff",		"DTC_HW_INVALID" },
				{ "LIN_UDS_ErrorMemory__Result10",			"LIN UDS ErrorMemory: BUTTON_ERROR" },
				{ "LIN_UDS_ErrorMemory__Result10__0x00",	"DTC_BUTTON_INACTIVE" },
				{ "LIN_UDS_ErrorMemory__Result10__0x01",	"DTC_BUTTON_ACTIVE" },
				{ "LIN_UDS_ErrorMemory__Result11",			"LIN UDS ErrorMemory: BUTTON_ERROR_CODE" },
				{ "LIN_UDS_ErrorMemory__Result11__0x00",	"DTC_BUTTON_OK" },
				{ "LIN_UDS_ErrorMemory__Result11__0x01",	"DTC_BUTTON_ERR" },
				{ "LIN_UDS_ErrorMemory__Result11__0x02",	"DTC_BUTTON_ILUM_ERR" },
				{ "LIN_UDS_ErrorMemory__Result11__0xff",	"DTC_BUTTON_INVALID" },
				{ "LIN_UDS_MotorStopLogger__0x00",		"LIN UDS MotorStopLogger: 0x00 NOT_STOPPED" },			// Motor f�hrt																		//ID: EK_K_IFE_738	
				{ "LIN_UDS_MotorStopLogger__0x01",		"LIN UDS MotorStopLogger: 0x01 POSITION_REACHED" },		// Angeforderte Endposition oder Endstellung erreicht								//ID: EK_K_IFE_739	
				{ "LIN_UDS_MotorStopLogger__0x02",		"LIN UDS MotorStopLogger: 0x02 STOP_MOVE" },			// Bewegung durch Benutzer/Bedienkonzept abgebrochen								//ID: EK_K_IFE_740	
				{ "LIN_UDS_MotorStopLogger__0x03",		"LIN UDS MotorStopLogger: 0x03 NORM" },					// Normierungsposition gefunden														//ID: EK_K_IFE_741	
				{ "LIN_UDS_MotorStopLogger__0x04",		"LIN UDS MotorStopLogger: 0x04 RENORM" },				// Nachnormierung durchgef�hrt														//ID: EK_K_IFE_742	
				{ "LIN_UDS_MotorStopLogger__0x05",		"LIN UDS MotorStopLogger: 0x05 PINCHING" },				// Einklemmfall erkannt																//ID: EK_K_IFE_743	
				{ "LIN_UDS_MotorStopLogger__0x06",		"LIN UDS MotorStopLogger: 0x06 REV_POS_REACHED" },		// Reversiervorgang beendet															//ID: EK_K_IFE_744	
				{ "LIN_UDS_MotorStopLogger__0x07",		"LIN UDS MotorStopLogger: 0x07 BLOCKED" },				// Blockierung erkannt																//ID: EK_K_IFE_745	
				{ "LIN_UDS_MotorStopLogger__0x08",		"LIN UDS MotorStopLogger: 0x08 NOT_MOVED" },			// Motor steht = Initialwert des Ringspeichers										//ID: EK_K_IFE_577	
				{ "LIN_UDS_MotorStopLogger__0x09",		"LIN UDS MotorStopLogger: 0x09 SAFETY_TIMER" }, 		// Max. Ansteuerzeit �berschritten													//ID: EK_K_IFE_746	
				{ "LIN_UDS_MotorStopLogger__0x0a",		"LIN UDS MotorStopLogger: 0x0a OPPOSITE_DIRECTION" }, 	// Ansteuerrichtung ungleich durch Hall-Sensoren ermittelte Fahrrichtung			//ID: EK_K_IFE_747	
				{ "LIN_UDS_MotorStopLogger__0x0b",		"LIN UDS MotorStopLogger: 0x0b TARGET_POS_LOW" }, 		// Zielposition zu niedrig															//ID: EK_K_IFE_905	
				{ "LIN_UDS_MotorStopLogger__0x0c",		"LIN UDS MotorStopLogger: 0x0c TARGET_POS_HIGH" }, 		// Zielposition zu hoch																//ID: EK_K_IFE_906	
				{ "LIN_UDS_MotorStopLogger__0x0d",		"LIN UDS MotorStopLogger: 0x0d TERM100" }, 				// Bewegung wurde aufgrund �berhitzung (Thermo 100) unterbrochen					//ID: EK_K_IFE_750	
				{ "LIN_UDS_MotorStopLogger__0x0e",		"LIN UDS MotorStopLogger: 0x0e HARDWARE_ERROR" }, 		// Genereller Fehler in der Ansteuerhardware/IFE									//ID: EK_K_IFE_907	
				{ "LIN_UDS_MotorStopLogger__0x0f",		"LIN UDS MotorStopLogger: 0x0f MOTOR_KURZSCHLUSS" }, 	// Kurzschlu� zwischen den Motorzuleitungen											//ID: EK_K_IFE_908	
				{ "LIN_UDS_MotorStopLogger__0x10",		"LIN UDS MotorStopLogger: 0x10 RESET" }, 				// Bewegung wurde durch Reset/Spannungsverlust unterbrochen							//ID: EK_K_IFE_748	
				{ "LIN_UDS_MotorStopLogger__0x11",		"LIN UDS MotorStopLogger: 0x11 HALL_PULSE_LOST" }, 		// Hall-Puls(e) verloren															//ID: EK_K_IFE_909	
				{ "LIN_UDS_MotorStopLogger__0x12",		"LIN UDS MotorStopLogger: 0x12 VOLTAGE_RANGE" }, 		// Bewegung wurde aufgrund Spannungsgrenzen unterbrochen							//ID: EK_K_IFE_749	
				{ "LIN_UDS_MotorStopLogger__0x13",		"LIN UDS MotorStopLogger: 0x13 HALL_HW_ERROR" }, 		// Fehler in Hallsensoren-HW														//ID: EK_K_IFE_910	
				{ "LIN_UDS_MotorStopLogger__0x14",		"LIN UDS MotorStopLogger: 0x14 OSEK" }, 				// keine Rechenzeit f�r EKS-Algorithmus												//ID: EK_K_IFE_911	
				{ "LIN_UDS_MotorStopLogger__0x15",		"LIN UDS MotorStopLogger: 0x15 AUTOMOVE_CMD_FAIL" },	
				{ "LIN_UDS_MotorStopLogger__0x16",		"LIN UDS MotorStopLogger: 0x16 reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x17",		"LIN UDS MotorStopLogger: 0x17 reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x18",		"LIN UDS MotorStopLogger: 0x18 reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x19",		"LIN UDS MotorStopLogger: 0x19 reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x1a",		"LIN UDS MotorStopLogger: 0x1a reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x1b",		"LIN UDS MotorStopLogger: 0x1b reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x1c",		"LIN UDS MotorStopLogger: 0x1c reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x1d",		"LIN UDS MotorStopLogger: 0x1d reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x1e",		"LIN UDS MotorStopLogger: 0x1e reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x1f",		"LIN UDS MotorStopLogger: 0x1f reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x60",		"LIN UDS MotorStopLogger: 0x60 (BROSE) AUTO_COND_LOST" },				//Wegfall Automatikfreigabe	
				{ "LIN_UDS_MotorStopLogger__0x61",		"LIN UDS MotorStopLogger: 0x61 (BROSE) CODING_FAIL" },					//Fehler Codierdaten Checksumme	
				{ "LIN_UDS_MotorStopLogger__0x62",		"LIN UDS MotorStopLogger: 0x62 (BROSE) WAITING_FOR_EE" },				//Warte auf EEPROM	
				{ "LIN_UDS_MotorStopLogger__0x63",		"LIN UDS MotorStopLogger: 0x63 (BROSE) EE_TIMEOUT" },					//Keine Antwort vom NVRAM Manager	
				{ "LIN_UDS_MotorStopLogger__0x64",		"LIN UDS MotorStopLogger: 0x64 (BROSE) NOT_POSSIBLE_TO_WRITE_EE" },		//Position kann nicht in EEPROM geschrieben werden	
				{ "LIN_UDS_MotorStopLogger__0x65",		"LIN UDS MotorStopLogger: 0x65 (BROSE) HALL_DISABLED" },				//Hall ausgeschaltet	
				{ "LIN_UDS_MotorStopLogger__0x66",		"LIN UDS MotorStopLogger: 0x66 (BROSE) CODING_SESSION" },				//	
				{ "LIN_UDS_MotorStopLogger__0x67",		"LIN UDS MotorStopLogger: 0x67 (BROSE) PANIC_NOT_VALID" },				//Panic Close nicht erlaubt	
				{ "LIN_UDS_MotorStopLogger__0x68",		"LIN UDS MotorStopLogger: 0x68 (BROSE) DIAGNOSTIC_SESSION_ACTIVE" },	//F�r BROSE und CONTI gleich??	
				{ "LIN_UDS_MotorStopLogger__0x69",		"LIN UDS MotorStopLogger: 0x69 (BROSE) STALL_PLAY_PROTECTION" },		//Keine Ansteuerung aufgrund von Spielschutz	
				{ "LIN_UDS_MotorStopLogger__0x6a",		"LIN UDS MotorStopLogger: 0x6a (BROSE) CRC_ALIVE_FAIL" },				//Fehler in FuSi Schnittstelle (CRC bzw. AliveCounter)	
				{ "LIN_UDS_MotorStopLogger__0x6b",		"LIN UDS MotorStopLogger: 0x6b (BROSE) LIN_TIMEOUT" },					//Stop wegen LIN-TimeOut 	
				{ "LIN_UDS_MotorStopLogger__0x6c",		"LIN UDS MotorStopLogger: 0x6c (BROSE) NO_DRVG_MODE" },					//LIN Driving Mode ung�ltig	
				{ "LIN_UDS_MotorStopLogger__0x6d",		"LIN UDS MotorStopLogger: 0x6d (BROSE) LIN_SLEEP_CMD" },				//Sleep w�hrend Motorlauf angefordert	
				{ "LIN_UDS_MotorStopLogger__0xNN",		"LIN UDS MotorStopLogger: 0x?? unknown" },								// Weitere, Hersteller spezifische Gr�nde sind mit der Fachabteilung abzustimmen	//ID: EK_K_IFE_1371
				{ "LIN_UDS_MotorStopLogger__invalid",	"LIN UDS MotorStopLogger: invalid" },
				// END OF MATERIAL TO LOCALIZE
			};
		}
	}

	/**
	 * RessourceBundle de / Deutsch
	 * @author Michael Scholler, remes GmbH
	 */
	public static class Resources_de extends ListResourceBundle {
		@Override
		protected Object[][] getContents() {
			return new Object[][] {
				// LOCALIZE THIS
				{ "Timeout", "Aktion dauert zu lange"},
				{ "Exception", "Ausnahme: "},
				{ "RequestDeviceFail", "Kann Device nicht holen"},
				{ "RequestDeviceException", "Exception w�hrend Anforderung des ASCOT DynamicDevice: "},
				{ "GetMethodNamesFail", "Devicemethoden fehlen"},
				{ "Result", "Ergebnis"},
				{ "Success", "Erfolgreich"},
				{ "Fail", "Fehlgeschlagen"},
				{ "UnknownStatus", "Unbekanter Status"},
				{ "NoJobName", "Sie m�ssen einen JobNamen angeben!" },
				{ "UserDialogOpenFail","Userdialog kann nicht angezeigt werden"},
				{ "UserDialogCloseFail","Userdialog kann nicht beendet werden"},
				{ "UserDialogCancel","Pr�fschritt von Benutzer abgebrochen"},
				{ "WaitingForUserAction", "ASCOT: Warte auf Benutzer-Interaktion..."},
				{ "ParameterCannotBeParsed__sName_sType_sValue", "%s[%s] = \"%s\" kann nicht verarbeitet werden!"},
				{ "ParameterIsNotDeclared__sName", "%s wird nicht als Parameter deklariert!"},
				{ "ParameterIsNotListedAsCascadeArg__sName", "%s wird nicht als ben�tigtes oder optionales Argument gelistet!"},
				{ "ParameterUnknown__sName", "Unbekannter Parameter: %s"},
				{ "ParameterTypeUnknown__sName_sType", "Unbekannter ParameterType: %s[s]"},
				{ "UnableToPowerOn", "Netzteil kann nicht aufgeschalten werden"},
				{ "UnableToConnectLIN", "LIN kann nicht aufgeschalten werden"},
				{ "Stroke", "Fenster f�hrt..."},
				{ "WindowFullUp", "Fenster schlie�t..."},
				{ "WindowFullDown", "Fenster �ffnet..."},
				{ "WindowShortUp", "Fenster schlie�t (Kurzhub)..."},
				{ "WindowShortDown", "Fenster �ffnet (Kurzhub)..."},
				{ "MaxCountReached", "Maximale Anzahl Versuche erreicht"},
				{ "GetErrormemory", "Auslesen Fehlerspeicher Daten"},
				{ "GetMotorStopLogger", "Auslesen Motor Stop Daten"},
				{ "PositionNotReached", "Zielposition nicht erreicht"},
				{ "WindowReversed", "Fenster reversiert"},
				{ "MoveInterrupted", "Fahrt abgebrochen"},
				{ "IFE_ButtonError__0x00", 					"IFE Fehlerspeicher aktiv, kein Fehler - Taster nicht gesteckt oder defekt"},
				{ "IFE_ButtonError__0x01", 					"IFE Fehlerspeicher aktiv, Fehler Taster - Taster nicht gesteckt oder defekt"},
				{ "IFE_ButtonError__0x02", 					"IFE Fehlerspeicher aktiv, Fehler Beleuchtung - Taster nicht gesteckt oder defekt"},
				{ "IFE_ButtonError__0x03", 					"IFE Fehlerspeicher aktiv, Fehler Taster und Beleuchtung - Taster nicht gesteckt oder defekt"},
				{ "IFE_ButtonError__0xff", 					"IFE Fehlerspeicher aktiv, Fehler ung�ltig - Taster nicht gesteckt oder defekt"},
				{ "LIN_UDS_MoveRelative__0",				"LIN UDS MoveRelative: service still not required"},
				{ "LIN_UDS_MoveRelative__1",				"LIN UDS MoveRelative: operation pending"},
				{ "LIN_UDS_MoveRelative__2",				"LIN UDS MoveRelative: routine cannot be executed"},
				{ "LIN_UDS_MoveRelative__3",				"LIN UDS MoveRelative: routine is executing"},
				{ "LIN_UDS_MoveRelative__4",				"LIN UDS MoveRelative: routine stopped succesfully"},
				{ "LIN_UDS_MoveRelative__5",				"LIN UDS MoveRelative: routine stopped with errrors"},
				{ "LIN_UDS_MoveRelative__6",				"LIN UDS MoveRelative: routine interrupted"},
				{ "LIN_UDS_MoveRelative__7",				"LIN UDS MoveRelative: invalid"},
				{ "LIN_UDS_MoveRelative__N",				"LIN UDS MoveRelative: unbekannte LIN UDS Antwort"},
				{ "LIN_UDS_ErrorMemory__0xNN",				"LIN UDS ErrorMemory: 0x?? unbekannt" },
				{ "LIN_UDS_ErrorMemory__invalid",			"LIN UDS ErrorMemory: ung�ltig" },
				{ "LIN_UDS_ErrorMemory__unexpected",		"LIN UDS ErrorMemory: unerwartete Antwort" },
				{ "LIN_UDS_ErrorMemory__Result1",			"LIN UDS ErrorMemory: Ergebnis der Ausf�hrung" },
				{ "LIN_UDS_ErrorMemory__Result2",			"LIN UDS ErrorMemory: Auftrag" },
				{ "LIN_UDS_ErrorMemory__Result3",			"LIN UDS ErrorMemory: Antwort" },
				{ "LIN_UDS_ErrorMemory__Result4",			"LIN UDS ErrorMemory: INIT_ERROR" },
				{ "LIN_UDS_ErrorMemory__Result4__0x00",		"DTC_INIT_INACTIVE" },
				{ "LIN_UDS_ErrorMemory__Result4__0x01",		"DTC_INIT_ACTIVE" },
				{ "LIN_UDS_ErrorMemory__Result5",			"LIN UDS ErrorMemory: INIT_ERROR_CODE" },
				{ "LIN_UDS_ErrorMemory__Result5__0x00",		"DTC_INIT_OK" },
				{ "LIN_UDS_ErrorMemory__Result5__0x01",		"DTC_INIT_CODING_INVALID" },
				{ "LIN_UDS_ErrorMemory__Result5__0x02",		"DTC_INIT_CODE_VERS_INVALID" },
				{ "LIN_UDS_ErrorMemory__Result5__0x04",		"DTC_INIT_HW:VERS_INVALID" },
				{ "LIN_UDS_ErrorMemory__Result5__0x08",		"DTC_INIT_NOT_POSSIBLE" },
				{ "LIN_UDS_ErrorMemory__Result5__0x10",		"DTC_INIT_ADAPT_INVALID" },
				{ "LIN_UDS_ErrorMemory__Result5__0x20",		"DTC_INIT_NORM_INVALID" },
				{ "LIN_UDS_ErrorMemory__Result5__0xff",		"DTC_INIT_INVALID" },
				{ "LIN_UDS_ErrorMemory__Result6",			"LIN UDS ErrorMemory: OPR_ERROR" },
				{ "LIN_UDS_ErrorMemory__Result6__0x00",		"DTC_OPR_INACTIVE" },
				{ "LIN_UDS_ErrorMemory__Result6__0x01",		"DTC_OPR_ACTIVE" },
				{ "LIN_UDS_ErrorMemory__Result7",			"LIN UDS ErrorMemory: OPR_ERROR_CODE" },
				{ "LIN_UDS_ErrorMemory__Result7__0x00",		"DTC_RUN_OK" },
				{ "LIN_UDS_ErrorMemory__Result7__0x01",		"DTC_RUN_THERMO_100" },
				{ "LIN_UDS_ErrorMemory__Result7__0x02",		"DTC_RUN_THERMO_90" },
				{ "LIN_UDS_ErrorMemory__Result7__0x04",		"DTC_RUN_CONTROL" },
				{ "LIN_UDS_ErrorMemory__Result7__0xff",		"DTC_RUN_INVALID" },
				{ "LIN_UDS_ErrorMemory__Result8",			"LIN UDS ErrorMemory: HW_ERROR" },
				{ "LIN_UDS_ErrorMemory__Result8__0x00",		"DTC_HW_INACTIVE" },
				{ "LIN_UDS_ErrorMemory__Result8__0x01",		"DTC_HW_ACTIVE" },
				{ "LIN_UDS_ErrorMemory__Result9",			"LIN UDS ErrorMemory: HW_ERROR_CODE" },
				{ "LIN_UDS_ErrorMemory__Result9__0x00",		"DTC_HW_OK" },
				{ "LIN_UDS_ErrorMemory__Result9__0x01",		"DTC_HW_REL_LOW_STUCK" },
				{ "LIN_UDS_ErrorMemory__Result9__0x02",		"DTC_HW_REL_HIGH_STUCK" },
				{ "LIN_UDS_ErrorMemory__Result9__0x04",		"DTC_HW_COMMON" },
				{ "LIN_UDS_ErrorMemory__Result9__0x08",		"DTC_HW_HAL" },
				{ "LIN_UDS_ErrorMemory__Result9__0xff",		"DTC_HW_INVALID" },
				{ "LIN_UDS_ErrorMemory__Result10",			"LIN UDS ErrorMemory: BUTTON_ERROR" },
				{ "LIN_UDS_ErrorMemory__Result10__0x00",	"DTC_BUTTON_INACTIVE" },
				{ "LIN_UDS_ErrorMemory__Result10__0x01",	"DTC_BUTTON_ACTIVE" },
				{ "LIN_UDS_ErrorMemory__Result11",			"LIN UDS ErrorMemory: BUTTON_ERROR_CODE" },
				{ "LIN_UDS_ErrorMemory__Result11__0x00",	"DTC_BUTTON_OK" },
				{ "LIN_UDS_ErrorMemory__Result11__0x01",	"DTC_BUTTON_ERR" },
				{ "LIN_UDS_ErrorMemory__Result11__0x02",	"DTC_BUTTON_ILUM_ERR" },
				{ "LIN_UDS_ErrorMemory__Result11__0xff",	"DTC_BUTTON_INVALID" },
				{ "LIN_UDS_MotorStopLogger__0x00",		"LIN UDS MotorStopLogger: 0x00 NOT_STOPPED" },			// Motor f�hrt																		//ID: EK_K_IFE_738	
				{ "LIN_UDS_MotorStopLogger__0x01",		"LIN UDS MotorStopLogger: 0x01 POSITION_REACHED" },		// Angeforderte Endposition oder Endstellung erreicht								//ID: EK_K_IFE_739	
				{ "LIN_UDS_MotorStopLogger__0x02",		"LIN UDS MotorStopLogger: 0x02 STOP_MOVE" },			// Bewegung durch Benutzer/Bedienkonzept abgebrochen								//ID: EK_K_IFE_740	
				{ "LIN_UDS_MotorStopLogger__0x03",		"LIN UDS MotorStopLogger: 0x03 NORM" },					// Normierungsposition gefunden														//ID: EK_K_IFE_741	
				{ "LIN_UDS_MotorStopLogger__0x04",		"LIN UDS MotorStopLogger: 0x04 RENORM" },				// Nachnormierung durchgef�hrt														//ID: EK_K_IFE_742	
				{ "LIN_UDS_MotorStopLogger__0x05",		"LIN UDS MotorStopLogger: 0x05 PINCHING" },				// Einklemmfall erkannt																//ID: EK_K_IFE_743	
				{ "LIN_UDS_MotorStopLogger__0x06",		"LIN UDS MotorStopLogger: 0x06 REV_POS_REACHED" },		// Reversiervorgang beendet															//ID: EK_K_IFE_744	
				{ "LIN_UDS_MotorStopLogger__0x07",		"LIN UDS MotorStopLogger: 0x07 BLOCKED" },				// Blockierung erkannt																//ID: EK_K_IFE_745	
				{ "LIN_UDS_MotorStopLogger__0x08",		"LIN UDS MotorStopLogger: 0x08 NOT_MOVED" },			// Motor steht = Initialwert des Ringspeichers										//ID: EK_K_IFE_577	
				{ "LIN_UDS_MotorStopLogger__0x09",		"LIN UDS MotorStopLogger: 0x09 SAFETY_TIMER" }, 		// Max. Ansteuerzeit �berschritten													//ID: EK_K_IFE_746	
				{ "LIN_UDS_MotorStopLogger__0x0a",		"LIN UDS MotorStopLogger: 0x0a OPPOSITE_DIRECTION" }, 	// Ansteuerrichtung ungleich durch Hall-Sensoren ermittelte Fahrrichtung			//ID: EK_K_IFE_747	
				{ "LIN_UDS_MotorStopLogger__0x0b",		"LIN UDS MotorStopLogger: 0x0b TARGET_POS_LOW" }, 		// Zielposition zu niedrig															//ID: EK_K_IFE_905	
				{ "LIN_UDS_MotorStopLogger__0x0c",		"LIN UDS MotorStopLogger: 0x0c TARGET_POS_HIGH" }, 		// Zielposition zu hoch																//ID: EK_K_IFE_906	
				{ "LIN_UDS_MotorStopLogger__0x0d",		"LIN UDS MotorStopLogger: 0x0d TERM100" }, 				// Bewegung wurde aufgrund �berhitzung (Thermo 100) unterbrochen					//ID: EK_K_IFE_750	
				{ "LIN_UDS_MotorStopLogger__0x0e",		"LIN UDS MotorStopLogger: 0x0e HARDWARE_ERROR" }, 		// Genereller Fehler in der Ansteuerhardware/IFE									//ID: EK_K_IFE_907	
				{ "LIN_UDS_MotorStopLogger__0x0f",		"LIN UDS MotorStopLogger: 0x0f MOTOR_KURZSCHLUSS" }, 	// Kurzschlu� zwischen den Motorzuleitungen											//ID: EK_K_IFE_908	
				{ "LIN_UDS_MotorStopLogger__0x10",		"LIN UDS MotorStopLogger: 0x10 RESET" }, 				// Bewegung wurde durch Reset/Spannungsverlust unterbrochen							//ID: EK_K_IFE_748	
				{ "LIN_UDS_MotorStopLogger__0x11",		"LIN UDS MotorStopLogger: 0x11 HALL_PULSE_LOST" }, 		// Hall-Puls(e) verloren															//ID: EK_K_IFE_909	
				{ "LIN_UDS_MotorStopLogger__0x12",		"LIN UDS MotorStopLogger: 0x12 VOLTAGE_RANGE" }, 		// Bewegung wurde aufgrund Spannungsgrenzen unterbrochen							//ID: EK_K_IFE_749	
				{ "LIN_UDS_MotorStopLogger__0x13",		"LIN UDS MotorStopLogger: 0x13 HALL_HW_ERROR" }, 		// Fehler in Hallsensoren-HW														//ID: EK_K_IFE_910	
				{ "LIN_UDS_MotorStopLogger__0x14",		"LIN UDS MotorStopLogger: 0x14 OSEK" }, 				// keine Rechenzeit f�r EKS-Algorithmus												//ID: EK_K_IFE_911	
				{ "LIN_UDS_MotorStopLogger__0x15",		"LIN UDS MotorStopLogger: 0x15 AUTOMOVE_CMD_FAIL" },	
				{ "LIN_UDS_MotorStopLogger__0x16",		"LIN UDS MotorStopLogger: 0x16 reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x17",		"LIN UDS MotorStopLogger: 0x17 reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x18",		"LIN UDS MotorStopLogger: 0x18 reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x19",		"LIN UDS MotorStopLogger: 0x19 reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x1a",		"LIN UDS MotorStopLogger: 0x1a reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x1b",		"LIN UDS MotorStopLogger: 0x1b reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x1c",		"LIN UDS MotorStopLogger: 0x1c reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x1d",		"LIN UDS MotorStopLogger: 0x1d reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x1e",		"LIN UDS MotorStopLogger: 0x1e reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x1f",		"LIN UDS MotorStopLogger: 0x1f reserved for BMW" },	
				{ "LIN_UDS_MotorStopLogger__0x60",		"LIN UDS MotorStopLogger: 0x60 (BROSE) AUTO_COND_LOST" },				//Wegfall Automatikfreigabe	
				{ "LIN_UDS_MotorStopLogger__0x61",		"LIN UDS MotorStopLogger: 0x61 (BROSE) CODING_FAIL" },					//Fehler Codierdaten Checksumme	
				{ "LIN_UDS_MotorStopLogger__0x62",		"LIN UDS MotorStopLogger: 0x62 (BROSE) WAITING_FOR_EE" },				//Warte auf EEPROM	
				{ "LIN_UDS_MotorStopLogger__0x63",		"LIN UDS MotorStopLogger: 0x63 (BROSE) EE_TIMEOUT" },					//Keine Antwort vom NVRAM Manager	
				{ "LIN_UDS_MotorStopLogger__0x64",		"LIN UDS MotorStopLogger: 0x64 (BROSE) NOT_POSSIBLE_TO_WRITE_EE" },		//Position kann nicht in EEPROM geschrieben werden	
				{ "LIN_UDS_MotorStopLogger__0x65",		"LIN UDS MotorStopLogger: 0x65 (BROSE) HALL_DISABLED" },				//Hall ausgeschaltet	
				{ "LIN_UDS_MotorStopLogger__0x66",		"LIN UDS MotorStopLogger: 0x66 (BROSE) CODING_SESSION" },				//	
				{ "LIN_UDS_MotorStopLogger__0x67",		"LIN UDS MotorStopLogger: 0x67 (BROSE) PANIC_NOT_VALID" },				//Panic Close nicht erlaubt	
				{ "LIN_UDS_MotorStopLogger__0x68",		"LIN UDS MotorStopLogger: 0x68 (BROSE) DIAGNOSTIC_SESSION_ACTIVE" },	//F�r BROSE und CONTI gleich??	
				{ "LIN_UDS_MotorStopLogger__0x69",		"LIN UDS MotorStopLogger: 0x69 (BROSE) STALL_PLAY_PROTECTION" },		//Keine Ansteuerung aufgrund von Spielschutz	
				{ "LIN_UDS_MotorStopLogger__0x6a",		"LIN UDS MotorStopLogger: 0x6a (BROSE) CRC_ALIVE_FAIL" },				//Fehler in FuSi Schnittstelle (CRC bzw. AliveCounter)	
				{ "LIN_UDS_MotorStopLogger__0x6b",		"LIN UDS MotorStopLogger: 0x6b (BROSE) LIN_TIMEOUT" },					//Stop wegen LIN-TimeOut 	
				{ "LIN_UDS_MotorStopLogger__0x6c",		"LIN UDS MotorStopLogger: 0x6c (BROSE) NO_DRVG_MODE" },					//LIN Driving Mode ung�ltig	
				{ "LIN_UDS_MotorStopLogger__0x6d",		"LIN UDS MotorStopLogger: 0x6d (BROSE) LIN_SLEEP_CMD" },				//Sleep w�hrend Motorlauf angefordert	
				{ "LIN_UDS_MotorStopLogger__0xNN",		"LIN UDS MotorStopLogger: 0x?? unbekannt" },							// Weitere, Hersteller spezifische Gr�nde sind mit der Fachabteilung abzustimmen	//ID: EK_K_IFE_1371					
				{ "LIN_UDS_MotorStopLogger__invalid",	"LIN UDS MotorStopLogger: ung�ltig" },
				// END OF MATERIAL TO LOCALIZE
			};
		}
	}
}

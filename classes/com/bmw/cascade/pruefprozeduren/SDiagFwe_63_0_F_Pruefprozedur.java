package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;

import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.devices.fwestand.*;
import com.bmw.cascade.pruefstand.ediabas.*;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * <b>Beschreibung</b> <br>
 * Diese Pr�fprozedur liefert die Funktionalit�t zur Steuerung des
 * Prozessablaufs am Fahrwerk-Einstellstand (FWE-Stand). Somit wird der
 * FWE-Stand von allen Umf�ngen zur Fahrzeugdiagnose entlastet. Zum Ansprechen
 * der diversen Ablaufschritte wurde die Pr�fprozedur in entsprechende Jobs
 * unterteilt, die �ber das zwingende Argument "JOB_ID" adressiert werden.
 * Jeder Job kann unabh�ngig von anderen Jobs und beliebig oft benutzt werden.
 * Die Pr�fprozedur benutzt zur Kommunikation das FweStandDevice.
 * 
 * Der zwingend erforderliche Parameter f�r alle Funktionen (vgl. getRequiredArgs())
 * ist "JOB_ID" als Identifizierer f�r einen Pr�fprozedur-internen Job. Die
 * unterst�tzten Werte k�nnen der Funktion getJobId() entnommen werden.
 * Die optionalen Parameter (vgl. getOptionalArgs()) liefern verschiedene
 * Zusatzparameter, die je nach pp-internem Job ben�tigt werden.
 * 
 * @author GEFASOFT GmbH, MM <br>
 *         K. Petersen, BMW AG<br>
 *         P. Jorge, IndustrieHansa GmbH<br>
 *         U. Plath, BMW AG<br>
 * @version 0_0_1_FA 01.10.2004 MM Ersterstellung<BR>
 * @version 0_0_2_FA 14.11.2004 MM Anzeige UserDialog generell �ber opt.
 *          Argument parametrierbar Diagnoseergebnisse CALIB_STEER_ANG Abfrage
 *          parametrisierbar �ber opt. Argumente class members ersetzt durch
 *          getter Ergebnisse kontrolliert und vervollst�ndigt einige Texte in
 *          package.properties eingebaut
 * @version 0_0_3_FA 16.11.2004 S. Pichler, TI-432; M. M�ller GEFASOFT GmbH
 *          unbenutzten Thread entfernt Methode cancelProzess Dialoganzeige
 *          deutsch auch ok ohne property text und Warteschleife wieder nach
 *          Abbruchprozedur aber unterbrechbar �ber Benutzerdialog Parameter f�r
 *          SET_RELEVANCE: alle au�er "ACC" entfernt Ergebnisse �berarbeitet
 * @version 0_0_4_FA 17.11.2004 S. Pichler, TI-432; M. M�ller GEFASOFT GmbH
 *          EDIABAS Call Exceptions speziell gefangen In Job INIT_FWE sleep nach
 *          allen write Aufrufen eingef�gt, um SiemensremoteFwe.Dll nicht zu
 *          �berfahren
 * @version 0_0_5_FA 30.05.2006 K. Petersen, TI-432 Optionale Argumente
 *          "LWS_JOB", "LWS_RESULT" und "LWS_LWM_DIFF" hinzugef�gt. Damit wird
 *          der Job und das Result f�r das Auslesen des momentanen
 *          Lenkwinkelsensorwert an die Pr�fprozedur �bergeben. Die maximal
 *          zul�ssige Abweichung wird �ber "LWS_LWM_DIFF" �bergeben. Alle 3
 *          Argumente sind mit Defaultwerte festegelegt: "LWS_JOB":
 *          STATUS_SENSOREN, "LWS_RESULT": STAT_LENKWINKEL_WERT, "LWS_LWM_DIFF":
 *          0.1 Abweichung zwischen Lenkwinkelwert und LWMWert werden ermittelt
 *          und mit LWS_LWM_DIFF verglichen. Pr�fschritt wird niO bei nicht
 *          Einhaltung der Grenzen.
 * @version 0_8_T 30.05.2006 K. Petersen, TI-432 Optionale Argumente
 *          "CALIB_WITHOUT_OFFSET", "HWT_LWS_LMW_DIFF", "HWT_LMW1_DIFF" und
 *          "HWT_BOTH_LMW" hinzugef�gt. "CALIB_WITHOUT_OFFSET": Bei true wird
 *          der Offsetwert der LMW beim LWS-Abgleich nicht �bergeben. Der Wert
 *          ist Standardm�ssig auf false gelegt. Bei nicht �bergabe, wird der
 *          Wert im LWS verglichen trotzdem mit dem Wert aus der LMW unter
 *          ber�cksichtigung des LMW-Wertes zum Zeitpunkt der LWS-Abgleiches.
 *          "HWT_LWS_LMW_DIFF": Hinweistext f�r den Fall, dass die Differenz
 *          zwischen LWS und LMW ausserhalb der Toleranz ist. "HWT_LMW1_DIFF":
 *          Hinweistext f�r den Fall, dass der zu �bergebende LMW-Wert (beim
 *          LWS-Abgleich) ausserhalb der Toleranz ist. "HWT_BOTH_LMW":
 *          Hinweistext f�r den Fall, dass die Differenz zwischen LMW-Wert vor
 *          und nach dem LWS-Abgleich zu hoch w�re. Beim Vergleich LWS-Wert mit
 *          LMW-Wert nach dem LWS-Abgleich zur �berpr�fung des korrekten
 *          LWS-Abgleiches, wird der LMW-Wert nochmal aktualisiert, damit
 *          Bewegungen am Lenkrad, die Prozesssicherheit nicht verhindern. <BR>
 * @version 13_0_F 18.10.2006 KP While Schleife hinzugef�gt f�r die �berpr�fung
 *          der LWS LMW Diferenz.
 * @version 14_0_T 18.10.2006 KP Fehler in While Schleife behoben.
 * @version 19_0_T 23.03.2007 KP Ergebnisse aus der While-Schleife werden nur
 *          einmal bei niO mit F bewertet sonst mit W.
 * @version 20_0_T 18.04.2007 KP ALLOW_HA hinzugef�gt un Hoehenstandsabgleich.
 * @version 23_0_F 10.05.2007 PJ Jobs READ_VARIABLE und WRITE_VARIABLE
 *          hinzugef�gt.
 * @version 24_0_F 18.06.2007 PJ Jobs READ_VARIABLE und WRITE_VARIABLE
 *          Erweiterungen und Bugs korrigiert. Job EHC_Hoehenstandsabgleich bug
 *          korrigiert.
 * @version 25_0_F 25.06.2007 PJ Blankplatz vom Job READ_VARIABLE entfernt
 * @version 27_0_F 04.09.2007 PJ DEVICE_TAG inserted as optional argument to
 *          allow in parallel execution. PAUSE_MS inserted as optional argument
 *          to allow different polling times. Missing optional argument
 *          PRESET_VALUE re-inserted.
 * @version 28_0_F 07.08.2008 UP Job Nr. 26 "DO_VA_HA" als Kopie von "DO_VA" f�r
 *          Bosch KDS hinzugef�gt, Job Nr. 27 "RESET" hinzugef�gt, Job Nr. 28
 *          "DISCONNECT" hinzugef�gt, Funktion "log" f�r Ausgaben ins
 *          CASCADE-Log hinzugef�gt, Behandlung des FWE_DEVICE_TAG ge�ndert:
 *          wenn nicht vorhanden, wird "" anstelle von NULL angezeigt.
 * @version 29_0_F 17.07.2009 UP Spezielle Abbruchbehandlung f�r Job Nr. 26
 *          hinzugef�gt, damit beim Bosch KDS RR2- und RR3-Fahrzeuge vor der
 *          letzten Messung in der H�he eingestellt werden k�nnen.
 * @version 30_0_T 15.12.2010 UP Unn�tige Ausgaben im Job "INIT" (Nr. 20)
 *          entfernt.
 * @version 31_0_F 11.01.2011 UP Freigabe Version 30_T
 * @version 32_0_F 15.06.2011 UP �berarbeitung des Jobs. Nr. 5 "CALIB_STEER_ANG"
 *          mit Anpassung der APDM-Hinweistexte und Entsch�rfung der Bewertung
 *          bei Abweichungen zwischen den verschiedenen Lenkwinkelmessungen.
 * @version 33_0_F 16.06.2011 UP Bugfix der Version 32 im Job CALIB_STEER_ANG
 * @version 34_0_T 15.08.2011 UP Testversion zur Nutzung der korrekten SGBD bei
 *          DO_VA und DO_VA_HA, Format�berarbeitung
 * @version 35_0_F 01.09.2011 UP Freigabe der �nderungen aus Version 34
 * @version 36_0_F 21.11.2011 UP Aufl�sung von @-Konstrukten in Job
 *          WRITE_VARIABLE hinzugef�gt.
 * @version 37_0_F 16.05.2011 UP Job-ID 29 "LOGGING" f�r das Ein- und
 *          Ausschalten der Log-Funktion des FWE-Stands hinzugef�gt. Verhalten
 *          von Cascade im Abbruchfall an Spezifikation angepasst: Cascade
 *          wartet auf den Status "0" vom FWS, erst dann geht es selbst in den
 *          Status "0" (cancelProcess). Toleranz f�r einen Netzwerkfehler
 *          hinzugef�gt, beim zweiten liefert Cascade einen Fehler.
 * @version 38_0_F 07.08.2012 UP Bugfix von Version 37
 * @version 39_0_F 16.01.2013 UP In den Jobs READ_VARIABLE und WAIT_LMW wird jetzt
 *          gepr�ft, ob ein Double-Wert ein Komma enth�lt. Dieses wird durch einen
 *          Punkt ersetzt (Workaround f�r D�RR-Anlagen)
 * @version 40_0_F 21.02.2013 UP Bug im Job Nr. 4 WAIT_LMW beseitigt, bei dem der
 *          Schritt OK beendet wurde, auch wenn der Lenkwinkel au�erhalb des
 *          Toleranzfensters gelegen hat.
 * @version 41_0_F 04.04.2013 UP Einf�hrung des neuen Parameters COMMA_VALUES, mit
 *          dem man das Format der Flie�kommazahlen beim senden an den FWE-Stand
 *          steuern kann (Punkt oder Komma).
 * @version 42_0_F 03.06.2013 UP Neuer catch-Block nach executeDiagJob im Job
 *          DO_VA hinzugef�gt, um Zuverl�ssigkeit des Jobs zu verbessern.
 * @version 43_0_F 21.05.2014 UP Nach SW-Anpassung bei D�RR kann der Parameter
 *          COMMA_VALUES entfallen, Werkerhinweise bei CANCEL_PROCESS angepasst
 * @version 44_0_T 09.10.2014 UP Zusammen mit S. Andree neue JOB_ID 30
 *          HEIGHT_CURVE_RECORD zur Aufzeichnung von H�henstandskurven an der RAMA
 *          hinzugef�gt. Funktion heightCurveComplete() hinzugef�gt, bei Funktion
 *          convertEdiabasValue() zweiten Parameter von StringBuffer auf String
 *          ge�ndert. Beschreibung im Quelltext-Kopf ge�ndert.
 * @version 45_0_F 31.10.2014 UP Freigegebene Version von v44 mit den neuen
 *          RAMA-Funktionen.
 * @version 46_0_T 15.01.2015 SA �nderung der Variablennamen dftCrvRecHeightFL,
 *          -FR, -RL, -RR zu dftCrvRecVoltageFL, -FR, -RL, RR
 * @version 47_0_F 15.01.2015 SA Freigabe der �nderungen aus Version 46
 * @version 48_0_T 27.04.2015 UP Implementierung der neuen Funktion
 *          OSCILLATION_CHECK ausschlie�lich zum Test im FIZ
 * @version 49_0_T 19.05.2015 UP Gr��ere �berarbeitung. Der Job CALIB_STEER_ANG,
 *          der in der L2 bei E9x genutzt wurde, wurde ersatzlos gestrichen.
 *          Desgleichen wurden die nicht mehr genutzen Jobs SET_RELEVANCE,
 *          GRANT_DIAG, WAIT_DIAG, WAIT_ACC_SOLVED, WAIT_SW_SOLVED, ALLOW_ACC und
 *          TEST_ALL entfernt. Die JOB_IDs wurden neu sortiert und leere JOB_IDs
 *          gel�scht. Der neue Job OSCILLATION_CHECK wurde hinzugef�gt (LOP 1943).
 *          Die Utility-Methoden IsJobIdIdxInvalid(), UsePresetValue(),
 *          getCascadeStatus(), convertEdiabasValue() und TestAllFwe() wurden
 *          entfernt, ebenso der optionale Parameter PRESET_VALUE.
 * @version 50_0_F 22.05.2015 UP Freigabe der �nderungen aus Version 49
 * @version 51_0_T 23.09.2015 UP Bugfix f�r Funktion OSCILLATION_CHECK, die un-
 *          genutzten optionalen Argumente STATUS und SYSTEM wurden gel�scht
 * @version 52_0_F 25.09.2015 UP Freigabe der �nderungen aus Version 51_0_T
 * @version 57_0_T 30.08.2016 MS �nderung am Job READY
 * @version 58_0_T 06.09.2016 MS �nderung wegen Anpassung an Siemens Pr�fst�nden
 * @version 60_0_T 13.09.2016 SA Anpassung des Abbruchverhaltens. 
 *			Cascade wechselt nur noch im Cascade FWE Ready Pr�fschritt auf Status 0 
 *			und setzt das Device zur�ck.
 * @version 61_0_T 16.09.2016 SA Beseitigung eines Fehlers in der ABORT-Funktion,
 *          bei dem die PP in einer Endlosschleife h�ngen blieb.
 * @version 62_0_T 16.09.2016 SA Beseitigung eines Fehlers in der READY-Funktion,
 *          bei dem CASCADE den Ablauf beendete, obwohl der Fahrwerksstand noch
 *          nicht fertig war  
 * @version 63_0_F 27.09.2016 SA Freigabe der �nderungen und T-Version
 */
public class SDiagFwe_63_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public SDiagFwe_63_0_F_Pruefprozedur() {
	}

	private Hashtable<String, Integer> hJobIds = null;

	/**
	 * Tabelle erlaubter JobIds f�r diese PP; wird in der Methode getJobId(...)
	 * initialisiert; Ist als class-member implementiert, um st�ndige
	 * Neuinitialisierung bei Aufruf von getJobId(...) zu vermeiden. Der Inhalt
	 * der Tabelle ist ansonsten konstant und deshalb f�r die
	 * Multi-Instanz-F�higkeit von CASCADE unproblematisch!
	 */

	/***************************************************************************
	 * Konstruktor mit Standard PP-Paramtern; diese werden �ber super Aufruf an
	 * Basisklasse weitergereicht.
	 * 
	 * @param pruefling
	 *            Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName
	 *            Name der Pr�fprozedur
	 * @param hasToBeExecuted
	 *            Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public SDiagFwe_63_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/***************************************************************************
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/***************************************************************************
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "SGBD", "SGBD_JOB", "SGBD_ARGUMENT", "SGBD_RESULT", "MIN_VALUE", "MAX_VALUE", "TIMEOUT_MS", "ERROR_INFO", "MESSAGE", "ALLOW_CANCEL", "VARIABLE", "TYPE", "SET_VALUE", "DEVICE_TAG", "PAUSE_MS", "DEBUG" };
		return args;
	}

	/***************************************************************************
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = { "JOB_ID" };
		return args;
	}

	/***************************************************************************
	 * Pr�ft die Argumente auf Existenz und Wert. Achtung: Diese Methode wird
	 * auf dem Cascade-Server aufgerufen und darf deshalb auf keine Ressourcen
	 * (Dateien, Verzeichnisse etc.) zugreifen, die nur auf dem
	 * Pr�fstands-Client verf�gbar sind.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt
	 *         hat, ansonsten <code>true</code>.
	 */
	@SuppressWarnings("unused")
	public boolean checkArgs() {
		boolean debug = (getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;

		log( debug, "Info", "Checking Arguments..." );
		if( !super.checkArgs() ) {
			log( debug, "Error", "checkArgs() failed!" );
			return false;
		}

		/** pr�fe zwingende Argumente */
		int nJobId = getJobId( getArg( "JOB_ID" ) );
		if( nJobId <= 0 )
			return false;

		/** pr�fe optionale Argumente, die f�r alle Jobs verwendet werden k�nnen */
		String sT = new String();
		double dT;
		int iT;
		long lT;

		boolean fOptArgsOk = true;
		// opt Args f�r alle Jobs
		if( checkExist( getArg( "MESSAGE" ) ) ) {
			if( checkExist( getArg( "ALLOW_CANCEL" ) ) ) {
				sT = getArg( "ALLOW_CANCEL" );
				if( !((sT.equalsIgnoreCase( "FALSE" )) || (sT.equalsIgnoreCase( "TRUE" ))) )
					fOptArgsOk = false;
			}
		}

		/** pr�fe optionale Argumente, die Job-abh�ngig sind */
		switch( nJobId ) {
			case 1: // CHECK_WDH_EINST
				// no optional args
				break;
			case 2: // CHECK_MEASUREMENT
				// no optional args
				break;
			case 3: // ALLOW_SW
				// no optional args
				break;
			case 4: // WAIT_LMW
				try {
					double dftV;
					if( checkExist( getArg( "MIN_VALUE" ) ) )
						dftV = Double.parseDouble( getArg( "MIN_VALUE" ) );
					if( checkExist( getArg( "MAX_VALUE" ) ) )
						dftV = Double.parseDouble( getArg( "MAX_VALUE" ) );
				} catch( Exception e ) {
					log( debug, "Error", "Exception while parseDouble MIN/MAX_VALUE " + e.getMessage() );
					fOptArgsOk = false;
				}
				fOptArgsOk = isPosLong( getArg( "TIMEOUT_MS" ) ); // is positive long
				break;
			case 5: // WAIT_VA_COMPLETE
				fOptArgsOk = isPosLong( getArg( "TIMEOUT_MS" ) );
				break;
			case 6: // WAIT_HA_COMPLETE
				fOptArgsOk = isPosLong( getArg( "TIMEOUT_MS" ) );
				break;
			case 7: // OSCILLATION_CHECK
				try {
					if( getArg( "MIN_VALUE" ).trim().equalsIgnoreCase( "" ) ) {
						throw new Exception ( "Parameter MIN_VALUE is emtpy!" );
					} else
						dT = Double.parseDouble( getArg( "MIN_VALUE" ) );

					if( getArg( "MIN_TRANS" ) != null )
						iT = Integer.parseInt( getArg( "MIN_TRANS" ) );

					if( getArg( "TIMEOUT_MS" ) != null )
						lT = Long.parseLong( getArg( "TIMEOUT_MS" ) );

					if( getArg( "PAUSE_MS" ) != null)
						lT = Long.parseLong( getArg( "PAUSE_MS" ) ); 
				} catch( Exception e ) {
					log( debug, "Error", "Exception while checking args " + e.getMessage() );
					e.printStackTrace();
					fOptArgsOk = false;
				}
				break;
			case 8: // ALLOW_VA
				// no optional args
				break;
			case 9: // DO_VA
				sT = getArg( "SGBD" );
				fOptArgsOk = checkExist( sT );
				sT = getArg( "SGBD_JOB" );
				if( fOptArgsOk )
					fOptArgsOk = checkExist( sT );
				break;
			case 10: // ALLOW_HA
				// no optional args
				break;
			case 11: // READ_VARIABLE (req: VARIABLE, TYPE, MIN_VALUE, MAX_VALUE
				// opt: TIMEOUT_MS, PAUSE_MS)
				try {
					// ----------------------- req
					// VARIABLE defined ?
					if( getArg( "VARIABLE" ).trim().equalsIgnoreCase( "" ) ) {
						throw new Exception( "VARIABLE empty!" );
					}
					// TYPE defined ? MIN_VALUE and MAX_VALUE from TYPE ?
					if( getArg( "TYPE" ).equalsIgnoreCase( "BOOLEAN" ) ) {
						new Boolean( getArg( "MIN_VALUE" ) ).booleanValue();
						new Boolean( getArg( "MAX_VALUE" ) ).booleanValue();
					} else if( getArg( "TYPE" ).equalsIgnoreCase( "SHORT" ) ) {
						new Short( getArg( "MIN_VALUE" ) ).shortValue();
						new Short( getArg( "MAX_VALUE" ) ).shortValue();
					} else if( getArg( "TYPE" ).equalsIgnoreCase( "LONG" ) ) {
						new Long( getArg( "MIN_VALUE" ) ).longValue();
						new Long( getArg( "MAX_VALUE" ) ).longValue();
					} else if( getArg( "TYPE" ).equalsIgnoreCase( "DOUBLE" ) ) {
						new Double( getArg( "MIN_VALUE" ) ).doubleValue();
						new Double( getArg( "MAX_VALUE" ) ).doubleValue();
					} else if( getArg( "TYPE" ).equalsIgnoreCase( "STRING" ) ) {
						if( getArg( "MIN_VALUE" ).trim().equalsIgnoreCase( "" ) )
							throw new Exception( "MIN_VALUE empty!" );
					} else {
						throw new Exception( "variable TYPE invalid!" );
					}
					// ----------------------- opt
					// TIMEOUT_MS > 0 ?
					if( getArg( "TIMEOUT_MS" ) != null && new Long( getArg( "TIMEOUT_MS" ) ).longValue() <= 0 )
						throw new Exception( "TIMEOUT_MS invalid! " + getArg( "TIMEOUT_MS" ) );
					// PAUSE_MS > 0 ?
					if( getArg( "PAUSE_MS" ) != null && new Long( getArg( "PAUSE_MS" ) ).longValue() <= 0 )
						throw new Exception( "PAUSE_MS invalid! " + getArg( "PAUSE_MS" ) );

				} catch( Exception e ) {
					log( debug, "Error", "Exception while checking args " + e.getMessage() );
					fOptArgsOk = false;
				}
				break;
			case 12: // WRITE_VARIABLE (req: VARIABLE, SET_VALUE opt: none)
				try {
					// ----------------------- req
					if( getArg( "VARIABLE" ).trim().equalsIgnoreCase( "" ) )
						throw new Exception( "VARIABLE empty!" );
					if( getArg( "SET_VALUE" ).trim().equalsIgnoreCase( "" ) )
						throw new Exception( "SET_VALUE empty!" );

				} catch( Exception e ) {
					log( debug, "Error", "Exception while checking args " + e.getMessage() );
					fOptArgsOk = false;
				}
				break;
			case 13: // RESET
				// no optional args
				break;
			case 14: // ABORT
				sT = getArg( "ERROR_INFO" );
				fOptArgsOk = checkExist( sT );
				break;
			case 15: // READY
				// no optional args
				break;
			case 16: // INIT_FWE
				// no optional args
				break;
			case 17: // DO_VA_HA
				sT = getArg( "SGBD" );
				fOptArgsOk = checkExist( sT );
				sT = getArg( "SGBD_JOB" );
				if( fOptArgsOk )
					fOptArgsOk = checkExist( sT );
				break;
			case 18: // EHC_HOEHENSTANDSABGLEICH
				sT = getArg( "SGBD" );
				fOptArgsOk = checkExist( sT );
				sT = getArg( "SGBD_JOB" );
				if( fOptArgsOk )
					fOptArgsOk = checkExist( sT );
				break;
			case 19: // DISCONNECT
				// no optional args
				break;
			case 20: // LOGGING
				sT = getArg( "SET_VALUE" );
				fOptArgsOk = checkExist( sT );
				break;
			case 21: // HEIGHT_CURVE_RECORD
				sT = getArg( "SGBD" );
				fOptArgsOk = checkExist( sT );
				sT = getArg( "SGBD_JOB" );
				if( fOptArgsOk )
					fOptArgsOk = checkExist( sT );
				sT = getArg( "SGBD_ARGUMENT" );
				if( fOptArgsOk )
					fOptArgsOk = checkExist( sT );
				sT = getArg( "SGBD_RESULT" );
				if( fOptArgsOk )
					fOptArgsOk = checkExist( sT );
				break;

			default: // all other jobs, no optional arguments used
				break;
		}

		return fOptArgsOk;
	}

	/***************************************************************************
	 * execute F�hrt die Pr�fprozedur aus. Die Methode enth�lt diverse
	 * Einzelumf�nge (Jobs), die �ber das zwingende Argument JOB_ID adressiert
	 * werden und jeweils unabh�ngig voneinander benutzt werden k�nnen.
	 * 
	 * @param info
	 *            ExecutionInfo Parameter
	 */
	public void execute( ExecutionInfo info ) {
		boolean debug = (getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
		Ergebnis result;
		/**
		 * Ergebnis-Objekt zur Aufnahme in der Ergebnisliste (ist
		 * Pr�fprozeduren-spezifisch)
		 */
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		/** Ergebnisliste (ist Pr�fprozeduren-spezifisch) */
		int status = STATUS_EXECUTION_ERROR;
		/**
		 * Konstante f�r den Ergebnis-Status dieser Pr�fprozedur (ist
		 * Pr�fprozeduren-spezifisch) Default ist ERROR
		 */

		FweStand FweDevice = null;
		/** Variable f�r das (Any)FweStandDevice */
		boolean finished = false;
		/** Ende-Kontrolle f�r den jweweiligen PP-job */
		int nFweStatus;
		/** ProcesState des FWE-Stand */
		StringBuffer sbT = new StringBuffer();
		/** temp. StringBuffer */
		UserDialog uiDlg = null;
		/** temp. Userdialog object */
		String sTemp = new String();
		/** temp. String */
		int nLoopErrorCnt = 0;
		/** Counter f�r EDIABAS Errors */
		long lStartTime = 0;
		/** Variable f�r Timeout Messung */

		String varName = "";
		String varType = "";
		String varReadValue = "";
		String varMinValue = "";
		String varMaxValue = "";
		String varSetValue = "";
		String sLWinverse = "";
		String errorText = "";
		
		long lTimeoutTime = 0;
		long lActualTime = 0;
		long lStopTime = 0;
		long lTime = 0;
		long lMaxTime = 0;
		float fLWinverse = 0;
		double dftLmwValue = 9999.9;

		EdiabasProxyThread ediabas = null;
		int lang = System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) ? 1 : 0; //1: German, 0: English

		/** Jobs with EDIABAS diagnostics */
		String sgbd = new String();
		String EdiabasErg = new String();
		String sArgs = new String();
		int nMaxErrors = 100;
		String sResults = new String();

		/** For the READ_VARIABLE and WRITE_VARIABLE jobs */
		String fweStandTag = (getArg( "DEVICE_TAG" ) != null) ? getArg( "DEVICE_TAG" ) : ""; // null is also valid!
		long lPauseTime = (getArg( "PAUSE_MS" ) != null) ? new Long( getArg( "PAUSE_MS" ) ).longValue() : 200; // DEFAULT = 200 ms

		// folgende Variablen nur f�r Testzwecke;
		int adr = 0;
		/** ben�tigt f�r Lenkwinkelsimulation */
		boolean fSimSG = false;
		/** Diagnose-Vollsimulation ein/ausschalten (ersetzt EDIABAS Simulation) */

		log( debug, "Info", "Pruefprozedur SDiagFwe " + this.getVersion() + ": execute() start." );

		try {

			// PP Argumente pr�fen
			if( !checkArgs() ) {
				result = new Ergebnis( "PARAM_ERROR", "SDiagFwe", getArg( "JOB_ID" ), "", "", "", "", "", "", "", "", "", "", PB.getString( "parametrierfehler" ), "Please check test procedure parameters.", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				log( debug, "Error", PB.getString( "parametrierfehler" ) );
				finished = true;
				throw new PPExecutionException( PB.getString( "parametrierfehler" ) );
			}

			// FWE Device holen und G�ltigkeit pr�fen
			FweDevice = getFweDev( fweStandTag );
			// wenn das Device hier ung�ltig ist, muss abgebrochen werden
			if( FweDevice == null ) {
				result = new Ergebnis( "DEVICE_ERROR", "FWE_DEVICE", getArg( "JOB_ID" ), "", "", "", "", "", "", "", "", "", "", "Software or configuration problem", "Please check installation and configuration of the ...FweStandDevice(Tag: " + fweStandTag + ")", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				log( debug, "Error", PB.getString( "fwedeviceinvalid" ) );
				finished = true;
				throw new PPExecutionException( PB.getString( "fwedeviceinvalid" ) );
			}

			// EDIABAS Device holen
			ediabas = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();

			// JOB_ID holen
			int nT = 0;
			String sJobId = new String();
			sJobId = getArg( "JOB_ID" );
			int nJobId = getJobId( sJobId );

			// Wenn der FWE-Stand aktiv ist, dann geht der Pr�fstand auch in
			// 'aktiv'
			nFweStatus = getFweStatus( FweDevice );
			log( debug, "OK", "FweStatus: " + nFweStatus );
			if( (nFweStatus == 1) || (nFweStatus == 2) || (nFweStatus == 3) || (nFweStatus == 5) ) {
				nT = FweDevice.write( "nProcessStateCascade[na]", "1" );
				log( debug, "OK", "FweDevice.write nProcessStateCascade[na], 1 " + nT );
			}

			// User Dialog anzeigen, wenn zutreffend
			if( mustShowUserDlg() && !finished && !((nFweStatus == 0) || (nFweStatus == 4)) ) {
				if( uiDlg == null ) {
					uiDlg = getPr�flingLaufzeitUmgebung().getUserDialog();
					if( getArg( "ALLOW_CANCEL" ).equalsIgnoreCase( "TRUE" ) )
						uiDlg.setAllowCancel( true );
					else
						uiDlg.setAllowCancel( false );
					uiDlg.displayMessage( lang == 1 ? "Info FWE-Stand" : "Info WAM rig", getArg( "MESSAGE" ), -1 );
				}
			}

			// JOB LOOP
			// --------------
			while( !finished ) {
				nFweStatus = getFweStatus( FweDevice );

				// Wenn FWE-Stand direkt inaktiv geht
				// oder wenn Abbruch des FWE Stands eingeleitet
				if( (nFweStatus == 0) || (nFweStatus == 4) ) {
					String sMsg = new String();
					sMsg = "FWE Status changed to: " + nFweStatus + " in Job " + sJobId;
					log( debug, "Abort", sMsg );
					boolean fIgnStatNull = false;

					fIgnStatNull |= sJobId.equalsIgnoreCase( "READY" );
					fIgnStatNull |= sJobId.equalsIgnoreCase( "INIT_FWE" );
					fIgnStatNull |= sJobId.equalsIgnoreCase( "RESET" );
					fIgnStatNull |= sJobId.equalsIgnoreCase( "DISCONNECT" );
					log( true, "Info", "fIgnoreStatusNull = " + fIgnStatNull );

					// Abbruch aller Pr�fschritt bei (nFweStatus = 4) oder (nFweStatus = 0) Ausnahme READY, INIT_FWE, RESET, DISCONNECT
					if( !fIgnStatNull && (nFweStatus == 4 || nFweStatus == 0)) {
						status = STATUS_EXECUTION_ERROR;
						result = new Ergebnis( "FWE_STAND_ABORT", "FWE-DEVICE", "", "", "", sJobId, "aborted", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );						
						// FweDevice.write( "nProcessStateCascade[na]", "0" );
						log( debug, "Error", "CASCADE cancels process now." );
						finished = true;
						throw new PPExecutionException();
						/*
						 * if(nFweStatus == 0) throw new
						 * PPExecutionException("FWE inaktiv w�hrend Prozess.
						 * (Undefinierter Zustand)"); break; //FWE Status == 4
						 * => Ende!
						 */
					}
				}

				// Kommunikationsfehler zum FWE, einen Fehler tolerieren
				if( nFweStatus < 0 ) {

					// Nach kurzem Warten erneut versuchen
					try {
						Thread.sleep( 1000 );
					} catch( Exception e ) {
						// nichts tun
					}

					nFweStatus = getFweStatus( FweDevice );

					if( nFweStatus < 0 ) {
						String sMsg = new String();
						sMsg = "FWE Communication error : " + nFweStatus;
						log( debug, "Error", sMsg );
						result = new Ergebnis( "COMMUNICATION_ERROR", "FWE_DEVICE", "", "", "", "STATUS", "NOK: " + nFweStatus, "0", "5", "0", "", "", "", "connection offline", "", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						throw new PPExecutionException( sMsg );
					}
				}

				try {
					switch( nJobId ) {
					// -----------------------------------------------------------------------------------------------------------------------------
						case 1: // "CHECK_WDH_EINST"
							sbT.delete( 0, sbT.length() );
							FweDevice.read( "fAlignment[na]", sbT );
							boolean fAl = sbT.toString().equalsIgnoreCase( "TRUE" );
							sbT.delete( 0, sbT.length() );
							nT = FweDevice.read( "nRepetitionCnt[na]", sbT );

							int nV = 0;
							try {
								nV = Integer.parseInt( sbT.toString() );
							} catch( Exception e ) {
								log( debug, "Error", "Number Format Exception nRepetitionCnt[na]: " + e.getMessage() );
							}

							int nPresetValue = 1;

							if( (nT == 0) && fAl && (nV > nPresetValue) ) {
								log( debug, "Info", "Repeated Alignment: " + sbT.toString() );
								status = STATUS_EXECUTION_OK; // zu oft wiederholte
								// Einstellung => OK

								result = new Ergebnis( "REP_ALIGNMENT", "FWE_DEVICE", "", "", "", "ALIGNMENT_IS_REP", "FALSE", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
							} else {
								status = STATUS_EXECUTION_ERROR; // normale Einstellung => ERROR;
								result = new Ergebnis( sJobId, "FWE_DEVICE", "", "", "", "ALIGNMENT_IS_REP", "TRUE", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
								ergListe.add( result );
							}
							finished = true;
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 2: // CHECK_MEASUREMENT
							sbT.delete( 0, sbT.length() );
							nT = FweDevice.read( "fAlignment[na]", sbT );
							if( (nT == 0) && (sbT.toString().equalsIgnoreCase( "FALSE" )) ) {
								status = STATUS_EXECUTION_OK;
								result = new Ergebnis( "MEASUREMENT", "FWE_DEVICE", "", "", "", "IS_MEASUREMENT", "FALSE", "", "0", "0", "", "", "", "", "", Ergebnis.FT_IO );
							} else {
								log( debug, "Error", "CHECK_MEASUREMENT FweDevice.read: " + sbT + " nErr: " + nT );
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( "MEASUREMENT", "FWE_DEVICE", "", "", "", "IS_MEASUREMENT", "TRUE", "", "0", "0", "", "", "", "", "", Ergebnis.FT_NIO );
							}
							ergListe.add( result );
							finished = true;
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 3: // "ALLOW_SW"
							nT = FweDevice.write( "fEnableSwCheck[na]", "TRUE" );
							if( nT == 0 ) // wenn write erfolgreich war
							{
								status = STATUS_EXECUTION_OK;
								result = new Ergebnis( "RELEASE_HL_ALIGN", "FWE_DEVICE", "", "", "", "RELEASE_STATUS", "TRUE", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							} else {
								log( debug, "Error", "Pruefprozedur SDiagFwe: ALLOW_SW: FweDevice.write: " + nT );
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( "RELEASE_HL_ALIGN", "FWE_DEVICE", "", "", "", "RELEASE_STATUS", "FALSE", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
							}
							ergListe.add( result );
							finished = true;
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 4: // "WAIT_LMW"
							boolean fQuitOk = false;
							boolean fTolOk = false;
							if( lStartTime == 0 )
								lStartTime = System.currentTimeMillis();
							sbT.delete( 0, sbT.length() );
							nT = FweDevice.read( "fLmwOk[na]", sbT );
							if( (sbT.toString().equalsIgnoreCase( "TRUE" ) ) && (nT == 0) ) {
								fQuitOk = true;
								sbT.delete( 0, sbT.length() );
								nT = FweDevice.read( "dftLmwValue[Grad]", sbT );
								if( nT == 0 ) {
									try {
										// Replace commas in double values by dots
										sTemp = sbT.toString();
										if( sTemp.contains( "," ) ) {
											sTemp = sTemp.replaceFirst( ",", "." );
										}
										dftLmwValue = Double.parseDouble( sTemp );
										log( debug, "Info", "LMW-Value: " + sTemp + " �" );
									} catch( Exception e ) {
										log( debug, "Error", "Exception in parseDouble while reading dftLmwValue[Grad]: " + e );
									}
								} else
									log( debug, "Error", "Communication error while reading dftLmwValue[Grad]: " + nT );

								double dftMin = Double.parseDouble( getArg( "MIN_VALUE" ) );
								double dftMax = Double.parseDouble( getArg( "MAX_VALUE" ) );

								// gr��er/kleiner Zuordnung automatisch richtig
								// drehen; Identit�t erlauben!
								if( dftMin > dftMax ) {
									dftMin = dftMax;
									dftMax = Double.parseDouble( getArg( "MIN_VALUE" ) );
								}

								if( (dftLmwValue > dftMin) && (dftLmwValue < dftMax) && (nT == 0) ) {
									status = STATUS_EXECUTION_OK;
									fTolOk = true;
									result = new Ergebnis( "WAIT_SWB", "FWE_DEVICE", "", "", "", "SWB_VALUE_IN_RANGE", Double.toString( dftLmwValue ), Double.toString( dftMin ), Double.toString( dftMax ), "0", "", "", "", "", "", Ergebnis.FT_IO );
								} else {
									status = STATUS_EXECUTION_ERROR;
									fTolOk = false;
									log( debug, "Error", "SWB value (" + dftLmwValue + ") out of tolerance or read error (" + nT + ")" );
									result = new Ergebnis( "WAIT_SWB", "FWE_DEVICE", "", "", "", "SWB_VALUE_NOT_IN_RANGE", Double.toString( dftLmwValue ), Double.toString( dftMin ), Double.toString( dftMax ), "0", "", "", "", "", "", Ergebnis.FT_NIO );
								}
								ergListe.add( result );
							}

							lStopTime = System.currentTimeMillis();
							lTime = lStopTime - lStartTime;
							lMaxTime = Long.parseLong( getArg( "TIMEOUT_MS" ) );
							if( lTime > lMaxTime ) {
								log( debug, "Error", "Timeout Wait LMW" );
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( "WAIT_SWB", "FWE_DEVICE", "TIMEOUT=" + lMaxTime, "", "", "SWB_VALUE", Double.toString( dftLmwValue ), "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
								ergListe.add( result );
								finished = true;
							}

							if( fQuitOk ) {
								log( debug, "OK", "LMW-Quittierung OK" );

								if( fTolOk ) {
									status = STATUS_EXECUTION_OK;
									result = new Ergebnis( "WAIT_SWB", "FWE_DEVICE", "", "", "", "SWB_VALUE", Double.toString( dftLmwValue ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								} else {
									status = STATUS_EXECUTION_ERROR;
									result = new Ergebnis( "WAIT_SWB", "FWE_DEVICE", "", "", "", "SWB_VALUE", Double.toString( dftLmwValue ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								}

								ergListe.add( result );
								finished = true;
							}
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 5: // "WAIT_VA_COMPLETE"
							if( lStartTime == 0 )
								lStartTime = System.currentTimeMillis();
							if( vaComplete( FweDevice ) ) {
								status = STATUS_EXECUTION_OK;
								result = new Ergebnis( "WAIT_FA_ALIGN", "FWE_DEVICE", "", "", "", "FA_ALIGN_READY", "TRUE", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
								finished = true;
							} else {
								lStopTime = System.currentTimeMillis();
								lTime = lStopTime - lStartTime;
								lMaxTime = Long.parseLong( getArg( "TIMEOUT_MS" ) );
								if( lTime > lMaxTime ) {
									status = STATUS_EXECUTION_ERROR;
									result = new Ergebnis( "WAIT_FA_ALIGN", "FWE_DEVICE", "TIMEOUT=" + lTime, "", "", "FA_ALIGN_READY", "FALSE", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
									ergListe.add( result );
									finished = true;
								}
							}
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 6: // "WAIT_HA_COMPLETE"
							if( lStartTime == 0 )
								lStartTime = System.currentTimeMillis();
							boolean fHaComplete = false;
							sbT.delete( 0, sbT.length() );
							nT = FweDevice.read( "fHaComplete[na]", sbT );
							if( (sbT.toString().equalsIgnoreCase( "TRUE" ) ) && (nT == 0) ) {
								finished = true;
								fHaComplete = true;
							}
							lStopTime = System.currentTimeMillis();
							lTime = lStopTime - lStartTime;
							lMaxTime = Long.parseLong( getArg( "TIMEOUT_MS" ) );
							if( (lTime > lMaxTime) && !fHaComplete ) {
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( "WAIT_RA_ALIGN", "FWE_DEVICE", "TIMEOUT=" + lTime, "", "", "RA_ALIGN_READY", "FALSE", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
								ergListe.add( result );
								finished = true;
							}

							if( finished ) {
								if( fHaComplete ) {
									status = STATUS_EXECUTION_OK;
									result = new Ergebnis( "WAIT_RA_ALIGN", "FWE_DEVICE", "", "", "", "RA_ALIGN_READY", "TRUE", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
									ergListe.add( result );
								} else {
									status = STATUS_EXECUTION_ERROR;
									result = new Ergebnis( "WAIT_RA_ALIGN", "FWE_DEVICE", "", "", "", "RA_ALIGN_READY", "FALSE", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
									ergListe.add( result );
								}
							}
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 7: // "OSCILLATION_CHECK"
							double minDifference = getArg( "MIN_VALUE" ) != null ? Double.parseDouble( getArg( "MIN_VALUE" ) ) : 1.0;
							lMaxTime = getArg( "TIMEOUT_MS" ) != null ? Long.parseLong( getArg( "TIMEOUT_MS" ) ) : 3000;

							ArrayList<Double> angleValues = new ArrayList<Double>();
							double min = 0.0;
							double max = 0.0;
							double diff = 0.0;

							if( lStartTime == 0 )
								lStartTime = System.currentTimeMillis();

							lTime = System.currentTimeMillis() - lStartTime;

							// record values from steering wheel balancer
							while( lTime <= lMaxTime ) {
								nT = FweDevice.read( "dftLmwValue[Grad]", sbT );

								if( nT == 0 ) {
									try {
										sTemp = sbT.toString();
										sTemp = sTemp.replace( ',', '.' );
										log( debug, "Info", "LMW-Value: " + sTemp + "�" );
										angleValues.add( Double.parseDouble( sTemp ) );
									} catch( Exception e ) {
										log( debug, "Error", "Exception in parseDouble while reading dftLmwValue[Grad]: " + e );
										e.printStackTrace();
									}
								} else
									log( debug, "Error", "Communication error while reading dftLmwValue[Grad]: " + nT );

								// sleep before next loop
								try {
									Thread.sleep( lPauseTime );
								} catch( Exception e ) {
									e.printStackTrace();
								}
								
								lTime = System.currentTimeMillis() - lStartTime;
							}

							// sort recorded values
							Collections.sort( angleValues );
							min = angleValues.get( 0 ) ;
							max = angleValues.get( angleValues.size() - 1);
							
							// round diff to two decimal places
							diff = max - min;
							
							log( debug, "Info", "Maximum angle: " + max + "�, minimum angle: " + min + "�, difference: " + diff + "�");
							
							// check difference
							if( diff <= minDifference ) {
								errorText = lang == 1 ? "Die Differenz zwischen gr��tem und kleinstem Winkelwert ist zu gering." : "The difference between maximum and mimimum angle is too small.";
								log( debug, "Error", errorText + " (" + diff + "� < " + minDifference + "�)" );
								result = new Ergebnis( "OSCILLATION_CHECK", "FWE_DEVICE", "", "", "", "ANGLE_DIFFERENCE_TOO_SMALL", String.format( Locale.ENGLISH, "%.2f", diff ), Double.toString( minDifference ), "", "0", "", "", "", errorText, "", Ergebnis.FT_NIO );
								status = STATUS_EXECUTION_ERROR;
							} else {
								result = new Ergebnis( "OSCILLATION_CHECK", "FWE_DEVICE", "", "", "", "ANGLE_DIFFERENCE", String.format( Locale.ENGLISH, "%.2f", diff ), Double.toString( minDifference ), "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								status = STATUS_EXECUTION_OK;
							}
							
							ergListe.add( result );
							finished = true;
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 8: // "ALLOW_VA"
							nT = FweDevice.write( "fEnableVaCheck[na]", "TRUE" );
							if( nT == 0 ) // wenn write erfolgreich war
							{
								status = STATUS_EXECUTION_OK;
								result = new Ergebnis( "RELEASE_FA_ALIGN", "FWE_DEVICE", "", "", "", "RELEASE_STATUS", "TRUE", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							} else {
								log( debug, "Error", "Pruefprozedur SDiagFwe: fEnableVaCheck: FweDevice.write: " + nT );
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( "RELEASE_FA_ALIGN", "FWE_DEVICE", "", "", "", "RELEASE_STATUS", "FALSE", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
							}
							ergListe.add( result );
							finished = true;
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 9: // "DO_VA"
							sgbd = extractValues( getArg( "SGBD" ) )[0];
							log( debug, "Info", "Loop DO_VA" );

							if( checkExist( getArg( "SGBD_ARGUMENT" ) ) )
								sArgs = getArg( "SGBD_ARGUMENT" );
							else
								sArgs = "";
							if( checkExist( getArg( "SGBD_RESULT" ) ) )
								sResults = getArg( "SGBD_RESULT" );
							else
								sResults = "";
							try {
								if( !fSimSG ) {
									try {
										EdiabasErg = ediabas.executeDiagJob( sgbd, getArg( "SGBD_JOB" ), sArgs, sResults );
									} catch( ApiCallFailedException e ) {
										EdiabasErg = "FAIL";
										sTemp = "DIAG_EXCEPTION";
										log( debug, "Error", "ApiCallFailedException in function executeDiagJob: " + e.getMessage() );
									} catch( Exception e ) {
										EdiabasErg = "FAIL";
										sTemp = "DIAG_EXCEPTION";
										log( debug, "Error", "General exception in job DO_VA (executeDiagJob): " + e.getMessage() );
										e.printStackTrace();
									}
								} else
									EdiabasErg = "OKAY";

								if( EdiabasErg.equalsIgnoreCase( "OKAY" ) ) {
									if( !fSimSG )
										try {
											EdiabasErg = ediabas.getDiagResultValue( sResults );
											nT = FweDevice.write( "dftSummenLenkWinkel[Grad]", EdiabasErg );
										} catch( ApiCallFailedException e ) {
											nLoopErrorCnt++;
											EdiabasErg = "";
											log( debug, "Error", "ApiCallFailedException in function getDiagResultValue: " + e.getMessage() );
										} catch( Exception e ) {
											nLoopErrorCnt++;
											EdiabasErg = "";
											log( debug, "Error", "General exception in job DO_VA (getDiagResultValue): " + e.getMessage() );
											e.printStackTrace();
										}
									else
										EdiabasErg = simLW( adr );
								} else
									nLoopErrorCnt++;
							} catch( Exception e ) {
								log( debug, "Error", "General exception in job DO_VA (execute): " + e.getMessage() );
								e.printStackTrace();
								status = STATUS_EXECUTION_ERROR;
								finished = true;
							}

							if( nLoopErrorCnt > nMaxErrors ) {
								log( debug, "Error", "Too many errors in diagnostic calls: " + nLoopErrorCnt );
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( "PERFORM_FA_ALIGN", "EDIABAS", sgbd, getArg( "SGBD_JOB" ), sArgs, "ERRORCOUNT", Integer.toString( nLoopErrorCnt ), "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
								ergListe.add( result );
								finished = true;

								if( lang == 1 ) {
									cancelProcess( "Zu viele Fehler beim Auslesen des Lenkwinkels �ber Diagnose", "CASCADE bricht den Ablauf jetzt ab", FweDevice );
								} else {
									cancelProcess( "Too many errors while obtaining steering angle via diagnostics", "CASCADE aborts the process now", FweDevice );
								}
							}
							if( (vaComplete( FweDevice )) ) {
								if( (nLoopErrorCnt < nMaxErrors) ) {
									status = STATUS_EXECUTION_OK;
									result = new Ergebnis( "PERFORM_FA_ALIGN", "FWE_DEVICE", "", "", "", "FA_ALIGN_READY", "TRUE", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
									ergListe.add( result );
								}
								finished = true;
							}
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 10: // "ALLOW_HA"
							nT = FweDevice.write( "fEnableHaCheck[na]", "TRUE" );
							if( nT == 0 ) // wenn write erfolgreich war
							{
								status = STATUS_EXECUTION_OK;
								result = new Ergebnis( "RELEASE_RA_ALIGN", "FWE_DEVICE", "", "", "", "RELEASE_STATUS", "TRUE", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							} else {
								log( debug, "Error", "fEnableHaCheck: FweDevice.write: " + nT );
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( "RELEASE_RA_ALIGN", "FWE_DEVICE", "", "", "", "RELEASE_STATUS", "FALSE", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
							}
							ergListe.add( result );
							finished = true;
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 11: // "READ_VARIABLE"
							if( lStartTime == 0 ) {
								// init...
								lStartTime = System.currentTimeMillis();
								lActualTime = lStartTime;
								// CASCADE arguments
								varName = getArg( "VARIABLE" );
								varType = getArg( "TYPE" );
								varMinValue = getArg( "MIN_VALUE" );
								varMaxValue = (getArg( "MAX_VALUE" ) != null ? getArg( "MAX_VALUE" ) : "");
								lTimeoutTime = (getArg( "TIMEOUT_MS" ) != null ? Long.parseLong( getArg( "TIMEOUT_MS" ) ) : 0);
							} else {
								lActualTime = System.currentTimeMillis();
							}
							// check timeout
							if( lActualTime - lStartTime <= lTimeoutTime ) {
								// read variable value...
								sbT.delete( 0, sbT.length() );
								nT = FweDevice.read( varName, sbT );
								varReadValue = sbT.toString().trim();

								// Replace commas in double values by dots
								if( varType.equalsIgnoreCase( "DOUBLE" ) && varReadValue.contains( "," ) ) {
									varReadValue = varReadValue.replaceFirst( ",", "." );
								}

								// check value read...
								if( nT == 0 ) {
									if( valueWithinTol( varType, varMinValue, varMaxValue, varReadValue ) ) {
										// IO
										status = STATUS_EXECUTION_OK;
										result = new Ergebnis( varName, "FWE_DEVICE", "READ", "", "", varName, String.valueOf( varReadValue ), String.valueOf( varMinValue ), String.valueOf( varMaxValue ), "0", "", "", "", "", "", Ergebnis.FT_IO );
										ergListe.add( result );
										log( debug, "Var", "Fwe" + varType + "Variable read. " + varName + " = " + varReadValue + " [min = " + varMinValue + ", max = " + varMaxValue + "]" );
										finished = true;
									} else {
										if( lActualTime == lStartTime )
											log( debug, "Info", "waiting value in tolerance, FweDevice.read(" + varName + "," + varReadValue + ")=" + String.valueOf( nT ) );
									}
								} else {
									// NIO - ReturnStatus!
									log( debug, "Error", "Reading Fwe" + varType + "Variable, return status = " + nT );
								}
							} else {
								// NIO - ValueOutOfTolerance! (with or without
								// timeout)
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( varName, "FWE_DEVICE", "READ", "", "", varName, String.valueOf( varReadValue ), String.valueOf( varMinValue ), String.valueOf( varMaxValue ), "0", "", "", "ValueOutOfTolerance" + (lTimeoutTime > 0 ? "(Timeout = " + lTimeoutTime + " ms)" : ""), "", "", Ergebnis.FT_NIO );
								ergListe.add( result );
								log( debug, "Error", "Reading Fwe" + varType + "Variable, ValueOutOfTolerance" + (lTimeoutTime > 0 ? "(Timeout = " + lTimeoutTime + " ms) " : " ") + varName + " = " + varReadValue + " [min = " + varMinValue + ", max = " + varMaxValue + "]" );
								finished = true;
							}
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 12: // "WRITE_VARIABLE"
							varName = getArg( "VARIABLE" );
							varSetValue = getArg( "SET_VALUE" );

							if( varSetValue.indexOf( "@" ) != -1 ) {
								// @construct value
								try {
									varSetValue = getPPResult( varSetValue );
								} catch( InformationNotAvailableException e ) {
									log( debug, "Error", "Couldn't resolve reference to result '" + varSetValue + "'" );
								}
							}

							nT = FweDevice.write( varName, varSetValue );
							log( debug, "Var", "FweDevice.write(" + varName + "," + varSetValue + ")=" + String.valueOf( nT ) );
							if( nT == 0 ) {
								// IO
								status = STATUS_EXECUTION_OK;
								result = new Ergebnis( varName, "FWE_DEVICE", "WRITE", "", "", varName, varSetValue, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								log( debug, "Var", "FweVariable written. " + varName + " = " + varSetValue );
							} else {
								// NIO - ReturnStatus
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( varName, "FWE_DEVICE", "WRITE", "", "", varName, "", "", "", "0", "", "", "", "FweDevice.write(" + varName + "," + varSetValue + ")=" + String.valueOf( nT ), "", Ergebnis.FT_NIO );
								log( debug, "Error", "Writing FweVariable, return status = " + nT );
							}
							ergListe.add( result );
							finished = true;
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 13: // RESET
							finished = FweDevice.reset();
							if( finished ) {
								log( debug, "Info", "Variables reset" );
								status = STATUS_EXECUTION_OK;
								result = new Ergebnis( "RESET_VARIABLES", "FWE_DEVICE", "", "", "", "RESET_OK", "TRUE", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
							} else
								log( debug, "Error", "Resetting of variables failed!" );
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 14: // ABORT: cancel process
							nT = cancelProcess( getArg( "ERROR_INFO" ), "", FweDevice );
							if( nT != 0 ) {
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( "CASCADE_ABORT", "FWE_DEVICE", "", "", "", "JOB", "abort failed: " + nT, "0", "0", "0", "", "", "", "", "", Ergebnis.FT_NIO );
								ergListe.add( result );
							} else {
								status = STATUS_EXECUTION_OK;
								result = new Ergebnis( "CASCADE_ABORT", "FWE_DEVICE", "", "", "", "JOB", "aborted", "0", "0", "0", "", "", "", "", "", Ergebnis.FT_NIO );
								ergListe.add( result );
							}

							finished = true;
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 15: // READY
							if( (nFweStatus == 0) || (nFweStatus == 4)) {
								FweDevice.reset();
								// set own status to inactive (0)
								System.out.println( "Changing to state 0..." );
								FweDevice.write( "nProcessStateCascade[na]", "0" );
								finished = true;
								status = STATUS_EXECUTION_OK;
								result = new Ergebnis( "CASCADE_READY", "FWE_DEVICE", "", "", "", "READY_STATUS: "+nFweStatus, "TRUE", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
								System.out.println( "Variables set back to default in cascade READY teststep" );
							}
							
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 16: // INIT_FWE
							nT = 0;
							String dblReset = "-999.9";
							String boolReset = "false";
							
							// according to variable list for DUERR v3.3
							String[][] initValues = {
									{ "dftAbsoluteHeightFL", dblReset },
									{ "dftAbsoluteHeightFR", dblReset },
									{ "dftAbsoluteHeightRL", dblReset },
									{ "dftAbsoluteHeightRR", dblReset },
									{ "dftDifferenceHeightPSFL", dblReset },
									{ "dftDifferenceHeightPSFR", dblReset },
									{ "dftDifferenceHeightPSRL", dblReset },
									{ "dftDifferenceHeightPSRR", dblReset },
									{ "dftLmwValue[Grad]", dblReset },
									{ "dftSummenLenkWinkel[Grad]", dblReset },
									{ "fAccSolved[na]", boolReset },
									{ "fAlignment[na]", boolReset },
									{ "fEMComplete", boolReset },
									{ "fEPSEnde", boolReset },
									{ "fEPSStart", boolReset },
									{ "fEnableHaCheck[na]", boolReset },
									{ "fEnableVaCheck[na]", boolReset },
									{ "fHaComplete[na]", boolReset },
									{ "fLmwOk[na]", boolReset },
									{ "fSwSolved[na]", boolReset },
									{ "fVaComplete[na]", boolReset },
									{ "nRepetitionCnt[na]", "0" },
									{ "sCascadeError[na]", "" },
									{ "sCascadeInfo[na]", ""},
									{ "sFahrgestellnummer[na]", "0000000"}
							};
							
							// cycle through variables
							for( int k = 0; k < initValues.length; k++ ) {
								if( nT == 0) {
									nT = FweDevice.write( initValues[k][0], initValues[k][1] );
									try {
										Thread.sleep( 50 );
									} catch( Exception e ) {
										e.printStackTrace();
									}
								} else
									// possibly communication failure
									break;
							}

							if( nT != 0 ) {
								log( debug, "Error", "INIT_FWE FweDevice.write: " + nT );
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( "INIT_FWE", "FWE_DEVICE", "", "", "", "PERFORMED", "FALSE", "0", "0", "0", "", "", "", "The variables of the interface could not be reset.", "", Ergebnis.FT_NIO );
							} else {
								status = STATUS_EXECUTION_OK;
								result = new Ergebnis( "INIT_FWE", "FWE_DEVICE", "", "", "", "PERFORMED", "TRUE", "0", "0", "0", "", "", "", "", "", Ergebnis.FT_IO );
							}
							
							ergListe.add( result );
							finished = true;
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 17: // "DO_VA_HA"
							sgbd = extractValues( getArg( "SGBD" ) )[0];
							log( debug, "Info", "Loop DO_VA_HA..." );

							if( checkExist( getArg( "SGBD_ARGUMENT" ) ) )
								sArgs = getArg( "SGBD_ARGUMENT" );
							else
								sArgs = "";
							if( checkExist( getArg( "SGBD_RESULT" ) ) )
								sResults = getArg( "SGBD_RESULT" );
							else
								sResults = "";
							try {
								if( !fSimSG ) {
									try {
										EdiabasErg = ediabas.executeDiagJob( sgbd, getArg( "SGBD_JOB" ), sArgs, sResults );
									} catch( ApiCallFailedException e ) {
										EdiabasErg = "FAIL";
										sTemp = "DIAG_EXCEPTION";
										log( debug, "Error", "ApiCallFailedException in SDiagFwe execute " + sJobId + " " + e.getMessage() );
									}
								} else
									EdiabasErg = "OKAY";

								if( EdiabasErg.equalsIgnoreCase( "OKAY" ) ) {
									if( !fSimSG )
										try {
											EdiabasErg = ediabas.getDiagResultValue( sResults );

											// Lenkwinkelwert invertieren, um die
											// Anzeige bei Bosch anzupassen
											// (Lenkrad nach links -> Winkelanzeige
											// nach links)
											sLWinverse = EdiabasErg;
											log( debug, "Info", "sLWinverse = " + sLWinverse );
											fLWinverse = Float.valueOf( sLWinverse ).floatValue();
											fLWinverse *= -1;
											log( debug, "Info", "sLWinverse = " + fLWinverse );

											nT = FweDevice.write( "dftLenkwinkel", Float.toString( fLWinverse ) );
											// sbR.toString());
										} catch( ApiCallFailedException e ) {
											nLoopErrorCnt++;
											EdiabasErg = "";
											log( debug, "Error", "ApiCallFailedException in SDiagFwe execute " + sJobId + " getDiagResultValue " + e.getMessage() );
										} catch( NumberFormatException e ) {
											nLoopErrorCnt++;
											EdiabasErg = "";
											log( debug, "Error", "NumberFormatException in SDiagFwe execute " + sJobId + " invert steering angle " + e.getMessage() );
										}
									else
										EdiabasErg = simLW( adr );
								} else
									nLoopErrorCnt++;
							} catch( Exception e ) {
								log( debug, "Error", "Exception while DO_VA_HA: " + e.getMessage() );
								status = STATUS_EXECUTION_ERROR;
								finished = true;
							}

							if( nLoopErrorCnt > nMaxErrors ) {
								log( debug, "Error", "Too many DiagErrors: " + nLoopErrorCnt );
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( "PERFORM_FA_RA_ALIGN", "EDIABAS", sgbd, getArg( "SGBD_JOB" ), sArgs, "ERRORCOUNT", Integer.toString( nLoopErrorCnt ), "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
								ergListe.add( result );
								finished = true;
								cancelProcess( PB.getString( "diagerrorreadsummenlenkwinkel" ), PB.getString( "cascadeprocescancelled" ), FweDevice );
							}
							if( (vaHaComplete( FweDevice )) ) {
								if( (nLoopErrorCnt < nMaxErrors) ) {
									status = STATUS_EXECUTION_OK;
									result = new Ergebnis( "PERFORM_FA_RA_ALIGN", "FWE_DEVICE", "", "", "", "FA_RA_ALIGN_READY", "TRUE", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
									ergListe.add( result );
								}
								finished = true;
							}
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 18: // "EHC_HOEHENSTANDSABGLEICH"
							sbT.delete( 0, sbT.length() );
							nT = FweDevice.read( "dftDifferenceHeightRR[mm]", sbT );

							float dftDeltaHintenRechts = 9999f;
							float dftDeltaHintenLinks = 9999f;

							try {
								if( nT == 0 )
									dftDeltaHintenRechts = Float.parseFloat( sbT.toString() );
								System.out.println( "Hoehenstand Hinten Rechts: " + dftDeltaHintenRechts );
							} catch( Exception e ) {
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( "EHC_HOEHENSTANDSABGLEICH", "FWE_DEVICE", "", "", "", "HOEHENSTAND_RECHTS", "LESEFEHLER: " + nT, "0", "0", "0", "", "", "", "", "", Ergebnis.FT_NIO );
								ergListe.add( result );
							}

							sbT.delete( 0, sbT.length() );
							nT = FweDevice.read( "dftDifferenceHeightRL[mm]", sbT );

							try {
								if( nT == 0 )
									dftDeltaHintenLinks = Float.parseFloat( sbT.toString() );
								System.out.println( "Hoehenstand Hinten Links: " + dftDeltaHintenLinks );
							} catch( Exception e ) {
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( "EHC_HOEHENSTANDSABGLEICH", "FWE_DEVICE", "", "", "", "HOEHENSTAND_LINKS", "LESEFEHLER: " + nT, "0", "0", "0", "", "", "", "", "", Ergebnis.FT_NIO );
								ergListe.add( result );
							}

							String sSgbd = extractValues( getArg( "SGBD" ) )[0];
							String sDiagJob = new String( getArg( "SGBD_JOB" ) );
							String sDiagResult = new String( "job_status;_tel_antwort" );

							String sDiagArg = new String( String.valueOf( Math.round( dftDeltaHintenLinks ) ) + ";" + String.valueOf( Math.round( dftDeltaHintenRechts ) ) + ";0" );

							sTemp = ediabas.executeDiagJob( sSgbd, sDiagJob, sDiagArg, sDiagResult );

							System.out.println( "Diagnose zur H�henstandsabgleich. SGBD: " + sSgbd + " Job: " + sDiagJob + " Arg: " + sDiagArg + "Result: " + sTemp );

							if( sTemp.equalsIgnoreCase( "OKAY" ) ) {
								// IO
								status = STATUS_EXECUTION_OK;
								result = new Ergebnis( "EHC_HOEHENSTANDSABGLEICH", "FWE_DEVICE", sSgbd, sDiagJob, sDiagArg, "STATUS", sTemp, "", "", "0", "", "", "", "", "Hoehenstandsabgleich war iO", Ergebnis.FT_IO );
							} else {
								// NIO
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( "EHC_HOEHENSTANDSABGLEICH", "FWE_DEVICE", sSgbd, sDiagJob, sDiagArg, "STATUS", sTemp, "", "", "0", "", "", "", "", "Hoehenstandsabgleich war niO", Ergebnis.FT_NIO );
							}
							ergListe.add( result );

							finished = true;
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 19: // DISCONNECT
							try {
								FweDevice.close();
								finished = true;
								status = STATUS_EXECUTION_OK;
								result = new Ergebnis( "DISCONNECT", "FWE_DEVICE", "", "", "", "DISCONNECT", "TRUE", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
							} catch( IOException e ) {
								log( debug, "Error", "Close()-Method failed!" + e.getMessage() );
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( "DISCONNECT", "FWE_DEVICE", "", "", "", "DISCONNECT", "FALSE", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
								ergListe.add( result );
							}
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 20: // LOGGING
							try {
								varSetValue = getArg( "SET_VALUE" );
								FweDevice.logging( varSetValue.equalsIgnoreCase( "TRUE" ) );
								varSetValue = (varSetValue.equalsIgnoreCase( "TRUE" ) ? "on" : "off");
								log( true, "Info", "Logging turned " + varSetValue + "." );
								finished = true;
								status = STATUS_EXECUTION_OK;
								result = new Ergebnis( "LOGGING", "FWE_DEVICE", "", "", "", "LOGGING", "TRUE", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
							} catch( Exception e ) {
								log( debug, "Error", "Logging()-Method failed!" + e.getMessage() );
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( "LOGGING", "FWE_DEVICE", "", "", "", "LOGGING", "FALSE", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
								ergListe.add( result );
							}
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 21: // "HEIGHT_CURVE_RECORD"
							String heightValueResultFL = new String();
							String heightValueResultFR = new String();
							String heightValueResultRL = new String();
							String heightValueResultRR = new String();

							sgbd = extractValues( getArg( "SGBD" ) )[0];
							log( debug, "Info", "Loop HEIGHT_CURVE_RECORD" );

							sArgs = getArg( "SGBD_ARGUMENT" );

							if( sArgs == null || sArgs.length() == 0 ) {
								throw new PPExecutionException( "Parameter SGBD_ARGUMENT must not be empty!" );
							}

							heightValueResultFL = extractValues( getArg( "SGBD_RESULT" ) )[0];
							heightValueResultFR = extractValues( getArg( "SGBD_RESULT" ) )[1];
							heightValueResultRL = extractValues( getArg( "SGBD_RESULT" ) )[2];
							heightValueResultRR = extractValues( getArg( "SGBD_RESULT" ) )[3];

							try {
								if( !fSimSG ) {
									try {
										EdiabasErg = ediabas.executeDiagJob( sgbd, getArg( "SGBD_JOB" ), sArgs, sResults );
									} catch( ApiCallFailedException e ) {
										EdiabasErg = "FAIL";
										sTemp = "DIAG_EXCEPTION";
										log( debug, "Error", "ApiCallFailedException in function executeDiagJob: " + e.getMessage() );
									} catch( Exception e ) {
										EdiabasErg = "FAIL";
										sTemp = "DIAG_EXCEPTION";
										log( debug, "Error", "General exception in job HEIGHT_CURVE_RECORD (executeDiagJob): " + e.getMessage() );
										e.printStackTrace();
									}
								} else
									EdiabasErg = "OKAY";

								if( EdiabasErg.equalsIgnoreCase( "OKAY" ) ) {
									if( !fSimSG )
										try {
											/** Cycle through all height values and write them to the WAM rig */
											if( !heightValueResultFL.equalsIgnoreCase( "null" ) ) {
												EdiabasErg = ediabas.getDiagResultValue( heightValueResultFL );
												nT = FweDevice.write( "dftCrvRecVoltageFL", EdiabasErg );
											}
											if( !heightValueResultFR.equalsIgnoreCase( "null" ) ) {
												EdiabasErg = ediabas.getDiagResultValue( heightValueResultFR );
												nT = FweDevice.write( "dftCrvRecVoltageFR", EdiabasErg );
											}

											if( !heightValueResultRL.equalsIgnoreCase( "null" ) ) {
												EdiabasErg = ediabas.getDiagResultValue( heightValueResultRL );
												nT = FweDevice.write( "dftCrvRecVoltageRL", EdiabasErg );
											}

											if( !heightValueResultRR.equalsIgnoreCase( "null" ) ) {
												EdiabasErg = ediabas.getDiagResultValue( heightValueResultRR );
												nT = FweDevice.write( "dftCrvRecVoltageRR", EdiabasErg );
											}
										} catch( ApiCallFailedException e ) {
											nLoopErrorCnt++;
											EdiabasErg = "";
											log( debug, "Error", "ApiCallFailedException in function getDiagResultValue: " + e.getMessage() );
										} catch( Exception e ) {
											nLoopErrorCnt++;
											EdiabasErg = "";
											log( debug, "Error", "General exception in job HEIGHT_CURVE_RECORD (getDiagResultValue): " + e.getMessage() );
											e.printStackTrace();
										}
									else
										EdiabasErg = simLW( adr );
								} else
									nLoopErrorCnt++;
							} catch( Exception e ) {
								log( debug, "Error", "General exception in job HEIGHT_CURVE_RECORD (execute): " + e.getMessage() );
								e.printStackTrace();
								status = STATUS_EXECUTION_ERROR;
								finished = true;
							}

							if( nLoopErrorCnt > nMaxErrors ) {
								log( debug, "Error", "Too many errors in diagnostic calls: " + nLoopErrorCnt );
								status = STATUS_EXECUTION_ERROR;
								result = new Ergebnis( "HEIGHT_CURVE_RECORD", "EDIABAS", sgbd, getArg( "SGBD_JOB" ), sArgs, "ERRORCOUNT", Integer.toString( nLoopErrorCnt ), "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
								ergListe.add( result );
								finished = true;

								if( lang == 1 ) {
									cancelProcess( "Zu viele Fehler beim Auslesen der H�henst�nde �ber Diagnose", "CASCADE bricht den Ablauf jetzt ab", FweDevice );
								} else {
									cancelProcess( "Too many errors while obtaining height values via diagnostics", "CASCADE aborts the process now", FweDevice );
								}
							}
							if( (heightCurveComplete( FweDevice )) ) {
								if( (nLoopErrorCnt < nMaxErrors) ) {
									status = STATUS_EXECUTION_OK;
									result = new Ergebnis( "HEIGHT_CURVE_RECORD", "FWE_DEVICE", "", "", "", "Record complete", "TRUE", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
									ergListe.add( result );
								}
								finished = true;
							}
							break;
						// -----------------------------------------------------------------------------------------------------------------------------
						case 0: // NO_JOB
						default:
							finished = true;
							status = STATUS_EXECUTION_ERROR;
							result = new Ergebnis( "PPExecJob", "PU", "JOB_ID", "", "", "JobId", "" + nJobId, "[1..21]", "", "0", "", "", "", "Parameter Error", "Invalid Job; modify Pruefling", Ergebnis.FT_NIO );
							ergListe.add( result );
					} // switch(nJobId)

				} catch( Exception e ) {
					log( debug, "Error", "Exception during job execution in SDiagFwe. Job: " + sJobId + ". Exception: " + (e.getMessage() != null ? e.getMessage() : "") );
					e.printStackTrace();
					finished = true;
					status = STATUS_EXECUTION_ERROR;
					result = new Ergebnis( "PPExecJob", "PU", "", "", "", "JobId", "" + nJobId, "1,[3..6],[8..21]", "", "0", "", "", "", "Exception in job: " + (e.getMessage() != null ? e.getMessage() : ""), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					if( lang == 1 ) {
						cancelProcess( "Abbruch nach CASCADE Exception in SDiagFwe w�hrend Job-Bearbeitung: " + e.getMessage(), "Job: " + sJobId, FweDevice );
					} else {
						cancelProcess( "Abort after CASCADE exception in SDiagFwe during job execution: " + e.getMessage(), "Job: " + sJobId, FweDevice );
					}

				}

				// genereller sleep, um Rechnerlast bei zyklischen Jobs zu
				// drosseln und Ediabas nicht zu �berlasten
				if( !finished && !isCancelled( uiDlg ) ) {
					try {
						Thread.sleep( lPauseTime );
					} catch( InterruptedException e ) {
					}
				}

				// Benutzer Abbruch innerhalb der UserDialogs behandeln
				if( isCancelled( uiDlg ) ) {
					String sMsg = lang == 1 ? new String( "Benutzerabbruch" ) : new String( "User abort" );
					System.out.println( sMsg );
					status = STATUS_EXECUTION_ERROR;
					result = new Ergebnis( "USER_ABORT", "FWE_DEVICE", "", "", "", "JOB", "aborted", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
					ergListe.add( result );
					finished = true;
					// Wenn es der DO_VA_HA-Bosch-KDS job ist, nicht beenden, da
					// hiermit nur der RR2/3 wieder hochgefahren wird, bevor die
					// Ausgangsmessung erfolgt
					if( nJobId != 26 )
						cancelProcess( sMsg, "", FweDevice );
				}
			} // Ende der While-Schleife

		} catch( PPExecutionException e ) {
			log( debug, "Error", "Execution Exception in SDiagFwe.execute: " + e.getMessage() );
			e.printStackTrace();
			finished = true;
		} catch( Exception e ) {
			log( debug, "Error", "General Exception in SDiagFwe.execute " + e.getMessage() );
			e.printStackTrace();
			status = STATUS_EXECUTION_ERROR;
			result = new Ergebnis( "EXEC_ERROR", "", "", "", "", "", "", "", "", "0", "", "", "", "GeneralException", PB.getString( "unerwarteter Laufzeitfehler" ), Ergebnis.FT_NIO );
			ergListe.add( result );
			
			finished = true;
			if( lang == 1 ) {
				cancelProcess( "Abbruch nach CASCADE Exception in SDiagFwe: " + e.getMessage(), "Job: " + getArg( "JOB_ID" ), FweDevice );
			} else {
				cancelProcess( "Abort after CASCADE exception in SDiagFwe: " + e.getMessage(), "Job: " + getArg( "JOB_ID" ), FweDevice );

			}

		} finally {
			if( uiDlg != null ) {
				try {
					uiDlg.close();
					getPr�flingLaufzeitUmgebung().releaseUserDialog();
					uiDlg = null;
				} catch( IOException e ) {
					System.out.println( "IOException while releasing UserDialog: " + e.getMessage() );
				} catch( DeviceLockedException e ) {
					System.out.println( "DeviceLockedException while releasing UserDialog: " + e.getMessage() );
				}
			}

			try {
				this.getPr�flingLaufzeitUmgebung().getDeviceManager().releaseFweStand( fweStandTag );
			} catch( DeviceLockedException e ) {
				log( debug, "Error", "DeviceLockedException while accessing FweStandDevice(" + fweStandTag + ")" );
			}
		}

		if( uiDlg != null ) {
			try {
				uiDlg.close();
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
				uiDlg = null;
			} catch( IOException e ) {
				System.out.println( "IOException while releasing UserDialog: " + e.getMessage() );
			} catch( DeviceLockedException e ) {
				System.out.println( "DeviceLockedException while releasing UserDialog: " + e.getMessage() );
			}
		}

		setPPStatus( info, status, ergListe );
	} // execute(....

	/***************************************************************************
	 * Utility method: getFweDev: get FweStandDevice from Devicemanager with the
	 * given device tag. If this fails trys to initialize the FweDevice from
	 * AnyFweStandDevice.
	 * 
	 * @return the FweDevice or null if it is not available
	 */
	private FweStand getFweDev( String fweStandTag ) {
		boolean debug = (getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
		FweStand mFweDevice = null;
		/** Device f�r Kommunikation zum FWE-Stand */
		try {
			log( debug, "Info", "Trying to get FweDevice(" + fweStandTag + ") from DeviceManager..." );
			mFweDevice = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getFweStand( fweStandTag );
			if( mFweDevice == null ) {
				log( debug, "Error", "Failed to get FweDevice(" + fweStandTag + ")" );
			} else
				log( debug, "Info", "Got FweDevice(" + fweStandTag + ") from DeviceManager" );
		} catch( DeviceLockedException e ) {
			log( debug, "Error", "DeviceLockedException during getFweStand(" + fweStandTag + ")" );
		} catch( DeviceNotAvailableException e ) {
			log( debug, "Error", "NotAvailableException during getFweStand(" + fweStandTag + ")" );
		} catch( Exception e ) {
			log( debug, "Error", "General Exception during getFweStand(" + fweStandTag + ")" + e.getMessage() );
			// throw new DeviceNotAvailableException( e.getMessage() );
		}

		if( mFweDevice == null ) {
			try {
				System.out.println( "FweDevice(" + fweStandTag + ") not delivered by DeviceManager; try instantiantion of AnyFweStandDevice()..." );
				mFweDevice = new com.bmw.cascade.pruefstand.devices.fwestand.AnyFweStandDevice();
			} catch( DeviceNotAvailableException e ) {
				System.out.println( "Device not available: FweDevice(" + fweStandTag + "): " + e.getMessage() );
			}
		}

		return mFweDevice;
	}

	/***************************************************************************
	 * Utility method: getJobId: return an int represantation of a textual job
	 * id.
	 * 
	 * @param sJobId
	 *            is the textual representation of a job id
	 * @return the int representation of the job id given by the paramter
	 *         sJobId; if sJobId is invalid, 0 will be returned
	 */
	private int getJobId( String sJobId ) {
		int nId = 0;
		boolean debug = (getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;

		/** list of valid JobIds */
		if( hJobIds == null ) {
			hJobIds = new Hashtable<String, Integer>();
			hJobIds.put( "NO_ID", new Integer( 0 ) );
			hJobIds.put( "CHECK_WDH_EINST", new Integer( 1 ) );
			hJobIds.put( "CHECK_MEASUREMENT", new Integer( 2 ) );
			hJobIds.put( "ALLOW_SW", new Integer( 3 ) );
			hJobIds.put( "WAIT_LMW", new Integer( 4 ) );

			hJobIds.put( "WAIT_VA_COMPLETE", new Integer( 5 ) );
			hJobIds.put( "WAIT_HA_COMPLETE", new Integer( 6 ) );
			hJobIds.put( "OSCILLATION_CHECK", new Integer( 7 ) );
			hJobIds.put( "ALLOW_VA", new Integer( 8 ) );
			hJobIds.put( "DO_VA", new Integer( 9 ) );

			hJobIds.put( "ALLOW_HA", new Integer( 10 ) );
			hJobIds.put( "READ_VARIABLE", new Integer( 11 ) );
			hJobIds.put( "WRITE_VARIABLE", new Integer( 12 ) );
			hJobIds.put( "RESET", new Integer( 13 ) );
			hJobIds.put( "ABORT", new Integer( 14 ) );

			hJobIds.put( "READY", new Integer( 15 ) );
			hJobIds.put( "INIT_FWE", new Integer( 16 ) );
			hJobIds.put( "DO_VA_HA", new Integer( 17 ) );
			hJobIds.put( "EHC_HOEHENSTANDSABGLEICH", new Integer( 18 ) );
			hJobIds.put( "DISCONNECT", new Integer( 19 ) );
			
			hJobIds.put( "LOGGING", new Integer( 20 ) );
			hJobIds.put( "HEIGHT_CURVE_RECORD", new Integer( 21 ) );
		}
		Integer i = (Integer) hJobIds.get( sJobId );
		if( i != null )
			nId = i.intValue();

		log( debug, "Info", "JobId for " + sJobId + " is: " + nId );
		return nId;
	}

	/***************************************************************************
	 * Utility method: cancelProcess: set own proces state to 4 to signal
	 * 'cancel' to FWE-Stand; then wait for the FWE-Stand to transition to state
	 * 0 (inavtice); finally set own proces state to 0 (inavtice); if FWE-Stand
	 * is already in state 4, then set own state immediately to 0.
	 * 
	 * @param ErrorText
	 *            String in opt. argument ERROR_TEXT; will be transmitted to
	 *            FWE-Stand for displaying error info if not null
	 * @param InfoText
	 *            String will be transmitted to FWE-Stand for displaying user
	 *            info if not null and positive length
	 * @return return code of FweDevice write method used for
	 *         mFweDevice.write("nProcessStateCascade[na]", "0");
	 */
	private int cancelProcess( String ErrorText, String InfoText, FweStand FweDevice ) {
		UserDialog uiDlg = null;
		int nT = 0;
		int lang = System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) ? 1 : 0; //1: German, 0: English
		boolean debug = (getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;

		try {

			// Error text uebertragen
			if( ErrorText != null )
				FweDevice.write( "sCascadeError[na]", ErrorText );
			if( InfoText != null )
				if( InfoText.length() > 0 )
					FweDevice.write( "sCascadeInfo[na]", InfoText );

			// neuen Dialog anzeigen
			if( (uiDlg == null) && (getFweStatus( FweDevice ) != 4) ) {
				uiDlg = getPr�flingLaufzeitUmgebung().getUserDialog();
				if( lang == 1 ) {
					uiDlg.displayMessage( "Abbruch durch CASCADE", "Ablauf durch Taste;GRUNDSTELLUNG;am FWE-Stand beenden!", -1 );
				} else {
					uiDlg.displayMessage( "Abort by CASCADE", "End process by pressing;HOME POSITION;on wheel aligner!", -1 );
				}
			}

				FweDevice.write( "nProcessStateCascade[na]", "4" );
				log( debug, "Info", "FWE-Status set to 4 in cancelProcess!");

				
			//Warte bis FWE Statuswechsel auf 0 oder 4 erfolgt
			
				while( (getFweStatus( FweDevice ) != 4) && (getFweStatus( FweDevice ) != 0)){
					Thread.sleep( 300 );
					}
				

		
			// User Dialog schliessen
			if( uiDlg != null ) {
				uiDlg.close();
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
				uiDlg = null;
			}
		} catch( Exception e ) {
			log( true, "Error", "Exception during cancel process: " + e.getMessage() );
			e.printStackTrace();
		}

		return nT;
	}

	/***************************************************************************
	 * Utility method: isCancelled Pr�ft den �bergebenen Userdialog auf einen
	 * Cancel/Abbruch Button Falls vorhanden, fragt er, ob dieser bet�tigt wurde
	 * 
	 * @return 'true' wenn ein Cancel Button bet�tigt wurde, sonst false
	 */
	private boolean isCancelled( UserDialog uiDlg ) {
		boolean fRet = false;
		if( uiDlg != null ) {
			if( uiDlg.isCancelled() )
				fRet = true;
		}
		return fRet;
	}

	/***************************************************************************
	 * Utility method: getFweStatus Read the status FWE variable from the
	 * FWE-Stand and check it
	 * 
	 * @return value of proces state of FWE-Stand [0..5] or < 0 if failed
	 */
	private int getFweStatus( FweStand FweDevice ) {
		int nRet = 0;
		int nFweState = -1;
		StringBuffer sVarResult = new StringBuffer();

		try {
			nRet = FweDevice.read( "nProcessStateFweStand[na]", sVarResult );

			if( nRet == 0 ) {
				nFweState = Integer.parseInt( sVarResult.toString() );
			} else
				nFweState = nRet; // -1: Variable nicht def.; -2 Keine
			// Verbindung
		} catch( Exception e ) {
			System.out.println( "Exception while reading FweProcesState in SDiagFwe: " + e.getMessage() );
			e.printStackTrace();
		}
		// if(DEBUG) System.out.println("FweProcesState == "+nFweState);
		return nFweState;
	}

	/***************************************************************************
	 * Utility method: Simulation der Summenwinkelwerte F�r einfache Simulation
	 * eines gleitenden Analogwertes. Hier f�r Simulation des
	 * Summenlenkwinkelwertes benutzt
	 * 
	 * @return Wert als String im Format "[-]f.f"
	 */
	private String simLW( int adr ) {
		String sResult = new String();
		double[] slwValueArray = new double[] { -2.1, -2.0, -1.9, -1.8, -1.7, -1.6, -1.5, -1.4, -1.3, -1.2, -1.1, -1.0, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1 };
		if( adr != 0 && adr != 41 ) {
			Random rand = new Random();
			int zufall = rand.nextInt( 1 );
			if( zufall == 0 )
				adr = adr - 1;
			else if( zufall == 1 )
				adr = adr + 1;
			sResult = Double.toString( slwValueArray[adr] ); // ""+new
			// Float(slwValueArray[adr]).floatValue();
		} else if( adr == 0 ) {
			sResult = Double.toString( slwValueArray[adr++] );
		} else if( adr == 41 ) {
			sResult = Double.toString( slwValueArray[adr--] );
		}
		// auf alle Faelle Wartezeit bei der Simulation (Diagnose-Laufzeit +
		// Plausibilit�t bez. Veraenderung)
		try {
			Thread.sleep( 500 );
		} catch( InterruptedException e ) {
			sResult = "0.5";
		}

		return sResult;
	}

	/***************************************************************************
	 * Utility method: VaComplete Fragt den FWE-Stand nach dem Status der
	 * Vorderachseinstellung ab
	 * 
	 * @return true, wenn VA Einstellung abgeschlossen, sonst false
	 */
	private boolean vaComplete( FweStand FweDevice ) {
		boolean fVaComplete = false;
		StringBuffer sbT = new StringBuffer();
		try {
			FweDevice.read( "fVaComplete[na]", sbT );
			if( sbT.toString().equalsIgnoreCase( "TRUE" ) )
				fVaComplete = true;
		} catch( Exception e ) {
			System.out.println( "Exception while requesting 'fVaComplete[na]' from WAM rig: " + e.getMessage() );
		}
		return fVaComplete;
	}

	/***************************************************************************
	 * Utility method: VaHaComplete fragt den FWE-Stand nach dem Status der
	 * Vorder- und Hinterachseinstellung ab (nur f�r Bosch KDS)
	 * 
	 * @return true, wenn VA- und HA-Einstellung abgeschlossen, sonst false
	 */
	private boolean vaHaComplete( FweStand FweDevice ) {
		boolean fVaHaComplete = false;
		StringBuffer sbT = new StringBuffer();
		try {
			FweDevice.read( "fAdjustmentComplete", sbT );
			if( sbT.toString().equalsIgnoreCase( "TRUE" ) )
				fVaHaComplete = true;
		} catch( Exception e ) {
			System.out.println( "Exception while requesting 'fAdjustmentComplete' from WAM rig: " + e.getMessage() );
		}
		return fVaHaComplete;
	}

	/***************************************************************************
	 * Pr�ft den �bergebenen String und gibt 'true' zur�ck, wenn der Inhalt
	 * einen positiven 'long'-Wert darstellt
	 * 
	 * @param sT
	 *            String zu pr�fender String
	 * @return true wenn der String sT einen positiven 'long' Wert darstellt,
	 *         sonst 'false'
	 */
	private boolean isPosLong( String sT ) {
		boolean fRet = true;
		try {
			if( Long.parseLong( sT ) < 0 )
				fRet = false;
		} catch( Exception e ) {
			fRet = false;
		}
		return fRet;
	}

	/***************************************************************************
	 * Utility method: CheckExist pr�ft �bergebenen String auf Existenz
	 * 
	 * @param sT
	 *            zu pr�fender String
	 * @return 'true' wenn zu pr�fender String existent ist
	 */
	private boolean checkExist( String sT ) {
		boolean fRet = false;

		if( sT != null ) {
			if( sT.length() > 0 )
				fRet = true;
		}
		return fRet;
	}

	/**
	 * <b>log</b> <br>
	 * Schreibt eine Nachricht ins CASCADE-Log, wenn das Argument DEBUG gesetzt
	 * ist oder es sich um einen Fehler handelt. <br>
	 * 
	 * @param debug
	 *            - Debug-Argument
	 * @param type
	 *            - Nachrichtentyp
	 * @param text
	 *            - Nachrichtentext
	 */
	private void log( boolean debug, String type, String text ) {
		if( type.equalsIgnoreCase( "Error" ) || debug ) {
			System.out.println( "[" + getLocalName() + "] [" + type + "] " + text );
		}
	}

	/***************************************************************************
	 * Utility method: MustShowUserDlg Pr�ft die opt. Argumente 'MESSAGE' und
	 * 'ALLOW_CANCEL' auf Existenz und korrekten Inhalt
	 * 
	 * @return true wenn beide Argumente vorhanden und korrekt sind, sonst false
	 */
	private boolean mustShowUserDlg() {
		boolean fShowUserDlg = false;
		if( checkExist( getArg( "MESSAGE" ) ) && checkExist( getArg( "ALLOW_CANCEL" ) ) ) {
			String sT = new String();
			sT = getArg( "ALLOW_CANCEL" );
			if( ((sT.equalsIgnoreCase( "FALSE" )) || (sT.equalsIgnoreCase( "TRUE" ))) )
				fShowUserDlg = true;
		}
		return fShowUserDlg;
	}

	/**
	 * valueWithinTol
	 * 
	 * @param varType
	 * @param varValue
	 * @param varMinValue
	 * @param varMaxValue
	 * @return
	 */
	private boolean valueWithinTol( String varType, String varMinValue, String varMaxValue, String varValue ) {
		return (
		// type BOOLEAN
		(varType.equalsIgnoreCase( "BOOLEAN" ) && (varValue.equalsIgnoreCase( varMinValue ))) ||
		// type SHORT
		(varType.equalsIgnoreCase( "SHORT" ) && (new Short( varValue ).shortValue() >= new Short( varMinValue ).shortValue() && new Short( varValue ).shortValue() <= new Short( varMaxValue ).shortValue())) ||
		// type LONG
		(varType.equalsIgnoreCase( "LONG" ) && (new Long( varValue ).longValue() >= new Long( varMinValue ).longValue() && new Long( varValue ).longValue() <= new Long( varMaxValue ).longValue())) ||
		// type DOUBLE
		(varType.equalsIgnoreCase( "DOUBLE" ) && (new Double( varValue ).doubleValue() >= new Double( varMinValue ).doubleValue() && new Double( varValue ).doubleValue() <= new Double( varMaxValue ).doubleValue())) ||
		// type STRING
		(varType.equalsIgnoreCase( "STRING" ) && (varValue.equalsIgnoreCase( varMinValue ))));
	}

	/***************************************************************************
	 * Utility method: HeightCurveComplete fragt den FWE-Stand nach dem Status der
	 * H�henstandskurve
	 * 
	 * @return true, wenn Aufzeichnung abgeschlossen, sonst false
	 */
	private boolean heightCurveComplete( FweStand FweDevice ) {
		boolean fCurveRecComplete = false;
		StringBuffer sbT = new StringBuffer();
		try {
			FweDevice.read( "fCurveRecComplete", sbT );
			if( sbT.toString().equalsIgnoreCase( "TRUE" ) )
				fCurveRecComplete = true;
		} catch( Exception e ) {
			System.out.println( "Exception while requesting 'fCurveRecComplete' from WAM rig: " + e.getMessage() );
		}
		return fCurveRecComplete;
	}
}

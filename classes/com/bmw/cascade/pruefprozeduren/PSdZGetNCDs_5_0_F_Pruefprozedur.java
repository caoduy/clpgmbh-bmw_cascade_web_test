package com.bmw.cascade.pruefprozeduren;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.psdz.PSdZ;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.CascadePSdZArgumentException;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.CascadePSdZConditionsNotCorrectException;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.CascadePSdZRuntimeException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementierung einer Pr�fprozedur, die die NCDs ermittelt (auf Basis einer include Liste).
 * 
 * @author Buboltz, BMW AG; Gampl, BMW AG <BR>
 *         28.03.2011 TB Ersterstellung auf Basis PSdZGetSVTs, erster Test <BR>
 *         01.12.2011 AN Erweiterung Paltzhalter Includeliste * <BR>
 *         07.10.2014 MG Generics sowie weitere Debugausgaben hinzu <BR>
 *         20.10.2014 MG F-Version <BR>
 */
public class PSdZGetNCDs_5_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSdZGetNCDs_5_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSdZGetNCDs_5_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG", "DEBUG_PERFORM" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = { "VEHICLE_STATE_INCLUDE_LIST" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		if( !super.checkArgs() )
			return false;
		return true;
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		PSdZ psdz = null;
		UserDialog userDialog = null;

		String[] vehicleStateIncludeList = null;

		// Mittels eines '*' beim ersten Element in der vehicleStateIncludeList
		// werden in der jeweils anderen Liste alle ECUs eingetragen ohne die
		// ECUs in der List wo kein Platzhalter steht und ohne die ECUs
		// aus der IGNORE_LIST.
		ArrayList<String> maskList = new ArrayList<String>();
		char cPlatzhalter = 'n'; // Verwaltung des '*' Platzhalters:
		// 'n'-nichts oder ohne Platzhalter, 'i'-include Liste hat Platzhalter, 'e'-exclude Liste hat Platzhalter df- funkt. Adr neues Bordnetz; ef-funkt. Adr altes Bordnetz; f1, f4, f5 Testeradressen
		final String[] STAR_IGNORE_LIST = { "df", "ef", "f1", "f4", "f5" }; // IGNORE
		// Liste f�r die Platzhalter-Verwendung, hier k�nnen z.B. die funktionalen ECU Adressen stehen, ACHTUNG: hier z.B. nur "ef" und nicht "EF" eintragen
		final List<String> STAR_IGNORE_LIST_AS_LIST = Arrays.asList( STAR_IGNORE_LIST );

		String ncdTargetDirectory = com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.logDirectory" );
		String testScreenID = this.getPr�flingLaufzeitUmgebung().getName(); // TestScreenID (aus Pr�fstandsinstanz)
		boolean bIsBlacklist = false; // gibt an, ob es eine Blacklist ist
		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
		boolean bDebugPerform = true; // gibt an, ob eine Performencemessung erfolgen sollen

		try {
			// Argumente einlesen und checken
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( !getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DEBUG_PERFORM
				if( getArg( "DEBUG_PERFORM" ) != null ) {
					if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "TRUE" ) )
						bDebugPerform = true;
					else if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "FALSE" ) )
						bDebugPerform = false;
					else if( !getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "FALSE" ) )
						throw new PPExecutionException( "DEBUG_PERFORM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// VEHICLE_STATE_INCLUDE_LIST

				if( getArg( "VEHICLE_STATE_INCLUDE_LIST" ) != null ) {

					vehicleStateIncludeList = extractValues( getArg( "VEHICLE_STATE_INCLUDE_LIST" ) );

					// includeList mit '*' ?
					if( vehicleStateIncludeList[0].equalsIgnoreCase( "*" ) )
						cPlatzhalter = 'i';

				}

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Performancemessung Start
			if( bDebugPerform )
				System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetNCDs PP START" );

			// Devices holen
			try {
				psdz = getPr�flingLaufzeitUmgebung().getDeviceManager().getPSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			try {
				userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Informiere den Benutzer
			userDialog.setDisplayProgress( true );
			userDialog.getProgressbar().setIndeterminate( true );
			userDialog.displayMessage( PB.getString( "psdz.ud.NCDErmittlung" ), ";" + PB.getString( "psdz.ud.SVTGenerierunglaeuft" ), -1 );

			// Berechnung der ECU Maske, bei Verwendung eines Platzhalters '*'
			// bei der vehicleStateIncludeList oder der vehicleStateExcludeList
			System.out.println( "PSdZ ID:" + testScreenID + ", cPlatzhalter: " + cPlatzhalter );
			switch( cPlatzhalter ) {
			// include Liste mit '*'
				case 'i':

					for( int i = 0; i < 256; i++ ) {
						if( !STAR_IGNORE_LIST_AS_LIST.contains( Integer.toHexString( i ) ) )
							maskList.add( Integer.toHexString( i ) );
					}
					vehicleStateIncludeList = (String[]) maskList.toArray( new String[maskList.size()] );

					if( bDebug )
						System.out.println( "PSdZ ID:" + testScreenID + ", MaskList for IncludeList: " + maskList.toString() );

					break;

				// tue nichts...
				default:
			}

			try {

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetNCDs 'psdz.getAndStoreNCDData' START" );
				}

				// NCDs ermitteln
				psdz.getAndStoreNCDData( vehicleStateIncludeList, bIsBlacklist, ncdTargetDirectory );

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetNCDs 'psdz.getAndStoreNCDData' ENDE" );
				}

				if( bDebug )
					System.out.println( "PSdZ ID:" + testScreenID + ", NCD target directory: " + ncdTargetDirectory.replace( '/', '\\' ) );

			} catch( CascadePSdZArgumentException e ) {
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZ", "PSdZGetNCDs", "", "", "", "", "", "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Argument" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( CascadePSdZConditionsNotCorrectException e ) {
				// Fehler abfangen und dokumentiern
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZ", "PSdZGetNCDs", "", "", "", "", "", "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Bedingungen" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( CascadePSdZRuntimeException e ) {
				// Fehler abfangen und dokumentiern
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZ", "PSdZGetNCDs", "", "", "", "", "", "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Laufzeit" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Freigabe der benutzten Devices
		if( psdz != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releasePSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
		try {
			getPr�flingLaufzeitUmgebung().releaseUserDialog();
		} catch( Exception e ) {
			if( e instanceof DeviceLockedException )
				result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "PSdZGetNCDs", "", "", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}
		setPPStatus( info, status, ergListe );

		// Performancemessung Ende
		if( bDebugPerform )
			System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGetNCDs PP ENDE" );
	}

}

/**
 * SDiagFlaCheck_5_0_F_Pruefprozedur.java Created on 01.02.05
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.opcext.*;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementation of the pruefprocedure performing the jobs to check the FLA justification. For
 * usage, a connection to external tools (LED target and a plc) has to exist. <br>
 * 
 * @author BMW AG, TI-432, S. Pichler
 * @author BMW AG, TI-432, W. Moeller
 * @version 0_0_1 13.01.05 WM First creation
 * @version 0_0_2 17.05.05 WM All Class variables set to null
 * @version 0_0_2 18.05.05 WM Debug to false, delete all comments
 * @version 3_0_F 19.05.05 SP Global review and bugfix concerning result creation
 * @version 4_0_F 13.02.06 SP Bugfix concerning dialog visualization
 * @version 5_0_F 07.04.06 SP Translation changed
 * @version 6_0_F 07.04.08 UP Bugfix concerning APDM-rating for variable reset
 * @version 7_0_F 24.06.08 CS APDM result adapted
 */
public class SDiagFlaCheck_7_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	/**
	 * Version id
	 */
	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public SDiagFlaCheck_7_0_F_Pruefprozedur() {
	}

	//special global variables
	/**
	 * implements the opcext device
	 */
	private OpcExt myOpcExt = null;

	/**
	 * logging Flag (System.out)
	 */
	private boolean debug = false;

	/**
	 * implements userdialog
	 */
	UserDialog myUd = null;

	/**
	 * user dialog in use
	 */
	private boolean udInUse = false;

	/**
	 * creates a new instance of SDiagFlaCheck_1_0_F_Pruefprozedur
	 * 
	 * @param pruefling current pruefling
	 * @param pruefprozName name of the current pruefprocedure
	 * @param hasToBeExecuted flag if execute
	 */
	public SDiagFlaCheck_7_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initializes the attributes
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * contains optional parameters
	 * 
	 * @return returs an array with the Strings of the optional parameters
	 */
	public String[] getOptionalArgs() {
		String[] args = { "TARGET_VALUE_Z", "WAITING_TIME_MILLIS", "CENTER_LOCATION" };
		return args;
	}

	/**
	 * contains required parameters
	 * 
	 * @return returns an array with the Strings of the reqired parameters
	 */
	public String[] getRequiredArgs() {
		String[] args = { "JOB" };
		return args;
	}

	/**
	 * parameter existence check
	 * 
	 * @return true if argument check is ok
	 */
	public boolean checkArgs() {
		if( debug )
			System.out.println( "SDiagFlaCheck: checkArgs gestartet" );
		boolean checkStatus = true;
		if( !super.checkArgs() )
			checkStatus = false;
		if( checkStatus ) {
			if( debug )
				System.out.println( "SDiagFlaCheck: checkStatus OK" );
			try {
				//if job is to position the target, the target value z has to exist
				if( getArg( getRequiredArgs()[0] ).equalsIgnoreCase( "POSITION_TARGET" ) ) {
					if( getArg( getOptionalArgs()[0] ) != null )
						Integer.parseInt( getArg( getOptionalArgs()[0] ) );
					else
						checkStatus = false;
				}
				//max waiting time is optional. Set to 15 s in execute, if not existing.
				if( getArg( getOptionalArgs()[1] ) != null )
					Integer.parseInt( getArg( getOptionalArgs()[1] ) );
				//center location (5 or 10) has to exist, if jo is to center or decenter the
				// vehicle
				if( getArg( getRequiredArgs()[0] ).equalsIgnoreCase( "CENTER_VEHICLE" ) || getArg( getRequiredArgs()[0] ).equalsIgnoreCase( "DECENTER_VEHICLE" ) ) {
					if( getArg( getOptionalArgs()[2] ) != null )
						checkStatus = false;
					if( Integer.parseInt( getArg( getOptionalArgs()[2] ) ) == 5 )
						checkStatus = true;
					if( Integer.parseInt( getArg( getOptionalArgs()[2] ) ) == 10 )
						checkStatus = true;
				}
				//parameters may also exist, if they are not to be used
				if( getArg( getOptionalArgs()[0] ) != null )
					Integer.parseInt( getArg( getOptionalArgs()[0] ) );
				if( getArg( getOptionalArgs()[1] ) != null )
					Integer.parseInt( getArg( getOptionalArgs()[1] ) );
				if( getArg( getOptionalArgs()[2] ) != null )
					Integer.parseInt( getArg( getOptionalArgs()[2] ) );
			} catch( NumberFormatException e ) {
				checkStatus = false;
			}
		} else if( debug )
			System.out.println( "SDiagFlaCheck: checkStatus Fehler" );
		return checkStatus;
	}

	/**
	 * Reference on the DB in the PLC
	 * 
	 * @param variableName Name of the Variable in the variables-file
	 * @return returns the DB Adress
	 * @throws VariablesException Thrown, if a problem exists using a Ps-variable.
	 */
	private String getReference( String variableName ) throws VariablesException {
		if( debug )
			System.out.println( "SDiagFlaCheck: Reference" );
		String reference = this.getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, variableName ).toString();
		if( debug )
			System.out.println( "SDiagFlaCheck: Reference erhalten" );
		return reference;
	}

	/**
	 * writes a boolean value to the Opc server
	 * 
	 * @param item DB adress
	 * @param value the value who has to been written to the plc
	 * @throws Exception thrown, if there's a problem with the sleepig thread
	 */
	private void writeBoolean( String item, boolean value ) throws Exception {
		if( debug )
			System.out.println( "SDiagFlaCheck: Boolean wird geschrieben" );
		myOpcExt.writeBoolean( item, value );
		if( debug )
			System.out.println( "SDiagFlaCheck: Boolean wurde geschrieben" );
	}

	/**
	 * writes a word value to the Opc server
	 * 
	 * @param item DB adress
	 * @param value the value who has to been written to the plc
	 * @throws Exception thrown, if there's a problem with the sleepig thread
	 */
	private void writeWord( String item, int value ) throws Exception {
		if( debug )
			System.out.println( "SDiagFlaCheck: Word wird geschrieben" );
		myOpcExt.writeInt( item, value );
		if( debug )
			System.out.println( "SDiagFlaCheck: Word wurde geschrieben" );
	}

	/**
	 * reads a boolean value from the Opc server
	 * 
	 * @param item DB adress
	 * @return the readed value
	 * @throws Exception thrown, if there's a problem with the sleepig thread
	 */
	private boolean readBoolean( String item ) throws Exception {
		if( debug )
			System.out.println( "SDiagFlaCheck: Boolean wird gelesen" );
		boolean valueBack1 = false, valueBack2 = false, valueBack3 = false;
		valueBack1 = myOpcExt.readBoolean( item );
		try {
			Thread.sleep( 100 );
		} catch( Exception e ) {
		}
		valueBack2 = myOpcExt.readBoolean( item );
		try {
			Thread.sleep( 100 );
		} catch( Exception e ) {
		}
		if( valueBack1 != valueBack2 ) {
			valueBack3 = myOpcExt.readBoolean( item );
			try {
				Thread.sleep( 100 );
			} catch( Exception e ) {
			}
			if( (valueBack1 != valueBack3) && (valueBack2 != valueBack3) ) {
				if( debug )
					System.out.println( "SDiagFlaCheck: dreimaliger Fehler beim lesen des Booleans" );
			}
		}
		if( valueBack1 == valueBack2 )
			valueBack3 = valueBack1;
		if( debug )
			System.out.println( "SDiagFlaCheck: Boolean wurde gelesen" );
		return valueBack3;
	}

	/**
	 * reads a word value from the Opc server
	 * 
	 * @param item DB adress
	 * @return the readed value
	 * @throws Exception thrown, if there's a problem with the sleepig thread
	 */
	private int readWord( String item ) throws Exception {
		if( debug )
			System.out.println( "SDiagFlaCheck: Word wird gelesen" );
		int valueBack1 = 0, valueBack2 = 0, valueBack3 = 0;
		valueBack1 = myOpcExt.readInt( item );
		try {
			Thread.sleep( 100 );
		} catch( Exception e ) {
		}
		valueBack2 = myOpcExt.readInt( item );
		try {
			Thread.sleep( 100 );
		} catch( Exception e ) {
		}
		if( valueBack1 != valueBack2 ) {
			valueBack3 = myOpcExt.readInt( item );
			try {
				Thread.sleep( 100 );
			} catch( Exception e ) {
			}
			if( (valueBack1 != valueBack3) && (valueBack2 != valueBack3) ) {
				if( debug )
					System.out.println( "SDiagFlaCheck: 3maliger Fehler beim Lesen des Words" );
			}
		}
		if( valueBack1 == valueBack2 )
			valueBack3 = valueBack1;
		if( debug )
			System.out.println( "SDiagFlaCheck: Word wurde gelesen" );
		return valueBack3;
	}

	/**
	 * Checks Plc State
	 * 
	 * @return 0=keine Betriebsart 1=Handbetrieb 2=Automatikbetrieb
	 */
	private int checkOpState() {
		long loop = 0;
		int opState = 9;
		boolean timeExpired = false, finished = false;
		long maxWaitingTime = 10000;
		try {
			//checks if operating state is automatic
			if( debug )
				System.out.println( "SDiagFlaCheck: check ob Betriebszustand ist automatik" );
			//read the current operating state
			while( !finished && !timeExpired ) {
				//read the current operating state
				if( debug )
					System.out.println( "SDiagFlaCheck: starte readWord f�r PLC_OPERATING_STATE" );
				opState = readWord( getReference( "PLC_OPERATING_STATE" ) );
				if( debug )
					System.out.println( "SDiagFlaCheck: readWord abgeschlossen, opState: " + opState );
				switch( opState ) {
					case 2: //Automatikmodus
						finished = true;
						if( udInUse ) {
							myUd.setVisible( false );
							udInUse = false;
						}
						break;
					case 1: //Handbetrieb
						if( !udInUse ) {
							if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
								myUd.displayAlertMessage( "FLA-Info", "Anlage im Handbetrieb", -1 );
							else
								myUd.displayAlertMessage( "FLA-Info", "Machine in manual mode", -1 );
							udInUse = true;
							myUd.setVisible( true );
						}
						break;
					case 0: //keine Betriebsart
						if( !udInUse ) {
							if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
								myUd.displayAlertMessage( "FLA-Info", "Keine Betriebsart der Anlage gew�hlt", -1 );
							else
								myUd.displayAlertMessage( "FLA-Info", "No mode for machine", -1 );
							udInUse = true;
							myUd.setVisible( true );
						}
						break;
					default: //Fehlerhafter Zustand
						if( !udInUse ) {
							if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
								myUd.displayAlertMessage( "FLA-Info", "Fehlerhafte Kennung der Anlagen-Betriebsart", -1 );
							else
								myUd.displayAlertMessage( "FLA-Info", "Incorrect id of machine readyness", -1 );
							udInUse = true;
							myUd.setVisible( true );
						}
						opState = -1;
						if( debug )
							System.out.println( "SDiagFlaCheck: oprationsstatus, falsch gelesen" );
				}
				if( !finished ) {
					//loop delay
					try {
						Thread.sleep( 100 );
					} catch( Exception e ) {
					}
					//check, if time expired
					if( debug )
						System.out.println( "SDiagFlaCheck: maxWaitingTime: " + maxWaitingTime );
					if( debug )
						System.out.println( "SDiagFlaCheck: loop*200: " + loop * 200 );
					if( (loop * 200) > maxWaitingTime )
						timeExpired = true;
					//increase loop counter
					loop++;
					udInUse = false;
				}
			}
		} catch( Exception e ) {
			if( debug )
				System.out.println( "SDiagFlaCheck: Exception read PlcOpState, plcOpState: " + opState );
			opState = -1;
		}
		return opState;
	}

	/**
	 * Checks Error State
	 * 
	 * @return 0=st�rungsfrei 1=St�rung 2=Notaus
	 */
	private int checkErrorState() {
		long loop = 0;
		int errorState = 9;
		boolean timeExpired = false, finished = false;
		long maxWaitingTime = 10000;
		try {
			if( debug )
				System.out.println( "SDiagFlaCheck: check ob SPS einen Fehler hat" );
			//checks if plc is in error
			while( !timeExpired && !finished ) {
				//read the current error state
				if( debug )
					System.out.println( "SDiagFlaCheck: starte readWord f�r PLC_ERROR" );
				errorState = readWord( getReference( "PLC_ERROR" ) );
				if( debug )
					System.out.println( "SDiagFlaCheck: readWord abgeschlossen, errorState: " + errorState );
				switch( errorState ) {
					case 0: //keine St�rung
						if( debug )
							System.out.println( "SDiagFlaCheck: errorstate erfolgreich beendet" );
						finished = true;
						if( udInUse ) {
							myUd.setVisible( false );
							udInUse = false;
						}
						break;
					case 1: //St�rung
						if( !udInUse ) {
							if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
								myUd.displayAlertMessage( "FLA-Info", "Anlagenfehler", -1 );
							else
								myUd.displayAlertMessage( "FLA-Info", "Machine error", -1 );
							udInUse = true;
							myUd.setVisible( true );
						}
						break;
					case 2: //Notaus
						if( !udInUse ) {
							if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
								myUd.displayAlertMessage( "FLA-Info", "Anlage im Not-Aus Zustand", -1 );
							else
								myUd.displayAlertMessage( "FLA-Info", "Machine in emergency off condition", -1 );
							udInUse = true;
							myUd.setVisible( true );
						}
						break;
					default:
						if( !udInUse ) {
							if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
								myUd.displayAlertMessage( "FLA-Info", "Fehlerhafte Kennung des Fehlerzustands", -1 );
							else
								myUd.displayAlertMessage( "FLA-Info", "Incorrect id of defect condition", -1 );
							udInUse = true;
							myUd.setVisible( true );
						}
						errorState = -1;
						if( debug )
							System.out.println( "SDiagFlaCheck: fehlerstatus, falsch gelesen" );
				}
				if( !finished ) {
					//loop delay
					try {
						Thread.sleep( 100 );
					} catch( Exception e ) {
					}
					//check, if time expired
					if( debug )
						System.out.println( "SDiagFlaCheck: maxWaitingTime: " + maxWaitingTime );
					if( debug )
						System.out.println( "SDiagFlaCheck: loop*200: " + loop * 200 );
					if( (loop * 200) > maxWaitingTime )
						timeExpired = true;
					//increase loop counter
					loop++;
					udInUse = false;
				}
			}
		} catch( Exception e ) {
			if( debug )
				System.out.println( "SDiagFlaCheck: Exception read PlcErrorState, plcErrorState " + errorState );
			errorState = -1;
		}
		return errorState;
	}

	/**
	 * set all from cascade written values back to an intial state
	 * 
	 * @param closeConnection if true the opc connection will be closed afterwards
	 * @return return true if all parameters are initialised
	 */
	private boolean ResetPlcDataToInitCondition( boolean closeConnection ) {
		if( debug )
			System.out.println( "SDiagFlaCheck: starte Reset" );
		boolean success = true;
		try {
			writeBoolean( this.getReference( "COMM_CENT_5_CLOSE" ), false );
			if( debug )
				System.out.println( "SDiagFlaCheck: COMM_CENT_5_CLOSE = false" );
			writeBoolean( this.getReference( "COMM_CENT_5_OPEN" ), true );
			if( debug )
				System.out.println( "SDiagFlaCheck: COMM_CENT_5_OPEN = true" );
			writeBoolean( this.getReference( "COMM_CENT_10_CLOSE" ), false );
			if( debug )
				System.out.println( "SDiagFlaCheck: COMM_CENT_10_CLOSE = false" );
			writeBoolean( this.getReference( "COMM_CENT_10_OPEN" ), true );
			if( debug )
				System.out.println( "SDiagFlaCheck: COMM_CENT_10_OPEN = true" );
			writeBoolean( this.getReference( "COMM_POS_DOWN" ), false );
			if( debug )
				System.out.println( "SDiagFlaCheck: COMM_POS_DOWN = false" );
			writeBoolean( this.getReference( "COMM_POS_UP" ), true );
			if( debug )
				System.out.println( "SDiagFlaCheck: COMM_POS_UP = true" );
			writeWord( this.getReference( "TARGET_VALUE_Z_SOLL" ), 1300 );
			if( debug )
				System.out.println( "SDiagFlaCheck: TARGET_VALUE_Z_SOLL = 1300" );
			writeBoolean( this.getReference( "COMM_LASER_ON" ), false );
			if( debug )
				System.out.println( "SDiagFlaCheck: COMM_LASER_ON = false" );
		} catch( Exception e ) {
			if( debug )
				System.out.println( "SDiagFlaCheck: Exception beim Reset" );
			success = false;
		}
		if( closeConnection ) {
			try {
				if( debug )
					System.out.println( "SDiagFlaCheck: Verbindung wird nach Reset getrennt" );
				this.myOpcExt.close();
				if( debug )
					System.out.println( "SDiagFlaCheck: Verbindung nach Reset erfolgreich getrennt" );
			} catch( Exception e ) {
				if( debug )
					System.out.println( "SDiagFlaCheck: Fehler beim Trennen der Verbindung nach Reset" );
				success = false;
			}
		}
		if( debug )
			System.out.println( "SDiagFlaCheck: Reset erfolgreich" );
		return (success);
	}

	/**
	 * add all items to the group
	 * 
	 * @param itemName name of the Opc - Item
	 * @return true if item succesfull added
	 */
	public boolean addItemToGroup( String itemName ) {
		boolean success = true;
		if( debug )
			System.out.println( "SDiagFlaCheck: try bevor item wird geaddet" );
		try {
			if( debug )
				System.out.println( "SDiagFlaCheck: item wird geaddet" );
			myOpcExt.addItemToGroup( itemName );
			if( debug )
				System.out.println( "SDiagFlaCheck: item wurde geaddet" );
		} catch( Exception e ) {
			success = false;
		}
		return success;
	}

	/**
	 * execution block
	 * 
	 * @param info some execution infos
	 */
	public void execute( ExecutionInfo info ) {

		/**
		 * the result vector
		 */
		Ergebnis result = null;
		/**
		 * vector list of all results
		 */
		Vector ergList = new Vector();
		/*
		 * status variable
		 */
		int status = STATUS_EXECUTION_OK;

		//execute internal, local variables
		/**
		 * loop counter
		 */
		long time = 0;
		long maxWaitingTime = 50000;
		int targetPositionZ = 0;//, centerLocation = 0;
		/**
		 * Userdisplay in use
		 */
		boolean timeExpired = false, finished = false, abbruch = false;
		int plcError = 9, plcOp = 9;

		//global try
		try {
			//check parameters
			if( !this.checkArgs() ) {
				result = new Ergebnis( "PPExecution", "CheckArgs", "", getArg( getRequiredArgs()[0] ), "", "ParameterExistence", "Nok", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
				ergList.add( result );
				throw new PPExecutionException();
			}
			if( debug )
				System.out.println( "SDiagFlaCheck: get specific parameter" );
			//get specific parameter, if available or set standard values, if they are not
			// available
			if( getArg( getOptionalArgs()[0] ) != null )
				targetPositionZ = Integer.parseInt( getArg( getOptionalArgs()[0] ) );
			if( getArg( getOptionalArgs()[1] ) != null )
				maxWaitingTime = Integer.parseInt( getArg( getOptionalArgs()[1] ) );

			if( debug )
				System.out.println( "SDiagFlaCheck: display User Dialog" );
			//display a user dialog if parameter message exists and useable
			try {
				myUd = this.getPr�flingLaufzeitUmgebung().getUserDialog();
				myUd.setVisible( false );
				myUd.setAllowCancel( false );
			} catch( Exception e ) {
				if( debug )
					System.out.println( "SDiagFlaCheck: Fehler beim initialisieren des UserDialogs" );
			}

			//get the opcext device for adressing the plc over opc server
			if( debug )
				System.out.println( "SDiagFlaCheck: OpcExtDevice holen" );
			myOpcExt = getPr�flingLaufzeitUmgebung().getDeviceManager().getOpcExt();
			if( debug )
				System.out.println( "SDiagFlaCheck: OpcExtDevice geholt" );

			// INIT
			// ---------------------------------------------
			// add Items
			if( getArg( getRequiredArgs()[0] ).equalsIgnoreCase( "INIT" ) ) {
				if( debug )
					System.out.println( "SDiagFlaCheck: Starte Init" );
				try {
					if( debug )
						System.out.println( "SDiagFlaCheck: Init gestartet" );
					if( addItemToGroup( getReference( "STAT_CENT_5_OPENED" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: STAT_CENT_5_OPENED erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: STAT_CENT_5_OPENED der Gruppe im OPC Server nicht hinzugef�gt" );
					if( addItemToGroup( getReference( "STAT_CENT_10_OPENED" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: STAT_CENT_10_OPENED erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: STAT_CENT_10_OPENED der Gruppe im OPC Server nicht hinzugef�gt" );
					if( addItemToGroup( getReference( "STAT_CENT_5_CLOSED" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: STAT_CENT_5_CLOSED erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: STAT_CENT_5_CLOSED der Gruppe im OPC Server nicht hinzugef�gt" );
					if( addItemToGroup( getReference( "STAT_CENT_10_CLOSED" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: STAT_CENT_10_CLOSED erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: STAT_CENT_10_CLOSED der Gruppe im OPC Server nicht hinzugef�gt" );
					if( addItemToGroup( getReference( "STAT_POS_UP" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: STAT_POS_UP erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: STAT_POS_UP der Gruppe im OPC Server nicht hinzugef�gt" );
					if( addItemToGroup( getReference( "STAT_POS_DOWN" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: STAT_POS_DOWN erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: STAT_POS_DOWN der Gruppe im OPC Server nicht hinzugef�gt" );
					if( addItemToGroup( getReference( "TARGET_VALUE_Z_IST" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: TARGET_VALUE_Z_IST erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: TARGET_VALUE_Z_IST der Gruppe im OPC Server nicht hinzugef�gt" );

					if( addItemToGroup( getReference( "COMM_CENT_5_OPEN" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: COMM_CENT_5_OPEN erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: COMM_CENT_5_OPEN der Gruppe im OPC Server nicht hinzugef�gt" );
					if( addItemToGroup( getReference( "COMM_CENT_10_OPEN" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: COMM_CENT_10_OPEN erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: COMM_CENT_10_OPEN der Gruppe im OPC Server nicht hinzugef�gt" );
					if( addItemToGroup( getReference( "COMM_CENT_5_CLOSE" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: COMM_CENT_5_CLOSE erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: COMM_CENT_5_CLOSE der Gruppe im OPC Server nicht hinzugef�gt" );
					if( addItemToGroup( getReference( "COMM_CENT_10_CLOSE" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: COMM_CENT_10_CLOSE erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: STAT_CENT_10_CLOSE der Gruppe im OPC Server nicht hinzugef�gt" );
					if( addItemToGroup( getReference( "COMM_POS_UP" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: COMM_POS_UP erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: COMM_POS_UP der Gruppe im OPC Server nicht hinzugef�gt" );
					if( addItemToGroup( getReference( "COMM_POS_DOWN" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: COMM_POS_DOWN erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: COMM_POS_DOWN der Gruppe im OPC Server nicht hinzugef�gt" );
					if( addItemToGroup( getReference( "TARGET_VALUE_Z_SOLL" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: TARGET_VALUE_Z_SOLL erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: TARGET_VALUE_Z_SOLL der Gruppe im OPC Server nicht hinzugef�gt" );

					if( addItemToGroup( getReference( "PLC_OPERATING_STATE" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: PLC_OPERATING_STATE erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: PLC_OPERATING_STATE der Gruppe im OPC Server nicht hinzugef�gt" );
					if( addItemToGroup( getReference( "PLC_ERROR" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: PLC_ERROR erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: PLC_ERROR der Gruppe im OPC Server nicht hinzugef�gt" );

					if( addItemToGroup( getReference( "PLC_ACTIVE" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: PLC_ACTIVE erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: PLC_ACTIVE der Gruppe im OPC Server nicht hinzugef�gt" );

					if( addItemToGroup( getReference( "STAT_LASER_ON" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: STAT_LASER_ON erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: STAT_LASER_ON der Gruppe im OPC Server nicht hinzugef�gt" );
					if( addItemToGroup( getReference( "COMM_LASER_ON" ) ) ) {
						if( debug )
							System.out.println( "SDiagFlaCheck: COMM_LASER_ON erfolgreich der Gruppe im OPC Server hinzugef�gt" );
					} else if( debug )
						System.out.println( "SDiagFlaCheck: COMM_LASER_ON der Gruppe im OPC Server nicht hinzugef�gt" );

					//reset all CASCADE written variables to init condition
					if( debug )
						System.out.println( "SDiagFlaCheck: INIT Reset" );
					ResetPlcDataToInitCondition( false );

					result = new Ergebnis( "INIT2", "PPExecution", "", "", "", "all Items", "added to group and reseted", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergList.add( result );

				} catch( Exception e ) {
					if( debug )
						System.out.println( "SDiagFlaCheck: Fehler w�hrend des Init-Pr�fschrittes" );
					result = new Ergebnis( "INIT1", "PPExecution", "", "", "", "all Items", "could not added to group or reseted", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
			}

			// MACHINE_READY_TEST
			// ---------------------------------------------
			else if( getArg( getRequiredArgs()[0] ).equalsIgnoreCase( "MACHINE_READY_TEST" ) ) {
				if( debug )
					System.out.println( "SDiagFlaCheck: Starte MachineReadyTest" );
				//job related variables;
				boolean statCent5 = false, statCent10 = false, statTargPos = false, laseroff = true;
				//reset the finish marker
				finished = false;
				time = 0;
				if( debug )
					System.out.println( "SDiagFlaCheck: Check ob Laser aus ist" );
				// checks if target is in startpostion
				while( !timeExpired && !finished && !abbruch ) {
					if( debug )
						System.out.println( "SDiagFlaCheck: pr�fe PlcState" );
					plcOp = checkOpState();
					plcError = checkErrorState();
					if( plcOp != 2 || plcError != 0 )
						abbruch = true;
					if( debug )
						System.out.println( "SDiagFlaCheck: PlcState gepr�ft" );
					if( debug )
						System.out.println( "SDiagFlaCheck: starte readBoolean f�r STAT_POS_UP und STAT_POS_DOWN" );
					if( readBoolean( getReference( "STAT_LASER_ON" ) ) ) {
						if( !udInUse ) {
							if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
								myUd.displayAlertMessage( "FLA-Info", "Laser ist noch nicht ausgeschalten", -1 );
							else
								myUd.displayAlertMessage( "FLA-Info", "Laser is still not switched off", -1 );
							udInUse = true;
							myUd.setVisible( true );
						}
					} else {
						if( debug )
							System.out.println( "SDiagFlaCheck: readBoolean f�r LASER_ON erfolgreich" );
						finished = true;
						laseroff = true;
						if( udInUse ) {
							myUd.setVisible( false );
							udInUse = false;
						}
					}
					if( !finished ) {
						//loop delay
						try {
							Thread.sleep( 100 );
						} catch( Exception e ) {
						}
						//check, if time expired
						if( (time * 800) > maxWaitingTime )
							timeExpired = true;
						//increase loop counter
						time++;
						udInUse = false;
					}
				}
				if( debug )
					System.out.println( "SDiagFlaCheck: check ob 5m Zentrierung offen ist" );
				finished = false;
				time = 0;
				//checks if 5m - Centration is open
				while( !timeExpired && !finished && !abbruch ) {
					if( debug )
						System.out.println( "SDiagFlaCheck: pr�fe PlcState" );
					plcOp = checkOpState();
					plcError = checkErrorState();
					if( plcOp != 2 || plcError != 0 )
						abbruch = true;
					if( debug )
						System.out.println( "SDiagFlaCheck: PlcState gepr�ft" );
					if( debug )
						System.out.println( "SDiagFlaCheck: starte readBoolean f�r STAT_CENT_5_OPENED und STAT_CENT_5_CLOSED" );
					if( !readBoolean( getReference( "STAT_CENT_5_OPENED" ) ) || readBoolean( getReference( "STAT_CENT_5_CLOSED" ) ) ) {
						if( !udInUse ) {
							if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
								myUd.displayAlertMessage( "FLA-Info", "5 Meter Zentrierung nicht ge�ffnet", -1 );
							else
								myUd.displayAlertMessage( "FLA-Info", "5 meter centration not completely unengaged", -1 );
							udInUse = true;
							myUd.setVisible( true );
						}
					} else {
						if( debug )
							System.out.println( "SDiagFlaCheck: readBoolean f�r STAT_CENT_5_OPENED und STAT_CENT_5_CLOSED erfolgreich" );
						finished = true;
						statCent5 = true;
						if( udInUse ) {
							myUd.setVisible( false );
							udInUse = false;
						}
					}
					if( !finished ) {
						//loop delay
						try {
							Thread.sleep( 100 );
						} catch( Exception e ) {
						}
						//check, if time expired
						if( debug )
							System.out.println( "SDiagFlaCheck: maxWaitingTime: " + maxWaitingTime );
						if( debug )
							System.out.println( "SDiagFlaCheck: time*800: " + time * 800 );
						if( (time * 800) > maxWaitingTime )
							timeExpired = true;
						//increase loop counter
						time++;
						udInUse = false;
					}
				}

				//reset the finish marker
				finished = false;
				time = 0;
				if( debug )
					System.out.println( "SDiagFlaCheck: check ob 10m Zentrierung offen" );
				//checks if 10m - Centration is open
				while( !timeExpired && !finished && !abbruch ) {
					if( debug )
						System.out.println( "SDiagFlaCheck: pr�fe PlcState" );
					plcOp = checkOpState();
					plcError = checkErrorState();
					if( plcOp != 2 || plcError != 0 )
						abbruch = true;
					if( debug )
						System.out.println( "SDiagFlaCheck: PlcState gepr�ft" );
					if( debug )
						System.out.println( "SDiagFlaCheck: starte readBoolean f�r STAT_CENT_10_OPENED und STAT_CENT_10_CLOSED" );
					if( !readBoolean( getReference( "STAT_CENT_10_OPENED" ) ) || readBoolean( getReference( "STAT_CENT_10_CLOSED" ) ) ) {
						if( !udInUse ) {
							if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
								myUd.displayAlertMessage( "FLA-Info", "10 Meter Zentrierung nicht ge�ffnet", -1 );
							else
								myUd.displayAlertMessage( "FLA-Info", "10 meter centration not completely unengaged", -1 );
							udInUse = true;
							myUd.setVisible( true );
						}
					} else {
						if( debug )
							System.out.println( "SDiagFlaCheck: readBoolean f�r STAT_CENT_10_OPENED und STAT_CENT_10_CLOSED erfolgreich" );
						finished = true;
						statCent10 = true;
						if( udInUse ) {
							myUd.setVisible( false );
							udInUse = false;
						}
					}
					if( !finished ) {
						//loop delay
						try {
							Thread.sleep( 100 );
						} catch( Exception e ) {
						}
						//check, if time expired
						if( (time * 800) > maxWaitingTime )
							timeExpired = true;
						//increase loop counter
						time++;
						udInUse = false;
					}
				}
				//reset the finish marker
				finished = false;
				time = 0;
				if( debug )
					System.out.println( "SDiagFlaCheck: Check ob Target oben ist" );
				// checks if target is in startpostion
				while( !timeExpired && !finished && !abbruch ) {
					if( debug )
						System.out.println( "SDiagFlaCheck: pr�fe PlcState" );
					plcOp = checkOpState();
					plcError = checkErrorState();
					if( plcOp != 2 || plcError != 0 )
						abbruch = true;
					if( debug )
						System.out.println( "SDiagFlaCheck: PlcState gepr�ft" );
					if( debug )
						System.out.println( "SDiagFlaCheck: starte readBoolean f�r STAT_POS_UP und STAT_POS_DOWN" );
					if( !readBoolean( getReference( "STAT_POS_UP" ) ) || readBoolean( getReference( "STAT_POS_DOWN" ) ) ) {
						if( !udInUse ) {
							if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
								myUd.displayAlertMessage( "FLA-Info", "Target ist nicht hochgefahren", -1 );
							else
								myUd.displayAlertMessage( "FLA-Info", "Target is not depositioned", -1 );
							udInUse = true;
							myUd.setVisible( true );
						}
					} else {
						if( debug )
							System.out.println( "SDiagFlaCheck: readBoolean f�r STAT_POS_UP und STAT_POS_DOWN erfolgreich" );
						finished = true;
						statTargPos = true;
						if( udInUse ) {
							myUd.setVisible( false );
							udInUse = false;
						}
					}
					if( !finished ) {
						//loop delay
						try {
							Thread.sleep( 100 );
						} catch( Exception e ) {
						}
						//check, if time expired
						if( (time * 800) > maxWaitingTime )
							timeExpired = true;
						//increase loop counter
						time++;
						udInUse = false;
					}
				}
				if( laseroff ) {
					result = new Ergebnis( "MachineReadyTest", "OpcExt", "", "", "", "Status Laser", "Off", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergList.add( result );
				} else {
					result = new Ergebnis( "MachineReadyTest", "OpcExt", "", "", "", "Status Laser", "on", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( timeExpired ) {
					result = new Ergebnis( "MachineReadyTest", "OpcExt", "", "", "", "TimeExpired", "", "", String.valueOf( maxWaitingTime ), "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( statCent5 ) {
					result = new Ergebnis( "MachineReadyTest", "OpcExt", "", "", "", "StatusCent5", "Opened", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergList.add( result );
				} else {
					result = new Ergebnis( "MachineReadyTest", "OpcExt", "", "", "", "StatusCent5", "Closed", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( statCent10 ) {
					result = new Ergebnis( "MachineReadyTest", "OpcExt", "", "", "", "StatusCent10", "Opened", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergList.add( result );
				} else {
					result = new Ergebnis( "MachineReadyTest", "OpcExt", "", "", "", "StatusCent10", "Closed", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}

				if( statTargPos ) {
					result = new Ergebnis( "MachineReadyTest", "OpcExt", "", "", "", "StatusTargetPos", "Up", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergList.add( result );
				} else {
					result = new Ergebnis( "MachineReadyTest", "OpcExt", "", "", "", "StatusTargetPos", "Down", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "Handmode", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == 0 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "no OperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "wrong OperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Error", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 2 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Emergency Out", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "wrong State", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				//status evaluation
				if( !laseroff || !statCent5 || !statCent10 || !statTargPos || timeExpired || abbruch )
					status = STATUS_EXECUTION_ERROR;
				else {
					status = STATUS_EXECUTION_OK;
				}
				if( udInUse ) {
					myUd.setVisible( false );
					udInUse = false;
				}
			}

			// CENTER_VEHICLE
			// ---------------------------------------------
			// assigns vehicle to be centered
			else if( getArg( getRequiredArgs()[0] ).equalsIgnoreCase( "CENTER_VEHICLE" ) ) {
				if( debug )
					System.out.println( "SDiagFlaCheck: CENTER_VEHICLE gestartet" );
				boolean statCent5 = false, statCent10 = false;
				//close 5m Centration
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "5" ) )
					writeBoolean( this.getReference( "COMM_CENT_5_OPEN" ), false );
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "5" ) )
					writeBoolean( this.getReference( "COMM_CENT_5_CLOSE" ), true );
				//close 10m Centration
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "10" ) )
					writeBoolean( this.getReference( "COMM_CENT_10_OPEN" ), false );
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "10" ) )
					writeBoolean( this.getReference( "COMM_CENT_10_CLOSE" ), true );
				time = 0;
				while( !timeExpired && !finished && !abbruch ) {
					finished = true;
					if( debug )
						System.out.println( "SDiagFlaCheck: pr�fe PlcState" );
					plcOp = checkOpState();
					plcError = checkErrorState();
					if( plcOp != 2 || plcError != 0 )
						abbruch = true;
					if( debug )
						System.out.println( "SDiagFlaCheck: PlcState gepr�ft" );
					//checks if 5m Centration is closed
					if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "5" ) && !udInUse ) {
						if( readBoolean( getReference( "STAT_CENT_5_OPENED" ) ) || !readBoolean( getReference( "STAT_CENT_5_CLOSED" ) ) ) {
							if( !udInUse ) {
								if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
									myUd.displayAlertMessage( "FLA-Info", "Zentrierung (5m) nicht geschlossen", -1 );
								else
									myUd.displayAlertMessage( "FLA-Info", "Centration (5m) not completely engaged", -1 );
								udInUse = true;
								myUd.setVisible( true );
							}
							finished = false;
						} else
							statCent5 = true;
					}
					if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "10" ) && !udInUse ) {
						//checks if 10m Centration is closed
						if( readBoolean( getReference( "STAT_CENT_10_OPENED" ) ) || !readBoolean( getReference( "STAT_CENT_10_CLOSED" ) ) ) {
							if( !udInUse ) {
								if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
									myUd.displayAlertMessage( "FLA-Info", "Zentrierung (10m) nicht geschlossen", -1 );
								else
									myUd.displayAlertMessage( "FLA-Info", "Centration (10m) not completely engaged", -1 );
								udInUse = true;
								myUd.setVisible( true );
							}
							finished = false;
						} else
							statCent10 = true;
					}
					//loop delay
					try {
						Thread.sleep( 100 );
					} catch( Exception e ) {
					}
					//check, if time expired
					if( (time * 800) > maxWaitingTime )
						timeExpired = true;
					//increase loop counter
					time++;
					udInUse = false;
				}
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "5" ) ) {
					if( statCent5 ) {
						result = new Ergebnis( "VehicleCentration", "OpcExt", "", "", "", "StatusCent5", "closed", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergList.add( result );
						status = STATUS_EXECUTION_OK;
					} else {
						result = new Ergebnis( "VehicleCentration", "OpcExt", "", "", "", "StatusCent5", "opened", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
						ergList.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				}
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "10" ) ) {
					if( statCent10 ) {
						result = new Ergebnis( "VehicleCentration", "OpcExt", "", "", "", "statCent10", "closed", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergList.add( result );
						status = STATUS_EXECUTION_OK;
					} else {
						result = new Ergebnis( "VehicleCentration", "OpcExt", "", "", "", "statCent10", "opened", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
						ergList.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				}
				//status evaluation
				if( udInUse ) {
					myUd.setVisible( false );
					udInUse = false;
				}
				if( timeExpired ) {
					result = new Ergebnis( "VehicleCentration", "OpcExt", "", "", "", "TimeExpired", "", "", String.valueOf( maxWaitingTime ), "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
				if( abbruch && plcOp == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "Handmode", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == 0 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "no OperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "wrong OperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Error", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 2 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Emergency Out", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "wrong State", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch )
					status = STATUS_EXECUTION_ERROR;
			}

			// POSITION_TARGET
			// ---------------------------------------------
			// assigns target to be positioned
			else if( getArg( getRequiredArgs()[0] ).equalsIgnoreCase( "POSITION_TARGET" ) ) {
				if( debug )
					System.out.println( "SDiagFlaCheck: POSITION_TARGET gestartet" );
				boolean statTargPos = false;
				//Save the TargetHeight
				targetPositionZ = Integer.parseInt( getArg( getOptionalArgs()[0] ) );
				writeWord( this.getReference( "TARGET_VALUE_Z_SOLL" ), targetPositionZ );
				targetPositionZ = readWord( getReference( "TARGET_VALUE_Z_SOLL" ) );
				//set the targe down
				writeBoolean( this.getReference( "COMM_POS_UP" ), false );
				writeBoolean( this.getReference( "COMM_POS_DOWN" ), true );
				time = 0;
				while( !timeExpired && !finished && !abbruch ) {
					if( debug )
						System.out.println( "SDiagFlaCheck: pr�fe PlcState" );
					plcOp = checkOpState();
					plcError = checkErrorState();
					if( plcOp != 2 || plcError != 0 )
						abbruch = true;
					if( debug )
						System.out.println( "SDiagFlaCheck: PlcState gepr�ft" );
					finished = true;
					if( readBoolean( getReference( "STAT_POS_UP" ) ) || !readBoolean( getReference( "STAT_POS_DOWN" ) ) ) {
						if( !udInUse ) {
							if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
								myUd.displayAlertMessage( "FLA-Info", "Target ist noch nicht heruntergefahren", -1 );
							else
								myUd.displayAlertMessage( "FLA-Info", "Target is still not set down", -1 );
							udInUse = true;
							myUd.setVisible( true );
						}
						finished = false;
					} else
						statTargPos = true;
					//loop delay
					try {
						Thread.sleep( 100 );
					} catch( Exception e ) {
					}
					//check, if time expired
					if( (time * 800) > maxWaitingTime )
						timeExpired = true;
					//increase loop counter
					time++;
					udInUse = false;
				}
				if( statTargPos ) {
					result = new Ergebnis( "TargePosition", "OpcExt", "", "", "", "statTargPos", "down", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergList.add( result );
					status = STATUS_EXECUTION_OK;
				} else {
					result = new Ergebnis( "TargePosition", "OpcExt", "", "", "", "statTargPos", "up", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
				//status evaluation
				if( udInUse ) {
					myUd.setVisible( false );
					udInUse = false;
				}
				if( timeExpired ) {
					result = new Ergebnis( "TargePosition", "OpcExt", "", "", "", "TimeExpired", "", "", String.valueOf( maxWaitingTime ), "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
				if( abbruch && plcOp == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "Handmode", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == 0 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "no OperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "wrong OperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Error", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 2 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Emergency Out", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "wrong State", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch )
					status = STATUS_EXECUTION_ERROR;
			}

			// CENTER_VEHICLE_POSITION_TARGET
			// ---------------------------------------------
			// assigns vehicle to be centered
			else if( getArg( getRequiredArgs()[0] ).equalsIgnoreCase( "CENTER_VEHICLE_POSITION_TARGET" ) ) {
				if( debug )
					System.out.println( "SDiagFlaCheck: CENTER_VEHICLE_POSITION_TARGET gestartet" );
				boolean statCent5 = false, statCent10 = false;
				//close 5m Centration
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "5" ) )
					writeBoolean( this.getReference( "COMM_CENT_5_OPEN" ), false );
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "5" ) )
					writeBoolean( this.getReference( "COMM_CENT_5_CLOSE" ), true );
				//close 10m Centration
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "10" ) )
					writeBoolean( this.getReference( "COMM_CENT_10_OPEN" ), false );
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "10" ) )
					writeBoolean( this.getReference( "COMM_CENT_10_CLOSE" ), true );
				boolean statTargPos = false;
				//Save the TargetHeight
				targetPositionZ = Integer.parseInt( getArg( getOptionalArgs()[0] ) );
				writeWord( this.getReference( "TARGET_VALUE_Z_SOLL" ), targetPositionZ );
				targetPositionZ = readWord( getReference( "TARGET_VALUE_Z_SOLL" ) );
				//set the targe down
				writeBoolean( this.getReference( "COMM_POS_UP" ), false );
				writeBoolean( this.getReference( "COMM_POS_DOWN" ), true );
				time = 0;
				while( !timeExpired && !finished && !abbruch ) {
					finished = true;
					if( debug )
						System.out.println( "SDiagFlaCheck: pr�fe PlcState" );
					plcOp = checkOpState();
					plcError = checkErrorState();
					if( plcOp != 2 || plcError != 0 )
						abbruch = true;
					if( debug )
						System.out.println( "SDiagFlaCheck: PlcState gepr�ft" );
					//checks if 5m Centration is closed
					if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "5" ) && !udInUse ) {
						if( readBoolean( getReference( "STAT_CENT_5_OPENED" ) ) || !readBoolean( getReference( "STAT_CENT_5_CLOSED" ) ) ) {
							if( !udInUse ) {
								if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
									myUd.displayAlertMessage( "FLA-Info", "Zentrierung (5m) nicht geschlossen", -1 );
								else
									myUd.displayAlertMessage( "FLA-Info", "Centration (5m) not closed", -1 );
								udInUse = true;
								myUd.setVisible( true );
							}
							finished = false;
						} else
							statCent5 = true;
					}
					if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "10" ) && !udInUse ) {
						//checks if 10m Centration is closed
						if( readBoolean( getReference( "STAT_CENT_10_OPENED" ) ) || !readBoolean( getReference( "STAT_CENT_10_CLOSED" ) ) ) {
							if( !udInUse ) {
								if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
									myUd.displayAlertMessage( "FLA-Info", "Zentrierung (10m) nicht geschlossen", -1 );
								else
									myUd.displayAlertMessage( "FLA-Info", "Centration (10m) not closed", -1 );
								udInUse = true;
								myUd.setVisible( true );
							}
							finished = false;
						} else
							statCent10 = true;
					}
					if( readBoolean( getReference( "STAT_POS_UP" ) ) || !readBoolean( getReference( "STAT_POS_DOWN" ) ) ) {
						if( !udInUse ) {
							if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
								myUd.displayAlertMessage( "FLA-Info", "Target ist noch nicht heruntergefahren", -1 );
							else
								myUd.displayAlertMessage( "FLA-Info", "Target is still not set down", -1 );
							udInUse = true;
							myUd.setVisible( true );
						}
						finished = false;
					} else
						statTargPos = true;
					//loop delay
					try {
						Thread.sleep( 100 );
					} catch( Exception e ) {
					}
					//check, if time expired
					if( (time * 800) > maxWaitingTime )
						timeExpired = true;
					//increase loop counter
					time++;
					udInUse = false;
				}
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "5" ) ) {
					if( statCent5 ) {
						result = new Ergebnis( "VehicleCentration", "OpcExt", "", "", "", "StatusCent5", "closed", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergList.add( result );
						status = STATUS_EXECUTION_OK;
					} else {
						result = new Ergebnis( "VehicleCentration", "OpcExt", "", "", "", "StatusCent5", "opened", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
						ergList.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				}
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "10" ) ) {
					if( statCent10 ) {
						result = new Ergebnis( "VehicleCentration", "OpcExt", "", "", "", "statCent10", "closed", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergList.add( result );
						status = STATUS_EXECUTION_OK;
					} else {
						result = new Ergebnis( "VehicleCentration", "OpcExt", "", "", "", "statCent10", "opened", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
						ergList.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				}
				if( statTargPos ) {
					result = new Ergebnis( "TargePosition", "OpcExt", "", "", "", "statTargPos", "down", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergList.add( result );
					status = STATUS_EXECUTION_OK;
				} else {
					result = new Ergebnis( "TargePosition", "OpcExt", "", "", "", "statTargPos", "up", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
				//status evaluation
				if( udInUse ) {
					myUd.setVisible( false );
					udInUse = false;
				}
				if( timeExpired ) {
					result = new Ergebnis( "VehicleCentration", "OpcExt", "", "", "", "TimeExpired", "", "", String.valueOf( maxWaitingTime ), "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
				if( abbruch && plcOp == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "Handmode", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == 0 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "no OperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "wrong OperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Error", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 2 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Emergency Out", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "wrong State", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch )
					status = STATUS_EXECUTION_ERROR;
			}

			// DEPOSITION_TARGET_DECENTER_VEHICLE
			// ---------------------------------------------
			// assigns target to be depositioned
			else if( getArg( getRequiredArgs()[0] ).equalsIgnoreCase( "DEPOSITION_TARGET_DECENTER_VEHICLE" ) ) {
				if( debug )
					System.out.println( "SDiagFlaCheck: DEPOSITION_TARGET_DECENTER_VEHICLE gestartet" );
				boolean statTargPos = false;
				//set the targe down
				writeBoolean( this.getReference( "COMM_POS_DOWN" ), false );
				writeBoolean( this.getReference( "COMM_POS_UP" ), true );
				boolean statCent5 = false, statCent10 = false;
				//open 5m Centration
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "5" ) )
					writeBoolean( this.getReference( "COMM_CENT_5_CLOSE" ), false );
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "5" ) )
					writeBoolean( this.getReference( "COMM_CENT_5_OPEN" ), true );
				//open 10m Centration
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "10" ) )
					writeBoolean( this.getReference( "COMM_CENT_10_CLOSE" ), false );
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "10" ) )
					writeBoolean( this.getReference( "COMM_CENT_10_OPEN" ), true );
				time = 0;
				while( !timeExpired && !finished && !abbruch ) {
					if( debug )
						System.out.println( "SDiagFlaCheck: pr�fe PlcState" );
					plcOp = checkOpState();
					plcError = checkErrorState();
					if( plcOp != 2 || plcError != 0 )
						abbruch = true;
					if( debug )
						System.out.println( "SDiagFlaCheck: PlcState gepr�ft" );
					finished = true;
					if( !readBoolean( getReference( "STAT_POS_UP" ) ) || readBoolean( getReference( "STAT_POS_DOWN" ) ) ) {
						if( !udInUse ) {
							if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
								myUd.displayAlertMessage( "FLA-Info", "Target ist noch nicht hochgefahren", -1 );
							else
								myUd.displayAlertMessage( "FLA-Info", "Target is still not set up", -1 );
							udInUse = true;
							myUd.setVisible( true );
						}
						finished = false;
					} else
						statTargPos = true;
					//checks if 5m Centration is opened
					if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "5" ) && !udInUse ) {
						if( !readBoolean( getReference( "STAT_CENT_5_OPENED" ) ) || readBoolean( getReference( "STAT_CENT_5_CLOSED" ) ) ) {
							if( !udInUse ) {
								if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
									myUd.displayAlertMessage( "FLA-Info", "Zentrierung (5m) nicht ge�ffnet", -1 );
								else
									myUd.displayAlertMessage( "FLA-Info", "Centration (5m) not completely unengaged", -1 );
								udInUse = true;
								myUd.setVisible( true );
							}
							finished = false;
						} else
							statCent5 = true;
					}
					if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "10" ) && !udInUse ) {
						//checks if 10m Centration is opened
						if( !readBoolean( getReference( "STAT_CENT_10_OPENED" ) ) || readBoolean( getReference( "STAT_CENT_10_CLOSED" ) ) ) {
							if( !udInUse ) {
								if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
									myUd.displayAlertMessage( "FLA-Info", "Zentrierung (10m) nicht ge�ffnet", -1 );
								else
									myUd.displayAlertMessage( "FLA-Info", "Centration (10m) not completely unengaged", -1 );
								udInUse = true;
								myUd.setVisible( true );
							}
							finished = false;
						} else
							statCent10 = true;
					}
					//loop delay
					try {
						Thread.sleep( 100 );
					} catch( Exception e ) {
					}
					//check, if time expired
					if( (time * 800) > maxWaitingTime )
						timeExpired = true;
					//increase loop counter
					time++;
					udInUse = false;
				}
				if( statTargPos ) {
					result = new Ergebnis( "TargeDePosition", "OpcExt", "", "", "", "statTargPos", "up", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergList.add( result );
					status = STATUS_EXECUTION_OK;
				} else {
					result = new Ergebnis( "TargeDePosition", "OpcExt", "", "", "", "statTargPos", "down", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "5" ) ) {
					if( statCent5 ) {
						result = new Ergebnis( "VehicleDeCentration", "OpcExt", "", "", "", "StatusCent5", "opened", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergList.add( result );
						status = STATUS_EXECUTION_OK;
					} else {
						result = new Ergebnis( "VehicleDeCentration", "OpcExt", "", "", "", "StatusCent5", "closed", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
						ergList.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				}
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "10" ) ) {
					if( statCent10 ) {
						result = new Ergebnis( "VehicleDeCentration", "OpcExt", "", "", "", "statCent10", "opened", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergList.add( result );
						status = STATUS_EXECUTION_OK;
					} else {
						result = new Ergebnis( "VehicleDeCentration", "OpcExt", "", "", "", "statCent10", "closed", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
						ergList.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				}
				//status evaluation
				if( udInUse ) {
					myUd.setVisible( false );
					udInUse = false;
				}
				if( timeExpired ) {
					result = new Ergebnis( "TargeDePosition", "OpcExt", "", "", "", "TimeExpired", "", "", String.valueOf( maxWaitingTime ), "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
				if( abbruch && plcOp == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "Handmode", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == 0 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "no OperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "wrong OperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 1 ) {
					result = new Ergebnis( "PPExecution3", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Error", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 2 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Emergency Out", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "wrong State", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch )
					status = STATUS_EXECUTION_ERROR;
			}

			// DEPOSITION_TARGET
			// ---------------------------------------------
			// assigns target to be depositioned
			else if( getArg( getRequiredArgs()[0] ).equalsIgnoreCase( "DEPOSITION_TARGET" ) ) {
				if( debug )
					System.out.println( "SDiagFlaCheck: DEPOSITION_TARGET gestartet" );
				boolean statTargPos = false;
				//set the targe down
				writeBoolean( this.getReference( "COMM_POS_DOWN" ), false );
				writeBoolean( this.getReference( "COMM_POS_UP" ), true );
				time = 0;
				while( !timeExpired && !finished && !abbruch ) {
					if( debug )
						System.out.println( "SDiagFlaCheck: pr�fe PlcState" );
					plcOp = checkOpState();
					plcError = checkErrorState();
					if( plcOp != 2 || plcError != 0 )
						abbruch = true;
					if( debug )
						System.out.println( "SDiagFlaCheck: PlcState gepr�ft" );
					finished = true;
					if( !readBoolean( getReference( "STAT_POS_UP" ) ) || readBoolean( getReference( "STAT_POS_DOWN" ) ) ) {
						if( !udInUse ) {
							if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
								myUd.displayAlertMessage( "FLA-Info", "Target ist noch nicht hochgefahren", -1 );
							else
								myUd.displayAlertMessage( "FLA-Info", "Target is still not set up", -1 );
							udInUse = true;
							myUd.setVisible( true );
						}
						finished = false;
					} else
						statTargPos = true;
					//loop delay
					try {
						Thread.sleep( 100 );
					} catch( Exception e ) {
					}
					//check, if time expired
					if( (time * 800) > maxWaitingTime )
						timeExpired = true;
					//increase loop counter
					time++;
					udInUse = false;
				}
				if( statTargPos ) {
					result = new Ergebnis( "TargeDePosition", "OpcExt", "", "", "", "statTargPos", "up", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergList.add( result );
					status = STATUS_EXECUTION_OK;
				} else {
					result = new Ergebnis( "TargeDePosition", "OpcExt", "", "", "", "statTargPos", "down", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
				//status evaluation
				if( udInUse ) {
					myUd.setVisible( false );
					udInUse = false;
				}
				if( timeExpired ) {
					result = new Ergebnis( "TargeDePosition", "OpcExt", "", "", "", "TimeExpired", "", "", String.valueOf( maxWaitingTime ), "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
				if( abbruch && plcOp == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "Handmode", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == 0 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "no OperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "wrong OperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Error", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 2 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Emergency Out", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "wrong State", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch )
					status = STATUS_EXECUTION_ERROR;
			}

			// DECENTER_VEHICLE
			// ---------------------------------------------
			// assigns vehicle to be decentered
			else if( getArg( getRequiredArgs()[0] ).equalsIgnoreCase( "DECENTER_VEHICLE" ) ) {
				if( debug )
					System.out.println( "SDiagFlaCheck: DECENTER_VEHICLE gestartet" );
				boolean statCent5 = false, statCent10 = false;
				//open 5m Centration
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "5" ) )
					writeBoolean( this.getReference( "COMM_CENT_5_CLOSE" ), false );
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "5" ) )
					writeBoolean( this.getReference( "COMM_CENT_5_OPEN" ), true );
				//open 10m Centration
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "10" ) )
					writeBoolean( this.getReference( "COMM_CENT_10_CLOSE" ), false );
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "10" ) )
					writeBoolean( this.getReference( "COMM_CENT_10_OPEN" ), true );
				time = 0;
				while( !timeExpired && !finished && !abbruch ) {
					if( debug )
						System.out.println( "SDiagFlaCheck: pr�fe PlcState" );
					plcOp = checkOpState();
					plcError = checkErrorState();
					if( plcOp != 2 || plcError != 0 )
						abbruch = true;
					if( debug )
						System.out.println( "SDiagFlaCheck: PlcState gepr�ft" );
					finished = true;
					//checks if 5m Centration is opened
					if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "5" ) && !udInUse ) {
						if( !readBoolean( getReference( "STAT_CENT_5_OPENED" ) ) || readBoolean( getReference( "STAT_CENT_5_CLOSED" ) ) ) {
							if( !udInUse ) {
								if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
									myUd.displayAlertMessage( "FLA-Info", "Zentrierung (5m) nicht ge�ffnet", -1 );
								else
									myUd.displayAlertMessage( "FLA-Info", "Centration (5m) not opened", -1 );
								udInUse = true;
								myUd.setVisible( true );
							}
							finished = false;
						} else
							statCent5 = true;
					}
					if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "10" ) && !udInUse ) {
						//checks if 10m Centration is opened
						if( !readBoolean( getReference( "STAT_CENT_10_OPENED" ) ) || readBoolean( getReference( "STAT_CENT_10_CLOSED" ) ) ) {
							if( !udInUse ) {
								if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
									myUd.displayAlertMessage( "FLA-Info", "Zentrierung (10m) nicht ge�ffnet", -1 );
								else
									myUd.displayAlertMessage( "FLA-Info", "Centration (10m) not opened", -1 );
								udInUse = true;
								myUd.setVisible( true );
							}
							finished = false;
						} else
							statCent10 = true;
					}
					//loop delay
					try {
						Thread.sleep( 100 );
					} catch( Exception e ) {
					}
					//check, if time expired
					if( (time * 800) > maxWaitingTime )
						timeExpired = true;
					//increase loop counter
					time++;
					udInUse = false;
				}
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "5" ) ) {
					if( statCent5 ) {
						result = new Ergebnis( "VehicleDeCentration", "OpcExt", "", "", "", "StatusCent5", "opened", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergList.add( result );
						status = STATUS_EXECUTION_OK;
					} else {
						result = new Ergebnis( "VehicleDeCentration", "OpcExt", "", "", "", "StatusCent5", "closed", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
						ergList.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				}
				if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "10" ) ) {
					if( statCent10 ) {
						result = new Ergebnis( "VehicleDeCentration", "OpcExt", "", "", "", "statCent10", "opened", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergList.add( result );
						status = STATUS_EXECUTION_OK;
					} else {
						result = new Ergebnis( "VehicleDeCentration", "OpcExt", "", "", "", "statCent10", "closed", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
						ergList.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				}
				//status evaluation
				if( udInUse ) {
					myUd.setVisible( false );
					udInUse = false;
				}
				if( timeExpired ) {
					result = new Ergebnis( "VehicleDeCentrationt", "OpcExt", "", "", "", "TimeExpired", "", "", String.valueOf( maxWaitingTime ), "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
				if( abbruch && plcOp == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "Handmode", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == 0 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "no OperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "wrong OperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Error", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 2 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Emergency Out", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "wrong State", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch )
					status = STATUS_EXECUTION_ERROR;
			}
			// SWITCH_LASER_ON
			// ---------------------------------------------
			// switch on the Laser
			else if( getArg( getRequiredArgs()[0] ).equalsIgnoreCase( "SWITCH_LASER_ON" ) ) {
				if( debug )
					System.out.println( "SDiagFlaCheck: SWITCH_LASER_ON gestartet" );
				boolean laseron = false;
				//switches the laser on
				writeBoolean( this.getReference( "COMM_LASER_ON" ), true );
				time = 0;
				while( !timeExpired && !finished && !abbruch ) {
					if( debug )
						System.out.println( "SDiagFlaCheck: pr�fe PlcState" );
					plcOp = checkOpState();
					plcError = checkErrorState();
					if( plcOp != 2 || plcError != 0 )
						abbruch = true;
					if( debug )
						System.out.println( "SDiagFlaCheck: PlcState gepr�ft" );
					finished = true;
					if( !readBoolean( getReference( "STAT_LASER_ON" ) ) ) {
						if( !udInUse ) {
							if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
								myUd.displayAlertMessage( "FLA-Info", "Laser ist noch nicht eingeschalten", -1 );
							else
								myUd.displayAlertMessage( "FLA-Info", "Laser is still not switched on", -1 );
							udInUse = true;
							myUd.setVisible( true );
						}
						finished = false;
					} else
						laseron = true;
					//loop delay
					try {
						Thread.sleep( 100 );
					} catch( Exception e ) {
					}
					//check, if time expired
					if( (time * 800) > maxWaitingTime )
						timeExpired = true;
					//increase loop counter
					time++;
					udInUse = false;
				}
				if( laseron ) {
					result = new Ergebnis( "switchLaserOn", "OpcExt", "", "", "", "Status Laser", "on", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergList.add( result );
					status = STATUS_EXECUTION_OK;
				} else {
					result = new Ergebnis( "switchLaserOn", "OpcExt", "", "", "", "Satus Laser", "off", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
				//status evaluation
				if( udInUse ) {
					myUd.setVisible( false );
					udInUse = false;
				}
				if( timeExpired ) {
					result = new Ergebnis( "switchLaserOn", "OpcExt", "", "", "", "TimeExpired", "", "", String.valueOf( maxWaitingTime ), "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
				if( abbruch && plcOp == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "Handmode", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == 0 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "no OperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "wrongOperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Error", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 2 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Emergency Out", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "wrong State", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch )
					status = STATUS_EXECUTION_ERROR;
			}

			// SWITCH_LASER_OFF
			// ---------------------------------------------
			// switch on the Laser
			else if( getArg( getRequiredArgs()[0] ).equalsIgnoreCase( "SWITCH_LASER_OFF" ) ) {
				if( debug )
					System.out.println( "SDiagFlaCheck: SWITCH_LASER_OFF gestartet" );
				boolean laseroff = false;
				//switches the laser of
				writeBoolean( this.getReference( "COMM_LASER_ON" ), false );
				time = 0;
				while( !timeExpired && !finished && !abbruch ) {
					if( debug )
						System.out.println( "SDiagFlaCheck: pr�fe PlcState" );
					plcOp = checkOpState();
					plcError = checkErrorState();
					if( plcOp != 2 || plcError != 0 )
						abbruch = true;
					if( debug )
						System.out.println( "SDiagFlaCheck: PlcState gepr�ft" );
					finished = true;
					if( readBoolean( getReference( "STAT_LASER_ON" ) ) ) {
						if( !udInUse ) {
							if( System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) )
								myUd.displayAlertMessage( "FLA-Info", "Laser ist noch nicht ausgeschalten", -1 );
							else
								myUd.displayAlertMessage( "FLA-Info", "Laser is still not switched off", -1 );
							udInUse = true;
							myUd.setVisible( true );
						}
						finished = false;
					} else
						laseroff = true;
					//loop delay
					try {
						Thread.sleep( 100 );
					} catch( Exception e ) {
					}
					//check, if time expired
					if( (time * 800) > maxWaitingTime )
						timeExpired = true;
					//increase loop counter
					time++;
					udInUse = false;
				}
				if( laseroff ) {
					result = new Ergebnis( "switchLaserOff", "OpcExt", "", "", "", "StatusLaser", "off", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergList.add( result );
					status = STATUS_EXECUTION_OK;
				} else {
					result = new Ergebnis( "switchLaserOff", "OpcExt", "", "", "", "Satus Laser", "on", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
				//status evaluation
				if( udInUse ) {
					myUd.setVisible( false );
					udInUse = false;
				}
				if( timeExpired ) {
					result = new Ergebnis( "switchLaserOff", "OpcExt", "", "", "", "TimeExpired", "", "", String.valueOf( maxWaitingTime ), "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
				if( abbruch && plcOp == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "Handmode", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == 0 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "no OperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "wrongOperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Error", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 2 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Emergency Out", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "wrong State", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch )
					status = STATUS_EXECUTION_ERROR;
			}

			// RESET
			// ---------------------------------------------
			// performs a ResetPlcDataToInitCondition of variables
			else if( getArg( getRequiredArgs()[0] ).equalsIgnoreCase( "RESET" ) ) {
				if( debug )
					System.out.println( "SDiagFlaCheck: RESET gestartet" );
				plcOp = checkOpState();
				plcError = checkErrorState();
				if( plcOp != 2 || plcError != 0 )
					abbruch = true;
				ResetPlcDataToInitCondition( true ); // warum m�ssen wir hier die Verbindung
				// trennen?
				if( abbruch && plcOp == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "Handmode", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == 0 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "no OperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcOp == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Operating State", "wrongOperatingState", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Error", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == 2 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "Emergency Out", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch && plcError == -1 ) {
					result = new Ergebnis( "PPExecution", "CheckPLCState", "", getArg( getRequiredArgs()[0] ), "", "Error State", "wrong State", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergList.add( result );
				}
				if( abbruch ) {
					status = STATUS_EXECUTION_ERROR;
				}
				if( !abbruch ) {
					result = new Ergebnis( "PPExecution", "OpcExt", "", "", "", "all_Comm_Parameters", "resetted", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergList.add( result );
				}
				this.getPr�flingLaufzeitUmgebung().getDeviceManager().releaseOpcExt();
			}

			// given job is not defined
			else {
				ResetPlcDataToInitCondition( true );
				result = new Ergebnis( "PPExecution", "ParamError", "", getArg( getRequiredArgs()[0] ), "", "Error", "No such job defined", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
				ergList.add( result );
				status = STATUS_EXECUTION_ERROR;
			}

			//release the user dialog
			if( myUd != null ) {
				try {
					myUd.close();
			        getPr�flingLaufzeitUmgebung().releaseUserDialog();
			        myUd = null;
					} catch( Exception e ) {
				}
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
			result = new Ergebnis( "PPExecution", "Unknown", "", getArg( getRequiredArgs()[0] ), "", "Exception", "Unknown", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
			ergList.add( result );
		} catch( DeviceLockedException e ) {
			status = STATUS_EXECUTION_ERROR;
			result = new Ergebnis( "PPExecution", "OpcExt", "", getArg( getRequiredArgs()[0] ), "", "Exception", "DeviceLocked", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
			ergList.add( result );
		} catch( DeviceNotAvailableException e ) {
			status = STATUS_EXECUTION_ERROR;
			result = new Ergebnis( "PPExecution", "OpcExt", "", getArg( getRequiredArgs()[0] ), "", "Exception", "DeviceNotAvailable", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
			ergList.add( result );
		} catch( OpcException e ) {
			status = STATUS_EXECUTION_ERROR;
			result = new Ergebnis( "PPExecution", "OpcExt", "", getArg( getRequiredArgs()[0] ), "", "Exception", "Opc", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
			ergList.add( result );
		} catch( Exception e ) {
			status = STATUS_EXECUTION_ERROR;
			result = new Ergebnis( "PPExecution", "Unknown", "", getArg( getRequiredArgs()[0] ), "", "Exception", "Unknown", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
			ergList.add( result );
		} catch( Throwable t ) {
			status = STATUS_EXECUTION_ERROR;
			result = new Ergebnis( "PPExecution", "Unknown", "", getArg( getRequiredArgs()[0] ), "", "Throwable", "Unknown", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
			ergList.add( result );
		}
		try {
			this.getPr�flingLaufzeitUmgebung().getDeviceManager().releaseOpcExt();
		} catch( Exception e ) {
			status = STATUS_EXECUTION_ERROR;
			result = new Ergebnis( "PPExecution", "OpcExt", "", getArg( getRequiredArgs()[0] ), "", "OpcDeviceNotReleased", "Opc", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
			ergList.add( result );
		}

		//report status to CASCADE
		setPPStatus( info, status, ergList );

		//reset used global variables
		if( debug )
			System.out.println( "SDiagFlaCheck: Das R�cksetzen der globalen Variablen wurde gestartet." );
		myOpcExt = null;
		time = 0;
		debug = true;
		udInUse = false;
		if( debug )
			System.out.println( "SDiagFlaCheck: Das R�cksetzen der globalen Variablen wurde durchgef�hrt." );
	}
}

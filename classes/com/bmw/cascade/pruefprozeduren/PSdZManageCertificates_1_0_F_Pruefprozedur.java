package com.bmw.cascade.pruefprozeduren;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
//import com.bmw.cascade.pruefstand.devices.psdz.PB;
import com.bmw.cascade.pruefstand.devices.psdz.PSdZ;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.pruefstand.devices.psdz.data.certManagement.CascadeBindingCalculationFailureCto;
import com.bmw.cascade.pruefstand.devices.psdz.data.certManagement.CascadeBindingCalculationProgressStatusTo;
import com.bmw.cascade.pruefstand.devices.psdz.data.certManagement.CascadeBindingDetailStatusCto;
import com.bmw.cascade.pruefstand.devices.psdz.data.certManagement.CascadeCertCalculatedObjectCto;
import com.bmw.cascade.pruefstand.devices.psdz.data.certManagement.CascadeCertCalculationDetailedStatusEto;
import com.bmw.cascade.pruefstand.devices.psdz.data.certManagement.CascadeCertCalculationOverallStatusEto;
import com.bmw.cascade.pruefstand.devices.psdz.data.certManagement.CascadeEcuCertCheckingMaxWaitingTime;
import com.bmw.cascade.pruefstand.devices.psdz.data.certManagement.CascadeEcuCertCheckingResponseEto;
import com.bmw.cascade.pruefstand.devices.psdz.data.certManagement.CascadeEcuCertCheckingStatusEto;
import com.bmw.cascade.pruefstand.devices.psdz.data.certManagement.CascadeEcuFailureResponseCto;
import com.bmw.cascade.pruefstand.devices.psdz.data.certManagement.CascadeFetchBindingCalculationResultCto;
import com.bmw.cascade.pruefstand.devices.psdz.data.certManagement.CascadeFetchEcuCertCheckingResultCto;
import com.bmw.cascade.pruefstand.devices.psdz.data.certManagement.CascadeOtherBindingDetailStatusCto;
import com.bmw.cascade.pruefstand.devices.psdz.data.certManagement.CascadeRequestEcuCertCheckingResultCto;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.CascadePSdZConditionsNotCorrectException;

/**
 * Implementierung einer Pr�fprozedur, die das Beschaffen, Einbringen und Pr�fen von Fahrzeug-Zertifikaten durchf�hrt
 * 
 * @author Martin Gampl, BMW AG <BR>
 * 
 *         07.11.2016 MG T-Version (0_0), Mindestversionsanforderung CASCADE 8.0.1b / PSdZ 5.03.00 <BR>
 *         ---------- -- - LOP 2125: Zertifikatemanagement SP2018 <BR>
 * 
 */
public class PSdZManageCertificates_1_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DE fuer Sprachermittlung
	 */
	private final boolean DE = checkDE(); // Systemsprache DE wenn true

	/**
	 * Aufz�hlung der m�glichen Aktionen
	 */
	public enum ACTIONS {
		READ_CSRS_FROM_ECUS, READ_CSRS_FROM_FILE, START_BINDING_REQUEST_TO_CBB, STORE_BINDING_REQUEST_TO_FILE, GET_BINDINGS_FROM_CBB, GET_BINDINGS_FROM_FILE, CALCULATE_BINDING_DISTRIBUTION, WRITE_BINDINGS_AND_OTHERBINDINGS_TO_ECUS, START_EXTENSIVE_CERTIFICATE_AND_BINDING_CHECK, GET_STATUS_EXTENSIVE_CERTIFICATE_AND_BINDING_CHECK
	}

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSdZManageCertificates_1_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSdZManageCertificates_1_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "BINDING_REQUEST_FILE_LOCATION", "BINDINGS_FILE_LOCATION", "CBB_URLS", "CSRS_FILE_LOCATION", "DIRECTORY_FOR_ITSM_TICKET_FILES", "DEBUG", "DEBUG_PERFORM", "ECU_EXCLUDE_LIST", "ECU_EXCLUDE_LIST_IPSEC", "ECU_INCLUDE_LIST", "EMAIL_RECIPIENTS_FOR_NOTIFICATION_IN_CASE_OF_ITSM_TICKET", "HINT_TEXT", "LOG_OBJECT_FILE", "MAXTIME_FOR_STATUS_CHECK_RETRIES", "SYSTEM_OPERATOR_INFO_FOR_ITSM_TICKET", "TIMEOUT_BEFORE_ERROR_ESCALATION_IN_CASE_OF_DELAYED_CBB_RESPONSE", "TIMEOUT_BEFORE_TRYING_NEXT_CBB_IN_CASE_OF_ERROR", "TIME_BETWEEN_STATUS_CHECK_RETRIES", "TOTAL_CBB_RETRIES" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = { "ACTION" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		if( !super.checkArgs() )
			return false;
		return true;
	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * Methode erzeugt einen ITSM-Fehlermeldungstext
	 * 
	 * @param connectionAttempt Verbindungsversuch
	 * @param errorText Fehlertext
	 * @param cbbUrl URL, bei der der Fehler aufgetreten ist
	 * @param systemOperatorInfoForItsmTicket Informationsfeld, in dem E-Mail-Adresse und Name des Systembetreuers hinterlegt wird
	 * @return ITSM-Fehlermeldungstext
	 * @throws Exception Exception die bei der Abfrage des localHost, oder bei der Ermittlung der persistenten Pr�fstandsvariable (-> LOCALITY) geworfen wird
	 */
	public String generateItsmMessgeText( String cascadeHostName, String connectionAttempt, String errorText, String prio, String cbbUrl, String systemOperatorInfoForItsmTicket ) throws Exception {
		String itsmMessageText = ""; // ITSM-Fehlertext initial bef�llen

		itsmMessageText = itsmMessageText.concat( "CASCADE Version = " + CascadeProperties.getCascadeVersion() + "\n" );
		itsmMessageText = itsmMessageText.concat( "Hostname = " + cascadeHostName + "\n" );
		itsmMessageText = itsmMessageText.concat( "IP = " + InetAddress.getLocalHost().getHostAddress() + "\n" );
		itsmMessageText = itsmMessageText.concat( "Plant = " + getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "LOCALITY" ).toString() + "\n" );
		itsmMessageText = itsmMessageText.concat( "Category = Certificates\n" );
		itsmMessageText = itsmMessageText.concat( "ClassName = " + this.getClassName() + "\n" );
		itsmMessageText = itsmMessageText.concat( "MethodName = execute\n" );
		if( connectionAttempt != null )
			itsmMessageText = itsmMessageText.concat( "Message = " + (DE ? "Verbindungsversuch: " : "Connection attempt: ") + connectionAttempt + ". " + errorText + "\n" );
		else
			itsmMessageText = itsmMessageText.concat( "Message = " + errorText + "\n" );
		if( prio.equalsIgnoreCase( "CRITICAL" ) )
			itsmMessageText = itsmMessageText.concat( "Priority = CBB CRITICAL ERROR REPORTED BY CASCADE\n" );
		else
			itsmMessageText = itsmMessageText.concat( "Priority = CBB FATAL ERROR REPORTED BY CASCADE\n" );
		itsmMessageText = itsmMessageText.concat( "CBB_URL = " + cbbUrl + "\n" );
		itsmMessageText = itsmMessageText.concat( "Sysop = " + systemOperatorInfoForItsmTicket + "\n" );

		return itsmMessageText;
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {

		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
		boolean bDebugPerform = true; // gibt an, ob eine Performencemessung erfolgen sollen
		boolean bLogObjectFile = false; // gibt an, ob Transport-Objekte zu Dokumentationszwecken in eine Datei ausgeleitet werden sollen
		boolean bEcuListIsBlackList = true; // gibt an, ob es sich bei der ecuList um eine Blacklist handelt
		boolean bFirstCbbFailure = true; // gibt an, ob es sich beim Aufsammeln der CBB-Verbindunsfehler um den ersten Eintrag handelt.
		int cbbUrlRetryAttempt = -1; // gibt an, der wievielte Versuch zum CBB-Verbindungsaufbau gegen eine bestimmte CBB-URL fehlgeschlagen ist
		int status = STATUS_EXECUTION_OK;
		int maxtimeForStatusCheckRetries = 1000; // gibt die maximale Zeit f�r die StatusCheck-Wiederholversuche in msec an
		int timeBetweenStatusCheckRetries = 1000; // gibt die Wartezeit zwischen den StatusCheck-Wiederholversuchen in msec an
		int timeoutBeforeErrorEscalationInCaseOfDelayedCbbResponse = 2000; // gibt die Zeit an, ab der eine versp�tete Antwort auf eine CBB-Bindinganfrage per ITSM-Ticket eskaliert wird 
		int timeoutBeforeTryingNextCbbInCaseOfError = 3000; // gibt die Zeit an, ab der eine bisher fehlende Antwort auf eine CBB-Bindinganfrage zu einem Abbau der https-Verbindung f�hrt und zur n�chsten CBB gewechselt wird
		int totalCbbRetries = 0; // gibt die Anzahl der maximalen CBB-Verbindungsanfragen - 1 an, falls Einzelverbindungsanfragen aufgrund von Timeoutverletzungen scheitern

		ACTIONS action = null;
		Ergebnis result = null;
		PSdZ psdz = null;
		SimpleDateFormat dateFormatXMLFilesOnDisk = new SimpleDateFormat( "yyyyMMdd'_'HHmmss'_'SSS" );
		String bindingRequestFileLocation = null;
		String bindingsFileLocation = "C:\\EC-Apps\\CASCADE\\PSdZ\\Bindings";
		String cbbUrl = null;
		String csrsFileLocation = null;
		String detailStatus = null;
		String errorDescription = DE ? " Fehlerbeschreibung: " : " Error Description: ";
		String errorText = null;
		String faultyBindingRequestFileLocation = null;
		String hintText = "";
		String itsmMessageText = "";
		String itsmMessageTextForEmail = "";
		String networkSharePath4ItsmTicketFiles = null;
		String optionalArgumentName = null;
		String optionalArgumentValue = null;
		String overallStatus = null;
		String requiredArgumentName = null;
		String requiredArgumentValue = null;
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";
		String systemOperatorInfoForItsmTicket = null;
		String testScreenID = this.getPr�flingLaufzeitUmgebung().getName(); // TestScreenID (aus Pr�fstandsinstanz)
		String udActionText = DE ? "Aktion: " : "Action: ";
		String[] cbbUrls = null;
		String[] ecuExcludeList = null;
		String[] ecuExcludeListIpsec = null;
		String[] ecuIncludeList = null;
		String[] ecuList = null;
		String[] emailRecipientsForNotificationInCaseOfItsmTicket = null;
		String[] optionalArgumentsNames = getOptionalArgs();
		String[] optionalArgumentsValues = null;
		String[] requiredArgumentsNames = getRequiredArgs();
		String[] requiredArgumentsValues = null;
		UserDialog userDialog = null;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();

		try {

			// Argumente loggen, einlesen und checken
			try {

				// Ausgabe der Pr�fprozedurversion sowie s�mtlicher an die Pr�fprozedur �bergebener Argumente (required/optional) f�r sp�tere Analysezwecke

				System.out.println( "PSdZ ID:" + testScreenID + ", Initialize " + this.getClassName() );

				for( int i = 0; i < requiredArgumentsNames.length; i++ ) {
					requiredArgumentName = requiredArgumentsNames[i];
					requiredArgumentValue = "";
					// gibt es nicht optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( requiredArgumentName ) != null ) {
						requiredArgumentsValues = extractValues( getArg( requiredArgumentName ) );
						for( int k = 0; k < requiredArgumentsValues.length; k++ ) {
							// Listen von nicht optionalen Argumenten werden durch ";" getrennt ausgegeben
							requiredArgumentValue = requiredArgumentValue + requiredArgumentsValues[k] + ((requiredArgumentsValues.length > 1 && k < (requiredArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						requiredArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZManageCertificates with required argument: " + requiredArgumentName + " = " + requiredArgumentValue );
				}

				for( int i = 0; i < optionalArgumentsNames.length; i++ ) {
					optionalArgumentName = optionalArgumentsNames[i];
					optionalArgumentValue = "";
					// gibt es optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( optionalArgumentName ) != null ) {
						// Sonderbehandlung f�r die Argumente "EMAIL_RECIPIENTS_FOR_NOTIFICATION_IN_CASE_OF_ITSM_TICKET" und "SYSTEM_OPERATOR_INFO_FOR_ITSM_TICKET", die "@"-Zeichen aufgrund E-Mailinformationen enthalten, die nicht durch CASCADE
						// interpretiert werden sollen
						if( optionalArgumentName.equalsIgnoreCase( "EMAIL_RECIPIENTS_FOR_NOTIFICATION_IN_CASE_OF_ITSM_TICKET" ) || optionalArgumentName.equalsIgnoreCase( "SYSTEM_OPERATOR_INFO_FOR_ITSM_TICKET" ) )
							optionalArgumentsValues = new String[] { getArg( optionalArgumentName ) };
						else
							optionalArgumentsValues = extractValues( getArg( optionalArgumentName ) );
						for( int k = 0; k < optionalArgumentsValues.length; k++ ) {
							// Listen von optionalen Argumenten werden durch ";" getrennt ausgegeben
							optionalArgumentValue = optionalArgumentValue + optionalArgumentsValues[k] + ((optionalArgumentsValues.length > 1 && k < (optionalArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						optionalArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZManageCertificates with optional argument: " + optionalArgumentName + " = " + optionalArgumentValue );
				}

				// Obligatorische Argumentpr�fung auf Vorhandensein von nicht optionalen sowie �berz�hligen Argumenten
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// ACTION
				if( extractValues( getArg( "ACTION" ) )[0].equalsIgnoreCase( "READ_CSRS_FROM_ECUS" ) )
					action = ACTIONS.READ_CSRS_FROM_ECUS;
				else if( extractValues( getArg( "ACTION" ) )[0].equalsIgnoreCase( "READ_CSRS_FROM_FILE" ) )
					action = ACTIONS.READ_CSRS_FROM_FILE;
				else if( extractValues( getArg( "ACTION" ) )[0].equalsIgnoreCase( "START_BINDING_REQUEST_TO_CBB" ) )
					action = ACTIONS.START_BINDING_REQUEST_TO_CBB;
				else if( extractValues( getArg( "ACTION" ) )[0].equalsIgnoreCase( "STORE_BINDING_REQUEST_TO_FILE" ) )
					action = ACTIONS.STORE_BINDING_REQUEST_TO_FILE;
				else if( extractValues( getArg( "ACTION" ) )[0].equalsIgnoreCase( "GET_BINDINGS_FROM_CBB" ) )
					action = ACTIONS.GET_BINDINGS_FROM_CBB;
				else if( extractValues( getArg( "ACTION" ) )[0].equalsIgnoreCase( "GET_BINDINGS_FROM_FILE" ) )
					action = ACTIONS.GET_BINDINGS_FROM_FILE;
				else if( extractValues( getArg( "ACTION" ) )[0].equalsIgnoreCase( "CALCULATE_BINDING_DISTRIBUTION" ) )
					action = ACTIONS.CALCULATE_BINDING_DISTRIBUTION;
				else if( extractValues( getArg( "ACTION" ) )[0].equalsIgnoreCase( "WRITE_BINDINGS_AND_OTHERBINDINGS_TO_ECUS" ) )
					action = ACTIONS.WRITE_BINDINGS_AND_OTHERBINDINGS_TO_ECUS;
				else if( extractValues( getArg( "ACTION" ) )[0].equalsIgnoreCase( "START_EXTENSIVE_CERTIFICATE_AND_BINDING_CHECK" ) )
					action = ACTIONS.START_EXTENSIVE_CERTIFICATE_AND_BINDING_CHECK;
				else if( extractValues( getArg( "ACTION" ) )[0].equalsIgnoreCase( "GET_STATUS_EXTENSIVE_CERTIFICATE_AND_BINDING_CHECK" ) )
					action = ACTIONS.GET_STATUS_EXTENSIVE_CERTIFICATE_AND_BINDING_CHECK;
				else
					throw new PPExecutionException( "DEBUG_PERFORM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );

				// BINDING_REQUEST_FILE_LOCATION
				if( getArg( "BINDING_REQUEST_FILE_LOCATION" ) != null ) {
					bindingRequestFileLocation = extractValues( getArg( "BINDING_REQUEST_FILE_LOCATION" ) )[0];
				}

				// BINDINGS_FILE_LOCATION
				if( getArg( "BINDINGS_FILE_LOCATION" ) != null ) {
					bindingsFileLocation = extractValues( getArg( "BINDINGS_FILE_LOCATION" ) )[0];
				}

				// CBB_URLS
				if( getArg( "CBB_URLS" ) != null ) {
					cbbUrls = extractValues( getArg( "CBB_URLS" ) );
				}

				// CSRS_FILE_LOCATION
				if( getArg( "CSRS_FILE_LOCATION" ) != null ) {
					csrsFileLocation = extractValues( getArg( "CSRS_FILE_LOCATION" ) )[0];
				}

				// DIRECTORY_FOR_ITSM_TICKET_FILES
				if( getArg( "DIRECTORY_FOR_ITSM_TICKET_FILES" ) != null ) {
					networkSharePath4ItsmTicketFiles = extractValues( getArg( "DIRECTORY_FOR_ITSM_TICKET_FILES" ) )[0];
				}

				// DEBUG
				if( getArg( "DEBUG" ) != null ) {
					if( extractValues( getArg( "DEBUG" ) )[0].equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( extractValues( getArg( "DEBUG" ) )[0].equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
					else
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DEBUG_PERFORM
				if( getArg( "DEBUG_PERFORM" ) != null ) {
					if( extractValues( getArg( "DEBUG_PERFORM" ) )[0].equalsIgnoreCase( "TRUE" ) )
						bDebugPerform = true;
					else if( extractValues( getArg( "DEBUG_PERFORM" ) )[0].equalsIgnoreCase( "FALSE" ) )
						bDebugPerform = false;
					else
						throw new PPExecutionException( "DEBUG_PERFORM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// ECU_EXCLUDE_LIST
				if( getArg( "ECU_EXCLUDE_LIST" ) != null ) {
					if( getArg( "ECU_INCLUDE_LIST" ) != null )
						throw new PPExecutionException( "EXCLUDE_LIST + INCLUDE_LIST" + PB.getString( "psdz.parameter.ParameterfehlerGegenseitigerAusschluss" ) );
					else
						ecuExcludeList = extractValues( getArg( "ECU_EXCLUDE_LIST" ) );
				}

				// ECU_EXCLUDE_LIST_IPSEC
				if( getArg( "ECU_EXCLUDE_LIST_IPSEC" ) != null ) {
					ecuExcludeListIpsec = extractValues( getArg( "ECU_EXCLUDE_LIST_IPSEC" ) );
				}

				// ECU_INCLUDE_LIST
				if( getArg( "ECU_INCLUDE_LIST" ) != null ) {
					if( getArg( "ECU_EXCLUDE_LIST" ) != null )
						throw new PPExecutionException( "INCLUDE_LIST + EXCLUDE_LIST" + PB.getString( "psdz.parameter.ParameterfehlerGegenseitigerAusschluss" ) );
					else
						ecuIncludeList = extractValues( getArg( "ECU_INCLUDE_LIST" ) );
				}

				// EMAIL_RECIPIENTS_FOR_NOTIFICATION_IN_CASE_OF_ITSM_TICKET
				if( getArg( "EMAIL_RECIPIENTS_FOR_NOTIFICATION_IN_CASE_OF_ITSM_TICKET" ) != null ) {
					emailRecipientsForNotificationInCaseOfItsmTicket = new String[] { getArg( "EMAIL_RECIPIENTS_FOR_NOTIFICATION_IN_CASE_OF_ITSM_TICKET" ) };
				}

				// HINT_TEXT
				if( getArg( "HINT_TEXT" ) != null ) {
					hintText = getArg( "HINT_TEXT" );
				}

				// LOG_OBJECT_FILE
				if( getArg( "LOG_OBJECT_FILE" ) != null ) {
					if( extractValues( getArg( "LOG_OBJECT_FILE" ) )[0].equalsIgnoreCase( "TRUE" ) )
						bLogObjectFile = true;
					else if( extractValues( getArg( "LOG_OBJECT_FILE" ) )[0].equalsIgnoreCase( "FALSE" ) )
						bLogObjectFile = false;
					else
						throw new PPExecutionException( "LOG_OBJECT_FILE " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// MAXTIME_FOR_STATUS_CHECK_RETRIES
				if( getArg( "MAXTIME_FOR_STATUS_CHECK_RETRIES" ) != null ) {
					try {
						maxtimeForStatusCheckRetries = Integer.parseInt( extractValues( getArg( "MAXTIME_FOR_STATUS_CHECK_RETRIES" ) )[0] );
					} catch( Exception e ) {
						throw new PPExecutionException( PB.getString( "psdz.parameter.ungueldigerWert" ) );
					}
				}

				// SYSTEM_OPERATOR_INFO_FOR_ITSM_TICKET
				if( getArg( "SYSTEM_OPERATOR_INFO_FOR_ITSM_TICKET" ) != null ) {
					systemOperatorInfoForItsmTicket = getArg( "SYSTEM_OPERATOR_INFO_FOR_ITSM_TICKET" );
				}

				// TIMEOUT_BEFORE_ERROR_ESCALATION_IN_CASE_OF_DELAYED_CBB_RESPONSE
				if( getArg( "TIMEOUT_BEFORE_ERROR_ESCALATION_IN_CASE_OF_DELAYED_CBB_RESPONSE" ) != null ) {
					try {
						timeoutBeforeErrorEscalationInCaseOfDelayedCbbResponse = Integer.parseInt( extractValues( getArg( "TIMEOUT_BEFORE_ERROR_ESCALATION_IN_CASE_OF_DELAYED_CBB_RESPONSE" ) )[0] );
					} catch( Exception e ) {
						throw new PPExecutionException( PB.getString( "psdz.parameter.ungueldigerWert" ) );
					}
				}

				// TIMEOUT_BEFORE_TRYING_NEXT_CBB_IN_CASE_OF_ERROR
				if( getArg( "TIMEOUT_BEFORE_TRYING_NEXT_CBB_IN_CASE_OF_ERROR" ) != null ) {
					try {
						timeoutBeforeTryingNextCbbInCaseOfError = Integer.parseInt( extractValues( getArg( "TIMEOUT_BEFORE_TRYING_NEXT_CBB_IN_CASE_OF_ERROR" ) )[0] );
					} catch( Exception e ) {
						throw new PPExecutionException( PB.getString( "psdz.parameter.ungueldigerWert" ) );
					}
				}

				// TIME_BETWEEN_STATUS_CHECK_RETRIES
				if( getArg( "TIME_BETWEEN_STATUS_CHECK_RETRIES" ) != null ) {
					try {
						timeBetweenStatusCheckRetries = Integer.parseInt( extractValues( getArg( "TIME_BETWEEN_STATUS_CHECK_RETRIES" ) )[0] );
					} catch( Exception e ) {
						throw new PPExecutionException( PB.getString( "psdz.parameter.ungueldigerWert" ) );
					}
				}

				// TOTAL_CBB_RETRIES
				if( getArg( "TOTAL_CBB_RETRIES" ) != null ) {
					try {
						totalCbbRetries = Integer.parseInt( extractValues( getArg( "TOTAL_CBB_RETRIES" ) )[0] );
						if( totalCbbRetries < 0 )
							throw new NumberFormatException( "Fehler: Wert muss >= 0 sein!" );
					} catch( NumberFormatException nfe ) {
						throw new PPExecutionException( PB.getString( "psdz.parameter.ParameterfehlerTotalCbbRetries" ) );
					} catch( Exception e ) {
						throw new PPExecutionException( PB.getString( "psdz.parameter.ungueldigerWert" ) );
					}
				}

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Performancemessung Start
			if( bDebugPerform )
				System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates PP START" );

			// Devices holen
			try {
				psdz = getPr�flingLaufzeitUmgebung().getDeviceManager().getPSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			try {
				userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Userdialog einblenden
			userDialog.setDisplayProgress( true );
			userDialog.setAllowCancel( true );
			userDialog.getProgressbar().setIndeterminate( true );
			userDialog.setRows( 8 );
			userDialog.setColumns( 28 );

			userDialog.displayMessage( "PSdZ Manage Certificates", PB.getString( "psdz.ud.ManageCertificatesLaeuft" ) + ";" + udActionText + action.toString(), -1 );

			// READ_CSRS_FROM_ECUS

			if( action.equals( ACTIONS.READ_CSRS_FROM_ECUS ) ) {
				try {
					Set<CascadeEcuFailureResponseCto> cascadeEcuFailureResponseCto = null;

					// Bestimmung der ecuList und ecuListIsBlackList
					if( ecuIncludeList != null ) {
						ecuList = ecuIncludeList;
						bEcuListIsBlackList = false;
					} else if( ecuExcludeList != null ) {
						ecuList = ecuExcludeList;
						bEcuListIsBlackList = true;
					} else {
						ecuList = null;
						bEcuListIsBlackList = true;
					}

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.readCertMemoryObjects' START" );
					}

					// Aufruf zum Auslesen der CSRs aus den Steuerger�ten mit Nachfilterung der Rolle "Ipsec"
					cascadeEcuFailureResponseCto = psdz.readCertMemoryObjects( ecuList, bEcuListIsBlackList, ecuExcludeListIpsec, bLogObjectFile );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.readCertMemoryObjects' ENDE" );
					}

					// SG Einzelfehler ausgeben, falls SG-Fehler aufgetreten sind
					if( !cascadeEcuFailureResponseCto.isEmpty() ) {
						HashMap<String, String> psdzECUStatisticsHexStr = new HashMap<String, String>( psdz.getEcuStatisticsHexString() );

						for( CascadeEcuFailureResponseCto failedEcu : cascadeEcuFailureResponseCto ) {
							String sgBaseVariantName = failedEcu.getBaseVariantName();
							String sgDiagAddress = "0x" + Integer.toHexString( failedEcu.getDiagnosticAddress() );
							String responseBehaviour = "";

							// Fehlertext zu SG-Fehler ermitteln
							errorText = failedEcu.getLocalizedMessage() + ", (PSdZ ID: " + failedEcu.getMessageId() + ")";

							// gehe die EcuStatistics-HapMap durch und suche die passende Diagnoseadresse in Hashmap zum fehlerhaften SG
							for( String ecu : psdzECUStatisticsHexStr.keySet() ) {
								if( ecu.equalsIgnoreCase( Integer.toHexString( failedEcu.getDiagnosticAddress() ) ) ) {
									// Treffer! SG Antwortverhalten nur eintragen, wenn SG nie geantwortet hat oder sporadisch									
									if( psdzECUStatisticsHexStr.get( ecu ).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.0" ) ) || psdzECUStatisticsHexStr.get( ecu ).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.1" ) ) )
										responseBehaviour = " (" + PB.getString( "psdz.SG" ) + " " + PB.getString( "psdz.antwortet" ) + " " + psdzECUStatisticsHexStr.get( ecu ) + ")";
									else
										responseBehaviour = "";
								}
							}

							result = new Ergebnis( "PSdZManageCertificates", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText + responseBehaviour, hintText, Ergebnis.FT_NIO );
							ergListe.add( result );
						}
						status = STATUS_EXECUTION_ERROR;
					}

				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					errorText = e.getLocalizedMessage();
					result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler beim Lesen der CSRs aus den Steuerger�ten!" : "Error while reading CSRs from ECUs!") + (errorText != null ? errorDescription + errorText : ""), hintText, Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

			// READ_CSRS_FROM_FILE

			if( action.equals( ACTIONS.READ_CSRS_FROM_FILE ) ) {
				if( csrsFileLocation == null ) {
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Parameter CSRS_FILE_LOCATION " + (DE ? "nicht gesetzt, obwohl dieser f�r die Aktion READ_CSRS_FROM_FILE ben�tigt wird!" : "not set although it is needed for action READ_CSRS_FROM_FILE!"), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}

				try {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.readCertMemoryObjectsFromFile' START" );
					}

					psdz.readCertMemoryObjectsFromFile( csrsFileLocation );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.readCertMemoryObjectsFromFile' ENDE" );
					}
				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					errorText = e.getLocalizedMessage();
					result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler beim Lesen der CSRs aus externer Datei!" : "Error while reading CSRs from external file!") + (errorText != null ? errorDescription + errorText : ""), hintText, Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

			// START_BINDING_REQUEST_TO_CBB

			if( action.equals( ACTIONS.START_BINDING_REQUEST_TO_CBB ) ) {
				if( cbbUrls == null ) {
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Parameter CBB_URLS " + (DE ? "nicht gesetzt, obwohl dieser f�r die Aktion REQUEST_BINDINGS ben�tigt wird!" : "not set although it is needed for action REQUEST_BINDINGS!"), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}

				// �bergebenes Array in Liste �berf�hren wobei 
				// 1. Die Reihenfolge der URLs beibehalten werden soll und
				// 2. Dupplikate entfernt werden sollen
				List<String> clarifiedCbbUrlsList = new ArrayList<String>();

				for( int i = 0; i < cbbUrls.length; i++ ) {
					boolean dupplicateUrlFound = false;
					for( String url : clarifiedCbbUrlsList ) {
						if( url.equalsIgnoreCase( cbbUrls[i] ) ) {
							dupplicateUrlFound = true;
						}
					}

					if( !dupplicateUrlFound ) {
						// ist aktuelle CBB-URL aus cbbUrls in Liste clarifiedCbbUrlsList noch nicht enthalten, dann addiere diese hinzu
						clarifiedCbbUrlsList.add( cbbUrls[i].toString() );
					}
				}

				try {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.requestBindingCalculation' START" );
					}

					psdz.requestBindingCalculation( clarifiedCbbUrlsList, totalCbbRetries, timeoutBeforeTryingNextCbbInCaseOfError );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.requestBindingCalculation' ENDE" );
					}
				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					errorText = e.getLocalizedMessage();
					result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler beim Start der Bindinganfrage an die CBB!" : "Error while starting binding request to CBB!") + (errorText != null ? errorDescription + errorText : ""), hintText, Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

			// STORE_BINDING_REQUEST_TO_FILE

			if( action.equals( ACTIONS.STORE_BINDING_REQUEST_TO_FILE ) ) {
				if( bindingRequestFileLocation == null ) {
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Parameter BINDING_REQUEST_FILE_LOCATION " + (DE ? "nicht gesetzt, obwohl dieser f�r die Ausleitung der CBB-Anfrage in eine Datei ben�tigt wird!" : "not set although it is needed to output the CBB query into a file!"), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}

				try {
					Set<CascadeBindingCalculationFailureCto> cascadeBindingCalculationFailureCto = null;

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.requestBindingCalculationOffline' START" );
					}

					cascadeBindingCalculationFailureCto = psdz.requestBindingCalculationOffline( bindingRequestFileLocation );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.requestBindingCalculationOffline' ENDE" );
					}

					// Verbindungsfehler auswerten und im Fehlerfall ausgeben
					if( !cascadeBindingCalculationFailureCto.isEmpty() ) {
						// gehe alle Verbindungsfehler durch und werfe jeweils einen Fehler
						for( CascadeBindingCalculationFailureCto offlineFailure : cascadeBindingCalculationFailureCto ) {
							faultyBindingRequestFileLocation = offlineFailure.getUrl();
							errorText = offlineFailure.getLocalizedMessage() + ", (PSdZ ID: " + offlineFailure.getMessageId() + ")";

							result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler beim Speichern der Bindinganfrage in die Datei: " : "Error while storing binding request to file: ") + faultyBindingRequestFileLocation + errorDescription + errorText, hintText, Ergebnis.FT_NIO );
							ergListe.add( result );
						}
					}

				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					errorText = e.getLocalizedMessage();
					result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler beim Ausleiten der CBB-Bindinganfrage in eine JSON-Datei!" : "Error while storing the CBB query into a JSON file!") + (errorText != null ? errorDescription + errorText : ""), hintText, Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

			// GET_BINDINGS_FROM_CBB

			if( action.equals( ACTIONS.GET_BINDINGS_FROM_CBB ) ) {
				if( networkSharePath4ItsmTicketFiles == null ) {
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Parameter DIRECTORY_FOR_ITSM_TICKET_FILES " + (DE ? "nicht gesetzt, obwohl dieser f�r die Aktion GET_BINDINGS_FROM_CBB ben�tigt wird!" : "not set although it is needed for action GET_BINDINGS_FROM_CBB!"), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}

				try {
					CascadeFetchBindingCalculationResultCto cascadeFetchBindingCalculationResultCto = null;

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.fetchBindingCalculation' START" );
					}

					cascadeFetchBindingCalculationResultCto = psdz.fetchBindingCalculation( bLogObjectFile );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.fetchBindingCalculation' ENDE" );
					}

					// Potentielle Verbindungsfehler zun�chst durch Indizierung vorsortieren, da diese unsortiert sind...
					Map<Integer, CascadeBindingCalculationFailureCto> cbbConnectionFailuresSorted = new TreeMap<Integer, CascadeBindingCalculationFailureCto>();
					if( !cascadeFetchBindingCalculationResultCto.getConnectionFailures().isEmpty() ) {
						for( CascadeBindingCalculationFailureCto cbbConnectionFailure : cascadeFetchBindingCalculationResultCto.getConnectionFailures() ) {
							cbbConnectionFailuresSorted.put( Integer.valueOf( cbbConnectionFailure.getRetry() ), cbbConnectionFailure );
						}
					}

					//					// Potentielle Konsistenzpr�fungfehler zun�chst durch Indizierung vorsortieren, da diese unsortiert sind...
					//					Map<Integer, CascadeBindingCalculationFailureCto> cbbConsCheckFailuresSorted = new TreeMap<Integer, CascadeBindingCalculationFailureCto>();
					//					if( !cascadeFetchBindingCalculationResultCto.getConsistencyCheckFailures().isEmpty() ) {
					//						for( CascadeBindingCalculationFailureCto cbbConCheckFailure : cascadeFetchBindingCalculationResultCto.getConsistencyCheckFailures() ) {
					//							cbbConsCheckFailuresSorted.put( Integer.valueOf( cbbConCheckFailure.getRetry() ), cbbConCheckFailure );
					//						}
					//					}

					// Progress-Status der Binding-Anfrage pr�fen, falls ungleich "SUCCESS" oder Fehler in CBB-Konsistenzcheck, dann
					// -> Ausgabe aller Verbindungs- und Konsistenzpr�fungsfehler in CASCADE-Fehlerprotokoll(!) 
					if( !cascadeFetchBindingCalculationResultCto.getProgressStatus().getCascadeBindingCalculationProgressStatusTo().equalsIgnoreCase( CascadeBindingCalculationProgressStatusTo.SUCCESS ) || !cascadeFetchBindingCalculationResultCto.getConsistencyCheckFailures().isEmpty() ) {

						// Einzelfehler (CBB-Verbindungsfehler oder Konsistenzpr�fungsfehler) pr�fen und falls Fehler aufgetreten sind diese komplett in CASCADE-Fehlerprotokoll ausgeben
						if( !cascadeFetchBindingCalculationResultCto.getConnectionFailures().isEmpty() || !cascadeFetchBindingCalculationResultCto.getConsistencyCheckFailures().isEmpty() ) {

							// Indizierte Verbindungsversuche sortiert ausgeben
							if( !cascadeFetchBindingCalculationResultCto.getConnectionFailures().isEmpty() ) {
								for( Integer cbbUrlRetryAttemptSorted : cbbConnectionFailuresSorted.keySet() ) {
									cbbUrl = cbbConnectionFailuresSorted.get( cbbUrlRetryAttemptSorted ).getUrl();
									cbbUrlRetryAttempt = cbbUrlRetryAttemptSorted.intValue();
									errorText = cbbConnectionFailuresSorted.get( cbbUrlRetryAttemptSorted ).getLocalizedMessage() + ", (PSdZ ID: " + cbbConnectionFailuresSorted.get( cbbUrlRetryAttemptSorted ).getMessageId() + ")";
									result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler beim Abholen der Bindingergebnisse von CBB! CBB-URL: " : "Error while getting binding results from CBB! CBB-URL: ") + cbbUrl + (DE ? ", Verbindungsversuch: " : ", connection attempt: ") + (cbbUrlRetryAttempt + 1) + "," + errorDescription + errorText, hintText, Ergebnis.FT_NIO );
									ergListe.add( result );
								}
							}

							// Konsistenzpr�fungsfehler unsortiert ausgeben
							if( !cascadeFetchBindingCalculationResultCto.getConsistencyCheckFailures().isEmpty() ) {
								for( CascadeBindingCalculationFailureCto cbbConsCheckFailure : cascadeFetchBindingCalculationResultCto.getConsistencyCheckFailures() ) {
									cbbUrl = cbbConsCheckFailure.getUrl();
									cbbUrlRetryAttempt = cbbConsCheckFailure.getRetry();
									errorText = cbbConsCheckFailure.getLocalizedMessage() + ", (PSdZ ID: " + cbbConsCheckFailure.getMessageId() + ")";
									result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Konsistenzpr�fung hat Fehler in Bindingergebnissen von CBB entdeckt! CBB-URL: " : "Consistency check found errors in binding results from CBB! CBB-URL: ") + cbbUrl + (DE ? ", Verbindungsversuch: " : ", connection attempt: ") + (cbbUrlRetryAttempt + 1) + "," + errorDescription + errorText, hintText, Ergebnis.FT_NIO );
									ergListe.add( result );
								}
								//								for( Integer cbbUrlRetryAttemptSorted : cbbConsCheckFailuresSorted.keySet() ) {
								//									cbbUrl = cbbConsCheckFailuresSorted.get( cbbUrlRetryAttemptSorted ).getUrl();
								//									cbbUrlRetryAttempt = cbbUrlRetryAttemptSorted.intValue();
								//									errorText = cbbConsCheckFailuresSorted.get( cbbUrlRetryAttemptSorted ).getLocalizedMessage() + ", (PSdZ ID: " + cbbConsCheckFailuresSorted.get( cbbUrlRetryAttemptSorted ).getMessageId() + ")";
								//									result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Konsistenzpr�fung hat Fehler in Bindingergebnissen von CBB entdeckt! CBB-URL: " : "Consistency check found errors in binding results from CBB! CBB-URL: ") + cbbUrl + (DE ? ", Verbindungsversuch: " : ", connection attempt: ") + (cbbUrlRetryAttempt + 1) + "," + errorDescription + errorText, hintText, Ergebnis.FT_NIO );
								//									ergListe.add( result );
								//								}
							}
						} else {
							result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler beim Abholen der Bindingergebnisse von CBB! Fortschrittstatus: " : "Error while getting binding results from CBB! Progress status: ") + cascadeFetchBindingCalculationResultCto.getProgressStatus().getCascadeBindingCalculationProgressStatusTo(), hintText, Ergebnis.FT_NIO );
							ergListe.add( result );
						}
						status = STATUS_EXECUTION_ERROR;
					}

					// im i.O. Fall (-> SUCCESS) UNABH�NGIG vom Fehlern im Konsistenzcheck die Zeitdauer der letzten CBB-Anfrage in APDM loggen
					if( cascadeFetchBindingCalculationResultCto.getProgressStatus().getCascadeBindingCalculationProgressStatusTo().equalsIgnoreCase( CascadeBindingCalculationProgressStatusTo.SUCCESS ) ) {

						result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "CBB-URL:=" + cascadeFetchBindingCalculationResultCto.getUrlOfLastRequest(), "", "", "CBB response time of last (successful) CBB request [msec]", Integer.toString( cascadeFetchBindingCalculationResultCto.getDurationOfLastRequest() ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );

						// im i.O. Fall zus�ztlich pr�fen, ob ggf. Timeoutverletzung vorliegt und wenn ja diese per ITSM-Ticket(!) und per E-Mail r�ckmelden

						// pr�fe auf Timing-Verletzung zu letztem CBB-Verbindungsaufbau und erzeuge ggf. ein ITSM-Ticket	
						if( cascadeFetchBindingCalculationResultCto.getDurationOfLastRequest() > timeoutBeforeErrorEscalationInCaseOfDelayedCbbResponse ) {

							// Erzeuge ITSM-Ticket (PRIO CRITICAL) �ber Dateischnittstelle
							File filePath4ItsmTicketFiles = new File( networkSharePath4ItsmTicketFiles, dateFormatXMLFilesOnDisk.format( new Date( System.currentTimeMillis() ) ) + "_CBB_" + psdz.getCascadeHostName() + ".txt" );

							// DIR eventuell erzeugen inklusive SubDIRs
							if( !filePath4ItsmTicketFiles.getParentFile().exists() )
								filePath4ItsmTicketFiles.getParentFile().mkdirs();

							// Meldetext aufbereiten							
							errorText = DE ? "Die Berechnung der Bindings wurde in der vorgegebenen Zeit von " + timeoutBeforeErrorEscalationInCaseOfDelayedCbbResponse + " ms nicht abgeschlossen!" : "The calculation of bindings was not processed within the given time frame of " + timeoutBeforeErrorEscalationInCaseOfDelayedCbbResponse + "ms!";

							itsmMessageText = generateItsmMessgeText( psdz.getCascadeHostName(), null, errorText, "CRITICAL", cascadeFetchBindingCalculationResultCto.getUrlOfLastRequest(), systemOperatorInfoForItsmTicket );

							// Daten rausschreiben
							if( !filePath4ItsmTicketFiles.exists() || (filePath4ItsmTicketFiles.exists() && filePath4ItsmTicketFiles.canWrite()) ) {
								BufferedWriter out = new BufferedWriter( new FileWriter( filePath4ItsmTicketFiles ) );
								out.write( itsmMessageText );
								out.close();
							}

							// E-Mail mit CBB-Timeoutverletzung via RMI schicken, jedoch nur wenn eine E-Mail-Addresse parametriert wurde
							if( emailRecipientsForNotificationInCaseOfItsmTicket != null ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", sending email to " + Arrays.asList( emailRecipientsForNotificationInCaseOfItsmTicket ).toString() + " via RMI..." );
								getPr�flingLaufzeitUmgebung().sendEmailviaRMI( Arrays.asList( emailRecipientsForNotificationInCaseOfItsmTicket ), DE ? "Timeout-Verletzung w�hrend CBB-Anfrage aufgetreten!" : "Timeout violation occured during CBB request!", itsmMessageText );
								System.out.println( "PSdZ ID:" + testScreenID + ", sending email via RMI finished" );
							}
						}
					}

					// Einzelfehler (CBB-Verbindungsfehler) generell pr�fen und falls Fehler aufgetreten sind diese per ITSM-Ticket (jedoch nur den ersten)(!) und per E-Mail r�ckmelden
					if( !cascadeFetchBindingCalculationResultCto.getConnectionFailures().isEmpty() ) {

						bFirstCbbFailure = true; // Flag zur�cksetzen !

						// gehe alle Verbindungsfehler durch und erzeuge jeweils ein Ticket	
						for( Integer cbbUrlRetryAttemptSorted : cbbConnectionFailuresSorted.keySet() ) {

							// ITSM-Fehlertext zur�cksetzen (f�r jeden Schleifendurchlauf erneut)!
							itsmMessageText = "";

							// Erzeuge ITSM-Ticket (PRIO FATAL) �ber Dateischnittstelle
							File filePathItsmTicketFiles = new File( networkSharePath4ItsmTicketFiles, dateFormatXMLFilesOnDisk.format( new Date( System.currentTimeMillis() ) ) + "_CBB_" + psdz.getCascadeHostName() + ".txt" );

							// DIR eventuell erzeugen inklusive SubDIRs
							if( !filePathItsmTicketFiles.getParentFile().exists() )
								filePathItsmTicketFiles.getParentFile().mkdirs();

							// Meldetext aufbereiten
							cbbUrl = cbbConnectionFailuresSorted.get( cbbUrlRetryAttemptSorted ).getUrl();
							cbbUrlRetryAttempt = cbbUrlRetryAttemptSorted.intValue();
							errorText = cbbConnectionFailuresSorted.get( cbbUrlRetryAttemptSorted ).getLocalizedMessage() + ", (PSdZ ID: " + cbbConnectionFailuresSorted.get( cbbUrlRetryAttemptSorted ).getMessageId() + ")";

							itsmMessageText = generateItsmMessgeText( psdz.getCascadeHostName(), Integer.toString( cbbUrlRetryAttempt + 1 ), errorText, "FATAL", cbbUrl, systemOperatorInfoForItsmTicket );

							// Daten rausschreiben, jedoch nur von erstem Fehler!
							if( !filePathItsmTicketFiles.exists() || (filePathItsmTicketFiles.exists() && filePathItsmTicketFiles.canWrite()) && bFirstCbbFailure ) {
								BufferedWriter out = new BufferedWriter( new FileWriter( filePathItsmTicketFiles ) );
								out.write( itsmMessageText );
								out.close();
							}
							bFirstCbbFailure = false;
							itsmMessageTextForEmail = itsmMessageTextForEmail + itsmMessageText.concat( "\n\n" );
						}

						// E-Mail mit gesammelten CBB-Verbindungsfehlern via RMI schicken, jedoch nur wenn eine E-Mail-Addresse parametriert wurde
						if( emailRecipientsForNotificationInCaseOfItsmTicket != null ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", sending email to " + Arrays.asList( emailRecipientsForNotificationInCaseOfItsmTicket ).toString() + " via RMI..." );
							getPr�flingLaufzeitUmgebung().sendEmailviaRMI( Arrays.asList( emailRecipientsForNotificationInCaseOfItsmTicket ), DE ? "CBB Verbindungsfehler aufgetreten!" : "CBB connection failure(s) occured!", itsmMessageTextForEmail );
							System.out.println( "PSdZ ID:" + testScreenID + ", sending email via RMI finished" );
						}

					}

					// Einzelfehler (CBB-Konsistenzpr�fungsfehler) generell pr�fen und falls Fehler aufgetreten sind diese per ITSM-Ticket (alle Fehler)(!) und per E-Mail r�ckmelden
					if( !cascadeFetchBindingCalculationResultCto.getConsistencyCheckFailures().isEmpty() ) {

						// gehe alle Konsistenzpr�fungsfehler durch und erzeuge jeweils ein Ticket	

						//for( Integer cbbUrlRetryAttemptSorted : cbbConsCheckFailuresSorted.keySet() ) {
						for( CascadeBindingCalculationFailureCto cbbConCheckFailure : cascadeFetchBindingCalculationResultCto.getConsistencyCheckFailures() ) {

							// ITSM-Fehlertext zur�cksetzen (f�r jeden Schleifendurchlauf erneut)!
							itsmMessageText = "";

							// Erzeuge ITSM-Ticket (PRIO FATAL) �ber Dateischnittstelle
							File filePathItsmTicketFiles = new File( networkSharePath4ItsmTicketFiles, dateFormatXMLFilesOnDisk.format( new Date( System.currentTimeMillis() ) ) + "_CBB_" + psdz.getCascadeHostName() + ".txt" );

							// DIR eventuell erzeugen inklusive SubDIRs
							if( !filePathItsmTicketFiles.getParentFile().exists() )
								filePathItsmTicketFiles.getParentFile().mkdirs();

							// JAVA ist schnell, pr�fe deshalb ob es das File eventuell schon gibt und warte dann 10 ms, um ein seperates File zu erzeugen
							while( filePathItsmTicketFiles.exists() ) {
								try {
									Thread.sleep( 10 );
								} catch( InterruptedException e ) {
								}

								filePathItsmTicketFiles = new File( networkSharePath4ItsmTicketFiles, dateFormatXMLFilesOnDisk.format( new Date( System.currentTimeMillis() ) ) + "_CBB_" + psdz.getCascadeHostName() + ".txt" );
							}

							// Meldetext aufbereiten
							//							cbbUrl = cbbConsCheckFailuresSorted.get( cbbUrlRetryAttemptSorted ).getUrl();
							//							cbbUrlRetryAttempt = cbbUrlRetryAttemptSorted.intValue();
							//							errorText = cbbConsCheckFailuresSorted.get( cbbUrlRetryAttemptSorted ).getLocalizedMessage() + ", (PSdZ ID: " + cbbConsCheckFailuresSorted.get( cbbUrlRetryAttemptSorted ).getMessageId() + ")";
							cbbUrl = cbbConCheckFailure.getUrl();
							cbbUrlRetryAttempt = cbbConCheckFailure.getRetry();
							errorText = cbbConCheckFailure.getLocalizedMessage() + ", (PSdZ ID: " + cbbConCheckFailure.getMessageId() + ")";

							itsmMessageText = generateItsmMessgeText( psdz.getCascadeHostName(), Integer.toString( cbbUrlRetryAttempt + 1 ), errorText, "FATAL", cbbUrl, systemOperatorInfoForItsmTicket );

							// Daten rausschreiben, jedoch nur von erstem Fehler!
							if( !filePathItsmTicketFiles.exists() || (filePathItsmTicketFiles.exists() && filePathItsmTicketFiles.canWrite()) ) {
								BufferedWriter out = new BufferedWriter( new FileWriter( filePathItsmTicketFiles ) );
								out.write( itsmMessageText );
								out.close();
							}
							itsmMessageTextForEmail = itsmMessageTextForEmail + itsmMessageText.concat( "\n\n" );
						}

						// E-Mail mit gesammelten CBB-Verbindungsfehlern via RMI schicken, jedoch nur wenn eine E-Mail-Addresse parametriert wurde
						if( emailRecipientsForNotificationInCaseOfItsmTicket != null ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", sending email to " + Arrays.asList( emailRecipientsForNotificationInCaseOfItsmTicket ).toString() + " via RMI..." );
							getPr�flingLaufzeitUmgebung().sendEmailviaRMI( Arrays.asList( emailRecipientsForNotificationInCaseOfItsmTicket ), DE ? "CBB Konsistenzpr�fungsfehler aufgetreten!" : "CBB consistency check failure(s) occured!", itsmMessageTextForEmail );
							System.out.println( "PSdZ ID:" + testScreenID + ", sending email via RMI finished" );
						}

					}

					// Bindingcontainereintr�ge aus der CBB-R�ckantwort f�r jedes Steuerger�t durchgehen und Einzelzertifikats- und Bindingsfehler ausgeben (Fehlerprotokoll und APDM)
					for( CascadeCertCalculatedObjectCto cascadeCertCalculatedObjectCto : cascadeFetchBindingCalculationResultCto.getCalculatedBindings() ) {

						String sgBaseVariantName = cascadeCertCalculatedObjectCto.getBaseVariantName();
						String sgDiagAddress = "0x" + Integer.toHexString( cascadeCertCalculatedObjectCto.getDiagnosticAddress() );

						// Gesamt- und Detailstatus-Fehler ausgeben
						if( !cascadeCertCalculatedObjectCto.getOverallStatus().getStatusName().equalsIgnoreCase( CascadeCertCalculationOverallStatusEto.OK ) ) {
							// Fehlertext zu Gesamtstatus-Fehler ermitteln
							overallStatus = cascadeCertCalculatedObjectCto.getOverallStatus().getStatusName();
							errorText = cascadeCertCalculatedObjectCto.getOverallStatus().getLocalizedMessage();

							result = new Ergebnis( "PSdZManageCertificates", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Gesamtstatus zum ermittelten Binding-Container fehlerhaft! Gesamtstatus: " : "Overall status of requested binding container not okay! Overall status: ") + overallStatus + "," + errorDescription + errorText, hintText, Ergebnis.FT_NIO );
							ergListe.add( result );

							// Detailstatus-Fehler ausgeben
							for( String certRole : cascadeCertCalculatedObjectCto.getRoleStatuses().keySet() ) {
								if( !cascadeCertCalculatedObjectCto.getRoleStatuses().get( certRole ).getStatusName().equalsIgnoreCase( CascadeCertCalculationDetailedStatusEto.OK ) ) {
									// Fehlertext zu Detailstatus-Fehler ermitteln
									detailStatus = cascadeCertCalculatedObjectCto.getRoleStatuses().get( certRole ).getStatusName();
									errorText = cascadeCertCalculatedObjectCto.getRoleStatuses().get( certRole ).getLocalizedMessage();

									result = new Ergebnis( "PSdZManageCertificates", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Detailstatus zum ermittelten Binding fehlerhaft! CSR/Zertifikate-Rolle: " : "Detail status of requested binding not okay! CSR/certificate role: ") + certRole + (DE ? ", Detailstatus: " : ", Detail status: ") + detailStatus + "," + errorDescription + errorText, hintText, Ergebnis.FT_NIO );
									ergListe.add( result );
								}
							}
							status = STATUS_EXECUTION_ERROR;
						}
					}

				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					errorText = e.getLocalizedMessage();
					result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler beim Abholen der Bindingergebnisse von CBB!" : "Error while getting binding results from CBB!") + (errorText != null ? errorDescription + errorText : ""), hintText, Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

			// GET_BINDINGS_FROM_FILE

			if( action.equals( ACTIONS.GET_BINDINGS_FROM_FILE ) ) {
				File pathToBindingsFileLocation = new File( bindingsFileLocation );
				File bindingFilePathName = null;
				File[] files = pathToBindingsFileLocation.listFiles();
				File finalVinFile = null;
				boolean isDirectory = true;

				// Exisitiert Datei/Verzeichnis ?
				if( !pathToBindingsFileLocation.exists() ) {
					// Datei oder Verzeichnis existiert nicht!
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), (DE ? "Datei oder Dateipfad " : "File or file path ") + bindingsFileLocation + (DE ? " existiert nicht!" : " does not exist!"), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				} else {
					if( pathToBindingsFileLocation.isDirectory() ) {
						// es handelt sich um ein Verzeichnis!
					} else {
						// es handelt sich um eine Datei!
						isDirectory = false;
					}
				}

				try {
					CascadeFetchBindingCalculationResultCto cascadeFetchBindingCalculationResultCto = null;

					// Ermittlung der einzulesenden Datei �ber den Dateinamen
					// Wird statt dessen nur ein Dateipfad �bergeben, wird das Verzeichnis nach Dateien durchsucht, die eine passende VIN im Dateinamen enthalten.
					// Existieren mehrere Dateien mit der passenden VIN im Dateiname wird die Datei mit dem j�ngsten �nderungsdatum herangezogen.
					if( !isDirectory ) {
						bindingFilePathName = new File( bindingsFileLocation );
					} else {
						// Datei in Verzeichnis suchen, die zu der aktuellen VIN aus dem Auftrag passt. Existiert mehr als eine Datei zu einer VIN, wird die bzgl. des �nderungsdatums j�ngste Datei angezogen
						if( psdz.getVehicleVin() == null )
							// vehicleVin wurde nicht bef�llt z.B. aufgrund von fehlerhaftem Auftragsdatensatz
							throw new CascadePSdZConditionsNotCorrectException( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.NoVinAvailable" ) );

						// suche Dateien, die VIN im Namen enthalten
						Set<File> vinFilteredFiles = new HashSet<File>();
						for( int i = 0; i < files.length; i++ ) {
							if( files[i].getName().toUpperCase().contains( psdz.getVehicleVin().substring( 10, 17 ).toUpperCase() ) ) {
								vinFilteredFiles.add( files[i] );
							}
						}
						if( vinFilteredFiles.isEmpty() ) {
							// keine JSON-Datei mit passender VIN im Dateinamen gefunden
							throw new CascadePSdZConditionsNotCorrectException( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.NoMatchingJsonFileFound" ) );
						} else {
							// eine oder mehrere Dateien mit VIN im Dateinamen gefunden
							finalVinFile = vinFilteredFiles.iterator().next();
							for( File searchedFile : vinFilteredFiles ) {
								if( searchedFile.lastModified() > finalVinFile.lastModified() ) {
									finalVinFile = searchedFile;
								}
							}

							// finalen Namen der JSON-Datei setzen
							bindingFilePathName = finalVinFile;
						}
					}

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.fetchBindingCalculationOffline' START" );
					}

					cascadeFetchBindingCalculationResultCto = psdz.fetchBindingCalculationOffline( bindingFilePathName, bLogObjectFile );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.fetchBindingCalculationOffline' ENDE" );
					}

					// Einzelfehler, die beim Einlesen der Datei aufgetreten sind werden hier gepr�ft

					// Potentielle Fehler zun�chst durch Indizierung vorsortieren, da diese unsortiert sind...
					Map<Integer, CascadeBindingCalculationFailureCto> cbbConnectionFailuresSorted = new TreeMap<Integer, CascadeBindingCalculationFailureCto>();
					if( !cascadeFetchBindingCalculationResultCto.getConnectionFailures().isEmpty() ) {
						for( CascadeBindingCalculationFailureCto cbbConnectionFailure : cascadeFetchBindingCalculationResultCto.getConnectionFailures() ) {
							cbbConnectionFailuresSorted.put( Integer.valueOf( cbbConnectionFailure.getRetry() ), cbbConnectionFailure );
						}
					}

					// Progress-Status der Binding-Anfrage pr�fen, falls ungleich "SUCCESS" oder Fehler in CBB-Konsistenzcheck, dann 
					// -> Ausgabe aller "Verbindungs-" und Konsistenzpr�fungsfehler in CASCADE-Fehlerprotokoll(!) 
					if( !cascadeFetchBindingCalculationResultCto.getProgressStatus().getCascadeBindingCalculationProgressStatusTo().equalsIgnoreCase( CascadeBindingCalculationProgressStatusTo.SUCCESS ) || !cascadeFetchBindingCalculationResultCto.getConsistencyCheckFailures().isEmpty() ) {

						// Einzelfehler (CBB-Verbindungsfehler oder Konsistenzpr�fungsfehler) pr�fen und falls Fehler aufgetreten sind diese komplett in CASCADE-Fehlerprotokoll ausgeben
						if( !cascadeFetchBindingCalculationResultCto.getConnectionFailures().isEmpty() || !cascadeFetchBindingCalculationResultCto.getConsistencyCheckFailures().isEmpty() ) {

							// Indizierte Versuche sortiert ausgeben
							if( !cascadeFetchBindingCalculationResultCto.getConnectionFailures().isEmpty() ) {
								for( Integer cbbUrlRetryAttemptSorted : cbbConnectionFailuresSorted.keySet() ) {
									cbbUrl = cbbConnectionFailuresSorted.get( cbbUrlRetryAttemptSorted ).getUrl();
									cbbUrlRetryAttempt = cbbUrlRetryAttemptSorted.intValue();
									errorText = cbbConnectionFailuresSorted.get( cbbUrlRetryAttemptSorted ).getLocalizedMessage() + ", (PSdZ ID: " + cbbConnectionFailuresSorted.get( cbbUrlRetryAttemptSorted ).getMessageId() + ")";
									result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler beim Einlesen der Bindingergebnisse aus Datei! Pfadname: " : "Error while reading binding results from file! File path: ") + cbbUrl + (DE ? ", Leseversuch: " : ", reading attempt: ") + (cbbUrlRetryAttempt + 1) + "," + errorDescription + errorText, hintText, Ergebnis.FT_NIO );
									ergListe.add( result );
								}
							}

							// Konsistenzpr�fungsfehler unsortiert ausgeben
							if( !cascadeFetchBindingCalculationResultCto.getConsistencyCheckFailures().isEmpty() ) {
								for( CascadeBindingCalculationFailureCto cbbConsCheckFailure : cascadeFetchBindingCalculationResultCto.getConsistencyCheckFailures() ) {
									cbbUrl = cbbConsCheckFailure.getUrl();
									cbbUrlRetryAttempt = cbbConsCheckFailure.getRetry();
									errorText = cbbConsCheckFailure.getLocalizedMessage() + ", (PSdZ ID: " + cbbConsCheckFailure.getMessageId() + ")";
									result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Konsistenzpr�fung hat Fehler in Bindingergebnissen von CBB entdeckt! Eingelesene Datei: " : "Consistency check found errors in binding results from CBB! Read file: ") + cbbUrl + (DE ? ", Einleseversuch: " : ", reading attempt: ") + (cbbUrlRetryAttempt + 1) + "," + errorDescription + errorText, hintText, Ergebnis.FT_NIO );
									ergListe.add( result );
								}
							}
						} else {
							result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler beim Lesen der Bindingergebnisse aus Datei! Ergebnisstatus: " : "Error while reading binding results from file! Result status: ") + cascadeFetchBindingCalculationResultCto.getProgressStatus().getCascadeBindingCalculationProgressStatusTo(), hintText, Ergebnis.FT_NIO );
							ergListe.add( result );
						}
						status = STATUS_EXECUTION_ERROR;
					}

					// Bindingcontainereintr�ge aus der CBB-R�ckantwort f�r jedes Steuerger�t durchgehen und Einzelzertifikats- und Bindingsfehler ausgeben (Fehlerprotokoll und APDM)
					for( CascadeCertCalculatedObjectCto cascadeCertCalculatedObjectCto : cascadeFetchBindingCalculationResultCto.getCalculatedBindings() ) {

						String sgBaseVariantName = cascadeCertCalculatedObjectCto.getBaseVariantName();
						String sgDiagAddress = "0x" + Integer.toHexString( cascadeCertCalculatedObjectCto.getDiagnosticAddress() );

						// Gesamt- und Detailstatus-Fehler ausgeben
						if( !cascadeCertCalculatedObjectCto.getOverallStatus().getStatusName().equalsIgnoreCase( CascadeCertCalculationOverallStatusEto.OK ) ) {
							// Fehlertext zu Gesamtstatus-Fehler ermitteln
							overallStatus = cascadeCertCalculatedObjectCto.getOverallStatus().getStatusName();
							errorText = cascadeCertCalculatedObjectCto.getOverallStatus().getLocalizedMessage();

							result = new Ergebnis( "PSdZManageCertificates", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Gesamtstatus zum ermittelten Binding fehlerhaft! Gesamtstatus: " : "Overall status of requested binding not okay! Overall status: ") + overallStatus + "," + errorDescription + errorText, hintText, Ergebnis.FT_NIO );
							ergListe.add( result );

							// Detailstatus-Fehler ausgeben
							for( String certRole : cascadeCertCalculatedObjectCto.getRoleStatuses().keySet() ) {
								if( !cascadeCertCalculatedObjectCto.getRoleStatuses().get( certRole ).getStatusName().equalsIgnoreCase( CascadeCertCalculationDetailedStatusEto.OK ) ) {
									// Fehlertext zu Detailstatus-Fehler ermitteln
									detailStatus = cascadeCertCalculatedObjectCto.getRoleStatuses().get( certRole ).getStatusName();
									errorText = cascadeCertCalculatedObjectCto.getRoleStatuses().get( certRole ).getLocalizedMessage();

									result = new Ergebnis( "PSdZManageCertificates", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Detailstatus zum ermittelten Binding fehlerhaft! CSR/Zertifikate-Rolle: " : "Detail status of requested binding not okay! CSR/certificate role: ") + certRole + (DE ? ", Detailstatus: " : ", Detail status: ") + detailStatus + "," + errorDescription + errorText, hintText, Ergebnis.FT_NIO );
									ergListe.add( result );
								}
							}
							status = STATUS_EXECUTION_ERROR;
						}
					}

				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					// wenn Dateiname ermittelt werden konnte folgt Ausgabe der Fehlermeldung mit Dateiname, sonst Ausgabe mit �bergebenem Dateipfad
					if( bindingFilePathName != null )
						errorText = (DE ? "Fehler beim Einlesen der CBB-Bindingantwort aus einer JSON-Datei! Pfadname: " : "Error while reading the CBB response from a JSON file! File path: ") + bindingFilePathName + "," + errorDescription + e.getLocalizedMessage();
					else
						errorText = (DE ? "Fehler beim Einlesen der CBB-Bindingantwort aus einer JSON-Datei! Suchpfad: " : "Error while reading the CBB response from a JSON file! Searched path: ") + bindingsFileLocation + "," + errorDescription + e.getLocalizedMessage();
					result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText, hintText, Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

			// CALCULATE_BINDING_DISTRIBUTION

			if( action.equals( ACTIONS.CALCULATE_BINDING_DISTRIBUTION ) ) {

				try {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.calculateBindingDistribution' START" );
					}

					psdz.calculateBindingDistribution( bLogObjectFile );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.calculateBindingDistribution' ENDE" );
					}
				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					errorText = e.getLocalizedMessage();
					result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler bei der Berechnung der Binding-Verteilung (other bindings)!" : "Error while calculation of binding distribution (other bindings)!") + (errorText != null ? errorDescription + errorText : ""), hintText, Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

			// WRITE_BINDINGS_AND_OTHERBINDINGS_TO_ECUS

			if( action.equals( ACTIONS.WRITE_BINDINGS_AND_OTHERBINDINGS_TO_ECUS ) ) {

				try {
					Set<CascadeEcuFailureResponseCto> cascadeEcuFailureResponseCto = null;

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.writeCertMemoryObjects' START" );
					}

					// Aufruf zum Schreiben der Bindings und OtherBindings in die Steuerger�te
					cascadeEcuFailureResponseCto = psdz.writeCertMemoryObjects();

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.writeCertMemoryObjects' ENDE" );
					}

					// SG Einzelfehler ausgeben, falls SG-Fehler aufgetreten sind
					if( !cascadeEcuFailureResponseCto.isEmpty() ) {
						HashMap<String, String> psdzECUStatisticsHexStr = new HashMap<String, String>( psdz.getEcuStatisticsHexString() );

						for( CascadeEcuFailureResponseCto failedEcu : cascadeEcuFailureResponseCto ) {
							String sgBaseVariantName = failedEcu.getBaseVariantName();
							String sgDiagAddress = "0x" + Integer.toHexString( failedEcu.getDiagnosticAddress() );
							String responseBehaviour = "";

							// Fehlertext zu SG-Fehler ermitteln
							errorText = failedEcu.getLocalizedMessage() + ", (PSdZ ID: " + failedEcu.getMessageId() + ")";

							// gehe die EcuStatistics-HapMap durch und suche die passende Diagnoseadresse in Hashmap zum fehlerhaften SG
							for( String ecu : psdzECUStatisticsHexStr.keySet() ) {
								if( ecu.equalsIgnoreCase( Integer.toHexString( failedEcu.getDiagnosticAddress() ) ) ) {
									// Treffer! SG Antwortverhalten nur eintragen, wenn SG nie geantwortet hat oder sporadisch									
									if( psdzECUStatisticsHexStr.get( ecu ).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.0" ) ) || psdzECUStatisticsHexStr.get( ecu ).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.1" ) ) )
										responseBehaviour = " (" + PB.getString( "psdz.SG" ) + " " + PB.getString( "psdz.antwortet" ) + " " + psdzECUStatisticsHexStr.get( ecu ) + ")";
									else
										responseBehaviour = "";
								}
							}

							result = new Ergebnis( "PSdZManageCertificates", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText + responseBehaviour, hintText, Ergebnis.FT_NIO );
							ergListe.add( result );
						}
						status = STATUS_EXECUTION_ERROR;
					}

				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					errorText = e.getLocalizedMessage();
					result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler beim Schreiben der Bindings und OtherBindings in Steuerger�te!" : "Error while writing bindings and other bindings to ECUs!") + (errorText != null ? errorDescription + errorText : ""), hintText, Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

			// START_EXTENSIVE_CERTIFICATE_AND_BINDING_CHECK
			if( action.equals( ACTIONS.START_EXTENSIVE_CERTIFICATE_AND_BINDING_CHECK ) ) {
				CascadeRequestEcuCertCheckingResultCto cascadeRequestEcuCertCheckingResultCto = null;
				int worstCheckTimeOfSlowestEcu = -1;

				try {

					// Bestimmung der ecuList und ecuListIsBlackList
					if( ecuIncludeList != null ) {
						ecuList = ecuIncludeList;
						bEcuListIsBlackList = false;
					} else if( ecuExcludeList != null ) {
						ecuList = ecuExcludeList;
						bEcuListIsBlackList = true;
					} else {
						ecuList = null;
						bEcuListIsBlackList = true;
					}

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.requestEcuCertChecking' START" );
					}

					cascadeRequestEcuCertCheckingResultCto = psdz.requestEcuCertChecking( ecuList, bEcuListIsBlackList );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.requestEcuCertChecking' ENDE" );
					}

					// Schlechtesten Wert aus allen WorstCaseCheckTimes ermitteln und WorstCaseCheckTimes aller Steuerger�te in APDM dokumentieren, die geantwortet haben
					if( !cascadeRequestEcuCertCheckingResultCto.getEcuCertCheckingMaxWaitingTimes().isEmpty() ) {
						for( CascadeEcuCertCheckingMaxWaitingTime ecuWorstCaseCheckTimes : cascadeRequestEcuCertCheckingResultCto.getEcuCertCheckingMaxWaitingTimes() ) {
							String sgBaseVariantName = ecuWorstCaseCheckTimes.getBaseVariantName();
							String sgDiagAddress = "0x" + Integer.toHexString( ecuWorstCaseCheckTimes.getDiagnosticAddress() );
							int sgWorstCheckTime = ecuWorstCaseCheckTimes.getWorstCaseCheckTime();

							if( worstCheckTimeOfSlowestEcu < sgWorstCheckTime )
								worstCheckTimeOfSlowestEcu = sgWorstCheckTime;

							// WorstCaseCheckTimes aller Steuerger�te in APDM dokumentieren
							result = new Ergebnis( "PSdZManageCertificates", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, "", "", "WorstCaseCheckTime [msec]", Integer.toString( sgWorstCheckTime ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						}
					}

					// WorstCaseCheckTime des langsamsten SGes als Result mit Result-ID "WorstCaseCheckTimeOfSlowestEcu" zur�ckgeben
					result = new Ergebnis( "WorstCaseCheckTimeOfSlowestEcu", "", "", "", "", "WorstCaseCheckTimeOfSlowestEcu", Integer.toString( worstCheckTimeOfSlowestEcu ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );

					// SG Einzelfehler ausgeben, falls SG-Fehler aufgetreten sind
					if( !cascadeRequestEcuCertCheckingResultCto.getFailedEcus().isEmpty() ) {
						HashMap<String, String> psdzECUStatisticsHexStr = new HashMap<String, String>( psdz.getEcuStatisticsHexString() );

						for( CascadeEcuFailureResponseCto failedEcu : cascadeRequestEcuCertCheckingResultCto.getFailedEcus() ) {
							String sgBaseVariantName = failedEcu.getBaseVariantName();
							String sgDiagAddress = "0x" + Integer.toHexString( failedEcu.getDiagnosticAddress() );
							String responseBehaviour = "";

							// Fehlertext zu SG-Fehler ermitteln
							errorText = failedEcu.getLocalizedMessage() + ", (PSdZ ID: " + failedEcu.getMessageId() + ")";

							// gehe die EcuStatistics-HapMap durch und suche die passende Diagnoseadresse in Hashmap zum fehlerhaften SG
							for( String ecu : psdzECUStatisticsHexStr.keySet() ) {
								if( ecu.equalsIgnoreCase( Integer.toHexString( failedEcu.getDiagnosticAddress() ) ) ) {
									// Treffer! SG Antwortverhalten nur eintragen, wenn SG nie geantwortet hat oder sporadisch									
									if( psdzECUStatisticsHexStr.get( ecu ).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.0" ) ) || psdzECUStatisticsHexStr.get( ecu ).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.1" ) ) )
										responseBehaviour = " (" + PB.getString( "psdz.SG" ) + " " + PB.getString( "psdz.antwortet" ) + " " + psdzECUStatisticsHexStr.get( ecu ) + ")";
									else
										responseBehaviour = "";
								}
							}

							result = new Ergebnis( "PSdZManageCertificates", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText + responseBehaviour, hintText, Ergebnis.FT_NIO );
							ergListe.add( result );
						}
						status = STATUS_EXECUTION_ERROR;
					}

				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					errorText = e.getLocalizedMessage();
					result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler beim Start der Zertifikate- und Bindungspr�fung in den Steuerger�ten!" : "Error during start of certificate and binding check in ECUs!") + (errorText != null ? errorDescription + errorText : ""), hintText, Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

			// GET_STATUS_EXTENSIVE_CERTIFICATE_AND_BINDING_CHECK

			if( action.equals( ACTIONS.GET_STATUS_EXTENSIVE_CERTIFICATE_AND_BINDING_CHECK ) ) {
				CascadeFetchEcuCertCheckingResultCto cascadeFetchEcuCertCheckingResultCto = null;

				try {

					// Bestimmung der ecuList und ecuListIsBlackList
					if( ecuIncludeList != null ) {
						ecuList = ecuIncludeList;
						bEcuListIsBlackList = false;
					} else if( ecuExcludeList != null ) {
						ecuList = ecuExcludeList;
						bEcuListIsBlackList = true;
					} else {
						ecuList = null;
						bEcuListIsBlackList = true;
					}

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.fetchEcuCertChecking' START" );
					}

					cascadeFetchEcuCertCheckingResultCto = psdz.fetchEcuCertChecking( ecuList, bEcuListIsBlackList, maxtimeForStatusCheckRetries, timeBetweenStatusCheckRetries, bLogObjectFile );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates 'psdz.fetchEcuCertChecking' ENDE" );
					}

					// SG Einzelfehler ausgeben, falls SG-Fehler aufgetreten sind (-> SGe haben nicht oder negativ geantwortet, oder Status/Detailstatus mindestens eines SGes ist n.i.O.)

					// SGe haben nicht oder negativ geantwortet
					if( !cascadeFetchEcuCertCheckingResultCto.getFailedEcus().isEmpty() ) {
						HashMap<String, String> psdzECUStatisticsHexStr = new HashMap<String, String>( psdz.getEcuStatisticsHexString() );

						for( CascadeEcuFailureResponseCto failedEcu : cascadeFetchEcuCertCheckingResultCto.getFailedEcus() ) {
							String sgBaseVariantName = failedEcu.getBaseVariantName();
							String sgDiagAddress = "0x" + Integer.toHexString( failedEcu.getDiagnosticAddress() );
							String responseBehaviour = "";

							// Fehlertext zu SG-Fehler ermitteln
							errorText = failedEcu.getLocalizedMessage() + ", (PSdZ ID: " + failedEcu.getMessageId() + ")";

							// gehe die EcuStatistics-HapMap durch und suche die passende Diagnoseadresse in Hashmap zum fehlerhaften SG
							for( String ecu : psdzECUStatisticsHexStr.keySet() ) {
								if( ecu.equalsIgnoreCase( Integer.toHexString( failedEcu.getDiagnosticAddress() ) ) ) {
									// Treffer! SG Antwortverhalten nur eintragen, wenn SG nie geantwortet hat oder sporadisch									
									if( psdzECUStatisticsHexStr.get( ecu ).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.0" ) ) || psdzECUStatisticsHexStr.get( ecu ).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.1" ) ) )
										responseBehaviour = " (" + PB.getString( "psdz.SG" ) + " " + PB.getString( "psdz.antwortet" ) + " " + psdzECUStatisticsHexStr.get( ecu ) + ")";
									else
										responseBehaviour = "";
								}
							}

							result = new Ergebnis( "PSdZManageCertificates", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText + responseBehaviour, hintText, Ergebnis.FT_NIO );
							ergListe.add( result );
						}
						status = STATUS_EXECUTION_ERROR;
					}

					// Status/Detailstatus mindestens eines SGes ist n.i.O.
					for( CascadeEcuCertCheckingResponseEto ecuCertCheckingResults : cascadeFetchEcuCertCheckingResultCto.getResults() ) {
						String sgBaseVariantName = ecuCertCheckingResults.getBaseVariantName();
						String sgDiagAddress = "0x" + Integer.toHexString( ecuCertCheckingResults.getDiagnosticAddress() );

						// Zertifikatestatus
						if( !ecuCertCheckingResults.getCertificatesStatus().getStatusName().equalsIgnoreCase( CascadeEcuCertCheckingStatusEto.OK ) ) {
							String errorTextGeneral = DE ? "Fehlerhafter Zertifikatestatus: " : "Incorrect certificate status: ";
							errorText = errorTextGeneral + ecuCertCheckingResults.getCertificatesStatus().getStatusName() + "," + errorDescription + ecuCertCheckingResults.getCertificatesStatus().getLocalizedMessage();
							result = new Ergebnis( "PSdZManageCertificates", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText, hintText, Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}

						// Binding-Status
						if( !ecuCertCheckingResults.getBindingsStatus().getStatusName().equalsIgnoreCase( CascadeEcuCertCheckingStatusEto.OK ) ) {
							String errorTextGeneral = DE ? "Fehlerhafter Binding-Status: " : "Incorrect binding status: ";
							errorText = errorTextGeneral + ecuCertCheckingResults.getBindingsStatus().getStatusName() + "," + errorDescription + ecuCertCheckingResults.getBindingsStatus().getLocalizedMessage();
							result = new Ergebnis( "PSdZManageCertificates", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText, hintText, Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}

						// Otherbinding-Status
						if( !ecuCertCheckingResults.getOtherBindingsStatus().getStatusName().equalsIgnoreCase( CascadeEcuCertCheckingStatusEto.OK ) ) {
							String errorTextGeneral = DE ? "Fehlerhafter OtherBinding-Status: " : "Incorrect other binding status: ";
							errorText = errorTextGeneral + ecuCertCheckingResults.getOtherBindingsStatus().getStatusName() + "," + errorDescription + ecuCertCheckingResults.getOtherBindingsStatus().getLocalizedMessage();
							result = new Ergebnis( "PSdZManageCertificates", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText, hintText, Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}

						// Binding-Detailstatus
						for( CascadeBindingDetailStatusCto cascadeBindingDetailStatusCto : ecuCertCheckingResults.getBindingsDetailStatus() ) {
							String rollenName = cascadeBindingDetailStatusCto.getRollenName();

							// Binding-Detailstatus - Zertifikatestatus
							if( !cascadeBindingDetailStatusCto.getCertificateStatus().getStatusName().equalsIgnoreCase( CascadeEcuCertCheckingStatusEto.OK ) ) {
								String errorTextGeneral = DE ? "Fehlerhafter Binding-Detailstatus f�r das Zertifikat der Rolle " + rollenName + ": " : "Incorrect binding detail status for certificate of role " + rollenName + ": ";
								errorText = errorTextGeneral + cascadeBindingDetailStatusCto.getCertificateStatus().getStatusName() + "," + errorDescription + cascadeBindingDetailStatusCto.getCertificateStatus().getLocalizedMessage();
								result = new Ergebnis( "PSdZManageCertificates", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText, hintText, Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}

							// Binding-Detailstatus - Binding-Status
							if( !cascadeBindingDetailStatusCto.getBindingStatus().getStatusName().equalsIgnoreCase( CascadeEcuCertCheckingStatusEto.OK ) ) {
								String errorTextGeneral = DE ? "Fehlerhafter Binding-Detailstatus f�r das Binding der Rolle " + rollenName + ": " : "Incorrect binding detail status for binding of role " + rollenName + ": ";
								errorText = errorTextGeneral + cascadeBindingDetailStatusCto.getBindingStatus().getStatusName() + "," + errorDescription + cascadeBindingDetailStatusCto.getBindingStatus().getLocalizedMessage();
								result = new Ergebnis( "PSdZManageCertificates", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText, hintText, Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						}

						// OtherBinding-Detailstatus
						for( CascadeOtherBindingDetailStatusCto cascadeOtherBindingDetailStatusCto : ecuCertCheckingResults.getOtherBindingsDetailStatus() ) {
							String ecuName = cascadeOtherBindingDetailStatusCto.getEcuName();
							String rollenName = cascadeOtherBindingDetailStatusCto.getRollenName();

							// Binding-Detailstatus - OtherBinding-Status
							if( !cascadeOtherBindingDetailStatusCto.getOtherBindingStatus().getStatusName().equalsIgnoreCase( CascadeEcuCertCheckingStatusEto.OK ) ) {
								String errorTextGeneral = DE ? "Fehlerhafter Binding-Detailstatus f�r das OtherBinding mit ecuName " + ecuName + " der Rolle " + rollenName + ": " : "Incorrect binding detail status for binding with ecuName " + ecuName + " of role " + rollenName + ": ";
								errorText = errorTextGeneral + cascadeOtherBindingDetailStatusCto.getOtherBindingStatus().getStatusName() + "," + errorDescription + cascadeOtherBindingDetailStatusCto.getOtherBindingStatus().getLocalizedMessage();
								result = new Ergebnis( "PSdZManageCertificates", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText, hintText, Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						}
					}

				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					errorText = e.getLocalizedMessage();
					result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler bei der Statusermittlung der Zertifikate- und Bindungspr�fung!" : "Error while getting status of certificate and binding check!") + (errorText != null ? errorDescription + errorText : ""), hintText, Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

			// reset UserDialog
			userDialog.reset();

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Freigabe der benutzten Devices
		if( psdz != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releasePSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
		try {
			getPr�flingLaufzeitUmgebung().releaseUserDialog();
		} catch( Exception e ) {
			if( e instanceof DeviceLockedException )
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "PSdZManageCertificates", "PSdZ", "", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}
		setPPStatus( info, status, ergListe );
		// Performancemessung Ende
		if( bDebugPerform )
			System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZManageCertificates PP ENDE" );

	}
}

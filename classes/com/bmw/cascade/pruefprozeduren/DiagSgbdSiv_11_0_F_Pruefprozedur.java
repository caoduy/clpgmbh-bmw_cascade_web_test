/*
 * DiagSgbdSiv_1_0_F_Pruefprozedur.java Created on 14.09.00
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;

/**
 * Implementierung der Pr�fprozedur, die per Diagnose �ber eine Gruppendatei die aktuelle
 * SGBD_Variante ermittelt und diese u.U. diversen Soll-Varianten gegen�berstellt. Ist die
 * �bergebene Sollvariante identisch der Gruppe, unterbleibt ausschliesslich die Bewertung. Ist die
 * Bewertung ok oder wurde explizit keine Bewertung gew�nscht, wird die ermittelte SGBD als Attribut
 * im Pr�fling eingetragen und s�mtliche Pr�fprozeduren des Prueflings upgedatet.
 * 
 * @author Winkler
 * @version Implementierung
 * @version 6_0_T C. Schumann: APDM Ergebnis angepasst und deprecated Ediabas-Klassen ausgetauscht
 * @version 7_0_F C. Schumann: Freigabe 6_0_T
 * @version 9_0_F C. Schumann: APDM Ergebnis angepasst und und Parameter 1 und 2 richtig bef�llt
 * @version 10_0_T M.Saller: �nderung des Exception-Handling bei Ediabas
 * @version 11_0_F M.Saller: Freigabe der 10_0_T
 */
public class DiagSgbdSiv_11_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagSgbdSiv_11_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DiagSgbdSiv_11_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = new String[0];
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "Gruppe", "SollSGBD" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	public boolean checkArgs() {
		boolean ok;
		try {
			ok = super.checkArgs();
			if( ok == true ) {
				if( getArg( getRequiredArgs()[0] ).indexOf( ';' ) != -1 )
					return false;
			}
			return ok;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String gruppe;
		String[] sollSgbden;
		String temp = "";
		int i;

		/***********************************************
		 * Label in Transientdatei schreiben, die folgende Zeile ist auch notig import
		 * com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		 ***********************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		try {
			// Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				gruppe = getArg( getRequiredArgs()[0] );
				sollSgbden = extractValues( getArg( getRequiredArgs()[1] ) );
			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			EdiabasProxyThread ediabas = null;
			try {
				ediabas = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();
			} catch( Exception e ) {
				throw new PPExecutionException( "Ediabas wurde nicht erfolgreich initialisiert!" );
			}

			try {
				// Ausf�hrung
				ediabas = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();
				ediabas.apiJob( gruppe, "INITIALISIERUNG", "", "VARIANTE" ); // Achtung Jobstatus
																				// existiert
																				// nicht!!!
				temp = ediabas.getDiagResultValue( 0, "VARIANTE" ).toUpperCase();
			} catch( ApiCallFailedException e ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", gruppe, "INITIALISIERUNG", "", "VARIANTE", temp, "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", gruppe, "INITIALISIERUNG", "", "VARIANTE", temp, "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", gruppe, "INITIALISIERUNG", "", "VARIANTE", temp, "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Analyse nur wenn SollSgbden != Gruppe
			if( (sollSgbden.length != 1) || (sollSgbden[(sollSgbden.length) - 1].equals( gruppe ) == false) ) {
				status = STATUS_EXECUTION_ERROR;
				i = 0;
				while( (i < sollSgbden.length) && (status == STATUS_EXECUTION_ERROR) ) {
					if( temp.equals( sollSgbden[i] ) == true )
						status = STATUS_EXECUTION_OK;
					i++;
				}
			}

			// Doku
			String temp1 = "";
			if( sollSgbden.length > 0 )
				temp1 = sollSgbden[0];
			for( i = 1; i < sollSgbden.length; i++ ) {
				temp1 = temp1 + ";" + sollSgbden[i];
			}
			if( status == STATUS_EXECUTION_OK )
				result = new Ergebnis( "VARIANTE", "EDIABAS", gruppe, "INITIALISIERUNG", "", "VARIANTE", temp, temp1, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			else
				result = new Ergebnis( "VARIANTE", "EDIABAS", gruppe, "INITIALISIERUNG", "", "VARIANTE", temp, temp1, "", "0", "", "", "", PB.getString( "variantenfehler" ), PB.getString( "sgTauschen" ), Ergebnis.FT_NIO );
			ergListe.add( result );

			// Tempor�r f�r E65/E66 f�r Doku
			if( (status == STATUS_EXECUTION_OK) && ((getPr�fling().getAuftrag().getBaureihe().equals( "E65" ) == true) || (getPr�fling().getAuftrag().getBaureihe().equals( "E66" ) == true)) ) {
				try {
					// Ausf�hrung
					ediabas.apiJob( temp, "INFO", "", "REVISION" ); // Achtung Jobstatus existiert
																	// nicht!!!
					temp1 = ediabas.getDiagResultValue( 0, "REVISION" );
					result = new Ergebnis( "REVISION", "EDIABAS", "", "", temp + "INFO", "REVISION", temp1, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				} catch( ApiCallFailedException e ) {
				} catch( EdiabasResultNotFoundException e ) {
				}
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		setPPStatus( info, status, ergListe );
	}

}

/*
 *DiagByteLesen_x_y_F_Pruefprozedur.java
 *
 * Created on 9.08.04 
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.ediabas.*;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;

/**
 * Implementierung der Pr�fprozedur, die Einzelbits aus den Pr�fbytes lesen kann.
 * Es gibt bis jetzt 3 Bytes pro Steuerger�t, welche f�r die Montage zur Verf�gung gestellt sind. 
 * Wenn das Result zutrifft wird ein Hinweistext ausgegeben und auf NIO gesetzt.
 * Result hat zwei Parameter: Welches Byte und die Maske, getrennt durch ; Byte;Maske
 * zwingende parameter: Sgbd / Result und Hinweistext mindestens 1x (result (8x 0, 1 oder M))
 * optional: weitere Results und dazugeh�rige Hinweistexte (Result (8x 0, 1 oder M))
 * optional: display (Fehlermeldungs am Bildschirm)
 * 
 * @author Gliss, Buboltz
 * @version Implementierung
 * @version	  4_0_T	  22.08.2016 TB  Erweiterungen f�r Paralleldiagnose analog zur DiagToleranzPP (LOP 2015), Generics eingebaut + Compiler Warnungen behoben<BR>
 * @version	  5_0_F	  25.01.2017 MKe F-Version
 */

public class DiagByteLesen_5_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagByteLesen_5_0_F_Pruefprozedur() {
	}

	// Debug flags
	private boolean byteDebug = false; // Byte
	public static final boolean SimulationDebug = false; //falls simulation mu� noch gewartet werden bis daten per hand ge�ndert sind

	private ByteMaskHwt testMasks[] = null;
	private boolean display = false; //Anzeige als fehlertext, default none

	/**
	 * erzeugt eine neue Pruefprozedur, die den Wert der anggegebenen Pr�fstempel aus den Steuerger�t auslesen kann 
	 * und mit den angebebenen Werten im Pr�fling vergleicht
	 * Ausgabe ins Virtuele Fahrzeug und wenn gew�nscht am Bildschirm
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DiagByteLesen_5_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	private void cleanup() {
		testMasks = null;
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "RESULT[2..N]", //weitere masken f�r die pr�fbytes
		"HWT[2..N]", //bei �bereinstimmung fehlertext
		"DISPLAY", //gibt an ob fehlerausgabe am bildschirm};
		"DEBUG", //true/false
		"TAG" //optional fuer Paralleldiagnose
		};
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "SGBD", "RESULT1", "HWT1" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	@Override
	public boolean checkArgs() {

		try {
			if( !checkArgsInternal() )
				return false;
		} catch( Exception e ) {
			if( byteDebug )
				e.printStackTrace( System.out );
			return false;
		}
		return true;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert mit ppExceptions
	 */
	private boolean checkArgsInternal() throws PPExecutionException {
		String strValue;

		//alle argumente in eine tabelle
		//1.check required Args not Null und Result 
		// Debug output on begin of checkArgs()
		//for checking if unknown args are present
		Set<Object> myargs = new HashSet<Object>(); //Set for keeping all keys that have not been handled so far
		myargs.addAll( getArgs().keySet() ); //Add all keys to the set
		if( byteDebug ) {
			Iterator<Object> dIter = myargs.iterator();
			while( dIter.hasNext() )
				writeDebug( "DiagByteLesen.checkArgs.myargs conatains: " + dIter.next() );
		}

		//required Args
		//=====================================================

		//SGBD
		if( (getArg( "SGBD" ) != null) && (getArg( "SGBD" ).length() != 0) ) {
			myargs.remove( "SGBD" ); //Remove SGBD
		}
		writeDebug( "DiagByteLesen.checkArgs.myargs conatains after remove SGBD" );

		//RESULT1
		try {
			if( (getArg( "RESULT1" ) == null) ) {
				writeDebug( "DiagByteLesen.checkArgs.myargs contains no RESULT1" );
				throw new PPExecutionException( "PPExecutionException: " + getPr�fling().getName() + "." + getName() + " :DiagByteLesen.checkArgs.myargs contains NO RESULT1" );
			}
		} catch( Exception e ) {
			cleanup();
			throw new PPExecutionException( "DiagByteLesen.checkArgs:" + e.getMessage() );
		}

		//HWT1
		try {
			if( (getArg( "HWT1" ) == null) ) {
				writeDebug( "DiagByteLesen.checkArgs.myargs contains NO HWT1" );
				throw new PPExecutionException( "PPExecutionException: " + getPr�fling().getName() + "." + getName() + " :DiagByteLesen.checkArgs.myargs contains NO HWT1" );
			}
		} catch( PPExecutionException e ) {
			cleanup();
			throw new PPExecutionException( "" + e.getMessage() );
		} catch( Exception e ) {
			cleanup();
			throw new PPExecutionException( "DiagByteLesen.checkArgs:" + e.getMessage() );
		}

		//optional Args + test result1
		//=====================================================

		strValue = getArg( "DEBUG" );
		if( strValue != null ) {
			//Debug is present
			try {
				if( (strValue.toLowerCase().compareTo( "true" ) == 0) || (strValue.toLowerCase().compareTo( "false" ) == 0) ) {
					if( strValue.toLowerCase().compareTo( "true" ) == 0 )
						byteDebug = true;
					else
						byteDebug = false;
					myargs.remove( "DEBUG" );
					writeDebug( "DiagByteLesen.checkArgs.myargs remove DEBUG" );
				} else {
					writeDebug( "DiagByteLesen.checkArgs.myargs contains wrong DEBUG (false/true): " + strValue );
					throw new PPExecutionException( "PPExecutionException: " + getPr�fling().getName() + "." + getName() + " :DiagByteLesen.checkArgs.myargs contains wrong DEBUG (false/true): " + strValue );
				}
			} catch( PPExecutionException e ) {
				cleanup();
				throw new PPExecutionException( "" + e.getMessage() );
			} catch( Exception e ) {
				cleanup();
				throw new PPExecutionException( "DiagByteLesen.checkArgs:" + e.getMessage() );
			}
		}

		strValue = getArg( "DISPLAY" );
		if( strValue != null ) {
			//Display is present
			try {
				if( (strValue.toLowerCase().compareTo( "true" ) == 0) || (strValue.toLowerCase().compareTo( "false" ) == 0) ) {
					if( strValue.toLowerCase().compareTo( "true" ) == 0 )
						display = true;
					myargs.remove( "DISPLAY" );
					writeDebug( "DiagByteLesen.checkArgs.myargs remove DISPLAY" );
				} else {
					writeDebug( "DiagByteLesen.checkArgs.myargs contains wrong DISPLAY (false/true): " + strValue );
					throw new PPExecutionException( "PPExecutionException: " + getPr�fling().getName() + "." + getName() + " :DiagByteLesen.checkArgs.myargs contains wrong DISPLAY (false/true): " + strValue );
				}
			} catch( PPExecutionException e ) {
				cleanup();
				throw new PPExecutionException( "" + e.getMessage() );
			} catch( Exception e ) {
				cleanup();
				throw new PPExecutionException( "DiagByteLesen.checkArgs:" + e.getMessage() );
			}
		}
		strValue = getArg( "TAG" );
		if( strValue != null ) {
			myargs.remove( "TAG" );
			writeDebug( "DiagByteLesen.checkArgs.myargs remove TAG" );
		}
		//Extract remaining Mask_args  Result/hwt
		try {
			this.extractArgsForTestMasks( myargs );
		} catch( Exception e ) {
			cleanup();
			throw new PPExecutionException( "" + e.getMessage() );
		}

		//�berpr�fung ob noch unbekannte parameter 
		if( myargs.size() > 0 ) {
			writeDebug( "DiagByteLesen.checkArgs: Number of unknown arguments " + myargs.size() );
			throw new PPExecutionException( "PPExecutionException: " + getPr�fling().getName() + "." + getName() + " :DiagByteLesen.checkArgs: Number of unknown arguments " + myargs.size() );
		}

		if( byteDebug )
			writeDebug( "DiagByteLesen.checkArgs: End" );
		return true; //no required Arguments, any Arguments allowed  
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String[] pruefbyte = { "BYTE1", "BYTE2", "BYTE3" }; //bytes aus dem Steuerger�t (ist-zustand)
		String pruefbyteName;
		String temp;
		String sgbd = null;
		boolean error = false;
		String job = null;
		String jobpara = null;
		boolean udInUse = false;

		EdiabasProxyThread ediabas = null;

		//Paralleldiagnose
		boolean parallel = false;
		String tag = null;
		DeviceManager devMan = null;

		writeDebug( "DiagByteLesen.execute: Start" );
		try {
			//Parameter holen 
			try {
				//parameter checken
				checkArgsInternal();
				//        if( checkArgs() == false ) 
				//          throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			} catch( PPExecutionException e ) {
				if( e.getMessage() != null ) {
					System.out.println( " " + e.getMessage() );
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), PB.getString( "parameterexistenz" ), Ergebnis.FT_NIO_SYS );
				} else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException( "" + e.getMessage() );
			}

			try {
				//sgbd einlesen
				sgbd = extractValues( getArg( getRequiredArgs()[0] ) )[0];
			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//test pause zum eintragen des stempels
			if( SimulationDebug ) {
				String text = "";
				for( int i = 0; i < testMasks.length; i++ )
					text = text + " Nr.:" + testMasks[i].pruefbyteNr + " Mask:" + testMasks[i].mask + " hwt:" + testMasks[i].hwt + "\n";
				getPr�flingLaufzeitUmgebung().getUserDialog().requestUserMessage( "Simulation : DiagByteSchreiben", text, 0 );
				udInUse = true;
			}

			try {
				// Paralleldiagnose, Pr�fstandvariabel auslesen, ob Paralleldiagnose 
				try {
					Object pr_var;

					pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
					if( pr_var != null ) {
						if( pr_var instanceof String ) {
							if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
								parallel = true;
							}
						} else if( pr_var instanceof Boolean ) {
							if( ((Boolean) pr_var).booleanValue() ) {
								parallel = true;
							}
						}
					}
				} catch( VariablesException e ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				}

				// Devicemanager
				devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();

				if( getArg( "TAG" ) != null )
					tag = getArg( "TAG" );

				if( parallel ) // Ediabas holen
				{
					try {
						ediabas = devMan.getEdiabasParallel( tag, sgbd );
					} catch( Exception ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Wrong Cascade configuration for parallel diagnosys", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();
					} catch( Throwable ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();
					}
				} else {
					try {
						ediabas = devMan.getEdiabas( tag );
					} catch( Exception ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();

					}
				}
				// <<<<< Paralleldiagnose            

				//gegenlesen, ob richtig geschrieben!
				job = "PRUEFSTEMPEL_LESEN";
				jobpara = "";

				try {
					temp = ediabas.executeDiagJob( sgbd, job, jobpara, "" );
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					}
				} catch( Exception e ) {
					if( byteDebug )
						e.printStackTrace( System.out );
					throw e;
				}

				try {
					//auslesen der bytes 
					for( int i = 0; i < pruefbyte.length; i++ ) {
						pruefbyteName = "BYTE" + (i + 1);
						pruefbyte[i] = ediabas.getDiagResultValue( pruefbyteName );
						result = new Ergebnis( "BYTE" + (i + 1), "EDIABAS", sgbd, job, jobpara, "BYTE" + (i + 1), pruefbyte[i], "0x00", "0xFF", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
						writeDebug( "DiagByteLesen.execute:Auslesen der Bytes " + pruefbyte[i] );
					}
				} catch( Exception e ) {
					if( e.getMessage() != null )
						result = new Ergebnis( "PS_Check", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "toleranzFehler1" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "PS_Check", "", sgbd, "", "", "", "", "", "", "0", "", "", "", PB.getString( "toleranzFehler1" ), "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException( "DiagByteLesen.execute: reading bytes:" + e.getMessage() );
				}

				//Masken mit pr�fstempel vergleich 
				String strError = "";
				try {
					for( int i = 0; i < testMasks.length; i++ ) {
						if( !checkPruefByte( testMasks[i].mask, pruefbyte[testMasks[i].pruefbyteNrInArray] ) ) {
							error = true;
							writeDebug( "DiagByteLesen.execute:checkPruefBytes : NIO= " + testMasks[i].mask + " = " + pruefbyte[testMasks[i].pruefbyteNrInArray] + " hwt : " + testMasks[i].hwt );
							result = new Ergebnis( "PS_CHECK", "", sgbd, "", "", "", "", "", "", "0", "", "", "", "", testMasks[i].hwt, Ergebnis.FT_NIO );
							strError = strError + PB.getString( "hinweis" ) + ": " + result.getHinweisText() + "\n";
							ergListe.add( result );

						} else
							writeDebug( "DiagByteLesen.execute:checkPruefBytes : IO " );
					}
				} catch( PPExecutionException e ) {
					if( e.getMessage() != null )
						result = new Ergebnis( "PS_Check", "", sgbd, "", "", "", "", "", "", "0", "", "", "", "PPExecutionException", PB.getString( "unerwarteterLaufzeitfehler" ) + e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "PS_Check", "", sgbd, "", "", "", "", "", "", "0", "", "", "", "PPExecutionException", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException( "" + e.getMessage() );
				} catch( Exception e ) {
					e.printStackTrace( System.out );
					if( e.getMessage() != null )
						result = new Ergebnis( "PS_Check", "", sgbd, "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ) + e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "PS_Check", "", sgbd, "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException( "DiagByteLesen.execute: check bytes:" + e.getMessage() );
				}

				if( error ) {
					writeDebug( "DiagByteLesen.execute: nach testpr�fung keine exception: und fehler" + strError );
					if( display ) {
						try {
							getPr�flingLaufzeitUmgebung().getUserDialog().requestAlertMessage( PB.getString( "meldung" ), strError, 0 );
							udInUse = true;
						} catch( Exception e ) {
							e.printStackTrace( System.out );
							throw new PPExecutionException( "" + e.getMessage() );
						}
					}
					throw new PPExecutionException();
				} else
					writeDebug( "DiagByteLesen.execute: nach testpr�fung keine exception: und passede maske" );

			} catch( ApiCallFailedException e ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpara, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException e ) {
			if( e.getMessage() != null )
				writeDebug( "PPExecutionException: " + e.getMessage() );
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		//Devices freigeben
		if( udInUse == true ) {
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		// Paralleldiagnose
		if( parallel ) {
			try {
				devMan.releaseEdiabasParallel( ediabas, tag, sgbd );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable t ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		cleanup();
		setPPStatus( info, status, ergListe );
	}

	/**
	* extract all args for the testmasks.
	* The number of Masks (min/max index) is detected.
	* If given LOOP_UNTIL and LOOP_FROM are extracted and resolved.
	* Other Links and %X% are not resolved.
	* If myargs is given, it is checked that all args in myargs have been consumed at the end!
	* NOTE: When this method is called myargs must only contain Arguments beginning with JOBN_ or JOBn (n=numeric)
	* @param myargs Set of args that have not already be handled.  If a arg is extracted it is removed from the set.
	* @param resolveLoopFromUntil If true LOOP_FROM and LOOP_UNTIL are resolved. (for checkArgs=false, for execute=true)
	*/
	private void extractArgsForTestMasks( Set<Object> myargs ) throws PPExecutionException {

		String value = null;
		String label = null;

		if( byteDebug ) {
			Iterator<Object> dIter = myargs.iterator();
			while( dIter.hasNext() )
				writeDebug( "DiagByteLesen.extractArgsForTestMask.myargs conatains: " + (value = (String) dIter.next()) + " = " + getArg( value ) );
		}

		try {
			//*RESULTK (K is an integer)
			int k = 1; //start with *RESULT1
			boolean goon = true;
			//determine number of results 1...k-max
			while( goon ) {
				label = "RESULT" + k;
				value = getArg( label );
				writeDebug( "DiagByteLesen.extractArgsForTestMask:" + label + " = '" + value + "'" );
				if( value != null ) {
					//*RESULTK is present
					goon = true;
					k++;
				} else {
					goon = false;
				}
			}
			k = k - 1; //the last RESULTK is not present
			writeDebug( "DiagByteLesen.extractArgsForTestMask: NumberOfResults = " + k );
			//initialisierung of MaskByteHwt arrays
			try {
				this.testMasks = new ByteMaskHwt[k];
				for( int i = 0; i < k; i++ ) {
					this.testMasks[i] = new ByteMaskHwt();
				}
			} catch( Exception e ) {
				e.printStackTrace( System.out );
				throw new PPExecutionException( "PPExecutionException: " + getPr�fling().getName() + "." + getName() + " :DiagByteLesen.extractArgsForTestMask: " + e.getMessage() );
			}

			goon = true;
			//extract results k-max...1
			for( int i = 0; i < k; i++ ) {
				label = "RESULT" + (i + 1);
				value = getArg( label );
				writeDebug( "DiagByteLesen.extractArgsForTestMask:" + label + " = '" + value + "'" );
				if( value != null ) {
					//*RESULTK is present
					myargs.remove( label ); //remove the label from the list of all remaining args
					writeDebug( "DiagByteLesen.extractArgsForTestMask.myargs remove " + label );
					try {
						setResult( i, value );
					} catch( Exception e ) {
						throw new PPExecutionException( "Problem " + label + ": " + value );
					}
				} else {
					throw new PPExecutionException( " " + label + " should not be empty" );
				}
				label = "HWT" + (i + 1);
				value = getArg( label );
				writeDebug( " " + label + " = '" + value + "'" );
				if( value != null ) {
					//*HWTK is present
					myargs.remove( label ); //remove the label from the list of all remaining args
					writeDebug( "DiagByteLesen.extractArgsForTestMask.myargs remove " + label );
					try {
						testMasks[i].setHwt( value );
					} catch( Exception e ) {
						throw new PPExecutionException( "Problem " + label + ": " + value );
					}
				} else {
					throw new PPExecutionException( " " + label + " should not be empty" );
				}
			}
		} catch( Exception e ) {
			throw new PPExecutionException( "PPExecutionException: " + getPr�fling().getName() + "." + getName() + " :DiagByteLesen.extractArgsForTestMask: " + e.getMessage() );
		}

		//check if myargs contains any unhandled args
		if( !myargs.isEmpty() ) {
			//we have unresolved arguments
			if( byteDebug ) {
				Iterator<Object> iter = myargs.iterator();
				while( iter.hasNext() ) {
					writeDebug( "DiagByteLesen.extractArgsForTestMask: unknown Argument " + iter.next() );
				}
			}
			throw new PPExecutionException( "PPExecutionException: " + getPr�fling().getName() + "." + getName() + " :unknown unhandled arguments" );
		}
	}

	/**debug funktion Ausgabewerte in PruefstandScreenLog**/
	private void writeDebug( String info ) {
		if( byteDebug )
			System.out.println( "DiagByteLesen: " + getPr�fling().getName() + "." + getName() + " " + info );
	}

	/** 
	 *result aufteilen in Byte und Maske
	 *@param zu �berpr�fendes Byte
	 *@param r�ckgabe der Bytenr (1-3)
	 *@param r�ckgabe der Maske 
	 *@return false / true
	 *@throws PPExecutionException falls keine Splittung m�glich
	 */
	private boolean setResult( int position, String strResult ) throws PPExecutionException {

		int pruefNr = 0;
		String mask = null;

		// splitten auf Byte (konvertierung auf int) und maske
		try {

			String[] byteNrAndMask = strResult.split( ";", 2 );
			//richtige zuorgnung auf maske und byte. Kleinerer String d�rfte Byte sein
			if( byteNrAndMask[0].length() < byteNrAndMask[1].length() ) {
				pruefNr = Integer.parseInt( byteNrAndMask[0] );
				mask = byteNrAndMask[1];
			} else {
				pruefNr = Integer.parseInt( byteNrAndMask[1] );
				mask = byteNrAndMask[0];
			}
			testMasks[position].setPruefByteNr( pruefNr );
			testMasks[position].setMask( mask );
		} catch( Exception e ) {
			writeDebug( "DiagByteLesen.splitResult: PruefByteNr " + pruefNr + " Mask: " + mask );
			throw new PPExecutionException( "" + e.getMessage() );
		}
		return true;
	}

	/** 
	 *pr�fen ob maske mit dem pruefbyte im ecu �bereinstimmt
	 *@param zu �berpr�fendes Byte und Maske
	 *@return true /false  
	 *@throws PPExecutionException falls nicht konvertiert werden kann
	 */
	private boolean checkPruefByte( String strMask, String strPruefbyte ) throws PPExecutionException {

		int intPruefbyte; //Die stringfelder auf intfelder ge�ndert 
		int[] maskBit = { 128, 64, 32, 16, 8, 4, 2, 1 }; //maskierungsfelder f�r die einzelnen bits
		int[] tempBit = new int[maskBit.length]; //neuer tempbyte nach maskierung
		boolean goon = true;

		try {
			//Pruefbytes aus ECU in int konvertieren 
			try {
				intPruefbyte = Integer.parseInt( strPruefbyte );
				writeDebug( "DiagByteSchreiben.maskArgs: konvertierung von string: " + strPruefbyte + " nach int: " + intPruefbyte );
			} catch( NumberFormatException e ) {
				e.printStackTrace( System.out );
				throw new PPExecutionException( e.getMessage() );
			}

			//Pruefbyte mit strMask vergleichen , Fehler bei nicht�bereinstimmung
			try {
				for( int x = 0; x < maskBit.length; x++ ) {
					tempBit[x] = ((intPruefbyte & maskBit[x]) >> ((maskBit.length - 1) - x));
					writeDebug( "DiagByteSchreiben.maskArgs: : tempbit[" + x + "]=" + tempBit[x] );
				}

				if( strMask != null ) {
					//lesen �ber die Bits des Bytes
					for( int x = 0; (x < maskBit.length && goon == true); x++ ) {
						switch( strMask.charAt( x ) ) {
							case '0':
								if( tempBit[x] != 0 ) {
									goon = false;
								}
								writeDebug( "DiagByteLesen.maskArgs: bit compare 0: Pruefbyte = " + intPruefbyte + " strMask = " + strMask + " tempbit[" + x + "]=" + tempBit[x] );
								break;
							case '1':
								if( tempBit[x] != 1 ) {
									goon = false;
								}
								writeDebug( "DiagByteLesen.maskArgs: bit compare 1: Pruefbyte = " + intPruefbyte + " strMask = " + strMask + " tempbit[" + x + "]=" + tempBit[x] );
								break;
							default:
								writeDebug( "DiagByteLesen.maskArgs: default okay Pruefbyte = " + intPruefbyte + " strMask = " + strMask + " tempbit[" + x + "]=" + tempBit[x] );
								break;
						}
					}
					//abbruch stattgefunden --> fehler
				} else
					throw new PPExecutionException( "PPExecutionException: " + getPr�fling().getName() + "." + getName() + " :DiagByteLesen.maskArgs: strMask should not be empty!" );

			} catch( Exception e ) {
				throw new PPExecutionException( "PPExecutionException: " + getPr�fling().getName() + "." + getName() + " :DiagByteLesen.maskArgs Exception:" + e.getMessage() );
			}
		} catch( PPExecutionException e ) {
			throw new PPExecutionException( e.getMessage() );
		}
		return (goon);
	}

	/*********************************************************************/
	/*   private classes                                                 */
	/*********************************************************************/

	/**
	 *private classe f�r Result, hwt und byte
	 *byte nur zwischen 1 und 3
	 *Maske nur 0,1,m und nur l�nge 8
	 */
	private class ByteMaskHwt {

		//Instanzvariablen
		//Values for each Result 1...K. Index 0 is never used!!!
		private String mask; //Mask of the Result
		private int pruefbyteNr; //Byte of the Result
		private int pruefbyteNrInArray; //Byte of the Result in array byte -1 
		private String hwt; //Hwt of Result

		/**
		 * Konstruktor
		 */
		ByteMaskHwt() {

			//Values for each Result 1...K. Index 0 is never used!!!
			mask = null; //Mask of the Result
			pruefbyteNr = 0; //Byte of the Result 
			pruefbyteNrInArray = -1;
			hwt = null; //Hwt of Result

		}

		ByteMaskHwt( ByteMaskHwt base ) {

			//Values for each Result 1...K. Index 0 is never used!!!
			mask = base.mask;
			pruefbyteNr = base.pruefbyteNr;
			pruefbyteNrInArray = base.pruefbyteNr - 1;
			hwt = base.hwt;
		}

		//mask hat dann mmmmmmmm
		ByteMaskHwt( int pruefbyteNr, String hwt ) {

			this.mask = "mmmmmmmm";
			this.pruefbyteNr = pruefbyteNr;
			this.pruefbyteNrInArray = pruefbyteNr - 1;
			this.hwt = hwt;
		}

		ByteMaskHwt( int pruefbyteNr, String hwt, String mask ) {

			//Values for each Result 1...K. Index 0 is never used!!!
			this.mask = mask;
			this.pruefbyteNr = pruefbyteNr;
			this.pruefbyteNrInArray = pruefbyteNr - 1;
			this.hwt = hwt;

		}

		public int getpruefbyteNr() {
			return (pruefbyteNr);
		}

		public int getpruefbyteNrInArray() {
			return (pruefbyteNrInArray);
		}

		public String getMask() {
			return (mask);
		}

		public String getHwt() {
			return (hwt);
		}

		public Object clone() {
			return new ByteMaskHwt( this );
		}

		public void setByteMaskHwt( int pruefbyteNr, String hwt, String mask ) throws PPExecutionException {

			try {
				if( checkPruefbyteNr( pruefbyteNr ) ) {
					this.pruefbyteNr = pruefbyteNr;
					this.pruefbyteNrInArray = pruefbyteNr - 1;
				}
				if( checkHwt( hwt ) )
					this.hwt = hwt;
				if( checkMask( mask ) )
					this.mask = mask;
				this.pruefbyteNrInArray = pruefbyteNr - 1;

			} catch( PPExecutionException e ) {
				throw new PPExecutionException( "" + e.getMessage() );
			} catch( Exception e ) {
				throw new PPExecutionException( "" + e.getMessage() );
			}
		}

		public void setPruefByteNr( int pruefbyteNr ) throws PPExecutionException {

			try {
				if( checkPruefbyteNr( pruefbyteNr ) ) {
					this.pruefbyteNr = pruefbyteNr;
					this.pruefbyteNrInArray = pruefbyteNr - 1;
				}
			} catch( PPExecutionException e ) {
				throw new PPExecutionException( "" + e.getMessage() );
			} catch( Exception e ) {
				throw new PPExecutionException( "" + e.getMessage() );
			}
		}

		public void setMask( String mask ) throws PPExecutionException {

			try {
				if( checkMask( mask ) )
					this.mask = mask;
			} catch( PPExecutionException e ) {
				throw new PPExecutionException( "" + e.getMessage() );
			} catch( Exception e ) {
				throw new PPExecutionException( "" + e.getMessage() );
			}
		}

		public void setHwt( String hwt ) throws PPExecutionException {

			try {
				if( checkHwt( hwt ) ) {
					this.hwt = hwt;
				}
			} catch( PPExecutionException e ) {
				throw new PPExecutionException( "" + e.getMessage() );
			}
		}

		//pruefbyteNr zwischen 1-3
		private boolean checkPruefbyteNr( int pruefbyteNr ) throws PPExecutionException {

			if( pruefbyteNr < 1 || pruefbyteNr > 3 ) {
				writeDebug( "DiagByteLesen.ByteMaskHwt.checkPruefbyteNr: Wrong testbyte have to be 0 < " + pruefbyteNr + "<3" );
				throw new PPExecutionException( "DiagByteLesen.ByteMaskHwt.checkPruefbyteNr: Wrong testbyte have to be 0 < " + pruefbyteNr + "<3" );
			}
			return true;
		}

		//hwt nicht null
		private boolean checkHwt( String hwt ) throws PPExecutionException {
			if( hwt == null ) {
				writeDebug( "DiagByteLesen.ByteMaskHwt.checkHwt:HWT should not be empty!" );
				throw new PPExecutionException( "DiagByteLesen.ByteMaskHwt.checkHwt:HWT should not be empty!" );
			}
			return true;
		}

		/**
		* �berpr�ft den �bergebenen sting auf 0,1,M bzw. L�nge 8
		* @param zu �berpr�fender Maske.
		* @throws PPExecutionException bei einem nicht konfromen Byte
		*/
		private boolean checkMask( String mask ) throws PPExecutionException {

			writeDebug( "checkMask: Eingansvariable: " + mask );
			try {
				if( mask.length() == 8 ) {
					for( int i = 0; i < mask.length(); i++ ) {
						if( ((mask.charAt( i ) != '0') && (mask.charAt( i ) != '1') && (mask.charAt( i ) != 'm') && (mask.charAt( i ) != 'M')) ) {
							writeDebug( "DiagByteLesen.ByteMaskHwt.checkMask: wrong value(0,1,m, M): " + mask );
							throw new PPExecutionException( "DiagByteLesen.ByteMaskHwt.checkMask: wrong value(0,1,m, M): " + mask );
						}
					}
				} else {
					writeDebug( "DiagByteLesen.ByteMaskHwt.checkMask: wrong length (8) : Length is " + mask.length() + ", variable: " + mask );
					throw new PPExecutionException( "DiagByteLesen.ByteMaskHwt.checkMask: wrong length (8) : Length is " + mask.length() + ", variable: " + mask );
				}
			} catch( PPExecutionException e ) {
				throw new PPExecutionException( "" + e.getMessage() );
			} catch( Exception e ) {
				throw new PPExecutionException( "" + e.getMessage() );
			}

			return true;
		}

		public boolean checkArgs() throws PPExecutionException {

			try {
				checkPruefbyteNr( this.pruefbyteNr );
				checkHwt( this.hwt );
				checkMask( this.mask );

			} catch( PPExecutionException e ) {
				throw new PPExecutionException( "" + e.getMessage() );
			} catch( Exception e ) {
				throw new PPExecutionException( "" + e.getMessage() );
			}
			return true;
		}

	}
}

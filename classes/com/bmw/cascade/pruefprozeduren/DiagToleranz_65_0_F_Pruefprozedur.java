/*
 * DiagToleranz_55_0_F_Pruefprozedur.java
 *
 * Created on 14.09.00
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.SwingUtilities;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResult;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultSet;
import com.bmw.cascade.pruefstand.framework.udnext.UDNCompoundSection;
import com.bmw.cascade.pruefstand.framework.udnext.UDNHandle;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.ImageLocation;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.UDNButtonConfiguration;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.UDNState;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModelEvent;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModelListener;
import com.bmw.cascade.pruefstand.framework.udnext.UDNMultiLineTextSection;
import com.bmw.cascade.pruefstand.framework.udnext.UDNSection;
import com.bmw.cascade.pruefstand.framework.udnext.UDNUtils;
import com.bmw.cascade.pruefstand.framework.udnext.UserDialogNextRuntimeEnvironment;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementierung der Pr�fprozedur, die per Diagnose diverse Infos bewertet (auf erf�llt bzw. nicht erf�llt).
 * Diese Bewertung kann pollend erfolgen, d.h. �ber das optionale Argument TIMEOUT kann gesteuert werden, wie
 * lange (Maximalzeit) die Bewertung erfolgt. �ber das optionale Argument OK_TIME kann angegeben werden, wie lange
 * der Status OK gehalten werden muss, damit dieser Status g�ltig ist. Optional ist die Ausgabe einer Werkeranweisung
 * m�glich, die bei gesetzten optionalen Parameter CANCEL zus�tzlich eine Abbruchtaste zeigt. Die Werkeranweisung kann optional
 * auch als User Dialog Next mit Bildern dargestellt werden.Auftretende EDIABAS-Fehler wie "Wrong UBATT" oder 
 * "No Response from Control Unit" werden ignoriert, wenn das optionale Argument IGNORE_ERRORS gesetzt ist.
 * Bei JOBPAR, MIN[1..N], MAX[1..N] und HWT koennen ueber das @-Konstrukt Ergebnisse aus dem virtuellen Fahrzeug verwendet werden.
 * In HWT kann das @-Konstrukt im Flie�text, abgetrennt durch Leerzeichen, verwendet werden.
 *
 * @author Winkler, Crichton, M�ller, St�binger, Buboltz
 * @version 0_0_3 29.05.2002  MM  Invert als optionaler Parameter hinzu<BR>
 * @version 0_0_4 21.03.2003  SS  Bei Jobpar kann jetzt auch das @-Konstrukt zum holen von Ergebnissen aus dem virtuellen<BR>
 *                                Fahrzeug verwendet werden.<BR>
 * @version 0_0_5 26.03.2003  SS  Korrektur f�r Jobpar, damit CASCADE-Pruefstaende vor Version 1.2.4<BR>
 *                                richtig funktionieren.<BR>
 * @version 0_0_6 12.06.2003  MM  Mehrere HWTs integriert<BR>
 * @version 0_0_7 12.06.2003  MM  Bugfix Checkargs<BR>
 * @version 0_0_8 01.07.2003  MM  Bugfix Checkargs<BR>
 * @version 0_0_9 03.07.2003  MM  FA Release<BR>
 * @version 0_1_0 04.07.2003  TB  Zusammenfuehrung mit DiagToleranzCancel, bei MIN[1..N] & MAX[1..N] kann jetzt auch das @-Konstrukt verwendet werden<BR>
 * @version 0_1_1 18.07.2003  TB  FA Release<BR>
 * @version 0_1_2 17.10.2003  TB  displayMessage mit Titel wenn CANCEL == true<BR>
 * @version 0_1_3 01.12.2003  NC  Ergebnis Fehlertexte angepasst<BR>
 * @version 0_1_4 08.12.2003  MM  Bei manuellem Abbruch durch den Werker wird kein Fehlertext mehr bef�llt, nur der Antworttext<BR>
 * @version 0_1_5 03.02.2004  NM  Freigabe als FA-Version<BR>
 * @version 0_1_6 23.07.2004  TB  Grafikfunktionalitaet eingebaut<BR>
 * @version 0_1_7 16.08.2004  AM  Einbau der M�glichkeit Results aus allen Ergebniss�tzen zu holen/zu bewerten<BR>
 * @version 0_1_8 18.08.2004  AM  Debug der Testversion 17 <BR>
 * @version V0_2_6_FA 24.03.2005  Gehring,  FA - Version der V0_2_5_TA - Version
 * @version 0_2_0 04.10.2004  AM  Simulationsmodus, ohne Ediabas integriert (abstract CASCADE 1.3 m�glich). <BR>
 * @version 0_2_1 11.10.2004  NC  Zus�tzliche (�berflussige) Abbruchtext biem CANCEL==TRUE wird nicht mehr dargestellt <BR>
 * @version V_0_2_2_TA  04.02.2004  Konstante FehlerText hinzu, damit dem Ergebnis-Objekt implements FT_NIO-Fall immer ein Fehlertext uebergeben wird<BR>
 * @version V0_2_5_TA 21.02.2005  Gehring,  Konstanter FehlerText durch PB.getString("pollingAbbruch") ersetzt.
 * @version V0_2_6_FA 24.03.2005  Gehring,  FA - Version der V0_2_5_TA - Version
 * @version 31_0_T    19.06.2007  M�hlbauer, Erweiterung optionaler Parameter FENSTERNAME f�r F1-Diagnose W6.1
 * @version 32_0_F    21.06.2007  M�hlbauer, Test-Version nach Produktiv-Version portiert
 * @version 33_0_F	  08.04.2008  F.Gebauer neues optinales Result zur Anzeige von SG internen Fehlermeldungen am SG
 * @version 35_0_T	  15.09.2008  M. Baum: Erg�nzung f�r Paralleldiagnose
 * @version 36_0_F    20.02.2010  M. Baum: F-Version <BR>
 * @version 37_0_T    20.02.2010  T. Buboltz: Erg�nzung f�r ECOS (unberwertetes Result), neuer optionaler Parameter NO_MIN_MAX[1..N] <BR>
 * @version 38_0_F    04.05.2010  T. Buboltz: F-Version <BR>
 * @version 39_0_T    22.12.2010  P. Voelz: die parametrierbare Pause wird jetzt zw. Jobaufrufen gewartet (und nicht zw. den Result-Abfragen) <BR>
 * @version 40_0_F    24.02.2011  P. Voelz: F-Version <BR>
 * @version 41_0_T    24.05.2011  C. Wolf: Erweiterung, sodass der Timeout Parameter auch mit @-Konstrukten bef�llt werden kann <BR>
 * @version 42_0_T    25.05.2011  C. Wolf: BugFix 41_0_T (beim Parameterexistenz-Check �berpr�fung ob Timeout!=null hinzugef�gt<BR>
 * @version 43_0_F    25.05.2011  C. Wolf: Freigabe 42_0_T<BR>
 * @version 44_0_T    08.02.2012  C. Wolf: Bei Diagnosefehler wird jetzt noch der HWT ausgegeben<BR>
 * @version 45_0_F    08.02.2012  C. Wolf: Freigabe 44_0_T<BR>
 * @version 46_0_T	  07.11.2023  C. Wolf: neuer Schalter DOKU=true f�hrt dazu, dass alle Ergenisse aus allen S�tzen unbewertet nach APDM dokumentiert werden<BR>
 * @version 47_0_F	  07.11.2013  C. Wolf: Freigabe 46_0_T<BR>
 * @version 48_0_T	  17.02.2015  M. K�ser: F�r die Auswertung der OK_TIME wird jetzt die Pause nach dem Ediabas-job mitgerechnet<BR>
 * @version 49_0_F	  17.02.2015  M. K�ser: Freigabe 48_0_T<BR>
 * @version 50_0_T	  15.04.2015  F. Sch�nert: Erm�glichung des @-Operators beim optinalen Argument HWT.<BR>
 * @version 51_0_T	  29.04.2015  F. Sch�nert: Bef�higung zu UDN Anweisungstext.<BR>
 * @version 52_0_T	  01.05.2015  P. Rettig: Parameter SHOW_COUNTDOWN eingebaut, welcher bei gesetztem TIMEOUT und AWT das Hochz�hlen der Zeit visualisiert (im bestehenden AWT-Dialog).<BR>
 * @version 53_0_F	  17.07.2015  P. Rettig: Freigabe 53_0_F<BR>
 * @version 54_0_T	  28.11.2015  P. Rettig: Statt Double.parseDouble rufen wir eine eigene Parse-Methode auf, welche auch mit hexadezimalen Zahlen umgehen kann. Anforderung resultiert aus neuem Sachnummernformat.<BR>
 * @version 55_0_F	  30.11.2015  P. Rettig: Freigabe 55_0_F<BR>
 * @version 56_0_T	  07.07.2016  M. K�ser: Genauere zeitliche Abarbeitung in Bezug auf den Parameter Timeout. Au�erdem wurden die LOG-Ausgaben erweitert.<BR>
 * @version 57_0_T    28.09.2016  T. Buboltz: LOP 2122, SGBD_SIV Job vorher ggf. ausf�hren, wenn die SGBD auf "VARIANTE@SGBD_SIV" steht und der SGBD_SIV noch nicht ausgef�hrt wurde. Generics und Kosmetik <BR>
 * @version 58_0_F    18.10.2016  T. Buboltz: Freigabe 58_0_F<BR
 * @version 59_0_T    29.03.2017  M. Kemp / C. Wolf: Umstrukturierung<BR>
 * @version 60_0_T    10.04.2017  M. Kemp: doppelte Fehlerausgaben entfernt<BR>
 * @version 61_0_F    02.05.2017  M. Kemp: F-Version<BR>
 * @version 62_0_T    05.05.2017  P. Rettig: Bugfix (Break bei SGBD_SIV-Bezug versetzt, siehe �nderung in 57_0_T)<BR>
 * @version 63_0_F    05.05.2017  P. Rettig: F-Version<BR>
 * @version 64_0_T    28.06.2017  M. Kemp: SGBD_SIV wird nun vor der EDIABAS Initialisierung ausgef�hrt, falls n�tig.<BR>
 * @version 65_0_F    28.06.2017  M. Kemp: F-Version<BR>
 */
public class DiagToleranz_65_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagToleranz_65_0_F_Pruefprozedur() {
	}

	/** debug flags **/
	private boolean debug_Standard;
	private boolean debug_CheckArgs;

	//Userdialog
	/** Fenstername beim UserDialog **/
	private String strFensterName;
	/** Speichert ob der Userdialog in Benutzung ist oder nicht **/
	private boolean udInUse;
	/** Speichert ob UserDiaglogNext verwendet werden soll **/
	private boolean udn;
	/** Speichert ob man den Userdialog abbrechen kann **/
	private boolean cancel;

	/** Ergebnisse zu allen Jobs�tzen werden gespeichert **/
	private boolean doku;
	/** Speichert ob Fehler ignoriert werden sollen **/
	private boolean ignore_errors;

	/** Speichert ob ein notwendiger SGBD_SIV schon korrekt ausgef�hrt wurde **/
	private boolean varianteAtSGBD_SIVUnavailable;

	/** Speichert ob alle Ergebnisse einer Pr�fung gemerkt werden sollen **/
	private boolean bGetAllResults;

	private boolean invert;
	private boolean showCountdown;
	private boolean bSimulationWithoutEdiabas;
	boolean bUseSimRes;
	private boolean[] bNoMinMax;

	private Vector<Ergebnis> ergListe;
	private int status, resAnzahl, result_index;
	private int[] iaResultSet;
	private long okTime, pause, firstOkTime;

	private EdiabasProxyThread ediabas;
	private String sgbd, job, jobpar, jobres, awt, hwt, sim_res, sCurrentResultLine, sCurrentResultName, sCurrentResultSet;
	private String image, imageLocation;
	private String[] mins, maxs, saCurrentResultDetails;
	private String[] results;
	private String[] hrts;
	private String[] jobparameters;
	private String[] hwts;

	private boolean parallelDiagnose; // MBA, paralleldiagnose
	private String diagnosetag; // MBa, Paralleldiagnose
	private DeviceManager devMan;
	private UserDialog myDialog;
	private UDNModelListener udn_modelListener;
	private UserDialogNextRuntimeEnvironment myDialogUDN;
	private UDNHandle udn_handle;

	/**
	 * Erzeugt eine neue Pruefprozedur, die auf Einhaltung bzw. auf Ausschlu� der zu analysierenden Werte pr�ft.
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DiagToleranz_65_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Attribute
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
		setAttribut( "INVERT", "FALSE" );
	}

	/**
	 * Liefert die optionalen Attribute
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "JOBPAR", "RESULT[1..N]", "MIN[1..N]", "MAX[1..N]", "TIMEOUT", "OK_TIME", "PAUSE", "AWT", "HWT", "INVERT", "HWT[1..N],", "IGNORE_ERRORS", "CANCEL", "SIM_RES", "DEBUG", "FENSTERNAME", "HRT[1..N]", "TAG", "NO_MIN_MAX[1..N]", "DOKU", "UDN", "IMAGE", "IMAGE_LOCATION", "SHOW_COUNTDOWN" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Attribute
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "SGBD", "JOB" };
		return args;
	}

	/**
	 * Ersetzt einen vorkommenden @-Operatoren in einem String durch dessen Werte
	 * Der Ausdruck muss dabei durch Leerzeichen abgetrennt sein
	 * @param   input - String mit zu ersetzenden Variablen
	 * @return	Ergbenis mit den aus dem @-Operator bezogenen Werten
	 * @author  Fabian Schoenert, BMW AG TI-545
	 */
	protected String replaceVariablesInString( String input ) throws PPExecutionException {
		if( input != null ) {
			if( input.indexOf( '@' ) != -1 ) { // Ersetzen notwendig?
				String[] variablenArray = null;
				StringBuilder output = new StringBuilder();
				// Finde Indizes f�r @-Operator Ausdruck
				int atIndex = input.indexOf( '@' );
				int beginIndex = input.lastIndexOf( ' ', atIndex ) + 1;
				int endIndex = input.indexOf( ' ', atIndex );
				if( endIndex == -1 )
					endIndex = input.length();
				// Werte @-Operatore aus
				try {
					variablenArray = extractValues( input.substring( beginIndex, endIndex ) );
				} catch( PPExecutionException exception ) {
					throw exception; // leite exception weiter
				}
				// Baue neuen String mit ausgewertetem Ausdruck
				output.append( input.substring( 0, beginIndex ) );
				for( int index = 0; index < variablenArray.length; ++index ) {
					output.append( variablenArray[index] );
				}
				output.append( input.substring( endIndex, input.length() ) );
				return output.toString();
			}
		}
		return input;
	}

	/**
	 * Pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die parent-Methode aufgrund
	 * der offenen Anzahl an Results
	 */
	@Override
	public boolean checkArgs() {

		//double d; // Variable wird ben�tigt zum �berpr�fen der MIN und MAX Werte
		int i, j;
		int maxIndex = 0;
		boolean bRequiredArg, bOptionalArg;
		String[] saParamDetails;
		String sResultLine;

		//Neu: Fenster-Name als optionales Argument
		this.strFensterName = getArg( getOptionalArgs()[15] );

		// Debug output on begin of checkArgs()
		if( debug_CheckArgs ) {
			Enumeration<?> enu = getArgs().keys();

			System.out.println( "" );
			System.out.println( "checkArgs in: " + this.getName() );
			System.out.println( "All Arguments from this PP:" );
			while( enu.hasMoreElements() ) {
				// The actual argument
				String strGivenkey = (String) enu.nextElement();
				String strActualArg = getArg( strGivenkey );
				System.out.println( strGivenkey + " = " + strActualArg );
			}
		}

		try {
			// 1. Check: Sind alle requiredArgs gesetzt
			String[] requiredArgs = getRequiredArgs();
			for( i = 0; i < requiredArgs.length; i++ ) {
				if( getArg( requiredArgs[i] ) == null )
					return false;
				if( getArg( requiredArgs[i] ).equals( "" ) == true )
					return false;
			}
			if( debug_CheckArgs ) {
				System.out.println( "Done 1. Check" );
			}

			// 2. Check: Sind �berfl�ssige Argumente gesetzt, d.h. Argumente, die weder required noch optional sind
			// Gesetzte Argumente, die mit "RESULT", "MIN", "MAX", "HWT" oder "HRT" beginnen, werden hierbei nicht analysiert, es wird
			// jedoch der maximale Index festgehalten. Die detaillierte Analyse erfolgt im 3-ten Check.
			Enumeration<?> enu = getArgs().keys();
			String[] optionalArgs = getOptionalArgs();
			String givenkey;
			String temp;
			while( enu.hasMoreElements() ) {
				givenkey = (String) enu.nextElement();
				// Wenn Argument ohne Index
				if( (givenkey.startsWith( "RESULT" ) == false) && (givenkey.startsWith( "MIN" ) == false) && (givenkey.startsWith( "MAX" ) == false) && (givenkey.startsWith( "HRT" ) == false) && (givenkey.startsWith( "NO_MIN_MAX" ) == false) && ((givenkey.startsWith( "HWT" ) == false) || (givenkey.equals( "HWT" ) == true)) ) {

					j = 0;
					bRequiredArg = false;
					while( (bRequiredArg == false) && (j < requiredArgs.length) ) {
						if( givenkey.equals( requiredArgs[j] ) == true )
							bRequiredArg = true;
						j++;
					}
					j = 0;
					bOptionalArg = false;
					while( (bOptionalArg == false) && (j < optionalArgs.length) ) {
						if( givenkey.equals( optionalArgs[j] ) == true )
							bOptionalArg = true;
						j++;
					}
					if( !(bRequiredArg || bOptionalArg) )
						return false;
					if( getArg( givenkey ).equals( "" ) == true )
						return false;
				}
				// Wenn Argument mit Index
				else {
					if( givenkey.startsWith( "RESULT" ) == true ) {
						temp = givenkey.substring( 6 );
					} else {
						if( givenkey.startsWith( "NO_MIN_MAX" ) == true ) {
							temp = givenkey.substring( 10 );
						} else
							temp = givenkey.substring( 3 );
					}
					try {
						j = Integer.parseInt( temp );
						if( j < 1 )
							return false;
						if( j > maxIndex )
							maxIndex = j;
					} catch( NumberFormatException e ) {
						return false;
					}
				}
			}
			if( debug_CheckArgs ) {
				System.out.println( "Done 2. Check" );
			}

			// 3. Check: Sind die Result- und Min-Argumente plausibel, Max-Argumente sind optional und werden daher gefordert.
			// Existieren jedoch Max-Werte, wird die Anzahl sowie die Konvertierung von Minwerten und Maxwerten zu Zahlen gepr�ft,
			// sofern es sich um kein Result einer anderen PP handelt
			// 
			// Funktion wurde erweitert um NO_MIN_MAX
			// Wenn NO_MIN_MAX vorhanden, dann darf es kein MIN oder MAX geben
			String[] mins, maxs;
			for( i = 1; i <= maxIndex; i++ ) {
				sResultLine = getArg( "RESULT" + i );
				if( sResultLine == null )
					return false;
				// Wenn ein ';' im Resultnamen vorkommt (evtl. nach erstem ; Ergebnissatz)
				if( sResultLine.indexOf( ';' ) != -1 ) {
					saParamDetails = sResultLine.split( ";" );
					if( saParamDetails.length == 2 ) {
						temp = saParamDetails[1];
						try {
							j = Integer.parseInt( temp );
							if( j < 0 )
								return false;
						} catch( NumberFormatException e ) {
							return false;
						}
					} else {
						return false;
					}
				}
				if( (i > 1) && (sResultLine.equals( getArg( "RESULT1" ) ) == true) )
					return false;
				temp = getArg( "MIN" + i );
				if( temp == null && getArg( "NO_MIN_MAX" + i ) == null )
					return false; // bei MIN, darf es kein NO_MIN_MAX geben
				if( getArg( "MAX" + i ) != null && getArg( "NO_MIN_MAX" + i ) != null )
					return false; // bei MAX, darf es kein NO_MIN_MAX geben
				if( getArg( "MAX" + i ) != null && getArg( "NO_MIN_MAX" + i ) == null ) {
					mins = splitArg( getArg( "MIN" + i ) );
					maxs = splitArg( getArg( "MAX" + i ) );
					if( mins.length != maxs.length )
						return false;
					for( j = 0; j < mins.length; j++ ) {
						try {
							if( mins[j].indexOf( '@' ) == -1 )
								parseDouble( mins[j] );
							else
								parseDouble( getPPResult( mins[j] ) );
							if( maxs[j].indexOf( '@' ) == -1 )
								parseDouble( maxs[j] );
							else
								parseDouble( getPPResult( maxs[j] ) );
						} catch( NumberFormatException e ) {
							return false;
						} catch( InformationNotAvailableException e ) {
							return false;
						}
					}
				}
			}
			if( debug_CheckArgs ) {
				System.out.println( "Done 3. Check" );
			}

			//bei TIMEOUT ist ein @-Konstrukt zul�ssig und ein long bzw. int value
			String timeoutTest = getArg( optionalArgs[4] );
			if( (timeoutTest != null) && (timeoutTest.indexOf( '@' ) == -1) ) {
				try {
					if( (Long.parseLong( timeoutTest )) < 0 )
						return false;
				} catch( NumberFormatException e ) {
					return false;
				}
			}

			//4. Check: Falls TIMEOUT, PAUSE und OK_TIME gesetzt, mu� Integer sein
			for( i = 5; i <= 6; i++ ) {
				if( getArg( optionalArgs[i] ) != null ) {
					try {
						if( (Long.parseLong( getArg( optionalArgs[i] ) )) < 0 )
							return false;
					} catch( NumberFormatException e ) {
						return false;
					}
				}
			}
			if( debug_CheckArgs ) {
				System.out.println( "Done 4. Check" );
			}

			// 5. Check: Argmente SGBD, JOB, RESULT[1..N] d�rfen keine Alternativen enthalten
			enu = getArgs().keys();
			while( enu.hasMoreElements() ) {
				givenkey = (String) enu.nextElement();
				if( (givenkey.equals( "SGBD" ) == true) || (givenkey.equals( "JOB" ) == true) ) {
					if( getArg( givenkey ).indexOf( ';' ) != -1 )
						return false;
				}
			}
			if( debug_CheckArgs ) {
				System.out.println( "Done 5. Check" );
			}

			// 6. Check: Argumente INVERT, IGNORE_ERROR, UDN, CANCEL und SHOW_COUNTDOWN m�ssen TRUE oder FALSE sein, falls vorhanden
			temp = getArg( getOptionalArgs()[9] );
			if( (temp != null) && (!temp.equalsIgnoreCase( "TRUE" )) && (!temp.equalsIgnoreCase( "FALSE" )) )
				return false;
			temp = getArg( getOptionalArgs()[11] );
			if( (temp != null) && (!temp.equalsIgnoreCase( "TRUE" )) && (!temp.equalsIgnoreCase( "FALSE" )) )
				return false;
			temp = getArg( getOptionalArgs()[12] );
			if( (temp != null) && (!temp.equalsIgnoreCase( "TRUE" )) && (!temp.equalsIgnoreCase( "FALSE" )) )
				return false;
			temp = getArg( getOptionalArgs()[19] ); // DOKU
			if( (temp != null) && (!temp.equalsIgnoreCase( "TRUE" )) && (!temp.equalsIgnoreCase( "FALSE" )) )
				return false;
			temp = getArg( getOptionalArgs()[20] ); // UDN
			if( (temp != null) && (!temp.equalsIgnoreCase( "TRUE" )) && (!temp.equalsIgnoreCase( "FALSE" )) )
				return false;
			temp = getArg( getOptionalArgs()[23] ); // SHOW_COUNTDOWN
			if( (temp != null) && (!temp.equalsIgnoreCase( "TRUE" )) && (!temp.equalsIgnoreCase( "FALSE" )) )
				return false;
			if( debug_CheckArgs ) {
				System.out.println( "Done 6. Check" );
			}
			// Tests bestanden, somit ok
			if( debug_CheckArgs ) {
				System.out.println( "ok = true" );
				System.out.println( "End checkArgs() from DiagToleranz." );
			}
			return true;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result = null;
		boolean exceptionExists = false;

		try {
			/***********************************************
			 * Label in Transientdatei schreiben, die folgende Zeile ist auch noetig
			 * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
			 ***********************************************/
			try {
				UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
				myAnalyser.LogSetTestStepName( this.getName() );
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e ) {
				try {
					getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
				} catch( Exception e1 ) {
				}
			}
			try {
				//Variablen initialisieren
				initExecution();
			} catch( PPExecutionException e ) {
				if( e.getMessage() != null ) {
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				} else {
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				}
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Wurde ggf. ein notwendiger SGBD_SIV noch nicht bzw. fehlerhaft ausgef�hrt od. ist dessen Ergebnis nicht im virt. Fzg., dann versuche dies vorab nachzuholen
			if( varianteAtSGBD_SIVUnavailable && !bUseSimRes) {
				executeSGBDSIV( info );
			}

			//Ediabas initialisieren
			initEdiabas();

			//wenn kein Ergebnis simuliert werden soll - sondern Verwendung von "echter Diagnose"
			if( !bUseSimRes ) {
				execution( info );
				//wenn das Ergebnis simuliert werden soll
			} else {
				executionWithSimulationResult();
			}

		} catch( VariablesException e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "VariablesException", e.getMessage(), Ergebnis.FT_NIO_SYS );
			exceptionExists = true;
		} catch( IOException e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "IOException", e.getMessage(), Ergebnis.FT_NIO_SYS );
			exceptionExists = true;
		} catch( DeviceLockedException e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", e.getMessage(), Ergebnis.FT_NIO_SYS );
			exceptionExists = true;
		} catch( DeviceNotAvailableException e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", e.getMessage(), Ergebnis.FT_NIO_SYS );
			exceptionExists = true;
		} catch( ApiCallFailedException e ) {
			if( result_index > 0 ) {
				if( results[result_index - 1] == null ) {
					results[result_index - 1] = "null";
				}
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, results[result_index - 1], "", "", "", "0", "", "", "", "ApiCallFailedException", ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
			} else {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", "ApiCallFailedException", ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
			}
			exceptionExists = true;
		} catch( EdiabasResultNotFoundException e ) {
			if( result_index > 0 ) {
				if( results[result_index - 1] == null ) {
					results[result_index - 1] = "null";
				}
				if( e.getMessage() != null ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, results[result_index - 1], "", "", "", "0", "", "", "", "EdiabasResultNotFoundException", e.getMessage(), Ergebnis.FT_NIO_SYS );
				} else {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, results[result_index - 1], "", "", "", "0", "", "", "", "EdiabasResultNotFoundException", "", Ergebnis.FT_NIO_SYS );
				}
			} else {
				if( e.getMessage() != null ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", "EdiabasResultNotFoundException", e.getMessage(), Ergebnis.FT_NIO_SYS );
				} else {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", "EdiabasResultNotFoundException", "", Ergebnis.FT_NIO_SYS );
				}
			}
			exceptionExists = true;
		} catch( PPExecutionException e ) {
			if( e.getMessage() != null ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "PPExecutionException", e.getMessage(), Ergebnis.FT_NIO_SYS );
			}
			exceptionExists = true;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			exceptionExists = true;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			exceptionExists = true;
		} finally {
			if( exceptionExists ) {
				if( result != null ) {
					ergListe.add( result );
				}
				status = STATUS_EXECUTION_ERROR;
			}
			//Devices freigeben, Pr�fprozedur-Status setzen
			terminateExecution( info );
		}
	}

	/**
	 * gibt s�mtliche Devices frei und setzt den Status der PP.
	 */
	private void terminateExecution( ExecutionInfo info ) {
		Ergebnis result;
		int i;

		//Freigabe der benutzten Devices
		if( debug_Standard ) {
			System.out.println( "Device Freigabe" );
		}
		if( udInUse == true ) {
			try {
				if( udn ) {
					myDialogUDN.releaseUserDialogNext( udn_handle );
				} else {
					getPr�flingLaufzeitUmgebung().releaseUserDialog();
				}
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException ) {
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
				} else {
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Exception: " + e.getMessage(), Ergebnis.FT_NIO_SYS );
				}
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
		if( debug_Standard ) {
			System.out.println( "Device Freigabe fertig" );
		}

		if( debug_Standard ) {
			System.out.println( "Adding " + ergListe.size() + " Results" );
			for( i = 0; i < ergListe.size(); i++ ) {
				Ergebnis erg = (Ergebnis) ergListe.get( i );
				System.out.println( erg.toString() );
			}
		}

		// MBa: Paralleldiagnose
		if( parallelDiagnose && ediabas != null && sgbd != null ) {
			try {
				devMan.releaseEdiabasParallel( ediabas, diagnosetag, sgbd );
				if( debug_Standard ) {
					System.out.println( "Release SGBD: " + sgbd + " ediabas: " + ediabas.getName() );
				}
			} catch( DeviceLockedException ex ) {
				result = new Ergebnis( "ExecFehler", "EDIABAS", sgbd, "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", ex.getMessage(), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "EDIABAS", sgbd, "", "", "", "", "", "", "0", "", "", "", "Exception", "Unexpected runtime error at releasing the parallel diagnosis", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable ex ) {
				result = new Ergebnis( "ExecFehler", "EDIABAS", sgbd, "", "", "", "", "", "", "0", "", "", "", "Throwable", "Unexpected runtime error at releasing the parallel diagnosis", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		//Status setzen
		setPPStatus( info, status, ergListe );
	}

	/**
	 * initialisiert s�mtliche Variablen und lie�t die entsprechenden Argumente/PS-Variablen aus.
	 * @throws PPExecutionException 
	 */
	private void initExecution() throws PPExecutionException {

		String temp;
		int i, j;
		String debugString = null;
		udInUse = false;
		udn = false;
		myDialogUDN = null;
		udn_handle = null;
		ergListe = new Vector<Ergebnis>();
		status = STATUS_EXECUTION_OK;
		devMan = null;
		ediabas = null;
		parallelDiagnose = false;
		diagnosetag = null;
		sgbd = null;
		varianteAtSGBD_SIVUnavailable = false; // true, wenn ein notwendiger SGBD_SIV noch nicht bzw. fehlerhaft ausgef�hrt wurde od. wenn dessen Ergebnis noch nicht im virt. Fzg. ist
		ignore_errors = false;
		cancel = false;
		doku = false;
		bGetAllResults = false;
		results = null;
		iaResultSet = null;
		invert = false;
		jobparameters = null;
		hwts = null;
		bNoMinMax = null;
		showCountdown = false;
		bSimulationWithoutEdiabas = false;
		bUseSimRes = false;
		myDialog = null;
		udn_modelListener = null;
		debug_Standard = false;
		debug_CheckArgs = false;
		firstOkTime = Long.MAX_VALUE;
		strFensterName = "";
		debug_Standard = false;
		debug_CheckArgs = false;

		if( getArg( "DEBUG" ) != null ) {
			debugString = getArg( "DEBUG" ); // debug argument in PP (preference)
		} else if( getPr�fling().getAttribut( "DEBUG" ) != null ) {
			debugString = getPr�fling().getAttribut( "DEBUG" ); // debug attribute in P
		}

		if( debugString != null ) {
			String[] debugFlags = splitArg( debugString );
			int n = debugFlags.length;
			if( n >= 1 ) {
				for( i = 0; i < n; i++ ) {
					if( debugFlags[i].equalsIgnoreCase( "DIAG_TOLERANZ" ) ) {
						debug_Standard = true;
					} else if( debugFlags[i].equalsIgnoreCase( "DIAG_TOLERANZ_CHECK_ARGS" ) ) {
						debug_CheckArgs = true;
					} else if( debugFlags[i].equalsIgnoreCase( "ALL" ) ) {
						debug_Standard = true;
						debug_CheckArgs = true;
					}
				}
			}
		}

		// Initial debug lines
		if( debug_Standard ) {
			System.out.println( "---" );
			System.out.println( "Start execute() from  DiagToleranz." );
			System.out.println( "Test step: " + this.getName() );
			System.out.println( "---" );
		}
		if( debugString != null ) {
			System.out.println( "The debug string is: " + debugString );
			System.out.println( "As a i_oResult of debug arguments/attributes the debug flags are: " );
			System.out.println( "i_bDiagToleranzDebug_Standard = " + debug_Standard );
			System.out.println( "i_bDiagToleranzDebug_CheckArgs = " + debug_CheckArgs );
		}

		if( checkArgs() == false )
			throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
		// Wurde ggf. ein notwendiger SGBD_SIV noch nicht bzw. fehlerhaft ausgef�hrt od. ist dessen Ergebnis noch nicht im virt. Fzg.
		try {
			sgbd = extractValues( getArg( getRequiredArgs()[0] ) )[0];
		} catch( PPExecutionException ppe ) {
			if( ppe.getMessage().toUpperCase().indexOf( "VARIANTE@SGBD_SIV" ) != -1 ) {
				varianteAtSGBD_SIVUnavailable = true;
			} else {
				throw ppe;
			}
		}
		job = getArg( getRequiredArgs()[1] );
		jobpar = getArg( getOptionalArgs()[0] );
		if( jobpar == null ) {
			jobpar = "";
		} else {
			if( jobpar.indexOf( '@' ) != -1 ) {
				jobparameters = extractValues( jobpar );
				jobpar = "";
				for( int k = 0; k < jobparameters.length - 1; k++ ) {
					jobpar = jobpar + jobparameters[k] + ";";
				}
				jobpar = jobpar + jobparameters[jobparameters.length - 1];
			}
		}
		jobres = "";
		resAnzahl = 0;
		while( (sCurrentResultLine = getArg( "RESULT" + (resAnzahl + 1) )) != null ) {
			resAnzahl++;
		}
		iaResultSet = new int[resAnzahl];
		if( debug_Standard ) {
			System.out.println( "Es gibt " + resAnzahl + " RESULT-Argumente:" );
		}

		int iCurrentResIndex = 0;
		int iCurrentResultSet = 1;
		bGetAllResults = false;
		while( (sCurrentResultLine = getArg( "RESULT" + (iCurrentResIndex + 1) )) != null ) {
			if( sCurrentResultLine.indexOf( ';' ) != -1 ) {
				saCurrentResultDetails = sCurrentResultLine.split( ";" );
				if( saCurrentResultDetails.length == 2 ) {
					sCurrentResultName = saCurrentResultDetails[0];
					sCurrentResultSet = saCurrentResultDetails[1];
					try {
						iCurrentResultSet = Integer.parseInt( sCurrentResultSet );
						iaResultSet[iCurrentResIndex] = iCurrentResultSet;
						if( iCurrentResultSet != 1 ) {
							bGetAllResults = true;
						}
					} catch( NumberFormatException e ) {
						throw new PPExecutionException( "Invalid set number in argument RESULT" + (iCurrentResIndex + 1) + " = " + sCurrentResultLine );
					}
				} else {
					throw new PPExecutionException( "Invalid number of parameter details in RESULT" + (iCurrentResIndex + 1) + " = " + sCurrentResultLine );
				}
			} else {
				sCurrentResultName = sCurrentResultLine;
				iaResultSet[iCurrentResIndex] = 1;
			}
			if( debug_Standard ) {
				System.out.println( "RESULT" + (iCurrentResIndex + 1) + "=" + sCurrentResultName );
			}
			jobres = jobres + ";" + extractValues( sCurrentResultName )[0];
			iCurrentResIndex++;
		}
		//NO_MIN_MAX[1..N]
		bNoMinMax = new boolean[resAnzahl];
		if( resAnzahl > 0 ) {
			for( i = 0; i < resAnzahl; i++ ) {
				try {
					bNoMinMax[i] = new Boolean( extractValues( getArg( "NO_MIN_MAX" + (i + 1) ) )[0] ).booleanValue();
				} catch( Exception e ) {
					//nichts, bei Exception ist bNoMinMax[i]=false
				}
			}
		}
		//CHECK MIN / MAX	
		if( resAnzahl > 0 ) {
			jobres = jobres.substring( 1 );
			results = splitArg( jobres ); //Links sind in jobres bereits aufgel�st
			if( results.length != resAnzahl ) {
				throw new PPExecutionException( PB.getString( "parameterexistenz" ) + "1" );
			}
			for( i = 1; i <= resAnzahl; i++ ) {
				if( getArg( "NO_MIN_MAX" + i ) != null ) {
					continue;
				}
				mins = extractValues( getArg( "MIN" + i ) );
				if( getArg( "MAX" + i ) != null ) {
					maxs = extractValues( getArg( "MAX" + i ) );
					for( j = 0; j < mins.length; j++ ) {
						try {
							parseDouble( mins[j] );
							parseDouble( maxs[j] );
						} catch( NumberFormatException e ) {
							throw new PPExecutionException( PB.getString( "parameterexistenz" ) + "2" );
						}
					}
				}
			}
		} else {
			jobres = "JOB_STATUS";
		}
		if( debug_Standard ) {
			System.out.println( "results=" + results );
		}

		//OK_Time
		temp = getArg( getOptionalArgs()[5] );
		if( temp == null ) {
			okTime = -1;
		} else {
			okTime = Long.parseLong( temp );
		}
		//Pause
		temp = getArg( getOptionalArgs()[6] );
		if( temp == null ) {
			pause = 0;
		} else {
			pause = Long.parseLong( temp );
		}
		//Anweisungstext
		awt = getArg( getOptionalArgs()[7] );
		if( awt == null ) {
			awt = "";
		} else {
			awt = PB.getString( awt );
		}
		//Hinweistext
		hwt = getArg( getOptionalArgs()[8] );
		if( hwt == null ) {
			hwt = "";
		} else {
			hwt = PB.getString( hwt );
		}
		// Ersetze @-Operator variablen im String
		hwt = replaceVariablesInString( hwt );

		// mehrere Hinweistexte einlesen, wenn vorhanden
		hwts = new String[resAnzahl];
		for( i = 0; i < resAnzahl; i++ ) {
			hwts[i] = getArg( "HWT" + (i + 1) );
			// Ersetze @-Operator variablen im String
			hwts[i] = replaceVariablesInString( hwts[i] );
		}

		//Invert
		temp = getAttribut( "INVERT" );
		if( temp != null ) {
			if( temp.equalsIgnoreCase( "TRUE" ) == true ) {
				invert = true;
			} else {
				invert = false;
			}
		}
		//Hinweistext aus Result
		hrts = new String[resAnzahl];
		for( i = 0; i < resAnzahl; i++ ) {
			hrts[i] = getArg( "HRT" + (i + 1) );
		}

		// Der Parameter �berschreibt das Attribut, das Attribut f�llt sp�ter weg
		temp = getArg( getOptionalArgs()[9] );
		if( (temp != null) && temp.equalsIgnoreCase( "TRUE" ) ) {
			invert = true;
		}
		if( (temp != null) && temp.equalsIgnoreCase( "FALSE" ) ) {
			invert = false;
		}
		// IGNORE_ERRORS
		temp = getArg( getOptionalArgs()[11] );
		if( (temp != null) && temp.equalsIgnoreCase( "TRUE" ) ) {
			ignore_errors = true;
		}
		// CANCEL
		temp = getArg( getOptionalArgs()[12] );
		if( (temp != null) && temp.equalsIgnoreCase( "TRUE" ) ) {
			cancel = true;
		}
		// DOKU
		temp = getArg( getOptionalArgs()[19] );
		if( (temp != null) && temp.equalsIgnoreCase( "TRUE" ) ) {
			bGetAllResults = true; //wenn der Parameter DOKU=true ist, so m�ssen die Ergebnisse zu allen Jobs�tzen ausgelesen werden
			doku = true;
		}
		// UDN
		temp = getArg( getOptionalArgs()[20] );
		if( (temp != null) && temp.equalsIgnoreCase( "TRUE" ) ) {
			if( !this.isMultiInstance() ) {
				udn = true;
			} else {
				// No UDN for MultiInstance Mode
				if( debug_Standard ) {
					System.out.println( "Kein UDN in Multiinstanzmodus verf�gbar." );
				}
			}
		}
		// SHOW_COUNTDOWN 
		temp = getArg( getOptionalArgs()[23] );
		if( (temp != null) && temp.equalsIgnoreCase( "TRUE" ) ) {
			showCountdown = true;
		}

		// Image. Dies ist das Bild, welches der Dialog anzeigen soll (falls gew�nscht).
		image = getArg( getOptionalArgs()[21] ); //kann auch null sein, dann kein Bild

		// Position des Bildes in Bezug zum Meldungstext.
		imageLocation = getArg( getOptionalArgs()[22] ); //kann auch null sein, dann existiert die Angabe nicht und der Default wird angenommen

		//SIM_RES
		sim_res = getArg( "SIM_RES" );

		// Simulation (without EDIABAS)
		try {
			String sSim = (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "SimulationWithoutEdiabas" );
			if( sSim != null && sSim.equalsIgnoreCase( "TRUE" ) ) {
				bSimulationWithoutEdiabas = true;
				if( debug_Standard ) {
					System.out.println( "Simulation without Ediabas; sSim=" + sSim );
				}
			}

			if( sim_res != null && bSimulationWithoutEdiabas ) {
				bUseSimRes = true;
			}
		} catch( VariablesException ex ) {
			if( debug_Standard ) {
				System.out.println( "Variable SimulationWithoutEdiabas nicht vorhanden: " + ex.getMessage() );
			}
		}
		try {
			// MBa: Paralleldiagnose >>>>>
			// Pr�fstandvariabel auslesen, ob Paralleldiagnose
			Object pr_var;
			pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
			if( pr_var != null ) {
				if( pr_var instanceof String ) {
					if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
						parallelDiagnose = true;
					}
				} else if( pr_var instanceof Boolean ) {
					if( ((Boolean) pr_var).booleanValue() ) {
						parallelDiagnose = true;
					}
				}
			}
		} catch( VariablesException ex ) {
			if( debug_Standard ) {
				System.out.println( "Variable DIAG_PARALLEL nicht vorhanden: " + ex.getMessage() );
			}
			//diese Exception f�hrt dazu, dass die DiagToleranz mit einer PPExecution Exception abbricht
			//dies ist so gewollt, da wenn in der DiagOpen die Variable "DIAG_PARALLEL" nicht initialisiert wurde
			//die Diagnose nicht korrekt aufgebaut werden konnte und damit eine Diagnose keinen Sinn macht
			throw new PPExecutionException( "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL" );
		}

		if( debug_Standard ) {
			System.out.println( "finished Execution Init!" );
		}

	}

	/**
	 * Initialisiert die Ediabas Anbindung f�r das Ausf�hren von Diagnose Jobs
	 * @throws PPExecutionException 
	 */
	private void initEdiabas() throws PPExecutionException {

		Ergebnis result;

		// Devicemanager
		devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();
		if( getArg( "TAG" ) != null )
			diagnosetag = getArg( "TAG" );

		try {
			if( parallelDiagnose ) { // Ediabas holen	
				ediabas = devMan.getEdiabasParallel( diagnosetag, sgbd );
			} else {
				ediabas = devMan.getEdiabas( diagnosetag );
			}
			if( debug_Standard ) {
				System.out.println( "Init EDIABAS, SGBD: " + sgbd + " ediabas: " + ediabas.getName() );
			}

		} catch( DeviceLockedException ex ) {
			result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", ex.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			throw new PPExecutionException();
		} catch( DeviceNotAvailableException ex ) {
			result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", ex.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			throw new PPExecutionException();
		} catch( Exception ex ) {
			result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			throw new PPExecutionException();
		} catch( Throwable ex ) {
			result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			throw new PPExecutionException();
		}
	}

	/**
	 * Diese Methode dient zum Ausf�hren des Diagnosejobs sowie der Analyse bzw. Dokumentation der Results.
	 * 
	 * @param info - die Ausf�hrungsinformation
	 * @throws VariablesException 
	 * @throws DeviceLockedException
	 * @throws DeviceNotAvailableException  
	 * @throws IOException  
	 * @throws PPExecutionException
	 * @throws ApiCallFailedException  
	 * @throws EdiabasResultNotFoundException  
	 */
	private void execution( ExecutionInfo info ) throws VariablesException, DeviceLockedException, DeviceNotAvailableException, IOException, PPExecutionException, ApiCallFailedException, EdiabasResultNotFoundException {

		// spezifische Variablen
		String temp = "";
		long endeZeit;
		boolean invalid = false;

		final AtomicBoolean udnCanceled = new AtomicBoolean( false );

		if( debug_Standard ) {
			System.out.println( "Start of execution and analysis loop:" );
		}

		//		wurde nach vorne geschoben
		//		// Wurde ggf. ein notwendiger SGBD_SIV noch nicht bzw. fehlerhaft ausgef�hrt od. ist dessen Ergebnis nicht im virt. Fzg., dann versuche dies vorab nachzuholen
		//		if( varianteAtSGBD_SIVUnavailable ) {
		//			executeSGBDSIV( info );
		//		}

		//Timeout Parameter holen
		//diese Parameter werden hier geholt, da man das Zeitmessergebnis so wenig wie m�glich f�lschen m�chte
		if( debug_Standard ) {
			System.out.println( "Timeoutparameter holen:" );
		}

		//Timeout
		temp = getArg( getOptionalArgs()[4] );
		endeZeit = 0; //Vorinitialisierung
		if( temp == null ) {
			endeZeit = System.currentTimeMillis();
		} else if( temp.indexOf( '@' ) != -1 ) {
			String[] timeoutArray = extractValues( temp );
			temp = "";
			if( timeoutArray.length == 1 ) {
				try {
					endeZeit = System.currentTimeMillis() + new Long( timeoutArray[0] ).longValue();
				} catch( NumberFormatException e ) {
					endeZeit = System.currentTimeMillis();
				}
			} else {//wenn die Auswertung des @-Konstrukts keinen sinnvollen Wert ergibt, lieber den Timeout auf 0 setzen
				endeZeit = System.currentTimeMillis();
			}
		} else {
			endeZeit = System.currentTimeMillis() + Long.parseLong( temp );
		}

		if( debug_Standard ) {
			System.out.println( "Timeoutparameter geholt!" );
		}

		//Die Ausf�hrungs- und Analyseschleife
		do {
			//Diagnose durchf�hren
			invalid = executeDiagnosticJob();

			//Ergebnis-Analyse
			executeAnalysis( invalid );

			//(Warte)-Zeiten anzeigen und pausieren
			doTimeAndUDHandling( endeZeit, udnCanceled );

		} while( (endeZeit > System.currentTimeMillis()) && (status != STATUS_EXECUTION_OK) );

		if( debug_Standard ) {
			System.out.println( "End of execution and analysis loop!" );
		}

	}

	/**
	 * Zeigt Wartezeiten an.
	 * Wenn eine Pause-Zeit vorgegeben ist, so schl�ft die PP die vorgegebene Zeit.
	 * @param endeZeit
	 * @throws DeviceLockedException
	 * @throws DeviceNotAvailableException
	 * @throws PPExecutionException
	 */

	private void doTimeAndUDHandling( long endeZeit, AtomicBoolean udnCanceled ) throws DeviceLockedException, DeviceNotAvailableException, PPExecutionException {
		long deltaT;
		Ergebnis result;

		if( okTime > 0 ) { //Unter Umst�nden Zeitdauer noch nicht erf�llt
			if( status == STATUS_EXECUTION_OK ) {
				if( firstOkTime > System.currentTimeMillis() ) {
					// Letzte Ausf�hrung war NIO
					firstOkTime = System.currentTimeMillis();
					result = new Ergebnis( "OK-Zeit", "", "", "", "", "Dauer", "0", "" + okTime, "" + Long.MAX_VALUE, "0", "", "", "", PB.getString( "okZeitUnterschritten" ), "", Ergebnis.FT_NIO );
					status = STATUS_EXECUTION_ERROR;
				} else {
					//Letzte bzw. die letzten Ausf�hrung waren IO
					deltaT = System.currentTimeMillis() + pause - firstOkTime;
					if( deltaT < okTime ) {
						status = STATUS_EXECUTION_ERROR;
						result = new Ergebnis( "OK-Zeit", "", "", "", "", "Dauer", "" + deltaT, "" + okTime, "" + Long.MAX_VALUE, "0", "", "", "", PB.getString( "okZeitUnterschritten" ), "", Ergebnis.FT_NIO );
					} else {
						result = new Ergebnis( "OK-Zeit", "", "", "", "", "Dauer", "" + deltaT, "" + okTime, "" + Long.MAX_VALUE, "0", "", "", "", "", "", Ergebnis.FT_IO );
					}
				}
				ergListe.add( result );
			} else {
				//Fehlerhafte Ausf�hrung, Zeitstempel-Reset
				firstOkTime = Long.MAX_VALUE;
			}
		}

		if( (status != STATUS_EXECUTION_OK) && (endeZeit > System.currentTimeMillis()) ) {
			//Nur wenn Loop nicht endet
			if( awt.equals( "" ) == false ) {
				long countdown = (endeZeit - System.currentTimeMillis()) / 1000 + 1; //Z�hler, welcher auf Wunsch im AWT angezeigt wird 

				if( udInUse == false ) {
					//nur wenn was auszugeben ist und dieses noch nicht ausgegeben wurde
					if( strFensterName == null ) { // Default Fenstername, wenn nichts spezifiziert
						strFensterName = PB.getString( "anweisung" );
					}
					if( udn ) {
						// Show User Dialog Next
						myDialogUDN = getPr�flingLaufzeitUmgebung().getUserDialogNext();
						udn_handle = myDialogUDN.allocateUserDialogNext();
						// Installiere Listener f�r UDN
						udn_modelListener = new UDNListener( udnCanceled );

						myDialogUDN.getModel( udn_handle ).addUDNModelListener( udn_modelListener );
						showAwtDialog( showCountdown ? countdown + " s: " + awt : awt, cancel, myDialogUDN, udn_handle, imageLocation, image );
					} else {
						// Show Normal User Dialog
						myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
						if( cancel )
							myDialog.setAllowCancel( true );
						showAwtDialog( showCountdown ? countdown + " s: " + awt : awt, myDialog );
					}
					udInUse = true;
				} else {
					// Falls ein Z�hler angezeigt werden soll, dann
					// aktualisiere den AWT-Dialog pro Iteration. 
					if( showCountdown ) {
						if( udn ) {
							updateAwtDialog( countdown + " s: " + awt, cancel, myDialogUDN, udn_handle );
						} else {
							updateAwtDialog( countdown + " s: " + awt, myDialog );
						}
					}

					// Pr�fe, ob Dialog beendet wurde
					if( udn ) {
						if( udnCanceled.get() ) {
							result = new Ergebnis( "Frage", "Userdialog", "", "", "", "", "", "", "", "0", awt, PB.getString( "werkerAbbruch" ), "", PB.getString( "pollingAbbruch" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						}
					} else {
						if( myDialog != null && (myDialog.isCancelled() == true) ) {
							result = new Ergebnis( "Frage", "Userdialog", "", "", "", "", "", "", "", "0", awt, PB.getString( "werkerAbbruch" ), "", PB.getString( "pollingAbbruch" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						}
					}
				}
			}
		}

		// Wartezeit???
		if( (pause > 0) && (endeZeit > System.currentTimeMillis()) && (status != STATUS_EXECUTION_OK) ) {
			if( debug_Standard ) {
				System.out.println( "Start Pause" );
			}
			try {
				Thread.sleep( pause );
			} catch( InterruptedException e ) {
			}
			if( debug_Standard ) {
				System.out.println( "End Pause" );
			}
		}

		if( debug_Standard ) {
			System.out.println( "Analyse stop" );
		}

	}

	/**
	 * Analysiert die Ergebnisse
	 * @param invalid - speichert ob die Diagnose valide war
	 * @throws ApiCallFailedException
	 * @throws EdiabasResultNotFoundException
	 * @throws PPExecutionException
	 */
	private void executeAnalysis( boolean invalid ) throws ApiCallFailedException, EdiabasResultNotFoundException, PPExecutionException {
		if( debug_Standard ) {
			System.out.println( "Analyse start" );
		}
		// spezifische Variablen
		Ergebnis result;
		String temp = "", temp1 = "", temp2 = "";
		int j;
		boolean ok;
		double d;

		status = STATUS_EXECUTION_OK;
		if( invalid == false ) {

			/**
			 * Wenn DOKU=True und keine Ergebnisse parametriert wurden,
			 * sollen alle Ergebnisse in allen Jobs�tzen unbewertet f�r APDM dokumentiert werden.
			 */
			if( (doku == true) && (resAnzahl == 0) ) {
				EdiabasResult er = ediabas.getResult();
				EdiabasResultSet[] ers = er.getResultSets();
				for( int m = 0; m < ediabas.getDiagJobSaetze(); m++ ) {
					Set<?> testSet = ers[m].keySet();
					Object[] resultNames = (Object[]) testSet.toArray();
					for( int n = 0; n < resultNames.length; n++ ) {
						temp = ediabas.getDiagResultValue( m, resultNames[n].toString() );
						result = new Ergebnis( resultNames[n].toString(), "EDIABAS", sgbd, job, jobpar, "Satz " + m + ": " + resultNames[n], temp, "", "", "0", "", "", awt, "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
				}
			}
			result_index = 0;
			for( result_index = 1; result_index <= resAnzahl; result_index++ ) {
				// Ergebnis abholen
				if( !bSimulationWithoutEdiabas ) {
					temp = ediabas.getDiagResultValue( iaResultSet[result_index - 1], results[result_index - 1] );
				} else {
					temp = "0.0";
				}
				if( getArg( "MIN" + result_index ) != null && !bNoMinMax[result_index - 1] ) {

					mins = extractValues( getArg( "MIN" + result_index ) );
					temp1 = mins[0];
					for( j = 1; j < mins.length; j++ ) {
						temp1 = temp1 + ";" + mins[j];
					}
					if( getArg( "MAX" + result_index ) != null ) {
						maxs = extractValues( getArg( "MAX" + result_index ) );
						temp2 = maxs[0];
						for( j = 1; j < maxs.length; j++ ) {
							temp2 = temp2 + ";" + maxs[j];
						}
					} else {
						maxs = null;
						temp2 = "";
					}
					if( maxs == null ) {
						//Stringanalyse
						ok = false;
						for( j = 0; j < mins.length; j++ ) {
							if( mins[j].equalsIgnoreCase( temp ) == true )
								ok = true;
						}
						// Simulation --> ok = true
						if( bSimulationWithoutEdiabas )
							ok = true;
						if( (ok == true) && (invert == false) ) {
							//Toleranz, somit IO
							result = new Ergebnis( results[result_index - 1], "EDIABAS", sgbd, job, jobpar, results[result_index - 1], temp, temp1, "", "0", "", "", awt, "", "", Ergebnis.FT_IO );
						} else if( (ok == false) && (invert == true) ) {
							//Invertierte Toleranz, somit IO
							result = new Ergebnis( results[result_index - 1], "EDIABAS", sgbd, job, jobpar, results[result_index - 1], temp, "!(" + temp1 + ")", "", "0", "", "", awt, "", "", Ergebnis.FT_IO );
						} else if( (ok == false) && (invert == false) ) {
							//Toleranz, somit NIO
							String hwtDummy = "";

							// wenn Hinweistext aus Result parametriert
							if( (hrts[result_index - 1] != null) && !hrts[result_index - 1].equals( "" ) ) {
								try {
									// Versuche HWT aus Ediabas Result zu gewinnen
									hwtDummy = ediabas.getDiagResultValue( iaResultSet[result_index - 1], hrts[result_index - 1] );

								} catch( Exception ex ) {
									// falls Result nicht verf�gbar oder Probleme mit Ediabas dann nehme normales HWT oder HWTs
									if( hwts[result_index - 1] != null ) {
										hwtDummy = hwts[result_index - 1];
									} else {
										hwtDummy = hwt;
									}
								}
							}
							// Sonst parametrierter HWT oder HWTs
							else {
								if( hwts[result_index - 1] != null ) {
									hwtDummy = hwts[result_index - 1];
								} else {
									hwtDummy = hwt;
								}

							}
							result = new Ergebnis( results[result_index - 1], "EDIABAS", sgbd, job, jobpar, results[result_index - 1], temp, temp1, "", "0", "", "", awt, PB.getString( "toleranzFehler1" ), hwtDummy, Ergebnis.FT_NIO );
							status = STATUS_EXECUTION_ERROR;
						} else {
							//Invertierte Toleranz mit ok == true, somit NIO
							String hwtDummy;

							// wenn Hinweistext aus Result parametriert
							if( (hrts[result_index - 1] != null) && !hrts[result_index - 1].equals( "" ) ) {
								try {
									// Versuche HWT aus Ediabas Result zu gewinnen
									hwtDummy = ediabas.getDiagResultValue( iaResultSet[result_index - 1], hrts[result_index - 1] );

								} catch( Exception ex ) {
									// falls Result nicht verf�gbar oder Probleme mit Ediabas dann nehme normales HWT oder HWTs
									if( hwts[result_index - 1] != null ) {
										hwtDummy = hwts[result_index - 1];
									} else {
										hwtDummy = hwt;
									}
								}
							}
							// Sonst parametrierter HWT oder HWTs
							else {
								if( hwts[result_index - 1] != null ) {
									hwtDummy = hwts[result_index - 1];
								} else {
									hwtDummy = hwt;
								}

							}
							result = new Ergebnis( results[result_index - 1], "EDIABAS", sgbd, job, jobpar, results[result_index - 1], temp, "!(" + temp1 + ")", "", "0", "", "", awt, PB.getString( "toleranzFehler2" ), hwtDummy, Ergebnis.FT_NIO );
							status = STATUS_EXECUTION_ERROR;
						}
					} else {
						//Zahlenanalyse (Interval Min-Max)
						d = parseDouble( temp );
						ok = false;
						for( j = 0; j < mins.length; j++ ) {
							if( (parseDouble( mins[j] ) <= d) && (parseDouble( maxs[j] ) >= d) )
								ok = true;
						}
						// Simulation --> ok = true
						if( bSimulationWithoutEdiabas ) {
							ok = true;
						}
						if( (ok == true) && (invert == false) ) {
							//Toleranz, somit IO
							result = new Ergebnis( results[result_index - 1], "EDIABAS", sgbd, job, jobpar, results[result_index - 1], temp, temp1, temp2, "0", "", "", awt, "", "", Ergebnis.FT_IO );
						} else if( (ok == false) && (invert == true) ) {
							//Invertierte Toleranz, somit IO
							result = new Ergebnis( results[result_index - 1], "EDIABAS", sgbd, job, jobpar, results[result_index - 1], temp, "!(" + temp1 + ")", "!(" + temp2 + ")", "0", "", "", awt, "", "", Ergebnis.FT_IO );
						} else if( (ok == false) && (invert == false) ) {
							//Toleranz, somit NIO

							String hwtDummy;
							// wenn Hinweistext aus Result parametriert
							if( (hrts[result_index - 1] != null) && !hrts[result_index - 1].equals( "" ) ) {
								try {
									// Versuche HWT aus Ediabas Result zu gewinnen
									hwtDummy = ediabas.getDiagResultValue( iaResultSet[result_index - 1], hrts[result_index - 1] );

								} catch( Exception ex ) {
									// falls Result nicht verf�gbar oder Probleme mit Ediabas dann nehme normales HWT oder HWTs
									if( hwts[result_index - 1] != null ) {
										hwtDummy = hwts[result_index - 1];
									} else {
										hwtDummy = hwt;
									}
								}
								// Sonst parametrierter HWT oder HWTs	
							} else {
								if( hwts[result_index - 1] != null ) {
									hwtDummy = hwts[result_index - 1];
								} else {
									hwtDummy = hwt;
								}
							}
							result = new Ergebnis( results[result_index - 1], "EDIABAS", sgbd, job, jobpar, results[result_index - 1], temp, temp1, temp2, "0", "", "", awt, PB.getString( "toleranzFehler3" ), hwtDummy, Ergebnis.FT_NIO );
							status = STATUS_EXECUTION_ERROR;
						} else {
							//Invertierte Toleranz mit ok == true, somit NIO

							String hwtDummy;
							// wenn Hinweistext aus Result parametriert
							if( (hrts[result_index - 1] != null) && !hrts[result_index - 1].equals( "" ) ) {
								try {
									// Versuche HWT aus Ediabas Result zu gewinnen
									hwtDummy = ediabas.getDiagResultValue( iaResultSet[result_index - 1], hrts[result_index - 1] );

								} catch( Exception ex ) {
									// falls Result nicht verf�gbar oder Probleme mit Ediabas dann nehme normales HWT oder HWTs
									if( hwts[result_index - 1] != null ) {
										hwtDummy = hwts[result_index - 1];
									} else {
										hwtDummy = hwt;
									}
								}
							}
							// Sonst parametrierter HWT oder HWTs
							else {
								if( hwts[result_index - 1] != null ) {
									hwtDummy = hwts[result_index - 1];
								} else {
									hwtDummy = hwt;
								}

							}
							result = new Ergebnis( results[result_index - 1], "EDIABAS", sgbd, job, jobpar, results[result_index - 1], temp, "!(" + temp1 + ")", "!(" + temp2 + ")", "0", "", "", awt, PB.getString( "toleranzFehler4" ), hwtDummy, Ergebnis.FT_NIO );
							status = STATUS_EXECUTION_ERROR;
						}
					}

				} else { // no MIN
					result = new Ergebnis( results[result_index - 1], "EDIABAS", sgbd, job, jobpar, results[result_index - 1], temp, "", "", "0", "", "", awt, "", "", Ergebnis.FT_IO );
				}

				if( debug_Standard ) {
					System.out.println( "Creating Result" );
					System.out.println( "job: " + job + " par: " + jobpar );
					System.out.println( "Result: " + results[result_index - 1] );
					System.out.println( "Result value: " + temp );
				}

				ergListe.add( result );

				// setze Variablen im Grafikkontext (1'ter-Istwert, 2'ter-Minwert, 3'ter-Maxwert)
				try {
					this.setResult( (result_index - 1) * 3, new Integer( temp.substring( 0, temp.indexOf( "." ) < 0 ? temp.length() : temp.indexOf( "." ) ) ) );
					this.setResult( ((result_index - 1) * 3 + 1), new Integer( temp1.substring( 0, temp1.indexOf( "." ) < 0 ? temp1.length() : temp1.indexOf( "." ) ) ) );
					this.setResult( ((result_index - 1) * 3 + 2), new Integer( temp2.substring( 0, temp2.indexOf( "." ) < 0 ? temp2.length() : temp2.indexOf( "." ) ) ) );
				} catch( Exception e ) {
					//Tue nichts! Grafikanzeige wird halt nicht aktualisiert. Logs extrem voll schreiben macht auch keinen Sinn.
				}
			}
		} else {
			//handling when invalid == true
			status = STATUS_EXECUTION_ERROR;
		}
	}

	/**
	 * F�hrt den Diagnosejob aus.
	 * 
	 * @return invalid - speichert ob die Diagnose valide durchgef�hrt wurde
	 * @throws PPExecutionException
	 */
	private boolean executeDiagnosticJob() throws PPExecutionException {
		Ergebnis result;
		String temp;
		boolean invalid = false; //true if no results could be retrieved due to errors, e.g. "Wrong UBATT"
		ergListe = new Vector<Ergebnis>(); //Neu f�r jeden Durchlauf

		// Ausf�hrung
		boolean tryBlockExecuted = false;
		//System.out.println("tryBlockExecuted " + tryBlockExecuted);
		try {
			if( !bSimulationWithoutEdiabas ) {
				if( bGetAllResults ) {
					jobres = "";
				}
				if( debug_Standard ) {
					System.out.println( "Execute Ediabas Job" );
				}
				temp = ediabas.executeDiagJob( sgbd, job, jobpar, jobres );
				if( debug_Standard ) {
					System.out.println( "Execute Ediabas Job done" );
				}
			} else {
				temp = "OKAY";
			}
			invalid = false;
			if( temp.equals( "OKAY" ) == false ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), temp, Ergebnis.FT_NIO );
				ergListe.add( result );
				invalid = true;
				if( !ignore_errors ) {
					throw new PPExecutionException();
				}
			} else if( resAnzahl == 0 ) {
				result = new Ergebnis( "Status", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );
			}
			tryBlockExecuted = true;

		} catch( ApiCallFailedException e ) {
			result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
			ergListe.add( result );
			invalid = true;
			if( !ignore_errors ) {
				throw new PPExecutionException();
			}
		} catch( EdiabasResultNotFoundException e ) {
			if( e.getMessage() != null ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
			} else {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
			}
			ergListe.add( result );
			throw new PPExecutionException();
		} catch( Exception e ) {
			result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
			ergListe.add( result );

			invalid = true;
			if( !ignore_errors ) {
				throw new PPExecutionException();
			}
		} finally {
			if( !tryBlockExecuted ) {
				if( hwt != null && (!hwt.equalsIgnoreCase( "" )) ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), hwt, Ergebnis.FT_NIO );
					ergListe.add( result );
				}
			}
		}
		return invalid;
	}

	private class UDNListener implements UDNModelListener {

		AtomicBoolean udnCanceled;

		public UDNListener( AtomicBoolean udnCanceled ) {
			this.udnCanceled = udnCanceled;
		}

		@Override
		public void sectionInserted( UDNModelEvent event ) {
		}

		@Override
		public void sectionRemoved( UDNModelEvent event ) {
		}

		@Override
		public void propertyChanged( UDNModelEvent event ) {
		}

		@Override
		public void close( UDNModelEvent event ) {
			// set flag if UDN is cancelled
			this.udnCanceled.set( true );
		}

		@Override
		public void show( UDNModelEvent event ) {
		}

		@Override
		public void allSectionsRemoved( UDNModelEvent event ) {
		}

		@Override
		public void heightChanged( UDNModelEvent event ) {
		}

		@Override
		public void widthChanged( UDNModelEvent event ) {
		}
	}

	/**
	 * Diese Methode dient zum Ausf�hren des SGBDSIV Diagnosejobs.
	 */
	private void executeSGBDSIV( ExecutionInfo info ) throws PPExecutionException, VariablesException {

		Ergebnis result;
		Pruefprozedur[] pps = this.getPr�fling().getPr�fprozeduren();
		for( int p = 0; p < pps.length; p++ ) {
			if( pps[p].getName().equalsIgnoreCase( "SGBD_SIV" ) ) {
				pps[p].setPr�flingLaufzeitUmgebung( this.getPr�flingLaufzeitUmgebung() );

				pps[p].execute( info ); // SGBD_SIV ausf�hren

				if( pps[p].getExecStatus() == STATUS_EXECUTION_OK ) {
					result = new Ergebnis( "Status", "Pruefprozedur", "", "", "", pps[p].getPr�fling().getName() + "." + pps[p].getName(), "" + status, "" + STATUS_EXECUTION_OK, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				} else {
					result = new Ergebnis( "Status", "Pruefprozedur", "", "", "", pps[p].getPr�fling().getName() + "." + pps[p].getName(), "" + status, "" + pps[p].getExecStatus(), "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					break;
				}
				sgbd = extractValues( getArg( getRequiredArgs()[0] ) )[0];
				break;
			}
		}

		// PP "SGBD_SIV" im PL nicht gefunden, ggf. Falschparametrierung od. falsche Vererbung
		if( sgbd == null ) {
			throw new PPExecutionException( "No result VARIANTE@SGBD_SIV available!" );
		}
	}

	/**
	 * Diese Methode simuliert die Execution.
	 * 
	 */
	private void executionWithSimulationResult() throws PPExecutionException {
		Ergebnis result;
		if( debug_Standard ) {
			System.out.println( "Simulation mit vorgegebenem Simulationsergebnis:" );
		}

		//Parameter holen
		String sSgbd = extractValues( getArg( "SGBD" ) )[0];
		String sJob = getArg( "JOB" );
		String sJobpar = "";
		String[] saJobpar = { "" };
		if( getArg( "JOBPAR" ) != null ) {
			sJobpar = getArg( "JOBPAR" );
			if( sJobpar.indexOf( '@' ) != -1 ) {
				saJobpar = extractValues( sJobpar );
				sJobpar = "";
				for( int k = 0; k < saJobpar.length - 1; k++ ) {
					sJobpar = sJobpar + saJobpar[k] + ";";
				}
				sJobpar = sJobpar + saJobpar[saJobpar.length - 1];
			}
		}

		String sResult = "IO";
		String sErrorType = Ergebnis.FT_IO;
		status = STATUS_EXECUTION_OK;
		if( getArg( "SIM_RES" ) != null ) {
			if( getArg( "SIM_RES" ).equalsIgnoreCase( "NIO" ) ) {
				status = STATUS_EXECUTION_ERROR;
				sResult = "NIO";
				sErrorType = Ergebnis.FT_NIO;
			}
		}

		result = new Ergebnis( this.getName(), "DIAG SIMULATION", sSgbd, sJob, sJobpar, sResult, "Simulation result", "Every MIN", "Every MAX", "0", "", "", "", "", "", sErrorType );
		ergListe.add( result );
		if( debug_Standard ) {
			System.out.println( "Simulation mit vorgegebenem Simulationsergebnis beendet!" );
		}

	}

	/**
	 * Konvertiert den IMAGE_LOCATION-Parameter der Pr�fprozedur in die 
	 * korrespondierende UDN-ImageLocation Angabe. Bei �bergabe von 
	 * <code>null</code> wird ImageLocation.IMAGE_LEFT als Default zur�ck 
	 * geliefert.
	 *  
	 * @param code IMAGE_LOCATION-Parameter der Pr�fprozedur.  
	 * @return Korrespondierendes UDN-ImageLocation.
	 */
	private ImageLocation toImageLocation( String locationCode ) {
		ImageLocation location;

		if( "R".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_RIGHT;
		else if( "L".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_LEFT;
		else if( "T".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_TOP;
		else if( "B".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_BOTTOM;
		else
			location = ImageLocation.IMAGE_LEFT;

		return location;
	}

	/**
	 * Pr�ft mit Hilfe eines Http Requests auf den 2. Pr�fstandscreen, ob der Pr�fstand im Multiinstanzmodus l�uft. 
	 * 
	 * @return <code>True</code>, wenn Pr�fstand im Multiinstanzmodus l�uft,
	 *         <code>false</code> wenn nicht.
	 * @author  Fabian Schoenert, BMW AG TI-545
	 */
	private boolean isMultiInstance() {
		String url = "http://localhost:8086/testscreen-2";
		try {
			HttpURLConnection con = (HttpURLConnection) new URL( url ).openConnection();
			con.setConnectTimeout( 500 ); // in ms
			con.setRequestMethod( "GET" ); // ist eigentlich Default
			con.getInputStream(); // throws IOException wenn Adresse nicht erreichbar
			con.disconnect();
		} catch( final IOException e ) {
			// Nicht im MI Modus
			return false;
		} catch( Exception e ) {
			if( debug_Standard )
				System.out.println( "HTTP Request zur Abfrage des MI Modus schlug fehl. " );
			// Wenn Fehler bei Http Abfrage kein MI Modus
			return false;
		}
		return true;
	}

	/** 
	 * Zeigt den AWT via UserDialog.
	 * 
	 * @param awt Der anzuzeigende AWT.
	 * @param dialog Der UserDialog.
	 */
	private void showAwtDialog( String awt, UserDialog dialog ) {
		dialog.displayMessage( this.strFensterName, awt, -1 );
	}

	/** 
	 * Zeigt den AWT via UserDialogNEXT (UDN).
	 * 
	 * @param awt Der anzuzeigende AWT.
	 * @param cancel Bei <code>true</code> besteht die M�glichkeit des Abbruchs.
	 * @param dialog Der UDN.
	 * @param handle Handle des UDN.
	 * @param imageLocation Angabe der Bildposition. 
	 * @param image Das gegebenenfalls anzuzeigende Bild. Kann auch <code>
	 * 		  null</code> sein, dann wird kein Bild angezeigt.
	 * @throws DeviceNotAvailableException Falls der UDN nicht benutzt werden 
	 * 		   kann.
	 */
	private void showAwtDialog( String awt, boolean cancel, UserDialogNextRuntimeEnvironment dialog, UDNHandle handle, String imageLocation, String image ) throws DeviceNotAvailableException {
		if( handle != null ) {
			UDNButtonConfiguration buttonConfig;
			if( cancel )
				buttonConfig = UDNButtonConfiguration.CANCEL_BUTTON;
			else
				buttonConfig = UDNButtonConfiguration.EMPTY_BUTTONS;

			if( image != null )
				dialog.displayMessage( handle, UDNState.MESSAGE, image, toImageLocation( imageLocation ), PB.getString( "anweisung" ), awt, -1, buttonConfig );
			else
				dialog.displayMessage( handle, UDNState.MESSAGE, PB.getString( "anweisung" ), awt, -1, buttonConfig );
		} else
			throw new DeviceNotAvailableException();

	}

	/** 
	 * Aktualisiert den AWT via UserDialog.
	 * 
	 * @param awt Der anzuzeigende AWT.
	 * @param dialog Der UserDialog.
	 */
	private void updateAwtDialog( String awt, UserDialog dialog ) {
		dialog.displayMessage( this.strFensterName, awt, -1 );
	}

	/**
	 * Aktualisiert den Text eines UDN-Dialoges, welcher mit 
	 * displayMessage(...) erzeugt wurde. Anders als beim alten
	 * UserDialog f�hrt ein wiederholtes displayMessage im UDN zu einem
	 * unerw�nschten Flackern, so dass wir direkt den Text der 
	 * Text-Sektion anpassen m�ssen. Der folgende Code st�tzt sich
	 * auf die Analyse der Methode displayMessagePrivate aus Klasse
	 * UserDialogNextRuntimeEnvironmentImpl. Dies ist ein unsch�nes
	 * Vorgehen, l�sst sich aber nicht vermeiden.
	 *    
	 * @param sections Liste der Sektionen, welche nach der Text-Sektion 
	 * 		  (konkret UDNMultiLineTextSection) durchsucht wird.
	 * @param str Der in der Text-Sektion zu setzende Text.
	 */
	class UpdateUtil {
		public void updateUDNMessageDialog( List<UDNSection> sections, final String str ) {
			for( UDNSection section : sections ) {
				if( section instanceof UDNMultiLineTextSection ) { //bei Messages mit Bild sind Text und Bild in einer CompoundSection
					final UDNMultiLineTextSection multiLineTextSection = (UDNMultiLineTextSection) section;
					// Die folgenden Methoden m�nden jeweils in einem Repaint.
					// Um zu vermeiden, dass es Flackern etc. gibt, wird der
					// Code in den EventDispatchThread verlegt, wo die 
					// generierten Repaints hintenanstehen.
					SwingUtilities.invokeLater( new Runnable() {
						@Override
						public void run() {
							multiLineTextSection.setText( str );
							multiLineTextSection.setFont( UDNUtils.LARGE_FONT ); //erforderlich, da com.bmw.cascade.pruefstand.framework.udnext.UDNSectionImpl.setFont(Font) das Setzen des gleichen Wertes (unten) ignoriert
							multiLineTextSection.setFont( UDNUtils.DEFAULT_FONT ); //bei jedem neuen Text erforderlich, da Font von com.bmw.cascade.pruefstand.framework.udnext.UDNMultiLineTextSectionView.setFont als Attribut dem gesetzten Text zugeordnet ist (bei neuem Text muss diese Zuordnung daher wiederholt werden)
						}
					} );
					break;
				} else if( section instanceof UDNCompoundSection ) {
					List<UDNSection> leftRightSections = new ArrayList<UDNSection>();
					leftRightSections.add( ((UDNCompoundSection) section).getLeftSection() );
					leftRightSections.add( ((UDNCompoundSection) section).getRightSection() );
					updateUDNMessageDialog( leftRightSections, str );
					break;
				}
			}
		}
	}

	/** 
	 * Aktualisiert den AWT via UserDialogNEXT (UDN).
	 * 
	 * @param awt Der anzuzeigende AWT.
	 * @param cancel Bei <code>true</code> besteht die M�glichkeit des Abbruchs.
	 * @param dialog Der UDN.
	 * @param handle Handle des UDN.
	 * @throws DeviceNotAvailableException Falls der UDN nicht benutzt werden 
	 * 		   kann.
	 */
	private void updateAwtDialog( String awt, boolean cancel, UserDialogNextRuntimeEnvironment dialog, UDNHandle handle ) throws DeviceNotAvailableException {
		UpdateUtil updateUtil = new UpdateUtil();

		if( handle != null ) {
			List<UDNSection> sections = dialog.getModel( handle ).getSections();
			updateUtil.updateUDNMessageDialog( sections, awt );
		} else
			throw new DeviceNotAvailableException();
	}

	/** 
	 * Pr�ft, ob der �bergebene Wert eine Zahl im hexadezimalen Format ist.
	 * Dies ist immer dann der Fall, wenn die Buchstaben a-f/A-F im Wert 
	 * vorkommen. Achtung: Nat�rlich kann eine Zahl nur aus Ziffern bestehen
	 * und trotzdem hex sein. In dem Fall geht die Methode von einer Zahl im 
	 * dezimalen Format aus, da eine Unterscheidung zwischen hex und dez nicht
	 * m�glich ist (Unsch�rfe).  
	 *  
	 * @param value Der zu pr�fende Wert.
	 * @return <code>True</code>, wenn die Zahl hexadezimal ist, andernfalls
	 * 		   <code>false</code>. Letztes ist auch der Fall, wenn es sich 
	 * 		   um reinen Text handelt.
	 */
	private boolean isHex( String value ) {
		try {
			Double.parseDouble( value ); //pr�fen, ob wir als Double parsen k�nnen
			return false;
		} catch( NumberFormatException e1 ) { //wenn nicht, dann pr�fen, ob wir als hex parsen k�nnen
			try {
				Long.parseLong( value.replaceAll( " ", "" ), 16 ); //eventuelle Leerzeichen, die als Trennzeichen fungieren (z. B. "A2 FF 7E") entfernen
				return true;
			} catch( NumberFormatException e2 ) {
				return false;
			}
		}
	}

	/** 
	 * Parst den �bergebenen Wert und wandelt ihn in ein Double um. Hierbei
	 * wird automatisch auf verschiedene Zahlenformate R�cksicht genommen.
	 *  
	 * @param value Der zu parsende Wert.
	 * @return Der ermittelte Double-Wert.
	 * @throws NumberFormatException Wenn kein Parsen m�glich ist, da entweder
	 * 		   reiner Text vorliegt oder das Zahlenformat unbekannt ist.  
	 */
	private double parseDouble( String value ) throws NumberFormatException {
		if( isHex( value ) )
			return (double) Long.parseLong( value.replaceAll( " ", "" ), 16 ); //eventuelle Leerzeichen, die als Trennzeichen fungieren (z. B. "A2 FF 7E") entfernen
		else
			return Double.parseDouble( value );
	}

}

package com.bmw.cascade.pruefprozeduren;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;

/**
 * Implementierung einer Wartezeit zur Sitzinitialisierung (parallel f�r alle Sitzmodule)
 * Die Sgbden werden den Pr�flingen entnommen, sofern diese im Auftrag vorhanden sind. Diagnose-
 * fehler werden nur einmal, nicht f�r jeden Fall ins Log eingetragen.
 * Positive Ergebnisse werden auch nur einmal ins Log eingetragen.
 * 
 * @author Burger, Andreas
 * 
 * @version 4_0_T  AB Try-Catch Bl�cke f�r SGBDen vereinzelt, Ergenbisse erg�nzt. <br>
 * @version 5_0_F  AB Freigabe nach 4_0_T 
 */
public class SDiagSitz_5_0_F_Pruefprozedur extends AbstractBMWPruefprozedur 
{

	static final long serialVersionUID = 1L;

	// language
	private final boolean isEnglish = System.getProperty( "user.language" ).equalsIgnoreCase( "EN" );

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public SDiagSitz_5_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public SDiagSitz_5_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName,
			Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialisiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() 
	{
		String[] args = { };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() 
	{
		String[] args = { 	"REF_PL_NAME_FAS", 
							"REF_PL_NAME_BFS", 
							"REF_PL_NAME_FAH", 
							"REF_PL_NAME_BFH",
							"JOB",
							"JOB_PAR", 
							"JOB_RESULT",
							"EXIT_VALUE",
							"TIMEOUT",
							"PAUSE"	};
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	public boolean checkArgs() 
	{
		return super.checkArgs();
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) 
	{
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		EdiabasProxyThread c_Ediabas = null;
		boolean b_EdiabasParallel = false;

		//spezifische Variablen
		String s_Job = "";
		String s_Par = "";
		String s_Result = "";
		String s_ExitValue = "";
		int i_Pause;
		int i_Timeout;		
		String s_Temp;
		String s_Sgbd;
		String s_JobResult;		
		LinkedList c_OpenEcus = new LinkedList();

		if( checkArgs() == false ) 
		{
			if (this.isEnglish)
				result = new Ergebnis( this.name, "Wrong parameters", "", "", "", "", "", "", "", "0", "", "", "", "Wrong parameters", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( this.name, "Parametrierfehler", "", "", "", "", "", "", "", "0", "", "", "", "Parametrierfehler", "", Ergebnis.FT_NIO_SYS );

			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;			
			setPPStatus( info, status, ergListe );
			return;
		}
			
		try 
		{
			s_Job = getArg ("JOB");
			s_Par = "ARG;" + getArg ("JOB_PAR");
			s_Result = getArg ("JOB_RESULT");
			s_ExitValue = getArg ("EXIT_VALUE");
			i_Pause = Integer.parseInt(getArg("PAUSE"));
			i_Timeout = Integer.parseInt(getArg("TIMEOUT"));
		} 
		catch (Exception e) 
		{
			if(e.getMessage() != null)
			{
				if (this.isEnglish)
					result = new Ergebnis( this.name, "Wrong parameters", "", "", "", "", "", "", "", "0", "", "", "", "Wrong parameters", e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( this.name, "Parametrierfehler", "", "", "", "", "", "", "", "0", "", "", "", "Parametrierfehler", e.getMessage(), Ergebnis.FT_NIO_SYS );
			}
			else
			{
				if (this.isEnglish)
					result = new Ergebnis( this.name, "Wrong parameters", "", "", "", "", "", "", "", "0", "", "", "", "Wrong parameters", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( this.name, "Parametrierfehler", "", "", "", "", "", "", "", "0", "", "", "", "Parametrierfehler", "", Ergebnis.FT_NIO_SYS );
			}

			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			setPPStatus( info, status, ergListe );
			return;
		}
		
		try 
		{
			// welche sgbdn m�ssen gepr�ft werden?
			if (getPr�fling().getAuftrag().getPr�fling(getArg("REF_PL_NAME_FAS")) != null)
			{
				s_Temp = getPr�fling().getAuftrag().getPr�fling(getArg("REF_PL_NAME_FAS")).getAttribut("SGBD");
				if ((s_Temp != null) && (s_Temp.trim().length() > 0)) 
					c_OpenEcus.add(s_Temp);
			}			
		} 
		catch (Exception e) 
		{
		}		

		try 
		{
			if (getPr�fling().getAuftrag().getPr�fling(getArg("REF_PL_NAME_BFS")) != null)
			{
				s_Temp = getPr�fling().getAuftrag().getPr�fling(getArg("REF_PL_NAME_BFS")).getAttribut("SGBD");
				if ((s_Temp != null) && (s_Temp.trim().length() > 0)) 
					c_OpenEcus.add(s_Temp);
			}
		} 
		catch (Exception e) 
		{
		}

		try 
		{	
			if (getPr�fling().getAuftrag().getPr�fling(getArg("REF_PL_NAME_FAH")) != null)
			{
				s_Temp = getPr�fling().getAuftrag().getPr�fling(getArg("REF_PL_NAME_FAH")).getAttribut("SGBD");
				if ((s_Temp != null) && (s_Temp.trim().length() > 0)) 
					c_OpenEcus.add(s_Temp);
			}
		} 
		catch (Exception e) 
		{
		}
		
		try 
		{
			if (getPr�fling().getAuftrag().getPr�fling(getArg("REF_PL_NAME_BFH")) != null)
			{
				s_Temp = getPr�fling().getAuftrag().getPr�fling(getArg("REF_PL_NAME_BFH")).getAttribut("SGBD");
				if ((s_Temp != null) && (s_Temp.trim().length() > 0)) 
					c_OpenEcus.add(s_Temp);	
			}
		} 
		catch (Exception e) 
		{
		}		
		
		// init ist schon gestartet
		// alle sitze alle 3 sec. pollen bis timeout oder alle i.o.
		// bei diagnosefehler wird der betroffene sitz nicht mehr gepr�ft..
			
			
		long l_StopTime = System.currentTimeMillis() + i_Timeout;
		// schleife bis timeout und alle sgbdn abgeschlossen sind
		while ((c_OpenEcus.size() > 0) && (l_StopTime > System.currentTimeMillis()))
		{			
			Iterator c_It = c_OpenEcus.iterator();
				
			while (c_It.hasNext())
			{
				s_Sgbd = (String) c_It.next();
				boolean b_Remove = false;
				
				//ediabas job
				try
				{
					if (b_EdiabasParallel)
						c_Ediabas = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabasParallel("", s_Sgbd);
					else
						c_Ediabas = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();
						
					s_JobResult = c_Ediabas.executeDiagJob( s_Sgbd, s_Job, s_Par, "");
					
					if( !s_JobResult.equalsIgnoreCase("OKAY")) 
					{   
						//diagnosefehler!
						if (this.isEnglish)
							result = new Ergebnis( this.name, "Diagnose", s_Sgbd, "", "", "", "", "", "", "0", "", "", "", "Communication failure", "ECU is not responding", Ergebnis.FT_NIO_SYS );
						else
							result = new Ergebnis( this.name, "Diagnose", s_Sgbd, "", "", "", "", "", "", "0", "", "", "", "Kommunikationsfehler", "Steuerger�t antwortet nicht", Ergebnis.FT_NIO_SYS );
						
						ergListe.add(result);
						status = STATUS_EXECUTION_ERROR;
						b_Remove = true; //--> sg antwortet nicht, entfernen!							
					} 
					else 
					{
						String s_Erg = c_Ediabas.getDiagResultValue(s_Result);
							
						if (s_Erg.equalsIgnoreCase(s_ExitValue))
						{							
							if (this.isEnglish)
								result = new Ergebnis( this.name, "Diagnose", s_Sgbd, "", "", "", "", "", "", "0", "", "", "", "Wait sequence IO", "", Ergebnis.FT_IO );
							else
								result = new Ergebnis( this.name, "Diagnose", s_Sgbd, "", "", "", "", "", "", "0", "", "", "", "Wartezeit IO", "", Ergebnis.FT_IO );

							ergListe.add(result);
							b_Remove = true; //--> sg hat bedingung erf�llt, entfernen!
						}
					}
						
					if (b_EdiabasParallel)
						this.getPr�flingLaufzeitUmgebung().getDeviceManager().releaseEdiabasParallel(c_Ediabas, "", s_Sgbd);
													
				} 
				catch (ApiCallFailedException e) 
				{
					result = new Ergebnis( this.name, "Diagnose", s_Sgbd, s_Job, s_Par, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), c_Ediabas.apiErrorCode()+": " + c_Ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );

					ergListe.add(result);
					status = STATUS_EXECUTION_ERROR;					
					b_Remove = true;
				} 
				catch (EdiabasResultNotFoundException e) 
				{
					if (e.getMessage() != null)
						result = new Ergebnis( this.name, "Diagnose", s_Sgbd, s_Job, s_Par, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( this.name, "Diagnose", s_Sgbd, s_Job, s_Par, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
					
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;					
					b_Remove = true;
				}	
				catch (Throwable ex)
				{
					result = new Ergebnis( this.name, "Diagnose", s_Sgbd, s_Job, s_Par, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), c_Ediabas.apiErrorCode()+": " + c_Ediabas.apiErrorText(), Ergebnis.FT_NIO );
					
					ergListe.add(result);
					status = STATUS_EXECUTION_ERROR;					
					b_Remove = true;
				}					

					
				//ok, dann sgbd entfernen.
				if (b_Remove) 
				{
					c_It.remove();
					c_OpenEcus.remove(s_Sgbd); //todo: evtl. ist die linkedlist durch die iteration gesperrt!?!
				}
			}
				
			//warten
			try
			{
				Thread.sleep(i_Pause);
			}
			catch (Exception ex)
			{
				;
			}
								
		}
		
		Iterator c_It = c_OpenEcus.iterator();
		while (c_It.hasNext())
		{			
			s_Sgbd = (String) c_It.next();

			if (isEnglish)
				result = new Ergebnis( this.name, "Diagnose", s_Sgbd, "", "", "", "", "", "", "0", "", "", "", "Time too long", "Move seat to home position manually", Ergebnis.FT_NIO );
			else
				result = new Ergebnis( this.name, "Diagnose", s_Sgbd, "", "", "", "", "", "", "0", "", "", "", "Wartezeit zu lange", "Fahre Sitz manuell in Grundstellung", Ergebnis.FT_NIO );
			
			ergListe.add(result);
			status = STATUS_EXECUTION_ERROR;	
			
			c_It.remove();
		}
			
		setPPStatus( info, status, ergListe );
	} // End of execute()

} // End of Class


/*
 * DiagCompare
 *
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.ediabas.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementierung der Pr�fprozedur, die per Diagnose Vergleiche zweier Results auf erf�llt bzw.
 * nicht erf�llt bewertet.
 * Diese Bewertung kann pollend erfolgen, d.h. �ber das optionale Argument TIMEOUT kann gesteuert werden, wie
 * lange (Maximalzeit) die Bewertung erfolgt, bei i. O. wird der Polling-Vorgang beendet.
 * �ber das optionale Argument OK_TIME kann angegeben werden, wie lange der Status OK gehalten werden muss,
 * damit dieser Status g�ltig ist.
 * Optional ist die Ausgabe einer Werkeranweisung m�glich, die eine Abbruchtaste anbietet.
 * @author BMW TI-430 Schaller
 * @version 0_0_1 02.10.2002  RS  Ersterstellung nach Spezifikation
 * 0_0_2 11.10.2002  RS  Ausfuehrung bei gleicher SGBD und gleichem Job auf einmalig optimiert
 * 0_0_3 16.12.2004 JM Optimierung vom 11.10. wieder entfernt, da problematisch bei STARUS_MESSWERTBLOCK_LESEN
 * 0_0_4 12.01.2007 AS zus�tzliche Ausgabe der JobResults
 *
 */
public class DiagCompare_4_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    	static final long serialVersionUID = 1L;
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public DiagCompare_4_0_F_Pruefprozedur() {}

  /**
     * erzeugt eine neue Pruefprozedur, die auf Einhaltung bzw. auf Ausschlu� der zu analysierenden Werte pr�ft.
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public DiagCompare_4_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    
    /**
     * liefert die optionalen Argumente
     */
    public String[] getOptionalArgs() {
        String[] args = {"JOBPAR1", "JOBPAR2", "LOW_PERCENT","HIGH_PERCENT", "TIMEOUT", "OK_TIME", "PAUSE", "AWT", "HWT","INVERT"};
        return args;
    }
    
    
    /**
     * liefert die zwingend erforderlichen Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = {"SGBD1", "JOB1", "RESULT1", "SGBD2", "JOB2", "RESULT2"};
        return args;
    }
    
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz, Plausibilit�t und Wert.
     */
    public boolean checkArgs() {
        boolean exist;
        int i;
        float f;
        
        String[] requiredArgs = null;
        String[] optionalArgs = null;
        String givenkey, high, low, temp;
        
        Enumeration enu;
        
        try{
            // 1. Check:
            // Sind alle sechs requiredArgs gesetzt
            requiredArgs = getRequiredArgs();
            for( i=0; i<requiredArgs.length; i++ ) {
                if( getArg(requiredArgs[i]) == null ) return false;
                // folgende Sachen werden beim 2. Check sowieso ausgef�hrt:
                //if( getArg(requiredArgs[i]).equals("") == true ) return false;
                //if( getArg(requiredArgs[i]).indexOf(';') != -1 ) return false;
                //if( getArg(requiredArgs[i]).indexOf('@') != -1 ) return false;
            }
            // 2. Check:
            // Sind �berfl�ssige Argumente gesetzt, d.h. Argumente, die weder required noch optional sind.
            // Die detaillierte Analyse erfolgt im 3-ten Check.
            enu = getArgs().keys();
            optionalArgs = getOptionalArgs();
            while( enu.hasMoreElements()) { // == TRUE, solange enu noch ein weiteres Element hat
                givenkey = (String) enu.nextElement();
                exist = false;
                i = 0;
                while( (exist == false) && (i < requiredArgs.length) ) {
                    // Ist givenkey ein requiredArgs, z. B. "SGBD1"?
                    if( givenkey.equals(requiredArgs[i]) == true ) exist = true;
                    i++;
                }
                i = 0;
                while( (exist == false) && (i < optionalArgs.length) ) {
                    if( givenkey.equals(optionalArgs[i]) == true ) exist = true;
                    i++;
                }
                if (exist == false) return false;
                // Ist der Inhalt eines givenkeys leer, ";" oder "@"?
                if( getArg(givenkey).equals("") == true ) return false;
                if( ( givenkey.equals(optionalArgs[0]) == false ) && ( givenkey.equals(optionalArgs[1]) == false ) ) {
                    if( getArg(givenkey).indexOf(';') != -1 ) return false;
                }
                if( ( givenkey.equals(requiredArgs[0]) == false ) && ( givenkey.equals(requiredArgs[3]) == false ) ) {
                    if( getArg(givenkey).indexOf('@') != -1 ) return false;
                }
            }
            // 3. Check:
            // Die Plausibilit�t des durchzuf�hrenden Vergleichs wird gepr�ft, d. h. sind die
            // zu vergleichenden Jobs unterschiedlich zueinander.
            temp = getArg("SGBD1");
            if( temp.equals(getArg("SGBD2")) == true ) {
                // Pruefung bei SGBD1 == SGBD2 und JOB1 == JOB2, dass RESULT1 != RESULT2
                temp = getArg("JOB1");
                if( temp.equals(getArg("JOB2")) == true ) {
                    temp = getArg("RESULT1");
                    if( temp.equals(getArg("RESULT2")) == true )return false;
                }
            }
            // 4. Check:
            // Die Konvertierung zu Zahlen von LOW_- und HIGH_PERCENT, falls vorhanden, wird gepr�ft.
            if( getArg(optionalArgs[2]) != null ) {
                low = getArg("LOW_PERCENT");
                if( low == null ) return false;
                try {
                    f = Float.parseFloat( low );
                } catch (NumberFormatException e) {
                    return false;
                }
            }
            if( getArg(optionalArgs[3]) != null ) {
                high = getArg("HIGH_PERCENT");
                if( high == null ) return false;
                try {
                    f = Float.parseFloat( high );
                } catch (NumberFormatException e) {
                    return false;
                }
            }
            // 5. Check:
            // Falls TIMEOUT, OK_TIME und PAUSE gesetzt sind, m�ssen diese Integer sein.
            for( i=4; i<=6; i++ ) {
                if( getArg(optionalArgs[i]) != null ) {
                    try {
                        if( (Long.parseLong( getArg(optionalArgs[i]) )) < 0) return false;
                    } catch (NumberFormatException e) {
                        return false;
                    }
                }
            }
            // 6. Check:
            // AWT darf keine Sonderzeichen enthalten.
            if( getArg(optionalArgs[7]) != null ) {
                temp = getArg("AWT");
                if( temp == null ) return false;
                // beim 2. Check wurde schon erledigt:
                //if( temp.equals("") == true ) return false;
                //if( temp.indexOf(';') != -1 ) return false;
                //if( temp.indexOf('@') != -1 ) return false;
            }
            // 7. Check:
            // AWT darf keine Sonderzeichen enthalten.
            if( getArg(optionalArgs[8]) != null ) {
                temp = getArg("HWT");
                if( temp == null ) return false;
                // beim 2. Check wurde schon erledigt:
                //if( temp.equals("") == true ) return false;
                //if( temp.indexOf(';') != -1 ) return false;
                //if( temp.indexOf('@') != -1 ) return false;
            }
            // 8. Check:
            // INVERT muss TRUE oder FALSE sein.
            if( getArg(optionalArgs[9]) != null ) {
                temp = getArg("INVERT");
                if( temp == null ) return false;
                if( (temp.equalsIgnoreCase("TRUE") == false) && (temp.equalsIgnoreCase("FALSE") == false) ) return false;
            }
            // Tests bestanden, somit i. O.
            return true;
        } catch (Exception e) {
            return false;
        } catch (Throwable e) {
            return false;
        }
    }
    
    
    /**
     * f�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) {
        // immer notwendig 
        Ergebnis result;
        Ergebnis result1;
        Ergebnis result2;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        // spezifische Variablen
        String temp, temp1, temp2;
        String sgbd1, job1, jobpar1, jobres1, sgbd2, job2, jobpar2, jobres2;
        String awt, hwt;
        
        Float f = null;
        Float f1 = null;
        Float f2 = null;
        Float high = null;
        Float low = null;
        
        long endeZeit, okTime, pause, deltaT;
        long firstOkTime = Long.MAX_VALUE;
        
        //int resAnzahl;
        boolean invert=false;
        boolean ok;
        boolean udInUse = false;
        UserDialog myDialog = null;
        
        try {
            //Parameter holen
            try {
                if( checkArgs() == false ) throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
                
                sgbd1   = extractValues( getArg( getRequiredArgs()[0] ) )[0];
                job1    = getArg( getRequiredArgs()[1] );
                jobres1 = getArg( getRequiredArgs()[2] );
                jobpar1 = getArg( getOptionalArgs()[0] );
                if (jobpar1 == null ) jobpar1 = "";
                
                sgbd2   = extractValues( getArg( getRequiredArgs()[3] ) )[0];
                job2    = getArg( getRequiredArgs()[4] );
                jobres2 = getArg( getRequiredArgs()[5] );
                jobpar2 = getArg( getOptionalArgs()[1] );
                if (jobpar2 == null ) jobpar2 = "";
                
                //LOW_- und HIGH_PERCENT
                temp1 = getArg( getOptionalArgs()[2] );
                if (temp1 == null ) temp1 = "0";
                temp2 = getArg( getOptionalArgs()[3] );
                if (temp2 == null ) temp2 = "0";
                try {
                    low = new Float( Float.parseFloat(temp1) );
                    high = new Float( Float.parseFloat(temp2) );
                } catch (NumberFormatException e) {
                    throw new PPExecutionException( PB.getString( "parameterexistenz" )+"2" );
                }
                
                //Timeout
                temp = getArg( getOptionalArgs()[4] );
                if( temp == null ) endeZeit = System.currentTimeMillis();
                else               endeZeit = System.currentTimeMillis() + Long.parseLong(temp);
                
                //OK_Time
                temp = getArg( getOptionalArgs()[5] );
                if( temp == null ) okTime = -1;
                else               okTime = Long.parseLong(temp);
                
                //Pause
                temp = getArg( getOptionalArgs()[6] );
                if( temp == null ) pause = 0;
                else               pause = Long.parseLong(temp);
                
                //Anweisungstext
                awt = getArg( getOptionalArgs()[7] );
                if( awt == null ) awt="";
                else              awt = PB.getString( awt );
                
                //Hinweisungstext
                hwt = getArg( getOptionalArgs()[8] );
                if( hwt == null ) hwt="";
                else              hwt = PB.getString( hwt );
                
                //Invert
                temp = getArg( getOptionalArgs()[9] );
                if ( (temp != null) && temp.equalsIgnoreCase("TRUE") ) invert = true;
                if ( (temp != null) && temp.equalsIgnoreCase("FALSE") ) invert = false;
            } catch (PPExecutionException e) {
                if (e.getMessage() != null)
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            //Ausf�hrungs- und Analyseschleife
            do {
                ergListe = new Vector(); //Neu f�r jeden Durchlauf
                
                //Ausf�hrung der EDIABAS-Jobs
               /* if( (sgbd1.equals(sgbd2) == true) && (job1.equals(job2) == true) ) {
                    try {
                        temp1 = Ediabas.executeDiagJob(sgbd1,job1,jobpar1,jobres1+";"+jobres2);
                        if( temp1.equals("OKAY") == false) {
                            result = new Ergebnis( PB.getString( "dbDiagFehler" ), "EDIABAS", sgbd1, job1, jobpar1, "JOB_STATUS", temp1, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
                            ergListe.add(result);
                            throw new PPExecutionException();
                        }
                        temp1 = Ediabas.getDiagResultValue(jobres1);
                        temp2 = Ediabas.getDiagResultValue(jobres2);
                    } catch( ApiCallFailedException e ) {
                        result = new Ergebnis( PB.getString( "dbDiagFehler" ), "EDIABAS", sgbd1, job1, jobpar1, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), Ediabas.apiErrorCode()+": " + Ediabas.apiErrorText(), Ergebnis.FT_NIO );
                        ergListe.add(result);
                        throw new PPExecutionException();
                    } catch( EdiabasResultNotFoundException e ) {
                        if( jobres1 == null ) jobres1 = "null";
                        if (e.getMessage() != null)
                            result = new Ergebnis( PB.getString( "dbDiagFehler" ), "EDIABAS", sgbd1, job1, jobpar1, jobres1, "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                        else
                            result = new Ergebnis( PB.getString( "dbDiagFehler" ), "EDIABAS", sgbd1, job1, jobpar1, jobres1, "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
                        ergListe.add(result);
                        throw new PPExecutionException();
                    }
                }
                else {*/
                    try {
                        temp1 = Ediabas.executeDiagJob(sgbd1,job1,jobpar1,jobres1);
                        if( temp1.equals("OKAY") == false) {
                            result = new Ergebnis( PB.getString( "dbDiagFehler" ), "EDIABAS", sgbd1, job1, jobpar1, "JOB_STATUS", temp1, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
                            ergListe.add(result);
                            throw new PPExecutionException();
                        }
                        temp1 = Ediabas.getDiagResultValue(jobres1);
                    } catch( ApiCallFailedException e ) {
                        result = new Ergebnis( PB.getString( "dbDiagFehler" ), "EDIABAS", sgbd1, job1, jobpar1, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), Ediabas.apiErrorCode()+": " + Ediabas.apiErrorText(), Ergebnis.FT_NIO );
                        ergListe.add(result);
                        throw new PPExecutionException();
                    } catch( EdiabasResultNotFoundException e ) {
                        if( jobres1 == null ) jobres1 = "null";
                        if (e.getMessage() != null)
                            result = new Ergebnis( PB.getString( "dbDiagFehler" ), "EDIABAS", sgbd1, job1, jobpar1, jobres1, "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                        else
                            result = new Ergebnis( PB.getString( "dbDiagFehler" ), "EDIABAS", sgbd1, job1, jobpar1, jobres1, "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
                        ergListe.add(result);
                        throw new PPExecutionException();
                    }
                    
                    try {
                        temp2 = Ediabas.executeDiagJob(sgbd2,job2,jobpar2,jobres2);
                        if( temp2.equals("OKAY") == false) {
                            result = new Ergebnis( PB.getString( "dbDiagFehler" ), "EDIABAS", sgbd2, job2, jobpar2, "JOB_STATUS", temp2, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
                            ergListe.add(result);
                            throw new PPExecutionException();
                        }
                        temp2 = Ediabas.getDiagResultValue(jobres2);
                    } catch( ApiCallFailedException e ) {
                        result = new Ergebnis( PB.getString( "dbDiagFehler" ), "EDIABAS", sgbd2, job2, jobpar2, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), Ediabas.apiErrorCode()+": " + Ediabas.apiErrorText(), Ergebnis.FT_NIO );
                        ergListe.add(result);
                        throw new PPExecutionException();
                    } catch( EdiabasResultNotFoundException e ) {
                        if( jobres2 == null ) jobres2 = "null";
                        if (e.getMessage() != null)
                            result = new Ergebnis( PB.getString( "dbDiagFehler" ), "EDIABAS", sgbd2, job2, jobpar2, jobres2, "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                        else
                            result = new Ergebnis( PB.getString( "dbDiagFehler" ), "EDIABAS", sgbd2, job2, jobpar2, jobres2, "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
                        ergListe.add(result);
                        throw new PPExecutionException();
                    }
                //}
                
                //Analyse
                status = STATUS_EXECUTION_OK;
                f1 = new Float(Float.parseFloat(temp1));
                f2 = new Float(Float.parseFloat(temp2));
                f = new Float( ( f1.floatValue() / f2.floatValue()  ) * 100 - 100 );
                
                //Zahlenanalyse
                ok = false;
                if( (low.floatValue() <= f.floatValue()) && (f.floatValue() <= high.floatValue()) ) ok = true;
                
                if( (ok==true) && (invert == false) ) {
                    //Toleranz, somit IO
                    result = new Ergebnis( PB.getString( "dbDiagnose" ), "EDIABAS", sgbd1, job1+" "+job2, jobpar1+" "+jobpar2, jobres1+" "+jobres2, f.toString(), low.toString(), high.toString(), "0", "", "", awt, "", "", Ergebnis.FT_IO );
                    result1 = new Ergebnis( jobres1, "EDIABAS", sgbd1, job1, jobpar1, jobres1, f1.toString(), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO); 
                    result2 = new Ergebnis( jobres2, "EDIABAS", sgbd1, job2, jobpar2, jobres2, f2.toString(), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO);
                } else if( (ok==false) && (invert == true) ) {
                    //invertierte Toleranz, somit IO
                    result = new Ergebnis( PB.getString( "dbDiagnose" ), "EDIABAS", sgbd1, job1+" "+job2, jobpar1+" "+jobpar2, jobres1+" "+jobres2, f.toString(), "!("+low.toString()+")", "!("+high.toString()+")", "0", "", "", awt, "", "", Ergebnis.FT_IO );
                    result1 = new Ergebnis( jobres1, "EDIABAS", sgbd1, job1, jobpar1, jobres1, f1.toString(), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO); 
                    result2 = new Ergebnis( jobres2, "EDIABAS", sgbd1, job2, jobpar2, jobres2, f2.toString(), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO);
                } else if( (ok==false) && (invert == false) ) {
                    //Toleranz, somit NIO
                    result = new Ergebnis( PB.getString( "dbDiagnose" ), "EDIABAS", sgbd1, job1+" "+job2, jobpar1+" "+jobpar2, jobres1+" "+jobres2, f.toString(), low.toString(), high.toString(), "0", "", "", awt, PB.getString( "toleranzFehler3" ), hwt, Ergebnis.FT_NIO );
                    result1 = new Ergebnis( jobres1, "EDIABAS", sgbd1, job1, jobpar1, jobres1, f1.toString(), "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO); 
                    result2 = new Ergebnis( jobres2, "EDIABAS", sgbd1, job2, jobpar2, jobres2, f2.toString(), "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO);
                    status = STATUS_EXECUTION_ERROR;
                } else {
                    //invertierte Toleranz mit ok == true, somit NIO
                    result = new Ergebnis( PB.getString( "dbDiagnose" ), "EDIABAS", sgbd1, job1+" "+job2, jobpar1+" "+jobpar2, jobres1+" "+jobres2, f.toString(), "!("+low.toString()+")", "!("+high.toString()+")", "0", "", "", awt, PB.getString( "toleranzFehler4" ), hwt, Ergebnis.FT_NIO );
                    result1 = new Ergebnis( jobres1, "EDIABAS", sgbd1, job1, jobpar1, jobres1, f1.toString(), "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO); 
                    result2 = new Ergebnis( jobres2, "EDIABAS", sgbd1, job2, jobpar2, jobres2, f2.toString(), "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO);
                    status = STATUS_EXECUTION_ERROR;
                }
                
                ergListe.add(result);
                ergListe.add(result1);
                ergListe.add(result2);
                
                if( pause > 0 ) {
                    try {
                        Thread.sleep(pause);
                    } catch( InterruptedException e ) {
                    }
                }
                
                if( okTime > 0 ) { // Unter Umst�nden Zeitdauer noch nicht erf�llt
                    if( status == STATUS_EXECUTION_OK ) {
                        if( firstOkTime > System.currentTimeMillis() ) {
                            // Letzte Ausf�hrung war NIO
                            firstOkTime = System.currentTimeMillis();
                            result = new Ergebnis( "OK-Zeit", "", "", "", "", "Dauer", "0", ""+okTime, ""+Long.MAX_VALUE, "0", "", "", "", PB.getString( "okZeitUnterschritten" ), "", Ergebnis.FT_NIO );
                            status = STATUS_EXECUTION_ERROR;
                        } else {
                            //Letzte bzw. die letzten Ausf�hrung waren IO
                            deltaT = System.currentTimeMillis() - firstOkTime;
                            if( deltaT < okTime ) {
                                status = STATUS_EXECUTION_ERROR;
                                result = new Ergebnis( "OK-Zeit", "", "", "", "", "Dauer", ""+deltaT, ""+okTime, ""+Long.MAX_VALUE, "0", "", "", "", PB.getString( "okZeitUnterschritten" ), "", Ergebnis.FT_NIO );
                            } else {
                                result = new Ergebnis( "OK-Zeit", "", "", "", "", "Dauer", ""+deltaT, ""+okTime, ""+Long.MAX_VALUE, "0", "", "", "", "", "", Ergebnis.FT_IO );
                            }
                        }
                        ergListe.add(result);
                    } else {
                        //Fehlerhafte Ausf�hrung, Zeitstempel-Reset
                        firstOkTime = Long.MAX_VALUE;
                    }
                }
                
                if( (status != STATUS_EXECUTION_OK) && (endeZeit > System.currentTimeMillis()) ) {
                    //Nur wenn Loop nicht zuende
                    if( awt.equals("") == false ) {
                        if( udInUse == false ) {
                            //nur wenn was auszugeben ist und dieses noch nicht ausgegeben wurde
                            myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
                            myDialog.setAllowCancel(true);
                            myDialog.displayStatusMessage( awt+" "+PB.getString( "abbruchDurchWerker" ), -1 );
                            udInUse = true;
                        } else {
                            if( myDialog.isCancelled() == true ) {
                                result = new Ergebnis( PB.getString( "dbFrage" ), "Userdialog", "", "", "", "", "", "", "", "0", awt, "OK", "", PB.getString( "abbruchDurchWerker" ), "", Ergebnis.FT_NIO );
                                ergListe.add(result);
                                throw new PPExecutionException();
                            }
                        }
                    }
                }
            } while( (endeZeit > System.currentTimeMillis()) && (status != STATUS_EXECUTION_OK) );
            
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            if (e instanceof DeviceLockedException)
                result = new Ergebnis( PB.getString( "dbExecFehler" ), "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
            else if (e instanceof DeviceNotAvailableException)
                result = new Ergebnis( PB.getString( "dbExecFehler" ), "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
            else
                result = new Ergebnis( PB.getString( "dbExecFehler" ), "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
            result = new Ergebnis( PB.getString( "dbExecFehler" ), "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
        //Freigabe der benutzten Devices
        if( udInUse == true ) {
            try {
                getPr�flingLaufzeitUmgebung().releaseUserDialog();
            } catch (Exception e) {
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis( PB.getString( "dbExecFehler" ), "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( PB.getString( "dbExecFehler" ), "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                status = STATUS_EXECUTION_ERROR;
            }
        }
        
        //Status setzen
        setPPStatus( info, status, ergListe );
    }
}

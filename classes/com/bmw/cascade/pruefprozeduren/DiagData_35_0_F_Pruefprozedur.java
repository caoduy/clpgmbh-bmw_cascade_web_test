/*
 * DiagData_x_y_F_Pruefprozedur.java
 *
 * Created on 22.03.2004
 *
 * *
 * Created:          22.03.2004
 * Last modified:    06.02.2015
 *
 * Author:           Peter Winklhofer, BMW TI-430
 * Contact:          Tel.: +49/89/382-52493 (Winklhofer)
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.PruefstandConstants;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.*;

/**
 * Schreiben / Lesen / Schreiben u. Gegenlesen von Daten.
 * Dabei ist es m�glich beliebig viele Paare (Schreiben / Lesen) mit einem Aufruf abzuarbeiten.
 *
 * @author Peter Winklhofer, TI-431
 * @version Implementierung
 * @version 0_0_1_FA PW 22.03.2004 Erstellung (Winklhofer) <BR>
 * @version 0_0_2_FA PW 21.06.2004 Umstellung auf Attribute als Storage <BR>
 * @version 0_0_3_FA PW 21.06.2004 Verlagerung einiger methoden in JobExecution<BR>
 * @version 0_0_4_TA PW 02.09.2004 Umstellung Zahlenformat in Expressions von Integer auf Long. Neues Argument JOBN_IWT.<BR>
 * @version 0_0_5_FA PW 03.09.2004 FA-Version.<BR>
 * @version 0_0_6_TA PW 14.09.2004 Verbesserungen bei Ergebnis-Erzeugung, Filterung von NIO/NIOSYS bei IO-Pr�fung, Neuer Parameter JOBN_HIDEPAR<BR>
 * @version 0_0_7_FA PW 17.09.2004 FA-Version.<BR>
 * @version 0_0_8_TA PW 22.09.2004 BugFix Expression TAIL (currently not in use), Better handling for UserDialogs and Timeout
 *                                 Added new Expression-Operators IFELSE and SWITCH
 * @version 0_0_9_TA PW 28.10.2004 Added new Expression-Operators HEX2ASC, Allow expressions in IWT and HWT, SplitExpression will accept empty strings
 *                                 Better handling for error-reporting when links cannot be resolved or expressions are invalid
 *                                 JOB_STATUS is always checked. Added Expressions FILLLEFT and FILLRIGHT
 * @version 0_1_0_FA PW 10.01.2005 FA-Version
 * @version 0_1_1_TA PW 06.03.2005 Preparation for remove of Arg IWT, Use JOBN_IWT instead
 *                                 Display of UserDialogs redesigned
 *                                 If no JOBN_HWTi is specified use JOBN_QWT_NIOi
 * @version 0_1_2_TA PW 07.03.2005 Improved Error-Handling
 * @version 0_1_3_FA PW 15.03.2005 FA-Version
 * @version 0_1_4_TA PW 21.03.2005 Workaround for handterminal added.
 * @version 0_1_5_FA PW 22.03.2005 FA-Version
 * @version 15_0_T Umstellung auf CASCADE 2.0 durch Kernteam
 * @version 16_0_T PW 01.06.2005 Umstellung UserDialog (Workaround f�r Handterminal entfernen)
 * @version 17_0_T PW 06.06.2005 Umstellung von Arrays auf Vectoren f�r JobExecutions, Results, Conditions, ...
 *                               Umstellung extractArgs auf single-pass
 *                               Umstellung ADDITIONAL_ auf direkte attribute von JobExecution
 * @version 18_0_T FD 14.07.2005 JOBN_BREAKPOINT hinzugef�gt
 * @version 19_0_T PW 19.07.2005 Farbe bei IWTs default blau, Bei QWT wird auch JOBN_JUMP_NIO gepr�ft
 * @version 20_0_T PW 21.07.2005 Bugfix QWT_NIOSYS
 * @version 21_0_T PW 27.07.2005 Neue Expression CONTAINS hinzu 
 * @version 22_0_F FD 31.08.2005 F-Version
 * @version 23_0_T PW 01.09.2005 Bugfix: equals("JUMP_NIOSYS") jetzt vor startsWith("JUMP_NIO")
 * @version 24_0_F PW 01.09.2005 F-Version
 * @version 26_0_T PR 17.03.2009 Wenn mehrere Dialog-Ausgaben hintereinander erfolgen, dann zeigen die Dialoge im Maximized-Modus (Oxford) keinen Text mehr an (ab CASCADE 2.2.3).
 * @version 27_0_F PR 01.09.2005 F-Version
 * @version 28_0_T CW 18.04.2011 APDM-Ergebnistext im FT.IO Fall angepasst
 * @version 29_0_F CW 18.04.2011 F-Version
 * @version 30_0_T TB 05.02.2015 Zus�tzliches opt. Argument "DISABLE_USERDIALOG", im Fall DISABLE_USERDIALOG = true wird kein UserDialog verwendet (Anwendungsfall: paralleler Block mit Verwendung UserDialog) vgl. LOP Punkt: 1892 <br>
 * @version 31_0_F TB 08.02.2015 F-Version<br>
 * @version 32_0_T TB 03.08.2016 Erweiterungen f�r Paralleldiagnose analog zur DiagToleranzPP (LOP 2015), Umstellung auf EdiabasProxy, �berfl�ssiges Import entfernt + Generics eingebaut + Compiler Warnungen behoben<br>
 * @version 33_0_T TB 23.08.2016 BugFix wenn die Variable f�r Parallel nicht da ist (Fall ohne EDIABAS), dann nicht NIO machen, sondern einfach die Variable auf false setzten <br>
 * @version 34_0_T MKe 22.11.2016 BugFix status wird am Anfang der Execute Methode nun auf den default Wert zur�ck gesetzt <br>
 * @version 35_0_F MKe 20.12.2016 F-Version <br> 
 */
public class DiagData_35_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    static final long serialVersionUID = 1L;
    /**
     * DefaultKonstruktor, nur fuer die Deserialisierung
     */
    public DiagData_35_0_F_Pruefprozedur() {}
    
    //instanz-variablen
    private int debug = 0;
    
    private Vector<JobExecution> jobs = null;       //[JobExecution] Vector for Jobs (index 0 may be used)
    private JobExecution jobTemplate = null;
    private int firstJobIndex = 1;                  //for future extension (at the moment always 1)
    private int lastJobIndex = 1;                   //maximum index (max N where JOBN_* is given as arg)
    private long timeout = 0;                       //timeout, default 0
    private String iwt = null;                      //InfoWerkerText, default none
    private boolean displayStatusDialog = false;    //True when statusdialog should be displayed (more than 1 Job
    UserDialog myDialog = null;                     //user dialog
    String previousMsg = null;      //prevoiously displayed message for displayMessage
    int previousMsgJobIndex = -1;   //Last Job-Index for displayStatus
    
    //needed until a mechanism is integrated into the cascade kernel
    String Storage = null;    //Defines the PL that stores the dynamic attribute values (default current PL)
    
    int status = STATUS_EXECUTION_OK;
    
    // Paralleldiagnose und Umstellung auf EDIABAS ProxyThread
    EdiabasProxyThread ediabas =null;
	boolean parallel = false;  
	String tag = null; 
	DeviceManager devMan = null;
    
    //public static definitionen
    /**
     * possible commands for Expressions
     * HEX,INT2HEX,DATA,UNDATA,EQUALS,...
     */
    public static final String Commands[][] = { {"HEX",         "H"},   //Put a '0x' at the beginning of the argument
    {"INT2HEX",     "I"},   //convert an integer to HEX and add a '0' at the beginning if the results has an odd number of characters
    {"HEX2INT",     "2"},   //convert a HEX to an integer e.g. HEX2INT("12") = "18"
    {"DATA",        "D"},   //covert a HEX-Input-String e.g. "123456" to a string where the bytes are separated by spaces e.g. "12 34 56". Also a toUpperCase is done.
    {"UNDATA",      "U"},   //undo the operation of DATA. That means spaces are removed. Also a toUpperCase is done.
    {"EQUALS",      "E"},   //Compares the two arguments
    {"AND",         "A"},   //All Arguments have to be DEF_TRUE
    {"OR",          "O"},   //One Argument has to be DEF_TRUE
    {"NOT",         "N"},   //The Argument is converted from DEF_TRUE to DEF_FALSE and from DEF_FALSE to DEF_TRUE
    {"EQUALSIC",    "C"},   //Does the same as EQUALS, but the comparison is not case sensitive.
    {"TOLOWER",     "K"},   //Converts the argument to lower case
    {"TOUPPER",     "T"},   //Converts the argument to upper case
    {"LESSTHAN",    "L"},   //the first argument has to be smaller than the second argument
    {"LESS",        "L"},   //the first argument has to be smaller than the second argument
    {"GREATERTHAN", "G"},   //the first argument has to be greater than the second argument
    {"GREATER",     "G"},   //the first argument has to be greater than the second argument
    {"LESSOREQ",    "X"},   //the first argument has to be smaller or equal than the second argument
    {"GREATEROREQ", "Y"},   //the first argument has to be greater or equal than the second argument
    {"BETWEEN",     "B"},   //first argument smaller second argument smaller third argument a1 < a2 < a3
    {"BETWEENIB",   "Z"},   //between including boundaries a1 <= a2 <= a3
    {"SUBSTR",      "S"},   //extract a substring of the first argument. The second argument determines the beginning. The third determines the length if present.
    {"SUBSTRING",   "S"},   //extract a substring of the first argument. The second argument determines the beginning. The third determines the length if present.
    {"HEAD",        "P"},   //extract a heading substring of the first argument. The second argument determines the length.
    {"TAIL",        "Q"},   //extract a tailing substring of the first argument. The second argument determines the length.
    {"REVERSE",     "R"},   //The Argument String is Reversed
    {"LENGTH",      "V"},   //Returns the length of the Argument
    {"LEN",         "V"},   //Returns the length of the Argument
    {"CONCAT",      "J"},   //concatenates the arguments in the given order
    {"TRIM",        "1"},   //remove all leading and ending spaces
    {"MINUS",       "M"},   //Substract the second argument from the first one
    {"PLUS",        "F"},   //add both arguments
    {"IF",          "3"},   //IF_THEN_ELSE: first argument is the condition (DEF_TRUE/DEF_FALSE), the second is the result in case DEF_TRUE, the third is the result in case DEF_FALSE.
    {"IFELSE",      "3"},   //IF_THEN_ELSE: first argument is the condition (DEF_TRUE/DEF_FALSE), the second is the result in case DEF_TRUE, the third is the result in case DEF_FALSE.
    {"SWITCH",      "4"},   //SWITCH: first argument is throws input, the last argument is the default result, between are pairs of comparevalue and result in case input equals comparevalue.
    {"HEX2ASC",     "5"},   //Converts the Input to ASCII. Two chars input are converted to one char output.
    {"HEX2ASCII",   "5"},   //Converts the Input to ASCII. Two chars input are converted to one char output.
    {"FILLLEFT",    "6"},   //Fills up the given String (arg1) to a certain length (arg2) by adding a given String (arg3) in front.
    {"FILLRIGHT",   "7"},   //Fills up the given String (arg1) to a certain length (arg2) by adding a given String (arg3) at the end.
    {"CONTAINS",    "8"}};  //Checks if Arg1 is contained in the list of Arg2, ... Argn. Returns DEF_TRUE when contained, else DEF_FALSE.
    
    /**
     * String signaling TRUE
     */
    public static final String DEF_TRUE = "T";
    /**
     * String signaling TRUE
     */
    public static final String DEF_FALSE = "F";
    
    //define jump-signals (normal values are 0...N for JOB 0...N
    private static final int DEF_JUMP_EXIT_IO = -1;
    private static final int DEF_JUMP_EXIT_NIO = -2;
    private static final int DEF_JUMP_UNDEF = -100;
    
    /**
     * erzeugt eine neue Pruefprozedur, die auf Einhaltung bzw. auf Ausschlu� der zu analysierenden Werte pr�ft.
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public DiagData_35_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    /**
     * Clear all instance-variables (set references to null)
     * Should be called at end of checkArgs and execute
     */
    private void cleanup() {
        jobs = null;
        jobTemplate = null;
        iwt = null;
        Storage = null;
        myDialog = null;
        previousMsg = null;      //prevoiously displayed message for displayMessage
        previousMsgJobIndex = -100;   //Last Job-Index for displayStatus
        // Ediabas + Parallel
        ediabas =null;
    	tag = null; 
    	devMan = null;
    }
    
    private void waitms(long mseconds) {
        
        if (debug > 2) System.out.println("DiagData.waitms: waiting " + mseconds);
        
        long ende = System.currentTimeMillis() + mseconds;
        long curr = 0;
        
        while (ende > (curr=System.currentTimeMillis()) ) {
            try {
                this.wait(ende-curr);
            } catch (Exception e) {
                //Do nothing
            }
        }
    }
    
    /**
     * @param ergs optional Erglist
     */
    private void releaseDialog(Vector<Ergebnis> ergs) {
        Ergebnis result;
        if (myDialog != null) {
            try {
                getPr�flingLaufzeitUmgebung().releaseUserDialog();
            } catch (Exception e) {
                //Create a result, but ignore the error
                if (e instanceof DeviceLockedException) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Device ist bereits belegt", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
                } else {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Fehler: "+e.getMessage(), Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Exception: "+e.getMessage(), Ergebnis.FT_NIO_SYS );
                }
                ergs.add(result);
                //status = STATUS_EXECUTION_ERROR;
                if (debug > 0) System.out.println("DiagData.releaseDialog: Error releaseUserDialog() failed");
                if (debug > 0) e.printStackTrace(System.out);
            }
        }
        myDialog = null;
        //Reset data for displayStatusDialog
        previousMsg = null;      //prevoiously displayed message for displayMessage
        previousMsgJobIndex = -100;   //Last Job-Index for displayStatus
        
    }
    
    /**
     * @param msg the message to be displayed
     * @param currentJobIndex (-1 = none)
     * @param ergs optional Erglist
     */
    private void displayStatusDialog(String msg, int currentJobIndex, Vector<Ergebnis> ergs) throws PPExecutionException {
        Ergebnis result;
        char style = '0';
        
        try {
            if (debug > 1) System.out.println("DiagData.displayStatus: displayDialog extract args");
            
            //check if something has changed
            if (( (msg == previousMsg) || ((msg != null) && msg.equals(previousMsg)))
            && (previousMsgJobIndex == currentJobIndex)) {
                //nothing to do cause the message and the jobIndex have not changed
                return;
            }
            
            //remember current values for next call
            previousMsgJobIndex = currentJobIndex;
            previousMsg = msg;
            
            //Added as workaround for handterminal problems
            if (myDialog != null) {
                //release user-dialog
                try {
                    if (debug > 1) System.out.println("DiagData.displayStatus: releaseDialog begin");
                    getPr�flingLaufzeitUmgebung().releaseUserDialog();
                } catch (Exception e) {
                    if (debug > 1) System.out.println("DiagData.displayStatus: releaseDialog exception");
                    if (debug > 1) e.printStackTrace(System.out);
                    if (e instanceof DeviceLockedException) {
                        if (isDE()) result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Device ist bereits belegt", Ergebnis.FT_NIO_SYS );
                        else result = new Ergebnis( "ExecError", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
                    } else {
                        if (isDE()) result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Fehler: "+e.getMessage(), Ergebnis.FT_NIO_SYS );
                        else result = new Ergebnis( "ExecError", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Exception: "+e.getMessage(), Ergebnis.FT_NIO_SYS );
                    }
                    if (ergs != null) ergs.add(result);
                }
                myDialog = null;
            } //[RETTIG: Wenn mehrere Dialog-Ausgaben hintereinander erfolgen, dann zeigen die Dialoge im Maximized-Modus (Oxford) keinen Text mehr an. Durch das obige releaseUserDialog wird das Problem beseitigt.]
            
            if (myDialog == null) {
                //request a user-dialog if not already done
                try {
                    if (debug > 1) System.out.println("DiagData.displayStatus: requestDialog begin");
                    myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
                    if (timeout > 0) {
                        //allow cancel when timeout is present
                        myDialog.setAllowCancel(true);
                    }
                    //Reset data for displayStatusDialog
                    //previousMsg = null;      //prevoiously displayed message for displayMessage
                    //previousMsgJobIndex = -100;   //Last Job-Index for displayStatus
                } catch (Exception e) {
                    if (debug > 1) System.out.println("DiagData.displayStatus: requestDialog exception");
                    if (debug > 1) e.printStackTrace(System.out);
                    myDialog = null;
                    if (e instanceof DeviceLockedException) {
                        if (isDE()) result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Device ist bereits belegt", Ergebnis.FT_NIO_SYS );
                        else result = new Ergebnis( "ExecError", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
                    } else {
                        if (isDE()) result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Fehler: "+e.getMessage(), Ergebnis.FT_NIO_SYS );
                        else result = new Ergebnis( "ExecError", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Exception: "+e.getMessage(), Ergebnis.FT_NIO_SYS );
                    }
                    if (ergs != null) ergs.add(result);
                    return; //no dialog available
                }
            }
            
            //determine message-style
            if ((msg != null) && (msg.charAt(1) == ';') && 
                    (Character.isDigit(msg.charAt(0))   //Numeric values
                    || (Character.toLowerCase(msg.charAt(0)) == 'r')           //Rot
                    || (Character.toLowerCase(msg.charAt(0)) == 'b')           //Blau
                    || (Character.toLowerCase(msg.charAt(0)) == 'g')           //Gr�n
                    )) {
                style = Character.toLowerCase(msg.charAt(0));  //1=requestStatusMessage, 2=requestAlertMessage, sonst=requestUserMessage
                msg = msg.substring(2);
            } else {
                style = '1';//1=requestStatusMessage=blue, 2=requestAlertMessage=red, sonst=requestUserMessage=green
            }
            // add jobIndex
            msg = (msg==null) ? "" : msg;
            if (currentJobIndex != -1) {
                msg = msg + "\r\n" + currentJobIndex + "/" + lastJobIndex;
            }
            
            if (debug > 1) System.out.println("DiagData.displayStatus: displayDialog begin to display");
            //allow cancel when timeout is present
            //            if (timeout > 0) {
            //                //allow cancel when timeout is present
            //                myDialog.setAllowCancel(true);
            //            }            
            switch (style) {
                case '1':
                case 'b':                	
                    myDialog.displayStatusMessage( PB.getString( "anweisung" ), msg, -1 );                    
                    break;
                case '2':
                case 'r':
                    myDialog.displayAlertMessage( PB.getString( "anweisung" ), msg, -1 );
                    break;
                default:
                    myDialog.displayMessage( PB.getString( "anweisung" ), msg, -1 );                
            }
            
            if (debug > 1) System.out.println("DiagData.displayStatus: displayDialog message display");
        } catch (Exception e) {
            if (debug > 1) System.out.println("DiagData.displayStatus: displayDialog exception during display");
            if (debug > 0) e.printStackTrace(System.out);
            result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Exception: "+e.getMessage(), Ergebnis.FT_NIO_SYS );
            if (ergs != null) ergs.add(result);
        }
        
    }
    
    /**
     * @param msg the message to be displayed
     * @param ergs optional Erglist
     */
    private void requestDialog(String msg, Vector<Ergebnis> ergs) throws PPExecutionException {
        Ergebnis result;
        char style = '2';
        
        if (myDialog != null) {
            //release user-dialog
            try {
                if (debug > 1) System.out.println("DiagData.requestDialog: releaseDialog begin");
                getPr�flingLaufzeitUmgebung().releaseUserDialog();
            } catch (Exception e) {
                if (debug > 1) System.out.println("DiagData.requestDialog: releaseDialog exception");
                if (debug > 1) e.printStackTrace(System.out);
                if (e instanceof DeviceLockedException) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Device ist bereits belegt", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
                } else {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Fehler: "+e.getMessage(), Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Exception: "+e.getMessage(), Ergebnis.FT_NIO_SYS );
                }
                if (ergs != null) ergs.add(result);
            }
            myDialog = null;
        }
        
        //Reset data for displayStatusDialog
        previousMsg = null;      //prevoiously displayed message for displayMessage
        previousMsgJobIndex = -100;   //Last Job-Index for displayStatus
        
        if (myDialog == null) {
            //request a user-dialog if not already done
            try {
                if (debug > 1) System.out.println("DiagData.requestDialog: requestDialog begin");
                myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
                previousMsg = null;      //Iwt in the previous loop
            } catch (Exception e) {
                if (debug > 1) System.out.println("DiagData.requestDialog: requestDialog exception");
                if (debug > 1) e.printStackTrace(System.out);
                myDialog = null;
                if (e instanceof DeviceLockedException) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Device ist bereits belegt", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
                } else {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Fehler: "+e.getMessage(), Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Exception: "+e.getMessage(), Ergebnis.FT_NIO_SYS );
                }
                if (ergs != null) ergs.add(result);
                return; //no dialog available
            }
        }
        
        try {
            if (debug > 1) System.out.println("DiagData.requestDialog: displayDialog extract args");
            
            //determine message-style
            if ((msg != null) && (msg.charAt(1) == ';') && 
                    (Character.isDigit(msg.charAt(0))   //Numeric values
                    || (Character.toLowerCase(msg.charAt(0)) == 'r')           //Rot
                    || (Character.toLowerCase(msg.charAt(0)) == 'b')           //Blau
                    || (Character.toLowerCase(msg.charAt(0)) == 'g')           //Gr�n
                    )) {
                style = Character.toLowerCase(msg.charAt(0));  //1=requestStatusMessage, 2=requestAlertMessage, sonst=requestUserMessage
                msg = msg.substring(2);
            } else {
                style = '2';    //1=requestStatusMessage=blue, 2=requestAlertMessage=red, sonst=requestUserMessage=green
            }
            
            if (debug > 1) System.out.println("DiagData.requestDialog: displayDialog begin to display");
            switch (style) {
                case '1':
                case 'b':
                    myDialog.requestStatusMessage( PB.getString( "anweisung" ), msg, 0 );
                    break;
                case '2':
                case 'r':
                    myDialog.requestAlertMessage( PB.getString( "anweisung" ), msg, 0 );
                    break;
                default:
                    myDialog.requestUserMessage( PB.getString( "anweisung" ), msg, 0 );
            }
            if (debug > 1) System.out.println("DiagData.requestDialog: displayDialog message display");
        } catch (Exception e) {
            if (debug > 1) System.out.println("DiagData.requestDialog: displayDialog exception during display");
            if (debug > 0) e.printStackTrace(System.out);
            if (isDE()) result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Fehler: "+e.getMessage(), Ergebnis.FT_NIO_SYS );
            else result = new Ergebnis( "ExecError", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Exception: "+e.getMessage(), Ergebnis.FT_NIO_SYS );
            if (ergs != null) ergs.add(result);
        }
        
        //release the dialog
        releaseDialog(ergs);
    }
    
    
    /**
     * initialsiert die Argumente
     */
    @Override
	protected void attributeInit() {
        super.attributeInit();
    }
    
    
    /**
     * liefert die optionalen Argumente
     * DEBUG: Debug on/off (TRUE,FALSE, or Level 0,1,2,...)
     */
    @Override
	public String[] getOptionalArgs() {
        String[] args = {
            "DEBUG",                           //0 ... n or true/false
            "STORAGE",                         //PL for storing dynamic Attributes
            "LOOP_FROM",                       //For Loop %N% starts with LOOP_FROM and ends with LOOP_UNTIL (default 1)
            "LOOP_UNTIL",                      //For Loop %N% starts with LOOP_FROM and ends with LOOP_UNTIL
            "TIMEOUT",                         //Timeout for exiting IO
            "QWT_TIMEOUT",                     //message which has to be confirmed after a timeout has occured
            //"IWT",                             //Infotext is DISPLAYED while multiple JOBS are executed or a PAUSE is present
            "JOBN_SGBD",                       //Default for All Jobs: SGBD
            "JOBN_JOB",                        //Default for All Jobs: JOB
            "JOBN_PAR",                        //Default for All Jobs: Parameters
            "JOBN_HIDEPAR",                    //Default for All Jobs: flag for hiding Par
            "JOBN_RESULT[1..X]",               //Default for All Jobs: ResultX
            "JOBN_HIDERESULT[1..X]",           //Default for All Jobs: flag for hiding ResultX
            "JOBN_CONDITION[1..Y]",            //Default for All Jobs: ConditionY
            "JOBN_HWT[1..Y]",                  //Default for All Jobs: HWT for Condition Y
            "JOBN_CONDITIONMODE",              //Default for All Jobs: CONDITIONMODE
            "JOBN_STORAGENAME[1..Z]",          //Default for All Jobs: StoragenameZ
            "JOBN_STORAGEFORMAT[1..Z]",        //Default for All Jobs: StorageformatZ
            "JOBN_HWT",                        //Default for All Jobs: HWT for all conditions
            "JOBN_PAUSE_BEFORE",               //Default for All Jobs: break-time before job is executed
            "JOBN_PAUSE_AFTER",                //Default for All Jobs: break-time after job has been executed
            "JOBN_JUMP_IO",                    //Default for All Jobs: action after a job has been executed IO: next JobNumber or EXITIO or EXITNIO
            "JOBN_JUMP_NIO",                   //Default for All Jobs: action after a job has been executed NIO: next JobNumber or EXITIO or EXITNIO
            "JOBN_JUMP_NIO[1..Y]",             //Default for All Jobs: action after condition Y has caused a NIO: next JobNumber or EXITIO or EXITNIO
            "JOBN_JUMP_NIOSYS",                //Default for All Jobs: action after a job has been executed NIOSYS: next JobNumber or EXITIO or EXITNIO
            "JOBN_QWT_NIO",                    //Default for All Jobs: message which has to be confirmed after a job has been executed NIO
            "JOBN_QWT_NIO[1..Y]",              //Default for All Jobs: message which has to be confirmed after condition Y has caused a NIO
            "JOBN_QWT_NIOSYS",                 //Default for All Jobs: message which has to be confirmed after a job has been executed NIOSYS
            "JOBN_IWT",                        //Default for All Jobs: Infotext which is displayed, when Job N is executed
            "JOBN_BREAKPOINT",                 //Default for All Jobs: Break after execution
            "DISABLE_USERDIALOG",              //Default: UserDialog is used
            "TAG"};                            //Vorhalt fuer Paralleldiagnose, aktuell ohne Funktion
            return args;
    }
    
    /**
     * liefert die zwingend erforderlichen Argumente
     */
    @Override
	public String[] getRequiredArgs() {
        String[] args = {};
        return args;
    }
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die parent-Methode aufgrund
     * der offenen Anzahl an Results
     * @return true when check is okay, false otherwise
     */
    @Override
	public boolean checkArgs() {
        Vector<Ergebnis> errors = new Vector<Ergebnis>();
        return checkArgs(errors);
    }
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die parent-Methode aufgrund
     * der offenen Anzahl an Results
     * @param Result decribing the occured error, NO IO-Results added!!!
     * @return true when check is okay, false otherwise
     */
    @SuppressWarnings("unchecked")
	public boolean checkArgs(Vector<Ergebnis> errors) {
        String value;
        Ergebnis result;

        //Debug
        value = getArg("DEBUG");
        if ((value == null) || (value.length() == 0)) {
            debug = 0;
        } else if ("TRUE".equalsIgnoreCase(value)) {
            debug = 2;
        } else if ("FALSE".equalsIgnoreCase(value)) {
            debug = 0;
        } else {
            try {
                debug = Integer.parseInt(value);
                if (debug < 0) debug = 0;
            } catch (NumberFormatException nfe) {
                if (isDE()) System.out.println("CASPlusData.checkArgs: DEBUG ung�ltig");
                else System.out.println("CASPlusData.checkArgs: DEBUG invalid");
                if (isDE()) result = new Ergebnis( "ExecFehler", "DEBUG", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "DEBUG", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }
        }
        if (debug > 0) System.out.println("DiagData.checkArgs:  DEBUG enabled! Level:"+debug);
        if (debug > 0) System.out.println("DiagData.checkArgs: Start");
        //for checking if unknown args are present
        Set<Object> myargs = new HashSet<Object>();             //Set for keeping all keys that have not been handled so far
        myargs.addAll(getArgs().keySet());      //Add all keys to the set
        if (debug > 1) {
            System.out.println("DiagData.checkArgs.myargs conatains");
            Iterator<Object> dIter = myargs.iterator();
            while (dIter.hasNext()) System.out.println("DiagData.checkArgs.myargs conatains: " + dIter.next());
        }
        //DEBUG
        if (getArg("DEBUG") != null) {
            myargs.remove("DEBUG");             //DEBUG has been handled
        }
        if (debug > 2) {
            System.out.println("DiagData.checkArgs.myargs conatains after remove DEBUG");
            Iterator<Object> dIter = myargs.iterator();
            while (dIter.hasNext()) System.out.println("DiagData.checkArgs.myargs conatains: " + dIter.next());
        }
        
        //required Args
        //=====================================================
        
        //no required args
        
        //optional Args
        //=====================================================
        
        //STORAGE
        if ((getArg("STORAGE") != null) && (getArg("STORAGE").length() != 0)){
            myargs.remove("STORAGE");           //Remove Storage
        }
        if (debug > 2) {
            System.out.println("DiagData.checkArgs.myargs conatains after remove STORAGE");
            Iterator<Object> dIter = myargs.iterator();
            while (dIter.hasNext()) System.out.println("DiagData.checkArgs.myargs conatains: " + dIter.next());
        }
        //TIMEOUT: Timeout for exiting IO
        value = getArg("TIMEOUT");
        if (value != null) {
            if (value.length() == 0) {
                System.out.println("DiagData.checkArgs: TIMEOUT '"+value+"' is invalid");
                if (isDE()) result = new Ergebnis( "ExecFehler", "TIMEOUT", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "TIMEOUT", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }
            try {
                timeout = Long.parseLong(value);
                if (timeout <= 0) {
                    System.out.println("DiagData.checkArgs: TIMEOUT '"+value+"' is invalid");
                    if (isDE()) result = new Ergebnis( "ExecFehler", "TIMEOUT", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "TIMEOUT", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error", Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    return false;
                }
            } catch (NumberFormatException nfe) {
                System.out.println("DiagData.checkArgs: TIMEOUT '"+value+"' is invalid");
                if (isDE()) result = new Ergebnis( "ExecFehler", "TIMEOUT", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "TIMEOUT", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }
            myargs.remove("TIMEOUT");           //Remove Timeout
        } else {
            timeout = 0;                        //default no timeout
        }
        //QWT_TIMEOUT: message which has to be confirmed after a timeout has occured
        value = getArg("QWT_TIMEOUT");
        if (value != null) {
            if (value.trim().length() == 0) {
                System.out.println("DiagData.checkArgs: QWT_TIMEOUT '"+value+"' is invalid");
                if (isDE()) result = new Ergebnis( "ExecFehler", "QWT_TIMEOUT", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "QWT_TIMEOUT", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }
            myargs.remove("QWT_TIMEOUT");           //Remove QWT_TIMEOUT
        }
        //IWT: Infotext is DISPLAYED while multiple JOBS are executed or a PAUSE is present
        value = getArg("IWT");
        if (value != null) {
            if (value.length() == 0) {
                System.out.println("DiagData.checkArgs: IWT '"+value+"' is invalid");
                if (isDE()) result = new Ergebnis( "ExecFehler", "IWT", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "IWT", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }
            myargs.remove("IWT");           //Remove Iwt
        }
        //DISABLE_USERDIALOG:
        value = getArg("DISABLE_USERDIALOG");
        if (value != null) {
        	// potentiellen @-Operator ber�cksichtigen
            try {
            	value = extractValues (value)[0];
            } catch (PPExecutionException pee) {
            	value = "empty";
            }
            // einfache �berpr�fung
            if ( !"TRUE".equalsIgnoreCase( value ) && !"FALSE".equalsIgnoreCase( value ) ) {
                System.out.println("DiagData.checkArgs: DISABLE_USERDIALOG '"+value+"' is invalid");
                if (isDE()) result = new Ergebnis( "ExecFehler", "DISABLE_USERDIALOG", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "DISABLE_USERDIALOG", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                return false;
            }
            myargs.remove("DISABLE_USERDIALOG");           //Remove DISABLE_USERDIALOG
        }
        //Extract remaining JOBN_ args
        try {
            this.extractArgsForAllsJobs(myargs, false,errors);
        } catch (Exception e) {
            e.printStackTrace(System.out);
            cleanup();
            if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler", Ergebnis.FT_NIO_SYS );
            else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error", Ergebnis.FT_NIO_SYS );
            errors.add(result);
            return false;
        }
        
        if (myargs.size() > 0) {
            System.out.println("DiagData.checkArgs: Number of unknown arguments " + myargs.size());
            if (isDE()) result = new Ergebnis( "ExecFehler", "UNKNOWNARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler unbekannte Argumente vorhanden", Ergebnis.FT_NIO_SYS );
            else result = new Ergebnis( "ExecError", "UNKNOWNARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error unknown args present", Ergebnis.FT_NIO_SYS );
            errors.add(result);
            return false;
        }
        
        if (debug > 0) System.out.println("DiagData.checkArgs: End");
        cleanup();
        return true;        //no required Arguments, any Arguments allowed
    }
    
    /**
     * f�hrt die Pr�fprozedur aus <br>
     * Parameter DISABLE_USERDIALOG (optional)
     *      Meaning: On true no UserDialog is used inside this PP
     *      Values: true / false
     *      Default: false<br>
     * Parameter DEBUG (optional)
     *      Meaning: Specify the trace-level. false or 0 disables the trace. true is the same as level 2.
     *      Values: 0 ... n or true/false
     *      Default: level 0.<br>
     * Parameter STORAGE (optional)
     *      Meaning: PL for storing dynamic Attributes
     *      Values: logical name of a Pr�fling
     *      Default: current PL where the current teststep is defined<br>
     * Parameter LOOP_FROM (optional)
     *      Meaning: When the first job-index is different from 1, you may specify another first job-index to be used.
     *      Values: a natural number greater or equal to 0 (e.g. 0,1,2,3,...).
     *      Default: 1<br>
     * Parameter LOOP_UNTIL (optional)
     *      Meaning: When the autodetection for the last job-index won�t work (e.g. you have only parameters staring with JOBN_),
     *              you may specify another last job-index.
     *              Note: When a parameter JOBx_ with a x greater than LOOP_UNTIL is present, the autodetection will set the last job-index to x.
     *      Values: a natural number greater or equal to LOOP_FROM (e.g. 0,1,2,3,...).
     *      Default: 1 or the greatest x of all parameters JOBx_ that is present (autodetection)<br>
     * Parameter TIMEOUT (optional)
     *      Meaning: Timeout for exiting IO. That means all Jobs executed IO (depending on the conditions) or a explicit JUMP_... with value EXIT_IO has been executed within timeout.
     *      Values: timeout in ms (e.g. 30000 for 30 seconds)
     *      Default: no timeout<br>
     * Parameter QWT_TIMEOUT (optional, only in conjunction with TIMEOUT)
     *      Meaning: message which has to be confirmed after a timeout occured
     *      Values: a text (no brakets) or an expression
     *      Default: none (no message will be displayed).<br>
     * Parameter IWT (deprecated, optional)
     *      Meaning: Infotext that is displayed while multiple JOBS are executed or a PAUSE is present. Same as JOBx_IWT (x is the first job-index).
     *      Values: a text (no brakets) or an expression (note the expression should be resolvable before the first job is executed).
     *      Default: none<br>
     * Parameter: JOBi_SGBD where i is the job-index (JOBi_SGBD or JOBN_SGBD required)
     *      Meaning: SGBD for job i.
     *      Values: SGBD-name or an expression
     *      Default: value of JOBN_SGBD
     *      Default for JOBN_SGBD: undefined<br>
     * Parameter: JOBi_JOB where i is the job-index (JOBi_JOB or JOBN_JOB required)
     *      Meaning: JOB-name for job i.
     *      Values: JOB-name or an expression
     *      Default: value of JOBN_JOB
     *      Default for JOBN_JOB: undefined<br>
     * Parameter: JOBi_PAR where i is the job-index (optional)
     *      Meaning: parameters for job i.
     *      Values: text or expressions separated by ';' (one or more parameter details, where each one is a text or an expression).
     *      Default: value of JOBN_PAR
     *      Default for JOBN_PAR: no parameters<br>
     * Parameter: JOBi_HIDEPAR where i is the job-index (optional, only in conjunction with JOBi_PAR)
     *      Meaning: When JOBi_HIDEPAR is true the parameters won�t be visible in the CASCADE- or APDM-traces.
     *              Note: This flag won�t influence the EDIABAS API- and IFH-traces.
     *      Values: true / false
     *      Default: value of JOBN_HIDEPAR
     *      Default for JOBN_HIDEPAR: false (parameters visible)<br>
     * Parameter: JOBi_RESULTx where i is the job-index and x is a numeration for the results (1,2,3,...) of the job i (optional)
     *      Meaning: JOBi_RESULTx specifies the name of the result x for job i.
     *              Note: The JOB_STATUS will always be queried, even when not specified.
     *      Values: a text or an expression
     *      Default: value of JOBN_RESULTx
     *      Default for JOBN_RESULTx: not defined<br>
     * Parameter: JOBi_HIDERESULTx where i is the job-index and x is a numeration for the results (1,2,3,...) of the job i (optional, only in conjunvtion with JOBi_RESULTx)
     *      Meaning: When JOBi_HIDERESULTx is true the result values won�t be visible in the CASCADE- or APDM-traces.
     *              Note: This flag won�t influence the EDIABAS API- and IFH-traces.
     *      Values: true / false
     *      Default: value of JOBN_HIDERESULTx
     *      Default for JOBN_HIDERESULTx: false (result values visible)<br>
     * Parameter: JOBi_CONDITIONy where i is the job-index and y is a numeration for the conditions (1,2,3,...) of the job i (optional)
     *      Meaning: JOBi_CONDITIONy specifies condition y for job i.
     *      Values: an expression, that resolves to DEF_TRUE or DEF_FALSE
     *      Default: value of JOBN_CONDITIONy
     *      Default for JOBN_CONDITIONy: not defined<br>
     * Parameter: JOBi_HWTy where i is the job-index and y is a numeration for the conditions (1,2,3,...) of the job i (optional, only in conjunction with JOBi_CONDITIONy)
     *      Meaning: JOBi_HWTy specifies the information text (HWT) included in the CASCADE NIO result, when JOBi_CONDITIONy has been responsible for exiting NIO.
     *      Values: a text or an expression
     *      Default: value of JOBN_HWTy if defined, else value of JOBN_QWTy if defined, else value of JOBN_HWT
     *      Default for JOBN_HWTy: not defined<br>
     * Parameter: JOBi_JUMP_NIOy where i is the job-index and y is a numeration for the conditions (1,2,3,...) of the job i (optional, only in conjunction with JOBi_CONDITIONy)
     *      Meaning: JOBi_JUMP_NIOy specifies where to jump (next job-index) / what to do (exit io / nio) when condition y for job i failed.
     *      Values: a job-index for specifying the next job to be executed OR EXIT_IO OR EXIT_NIO
     *      Default: value of JOBN_JUMP_NIOy when defined, else value of JOBi_JUMP_NIO
     *      Default for JOBN_JUMP_NIOy: not defined<br>
     * Parameter: JOBi_QWT_NIOy where i is the job-index and y is a numeration for the conditions (1,2,3,...) of the job i (optional, only in conjunction with JOBi_CONDITIONy)
     *      Meaning: JOBi_QWT_NIOy specifies the message that has to be confirem, when condition y for job i failed.
     *      Values: a text or an expression
     *      Default: value of JOBN_QWT_NIOy when defined, else value of JOBi_QWT_NIO
     *      Default for JOBN_QWT_NIOy: not defined<br>
     * Parameter: JOBi_CONDITIONMODE where i is the job-index (optional)
     *      Meaning: Specifies if all conditions have to be true (AND) / one condition has to be true (OR) / all conditions must be false (NOR)
     *      Values: AND / OR / NOR
     *      Default: value of JOBN_CONDITIONMODE
     *      Default for JOBN_CONDITIONMODE: AND<br>
     * Parameter: JOBi_STORAGENAMEz where i is the job-index and z (1,2,3,...) is a numeration for the values to be exported as a dynamic attribute of the job i (optional)
     *      Meaning: JOBi_STORAGENAMEz specifies the label of the dynamic attribute z for job i.
     *      Values: a text or an expression
     *      Default: value of JOBN_STORAGENAMEz
     *      Default for JOBN_STORAGENAMEz: not defined<br>
     * Parameter: JOBi_STORAGEFORMATz where i is the job-index and z (1,2,3,...) is a numeration for the values to be exported as a dynamic attribute of the job i
     *          (optional, only in conjuction with JOBi_STORAGENAMEz)
     *      Meaning: JOBi_STORAGEFORMATz specifies the content of the dynamic attribute z for job i.
     *      Values: an expression specifying the content for the dynamic attribute
     *      Default: value of JOBN_STORAGEFORMATz
     *      Default for JOBN_STORAGEFORMATz: not defined<br>
     * Parameter: JOBi_HWT where i is the job-index (optional)
     *      Meaning: specifies the information text (HWT) included in the CASCADE NIO result, when the current job leads to a EXIT_NIO and no specific JOBi_HWTy or JOBi_QWTy is present.
     *              Also used when a NIOSYS is responsible for exiting NIO.
     *      Values: a text or an expression
     *      Default: value of JOBN_HWT
     *      Default for JOBN_HWT: not defined<br>
     * Parameter: JOBi_PAUSE_BEFORE where i is the job-index (optional)
     *      Meaning: JOBi_PAUSE_BEFORE specifies the time to be waited before executing the job i.
     *      Values: natural number greater than 0 (e.g. 30000 for 30 seconds)
     *      Default: value of JOBN_PAUSE_BEFORE
     *      Default for JOBN_PAUSE_BEFORE: 0<br>
     * Parameter: JOBi_PAUSE_AFTER where i is the job-index (optional)
     *      Meaning: JOBi_PAUSE_AFTER specifies the time to be waited before executing the job i.
     *          Note: The time is always waited, also when the job failed!
     *      Values: natural number greater than 0 (e.g. 30000 for 30 seconds)
     *      Default: value of JOBN_PAUSE_AFTER
     *      Default for JOBN_PAUSE_AFTER: 0<br>
     * Parameter: JOBi_JUMP_NIO where i is the job-index (optional)
     *      Meaning: JOBi_JUMP_NIO specifies where to jump (next job-index) / what to do (exit io / nio) when the condition status for job i is NIO.
     *      Values: a job-index for specifying the next job to be executed OR EXIT_IO OR EXIT_NIO
     *      Default: value of JOBN_JUMP_NIO if defined, else: EXIT_NIO (no timeout) / first job-index (with timeout)<br>
     * Parameter: JOBi_JUMP_NIOSYS where i is the job-index (optional)
     *      Meaning: JOBi_JUMP_NIOSYS specifies where to jump (next job-index) / what to do (exit io / nio) when the status for job i is NIOSYS.
     *      Values: a job-index for specifying the next job to be executed OR EXIT_IO OR EXIT_NIO
     *      Default: value of JOBN_JUMP_NIOSYS if defined, else JOBN_JUMP_NIO<br>
     * Parameter: JOBi_JUMP_IO where i is the job-index (optional)
     *      Meaning: JOBi_JUMP_IO specifies where to jump (next job-index) / what to do (exit io / nio) when the condition staus for job i is IO.
     *      Values: a job-index for specifying the next job to be executed OR EXIT_IO OR EXIT_NIO
     *      Default: value of JOBN_JUMP_IO if defined, else: EXIT_IO (job i is the last job) / i+1 = next job-index (job i is NOT the last job)<br>
     * Parameter: JOBi_QWT_NIO where i is the job-index (optinal)
     *      Meaning: JOBi_QWT_NIO specifies the message that has to be confirem, when the condition status for job i is NIO and no specific JOBi_QWT_NIOy is present.
     *      Values: a text or an expression
     *      Default: value of JOBN_QWT_NIO
     *      Default for JOBN_QWT_NIO: not defined<br>
     * Parameter: JOBi_QWT_NIOSYS where i is the job-index (optinal)
     *      Meaning: JOBi_QWT_NIOSYS specifies the message that has to be confirem, when the status for job i is NIOSYS.
     *      Values: a text or an expression
     *      Default: value of JOBN_QWT_NIOSYS when defined, else value of JOBi_QWT_NIO
     * Parameter: JOBi_IWT where i is the job-index (optinal)
     *      Meaning: JOBi_IWT specifies the message that is displayed during and after the job i is executed.
     *          Note: When no IWT is specified for the next job(s) the message will still be displayed.
     *      Values: a text or an expression
     *      Default: value of JOBN_IWT
     *      Default for JOBN_IWT: not defined<br>
     * Parameter: JOBi_BREAKPOINT where i is the job-index (optinal)
     *      Meaning: If JOBi_BREAKPOINT set, execution is breaked after the job.
     *               It tries to connect with the test program on socket 7575. 
     *               If this is successfull, it sends the text of this parameter through the connection. 
     *               When the test program sends at least one byte back, the program continues. 
     *               If it is not possible to connect to socket 7575, a MessageBox is shown instead.
     *      Values: a text
     *      Default: value of JOBN_IWT
     *      Default for JOBN_IWT: null<br>
     * @param info Information zur Ausf�hrung
     */
    @Override
	public void execute( ExecutionInfo info ) {
        // immer notwendig
        Ergebnis result;
        Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
        
        status = STATUS_EXECUTION_OK;
        //boolean udInUse = false;
        boolean timeoutOccurred = false;        //true when a timeout occurred
        //char myDialogStyle = '0';  //1=requestStatusMessage, 2=requestAlertMessage, sonst=requestUserMessage
        //int previousJob = -1;           //job in the previous loop
        String currentIwt = null;
        String qwt_timeout = null;
        String qwt = null;
        boolean timing = false;
        boolean disableUserDialog = false; //true, when any UserDialog usage inside the PP should be disabled (e.q. parallel block also uses the UserDialog)
        
        // Method variables
        String value;  //for storing a pair key/value
        //loop variables
        int i;
        if (timing) System.out.println("TIMING: before checkArgs(): "+System.currentTimeMillis());
        
        try {
            // Parameter check
            //----------------
            if( !checkArgs(ergListe) ) {
                // Pass a new exception upwards (PPExecutionException -> error in parameters!)
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                if (isDE()) throw new PPExecutionException( "Parametrierfehler" );
                else throw new PPExecutionException( "parameter error" );
            }
            
            // Parameter auswerten/holen
            if (timing) System.out.println("TIMING: before extract args(): "+System.currentTimeMillis());
            try {
                //create a list with all args that have not been handled so far
                Set<Object> myargs = new HashSet<Object>();             //Set for keeping all keys that have not been handled so far
                myargs.addAll(getArgs().keySet());      //Add all keys to the set
                //DEBUG
                if (getArg("DEBUG") != null) {
                    myargs.remove("DEBUG");             //DEBUG has been handled
                }
                value = getArg("DEBUG");
                if ((value == null) || (value.length() == 0)) {
                    debug = 0;
                } else if ("TRUE".equalsIgnoreCase(value)) {
                    debug = 2;
                } else if ("FALSE".equalsIgnoreCase(value)) {
                    debug = 0;
                } else {
                    try {
                        debug = Integer.parseInt(value);
                        if (debug < 0) debug = 0;
                    } catch (NumberFormatException nfe) {
                        System.out.println("DiagData.execute: DEBUG invalid");
                        if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler DEBUG", Ergebnis.FT_NIO_SYS );
                        else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error DEBUG", Ergebnis.FT_NIO_SYS );
                        ergListe.add(result);
                        if (isDE()) throw new PPExecutionException( "Parametrierfehler" );
                        else throw new PPExecutionException( "parameter error" );
                    }
                }
                if (debug > 0) System.out.println("DiagData.execute:  DEBUG enabled! Level:"+debug);
                if (debug > 0) System.out.println("DiagData.execute: Start " + this.getName());
                //required Args
                //=====================================================
                
                //optional Args (may also be required depending on OP)
                //=====================================================
                //STORAGE
                if (getArg("STORAGE") != null) {
                    Storage = getArg("STORAGE");
                    myargs.remove("STORAGE");
                    if (debug > 0) System.out.println("DiagData.execute: STORAGE '"+Storage+"'");
                } else {
                    Storage = null;     //default use current PL
                }
                //TIMEOUT: Timeout for exiting IO
                value = getArg("TIMEOUT");
                if (value != null) {            //TIMEOUT is present
                    try {
                        timeout = Long.parseLong(value);
                    } catch (NumberFormatException nfe) {
                        if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler TIMEOUT", Ergebnis.FT_NIO_SYS );
                        else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error TIMEOUT", Ergebnis.FT_NIO_SYS );
                        ergListe.add(result);
                        if (isDE()) throw new PPExecutionException("DiagData.checkArgs: TIMEOUT '"+value+"' ist ung�ltig");
                        else throw new PPExecutionException("DiagData.checkArgs: TIMEOUT '"+value+"' is invalid");
                    }
                    myargs.remove("TIMEOUT");           //Remove Timeout
                    if (debug > 0) System.out.println("DiagData.execute: TIMEOUT '"+timeout+"'");
                } else {
                    timeout = 0;                    //default no timeout
                }
                //QWT_TIMEOUT
                value = getArg("QWT_TIMEOUT");
                if (value != null) {
                    //*QWT_TIMEOUT is present
                    myargs.remove("QWT_TIMEOUT");           //remove the label from the list of all remaining args
                    if (debug > 0) System.out.println("DiagData.execute: QWT_TIMEOUT '"+value+"'");
                    if (value.trim().length() == 0) {
                        //QWT_NIOl is empty
                        if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler QWT_TIMEOUT", Ergebnis.FT_NIO_SYS );
                        else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error QWT_TIMEOUT", Ergebnis.FT_NIO_SYS );
                        ergListe.add(result);
                        if (isDE()) throw new PPExecutionException("DiagData.extractArgsForOneJob: QWT_TIMEOUT ist leer");
                        else throw new PPExecutionException("DiagData.extractArgsForOneJob: QWT_TIMEOUT is empty");
                    }
                    qwt_timeout = value;
                }
                //IWT: Infotext is DISPLAYED while multiple JOBS are executed or a PAUSE is present
                value = getArg("IWT");
                if (value != null) {
                    iwt = value;
                    myargs.remove("IWT");           //Remove Iwt
                } else {
                    iwt = null;                     //no Infotext
                }
                //DISABLE_USERDIALOG: UserDialog will be not used, as soon as it is true
                value = getArg("DISABLE_USERDIALOG");
                if ( getArg("DISABLE_USERDIALOG") != null ) {
                	try {
                		disableUserDialog = "TRUE".equalsIgnoreCase( extractValues ( getArg("DISABLE_USERDIALOG") )[0]);
                	} catch (Exception e) {
                        if (isDE()) throw new PPExecutionException("DiagData.checkArgs: DISABLE_USERDIALOG '"+value+"' ist ung�ltig");
                        else throw new PPExecutionException("DiagData.checkArgs: DISABLE_USERDIALOG '"+value+"' is invalid");
                	}
                	myargs.remove("DISABLE_USERDIALOG"); //DISABLE_USERDIALOG
                }
                //Jobs and Job-Arguments
                this.extractArgsForAllsJobs(myargs,true,ergListe);
                
                //Replace IWT by first�s JOB JOBN_IWT
                if (((JobExecution)jobs.get(this.firstJobIndex)).getIwt() != null) {
                    //the first job already has its own IWT, so do nothing
                } else {
                    if (iwt != null) {
                        ((JobExecution)jobs.get(this.firstJobIndex)).setIwt(iwt);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace(System.out);
                if (debug > 0) System.out.println("DiagData.execute: Error during extracting arguments");
                if (isDE()) result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parameterexistenz" ), ""+ ((e.getMessage() == null) ? "" : e.getMessage()), Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ParaError", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parameterexistenz" ), ""+ ((e.getMessage() == null) ? "" : e.getMessage()), Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                if (isDE()) throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
                else throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
            }
            
            //execution try / catch
            if (timing) System.out.println("TIMING: before execute(): "+System.currentTimeMillis());
            try {
            	checkParallelDiagnosis();
            	
                if (debug > 2) this.printJobs();
                
                //if a timeout is added the loop should start here
                ergListe = new Vector<Ergebnis>();
                
                //loop over all jobs: execute and apply the conditions (calculate wether a condition is true or false)
                int currentJob = this.firstJobIndex;
                int nextJob = currentJob;
                boolean timeoutPresent;			//true when a timeout is present
                if (timeout > 0) timeoutPresent = true;
                else timeoutPresent = false;
                long endtime = System.currentTimeMillis() + timeout;
                
                //will a statusdialog be displayed
                if ((lastJobIndex - firstJobIndex) > 1) displayStatusDialog = true;
                
                //Setup result lists for each job
                Vector<Ergebnis> ergLists[] = new Vector[this.lastJobIndex+1];
                for (i = this.firstJobIndex; i<=this.lastJobIndex;i++) {
                    ergLists[i] = new Vector<Ergebnis>();
                }
                
                //enter loop, loop until nextJob is no longer between firstJobIndex and lastJobIndex or time is expired
                while ((this.firstJobIndex <= nextJob) && (nextJob <= this.lastJobIndex) &&
                (!timeoutOccurred)) {
                    if (timeoutPresent && (System.currentTimeMillis() > endtime)) {
                        //timeout
                        timeoutOccurred = true;
                        continue;
                    }
                    currentJob = nextJob;
                    nextJob = DEF_JUMP_UNDEF;  //we do not know this yet
                    ergLists[currentJob] = new Vector<Ergebnis>();                        //clear erglist
                    //Is a new Iwt available for current Job?
                    String tempIwt = ((JobExecution)jobs.get(currentJob)).getIwt();
                    if (tempIwt != null) {
                        //set current iwt
                        currentIwt = tempIwt;
                        //if (currentIwt == null) currentIwt = iwt;
                        //if (currentIwt == null) currentIwt = "";
                        //make Substitutions in IWT
                        if (debug > 1) System.out.println("DiagData.execute: displayMessage before substitutions: '"+currentIwt+"'");
                        currentIwt = ((JobExecution)jobs.get(currentJob)).resolveSubstLinksExpr(currentIwt);
                        if (debug > 1) System.out.println("DiagData.execute: displayMessage after substitutions: '"+currentIwt+"'");
                    }
                    //change displayed message
                    if (!disableUserDialog && (displayStatusDialog || currentIwt != null)) {
                        displayStatusDialog(currentIwt, currentJob, ergLists[currentJob]);
                    }
                    //execute the job
                    int s = executeOneJob(currentJob,ergLists[currentJob]);
                    if (debug > 1) System.out.println("DiagData.execute: Job"+currentJob+" OverallConditionState: "+s);
                    //check for QWT: if present the qwt is put into the vaiable qwt
                    qwt = null;
                    if (s == JobExecution.STATE_IO) {   //Execution IO
                        //execution IO no QWT
                    } else if (s == JobExecution.STATE_NIO) {   //Execution NIO
                        //QWT_NIO
                        qwt = ((JobExecution)jobs.get(currentJob)).getQwtNioDefault();
                        if (debug > 1) System.out.println("DiagData.execute: QWT_NIO is '"+qwt+"'");
                    } else if (s == JobExecution.STATE_NIOSYS) {    //Execution NIOSYS
                        //QWT_NIO_SYS
                        qwt = ((JobExecution)jobs.get(currentJob)).getQwtNioSys();
                        if (debug > 1) System.out.println("DiagData.execute: QWT_NIOSYS is '"+qwt+"'");
                        if (qwt == null) {
                            //use nio qwt as default for niosys
                            qwt = ((JobExecution)jobs.get(currentJob)).getQwtNioDefault();
                            if (debug > 1) System.out.println("DiagData.execute: QWT_NIOSYS is null using QWT_NIO '"+qwt+"'");
                        }
                    } else if ((1 <= s) && (s <= ((JobExecution)jobs.get(currentJob)).getNumberOfConditions())) {                                        //Execution NIO with Condition s failed
                        //QWT_NIO1 ... QWT_NIOn
                        qwt = ((JobExecution)jobs.get(currentJob)).getQwtNio(s);
                        if (debug > 1) System.out.println("DiagData.execute: QWT_NIO"+s+" is '"+qwt+"'");
                        if (qwt == null) {
                            //use nio qwt as default
                            qwt = ((JobExecution)jobs.get(currentJob)).getQwtNioDefault();
                            if (debug > 1) System.out.println("DiagData.execute: QWT_NIO"+s+" is null using QWT_NIO '"+qwt+"'");
                        }
                    } else {
                        //internal error
                        if (debug > 1) System.out.println("DiagData.execute: Invalid execution status '"+s+"'");
                        if (isDE()) result = new Ergebnis( "ExecFehler", "Intern", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler" ), "Interner Fehler Ausf�hrungsstatus ung�ltig", Ergebnis.FT_NIO_SYS );
                        else result = new Ergebnis( "ExecError", "Internal", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler" ), "Internal Error execution status invalid", Ergebnis.FT_NIO_SYS );
                        ergLists[currentJob].add(result);
                        nextJob=DEF_JUMP_EXIT_NIO;         //exit loop
                        qwt = null;
                        s = JobExecution.STATE_UNKNOWN;
                        break;
                    }
                    //make substitutions in QWT
                    if (qwt != null) {
                        qwt=((JobExecution)jobs.get(currentJob)).resolveSubstLinksExpr(qwt);
                    }
                    //display QWT if present and exit
                    if (qwt != null) {
                        //we have a qwt
                        if (!disableUserDialog) {
                        	requestDialog(qwt, ergLists[currentJob]);
                        }
                        //look at JUMP_NIO for current condition, when JUMP_EXITIO use it
                        nextJob = ((JobExecution)jobs.get(currentJob)).getJumpNio(s);
                        if (nextJob == DEF_JUMP_UNDEF) {
                            //We have no specific JUMP_NIO for current condition, so check default
                            nextJob = ((JobExecution)jobs.get(currentJob)).getJumpNioDefault();
                        }
                        if (nextJob != DEF_JUMP_UNDEF) {
                            if (nextJob != DEF_JUMP_EXIT_IO) {
                                //by default exit NIO
                                nextJob = DEF_JUMP_EXIT_NIO;
                                status = STATUS_EXECUTION_ERROR;
                            }
                        } else {
                            //by default exit NIO
                            nextJob = DEF_JUMP_EXIT_NIO;
                            status = STATUS_EXECUTION_ERROR;
                        }
                        break;                                  //exit main-loop
                    }
                    
                    //handle Jumps
                    if (s == JobExecution.STATE_IO) {           //Execution IO
                        //set next job
                        nextJob = ((JobExecution)jobs.get(currentJob)).getJumpIo();
                        if (debug > 1) System.out.println("DiagData.execute: JUMP_IO is '"+nextJob+"'");
                    } else if (s == JobExecution.STATE_NIO) {   //Execution NIO
                        //set next job
                        nextJob = ((JobExecution)jobs.get(currentJob)).getJumpNioDefault();
                        if (debug > 1) System.out.println("DiagData.execute: JUMP_NIO is '"+nextJob+"'");
                    } else if (s == JobExecution.STATE_NIOSYS) {    //Execution NIOSYS
                        //set next job
                        nextJob = ((JobExecution)jobs.get(currentJob)).getJumpNioSys();
                        if (debug > 1) System.out.println("DiagData.execute: JUMP_NIOSYS is '"+nextJob+"'");
                        if (nextJob == DEF_JUMP_UNDEF) {
                            //use nio jump as default for niosys
                            nextJob = ((JobExecution)jobs.get(currentJob)).getJumpNioDefault();
                            if (debug > 1) System.out.println("DiagData.execute: JUMP_NIOSYS is not defined using JUMP_NIO '"+nextJob+"'");
                        }
                    } else if ((1 <= s) && (s <= ((JobExecution)jobs.get(currentJob)).getNumberOfConditions())) {                                        //Execution NIO with Condition s failed
                        //set next job
                        nextJob = ((JobExecution)jobs.get(currentJob)).getJumpNio(s);
                        if (debug > 1) System.out.println("DiagData.execute: JUMP_NIO"+s+" is '"+nextJob+"'");
                        if (nextJob == DEF_JUMP_UNDEF) {
                            //use nio jump as default
                            nextJob = ((JobExecution)jobs.get(currentJob)).getJumpNioDefault();
                            if (debug > 1) System.out.println("DiagData.execute: JUMP_NIO"+s+" is null using JUMP_NIO '"+nextJob+"'");
                        }
                    } else {
                        //internal error
                        if (debug > 1) System.out.println("DiagData.execute: Invalid execution status '"+s+"'");
                        if (isDE()) result = new Ergebnis( "ExecFehler", "Intern", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler" ), "Interner Fehler Ausf�hrungsstatus ung�ltig", Ergebnis.FT_NIO_SYS );
                        else result = new Ergebnis( "ExecError", "Internal", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler" ), "Internal Error execution status invalid", Ergebnis.FT_NIO_SYS );
                        ergLists[currentJob].add(result);
                        nextJob=DEF_JUMP_EXIT_NIO;         //exit loop
                        qwt = null;
                        s = JobExecution.STATE_UNKNOWN;
                        break;
                    }
                    
                    //check for canceled
                    if( (myDialog != null) && (myDialog.isCancelled() == true) ) {
                        if (debug > 1) System.out.println("DiagData.execute: Dialog canceled");
                        if (isDE()) result = new Ergebnis( "Frage", "Userdialog", "", "", "", "", "", "", "", "0", currentIwt, PB.getString( "werkerAbbruch" ), "", "", "", Ergebnis.FT_NIO );
                        else result = new Ergebnis( "Question", "Userdialog", "", "", "", "", "", "", "", "0", currentIwt, PB.getString( "werkerAbbruch" ), "", "", "", Ergebnis.FT_NIO );
                        ergLists[currentJob].add(result);
                        nextJob=DEF_JUMP_EXIT_NIO;
                        status = STATUS_EXECUTION_ERROR;
                    }
                    //when jumping back wait at 150 ms by default
                    if ((nextJob <= currentJob) && (nextJob >= firstJobIndex)
                    && (((JobExecution)jobs.get(currentJob)).getPauseAfter() <= 0)) {
                        waitms(150);
                    }
                    
                    // When not looping over the same job, handle breakpoint
                    if (nextJob!=currentJob)
                    {
                        String tempBreakpoint = ((JobExecution)jobs.get(currentJob)).getBreakpoint();
                        if (tempBreakpoint != null) {
                            if (debug > 1) System.out.println("DiagData.execute: Breakpoint reached");
                         
                            
                            if (!remoteBreakpoint(tempBreakpoint))
                            {
                                if (debug > 1) System.out.println("DiagData.execute: No remote test program found ");
                                
                                if (!disableUserDialog) {
                                	requestDialog(tempBreakpoint, null);
                                }
                            }
                            
                        }
                    }
                    
                    //when jumping back set displayStatusDialog to true
                    if ((nextJob <= currentJob) && (nextJob >= firstJobIndex)) displayStatusDialog=true;
                }   //End of execution loop
                if (timing) System.out.println("TIMING: after execution-loop: "+System.currentTimeMillis());
                
                //Extract results from the result lists
                for (i = this.firstJobIndex; i<=this.lastJobIndex;i++) {
                    ergListe.addAll(ergLists[i]);                               //put all result lists together again
                }
                if (timing) System.out.println("TIMING: after building-results: "+System.currentTimeMillis());
                
                
                //set current executionstatus
                if (nextJob != DEF_JUMP_EXIT_IO) {
                    status = STATUS_EXECUTION_ERROR;
                    //check for timeout
                    if ((timeout > 0) && timeoutOccurred) {
                        if (debug > 1) System.out.println("DiagData.execute: Timeout occured");
                        String thwt = jobTemplate.getDefaultHwt();
                        if (thwt == null) {
                            if (isDE()) thwt = "Zeit�berschreitung";
                            else thwt = "Execution-Timeout";
                        }
                        else {
                            //resolve substitutions and links
                            thwt = ((JobExecution)jobs.get(firstJobIndex)).resolveSubstLinksExpr(thwt);
                        }
                        String tawt = qwt_timeout;
                        if (tawt == null) {
                            if (isDE()) tawt = "Zeit�berschreitung";
                            else tawt = "Execution-Timeout";
                        }
                        if (isDE()) result = new Ergebnis( "ExecFehler", "Timeout", "", "", "", "", "", "", "", "0", "", "", tawt, thwt, "Zeit�berschreitung", Ergebnis.FT_NIO );
                        else result = new Ergebnis( "ExecError", "Timeout", "", "", "", "", "", "", "", "0", "", "", tawt, thwt, "Execution-Timeout", Ergebnis.FT_NIO );
                        ergListe.add(result);
                        
                        //display QWT_TIMEOUT if present and exit
                        if (qwt_timeout != null && !disableUserDialog) {
                            //we have a qwt
                       		requestDialog(qwt_timeout, ergListe);
                        } else {
                            if (debug > 1) System.out.println("DiagData.execute: Timeout without QWT_TIMEOUT");
                        }
                    }
                }
                
                if (timing) System.out.println("TIMING: before exporting attributes: "+System.currentTimeMillis());
                
                if (debug > 1) System.out.println("DiagData.execute: status before export = " + status);
                if (debug > 1) System.out.println("DiagData.execute: qwt before export = " + qwt);
                //if io and we have no qwt, export results to storagenames using storageformats
                if ((status == STATUS_EXECUTION_OK) && (qwt == null)) {
                    if (debug > 1) System.out.println("DiagData.execute: Exporting dynamic attributes");
                    for (i = this.firstJobIndex; i <= this.lastJobIndex;i++) {
                        ((JobExecution)jobs.get(i)).exportDynamicAttributes();
                    }
                }
                if (timing) System.out.println("TIMING: after exporting attributes: "+System.currentTimeMillis());
                //dummy for being able to compile without statement throwing an exception
                //if (this.firstJobIndex == 200) throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
            } catch (PPExecutionException ppe) {
                if (debug > 0) System.out.println("DiagData.execute: Error during execution");
                throw ppe;
            }
            
        } catch (PPExecutionException ppee) {
            if (isDE()) result = new Ergebnis( "ExecFehler", "Execute", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler" ), "PPExecutionException: "+((ppee.getMessage() == null) ? "keine Meldung" : ppee.getMessage()), Ergebnis.FT_NIO_SYS );
            else result = new Ergebnis( "ExecError", "Execute", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler" ), "PPExecutionException: "+((ppee.getMessage() == null) ? "no message" : ppee.getMessage()), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
            ppee.printStackTrace();
        } catch (Exception ex) {
            if (isDE()) result = new Ergebnis( "ExecFehler", "Execute", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler" ), "Exception: "+((ex.getMessage() == null) ? "keine Meldung" : ex.getMessage()), Ergebnis.FT_NIO_SYS );
            else result = new Ergebnis( "ExecError", "Execute", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler" ), "Exception: "+((ex.getMessage() == null) ? "no message" : ex.getMessage()), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
            ex.printStackTrace();
        } catch (Throwable t) {
            if (isDE()) result = new Ergebnis( "ExecFehler", "Execute", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler" ), "Throwable: "+((t.getMessage() == null) ? "keine Meldung" : t.getMessage()), Ergebnis.FT_NIO_SYS );
            else result = new Ergebnis( "ExecError", "Execute", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler" ), "Throwable: "+((t.getMessage() == null) ? "no message" : t.getMessage()), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
            t.printStackTrace();
        }
        
        //create a IO-Ergebnis
        if (status == STATUS_EXECUTION_OK) {
            if (isDE()) result = new Ergebnis( "Execution", "Diagdata", "", "", "", "", "", "", "", "0", "", "", "", "", "Ausf�hrung IO", Ergebnis.FT_IO );
            else result = new Ergebnis( "Execution", "Diagdata", "", "", "", "", "", "", "", "0", "", "", "", "", "Execution IO", Ergebnis.FT_IO );
            ergListe.add(result);
        } else {
            //we have a NIO-Status, so check if we have a NIO-Result
            if (!containsError(ergListe)) {
                //produce missing NIO-Result
                if (isDE()) result = new Ergebnis( "Execution", "Diagdata", "", "", "", "", "", "", "", "0", "", "", "Unbekannter Fehler", "", "Execution NIO", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "Execution", "Diagdata", "", "", "", "", "", "", "", "0", "", "", "Unknown Error", "", "Execution NIO", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
            }
        }
        
        //Release UserDialog
        releaseDialog(ergListe);
        
        //filter all NIO-Results and set them to IGNORE, if the status is IO
        if (status == STATUS_EXECUTION_OK) {
            try {
                filterErrors(ergListe);
            } catch (Exception e) {
                if (debug > 0) System.out.println("DiagData.execute: filterErrors failed");
            }
        }
        
        //Status setzen
        if (debug > 2) System.out.println("DiagData.execute: "+ergListe.size()+" result(s) during execution ");
        if (debug > 2) printResults(ergListe);
        if (debug > 0) System.out.println("DiagData.execute: status = '"+status+"'");
        if (debug > 0) System.out.println("DiagData.execute: End");
        cleanup();
        setPPStatus( info, status, ergListe );
    }
  
    /**
     * Check if parallel diagnosis is configured
     */
    private void checkParallelDiagnosis ( ) {
    	// Pr�fstandvariabel auslesen, ob Paralleldiagnose
		try 
		{
			Object pr_var;

			pr_var=getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.TEMPORARY, "DIAG_PARALLEL");
			if (pr_var!=null) 
			{
				if (pr_var instanceof String) 
				{
					if ("TRUE".equalsIgnoreCase((String)pr_var))
					{
						parallel=true;
					}
				}
				else if (pr_var instanceof Boolean)
				{
					if (((Boolean)pr_var).booleanValue())
					{
						parallel=true;
					}
				}
			}
		}
		catch (VariablesException e) 
		{
			// Nothing! Die Variable 'parallel' ist in diesem Fall halt per default false
		}
    	
    }
    
    /**
     * Get Ediabas
     * @param ergListe, ergListe
     * @param sgbd, sgbd
     * @throws PPExecutionException
     */
    private void getEdiabas( Vector<Ergebnis> ergListe, String sgbd ) throws PPExecutionException {
    	Ergebnis result;
    	
		// Devicemanager
		devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();

		if (getArg("TAG")!= null) tag = getArg("TAG");

		if (parallel) // Ediabas holen
		{
			try
			{
				ediabas = devMan.getEdiabasParallel(tag, sgbd);
			}
			catch (Exception ex)
			{
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Wrong Cascade configuration for parallel diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add(result);
				status = STATUS_EXECUTION_ERROR;
				throw new PPExecutionException();
			}
			catch (Throwable ex)
			{
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add(result);
				status = STATUS_EXECUTION_ERROR;
				throw new PPExecutionException();
			}
		}
		else
		{
			try{
				ediabas=devMan.getEdiabas(tag);
			}
			catch (Exception ex)
			{
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
				ergListe.add(result);
				status = STATUS_EXECUTION_ERROR;
				throw new PPExecutionException();

			}
		}    	
    }
    
    /**
     * Release Ediabas
     * @param ergListe, ergListe
     * @param sgbd, sgbd
     * @throws PPExecutionException
     */
    private void releaseEdiabasParallel( Vector<Ergebnis> ergListe, String sgbd ) throws PPExecutionException {
    	Ergebnis result;
    	
		// Paralleldiagnose
		if (parallel )
		{
			try{
				devMan.releaseEdiabasParallel(ediabas, tag, sgbd);
			}
			catch(Exception ex){
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add(result);
				status = STATUS_EXECUTION_ERROR;
			} catch (Throwable t) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add(result);
				status = STATUS_EXECUTION_ERROR;
			}
		}
    }
    
    /**
     * execute the one job and apply conditions
     * @param i Index of the Job
     * @param ergs Ergebnis-Liste (wird bei Fehlern und anderen Ergebnissen bef�llt)
     */
    private int executeOneJob(int i, Vector<Ergebnis> ergs) throws PPExecutionException {
        int status=JobExecution.STATE_IO;
        long before=0,after=0;

        Ergebnis result;
        
        try {
            //pause before
            before = ((JobExecution)jobs.get(i)).getPauseBefore();
            if (before < 0)  {
                before = 0;         //no pause
            }
            if (before > 0) {
                waitms(before);
            }
            
            //execute the SGBD-Job
            try {
                ((JobExecution)jobs.get(i)).execute(ergs);
                if (debug > 1) System.out.println("DiagData.executeOneJob: Execution of Job "+i+" IO");
            } catch (PPExecutionException ppe) {
                if (debug > 1) System.out.println("DiagData.executeOneJob: Execution of Job "+i+" failed");
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXECUTE", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler"), "Fehler: " + ppe.getMessage(), Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXECUTE", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler"), "Exception: " + ppe.getMessage(), Ergebnis.FT_NIO_SYS );
                ergs.add(result);
                status = JobExecution.STATE_NIOSYS;
            }
            if (debug > 2) this.printJobs();
            
            //pause after (also if NIOSYS)
            after = ((JobExecution)jobs.get(i)).getPauseAfter();
            if (after < 0) {
                after = 0;         //no pause
            }
            if (after > 0) {
                waitms(after);
            }
            
            //exit if an error occured before
            if (status != JobExecution.STATE_IO) return JobExecution.STATE_NIOSYS;
            
            //Set Substitutions for all other Jobs
            int j;              //for loop over all results
            int k;
            String pattern,value; //for keeping label and resultvalue
            for (j=1;j<=((JobExecution)jobs.get(i)).getNumberOfResults();j++) {
                pattern = "%J"+i+"R"+j+"%";
                value = ((JobExecution)jobs.get(i)).getResultValue(j);
                //loop over all jobs and set the substitution
                for (k = this.firstJobIndex; k <= this.lastJobIndex;k++) {
                    ((JobExecution)jobs.get(k)).setSubstitution(pattern, value);            //define %JiRj% as value
                }
            }
            
            //apply the conditions (calculate if a condition is true or false)
            try {
                status = ((JobExecution)jobs.get(i)).applyConditions(ergs);
                if (debug > 1) System.out.println("DiagData.executeOneJob: applying Conditions of Job "+i+" Status is "+status);
            } catch (PPExecutionException ppe) {
                if (debug > 1) System.out.println("DiagData.executeOneJob: applying Conditions of Job "+i+" failed");
                return JobExecution.STATE_NIOSYS;
            }
        } catch (Exception e) {
            if (debug>0) System.out.println("DiagData.executeOneJob: Unexpected Exception during Execution of Job "+i+" Message is: "+e.getMessage());
            if (debug>0) e.printStackTrace(System.out);
            if (isDE()) result = new Ergebnis( "ExecFehler", "EXECUTE", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler"), "Fehler: " + e.getMessage(), Ergebnis.FT_NIO_SYS );
            else result = new Ergebnis( "ExecError", "EXECUTE", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler"), "Exception: " + e.getMessage(), Ergebnis.FT_NIO_SYS );
            ergs.add(result);
            if (isDE()) throw new PPExecutionException("DiagData.executeOneJob: Unerwarteter Fehler "+e.getMessage());
            else throw new PPExecutionException("DiagData.executeOneJob: Unexpected Exception "+e.getMessage());
        }
        return status;
    }
    
    //Methods for accessing dynamic attributes
    //*******************************************
    
    /**
     * Assigns a given value to a specified dynamic attribute
     * @param label the name of the dynamic attribute
     * @param value the value of the dynamic attribute
     */
    private void setDynamicAttribute(String label, String value) throws PPExecutionException {
        setDynamicAttribute(label, value, Storage);
    }
    
    /**
     * Assigns a given value to a specified dynamic attribute
     * @param label the name of the dynamic attribute
     * @param value the value of the dynamic attribute
     * @storage defines the PL that will be used for storing dynamic attributes
     */
    public void setDynamicAttribute(String label, String value, String storage) throws PPExecutionException {
        if (debug > 1) System.out.println("DiagData: setDynamicAttribute("+label+","+value+","+storage+")");
        Pruefling pl;   //reference to the storing PL
        if (storage != null) {
            pl = getPr�fling().getAuftrag().getPr�fling(storage);   //get the reference to the storing PL
        } else {
            pl = getPr�fling();   //get the reference to the storing PL (use current PL)
        }
        if (pl == null) {
            //throw an exeception if storing PL is null
            if (debug > 1) System.out.println("DiagData.setDynamicAttribute: Storage-PL not found");
            if (isDE()) throw new PPExecutionException("DiagData.setDynamicAttribute: Storage-PL nicht vorhanden");
            else throw new PPExecutionException("DiagData.setDynamicAttribute: Storage-PL not found");
        }
        //get the hashtable of the PL where the attributes are stored
        @SuppressWarnings("unchecked")
		Map<String, String> ht = pl.getAllAttributes();
        if (ht == null) {
            //thow an exception if ht is null
            if (debug > 1) System.out.println("DiagData.setDynamicAttribute: Hashtable not found");
            if (isDE()) throw new PPExecutionException("DiagData.setDynamicAttribute: Hashtable nicht vorhanden");
            else throw new PPExecutionException("DiagData.setDynamicAttribute: Hashtable not found");
        }
        ht.put(label, value);   //put the pair (label/value) to the hashtable
    }
    
    /**
     * Assigns an array of values to dynamic attributes with the name (labelPrefix + index)
     * @param labelPrefix the common prefix for the names of the dynamic attributes
     * @param valueArray the array of values
     * @param beginIndex the first index that will be used for getting the value from the array
     * @param numEntries the number of values that will be taken from the array
     */
    private void setDynamicAttributes(String labelPrefix, String[] valueArray, int beginIndex, int numEntries) throws PPExecutionException {
        setDynamicAttributes(labelPrefix, valueArray, beginIndex, numEntries, Storage);
    }
    
    /**
     * Assigns an array of values to dynamic attributes with the name (labelPrefix + index)
     * @param labelPrefix the common prefix for the names of the dynamic attributes
     * @param valueArray the array of values
     * @param beginIndex the first index that will be used for getting the value from the array
     * @param numEntries the number of values that will be taken from the array
     * @param storage defines the PP that will be used for storing dynamic attributes
     */
    public void setDynamicAttributes(String labelPrefix, String[] valueArray, int beginIndex, int numEntries, String storage) throws PPExecutionException {
        int i;  //counter for index
        //loop over all indices
        for (i=beginIndex;i<(beginIndex+numEntries);i++) {
            setDynamicAttribute(labelPrefix+i,valueArray[i],storage);
        }
    }
    
    /**
     * Returns the value of a specified dynamic attribute
     * @param label the name of the dynamic attribute
     * @return value of the dynamic attribute or null
     */
    private String getDynamicAttribute(String label) throws PPExecutionException {
        return getDynamicAttribute(label, Storage);
    }
    
    /**
     * Returns the value of a specified dynamic attribute
     * @param label the name of the dynamic attribute
     * @param storage defines the PL that will be used for storing dynamic attributes
     * @return value of the dynamic attribute or null
     */
    public String getDynamicAttribute(String label, String storage) throws PPExecutionException {
        if (debug > 1) System.out.println("DiagData: getDynamicAttribute("+label+","+storage+")");
        Pruefling pl;   //reference to the storing PL
        if (storage != null) {
            pl = getPr�fling().getAuftrag().getPr�fling(storage);   //get the reference to the storing PL
        } else {
            pl = getPr�fling();   //get the reference to the storing PL (use current PL)
        }
        if (pl == null) {
            //throw an exeception if storing PL is null
            if (debug > 1) System.out.println("DiagData.getDynamicAttribute: Storage-PL not found");
            if (isDE()) throw new PPExecutionException("DiagData.getDynamicAttribute: Storage-PL nicht vorhanden");
            else throw new PPExecutionException("DiagData.getDynamicAttribute: Storage-PL not found");
        }
        try {
            //get the hashtable of the pp where the arguments are stored
            Map ht = pl.getAllAttributes();
            if (ht == null) {
                //thow an exception if ht is null
                if (debug > 1) System.out.println("DiagData.getDynamicAttribute: Hashtable not found");
                if (isDE()) throw new PPExecutionException("DiagData.getDynamicAttribute: Hashtable nicht vorhanden");
                else throw new PPExecutionException("DiagData.getDynamicAttribute: Hashtable not found");
            }
            Object temp = ht.get(label);    //get the value assigned to label (in the hashtable)
            if (debug > 1) System.out.println("DiagData.getDynamicAttribute: Value="+temp);
            if (temp == null) return null;  //return null if value is null
            else return (String)temp;       //return value with typecast to String
        } catch (PPExecutionException ppe) {
            if (isDE()) throw new PPExecutionException("DiagData.getDynamicAttribute: Unbekannter PP-Fehler " + ppe.getMessage());
            else throw new PPExecutionException("DiagData.getDynamicAttribute: Unknown PP-Exception " + ppe.getMessage());
        } catch (Exception e) {
            //throw an exception if something unknown/unexpected went wrong
            e.printStackTrace();
            if (isDE()) throw new PPExecutionException("DiagData.getDynamicAttribute: Unbekannter Fehler " + e.getMessage());
            else throw new PPExecutionException("DiagData.getDynamicAttribute: Unknown Exception " + e.getMessage());
        }
    }
    
    /**
     * Assigns dynamic attributes with the name (labelPrefix + index) to the given array
     * @param labelPrefix the common prefix for the names of the dynamic attributes
     * @param valueArray the array of values
     * @param beginIndex the first index that will be used for getting the value from the array
     * @param numEntries the number of values that will be taken from the array
     */
    private void getDynamicAttributes(String labelPrefix, String[] valueArray, int beginIndex, int numEntries) throws PPExecutionException {
        getDynamicAttributes(labelPrefix, valueArray, beginIndex, numEntries, Storage);
    }
    
    /**
     * Assigns dynamic attributes with the name (labelPrefix + index) to the given array
     * @param labelPrefix the common prefix for the names of the dynamic attributes
     * @param valueArray the array of values
     * @param beginIndex the first index that will be used for getting the value from the array
     * @param numEntries the number of values that will be taken from the array
     * @param storage defines the PP that will be used for storing dynamic attributes
     */
    public void getDynamicAttributes(String labelPrefix, String[] valueArray, int beginIndex, int numEntries, String storage) throws PPExecutionException {
        int i;  //counter for index
        //loop over all indices
        for (i=beginIndex;i<(beginIndex+numEntries);i++) {
            valueArray[i] = getDynamicAttribute(labelPrefix+i, storage);
        }
    }
    
    /**
     * extract all args for all jobs.
     * The number of Jobs (min/max index) is detected.
     * If given LOOP_UNTIL and LOOP_FROM are extracted and resolved.
     * Other Links and %X% are not resolved.
     * If myargs is given, it is checked that all args in myargs have been consumed at the end!
     * NOTE: When this method is called myargs must only contain Arguments beginning with JOBN_ or JOBn (n=numeric)
     * @param myargs Set of args that have not already be handled.  If a arg is extracted it is removed from the set.
     * @param resolveLoopFromUntil If true LOOP_FROM and LOOP_UNTIL are resolved. (for checkArgs=false, for execute=true)
     * @param errors When an error occurs the reason may be put in this vector as a Ergebnis, before throwing an exception.
     * No IO-Results will be added.
     */
    private void extractArgsForAllsJobs(Set<Object> myargs, boolean resolveLoopFromUntil, Vector<Ergebnis> errors) throws PPExecutionException {
        //First extracts LOOP_UNTIL and LOOP_FROM when given
        //Loop over all remaining Args and extract the Job-Template: everything starting with JOBN_
        //Loop over all remaining Args and extract the Job-parameters. During this correct firstJobIndex and lastJobIndex.
        //Loop over all JobExecutions and set default substitutions and jump destinations
        //Is every Jump within firstJobIndex and lastJobIndex???
        //loop over all conditions of the current job: Set the HWT to QWT_NIO when not present. Check jumps.
        //Call JobExecution.checkArgs for each JobExecution
        //are there any unhandled arguments (label does not start with JOBn_ or JOBN_ or LOOP_UNTIL or LOOP_FROM)
        
        //local variables
        JobExecution currentJobExecution = null;
        String value = null;
        String label = null;
        Ergebnis result;
        int idx,n;
        
        if (debug > 2) {
            System.out.println("DiagData.extractArgsForAllJobs.myargs conatains");
            Iterator<Object> dIter = myargs.iterator();
            while (dIter.hasNext()) System.out.println("DiagData.extractArgsForAllJobs.myargs conatains: " + (value=(String)dIter.next()) + " = " + getArg(value));
        }
        
        //fetch LOOP_FROM
        label = "LOOP_FROM";
        value = getArg(label);
        if (debug > 1) System.out.println("DiagData.extractArgsForAllJobs:"+label+" = '"+value+"'");
        if (value != null) {
            //LOOP_FROM is present
            myargs.remove(label);           //remove the label from the list of all remaining args
            if (debug > 2) {
                System.out.println("DiagData.extractArgsForAllJobs.myargs conatains after LOOP_FROM");
                Iterator<Object> dIter = myargs.iterator();
                while (dIter.hasNext()) System.out.println("DiagData.extractArgsForAllJobs.myargs conatains: " + dIter.next());
            }
            if (resolveLoopFromUntil) value = resolveLinks(value);    //resolve links
            try {
                this.firstJobIndex = Integer.parseInt(value);         //for checkargs works only if we have no link
            } catch (Exception i) {
                if (resolveLoopFromUntil) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler LOOP_FROM", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error LOOP_FROM", Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.extractArgsForAllJobs: LOOP_FROM = '"+value+"' ist ung�ltig");
                    else throw new PPExecutionException("DiagData.extractArgsForAllJobs: LOOP_FROM = '"+value+"' is invalid");
                } else {
                    this.firstJobIndex = 1;         //assume 1 if not resolveable yet
                }
            }
        } else {
            //NO LOOP_FROM is given, so use 1 as default
            this.firstJobIndex = 1;
        }
        //set the default for lastJobIndex to firstJobIndex
        this.lastJobIndex = this.firstJobIndex;
        //fetch LOOP_UNTIL
        label = "LOOP_UNTIL";
        value = getArg(label);
        if (debug > 1) System.out.println("DiagData.extractArgsForAllJobs:"+label+" = '"+value+"'");
        if (value != null) {
            //LOOP_UNTIL is present
            myargs.remove(label);           //remove the label from the list of all remaining args
            if (debug > 2) {
                System.out.println("DiagData.extractArgsForAllJobs.myargs conatains after LOOP_UNTIL");
                Iterator<Object> dIter = myargs.iterator();
                while (dIter.hasNext()) System.out.println("DiagData.extractArgsForAllJobs.myargs conatains: " + dIter.next());
            }
            if (resolveLoopFromUntil) value = resolveLinks(value);    //resolve links
            try {
                this.lastJobIndex = Integer.parseInt(value);         //for checkargs works only if we have no link
            } catch (Exception i) {
                if (resolveLoopFromUntil) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler LOOP_UNTIL", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error LOOP_UNTIL", Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.extractArgsForAllJobs: LOOP_UNTIL = '"+value+"' ist ung�ltig");
                    else throw new PPExecutionException("DiagData.extractArgsForAllJobs: LOOP_UNTIL = '"+value+"' is invalid");
                } else {
                    this.lastJobIndex = this.firstJobIndex;
                }
            }
        }
        if ((this.lastJobIndex < 0) || (this.lastJobIndex < this.firstJobIndex)) {
            if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler LOOP_UNTIL", Ergebnis.FT_NIO_SYS );
            else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error LOOP_UNTIL", Ergebnis.FT_NIO_SYS );
            errors.add(result);
            if (isDE()) throw new PPExecutionException("DiagData.extractArgsForAllJobs: LOOP_UNTIL = '"+value+"' ist ung�ltig (zu klein)");
            else throw new PPExecutionException("DiagData.extractArgsForAllJobs: LOOP_UNTIL = '"+value+"' is invalid (too small)");
        }
        //create a new vector for all jobs
        this.jobs = new Vector<JobExecution>(20);
        //now we have a first guess for the max. index n for JOBn_* (lastJobIndex). Note: This may grow later.
        this.jobs.setSize(this.lastJobIndex+1);
        
        //create the template (empty)
        this.jobTemplate = new JobExecution();
        
        //loop over all remaining args and extract jobTemplate (JOBN_...)
        Iterator<Object> iter = myargs.iterator();
        while (iter.hasNext()) {
            label = (String)iter.next();
            if (debug > 2) System.out.println("DiagData.extractArgsForAllJobs: extracting template arg "+label+" = " + getArg(label));
            if (label.toLowerCase().startsWith("jobn_")) {
                //we have a JOBN_
                iter.remove();  //remove the label from the list of all remaining args
                //myargs.remove(label);           //remove the label from the list of all remaining args
                
                //create the template when necessary and set currentJobExecution
                if (this.jobTemplate == null) this.jobTemplate = new JobExecution();
                currentJobExecution=this.jobTemplate;
                //get the value and do basic checks (not empty when label is present)
                value = getArg(label);
                if ((value == null) || (value.length() <= 0)) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler leeres Argument " + label + " vorhanden", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error empty argument " + label + " present", Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.extractArgsForAllJobs: leeres Argument " + label + " vorhanden");
                    else throw new PPExecutionException("DiagData.extractArgsForAllJobs: empty argument " + label + " present");
                }
                //extract content and set the value to the current JobExecution-object
                extractOneArg(label.substring(5), value, currentJobExecution, errors);
            } else {
                //we have a JOB*_ so just correct firstJobIndex and lastJobIndex
                idx = label.indexOf('_');
                if (idx < 4) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler "+label, Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error "+label, Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.extractArgsForAllJobs: "+label+" unbekanntes Argument");
                    else throw new PPExecutionException("DiagData.extractArgsForAllJobs: "+label+" unknown arg");
                }
                //get the String between 'JOB' and the underscore '_': e.g. '1','2','3',... (must be a number)
                value = label.substring(3,idx).toUpperCase();
                //Parse the number and extend firstJobIndex and lastJobIndex when necessary
                try {
                    n = Integer.parseInt(value);
                    //correct firstJobIndex
                    if (n < this.firstJobIndex) {
                        //we have a new minimum index
                        this.firstJobIndex = n;
                    }
                    //correct lastJobIndex
                    if (n > this.lastJobIndex) {
                        //we have a new maximum index
                        this.lastJobIndex = n;
                        //now we have a new maximum index N for JOBN_* (lastJobIndex)
                        this.jobs.setSize(this.lastJobIndex+1);
                    }
                } catch (Exception i) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler "+label, Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error "+label, Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.extractArgsForAllJobs: gr�sster Job-Index f�r JOBN_ '"+value+"' ist ung�ltig");
                    else throw new PPExecutionException("DiagData.extractArgsForAllJobs: determining max index for JOBN_ '"+value+"' is invalid");
                }
            }
        }
        
        //define default JUMP_NIO in jobTemplate
        if (jobTemplate.getJumpNioDefault() == DEF_JUMP_UNDEF) {
            //no default-value specified
            if (timeout > 0) {
                //timeout present, so create a endless-loop beginning at the first job
                jobTemplate.setJumpNioDefault(this.firstJobIndex);
            } else {
                //no timeout present, so exit
                jobTemplate.setJumpNioDefault(DEF_JUMP_EXIT_NIO);
            }
        }
        //until here the template has been built up completely
        
        //Loop over all remaining Args and extract the Job-parameters. During this correct firstJobIndex and lastJobIndex.
        iter = myargs.iterator();
        while (iter.hasNext()) {
            label = (String)iter.next();
            if (debug > 2) System.out.println("DiagData.extractArgsForAllJobs: extracting arg "+label+" = " + getArg(label));
            if (label.toLowerCase().startsWith("job")) {
                //we have a JOB*
                idx = label.indexOf('_');
                if (idx < 4) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler "+label, Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error "+label, Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.extractArgsForAllJobs: "+label+" unbekanntes Argument");
                    else throw new PPExecutionException("DiagData.extractArgsForAllJobs: "+label+" unknown arg");
                }
                //get the String between 'JOB' and the underscore '_': e.g. '1','2','3',... (must be a number)
                value = label.substring(3,idx).toUpperCase();
                //Parse the number and extend firstJobIndex and lastJobIndex when necessary
                try {
                    n = Integer.parseInt(value);
                    //correct firstJobIndex (already done before, so just to be sure)
                    if (n < this.firstJobIndex) {
                        //we have a new minimum index
                        this.firstJobIndex = n;
                    }
                    //correct lastJobIndex (already done before, so just to be sure)
                    if (n > this.lastJobIndex) {
                        //we have a new maximum index
                        this.lastJobIndex = n;
                        //now we have a new maximum index N for JOBN_* (lastJobIndex)
                        this.jobs.setSize(this.lastJobIndex+1);
                    }
                } catch (Exception i) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler "+label, Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error "+label, Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.extractArgsForAllJobs: gr�sster Job-Index f�r JOBN_ '"+value+"' ist ung�ltig");
                    else throw new PPExecutionException("DiagData.extractArgsForAllJobs: determining max index for JOBN_ '"+value+"' is invalid");
                }
                //we have a JOBn_
                iter.remove();  //remove the label from the list of all remaining args
                //myargs.remove(label);           //remove the label from the list of all remaining args
                
                //create the template when necessary and set currentJobExecution
                if (this.jobs.get(n) == null) this.jobs.setElementAt(new JobExecution(this.jobTemplate),n);
                currentJobExecution = (JobExecution)this.jobs.get(n);
                //get the value and do basic checks (not empty when label is present)
                value = getArg(label);
                if ((value == null) || (value.length() <= 0)) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler leeres Argument " + label + " vorhanden", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error empty argument " + label + " present", Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.extractArgsForAllJobs: leeres Argument " + label + " vorhanden");
                    else throw new PPExecutionException("DiagData.extractArgsForAllJobs: empty argument " + label + " present");
                }
                //extract content and set the value to the current JobExecution-object
                extractOneArg(label.substring(idx+1), value, currentJobExecution, errors);
            }
        }
        ////Loop over all JobExecutions and set default substitutions and jump destinations. Also do some basic checks.
        for (n=this.firstJobIndex; n <= this.lastJobIndex; n++) {
            //create the template when necessary and set currentJobExecution
            if (this.jobs.get(n) == null) this.jobs.setElementAt(new JobExecution(this.jobTemplate),n);
            currentJobExecution = (JobExecution)jobs.get(n);
            //define %N% for substitutions
            currentJobExecution.setSubstitution("%N%", ""+n);                           //%N% will be substituted by current n
            //define default JUMP_IO
            if (currentJobExecution.getJumpIo() == DEF_JUMP_UNDEF) {
                //no default-value specified
                if (n < this.lastJobIndex) {
                    //we are not at the last Job
                    currentJobExecution.setJumpIo(n + 1);
                } else {
                    //we are at the last Job
                    currentJobExecution.setJumpIo(DEF_JUMP_EXIT_IO);
                }
            }
            
            //loop over all conditions of the current job: Set the HWT to QWT_NIO when not present. Check jumps.
            for (int l=1;l<=currentJobExecution.getNumberOfConditions();l++) {
                //if no HWTL (L is an integer) is present for condition l, use QWT_NIOL
                if (currentJobExecution.getHwt(l) == null) currentJobExecution.setHwt(l,currentJobExecution.getQwtNio(l));
                //Is JUMP_NIOL within firstJobIndex and lastJobIndex???
                idx = currentJobExecution.getJumpNio(l);
                if ((idx != DEF_JUMP_UNDEF) && resolveLoopFromUntil) {
                    //We have JUMP_NIOL, so check the number
                    //correct firstJobIndex (already done before, so just to be sure)
                    if (((idx < this.firstJobIndex) || (idx > this.lastJobIndex))
                    && (idx!=DEF_JUMP_EXIT_IO) && (idx!=DEF_JUMP_EXIT_NIO) ) {
                        //Out of Range
                        if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler JOB"+n+" JUMP_NIO"+l+" "+idx, Ergebnis.FT_NIO_SYS );
                        else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error JOB"+n+" JUMP_NIO"+l+" "+idx, Ergebnis.FT_NIO_SYS );
                        errors.add(result);
                        if (isDE()) throw new PPExecutionException("DiagData.extractArgsForAllJobs: unzl�ssiger Wert JOB"+n+" JUMP_NIO"+l+" "+idx);
                        else throw new PPExecutionException("DiagData.extractArgsForAllJobs: out of range JOB"+n+" JUMP_NIO"+l+" "+idx);
                    }
                } //end JUMP_NIOl is present
            }
            //JUMP_IO, JUMP_NIO, JUMP_... are only allowed if TIMEOUT is present
            //Is JUMP_NIOSYS within firstJobIndex and lastJobIndex???
            idx = currentJobExecution.getJumpNioSys();
            if ((idx != DEF_JUMP_UNDEF) && resolveLoopFromUntil) {
                //check the number
                //correct firstJobIndex (already done before, so just to be sure)
                if (((idx < this.firstJobIndex) || (idx > this.lastJobIndex))
                && (idx!=DEF_JUMP_EXIT_IO) && (idx!=DEF_JUMP_EXIT_NIO) ) {
                    //Out of Range
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler JOB"+n+" JUMP_NIOSYS "+idx, Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error JOB"+n+" JUMP_NIOSYS "+idx, Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.extractArgsForAllJobs: unzl�ssiger Wert JOB"+n+" JUMP_NIOSYS "+idx);
                    else throw new PPExecutionException("DiagData.extractArgsForAllJobs: out of range JOB"+n+" JUMP_NIOSYS "+idx);
                }
            }
            //Is JUMP_NIO within firstJobIndex and lastJobIndex???
            idx = currentJobExecution.getJumpNioDefault();
            if ((idx != DEF_JUMP_UNDEF) && resolveLoopFromUntil) {
                //check the number
                //correct firstJobIndex (already done before, so just to be sure)
                if (((idx < this.firstJobIndex) || (idx > this.lastJobIndex))
                && (idx!=DEF_JUMP_EXIT_IO) && (idx!=DEF_JUMP_EXIT_NIO) ) {
                    //Out of Range
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler JOB"+n+" JUMP_NIO "+idx, Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error JOB"+n+" JUMP_NIO "+idx, Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.extractArgsForAllJobs: unzl�ssiger Wert JOB"+n+" JUMP_NIO "+idx);
                    else throw new PPExecutionException("DiagData.extractArgsForAllJobs: out of range JOB"+n+" JUMP_NIO "+idx);
                }
            }
            //Is JUMP_IO within firstJobIndex and lastJobIndex???
            idx = currentJobExecution.getJumpIo();
            if ((idx != DEF_JUMP_UNDEF) && resolveLoopFromUntil) {
                //check the number
                //correct firstJobIndex (already done before, so just to be sure)
                if (((idx < this.firstJobIndex) || (idx > this.lastJobIndex))
                && (idx!=DEF_JUMP_EXIT_IO) && (idx!=DEF_JUMP_EXIT_NIO) ) {
                    //Out of Range
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler JOB"+n+" JUMP_IO "+idx, Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error JOB"+n+" JUMP_IO "+idx, Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.extractArgsForAllJobs: unzl�ssiger Wert JOB"+n+" JUMP_IO "+idx);
                    else throw new PPExecutionException("DiagData.extractArgsForAllJobs: out of range JOB"+n+" JUMP_IO "+idx);
                }
            }
            
            //check if JOB and SGBD are defined
            ((JobExecution)jobs.get(n)).checkArgs();
        }
        
        //check if myargs contains any unhandled args
        if (!myargs.isEmpty()) {
            //we have unresolved arguments
            if (debug > 0) {
                iter = myargs.iterator();
                while (iter.hasNext()) {
                    System.out.println("DiagData.extractArgsForAllJobs: unknown Argument "+iter.next());
                }
            }
            if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler unbekannte Argumente vorhanden", Ergebnis.FT_NIO_SYS );
            else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error unknown args present", Ergebnis.FT_NIO_SYS );
            errors.add(result);
            if (isDE()) throw new PPExecutionException("DiagData.extractArgsForAllJobs: es sind nicht verarbeitete Argumente vorhanden");
            else throw new PPExecutionException("DiagData.extractArgsForAllJobs: there were unknown unhandled arguments");
        }
        
    }
    
    /**
     * Set the value of an argument specified by labelWithoutPrefix for the JobExecution currentJobExecution.
     * Assumptions: labelWithoutPrefix NOT null. value NOT null and NOT empty. currentJobExecution NOT null.
     * @param labelWithoutPrefix label of the argument without the leading JOBN_ or JOBn_ (n=0,...).
     * @param value value to be set
     * @param currentJobExecution specifies the modified JobExecution-Object
     * @param errors specifies a Vector for storing errors (an exception is also thrown)
     **/
    private void extractOneArg(String labelWithoutPrefix, String value, JobExecution currentJobExecution, Vector<Ergebnis> errors) throws PPExecutionException {
        
        Ergebnis result;
        int k,l,m,i, jump;
        
        if (debug > 2) {
            System.out.println("DiagData.extractOneArg: Label = " + labelWithoutPrefix);
            System.out.println("DiagData.extractOneArg: Value = " + value);
        }
        
        //to upper case (so startsWith works)
        labelWithoutPrefix = labelWithoutPrefix.toUpperCase();
        
        if (labelWithoutPrefix.equalsIgnoreCase("QWT_NIO")) {
            //QWT_NIO
            currentJobExecution.setQwtNioDefault(value);
        } else if (labelWithoutPrefix.equalsIgnoreCase("QWT_NIOSYS")) {
            //QWT_NIOSYS
            currentJobExecution.setQwtNioSys(value);
        } else if (labelWithoutPrefix.startsWith("QWT_NIO")) {
            //QWT_NIOL (L is an integer)
            //first parse L to int
            try {
                l = Integer.parseInt(labelWithoutPrefix.substring("QWT_NIO".length()));
                if (l < 0) throw new PPExecutionException();    //handled by catch below
            } catch (Exception e) {
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler unbekanntes Argument '"+labelWithoutPrefix+"'", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error unknown argument '"+labelWithoutPrefix+"'", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: unbekanntes Argument '"+labelWithoutPrefix+"'");
                else throw new PPExecutionException("DiagData.extractOneArg: unknown argument '"+labelWithoutPrefix+"'");
            }
            currentJobExecution.setQwtNio(l,value);
        } else if (labelWithoutPrefix.equalsIgnoreCase("SGBD")) {
            //*SGBD
            currentJobExecution.setSgbd(value);
        } else if (labelWithoutPrefix.equalsIgnoreCase("JOB")) {
            //*JOB
            currentJobExecution.setJob(value);
        } else if (labelWithoutPrefix.equalsIgnoreCase("PAR")) {
            //PAR
            currentJobExecution.setPar(value);
        } else if (labelWithoutPrefix.equalsIgnoreCase("HIDEPAR")) {
            //HIDEPAR
            if (value.equalsIgnoreCase("TRUE")) {
                //set HidePar to true
                currentJobExecution.setHidePar(true);
            } else if (value.equalsIgnoreCase("FALSE")) {
                //set HidePar to false;
                currentJobExecution.setHidePar(false);
            } else {
                //Argument has invalid value
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: unbekannter Wert f�r HIDEPAR '"+value+"' nur TRUE oder FALSE zul�ssig");
                else throw new PPExecutionException("DiagData.extractOneArg: unknown value for HIDEPAR '"+value+"' only TRUE or FALSE allowed");
            }
        } else if (labelWithoutPrefix.equalsIgnoreCase("CONDITIONMODE")) {
            //CONDITIONMODE
            if ((!value.equalsIgnoreCase("AND")) && (!value.equalsIgnoreCase("NOR")) &&
            (!value.equalsIgnoreCase("OR"))) {
                //Mode is unknown
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: unbekannter CONDITIONMODE '"+value+"'");
                else throw new PPExecutionException("DiagData.extractOneArg: unknown CONDITIONMODE '"+value+"'");
            }
            currentJobExecution.setConditionMode(value);
        } else if (labelWithoutPrefix.equalsIgnoreCase("HWT")) {
            //HWT
            currentJobExecution.setDefaultHwt(value);
        } else if (labelWithoutPrefix.startsWith("RESULT")) {
            //RESULTK (K is an integer)
            //first parse K to int
            try {
                k = Integer.parseInt(labelWithoutPrefix.substring("RESULT".length()));
                if (k < 0) throw new PPExecutionException();    //handled by catch below
            } catch (Exception e) {
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler unbekanntes Argument '"+labelWithoutPrefix+"'", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error unknown argument '"+labelWithoutPrefix+"'", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: unbekanntes Argument '"+labelWithoutPrefix+"'");
                else throw new PPExecutionException("DiagData.extractOneArg: unknown argument '"+labelWithoutPrefix+"'");
            }
            currentJobExecution.setResult(k, value);
        } else if (labelWithoutPrefix.startsWith("HIDERESULT")) {
            //HIDERESULTK (K is an integer)
            //first parse K to int
            try {
                k = Integer.parseInt(labelWithoutPrefix.substring("HIDERESULT".length()));
                if (k < 0) throw new PPExecutionException();    //handled by catch below
            } catch (Exception e) {
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler unbekanntes Argument '"+labelWithoutPrefix+"'", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error unknown argument '"+labelWithoutPrefix+"'", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: unbekanntes Argument '"+labelWithoutPrefix+"'");
                else throw new PPExecutionException("DiagData.extractOneArg: unknown argument '"+labelWithoutPrefix+"'");
            }
            //check value only TRUE/FALSE are allowed
            if (value.equalsIgnoreCase("TRUE")) {
                currentJobExecution.setHideResult(k,true);
            } else if(value.equalsIgnoreCase("FALSE")) {
                currentJobExecution.setHideResult(k,false);
            } else {
                //HideResultK is invalid
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.extractArgsForOneJob: unbekannter Wert f�r HIDERESULT '"+value+"': nur TRUE oder FALSE zul�ssig");
                else throw new PPExecutionException("DiagData.extractArgsForOneJob: unknown value for HIDERESULT '"+value+"': only TRUE or FALSE allowed");
            }
        } else if (labelWithoutPrefix.startsWith("CONDITION")) {
            //CONDITIONL (L is an integer)
            //first parse L to int
            try {
                l = Integer.parseInt(labelWithoutPrefix.substring("CONDITION".length()));
                if (l < 0) throw new PPExecutionException();    //handled by catch below
            } catch (Exception e) {
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler unbekanntes Argument '"+labelWithoutPrefix+"'", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error unknown argument '"+labelWithoutPrefix+"'", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: unbekanntes Argument '"+labelWithoutPrefix+"'");
                else throw new PPExecutionException("DiagData.extractOneArg: unknown argument '"+labelWithoutPrefix+"'");
            }
            currentJobExecution.setCondition(l,value);
        } else if (labelWithoutPrefix.startsWith("HWT")) {
            //HWTL (L is an integer)
            //first parse L to int
            try {
                l = Integer.parseInt(labelWithoutPrefix.substring("HWT".length()));
                if (l < 0) throw new PPExecutionException();    //handled by catch below
            } catch (Exception e) {
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler unbekanntes Argument '"+labelWithoutPrefix+"'", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error unknown argument '"+labelWithoutPrefix+"'", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: unbekanntes Argument '"+labelWithoutPrefix+"'");
                else throw new PPExecutionException("DiagData.extractOneArg: unknown argument '"+labelWithoutPrefix+"'");
            }
            currentJobExecution.setHwt(l,value);
        } else if (labelWithoutPrefix.equalsIgnoreCase("JUMP_NIO")) {
            //JUMP_NIO
            //check value and substitute IO/NIO by DEF_JUMP_EXIT_IO/DEF_JUMP_EXIT_NIO
            if (value.equalsIgnoreCase("IO") || value.equalsIgnoreCase("EXIT_IO") || value.equalsIgnoreCase("EXITIO")) {
                //NIO so use DEF_JUMP_NIO
                jump = DEF_JUMP_EXIT_IO;
            } else if (value.equalsIgnoreCase("NIO") || value.equalsIgnoreCase("EXIT_NIO") || value.equalsIgnoreCase("EXITNIO")) {
                //NIO so use DEF_JUMP_NIO
                jump = DEF_JUMP_EXIT_NIO;
            } else try {
                //A JOBN has been specified by giving a number N
                jump = Integer.parseInt(value);
                if (jump < 0) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: ung�ltiges JUMP_NIO '"+value+"': nur IO, NIO oder eine JOB-Nummmer sind zul�ssig");
                    else throw new PPExecutionException("DiagData.extractOneArg: invalid JUMP_NIO '"+value+"': only IO, NIO or a JOB-Number are allowed");
                }
            } catch (NumberFormatException nfe) {
                //JUMP_NIO is invalid
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: ung�ltiges JUMP_NIO '"+value+"': nur IO, NIO oder eine JOB-Nummmer sind zul�ssig");
                else throw new PPExecutionException("DiagData.extractOneArg: invalid JUMP_NIO '"+value+"': only IO, NIO or a JOB-Number are allowed");
            }
            currentJobExecution.setJumpNioDefault(jump);
        } else if (labelWithoutPrefix.equalsIgnoreCase("JUMP_NIOSYS")) {
            //JUMP_NIOSYS
            //substitute IO/NIO by DEF_JUMP_EXIT_IO/DEF_JUMP_EXIT_NIO
            if (value.equalsIgnoreCase("IO") || value.equalsIgnoreCase("EXIT_IO") || value.equalsIgnoreCase("EXITIO")) {
                //IO so use DEF_JUMP_IO
                jump = DEF_JUMP_EXIT_IO;
            } else if (value.equalsIgnoreCase("NIO") || value.equalsIgnoreCase("EXIT_NIO") || value.equalsIgnoreCase("EXITNIO")) {
                //NIO so use DEF_JUMP_NIO
                jump = DEF_JUMP_EXIT_NIO;
            } else try {
                //A JOBN has been specified by giving a number N
                jump = Integer.parseInt(value);
                if (jump < 0) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: ung�ltiges JUMP_NIOSYS '"+value+"': nur IO, NIO oder eine JOB-Nummmer sind zul�ssig");
                    else throw new PPExecutionException("DiagData.extractOneArg: invalid JUMP_NIOSYS '"+value+"': only IO, NIO or a JOB-Number are allowed");
                }
            } catch (NumberFormatException nfe) {
                //JUMP_NIO is invalid
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: ung�ltiges JUMP_NIOSYS '"+value+"': nur IO, NIO oder eine JOB-Nummmer sind zul�ssig");
                else throw new PPExecutionException("DiagData.extractOneArg: invalid JUMP_NIOSYS '"+value+"': only IO, NIO or a JOB-Number are allowed");
            }
            currentJobExecution.setJumpNioSys(jump);
        } else if (labelWithoutPrefix.startsWith("JUMP_NIO")) {
            //JUMP_NIOL (L is an integer)
            //first parse L to int
            try {
                l = Integer.parseInt(labelWithoutPrefix.substring("JUMP_NIO".length()));
                if (l < 0) throw new PPExecutionException();    //handled by catch below
            } catch (Exception e) {
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler unbekanntes Argument '"+labelWithoutPrefix+"'", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error unknown argument '"+labelWithoutPrefix+"'", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: unbekanntes Argument '"+labelWithoutPrefix+"'");
                else throw new PPExecutionException("DiagData.extractOneArg: unknown argument '"+labelWithoutPrefix+"'");
            }
            //substitute IO/NIO by DEF_JUMP_EXIT_IO/DEF_JUMP_EXIT_NIO
            if (value.equalsIgnoreCase("IO") || value.equalsIgnoreCase("EXIT_IO") || value.equalsIgnoreCase("EXITIO")) {
                //NIO so use DEF_JUMP_NIO
                jump = DEF_JUMP_EXIT_IO;
            } else if (value.equalsIgnoreCase("NIO") || value.equalsIgnoreCase("EXIT_NIO") || value.equalsIgnoreCase("EXITNIO")) {
                //NIO so use DEF_JUMP_NIO
                jump = DEF_JUMP_EXIT_NIO;
            } else try {
                //A JOBN has been specified by giving a number N
                jump = Integer.parseInt(value);
                if (jump < 0) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: ung�ltiges JUMP_NIO"+l+" '"+value+"': nur IO, NIO oder eine JOB-Nummmer sind zul�ssig");
                    else throw new PPExecutionException("DiagData.extractOneArg: invalid JUMP_NIO"+l+" '"+value+"': only IO, NIO or a JOB-Number are allowed");
                }
            } catch (NumberFormatException nfe) {
                //JUMP_NIO is invalid
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: ung�ltiges JUMP_NIO"+l+" '"+value+"': nur IO, NIO oder eine JOB-Nummmer sind zul�ssig");
                else throw new PPExecutionException("DiagData.extractOneArg: invalid JUMP_NIO"+l+" '"+value+"': only IO, NIO or a JOB-Number are allowed");
            }
            currentJobExecution.setJumpNio(l,jump);
        } else if (labelWithoutPrefix.startsWith("STORAGENAME")) {
            //STORAGENAMEM (M is an integer)
            //first parse M to int
            try {
                m = Integer.parseInt(labelWithoutPrefix.substring("STORAGENAME".length()));
                if (m < 0) throw new PPExecutionException();    //handled by catch below
            } catch (Exception e) {
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler unbekanntes Argument '"+labelWithoutPrefix+"'", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error unknown argument '"+labelWithoutPrefix+"'", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: unbekanntes Argument '"+labelWithoutPrefix+"'");
                else throw new PPExecutionException("DiagData.extractOneArg: unknown argument '"+labelWithoutPrefix+"'");
            }
            currentJobExecution.setStorageName(m,value);
        } else if (labelWithoutPrefix.startsWith("STORAGEFORMAT")) {
            //STORAGEFORMATM (M is an integer)
            //first parse M to int
            try {
                m = Integer.parseInt(labelWithoutPrefix.substring("STORAGEFORMAT".length()));
                if (m < 0) throw new PPExecutionException();    //handled by catch below
            } catch (Exception e) {
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler unbekanntes Argument '"+labelWithoutPrefix+"'", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error unknown argument '"+labelWithoutPrefix+"'", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: unbekanntes Argument '"+labelWithoutPrefix+"'");
                else throw new PPExecutionException("DiagData.extractOneArg: unknown argument '"+labelWithoutPrefix+"'");
            }
            currentJobExecution.setStorageFormat(m,value);
        } else if (labelWithoutPrefix.equalsIgnoreCase("PAUSE_BEFORE")) {
            //PAUSE_BEFORE
            //Check value
            try {
                i = Integer.parseInt(value);
                if (i < 0) throw new PPExecutionException();    //handled by catch below
            } catch (Exception e) {
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler PAUSE_BEFORE unzul�ssig '"+value+"'", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error PAUSE_BEFORE invalid '"+value+"'", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: PAUSE_BEFORE unzul�ssig '"+value+"'");
                else throw new PPExecutionException("DiagData.extractOneArg: PAUSE_BEFORE invalid '"+value+"'");
            }
            currentJobExecution.setPauseBefore(i);
        } else if (labelWithoutPrefix.equalsIgnoreCase("PAUSE_AFTER")) {
            //PAUSE_AFTER
            //Check value
            try {
                i = Integer.parseInt(value);
                if (i < 0) throw new PPExecutionException();    //handled by catch below
            } catch (Exception e) {
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler PAUSE_AFTER unzul�ssig '"+value+"'", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error PAUSE_AFTER invalid '"+value+"'", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: PAUSE_AFTER unzul�ssig '"+value+"'");
                else throw new PPExecutionException("DiagData.extractOneArg: PAUSE_AFTER invalid '"+value+"'");
            }
            currentJobExecution.setPauseAfter(i);
        } else if (labelWithoutPrefix.equalsIgnoreCase("JUMP_IO")) {
            //JUMP_IO
            //check value and substitute IO/NIO by DEF_JUMP_EXIT_IO/DEF_JUMP_EXIT_NIO
            if (value.equalsIgnoreCase("IO") || value.equalsIgnoreCase("EXIT_IO") || value.equalsIgnoreCase("EXITIO")) {
                //IO so use DEF_JUMP_IO
                jump = DEF_JUMP_EXIT_IO;
            } else if (value.equalsIgnoreCase("NIO") || value.equalsIgnoreCase("EXIT_NIO") || value.equalsIgnoreCase("EXITNIO")) {
                //NIO so use DEF_JUMP_NIO
                jump = DEF_JUMP_EXIT_NIO;
            } else try {
                //A JOBN has been specified by giving a number N
                jump = Integer.parseInt(value);
                if (jump < 0) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: ung�ltiges JUMP_IO '"+value+"': nur IO, NIO oder eine JOB-Nummer sind zul�ssig");
                    else throw new PPExecutionException("DiagData.extractOneArg: invalid JUMP_IO '"+value+"': only IO, NIO or a JOB-Number are allowed");
                }
            } catch (NumberFormatException nfe) {
                //JUMP_IO is invalid
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: ung�ltiges JUMP_IO '"+value+"': nur IO, NIO oder eine JOB-Nummer sind zul�ssig");
                else throw new PPExecutionException("DiagData.extractOneArg: invalid JUMP_IO '"+value+"': only IO, NIO or a JOB-Number are allowed");
            }
            currentJobExecution.setJumpIo(jump);
        } else if (labelWithoutPrefix.equalsIgnoreCase("IWT")) {
            //IWT
            //check if empty or NONE
            if ((value.trim().length() == 0) || (value.equalsIgnoreCase("NONE"))) {
                //Empty String, so set to null
                value = null;
            }
            currentJobExecution.setIwt(value);
        } else if (labelWithoutPrefix.equalsIgnoreCase("BREAKPOINT")) {
            // BREAKPOINT
            //check if empty or NONE
            
            Object pr_var;
            if ((value.trim().length() == 0) || (value.equalsIgnoreCase("NONE"))) {
                //Empty String, so set to null
                value = null;
            }
            
            if (getPr�flingLaufzeitUmgebung().getMode().equals(PruefstandConstants.PSMODE_TEST)) {
                // Breakpoints may only be active if Teststand is in Test mode
                try {
                    pr_var=getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PS_CONFIG, "BREAKPOINTS_ACTIVE");
                    if (pr_var!=null) {
                        // To activate Breakpoints, Pruefstandsvariable BREAKPOINTS_ACTIVE=TRUE must be set
                        if (pr_var instanceof String) {
                            if (((String)pr_var).equalsIgnoreCase("TRUE"))  {
                                currentJobExecution.setBreakpoint(value);
                            }
                        }
                    }
                }
                catch (VariablesException e) { }
            }
            
        } else {
            //Unknown Argument
            if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler unbekanntes Argument: " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
            else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error unknown argument: " + labelWithoutPrefix, Ergebnis.FT_NIO_SYS );
            errors.add(result);
            if (isDE()) throw new PPExecutionException("DiagData.extractOneArg: Parametrierfehler unbekanntes Argument: " + labelWithoutPrefix);
            else throw new PPExecutionException("DiagData.extractOneArg: parameter error unknown argument: " + labelWithoutPrefix);
        }
    }
    
    /**
     * Resolve %N% and links in all defined jobs
     */
    private void printJobs() {
        int i;            //i for loop over jobs
        //loop over all jobs
        for (i = this.firstJobIndex; i <= this.lastJobIndex;i++) {
            System.out.println(((JobExecution)jobs.get(i)).toString());
        }
    }
    
    /**
     * Subsitute pattern by newValue in input. The comparision is NOT case sensitive.
     * @param input String containing %N%
     * @param pattern Will be replaced by newValue
     * @param newValue Value for the pattern
     * @return the resolved String
     */
    private static String resolveAny(String input, String newValue, String pattern) {
        if (input == null) return input;
        StringBuffer res = new StringBuffer(input);
        boolean goon = true;
        int idx;
        while (goon) {
            //resolve pattern
            idx = res.toString().toLowerCase().indexOf(pattern.toLowerCase());
            if (idx < 0) {
                //pattern not found
                goon = false;
            } else {
                res.replace(idx,idx+pattern.length(), newValue);
            }
        }
        return res.toString();
    }
    
    /**
     * Subsitute pattern by newValue in input. The comparision is NOT case sensitive.
     * @param input String containing %N%
     * @param substitutions Hashtable containing pairs of pattern and newValue
     * @return the resolved String
     */
    private static String resolveSubstitutions(String input, Hashtable substitutions) throws PPExecutionException {
        String pattern=null;
        String newValue=null;
        Iterator iter = substitutions.keySet().iterator();  //iterator over all patterns
        while (iter.hasNext()) {
            try {
                pattern = (String)iter.next();
                newValue = (String)substitutions.get(pattern);
            } catch (ClassCastException cce) {
                if (isDE()) throw new PPExecutionException("DiagData.resolveSubstitutions: Unbekannte Java-Klasse Interner Fehler");
                else throw new PPExecutionException("DiagData.resolveSubstitutions: ClassCastException internal error");
            }
            input = resolveAny(input,newValue,pattern);
        }
        return input;
    }
    
    /**
     * Resolve Links for Dynamic Attributes (@...@) and normal Results (...@...)
     * @param input The String, that contains Links. Links may be separated by ' ', ';', ',', '(' or ')'
     * @return The String with resolved Links
     */
    private String resolveLinks(String input) throws PPExecutionException {
        StringBuffer res = new StringBuffer();
        StringTokenizer tokenizer = new StringTokenizer(input," ,;()",true); //create the stringtokenizer, split-chars are returned as tokens
        String currentToken = null;     //for storing the current token
        String currentResolved = null;  //for storing the resolved current token
        if (debug > 2) System.out.println("DiagData.resolveLinks: input='"+input+"'");
        try {
            while (tokenizer.hasMoreTokens()) {
                //get the token
                currentToken = tokenizer.nextToken();
                if (debug > 3) System.out.println("DiagData.resolveLinks: resolving token '"+currentToken+"'");
                //analyse the token
                if (currentToken.indexOf('@') == -1) {
                    //no @ contained, so nothing to resolve
                    currentResolved = currentToken;
                } else if ((currentToken.charAt(0) == '@') && (currentToken.charAt(currentToken.length() - 1) == '@')) {
                    //@...@ detected
                    //first check length
                    if (currentToken.length() <= 3) {
                        if (isDE()) throw new PPExecutionException("DiagData.resolveLinks: kann Verweis '"+currentToken+"' nicht aufl�sen -> Name zu kurz");
                        else throw new PPExecutionException("DiagData.resolveLinks: can't resolve '"+currentToken+"' -> String is too short");
                    }
                    //remove the @ at the beginning and the end
                    currentToken = currentToken.substring(1,currentToken.length()-1);
                    //resolve dynamic attribute
                    currentResolved = this.getDynamicAttribute(currentToken);
                    //check if the resolved dynamic attribute is a ...@...-link
                    int first = currentResolved.indexOf('@');
                    if (first != -1) {
                        //resolve ...@...
                        int last = currentResolved.lastIndexOf('@');
                        if (first != last) {
                            if (isDE()) throw new PPExecutionException("DiagData.resolveLinks: kann Verweis '"+currentResolved+"' nicht aufl�sen -> ung�ltiges Format");
                            else throw new PPExecutionException("DiagData.resolveLinks: can't resolve link '"+currentResolved+"' -> bad format");
                        }
                        //resolve
                        currentResolved = this.getPPResult(currentResolved);
                    }
                } else {
                    //...@... assumed
                    //only one @ allowed
                    int first = currentToken.indexOf('@');
                    int last = currentToken.lastIndexOf('@');
                    if (first != last) {
                        if (isDE()) throw new PPExecutionException("DiagData.resolveLinks: kann Verweis '"+currentToken+"' nicht aufl�sen -> ung�ltiges Format");
                        else throw new PPExecutionException("DiagData.resolveLinks: can't resolve link '"+currentToken+"' -> bad format");
                    }
                    //resolve
                    currentResolved = this.getPPResult(currentToken);
                }
                //check if currentResolved is null
                if (currentResolved == null) {
                    if (isDE()) throw new PPExecutionException("DiagData.resolveLinks: kann '"+currentToken+"' nicht aufl�sen");
                    else throw new PPExecutionException("DiagData.resolveLinks: unable to resolve '"+currentToken+"'");
                }
                //append the resolved token to res
                res.append(currentResolved);
                if (debug > 3) System.out.println("DiagData.resolveLinks: resolved token '"+currentResolved+"'");
            }
        } catch(PPExecutionException ppe) {
            throw ppe;
        } catch (Exception e) {
            e.printStackTrace(System.out);
            if (e.getMessage() != null) {
                //e has a message
                if (isDE()) throw new PPExecutionException("DiagData.resolveLinks: unerwarteter Fehler: " + e.getMessage());
                else throw new PPExecutionException("DiagData.resolveLinks: unexpected Exception: " + e.getMessage());
            } else {
                //e has no message
                if (isDE()) throw new PPExecutionException("DiagData.resolveLinks: unerwarteter Fehler");
                else throw new PPExecutionException("DiagData.resolveLinks: unexpected Exception");
            }
        }
        if (debug > 2) System.out.println("DiagData.resolveLinks: output='"+res+"'");
        return res.toString();
    }
    
    /**
     * Compares two arguments a1 and a2. The arguments maybe integers, floats, or strings.
     * Returns an integer with the following meaning: 0-> a1 == a2, 1-> a1 < a2, 2-> a2 < a1,
     * 3-> a1 < a2 Strings have different length (not for integers and floats)
     * 4-> a2 < a1 Strings have different length (not for integers and floats)
     * @param arg1 Argument a1 for the comparison
     * @param arg2 Argument a2 for the comparison
     * @return The integer describing the result of the comparison. See above.
     */
    public static int compare( String arg1, String arg2) {
        long i1=0,i2=0;
        double d1=0,d2=0;
        boolean bothInteger = false;
        boolean bothDouble=false;
        
        //check for arg1 and arg2 to be integers or doubles
        try {
            i1=Long.parseLong(arg1);
            i2=Long.parseLong(arg2);
            //if we came here arg1 and arg2 are integers
            bothInteger = true;
            bothDouble = false;
        } catch(NumberFormatException nfe) {
            bothInteger = false;    //at least one is no integer
            try {
                d1=Double.parseDouble(arg1);
                d2=Double.parseDouble(arg2);
                //if we came here arg1 and arg2 are doubles
                bothDouble = true;
            } catch(NumberFormatException nfe2) {
                bothDouble = false; //at least one is no double
            }
        }
        if (bothInteger) {
            //both args are integers
            if (i1==i2) {
                return 0;       //equal
            } else if (i1 < i2) {
                return 1;       //arg1 < arg2
            } else {
                return 2;       //arg2 < arg1
            }
        } else if (bothDouble) {
            //both args are doubles
            if (d1==d2) {
                return 0;       //equal
            } else if (d1 < d2) {
                return 1;       //arg1 < arg2
            } else {
                return 2;       //arg2 < arg1
            }
        } else {
            //both args are strings
            if (arg1.length() != arg2.length()) {
                //both strings have a different length
                if (arg1.compareTo(arg2) < 0) {
                    return 3;   //arg1 < arg2
                } else {
                    return 4;   //arg2 < arg1 (arg1 == arg2 is not possible because the different length)
                }
            } else {
                //both strings have the same length
                if (arg1.equals(arg2)) {
                    return 0;   //arg1 == arg2
                } else if (arg1.compareTo(arg2) < 0) {
                    return 1;   //arg1 < arg2
                } else {
                    return 2;   //arg2 < arg1
                }
            }
        }
    }
    
    /**
     * resolve all expressions in input
     * All Links, %N%, and %R*% must have already been resolved
     * @param input Expression to be resolved
     * @param debug true if debugging is enabled
     * @return resolved expression
     */
    private static String resolveExpression(String input, int debug) throws PPExecutionException {
        String command;
        char op = ' ';
        String argument;
        String res;
        StringBuffer temp;
        String[] args;
        boolean ok = false;
        long num,long1,long2;
        int idx1,idx2,i,j,pos;
        if (debug > 1) System.out.println("DiagData.resolveExpression: input='"+input+"'");
        idx1 = input.indexOf('(');      //position of the first '('
        idx2 = input.lastIndexOf(')');  //position of the last ')'
        if ((idx1 < 0) && (idx2 < 0)) {
            //no '(' and ')' included, so nothing to do
            if (debug > 1) System.out.println("DiagData.resolveExpression: nothing to do");
            return input;
        } else if ((idx1 > 0) && (idx2 > 1) && ((idx1) < idx2)){
            //'(' and ')' are included and in acceptable position
            if (debug > 3) System.out.println("DiagData.resolveExpression: command detected");
            command = input.substring(0,idx1);
            if (debug > 3) System.out.println("DiagData.resolveExpression: command='"+command+"'");
            argument = input.substring(idx1+1,idx2);
            if (debug > 3) System.out.println("DiagData.resolveExpression: argument='"+argument+"'");
            ok = false;
            //loop over all known commands
            for (i=0; i<Commands.length; i++) {
                if (command.equalsIgnoreCase(Commands[i][0])||
                command.equalsIgnoreCase(Commands[i][1])) {
                    ok = true;
                    op = Commands[i][1].charAt(0);              //put a abbreviation for the command into op
                }
            }
            if (!ok) {
                if (debug > 1) System.out.println("DiagData.resolveExpression: unknown command: '"+command+"'");
                if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: unbekanntes Kommando: '"+command+"'");
                else throw new PPExecutionException("DiagData.resolveExpression: unknown command: '"+command+"'");
            }
            //split argument into args
            args = splitExpression(argument, debug);
            /**
             * possible commands for Expressions
             * HEX,INT2HEX,DATA,UNDATA,EQUALS,...
             */
    /*public static final String Commands[][] = { {"HEX",         "H"},   //Put a '0x' at the beginning of the argument
                                                {"INT2HEX",     "I"},   //convert an integer to HEX and add a '0' at the beginning if the results has an odd number of characters
                                                {"HEX2INT",     "2"},   //convert a HEX to an integer e.g. HEX2INT("12") = "18"
                                                {"DATA",        "D"},   //covert a HEX-Input-String e.g. "123456" to a string where the bytes are separated by spaces e.g. "12 34 56". Also a toUpperCase is done.
                                                {"UNDATA",      "U"},   //undo the operation of DATA. That means spaces are removed. Also a toUpperCase is done.
                                                {"EQUALS",      "E"},   //Compares the two arguments
                                                {"AND",         "A"},   //All Arguments have to be DEF_TRUE
                                                {"OR",          "O"},   //One Argument has to be DEF_TRUE
                                                {"NOT",         "N"},   //The Argument is converted from DEF_TRUE to DEF_FALSE and from DEF_FALSE to DEF_TRUE
                                                {"EQUALSIC",    "C"},   //Does the same as EQUALS, but the comparison is not case sensitive.
                                                {"TOLOWER",     "K"},   //Converts the argument to lower case
                                                {"TOUPPER",     "T"},   //Converts the argument to upper case
                                                {"LESSTHAN",    "L"},   //the first argument has to be smaller than the second argument
                                                {"LESS",        "L"},   //the first argument has to be smaller than the second argument
                                                {"GREATERTHAN", "G"},   //the first argument has to be greater than the second argument
                                                {"GREATER",     "G"},   //the first argument has to be greater than the second argument
                                                {"LESSOREQ",    "X"},   //the first argument has to be smaller or equal than the second argument
                                                {"GREATEROREQ", "Y"},   //the first argument has to be greater or equal than the second argument
                                                {"BETWEEN",     "B"},   //first argument smaller second argument smaller third argument a1 < a2 < a3
                                                {"BETWEENIB",   "Z"},   //between including boundaries a1 <= a2 <= a3
                                                {"SUBSTR",      "S"},   //extract a substring of the first argument. The second argument determines the beginning. The third determines the length if present.
                                                {"SUBSTRING",   "S"},   //extract a substring of the first argument. The second argument determines the beginning. The third determines the length if present.
                                                {"HEAD",        "P"},   //extract a heading substring of the first argument. The second argument determines the length.
                                                {"TAIL",        "Q"},   //extract a tailing substring of the first argument. The second argument determines the length.
                                                {"REVERSE",     "R"},   //The Argument String is Reversed
                                                {"LENGTH",      "V"},   //Returns the length of the Argument
                                                {"LEN",         "V"},   //Returns the length of the Argument
                                                {"CONCAT",      "J"},   //concatenates the arguments in the given order
                                                {"TRIM",        "1"},   //remove all leading and ending spaces
                                                {"MINUS",       "M"},   //Substract the second argument from the first one
                                                {"PLUS",        "F"}};  //add both arguments
                                                {"IF",          "3"},   //IF_THEN_ELSE: first argument is the condition (DEF_TRUE/DEF_FALSE), the second is the result in case DEF_TRUE, the third is the result in case DEF_FALSE.
                                                {"IFELSE",      "3"},   //IF_THEN_ELSE: first argument is the condition (DEF_TRUE/DEF_FALSE), the second is the result in case DEF_TRUE, the third is the result in case DEF_FALSE.
                                                {"SWITCH",      "4"}};  //SWITCH: first argument is throws input, the last argument is the default result, between are pairs of comparevalue and result in case input equals comparevalue.
                                                {"HEX2ASC",     "5"},   //Converts the Input to ASCII. Two chars input are converted to one char output.
                                                {"HEX2ASCII",   "5"}};  //Converts the Input to ASCII. Two chars input are converted to one char output.
                                                {"FILLLEFT",    "6"},   //Fills up the given String (arg1) to a certain length (arg2) by adding a given String (arg3) in front.
                                                {"FILLRIGHT",   "7"}};  //Fills up the given String (arg1) to a certain length (arg2) by adding a given String (arg3) at the end.*/
            //decide what to do
            switch (op) {
                case 'H':
                    //{"HEX",         "H"},   //Put a '0x' at the beginning of the argument
                    //only one argument allowed
                    if (args.length != 1) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando HEX: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command HEX: wrong number of arguments");
                    }
                    res =  "0x"+resolveExpression(args[0], debug);
                    break;
                case 'I':
                    //{"INT2HEX",     "I"},   //convert an integer to HEX and add a '0' at the beginning if the results has an odd number of characters
                    //only one argument allowed
                    if (args.length != 1) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando INT2HEX: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command INT2HEX: wrong number of arguments");
                    }
                    num = 0;
                    argument = resolveExpression(args[0], debug);
                    try {
                        num = Long.parseLong(argument);
                    } catch (Exception n) {
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando INT2HEX: unzul�ssiges Argument '"+argument+"'");
                        else throw new PPExecutionException("DiagData.resolveExpression: command INT2HEX: input invalid '"+argument+"'");
                    }
                    res = "" + Long.toHexString(num).toUpperCase();
                    if ((res.length() % 2) > 0) res = "0"+res;
                    break;
                case '2':
                    //{"HEX2INT",     "2"},   //convert a HEX to an integer e.g. HEX2INT("12") = "18"
                    //only one argument allowed
                    if (args.length != 1) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando HEX2INT: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command HEX2INT: wrong number of arguments");
                    }
                    argument = resolveExpression(args[0], debug);
                    //check if number of characters is even
                    if ((argument.length() % 2) != 0) {
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando HEX2INT: unzul�ssiges Argument '"+argument+"'");
                        else throw new PPExecutionException("DiagData.resolveExpression: command HEX2INT: input invalid '"+argument+"'");
                    }
                    //cut-off leading "0x"
                    if (argument.startsWith("0x")) {
                        argument = argument.substring(2);                 //cut-off leading 0x
                    }
                    //parse the hex-string
                    try {
                        num = Long.parseLong(argument,16);
                    } catch (Exception n) {
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando HEX2INT: unzul�ssiges Argument '"+argument+"'");
                        else throw new PPExecutionException("DiagData.resolveExpression: command HEX2INT: input invalid '"+argument+"'");
                    }
                    res = "" + num;                                     //convert to String
                    break;
                case 'D':
                    //{"DATA",        "D"},   //covert a HEX-Input-String e.g. "123456" to a string where the bytes are separated by spaces e.g. "12 34 56". Also a toUpperCase is done.
                    //only one argument allowed
                    if (args.length != 1) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando DATA: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command DATA: wrong number of arguments");
                    }
                    temp = new StringBuffer(resolveExpression(args[0], debug));
                    //number of characters must be even
                    if ((temp.length() % 2) != 0) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando DATA: falsche L�nge des Arguments '"+temp+"'");
                        else throw new PPExecutionException("DiagData.resolveExpression: command DATA: wrong length in argument '"+temp+"'");
                    }
                    pos = temp.length() - 2;
                    for (;pos > 0;pos=pos-2) {
                        temp.insert(pos,' ');
                    }
                    res = temp.toString().toUpperCase();
                    break;
                case 'U':
                    //{"UNDATA",      "U"},   //undo the operation of DATA. That means spaces are removed. Also a toUpperCase is done.
                    //only one argument allowed
                    if (args.length != 1) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando UNDATA: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command UNDATA: wrong number of arguments");
                    }
                    temp = new StringBuffer(resolveExpression(args[0], debug));
                    //only one argument allowed, number of characters must be even
                    if (((temp.length() + 1) % 3) != 0) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando UNDATA: falsche L�nge des Arguments '"+temp+"'");
                        else throw new PPExecutionException("DiagData.resolveExpression: command UNDATA: wrong length in argument '"+temp+"'");
                    }
                    pos = temp.length() - 3;
                    for (;pos > 0;pos=pos-3) {
                        if (temp.charAt(pos) != ' ') {
                            if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando UNDATA: Falsches Format des Arguments: '"+temp+"'");
                            else throw new PPExecutionException("DiagData.resolveExpression: command UNDATA: wrong format of argument: '"+temp+"'");
                        }
                        temp.delete(pos, pos+1);
                    }
                    res = temp.toString().toUpperCase();
                    break;
                case 'E':
                    //{"EQUALS",      "E"},   //Compares the two arguments
                    //only two arguments allowed
                    if (args.length != 2) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando EQUALS: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command EQUALS: wrong number of arguments");
                    }
                    if (resolveExpression(args[0], debug).equals(resolveExpression(args[1], debug))) {
                        res = DEF_TRUE;
                    } else {
                        res = DEF_FALSE;
                    }
                    break;
                case 'A':
                    //{"AND",         "A"},   //All Arguments have to be DEF_TRUE
                    //at least two arguments needed
                    if (args.length < 2) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando AND: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command AND: wrong number of arguments");
                    }
                    res = DEF_TRUE;     //Default res is True
                    //loop over all args, stop if res == RES_FALSE
                    for (i=0;(i < args.length) && (res.equals(DEF_TRUE));i++) {
                        if (!resolveExpression(args[i], debug).equals(DEF_TRUE)) res = DEF_FALSE;    //If one is not DEF_TRUE the whole expression is DEF_FALSE
                    }
                    break;
                case 'O':
                    //{"OR",          "O"},   //One Argument has to be DEF_TRUE
                    //at least two arguments needed
                    if (args.length < 2) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando OR: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command OR: wrong number of arguments");
                    }
                    res = DEF_FALSE;     //Default res is True
                    //loop over all args, stop if res == RES_TRUE
                    for (i=0;(i < args.length) && (res.equals(DEF_FALSE));i++) {
                        if (resolveExpression(args[i], debug).equals(DEF_TRUE)) res = DEF_TRUE;    //If one is DEF_TRUE the whole expression is DEF_TRUE
                    }
                    break;
                case 'N':
                    //{"NOT",         "N"},   //The Argument is converted from DEF_TRUE to DEF_FALSE and from DEF_FALSE to DEF_TRUE
                    //only one argument allowed
                    if (args.length != 1) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando NOT: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command NOT: wrong number of arguments");
                    }
                    res = resolveExpression(args[0], debug);    //resolve expression
                    if (res.equals(DEF_TRUE)) {
                        res = DEF_FALSE;
                    } else if (res.equals(DEF_FALSE)) {
                        res = DEF_TRUE;
                    } else {
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando NOT: Arfuments ist nicht boolsch");
                        else throw new PPExecutionException("DiagData.resolveExpression: command NOT: input is not boolean");
                    }
                    break;
                case 'C':
                    //{"EQUALSIC",    "C"},   //Does the same as EQUALS, but the comparison is not case sensitive.
                    //only two arguments allowed
                    if (args.length != 2) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando EQUALSIC: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command EQUALSIC: wrong number of arguments");
                    }
                    if (resolveExpression(args[0], debug).equalsIgnoreCase(resolveExpression(args[1], debug))) {
                        res = DEF_TRUE;
                    } else {
                        res = DEF_FALSE;
                    }
                    break;
                case 'K':
                    //{"TOLOWER",     "K"},   //Converts the argument to lower case
                    //only one argument allowed
                    if (args.length != 1) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando TOLOWER: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command TOLOWER: wrong number of arguments");
                    }
                    res = resolveExpression(args[0], debug);    //resolve expression
                    res = res.toLowerCase();
                    break;
                case 'T':
                    //{"TOUPPER",     "T"},   //Converts the argument to upper case
                    //only one argument allowed
                    if (args.length != 1) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando TOUPPER: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command TOUPPER: wrong number of arguments");
                    }
                    res = resolveExpression(args[0], debug);    //resolve expression
                    res = res.toUpperCase();
                    break;
                case 'L':
                    //{"LESSTHAN",    "L"},   //the first argument has to be smaller than the second argument
                    //{"LESS",        "L"},   //the first argument has to be smaller than the second argument
                    //only two arguments allowed
                    if (args.length != 2) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando LESSTHAN: falsche Anzahl Argumente");
                        throw new PPExecutionException("DiagData.resolveExpression: command LESSTHAN: wrong number of arguments");
                    }
                    i = compare(resolveExpression(args[0],debug), resolveExpression(args[1],debug));          //see method compare for details
                    if ((i==1) || (i==3)) res = DEF_TRUE;
                    else res = DEF_FALSE;
                    break;
                case 'G':
                    //{"GREATERTHAN", "G"},   //the first argument has to be greater than the second argument
                    //{"GREATER",     "G"},   //the first argument has to be greater than the second argument
                    //only two arguments allowed
                    if (args.length != 2) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando GREATERTHAN: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command GREATERTHAN: wrong number of arguments");
                    }
                    i = compare(resolveExpression(args[0],debug), resolveExpression(args[1],debug));          //see method compare for details
                    if ((i==2) || (i==4)) res = DEF_TRUE;
                    else res = DEF_FALSE;
                    break;
                case 'X':
                    //{"LESSOREQ",    "X"},   //the first argument has to be smaller or equal than the second argument
                    //only two arguments allowed
                    if (args.length != 2) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando LESSOREQ: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command LESSOREQ: wrong number of arguments");
                    }
                    i = compare(resolveExpression(args[0],debug), resolveExpression(args[1],debug));          //see method compare for details
                    if ((i==0) || (i==1) || (i==3)) res = DEF_TRUE;
                    else res = DEF_FALSE;
                    break;
                case 'Y':
                    //{"GREATEROREQ", "Y"},   //the first argument has to be greater or equal than the second argument
                    //only two arguments allowed
                    if (args.length != 2) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando GREATEROREQ: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command GREATEROREQ: wrong number of arguments");
                    }
                    i = compare(resolveExpression(args[0],debug), resolveExpression(args[1],debug));          //see method compare for details
                    if ((i==0) || (i==2) || (i==4)) res = DEF_TRUE;
                    else res = DEF_FALSE;
                    break;
                case 'B':
                    //{"BETWEEN",     "B"},   //first argument smaller second argument smaller third argument a1 < a2 < a3
                    //only three arguments allowed
                    if (args.length != 3) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando BETWEEN: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command BETWEEN: wrong number of arguments");
                    }
                    args[0] = resolveExpression(args[0], debug);
                    args[1] = resolveExpression(args[1], debug);
                    args[2] = resolveExpression(args[2], debug);
                    i = compare(args[0], args[1]);          //see method compare for details
                    j = compare(args[1], args[2]);
                    if (((i==1) || (i==3)) && ((j==1) || (j==3))) res = DEF_TRUE;
                    else res = DEF_FALSE;
                    break;
                case 'Z':
                    //{"BETWEENIB",   "Z"}};  //between including boundaries a1 <= a2 <= a3
                    //only three arguments allowed
                    if (args.length != 3) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando BETWEENIB: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command BETWEENIB: wrong number of arguments");
                    }
                    args[0] = resolveExpression(args[0], debug);
                    args[1] = resolveExpression(args[1], debug);
                    args[2] = resolveExpression(args[2], debug);
                    i = compare(args[0], args[1]);          //see method compare for details
                    j = compare(args[1], args[2]);
                    if (((i==1) || (i==3) || (i==0)) && ((j==1) || (j==3) || (j==0))) res = DEF_TRUE;
                    else res = DEF_FALSE;
                    break;
                case 'S':
                    //{"SUBSTR",      "S"},   //extract a substring of the first argument. The second argument determines the beginning. The third determines the length if present.
                    //{"SUBSTRING",   "S"}};  //extract a substring of the first argument. The second argument determines the beginning. The third determines the length if present.
                    //only two or three arguments allowed
                    if ((args.length != 3) && (args.length != 2)) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando SUBSTRING: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command SUBSTRING: wrong number of arguments");
                    }
                    args[0] = resolveExpression(args[0], debug);
                    args[1] = resolveExpression(args[1], debug);
                    try {
                        idx1 = Integer.parseInt(args[1]);
                        if (idx1 < 0) {
                            if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando SUBSTRING: Position '"+args[1]+"' ist keine zul�ssige Zahl");
                            else throw new PPExecutionException("DiagData.resolveExpression: command SUBSTRING: position '"+args[1]+"' is not a valid number");
                        }
                    } catch (NumberFormatException nfe) {
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando SUBSTRING: Position '"+args[1]+"' ist keine zul�ssige Zahl");
                        else throw new PPExecutionException("DiagData.resolveExpression: command SUBSTRING: position '"+args[1]+"' is not a valid number");
                    }
                    if (args.length == 3) {     //extract length
                        args[2] = resolveExpression(args[2], debug);
                        try {
                            i = Integer.parseInt(args[2]);
                            if (i <= 0) {
                                if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando SUBSTRING: L�nge '"+args[1]+"'ist keine zul�ssige Zahl");
                                else throw new PPExecutionException("DiagData.resolveExpression: command SUBSTRING: length '"+args[1]+"'is not a valid number");
                            }
                        } catch (NumberFormatException nfe) {
                            if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando SUBSTRING: L�nge '"+args[1]+"'ist keine zul�ssige Zahl");
                            else throw new PPExecutionException("DiagData.resolveExpression: command SUBSTRING: length '"+args[1]+"'is not a valid number");
                        }
                    } else {
                        i = args[0].length()-idx1;  //default use the max length
                    }
                    //so now we have the string args[0], the position idx1 and the length i
                    //check if position and length are valid
                    j = args[0].length();           //put the remaining length to j
                    j = j-idx1;                     //substract the startposition from remaining length
                    if (j <= 0) {
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando SUBSTRING: Position '"+args[1]+"' ist unzul�ssig");
                        else throw new PPExecutionException("DiagData.resolveExpression: command SUBSTRING: position '"+args[1]+"' is out of range");
                    }
                    j = j - i;                      //substract the length from remaining length
                    if (j < 0) {
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando SUBSTRING: L�nge '"+args[1]+"'ist keine zul�ssige Zahl");
                        else throw new PPExecutionException("DiagData.resolveExpression: command SUBSTRING: length '"+args[1]+"'is out of range");
                    }
                    res = args[0].substring(idx1,idx1+i);
                    break;
                case 'P':
                    //{"HEAD",        "P"},   //extract a heading substring of the first argument. The second argument determines the length.
                    //only two arguments allowed
                    if (args.length != 2) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando HEAD: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command HEAD: wrong number of arguments");
                    }
                    res = resolveExpression("SUBSTRING("+args[0]+",0,"+args[1]+")", debug);
                    break;
                case 'Q':
                    //{"TAIL",        "Q"},   //extract a tailing substring of the first argument. The second argument determines the length.
                    //only two arguments allowed
                    if (args.length != 2) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando TAIL: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command TAIL: wrong number of arguments");
                    }
                    args[0] = resolveExpression(args[0], debug);
                    args[1] = resolveExpression(args[1], debug);
                    try {
                        idx1 = Integer.parseInt(args[1]);
                        if (idx1 < 0) {
                            if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando TAIL: Position '"+args[1]+"' ist keine zul�ssige Zahl");
                            else throw new PPExecutionException("DiagData.resolveExpression: command TAIL: position '"+args[1]+"' is not a valid number");
                        }
                    } catch (NumberFormatException nfe) {
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando TAIL: Position '"+args[1]+"' ist keine zul�ssige Zahl");
                        else throw new PPExecutionException("DiagData.resolveExpression: command TAIL: position '"+args[1]+"' is not a valid number");
                    }
                    res = resolveExpression("SUBSTRING("+args[0]+","+(args[0].length()-idx1)+","+args[1]+")", debug);
                    break;
                case 'R':
                    //{"REVERSE",     "R"},   //The Argument String is Reversed
                    //only one argument allowed
                    if (args.length != 1) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando REVERSE: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command REVERSE: wrong number of arguments");
                    }
                    temp = new StringBuffer(resolveExpression(args[0], debug));
                    res = temp.reverse().toString();
                    break;
                case 'V':
                    //{"LENGTH",      "V"},   //Returns the length of the Argument
                    //{"LEN",         "V"}};  //Returns the length of the Argument
                    //only one argument allowed
                    if (args.length != 1) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando LENGTH: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command LENGTH: wrong number of arguments");
                    }
                    args[0] = resolveExpression(args[0], debug);
                    res = ""+args[0].length();
                    break;
                case 'J':
                    //{"CONCAT",      "J"},   //concatenates the arguments in the given order
                    //at least two arguments needed
                    if (args.length < 2) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando CONCAT: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command CONCAT: wrong number of arguments");
                    }
                    temp = new StringBuffer();
                    //loop over all args
                    for (i=0;i < args.length;i++) {
                        temp.append(resolveExpression(args[i], debug));
                    }
                    res = temp.toString();
                    break;
                case '1':
                    //{"TRIM",        "1"},   //remove all leading and ending spaces
                    //only one argument allowed
                    if (args.length != 1) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando TRIM: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command TRIM: wrong number of arguments");
                    }
                    res = resolveExpression(args[0], debug).trim();
                    break;
                case 'M':
                    //{"MINUS",       "M"},   //Substract the second argument from the first one
                    //only two arguments allowed
                    if (args.length != 2) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando MINUS: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command MINUS: wrong number of arguments");
                    }
                    args[0] = resolveExpression(args[0], debug);
                    args[1] = resolveExpression(args[1], debug);
                    try {
                        long1 = Long.parseLong(args[0]);
                    } catch (NumberFormatException nfe) {
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando MINUS: '"+args[0]+"' ist keine zul�ssige Zahl");
                        else throw new PPExecutionException("DiagData.resolveExpression: command MINUS: '"+args[0]+"' is not a valid number");
                    }
                    try {
                        long2 = Long.parseLong(args[1]);
                    } catch (NumberFormatException nfe) {
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando MINUS: '"+args[1]+"' ist keine zul�ssige Zahl");
                        else throw new PPExecutionException("DiagData.resolveExpression: command MINUS: '"+args[1]+"' is not a valid number");
                    }
                    res = ""+(long1-long2);
                    break;
                case 'F':
                    //{"PLUS",        "F"}};  //add both arguments
                    //only two arguments allowed
                    if (args.length != 2) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando PLUS: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command PLUS: wrong number of arguments");
                    }
                    long2 = 0;                       //use long2 to sum up all args
                    //loop over all args
                    for (i=0;i < args.length;i++) {
                        args[i] = resolveExpression(args[i], debug);
                        try {
                            long1 = Long.parseLong(args[i]);
                        } catch (NumberFormatException nfe) {
                            if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando PLUS: '"+args[i]+"' ist keine zul�ssige Zahl");
                            else throw new PPExecutionException("DiagData.resolveExpression: command PLUS: '"+args[i]+"' is not a valid number");
                        }
                        long2 = long2 + long1;
                    }
                    res=  ""+long2;
                    break;
                case '3':
                    //{"IF",          "3"},   //IF_THEN_ELSE: first argument is the condition (DEF_TRUE/DEF_FALSE), the second is the result in case DEF_TRUE, the third is the result in case DEF_FALSE.
                    //{"IFELSE",      "3"},   //IF_THEN_ELSE: first argument is the condition (DEF_TRUE/DEF_FALSE), the second is the result in case DEF_TRUE, the third is the result in case DEF_FALSE.
                    //exact 3 arguments needed
                    if (args.length != 3) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando IFELSE: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command IFELSE: wrong number of arguments");
                    }
                    //Resolve Condition
                    args[0] = resolveExpression(args[0], debug);
                    if (args[0].equals(DEF_TRUE)) {
                        //IF-Part
                        res = args[0] = resolveExpression(args[1], debug);
                    } else if (args[0].equals(DEF_FALSE)) {
                        //ELSE-Part
                        res = args[0] = resolveExpression(args[2], debug);
                    } else {
                        //Error
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando IFELSE: '"+args[0]+"' ist keine zul�ssige Bedingung");
                        else throw new PPExecutionException("DiagData.resolveExpression: command IFELSE: '"+args[0]+"' is not a valid condition");
                    }
                    break;
                case '4':
                    //{"SWITCH",      "4"}};  //SWITCH: first argument is throws input, the last argument is the default result, between are pairs of comparevalue and result in case input equals comparevalue.
                    //at least 4 arguments needed, even numer of arguments
                    if ((args.length < 4) || ((args.length % 2) != 0)) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando SWITCH: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command SWITCH: wrong number of arguments");
                    }
                    res = null;
                    //Resolve Input
                    args[0] = resolveExpression(args[0], debug);
                    //loop over (compareValue,Result)-pairs
                    for (i=1;i<(args.length - 1);i=i+2) {
                        //Resolve compareValue
                        args[i] = resolveExpression(args[i], debug);
                        //is input equal compareValue
                        if (args[0].equals(args[i])) {
                            res = resolveExpression(args[i+1], debug);
                            break;
                        }
                    }
                    if (res == null) res = resolveExpression(args[args.length - 1], debug); //default is last argument
                    break;
                case '5':
                    //{"HEX2ASC",     "5"},   //Converts the Input to ASCII. Two chars input are converted to one char output.
                    //{"HEX2ASCII",   "5"}};  //Converts the Input to ASCII. Two chars input are converted to one char output.
                    //exact 1 arguments needed
                    if ((args.length != 1)) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando HEX2ASCII: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command HEX2ASCII: wrong number of arguments");
                    }
                    //Resolve Input
                    args[0] = resolveExpression(args[0], debug);
                    //exact 1 arguments needed
                    if (((args[0].length() % 2) != 0) || (args[0].length() < 2)) {
                        //wrong number of characters in input
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando HEX2ASCII: falsche Anzahl Zeichen im Argument: '"+args[0]+"'");
                        else throw new PPExecutionException("DiagData.resolveExpression: command HEX2ASCII: wrong number of characters in input: '"+args[0]+"'");
                    }
                    //use StringBuffer temp for building up the result
                    temp = new StringBuffer();
                    for (i=0; i < args[0].length(); i=i+2) {
                        char c;
                        try {
                            j = Integer.parseInt(args[0].substring(i,i+2),16);  //Parse two chars hex to integer
                            c = (char)j;                                   //convert the integer j to the corresponding ascii-char
                            if (j<10) {
                                //no normal character: steering command e.g. linefeed
                                //throw new PPExecutionException("DiagData.resolveExpression: command HEX2ASCII: Illegal Character: '"+args[0]+"'");
                                c = '?';    //Kein Abbruch, falls Konvertierung fehlgeschlagen
                            }
                        } catch(Exception n) {
                            //throw new PPExecutionException("DiagData.resolveExpression: command HEX2ASCII: Illegal argument: '"+args[0]+"'");
                            c = '?';    //Kein Abbruch, falls Konvertierung fehlgeschlagen
                        }
                        temp.append(c);
                    }
                    res = temp.toString();
                    break;
                case '6':
                    //{"FILLLEFT",    "6"},   //Fills up the given String (arg1) to a certain length (arg2) by adding a given String (arg3) in front.
                    //only three arguments allowed
                    if (args.length != 3) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando FILLLEFT: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command FILLLEFT: wrong number of arguments");
                    }
                    //resolve args
                    args[0] = resolveExpression(args[0], debug);    //Input
                    args[1] = resolveExpression(args[1], debug);    //Length
                    args[2] = resolveExpression(args[2], debug);    //Fillstring/FillCharacter
                    try {
                        idx1 = Integer.parseInt(args[1]);           //Parse length
                        if (idx1 <= 0) {
                            if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando FILLLEFT: L�nge '"+args[1]+"' ist keine zul�ssige Zahl");
                            else throw new PPExecutionException("DiagData.resolveExpression: command FILLLEFT: length '"+args[1]+"' is not a valid number");
                        }
                    } catch (NumberFormatException nfe) {
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando FILLLEFT: L�nge '"+args[1]+"' ist keine zul�ssige Zahl");
                        else throw new PPExecutionException("DiagData.resolveExpression: command FILLLEFT: length '"+args[1]+"' is not a valid number");
                    }
                    if (args[2].length() <= 0) {
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando FILLLEFT: F�ll-Text/Zeichen '"+args[2]+"' ist kein zul�ssiger Text/Zeichen");
                        else throw new PPExecutionException("DiagData.resolveExpression: command FILLLEFT: fill string/character '"+args[2]+"' is not a valid fill string/character");
                    }
                    temp = new StringBuffer(args[0]);   //use a stringBuffer to be able to insert strings
                    //so now we have the string temp, the length idx1 and the FillCharacter args[2]
                    while (temp.length() < idx1) {
                        //expand the string, if desired length is not already reached
                        temp.insert(0,args[2]);
                    }
                    res = temp.toString();
                    break;
                case '7':
                    //{"FILLRIGHT",   "7"}};  //Fills up the given String (arg1) to a certain length (arg2) by adding a given String (arg3) at the end.
                    //only three arguments allowed
                    if (args.length != 3) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando FILLRIGHT: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command FILLRIGHT: wrong number of arguments");
                    }
                    //resolve args
                    args[0] = resolveExpression(args[0], debug);    //Input
                    args[1] = resolveExpression(args[1], debug);    //Length
                    args[2] = resolveExpression(args[2], debug);    //Fillstring/FillCharacter
                    try {
                        idx1 = Integer.parseInt(args[1]);           //Parse length
                        if (idx1 <= 0) {
                            if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando FILLRIGHT: L�nge '"+args[1]+"' ist keine zul�ssige Zahl");
                            else throw new PPExecutionException("DiagData.resolveExpression: command FILLRIGHT: length '"+args[1]+"' is not a valid number");
                        }
                    } catch (NumberFormatException nfe) {
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando FILLRIGHT: L�nge '"+args[1]+"' ist keine zul�ssige Zahl");
                        else throw new PPExecutionException("DiagData.resolveExpression: command FILLRIGHT: length '"+args[1]+"' is not a valid number");
                    }
                    if (args[2].length() <= 0) {
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando FILLRIGHT: F�ll-Text/Zeichen '"+args[2]+"' ist kein zul�ssiger Text/Zeichen");
                        else throw new PPExecutionException("DiagData.resolveExpression: command FILLRIGHT: fill string/character '"+args[2]+"' is not a valid fill string/character");
                    }
                    temp = new StringBuffer(args[0]);   //use a stringBuffer to be able to insert strings
                    //so now we have the string temp, the length idx1 and the FillCharacter args[2]
                    while (temp.length() < idx1) {
                        //expand the string, if desired length is not already reached
                        temp.append(args[2]);
                    }
                    res = temp.toString();
                    break;
                case '8':
                    //{"CONTAINS",    "8"}};  //Checks if Arg1 is contained in the list of Arg2, ... Argn. Returns DEF_TRUE when contained, else DEF_FALSE.
                    //at least two arguments needed
                    if (args.length < 2) {
                        //wrong number of arguments
                        if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Kommando CONTAINS: falsche Anzahl Argumente");
                        else throw new PPExecutionException("DiagData.resolveExpression: command CONTAINS: wrong number of arguments");
                    }
                    res = DEF_FALSE;    //By default not contained
                    args[0] = resolveExpression(args[0], debug);    //Input
                    //loop over all args
                    for (i=1;i < args.length;i++) {
                        if (args[0].equals(resolveExpression(args[i], debug))) {
                            res = DEF_TRUE;
                            break;
                        }
                    }
                    break;
                default:
                    if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: nicht unterst�tztes Kommando: '"+command+"'");
                    else throw new PPExecutionException("DiagData.resolveExpression: command not supported yet: '"+command+"'");
            }
            if (debug > 1) System.out.println("DiagData.resolveExpression: output='"+res+"'");
            return res;
        } else {
            if (debug > 0) System.out.println("DiagData.resolveExpression: error in expression: '"+input+"'");
            if (isDE()) throw new PPExecutionException("DiagData.resolveExpression: Fehler im Ausdruck: '"+input+"'");
            else throw new PPExecutionException("DiagData.resolveExpression: error in expression: '"+input+"'");
        }
    }
    
    /**
     * spiltExpression
     */
    private static String[] splitExpression(String input,int debug) throws PPExecutionException {
        StringBuffer res = new StringBuffer();
        List<String> parts = new LinkedList<String>();
        StringTokenizer tokenizer = new StringTokenizer(input,",()",true); //create the stringtokenizer, split-chars are returned as tokens
        String currentToken = null;     //for storing the current token
        int depth = 0;
        if (debug > 2) System.out.println("DiagData.splitExpression: input = '"+input+"'");
        while (tokenizer.hasMoreTokens()) {
            currentToken = tokenizer.nextToken();
            if (currentToken.equals("(")) {
                //'(' detected
                //'(' not allowed when res is still empty
                if (res.length() == 0) {
                    if (isDE()) throw new PPExecutionException("DiagData.splitExpression: Fehler im Ausdruck: '"+input+"'");
                    else throw new PPExecutionException("DiagData.splitExpression: error in expression: '"+input+"'");
                }
                depth++;    //depth + 1 , one '(' detected
                res.append(currentToken);
            } else if (currentToken.equals(")")) {
                //')' detected
                //')' not allowed when depth is 0
                if (depth == 0) {
                    if (isDE()) throw new PPExecutionException("DiagData.splitExpression: Fehler im Ausdruck: '"+input+"'");
                    else throw new PPExecutionException("DiagData.splitExpression: error in expression: '"+input+"'");
                }
                depth--;    //depth - 1 , one ')' detected
                res.append(currentToken);
            } else if (currentToken.equals(",")) {
                //',' detected
                if (depth == 0) {
                    //part is complete
                    parts.add(res.toString());      //append res to parts
                    res.setLength(0);               //clear buffer
                } else {
                    //part is not comlete
                    res.append(currentToken);
                }
            } else {
                //other
                res.append(currentToken);
            }
        }
        //res must contain the last part:    NEW:Empty Strings cause problems, EQUALS(%R1%,) fails!!!!
        //if (res.length() == 0) {
        //    throw new PPExecutionException("DiagData.splitExpression: error in expression: '"+input+"'");
        //}
        //part is complete
        parts.add(res.toString());      //append res to parts
        //put all parts into an array
        String temp[] = new String[parts.size()];
        Iterator<String> iter = parts.iterator();
        for (int i = 0; i < temp.length;i++) {
            temp[i] = (String)iter.next();
        }
        return temp;
        
    }
    
    /**
     * Returns true if the Vector input contains results (Ergebnis) that are NIO or NIO_SYS
     */
    private static boolean containsError(Vector<Ergebnis> input) {
        Iterator<Ergebnis> iter = input.iterator();
        while (iter.hasNext()) {
            Object co = iter.next();
            if (co instanceof Ergebnis) {
                if (((Ergebnis)co).getFehlerTyp().equalsIgnoreCase(Ergebnis.FT_NIO) ||
                ((Ergebnis)co).getFehlerTyp().equalsIgnoreCase(Ergebnis.FT_NIO_SYS)) return true;   //error detected
            }
        }
        return false;   //by default no error
    }
    
    /**
     * Sets all NIO- and NIOSYS-Results in the input-vector to Ignore.
     */
    private static void filterErrors(Vector<Ergebnis> input) {
        Iterator<Ergebnis> iter = input.iterator();
        while (iter.hasNext()) {
            Object co = iter.next();
            if (co instanceof Ergebnis) {
                if (((Ergebnis)co).getFehlerTyp().equalsIgnoreCase(Ergebnis.FT_NIO) ||
                ((Ergebnis)co).getFehlerTyp().equalsIgnoreCase(Ergebnis.FT_NIO_SYS)) {
                    //error detected
                    ((Ergebnis)co).setFehlerTyp(Ergebnis.FT_IGNORE);        //Set to ignore (F and S become A)
                }
            }
        }
    }
    
    /**
     * Prints all contained Errors in short form
     */
    private static void printResults(Vector<Ergebnis> input) {
        Iterator<Ergebnis> iter = input.iterator();
        int i = 1;
        while (iter.hasNext()) {
            Object co = iter.next();
            if (co instanceof Ergebnis) {
                Ergebnis erg = (Ergebnis)co;
                if (((Ergebnis)co).getFehlerTyp().equalsIgnoreCase(Ergebnis.FT_NIO) ||
                ((Ergebnis)co).getFehlerTyp().equalsIgnoreCase(Ergebnis.FT_NIO_SYS)) {
                    if (isDE()) System.out.println("ERGEBNIS"+i+": NIO ID='"+erg.getID()+"' TYP='"+erg.getFehlerTyp()+"'");
                    else System.out.println("ERGEBNIS"+i+": NIO ID='"+erg.getID()+"' TYPE='"+erg.getFehlerTyp()+"'");
                } else {
                    if (isDE()) System.out.println("ERGEBNIS"+i+": IO ID='"+erg.getID()+"' TYP='"+erg.getFehlerTyp()+"'");
                    else System.out.println("ERGEBNIS"+i+": IO ID='"+erg.getID()+"' TYPE='"+erg.getFehlerTyp()+"'");
                }
            } else {
                if (isDE()) System.out.println("ERGEBNIS"+i+": Unbekannte Java-Klasse "+co);
                else System.out.println("ERGEBNIS"+i+": Unknown Java-CLASS "+co);
            }
            i++;
        }
    }
    
    /**
     *  remoteBreakpoint tries to connect to socket 7575. If succesfull, it sends content of awt through the socket.
     *  Once it then receieves at least one byte, it continues and return true.  
     * @param awt String to send through the socket
     * @return true: Connenction was successfull and at least one byte was received (continue)
     *         false: Connection could not be established.
     */
    private boolean remoteBreakpoint(String awt)
    {
        int i;
        Socket BPSocket;
        InetSocketAddress Addr;
        byte Outbuffer[];
        byte Inbyte;
        
        if (debug > 1) System.out.println("remoteBreakpoint: Creating Socket");
        BPSocket=new Socket();
        if (debug > 1) System.out.println("remoteBreakpoint: Creating local Address");
        Addr=new InetSocketAddress("127.0.0.1", 7575);
        Outbuffer=new byte[1024];
        for (i=0; i<awt.length() && i<Outbuffer.length; i++) Outbuffer[i]=(byte)(awt.charAt(i));
        for (i=awt.length(); i<Outbuffer.length; i++) Outbuffer[i]=0;
        
        // Look for local RemoteBPServer
        try
        {
            if (debug > 1)System.out.println("remoteBreakpoint: Connect");
            BPSocket.connect(Addr, 500);
            if (debug > 1)System.out.println("remoteBreakpoint: Write Output");
            BPSocket.getOutputStream().write(Outbuffer);
        }
        catch (IOException ex)
        {
            // No server
            if (debug > 1)System.out.println("remoteBreakpoint: failed");
            try {
				BPSocket.close();
			} catch( IOException e ) {
			}
            return false;       
        }    

        // Server found. If Server sends, this is signal to continue
        if (debug > 1) System.out.println("remoteBreakpoint: Wait for response");
        try
        {
            while (BPSocket.getInputStream().available()<1)
            {
                try
                {
                    Thread.sleep(50);
                }
                catch (InterruptedException e) { }
            }
            
            if (debug > 1) System.out.println("remoteBreakpoint: Byte received");
            Inbyte=(byte)(BPSocket.getInputStream().read());
        }
        catch (IOException ex)
        {
            if (debug > 1) System.out.println("remoteBreakpoint: Exception");
        }
        
        if (debug > 1) System.out.println("remoteBreakpoint: Close socket");
        try
        {
            BPSocket.close();
        }
        catch (IOException e)
        {
            if (debug > 1) System.out.println("remoteBreakpoint: IOException");
        }
        
        return true;
    }

    
    /**
     * Liefert true zur�ck, wenn deutsche Texte verwendet werden sollen.
     * @result True bei deutsche texte, false sonst.
     */
    private static boolean isDE() {
        try {
            if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true ) return true;
            else return false;
        } catch (Exception e) {
            return false;   //default is english
        }
    }
    
    //Private Klassen-Definitionen
    //*************************************************
    
    private class JobExecution {
        
        /**
         * possible conditionModes
         * AND,OR,NOR
         */
        final String ConditionModes[][] = {{"AND","A"},{"OR","O"},{"NOR","N"}};
        
        /**
         * initial capacity of all Vectors
         */
        static final int INITIAL_CAPACITY = 20;
        
        /**
         * conditionState for all Conditions OK
         */
        static final int STATE_IO = -3;
        /**
         * conditionState for at least one condition failed
         */
        static final int STATE_NIO = -1;
        /**
         * conditionState for error during job-execution
         */
        static final int STATE_NIOSYS= -2;
        /**
         * conditionState for conditions not applied yet or job not executed yet
         */
        static final int STATE_UNKNOWN=-4;
        
        //Instanzvariablen
        //common values
        private String Job;     //Ediabas-Job to be executed
        private String Sgbd;    //Sgbd which contains Job
        private String Par;     //List of parameters seperated by ';'
        private boolean HidePar;    //If true the parameters will not be contained in the Cascade-Results
        //Values for each Result 1...K. Index 0 is never used!!!
        private Vector Result;        //[String] Name of the Result
        private Vector HideResult;   //[boolean] If true the value of the Result will not be contained in the Cascade-Results
        private Vector ResultValue;   //[String] After execution the values are stored here
        //Values for each Condition 1...L. Index 0 is never used!!!
        private Vector Condition;         //[String] Boolean expression
        private Vector Hwt;               //[String] Hwt if the expression results in false
        private Vector ConditionState;  //[boolean] True if the condition is true, default false
        //Values for each Value that should be stored 1...M. Index 0 is never used!!!
        private Vector StorageName;       //[String] Name of the Dynamic Attribute
        private Vector StorageFormat;     //[String] Conversion-algorithm executed before storing
        //Other common values
        private String DefaultHwt;          //Default HWT
        private char ConditionMode;     //If OR only one condition has to be true
        private int OverallConditionState;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
        
        //additional settings not relevant for methods in JobExecution itself
        private long PauseBefore;   //defines the pause before the job is executed. Default: 0
        private long PauseAfter;    //defines the pause after the job has been executed (also when NIO or NIOSYS). Default: 0
        private int JumpIo;                 //defines where to jump when the status for the job is IO. Default: not defined
        private int JumpNioDefault;               //defines where to jump (default) when the status for the job is NIO. Default: exit nio
        private Vector JumpNio;         //[Integer] defines where to jump when condition l is responsible for NIO. Default: not defined
        private int JumpNioSys;         //defines where to jump when the status for the job is NIOSYS. Default: not defined
        private String QwtNioDefault;            //defines a QWT-message (default) when the status for the job is NIO. Default: not defined
        private Vector QwtNio;            //[String] defines a QWT-message (default) when condition l is responsible for NIO. Default: not defined
        private String QwtNioSys;      //defines a QWT-message when the status for the job is NIOSYS. Default: not defined
        private String Iwt;                    //defines a IWT-message displayed during and since the execution of this job. Default: not defined
        private String Breakpoint; // defines a string to display (or send over TCP) for this breakpoint. If String is null, the job does not have a breakpoint
        
        //Hashtable for storing pattern/newValue-Pairs for substitutions
        private Hashtable Substitutions;
        
        /**
         * Konstruktor
         */
        JobExecution() {
            setupJobExecution(null);
        }
        
        JobExecution(JobExecution base) {
            setupJobExecution(base);
        }
        
        void setupJobExecution(JobExecution base) {
            if (base != null) {
                //We have a template
                Job = base.Job;
                Sgbd = base.Sgbd;
                Par = base.Par;
                HidePar = base.HidePar;
                Result = (base.Result == null) ? null : (Vector)base.Result.clone();
                HideResult = (base.HideResult == null) ? null : (Vector)base.HideResult.clone();
                ResultValue = (base.ResultValue == null) ? null : (Vector)base.ResultValue.clone();
                Condition = (base.Condition == null) ? null : (Vector)base.Condition.clone();
                Hwt = (base.Hwt == null) ? null : (Vector)base.Hwt.clone();
                ConditionState = (base.ConditionState == null) ? null : (Vector)base.ConditionState.clone();
                StorageName = (base.StorageName == null) ? null : (Vector)base.StorageName.clone();
                StorageFormat = (base.StorageFormat == null) ? null : (Vector)base.StorageFormat.clone();
                DefaultHwt = base.DefaultHwt;
                ConditionMode = base.ConditionMode;
                OverallConditionState = base.OverallConditionState;
                Substitutions = new Hashtable();    //Hashtable for storing pattern/newValue-Pairs for substitutions
                Substitutions.putAll(base.Substitutions);   //Copy old hashtable
                
                //additional settings not relevant for methods in JobExecution itself
                PauseBefore = base.PauseBefore;   //defines the pause before the job is executed. Default: 0
                PauseAfter = base.PauseAfter;    //defines the pause after the job has been executed (also when NIO or NIOSYS). Default: 0
                JumpIo = base.JumpIo;                 //defines where to jump when the status for the job is IO. Default: not defined
                JumpNioDefault = base.JumpNioDefault;               //defines where to jump (default) when the status for the job is NIO. Default: exit nio
                JumpNio = base.JumpNio;         //[Integer] defines where to jump when condition l is responsible for NIO. Default: not defined
                JumpNioSys = base.JumpNioSys;         //defines where to jump when the status for the job is NIOSYS. Default: not defined
                QwtNioDefault = base.QwtNioDefault;            //defines a QWT-message (default) when the status for the job is NIO. Default: not defined
                QwtNio = base.QwtNio;            //[String] defines a QWT-message (default) when condition l is responsible for NIO. Default: not defined
                QwtNioSys = base.QwtNioSys;      //defines a QWT-message when the status for the job is NIOSYS. Default: not defined
                Iwt = base.Iwt;                    //defines a IWT-message displayed during and since the execution of this job. Default: not defined
                Breakpoint = base.Breakpoint;   // defines a string to display (or send over TCP) for this breakpoint. If String is null, the job does not have a breakpoint
                if (debug > 2) System.out.println("DiagData.JobExecution: new Object created with template");
            } else {
                //We have no template
                //common values
                Job = null;     //Ediabas-Job to be executed
                Sgbd = null;    //Sgbd which contains Job
                Par = null;     //List of parameters seperated by ';'
                HidePar = false;    //If true the parameters will not be contained in the Cascade-Results
                //Values for each Result 1...K
                Result = null;        //[String] Names of the Results
                HideResult = null;   //[Boolean] If true the value of the Result will not be contained in the Cascade-Results
                ResultValue = null;   //[String] After execution the values are stored here
                //Values for each Condition 1...L
                Condition = null;         //[String] Boolean expression
                Hwt = null;               //[String] Hwt if the expression results in false
                ConditionState = null;  //[Boolean] True if the condition is true, default false
                //Values for each Value that should be stored 1...M
                StorageName = null;       //[String] Name of the Dynamic Attribute
                StorageFormat = null;     //[String] Conversion-algorithm executed before storing
                //Other common values
                DefaultHwt = null;          //Default HWT
                ConditionMode = 'A';     //default "AND"
                OverallConditionState = STATE_UNKNOWN;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                Substitutions = new Hashtable();    //Hashtable for storing pattern/newValue-Pairs for substitutions
                
                //additional settings not relevant for methods in JobExecution itself
                PauseBefore = 0;   //defines the pause before the job is executed. Default: 0
                PauseAfter = 0;    //defines the pause after the job has been executed (also when NIO or NIOSYS). Default: 0
                JumpIo = DEF_JUMP_UNDEF;                 //defines where to jump when the status for the job is IO. Default: not defined
                JumpNioDefault = DEF_JUMP_UNDEF;               //defines where to jump (default) when the status for the job is NIO. Default: DEF_JUMP_UNDEF
                JumpNio = null;         //[Integer] defines where to jump when condition l is responsible for NIO. Default: not defined
                JumpNioSys = DEF_JUMP_UNDEF;         //defines where to jump when the status for the job is NIOSYS. Default: not defined
                QwtNioDefault = null;            //defines a QWT-message (default) when the status for the job is NIO. Default: not defined
                QwtNio = null;            //[String] defines a QWT-message (default) when condition l is responsible for NIO. Default: not defined
                QwtNioSys = null;      //defines a QWT-message when the status for the job is NIOSYS. Default: not defined
                Iwt = null;                    //defines a IWT-message displayed during and since the execution of this job. Default: not defined
                Breakpoint = null;   // defines a string to display (or send over TCP) for this breakpoint. If String is null, the job does not have a breakpoint
                if (debug > 2) System.out.println("DiagData.JobExecution: new Object created");
            }
        }
        
        @Override
		public Object clone() {
            return new JobExecution(this);
        }
        
        /**
         * Resolve Substitutions and Links
         * NOTE: no ';' allowed in input
         **/
        private String resolveSubstLinks(String input) throws PPExecutionException {
            if (input == null) return null;                       //nothing to do null
            if (input.length() == 0) return input;                //nothing to do empty string
            input = resolveSubstitutions(input, Substitutions);   //Substitute e.g. %N%
            input = resolveLinks(input);                          //resolve links @...@ and ...@...
            return input;
        }
        
        /**
         * Resolve Substitutions and Links and Expressions
         * NOTE: no ';' allowed in input
         **/
        private String resolveSubstLinksExpr(String input) throws PPExecutionException {
            if (input == null) return null;                       //nothing to do null
            if (input.length() == 0) return input;                //nothing to do empty string
            input = resolveSubstitutions(input, Substitutions);   //Substitute e.g. %N%
            input = resolveLinks(input);                          //resolve links @...@ and ...@...
            input = resolveExpression(input, debug);              //resolve expressions e.g. EQUALS(...,...)
            return input;
        }
        
        /**
         * Resolve Substitutions and Links and Expressions
         * NOTE: ';' are allowed in input.
         **/
        private String resolveSubstLinksExprWithSemicolon(String input) throws PPExecutionException {
            if (input == null) return null;                       //nothing to do null
            if (input.length() == 0) return input;                //nothing to do empty string
            input = resolveSubstitutions(input, Substitutions);   //Substitute e.g. %N%
            input = resolveLinks(input);                          //resolve links @...@ and ...@...
            //split input spearated by ';'
            String parts[] = splitArg(input);                 //split input into parts by ';'
            StringBuffer buff = new StringBuffer();
            //loop over all parts
            for (int j=0;j<parts.length;j++) {
                buff.append(resolveExpression(parts[j], debug));    //resolve expressions e.g. EQUALS(...,...)
                if (j!=(parts.length-1)) buff.append(';');
            }
            return buff.toString();
        }
        
        /**
         * execute the job
         */
        public void execute(Vector<Ergebnis> ergs) throws PPExecutionException {
            Ergebnis result;
            StringBuffer temp = new StringBuffer();
            String ediabasStatus,value;
            String internalSgbd=this.Sgbd;                                      //for storing the resolved sgbd
            String internalJob=this.Job;                                        //for storing the resolved job
            String internalPar=this.Par;                                        //for storing the resolved pars
            String internalResults[] = new String[getNumberOfResults() + 1];    //for storing the resolved resultnames
            String dokuPar=this.Par;                                            //parameters appearing in Ergebnis-Objects
            
            int i;
            if (debug > 0) System.out.println("DiagData.JobExecution.execute: start");
            if (debug > 2) System.out.println("DiagData.JobExecution.execute: Sgbd = '"+internalSgbd+"'");
            if (debug > 2) System.out.println("DiagData.JobExecution.execute: Job = '"+internalJob+"'");
            if (debug > 2) System.out.println("DiagData.JobExecution.execute: Par = '"+internalPar+"'");
            //handle substitutions and links
            internalSgbd = resolveSubstLinksExpr(internalSgbd);   //Resolve Substitutions and Links and Expressions
            internalJob = resolveSubstLinksExpr(internalJob);     //Resolve Substitutions and Links and Expressions
            internalPar = resolveSubstLinksExprWithSemicolon(internalPar);     //Resolve Substitutions and Links and Expressions
            if (debug > 1) System.out.println("DiagData.JobExecution.execute: resolved Sgbd = '"+internalSgbd+"'");
            if (debug > 1) System.out.println("DiagData.JobExecution.execute: resolved Job = '"+internalJob+"'");
            if (debug > 1) System.out.println("DiagData.JobExecution.execute: resolved Par = '"+internalPar+"'");
            
            //assign dokuPar
            if (this.HidePar) {
                //hide the parameters, so use SECRET PARAMETERS
                if (isDE()) dokuPar = "GEHEIMER PARAMETER";
                else dokuPar = "SECRET PARAMETERS";
            } else {
                //the parameters are not secret, so use internalPar
                dokuPar = internalPar;
            }
            
            //If SGBD is NONE and JOB is NONE, do nothing
            if ("NONE".equalsIgnoreCase(internalSgbd) && "NONE".equalsIgnoreCase(internalJob)) {
                if (getNumberOfResults() > 0) {
                    if (debug > 0) System.out.println("DiagData.JobExecution.execute: Result not allowed for JOB=NONE and SGBD=NONE");
                    if (isDE()) result = new Ergebnis( "ParaFehler", "", "NONE", "NONE", "", "", "", "", "", "0", "", "", "", PB.getString( "parameterexistenz" ), "Ergebnis Nich verf�gbar", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ParaError", "", "NONE", "NONE", "", "", "", "", "", "0", "", "", "", PB.getString( "parameterexistenz" ), "Result Not Available", Ergebnis.FT_NIO_SYS );
                    ergs.add(result);
                    //Set condition state to NIOSYS, cause the execution failed
                    OverallConditionState=STATE_NIOSYS;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                    if (isDE()) throw new PPExecutionException("DiagData.JobExecution.execute: Ergebnis Nich verf�gbar");
                    else throw new PPExecutionException("DiagData.JobExecution.execute: Result Not Available");
                } else if (internalPar != null) {
                    if (debug > 0) System.out.println("DiagData.JobExecution.execute: Parameters not allowed for JOB=NONE and SGBD=NONE");
                    if (isDE()) result = new Ergebnis( "ParaFehler", "", "NONE", "NONE", ""+((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "parameterexistenz" ), "Parameter sind nicht zul�ssig", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ParaError", "", "NONE", "NONE", ""+((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "parameterexistenz" ), "Parameters Not Allowed", Ergebnis.FT_NIO_SYS );
                    ergs.add(result);
                    //Set condition state to NIOSYS, cause the execution failed
                    OverallConditionState=STATE_NIOSYS;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                    if (isDE()) throw new PPExecutionException("DiagData.JobExecution.execute: Parameter sind nicht zul�ssig");
                    else throw new PPExecutionException("DiagData.JobExecution.execute: Parameters Not Allowed");
                } else {
                    OverallConditionState=STATE_UNKNOWN;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                    return;
                }
            }
            
            boolean jobStatusPresent = false;
            //collect all results as arg for ediabas
            for (i=1;i<=getNumberOfResults();i++) {
                internalResults[i] = resolveSubstLinksExpr(getResult(i));  //Resolve Substitutions and Links and Expressions
                if (i>1) temp.append(';');      //append ';'
                temp.append(internalResults[i]);      //append result i
                if (internalResults[i].equalsIgnoreCase("JOB_STATUS")) jobStatusPresent = true;
            }
            //append JOB_STATUS if not already present
            if (!jobStatusPresent && (temp.length() > 0)) {
                //JOB_STATUS not present
                temp.append(";JOB_STATUS");
            }
            //append JOB_STATUS if no results are queried
            if (temp.length() == 0) {
                //No results present use JOB_STATUS
                temp.append("JOB_STATUS");
            }
            if (debug > 1) System.out.println("DiagData.JobExecution.execute: Results = '"+temp+"'");
            
            //execute the job
            try {
            	getEdiabas( ergs, internalSgbd );
                ediabasStatus = ediabas.executeDiagJob(internalSgbd,internalJob,(internalPar!=null) ? internalPar : "",temp.toString());
                if (debug > 1) System.out.println("DiagData.JobExecution.execute: EdiabasStatus = '"+ediabasStatus+"'");
                if( ediabasStatus.equals("OKAY") == false) {
                    if (isDE()) result = new Ergebnis( "DiagFehler", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "JOB_STATUS", ediabasStatus, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabasStatus, Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "DiagError", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "JOB_STATUS", ediabasStatus, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabasStatus, Ergebnis.FT_NIO_SYS );
                    ergs.add(result);
                    //Set condition state to NIOSYS, cause the execution failed
                    OverallConditionState=STATE_NIOSYS;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                    if (isDE()) throw new PPExecutionException("DiagData.JobExecution.execute: DiagFehler Ediabasstatus = " + ediabasStatus);
                    else throw new PPExecutionException("DiagData.JobExecution.execute: DiagError Ediabasstatus = " + ediabasStatus);
                } else if (getNumberOfResults() == 0) {
                    result = new Ergebnis("Status", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "JOB_STATUS", ediabasStatus, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                    ergs.add(result);
                }
            } catch( ApiCallFailedException e ) {
                if (debug > 1) System.out.println("DiagData.JobExecution.execute: ApiCallFailedException = '"+e.getMessage()+"'");
                if (isDE()) result = new Ergebnis( "DiagFehler", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "DiagError", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
                ergs.add(result);
                //Set condition state to NIOSYS, cause the execution failed
                OverallConditionState=STATE_NIOSYS;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                if (isDE()) throw new PPExecutionException("DiagData.JobExecution.execute: DiagFehler apiErrorText = " + ediabas.apiErrorText());
                else throw new PPExecutionException("DiagData.JobExecution.execute: DiagError apiErrorText = " + ediabas.apiErrorText());
            } catch( EdiabasResultNotFoundException e ) {
                if (debug > 1) System.out.println("DiagData.JobExecution.execute: EdiabasResultNotFoundException = '"+e.getMessage()+"'");
                if (e.getMessage() != null) {
                    if (isDE()) result = new Ergebnis( "DiagFehler", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "DiagError", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                } else {
                    if (isDE()) result = new Ergebnis( "DiagFehler", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "DiagError", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
                }
                ergs.add(result);
                //Set condition state to NIOSYS, cause the execution failed
                OverallConditionState=STATE_NIOSYS;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                if (isDE()) throw new PPExecutionException("DiagData.JobExecution.execute: DiagFehler: " + e.getMessage());
                else throw new PPExecutionException("DiagData.JobExecution.execute: DiagError: " + e.getMessage());
            } catch (PPExecutionException e) {
                throw e;        //In Case JOB_STATUS is not OKAY
            } catch( Exception e) {
                if (debug > 1) System.out.println("DiagData.JobExecution.execute: other Exception = '"+e.getMessage()+"'");
                if (isDE()) result = new Ergebnis( "DiagFehler", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "FehlerCode: "+ediabas.apiErrorCode()+" FehlerText: " + ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "DiagError", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "ErrorCode: "+ediabas.apiErrorCode()+" ErrorText: " + ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
                ergs.add(result);
                //Set condition state to NIOSYS, cause the execution failed
                OverallConditionState=STATE_NIOSYS;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                if (isDE()) throw new PPExecutionException("DiagData.JobExecution.execute: DiagFehler: " + e.getMessage());
                else throw new PPExecutionException("DiagData.JobExecution.execute: DiagError: " + e.getMessage());
            }
            //check JOB_STATUS
            try {
                value = ediabas.getDiagResultValue("JOB_STATUS");
                if (debug > 1) System.out.println("DiagData.JobExecution.execute: Ediabas JOB_STATUS = '"+value+"'");
                if( value.equals("OKAY") == false) {
                    if (isDE()) result = new Ergebnis( "DiagFehler", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "JOB_STATUS", value, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), value, Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "DiagError", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "JOB_STATUS", value, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), value, Ergebnis.FT_NIO_SYS );
                    ergs.add(result);
                    //Set condition state to NIOSYS, cause the execution failed
                    OverallConditionState=STATE_NIOSYS;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                    if (isDE()) throw new PPExecutionException("DiagData.JobExecution.execute: DiagFehler: JOB_STATUS = " + value);
                    else throw new PPExecutionException("DiagData.JobExecution.execute: DiagError: JOB_STATUS = " + value);
                }
            } catch( ApiCallFailedException e ) {
                if (debug > 1) System.out.println("DiagData.JobExecution.execute: Ediabas JOB_STATUS ApiCallFailed: '"+e.getMessage()+"'");
                if (isDE()) result = new Ergebnis( "DiagFehler", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "DiagError", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
                ergs.add(result);
                //Set condition state to NIOSYS, cause the execution failed
                OverallConditionState=STATE_NIOSYS;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                if (isDE()) throw new PPExecutionException("DiagData.JobExecution.execute: DiagFehler apiErrorText = " + ediabas.apiErrorText());
                else throw new PPExecutionException("DiagData.JobExecution.execute: DiagError apiErrorText = " + ediabas.apiErrorText());
            } catch( EdiabasResultNotFoundException e ) {
                if (debug > 1) System.out.println("DiagData.JobExecution.execute: Ediabas JOB_STATUS ResultNotFound: '"+e.getMessage()+"'");
                if (e.getMessage() != null) {
                    if (isDE()) result = new Ergebnis( "DiagFehler", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "DiagError", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                } else {
                    if (isDE()) result = new Ergebnis( "DiagFehler", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "DiagError", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
                }
                ergs.add(result);
                //Set condition state to NIOSYS, cause the execution failed
                OverallConditionState=STATE_NIOSYS;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                if (isDE()) throw new PPExecutionException("DiagData.JobExecution.execute: DiagFehler: " + e.getMessage());
                else throw new PPExecutionException("DiagData.JobExecution.execute: DiagError: " + e.getMessage());
            } catch( Exception e) {
                if (debug > 1) System.out.println("DiagData.JobExecution.execute: Ediabas JOB_STATUS ERROR: '"+e.getMessage()+"'");
                if (isDE()) result = new Ergebnis( "DiagFehler", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "FehlerCode: "+ediabas.apiErrorCode()+" FehlerText: " + ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "DiagError", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "ErrorCode: "+ediabas.apiErrorCode()+" ErrorText: " + ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
                ergs.add(result);
                //Set condition state to NIOSYS, cause the execution failed
                OverallConditionState=STATE_NIOSYS;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                if (isDE()) throw new PPExecutionException("DiagData.JobExecution.execute: DiagFehler: " + e.getMessage());
                else throw new PPExecutionException("DiagData.JobExecution.execute: DiagError: " + e.getMessage());
            }
            
            //collect the results
            for (i=1;i<=getNumberOfResults();i++) {
                try {
                    value = ediabas.getDiagResultValue(internalResults[i]);
                    setResultValue(i,value);                                    //remember result value
                    setSubstitution("%R"+i+"%", value);                         //set value for substitutions in conditions and storageformats
                    if (!this.getHideResult(i)) {
                        //do not hide results
                        result = new Ergebnis( ""+internalResults[i], "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), internalResults[i], ""+value, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                    } else {
                        //hide results
                        if (isDE()) result = new Ergebnis( ""+internalResults[i], "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), internalResults[i], "GEHEIMER WERT", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                        else result = new Ergebnis( ""+internalResults[i], "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), internalResults[i], "SECRET VALUE", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                    }
                    ergs.add(result);
                    if (debug > 1) System.out.println("DiagData.JobExecution.execute: "+internalResults[i]+" = '"+getResultValue(i)+"'");
                } catch( ApiCallFailedException e ) {
                    if (debug > 0) System.out.println("DiagData.JobExecution.execute: ApiCallFailedException = '"+e.getMessage()+"'");
                    setSubstitution("%R"+i+"%", "NIOSYS_RESULT_NOT_FOUND");                         //set value for substitutions in conditions and storageformats
                    if (isDE()) result = new Ergebnis( "DiagFehler", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), internalResults[i], "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "FehlerCode: "+ediabas.apiErrorCode()+" FehlerText: " + ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "DiagError", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), internalResults[i], "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "ErrorCode: "+ediabas.apiErrorCode()+" ErrorText: " + ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
                    ergs.add(result);
                    //Set condition state to NIOSYS, cause the execution failed
                    OverallConditionState=STATE_NIOSYS;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                    if (isDE()) throw new PPExecutionException("DiagData.JobExecution.execute: DiagFehler apiErrorText = " + ediabas.apiErrorText());
                    else throw new PPExecutionException("DiagData.JobExecution.execute: DiagError apiErrorText = " + ediabas.apiErrorText());
                } catch( EdiabasResultNotFoundException e ) {
                    if (debug > 0) System.out.println("DiagData.JobExecution.execute: EdiabasResultNotFoundException = '"+e.getMessage()+"'");
                    setSubstitution("%R"+i+"%", "NIOSYS_RESULT_NOT_FOUND");                         //set value for substitutions in conditions and storageformats
                    if( e.getMessage() != null ) {
                        if (isDE()) result = new Ergebnis( "DiagFehler", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), internalResults[i], "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                        else result = new Ergebnis( "DiagError", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), internalResults[i], "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                    } else {
                        if (isDE()) result = new Ergebnis( "DiagFehler", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), internalResults[i], "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
                        else result = new Ergebnis( "DiagError", "EDIABAS", internalSgbd, internalJob, ((dokuPar==null) ? "" : dokuPar), internalResults[i], "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
                    }
                    ergs.add(result);
                    //Set condition state to NIOSYS, cause the execution failed
                    OverallConditionState=STATE_NIOSYS;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                    if (isDE()) throw new PPExecutionException("DiagData.JobExecution.execute: DiagFehler: " + e.getMessage());
                    else throw new PPExecutionException("DiagData.JobExecution.execute: DiagError: " + e.getMessage());
                }
            }
            releaseEdiabasParallel( ergs, internalSgbd );
            
            //Set condition state to UNKNOWN, cause the conditions have not been applied yet
            OverallConditionState=STATE_UNKNOWN;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
            if (debug > 0) System.out.println("DiagData.JobExecution.execute: done");
        }
        
        /**
         * apply conditions
         * @return The overallconditionstate: STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
         */
        public int applyConditions(Vector<Ergebnis> ergs) throws PPExecutionException {
            int i;
            String temp;
            Ergebnis result;
            boolean goon = true;
            char cMode = this.getConditionMode();       //condition mode
            if (debug > 0) System.out.println("DiagData.JobExecution.applyConditions: applying conditions");
            //do not apply conditions if state is NIOSYS
            if (OverallConditionState == STATE_NIOSYS) {
                if (debug > 0) System.out.println("DiagData.JobExecution.applyConditions: NIOSYS before applyConditions");
                return OverallConditionState;
            }
            //Set default state
            if (this.getNumberOfConditions() == 0) {
                //no Conditions, so IO
                OverallConditionState=STATE_IO;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
            } else {
                switch (cMode) {
                    case 'A':
                    case 'N':
                        //AND or NOR, so default IO
                        OverallConditionState=STATE_IO;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                        break;
                    case 'O':
                        //OR, so default NIO
                        OverallConditionState=STATE_NIO;  //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                        break;
                    default:
                        if (isDE()) result = new Ergebnis( "ExecFehler", "CONDITIONS", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler"), "Bedingungsmodus unbekannt", Ergebnis.FT_NIO_SYS );
                        else result = new Ergebnis( "ExecError", "CONDITIONS", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler"), "Conditionmode unknwon", Ergebnis.FT_NIO_SYS );
                        ergs.add(result);
                        if (isDE()) throw new PPExecutionException("DiagData.JobExecution.applyConditions: Unbekannter ConditionMode " + cMode);
                        else throw new PPExecutionException("DiagData.JobExecution.applyConditions: Unknown ConditionMode " + cMode);
                }
            }
            //supported condition modes
            //public static final String ConditionModes[][] = {{"AND","A"},{"OR","O"},{"NOR","N"}};
            
            for (i=1;(i<=this.getNumberOfConditions()) && goon;i++) {     //loop over all conditions
                String cond=(String)this.Condition.get(i);    //local runtime-variables for storing the condition before resolving expressions, needed for error messages
                //determine hwt
                String condHWT = getHwt(i);                  //try to get HWTi
                if (condHWT == null) {
                    //use default hwt
                    condHWT = getDefaultHwt();
                    if (condHWT == null) {
                        //empty HWT
                        condHWT = "";
                    } else {
                        //resolve substitutions and links (no Expressions)
                        condHWT = resolveSubstLinks(condHWT);
                    }
                } else {
                    //resolve expressions for condition i HWT (also Expressions)
                    condHWT = resolveSubstLinksExpr(condHWT);
                }
                if (debug > 1) System.out.println("DiagData.JobExecution.applyConditions: start applying condition"+i+": "+cond);
                try {
                    //just resolve links
                    cond = resolveSubstLinks((String)this.Condition.get(i));     //just resolve substitutions and links
                    if (debug > 1) System.out.println("DiagData.JobExecution.applyConditions: resolved links in condition"+i+": "+cond);
                } catch (Exception e) {
                    //error during resolving links, so create Result
                    if (debug > 1) System.out.println("DiagData.JobExecution.applyConditions: error resolving links in condition"+i+": "+cond);
                    condHWT = resolveSubstLinksExpr(condHWT);
                    if (isDE()) result = new Ergebnis( "CONDITION"+i, "EDIABAS", getSgbd(), getJob(), ((getPar() == null) ? "" : getPar()), "CONDITION"+i+ " -> "+cond, "NIO", "IO", "IO", "0", "", "", "", "Fehler beim Aufl�sen von Verweisen in der Bedingung: "+cond, condHWT, Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "CONDITION"+i, "EDIABAS", getSgbd(), getJob(), ((getPar() == null) ? "" : getPar()), "CONDITION"+i+ " -> "+cond, "NIO", "IO", "IO", "0", "", "", "", "Error resolving links in condition: "+cond, condHWT, Ergebnis.FT_NIO_SYS );
                    ergs.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.JobExecution.applyConditions: Fehler "+e.getMessage());
                    else throw new PPExecutionException("DiagData.JobExecution.applyConditions: Exception "+e.getMessage());
                }
                try {
                    //resolve expressions
                    temp = resolveSubstLinksExpr(cond);     //resolve expressions, links have already been resolved
                    if (debug > 1) System.out.println("DiagData.JobExecution.applyConditions: resolved expressions in condition"+i+": "+temp);
                } catch (Exception e) {
                    //error during resolving expressions, so create Result
                    if (debug > 1) System.out.println("DiagData.JobExecution.applyConditions: error resolving expressions in condition"+i+": "+cond);
                    condHWT = resolveSubstLinksExpr(condHWT);
                    if (isDE()) result = new Ergebnis( "CONDITION"+i, "EDIABAS", getSgbd(), getJob(), ((getPar() == null) ? "" : getPar()), "CONDITION"+i+ " -> "+cond, "NIO", "IO", "IO", "0", "", "", "", "Fehler beim Aufl�sen von Ausr�cken in der Bedingung: "+cond, condHWT, Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "CONDITION"+i, "EDIABAS", getSgbd(), getJob(), ((getPar() == null) ? "" : getPar()), "CONDITION"+i+ " -> "+cond, "NIO", "IO", "IO", "0", "", "", "", "Error resolving expressions in condition: "+cond, condHWT, Ergebnis.FT_NIO_SYS );
                    ergs.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.JobExecution.applyConditions: Fehler "+e.getMessage());
                    else throw new PPExecutionException("DiagData.JobExecution.applyConditions: Exception "+e.getMessage());
                }
                if (temp.equals(DEF_TRUE)) {
                    //condition ok
                    this.setConditionState(i, true);
                    switch (cMode) {
                        case 'N':
                            //if one condition is true NOR is NIO
                            OverallConditionState=i;
                            goon = false;
                            if (ergs != null) {
                                //Make substitutions in condHWT
                                condHWT = resolveSubstLinksExpr(condHWT);
                                result = new Ergebnis( "CONDITION"+i, "EDIABAS", getSgbd(), getJob(), ((getPar() == null) ? "" : getPar()), "CONDITION"+i+ " -> "+cond, "TRUE", "FALSE", "FALSE", "0", "", "", "", PB.getString( "toleranzFehler1" ), condHWT, Ergebnis.FT_NIO );
                                ergs.add(result);
                            }
                            break;
                        case 'O':
                            //if one condition is true OR is IO
                            OverallConditionState=STATE_IO;
                            goon = false;
                            if (ergs != null) {
                                //Make substitutions in condHWT
                                condHWT = resolveSubstLinksExpr(condHWT);
                                result = new Ergebnis( "CONDITION"+i, "EDIABAS", getSgbd(), getJob(), ((getPar() == null) ? "" : getPar()), "CONDITION"+i+ " -> "+cond, "TRUE", "TRUE", "TRUE", "0", "", "", "", "", "", Ergebnis.FT_IO );
                                ergs.add(result);
                            }
                            break;
                    }
                } else {
                    //condition not ok
                    this.setConditionState(i, false);
                    switch (cMode) {
                        case 'A':
                            //if one condition is false AND is NIO
                            OverallConditionState=i;
                            goon = false;
                            if (ergs != null) {
                                //Make substitutions in condHWT
                                condHWT = resolveSubstLinksExpr(condHWT);
                                result = new Ergebnis( "CONDITION"+i, "EDIABAS", this.getSgbd(), this.getJob(), this.getPar(), "CONDITION"+i+ " -> "+cond, "FALSE", "TRUE", "TRUE", "0", "", "", "", PB.getString( "toleranzFehler1" ), condHWT, Ergebnis.FT_NIO );
                                ergs.add(result);
                            }
                            break;
                    }
                }
                
            }//end for
            return OverallConditionState;
        }
        
        /**
         * export values to dynamic attributes
         */
        public void exportDynamicAttributes() throws PPExecutionException {
            String label,value,format;
            int i;
            
            if (debug > 0)  System.out.println("DiagData.JobExecution.exportDynamicAttributes: start");
            //loop over all storagenames
            for (i =1; i<=this.getNumberOfStorageNames();i++) {
                
                label = this.getStorageName(i);             //get the name of the dynamic attribute
                if (label == null) {
                    if (isDE()) throw new PPExecutionException("DiagData.JobExecution.exportDynamicAttributes: ung�ltiger Storagename"+i+": "+label);
                    else throw new PPExecutionException("DiagData.JobExecution.exportDynamicAttributes: invalid Storagename"+i+": "+label);
                }
                format = this.getStorageFormat(i);          //get the expression for the dynamic attribute
                if (format == null) {
                    if (isDE()) throw new PPExecutionException("DiagData.JobExecution.exportDynamicAttributes: ung�ltiges Storageformat"+i+": "+format);
                    else throw new PPExecutionException("DiagData.JobExecution.exportDynamicAttributes: invalid Storageformat"+i+": "+format);
                }
                value = resolveSubstLinksExpr(format);   //execute the expression
                label = resolveSubstLinks(label);        //resolve substitutions and links
                setDynamicAttribute(label,value);           //export value
            }
            if (debug > 0)  System.out.println("DiagData.JobExecution.exportDynamicAttributes: end");
        }
        
        /**
         * Check if SGBD and Job are set (required args for each job)
         */
        public void checkArgs() throws PPExecutionException {
            Vector<Ergebnis> ergs = new Vector<Ergebnis>();
            this.checkArgs(ergs);
        }
        
        /**
         * Check if SGBD and Job are set (required args for each job)
         * @param errors When an error occurs the reason may be put in this vector as a Ergebnis, before throwing an exception.
         * No IO-Results will be added.
         */
        public void checkArgs(Vector<Ergebnis> errors) throws PPExecutionException {
            Ergebnis result;
            
            //Is the SGBD-Job set?
            if (Job == null) {
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler JOB fehlt", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error JOB missing", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.JobExecution.checkArgs: JOB fehlt");
                else throw new PPExecutionException("DiagData.JobExecution.checkArgs: JOB missing");
            }
            //Is the SGBD set?
            if (Sgbd == null) {
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler SGBD fehlt", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error SGBD missing", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.JobExecution.checkArgs: SGBD fehlt");
                else throw new PPExecutionException("DiagData.JobExecution.checkArgs: SGBD missing");
            }
            
            //Are all Results defined?
            for (int i=1; i <= getNumberOfResults(); i++) {
                if (this.Result.get(i) == null) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler RESULT"+ i +" fehlt", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error RESULT"+i+" missing", Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.JobExecution.checkArgs: RESULT"+i+" fehlt");
                    else throw new PPExecutionException("DiagData.JobExecution.checkArgs: RESULT"+i+" missing");
                }
            }
            
            //Are more HideResults defined than Results
            if (this.getNumberOfResults() < this.getNumberOfHideResults()) { //we have no results or less results than hideresult
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler zu viele HIDERESULT", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error too many HIDERESULT", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.JobExecution.checkArgs: zu viele HIDERESULT");
                else throw new PPExecutionException("DiagData.JobExecution.checkArgs: too many HIDERESULT");
            }
            
            //Are all Conditions defined?
            for (int i=1; i <= getNumberOfConditions(); i++) {
                if (this.Condition.get(i) == null) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler CONDITION"+ i +" fehlt", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error CONDITION"+i+" missing", Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.JobExecution.checkArgs: CONDITION"+i+" fehlt");
                    else throw new PPExecutionException("DiagData.JobExecution.checkArgs: CONDITION"+i+" missing");
                }
            }
            
            //Are more Hwts defined than Conditions
            if (this.getNumberOfConditions() < this.getNumberOfHwts()) {
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler zu viele HWT", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error too many HWT", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.JobExecution.checkArgs: zu viele HWT");
                else throw new PPExecutionException("DiagData.JobExecution.checkArgs: too many HWT");
            }
            
            //Are more JumpNios defined than Conditions
            if (this.getNumberOfConditions() < this.getNumberOfJumpNios()) {
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler zu viele JUMP_NIO", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error too many JUMP_NIO", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.JobExecution.checkArgs: zu viele JUMP_NIO");
                else throw new PPExecutionException("DiagData.JobExecution.checkArgs: too many JUMP_NIO");
            }
            
            //Are more QwtNios defined than Conditions
            if (this.getNumberOfConditions() < this.getNumberOfQwtNios()) {
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler zu viele QWT_NIO", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error too many QWT_NIO", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.JobExecution.checkArgs: zu viele QWT_NIO");
                else throw new PPExecutionException("DiagData.JobExecution.checkArgs: too many QWT_NIO");
            }
            
            //Do we have the same number of StorageNames and of StorageFormats?
            if (this.getNumberOfStorageFormats() != this.getNumberOfStorageNames()) {
                if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler Anzahl STORAGEFORMAT ungleich Anzahl STORAGENAME", Ergebnis.FT_NIO_SYS );
                else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error number of STORAGEFORMAT different to number of STORAGENAME", Ergebnis.FT_NIO_SYS );
                errors.add(result);
                if (isDE()) throw new PPExecutionException("DiagData.JobExecution.checkArgs: Anzahl STORAGEFORMAT ungleich Anzahl STORAGENAME");
                else throw new PPExecutionException("DiagData.JobExecution.checkArgs: number of STORAGEFORMAT different to number of STORAGENAME");
            }
            
            //Are all StorageNames and StorageFormats defined?
            for (int i=1; i <= getNumberOfStorageNames(); i++) {
                if (this.StorageName.get(i) == null) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler STORAGENAME"+ i +" fehlt", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error STORAGENAME"+i+" missing", Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.JobExecution.checkArgs: STORAGENAME"+i+" fehlt");
                    else throw new PPExecutionException("DiagData.JobExecution.checkArgs: STORAGENAME"+i+" missing");
                }
                if (this.StorageFormat.get(i) == null) {
                    if (isDE()) result = new Ergebnis( "ExecFehler", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler STORAGEFORMAT"+ i +" fehlt", Ergebnis.FT_NIO_SYS );
                    else result = new Ergebnis( "ExecError", "EXTRACTARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error STORAGEFORMAT"+i+" missing", Ergebnis.FT_NIO_SYS );
                    errors.add(result);
                    if (isDE()) throw new PPExecutionException("DiagData.JobExecution.checkArgs: STORAGEFORMAT"+i+" fehlt");
                    else throw new PPExecutionException("DiagData.JobExecution.checkArgs: STORAGEFORMAT"+i+" missing");
                }
            }
            
            //*****************************************************
            //May be extended for use of Vectors
            //*****************************************************
        }
        
        /**
         * Define a new Substitution
         * @param pattern searchPattern
         * @param newValue String that is put in for pattern
         */
        void setSubstitution(String pattern, String newValue) {
            this.Substitutions.put(pattern,newValue);
            if (debug > 1) System.out.println("DiagData.JobExecution.setSubstitution: '"+pattern+"'->'"+newValue+"'");
        }
        
        /**
         * Get all active Substitutions
         * @return Hashtable containning all active mappings from patterns to newValues
         */
        Hashtable getSubstitutions() {
            return this.Substitutions;
        }
        
        /**
         * Set the Ediabas-Job to be executed
         */
        void setJob(String Job) {
            this.Job = Job;
            if (debug > 2) System.out.println("DiagData.JobExecution.setJob: '"+Job+"'");
        }
        
        /**
         * Get the resolved Ediabas-Job to be executed
         */
        String getJob() throws PPExecutionException {
            return resolveSubstLinksExpr(this.Job);
        }
        
        /**
         * Set the Sgbd which contains Job
         */
        void setSgbd(String Sgbd) {
            this.Sgbd = Sgbd;
            if (debug > 2) System.out.println("DiagData.JobExecution.setSgdb: '"+this.Sgbd+"'");
        }
        
        /**
         * Get the resolved Sgbd which contains Job
         */
        String getSgbd() throws PPExecutionException {
            return resolveSubstLinksExpr(this.Sgbd);
        }
        
        /**
         * Set the List of parameters seperated by ';'
         */
        void setPar(String Par) {
            this.Par = Par;
            if (debug > 2) System.out.println("DiagData.JobExecution.setPar: '"+this.Par+"'");
        }
        
        /**
         * Get the ';'-separated List of resolved parameters seperated by ';'
         */
        String getPar() throws PPExecutionException {
            return resolveSubstLinksExprWithSemicolon(this.Par);
        }
        
        /**
         * Set the HIDEPAR flag. If true, the parameters will not be contained in the CASCADE-results
         */
        void setHidePar(boolean HidePar) {
            this.HidePar = HidePar;
            if (debug > 2) System.out.println("DiagData.JobExecution.setHidePar: '"+this.HidePar+"'");
        }
        
        /**
         * Set the HIDEPAR flag. If true, the parameters will not be contained in the CASCADE-results
         */
        boolean getHidePar() throws PPExecutionException {
            return this.HidePar;
        }
        
        //Values for each Result 1...K
        //The Arrays Result, HideResult and ResultValue have always the same size
        
        /**
         * Get the Number of Results
         */
        int getNumberOfResults() {
            if (Result == null) return 0;
            else if (this.Result.size() <= 1) return 0;
            else return (this.Result.size() - 1);
        }
        
        /**
         * set the entry at index in the Array containing the Names of the Results
         */
        void setResult(int index, String Result) {
            if (this.Result == null) this.Result = new Vector(INITIAL_CAPACITY);
            if (this.Result.size() <= index) {
                //sizeup arrays
                this.Result.setSize(index + 1);
            }
            this.Result.setElementAt(Result, index);
            if (debug > 2) System.out.println("DiagData.JobExecution.setOneResult: "+index+" ->'"+Result+"'");
        }
        
        /**
         * Get the resolved entry index from the Array containing the Names of the Results
         */
        String getResult(int index) throws PPExecutionException {
            if ((this.Result == null) ||
            (this.Result.size() <= index) ||
            (this.Result.get(index) == null)) return null;
            return resolveSubstLinksExpr((String)this.Result.get(index));
        }
        
        /**
         * Get the Number of HideResults
         */
        int getNumberOfHideResults() {
            if (this.HideResult == null) return 0;
            else if (this.HideResult.size() <= 1) return 0;
            else return (this.HideResult.size() - 1);
        }
        
        /**
         * set an entry in the Array of booleans defining wether the value of the Result will be contained in the Cascade-Results or not
         */
        void setHideResult(int index, boolean HideResult) {
            if (this.HideResult == null) this.HideResult = new Vector(INITIAL_CAPACITY);
            if (this.HideResult.size() <= index) {
                //sizeup arrays
                this.HideResult.setSize(index + 1);
            }
            this.HideResult.setElementAt(new Boolean(HideResult), index);
            if (debug > 2) System.out.println("DiagData.JobExecution.setOneHideResult: "+index+" ->'"+HideResult+"'");
        }
        
        /**
         * Get an entry from the Array of booleans defining wether the value of the Result will be contained in the Cascade-Results or not
         */
        boolean getHideResult(int index) {
            if ((this.HideResult == null) ||
            (this.HideResult.size() <= index) ||
            (this.HideResult.get(index) == null)) return false;
            return ((Boolean)this.HideResult.get(index)).booleanValue();
        }
        
        /**
         * Set an entry in the Array of result values (after execution)
         */
        void setResultValue(int index, String ResultValue) {
            if (this.ResultValue == null) this.ResultValue = new Vector(INITIAL_CAPACITY);
            if (this.ResultValue.size() <= index) {
                //sizeup arrays
                this.ResultValue.setSize(index + 1);
            }
            this.ResultValue.setElementAt(ResultValue, index);
            if (debug > 2) System.out.println("DiagData.JobExecution.setOneResultValue: "+index+" ->'"+ResultValue+"'");
        }
        
        /**
         * get the Array of result values (after execution)
         */
        String getResultValue(int index) {
            if ((this.ResultValue == null) ||
            (this.ResultValue.size() <= index) ||
            (this.ResultValue.get(index) == null)) return null;
            return (String)this.ResultValue.get(index);
        }
        
        //Values for each Condition 1...L
        //Note the arrays Condition, Hwt and ConditionState have always the same size
        
        /**
         * Get number of conditions
         */
        int getNumberOfConditions() {
            if (this.Condition == null) return 0;
            else if (this.Condition.size() <= 1) return 0;
            else return (this.Condition.size() - 1);
        }
        
        /**
         * Set one entry in the array of condition-expressions
         */
        void setCondition(int index, String Condition) {
            if (this.Condition == null) this.Condition = new Vector(INITIAL_CAPACITY);
            if (this.Condition.size() <= index) {
                //sizeup arrays
                this.Condition.setSize(index + 1);
            }
            this.Condition.setElementAt(Condition,index);
            if (debug > 2) System.out.println("DiagData.JobExecution.setOneCondition: "+index+" ->'"+Condition+"'");
        }
        
        /**
         * get one resolved entry in the array of condition-expressions
         */
        String getCondition(int index) throws PPExecutionException {
            if ((this.Condition == null) ||
            (this.Condition.size() <= index) ||
            (this.Condition.get(index) == null)) return null;
            return resolveSubstLinksExpr((String)this.Condition.get(index));
        }
        
        /**
         * Get the Number of Hwts
         */
        int getNumberOfHwts() {
            if (this.Hwt == null) return 0;
            else if (this.Hwt.size() <= 1) return 0;
            else return (this.Hwt.size() - 1);
        }
        
        /**
         * Set the array of hwts
         */
        void setHwt(int index, String Hwt) {
            if (this.Hwt == null) this.Hwt = new Vector(INITIAL_CAPACITY);
            if (this.Hwt.size() <= index) {
                //sizeup arrays
                this.Hwt.setSize(index + 1);
            }
            this.Hwt.setElementAt(Hwt,index);
            if (debug > 2) System.out.println("DiagData.JobExecution.setOneHwt: "+index+" ->'"+Hwt+"'");
        }
        
        /**
         * get one resolved entry from the array of hwts
         */
        String getHwt(int index) throws PPExecutionException {
            if ((this.Hwt == null) ||
            (this.Hwt.size() <= index) ||
            (this.Hwt.get(index) == null)) return null;
            //return resolveSubstLinksExpr(this.Hwt[index]);
            return (String)this.Hwt.get(index);
        }
        
        /**
         * set an entry in the array of condition states
         */
        void setConditionState(int index, boolean ConditionState){
            if (this.ConditionState == null) this.ConditionState = new Vector(INITIAL_CAPACITY);
            if (this.ConditionState.size() <= index) {
                //sizeup arrays
                this.ConditionState.setSize(index + 1);
            }
            this.ConditionState.setElementAt(new Boolean(ConditionState),index);
            if (debug > 2) System.out.println("DiagData.JobExecution.setOneConditionState: "+index+" ->'"+ConditionState+"'");
        }
        
        /**
         * get an entry in the array of condition states
         */
        boolean getConditionState(int index){
            if ((this.ConditionState == null) ||
            (this.ConditionState.size() <= index) ||
            (this.ConditionState.get(index) == null)) return false;
            return ((Boolean)this.ConditionState.get(index)).booleanValue();
        }
        
        //Values for each Value that should be stored 1...M
        //Note the arrays StorageName and StorageFormat have all the same size
        
        /**
         * Get number of StorageNames
         */
        int getNumberOfStorageNames() {
            if (this.StorageName == null) return 0;
            else if (this.StorageName.size() <= 1) return 0;
            else return (this.StorageName.size() - 1);
        }
        
        /**
         * set one entry in the array of storage names (names of the dynamic attributes)
         */
        void setStorageName(int index, String StorageName) {
            if (this.StorageName == null) this.StorageName = new Vector(INITIAL_CAPACITY);
            if (this.StorageName.size() <= index) {
                //sizeup arrays
                this.StorageName.setSize(index + 1);
            }
            this.StorageName.setElementAt(StorageName,index);
            if (debug > 2) System.out.println("DiagData.JobExecution.setOneStorageName: "+index+" ->'"+StorageName+"'");
        }
        
        /**
         * get one entry in the array of storage names (names of the dynamic attributes)
         */
        String getStorageName(int index) throws PPExecutionException {
            if ((this.StorageName == null) ||
            (this.StorageName.size() <= index) ||
            (this.StorageName.get(index) == null)) return null;
            return (String)this.StorageName.get(index);
        }
        
        /**
         * Get number of StorageFormats
         */
        int getNumberOfStorageFormats() {
            if (this.StorageFormat == null) return 0;
            else if (this.StorageFormat.size() <= 1) return 0;
            else return (this.StorageFormat.size() - 1);
        }
        
        /**
         * set one entry in the array of storage formats (Conversion-algorithm executed before storing)
         */
        void setStorageFormat(int index, String StorageFormat) {
            if (this.StorageFormat == null) this.StorageFormat = new Vector(INITIAL_CAPACITY);
            if (this.StorageFormat.size() <= index) {
                //sizeup arrays
                this.StorageFormat.setSize(index + 1);
            }
            this.StorageFormat.setElementAt(StorageFormat, index);
            if (debug > 2) System.out.println("DiagData.JobExecution.setOneStorageFormat: "+index+" ->'"+StorageFormat+"'");
        }
        
        /**
         * get one resolved entry from the array of storage formats (Conversion-algorithm executed before storing)
         */
        String getStorageFormat(int index) throws PPExecutionException {
            if ((this.StorageFormat == null) ||
            (this.StorageFormat.size() <= index) ||
            (this.StorageFormat.get(index) == null)) return null;
            return (String)this.StorageFormat.get(index);
        }
        
        //Other common values
        /**
         * set the default hwt
         */
        void setDefaultHwt(String DefaultHwt) {
            this.DefaultHwt = DefaultHwt;
            if (debug > 2) System.out.println("DiagData.JobExecution.setDefaultHwt: '"+this.DefaultHwt+"'");
        }
        
        /**
         * get the resolved default hwt
         */
        String getDefaultHwt() throws PPExecutionException {
            //return resolveSubstLinks(this.DefaultHwt);
            return this.DefaultHwt;
        }
        
        /**
         * set the conditionMode (If "OR" only one condition has to be true, default "AND")
         */
        void setConditionMode(char ConditionMode) throws PPExecutionException {
            String temp = new String(" ");
            temp.replace(' ', ConditionMode);
            setConditionMode(temp);
        }
        
        /**
         * set the conditionMode (If "OR" only one condition has to be true, default "AND")
         */
        void setConditionMode(String ConditionMode) throws PPExecutionException {
            char c = ' ';                                           //coditionmodecode
            int i;
            //loop over all known ConditionModes
            for (i=0; i<ConditionModes.length; i++) {
                if (ConditionMode.equalsIgnoreCase(ConditionModes[i][0])||
                ConditionMode.equalsIgnoreCase(ConditionModes[i][1])) {
                    c = ConditionModes[i][1].charAt(0);              //put a abbreviation for the command into c
                }
            }
            if (c == ' ') {
                if (isDE()) throw new PPExecutionException("DiagData.JobExecution.setConditionMode: Unbekannter ConditionMode " + ConditionMode);
                else throw new PPExecutionException("DiagData.JobExecution.setConditionMode: Unknown ConditionMode " + ConditionMode);
            }
            this.ConditionMode = c;
            if (debug > 2) System.out.println("DiagData.JobExecution.setConditionMode: '"+this.ConditionMode+"'");
        }
        
        /**
         * set the conditionMode (If "OR" only one condition has to be true, default "AND")
         */
        char getConditionMode() {
            return this.ConditionMode;
        }
        
        /**
         * set the conditionMode (If "OR" only one condition has to be true, default "AND")
         */
        String getConditionModeString() {
            int i;
            //loop over all known ConditionModes
            for (i=0; i<ConditionModes.length; i++) {
                if (ConditionModes[i][1].charAt(0) == this.ConditionMode) {
                    return ConditionModes[i][0];
                }
            }
            return null;
        }
        
        //additional settings not relevant for methods in JobExecution itself
        /**
         * defines the pause before the job is executed. Default: 0
         */
        void setPauseBefore(long PauseBefore) {
            this.PauseBefore = PauseBefore;
            if (debug > 2) System.out.println("DiagData.JobExecution.setPauseBefore: '"+this.PauseBefore+"'");
        }
        
        /**
         * returns the pause before the job is executed. Default: 0
         */
        long getPauseBefore() {
            return this.PauseBefore;
        }
        
        /**
         * defines the pause after the job is executed. Default: 0
         */
        void setPauseAfter(long PauseAfter) {
            this.PauseAfter = PauseAfter;
            if (debug > 2) System.out.println("DiagData.JobExecution.setPauseAfter: '"+this.PauseAfter+"'");
        }
        
        /**
         * returns the pause after the job is executed. Default: 0
         */
        long getPauseAfter() {
            return this.PauseAfter;
        }
        
        /**
         * defines where to jump when the status for the job is IO. Default: not defined
         */
        void setJumpIo(int JumpIo) {
            this.JumpIo = JumpIo;
            if (debug > 2) System.out.println("DiagData.JobExecution.setJumpIo: '"+this.JumpIo+"'");
        }
        
        /**
         * returns where to jump when the status for the job is IO. Default: not defined
         */
        int getJumpIo() {
            return this.JumpIo;
        }
        
        /**
         * defines where to jump (default) when the status for the job is NIO. Default: exit nio
         */
        void setJumpNioDefault(int JumpNioDefault) {
            this.JumpNioDefault = JumpNioDefault;
            if (debug > 2) System.out.println("DiagData.JobExecution.setJumpNioDefault: '"+this.JumpNioDefault+"'");
        }
        
        /**
         * returns where to jump (default) when the status for the job is NIO. Default: exit nio
         */
        int getJumpNioDefault() {
            return JumpNioDefault;
        }
        
        /**
         * Get number of JumpNios
         */
        int getNumberOfJumpNios() {
            if (this.JumpNio == null) return 0;
            else if (this.JumpNio.size() <= 1) return 0;
            else return (this.JumpNio.size() - 1);
        }
        
        /**
         * defines where to jump when condition index is responsible for NIO. Default: not defined
         */
        void setJumpNio(int index, int JumpNio) {
            if (this.JumpNio == null) this.JumpNio = new Vector(INITIAL_CAPACITY);
            if (this.JumpNio.size() <= index) {
                //sizeup arrays
                this.JumpNio.setSize(index + 1);
            }
            this.JumpNio.setElementAt(new Integer(JumpNio),index);
            if (debug > 2) System.out.println("DiagData.JobExecution.setJumpNio: "+index+" ->'"+JumpNio+"'");
        }
        
        /**
         * get where to jump when condition index is responsible for NIO. Default: not defined
         */
        int getJumpNio(int index) throws PPExecutionException {
            if ((this.JumpNio == null) ||
            (this.JumpNio.size() <= index) ||
            (this.JumpNio.get(index) == null)) return DEF_JUMP_UNDEF;
            return ((Integer)this.JumpNio.get(index)).intValue();
        }
        
        /**
         * defines where to jump when the status for the job is NIOSYS. Default: not defined
         */
        void setJumpNioSys(int JumpNioSys) {
            this.JumpNioSys = JumpNioSys;
            if (debug > 2) System.out.println("DiagData.JobExecution.setJumpNioSys: '"+this.JumpNioSys+"'");
        }
        
        /**
         * returns where to jump when the status for the job is NIOSYS. Default: not defined
         */
        int getJumpNioSys() {
            return this.JumpNioSys;
        }
        
        /**
         * defines a QWT-message (default) when the status for the job is NIO. Default: not defined
         */
        void setQwtNioDefault(String QwtNioDefault) {
            this.QwtNioDefault = QwtNioDefault;
            if (debug > 2) System.out.println("DiagData.JobExecution.setQwtNioDefault: '"+this.QwtNioDefault+"'");
        }
        
        /**
         * returns a QWT-message (default) when the status for the job is NIO. Default: not defined
         */
        String getQwtNioDefault() {
            return this.QwtNioDefault;
        }
        
        /**
         * Get number of QwtNio
         */
        int getNumberOfQwtNios() {
            if (this.QwtNio == null) return 0;
            else if (this.QwtNio.size() <= 1) return 0;
            else return (this.QwtNio.size() - 1);
        }
        
        /**
         * defines a QWT-message (default) when condition index is responsible for NIO. Default: not defined
         */
        void setQwtNio(int index, String QwtNio) {
            if (this.QwtNio == null) this.QwtNio = new Vector(INITIAL_CAPACITY);
            if (this.QwtNio.size() <= index) {
                //sizeup arrays
                this.QwtNio.setSize(index + 1);
            }
            this.QwtNio.setElementAt(QwtNio,index);
            if (debug > 2) System.out.println("DiagData.JobExecution.setQwtNio: "+index+" ->'"+QwtNio+"'");
        }
        
        /**
         * get a QWT-message (default) when condition index is responsible for NIO. Default: not defined
         */
        String getQwtNio(int index) throws PPExecutionException {
            if ((this.QwtNio == null) ||
            (this.QwtNio.size() <= index) ||
            (this.QwtNio.get(index) == null)) return null;
            return (String)this.QwtNio.get(index);
        }
        
        /**
         * defines a QWT-message when the status for the job is NIOSYS. Default: not defined
         */
        void setQwtNioSys(String QwtNioSys) {
            this.QwtNioSys = QwtNioSys;
            if (debug > 2) System.out.println("DiagData.JobExecution.setQwtNioSys: '"+this.QwtNioSys+"'");
        }
        
        /**
         * returns a QWT-message when the status for the job is NIOSYS. Default: not defined
         */
        String getQwtNioSys() {
            return this.QwtNioSys;
        }
        
        /**
         * defines a IWT-message displayed during and since the execution of this job. Default: not defined
         */
        void setIwt(String Iwt) {
            this.Iwt = Iwt;
            if (debug > 2) System.out.println("DiagData.JobExecution.setIwt: '"+this.Iwt+"'");
        }
        
        /**
         * returns a IWT-message displayed during and since the execution of this job. Default: not defined
         */
        String getIwt() {
            return this.Iwt;
        }
        
        /**
         * defines that the job execution breaks after this job: Default: no breakpoint (String is null)
         */
        void setBreakpoint(String Brp) {
            this.Breakpoint = Brp;
            if (debug > 2) System.out.println("DiagData.JobExecution.setBreakpoint: '"+this.Breakpoint+"'");
        }
        
        /**
         * returns the Breakpoint-Message (null: no breakpoint) for this job. Default: no breakpoint (String is null)
         */
        String getBreakpoint() {
            return this.Breakpoint;
        }
        
        
        /**
         * Get the OverallConditionState
         * STATE_IO, STATE_NIO, STATE_NIOSYS, 1..n for Condition n failed
         */
        int getOverallConditionState() {
            return this.OverallConditionState;
        }
        
        @Override
		public String toString() {
            StringBuffer res = new StringBuffer();
            Iterator iter;
            String label,value;
            int i;
            try {
                //common values
                res.append("Job: " + Job);     //Ediabas-Job to be executed
                res.append("Sgbd: " + Sgbd);    //Sgbd which contains Job
                res.append("Par: " + Par);     //List of parameters seperated by ';'
                //Values for each Result 1...K
                for (int k = 1; k<=this.getNumberOfResults();k++) {
                    res.append("Result"+k +": "+ getResult(k));        //Name of the Result
                    res.append("HideResult"+k +": "+ getHideResult(k));   //If true the value of the Result will not be contained in the Cascade-Results
                    res.append("ResultValue"+k +": "+ getResultValue(k));   //After execution the values are stored here
                }
                //Values for each Condition 1...L
                for (int l = 1; l<=this.getNumberOfConditions();l++) {
                    res.append("Condition"+l +": "+ getCondition(l));         //Boolean expression
                    res.append("Hwt"+l +": "+ getHwt(l));               //Hwt if the expression results in false
                    res.append("ConditionState"+l +": "+ getConditionState(l));  //True if the condition is true, default false
                }
                //Values for each Value that should be stored 1...M
                for (int m = 1; m<=this.getNumberOfStorageNames();m++) {
                    res.append("StorageName"+m +": "+ getStorageName(m));       //Name of the Dynamic Attribute
                    res.append("StorageFormat"+m +": "+ getStorageFormat(m));     //Conversion-algorithm executed before storing
                }
                //Other common values
                iter = Substitutions.keySet().iterator();
                res.append("SUBSTITUTIONS: ");
                while (iter.hasNext()) {
                    label = (String)iter.next();
                    value = (String)Substitutions.get(label);
                    res.append(label);
                    res.append("->");
                    res.append(value);
                    res.append("  ");
                }
                res.append("ADDITIONALSETTINGS: ");
                res.append("PauseBefore: " + getPauseBefore());
                res.append("PauseAfter: " + getPauseAfter());
                res.append("JumpIo: " + getJumpIo());
                res.append("JumpNioDefault: " + getJumpNioDefault());
                if (this.JumpNio != null) {
                    iter = this.JumpNio.iterator();
                    i = 1;
                    if (iter.hasNext()) iter.next();
                    while (iter.hasNext()) {
                        res.append("JumpNio"+i+": "+iter.next());
                        i++;
                    }
                } else {
                    res.append("JumpNioX: not defined");
                }
                res.append("JumpNioSys: " + getJumpNioSys());
                res.append("QwtNioDefault: " + getQwtNioDefault());
                if (this.QwtNio != null) {
                    iter = this.QwtNio.iterator();
                    i = 1;
                    if (iter.hasNext()) iter.next();
                    while (iter.hasNext()) {
                        res.append("QwtNio"+i+": "+iter.next());
                        i++;
                    }
                } else {
                    res.append("QwtNioX: not defined");
                }
                res.append("QwtNioSys: " + getQwtNioSys());
                res.append("Iwt: " + getIwt());
                res.append("DefaultHwt: " + DefaultHwt);          //Default HWT
                res.append("ConditionMode: " + ConditionMode);     //If OR only one condition has to be true
                switch (OverallConditionState) {
                    case STATE_IO:
                        res.append("OverallConditionState: IO");     //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                        break;
                    case STATE_NIO:
                        res.append("OverallConditionState: NIO");     //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                        break;
                    case STATE_NIOSYS:
                        res.append("OverallConditionState: NIOSYS");     //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                        break;
                    case STATE_UNKNOWN:
                        res.append("OverallConditionState: UNKNOWN");     //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                        break;
                    default:
                        res.append("OverallConditionState: " + OverallConditionState);     //STATE_IO, STATE_NIO, STATE_NIOSYS, STATE_UNKNOWN, 1..n for Condition n failed
                }
            } catch (Throwable t) {
                if (debug > 0) System.out.println("JobExecution.toString: THROWABLE catched!");
                if (debug > 0) System.out.println("JobExecution.toString: " + res);
            }
            return res.toString();
        }
    }
    
}

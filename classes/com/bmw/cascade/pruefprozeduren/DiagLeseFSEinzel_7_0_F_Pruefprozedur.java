/*
 * DiagLeseFSEinzel_VX_X_X_YY_Pruefprozedur.java
 *
 * Created on 18.11.03
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.*;

/**
 * Implementierung der Pr�fprozedur, die per Diagnose den Fehlerspeicher ausliest und
 * dabei Fehler auf Basis des Fehlerortes ignoriert bzw. weitere Analysen durch Ausf�hrung der
 * Analyse-PPs vornimmt. Sollten nur zu ignorierende Fehler vorliegen, wird mit der �bergebenen
 * Clear-PP das L�schen vorgenommen.
 * @author Gramer
 * @version 0_0_1_FA  18.11.03  GR  Uebernahme der PP DiagLeseFS_V0_0_9_FA_Pruefprozedur 
 * @version 4_0_T     18.09.08  MB  Erweiterung Paralleldiagnose, Umstellung EdiabasProxyThread
 * @version 5_0_F     25.01.17	MKe F-Version 
 * @version 6_0_T     23.01.18	MKe Bugfix bei der Freigabe der Paralleldiagnose
 * @version 7_0_F     23.01.18	MKe F-Version  
 */
public class DiagLeseFSEinzel_7_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagLeseFSEinzel_7_0_F_Pruefprozedur() {
	}

	/**
	* erzeugt eine neue Pruefprozedur.
	* @param pruefling Klasse des zugeh. Pr�flings
	* @param pruefprozName Name der Pr�fprozedur
	* @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	*/
	public DiagLeseFSEinzel_7_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	* initialsiert die Argumente
	*/
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	* liefert die optionalen Argumente
	*/
	public String[] getOptionalArgs() {
		String[] args = { "JOB", "IGNORE", "ANALYSE_PP", "CLEAR_PP", "IGNORE_MESSAGE", "FCODE[1..N]", "HWT[1..N]", "TAG" };
		return args;
	}

	/**
	* liefert die zwingend erforderlichen Argumente
	*/
	public String[] getRequiredArgs() {
		String[] args = { "SGBD" };
		return args;
	}

	/**
	* pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	*/
	public boolean checkArgs() {
		Pruefprozedur p;
		String temp;
		boolean ok;
		int i;

		try {

			//  super.checkArgs() auskommentiert, da ansonsten bei FCODE1 und HWT der Aufruf fehlschl�gt
			ok = true; //super.checkArgs();
			//System.out.print("Check Args " + String.valueOf(ok));
			if( ok == true ) {
				//SGBD
				temp = getArg( getRequiredArgs()[0] );
				if( temp.indexOf( ';' ) != -1 )
					return false;
				//Job
				temp = getArg( getOptionalArgs()[0] );
				if( temp != null ) {
					if( temp.indexOf( ';' ) != -1 )
						return false;
				}
				//Ignore
				temp = getArg( getOptionalArgs()[1] );
				try {
					if( temp != null )
						temp = extract( temp );
				} catch( NumberFormatException e ) {
					return false;
				}
				//Analyse-PPs
				temp = getArg( getOptionalArgs()[2] );
				if( temp != null ) {
					String[] tempArray = splitArg( temp );
					for( i = 0; i < tempArray.length; i++ ) {
						p = getPr�fling().getPr�fprozedur( tempArray[i] );
						if( p == null )
							return false;
						if( p.checkArgs() == false )
							return false;
					}
				}
				//Clear-PP
				temp = getArg( getOptionalArgs()[3] );
				if( temp != null ) {
					p = getPr�fling().getPr�fprozedur( temp );
					if( p == null )
						return false;
					if( p.checkArgs() == false )
						return false;
				}
			}
			return ok;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	* f�hrt die Pr�fprozedur aus
	* @param info Information zur Ausf�hrung
	*/
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		EdiabasProxyThread ept = null;
		Pruefprozedur p;
		String[] tempArray;
		String temp;
		String sgbd = "";
		String job;
		String jobResults;
		String ignore;
		String analyse;
		String clear;
		int ergS�tze = 0;
		int i, j, k, l;
		boolean hexCode = false;
		boolean ignoreMessage = false;
		boolean alreadyReleased = false;
		String ignoreMessageText = null;

		int FCodeAnzahl; //  Variable fuer die Anzahl angegeben FCodes
		//String argFCodes; // Wert des aktuellen FCodes
		String[] listFCodes = null; // Die angegeben FCodes werden in diesem String abgelegt
		String hwt;
		String[] hwts = null;
		//String list2 = null;

		// >>> MBa: Paralleldiagnose
		boolean parallel = false;
		String tag = null;
		DeviceManager devMan = null;
		// <<<

		try {
			//Parameter holen

			try {

				if( checkArgs() == false )
					throw new PPExecutionException( "Parameterexistenz" );

				sgbd = extractValues( getArg( getRequiredArgs()[0] ) )[0];

				job = getArg( getOptionalArgs()[0] );
				if( job == null )
					job = "FS_LESEN";

				ignore = getArg( getOptionalArgs()[1] );
				if( ignore != null )
					ignore = extract( ignore );

				analyse = getArg( getOptionalArgs()[2] );

				temp = getPr�fling().getAuftrag().getBaureihe();
				if( temp.startsWith( "E6" ) == true ) {
					hexCode = true;
					jobResults = ";F_ORT_NR;F_ORT_TEXT;F_HEX_CODE;";
				} else {
					jobResults = ";F_ORT_NR;F_ORT_TEXT;";
				}

				// Einlesen der Anzahl der FehlerCodes
				FCodeAnzahl = 0;
				while( (getArg( "FCODE" + (FCodeAnzahl + 1) )) != null )
					FCodeAnzahl++;

				// Einlesen der Anzahl der angegeben Fehler-Codes und Ablage aller Fehler-Codes
				//System.out.println("Anzahl FCODES " + String.valueOf(FCodeAnzahl));
				if( FCodeAnzahl > 0 ) {
					listFCodes = new String[FCodeAnzahl];
					for( i = 0; i < FCodeAnzahl; i++ ) {
						//System.out.println("Aktueller FCode " + String.valueOf(getArg("FCODE"+(i+1))));
						listFCodes[i] = getArg( "FCODE" + (i + 1) );
						//System.out.println("Arg " + String.valueOf(argFCodes));
						//System.out.println("i " + String.valueOf(i));
					}
				}

				// System.out.println("------------------"); 
				// Zu jedem FehlerCode wird auch HWT angegeben
				hwt = getArg( getOptionalArgs()[6] );
				// System.out.println("erster HWT " + hwt);
				if( hwt == null )
					hwt = "";
				else
					hwt = PB.getString( hwt );
				// mehrere Hinweistexte einlesen, wenn vorhanden
				hwts = new String[FCodeAnzahl];
				for( i = 0; i < FCodeAnzahl; i++ ) {
					hwts[i] = getArg( "HWT" + (i + 1) );
					//System.out.println("i " + String.valueOf(i));
					//System.out.println("HWT einlesen " + hwts[i]);
				}

				if( analyse != null ) {

					tempArray = splitArg( analyse );
					for( i = 0; i < tempArray.length; i++ ) {
						p = getPr�fling().getPr�fprozedur( tempArray[i] );
						temp = extract( p.getArg( "FORT" ) );
						if( (p.getArg( "SGBD" ) == null) && (p.getArg( "JOB" ) == null) ) {
							//Holen der relevanten Fehlerspeicherinformationen
							j = 1;
							while( p.getArg( "RESULT" + j ) != null ) {
								temp = p.getArg( "RESULT" + j );
								j++;
								if( jobResults.indexOf( ";" + temp + ";" ) < 0 )
									jobResults = jobResults + temp + ";";
							}
						}
					}
				}
				jobResults = jobResults.substring( 1, jobResults.length() - 1 );
				clear = getArg( getOptionalArgs()[3] );

				temp = getArg( getOptionalArgs()[4] );
				if( temp != null )
					if( temp.equalsIgnoreCase( "TRUE" ) )
						ignoreMessage = true;

			} catch( PPExecutionException e ) {
				//e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			if( ignoreMessage ) {
				// Anzeigetext holen
				ignoreMessageText = PB.getString( "ignoreMessage" );
				if( ignoreMessageText.startsWith( "ignore" ) ) {
					ignoreMessageText = "Fehler kann ignoriert werden";
				}
			}

			// MBa: Paralleldiagnose >>>>>
			try // Pr�fstandvariabel auslesen, ob Paralleldiagnose
			{
				Object pr_var;

				pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
				if( pr_var != null ) {
					if( pr_var instanceof String ) {
						if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
							parallel = true;
						}
					} else if( pr_var instanceof Boolean ) {
						if( ((Boolean) pr_var).booleanValue() ) {
							parallel = true;
						}
					}
				}
			} catch( VariablesException e ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				throw new PPExecutionException();
			}
			try {

				// Devicemanager
				devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();

				if( getArg( "TAG" ) != null )
					tag = getArg( "TAG" );

				if( parallel ) // Ediabas holen
				{
					try {
						ept = devMan.getEdiabasParallel( tag, sgbd );
					} catch( Exception ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Wrong Cascade configuration for parallel diagnosys", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						throw new PPExecutionException();
					} catch( Throwable ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();
					}
				} else {
					try {
						ept = devMan.getEdiabas( tag );
					} catch( Exception ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();
					}
				}

				// <<<<< Paralleldiagnose

				//Ausf�hrung
				temp = ept.executeDiagJob( sgbd, job, "", jobResults );
				if( temp.equals( "OKAY" ) == false ) { // Es sind Fehler bei der Asuf�hrung aufgetreten
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} else {
					result = new Ergebnis( "Status", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				}

				//Alle Fehlerspeicherinfos speichern mit Status Fehler (F)
				//System.out.println("Ediabas-Results: Hole alle FS-Eintr�ge");
				ergS�tze = ept.getDiagJobSaetze(); // Anzahl der gefunden Fehler
				tempArray = splitArg( jobResults );
				String[][] errors = new String[ergS�tze - 2][tempArray.length + 1];
				// Aufsplittung der Ergebnisse  in ein Array
				for( i = 0; i < ergS�tze - 2; i++ ) {
					for( j = 0; j < tempArray.length; j++ ) {
						errors[i][j] = ept.getDiagResultValue( i + 1, tempArray[j] );
					}
					errors[i][tempArray.length] = Ergebnis.FT_NIO;
				}

				if( ergS�tze > 2 ) {
					// Grunds�tzlich auszublendende Fehler ausblenden
					//  System.out.println("Ediabas-Results: Analysiere IGNORE");

					if( ignore != null ) {
						for( i = 0; i < ergS�tze - 2; i++ ) {
							if( ignore.indexOf( ";" + errors[i][0] + ";" ) >= 0 )
								errors[i][tempArray.length] = Ergebnis.FT_IGNORE;
						}
					}
					boolean weitereAnalyse = false;
					// Sind noch Datens�tze vorhanden, die nicht ausgblendet werden, wird die weitere Analyse enabled
					for( i = 0; i < ergS�tze - 2; i++ ) {
						if( errors[i][tempArray.length].equals( Ergebnis.FT_NIO ) == true )
							weitereAnalyse = true;
					}

					// Auswertung der Erg-S�tze nach FCodes

					//result = new Ergebnis( results[i-1], "EDIABAS", sgbd, job, jobpar, results[i-1], temp, temp1, temp2, "0", "", "", awt, PB.getString( "toleranzFehler3" ), hwtDummy, Ergebnis.FT_NIO );

					// Alle Ergebniss�tze nach 
					for( i = 0; i < ergS�tze - 2; i++ ) {

						//     System.out.println("Aktueller ErgSatz " + String.valueOf(i));
						// Mit der errors[i][0] muss das Array der Fehlernummern durchsucht werden
						for( j = 0; j < FCodeAnzahl; j++ ) {
							/*     System.out.println("Aktueller Fehler " + String.valueOf(errors[i][0]));   
							     System.out.println("Aktueller Fehlerort " + String.valueOf(errors[i][1]));   
							     System.out.println("Wat dat " + String.valueOf(errors[i][tempArray.length]));   
							     System.out.println("Aktueller FCODE " + String.valueOf(listFCodes[j]));
							     System.out.println("Aktueller HWT " + String.valueOf(hwts[j])); */
							if( errors[i][0].equals( listFCodes[j] ) ) {
								// falls der Code beinhaltet ist, wird einer neuer Eintrag generiert
								result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", errors[i][0], "", "", "0", "", "", "", errors[i][1], hwts[j], errors[i][tempArray.length] );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR; // Job ist fehlgeschlagen
							}
						}

					}

					// MBa: Paralleldiagnose
					if( parallel ) //&& ept!=null)
					{
						try {
							devMan.releaseEdiabasParallel( ept, tag, sgbd );
							alreadyReleased = true;
						} catch( Exception ex ) {
							result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable t ) {
							result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;

						}
					}

					// Wenn Fcodes eingetragen sind, dann wird der normale FS_Lesen_Meachanismus deaktiviert
					if( FCodeAnzahl > 0 ) {
						weitereAnalyse = false;
						ergS�tze = 0;
					}

					//Andere PPs, hier nur die ohne Diagnose
					if( (analyse != null) && (weitereAnalyse == true) ) {
						i = 0;
						while( (weitereAnalyse == true) && (i < splitArg( analyse ).length) ) {
							//System.out.println("Checke Pruefprozedur (FS) "+splitArg(analyse)[i]);
							p = getPr�fling().getPr�fprozedur( splitArg( analyse )[i] );
							//System.out.println("Pruefprozedur-Attrib FORT="+p.getArg("FORT"));
							//Nur die, wo keine neue Diagnose notwendig ist
							//System.out.println("Pruefprozedur "+p);
							if( (p.getArg( "SGBD" ) == null) && (p.getArg( "JOB" ) == null) ) {
								//System.out.println("Pruefprozedur-Attrib FORT="+p.getArg("FORT"));
								ignore = extract( p.getArg( "FORT" ) );
								//System.out.println("Pruefprozedur "+splitArg(analyse)[i]+" geladen: F-Orte="+ignore);
								for( j = 0; j < ergS�tze - 2; j++ ) {
									//Ueber alle noch fehlerhaften FS-Eintr�ge
									//System.out.println("Analyse des "+j+"-ten Fehlers noetig?");
									if( (ignore.indexOf( ";" + errors[j][0] + ";" ) >= 0) && (errors[j][tempArray.length].equals( Ergebnis.FT_NIO ) == true) ) {
										//Gefunden, Args umbiegen (Resultnamen durch Werte ersetzen) und executen
										//System.out.println("Analyse des "+j+"-ten Fehlers ist noetig");
										//
										//Hier sollte eigentlich geclont werden, geht zur Zeit jedoch nicht.
										//Somit alle Results holen, zwischenspeichern und sp�ter zur�ckschreiben;
										//pc = (Pruefprozedur) p.clone();
										String argStringOrig = "";
										k = 1;
										while( p.getArg( "RESULT" + k ) != null ) {
											temp = p.getArg( "RESULT" + k );
											//System.out.print("RESULT"+k+" von "+temp);
											argStringOrig = argStringOrig + ", RESULT" + k + "=" + temp;
											l = 0;
											while( l < tempArray.length ) {
												if( tempArray[l].equalsIgnoreCase( temp ) == true ) {
													//System.out.print(" auf "+errors[j][l]+" geaendern:");
													p.setArgs( "RESULT" + k + "=" + errors[j][l] );
													//System.out.println(" DONE");
													l = tempArray.length;
												}
												l++;
											}
											k++;
										}
										p.setPr�flingLaufzeitUmgebung( getPr�flingLaufzeitUmgebung() ); // Crichton
										p.execute( info );
										if( p.getExecStatus() == STATUS_EXECUTION_OK )
											errors[j][tempArray.length] = Ergebnis.FT_IGNORE;
										p.setArgs( argStringOrig.substring( 1 ) );

									}
								} //Ende FOR
								weitereAnalyse = false;
								for( j = 0; j < ergS�tze - 2; j++ ) {
									if( errors[j][tempArray.length].equals( Ergebnis.FT_NIO ) == true )
										weitereAnalyse = true;
								}
							}
							i++;
						} //Ende while
					}

					//Andere PPs, hier nur die mit Diagnose
					if( (analyse != null) && (weitereAnalyse == true) ) {
						i = 0;
						while( (weitereAnalyse == true) && (i < splitArg( analyse ).length) ) {
							//System.out.println("Checke Pruefprozedur (DIAG) "+splitArg(analyse)[i]);
							p = getPr�fling().getPr�fprozedur( splitArg( analyse )[i] );
							//Nur die, wo Diagnose gemacht wird
							if( (p.getArg( "SGBD" ) != null) && (p.getArg( "JOB" ) != null) ) {
								ignore = extract( p.getArg( "FORT" ) );
								boolean codeExistiert = false;
								for( j = 0; j < ergS�tze - 2; j++ ) {
									if( (ignore.indexOf( ";" + errors[j][0] + ";" ) >= 0) && (errors[j][tempArray.length].equals( Ergebnis.FT_NIO ) == true) )
										codeExistiert = true;
								}
								if( codeExistiert == true ) {
									//System.out.println("Pruefprozedur "+splitArg(analyse)[i]+" wird ausgef�hrt: F-Orte="+ignore);
									p.setPr�flingLaufzeitUmgebung( getPr�flingLaufzeitUmgebung() ); // Crichton
									p.execute( info );
									if( p.getExecStatus() == STATUS_EXECUTION_OK ) {
										for( j = 0; j < ergS�tze - 2; j++ ) {
											if( ignore.indexOf( ";" + errors[j][0] + ";" ) >= 0 )
												errors[j][tempArray.length] = Ergebnis.FT_IGNORE;
										}
										weitereAnalyse = false;
										for( j = 0; j < ergS�tze - 2; j++ ) {
											if( errors[j][tempArray.length].equals( Ergebnis.FT_NIO ) == true )
												weitereAnalyse = true;
										}
									}
								}
							}
							i++;
						} //Ende while
					}

					//Doku und Status
					for( i = 0; i < ergS�tze - 2; i++ ) {
						hwt = "";

						//Fehlerort und Text als Ergebnis speichern (kann aber sein dass es FT_IGNORE ist)
						result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", errors[i][0], "", "", "0", "", "", "", errors[i][1], hwt, errors[i][tempArray.length] );
						ergListe.add( result );
						//beim FT_NIO  Pruefprozedur status auf NIO setzen und ggf. Hex Code als Ergebnis speichern
						if( errors[i][tempArray.length].equals( Ergebnis.FT_NIO ) == true ) {
							status = STATUS_EXECUTION_ERROR;
							if( hexCode == true ) {
								result = new Ergebnis( "FORT " + errors[i][0], "EDIABAS", sgbd, job, "", "F_HEX_CODE", errors[i][2], "", "", "0", "", "", "", "", "", errors[i][tempArray.length] );
								ergListe.add( result );
							}
						}
					}

					if( (status == STATUS_EXECUTION_OK) && (clear != null) ) {
						p = getPr�fling().getPr�fprozedur( clear );
						p.setPr�flingLaufzeitUmgebung( getPr�flingLaufzeitUmgebung() ); // Crichton
						p.execute( info );
						status = p.getExecStatus(); // Crichton
					}

				} // Ende if (ergS�tze > 2)

			} catch( ApiCallFailedException e ) {
				e.printStackTrace();
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ept.apiErrorCode() + ": " + ept.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// MBa: Paralleldiagnose
		if( parallel && alreadyReleased == false ) //&& ept!=null)
		{
			try {
				devMan.releaseEdiabasParallel( ept, tag, sgbd );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable t ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;

			}
		}

		setPPStatus( info, status, ergListe );
	}

	//f�r Argument IGNORE: L�st den �bergebenen String auf
	private String extract( String in ) {
		String out;
		String sub, subSub;
		int[] posArray;
		int counter = 1;
		//int Pos = 0;
		int min, max;
		int i, j;

		for( i = 0; i < in.length(); i++ ) {
			if( in.charAt( i ) == ';' )
				counter++;
		}

		posArray = new int[counter + 1];
		j = 0;
		posArray[j++] = -1;
		for( i = 0; i < in.length(); i++ ) {
			if( in.charAt( i ) == ';' )
				posArray[j++] = i;
		}
		posArray[j++] = in.length();

		out = ";";
		for( i = 0; i < counter; i++ ) {
			sub = (in.substring( posArray[i] + 1, posArray[i + 1] )).trim();
			if( sub.compareTo( "" ) != 0 ) {
				j = sub.indexOf( "-" );
				if( (j < 1) || (j == (sub.length() - 1)) )
					out = out + sub + ";";
				else {
					subSub = sub.substring( 0, j );
					min = Integer.parseInt( subSub );
					subSub = sub.substring( j + 1, sub.length() );
					max = Integer.parseInt( subSub );
					for( j = min; j <= max; j++ ) {
						out = out + j + ";";
					}
				}
			}
		}

		return out;
	}

}

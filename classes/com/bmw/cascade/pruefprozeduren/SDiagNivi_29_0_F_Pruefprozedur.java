package com.bmw.cascade.pruefprozeduren;

import java.awt.*;
import java.awt.image.*;
import java.util.*;
import java.io.*;
import java.lang.String;

import javax.imageio.ImageIO;
import javax.swing.*;

import java.text.DecimalFormat;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.devices.fascontrol.FasControl;
import com.bmw.cascade.pruefstand.ediabas.*;

/**
 * Implementation of the Night Vision system calibration in the DAS-stand with a
 * heated cross target. <BR>
 * Created on 22.02.07 <BR>
 * 
 * @author (PJ) Pedro Jorge (IndustrieHansa), TI-432, BMW<BR>
 *         (UP) Ulf Plath, TI-544<BR>
 * 
 * @version 1_0_F PJ 22.02.07 First implementation with a graphical user
 *          interface and effective point identification method.
 * @version 2_0_F PJ 10.09.07 Extended to use also with NIVI2 for F0x. Added all
 *          the additional SGBD arguments and some others.
 * @version 3_0_F PJ 11.09.07 Added the possibility of giving an offset in the
 *          SGBD_RESULT allowing the use with NIVI 1 despite the bug the in
 *          NVC_65 SGBD.
 * @version 4_0_F UP 18.09.07 Disabled argument checking for
 *          ANGLE_CONVERSION_FACTOR, now also negative values are valid
 * @version 5_0_F UP 29.01.08 Changed APDM-Status from F to A in case rotational
 *          adjustment is needed, small graphical changes in GUI, very last
 *          measured deviation (x,y,z) will be forwarded to CASCADE (check of
 *          cycle count)
 * @version 6_0_F UP 06.03.08 Changed APDM-Status from F to A in case points
 *          plausibility is not given or an insufficient amount of points has
 *          been captured, shifted the very last deviation angles to the end of
 *          the result table
 * @version 7_0_F UP 31.03.08 Corrected bug: zero angles shown in case of a
 *          required adjustment <360�
 * @version 9_0_F UP 18.04.08 (formerly known as 8_0_T) Changed default value of
 *          "HEATING_WIRE_MIN_PIX_WIDTH" (configuration file) from 3 to 10 to
 *          avoid plausibility check for pixels located close to the border of
 *          the picture Renamed function "applyMediaFilter" to "applyMeanFilter"
 *          for correct terminology Extended the plausibility checking feature
 *          to check with two neighboring points, changed angle calculation
 *          algorithm
 * @version 10_0_F UP 02.06.08 Fixed a bug where organizePairsAB erased a point
 *          pair to which organize Points would apply plausibility checking in
 *          the next step
 * @version 11_0_F UP 08.07.08 Fixed a bug where organizePairsAB didn't erase
 *          the last pair if it was not plausible to the previous one. Changed
 *          parseLinePoints to nominate up to five possible maxima and return
 *          the two most promising points.
 * @version 12_0_F UP 02.09.08 Changed algorithm that calculates the revolutions
 *          and remaining angle for rotational adjustment with a screwdriver.
 * @version 13_0_F UP 04.09.08 Changed display of screwing angle to tens of
 *          degrees.
 * @version 14_0_F UP 25.09.08 Inserted special output (tolerances) for RR4
 *          vehicles
 * @version 15_0_F UP 27.10.08 Removed special output for RR4 vehicles, changed
 *          rounding algorithm for tens of degrees
 * @version 16_0_F UP 17.09.09 Deactivated output of every single heat point in
 *          debug mode, added optional arguments VEHICLE_HEIGHT_FL,
 *          VEHICLE_HEIGHT_FR, VEHICLE_WIDTH_FRONT which influence the
 *          calculated rotation angle if given, reduced the GUI-log output.
 * @version 17_0_F UP 03.02.10 Corrected Array Index Bug in parseLinePoints,
 *          added APDM output with first measured rotational angle
 * @version 18_0_F UP 24.08.10 Changed result name of first measured rotational
 *          angle, added automatic method to calculate the threshold value for
 *          cross detection (configurable)
 * @version 19_0_F UP 13.09.10 Fixed a bug where both threshold and min pix
 *          width were set to zero in case threshold mode was set to manual
 * @version 20_0_F UP 13.05.12 Included handling of different camera types
 *          NIVI2, NIVI3 and NIVI3XL, activated failures in case of necessary
 *          horizontal or vertical adjustments
 * @version 21_0_F UP 03.09.12 Corrections and additional log outputs added
 * @version 22_0_T UP 28.11.12 Bugfix for version 21 to be tested in the plants
 * @version 23_0_F UP 04.12.12 Official release of v21
 * @version 24_0_F UP 13.02.13 Included a workaround for NIVI3 where negative
 *          responses from the ECU during line reading can be fixed by a ECU reset
 * @version 25_0_F UP 14.02.13 Added plausibility check for alpha angle between
 *          lines and APDM output for ECU resets caused by workaround from v24.
 * @version 26_0_F UP 09.09.13 Added special handling of rotational angle for RR4
 *          as this model's camera (3XL) can't be reached manually for adjustment.
 *          Therefore it is handled as NIVI3 camera and the image is turned
 *          electronically.
 * @version 27_0_F UP 20.01.14 Fine tuned algorithm in OrganizePoints() and
 *          OrganizePairsAB()
 * @version 28_0_T UP 12.05.15 Following the changes of v24 also the result
 *          NO_RESPONSE_FROM_CONTROL_UNIT triggers a ECU reset (LOP 1942)
 * @version 29_0_F UP 22.05.15 Production release including the changes of v28
 */
public class SDiagNivi_29_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	// button consts
	private static final int NO_BUTTON = 0;
	private static final int CANCEL_BUTTON = 1;
	private static final int REPEAT_BUTTON = 2;
	// camera consts
	private static final int CAMERA_IMAGE_HEIGHT = 240; // px
	private static final int CAMERA_IMAGE_WIDTH = 320; // px
	// line math consts
	private static final int SLOPE = 0;
	private static final int CROSSING = 1;
	private static final int ANGLE = 2;
	// points consts
	private static final int NONE = -999; // pxValue
	private static final int X = 0; // arrayIndex
	private static final int Y = 1; // arrayIndex
	private static final int LOOKUP_Y = 0; // arrayIndex
	private static final int LOOKUP_X1 = 1; // arrayIndex
	private static final int LOOKUP_X2 = 2; // arrayIndex
	// orientation consts
	private static final int LEFT = 0;
	private static final int RIGHT = 1;
	private static final int UP = 2;
	private static final int DOWN = 3;
	// camera type consts
	private static final int NIVI3 = 2;
	// swing consts
	private static final int ARROW_AREA = 20; // px
	private static final int VAR_CAR_ORDER = 0;
	private static final int VAR_TEST_NUMBER = 1;
	private static final int VAR_TEST_DURATION = 2;
	private static final int VAR_ADJ_ROTATIONAL = 3;
	private static final int VAR_ADJ_HORIZONTAL = 4;
	private static final int VAR_ADJ_VERTICAL = 5;
	private static final int VAR_CAMERA_TYPE = 6;
	// diagnostics consts
	private static final int MAX_DIAG_RESET = 4;

	/**
	 * Default-Konstruktor, nur fuer die Deserialisierung
	 */
	public SDiagNivi_29_0_F_Pruefprozedur() {
	}

	/**
	 * Erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling
	 *            Klasse des zugeh. Prueflings
	 * @param pruefprozName
	 *            Name der Pruefprozedur
	 * @param hasToBeExecuted
	 *            Ausfuehrbedingung f�r Fehlerfreiheit
	 */
	public SDiagNivi_29_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "SGBD", "SGBD_JOB", "SGBD_RESULT", "HEIGHT_TARGET", // mm
		"HEIGHT_CAMERA", // mm
		"DISTANCE_CAMERA_TARGET", // mm
		"CAMERA_HORIZONTAL_APERTURE_ANGLE", // �
		"CAMERA_VERTICAL_APERTURE_ANGLE", // �
		"TOLERANCE_ROTATION_ANGLE", // �
		"TOLERANCE_HORIZONTAL_ANGLE", // �
		"TOLERANCE_VERTICAL_ANGLE", // �
		"ANGLE_CONVERSION_FACTOR", "WORK_DIRECTORY" };
		return args;
	}

	/**
	 * Liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "SGBD_ARGUMENTS", // DEFAULT = ""
		"VEHICLE_HEIGHT_FL", // DEFAULT = null
		"VEHICLE_HEIGHT_FR", // DEFAULT = null
		"VEHICLE_WIDTH_FRONT", // DEFAULT = 0
		"RESULT_NAME_ROTATION_ANGLE", // DEFAULT = "ROTATION_ANGLE"
		"RESULT_NAME_HORIZONTAL_ANGLE", // DEFAULT = "HORIZONTAL_ANGLE"
		"RESULT_NAME_VERTICAL_ANGLE", // DEFAULT = "VERTICAL_ANGLE"
		"TEXT_SIZE", // DEFAULT = 50
		"WAIT_TIME_START", // DEFAULT = 0 ms
		"WAIT_TIME_END", // DEFAULT = 3000 ms
		"FAS_DEVICE_TAG", // DEFAULT = null
		"FAS_BUTTON_REPEAT", // DEFAULT = null
		"FAS_BUTTON_CANCEL", // DEFAULT = null
		"SIMULATION_IMAGES_DIRECTORY", // DEFAULT = null
		"CREATE_REPORT", // DEFAULT = ON_ERROR
		"MAX_REPORTS_SIZE", // DEFAULT = 1000000 Bytes (1MB)
		"DEBUG" // DEFAULT = FALSE
		};
		return args;
	}

	/**
	 * Pr�ft - soweit wie m�glich - die Argumente auf Existenz und Wert.
	 * �berschreibt die parent-Methode aufgrund der offenen Anzahl an Results
	 * 
	 * @return true, wenn Ueberpruefung der Argumente erfolgreich
	 */
	public boolean checkArgs() {
		boolean debug = (getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;

		try {
			if( !super.checkArgs() ) {
				log( debug, "Error", "super.checkArgs failed!" );
				throw new Exception( PB.getString( "Parameterexistenz" ) );
			}
			// ---------------------- req: SGBD, SGBD_JOB, SGBD_RESULT,
			// HEIGHT_TARGET, HEIGHT_CAMERA, DISTANCE_CAMERA_TARGET,
			// TOLERANCE_ROTATION_ANGLE, TOLERANCE_HORIZONTAL_ANGLE,
			// TOLERANCE_VERTICAL_ANGLE,
			// CAMERA_HORIZONTAL_APERTURE_ANGLE, CAMERA_VERTICAL_APERTURE_ANGLE,
			// ANGLE_CONVERSION_FACTOR, WORK_DIRECTORY
			// SGBD
			if( extractArg( "SGBD" ).equalsIgnoreCase( "" ) )
				throw new Exception( "SGBD empty!" );
			// SGBD_JOB
			if( extractArg( "SGBD_JOB" ).equalsIgnoreCase( "" ) )
				throw new Exception( "SGBD_JOB empty!" );
			// SGBD_RESULT
			if( extractArg( "SGBD_RESULT" ).equalsIgnoreCase( "" ) )
				throw new Exception( "SGBD_RESULT empty!" );
			// HEIGHT_CAMERA > 0
			if( new Integer( extractArg( "HEIGHT_CAMERA" ) ).intValue() <= 0 )
				throw new Exception( "HEIGHT_CAMERA out of range! (>0): " + extractArg( "HEIGHT_CAMERA" ) );
			// HEIGHT_TARGET > 0
			if( new Integer( extractArg( "HEIGHT_TARGET" ) ).intValue() <= 0 )
				throw new Exception( "HEIGHT_TARGET out of range! (>0): " + extractArg( "HEIGHT_TARGET" ) );
			// DISTANCE_CAMERA_TARGET > 0 ?
			if( new Integer( extractArg( "DISTANCE_CAMERA_TARGET" ) ).intValue() <= 0 )
				throw new Exception( "DISTANCE_CAMERA_TARGET out of range! (>0): " + extractArg( "DISTANCE_CAMERA_TARGET" ) );
			// ROTATION_ANGLE_TOLERANCE >= 0 ?
			if( new Double( extractArg( "TOLERANCE_ROTATION_ANGLE" ) ).doubleValue() < 0 )
				throw new Exception( "TOLERANCE_ROTATION_ANGLE out of range! (>=0): " + extractArg( "TOLERANCE_ROTATION_ANGLE" ) );
			// HORIZONTAL_ANGLE_TOLERANCE >= 0 ?
			if( new Double( extractArg( "TOLERANCE_HORIZONTAL_ANGLE" ) ).doubleValue() < 0 )
				throw new Exception( "TOLERANCE_HORIZONTAL_ANGLE out of range! (>=0): " + extractArg( "TOLERANCE_HORIZONTAL_ANGLE" ) );
			// CAMERA_HORIZONTAL_APERTURE_ANGLE > 0 ?
			if( new Double( extractArg( "CAMERA_HORIZONTAL_APERTURE_ANGLE" ) ).doubleValue() <= 0 )
				throw new Exception( "CAMERA_HORIZONTAL_APERTURE_ANGLE out of range! (>0): " + extractArg( "CAMERA_HORIZONTAL_APERTURE_ANGLE" ) );
			// CAMERA_VERTICAL_APERTURE_ANGLE > 0 ?
			if( new Double( extractArg( "CAMERA_VERTICAL_APERTURE_ANGLE" ) ).doubleValue() <= 0 )
				throw new Exception( "CAMERA_VERTICAL_APERTURE_ANGLE out of range! (>0): " + extractArg( "CAMERA_VERTICAL_APERTURE_ANGLE" ) );
			/*
			 * // ANGLE_CONVERSION_FACTOR > 0 ? if (new
			 * Double(extractArg("ANGLE_CONVERSION_FACTOR")).doubleValue() <= 0)
			 * throw new
			 * Exception("ANGLE_CONVERSION_FACTOR out of range! (>0): "+
			 * extractArg("ANGLE_CONVERSION_FACTOR"));
			 */
			// WORK_DIRECTORY defined ?
			File workDir = new File( extractArg( "WORK_DIRECTORY" ) );
			if( !workDir.exists() ) {
				// create...
				if( !workDir.mkdir() ) {
					throw new Exception( "WORK_DIRECTORY invalid! (unable to create...): " + extractArg( "WORK_DIRECTORY" ) );
				}
				log( debug, "Ok", "work dir created. " + workDir.getAbsolutePath() );
			} else {
				// already exists...
				if( !workDir.isDirectory() ) {
					throw new Exception( "WORK_DIRECTORY invalid! (not a directory...): " + extractArg( "WORK_DIRECTORY" ) );
				}
				if( !workDir.canWrite() ) {
					throw new Exception( "WORK_DIRECTORY invalid! (not writable...): " + extractArg( "WORK_DIRECTORY" ) );
				}
			}
			// ---------------------- opt: WAIT_TIME_START, WAIT_TIME_END,
			// TEXT_SIZE, CREATE_REPORT
			// WAIT_TIME_START > 0 ?
			if( extractArg( "WAIT_TIME_START" ) != null && new Long( extractArg( "WAIT_TIME_START" ) ).longValue() <= 0L )
				throw new Exception( "WAIT_TIME_START out of range! (>0): " + extractArg( "WAIT_TIME_START" ) );
			// WAIT_TIME_END > 0 ?
			if( extractArg( "WAIT_TIME_END" ) != null && new Long( extractArg( "WAIT_TIME_END" ) ).longValue() <= 0L )
				throw new Exception( "WAIT_TIME_END out of range! (>0): " + extractArg( "WAIT_TIME_END" ) );
			// TEXT_SIZE > 0 ?
			if( extractArg( "TEXT_SIZE" ) != null && new Integer( extractArg( "TEXT_SIZE" ) ).intValue() <= 0 )
				throw new Exception( "TEXT_SIZE out of range! (>0): " + extractArg( "TEXT_SIZE" ) );
			// CREATE_REPORT is NEVER/ON_ERROR/ALWAYS ?
			if( extractArg( "CREATE_REPORT" ) != null ) {
				if( !extractArg( "CREATE_REPORT" ).equalsIgnoreCase( "NEVER" ) && !extractArg( "CREATE_REPORT" ).equalsIgnoreCase( "ON_ERROR" ) && !extractArg( "CREATE_REPORT" ).equalsIgnoreCase( "ALWAYS" ) )
					throw new Exception( "CREATE_REPORT invalid! (NEVER/ON_ERROR/ALWAYS): " + extractArg( "CREATE_REPORT" ) );
			}
			return true;

		} catch( InformationNotAvailableException e ) {
			log( debug, "Error", "checking args, InformationNotAvailableException: " + e.getMessage() );
			return false;
		} catch( NumberFormatException e ) {
			log( debug, "Error", "checking args, NumberFormatException: " + e.getMessage() );
			return false;
		} catch( Exception e ) {
			log( debug, "Error", "checking args, Exception: " + e.getMessage() );
			return false;
		}
	}

	/**
	 * Fuehrt die Pruefprozedur aus
	 * 
	 * @param info
	 *            Information zur Ausfuehrung
	 */
	public void execute( ExecutionInfo info ) {
		// Constants
		final String CONFIG_FILENAME = "config.ini";
		final String[][] VAR_NAMES = { { "VIN:", "Fahrgestellnummer: " }, // 0
		{ "Test Number: ", "Pr�fungsnummer: " }, // 1
		{ "Test Duration: ", "Pr�fungsdauer: " }, // 2
		{ "Rotational Adjustment: ", "Rotationsjustage: " }, // 3
		{ "Horizontal Adjustment(<->): ", "Horizontaljustage (<->):" }, // 4
		{ "Vertical Adjustment: ", "Vertikaljustage: " }, // 5
		{ "Camera Type: ", "Kameratyp: " } // 6
		};
		// Global CASCADE parameters
		boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;

		String fasTag = getArg( "FAS_DEVICE_TAG" ); // Default is null
		String fasButtonRepeat = getArg( "FAS_BUTTON_REPEAT" ); // Default is null
		String fasButtonCancel = getArg( "FAS_BUTTON_CANCEL" ); // Default is null
		String simImgsDir = getArg( "SIMULATION_IMAGES_DIRECTORY" ); // Default is null

		// swings
		JFrame frame = null;
		JPanel topPanel = null; // TOP panel
		Font bigFont = null;
		JProgressBar progressBar = null;
		JPanel middlePanel = null; // MIDDLE panel
		Font mediumFont = null;
		JPanel imgsPanel = null;
		JLabel targetImgLabel = null;
		JLabel lookupImgLabel = null;
		JPanel spacerPanel1 = null;
		JPanel spacerPanel2 = null;
		JPanel infoPanel = null;
		JLabel[] varNamesLabels = null;
		JPanel varNamesPanel = null;
		JLabel[] varValuesLabels = null;
		JPanel varValuesPanel = null;
		JPanel bottomPanel = null; // BOTTOM panel
		JPanel textPanel = null;
		JLabel textLabel = null;
		JPanel areasPanel = null;
		JTextArea logArea = null;
		JScrollPane logScroll = null;
		JTextArea configArea = null;
		JScrollPane configScroll = null;

		// aux
		int status = STATUS_EXECUTION_ERROR;
		Vector<Ergebnis> resList = new Vector<Ergebnis>();
		EdiabasProxyThread ediabas = null;

		short[] binres;
		String sgbdStatus = "";
		String sgbdDynArgs = "";
		String sgbdResultName = "";
		int sgbdResultOffset = 1;
		String[] sgbdResultComp = { "", "" };
		int cameraType = NONE;
		int ecuResetCounter = 0;

		FasControl fas = null;
		int step = 0;
		float[][] pts = null;
		double[] line1 = { 0.0, 0.0, 0.0 }; // line1 math {slope, crossing, angle}
		double[] line2 = { 0.0, 0.0, 0.0 }; // line2 math {slope, crossing, angle}

		DecimalFormat decForm = (DecimalFormat) DecimalFormat.getInstance( Locale.ENGLISH );
		decForm.applyPattern( "0.00" );

		BufferedImage targetImg = new BufferedImage( CAMERA_IMAGE_WIDTH, CAMERA_IMAGE_HEIGHT, BufferedImage.TYPE_INT_RGB/* TYPE_3BYTE_BGR */);
		BufferedImage lookupImg = new BufferedImage( CAMERA_IMAGE_WIDTH, CAMERA_IMAGE_HEIGHT, BufferedImage.TYPE_INT_RGB/* TYPE_3BYTE_BGR */);
		int dlgResult = REPEAT_BUTTON;
		String simImgFile = null;
		String tmp = "";
		Color guiColor = Color.GREEN;
		String guiMsg = "";
		int lang = System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) ? 1 : 0; // 0: English, 1: German
		int count = 0;
		long startTime = 0;
		Properties config = null;
		String auftrag = this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7();
		boolean readOutSuccessful = false;

		try {
			ediabas = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();

			log( debug, "Var", "---> " + getClassName() + (simImgsDir != null ? "[sim: " + simImgsDir + "]" : "[auftrag: " + auftrag + "]") );
			// -------------------------------------------------------------------------------------------
			// check/extract args
			// -------------------------------------------------------------------------------------------
			if( !checkArgs() ) {
				// NIO - check args failed!
				status = STATUS_EXECUTION_ERROR;
				resList.add( new Ergebnis( "StatusCheckArgs", "SDiagNivi", "", "", "", "CheckArgsNOk", "NOk", "Ok", "Ok", "0", "", "", "", PB.getString( "parameterexistenz" ), "", Ergebnis.FT_NIO ) );
				log( debug, "Error", "check args failed." );
				throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			}
			// IO
			resList.add( new Ergebnis( "StatusCheckArgs", "SDiagNivi", "", "", "", "CheckArgsOk", "Ok", "Ok", "Ok", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
			log( debug, "Ok", "check args ok." );

			// CASCADE parameters
			String sgbd = extractArg( "SGBD" );
			String sgbdJob = extractArg( "SGBD_JOB" );
			String sgbdResult = extractArg( "SGBD_RESULT" );
			int heightTarget = new Integer( extractArg( "HEIGHT_TARGET" ) ).intValue(); // mm
			int heightCamera = new Integer( extractArg( "HEIGHT_CAMERA" ) ).intValue(); // mm
			int distCamTarget = new Integer( extractArg( "DISTANCE_CAMERA_TARGET" ) ).intValue(); // mm
			
			double heightFL = (extractArg( "VEHICLE_HEIGHT_FL" ) != null) ? new Double( extractArg( "VEHICLE_HEIGHT_FL" ) ).doubleValue() : 0; // mm
			double heightFR = (extractArg( "VEHICLE_HEIGHT_FR" ) != null) ? new Double( extractArg( "VEHICLE_HEIGHT_FR" ) ).doubleValue() : 0; // mm
			double vehicleWidthFront = (extractArg( "VEHICLE_WIDTH_FRONT" ) != null) ? new Double( extractArg( "VEHICLE_WIDTH_FRONT" ) ).doubleValue() : 0; // mm
			double rotAngCarFront = 0;

			double rotAngleTol = new Double( extractArg( "TOLERANCE_ROTATION_ANGLE" ) ).doubleValue(); // �
			double horAngleTol = new Double( extractArg( "TOLERANCE_HORIZONTAL_ANGLE" ) ).doubleValue(); // �
			double verAngleTol = new Double( extractArg( "TOLERANCE_VERTICAL_ANGLE" ) ).doubleValue(); // �
			double horAngleAperture = new Double( extractArg( "CAMERA_HORIZONTAL_APERTURE_ANGLE" ) ).doubleValue(); // �
			double verAngleAperture = new Double( extractArg( "CAMERA_VERTICAL_APERTURE_ANGLE" ) ).doubleValue(); // �
			double angleConvFactor = new Double( extractArg( "ANGLE_CONVERSION_FACTOR" ) ).doubleValue();
			File workDir = new File( extractArg( "WORK_DIRECTORY" ) );
			String sgbdArgs = (extractArg( "SGBD_ARGUMENTS" ) != null) ? extractArg( "SGBD_ARGUMENTS" ) : "";
			String rotAngleResName = (extractArg( "RESULT_NAME_ROTATION_ANGLE" ) != null ? extractArg( "RESULT_NAME_ROTATION_ANGLE" ) : "NIVI_ROTATION_ANGLE");
			String horAngleResName = (extractArg( "RESULT_NAME_HORIZONTAL_ANGLE" ) != null ? extractArg( "RESULT_NAME_HORIZONTAL_ANGLE" ) : "NIVI_HORIZONTAL_ANGLE");
			String verAngleResName = (extractArg( "RESULT_NAME_VERTICAL_ANGLE" ) != null ? extractArg( "RESULT_NAME_VERTICAL_ANGLE" ) : "NIVI_VERTICAL_ANGLE");
			String createReport = (extractArg( "CREATE_REPORT" ) != null) ? extractArg( "CREATE_REPORT" ) : "ON_ERROR";
			long maxReportsSize = (extractArg( "MAX_REPORTS_SIZE" ) != null) ? new Long( extractArg( "MAX_REPORTS_SIZE" ) ).longValue() : 1000000; // Bytes
			// (1MB)
			int textSize = (extractArg( "TEXT_SIZE" ) != null) ? new Integer( extractArg( "TEXT_SIZE" ) ).intValue() : 50;
			long waitTimeStart = (extractArg( "WAIT_TIME_START" ) != null) ? new Long( extractArg( "WAIT_TIME_START" ) ).longValue() : 0; // ms
			long waitTimeEnd = (extractArg( "WAIT_TIME_END" ) != null) ? new Long( extractArg( "WAIT_TIME_END" ) ).longValue() : 3000; // ms
			// others
			int heightOffset = heightTarget - heightCamera; // mm
			int yOffset = 0; // px
			int[] iPointIdeal = { -1, -1 };
			int[] iPointReal = { -1, -1 };

			double alphaAngleCalc = 0;
			double alphaAngleConfig = 0;
			double alphaAngleLimit = 0;

			if( vehicleWidthFront != 0 ) {
				rotAngCarFront = Math.atan( (0.0 + heightFL - heightFR) / vehicleWidthFront ); // rad, from drivers view CCW -,
				// CW +, add 0.0 to perform a
				// floating point division
			}
			double heightDeviance = 0, widthDeviance = 0;
			double idealAngle = 0, realAngle = 0;
			double rotAngle = 0, horAngle = 0, verAngle = 0;
			double verAngleTolUp = 0, verAngleTolDown = 0;
			
			// Special handling of rotation angle for RR4
			boolean isRollsRoyceGhost = this.getPr�fling().getAuftrag().getBaureihe().equalsIgnoreCase( "RR4" );

			// -------------------------------------------------------------------------------------------
			// get FAS device
			// -------------------------------------------------------------------------------------------
			// uses the FAS device only when not in simulation mode and when
			// both buttons are given
			if( simImgsDir == null && fasButtonRepeat != null && fasButtonCancel != null ) {
				try {
					fas = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getFasControl( fasTag );
					if( fas == null ) {
						throw new Exception( PB.getString( "nichtVerfuegbar" ) );
					}
					// IO
					resList.add( new Ergebnis( "StatusFasControlDevice", "SDiagNivi", "tag: " + fasTag, "", "", "FasControlDeviceOk", "OK", "OK", "OK", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
					log( debug, "Ok", "got FasControl device (tag: " + fasTag + "), fas = " + fas.toString() );

				} catch( Exception e ) {
					// NIO - FasControl device error!
					status = STATUS_EXECUTION_ERROR;
					resList.add( new Ergebnis( "StatusFasControlDevice", "SDiagNivi", "tag: " + fasTag, "", "", "FasControlDeviceNOk", "NOK", "OK", "OK", "0", "", "", "", e.getMessage(), "", Ergebnis.FT_NIO ) );
					log( debug, "Error", "getting FasControl device (tag: " + fasTag + "), Exception: " + e.getMessage() );
					throw new PPExecutionException();
				}
			}

			// -------------------------------------------------------------------------------------------
			// Determine camera type and adapt rotation tolerance
			// -------------------------------------------------------------------------------------------
			try {
				if( simImgsDir == null ) {

					log( debug, "Info", "Trying to determine camera type..." );
					sgbdStatus = ediabas.executeDiagJob( sgbd, "SENSOREN_IDENT_LESEN", "", "" );
					sgbdStatus = "OKAY";

					try {
						tmp = ediabas.apiResultText( "SENSOR_PART_NR", 1, "" );
						tmp = ediabas.getDiagResultValue( 1, "SENSOR_PART_NR" );
						log( debug, "Info", "Camera Sensor Part No.: " + tmp );
						cameraType = Integer.parseInt( (tmp.substring( 0, 2 )) ); // first two digits of part no contain camera type
					} catch( Exception e ) {
						e.printStackTrace();
					}

					if( cameraType == NIVI3 || isRollsRoyceGhost ) {
						// NIVI3 is able to compensate rotation angles up to �3�
						rotAngleTol = 3;
					}
				}
			} catch( ApiCallFailedException e ) {
				e.printStackTrace();
				log( debug, "Error", "Ediabas Api-Call failed. Diagnostics not available." );
			} catch( EdiabasResultNotFoundException e ) {
				e.printStackTrace();
				log( debug, "Error", "The desired result couldn't be found in the diagnostics response." );
			} catch( Exception e ) {
				e.printStackTrace();
			}

			// --------------------------------------------------------------------------------------------------
			// swings init
			// --------------------------------------------------------------------------------------------------
			Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
			bigFont = new Font( null, Font.BOLD, textSize );
			mediumFont = new Font( null, Font.BOLD, Math.round( textSize * 2 / 5 ) );

			frame = new JFrame( lang == 1 ? "NIVI-Kamera Justage  |  Version " + this.version + " |  BMW Group, TI-544" : "NIVI Camera Adjustment  |  Version  " + this.version + "  |  BMW Group, TI-544" );

			frame.setSize( screenDim.width, screenDim.height );
			frame.setDefaultCloseOperation( WindowConstants.DO_NOTHING_ON_CLOSE );
			frame.setResizable( false );
			frame.setLocation( screenDim.width / 2 - frame.getWidth() / 2, screenDim.height / 2 - frame.getHeight() / 2 );

			// TOP panel
			Dimension topPanelDim = new Dimension( screenDim.width, screenDim.height * 1 / 5 );
			progressBar = new JProgressBar( JProgressBar.HORIZONTAL, 0, 100 );
			progressBar.setIndeterminate( false );
			progressBar.setStringPainted( true );
			progressBar.setString( "" );
			progressBar.setFont( bigFont );
			topPanel = new JPanel( new BorderLayout() );
			topPanel.setBorder( BorderFactory.createLoweredBevelBorder() );
			topPanel.setPreferredSize( topPanelDim );
			topPanel.add( progressBar );
			frame.getContentPane().add( topPanel, BorderLayout.PAGE_START );

			// MIDDLE panel
			Dimension middlePanelDim = new Dimension( screenDim.width, ((CAMERA_IMAGE_HEIGHT > mediumFont.getSize() * VAR_NAMES.length) ? CAMERA_IMAGE_HEIGHT : mediumFont.getSize() * VAR_NAMES.length) + 100 );
			targetImgLabel = new JLabel( new ImageIcon( targetImg ) );
			targetImgLabel.setBorder( BorderFactory.createEmptyBorder() );
			lookupImgLabel = new JLabel( new ImageIcon( lookupImg ) );
			lookupImgLabel.setBorder( BorderFactory.createEmptyBorder() );
			spacerPanel1 = new JPanel();
			spacerPanel1.setPreferredSize( new Dimension( 5, middlePanelDim.height ) );
			spacerPanel1.setBorder( BorderFactory.createEmptyBorder() );
			imgsPanel = new JPanel();
			imgsPanel.setLayout( new BoxLayout( imgsPanel, BoxLayout.X_AXIS ) );
			// imgsPanel.setPreferredSize(new Dimension((2*CAMERA_IMAGE_WIDTH)+
			// 10, middlePanelDim.height));
			imgsPanel.setAlignmentY( Component.CENTER_ALIGNMENT );
			imgsPanel.setAlignmentX( Component.CENTER_ALIGNMENT );
			imgsPanel.setBorder( BorderFactory.createTitledBorder( BorderFactory.createLoweredBevelBorder(), " Bildauswertung " ) );
			imgsPanel.add( targetImgLabel );
			imgsPanel.add( spacerPanel1 );
			imgsPanel.add( lookupImgLabel );
			varNamesPanel = new JPanel();
			varNamesPanel.setLayout( new BoxLayout( varNamesPanel, BoxLayout.Y_AXIS ) );
			varNamesPanel.setPreferredSize( new Dimension( (middlePanelDim.width - 2 * CAMERA_IMAGE_WIDTH - 20) * 2 / 3, middlePanelDim.height ) );
			varNamesLabels = new JLabel[VAR_NAMES.length];
			for( int i = 0; i < VAR_NAMES.length; i++ ) {
				varNamesLabels[i] = new JLabel( VAR_NAMES[i][lang], SwingConstants.LEFT );
				varNamesLabels[i].setFont( mediumFont );
				varNamesPanel.add( varNamesLabels[i] );
			}
			varValuesPanel = new JPanel();
			varValuesPanel.setLayout( new BoxLayout( varValuesPanel, BoxLayout.Y_AXIS ) );
			varValuesPanel.setPreferredSize( new Dimension( (middlePanelDim.width - 2 * CAMERA_IMAGE_WIDTH - 20) * 1 / 3, middlePanelDim.height ) );
			varValuesLabels = new JLabel[VAR_NAMES.length];
			for( int i = 0; i < VAR_NAMES.length; i++ ) {
				if( i == VAR_CAR_ORDER ) {
					tmp = auftrag;
				} else if( i == VAR_CAMERA_TYPE ) {
					switch( cameraType ) {
						case 0:
							tmp = "NIVI2";
							break;
						case 1:
							tmp = "NIVI3XL";
							break;
						case 2:
							tmp = "NIVI3";
							break;
						default:
							tmp = "...";
					}
				} else {
					tmp = "...";
				}
				varValuesLabels[i] = new JLabel( tmp, SwingConstants.LEFT );
				varValuesLabels[i].setFont( mediumFont );
				varValuesLabels[i].setOpaque( true );
				varValuesPanel.add( varValuesLabels[i] );
			}

			spacerPanel2 = new JPanel();
			spacerPanel2.setPreferredSize( new Dimension( 5, middlePanelDim.height ) );
			spacerPanel2.setBorder( BorderFactory.createEmptyBorder() );
			infoPanel = new JPanel();
			infoPanel.setLayout( new BoxLayout( infoPanel, BoxLayout.X_AXIS ) );
			// imgsPanel.setPreferredSize(new Dimension(middlePanelDim.width -
			// 2*CAMERA_IMAGE_WIDTH - 10, middlePanelDim.height + 10));
			infoPanel.setBorder( BorderFactory.createTitledBorder( BorderFactory.createLoweredBevelBorder(), " Auftragsdaten " ) );
			infoPanel.add( varNamesPanel );
			infoPanel.add( spacerPanel2 );
			infoPanel.add( varValuesPanel );
			middlePanel = new JPanel();
			middlePanel.setLayout( new BoxLayout( middlePanel, BoxLayout.X_AXIS ) );
			middlePanel.setPreferredSize( middlePanelDim );
			middlePanel.setAlignmentX( Component.CENTER_ALIGNMENT );
			middlePanel.setAlignmentY( Component.CENTER_ALIGNMENT );
			middlePanel.add( imgsPanel );
			middlePanel.add( infoPanel );
			frame.getContentPane().add( middlePanel, BorderLayout.CENTER );

			// BOTTOM panel
			Dimension bottomPanelDim = new Dimension( screenDim.width, screenDim.height - topPanelDim.height - middlePanelDim.height );
			logArea = new JTextArea( "" );
			logArea.setEditable( false );
			logArea.setWrapStyleWord( true );
			logArea.setForeground( Color.orange );
			logArea.setBackground( Color.BLACK );
			logScroll = new JScrollPane( logArea );
			logScroll.setPreferredSize( new Dimension( bottomPanelDim.width - CAMERA_IMAGE_WIDTH - 25, bottomPanelDim.height ) );
			logScroll.setAutoscrolls( true );
			logScroll.setBorder( BorderFactory.createTitledBorder( BorderFactory.createLoweredBevelBorder(), " Protokoll " ) );
			configArea = new JTextArea( "" );
			configArea.setEditable( false );
			configArea.setWrapStyleWord( true );
			configArea.setBackground( null );
			configScroll = new JScrollPane( configArea );
			configScroll.setPreferredSize( new Dimension( CAMERA_IMAGE_WIDTH, bottomPanelDim.height ) );
			configScroll.setAutoscrolls( true );
			configScroll.setBorder( BorderFactory.createTitledBorder( BorderFactory.createLoweredBevelBorder(), " Konfiguration " ) );
			textLabel = new JLabel( "" );
			textLabel.setFont( mediumFont );
			textLabel.setOpaque( false );
			textPanel = new JPanel( new FlowLayout( FlowLayout.CENTER ) );
			textPanel.setVisible( false );
			textPanel.setBackground( Color.YELLOW );
			textPanel.setMaximumSize( new Dimension( screenDim.width, mediumFont.getSize() + 10 ) );
			textPanel.setOpaque( true );
			textPanel.add( textLabel );
			areasPanel = new JPanel();
			areasPanel.setLayout( new BoxLayout( areasPanel, BoxLayout.X_AXIS ) );
			areasPanel.add( logScroll );
			areasPanel.add( configScroll );
			bottomPanel = new JPanel();
			bottomPanel.setLayout( new BoxLayout( bottomPanel, BoxLayout.Y_AXIS ) );
			bottomPanel.setPreferredSize( bottomPanelDim );
			bottomPanel.add( areasPanel );
			bottomPanel.add( textPanel );
			frame.getContentPane().add( bottomPanel, BorderLayout.PAGE_END );
			//
			frame.setVisible( true );
			appSleep( waitTimeStart );

			// -------------------------------------------------------------------------------------------
			// Simulation dialog
			// -------------------------------------------------------------------------------------------
			if( simImgsDir != null ) {
				// show simulation images dialog
				simImgFile = GUI_showImgsDlg( debug, frame, simImgsDir );
				if( simImgFile != null ) {
					dlgResult = REPEAT_BUTTON;
				} else {
					dlgResult = CANCEL_BUTTON;
				}
			}

			// --------------------------------------------------------------------------------------------------
			// Calibration cycle
			// --------------------------------------------------------------------------------------------------
			while( dlgResult == REPEAT_BUTTON ) {

				switch( step ) {
					case 0:
						// reset variables & GUI
						// -------------------------------------------------
						startTime = System.currentTimeMillis();
						log( debug, "Var", "------------- " + count + "x --------------" );
						// config init
						try {
							config = initConfig( debug, workDir + File.separator + CONFIG_FILENAME );
							// IO
							resList.add( new Ergebnis( "StatusReadConfigFile", "SDiagNivi", String.valueOf( count ) + "x", workDir.getAbsolutePath(), "", "ReadConfigFileOk", "OK", "OK", "OK", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
							log( debug, "Var", "config: " + config.toString() );

						} catch( Exception e ) {
							// NIO - config.ini!
							status = STATUS_EXECUTION_ERROR;
							resList.add( new Ergebnis( "StatusReadConfigFile", "SDiagNivi", String.valueOf( count ) + "x", workDir.getAbsolutePath(), "", "ReadConfigFileNOk", "NOK", "OK", "OK", "0", "", "", "", e.getMessage(), "", Ergebnis.FT_NIO ) );
							log( debug, "Error", "config init failure (dir: " + workDir + "), Exception: " + e.getMessage() );
							throw new PPExecutionException( "config.ini (dir: " + workDir + "), Exception: " + e.getMessage() );
						}
						pts = getLookupPoints( config );
						// printPoints(debug, "Generated Lookup Points:", pts);
						count++;
						// Clean GUI
						progressBar.setValue( 0 );
						progressBar.setForeground( Color.YELLOW );
						progressBar.setString( lang == 1 ? "Justage wird durchgef�hrt..." : "Performing adjustment..." );
						GUI_fillImage( targetImg, Color.BLACK );
						targetImgLabel.setIcon( new ImageIcon( targetImg ) );
						GUI_fillImage( lookupImg, Color.BLACK );
						lookupImgLabel.setIcon( new ImageIcon( lookupImg ) );
						varValuesLabels[VAR_TEST_NUMBER].setText( String.valueOf( count ) );
						varValuesLabels[VAR_TEST_DURATION].setText( "..." );
						varValuesLabels[VAR_ADJ_ROTATIONAL].setBackground( null );
						varValuesLabels[VAR_ADJ_ROTATIONAL].setText( "..." );
						varValuesLabels[VAR_ADJ_HORIZONTAL].setBackground( null );
						varValuesLabels[VAR_ADJ_HORIZONTAL].setText( "..." );
						varValuesLabels[VAR_ADJ_VERTICAL].setBackground( null );
						varValuesLabels[VAR_ADJ_VERTICAL].setText( "..." );
						textLabel.setText( "" );
						textPanel.setVisible( false );
						logArea.setText( "" );
						GUI_showConfig( config, configArea );

						GUI_log( debug, "Ok", "Starting (" + count + "x)... " + new Date().toString() + " " + (simImgFile != null ? simImgFile : auftrag), logArea );
						GUI_log( debug, "Var", "Distance camera target = " + distCamTarget + " mm, Target height = " + heightTarget + " mm, Camera height = " + heightCamera + " mm, Height offset = " + heightOffset + " mm", logArea );
						GUI_log( debug, "Var", "Camera aperture angles: horizontal = " + horAngleAperture + "�(" + CAMERA_IMAGE_WIDTH + " px <=> " + convPix2Mm( distCamTarget, "WIDTH", horAngleAperture, CAMERA_IMAGE_WIDTH ) + " mm), vertical = " + verAngleAperture + "�(" + CAMERA_IMAGE_HEIGHT + " px <=> " + convPix2Mm( distCamTarget, "HEIGHT", verAngleAperture, CAMERA_IMAGE_HEIGHT ) + " mm)", logArea );
						GUI_log( debug, "Ok", "Config init ok. (" + workDir + File.separator + CONFIG_FILENAME + ")", logArea );
						// draw ideal target lines
						yOffset = (-1) * convMm2Pix( distCamTarget, "HEIGHT", verAngleAperture, heightOffset ); // px
						GUI_drawTargetLine( targetImg, rnd( ((double) targetImg.getHeight() / (double) targetImg.getWidth()), 2 ), yOffset, Color.YELLOW, 3 );
						GUI_drawTargetLine( targetImg, rnd( ((double) -targetImg.getHeight() / (double) targetImg.getWidth()), 2 ), (double) (targetImg.getHeight() + yOffset), Color.YELLOW, 3 );
						GUI_drawTargetLine( targetImg, 0.0, (double) (Math.round( targetImg.getHeight() / 2 ) + yOffset), Color.YELLOW, 4 );
						GUI_drawTargetLine( targetImg, Double.POSITIVE_INFINITY, (double) Math.round( targetImg.getWidth() / 2 ), Color.YELLOW, 4 );
						GUI_log( debug, "Ok", "Ideal lines drawed. (with height offset)", logArea );
						step = 1;
						break;

					case 1:
						// capture/draw points & lines
						// -------------------------------------------------
						float[] x1x2 = { NONE, NONE, NONE, NONE };
						BufferedImage simImg = null;
						// read simulation image file if supplied
						if( simImgFile != null ) {
							try {
								simImg = ImageIO.read( new File( simImgFile ) );
								simImg = simImg.getSubimage( Math.round( simImg.getWidth() / 2 ) - (CAMERA_IMAGE_WIDTH / 2), Math.round( simImg.getHeight() / 2 ) - (CAMERA_IMAGE_HEIGHT / 2), CAMERA_IMAGE_WIDTH, CAMERA_IMAGE_HEIGHT );
								ImageIO.write( simImg, "JPG", new File( new File( simImgFile ).getParent() + "/out.jpg" ) );
							} catch( IOException e ) {
								throw new Exception( "Reading simulation image file: " + simImgFile + ", Exception: " + e.getMessage() );
							}
						}
						// capture the specified lookup lines & points
						GUI_log( debug, "Var", "Capturing lines and points...", logArea );
						//String lineStr = "";
						String imgDiagStr = "";
						int x = 0;
						int y = 0;
						int xPart = 0;
						int[] line = new int[CAMERA_IMAGE_WIDTH];

						// capture requested (-> config) number of lines
						for( int l = 0; l < pts.length; l++ ) {
							imgDiagStr = "";
							y = Math.round( pts[l][LOOKUP_Y] );
							xPart = 0;
							if( simImg != null ) {
								// -> simulation...
								// read simulation image line
								readOutSuccessful = true;
								for( x = 0; x < CAMERA_IMAGE_WIDTH; x++ ) {
									line[x] = simImg.getRGB( x, y ) & 0xFF;
									imgDiagStr += String.format("%02X,", line[x]);
								}
								// write image data to log file like it came from ifh trace
								log( debug, "Info", "[Image diagnostics string for line " + (l + 1) + "]: " + imgDiagStr);
							} else {
								// -> diagnostics...
								// capture line (EDIABAS): read both left(1) and
								// right(2) halves of the line
								try {
									for( xPart = 1; xPart <= 2; xPart++ ) {
										readOutSuccessful = false;
										// Read half line...
										sgbdDynArgs = getDynSgbdArgs( sgbdArgs, xPart, y );
										//log(debug, "TEST", "sgbdDynArgs: "+ sgbdDynArgs +" [sgbdArgs: "+ sgbdArgs +" xPart="+ xPart +" y="+ y +"]");
										sgbdStatus = ediabas.executeDiagJob( sgbd, sgbdJob, sgbdDynArgs, "" );
										appSleep( 250 );
										if( !sgbdStatus.equals( "OKAY" ) ) {
											// NIO - Ediabas job status!
											if( sgbdStatus.equals( "ERROR_ECU_CONDITIONS_NOT_CORRECT" ) && ecuResetCounter <= MAX_DIAG_RESET ) {
												// continue with loop and tolerate up to five failures
												GUI_log( true, "Info", "Job " + sgbdJob + " returned ERROR_ECU_CONDITIONS_NOT_CORRECT. Resetting ECU...", logArea );
												sgbdStatus = ediabas.executeDiagJob( sgbd, "STEUERGERAETE_RESET", "", "" );
												ecuResetCounter++;
												// let ECU recover
												appSleep( 5000 ); 
												// reset loop counter to redo this line
												l -= 1; 
												break;
											} else {
												status = STATUS_EXECUTION_ERROR;
												resList.add( new Ergebnis( "DiagError", "EDIABAS", sgbd, sgbdJob, sgbdDynArgs, sgbdResult, sgbdStatus, "OKAY", "OKAY", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO ) );
												resList.add( new Ergebnis( "ECU_RESETS", "SDiagNivi", "ECU Reset Counter", "STEUERGERAETE_RESET", "", "", String.valueOf(ecuResetCounter), "0", "4", String.valueOf(ecuResetCounter), "", "", "", "", "Too many ECU resets performed!", Ergebnis.FT_NIO ) );
												log( debug, "Error", "executing EDIABAS, sgbd: " + sgbd + ", sgbdJob: " + sgbdJob + ", sgbdDynArgs: " + sgbdDynArgs + ", sgbdResult: " + sgbdResult + ", sgbdStatus: " + sgbdStatus + ", job status failed!" );
												throw new PPExecutionException( PB.getString( "Diagnosefehler" ) );
											}
										} else {
											readOutSuccessful = true;
										}

										if( readOutSuccessful ) {
											// to solve the bug in NIVI 1 sgbd
											if( sgbdResult.indexOf( ":" ) == -1 ) {
												// no result offset specified
												sgbdResultName = sgbdResult;
												sgbdResultOffset = 0;
											} else {
												// result offset specified
												sgbdResultComp = sgbdResult.split( ":" );
												sgbdResultName = sgbdResultComp[0];
												sgbdResultOffset = new Integer( sgbdResultComp[1] ).intValue();
											}
											binres = ediabas.apiResultBinary( sgbdResultName, 1 );
											// run through the half line (part1 =
											// [1..160], part2 = [161..320])
											for( int xcam = 1 + sgbdResultOffset; xcam < (int) Math.floor( CAMERA_IMAGE_WIDTH / 2 ) + sgbdResultOffset; xcam++ ) {
												x = (xPart - 1) * (int) Math.floor( CAMERA_IMAGE_WIDTH / 2 ) + xcam - sgbdResultOffset;
												line[x] = binres[xcam];
											}
											// update progress bar
											progressBar.setValue( progressBar.getValue() + 1 );
										}
									}
								} catch( ApiCallFailedException e ) {
									// NIO - Ediabas error!
									if( ecuResetCounter <= MAX_DIAG_RESET ) {
										// continue with loop and tolerate up to five failures
										GUI_log( true, "Info", "Job " + sgbdJob + " returned NO_RESPONSE_FROM_CONTROL_UNIT. Resetting ECU...", logArea );
										sgbdStatus = ediabas.executeDiagJob( sgbd, "STEUERGERAETE_RESET", "", "" );
										ecuResetCounter++;
										// let ECU recover
										appSleep( 5000 ); 
										// reset loop counter to redo this line
										l -= 1;
									} else if( ecuResetCounter > MAX_DIAG_RESET ) {
										status = STATUS_EXECUTION_ERROR;
										resList.add( new Ergebnis( "DiagError", "EDIABAS", sgbd, sgbdJob, sgbdDynArgs, sgbdResult, sgbdStatus, "OKAY", "OKAY", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO ) );
										resList.add( new Ergebnis( "ECU_RESETS", "SDiagNivi", "ECU Reset Counter", "STEUERGERAETE_RESET", "", "", String.valueOf(ecuResetCounter), "0", String.valueOf( MAX_DIAG_RESET ), String.valueOf(ecuResetCounter), "", "", "", "", "The ECU had to be reset too often!", Ergebnis.FT_NIO ) );
										log( debug, "Error", "executing EDIABAS, sgbd: " + sgbd + ", sgbdJob: " + sgbdJob + ", sgbdDynArgs: " + sgbdDynArgs + ", sgbdResult: " + sgbdResult + ", sgbdStatus: " + sgbdStatus + ", job status failed!" );
										throw new PPExecutionException( PB.getString( "Diagnosefehler" ) );
									} else {
										resList.add( new Ergebnis( "DiagError", "EDIABAS", sgbd, sgbdJob, sgbdDynArgs, sgbdResult, sgbdStatus, "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO ) );
										log( debug, "Error", "executing EDIABAS, sgbd: " + sgbd + ", sgbdJob: " + sgbdJob + ", sgbdDynArgs: " + sgbdDynArgs + ", sgbdResult: " + sgbdResult + ", sgdbStatus: " + sgbdStatus + ", ApiCallFailedException: " + e.getMessage() );
										throw new PPExecutionException();
									}
								} catch( EdiabasResultNotFoundException e ) {
									// NIO - Ediabas error!
									resList.add( new Ergebnis( "DiagError", "EDIABAS", sgbd, sgbdJob, sgbdDynArgs, sgbdResult, sgbdStatus, "", "", "0", "", "", "", PB.getString( "diagnosefehler" ) + "->" + (e.getMessage() != null ? e.getMessage() : ""), "", Ergebnis.FT_NIO_SYS ) );
									log( debug, "Error", "executing EDIABAS, sgbd: " + sgbd + ", sgbdJob: " + sgbdJob + ", sgbdDynArgs: " + sgbdDynArgs + ", sgbdResult: " + sgbdResult + ", sgdbStatus: " + sgbdStatus + ", EdiabasResultNotFoundException: " + e.getMessage() );
									throw new PPExecutionException();
								}
							}
							if( readOutSuccessful ) {
								try {
									// print captured line and apply median filter
									//log(debug, "Var", "line("+ y +"):"+ lineStr);
									line = applyMeanFilter( line, 0 );
									x1x2 = parseLinePoints( config, line, Math.round( (float) CAMERA_IMAGE_HEIGHT / (float) (2 * pts.length) ), logArea, l );
									// store parsed line points
									pts[l][LOOKUP_X1] = x1x2[0];
									pts[l][LOOKUP_X2] = x1x2[1];
									// draw on GUI
									GUI_drawLookupLine( config, lookupImg, Math.round( pts[l][LOOKUP_Y] ), Math.round( (float) CAMERA_IMAGE_HEIGHT / (float) (2 * pts.length) ), line, Math.round( x1x2[0] ), Math.round( x1x2[1] ), Color.BLUE, Color.RED, Color.WHITE );
									lookupImgLabel.setIcon( new ImageIcon( lookupImg ) );
									progressBar.setValue( progressBar.getValue() + 10 );
								} catch( Exception e ) {
									log( debug, "Error", "An error occurred while processing the captured points." );
									e.printStackTrace();
								}
							}
						}
						resList.add( new Ergebnis( "ECU_RESETS", "SDiagNivi", "ECU Reset Counter", "STEUERGERAETE_RESET", "", "", String.valueOf(ecuResetCounter), "0", "4", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
						step = 2;
						break;

					case 2:
						// organize & check points pairs plausibility
						// -------------------------------------------------
						progressBar.setValue( 60 );
						GUI_log( debug, "Ok", "Checking points for plausibility...", logArea );
						organizePoints( debug, config, pts );
						step = 3;

						// check minimum points captured
						try {
							GUI_drawTargetPoints( targetImg, pts, Color.WHITE );
							printPoints( debug, "Captured points:", pts );
						} catch( Exception e ) {
							log( debug, "Error", "Exception occurred during GUI_drawTargetPoints" );
						}
						int nPairs = numberOfPairs( pts );
						int nMinPairs = new Integer( config.getProperty( "LOOKUP_MIN_POINTS_PAIRS" ) ).intValue();
						GUI_log( debug, "Ok", "Valid pairs of points captured: " + numberOfPairs( pts ) + " (min: " + nMinPairs + " max: " + pts.length + ")", logArea );

						if( nPairs >= nMinPairs ) {
							// IO
							resList.add( new Ergebnis( "StatusCapturedPoints", "SDiagNivi", String.valueOf( count ) + "x", "", "", "CapturedPointsOk", String.valueOf( nPairs ), String.valueOf( nMinPairs ), String.valueOf( pts.length ), "0", "", "", "", "", "", Ergebnis.FT_IO ) );
							GUI_log( debug, "Ok", "Enough pairs of points captured.", logArea );
						} else {
							// NIO - not enough points found!
							tmp = (lang == 1) ? "Zu wenig Punkte gefunden!" : "Not enough points found!";
							status = STATUS_EXECUTION_ERROR;
							resList.add( new Ergebnis( "StatusCapturedPoints", "SDiagNivi", String.valueOf( count ) + "x", "", "", "CapturedPointsNOk", String.valueOf( nPairs ), String.valueOf( nMinPairs ), String.valueOf( pts.length ), "0", "", "", "", tmp, "", Ergebnis.FT_IGNORE ) );
							GUI_log( debug, "Error", "Not enough pairs of points captured!", logArea );
							guiColor = Color.RED;
							guiMsg = tmp;
							step = -1;
						}
						break;

					case 3:
						// compute & draw real lines in the image
						// -------------------------------------------------
						progressBar.setValue( 70 );
						line1 = getLineMath( debug, pts, 1 );
						line2 = getLineMath( debug, pts, 2 );
						resList.add( new Ergebnis( "StatusLine1", "SDiagNivi", String.valueOf( count ) + "x", "", "", "Line1", "y1(x) = " + String.valueOf( line1[SLOPE] ) + "x" + (line1[CROSSING] >= 0 ? " + " : " - ") + String.valueOf( Math.abs( line1[CROSSING] ) ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
						resList.add( new Ergebnis( "StatusLine2", "SDiagNivi", String.valueOf( count ) + "x", "", "", "Line2", "y2(x) = " + String.valueOf( line2[SLOPE] ) + "x" + (line2[CROSSING] >= 0 ? " + " : " - ") + String.valueOf( Math.abs( line2[CROSSING] ) ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
						GUI_log( debug, "Var", "Line 1: y1(x) = " + decForm.format( line1[SLOPE] ) + "x" + (line1[CROSSING] >= 0 ? " + " : " - ") + decForm.format( Math.abs( line1[CROSSING] ) ) + " [angle: " + decForm.format( line1[ANGLE] ) + "�]", logArea );
						GUI_log( debug, "Var", "Line 2: y2(x) = " + decForm.format( line2[SLOPE] ) + "x" + (line2[CROSSING] >= 0 ? " + " : " - ") + decForm.format( Math.abs( line2[CROSSING] ) ) + " [angle: " + decForm.format( line2[ANGLE] ) + "�]", logArea );
						GUI_log( debug, "Var", "Lines Plausibility: alpha = " + String.valueOf( rnd( alphaAngleCalc, 4 ) ) + "�, beta = " + String.valueOf( rnd( Math.abs( line1[ANGLE] ) + Math.abs( line2[ANGLE] ), 4 ) ) + "�", logArea );
						GUI_drawTargetLine( targetImg, line1[SLOPE], line1[CROSSING], Color.WHITE, 0 );
						GUI_drawTargetLine( targetImg, line2[SLOPE], line2[CROSSING], Color.WHITE, 0 );
						GUI_log( debug, "Ok", "Lines computed and drawed.", logArea );

						alphaAngleCalc = 180.0 - Math.abs( line1[ANGLE] ) - Math.abs( line2[ANGLE] );
						alphaAngleConfig = Double.parseDouble( config.getProperty( "HEATING_WIRE_ALPHA" ) );
						alphaAngleLimit = Double.parseDouble( config.getProperty( "LOOKUP_PLAUSIB_ANGLE_TOL" ) );

						if( Math.abs( alphaAngleConfig - alphaAngleCalc ) >= alphaAngleLimit ) {
							GUI_log( debug, "Var", "Alpha angle between lines is out of tolerance!", logArea );
							status = STATUS_EXECUTION_ERROR;
							guiColor = Color.RED;
							guiMsg = (lang == 1) ? "Berechnete Geraden sind nicht plausibel!" : "Calculated lines aren't plausible!";
							step = -1;
							break;
						}
						iPointIdeal[X] = Math.round( targetImg.getWidth() / 2 ); // pix
						iPointIdeal[Y] = Math.round( targetImg.getHeight() / 2 - convMm2Pix( distCamTarget, "HEIGHT", verAngleAperture, heightOffset ) ); // pix
						iPointReal = getIntersectionPoint( line1, line2 ); // pix
						GUI_log( debug, "Var", "Intersection points(x,y): real(" + iPointReal[X] + ", " + iPointReal[Y] + "), ideal(" + iPointIdeal[X] + ", " + iPointIdeal[Y] + ")", logArea );
						heightDeviance = (-1.0) * convPix2Mm( distCamTarget, "HEIGHT", verAngleAperture, iPointReal[Y] - iPointIdeal[Y] ); // mm (y[pix] has
						// oposite
						// direction to
						// z[mm])
						widthDeviance = (-1.0) * convPix2Mm( distCamTarget, "WIDTH", horAngleAperture, iPointReal[X] - iPointIdeal[X] ); // mm
						GUI_log( debug, "Var", "Deviances: height = " + decForm.format( heightDeviance ) + " mm, width = " + decForm.format( widthDeviance ) + " mm (real - ideal)", logArea );
						step = 4;
						break;

					case 4:
						// check horizontal camera adjustment (externally made
						// through diagnose job)
						// -------------------------------------------------
						// adjust camera: [- left] [+ right]
						progressBar.setValue( 80 );
						horAngle = -1 * Math.toDegrees( Math.atan( (double) widthDeviance / (double) distCamTarget ) ); // �
						log( debug, "Var", "Horiz. angles:  real = " + realAngle + "�, ideal = 0�, dif = " + horAngle + "�" );
						tmp = Math.abs( rnd( horAngle, 2 ) ) + "� " + (horAngle >= 0 ? "right" : "left") + " (" + horAngleTol + "�)";
						GUI_log( debug, "Var", "Horizontal adjustment: " + tmp, logArea );
						if( Math.abs( horAngle ) <= horAngleTol ) {
							// IO
							if( count == 1 ) // First time to write Horizontal Angle
								// Result
								resList.insertElementAt( new Ergebnis( horAngleResName, "SDiagNivi", String.valueOf( count ) + "x", "", "", horAngleResName, String.valueOf( rnd( horAngle, 2 ) ), String.valueOf( -1 * horAngleTol ), String.valueOf( horAngleTol ), "0", "", "", "", "", tmp, Ergebnis.FT_IO ), 0 );
							else
								// Overwrite previous results
								resList.set( 0, new Ergebnis( horAngleResName, "SDiagNivi", String.valueOf( count ) + "x", "", "", horAngleResName, String.valueOf( rnd( horAngle, 2 ) ), String.valueOf( -1 * horAngleTol ), String.valueOf( horAngleTol ), "0", "", "", "", "", tmp, Ergebnis.FT_IO ) );

							GUI_log( debug, "Ok", "-> No horizontal adjustment needed.", logArea );
							guiColor = Color.GREEN;

							step = 5;
						} else {
							// RETRY - horizontal adjustment needed!
							status = STATUS_EXECUTION_ERROR;

							if( count == 1 )
								resList.insertElementAt( new Ergebnis( horAngleResName, "SDiagNivi", String.valueOf( count ) + "x", "", "", horAngleResName, String.valueOf( rnd( horAngle, 2 ) ), String.valueOf( -1 * horAngleTol ), String.valueOf( horAngleTol ), "0", "", "", "", "HorizontalAdjustmentNeeded", tmp, Ergebnis.FT_IGNORE ), 0 );
							else
								resList.set( 0, new Ergebnis( horAngleResName, "SDiagNivi", String.valueOf( count ) + "x", "", "", horAngleResName, String.valueOf( rnd( horAngle, 2 ) ), String.valueOf( -1 * horAngleTol ), String.valueOf( horAngleTol ), "0", "", "", "", "HorizontalAdjustmentNeeded", tmp, Ergebnis.FT_IGNORE ) );
							GUI_log( debug, "Ok", "-> HORIZONTAL ADJUSTMENT NEEDED!", logArea );
							guiColor = Color.RED;

							guiMsg = (lang == 1) ? "Horizontale Einstellung n�tig!" : "Horizontal adjustment needed!";

							step = -1;
						}
						//
						if( horAngle != 0 )
							GUI_drawArrow( targetImg, Math.round( targetImg.getWidth() / 2 ), Math.round( ARROW_AREA / 2 ), (horAngle >= 0 ? RIGHT : LEFT), guiColor );
						varValuesLabels[VAR_ADJ_HORIZONTAL].setBackground( guiColor );
						varValuesLabels[VAR_ADJ_HORIZONTAL].setText( rnd( horAngle, 2 ) + " �" );

						break;

					case 5:
						// check vertical camera adjustment (externally made through
						// diagnose job)
						// -------------------------------------------------
						// adjust camera: [- down] [+ up]
						progressBar.setValue( 85 );
						idealAngle = Math.toDegrees( Math.atan( (double) heightOffset / (double) distCamTarget ) ); // �
						realAngle = Math.toDegrees( Math.atan( ((double) heightOffset + heightDeviance) / (double) distCamTarget ) ); // �
						verAngle = realAngle - idealAngle; // �
						log( debug, "Var", "Vert. angles:  real = " + realAngle + "�, ideal = " + idealAngle + "�, dif = " + verAngle + "�" );

						if( cameraType == NIVI3 ) {
							verAngleTolUp = Math.abs( verAngleTol ) + 0.5;
						} else {
							verAngleTolUp = Math.abs( verAngleTol );
						}

						verAngleTolDown = Math.abs( verAngleTol ) * -1;

						tmp = Math.abs( rnd( verAngle, 2 ) ) + "� " + (verAngle >= 0 ? "up" : "down") + " (tol_up = " + verAngleTolUp + "�, tol_down = " + verAngleTolDown + "�)";

						GUI_log( debug, "Var", "Vertical adjustment: " + tmp, logArea );
						if( verAngle <= verAngleTolUp && verAngle >= verAngleTolDown ) {
							// IO
							if( count == 1 )
								resList.insertElementAt( new Ergebnis( verAngleResName, "SDiagNivi", String.valueOf( count ) + "x", "HeightOffset = " + heightOffset + "mm", "", verAngleResName, String.valueOf( rnd( verAngle, 2 ) ), String.valueOf( -1 * verAngleTol ), String.valueOf( verAngleTol ), "0", "", "", "", "", tmp, Ergebnis.FT_IO ), 1 );
							else
								resList.set( 1, new Ergebnis( verAngleResName, "SDiagNivi", String.valueOf( count ) + "x", "HeightOffset = " + heightOffset + "mm", "", verAngleResName, String.valueOf( rnd( verAngle, 2 ) ), String.valueOf( -1 * verAngleTol ), String.valueOf( verAngleTol ), "0", "", "", "", "", tmp, Ergebnis.FT_IO ) );

							GUI_log( debug, "Ok", "-> No vertical adjustment needed.", logArea );
							guiColor = Color.GREEN;

							step = 6;
						} else {
							// ABORT - vertical adjustment needed!
							status = STATUS_EXECUTION_ERROR;

							if( count == 1 )
								resList.insertElementAt( new Ergebnis( verAngleResName, "SDiagNivi", String.valueOf( count ) + "x", "HeightOffset = " + heightOffset + "mm", "", verAngleResName, String.valueOf( rnd( verAngle, 2 ) ), String.valueOf( verAngleTolDown ), String.valueOf( verAngleTolUp ), "0", "", "", "", "VerticalAdjustmentNeeded", tmp, Ergebnis.FT_IGNORE ), 1 );
							else
								resList.set( 1, new Ergebnis( verAngleResName, "SDiagNivi", String.valueOf( count ) + "x", "HeightOffset = " + heightOffset + "mm", "", verAngleResName, String.valueOf( rnd( verAngle, 2 ) ), String.valueOf( verAngleTolDown ), String.valueOf( verAngleTolUp ), "0", "", "", "", "VerticalAdjustmentNeeded", tmp, Ergebnis.FT_IGNORE ) );
							GUI_log( debug, "Ok", "-> VERTICAL ADJUSTMENT NEEDED!", logArea );
							guiColor = Color.RED;

							guiMsg = (lang == 1) ? "Vertikale Einstellung n�tig!" : "Vertical adjustment needed!";

							step = -1;
						}
						//
						if( verAngle != 0 )
							GUI_drawArrow( targetImg, Math.round( ARROW_AREA / 2 ), Math.round( targetImg.getHeight() / 2 ) + yOffset, (verAngle >= 0 ? UP : DOWN), guiColor );
						varValuesLabels[VAR_ADJ_VERTICAL].setBackground( guiColor );
						varValuesLabels[VAR_ADJ_VERTICAL].setText( rnd( verAngle, 2 ) + " �" );

						break;

					case 6:
						// check rotational camera adjustment (Manual)
						// -------------------------------------------------
						// adjust camera: [- counterclockwise] [+ clockwise]
						progressBar.setValue( 90 );
						double HEATING_WIRE_ROTATION_OFFSET = new Double( config.getProperty( "HEATING_WIRE_ROTATION_OFFSET" ) ).doubleValue(); // �
						// rotAngle: geht ins APDM [+] Kamera aus Fahrersicht zu
						// weit nach links (Punktekreuz in GUI nach rechts verdreht
						// [-] Kamera aus Fahrersicht zu weit nach rechts
						// (Punktekreuz in GUI nach links verdreht
						rotAngle = (line1[ANGLE] + line2[ANGLE]) / 2.0 + HEATING_WIRE_ROTATION_OFFSET + Math.toDegrees( rotAngCarFront ); // �
						GUI_log( debug, "Var", "Total rotation angle was calculated with line1 " + decForm.format( line1[ANGLE] ) + "�, line2 " + decForm.format( line2[ANGLE] ) + "�, and car's angle of " + decForm.format( Math.toDegrees( rotAngCarFront ) ) + "�.", logArea );
						GUI_log( debug, "Var", "Rotational adjustment: " + decForm.format( Math.abs( rnd( rotAngle, 2 ) ) ) + "� " + (rotAngle >= 0 ? "clockwise" : "counter-clockwise") + " (tol = " + rotAngleTol + "�, offset = " + HEATING_WIRE_ROTATION_OFFSET + "�, ACF = " + angleConvFactor + ")", logArea );
						if( count == 1 )
							resList.insertElementAt( new Ergebnis( "INITIAL_ROTATION_ANGLE", "SDiagNivi", String.valueOf( count ) + "x", "RotationOffset = " + String.valueOf( HEATING_WIRE_ROTATION_OFFSET ) + "�", "", "InitialRotationAngle", String.valueOf( rnd( rotAngle, 2 ) ), String.valueOf( -1 * rotAngleTol ), String.valueOf( rotAngleTol ), "0", "", "", "", "", tmp, Ergebnis.FT_IO ), 3 );
						if( Math.abs( rotAngle ) <= rotAngleTol ) {
							// IO
							status = STATUS_EXECUTION_OK;
							if( count == 1 )
								resList.insertElementAt( new Ergebnis( rotAngleResName, "SDiagNivi", String.valueOf( count ) + "x", "RotationOffset = " + String.valueOf( HEATING_WIRE_ROTATION_OFFSET ) + "�", "", rotAngleResName, String.valueOf( rnd( rotAngle, 2 ) ), String.valueOf( -1 * rotAngleTol ), String.valueOf( rotAngleTol ), "0", "", "", "", "", tmp, Ergebnis.FT_IO ), 2 );
							else
								resList.set( 2, new Ergebnis( rotAngleResName, "SDiagNivi", String.valueOf( count ) + "x", "RotationOffset = " + String.valueOf( HEATING_WIRE_ROTATION_OFFSET ) + "�", "", rotAngleResName, String.valueOf( rnd( rotAngle, 2 ) ), String.valueOf( -1 * rotAngleTol ), String.valueOf( rotAngleTol ), "0", "", "", "", "", tmp, Ergebnis.FT_IO ) );

							GUI_log( debug, "Ok", "-> No rotational adjustment needed.", logArea );
							guiColor = Color.GREEN;

							guiMsg = (lang == 1) ? "Drehen der Kamera nicht n�tig!" : "No rotational adjustment needed.";
						} else {
							// RETRY - rotational adjustment needed!
							status = STATUS_EXECUTION_ERROR;

							if( cameraType == NIVI3 || isRollsRoyceGhost) {
								tmp = (lang == 1) ? "Rotationswinkel ist zu gro�!" : "Rotational angle is too big!";
							} else {
								double hRotationTotal = rotAngle * angleConvFactor; // �bersetzungsverh�ltnis
								// 1:1000
								// f�r
								// NIVI
								// 1
								// [z.B.
								// 0,78�
								// entspr.
								// 2,17
								// Umdrehungen](0,78*1000/360)
								// vollst�ndige Umdrehungen, int-cast zum
								// Abschneiden der ".0"
								int hRotationRev = (int) Math.floor( Math.abs( hRotationTotal / 360 ) );
								// unvollst�ndige Umdrehungen, int-cast zum
								// Abschneiden der ".0", gerundet auf
								// Zehner-Schritte
								int hRotationRest = (int) Math.round( Math.abs( hRotationTotal % 360 ) / 10 );
								hRotationRest *= 10;
								if( hRotationRest == 360 ) {
									hRotationRev++;
									hRotationRest = 0;
								}
								tmp = (lang == 1 ? "" : "Turn ") + hRotationRev + " " + (lang == 1 ? "Umdr. und " : "rev. and ") + " " + hRotationRest + "� " + (lang == 1 ? "nach" : "to the") + " " + ((hRotationTotal/* rotAngle */> 0) ? (lang == 1 ? "rechts " : "right!") : (lang == 1 ? "links " : "left!")) + (lang == 1 ? "drehen!" : "");

								if( count == 0 )
									resList.insertElementAt( new Ergebnis( rotAngleResName, "SDiagNivi", String.valueOf( count ) + "x", "RotationOffset = " + String.valueOf( HEATING_WIRE_ROTATION_OFFSET ) + "�", "", rotAngleResName, String.valueOf( rnd( rotAngle, 2 ) ), String.valueOf( -1 * rotAngleTol ), String.valueOf( rotAngleTol ), "0", "", "", "", "RotationalAdjustmentNeeded", tmp, Ergebnis.FT_IGNORE ), 2 );
								else
									resList.set( 2, new Ergebnis( rotAngleResName, "SDiagNivi", String.valueOf( count ) + "x", "RotationOffset = " + String.valueOf( HEATING_WIRE_ROTATION_OFFSET ) + "�", "", rotAngleResName, String.valueOf( rnd( rotAngle, 2 ) ), String.valueOf( -1 * rotAngleTol ), String.valueOf( rotAngleTol ), "0", "", "", "", "", tmp, Ergebnis.FT_IO ) );
								GUI_log( debug, "Error", "-> ROTATIONAL ADJUSTMENT NEEDED! [" + tmp + "]", logArea );
							}
							guiColor = Color.RED;
							guiMsg = tmp;
						}
						// draw target lines
						GUI_drawTargetLine( targetImg, line1[SLOPE], line1[CROSSING], guiColor, 0 );
						GUI_drawTargetLine( targetImg, line2[SLOPE], line2[CROSSING], guiColor, 0 );
						GUI_drawTargetPoints( targetImg, pts, Color.WHITE );
						//
						varValuesLabels[VAR_ADJ_ROTATIONAL].setBackground( guiColor );
						varValuesLabels[VAR_ADJ_ROTATIONAL].setText( rnd( rotAngle, 2 ) + " �" );

						step = -1;
						break;

					default:
						// show results and end dialog
						// -------------------------------------------------
						// update GUI...
						progressBar.setValue( 100 );
						progressBar.setForeground( guiColor );
						progressBar.setString( guiMsg );
						targetImgLabel.setIcon( new ImageIcon( targetImg ) );
						Toolkit.getDefaultToolkit().beep();
						GUI_log( debug, "Var", "Duration: " + String.valueOf( rnd( (System.currentTimeMillis() - startTime) / 1000.0, 2 ) ) + " sec.", logArea );
						varValuesLabels[VAR_TEST_NUMBER].setText( String.valueOf( count ) );
						varValuesLabels[VAR_TEST_DURATION].setText( String.valueOf( rnd( (System.currentTimeMillis() - startTime) / 1000.0, 2 ) ) + " s" );
						// report
						if( createReport.equals( "ALWAYS" ) || (createReport.equals( "ON_ERROR" ) && status == STATUS_EXECUTION_ERROR) ) {
							appSleep( 100 );
							tmp = createReport( debug, workDir, maxReportsSize, auftrag, status );
							if( tmp != null ) {
								GUI_log( debug, "Ok", "Created report: " + tmp, logArea );
							}
						}
						// show dlg
						long waitTime = waitTimeEnd; // ms
						if( status == STATUS_EXECUTION_ERROR ) {
							waitTime = -1;
							textPanel.setVisible( true );
							textLabel.setText( lang == 1 ? "Wiederholen oder abbrechen?" : "Repeat or cancel?" );
						}
						if( simImgsDir == null ) {
							//
							// REAL MODE...
							//
							if( fasButtonRepeat != null && fasButtonCancel != null ) {
								// test stand buttons
								dlgResult = testStandDialog( fas, debug, false, null, null, waitTime, -1, fasButtonCancel, fasButtonRepeat );
							} else {
								appSleep( waitTimeEnd );
								if( status == STATUS_EXECUTION_OK ) {
									// ok
									dlgResult = NO_BUTTON;
								} else {
									// dlg options without test stand buttons
									dlgResult = GUI_showOptsDlg( frame, lang == 1 ? "NIVI-Justage" : "NIVI-Adjustment", lang == 1 ? "Wiederholen" : "Repeat", lang == 1 ? "Abbrechen" : "Cancel" );
								}
							}
						} else {
							//
							// SIMULATION MODE...
							//
							appSleep( waitTimeEnd );
							simImgFile = GUI_showImgsDlg( debug, frame, simImgsDir ); // simulation
							// dlg
							if( simImgFile != null ) {
								dlgResult = REPEAT_BUTTON;
							} else {
								dlgResult = CANCEL_BUTTON;
							}
						}
						log( debug, "Var", "dlgResult: " + dlgResult + " (waitTime: " + waitTime + " sec.)" );
						step = 0;
						break;
				}// end switch
					// refresh img
					// -------------------------------------------------
				targetImgLabel.setIcon( new ImageIcon( targetImg ) );
				appSleep( 50 ); // sleep 50ms
			}// end while
				// -------------------------------------------------------------------------------------------

			// shift results into logical order (with regard to repitition)
			for( int i = 0; i <= 3; i++ ) {
				resList.add( resList.remove( 0 ) );
			}

			if( status == STATUS_EXECUTION_OK ) {
				resList.add( new Ergebnis( "StatusNiviAdjustment", "SDiagNivi", String.valueOf( count ) + "x", "", "", "NiviAdjustmentOk", "OK", "OK", "OK", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
			} else {
				resList.add( new Ergebnis( "StatusNiviAdjustment", "SDiagNivi", String.valueOf( count ) + "x", "", "", "NiviAdjustmentNOk", "NOK", "OK", "OK", "0", "", "", "", (lang == 1 ? "BenutzerAbbruch" : "UserCancel"), "", Ergebnis.FT_NIO ) );
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
			log( debug, "Error", "executing procedure, PPExecutionException: " + e.getMessage() );
			e.printStackTrace();
		} catch( Exception e ) {
			status = STATUS_EXECUTION_ERROR;
			resList.add( new Ergebnis( "ExecError", "SDiagNivi", "", "", "", "", "", "", "", "0", "", "", "", "Exception: " + (e.getMessage() != null ? e.getMessage() : ""), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS ) );
			log( debug, "Error", "executing procedure, Exception: " + e.getMessage() );
		} catch( Throwable e ) {
			status = STATUS_EXECUTION_ERROR;
			resList.add( new Ergebnis( "ExecError", "SDiagNivi", "", "", "", "", "", "", "", "0", "", "", "", "Throwable: " + (e.getMessage() != null ? e.getMessage() : ""), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS ) );
			log( debug, "Error", "executing procedure, Throwable: " + e.getMessage() );
		}

		// -------------------------------------------------------------------------------------------
		// close GUI
		// -------------------------------------------------------------------------------------------
		if( frame != null ) {
			frame.dispose();
			log( debug, "Ok", "closed GUI." );
		}
		// -------------------------------------------------------------------------------------------
		// release FAS device
		// -------------------------------------------------------------------------------------------
		if( simImgsDir == null && fasButtonRepeat != null && fasButtonCancel != null ) {
			try {
				this.getPr�flingLaufzeitUmgebung().getDeviceManager().releaseFasControl( fasTag );
				fas = null; // evtl. notwendig
				log( debug, "Ok", "released FasControl device (tag: " + fasTag + ")." );
			} catch( Exception e ) {
				log( debug, "Error", "releasing FasControl device (tag: " + fasTag + "), Exception: " + e.getMessage() );
			}
		}

		// -------------------------------------------------------------------------------------------
		// log APDM results
		// -------------------------------------------------------------------------------------------
//		for( int i = 0; i < resList.size(); i++ ) {
//			log( debug, "APDMResult" + i, resList.get( i ).toString() );
//		}
		// -------------------------------------------------------------------------------------------
		// set results
		// -------------------------------------------------------------------------------------------
		setPPStatus( info, status, resList );
		log( debug, "Var", "Execution status: " + (status == STATUS_EXECUTION_OK ? "Ok" : "Error") );
	}

	// ////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// GUI
	//
	// ////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * GUI_log
	 * 
	 * @param debug
	 * @param type
	 * @param text
	 * @param logArea
	 */
	private void GUI_log( boolean debug, String type, String text, JTextArea logArea ) {
		if( logArea != null ) {
			logArea.append( " " + text + "\n" );
			logArea.setCaretPosition( logArea.getDocument().getLength() );
		}
		log( debug, type, text );
	}

	/**
	 * GUI_showImgsDlg
	 * 
	 * @param debug
	 * @param frame
	 * @param simImgDirPath
	 * @return selected img file path or null
	 */
	private String GUI_showImgsDlg( boolean debug, JFrame frame, String simImgDirPath ) throws Exception {
		String filename = null;
		File simImgDir = new File( simImgDirPath );
		String[] imgsList = simImgDir.list();
		if( imgsList == null || imgsList.length == 0 ) {
			throw new FileNotFoundException( "Specified simulation images directory empty or not found!" );
		}
		filename = (String) JOptionPane.showInputDialog( frame, "Select the simulation image file to use...", "Image file selection", JOptionPane.QUESTION_MESSAGE, null, imgsList, imgsList[0] );
		if( filename == null ) {
			throw new Exception( "User canceled dialog!" );
		}
		return simImgDir + "/" + filename;
	}

	/**
	 * GUI_showOptsDlg
	 * 
	 * @return global int codes REPEAT or CANCEL
	 */
	private int GUI_showOptsDlg( JFrame frame, String txt, String repeatTxt, String cancelTxt ) {
		Object[] options = { repeatTxt, cancelTxt };
		int ret = JOptionPane.showOptionDialog( frame, "", txt, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, // don't
				// use
				// a
				// custom
				// Icon
				options, // the titles of buttons
				options[0] ); // default button title
		return (ret == JOptionPane.YES_OPTION ? REPEAT_BUTTON : CANCEL_BUTTON);
	}

	/**
	 * <b>GUI_fillImage</b> <br>
	 * Fills the given image with the specified color <br>
	 * 
	 * @param image
	 * @param color
	 */
	private void GUI_fillImage( BufferedImage img, Color color ) {
		for( int y = 0; y < img.getHeight(); y++ ) {
			for( int x = 0; x < img.getWidth(); x++ ) {
				img.setRGB( x, y, color.getRGB() );
			}
		}
	}

	/**
	 * <b>GUI_drawTargetPoints</b> <br>
	 * Draws a 3x3 block around the given points in the given image <br>
	 * 
	 * @param image
	 * @param pts
	 * @param color
	 */
	private void GUI_drawTargetPoints( BufferedImage img, float[][] pts, Color color ) {
		int y = NONE;
		int x1 = NONE;
		int x2 = NONE;

		for( int p = 0; p < pts.length; p++ ) {
			y = Math.round( pts[p][LOOKUP_Y] );
			x1 = Math.round( pts[p][LOOKUP_X1] );
			x2 = Math.round( pts[p][LOOKUP_X2] );
			// draw x1 pts
			if( x1 != NONE && x1 > 0 && x1 < img.getWidth() && y > 0 && y < img.getHeight() ) {
				for( int q = 0; q < 3; q++ ) {
					for( int r = 0; r < 3; r++ ) {
						img.setRGB( x1 - 1 + q, y - 1 + r, color.getRGB() );
					}
				}
			}
			// draw x2 pts
			if( x2 != NONE && x2 > 0 && x2 < img.getWidth() && y > 0 && y < img.getHeight() ) {
				// draw 3x3 block
				for( int q = 0; q < 3; q++ ) {
					for( int r = 0; r < 3; r++ ) {
						img.setRGB( x2 - 1 + q, y - 1 + r, color.getRGB() );
					}
				}
			}
		}
	}

	/**
	 * <b>GUI_drawTargetLine</b> <br>
	 * Draws the given line in the given image <br>
	 * 
	 * @param image
	 * @param lineSlope
	 * @param lineCrossing
	 * @param color
	 * @param dotSpaces
	 */
	private void GUI_drawTargetLine( BufferedImage img, double lineSlope, double lineCrossing, Color color, int dotSpaces ) {
		//
		// Pure vertical line (lineCrossing in x axis)
		//
		if( lineSlope == Double.NEGATIVE_INFINITY || lineSlope == Double.POSITIVE_INFINITY ) {
			int x = (int) Math.round( lineCrossing );
			for( int y = 0; y < img.getHeight(); y++ ) {
				img.setRGB( x, y, color.getRGB() );
				y += dotSpaces;
			}
			//
			// Diagonal/Horizontal line (lineCrossing in y axis)
			//
		} else {
			int y = 0;
			for( int x = 0; x < img.getWidth(); x++ ) {
				// y = mx + b
				y = (int) Math.round( lineSlope * (double) x + lineCrossing );
				if( y >= 0 && y < img.getHeight() ) {
					img.setRGB( x, y, color.getRGB() );
				}
				x += dotSpaces;
			}
		}
	}

	/**
	 * <b>GUI_drawLookupLine</b> <br>
	 * Draws the given captured line data in the given lookup image <br>
	 * 
	 * @param config
	 *            Configuration file
	 * @param img
	 *            Image that is shown on the screen (upper left has coordinates
	 *            0,0)
	 * @param y
	 *            Line to be drawn is y pixels from top of image
	 * @param yrange
	 *            Maximum height of each drawn line
	 * @param line
	 *            Array that contains the lightness values from the ECU
	 * @param x1
	 *            Peak 1
	 * @param x2
	 *            Peak 2
	 * @param coldColor
	 *            Color for points above threshold
	 * @param warmColor
	 *            Color for points below threshold
	 * @param pointColor
	 *            Color for detected Peaks (refers to x1 and x2)
	 */
	private void GUI_drawLookupLine( Properties config, BufferedImage img, int y, int yrange, int[] line, int x1, int x2, Color coldColor, Color warmColor, Color pointColor ) {
		int HEATING_WIRE_THRESHOLD = new Integer( config.getProperty( "HEATING_WIRE_THRESHOLD" ) ).intValue();

		String hwtMode = new String( config.getProperty( "HEATING_WIRE_THRESHOLD_MODE" ) );
		int[] result = { 0, 0 };

		// int yNorm = 0;
		Color color = Color.BLACK;

		if( hwtMode.equalsIgnoreCase( "AUTO" ) ) {
			result = defineThresholdValue( HEATING_WIRE_THRESHOLD, 0, line, yrange );
			HEATING_WIRE_THRESHOLD = result[0];
		}

		// cycle through every point on x-axis and define its color
		for( int x = 0; x < img.getWidth(); x++ ) {
			color = (line[x] >= HEATING_WIRE_THRESHOLD) ? warmColor : coldColor;
			if( x == x1 || x == x2 ) {
				color = pointColor;
			}
			// start painting topmost pixel and continue to bottom line
			for( int yNorm = Math.round( y - (((float) (line[x] * yrange)) / 256f) ); yNorm <= y; yNorm++ ) {
				if( yNorm >= 0 && yNorm < img.getHeight() )
					img.setRGB( x, yNorm, color.getRGB() );
			}
		}
	}

	/**
	 * <b>GUI_showConfig</b> <br>
	 * Shows the given configuration properties <br>
	 * 
	 * @param config
	 * @param configArea
	 */
	private void GUI_showConfig( Properties config, JTextArea configArea ) {
		String[] keys = (String[]) config.keySet().toArray( new String[0] );
		Arrays.sort( keys );
		configArea.setText( "" );
		for( int i = 0; i < keys.length; i++ ) {
			configArea.append( "\n " + keys[i] + " = " + config.getProperty( keys[i] ) );
			configArea.setCaretPosition( configArea.getDocument().getLength() );
		}
	}

	/**
	 * <b>GUI_drawTargetLine</b> <br>
	 * Draws the given line in the given image <br>
	 * 
	 * @param image
	 * @param lineSlope
	 * @param lineCrossing
	 * @param color
	 * @param dotSpaces
	 */
	private void GUI_drawArrow( BufferedImage img, int xAnchorPx, int yAnchorPx, int orientation, Color color ) {
		int halfArrowArea = Math.round( ARROW_AREA / 2 );
		int x = 0;
		int y = 0;

		// write arrow
		for( int f = 0; f < ARROW_AREA; f++ ) {
			for( int b = -halfArrowArea + f; b < halfArrowArea - f; b++ ) {
				switch( orientation ) {
					case DOWN:
						x = xAnchorPx + b;
						y = yAnchorPx + f;
						break;
					case UP:
						x = xAnchorPx + b;
						y = yAnchorPx - f;
						break;
					case LEFT:
						x = xAnchorPx - f;
						y = yAnchorPx + b;
						break;
					case RIGHT:
						x = xAnchorPx + f;
						y = yAnchorPx + b;
						break;
				}
				if( x >= 0 && x < img.getWidth() && y >= 0 && y < img.getHeight() ) {
					img.setRGB( x, y, color.getRGB() );
				}
			}
		}

	}

	/**
	 * <b>parseLinePoints</b> <br>
	 * Returns one pair of the required type of parsed points from the given
	 * line. <br>
	 * 
	 * @param line
	 *            - captured line
	 * @return points x1x2 of the specified points type
	 */
	private float[] parseLinePoints( Properties config, int[] line, float yrange, JTextArea logArea, int lineNumber ) {
		String LOOKUP_POINTS_TYPE = "MIDDLE"; // PEAK / MIDDLE / LEFT / RIGHT
		// //config.getProperty("LOOKUP_POINTS_TYPE");

		boolean debug = true;

		int HEATING_WIRE_THRESHOLD = new Integer( config.getProperty( "HEATING_WIRE_THRESHOLD" ) ).intValue();
		int HEATING_WIRE_MIN_PIX_WIDTH = new Integer( config.getProperty( "HEATING_WIRE_MIN_PIX_WIDTH" ) ).intValue();
		int nHeated = 0;
		int nPeak = 0;

		int xMaxIndex1 = 0;
		int xMaxValue1 = 0;
		int xMaxIndex2 = 0;
		int xMaxValue2 = 0;

		int xAbsMax_1 = 0;
		int xAbsMax_2 = 0;
		int i = 0;
		int x = 0;
		int[] result = { 0, 0 };

		String hwtMode = new String( config.getProperty( "HEATING_WIRE_THRESHOLD_MODE" ) );

		float[] x1x2Peak = { NONE, NONE };
		float[] x1x2Left = { NONE, NONE };
		float[] x1x2Middle = { NONE, NONE };
		float[] x1x2Right = { NONE, NONE };

		float[] xPeaks = { NONE, NONE, NONE, NONE, NONE };
		float[] xPkValues = { NONE, NONE, NONE, NONE, NONE };
		float[] xLefts = { NONE, NONE, NONE, NONE, NONE };
		float[] xMiddles = { NONE, NONE, NONE, NONE, NONE };
		float[] xRights = { NONE, NONE, NONE, NONE, NONE };

		// define best threshold value if configured
		if( hwtMode.equalsIgnoreCase( "AUTO" ) ) {
			result = defineThresholdValue( HEATING_WIRE_THRESHOLD, HEATING_WIRE_MIN_PIX_WIDTH, line, yrange );
			HEATING_WIRE_THRESHOLD = result[0];
			HEATING_WIRE_MIN_PIX_WIDTH = result[1];
			 GUI_log(true, "Info", "[Line " + (lineNumber + 1) + "] Threshold: " + HEATING_WIRE_THRESHOLD +
					 ", Width: " + HEATING_WIRE_MIN_PIX_WIDTH, logArea);
		}

		for( x = 1; x < line.length; x++ ) {

			// state transition COLD -> HOT
			// ----------------------------
			if( line[x - 1] < HEATING_WIRE_THRESHOLD && line[x] >= HEATING_WIRE_THRESHOLD ) {
				nHeated++;
				xMaxIndex1 = x;
				xMaxValue1 = line[x];
				xMaxIndex2 = xMaxIndex1;
				xMaxValue2 = xMaxValue1;

				// state remains in HOT
				// ----------------------------
			} else if( line[x] >= HEATING_WIRE_THRESHOLD ) {
				nHeated++;
				if( line[x] > xMaxValue1 ) {
					xMaxIndex1 = x;
					xMaxValue1 = line[x];
					xMaxIndex2 = xMaxIndex1;
					xMaxValue2 = xMaxValue1;
				} else if( line[x] == xMaxValue1 ) {
					xMaxIndex2 = x;
					xMaxValue2 = line[x];
				}

				// state transition HOT -> COLD
				// ----------------------------
			} else if( line[x - 1] >= HEATING_WIRE_THRESHOLD && line[x] < HEATING_WIRE_THRESHOLD ) {
				// check min width
				if( nHeated >= HEATING_WIRE_MIN_PIX_WIDTH ) {
					xPeaks[nPeak] = (xMaxIndex1 + xMaxIndex2) / 2f;
					xPkValues[nPeak] = (xMaxValue1 + xMaxValue2) / 2f;
					xLefts[nPeak] = x - nHeated;
					xRights[nPeak] = x - 1;
					xMiddles[nPeak] = (xLefts[nPeak] + xRights[nPeak]) / 2f;

					// detect up to five peaks
					if( nPeak < 4 ) {
						// peak detected -> continue
						nPeak++;
					} else {
						// fifth peak detected -> exit
						break;
					}
					xMaxIndex1 = 0;
					xMaxValue1 = 0;
					xMaxIndex2 = xMaxIndex1;
					xMaxValue2 = xMaxValue1;
				}
				nHeated = 0;
			}
		}

		// if more than two peaks have been detected
		if( xPeaks[2] != NONE ) {
			// get first Peak with highest value
			for( i = 1; i < 5; i++ ) {
				if( xPkValues[i] >= xPkValues[xAbsMax_1] )
					xAbsMax_1 = i;
			}
			x1x2Peak[0] = xAbsMax_1;
			x1x2Left[0] = xLefts[xAbsMax_1];
			x1x2Right[0] = xRights[xAbsMax_1];
			x1x2Middle[0] = xMiddles[xAbsMax_1];
			log( debug, "Info", "x1x2Peak[0] = " + x1x2Peak[0] );
			// delete previously saved peak from array
			xPkValues[xAbsMax_1] = NONE;

			// get second Peak with highest value
			for( i = 1; i < 5; i++ ) {
				if( xPkValues[i] >= xPkValues[xAbsMax_2] )
					xAbsMax_2 = i;
			}
			x1x2Peak[1] = xAbsMax_2;
			x1x2Left[1] = xLefts[xAbsMax_2];
			x1x2Right[1] = xRights[xAbsMax_2];
			x1x2Middle[1] = xMiddles[xAbsMax_2];

		} else {
			x1x2Peak[0] = xPeaks[0];
			x1x2Left[0] = xLefts[0];
			x1x2Right[0] = xRights[0];
			x1x2Middle[0] = xMiddles[0];

			x1x2Peak[1] = xPeaks[1];
			x1x2Left[1] = xLefts[1];
			x1x2Right[1] = xRights[1];
			x1x2Middle[1] = xMiddles[1];
		}

		// return the desired points
		if( LOOKUP_POINTS_TYPE.equalsIgnoreCase( "PEAK" ) ) {
			return x1x2Peak;
		} else if( LOOKUP_POINTS_TYPE.equalsIgnoreCase( "LEFT" ) ) {
			return x1x2Left;
		} else if( LOOKUP_POINTS_TYPE.equalsIgnoreCase( "RIGHT" ) ) {
			return x1x2Right;
		} else {// default: MIDDLE
			return x1x2Middle;
		}
	}

	/**
	 * <b>defineThresholdValue</b> <br>
	 * Defines the best threshold value to detect the 'hot spots'. <br>
	 * 
	 * @param config
	 * @param line
	 * @param yrange
	 */
	private int[] defineThresholdValue( int hwThres, int hwMpw, int[] line, float yrange ) {
		try {
			float mean = 0f;
			float xThrMaxVal = 0f;

			xThrMaxVal = line[0];
			for( int x = 0; x < line.length; x++ ) {
				if( line[x] > xThrMaxVal )
					xThrMaxVal = line[x];
				mean += line[x];
			}
			mean /= line.length;

			// The threshold is defined as the average lightness of the line
			// plus 40% of the difference between the lines
			// average and the lightest pixel in the line.
			hwThres = (int) Math.round( mean + ((xThrMaxVal - mean) * 0.4) );
			// The width is defined as the width of a cut through a triangle in
			// the height of the standardized
			// threshold. The triangle is equal sided, as high as 'yrange' and
			// has and the upper inner angle is 70�
			hwMpw = (int) Math.round( ((yrange * (1 - ((float) hwThres / 256))) * 2 * Math.tan( Math.toRadians( 35 ) )) );

		} catch( Exception e ) {
			e.printStackTrace();
			log( true, "error", "An error occured when defining the best threshold value." );
		}

		return new int[] { hwThres, hwMpw };
	}

	/**
	 * <b>organizePoints</b> <br>
	 * Organizes the given points based on the plausibility test between each
	 * pair. Erases all non-valid points' pairs (pairs containing at least one
	 * NONE value). <br>
	 * 
	 * @param debug
	 * @param config
	 * @param pts
	 */
	private void organizePoints( boolean debug, Properties config, float[][] pts ) {
		int a = -1, b = -1, id = -1, erased = 0;

		for( int l = 0; l < pts.length; l++ ) {
			// valid pair of points
			if( pts[l][LOOKUP_X1] != NONE && pts[l][LOOKUP_X2] != NONE ) {
				if( a == -1 ) {
					// save pair A
					a = l; // Letter 'l' not Digit '1'
					id = a;
				} else if( b == -1 ) {
					// save pair B
					b = l; // Letter 'l' not Digit '1'
					try {
						// org pairs A & B
						erased = organizePairsAB( debug, config, pts, a, b, id );
						} catch( Exception e ) {
						log( debug, "Error", "Exception occurred during organizePairsAB: " + e.getMessage() );
					}
					
					if (erased >= 0)  {
						// pair might have been erased by organizePairsAB()
						a = b + erased; // skip pair b in next loop if it was erased
						l += erased; // increase index of loop counter if b was erased
					} else {
						// pair a has been erased
						a = b;
						id++;
					}

					b = -1;
				}
			// invalid pair of points
			} else {
				// erase
				pts[l][LOOKUP_X1] = NONE;
				pts[l][LOOKUP_X2] = NONE;
			}
		}
	}

	/**
	 * <b>organizePairsAB</b> <br>
	 * Organizes the given A and B points' pairs based on the plausibility test
	 * between them. If the A points are not plausible to the B points the line
	 * after B is checked. If no plausibility is given, the checked point or its
	 * neighbors are erased. <br>
	 * 
	 * @param debug
	 * @param pts
	 */
	private int organizePairsAB( boolean debug, Properties config, float[][] pts, int a, int b, int idFirst ) {
		boolean[] plausibDirCross = { false, false };
		int erased = 0; // tell if a pair has been erased by this function: -1 erased a, 1 erased b, 0 erased nothing
		float tmp = 0;
		
		// check plausibility on current and next/next but one points
		for( int i = 0; i < 2; i++ ) {
			
			// check if more than two lines have been skipped
			if( (b + i - a) > 3) {
				// distance to next possibly plausible point is too big
				if( a == idFirst ) {
					pts[a][LOOKUP_X1] = NONE; // erase a
					pts[a][LOOKUP_X2] = NONE;
					erased = -1;	
				} else {
					pts[b][LOOKUP_X1] = NONE; // erase b
					pts[b][LOOKUP_X2] = NONE;
					pts[b + 1][LOOKUP_X1] = NONE; // erase c
					pts[b + 1][LOOKUP_X2] = NONE;
				}
				break;
			}
			// check neighbor points
			plausibDirCross = checkPointPlausibility( debug, config, pts, a, b + i );
			if( plausibDirCross[0] ) {
				// direct (||) plausibility given
				if( i == 1 ) {
					pts[b][LOOKUP_X1] = NONE; // points from a and c line are
					// plausible , erase b
					pts[b][LOOKUP_X2] = NONE; // points from a and c line are
					// plausible , erase b
					erased = 1;
				}
				break;

			} else if( plausibDirCross[1] ) {
				// crossed (><) plausibility given
				if( i == 1 ) {
					pts[b][LOOKUP_X1] = NONE; // points from a and c line are
					// plausible, erase b
					pts[b][LOOKUP_X2] = NONE; // points from a and c line are
					// plausible, erase b
					erased = 1;
				}
				// -> exchange line IDs of pts to keep algorithm valid
				tmp = pts[b + i][LOOKUP_X1];
				pts[b + i][LOOKUP_X1] = pts[b + i][LOOKUP_X2];
				pts[b + i][LOOKUP_X2] = tmp;
				break;

			} else if( (b + i) == (pts.length - 1) ) {
				// reached end of lines
				System.out.println("Reached end of lines");
				pts[b][LOOKUP_X1] = NONE; // erase b
				pts[b][LOOKUP_X2] = NONE;
				erased = 1;
				if( i == 1 ) {
					pts[b + i][LOOKUP_X1] = NONE; // erase c
					pts[b + i][LOOKUP_X2] = NONE;
				}
				break;

			} else if( (pts[b + 1][LOOKUP_X1] == NONE) || (pts[b + 1][LOOKUP_X2] == NONE) ) {
				// c line is not usable
				if( a == idFirst ) {
					pts[a][LOOKUP_X1] = NONE; // erase a
					pts[a][LOOKUP_X2] = NONE;
					erased = -1;	
				} else {
					pts[b][LOOKUP_X1] = NONE; // erase b
					pts[b][LOOKUP_X2] = NONE;
					pts[b + 1][LOOKUP_X1] = NONE; // erase c
					pts[b + 1][LOOKUP_X2] = NONE;
				}
				break;

			} else if( i == 1 ) {
				// no match in b and c lines
				if( a == idFirst ) {
					pts[a][LOOKUP_X1] = NONE; // erase a
					pts[a][LOOKUP_X2] = NONE;
					erased = -1;
				} else {
					pts[b + i][LOOKUP_X1] = NONE; // erase c
					pts[b + i][LOOKUP_X2] = NONE;
				}
			}
		}
		return erased;

	}

	/**
	 * <b>checkPointPlausibility</b> <br>
	 * Checks the plausibility (direct and crossed) of points contained in two
	 * different lines by means of angle checking. <br>
	 * 
	 * @param debug
	 *            - debug output switch
	 * @param config
	 *            - configuration table
	 * @param pts
	 *            - Array of captured points
	 * @param a
	 *            - line index
	 * @param b
	 *            - line index
	 * 
	 * @return Plausibility Check Result
	 */

	private boolean[] checkPointPlausibility( boolean debug, Properties config, float[][] pts, int a, int b ) {
		// config
		double HEATING_WIRE_ALPHA = new Double( config.getProperty( "HEATING_WIRE_ALPHA" ) ).doubleValue(); // �
		double LOOKUP_PLAUSIB_ANGLE_TOL = new Double( config.getProperty( "LOOKUP_PLAUSIB_ANGLE_TOL" ) ).doubleValue(); // �
		// aux
		double theta1 = 0.0;
		double theta2 = 0.0;
		double alphaValue = 0.0;
		double alphaDirectDif = 0.0;
		double alphaCrossedDif = 0.0;
		boolean[] plausibDirCross = { false, false };

		// Organize base points A & B through plausibility
		// ------------------------------------------------------
		log( debug, "Var", "- I -- A=" + pts[a][LOOKUP_Y] + "(" + pts[a][LOOKUP_X1] + "," + pts[a][LOOKUP_X2] + ") / B=" + pts[b][LOOKUP_Y] + "(" + pts[b][LOOKUP_X1] + "," + pts[b][LOOKUP_X2] + ") ---" );

		// Test crossed (><) points plausibility
		theta1 = getAngleBetween( pts[a][LOOKUP_X1], pts[a][LOOKUP_Y], pts[b][LOOKUP_X2], pts[b][LOOKUP_Y] ); // �
		theta2 = getAngleBetween( pts[b][LOOKUP_X1], pts[a][LOOKUP_Y], pts[a][LOOKUP_X2], pts[b][LOOKUP_Y] ); // �
		alphaValue = 180.0 - theta1 - theta2; // �
		alphaCrossedDif = Math.abs( alphaValue - HEATING_WIRE_ALPHA );

		if( (alphaValue >= HEATING_WIRE_ALPHA - LOOKUP_PLAUSIB_ANGLE_TOL) && (alphaValue <= HEATING_WIRE_ALPHA + LOOKUP_PLAUSIB_ANGLE_TOL) ) {
			plausibDirCross[1] = true;
			log( debug, "Ok", "Plausibility crossed ok." );
		}
		// Test direct (||) points plausibility
		theta1 = getAngleBetween( pts[a][LOOKUP_X1], pts[a][LOOKUP_Y], pts[b][LOOKUP_X1], pts[b][LOOKUP_Y] ); // �
		theta2 = getAngleBetween( pts[b][LOOKUP_X2], pts[a][LOOKUP_Y], pts[a][LOOKUP_X2], pts[b][LOOKUP_Y] ); // �
		alphaValue = 180.0 - theta1 - theta2; // �
		alphaDirectDif = Math.abs( Math.abs( alphaValue ) - HEATING_WIRE_ALPHA ); // �

		if( (alphaValue >= HEATING_WIRE_ALPHA - LOOKUP_PLAUSIB_ANGLE_TOL) && (alphaValue <= HEATING_WIRE_ALPHA + LOOKUP_PLAUSIB_ANGLE_TOL) ) {
			plausibDirCross[0] = true;
			log( debug, "Ok", "Plausibility direct ok." );
		}
		// Evaluate crossed(X) and direct(||) plausibilities
		if( plausibDirCross[0] && plausibDirCross[1] ) {
			// both direct and crossed points are plausible...
			if( alphaDirectDif < alphaCrossedDif ) {
				plausibDirCross[1] = false;
			} else {
				plausibDirCross[0] = false;
			}
		}
		return plausibDirCross;
	}

	/**
	 * <b>printPoints</b> <br>
	 * 
	 * @param debug
	 * @param title
	 * @param pts
	 */
	private void printPoints( boolean debug, String title, float[][] pts ) {
		log( debug, "Var", "-> " + title );
		for( int p = 0; p < pts.length; p++ ) {
			log( debug, "Var", "y(x1, x2) = " + pts[p][LOOKUP_Y] + "(" + pts[p][LOOKUP_X1] + "," + pts[p][LOOKUP_X2] + ")" );
		}
		log( debug, "Var", "----------------------" );
	}

	/**
	 * <b>getIntersectionPoint</b> <br>
	 * Returns the intersection point between the given lines <br>
	 * 
	 * @param line0
	 *            - line 0 data
	 * @param line1
	 *            - line 1 data
	 * @return intersection point
	 */
	private int[] getIntersectionPoint( double[] line0, double[] line1 ) {
		int[] iPoint = { 0, 0 }; // x,y
		// intersection point x,y
		iPoint[0] = (int) Math.round( (line1[CROSSING] - line0[CROSSING]) / (line0[SLOPE] - line1[SLOPE]) );
		iPoint[1] = (int) Math.round( line0[SLOPE] * iPoint[0] + line0[CROSSING] );
		return iPoint;
	}

	/**
	 * <b>getSlopeBetween</b> <br>
	 * Returns the slope between point A and point B <br>
	 * 
	 * @param xA
	 * @param yA
	 * @param xB
	 * @param yB
	 * @return slope
	 */
	private double getSlopeBetween( float xA, float yA, float xB, float yB ) {
		return (new Double( yB - yA ).doubleValue() / new Double( xB - xA ).doubleValue());
	}

	/**
	 * <b>getAngleBetween</b> <br>
	 * Returns the angle between point A and point B <br>
	 * 
	 * @param xA
	 * @param yA
	 * @param xB
	 * @param yB
	 * @return angle in degrees
	 */
	private double getAngleBetween( float xA, float yA, float xB, float yB ) {
		return Math.toDegrees( Math.atan( getSlopeBetween( xA, yA, xB, yB ) ) );
	}

	/**
	 * <b>getLineMath</b> <br>
	 * y(x) = mx + b <br>
	 * 
	 * @param debug
	 *            - debug flag
	 * @param pts
	 *            - the complete points array
	 * @param lineId
	 *            - decrease line (1) / increase line (2)
	 * @return line data {SLOPE, CROSSING, ANGLE}
	 */
	private double[] getLineMath( boolean debug, float[][] fpts, int lineId ) {
		double sumX = 0.0, sumY = 0.0;
		double sumXX = 0.0, sumXY = 0.0;
		int nPts = 0;
		double x = 0;
		double y = 0;
		double[] lineData = { 0.0, 0.0, 0.0, 0 };

		for( int i = 0; i < fpts.length; i++ ) {
			x = (double) fpts[i][lineId];
			y = (double) fpts[i][LOOKUP_Y];
			if( x != NONE ) {
				sumX += x; // generating the sum of x-values
				sumY += y; // generating the sum of Y -Werte
				sumXX += x * x; // generating the sum of product x*x
				sumXY += x * y; // generating the sum of product x*y
				nPts++; // number of valid points
			}
		}
		// line slope: m = (n[Exy] - [Ex][Ey]) / (n[Exx] - [Ex][Ex])
		lineData[SLOPE] = (nPts * sumXY - sumX * sumY) / (nPts * sumXX - sumX * sumX);
		// y-axis intersection: b = ([Ey] - m[Ex]) / n
		lineData[CROSSING] = (sumY - lineData[SLOPE] * sumX) / nPts;
		// slope angle: � = atan(m)
		lineData[ANGLE] = Math.toDegrees( Math.atan( lineData[SLOPE] ) );

		return lineData;
	}

	/**
	 * <b>convMm2Pix</b> <br>
	 * Convert mm to pix <br>
	 * 
	 * @param distCamTarget
	 *            - distance from camera to target
	 * @param ref
	 *            - reference direction HEIGHT or WIDTH
	 * @param refAngleAperture
	 *            - reference angle aperture(�) accordingly to the given
	 *            reference direction
	 * @param mm
	 *            - value in mm
	 * @return pix
	 */
	private static int convMm2Pix( double distCamTarget, String ref, double refAngleAperture, double mm ) {
		double refTargetMm = 2 * (Math.tan( Math.toRadians( refAngleAperture / 2 ) ) * distCamTarget); // mm
		double refImgPx = ref.equalsIgnoreCase( "HEIGHT" ) ? CAMERA_IMAGE_HEIGHT : CAMERA_IMAGE_WIDTH; // px

		return (int) Math.round( (refImgPx / refTargetMm) * mm ); // px
	}

	/**
	 * <b>convPix2Mm</b> <br>
	 * Convert pix to mm <br>
	 * 
	 * @param distCamTarget
	 *            - distance from camera to target
	 * @param ref
	 *            - reference HEIGHT or WIDTH
	 * @param refAngleAperture
	 *            - reference angle aperture(�) accordingly to the given
	 *            reference direction
	 * @param pix
	 *            - value in pix
	 * @return mm
	 */
	private static double convPix2Mm( double distCamTarget, String ref, double refAngleAperture, int pix ) {
		double refTargetMm = 2 * (Math.tan( Math.toRadians( refAngleAperture / 2 ) ) * distCamTarget); // mm
		double refImgPx = ref.equalsIgnoreCase( "HEIGHT" ) ? CAMERA_IMAGE_HEIGHT : CAMERA_IMAGE_WIDTH; // px

		return rnd( (refTargetMm / refImgPx) * (double) pix, 2 ); // mm
	}

	/**
	 * <b>rnd</b> <br>
	 * Rounds a double value to n decimal places. <br>
	 * 
	 * @param value
	 *            - the double value to round
	 * @param decPlaces
	 *            - the number of decimal places to use while rounding
	 * @return the rounded double value
	 */
	private static double rnd( double value, int decPlaces ) {
		float tmp = 1;
		for( int i = 1; i <= decPlaces; i++ )
			tmp *= 10;
		return (Math.rint( value * tmp )) / tmp;
	}

	/**
	 * <b>log</b> <br>
	 * Logs a message if the given debug flag is active or if it is an 'Error'
	 * message. <br>
	 * 
	 * @param debug
	 *            - the debug flag
	 * @param type
	 *            - the type of message to display
	 * @param text
	 *            - the message text
	 */
	private void log( boolean debug, String type, String text ) {
		if( type.equalsIgnoreCase( "Error" ) || debug ) {
			System.out.println( "[" + getLocalName() + "] [" + type + "] " + text );
		}
	}

	/**
	 * <b>extractArg</b> <br>
	 * Extract the 'CASCADEconstruct' value if present in the given argument. <br>
	 * 
	 * @param debug
	 *            is the debug flag
	 * @return the arg extracted value string
	 * @throws InformationNotAvailableException
	 *             if a '@contruct' value is no available
	 */
	private String extractArg( String argName ) throws InformationNotAvailableException {
		String argValue = getArg( argName );

		if( argValue == null ) {
			// NULL value
			return null;
		}
		if( argValue.indexOf( "@" ) != -1 ) {
			// @construct value
			argValue = getPPResult( argValue.toUpperCase() );
		}
		return argValue;
	}

	/**
	 * <b>testStandDialog</b> <br>
	 * Shows a dialog with the specified message in the screen and with the
	 * specified Pruefstand buttons to interact with. Can be used with 0, 1 or 2
	 * buttons with or without message dialog. <br>
	 * 
	 * @param fas
	 *            - FasControl device object
	 * @param debug
	 *            - Debug flag
	 * @param simulation
	 *            - Simulation flag for simulation purposes.
	 * @param title
	 *            - Dialog's title. Default is "FAS Dialog".
	 * @param text
	 *            - Dialog's text. If null/"" no dialog will be shown.
	 * @param timeout
	 *            - Timeout for the dialog (ms). if < 0 no timeout will be used.
	 * @param polling
	 *            - Polling time (ms) or -1 to use the default value of 250ms.
	 * @param switchName1
	 *            - Name of the pruefstand button 1 or null/"" for no button.
	 * @param switchName2
	 *            - Name of the pruefstand button 2 or null/"" for no button.
	 * @return 2 : pressed button 2<br>
	 *         1 : pressed button 1<br>
	 *         0 : no button pressed<br>
	 *         -1 : error on button 1<br>
	 *         -2 : error on button 2<br>
	 */
	private int testStandDialog( FasControl fas, boolean debug, boolean simulation, String title, String text, long timeout, long polling, String switchName1, String switchName2 ) throws PPExecutionException {
		long startTime = System.currentTimeMillis();
		long actualTime = System.currentTimeMillis();
		long pollingTime = (polling != -1) ? polling : 250;
		int dllResult = -1;
		UserDialog ud = null;
		boolean displayDlg = (text != null) ? true : false;

		title = (title != null) ? title : "FAS Dialog";

		try {
			// start monitoring the specified buttons
			// b1
			if( switchName1 != null && !switchName1.equalsIgnoreCase( "" ) ) {
				dllResult = (simulation) ? 1 : fas.switchMonitoring( switchName1, 1 );
				if( dllResult != 1 ) {
					log( debug, "Error", "turning on switch " + switchName1 + ", " + getDllErrorMsg( String.valueOf( dllResult ) ) + " (dllResult = " + String.valueOf( dllResult ) + ")" );
					return -1; // Error starting mon1
				}
				dllResult = (simulation) ? 2 : fas.controlSwitchIllumination( switchName1, 2 );
				log( debug, "Ok", "turned on switch " + switchName1 );
				// b2
				if( switchName2 != null && !switchName2.equalsIgnoreCase( "" ) ) {
					dllResult = (simulation) ? 1 : fas.switchMonitoring( switchName2, 1 );
					if( dllResult != 1 ) {
						log( debug, "Error", "turning on switch " + switchName2 + ", " + getDllErrorMsg( String.valueOf( dllResult ) ) + " (dllResult = " + String.valueOf( dllResult ) + ")" );
						return -2; // Error starting mon2
					}
					dllResult = (simulation) ? 1 : fas.controlSwitchIllumination( switchName2, 2 );
					log( debug, "Ok", "turned on switch " + switchName2 );
				}
			}
			// build buttons line
			final int SIZE_BUTTONS_LINE = 50; // 33;
			// //"123456789012345678901234567890123";
			String buttonsLine = "";
			int ins1 = -1;
			int ins2 = -1;
			if( switchName1 != null && !switchName1.equalsIgnoreCase( "" ) ) {
				// define insertion points
				if( switchName2 != null && !switchName2.equalsIgnoreCase( "" ) ) {
					ins1 = Math.round( (SIZE_BUTTONS_LINE / 3) - ((switchName1.length() + 2) / 2) );
					ins2 = Math.round( 2 * (SIZE_BUTTONS_LINE / 3) - ((switchName2.length() + 2) / 2) );
				} else {
					ins1 = Math.round( (SIZE_BUTTONS_LINE / 2) - ((switchName1.length() + 2) / 2) );
				}
				// insert button names centered
				while( buttonsLine.length() < SIZE_BUTTONS_LINE ) {
					if( buttonsLine.length() == ins1 ) {
						buttonsLine = buttonsLine + "[" + switchName1 + "]";
					} else if( buttonsLine.length() == ins2 ) {
						buttonsLine = buttonsLine + "[" + switchName2 + "]";
					} else {
						buttonsLine = buttonsLine + " ";
					}
				}
			}
			if( displayDlg ) {
				text = text + "\n\n" + buttonsLine;
				// get user Dialog
				ud = getPr�flingLaufzeitUmgebung().getUserDialog();
				ud.setAllowCancel( false );
				log( debug, "Ok", "got user dialog." );
			}

			// polling cycle
			int pressed = 0;
			log( debug, "Ok", "starting switches polling..." );
			while( pressed == 0 ) {
				// log(debug, "Var", "cycle "+ String.valueOf(count));
				if( timeout > 0 && (actualTime - startTime) > timeout ) {
					break;
				}
				if( displayDlg ) {
					ud.displayStatusMessage( title, text, -1 );
				}
				// mon buttons
				// b1
				if( switchName1 != null && !switchName1.equalsIgnoreCase( "" ) ) {
					dllResult = (simulation) ? 0 : fas.switchActivated( switchName1 );
					if( dllResult == 1 ) {
						pressed = 1; // Button 1 pressed
						log( debug, "Ok", "pressed switch " + switchName1 );
					} else if( dllResult == 0 ) {
						// b2
						if( switchName2 != null && !switchName2.equalsIgnoreCase( "" ) ) {
							dllResult = (simulation) ? 0 : fas.switchActivated( switchName2 );
							if( dllResult == 1 ) {
								pressed = 2; // Button 2 pressed
								log( debug, "Ok", "pressed switch " + switchName2 );
							} else if( dllResult < 0 ) {
								// NIO
								pressed = -2;
								log( debug, "Error", "checking pressed switch " + switchName2 + ", " + getDllErrorMsg( String.valueOf( dllResult ) ) + " (dllResult = " + String.valueOf( dllResult ) + ")" );
							}
						}
					} else {
						// NIO
						pressed = -1;
						log( debug, "Error", "checking pressed switch " + switchName1 + ", " + getDllErrorMsg( String.valueOf( dllResult ) ) + " (dllResult = " + String.valueOf( dllResult ) + ")" );
					}
				}
				// sleep
				try {
					Thread.sleep( pollingTime );
				} catch( InterruptedException e ) {
					// ignore
				}
				actualTime = System.currentTimeMillis();
			}

			if( displayDlg ) {
				// hide dialog text
				try {
					getPr�flingLaufzeitUmgebung().releaseUserDialog();
					log( debug, "Ok", "released user dialog." );
				} catch( DeviceLockedException e ) {
					log( debug, "Error", "releasing user dialog, Exception: " + e.getMessage() );
				}
			}
			// end monitoring the specified buttons
			// b1
			if( switchName1 != null && !switchName1.equalsIgnoreCase( "" ) ) {
				dllResult = (simulation) ? 0 : fas.switchMonitoring( switchName1, 0 );
				dllResult = (simulation) ? 0 : fas.controlSwitchIllumination( switchName1, 0 );
				log( debug, "Ok", "turned off switch " + switchName1 );
				// b2
				if( switchName2 != null && !switchName2.equalsIgnoreCase( "" ) ) {
					dllResult = (simulation) ? 0 : fas.switchMonitoring( switchName2, 0 );
					dllResult = (simulation) ? 0 : fas.controlSwitchIllumination( switchName2, 0 );
					log( debug, "Ok", "turned off switch " + switchName2 );
				}
			}
			return pressed;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>getDllErrorMsg</b> <br>
	 * Gets the dll error message matching the given dll result code. <br>
	 * 
	 * @param dllJobResult
	 * @return
	 */
	private String getDllErrorMsg( String dllJobResult ) {
		String errorMsg = "DllInvalidResultError";
		dllJobResult = dllJobResult.trim();
		dllJobResult = dllJobResult.toUpperCase();
		boolean isInt = false;
		try {
			new Integer( dllJobResult ).intValue();
			isInt = true;
		} catch( Exception e ) {
			// ignore
		}

		// NIO_NETWORK
		if( dllJobResult.equalsIgnoreCase( "NIO_NETWORK" ) || dllJobResult.equalsIgnoreCase( "-1" ) ) {
			errorMsg = "DllNetworkError";

			// NIO_TARGET_NOT_ACTIVE
		} else if( dllJobResult.equalsIgnoreCase( "NIO_TARGET_NOT_ACTIVE" ) || dllJobResult.equalsIgnoreCase( "-2" ) ) {
			errorMsg = "DllTargetNotActiveError";

			// NIO_MECHANICAL_LIMITATIONS
		} else if( dllJobResult.equalsIgnoreCase( "NIO_MECHANICAL_LIMITATIONS" ) || dllJobResult.equalsIgnoreCase( "-3" ) ) {
			errorMsg = "DllMechanicalLimitsError";

			// NIO_TIMEOUT (temp TIMEOUT in DGF)
		} else if( dllJobResult.startsWith( "NIO_TIMEOUT" ) || dllJobResult.startsWith( "TIMEOUT" ) || dllJobResult.equalsIgnoreCase( "-4" ) ) {
			String sTimeout = "";
			int beginIndex = dllJobResult.lastIndexOf( "_" ) + 1;
			int endIndex = dllJobResult.length();
			if( (beginIndex != -1) && (beginIndex < endIndex) ) {
				sTimeout = dllJobResult.substring( beginIndex, endIndex );
			}
			errorMsg = "DllTimeout" + sTimeout + "Error";

			// NIO_RVC_TARGET (temp RFKZIEL in FIZ)
		} else if( dllJobResult.equalsIgnoreCase( "NIO_RVC_TARGET" ) || dllJobResult.equalsIgnoreCase( "RFKZIEL" ) || dllJobResult.equalsIgnoreCase( "-5" ) ) {
			errorMsg = "DllRvcTargetError";

			// ...UNDEFINED
		} else if( (isInt) || (dllJobResult.startsWith( "NIO" )) ) {
			errorMsg = "DllUndefinedError";

		}
		return errorMsg;
	}

	/**
	 * <b>appSleep</b> <br>
	 * Sleeps for the specified time in milliseconds <br>
	 * 
	 * @param ms
	 *            - sleeping time in milliseconds
	 */
	private void appSleep( long ms ) {
		try {
			Thread.sleep( ms );
		} catch( Exception e ) {
			// ..
		}
	}

	/**
	 * <b>applyMediaFilter</b> <br>
	 * Applies a mean filter to the line points. One neighboring value on each
	 * side is used. <br>
	 * 
	 * @param line
	 *            - array of captured lightness values
	 * @param xOffset
	 *            - horizontal lookup offset
	 * @return the filtered line data
	 */
	private int[] applyMeanFilter( int[] line, int xOffset ) {
		int[] meanLine = new int[CAMERA_IMAGE_WIDTH];
		// keep first and last value unchanged
		meanLine[xOffset] = line[xOffset];
		meanLine[CAMERA_IMAGE_WIDTH - xOffset - 1] = line[CAMERA_IMAGE_WIDTH - xOffset - 1];

		for( int x = xOffset + 1; x < CAMERA_IMAGE_WIDTH - xOffset - 1; x++ ) {
			meanLine[x] = Math.round( (line[x - 1] + line[x] + line[x + 1]) / 3f );
		}
		return meanLine;
	}

	/**
	 * <b>numberOfPairs</b> <br>
	 * Counts the number of pairs of points <br>
	 * 
	 * @param pts
	 * @return number of pairs of points
	 */
	private int numberOfPairs( float[][] pts ) {
		int nValidPairs = 0;
		for( int p = 0; p < pts.length; p++ ) {
			if( pts[p][LOOKUP_X1] != NONE && pts[p][LOOKUP_X2] != NONE ) {
				nValidPairs++;
			}
		}
		return nValidPairs;
	}

	/**
	 * <b>getLookupPoints</b> <br>
	 * Gets the lookup points array <br>
	 * 
	 * @return array with the lookup points {y, x1, x2}
	 */
	private float[][] getLookupPoints( Properties config ) {
		int LOOKUP_LINES = new Integer( config.getProperty( "LOOKUP_LINES" ) ).intValue();
		int lookupGap = Math.round( CAMERA_IMAGE_HEIGHT / LOOKUP_LINES ); // px
		float[][] pts = new float[LOOKUP_LINES][3];
		int y = Math.round( lookupGap / 2 );
		for( int i = 0; i < pts.length; i++ ) {
			pts[i][LOOKUP_Y] = y;
			pts[i][LOOKUP_X1] = NONE;
			pts[i][LOOKUP_X2] = NONE;
			y += lookupGap;
		}
		return pts;
	}

	/**
	 * <b>initConfig</b> <br>
	 * Initializes (create/load file) and gets config <br>
	 * 
	 * @param debug
	 * @param workDir
	 * @return config properties
	 */
	private Properties initConfig( boolean debug, String configFilePath ) throws Exception {
		Properties config = new Properties();
		boolean replace = false;
		final String lineSep = System.getProperty( "line.separator" );

		// header text
		final String HEADER_TEXT = "####################################################" + lineSep + "#### NIVI calibration configuration properties #####" + lineSep + "####################################################" + lineSep + "# " + lineSep + "# HEATING_WIRE_ALPHA:" + lineSep + "# -> upper/lower angle(�) between heating wire line 1 and line 2." + lineSep + "# " + lineSep + "# HEATING_WIRE_BETA:" + lineSep + "# -> left/right angle(�) between heating wire line 1 and line 2." + lineSep + "# " + lineSep + "# HEATING_WIRE_MIN_PIX_WIDTH:" + lineSep + "# -> number of sequencial pixels needed to detect the heating wire." + lineSep + "# " + lineSep + "# HEATING_WIRE_ROTATION_OFFSET:" + lineSep + "# -> heated wire physical rotation angle(�) Use >0 when the wire is rotated clockwise." + lineSep + "# " + lineSep + "# HEATING_WIRE_THRESHOLD:" + lineSep + "# -> heated wire threshold value (0..255)." + lineSep + "# " + lineSep + "# HEATING_WIRE_THRESHOLD_MODE:" + lineSep + "# -> method for threshold definition (MANUAL or AUTO)." + lineSep + "# " + lineSep + "# HEATING_WIRE_HEIGHT:" + lineSep + "# -> heating wire height(mm) in the target board." + lineSep + "# " + lineSep + "# HEATING_WIRE_WIDTH:" + lineSep + "# -> heating wire width(mm) in the target board." + lineSep + "# " + lineSep + "# LOOKUP_LINES:" + lineSep + "# -> number of lines to capture from the camera image. Each line takes around 1 sec to read." + lineSep + "# " + lineSep + "# LOOKUP_MIN_POINTS_PAIRS:" + lineSep + "# -> minimum number of pairs of points needed to calibrate." + lineSep + "# " + lineSep + "# LOOKUP_PLAUSIB_ANGLE_TOL:" + lineSep + "# -> maximum angle(�) tolerance used in the plausibility tests of the pairs of points." + lineSep + "# " + lineSep + "######################################################" + lineSep;

		// default configuration values
		String[][] configVal = { { "HEATING_WIRE_THRESHOLD", "130" }, // 0 px
		// value
		{ "HEATING_WIRE_THRESHOLD_MODE", "MANUAL" }, // 1 MANUAL or AUTO
		{ "HEATING_WIRE_MIN_PIX_WIDTH", "13" }, // 2 number of px
		{ "HEATING_WIRE_ALPHA", "106.26020" }, // 3 �
		{ "HEATING_WIRE_BETA", "73.73980" }, // 4 �
		{ "HEATING_WIRE_ROTATION_OFFSET", "0.0" }, // 5 �
		{ "HEATING_WIRE_HEIGHT", "570" }, // 6 mm
		{ "HEATING_WIRE_WIDTH", "700" }, // 7 mm
		{ "LOOKUP_LINES", "10" }, // 8 number of lines to capture
		{ "LOOKUP_MIN_POINTS_PAIRS", "4" }, // 9 number of pairs of
		// points
		{ "LOOKUP_PLAUSIB_ANGLE_TOL", "3.0" } // 10 �
		};

		// check existence/create config file
		File configFile = new File( configFilePath );
		if( configFile.exists() ) {
			// config.ini exists...
			// -> load config properties from file
			config.load( new FileInputStream( configFile ) );

			for( int i = 0; i < configVal.length; i++ ) {
				if( !config.containsKey( configVal[i][0] ) ) {
					replace = true;
					config.setProperty( configVal[i][0], configVal[i][1] );
				}
			}

			if( replace ) {
				config.store( new FileOutputStream( configFile ), HEADER_TEXT );
				log( debug, "Ok", "Added missing values to existing configuration file." );
			}

		} else {
			// config.ini does not exists...
			// -> create & save default config properties to file
			if( !configFile.createNewFile() ) {
				throw new Exception( "creating config file: " + configFile.getAbsolutePath() );
			}
			for( int i = 0; i < configVal.length; i++ ) {
				config.setProperty( configVal[i][0], configVal[i][1] );
			}
			log( debug, "Ok", "created config file with default config: " + configFile.getAbsolutePath() );
			config.store( new FileOutputStream( configFile ), HEADER_TEXT );
			log( debug, "Ok", "config saved. (using default configuration)" );
		}
		return config;
	}

	/**
	 * <b>createReport</b> <br>
	 * Takes a screenshoot and saves in the file
	 * 'Timestamp_Auftrag_NIVI_Status.png' under the directory
	 * '${workDir}\Reports'. <br>
	 */
	private String createReport( boolean debug, File workDir, long maxReportsSize, String auftrag, int status ) {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		String reportStatus = (status == STATUS_EXECUTION_OK) ? "Ok" : "NOk";
		String reportName = String.valueOf( System.currentTimeMillis() ) + "_" + auftrag + "_NiVi_" + reportStatus + ".png";
		try {
			Dimension screenDim = toolkit.getScreenSize();
			Rectangle screenRect = new Rectangle( screenDim );
			// check/create reports dir
			File reportsDir = new File( workDir.getAbsolutePath() + File.separator + "Reports" );
			if( reportsDir.exists() ) {
				// check dir size
				String[] filesList = reportsDir.list();
				long filesSize = 0; // Bytes
				if( filesList != null ) {
					for( int i = 0; i < filesList.length; i++ ) {
						filesSize += new File( filesList[i] ).length();
					}
					if( filesSize > maxReportsSize ) {
						// delete reports dir
						if( !reportsDir.delete() ) {
							throw new Exception( "unable to delete reports dir: " + reportsDir );
						}
						log( debug, "Ok", "reports dir deleted! (" + filesSize + " > " + maxReportsSize + " Bytes)" );
					}
				}
			} else {
				// make dir
				if( !reportsDir.mkdir() ) {
					throw new Exception( "unable to create reports dir: " + reportsDir );
				}
			}
			// create screenshot & save to PNG file
			File reportFile = new File( reportsDir.getAbsolutePath() + File.separator + reportName );
			ImageIO.write( new Robot().createScreenCapture( screenRect ), "png", reportFile );
			return reportName;

		} catch( Exception e ) {
			log( debug, "Error", "creating report " + reportName + ", Exception: " + e.getMessage() );
			return null;
		}
	}

	/**
	 * <b>getDynSgbdArgs </b> <br>
	 * Replaces 'XX' by the xPart hex value and 'YY' by the y hex value in the
	 * given string. <br>
	 * 
	 * @param sgbdArgs
	 *            - arguments string to change
	 * @param xPart
	 *            - the left half (0 or 1) or the right half (1 or 2) of the line. Will
	 *            replace the 'XX' string.
	 * @param y
	 *            - the line (0..240) id. Will replace the 'YY' string.
	 * @return arguments string with the concret hex values for XX and YY.
	 */
	private String getDynSgbdArgs( String sgbdArgs, int xPart, int y ) {
		String sgbdDynArgs = sgbdArgs;

		// XX replacement
		int xxIndex = sgbdDynArgs.indexOf( "XX" );
		if( xxIndex != -1 ) {
			String xPartHex = Integer.toHexString( xPart );
			if( xPartHex.length() == 1 ) {
				xPartHex = "0" + xPartHex;
			}
			sgbdDynArgs = sgbdDynArgs.replaceAll( "XX", String.valueOf( xPartHex ) );
		}
		// YY replacement
		int yyIndex = sgbdDynArgs.indexOf( "YY" );
		if( yyIndex != -1 ) {
			String yHex = Integer.toHexString( y );
			if( yHex.length() == 1 ) {
				yHex = "0" + yHex;
			}
			sgbdDynArgs = sgbdDynArgs.replaceAll( "YY", String.valueOf( yHex ) );
		}
		return sgbdDynArgs;
	}
}

package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;

/** Implementierung der Pruefprozedur, die beliebige Zeilen persistent in eine Datei im Filesystem schreibt<BR>
 * <BR>
 * Created on 16.03.04<BR>
 * <BR>
 * bei Aenderung Hr. Buboltz TI-430 Tel. 35789 informieren!<BR>
 * <BR>
 * @author BMW TI-430 Buboltz<BR>
 * @version V0_0_1  16.03.2004  TB  Implementierung<BR>
 * @version V2_0    15.11.2005  MN  Wildcard-Unterst�tzung erg�nzt
 * @version V3_0_F  03.06.2008  CS  APDM Ergebnis Format angepasst
 * @version V4_0_T  31.03.2016  MK  APDM Ergebnis Hinzugef�gt falls die zu l�schende Datei nicht vorhanden ist
 * @version V5_0_F  31.03.2016  MK  Freigabe Version 4_0_T
 * @version V6_0_T  01.04.2016  MK  Kleine Anpassung des Ergebnisses der Version 5_0_F
 * @version V7_0_F  01.04.2016  MK  Kleine Anpassung des Ergebnisses der Version 5_0_F
 * @version V8_0_T  12.05.2016  FS  Optionaler ACCESS_TIMEOUT f�r Dateizugriff hinzugef�gt. Default: 10 Sekunden.
 * @version V9_0_T 25.05.2016	MK	Bugfix bez�glich des optionalen Parameters ACCESS_TIMEOUT. 
 * @version V10_0_T 07.06.2016  MK  Default Timeout auf 5 Sekunden angepasst.<BR>
 * @version V11_0_F 07.06.2016  MK  Produktive Freigabe Version V_10_0_T.<BR>
 * @version V12_0_T 17.06.2016  MK  Schnellerer Ablauf wenn keine Wildcards gesetzt (LOP2090).<BR>
 * @version V13_0_F 20.06.2016  MK  Produktive Freigabe Version V_12_0_T.<BR>
 */
public class DateiDelete_13_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	static final long DEFAULT_TIMEOUT = 5;
	private boolean debug = false;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DateiDelete_13_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * @param pruefling Klasse des zugeh. Prueflings
	 * @param pruefprozName Name der Pruefprozedur
	 * @param hasToBeExecuted Ausfuehrbedingung f�r Fehlerfreiheit
	 */
	public DateiDelete_13_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "ACCESS_TIMEOUT" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "DIR", "FILENAME", "EXTENSION" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die parent-Methode aufgrund
	 * der offenen Anzahl an Results
	 */
	public boolean checkArgs() {
		int i, j;
		boolean exist;

		try {
			// 1. Check: Sind alle requiredArgs gesetzt
			String[] requiredArgs = getRequiredArgs();
			for( i = 0; i < requiredArgs.length; i++ ) {
				if( getArg( requiredArgs[i] ) == null )
					return false;
				if( getArg( requiredArgs[i] ).equals( "" ) == true )
					return false;
			}

			// 2. Check: Sind �berfl�ssige Argumente gesetzt, d.h. Argumente, die weder required noch optional sind
			// Ein gesetztes Argument, das mit LINE wird nicht analysiert, es wird aber der maximale Index festgehalten.
			// Die detaillierte Analyse erfolgt im 3-ten Check.
			Enumeration enu = getArgs().keys();
			String givenkey;
			String temp;
			while( enu.hasMoreElements() ) {
				givenkey = (String) enu.nextElement();
				if( (givenkey.startsWith( "DIR" ) == false) && (givenkey.startsWith( "FILENAME" ) == false) && (givenkey.startsWith( "EXTENSION" ) == false) ) {
					exist = false;
					j = 0;
					while( (exist == false) && (j < requiredArgs.length) ) {
						if( givenkey.equals( requiredArgs[j] ) == true )
							exist = true;
						j++;
					}
					j = 0;
					if( exist == false )
						return false;
					if( getArg( givenkey ).equals( "" ) == true )
						return false;
				}
			}

			// Tests bestanden, somit ok
			return true;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * fuehrt die Pruefprozedur aus
	 * @param info Information zur Ausfuehrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		long timeout = DEFAULT_TIMEOUT;
		String filename, extension, dir, dir_temp = null;
		File[] fileList;

		try {
			//Parameter holen
			try {
				//Parameterpruefung
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				//Zwingenden singulaeren Parameter DIR holen
				if( getArg( getRequiredArgs()[0] ).indexOf( '@' ) != -1 )
					dir_temp = getPPResult( getArg( getRequiredArgs()[0] ) ).trim();
				else
					dir_temp = getArg( getRequiredArgs()[0] ).trim();
				// falls ";" im DateiPfad enthalten (z.B. Aus �bergabe aus Dim DirectoryReader f�r Delete File=false, z.B. c:/dim;fals
				StringTokenizer myTokenizer = new StringTokenizer( dir_temp, ";" );
				try {
					dir = myTokenizer.nextToken();
				} catch( NoSuchElementException e ) {
					dir = dir_temp;
				}
				//Zwingenden singulaeren Parameter NAME holen
				if( getArg( getRequiredArgs()[1] ).indexOf( '@' ) != -1 )
					filename = getPPResult( getArg( getRequiredArgs()[1] ) ).trim();
				else
					filename = getArg( getRequiredArgs()[1] ).trim();
				//Zwingenden singulaeren Parameter EXTENSION holen
				if( getArg( getRequiredArgs()[2] ).indexOf( '@' ) != -1 )
					extension = getPPResult( getArg( getRequiredArgs()[2] ) ).trim();
				else
					extension = getArg( getRequiredArgs()[2] ).trim();

				//Optionalen Parameter TIMEOUT "checken"
				if( getArg( getOptionalArgs()[0] ) != null ) {
					timeout = Long.parseLong( getArg( getOptionalArgs()[0] ).trim() );
				}

			} catch( PPExecutionException e ) {
				e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			try {
				//Seperator am Ende kuerzen
				if( dir.endsWith( "\\" ) || (dir.endsWith( "/" )) )
					dir = dir.substring( 0, dir.length() - 1 );
				if( dir.endsWith( "\\\\" ) )
					dir = dir.substring( 0, dir.length() - 2 );

				//Fileobjekt generieren
				File f = new File( dir );

				//DIR eventuell erzeugen inklusive SubDIRs
				// Pr�fe angegebenes Verzeichnis in Executorthread um einen Timeout zu erm�glichen
				ExecutorService exSrv = Executors.newSingleThreadExecutor();
				ExecutorCompletionService completionService = new ExecutorCompletionService( exSrv );
				completionService.submit( new CreateDirectory( f ) );
				Future<Boolean> createDirectorySuccess = completionService.poll( timeout, TimeUnit.SECONDS );
				// Fehler bei Zugriff auf das Verzeichnis
				if( createDirectorySuccess == null || createDirectorySuccess.get() != true ) {
					//Access Timeout
					result = new Ergebnis( "Status", "System", "", "", f.toString(), "ACCESS TIMEOUT", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					exSrv.shutdown();
					throw new PPExecutionException();
				}
				exSrv.shutdown();
				//Zu l�schende Dateinamen unter Ber�cksichtigung von max. zwei Wildcards ermitteln
				FilenameFilter FF = new FilenameFilter( filename + "." + extension );
				
				File f1 = new File( dir + System.getProperty( "file.separator" ) + filename + "." + extension );
				if( FF.numberTokens == 1 && FF.token1.equalsIgnoreCase( (filename + "." + extension).toLowerCase() ) && f1.exists() ) {
					//direktes l�schen der Datei wenn keine Wildcard -> deutlich schnellerer Ablauf bei Netzlaufwerken mit vielen Dateien
					fileList = new File[] { f1 };
				} else {
					fileList = f.listFiles( FF );
				}
				if( fileList == null || fileList.length == 0 ) {
					result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE(S) NOT AVAILABLE", "FALSE", "TRUE", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE );
					ergListe.add( result );
				} else {

					//Alle Dateien, die dem Suchmuster entsprechen, werden gel�scht
					for( int i = 0; i < fileList.length; i++ ) {
						if( !fileList[i].exists() || (fileList[i].exists() && fileList[i].canWrite()) ) {
							fileList[i].delete();
							//Ergebnis Doc
							result = new Ergebnis( "Status", "System", "", "", fileList[i].toString(), "FILE DELETE", "TRUE", "TRUE", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						} else {
							//Ergebnis Doc
							result = new Ergebnis( "Status", "System", "", "", fileList[i].toString(), "FILE DELETE EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						}

					}
				}
			} catch( SecurityException e ) {
				e.printStackTrace();
				result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE DELETE SECURITY EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		setPPStatus( info, status, ergListe );
	}

	private static class FilenameFilter implements FileFilter {
		String token1, token2, token3;
		int numberTokens;

		public FilenameFilter( String filter ) { // Konstruktor; pr�ft Dateinamen auf Wildcards und teilt ihn ggf. in Teilausdr�cke auf.
			if( filter == null ) {
				token1 = "";
				token2 = "";
			} else {
				StringTokenizer parser = new StringTokenizer( filter, "*" );
				numberTokens = parser.countTokens();
				switch( numberTokens ) {
					case 0: // Kein Token; ohne Wildcard
						token1 = filter.toLowerCase();
						token2 = "";
						token3 = "";
						break;
					case 1: // 1 Token; ein Wildcard
						token1 = parser.nextToken().toLowerCase();
						token2 = "";
						token3 = "";
						break;
					case 2: // 2 Token; ein Wildcard
						token1 = parser.nextToken().toLowerCase();
						token2 = parser.nextToken().toLowerCase();
						token3 = "";
						break;
					default: // 3 oder mehr Token; mind. 2 Wildcards
						token1 = parser.nextToken().toLowerCase();
						token2 = parser.nextToken().toLowerCase();
						token3 = parser.nextToken().toLowerCase();
						break;
				}
			}
		}

		public boolean accept( File file ) { // Methode, die f�r jeden Dateinamen im Verzeichnis �berpr�ft, ob er dem �bergebenen Suchmuster entspricht
			boolean isOK;
			int pos;

			switch( numberTokens ) {
				case 0: // Kein Token, ohne Wildcard
					isOK = file.isFile() && file.getName().equalsIgnoreCase( token1 );
					break;
				case 1: // 1 Token; ein Wildcard
					isOK = file.isFile() && file.getName().toLowerCase().startsWith( token1 );
					break;
				case 2: // 2 Token; ein Wildcard
					isOK = file.isFile() && file.getName().toLowerCase().startsWith( token1 );
					pos = file.getName().toLowerCase().indexOf( token2, token1.length() );
					isOK = isOK && pos >= 0;
					break;
				default: // 3 oder mehr Token; mind. 2 Wildcards
					pos = token1.length();
					isOK = file.isFile() && file.getName().toLowerCase().startsWith( token1 );
					pos = file.getName().toLowerCase().indexOf( token2, pos );
					isOK = isOK && pos >= 0;
					pos += token2.length();
					pos = file.getName().toLowerCase().indexOf( token3, pos );
					isOK = isOK && pos >= 0;
					break;
			}

			return (isOK);
		}

	}

	/**
	 *  Die Klasse CreateDirectory erm�glicht ein threadbasiertes �berpr�fen/Erzeugen des Zielordners.
	 * 
	 * @author Sch�nert Fabian, TI-545 BMW AG
	 *
	 */
	class CreateDirectory implements Callable {

		private File pathFile;

		public CreateDirectory( File pathFile ) {
			this.pathFile = pathFile;
		}

		@Override
		public Boolean call() {
			if( pathFile.exists() ) {
				return pathFile.canWrite();
			} else {
				return pathFile.mkdirs();
			}
		}
	}

	public static void main( String args[] ) { // Testmethode

		File f = new File( "c:/ec-apps/carserver/trace/" );
		File[] fileList;

		fileList = f.listFiles( new FilenameFilter( "csd*.log" ) );

		//Dateien l�schen
		for( int i = 0; i < fileList.length; i++ ) {
			if( !fileList[i].exists() || (fileList[i].exists() && fileList[i].canWrite()) ) {
				fileList[i].delete();
			} else {
			}

		}

	}

}

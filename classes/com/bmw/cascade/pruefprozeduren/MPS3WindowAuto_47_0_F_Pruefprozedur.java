package com.bmw.cascade.pruefprozeduren;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Date;
import java.util.LinkedList;
import java.util.Vector;

import com.bmw.appframework.logging.LogCategory;
import com.bmw.appframework.logging.LogLevel;
import com.bmw.appframework.logging.Logger;
import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.mps3.CardFensterheber;
import com.bmw.cascade.pruefstand.devices.mps3.Helper;
import com.bmw.cascade.pruefstand.devices.mps3.IPDefs;
import com.bmw.cascade.pruefstand.devices.mps3.MsgDefs;
import com.bmw.cascade.pruefstand.devices.mps3.MsgReceiver;
import com.bmw.cascade.pruefstand.devices.mps3.TcpClient;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.util.logging.CascadeLogging;
import com.bmw.cascade.server.PB;

/**
 * Diese Pr�fprozedur initialisiert die Messkarten im Multifunktionspr�fstand.
 * 
 * @author BMW TI-431 Burger
 * @version V_1_0 19.02.2010 AB Implementierung. To Do: DE/E, Logging, Fehlerzuordnung, Zeitverz�gertes Senden per Zufallszahl <br>
 * @version V_2_0_T 23.02.2010
 * @version V_3_0_F 09.03.2010 FR CardURI nach CardFensterheber
 * @version V_5_0_7 07.08.2010 User dialog, send params, window running
 * @version V_24_0_F 10.02.2011 ADD: BlockResult
 * @version V_27_0_F 08.03.2011 CHA: Streaming der Stromwerte in Datei (CSV) und APDM (XML) fertig gestellt; ADD: Fehler-HexCode-Ausgabe
 * @version V_28_0_F 29.03.2011 CHA: Streaming-Ergebnis = "Pr�fumfang + _BlockResult"
 * @version V_29_0_F 30.03.2011 CHA: Streaming-Ergebnis = "Pr�fumfang + _BLOCKRESULT"
 * @version V_31_0_T 30.06.2011 DEL: Nicht benutzter Pflicht-Parameter ZONES; CHA: Fehlerausgabe (Messkanal --> Port); CHA: StreamToFile (XML --> CSV,  VIN im Dateinamen, FileHandler --> Ordnergr��en�berwachung); ADD: Hall-Streaming
 * @version V_32_0_T 04.07.2011 CHA: Abfrage des Pflicht-Parameters IHALL von Integer (0/1) zu Boolean (T/True/F/False)
 * @version V_34_0_F 05.07.2011 F-Version
 * @version V_35_0_T 08.07.2011	ADD: Log-Output und Fehlermeldungen; CHA: Debug-Output --> Log-Output
 * @version V_36_0_T 08.07.2011	ADD: Bearbeitung der Parameter-Werte per trim();
 * 								ADD: Bei Parametern mit Boolean (T/True/F/False) werden auch Integer (0/1) akzeptiert
 * @version V_37_0_F 11.10.2011 F-Version
 * @version V_38_0_F 20.10.2011 CHA: getTrimmedArg -> Abfrage auf NULL vor trim()-Aufruf hinzugef�gt
 * @version V_39_0_F 10.04.2012 CHA: Fehlerausgabe "Drehrichtung falsch" -> "Z�hlrichtung falsch"; CHA: Kein Streaming bei Mehrfachablauf; Fensterheber-Fehlermeldungen erg�nzt
 * @version V_40_0_F 14.05.2012 CHA: Allgemeine und Fensterheber-Fehlermeldungen erg�nzt; CHA: Timeout auf 64000 begrenzt
 * @version V_41_0_F 18.02.2014 CHA: Allgemeinen LOG-Level auf 0 gesetzt
 * @version V_42_0_F 07.07.2014 ADD: Optional parameter DLGTXT_STREAMING
 * @version V_43_0_T 20.01.2015 CHA: Blue frame for "button press" instruction
 * @version V_44_0_F 28.01.2015 T version of V_43_0_T
 * @version V_45_0_F 04.05.2015 CHA: Zone parameters from _N to _[4...N]
 * 								DEL: Unused optional parameters VMIN_ZONE_N and VMAX_ZONE_N
 * @version V46_0_T 04.08.2015 TM T version
 * @version V47_0_F 07.08.2015 TM F version
 */
public class MPS3WindowAuto_47_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	static final long serialVersionUID = 1L;
	int i_StreamFileEnabled = 0;
	int i_StreamAPDMEnabled = 0;
	int i_Debug = 0;
	
	private Logger pruefstandLogger = CascadeLogging.getLogger("PruefstandLogger");
	
	/**
	 * Default-Konstruktor, nur fuer die Deserialisierung
	 */
	public MPS3WindowAuto_47_0_F_Pruefprozedur() {
		
	}
	
	/**
	 * Erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugehoerigen Prueflings
	 * @param pruefprozName Name der Pruefprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public MPS3WindowAuto_47_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
		super(pruefling, pruefprozName, hasToBeExecuted);
		attributeInit();
	}
	
	/**
	 * Initialsiert die Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}
	
	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * JP1: Jumper 1 T / F, TRUE / FALSE JP2: Jumper 2 T / F, TRUE / FALSE JP3:
	 * Jumper 3 T / F, TRUE / FALSE JP4: Jumper 4 T / F, TRUE / FALSE DI1:
	 * Digitaleingang 1 T / F, TRUE / FALSE DI2: Digitaleingang 2 T / F, TRUE /
	 * FALSE TIMEOUT: Timeout, bis Messwert erreicht DEBUG: Debug-Level 0, 1, 2, 3
	 * (0 = off) PAUSE: Wartezeit nach der Ausf�hrung AWT_TEXT: Text f�r AWT
	 * AWT_TITLE: Titel f�r AWT AWT_TIME: Timeout f�r AWT in ms AWT_STYLE: 1 =
	 * Status, 2 = Error, 99 = Alarm, Rest = Standard FWT_TEXT: Text f�r FWT
	 * FWT_TITLE: Titel f�r FWT FWT_TIME: Timeout f�r FWT in ms QWT_TEXT: Text f�r
	 * QWT QWT_TITLE: Titel f�r QWT QWT_TIME: Timeout f�r QWT in ms QWT_STYLE: 1 =
	 * Status, 2 = Error, 99 = Alarm, Rest = Standard
	 */
	public String[] getOptionalArgs() {
		String[] args = {/*"INC_ZONE_N", "IMIN_ZONE_N", "IMAX_ZONE_N",*/
				"INC_ZONE_[4..N]", "IMIN_ZONE_[4..N]", "IMAX_ZONE_[4..N]",
				"DEBUG", "STREAM_TO_FILE", "STREAM_TO_APDM",
				"THRESHOLD_EDGE_HALL", "DIGIN_START", "DLG_CAPTION", "DLGTXT_SEND_PARAMS",
				"DLGTXT_WAIT_START", "DLGTXT_WND_RUNNING", "DLGTXT_STREAMING", "HALL_DIFF"};
		return args;
	}
	
	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * TAG: Bezeichnung der anzusteuernden Messkarte wie in Pruefstandskonfiguration
	 */
	public String[] getRequiredArgs() {
		String[] args = { "TAG", "DIRECTION", "PWM", "IHALL", "IBLOCK", "LENGTH",
							"LENGTH_MAX_DEVIATION", "OPEN", "REVERS", "TIMEOUT_USERSTART", "TIMEOUT",
							"NUMBER_OF_EXECUTIONS", "INC_ZONE_1", "IMIN_ZONE_1", "IMAX_ZONE_1", "INC_ZONE_2",
							"IMIN_ZONE_2", "IMAX_ZONE_2", "INC_ZONE_3", "IMIN_ZONE_3", "IMAX_ZONE_3" };
		return args;
	}
	
	/**
	 * Entfernt �berfl�ssige Leerzeichen (nur am Anfang und am Ende) bei Parameter-Werten
	 */
	private String getTrimmedArg(String arg) {
		String tmp = getArg(arg);
		
		if (tmp != null) {
			return tmp.trim();
		} else {
			return tmp;
		}
	}
	
	/**
	 * Prueft - soweit moeglich - die Argumente auf Existenz und Wert.
	 * Ueberschreibt die parent-Methode aufgrund der offenen Anzahl an Results.
	 */	
	public boolean checkArgs() {
		if (getTrimmedArg("TAG").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("DIRECTION").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("PWM").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("IHALL").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("LENGTH").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("LENGTH_MAX_DEVIATION").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("OPEN").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("REVERS").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("TIMEOUT").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("TIMEOUT_USERSTART").length() <= 0) {
			return false;
		} else {
			int n = Integer.parseInt(getTrimmedArg("TIMEOUT_USERSTART"));
			if ((0 > n) || (n > 100000)) {
				return false;
			}
		}
		if (getTrimmedArg("NUMBER_OF_EXECUTIONS").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("INC_ZONE_1").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("IMIN_ZONE_1").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("IMAX_ZONE_1").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("INC_ZONE_2").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("IMIN_ZONE_2").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("IMAX_ZONE_2").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("INC_ZONE_3").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("IMIN_ZONE_3").length() <= 0) {
			return false;
		}
		if (getTrimmedArg("IMAX_ZONE_3").length() <= 0) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Fuehrt die Pruefprozedur aus.
	 * 
	 * @param info Information zur Ausfuehrung.
	 */
	public void execute(ExecutionInfo info) {
		// Result vars
		Vector v_ErgListe = new Vector();
		int i_Status = STATUS_EXECUTION_ERROR;
		
		// Hardware vars
		DeviceManager c_DevMan = null;
		if (getPr�flingLaufzeitUmgebung() != null) {
			c_DevMan = getPr�flingLaufzeitUmgebung().getDeviceManager();
		}
		
		CardFensterheber c_Card = null;
		Helper c_Helper = new Helper();
		
		// Message vars
		TcpClient c_Channel = null;
		MsgReceiver c_Receiver = null;
		
		// System vars
		boolean b_NoExecError = true;
		boolean b_NoCancel = true;
		
		// Local measuring vars
		String s_DlgCaption = "Fensterheber";
		String s_DlgTxtSendParams = "Fensterheber: Parameter senden. . .";
		String s_DlgTxtWaitStart = "Fensterheber: Warte auf Start. . .";
		String s_DlgTxtWndRunning = "Fensterheber l�uft. . .";
		String s_DlgTxtStreaming = "Messdatenaufzeichnung l�uft...";
		
		String s_Tag = "";
		
		c_Helper.logDebug(2,"MPS3WindowAuto_42_0_F_Pruefprozedur execute ...", "MPS3WindowAuto_42_0_F_Pruefprozedur execute ...", this.getClass());
		
		int i_digInStart			= 2;
		int i_ThresholdEdgeHall		= 700;
		int i_Direction				= 0;
		int i_Zones					= 0;
		int i_Pwm					= 0;
		int i_Hall					= 0;
		int i_Block					= 0;
		int i_IncLength				= 0;
		int i_IncLengthMaxDeviation	= 20;
		int i_IncOpen				= 0;
		int i_TimeRevers			= 0;
		int i_Timeout				= 0;
		int i_TimeoutUserStart		= 100000;
		int i_nbrOfExec				= 1;
		int i_hallDiff				= 10;
		int i_ResErrGen				= 0;
		int i_ResErrFh				= 0;
		
		LinkedList ll_TxData = new LinkedList();
		
		// Timing vars
		long l_StopTime = 0;
		
		v_ErgListe.clear();
		
		c_Helper.logInit(i_Debug);
		c_Helper.logDebug(2, "�berpr�fe Parameter", "check parameters", this.getClass());
		
		if (checkArgs() == false) {
			c_Helper.logDebug(0, "Mindestens ein zwingender Parameter fehlt.", "At least one required parameter is not supplied.", this.getClass());
									pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(),
									"At least one required parameter is not supplied.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "Karte (Parameter) erforderlich",
												"Mindestens ein erforderlicher Parameter fehlt.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "Card (Parameter) required",
												"At least one required parameter is not supplied.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		String s_Txt = "DEBUG";
		try {
			if (getTrimmedArg(s_Txt) != null) {
				String s_Tmp = getTrimmedArg(s_Txt);
				if ((s_Tmp == null) || (s_Tmp.equalsIgnoreCase("F")) || (s_Tmp.equalsIgnoreCase("False"))) {
					i_Debug = 0;
				} else {
					if ((s_Tmp.equalsIgnoreCase("T")) || (s_Tmp.equalsIgnoreCase("True"))) {
						i_Debug = 1;
					} else {
						try {
							i_Debug = Integer.parseInt(s_Tmp);
						} catch (NumberFormatException e) {
							i_Debug = 0;
							pruefstandLogger.logC(LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()",
														"\"DEBUG\" has wrong parameter format. Accepts only integer.", e);
						}
					}
				}
			}
		} catch (Exception e) {
			
		}
		
		c_Helper.logInit(i_Debug);
		
		s_Txt = "DLG_CAPTION";
		try {
			if (getTrimmedArg(s_Txt) != null) {
				s_DlgCaption = getTrimmedArg(s_Txt);
			}
		} catch (Exception e) {
			
		}
		
		s_Txt = "DLGTXT_SEND_PARAMS";
		try {
			if (getTrimmedArg(s_Txt) != null) {
				s_DlgTxtSendParams = getTrimmedArg(s_Txt);
			}
		} catch (Exception e) {
			
		}
		
		s_Txt = "DLGTXT_WAIT_START";
		try {
			if (getTrimmedArg(s_Txt) != null) {
				s_DlgTxtWaitStart = getTrimmedArg(s_Txt);
			}
		} catch (Exception e) {
			
		}
		
		s_Txt = "DLGTXT_WND_RUNNING";
		try {
			if (getTrimmedArg(s_Txt) != null) {
				s_DlgTxtWndRunning = getTrimmedArg(s_Txt);
			}
		} catch (Exception e) {
			
		}
		
		s_Txt = "DLGTXT_STREAMING";
		try {
			if (getTrimmedArg(s_Txt) != null) {
				s_DlgTxtStreaming = getTrimmedArg(s_Txt);
			}
		} catch (Exception e) {
			
		}
		
		s_Txt = "STREAM_TO_FILE";
		try {
			if (getTrimmedArg(s_Txt) != null) {
				String s_Tmp = getTrimmedArg(s_Txt);
				if ((s_Tmp == null) || (s_Tmp.equalsIgnoreCase("F")) || (s_Tmp.equalsIgnoreCase("False"))) {
					i_StreamFileEnabled = 0;
				} else {
					if ((s_Tmp.equalsIgnoreCase("T")) || (s_Tmp.equalsIgnoreCase("True"))) {
						i_StreamFileEnabled = 1;
					} else {
						try {
							i_StreamFileEnabled = Integer.parseInt(s_Tmp);
						} catch (NumberFormatException e) {
							i_StreamFileEnabled = 0;
							pruefstandLogger.logC(LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()",
														"\"STREAM_TO_FILE\" has wrong parameter format. Accepts only integer.", e);
						}
					}
				}
			}
		} catch (Exception e) {
			
		}
		
		s_Txt = "STREAM_TO_APDM";
		try {
			if (getTrimmedArg(s_Txt) != null) {
				String s_Tmp = getTrimmedArg(s_Txt);
				if ((s_Tmp == null) || (s_Tmp.equalsIgnoreCase("F")) || (s_Tmp.equalsIgnoreCase("False"))) {
					i_StreamAPDMEnabled = 0;
				} else {
					if ((s_Tmp.equalsIgnoreCase("T")) || (s_Tmp.equalsIgnoreCase("True"))) {
						i_StreamAPDMEnabled = 1;
					} else {
						try {
							i_StreamAPDMEnabled = Integer.parseInt(s_Tmp);
						} catch (NumberFormatException e) {
							i_StreamAPDMEnabled = 0;
							pruefstandLogger.logC(LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()",
														"\"STREAM_TO_APDM\" has wrong parameter format. Accepts only integer.", e);
						}
					}
				}
			}
		} catch (Exception e) {
			
		}
		
		s_Txt = "DIRECTION";
		try {
			if (getTrimmedArg(s_Txt) != null) {
				String s_Tmp = getTrimmedArg(s_Txt);
				if ((s_Tmp == null) || (s_Tmp.equalsIgnoreCase("F")) || (s_Tmp.equalsIgnoreCase("False"))) {
					i_Direction = 0;
				} else {
					if ((s_Tmp.equalsIgnoreCase("T")) || (s_Tmp.equalsIgnoreCase("True"))) {
						i_Direction = 1;
					} else {
						try {
							i_Direction = Integer.parseInt(s_Tmp);
						} catch (NumberFormatException e) {
							i_Direction = 0;
							pruefstandLogger.logC(LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()",
														"\"DIRECTION\" has wrong parameter format. Accepts only boolean or integer.", e);
						}
					}
				}
			}
		} catch (Exception e) {
			c_Helper.logDebug(0, "Argument \"" + s_Txt + " ist ung�ltig", "Argument \"" + s_Txt + " is invalid", this.getClass());
			c_Helper.logDebug(0, "Mindestens ein zwingender Parameter fehlt.", "At least one required parameter is not supplied.", this.getClass());
			pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one required parameter is not supplied.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "Karte (Parameter) erforderlich",
												"Mindestens ein erforderlicher Parameter fehlt.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "Card (Parameter) required",
												"At least one required parameter is not supplied.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		s_Txt = "TAG";
		try {
			s_Tag = getTrimmedArg(s_Txt);
		} catch (Exception e) {
			c_Helper.logDebug(0, "Argument \"" + s_Txt + " ist ung�ltig", "Argument \"" + s_Txt + " is invalid", this.getClass());
			c_Helper.logDebug(0, "Mindestens ein zwingender Parameter fehlt.", "At least one required parameter is not supplied.", this.getClass());
			pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one required parameter is not supplied.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "TAG (Parameter) erforderlich",
												"Mindestens ein erforderlicher Parameter fehlt.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "TAG (Parameter) required",
												"At least one required parameter is not supplied.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		s_Txt = "PWM";
		try {
			i_Pwm = Integer.parseInt(getTrimmedArg(s_Txt));
		} catch (Exception e) {
			c_Helper.logDebug(0, "Argument \"" + s_Txt + " ist ung�ltig", "Argument \"" + s_Txt + " is invalid", this.getClass());
			c_Helper.logDebug(0, "Mindestens ein zwingender Parameter fehlt.", "At least one required parameter is not supplied.", this.getClass());
			pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one required parameter is not supplied.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "PWM (Parameter) erforderlich",
												"Mindestens ein erforderlicher Parameter fehlt.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "PWM (Parameter) required",
												"At least one required parameter is not supplied.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		s_Txt = "IHALL";
		try {
			if (getTrimmedArg(s_Txt) != null) {
				String s_Tmp = getTrimmedArg(s_Txt);
				if ((s_Tmp == null) || (s_Tmp.equalsIgnoreCase("F")) || (s_Tmp.equalsIgnoreCase("False"))) {
					i_Hall = 0;
				} else {
					if ((s_Tmp.equalsIgnoreCase("T")) || (s_Tmp.equalsIgnoreCase("True"))) {
						i_Hall = 1;
					} else {
						try {
							i_Hall = Integer.parseInt(s_Tmp);
						} catch (NumberFormatException e) {
							i_Hall = 0;
							pruefstandLogger.logC(LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()",
														"\"IHALL\" has wrong parameter format. Accepts only boolean or integer.", e);
						}
					}
				}
			}
		} catch (Exception e) {
			c_Helper.logDebug(0, "Argument \"" + s_Txt + " ist ung�ltig", "Argument \"" + s_Txt + " is invalid", this.getClass());
			c_Helper.logDebug(0, "Mindestens ein zwingender Parameter fehlt.", "At least one required parameter is not supplied.", this.getClass());
			pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one required parameter is not supplied.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "IHALL (Parameter) erforderlich",
												"Mindestens ein erforderlicher Parameter fehlt.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "IHALL (Parameter) required",
												"At least one required parameter is not supplied.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		s_Txt = "IBLOCK";
		try {
			i_Block = Integer.parseInt(getTrimmedArg(s_Txt));
		} catch (Exception e) {
			c_Helper.logDebug(0, "Argument \"" + s_Txt + " ist ung�ltig", "Argument \"" + s_Txt + " is invalid", this.getClass());
			c_Helper.logDebug(0, "Mindestens ein zwingender Parameter fehlt.", "At least one required parameter is not supplied.", this.getClass());
			pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one required parameter is not supplied.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "IHALL (Parameter) erforderlich",
												"Mindestens ein erforderlicher Parameter fehlt.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "IHALL (Parameter) required",
												"At least one required parameter is not supplied.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		s_Txt = "LENGTH";
		try {
			i_IncLength = Integer.parseInt(getTrimmedArg(s_Txt));
		} catch (Exception e) {
			c_Helper.logDebug(0, "Argument \"" + s_Txt + " ist ung�ltig", "Argument \"" + s_Txt + " is invalid", this.getClass());
			c_Helper.logDebug(0, "Mindestens ein zwingender Parameter fehlt.", "At least one required parameter is not supplied.", this.getClass());
			pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one required parameter is not supplied.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "LENGTH (Parameter) erforderlich",
												"Mindestens ein erforderlicher Parameter fehlt.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "LENGTH (Parameter) required",
												"At least one required parameter is not supplied.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		s_Txt = "LENGTH_MAX_DEVIATION";
		try {
			i_IncLengthMaxDeviation = Integer.parseInt(getTrimmedArg(s_Txt));
		} catch (Exception e) {
			c_Helper.logDebug(0, "Argument \"" + s_Txt + " ist ung�ltig", "Argument \"" + s_Txt + " is invalid", this.getClass());
			c_Helper.logDebug(0, "Mindestens ein zwingender Parameter fehlt.", "At least one required parameter is not supplied.", this.getClass());
			pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one required parameter is not supplied.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "LENGTH (Parameter) erforderlich",
												"Mindestens ein erforderlicher Parameter fehlt.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "LENGTH (Parameter) required",
												"At least one required parameter is not supplied.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		s_Txt = "OPEN";
		try {
			i_IncOpen = Integer.parseInt(getTrimmedArg(s_Txt));
		} catch (Exception e) {
			c_Helper.logDebug(0, "Argument \"" + s_Txt + " ist ung�ltig", "Argument \"" + s_Txt + " is invalid", this.getClass());
			c_Helper.logDebug(0, "Mindestens ein zwingender Parameter fehlt.", "At least one required parameter is not supplied.", this.getClass());
			pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one required parameter is not supplied.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "OPEN (Parameter) erforderlich",
												"Mindestens ein erforderlicher Parameter fehlt.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "OPEN (Parameter) required",
												"At least one required parameter is not supplied.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		s_Txt = "REVERS";
		try {
			i_TimeRevers = Integer.parseInt(getTrimmedArg(s_Txt));
		} catch (Exception e) {
			c_Helper.logDebug(0, "Argument \"" + s_Txt + " ist ung�ltig", "Argument \"" + s_Txt + " is invalid", this.getClass());
			c_Helper.logDebug(0, "Mindestens ein zwingender Parameter fehlt.", "At least one required parameter is not supplied.", this.getClass());
			pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one required parameter is not supplied.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "REVERS (Parameter) erforderlich",
												"Mindestens ein erforderlicher Parameter fehlt.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "REVERS (Parameter) required",
												"At least one required parameter is not supplied.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		s_Txt = "TIMEOUT";
		try {
			i_Timeout = Integer.parseInt(getTrimmedArg(s_Txt));
			if (i_Timeout > 64000) {
				i_Timeout = 64000;
			}
		} catch (Exception e) {
			c_Helper.logDebug(0, "Argument \"" + s_Txt + " ist ung�ltig", "Argument \"" + s_Txt + " is invalid", this.getClass());
			c_Helper.logDebug(0, "Mindestens ein zwingender Parameter fehlt.", "At least one required parameter is not supplied.", this.getClass());
			pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one required parameter is not supplied.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "TIMEOUT (Parameter) erforderlich",
												"Mindestens ein erforderlicher Parameter fehlt.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "TIMEOUT (Parameter) required",
												"At least one required parameter is not supplied.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		s_Txt = "TIMEOUT_USERSTART";
		try {
			i_TimeoutUserStart = Integer.parseInt(getTrimmedArg(s_Txt));
		} catch (Exception e) {
			c_Helper.logDebug(0, "Argument \"" + s_Txt + " ist ung�ltig", "Argument \"" + s_Txt + " is invalid", this.getClass());
			c_Helper.logDebug(0, "Mindestens ein zwingender Parameter fehlt.", "At least one required parameter is not supplied.", this.getClass());
			pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one required parameter is not supplied.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "TIMEOUT_USERSTART (Parameter) erforderlich",
												"Mindestens ein erforderlicher Parameter fehlt.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "TIMEOUT_USERSTART (Parameter) required",
												"At least one required parameter is not supplied.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		s_Txt = "DIGIN_START";
		try {
			if (getTrimmedArg(s_Txt) != null) {
				i_digInStart = Integer.parseInt(getTrimmedArg(s_Txt));
			}
		} catch (Exception e) {
			c_Helper.logDebug(0, "Argument \"" + s_Txt + " ist ung�ltig", "Argument \"" + s_Txt + " is invalid", this.getClass());
			pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one parameter is invalid.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "DIGIN_START (Parameter)", "Mindestens ein Parameter ist ung�ltig.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "DIGIN_START (Parameter)", "At least one parameter is invalid.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		s_Txt = "THRESHOLD_EDGE_HALL";
		try {
			if (getTrimmedArg(s_Txt) != null) {
				i_ThresholdEdgeHall = Integer.parseInt(getTrimmedArg(s_Txt));
			}
		} catch (Exception e) {
			c_Helper.logDebug(0, "Argument \"" + s_Txt + " ist ung�ltig", "Argument \"" + s_Txt + " is invalid", this.getClass());
			pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one required parameter is not supplied.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "THRESHOLD_EDGE_HALL (Parameter)",
												"Mindestens ein Parameter ung�ltig.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "THRESHOLD_EDGE_HALL (Parameter)",
												"At least one parameter is invalid.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		s_Txt = "NUMBER_OF_EXECUTIONS";
		try {
			i_nbrOfExec = Integer.parseInt(getTrimmedArg(s_Txt));
		} catch (Exception e) {
			c_Helper.logDebug(0, "Argument \"" + s_Txt + " ist ung�ltig", "Argument \"" + s_Txt + " is invalid", this.getClass());
			c_Helper.logDebug(0, "Mindestens ein zwingender Parameter fehlt.", "At least one required parameter is not supplied.", this.getClass());
			pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one required parameter is not supplied.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "NUMBER_OF_EXECUTIONS (Parameter) erforderlich",
												"Mindestens ein erforderlicher Parameter fehlt.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "NUMBER_OF_EXECUTIONS (Parameter) required",
												"At least one required parameter is not supplied.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		s_Txt = "HALL_DIFF";
		try {
			if (getTrimmedArg(s_Txt) != null) {
				i_hallDiff = Integer.parseInt(getTrimmedArg(s_Txt));
			}
		} catch (Exception e) {
			c_Helper.logDebug(0, "Argument \"" + s_Txt + " ist ung�ltig", "Argument \"" + s_Txt + " is invalid", this.getClass());
			pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one parameter is not supplied.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "HALL_DIFF (Parameter)",
												"Mindestens ein Parameter ung�ltig.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "HALL_DIFF (Parameter)",
												"At least one parameter is invalid.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		for (int i=1; i<=100; i++) {
			s_Txt = "_ZONE_" + Integer.toString(i);
			if (getTrimmedArg("INC" + s_Txt) != null) {
				try {
					WindowData c_Tmp = new WindowData();
					c_Tmp.TxInc = Integer.parseInt(getTrimmedArg("INC" + s_Txt));
					c_Tmp.TxImin = Integer.parseInt(getTrimmedArg("IMIN" + s_Txt));
					c_Tmp.TxImax = Integer.parseInt(getTrimmedArg("IMAX" + s_Txt));
					
					ll_TxData.add(c_Tmp);
					i_Zones++;
				} catch (Exception e) {
					c_Helper.logDebug(0, "Argument \"" + s_Txt + " ist ung�ltig", "Argument \"" + s_Txt + " is invalid", this.getClass());
					c_Helper.logDebug(0, "Mindestens ein zwingender Parameter fehlt.", "At least one required parameter is not supplied.", this.getClass());
					pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one required parameter is not supplied.");
					if (isDE()) {
						i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", s_Txt + " (Parameter) erforderlich",
														"Mindestens ein erforderlicher Parameter fehlt.", v_ErgListe);
					} else {
						i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", s_Txt + " (Parameter) required",
														"At least one required parameter is not supplied.", v_ErgListe);
					}
					setPPStatus(info, i_Status, v_ErgListe);
					return;
				}
			}
		}
		
		if (i_Zones < 3) {
			c_Helper.logDebug(0, "Zu wenig Zonen.", "Too less zones.", this.getClass());
			pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "Too less zones.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "Zu wenig Zonen", "Mindestens ein erforderlicher Parameter fehlt.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "Too less Zones", "At least one required parameter is not supplied.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		if (i_Zones > 100) {
			c_Helper.logDebug(0, "Zu viele Zonen.", "Too many zones.", this.getClass());
			pruefstandLogger.logC(LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "Too less zones.");
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "Zu viele Zonen", "Mindestens ein erforderlicher Parameter fehlt.", v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "Too many Zones", "At least one required parameter is not supplied.", v_ErgListe);
			}
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		
		/*
		try {
			if (c_DevMan != null) {
				try {
					c_Card = (CardFensterheber) c_DevMan.getMPS3Card(s_Tag);
				} catch (Exception ex) {
					if (i_Debug != 0) {
						System.out.println("MPS3_WndAuto: c_DevMan.getMPS3Card, " + ex);
					}
					// throw(ex);
				}
			}
			if (c_Card == null) {*/
				// TODO: evtl nur f�r test. try direct
				/*
				if(i_Debug != 0) {
					System.out.println("MPS3_WndAuto: Failed to get card by DevMan, try direct." );
				}
				c_Card = new CardFensterheber("card_1", "1");
				*//*
			}
			c_Card.setDebug(i_Debug);
			c_Card.connectChannel(Integer.toString(IPDefs.FH), "5000");
			c_Channel = c_Card.getClient(Integer.toString(IPDefs.FH));
		} catch (Exception e) {
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_CHANNEL, "Konfiguration Pr�fstand pr�fen", "", "Fehler beim �ffnen von Port: " + IPDefs.FH + " auf Karte: " + s_Tag, v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_CHANNEL, "Check configuration of pr�fstand", "", "Error while opening port: " + IPDefs.FH + " at card: " + s_Tag, v_ErgListe);
			}
			pruefstandLogger.logC(LogLevel.WARNING, LogCategory.LOG_CATEGORY_DEVICE, this.getClass().getName(), "Error while opening port: " + IPDefs.FH
										+ " at card: " + s_Tag, "Error while Opening.", e);
			setPPStatus(info, i_Status, v_ErgListe);
			return;
		}
		*/
		
		try {
			//Connect to measuring channel
			c_Card = (CardFensterheber) c_DevMan.getMPS3Card(s_Tag);
			c_Card.setDebug(i_Debug);
			c_Card.connectChannel(Integer.toString(IPDefs.FH), "5000");
			c_Channel = c_Card.getClient(Integer.toString(IPDefs.FH));
		} catch (Exception e) {
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_CHANNEL, "Pr�fe Konfiguration vom Pr�fstand, Karte neu starten", "", "Fehler beim �ffnen von Port: " + IPDefs.FH + " auf Karte: " + s_Tag, v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_CHANNEL, "Check configuration of pr�fstand, restart card", "", "Error while opening port: " + IPDefs.FH + " at card: " + s_Tag, v_ErgListe);
			}
			pruefstandLogger.logC(LogLevel.WARNING, LogCategory.LOG_CATEGORY_DEVICE, this.getClass().getName(), "Error while opening port: " + IPDefs.FH + " at card: " + s_Tag, "Error while Opening.", e);
			setPPStatus (info, i_Status, v_ErgListe);
			return;
		}
		
		// Measure until result is "green" or timeout expired
		c_Helper.logDebug(2, "Karte verbunden", "card connected", this.getClass());
		
		UserDialog dlg = null;
		try {
			if (getPr�flingLaufzeitUmgebung() != null) {
				dlg = getPr�flingLaufzeitUmgebung().getUserDialog();
				if (dlg != null) {
					dlg.setAllowCancel(true);
					dlg.displayMessage(s_DlgCaption, s_DlgTxtSendParams, -1);
				}
			}
		} catch (DeviceLockedException e1) {
			c_Helper.logDebug(0, "Karte nicht verbunden: " + e1.getLocalizedMessage(), "Card not connected: " + e1.getLocalizedMessage(), this.getClass());
		} catch (DeviceNotAvailableException e1) {
			c_Helper.logDebug(0, "Karte nicht verbunden: " + e1.getLocalizedMessage(), "Card not connected: " + e1.getLocalizedMessage(), this.getClass());
		}
		
		// Measure until result is "green" or timeout expired
		c_Helper.logDebug(2, "Starte Timeout", "start timeout", this.getClass());
		
		l_StopTime = System.currentTimeMillis() + i_TimeoutUserStart;
		
		boolean b_UserStarted = false;
		boolean b_Measuring = false;
		
		int i_Data[] = new int[15 + (i_Zones * 3)];
		int idx = 0;
		i_Data[idx++] = MsgDefs.ACT_START;
		i_Data[idx++] = i_Zones;					// count of zones
		i_Data[idx++] = i_Direction;				// motor direction
		i_Data[idx++] = i_Hall;						// invert hall direction
		i_Data[idx++] = i_ThresholdEdgeHall;		// 700; // threshold for edge detection hall
		i_Data[idx++] = i_Pwm;						// power motor
		i_Data[idx++] = i_digInStart;				// 2; // dig in
		i_Data[idx++] = i_Block;					// max current window down
		i_Data[idx++] = i_IncLength;				// length of window (from down to up)
		i_Data[idx++] = i_IncOpen;					// increments to move window down on pass
		i_Data[idx++] = i_TimeRevers;				// time to move window down on error
		i_Data[idx++] = i_Timeout;					// timeout for test on card
		i_Data[idx++] = i_IncLengthMaxDeviation;	// [INC] max position error (length of window)
		i_Data[idx++] = i_hallDiff;					// [INC] max difference between Hall 1 and Hall 2
		i_Data[idx++] = i_nbrOfExec;				// count of tests
		
		for (int i=0; i<i_Zones; i++) {
			WindowData c_Tmp = (WindowData) ll_TxData.get(i);
			i_Data[idx++] = c_Tmp.TxInc;
			i_Data[idx++] = c_Tmp.TxImin;
			i_Data[idx++] = c_Tmp.TxImax;
		}
		
		if (i_nbrOfExec > 1) {
			if (i_StreamAPDMEnabled > 0) {
				i_StreamAPDMEnabled = 0;
				c_Helper.logDebug(0, "Kein APDM-Streaming, da Mehrfachablauf.", "No streaming to APDM because of multiple sequence.", this.getClass());
			}
			
			if (i_StreamFileEnabled > 0) {
				i_StreamFileEnabled = 0;
				c_Helper.logDebug(0, "Kein Datei-Streaming, da Mehrfachablauf.", "No streaming to file because of multiple sequence.", this.getClass());
			}
		}
		
		Socket socStream = null;
		
		if ((i_StreamFileEnabled != 0) || (i_StreamAPDMEnabled != 0)) {
			socStream = InitStreamSocket(c_Card.getAdress(), 5501);
		}
		
		if (c_Channel != null) {
			c_Helper.logDebug(2, "Sende. . .", "Send. . .", this.getClass());
			c_Channel.setTxMsg(MsgDefs.ID_CTRL_FH, i_Data, null, null);
			c_Helper.logDebug(2, "Warte auf Start. . .", "Wait for start. . .", this.getClass());
		} else {
			if (isDE()) {
				i_Status = setSystemError(MsgDefs.ERR_CHANNEL, "Pr�fe Konfiguration vom Pr�fstand, Karte neu starten", "", "Verbindung verloren. Port: " + IPDefs.FH + " auf Karte: " + s_Tag, v_ErgListe);
			} else {
				i_Status = setSystemError(MsgDefs.ERR_CHANNEL, "Check configuration of pr�fstand, restart card", "", "Lost connection with port: " + IPDefs.FH + " at card: " + s_Tag, v_ErgListe);
			}
			pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_DEVICE, this.getClass().getName(), "Lost connection with port: " + IPDefs.FH + " at card: " + s_Tag, "Error while Opening.");
			setPPStatus (info, i_Status, v_ErgListe);	
			return;
		}
		
		if (dlg != null) {
			dlg.displayStatusMessage(s_DlgCaption, s_DlgTxtWaitStart, -1);
		}
		
		boolean bSendStopCmd = true;
		
		while (b_NoExecError && b_NoCancel) {
			if (l_StopTime < System.currentTimeMillis()) {
				b_NoExecError = false;
				break;
			}
			
			//c_Helper.logDebug(2, "Starte Fensterheber", "Start window-lifter", this.getClass());
			
			c_Receiver = c_Channel.getRxMsgByID(MsgDefs.ID_STAT_FH, true);
			if (c_Receiver == null) {
				c_Helper.timer(100);
			} else {
				c_Helper.logDebug(2, "Daten empfangen", "Received data", this.getClass());
				
				if ((c_Receiver.words.length >= 1) && (c_Receiver.words[0] == MsgDefs.STATE_ACT_RUNNING)) {
					// user pressed start button => card started, new timeout !!!
					b_UserStarted = true;
					b_Measuring = true;
					l_StopTime = System.currentTimeMillis() + i_Timeout + 8000;
					
					c_Helper.logDebug(2, "Fensterheber l�uft", "Window lifter is running", this.getClass());
					
					if (dlg != null) {
						dlg.displayMessage(s_DlgCaption, s_DlgTxtWndRunning, -1);
					}
				} else if ((c_Receiver.words.length >= 1) && (c_Receiver.words[0] == 0x0007)) {
					if (dlg != null) {
						dlg.displayMessage(s_DlgCaption, s_DlgTxtStreaming, -1);
					}
					
					c_Helper.logDebug(2, "Streaming. . .", "Streaming. . .", this.getClass());
				} else if ((c_Receiver.words.length >= 1) && ((c_Receiver.words[0] == MsgDefs.STATE_ACT_FINISHED) || (c_Receiver.words[0] == MsgDefs.STATE_ACT_FAILED))) {
					bSendStopCmd = false; // don�t send stop cmd, card has finished if state is STATE_ACT_FINISHED || STATE_ACT_FAILED
					
					if (socStream != null) {
						GetStreamData(socStream, ll_TxData);
					}
					
					c_Helper.logDebug(2, "Fensterheberablauf beendet", "Window lifter sequence is done", this.getClass());
					
					if (c_Receiver.words.length >= 2) {
						int idxR = 1;
						int zonesCnt = c_Receiver.words[idxR++];
						
						//7 = state + zonesCnt + 3Dummies + error + errFh + (zonesCnt*current)
						if (c_Receiver.words.length != (7 + zonesCnt)) {
							c_Helper.logDebug(0, "Unbekannte Daten: recWordCnt=" + c_Receiver.words.length + ", zonesCnt=" + zonesCnt, "Unknown data: recWordCnt=" + c_Receiver.words.length + ", zonesCnt=" + zonesCnt, this.getClass());
							
							b_Measuring = false;
							b_NoExecError = false;
							
							break;
						} else {
							idxR++; // int dummy1 = c_Receiver.words[idxR++];
							idxR++; // int dummy2 = c_Receiver.words[idxR++];
							idxR++; // int dummy3 = c_Receiver.words[idxR++];
							
							// TODO check length
							for (int i=0; i<zonesCnt; i++) {
								v_ErgListe.add(new Ergebnis("WindowAuto" + (i + 1), this.getPPName(), "", "", "", "ZONE_INC [Inc]", Integer.toString(i_Data[15 + (i * 3)]), "",
													"", "", "", "", "", "", "", Ergebnis.FT_IGNORE));
								v_ErgListe.add(new Ergebnis("WindowAuto" + (i + 1), this.getPPName(), "", "", "", "ZONE_CURRENT [mA]", Integer
													.toString(c_Receiver.words[idxR++]), Integer.toString(i_Data[16 + (i * 3)]), Integer.toString(i_Data[17 + (i * 3)]), "", "", "", "", "",
													"", Ergebnis.FT_IGNORE));
								v_ErgListe.add(new Ergebnis("WindowAuto" + (i + 1), this.getPPName(), "", "", "", "ZONE_INC [Inc / s]", "", "0", "0", "", "", "", "", "", "",
													Ergebnis.FT_IGNORE));
							}
							
							i_ResErrGen = c_Receiver.words[idxR++];
							i_ResErrFh = c_Receiver.words[idxR++];
							
							b_NoExecError = true;
							b_Measuring = false;
							
							c_Helper.logDebug(1, "Complete error=" + i_ResErrGen + ", errFh=" + i_ResErrFh, "Complete error=" + i_ResErrGen + ", errFh=" + i_ResErrFh, this.getClass());
							
							break;
						}
					} else {
						c_Helper.logDebug(0, "Unbekannte Daten: recWordCnt=" + c_Receiver.words.length, "Unknown data: recWordCnt=" + c_Receiver.words.length, this.getClass());
						
						b_Measuring = false;
						b_NoExecError = false;
						break;
					}
				} else {
					b_NoExecError = false;
					break;
				}
			}
			
			try {
				if (dlg != null) {
					b_NoCancel = !dlg.isCancelled();
				}
			} catch (Exception e) {
				
			}
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				
			}
		}
		
		try {
			if (getPr�flingLaufzeitUmgebung() != null) {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			}
		} catch (Exception e) {
			c_Helper.logDebug(0, "Window-lifter" + " " + e, "window-lifter running" + " " + e, this.getClass());
			pruefstandLogger.logC(LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while closing AWT-Dialog: "
										+ "window-lifter running", e);
		}
		
		if (!b_NoCancel) {
			b_Measuring = false;
			
			if (isDE()) {
				i_Status = setMeasureError(0, 0, 0, 0, "", "", "User hat Pr�fung abgebrochen.", v_ErgListe);
			} else {
				i_Status = setMeasureError(0, 0, 0, 0, "", "", "User cancelled teststep.", v_ErgListe);
			}
		} else {
			if (b_NoExecError) {
				if ((i_ResErrGen == MsgDefs.ERR_NONE) && (i_ResErrFh == MsgDefs.ERR_NONE)) {
					c_Helper.logDebug(2, "Messung IO", "Measurement IO", this.getClass());
					i_Status = setMeasureOK(0, 0, 0, v_ErgListe);
				} else {
					c_Helper.logDebug(2, "Messung mit Systemfehler", "Mesurement with system error", this.getClass());

					if (isDE()) {
						i_Status = setSystemError(i_ResErrGen, "Pr�fe Parameter, Karte und Spannungsversorgung", getErrTxtGeneral(i_ResErrGen), "Fehler allgemein", v_ErgListe);
					} else {
						i_Status = setSystemError(i_ResErrGen, "Check parameters, card and power-supply", getErrTxtGeneral(i_ResErrGen), "error general", v_ErgListe);
					}
					
					if (i_ResErrFh != MsgDefs.ERR_NONE) {
						if (isDE()) {
							i_Status = setSystemError(i_ResErrFh, "Pr�fe Parameter, Karte und Spannungsversorgung", getErrTxtFh(i_ResErrFh), "Fehler Fensterheberspezifisch", v_ErgListe);
						} else {
							i_Status = setSystemError(i_ResErrFh, "Check parameters, card and power-supply", getErrTxtFh(i_ResErrFh), "error window specific", v_ErgListe);
						}
					}
				}
			} else {
				c_Helper.logDebug(0, "Mit Ausf�hrungsfehler beendet.", "Finished with execution errors.", this.getClass());
				
				if (b_UserStarted) {
					if (isDE()) {
						i_Status = setSystemError(MsgDefs.ERR_HW, "Pr�fe Parameter, Karte und Spannungsversorgung", "Fehler Fensterheberverlauf", "Ausf�hrungsfehler", v_ErgListe);
					} else {
						i_Status = setSystemError(MsgDefs.ERR_HW, "Check parameters, card and power-supply", "Error window regulator run", "Execution error", v_ErgListe);
					}
				} else {
					if (isDE()) {
						i_Status = setSystemError(MsgDefs.ERR_TIMEOUT, "Pr�fe Parameter, Karte und Spannungsversorgung", "Zeit�berschreitung beim Warten auf Start Taster", "Fehler allgemein", v_ErgListe);
					} else {
						i_Status = setSystemError(MsgDefs.ERR_TIMEOUT, "Check parameters, card and power-supply", "Timeout waiting for start button", "error general", v_ErgListe);
					}
				}
			}
		}
		
		if (b_Measuring) {
			if (isDE()) {
				i_Status = setMeasureError(0, 0, 0, 0, "Messung wiederholen", "Kommunikationsfehler", "Fehler beim Nachrichtenempfang", v_ErgListe);
			} else {
				i_Status = setMeasureError(0, 0, 0, 0, "Repeat measurement", "Communication error", "Error while receiving message", v_ErgListe);
			}
			
			c_Helper.logDebug(0, "Fehler beim Nachrichtenempfang", "Error while receiving message", this.getClass());
			pruefstandLogger.logC(LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while receiving message");
		}
		
		if (socStream != null) {
			ExitStreamSocket(socStream);
			socStream = null;
		}
		
		try {
			//if (c_Channel != null) {
			if (bSendStopCmd) {
				int data[] = new int[1];
				data[0] = MsgDefs.ACT_STOP;
				c_Channel.setTxMsg(MsgDefs.ID_CTRL_FH, data, null, null);
				l_StopTime = System.currentTimeMillis() + 2500;
				
				while (true) {
					c_Receiver = c_Channel.getRxMsgByID(MsgDefs.ID_STAT_FH, true);
					if (c_Receiver != null) {
						if (c_Receiver.words.length >= 1) {
							final int STATE_ACT_STOPPED = 6;
							if (c_Receiver.words[0] == STATE_ACT_STOPPED) {
								c_Helper.logDebug(2, "STATE_ACT_STOPPED reveiced (ack on stop command)", "STATE_ACT_STOPPED reveiced (ack on stop command)", this.getClass());
								break;
							}
						}
					}
					
					if (l_StopTime < System.currentTimeMillis()) {
						c_Helper.logDebug(2, "Timeout wait for ack on stop command", "Timeout wait for ack on stop command", this.getClass());
						break;
					}
					
					Thread.sleep(100);
				}
			}
			//}
			c_Card.closeChannel(Integer.toString(IPDefs.FH), true);
		} catch (Exception e) {
			c_Helper.logDebug(0, "Fehler beim Schlie�en des Kanals: " + e.getMessage(), "Error while closing channel: " + e.getMessage(), this.getClass());
			pruefstandLogger.logC(LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error ehile closing channel: " + e.getMessage());
		}
		
		try {
			c_Card.closeChannel(Integer.toString(IPDefs.FH), true);
		} catch (DeviceNotAvailableException e1) {
			c_Helper.logDebug(1, "Could not close card " + e1, "Could not close card " + e1, this.getClass());
		} catch (DeviceLockedException e1) {
			c_Helper.logDebug(1, "Could not close card " + e1, "Could not close card " + e1, this.getClass());
		}
		
		setPPStatus(info, i_Status, v_ErgListe);
	}
	
	Socket InitStreamSocket(String ip, int port) {
		Socket soc = null;
		Helper c_Helper = new Helper();
		
		try {
			InetAddress addr = InetAddress.getByName(ip);
			soc = new Socket(addr, port);
		} catch (Exception ex) {
			soc = null;
			c_Helper.logDebug(0, "MPS3_WndAuto: InitStreamSocket ip=" + ip + " port=" + port + ((soc == null) ? " failed" : " passed"), "MPS3_WndAuto: InitStreamSocket ip=" + ip + " port=" + port + ((soc == null) ? " failed due to " + ex.getLocalizedMessage() : " passed"), this.getClass());
		}
		
		c_Helper.logDebug(0, "MPS3_WndAuto: InitStreamSocket ip=" + ip + " port=" + port + ((soc == null) ? " failed" : " passed"), "MPS3_WndAuto: InitStreamSocket ip=" + ip + " port=" + port + ((soc == null) ? " failed" : " passed"), this.getClass());
		
		return soc;
	}
	
	void ExitStreamSocket(Socket soc) {
		Helper c_Helper = new Helper();
		
		try {
			if (soc != null) {
				soc.close();
			}
		} catch (Exception ex) {
			c_Helper.logDebug(1, "Fehler beim Schliessen: " + ex.getLocalizedMessage(), "Error while closing: " + ex.getLocalizedMessage(), this.getClass());
		}
	}
	
	long GetDirSize(File dir) {
		long size = 0;
		File[] files = dir.listFiles();
		if (files != null) {
			for (int i=0; i<files.length; i++) {
				if (files[i].isDirectory()) {
					size += GetDirSize(files[i]);
				} else {
					size += files[i].length();
				}
			}
		}
		return size;
	}
	
	boolean GetStreamData(Socket soc, LinkedList ll_TxData) {
		File f = null;
		FileWriter fw_file = null;
		FileWriter fw_apdm = null;
		Date now = null;
		Helper c_Helper = new Helper(); 
		
		try {
			c_Helper.logDebug(2, "Empfange Stream. . .", "Get stream. . .", this.getClass());
				
			Vector vecAllData = new Vector();
			InputStream stream = soc.getInputStream();
			
			if (stream == null) {
				c_Helper.logDebug(2, "GetStreamData Error, SocketInputStream is null", "GetStreamData Error, SocketInputStream is null", this.getClass());
			} else {
				boolean first = true;
				while (true) {
					int n = stream.available();
					if (!first && (n == 0)) {
						break;
					}
					first = false;
					if (n > 0) {
						c_Helper.logDebug(2, "GetStreamData : " + n + " bytes received", "GetStreamData : " + n + " bytes received", this.getClass()); 
						
						byte[] b = new byte[n];
						/* int read = */stream.read(b, 0, n);
						vecAllData.add(b);
					}
					Thread.sleep(2500);
				}
				
				if (i_StreamFileEnabled > 0) {
					File dir = new File("C:\\EC-Apps\\Cascade\\log\\windowData");
					try {
						if (!dir.exists()) {
							/*
							if (i_Debug != 0) {
								System.out.println("FileStreamverzeichnis noch nicht da.");
							}
							*/
							dir.mkdir();
							/*
							if (i_Debug != 0) {
								System.out.println("FileStreamverzeichnis jetzt da.");
							}
							*/
						} else {
							//System.out.println("FileStreamverzeichnis bereits da.");
							if (!dir.isDirectory()) {
								/*
								if (i_Debug != 0) {
									System.out.println("FileStreamverzeichnis ist kein Verzeichnis.");
								}
								*/
								i_StreamFileEnabled = 0;
							}
						}
					} catch (SecurityException ex) {
						c_Helper.logDebug(0, "FileStreamVerzeichniszugriff nicht moeglich.", "No directory access", this.getClass());
						i_StreamFileEnabled = 0;
					}
				}
				
				if (i_StreamFileEnabled > 0) {
					String puName = this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getLogicalName();
					String vin7 = this.getPr�fling().getAuftrag().getFahrgestellnummer7();
					
					File dir = new File("C:\\EC-Apps\\Cascade\\log\\windowData");
					String fileName = "C:\\EC-Apps\\Cascade\\log\\windowData\\" + vin7 + "_" + puName + "_" + System.currentTimeMillis() + ".csv";
					
					try {
						if (GetDirSize(dir) >= (1024000000 - 20480)) {
							c_Helper.logDebug(0, "FileStreamverzeichnis voll -> �lteste Datei loeschen...", "FileStreamverzeichnis voll -> �lteste Datei loeschen...", this.getClass());
							
							File[] files = dir.listFiles();
							int index = 0;
							for (int i=1; i<files.length; i++) {
								if (files[i].lastModified() < files[index].lastModified()) {
									index = i;
								}
							}
							
							files[index].delete();
						}
						
						f = new File(fileName);
						if (f.exists()) {
							f.delete();
						}
						f.createNewFile();
						fw_file = new FileWriter(f);
					} catch (IOException ex) {
						c_Helper.logDebug(0, "FileStreamVerzeichniszugriff nicht moeglich.", "No directory access", this.getClass());
						i_StreamFileEnabled = 0;
					} catch (SecurityException ex) {
						c_Helper.logDebug(0, "FileStreamVerzeichniszugriff nicht moeglich.", "No directory access", this.getClass());
						i_StreamFileEnabled = 0;
					}
				}
				
				try {
					if (i_StreamAPDMEnabled > 0) {
						now = new Date();
						String vin7 = this.getPr�fling().getAuftrag().getFahrgestellnummer7();
						String puName = this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getLogicalName();
						
						String s_StreamingDir = /*com.bmw.cascade.server.*/PB.getString("cascade.server.auftrag.reportxmlpath");
						String fileName = s_StreamingDir + "/X" + vin7 + "_" + puName + "_BLOCKRESULT" + "_" + System.currentTimeMillis() + ".xml";
						
						f = new File(fileName);
						if (f.exists()) {
							f.delete();
						}
						f.createNewFile();
						fw_apdm = new FileWriter(f);
					}
					
					int l = vecAllData.size();
					byte[] data = null;
					if (l == 1) {
						data = (byte[])(vecAllData.elementAt(0));
					} else if (l > 1) {
						int allLen = 0;
						for (int i=0; i<l; i++) {
							byte[] a = (byte[])(vecAllData.elementAt(i));
							allLen += a.length;
						}
						data = new byte[allLen];
						int dst = 0;
						for (int i=0; i<l; i++) {
							byte[] a = (byte[])(vecAllData.elementAt(i));
							System.arraycopy(a, 0, data, dst, a.length);
							dst += a.length;
						}
					}
					if (data != null) {
						final int headerLen = 24;
						if (data.length >= headerLen) {
							int o = 0;
							int hwId = (0x0000FF00 & (((int) data[o + 1]) << 8)) | (0x000000FF & (int) data[o]);
							o += 4;
							int offsI = (0x0000FF00 & (((int) data[o + 1]) << 8)) | (0x000000FF & (int) data[o]);
							o += 2;
							int offsU = (0x0000FF00 & (((int) data[o + 1]) << 8)) | (0x000000FF & (int) data[o]);
							o += 2;
							int sizeL = (0x0000FF00 & (((int) data[o + 1]) << 8)) | (0x000000FF & (int) data[o]);
							o += 2;
							int sizeH = (0x0000FF00 & (((int) data[o + 1]) << 8)) | (0x000000FF & (int) data[o]);
							o += 2;
							int err = (0x0000FF00 & (((int) data[o + 1]) << 8)) | (0x000000FF & (int) data[o]);
							o += 2;
							int errFh = (0x0000FF00 & (((int) data[o + 1]) << 8)) | (0x000000FF & (int) data[o]);
							o += 2;
							
							float facI = Float.intBitsToFloat(((0xFF000000 & (((int) data[o + 3]) << 24)) | (0x00FF0000 & (((int) data[o + 2]) << 16))
											| (0x0000FF00 & (((int) data[o + 1]) << 8)) | (0x000000FF & (int) data[o])));
							o += 4;
							float facU = Float.intBitsToFloat(((0xFF000000 & (((int) data[o + 3]) << 24)) | (0x00FF0000 & (((int) data[o + 2]) << 16))
											| (0x0000FF00 & (((int) data[o + 1]) << 8)) | (0x000000FF & (int) data[o])));
							o += 4;
							
							int dwSize = (((sizeH<<16) & 0xFFFF0000) | sizeL);
							
							String s = "MPS3_WndAuto: GetStreamData header: hwId=" + hwId + " offsI=" + offsI + " offsU=" + offsU + " sizeL=" + sizeL + " sizeH="
											+ sizeH + " err=" + err + " errFh=" + errFh + " facI=" + facI + " facU=" + facU + " dwSize=" + dwSize;
							
							c_Helper.logDebug(2, s, s, this.getClass());
							
							StringBuffer sb = new StringBuffer();
							int pos = 0;
							int zone = 0;
							WindowData wdZone = (WindowData)ll_TxData.get(zone);
							
							for (; o<(data.length-1); o+=2, pos++) {
								int nCur = (0x0000FF00 & (((int)data[o+1])<<8)) | (0x000000FF & (int)data[o]);
								int cur = (int)( (float)(nCur-offsI) * facI);
								
								if ((pos > wdZone.TxInc) && (ll_TxData.size() > (zone+1))) {
									zone++;
									wdZone = (WindowData)ll_TxData.get(zone);
								}
								
								if (i_StreamFileEnabled > 0) {
									fw_file.write("" + wdZone.TxImin);
									fw_file.write(";");
									fw_file.write("" + cur);
									fw_file.write(";");
									fw_file.write("" + wdZone.TxImax);
									fw_file.write("\r\n");
								}
								
								if (i_StreamAPDMEnabled > 0) {
									if (pos != 0) {
										sb.append('\n');
									}
									
									sb.append(pos);
									sb.append('\t');
									sb.append(cur);
									sb.append('\t');
									sb.append(wdZone.TxImin);
									sb.append('\t');
									sb.append(wdZone.TxImax);
								}
							}
							
							if (i_StreamAPDMEnabled > 0) {
								String b64Encoded = new sun.misc.BASE64Encoder().encode(sb.toString().getBytes());
								
								fw_apdm.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\r\n");
								fw_apdm.write("<testResult xmlns=\"http://bmw.com/standardResultData\"");
								fw_apdm.write(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
								fw_apdm.write(" xsi:schemaLocation=\"http://bmw.com/standardResultData \\\\Nvgm502.muc\\T-ee\\51_APDM\\a_System\\a_Systembeschreibung\\Standard-Ergebnisformat\\standardResultDataV0119.xsd\">\r\n");
								fw_apdm.write("<fileInfo systemName=\"");
								fw_apdm.write("Cascade\"");
								fw_apdm.write(" softwareVersion=\"");
								fw_apdm.write("MPS3Auto 40.0\"");
								//fw_apdm.write(" resultFormatVersion=\"");
								//fw_apdm.write("MPS3Auto 40.0\"");
								fw_apdm.write("/>\r\n");
								fw_apdm.write("<vehicleInfo shortVIN=\"");
								fw_apdm.write(this.getPr�fling().getAuftrag().getFahrgestellnummer7());
								fw_apdm.write("\" FZS=\"");
								fw_apdm.write(this.getPr�fling().getAuftrag().getSteuerschl�ssel());
								fw_apdm.write("\" typeKey=\"");
								fw_apdm.write(this.getPr�fling().getAuftrag().getTypschl�ssel());
								fw_apdm.write("\"/>\r\n");
								fw_apdm.write("<testInfo testStand=\"");
								fw_apdm.write(this.getPr�flingLaufzeitUmgebung().getName().substring(this.getPr�flingLaufzeitUmgebung().getName().indexOf('@') + 1));
								//fw_apdm.write("\" testDuration=\"");
								//fw_apdm.write();
								fw_apdm.write("\" errorCount=\"");
								fw_apdm.write("0");
								fw_apdm.write("\" complete=\"");
								fw_apdm.write("1");
								fw_apdm.write("\" pruefumfangName=\"");
								fw_apdm.write(this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getLogicalName() + "_BLOCKRESULT");
								fw_apdm.write("\" testVersion=\"");
								fw_apdm.write(this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getVersion());
								//fw_apdm.write("\" editor=\"");
								//fw_apdm.write(this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getAuthor());
								//fw_apdm.write("\" retryCountSummary=\"");
								//fw_apdm.write();
								fw_apdm.write("\" testTime=\"");
								fw_apdm.write(com.bmw.cascade.util.CascadeDateFormat.formatResultDate(now));
								fw_apdm.write("\" physicalPruefumfangName=\"");
								fw_apdm.write(this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getName());
								fw_apdm.write("\">\r\n");
								fw_apdm.write("<result testStepName=\"");
								fw_apdm.write(this.getPr�fling().getName());
								//fw_apdm.write("\" sequenceNrResult=\"");
								//fw_apdm.write();
								fw_apdm.write("\" testStepResult=\"");
								fw_apdm.write("OK");
								fw_apdm.write("\" blockResult=\"");
								fw_apdm.write(b64Encoded);
								//fw_apdm.write("\" errorType=\"");
								//fw_apdm.write();
								//fw_apdm.write("\" testStepDuration=\"");
								//fw_apdm.write();
								fw_apdm.write("\"/>\r\n");
								fw_apdm.write("</testInfo>\r\n");
								fw_apdm.write("</testResult>");
							}
						}
					}
				} catch (Exception ex) {
					c_Helper.logDebug(0, "GetStreamData Exception while write file, " + ex.getMessage(), "GetStreamData Exception while write file, " + ex.getMessage(), this.getClass());
				} finally {
					if (i_StreamFileEnabled > 0) {
						if (fw_file != null) {
							try {
								fw_file.close();
							} catch (Exception ex) {
								String s = "MPS3_WndAuto: GetStreamData Exception while close fileWriter, " + ex.getMessage();
								c_Helper.logDebug(0, s, s, this.getClass());
							}
						}
					}
					
					if (i_StreamAPDMEnabled > 0) {
						if (fw_apdm != null) {
							try {
								fw_apdm.close();
							} catch (Exception ex) {
								String s = "GetStreamData Exception while close apdmFileWriter, " + ex.getMessage();
								c_Helper.logDebug(0, s, s, this.getClass());
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			String s = "GetStreamData Exception, " + ex.getMessage();
			c_Helper.logDebug(0, s, s, this.getClass());
		}
		
		String s = "GetStreamData completed />";
		c_Helper.logDebug(0, s, s, this.getClass());
		
		return true;
	}
	
	private String getErrTxtGeneral(int err) {
		String ret = "";
		if (err == 0x0000) {
			ret = isDE() ? "Kein Fehler" : "No Error";
		} else {
			String str = new Integer(err).toString();
			while (str.length() < 4) {
				str = "0" + str;
			}
			str = "0x" + str;
			ret += str + " - ";
			
			if ((0x0001 & err) != 0) {
				ret += (isDE() ? "Ablauf zu sp�t gestartet ODER Endanschlag nicht erreicht. " : "Process started too late OR end stop not reached. ");
			}
			if ((0x0002 & err) != 0) {
				ret += (isDE() ? "�bertemperatur. " : "Overtemperature. ");
			}
			if ((0x0004 & err) != 0) {
				ret += (isDE() ? "�berstrom. " : "Overcurrent. ");
			}
			if ((0x0008 & err) != 0) {
				ret += (isDE() ? "Me�bereichs�berschreitung Strom. " : "Out of measuring range. ");
			}
			if ((0x0010 & err) != 0) {
				ret += (isDE() ? "Me�bereichs�berschreitung Spannung. " : "Out of measuring range. ");
			}
			if ((0x0020 & err) != 0) {
				ret += (isDE() ? "Hardwarefehler. " : "Hardware error. ");
			}
			if ((0x0040 & err) != 0) {
				ret += (isDE() ? "Fehler Fensterheber. " : "Error power window specific. ");
			}
			if ((0x0080 & err) != 0) {
				ret += (isDE() ? "Abbruch der Messung. " : "Test cancelled. ");
			}
			if ((0x0100 & err) != 0) {
				ret += (isDE() ? "Falsche Konfiguration. " : "Wrong configuration. ");
			}
			if ((0x0200 & err) != 0) {
				ret += (isDE() ? "Spannung zu niedrig. " : "Voltage too low. ");
			}
			if ((0x0400 & err) != 0) {
				ret += (isDE() ? "Spannung zu hoch. " : "Voltage to high. ");
			}
			if ((0x0800 & err) != 0) {
				ret += (isDE() ? "Strom zu niedrig. " : "Current too low. ");
			}
			if ((0x1000 & err) != 0) {
				ret += (isDE() ? "Strom zu hoch. " : "Current too high. ");
			}
			if ((0x2000 & err) != 0) {
				ret += (isDE() ? "Phase falsch. " : "Phase wrong. ");
			}
			if ((0x4000 & err) != 0) {
				ret += (isDE() ? "NOT-Aus. " : "Emergency stop. ");
			}
			if ((0x8000 & err) != 0) {
				ret += (isDE() ? "Nicht bereit. " : "Busy. ");
			}
			
			if(ret.length() == 0) {
				ret += (isDE() ? "Unbekannter Fehler. " : "Unknown error. ");
			}
		}
		return ret;
	}

	private String getErrTxtFh(int err) {
		String ret = "";
		if (err == 0x0000) {
			ret = isDE() ? "Kein Fensterheber Fehler" : "No Power window error";
		} else {
			String str = new Integer(err).toString();
			while (str.length() < 4) {
				str = "0" + str;
			}
			str = "0x" + str;
			ret += str + " - ";
			
			if ((0x0001 & err) != 0) {
				ret += (isDE() ? "Hallgeber FH fehlt / defekt / nicht gesteckt. " : "Hall sensor missing / defect / not connected. ");
			}
			if ((0x0002 & err) != 0) {
				ret += (isDE() ? "Z�hlrichtung FH falsch. " : "Wrong direction of rotation. ");
			}
			if ((0x0004 & err) != 0) {
				ret += (isDE() ? "Stromaufnahme zu niedrig. " : "Current too low. ");
			}
			if ((0x0008 & err) != 0) {
				ret += (isDE() ? "Geschwindigkeit zu niedrig. " : "Velocity too low. ");
			}
			if ((0x0010 & err) != 0) {
				ret += (isDE() ? "Stromaufnahme zu hoch. " : "Current to high. ");
			}
			if ((0x0020 & err) != 0) {
				ret += (isDE() ? "Geschwindigkeit zu hoch. " : "Velocity to high. ");
			}
			if ((0x0400 & err) != 0) {
				ret += (isDE() ? "Gefahrener Weg passt nicht. " : "Moved distance does not match. ");
			}
			if ((0x0800 & err) != 0) {
				ret += (isDE() ? "Panikabbruch (Not-Halt). " : "Emergency stop. ");
			}
			if (err == 0x1000) {
				ret += (isDE() ? "Panikabbruch (Not-Aus). " : "Emergency stop. ");
			}
			if (err == 0x2000) {
				ret += (isDE() ? "Nicht bereit, laufender Prozess. " : "Busy, running process. ");
			}
			if (ret.length() == 0) {
				ret += (isDE() ? "Unbekannter Fensterheber Fehler. " : "Unknown Power window error. ");
			}
		}
		return ret;
	}
	
	/**
	 * Setzt das Pr�fungsergebnis auf Systemfehler und generiert entsprechende
	 * Eintr�ge.
	 * 
	 * @param errCode
	 *          spezifischer Fehlercode
	 * @param awt
	 *          spezifischer Anweisungstext
	 * @param hwt
	 *          spezifischer Hinweistext
	 * @param errt
	 *          spezifischer Fehlertext
	 * @param ergebnisListe
	 *          zu beschreibende ErgListe
	 * @return status status als int
	 */
	private int setSystemError(int errCode, String awt, String hwt, String errt, Vector ergListe) {
		ergListe.add(new Ergebnis(Integer.toString(errCode), this.getPPName(), "", "", "", "", "", "", "", "", "", "", awt, errt, hwt, Ergebnis.FT_NIO));
		return STATUS_EXECUTION_ERROR;
	}
	
	/**
	 * Setzt das Pruefungsergebnis auf Messung IO und generiert entsprechende Eintraege.
	 * 
	 * @param value finale(r) Messwert(e)
	 * @param min Minimalwert
	 * @param max Maximalwert
	 * @param ergebnisListe zu beschreibende Ergebnisliste
	 * @return status Status als Integer
	 */
	private int setMeasureOK(int value, int min, int max, Vector ergListe) {
		if (isDE()) {
			ergListe.add(new Ergebnis("0", this.getPPName(), "", "", "", "Ergebnis IO", Integer.toString(value), Integer.toString(min), Integer.toString(max), "",
								"", "", "", "", "", Ergebnis.FT_IO));
		} else {
			ergListe.add(new Ergebnis("0", this.getPPName(), "", "", "", "Result ok", Integer.toString(value), Integer.toString(min), Integer.toString(max), "", "",
								"", "", "", "", Ergebnis.FT_IO));
		}
		return STATUS_EXECUTION_OK;
	}
	
	/**
	 * Setzt das Pruefungsergebnis auf Messung NIO und generiert entsprechende Eintraege.
	 * 
	 * @param errCode spezifischer Fehlercode
	 * @param value finaler Messwert
	 * @param min Minimalwert
	 * @param max Maximalwert
	 * @param awt spezifischer Anweisungstext
	 * @param hwt spezifischer Hinweistext
	 * @param errt spezifischer Fehlertext
	 * @param ergebnisListe zu beschreibende Ergebnisliste
	 * @return status Status als int
	 */
	private int setMeasureError(int errCode, int value, int min, int max, String awt, String hwt, String errt, Vector ergListe) {
		if (isDE()) {
			ergListe.add(new Ergebnis(Integer.toString(errCode), this.getPPName(), "", "", "", "Ergebnis NIO", Integer.toString(value), Integer.toString(min),
								Integer.toString(max), "", "", "", awt, errt, hwt, Ergebnis.FT_NIO));
		} else {
			ergListe.add(new Ergebnis(Integer.toString(errCode), this.getPPName(), "", "", "", "Result out of range", Integer.toString(value), Integer.toString(min),
								Integer.toString(max), "", "", "", awt, errt, hwt, Ergebnis.FT_NIO));
		}
		return STATUS_EXECUTION_ERROR;
	}
	
	/*
	 * Loest in
	 * 
	 * @-geklammerte Attribute dynamisch (zur Laufzeit) auf.
	 * 
	 * @param input Input-String
	 * @return gefundener Wert; input ohne @-Klammerung, dynamisch geholte mit Klammerung.
	 */
	/*
	private String getDynamicAttribute(String input) {
		String result = input;
		Helper funcs = new Helper();

		funcs.logInit(i_Debug);

		funcs.logDebug(2, "Start getDynamicAttribute. . .", "Start getDynamicAttribute . . .", this.getClass());
		if (input.startsWith("@")) {
			funcs.logDebug(2, "Erstes Token gefunden", "Found first Token", this.getClass());
			if (input.endsWith("@")) {
				funcs.logDebug(2, "Zweites Token gefunden", "Found second Token", this.getClass());
				if (input.length() >= 3) {
					try {
						funcs.logDebug(2, "L�nge g�ltig. Hole dynamisches Attribut. . .", "Length valid. fetch dynmaic attribute. . .", this.getClass());
						result = (String) getPr�fling().getAllAttributes().get(input.substring(1, input.length() - 1));
						funcs.logDebug(2, "Dynamisches Attribut aufgel�st: " + result, "Resolved dynamic attribute: " + result, this.getClass());
					} catch (Exception e) {
						//@todo: wenn ein fehler auftritt nicht versuchen mit einem beliebigen text weiterzuarbeiten. sondern exception!
						result = input.substring(0, input.length() - 1);
						funcs.logDebug(0, "Fehler beim dynamischen Aufl�sen. Es wird zur�ck gegeben: " + result, "Error during dynamic lookup. It will be returned: " + result, this.getClass());
					}
				} else {
					result = null;
					funcs.logDebug(0, "L�nge ung�ltig. Es wird \"null\" zur�ck gegeben.", "Length invalid. \"null\" is returned.", this.getClass());
				}
			} else {
				result = input.substring(1, input.length());
				funcs.logDebug(0, "Zweites \"@\" nicht gefunden. Es wird zur�ck gegeben: " + result, "Did not find second \"@\". It will be returned: " + result, this.getClass());
			}
		} else if (input.endsWith("@")) {
			result = input.substring(0, input.length() - 1);
			funcs.logDebug(0, "Erstes \"@\" nicht gefunden. Es wird zur�ck gegeben: " + result, "Did not find first \"@\". It will be returned: " + result, this.getClass());
		} else {
			funcs.logDebug(2, "Kein \"@\" gefunden. Statisches Attribut. Value: " + result, "No \"@\" found. Static attribute. Value: " + result, this.getClass());
			result = input;
		}
		
		return result;
	}
	*/
	
	/**
	 * Diese Funktion prueft, ob in den CascadeProperties deutsche Texte eingestellt sind.
	 * 
	 * @result True fuer deutsche Texte, sonst False (Default ist Englisch).
	 */
	private static boolean isDE() {
		try {
			if (CascadeProperties.getLanguage().equalsIgnoreCase("DE") == true) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	private class WindowData {
		public WindowData() {
			
		}
		
		public int TxInc;
		public int TxImin;
		public int TxImax;
	}
}

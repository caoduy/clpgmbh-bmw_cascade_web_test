/*
 * ReadRDCHandheldCorghi_1_1_F_Pruefprozedur.java
 *
 * Created on 19.02.12
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.DeviceIOException;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.serial.SerialGeneric;
import com.bmw.cascade.pruefstand.devices.serial.SerialParameters64;
import com.bmw.cascade.pruefstand.framework.udnext.UDNButtonSection.UDNButton;
import com.bmw.cascade.pruefstand.framework.udnext.UDNHandle;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.UDNState;
import com.bmw.cascade.pruefstand.framework.udnext.UserDialogNextRuntimeEnvironment;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementierung einer Pr�fprozedur zum Auslesen und Speichern der RDC Transponderdaten, die �ber ein RDC CORGHI Handger�t erfasst wurden.<BR>
 * @author Buboltz <BR>
 * @version Implementierung <BR>
 * @version 1_0_F	20.10.2015  TB Erstimplementierung <BR>
 * @version 1_1_F	27.01.2016  KW Anpassung f�r Corghi Firmware 2.28 <BR>
 * @version 2_0_F   03.02.2016  KW Anpassung f�r Corghi Firmware 2.29 <BR>
 * @version 3_0_F   23.03.2016  KW Update Lib f�r Serialkommunikation <BR>
 * @version 4_0_T   24.08.2016  KW Anpassung: Optionales Argument "TAG" ist definiert f�r CommPort Identifier <BR>
 * @version 5_0_F   02.09.2016  KW F-version <BR>
 */
public class ReadRDCHandheldCorghi_5_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	// gibt an, ob Debug-Ausgaben erfolgen sollen
	boolean debug = false;

	// generisches serielles Device 
	private SerialGeneric myGenericSerialDevice = null;
	
	// serielles Device CommPort Identifier
	private String tag = null;

	// Systemsprache DE
	private final boolean DE = checkDE(); // Systemsprache DE wenn true

	/** Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public ReadRDCHandheldCorghi_5_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public ReadRDCHandheldCorghi_5_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 * @return Optionale Argumente als String Array
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG", "TAG"};
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 * @return Zwingende Argumente als String Array
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = {};
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 *
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt
	 *         hat, ansonsten <code>true</code>.
	 */
	@Override
	public boolean checkArgs() {
		// Obligatorisch.
		return super.checkArgs();
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		final byte pongConfirmation[] = new byte[] { 0x7C, 0x00, 0x14, 0x50, 0x4F, 0x4E, 0x47, 0x2C, 0x30, 0x33, 0x30, 0x32, 0x35, 0x39, 0x39, 0x39, 0x39, 0x2C, 0x3A, 0x34 }; //{|..PONG,030259999,:4
		final byte okConfirmation[] = new byte[] { 0x7C, 0x00, 0x0C, 0x54, 0x43, 0x2C, 0x31, 0x2C, 0x4F, 0x4B, 0x3A, 0x7C }; // |..TC,1,OK:|
		UserDialogNextRuntimeEnvironment udSingle = null;
		UDNHandle udSingleHandle = null;
		UserDialog udMulti = null;
		boolean singleMode = !this.isMultiInstance();

		try {
			//Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// TAG abfragen
				if( getArg( "TAG" ) != null ) {
					tag = getArg( "TAG" ); 
				}
				
				// DEBUG abfragen
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						debug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						debug = false;
				} else {
					// noch Pr�fstandvariable pr�fen, ob Debug
					try {
						Object pr_var_debug;

						pr_var_debug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
						if( pr_var_debug != null ) {
							if( pr_var_debug instanceof String ) {
								if( "TRUE".equalsIgnoreCase( (String) pr_var_debug ) ) {
									debug = true;
								} else {
									debug = false;
								}
							} else if( pr_var_debug instanceof Boolean ) {
								if( ((Boolean) pr_var_debug).booleanValue() ) {
									debug = true;
								} else {
									debug = false;
								}
							}
						} else {
							debug = false;
						}

					} catch( VariablesException e ) {
						debug = false;
					}
				}

				// debug
				if( debug ) {
					System.out.println( "ReadRDCHandheldCorghi PP: Parameter Check is done." );
				}

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// UserDialog holen
			if( singleMode ) {
				udSingle = getPr�flingLaufzeitUmgebung().getUserDialogNext();
				udSingleHandle = udSingle.allocateUserDialogNext();
			} else {
				udMulti = getPr�flingLaufzeitUmgebung().getUserDialog();
			}

			// Benutzerausgabe, warte auf Kommunikation
			UserInteractionThread uiThread = new UserInteractionThread( udSingle, udSingleHandle, udMulti, singleMode, DE ? "RDC-Corghi Handterminal" : "RDC-Corghi handheld", DE ? "\n\rWarte auf Daten..." : "\n\rWaiting on data..." );
			uiThread.start();

			DeviceManager deviceManager = getPr�flingLaufzeitUmgebung().getDeviceManager();

			try {
				if( tag != null ) {
					myGenericSerialDevice = deviceManager.getSerialGeneric(tag);
				} else {
					myGenericSerialDevice = deviceManager.getSerialGeneric();
				}
			} catch( Exception e ) {
				try {
					deviceManager.releaseSerialGeneric();
				} catch( Exception ex ) {
					if( debug ) {
						ex.printStackTrace();
					}
				} catch( Throwable th ) {
					if( debug ) {
						th.printStackTrace();
					}
				}
				myGenericSerialDevice = null;
				throw e;
			}

			/** CORGHI RDC Handger�t nutzt folgende fixe Kommunikationsparameter:
			 *  - Baudrate: 115200 Bits\s, 8 Datenbits, 1 Stopp-Bit, kein Parit�t, kein Handshake / keine Flusskontrolle
			 */

			myGenericSerialDevice.open( 115200, 0, 8, 1, SerialParameters64.Parity.NONE );

			// warte auf "PING" Kommunikationsaufbau vom RDC Handheld 
			readDataLoop( uiThread, "PING", ergListe );

			// Direkte Kommunikationsantwort wird vom Corghi Handterminal nicht verstanden
			try {
				Thread.sleep( 300 );
			} catch( InterruptedException iex ) {
				// nothing
			}

			// sende Bestaetigung "PONG"
			writeData( pongConfirmation );

			// Direkte Kommunikationsantwort wird vom Corghi Handterminal nicht verstanden
			try {
				Thread.sleep( 300 );
			} catch( InterruptedException iex ) {
				// nothing
			}

			// warte auf Transponderdaten vom RDC Handheld, wenn diese g�ltig sind enthalten sie das TOKEN "FRW" = 'front right wheel'
			String transponderData = new String( readDataLoop( uiThread, "FRW,", ergListe ) );

			// UserDialog / Thread schlie�en
			uiThread.releaseUserDialog();
			uiThread = null;

			// Direkte Kommunikationsantwort wird vom Corghi Handterminal nicht verstanden
			try {
				Thread.sleep( 300 );
			} catch( InterruptedException iex ) {
				// nothing
			}

			// sende Bestaetigung "OK"
			writeData( okConfirmation );

			// UserInfo und dokumentieren
			List<String> tokens = Arrays.asList( "FRW", "RRW", "RLW", "FLW" );
			StringBuffer udTransponderContent = new StringBuffer( DE ? "Gelesene Transponder ID's:\n\n" : "Read Transponder ID's:\n\n" );

			for( String token : tokens ) {
				@SuppressWarnings("resource")
				Scanner scanner = new Scanner( transponderData.substring( transponderData.indexOf( token ) + token.length() ) ).useDelimiter( "," );
				String transponder_id = scanner.next();
				String transponder_name = token.substring( 0, 3 );
				scanner.close();

				udTransponderContent.append( transponder_name + " ID: " + transponder_id + "\n" );

				// Doku virtuelles Fahrzeug
				result = new Ergebnis( transponder_name, "ReadRDCHandheldCorghi", "", "", "", transponder_name, transponder_id, transponder_id, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );

				// Debug
				if( debug ) {
					System.out.println( "ReadRDCHandheldCorghi PP: Transponder Name <" + transponder_name + ">, ID <" + transponder_id + ">" );
				}
			}

		} catch( PPExecutionException e ) {
			if( debug ) {
				e.printStackTrace();
			}
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			if( debug ) {
				e.printStackTrace();
			}
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			if( debug ) {
				e.printStackTrace();
			}
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// serielle Schnittstelle Verbindung schlie�en
		try {
			myGenericSerialDevice.close();
		} catch( DeviceIOException e ) {
			if( debug ) {
				e.printStackTrace();
			}
		}

		// Device freigeben
		if( myGenericSerialDevice != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseSerialGeneric();
			} catch( DeviceLockedException e ) {
				if( debug ) {
					e.printStackTrace();
				}
			}

			myGenericSerialDevice = null; // serielles Device wieder freigeben
		}

		if( singleMode && udSingle != null ) {
			udSingle.releaseUserDialogNext( udSingleHandle );
		}

		if( !singleMode && udMulti != null ) {
			try {
				this.getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( DeviceLockedException e ) {
			}
		}

		//Status setzen
		setPPStatus( info, status, ergListe );
	}

	/**
	 * Eigentliches Daten einlesen
	 * 
	 * @param uiThread
	 * @param TOKEN
	 * @param ergListe
	 * @return
	 * @throws PPExecutionException, DeviceIOException
	 */
	private byte[] readDataLoop( UserInteractionThread uiThread, final String TOKEN, Vector<Ergebnis> ergListe ) throws PPExecutionException, DeviceIOException {
		byte[] readData = null;
		Ergebnis result;

		while( true ) {
			// checke paralleln Nutzerabbruch
			if( uiThread != null && !uiThread.isAlive() ) {
				result = new Ergebnis( "ExecFehler", "RDC-Corghi", "", "", "", "", "", "", "", "0", "", "", "", DE ? "CANCEL-Button gedrueckt!" : "CANCEL-Button pressed!", PB.getString( "Benutzerabbruch" ), Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException( "UserDialog and corresponding Data Communication was cancelled" );
			}

			// lese Daten vom RDC Handheld
			readData = readData();
			if( readData != null && new String( readData ).indexOf( TOKEN ) > -1 ) {
				if( debug ) {
					System.out.println( TOKEN + " received... OK." );
				}
				break;
			}

			// Warte etwas
			try {
				Thread.sleep( 100 );
			} catch( InterruptedException iex ) {
				//nothing
			}

			// Logausgabe
			if( debug ) {
				System.out.println( "waiting for Data..." );
			}
		}

		return readData;
	}

	/** 
	 * Daten von serieller Schnittstelle empfangen 
	 * Wenn keine Daten bereit: Array der l�nge 0 
	 * 
	 * @return Empfangene Bytes
	 * @throws DeviceIOException Fehler
	 */
	private byte[] readData() throws DeviceIOException {
		byte[] readData;

		if( debug ) {
			System.out.println( "read serial Data started..." );
		}

		readData = myGenericSerialDevice.readData();

		if( debug ) {
			System.out.println( "serial Data <" + Arrays.toString( readData ) + "> received" );
			System.out.println( "serial Data <" + (readData != null ? new String( readData ) : "?") + "> received" );
		}
		return readData;
	}

	/** Daten an serieller Schnittstelle ausgeben
	 * 
	 * @param buffer zu sendende Bytes
	 * @throws DeviceIOException Fehler
	 */
	private void writeData( byte[] buffer ) throws DeviceIOException {
		if( debug ) {
			System.out.println( "write serial Data started..." );
		}

		myGenericSerialDevice.writeData( buffer );

		if( debug ) {
			System.out.println( "serial Data <" + Arrays.toString( buffer ) + "> written" );
			System.out.println( "serial Data <" + new String( buffer ) + "> received" );
		}
	}

	/**
	 * Pr�ft mit Hilfe eines Http Requests auf den 2. Pr�fstandscreen, ob der Pr�fstand im Multiinstanzmodus l�uft. 
	 * 
	 * @return <code>True</code>, wenn Pr�fstand im Multiinstanzmodus l�uft,
	 *         <code>false</code> wenn nicht.
	 * @author  Fabian Schoenert, BMW AG TI-545
	 */
	private boolean isMultiInstance() {
		String url = "http://localhost:8086/testscreen-2";
		try {
			HttpURLConnection con = (HttpURLConnection) new URL( url ).openConnection();
			con.setConnectTimeout( 500 ); // in ms
			con.setRequestMethod( "GET" ); // ist eigentlich Default
			con.getInputStream(); // throws IOException wenn Adresse nicht erreichbar
			con.disconnect();
		} catch( final IOException e ) {
			// Nicht im MI Modus
			return false;
		} catch( Exception e ) {
			// Wenn Fehler bei Http Abfrage kein MI Modus
			return false;
		}
		return true;
	}

	/**
	 * Eigener Thread zur Benutzerinteraktion
	 */
	private class UserInteractionThread extends Thread {
		private UserDialogNextRuntimeEnvironment udSingle = null;
		private UDNHandle udSingleHandle = null;
		private UserDialog udMulti = null;
		private final boolean singleMode;
		private final String TITLE, MESSAGE;

		/**
		 * Freigabe des UserDialoges
		 */
		public void releaseUserDialog() {
			if( singleMode ) {
				udSingle.releaseUserDialogNext( udSingleHandle );
			} else {
				try {
					getPr�flingLaufzeitUmgebung().releaseUserDialog();
				} catch( DeviceLockedException e ) {
				}
			}
		}

		/**
		 * Konstruktor
		 * 
		 * @param ud
		 * @param TITLE
		 * @param MESSAGE
		 */
		public UserInteractionThread( UserDialogNextRuntimeEnvironment udSingle, UDNHandle udSingleHandle, UserDialog udMulti, boolean singleMode, final String TITLE, final String MESSAGE ) {
			this.udSingle = udSingle;
			this.udSingleHandle = udSingleHandle;
			this.udMulti = udMulti;
			this.singleMode = singleMode;
			this.TITLE = TITLE;
			this.MESSAGE = MESSAGE;
		}

		/**
		 * �berschriebene run Methode
		 */
		@Override
		public void run() {
			if( singleMode ) {
				udSingle.displayMessage( udSingleHandle, UDNState.STATUS, TITLE, MESSAGE, 0, UDNButton.CANCEL_BUTTON );
			} else {
				udMulti.setAllowCancel( true );
				udMulti.displayMessage( TITLE, MESSAGE, 0 );
			}
		}
	}

	/**
	 * Testmethode
	 * @param args
	 */
	public static void main( String args[] ) {
		//		ReadRDCHandheldCorghi_1_1_F_Pruefprozedur pp = new ReadRDCHandheldCorghi_1_1_F_Pruefprozedur();
		//		Enumeration<CommPortIdentifier> en = CommPortIdentifier.getPortIdentifiers();
		//	    while(en.hasMoreElements())
		//	    {
		//	    	CommPortIdentifier ident =en.nextElement();
		//	        System.out.println( ident.getName() );
		//
		//	    }
	}
}

/*
 * UiLogStart
 * created 17.06.2005
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;

import com.bmw.cascade.*;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceIOException;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.*;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Schaltet den Transientenmodus auf ContinuousMode
 * @author Gehring Dirk, TI-431
 * @version V0_1_0_F, 06.06.2005, Ersterstellung
 * Momentan nicht �bersetzbar, da die entsprechenden Methoden noch nicht im UIAnalyser implementiert sind.
 */
public class UiLogStart_6_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	private boolean DEBUG = false;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public UiLogStart_6_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling
	 *            Klasse des zugeh. Pr�flings
	 * @param pruefprozName
	 *            Name der Pr�fprozedur
	 * @param hasToBeExecuted
	 *            Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public UiLogStart_6_0_F_Pruefprozedur(Pruefling pruefling,
			String pruefprozName, Boolean hasToBeExecuted) {
		super(pruefling, pruefprozName, hasToBeExecuted);
		attributeInit();
	}
	
	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = {"MAX_LOG","DELETE_INTERVALL","AWT"};
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = new String[0];
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 */
	public boolean checkArgs() {
		String temp;
		int check;
		boolean ok;
		try {
			ok = super.checkArgs();
			//MAX_LOG, optionaler Parameter der die maximale Aufnahmezeit in sec. darstellt
			temp = getArg(getOptionalArgs()[0]);
			if(temp!=null){
				if( DEBUG ) {
					System.out.println( "UiLogStart: MAX_LOG: " + temp + "\n" );
				}
				try{
					check=Integer.parseInt(temp);
					//Falls MAX_LOG Kleiner als 0 ist -> ParametrierFehler
					if(check<0){
						return false;
					}
				}catch(NumberFormatException e){
					return false;
				}
			}
			//DELETE_INTERVALL
			temp=getArg(getOptionalArgs()[1]);
			//Falls DELETE_INTERVALL kleiner als 0 ist -> ParametrierFehler
			if (temp!=null){
				try{
					check = Integer.parseInt(temp);
					if (check < 0) {
						return false;
					}				
				}catch(NumberFormatException e){
					return false;
				}				
			}
			return ok;
		} catch (Exception e) {
			return false;
		} catch (Throwable e) {
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info
	 *            Information zur Ausf�hrung
	 */
	public void execute(ExecutionInfo info) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		//spezifische Variablen
		boolean ECOS_TRACE_CONTINUOUS=false;
		int max_log;
		int delete_intervall;
		String awt = null;
		UserDialog myDialog = null;
		UIAnalyserEcos myUI = null;
		String temp=null;		//Tempor�re Variable zur Initialisation der optionalen Parameter		
		boolean udInUse = false; //Ob eine Meldung auf dem Handterminal dargestellt wird

		if( DEBUG ) {
			System.out.println( "UiLogStart: Excecute start." + "\n" );
		}
		try {		
			try {//Auswerten des Argumente
				if (checkArgs() == false)
					throw new PPExecutionException(PB
							.getString("parameterexistenz"));
				//MAX_LOG
				temp=getArg(getOptionalArgs()[0]);
				if(temp!=null){
					if( DEBUG ) {
						System.out.println( "UiLogStart: MAX_LOG: " + temp + "\n" );
					}
					max_log=Integer.parseInt(temp);
				}
				else{
					//Default-Wert =600
					max_log=600;
				}
				//DELETE_INTERVALL
				temp=getArg(getOptionalArgs()[1]);
				if(temp!=null){
					delete_intervall=Integer.parseInt(temp);
				}
				else{
					//Default-Wert=10;
					delete_intervall=10;
				}
				//AWT
				temp = getArg(getOptionalArgs()[2]);
				if(temp!=null){
					awt = temp;
				}
				else{
					//Default-Wert="";
					awt = "";
				}
			} catch (PPExecutionException e) {
				if (e.getMessage() != null)
					result = new Ergebnis("ParaFehler", "", "", "", "", "", "",
							"", "", "0", "", "", "", PB
									.getString("parametrierfehler"), e
									.getMessage(), Ergebnis.FT_NIO_SYS);
				else
					result = new Ergebnis("ParaFehler", "", "", "", "", "", "",
							"", "", "0", "", "", "", PB
									.getString("parametrierfehler"), "",
							Ergebnis.FT_NIO_SYS);
				ergListe.add(result);
				throw new PPExecutionException();
			}//Ende Auswerten Argumente

			try {//Holen des Devices UIAnalyser
				myUI = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			} catch (Exception e) {
				//Systemfehler, muss man genau analysieren
				e.printStackTrace();
				if (e instanceof DeviceLockedException)
					result = new Ergebnis("ExecFehler", "UIAnalyser", "", "",
							"", "", "", "", "", "0", "", "", "",
							e.getMessage(), "DeviceLockedException",
							Ergebnis.FT_NIO_SYS);
				else if (e instanceof DeviceNotAvailableException)
					result = new Ergebnis("ExecFehler", "UIAnalyser", "", "",
							"", "", "", "", "", "0", "", "", "",
							e.getMessage(), "DeviceNotAvailableException",
							Ergebnis.FT_NIO_SYS);
				else
					result = new Ergebnis("ExecFehler", "UIAnalyser", "", "",
							"", "", "", "", "", "0", "", "", "",
							e.getMessage(), "Exception", Ergebnis.FT_NIO_SYS);
				ergListe.add(result);
				throw new PPExecutionException();
			}//Ende Holen des Devices UIAnalyser

			try {//Holen der Pr�fstandsvariablen TransientenModus
				ECOS_TRACE_CONTINUOUS =((Boolean)getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PS_CONFIG,"ECOS_TRACE_CONTINUOUS")).booleanValue();
			} catch (Exception e) {
				if (e instanceof VariablesException) {
					//Falls keine Pr�fstandsvariable angegeben ist, oder es tritt beim Abholen ein Fehler auf
					// TransientenModus inaktiv
					ECOS_TRACE_CONTINUOUS = false;
				} else {
					throw new PPExecutionException();
				}
			}//Ende Holen der Pr�fstandsvariablen

			if( DEBUG ) {
				System.out.println( "UiLogStart: ECOS_TRACE_CONTINUOUS:" + ECOS_TRACE_CONTINUOUS + "\n" );
			}
			//Auswerten der Pr�fstandsvariablen und Setzen des ContinuousMode
			if (ECOS_TRACE_CONTINUOUS) {
				try {
					//AWT wird auf dem Bildschirm dargestellt
					myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
					myDialog.displayMessage(PB.getString("anweisung"), awt, -1);
					udInUse = true;
				} catch (Exception e) {
					//Systemfehler, muss man genau analysieren
					e.printStackTrace();
					result = new Ergebnis("ExecFehler", "Userdialog", "", "",
							"", "", "", "", "", "0", "", "", "",
							e.getMessage(), PB
									.getString("unerwarteterLaufzeitfehler"),
							Ergebnis.FT_NIO_SYS);
					ergListe.add(result);
					throw new PPExecutionException("Get UserDialog");
				}
				myUI.SetContinuesMode(true);
/*				try {
					new File("..\\trace\\"+"ecos"+File.separator+
							cal.get(Calendar.YEAR)+//Jahr
							"_"+month+"_"+//Monat
							cal.get(Calendar.DAY_OF_MONTH)+File.separator).mkdirs();
				} catch(SecurityException e) {
					throw new DeviceIOException(e.getMessage());
				}
*/			    Date date = new Date();
		        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
		        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
				String FileName = CascadeProperties.getCascadeHome()+File.separator+
					"trace"+File.separator+
					"ecos"+File.separator+dateFormat.format(date)+File.separator+//Tag/Ordner
					getPr�fling().getAuftrag().getFahrgestellnummer7()+"_"+//FGNR
					dateTimeFormat.format(date);
				/*if( DEBUG ) {
					System.out.println( "Complete Path:" +  FileName);
				}*/

				myUI.StartTransientRecording(FileName,max_log);
				//Bsp f�r einen TransientenNamen im Verzeichnis ../cascade/trace/ecos/2005_06_17/
				//KX77331_20050611_131205.zip
			}
			else{//andernfalls definierten Zustand herstellen
				myUI.StopTransientRecording();
				myUI.SetContinuesMode(false);
			}//Ende Auswerten der Pruefstandsvariablen und Setzen des ContinuousMode
			
			//L�schen aller Ordner und aller Dateien im Verzeichnis ../cascade/trace/ecos, die �lter als DELETE_INTERVALL Tage sind
			//unter Benutzung der lastModified Methode
			if(delete_intervall>0){
				//Umrechnen der Tage in Millisekunden
				long millisekunden=delete_intervall*24*60*60*1000;
				//Aktuelle Systemzeit holen
				long time=System.currentTimeMillis();
				File ElternVerzeichnis=new File(CascadeProperties.getCascadeHome()+File.separator+"trace"+File.separator+"ecos");
				if( DEBUG ) {
					//System.out.println( "UiLogStart: Eltern Verzeichniss, " +  ElternVerzeichnis.toString());
				}
				if(ElternVerzeichnis.isDirectory()){
					File[] Kinder =ElternVerzeichnis.listFiles();					
					if( DEBUG ) {
						System.out.println( "UiLogStart: delete in " +  ElternVerzeichnis.toString());
					}
					for(int i=0;i<Kinder.length-1;i++){
						long lmf=Kinder[i].lastModified();
						if((time-lmf)>=millisekunden){
							File[] EnkelKinder =Kinder[i].listFiles();					
							for(int j=0;j<EnkelKinder.length;j++){
								EnkelKinder[j].delete();
								if( DEBUG ) {
									System.out.println( "UiLogStart: delete " +  EnkelKinder[j].toString());
								}
							}	
							Kinder[i].delete();
							if( DEBUG ) {
								System.out.println( "UiLogStart: delete " +  Kinder[i].toString());
							}
						}	
					}
					if( DEBUG ) {
						System.out.println( "UiLogStart: delete finished." );
					}
				}
			}

			
		} catch (PPExecutionException e) {
			status = STATUS_EXECUTION_ERROR;
		} catch (Exception e) {
			//Systemfehler, muss man genau analysieren
			e.printStackTrace();
			result = new Ergebnis("ExecFehler", "", "", "", "", "", "", "", "",
					"0", "", "", "", e.getMessage(), PB
							.getString("unerwarteterLaufzeitfehler"),
					Ergebnis.FT_NIO_SYS);
			ergListe.add(result);
			status = STATUS_EXECUTION_ERROR;
		} catch (Throwable e) {
			//Systemfehler, muss man genau analysieren
			e.printStackTrace();
			result = new Ergebnis("ExecFehler", "", "", "", "", "", "", "", "",
					"0", "", "", "", e.getMessage(), PB
							.getString("unerwarteterLaufzeitfehler"),
					Ergebnis.FT_NIO_SYS);
			ergListe.add(result);
			status = STATUS_EXECUTION_ERROR;
		}

		if(myUI != null){
			try {
				//Device UIAnalyser wird freigegeben
				getPr�flingLaufzeitUmgebung().getDeviceManager()
						.releaseUIAnalyserEcos();
			} catch (Exception e) {
				//Systemfehler, muss man genau analysieren
				e.printStackTrace();
				if (e instanceof DeviceLockedException)
					result = new Ergebnis("ExecFehler", "UIAnalyser", "", "", "",
							"", "", "", "", "0", "", "", "", e.getMessage(),
							"DeviceLockedException", Ergebnis.FT_NIO_SYS);
				else
					result = new Ergebnis("ExecFehler", "UIAnalyser", "", "", "",
							"", "", "", "", "0", "", "", "", e.getMessage(),
							"release", Ergebnis.FT_NIO_SYS);
				ergListe.add(result);
				status = STATUS_EXECUTION_ERROR;
			}
		}
		myUI = null;
        //UserDialog wird frei gegeben
		if (udInUse == true) {
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch (Exception e) {
				//Systemfehler, muss man genau analysieren
				e.printStackTrace();
				if (e instanceof DeviceLockedException)
					result = new Ergebnis("ExecFehler", "Userdialog", "", "",
							"", "", "", "", "", "0", "", "", "",
							e.getMessage(), "DeviceLockedException",
							Ergebnis.FT_NIO_SYS);
				else
					result = new Ergebnis("ExecFehler", "Userdialog", "", "",
							"", "", "", "", "", "0", "", "", "",
							e.getMessage(), "release", Ergebnis.FT_NIO_SYS);
				ergListe.add(result);
				status = STATUS_EXECUTION_ERROR;
			}
		}
		//Status setzen
		setPPStatus(info, status, ergListe);

	}//Ende execute

}

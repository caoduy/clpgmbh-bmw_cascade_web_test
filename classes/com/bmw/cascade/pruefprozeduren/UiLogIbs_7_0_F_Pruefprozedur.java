package com.bmw.cascade.pruefprozeduren;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;

/**
 * Spezialpr�fprozedur, die auf Basis eines referenzierten Pr�fschrittes und eines referenzierten
 * Grundwertes f�r den IBS Sensor, die dort ermittelten Daten verwendet und diese formatiert f�r das
 * Transiententool als Datei im Filesystem ablegt.<BR>
 * <BR>
 * Verwendung im Rahmen der EFII Pr�fung (IBS-Sensor ab L7) <BR>
 * <BR>
 * Hintergr�nde: EFII IBS Werte kommen als Wort (2 Byte) - Status- und Nutzinformation (Wert) Die
 * Statusinformation ist in den oberen 2 Bits des Wortes codiert, der Rest sind die Nutzinformation
 * Die Stromwerte aus dem IBS SG kommen in mA (17000 == 17 A) Einzelne Fileeintr�ge bestehen
 * Kurventyp und bis zu 4 weiteren Werterreihen T+ 0 17230 17230 17215 Kurventyp Spannungswert
 * Rohwerte Stromwerte Grundwert <BR>
 * <BR>
 * <BR>
 * --> ZUSATZINFO VON "Nils.Wilgen@hella.com" BEGIN a.) Stromwerte werden im "little endian" Format
 * (LSB zuerst) als "unsigned" 16 Bit �bertragen b.) Die obersten 2 Bit werden zur Kennung vom
 * Messfortschritt verwendet:
 * |--------------+--------------+---------------------------------------------| | BIT 15 | BIT 14 |
 * BEDEUTUNG | |--------------+--------------+---------------------------------------------| | 0 | 0
 * | Messwerte ung�ldig, Initialisierungswert |
 * |--------------+--------------+---------------------------------------------| | 0 | 1 | G�ldige
 * Messwerte, Transientenmesswerte |
 * |--------------+--------------+---------------------------------------------| | 1 | 0 | Daten
 * nach Trigger (Totzeit) |
 * |--------------+--------------+---------------------------------------------| | 1 | 1 | Daten
 * Messzeit | |--------------+--------------+---------------------------------------------| c.) Die
 * Aufl�sung der Stromwerte:
 * |--------------+------------------------------------------------------------| | Interface: | LIN
 * BMW element 154 | |--------------+------------------------------------------------------------| |
 * Name IBS: | linBmwEcosAcqBuf_au16 |
 * |--------------+------------------------------------------------------------| | Name ECU:
 * |Eco2_transi_trbuf |
 * |--------------+------------------------------------------------------------| | Description: |
 * Measurement buffer |
 * |--------------+------------------------------------------------------------| | Type: |Array of
 * unsigned 16 bit | |--------------+------------------------------------------------------------| |
 * Read/write: |write |
 * |--------------+------------------------------------------------------------| | Range: |0 � 160 A
 * | |--------------+------------------------------------------------------------| | Resolution: |10
 * mA | |--------------+------------------------------------------------------------| | Declared in:
 * | LIN_BMW_public.h |
 * |--------------+------------------------------------------------------------| Beipiel: 0x44; 0xC5
 * -> 0100 0100 1100 0101b - Messvorgang = 01 (g�ltige Messwerte) - Strom = 00 0100 1100 0101
 * (12,21A) <BR>
 * <-- ZUSATZINFO VON "Nils.Wilgen@hella.com" END <BR>
 * <BR>
 * <BR>
 * Created on 14.02.10<BR>
 * <BR>
 * Bei �nderung Hr. Buboltz TI-538 Tel. 35789 informieren!<BR>
 * <BR>
 * 
 * @author BMW TI-538 Thomas Buboltz (TB)<BR>
 * @version V_1_0_F 14.02.2010 TB Implementierung<BR>
 *          V_2_0_F 22.02.2010 TB Kleinere Bugfixes (Grundwert kommt als Double)<BR>
 *          V_3_0_F 23.04.2010 TB Anwenderwunsch: Ergebnisse aus dem virt. Fzg. auch dann verwenden,
 *          wenn vorausgegangener Pr�fschritt NIO war<BR>
 *          V_4_0_T 11.10.2010 AS Anwenderwunsch: neuer Parameter eingef�hrt "WRITE_ONE_FILE" -
 *          bestimmt ob f�r alle IBS Messungen eine Datei<BR>
 *          geschrieben wird und "IBS_STEP_NAME" gibt an welche PP genutzt wird um die Parameter zu
 *          erhalten<BR>
 *          Bugfix: eindeutige ID(Zeit in ms) im Dateinamen - wenn min. zwei Messungen in einer s
 *          gemacht werden, w�rden<BR>
 *          diese in eine Datei geschrieben werden -> �ffnen mit Transiententool nicht m�glich<BR>
 *          V_5_0_F 02.11.2010 F_VERSION
 *          V_6_0_T 21.07.2017 CW Zugriff mit Pr�fstands�bergreifenden @-Operator funktioniert nicht beim SMART-Server <BR>
 *          V_7_0_F 21.07.2017 CW Freigabe 6_0_T <BR>
 */
public class UiLogIbs_7_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public UiLogIbs_7_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh. Prueflings
	 * @param pruefprozName Name der Pruefprozedur
	 * @param hasToBeExecuted Ausfuehrbedingung f�r Fehlerfreiheit
	 */
	public UiLogIbs_7_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG", "WRITE_ONE_FILE", "IBS_STEP_NAME" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "IBS_RESULT_GRUNDWERT", "IBS_RESULT_TRANSIENT" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten
	 *         <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		if( !super.checkArgs() )
			return false;
		return true;
	}

	/**
	 * Hilfsfunktion f�r die Umwandlung High- / LowByte
	 * 
	 * @param highbyte
	 * @param lowbyte
	 * @return Ergebnis
	 */
	public int parseBytes( byte highbyte, byte lowbyte ) {
		int value = (highbyte << 8) | lowbyte;
		return value;
	}

	/**
	 * Byte in int wandeln
	 * 
	 * @param byte
	 * @return Ergebnis
	 */
	public int byteToInt( byte b ) {
		return ((int) b & 0x7f) + (b < 0 ? 0x80 : 0);
	}

	/**
	 * fuehrt die Pruefprozedur aus
	 * 
	 * @param info Information zur Ausfuehrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		double grundwert = 0;
		String datenpaket = null;

		final int MASK_VALUE = 0x3FFF;
		final int MASK_STATUS_BYTES = 0xC000;

		List ibsValues = new ArrayList();

		BufferedWriter myFileWriter = null;

		// Verzeichnis bei Bedarf anlegen und Dateiname zusammenbauen
		// Filename format: vin_datum_zeit_pruefschrittname.eco
		SimpleDateFormat dateDirFormat = new SimpleDateFormat( "yyyy_MM_dd" );
		SimpleDateFormat dateFileFormat = new SimpleDateFormat( "yyyyMMdd_HHmmssSSS" );
		Date date = new Date( System.currentTimeMillis() );

		String pathName = CascadeProperties.getCascadeHome() + File.separator + "trace" + File.separator + "ecos" + File.separator + dateDirFormat.format( date ) + File.separator;

		// IBS Testpacket f�r das Testen
		// final String IBS_TEST_PAKET_HEX = "" + "82408140814081408240814082408040" +
		// "82408140824081408140814081408140" + "81408240814081408140814081408140" +
		// "81408140814081408240814081408140" + "81408240814081408240854088408f40" +
		// "95409d40a640af40b440b440b440b340" + "b440b340b940c740d740da40da40d940" +
		// "d940d940d940e140ff40064106410641" + "05410541064112412241234123412341" +
		// "2341234124412b414441444145414441" + "45414541454144415781708170817081" +
		// "70c16fc170c170c17ac19cc1a341a441" + "a341a341a341a341a341a341a441a341" +
		// "a341b341c341c341c341c341c341c341" + "c341c341c341c341c341c341c341c341" +
		// "c341c341c341c441c341c241c341c341" + "c341c241c341c441c341c341c341c341" +
		// "c341c341c341c441c341c341c341c341" + "c341c341c341c341c341c341c341c341" +
		// "c341c341c341c341c341c341c341";

		boolean write_one_file = false; // gibt an, ob einzelne Dateien geschrieben werden sollen
		int ibs_step_number = getPr�fling().getAuftrag().getBearbeitungsInfo().size(); // gibt die
		// Anzahl
		// der
		// Bearbeitungen
		// zur�ck
		// oder eine gesamte
		try {
			// Parameter holen
			try {
				// Parameterpruefung
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG
				// Pr�fstandvariabel auslesen, ob Debug
				try {
					Object pr_var_debug;

					pr_var_debug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
					if( pr_var_debug != null ) {
						if( pr_var_debug instanceof String ) {
							if( "TRUE".equalsIgnoreCase( (String) pr_var_debug ) ) {
								bDebug = true;
							} else {
								bDebug = false;
							}
						} else if( pr_var_debug instanceof Boolean ) {
							if( ((Boolean) pr_var_debug).booleanValue() ) {
								bDebug = true;
							} else {
								bDebug = false;
							}
						}
					} else {
						bDebug = false;
					}

				} catch( VariablesException e ) {
					System.out.println( "FS-ANA: cannot read VariableModes.PSCONFIG, DEBUG, set Debug false" );
					bDebug = false;
				}

				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
				}
				// EINZEL
				if( getArg( "WRITE_ONE_FILE" ) != null ) {
					if( getArg( "WRITE_ONE_FILE" ).toUpperCase().equals( "TRUE" ) )
						write_one_file = true;
					else if( getArg( "WRITE_ONE_FILE" ).toUpperCase().equals( "FALSE" ) )
						write_one_file = false;
				}

				// IBS_RESULT_GRUNDWERT
				if( getArg( getRequiredArgs()[0] ).indexOf( '@' ) != -1 )
					grundwert = Double.parseDouble( getResultRedesigned( getArg( getRequiredArgs()[0] ) ).trim() );
				if( bDebug )
					System.out.println( "PP UiLogIbs: PARAMETERCHECK, Parameter: IBS_RESULT_GRUNDWERT=<" + grundwert + ">" );

				// IBS_RESULT_TRANSIENT
				if( getArg( getRequiredArgs()[1] ).indexOf( '@' ) != -1 ) {
					datenpaket = getResultRedesigned( getArg( getRequiredArgs()[1] ) ).trim().replaceAll( " ", "" );
					// TODO - Aktuell kommt ein Zeichen zuviel und zwar genau das erste -> entferne
					// dieses, wenn L�nge nicht durch 4 teilbar ist
					if( datenpaket.length() % 4 > 0 )
						datenpaket = datenpaket.substring( 2, datenpaket.length() );
				}
				if( bDebug )
					System.out.println( "PP UiLogIbs: PARAMETERCHECK, Parameter: IBS_RESULT_TRANSIENT=<" + datenpaket + ">" );

			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// PFAD FUER LOGDATEI PRUEFEN // ANLEGEN
			try {
				File path = new File( pathName );
				if( !path.exists() ) {
					boolean success = path.mkdirs();
					if( !success ) {
						result = new Ergebnis( "Fehler", "", "", "", "", "", "", "", "", "0", "", "", "", "LOG_Datei nicht gefunden", PB.getString( "logdateiNichtGefunden" ), Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						throw new PPExecutionException();
					}
				}
			} catch( SecurityException e ) {
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "Fehler", "", "", "", "", "", "", "", "", "0", "", "", "", "SecurityException", "SecurityException", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// KONVERTIERUNG DER DATEN
			if( datenpaket != null ) {
				if( bDebug ) {
					System.out.println( "PP UiLogIbs: KONVERTIERUNG, Daten von Pruefschritt=<" + getArg( getRequiredArgs()[1] ).trim() + ">, Rohdaten<" + datenpaket + ">" );
					System.out.println( "PP UiLogIbs: KONVERTIERUNG, Daten von Pruefschritt=<" + getArg( getRequiredArgs()[1] ).trim() + ">, Laenge der Daten in Zeichen=<" + datenpaket.length() + ">" );
				}

				if( datenpaket.length() > 0 && datenpaket.length() <= 960 ) { // Quelldaten sind max
					// 480 Bytes a 2
					// Zeichen !
					// last Status auf Dummy
					String lastStatus = "D+";

					// Punktenummer
					int pointNr = 0;

					for( int i = 0; i < datenpaket.length(); i += 4 ) {

						if( bDebug )
							System.out.println( "Schleife" + i );

						// Bytes kommen als Bytewerte (low- und dann high Byte) / Wandlung in
						// Intwerte, damit kein Overflow
						int firstByte = Integer.parseInt( datenpaket.substring( i, i + 2 ), 16 );
						int secondByte = Integer.parseInt( datenpaket.substring( i + 2, i + 4 ), 16 );

						int value = (firstByte << 8) | secondByte;

						if( value != MASK_VALUE ) {
							int statusBits = (value & MASK_STATUS_BYTES) >> 14;
							int resultvalue = value & MASK_VALUE;

							if( bDebug ) {
								System.out.println( "PP UiLogIbs: KONVERTIERUNG, Analysiere Word=<" + i / 2 + ">" );
								System.out.println( "PP UiLogIbs: KONVERTIERUNG, FirstByte=<" + firstByte + ">" );
								System.out.println( "PP UiLogIbs: KONVERTIERUNG, SecondByte=<" + secondByte + ">" );
								System.out.println( "PP UiLogIbs: KONVERTIERUNG, Rohwert=<" + value + ">" );
								System.out.println( "PP UiLogIbs: KONVERTIERUNG, Statusbits=<" + statusBits + ">" );
								System.out.println( "PP UiLogIbs: KONVERTIERUNG, Resultwert=<" + resultvalue + ">\n\r" );
							}

							String point = "";
							switch( statusBits ) {
								case 0:
									// D+ == gelbe Kurve [yellow]
									point = "D+" + "\t0\t" + resultvalue * 10 + "\t" + resultvalue * 10;
									ibsValues.add( point );

									// erh�he Nr
									pointNr++;
									break;

								case 1:
									// T+ == rosa Kurve [pink]
									point = "T+" + "\t0\t" + resultvalue * 10 + "\t" + resultvalue * 10;
									ibsValues.add( point );

									// erh�he Nr
									pointNr++;
									break;

								case 2:
									// H+ == violette Kurve [purple]
									point = "H+" + "\t0\t" + resultvalue * 10 + "\t" + resultvalue * 10;

									// Aktueller Wert violett und letzter Wert rosa?
									if( lastStatus.startsWith( "T+" ) )
										// Grundwert bei einem Punkt vor dem Triggerzeitpunkt
										// schreiben, sonst wird er nicht geschrieben
										ibsValues.set( pointNr - 1, (String) ibsValues.get( pointNr - 1 ) + "\t" + (int) (grundwert * 1000) ); // Grundwert
									// Bsp.:
									// 17.12
									// A
									// ->
									// 17120
									// mA

									ibsValues.add( point );

									// erh�he Nr
									pointNr++;
									break;

								case 3:
									// M+ == schwarze Kurve [black]
									point = "M+" + "\t0\t" + resultvalue * 10 + "\t" + resultvalue * 10;
									ibsValues.add( point );

									// erh�he Nr
									pointNr++;
									break;
							}
							lastStatus = point.substring( 0, 2 );
						}

						else {
							// Wenn Overflow, dann tue nichts
						}
					}

					// Debug
					if( bDebug ) {
						System.out.println( "PP UiLogIbs: KONVERTIERUNG, IBS Values BEGIN<" );
						for( int k = 0; k < ibsValues.size(); k++ ) {
							String listPoint = (String) ibsValues.get( k );
							System.out.println( "   " + listPoint );
						}
						System.out.println( "> END" );
					}

				} else {
					// IBS data package error (should be exact 490 bytes)
					if( bDebug )
						System.out.println( "PP UiLogIbs: KONVERTIERUNG, Laenge der IBS Daten passt nicht" );
				}
			} else {
				// IBS data not found in Result
				if( bDebug )
					System.out.println( "PP UiLogIbs: KONVERTIERUNG, Keine IBS Daten in Pruefschritt=<" + getArg( getRequiredArgs()[1] ).trim() + ">, IBS Rohdaten=<" + datenpaket + ">" );
			}

			// SCHREIBEN DER IBS Files
			try {
				String fileName;
				boolean new_file = false;
				boolean write_parameter;
				String[] arr = null;

				if( datenpaket != null ) {
					String testStep = getArg( getRequiredArgs()[1] ).trim();

					if( testStep.indexOf( "@" ) >= 0 )
						testStep = testStep.substring( testStep.indexOf( "@" ) + 1, testStep.length() );

					if( testStep.indexOf( "." ) >= 0 )
						testStep = testStep.substring( testStep.indexOf( "." ) + 1, testStep.length() );

					if( !write_one_file ) {
						fileName = pathName + this.getPr�fling().getAuftrag().getFahrgestellnummer7() + "_" + dateFileFormat.format( date ) + "_" + testStep + "_" + "IBS" + ".eco";
						new_file = true;
					} else {
						fileName = pathName + this.getPr�fling().getAuftrag().getFahrgestellnummer7() + "_" + ibs_step_number + "_" + "IBS" + ".eco";
						try {
							new FileInputStream( fileName );
						} catch( FileNotFoundException e_fnf ) {
							new_file = true;
						}
					}

					myFileWriter = new BufferedWriter( new FileWriter( fileName, true ) );

					if( new_file ) {
						myFileWriter.write( "TR_V2\n" );
					}

					if( getArg( "IBS_STEP_NAME" ) != null ) {
						try {
							arr = getParameter( getArg( "IBS_STEP_NAME" ) );
							write_parameter = true;
						} catch( PPExecutionException e ) {
							result = new Ergebnis( "Parametrierfehler", "", "", "", "", "", "", "", "", "0", "", "IBS_STEP_NAME fehlerhaft: " + getArg( "IBS_STEP_NAME" ), "", e.getMessage(), PB.getString( "ibsnamenichtgefunden" ), Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							write_parameter = false;
						}
						if( write_parameter ) {
							myFileWriter.write( "Name: " + testStep + "\n" );
							myFileWriter.write( "ZEIT = " + new SimpleDateFormat( "yyyy-MM-dd kk:mm:ss.SSS" ).format( new Date() ) + "\n" );
							myFileWriter.write( "REF = " + getArg( "IBS_STEP_NAME" ) + "\n" );

							if( arr.length == 11 ) {

								myFileWriter.write( "STARTTRIGGER = " + arr[0] + "mA" + "\n" );
								myFileWriter.write( "AUSSCHALTTRIGGER = " + arr[1] + "mA" + "\n" );
								myFileWriter.write( "TOT-ZEIT = " + arr[2] + "ms" + "\n" );
								myFileWriter.write( "MESSZEIT = " + arr[3] + "ms" + "\n" );
								myFileWriter.write( "I-MIN = " + arr[4] + "mA" + "\n" );
								myFileWriter.write( "I-MAX = " + arr[5] + "mA" + "\n" );
								myFileWriter.write( "MESSPUNKTE = " + arr[6] + "\n" );
								myFileWriter.write( "TRIGGERFILTER = " + arr[7] + "ms" + "\n" );
								myFileWriter.write( "MESSFILTER = " + arr[8] + "ms" + "\n" );
								myFileWriter.write( "TIMEOUT = " + arr[9] + "s" + "\n" );
								myFileWriter.write( "POSTTRIGGER = " + arr[10] + "ms" + "\n" );

							} else {

								myFileWriter.write( " JOBPAR = " + arr[0] + "\n" );

							}
							myFileWriter.write( "Ende" + "\n" );
						} else if( write_one_file ) {
							myFileWriter.write( "Name: " + testStep + "\n" );
							myFileWriter.write( "Ende" + "\n" );
						}
					} else if( write_one_file ) {
						myFileWriter.write( "Name: " + testStep + "\n" );
						myFileWriter.write( "Ende" + "\n" );
					}

					// Werte ins File schreiben
					for( int i = 0; i < ibsValues.size(); i++ ) {
						String listPoint = (String) ibsValues.get( i );
						myFileWriter.write( listPoint + "\n" );
					}
				}

			} catch( IOException e ) {
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "IO Fehler", "", "", "", "", "", "", "", "", "0", "", "", "", "IOException", "IOExcepiton", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} finally {
				if( myFileWriter != null ) {
					try {
						myFileWriter.close();
					} catch( IOException e ) {
						if( bDebug )
							e.printStackTrace();

						throw new PPExecutionException();
					}
				}
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "UiLogIbs", "", "", "", "", "STATUS", "OKAY", "IBS Step Number: " + ibs_step_number, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}

		setPPStatus( info, status, ergListe );
	}

	/**
	 * Liefert den Ergebniswert einer Pr�fprozedur eines Pr�flings Originalmethode in der
	 * AbstractPruefprozedur funktioniert nicht, wenn der Ergebnisstatus nicht STATUS_EXECUTION_OK
	 * war
	 * 
	 * @return der Wert des Ergebnisses
	 * @param resultName Name des Ergebnisses entsprechend der Form
	 *            'ergname@pruefprozedur(.pruefling)'.
	 * @throws PPExecutionException falls Result nicht verf�gbar bzw. nicht verwertbar ist.
	 */
	private String getResultRedesigned( String resultName ) throws PPExecutionException {

		Pruefprozedur pp;
		String temp;
		String resultValue;
		int idx;

		resultName = resultName.toUpperCase();

		// Formatcheck
		if( (idx = resultName.indexOf( '@' )) < 0 ) {
			throw new PPExecutionException( "Token '@' in " + resultName + " nicht enthalten" );
		}
		temp = resultName.substring( idx + 1 );
		if( temp.indexOf( "." ) < 0 ) {
			// Kein Punkt somit Suche in eigenen Pr�fling ...
			pp = getPr�fling().getPr�fprozedur( temp );
		} else if( temp.indexOf( "." ) == temp.lastIndexOf( "." ) ) {
			// Ein Punkt enthalten, Suche in einem anderen Pr�fling �ber den Auftrag...
			pp = getPr�fling().getAuftrag().getPr�fprozedur( temp );
		} else {
			// 2 oder mehr Punkte, somit Fehler
			throw new PPExecutionException( "Punktnotation in " + resultName + " fehlerhaft" );
		}
		if( pp == null ) {
			throw new PPExecutionException( "Pr�fprozedur " + temp + " nicht vorhanden" );
		}
		if( pp.isValid() == false )
			throw new PPExecutionException( "Status der Pr�fprozedur " + temp + " nicht verwendbar" );

		// Jetzt das Ergebnis holen
		idx = resultName.indexOf( '@' );
		temp = resultName.substring( 0, idx ).trim();

		try {
			//resultValue = pp.getHistoryListe().getLatestErgebnis( temp ).getErgebnisWert();
			resultValue = ((AbstractBMWPruefprozedur)pp).getPPResult( resultName );
		} catch( NullPointerException e ) {
			throw new PPExecutionException( "Result " + resultName + " nicht vorhanden" );
		} catch( InformationNotAvailableException e ) {
			throw new PPExecutionException( "Result " + resultName + " nicht vorhanden" );
		} catch (Exception e){
			throw new PPExecutionException( "Result " + resultName + " nicht vorhanden" );
		}

		if( resultValue == null ) {
			throw new PPExecutionException( "Result " + resultName + " nicht vorhanden" );
		}
		return resultValue;
	}

	/**
	 * * Liefert die Parametrierung einer Pr�fprozedur eines Pr�flings
	 * 
	 * @return die Parameter als Liste
	 * @param resultName Name des Ergebnisses entsprechend der Form pruefprozedur(.pruefling)
	 * @throws PPExecutionException falls Result nicht verf�gbar bzw. nicht verwertbar ist.
	 */
	private String[] getParameter( String resultName ) throws PPExecutionException {

		Pruefprozedur pp;
		String temp = "";
		String[] resultValue = null;

		resultName = resultName.toUpperCase();

		temp = resultName;
		if( temp.indexOf( "." ) < 0 ) {
			// Kein Punkt somit Suche in eigenen Pr�fling ...
			pp = getPr�fling().getPr�fprozedur( temp );
		} else if( temp.indexOf( "." ) == temp.lastIndexOf( "." ) ) {
			// Ein Punkt enthalten, Suche in einem anderen Pr�fling �ber den Auftrag...
			pp = getPr�fling().getAuftrag().getPr�fprozedur( temp );
		} else {
			// 2 oder mehr Punkte, somit Fehler
			throw new PPExecutionException( "Punktnotation in " + resultName + " fehlerhaft" );
		}

		if( !(pp != null && pp.isValid()) ) {
			throw new PPExecutionException( "Status der Pr�fprozedur " + temp + " nicht verwendbar" );
		}

		try {
			// optionale Parameter werden geholt
			String[] arr_optional = pp.getOptionalArgs();

			String parameterString = "";

			for( int k = 0; k < arr_optional.length; k++ ) {
				try {
					if( (arr_optional[k].equals( "JOBPAR" )) && (!pp.getArg( arr_optional[k] ).equals( null )) ) {

						parameterString = (pp.getArg( arr_optional[k] ));
					}

				} catch( NullPointerException e ) {

				}
			}

			if( parameterString == null ) {
				throw new PPExecutionException( "Result " + resultName + " nicht vorhanden" );
			}

			resultValue = parameterString.split( ";" );

			if( resultValue.length == 11 ) {

				return resultValue;

			} else {

				return new String[] { parameterString };

			}

		} catch( PPExecutionException e ) {
			throw new PPExecutionException( "Result " + resultName + " nicht vorhanden" );
		}

	}
}

package com.bmw.cascade.pruefprozeduren;

import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.devices.DeviceExecutionException;
import com.bmw.cascade.devices.DynamicDevice;
import com.bmw.cascade.devices.DynamicDeviceManager;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.server.PB;
import com.bmw.cascade.util.dynamicdevices.PropertyObject;

/**
 * Implementation of general LIN communication
 * 
 * @author Torsten Mager, GEFASOFT Engineering GmbH, BMW TI-545
 * @version V1_0_F 03.11.2014 TM First implementation
 * @version V2_0_F 12.01.2015 TM Added TOEHKI test
 * @version V3_0_T 11.02.2015 TM Added window lifter button test
 * @version V4_0_F 17.02.2015 TM Changed error from system to normal for cancelling and measure errors
 * @version V5_0_F 27.02.2015 TM Added answer '2' to window lifter button test
 * @version V6_0_F 25.03.2015 TM Added SZT's window lifter button test
 * @version V7_0_T 16.04.2015 TM Added SZT's passenger window lifter button test
 * @version V8_0_F 20.05.2015 TM T version
 * @version V9_0_F 18.06.2015 TM Changed toehkiTimeout to windowLifterButtonTimeout for window lifter button LIN request
 * @version V10_0_F 18.06.2015 TM Changed response value for passenger's window lifter button at SZT from 195/196 to 3/4
 * @version V11_0_F 08.07.2015 TM Added user dialogue variable; Added 'user dialogue active' check to user dialogue cancelling request
 * @version V12_0_T 04.08.2015 TM T version
 * @version V13_0_F 07.08.2015 TM F version
 */
public class MPS3DynamicLinGen_13_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	static final long serialVersionUID = 1L;
	
	private UserDialog userdialog;
	
	/**
	 * Default constructor, only for de-seriaslisation
	 */
	public MPS3DynamicLinGen_13_0_F_Pruefprozedur() {
		
	}
	
	/**
	 * Creates a new test procedure
	 * 
	 * @param pruefling			Class of related Pruefling
	 * @param pruefprozName		Name of test procedure
	 * @param hasToBeExecuted	Execution condition for absence of errors
	 */
	public MPS3DynamicLinGen_13_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
		super(pruefling, pruefprozName, hasToBeExecuted);
		attributeInit();
	}
	
	/**
	 * Initialises arguments
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Provides optional arguments
	 */
	public String[] getOptionalArgs() {
		String[] args = {/*"DEBUG",*/"CURR_MIN", "CURR_MAX", "APDM_CUSTOM_ERRORTEXT", "APDM_TEST_DESCRIPTION", "AWT_TEXT", "AWT_TITLE", "AWT_STYLE", "AWT_TIME", "AWT_CANCEL", "COMPONENT",
								"TOEHKI", "TOEHKI_TIMEOUT", "WINDOW_LIFTER_BUTTON", "WINDOW_LIFTER_BUTTON_TIMEOUT","SZT", "SZT_TIMEOUT"};
		
		return args;
	}
	
	/**
	 * Provides mandatory arguments
	 */
	public String[] getRequiredArgs() {
		String[] args = {"TAG", "MKHWID", "CONTROL_MKHWID", "TIMEOUT", "PAUSE", "DELAY_BEFORE", "DELAY_AFTER", "CURR_LIMIT", "CURR_DELAY", "CURR_TIME", "REPETITIONS", "BAUDRATE", "FORMAT", "TERMINATION", "MESSAGE_ACTION",
								"MESSAGE_ID", "MESSAGE_DLC", "MESSAGE_DATA1", "MESSAGE_DATA2", "MESSAGE_DATA3", "MESSAGE_DATA4", "MESSAGE_DATA5", "MESSAGE_DATA6", "MESSAGE_DATA7", "MESSAGE_DATA8"};
		
		return args;
	}
	
	/**
	 * Checks arguments regarding existence and value, as far as possible
	 */
	public boolean checkArgs() {
		boolean ok;
		
		try {
			ok = super.checkArgs();
			return ok;
		} catch (Exception e) {
			return false;
		} catch (Throwable e)
		{
			return false;
		}
	}
	
	/**
	 * Executes test procedure
	 * 
	 * @param info	Information for execution
	 */
	public void execute(ExecutionInfo info) {
		DynamicDeviceManager ddm = null;
		DynamicDevice cardDevice = null;
		
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_ERROR;
		
		//TODO: remove isDe or rebuild
		boolean isDe = CascadeProperties.getLanguage().equalsIgnoreCase("DE");
		
		Logger mps3Logger = null;
		
		String tag				= null;
		String mkhwid			= null;
		boolean controlMKHWID	= false;
		int timeout				= 0;
		int pause				= 0;
		int delayBefore			= 0;
		int delayAfter			= 0;
		int currLimit			= 0;
		int currDelay			= 0;
		int currTime			= 0;
		int repetitions			= 0;
		int baudrate			= 0;
		int format				= 0;
		boolean termination		= false;
		int messageAction		= 0;
		int messageID			= 0;
		int messageDLC			= 0;
		int messageData1		= 0;
		int messageData2		= 0;
		int messageData3		= 0;
		int messageData4		= 0;
		int messageData5		= 0;
		int messageData6		= 0;
		int messageData7		= 0;
		int messageData8		= 0;
		int[] message			= new int[10];
		String messageAsString	= new String();
		int currMin				= 0;
		int currMax				= 0;
		String compText			= null;
		boolean toehki			= false;
		int toehkiTimeout		= 0;
		boolean windLiftButt	= false;
		int windLiftButtTimeout	= 0;
		boolean szt				= false;
		int sztTimeout			= 0;
		String awt_text			= "";
		String awt_title		= "";
		int awt_style			= 0;
		int awt_time			= 0;
		boolean awt_cancel		= false;
		//int debug				= 0;
		
		/*
		int quantity = 0;
		int[] byteIndex;
		int[] byteMask;
		int[] byteValue;
		boolean[] byteInvert;
		*/
		
		//Default
		int msgID	= 0x0014;
		int action	= 0x0001;
		
		try {
			//Required
			tag = getArg("TAG").trim().toUpperCase();
			mkhwid = getArg("MKHWID").trim().toUpperCase();
			controlMKHWID = ((Boolean) getValidParameterWithDefault("CONTROL_MKHWID", false));
			timeout = Integer.parseInt(getArg("TIMEOUT").trim().toUpperCase());
			pause = Integer.parseInt(getArg("PAUSE").trim().toUpperCase());
			delayBefore = Integer.parseInt(getArg("DELAY_BEFORE").trim().toUpperCase());;
			delayAfter = Integer.parseInt(getArg("DELAY_AFTER").trim().toUpperCase());;
			repetitions = Integer.parseInt(getArg("REPETITIONS").trim().toUpperCase());
			baudrate = Integer.parseInt(getArg("BAUDRATE").trim().toUpperCase());
			String fmt = getArg("FORMAT").trim().toUpperCase();
			if (fmt.equalsIgnoreCase("1.3")) {
				format = 0;
			} else if (fmt.equalsIgnoreCase("2.0")) {
				format = 1;
			} else if (fmt.equalsIgnoreCase("2.1")) {
				format = 2;
			}
			termination = ((Boolean) getValidParameterWithDefault("TERMINATION", false));
			
			if (getArg("MESSAGE_ACTION").equalsIgnoreCase("CTRL")) {
				messageAction = 0x0015;
			} else {
				messageAction = 0x0017;
			}
			messageID = Integer.parseInt(getArg("MESSAGE_ID").trim().toUpperCase());
			messageDLC = Integer.parseInt(getArg("MESSAGE_DLC").trim().toUpperCase());
			messageData1 = Integer.parseInt(getArg("MESSAGE_DATA1").trim().toUpperCase());
			messageData2 = Integer.parseInt(getArg("MESSAGE_DATA2").trim().toUpperCase());
			messageData3 = Integer.parseInt(getArg("MESSAGE_DATA3").trim().toUpperCase());
			messageData4 = Integer.parseInt(getArg("MESSAGE_DATA4").trim().toUpperCase());
			messageData5 = Integer.parseInt(getArg("MESSAGE_DATA5").trim().toUpperCase());
			messageData6 = Integer.parseInt(getArg("MESSAGE_DATA6").trim().toUpperCase());
			messageData7 = Integer.parseInt(getArg("MESSAGE_DATA7").trim().toUpperCase());
			messageData8 = Integer.parseInt(getArg("MESSAGE_DATA8").trim().toUpperCase());
			
			message[0] = messageID;
			message[1] = messageDLC;
			message[2] = messageData1;
			message[3] = messageData2;
			message[4] = messageData3;
			message[5] = messageData4;
			message[6] = messageData5;
			message[7] = messageData6;
			message[8] = messageData7;
			message[9] = messageData8;
			
			for (int i=0; i<10; i++) {
				if (i == 0) {
					messageAsString = ((Integer)message[i]).toString();
				} else {
					messageAsString += " | " + ((Integer)message[i]).toString();
				}
			}
			
			//Optional
			currLimit = ((Integer) getValidParameterWithDefault("CURR_LIMIT", (int) 3000)).intValue();
			currDelay = ((Integer) getValidParameterWithDefault("CURR_DELAY", (int) 0)).intValue();
			currTime = ((Integer) getValidParameterWithDefault("CURR_TIME", (int) 500)).intValue();
			currMin = ((Integer) getValidParameterWithDefault("CURR_MIN", (int) 0)).intValue();
			if (currMin < 0)
				currMin = 0;
			else if (currMin > 25000)
				currMin = 25000;
			currMax = ((Integer) getValidParameterWithDefault("CURR_MAX", (int) 25000)).intValue();
			if (currMax < 0)
				currMax = 0;
			else if (currMax > 25000)
				currMax = 25000;
			toehki = ((Boolean) getValidParameterWithDefault("TOEHKI", false));
			toehkiTimeout = ((Integer) getValidParameterWithDefault("TOEHKI_TIMEOUT", (int) 10000)).intValue();
			windLiftButt = ((Boolean) getValidParameterWithDefault("WINDOW_LIFTER_BUTTON", false));
			windLiftButtTimeout = ((Integer) getValidParameterWithDefault("WINDOW_LIFTER_BUTTON_TIMEOUT", (int) 10000)).intValue();
			szt = ((Boolean) getValidParameterWithDefault("SZT", false));
			sztTimeout = ((Integer) getValidParameterWithDefault("SZT_TIMEOUT", (int) 10000)).intValue();
			
			/*
			debug = ((Integer) getValidParameterWithDefault("DEBUG", (int) -1)).intValue();
			if (debug == -1) {
				if (((Boolean) getValidParameterWithDefault("DEBUG", false)).booleanValue())
					debug = 1;
				} else {
					debug = 0;
				}
			}
			*/
			
			//Deactivated
			/*
			for (int i=1; i<=4; i++) {
				if (getArg("BYTE_INDEX_" + Integer.toString(i)) != null) {
					quantity++;
				} else {
					break;
				}
			}
			
			byteIndex = new int [quantity];
			byteMask = new int [quantity];
			byteValue = new int [quantity];
			byteInvert = new boolean [quantity];
			
			for (int i=0; i<quantity; i++) {
				byteIndex[i] = ((Integer) getValidParameterWithDefault("BYTE_INDEX_" + Integer.toString(i + 1), (int) -1)).intValue();
				byteMask[i] = ((Integer) getValidParameterWithDefault("BYTE_MASK_" + Integer.toString(i + 1), (int) -1)).intValue();
				byteValue[i] = ((Integer) getValidParameterWithDefault("BYTE_VALUE_" + Integer.toString(i + 1), (int) -1)).intValue();
				byteInvert[i] = ((Boolean) getValidParameterWithDefault("BYTE_INVERT_" + Integer.toString(i + 1), false));
			}
			*/
			
			awt_text = (String) getValidParameter("AWT_TEXT", "");
			awt_title = (String) getValidParameter("AWT_TITLE", "");
			awt_style = ((Integer) getValidParameterWithDefault("AWT_STYLE", (int) 1)).intValue();
			awt_time = ((Integer) getValidParameterWithDefault("AWT_TIME", (int) -1)).intValue();
			awt_cancel = ((Boolean) getValidParameterWithDefault("AWT_CANCEL", false)).booleanValue();
			if (isDe) {
				compText = (String) getValidParameterWithDefault("COMPONENT", "Fehler w�hrend LIN-Kommunikation.");
			} else {
				compText = (String) getValidParameterWithDefault("COMPONENT", "Error during LIN communication.");
			}
		} catch (PPExecutionException e) {
			Ergebnis apdmErg = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
			apdmErg.setWerkzeug("MPS3, LinGenDevice");
			apdmErg.setFehlerTyp(Ergebnis.FT_NIO_SYS);
			apdmErg.setFehlerText(e.getMessage());
			ergListe.add(apdmErg);
			setPPStatus(info, status, ergListe);
			return;
		}
		
		ddm = getPr�flingLaufzeitUmgebung().getDynamicDeviceManager();
		try {
			cardDevice = ddm.requestDevice("MPS3", "LinGenDevice");
			if (cardDevice == null) {
				if (isDe) {
					throw new DeviceExecutionException("Kann Device nicht holen");
				} else {
					throw new DeviceExecutionException("Cannot get Device");
				}
			}
		} catch (Exception ex) {
			Ergebnis apdmErg = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
			apdmErg.setWerkzeug("MPS3, LinGenDevice");
			apdmErg.setFehlerTyp(Ergebnis.FT_NIO_SYS);
			apdmErg.setFehlerText(ex.getMessage());
			ergListe.add(apdmErg);
			setPPStatus(info, status, ergListe);
			return;
		}
		
		Set<String> methods = cardDevice.getMethodNames();
		if (!methods.contains("connect") || !methods.contains("communicate") || !methods.contains("closeChannel") || !methods.contains("getLogger")) {
			String err = null;
			
			if (isDe) {
				err = "Devicemethoden fehlen";
			} else {
				err = "Device doesn't contain all required methods";
			}
			
			Ergebnis apdmErg = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
			apdmErg.setWerkzeug("MPS3, LinGenDevice");
			apdmErg.setFehlerTyp(Ergebnis.FT_NIO_SYS);
			apdmErg.setFehlerText(err);
			ergListe.add(apdmErg);
			setPPStatus(info, status, ergListe);
			return;
		}
		
		String cardId = null;
		boolean resHasErrors;
		boolean isCancelled = false;
		List<PropertyObject> resErrors;
		int resCurrent;
		int resTime;
		int[] resMessage;
		String resMessageAsString = new String();
		boolean toehkiErr = false;
		boolean windLiftButtErr = false;
		boolean sztErr = false;
		
		userdialog = null;
		if ((awt_text != null) && (awt_text.length() > 0)) {
			try {
				if (getPr�flingLaufzeitUmgebung() != null) {
					userdialog = getPr�flingLaufzeitUmgebung().getUserDialog();
					if (userdialog != null) {
						userdialog.setAllowCancel(awt_cancel);
					}
				}
				
				if (awt_style == 99) {
					if (userdialog != null) {
						userdialog.displayMessage(awt_title, awt_text, awt_time);
					}
				} else if (awt_style == 1) {
					if (userdialog != null) {
						userdialog.displayStatusMessage(awt_title, awt_text, awt_time);
					}
				} else if (awt_style == 2) {
					if (userdialog != null) {
						userdialog.displayAlertMessage(awt_title, awt_text, awt_time);
					}
				} else {
					if (userdialog != null) {
						userdialog.displayUserMessage(awt_title, awt_text, awt_time);
					}
				}
			} catch (Exception e) {
				String err = null;
				
				if (isDe) {
					err = "Kann Userdialog nicht anzeigen";
				} else {
					err = "Cannot show userdialog";
				}
				
				Ergebnis apdmErg = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
				apdmErg.setWerkzeug("MPS3, LinGenDevice");
				apdmErg.setFehlerTyp(Ergebnis.FT_NIO_SYS);
				apdmErg.setFehlerText(err);
				ergListe.add(apdmErg);
				setPPStatus(info, status, ergListe);
				return;
			}
		}
		
		try {
			List<PropertyObject> result = null;
			String linGenChannel = null;
			
			//Get Logger
			result = cardDevice.execute("getLogger");
			mps3Logger = Logger.getLogger(result.get(0).asString());
			
			//Connect
			result = cardDevice.execute("connect", new PropertyObject(tag));
			linGenChannel = result.get(0).asString();
			mps3Logger.log(Level.INFO, "Card connected");
			
			//Communicate
			if (toehki) {
				boolean b_MeasurementRunning = true;
				long stopTime = System.currentTimeMillis() + toehkiTimeout;
				toehkiErr = true;
				
				while (b_MeasurementRunning) {
					result = cardDevice.execute("communicate", new PropertyObject(linGenChannel), new PropertyObject(msgID), new PropertyObject(action), new PropertyObject(mkhwid), new PropertyObject(controlMKHWID), new PropertyObject(timeout),
							new PropertyObject(pause), new PropertyObject(delayBefore), new PropertyObject(delayAfter), new PropertyObject(repetitions), new PropertyObject(baudrate), new PropertyObject(format),
							new PropertyObject(termination), new PropertyObject(messageAction), new PropertyObject(message), new PropertyObject(currLimit), new PropertyObject(currDelay), new PropertyObject(currTime),
							new PropertyObject(currMin), new PropertyObject(currMax));
					
					resMessage = PropertyObject.getValueFromList(result, "message").asIntArray();
					if ((((Integer)resMessage[4] & (1 << 6)) > 0) || (((Integer)resMessage[4] & (1 << 7)) > 0)) {
						toehkiErr = false;
						b_MeasurementRunning = false;
						resMessage = null;
						break;
					}
					
					//Stop measurement if cancelled
					if ((userdialog != null) && (userdialog.isCancelled())) {
						break;
					}
					
					//Warten, wenn Messung weiter laufen soll
					if ((toehkiTimeout > 0) && b_MeasurementRunning) {
						try {
							Thread.sleep(500);
						} catch (InterruptedException e) {
							;
						}
					}
					
					//Stop mit Timeout
					if ((toehkiTimeout > 0) && (System.currentTimeMillis() > stopTime)) {
						break;
					}
				}
			} else if (windLiftButt) {
				boolean b_MeasurementRunning = true;
				long stopTime = System.currentTimeMillis() + windLiftButtTimeout;
				windLiftButtErr = true;
				
				while (b_MeasurementRunning) {
					result = cardDevice.execute("communicate", new PropertyObject(linGenChannel), new PropertyObject(msgID), new PropertyObject(action), new PropertyObject(mkhwid), new PropertyObject(controlMKHWID), new PropertyObject(timeout),
							new PropertyObject(pause), new PropertyObject(delayBefore), new PropertyObject(delayAfter), new PropertyObject(repetitions), new PropertyObject(baudrate), new PropertyObject(format),
							new PropertyObject(termination), new PropertyObject(messageAction), new PropertyObject(message), new PropertyObject(currLimit), new PropertyObject(currDelay), new PropertyObject(currTime),
							new PropertyObject(currMin), new PropertyObject(currMax));
					
					resMessage = PropertyObject.getValueFromList(result, "message").asIntArray();
					if ((resMessage[7] == 2) || (resMessage[7] == 4)) {
						windLiftButtErr = false;
						b_MeasurementRunning = false;
						resMessage = null;
						break;
					}
					
					//Stop measurement if cancelled
					if ((userdialog != null) && (userdialog.isCancelled())) {
						break;
					}
					
					//Warten, wenn Messung weiter laufen soll
					if ((windLiftButtTimeout > 0) && b_MeasurementRunning) {
						try {
							Thread.sleep(500);
						} catch (InterruptedException e) {
							;
						}
					}
					
					//Stop mit Timeout
					if ((toehkiTimeout > 0) && (System.currentTimeMillis() > stopTime)) {
						break;
					}
				}
			} else if (szt) {
				boolean b_MeasurementRunning = true;
				long stopTime = System.currentTimeMillis() + sztTimeout;
				sztErr = true;
				
				while (b_MeasurementRunning) {
					result = cardDevice.execute("communicate", new PropertyObject(linGenChannel), new PropertyObject(msgID), new PropertyObject(action), new PropertyObject(mkhwid), new PropertyObject(controlMKHWID), new PropertyObject(timeout),
							new PropertyObject(pause), new PropertyObject(delayBefore), new PropertyObject(delayAfter), new PropertyObject(repetitions), new PropertyObject(baudrate), new PropertyObject(format),
							new PropertyObject(termination), new PropertyObject(messageAction), new PropertyObject(message), new PropertyObject(currLimit), new PropertyObject(currDelay), new PropertyObject(currTime),
							new PropertyObject(currMin), new PropertyObject(currMax));
					
					resMessage = PropertyObject.getValueFromList(result, "message").asIntArray();
					if ((resMessage[5] == 195) || (resMessage[5] == 196) || (resMessage[6] == 3) || (resMessage[6] == 4)) {
						sztErr = false;
						b_MeasurementRunning = false;
						resMessage = null;
						break;
					}
					
					//Stop measurement if cancelled
					if ((userdialog != null) && (userdialog.isCancelled())) {
						break;
					}
					
					//Warten, wenn Messung weiter laufen soll
					if ((sztTimeout > 0) && b_MeasurementRunning) {
						try {
							Thread.sleep(500);
						} catch (InterruptedException e) {
							;
						}
					}
					
					//Stop mit Timeout
					if ((sztTimeout > 0) && (System.currentTimeMillis() > stopTime)) {
						break;
					}
				}
			} else {
				result = cardDevice.execute("communicate", new PropertyObject(linGenChannel), new PropertyObject(msgID), new PropertyObject(action), new PropertyObject(mkhwid), new PropertyObject(controlMKHWID), new PropertyObject(timeout),
						new PropertyObject(pause), new PropertyObject(delayBefore), new PropertyObject(delayAfter), new PropertyObject(repetitions), new PropertyObject(baudrate), new PropertyObject(format),
						new PropertyObject(termination), new PropertyObject(messageAction), new PropertyObject(message), new PropertyObject(currLimit), new PropertyObject(currDelay), new PropertyObject(currTime),
						new PropertyObject(currMin), new PropertyObject(currMax));

			}
			
			//Close
			cardDevice.execute("closeChannel", new PropertyObject(tag));
			
			//Get results
			Ergebnis apdmRes;
			cardId = PropertyObject.getValueFromList(result, "cardId").asString();
			resHasErrors = PropertyObject.getValueFromList(result, "hasErrors").asBoolean();
			resErrors = PropertyObject.getValueFromList(result, "errors").asList();
			resCurrent = PropertyObject.getValueFromList(result, "current").asInt();
			resTime = PropertyObject.getValueFromList(result, "time").asInt();
			
			resMessage = PropertyObject.getValueFromList(result, "message").asIntArray();
			for (int i=0; i<resMessage.length; i++) {
				if (i == 0) {
					resMessageAsString = ((Integer)resMessage[i]).toString();
				} else {
					resMessageAsString += " | " + ((Integer)resMessage[i]).toString();
				}
			}
			
			if (resHasErrors) {
				for (int i=0; i<resErrors.size(); i++) {
					String err = resErrors.get(i).asString();
					mps3Logger.log(Level.WARNING, "MeasureError: " + err);
					
					if (isDe && (err.equalsIgnoreCase("Zeit�berschreitung beim Empfang der LIN-Antwort."))) {
						err = compText + ": " + err;
					} else if (err.equalsIgnoreCase("Timeout while receiving LIN answer.")) {
						err = compText + ": " + err;
					}
					
					apdmRes = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
					apdmRes.setWerkzeug("MPS3, LinGenDevice");
					apdmRes.setParameter1(cardId);
					apdmRes.setFehlerTyp(Ergebnis.FT_NIO);
					apdmRes.setFehlerText(err);
					//apdmRes.setHinweisText(compText);
					ergListe.add(apdmRes);
				}
			}
			
			if ((userdialog != null) && (userdialog.isCancelled())) {
				isCancelled = true;
				
				apdmRes = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
				apdmRes.setWerkzeug("MPS3, LinGenDevice");
				apdmRes.setParameter1(cardId);
				apdmRes.setFehlerTyp(Ergebnis.FT_NIO);
				if (isDe) {
					apdmRes.setFehlerText("Pr�fung wurde abgebrochen.");
				} else {
					apdmRes.setFehlerText("Test was cancelled.");
				}
				//apdmRes.setHinweisText(compText);
				ergListe.add(apdmRes);
				
				status = STATUS_EXECUTION_ABORTED_BY_USER;
			}
			
			//Evaluate
			if (!resHasErrors && !isCancelled) {
				boolean error = false;
				
				mps3Logger.log(Level.INFO, "Test finished. Evaluation tolerances.");
				
				//Time
				apdmRes = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
				apdmRes.setWerkzeug("MPS3, LinGenDevice");
				apdmRes.setParameter1(cardId);
				if (isDe) {
					apdmRes.setErgebnis("Gemessene Zeit (ms)");
				} else {
					apdmRes.setErgebnis("Measured time (ms)");
				}
				apdmRes.setErgebnisWert(Integer.toString(resTime));
				
				if (controlMKHWID) {
					if (resTime < 0) {
						error = true;
						apdmRes.setFehlerTyp(Ergebnis.FT_NIO);
						
						if (isDe) {
							apdmRes.setFehlerText(compText + ": Dauer der Lin-Kommunikation zu klein.");
						}
						else {
							apdmRes.setFehlerText(compText + ": Duration of Lin communication too small.");
						}
					} else if (resTime > timeout) {
						error = true;
						apdmRes.setFehlerTyp(Ergebnis.FT_NIO);
						
						if (isDe) {
							apdmRes.setFehlerText(compText + ": Dauer der Lin-Kommunikation zu gro�.");
						} else {
							apdmRes.setFehlerText(compText + ": Duration of Lin communication too large.");
						}
					} else {
						apdmRes.setFehlerTyp(Ergebnis.FT_IO);
					}
				} else {
					apdmRes.setFehlerTyp(Ergebnis.FT_IO);
				}
				
				//apdmRes.setFehlerTyp(Ergebnis.FT_IGNORE);
				ergListe.add(apdmRes);
				
				//Current
				apdmRes = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
				apdmRes.setWerkzeug("MPS3, LinGenDevice");
				apdmRes.setParameter1(cardId);
				if (isDe) {
					apdmRes.setErgebnis("Gemessener Strom (mA)");
				} else {
					apdmRes.setErgebnis("Measured current (mA)");
				}
				apdmRes.setErgebnisWert(Integer.toString(resCurrent));
				
				if (controlMKHWID) {
					if ((currMin >= 0) && (resCurrent < currMin)) {
						error = true;
						apdmRes.setFehlerTyp(Ergebnis.FT_NIO);
						
						if (isDe) {
							apdmRes.setFehlerText(compText + ": Gemessener Strom zu niedrig.");
						} else {
							apdmRes.setFehlerText(compText + ": Measured current too small.");
						}
					} else if ((currMax >= 0) && (resCurrent > currMax)) {
						error = true;
						apdmRes.setFehlerTyp(Ergebnis.FT_NIO);
						
						if (isDe) {
							apdmRes.setFehlerText(compText + ": Gemessener Strom zu gro�.");
						} else {
							apdmRes.setFehlerText(compText + ": Measured current too large.");
						}
					} else {
						apdmRes.setFehlerTyp(Ergebnis.FT_IO);
					}
				} else {
					apdmRes.setFehlerTyp(Ergebnis.FT_IO);
				}
				
				//apdmRes.setFehlerTyp(Ergebnis.FT_IGNORE);
				ergListe.add(apdmRes);
				
				//TODO: error texts -> array?!?
				
				//Response
				boolean err = false;
				apdmRes = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
				apdmRes.setWerkzeug("MPS3, LinGenDevice");
				apdmRes.setParameter1(cardId);
				apdmRes.setParameter2(messageAsString);
				if (isDe) {
					apdmRes.setErgebnis("Lin-Antwortnachricht");
				} else {
					apdmRes.setErgebnis("Lin-Answermessage");
				}
				apdmRes.setErgebnisWert(resMessageAsString);
				
				/*
				for (int i=0; i<quantity; i++) {
					//byteIndex[i] = ((Integer) getValidParameterWithDefault("BYTE_INDEX_" + Integer.toString(i + 1), (int) -1)).intValue();
					//byteMask[i] = ((Integer) getValidParameterWithDefault("BYTE_MASK" + Integer.toString(i + 1), (int) -1)).intValue();
					//byteValue[i] = ((Integer) getValidParameterWithDefault("BYTE_VALUE" + Integer.toString(i + 1), (int) -1)).intValue();
					//byteInvert[i] = ((Boolean) getValidParameterWithDefault("BYTE_INVERT" + Integer.toString(i + 1), false));
					
					if (byteInvert[i] == true) {
						mps3Logger.log(Level.INFO, "resMessage[byteIndex[i]: " + ((Integer)resMessage[byteIndex[i]]).toString() + " --- byteMask[i]: " + ((Integer)byteMask[i]).toString() + " --- byteValue[i]: " + ((Integer)byteValue[i]).toString());
						if ((resMessage[byteIndex[i] + 2] & byteMask[i]) != byteValue[i]) {
							if (!err) {
								err = true;
							}
							
							apdmRes.setFehlerTyp(Ergebnis.FT_NIO);
							
							if (isDe) {
								apdmRes.setFehlerText(compText + ": Der erwartete Antwortwert - " + ((Integer)byteValue[i]).toString() + " - passt nicht.");
							}	else {
								apdmRes.setFehlerText(compText + ": At least one expected answer value - " + ((Integer)byteValue[i]).toString() + " - does not match.");
							}
							
							break;
						}
					} else {
						mps3Logger.log(Level.INFO, "resMessage[byteIndex[i]: " + ((Integer)resMessage[byteIndex[i]]).toString() + " --- byteMask[i]: " + ((Integer)byteMask[i]).toString() + " --- byteValue[i]: " + ((Integer)byteValue[i]).toString());
						if ((resMessage[byteIndex[i] + 2] & byteMask[i]) == byteValue[i]) {
							if (!err) {
								err = true;
							}
							
							apdmRes.setFehlerTyp(Ergebnis.FT_NIO);
							
							if (isDe) {
								apdmRes.setFehlerText(compText + ": Der erwartete Antwortwert - " + ((Integer)byteValue[i]).toString() + " - passt nicht.");
							} else {
								apdmRes.setFehlerText(compText + ": At least one expected answer value - " + ((Integer)byteValue[i]).toString() + " - does not match.");
							}
							
							break;
						}
					}
				}
				*/
				
				if (toehki && toehkiErr) {
					if (!err) {
						err = true;
					}
					
					apdmRes.setFehlerTyp(Ergebnis.FT_NIO);
					
					if (isDe) {
						apdmRes.setFehlerText(compText + ": Der erwartete Antwortwert passt nicht.");
					} else {
						apdmRes.setFehlerText(compText + ": Expected answer value does not match.");
					}
				} else if (windLiftButt && windLiftButtErr) {
					if (!err) {
						err = true;
					}
					
					apdmRes.setFehlerTyp(Ergebnis.FT_NIO);
					
					if (isDe) {
						apdmRes.setFehlerText(compText + ": Der erwartete Antwortwert passt nicht.");
					} else {
						apdmRes.setFehlerText(compText + ": Expected answer value does not match.");
					}
				} else if (szt && sztErr) {
					if (!err) {
						err = true;
					}
					
					apdmRes.setFehlerTyp(Ergebnis.FT_NIO);
					
					if (isDe) {
						apdmRes.setFehlerText(compText + ": Der erwartete Antwortwert passt nicht.");
					} else {
						apdmRes.setFehlerText(compText + ": Expected answer value does not match.");
					}
				}
				
				if (err) {
					error = true;
				} else {
					apdmRes.setFehlerTyp(Ergebnis.FT_IO);
				}
				
				//apdmRes.setFehlerTyp(Ergebnis.FT_IGNORE);
				ergListe.add(apdmRes);
				
				if (!error) {
					status = STATUS_EXECUTION_OK;
				}
			}
		} catch (Exception e) {
			try {
				cardDevice.execute("closeChannel", new PropertyObject (tag));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			if (mps3Logger != null) {
				mps3Logger.log(Level.WARNING, "Caught Exeption: " + e.getMessage());
			}
			
			Ergebnis apdmErg = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
			apdmErg.setWerkzeug("MPS3, LinGenDevice");
			apdmErg.setFehlerTyp(Ergebnis.FT_NIO_SYS);
			ergListe.add(apdmErg);
		}
		
		try {
			if ((userdialog != null) && (userdialog.isShowing())) {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		setPPStatus(info, status, ergListe);
	}
	
	/**
	 * Checks for a valid parameter
	 * 
	 * @param name				Name of parameter
	 * @param referenceObject	Class of parameter
	 * 
	 * @throws PPExecutionException
	 */
	private boolean hasValidParameter(String name, Object referenceObject) throws PPExecutionException {
		String value = getArg(name);
		
		if ((value == null) || (value.length() == 0)) {
			return false;
		}
		
		value = value.trim().toUpperCase();
		
		//TODO: to be completed
		if (referenceObject.getClass().equals(String.class)) {
			return true;
		} else if (referenceObject.getClass().equals(Byte.class)) {
			try {
				Byte.parseByte(value);
			} catch (Exception e) {
				return false;
			}
			
			return true;
		} else if (referenceObject.getClass().equals(Integer.class)) {
			try {
				Integer.parseInt(value);
			} catch (Exception e) {
				return false;
			}
			
			return true;
		} else if (referenceObject.getClass().equals(Float.class)) {
			try {
				Float.parseFloat(value);
			} catch (Exception e) {
				return false;
			}
			
			return true;
		} else if (referenceObject.getClass().equals(Double.class)) {
			try {
				Double.parseDouble(value);
			} catch (Exception e) {
				return false;
			}
			
			return true;
		} else if (referenceObject.getClass().equals(Boolean.class)) {
			if (value.equals("T") || value.equals("TRUE") || value.equals("F") || value.equals("FALSE")) {
				return true;
			} else {
				return false;
			}
		} else {
			throw new PPExecutionException("Parameter: " + name + PB.getString("parameterexistenz") + ". (unknown data type)");
		}
	}
	
	/**
	 * Returns content of a valid parameter
	 * 
	 * @param name				Name of parameter
	 * @param referenceObject	Class of parameter
	 * 
	 * @return					Value of parameter
	 * @throws PPExecutionException
	 */
	private Object getValidParameter(String name, Object referenceObject) throws PPExecutionException {
		String value = getArg(name);
		
		if (!hasValidParameter (name, referenceObject)) {
			return null;
		}
		
		value = value.trim().toUpperCase();
		
		if (referenceObject.getClass().equals(String.class)) {
			return value;
		} else if (referenceObject.getClass().equals(Byte.class)) {
			try {
				return Byte.parseByte(value);
			} catch (Exception e) {
				return null;
			}
		} else if (referenceObject.getClass().equals(Integer.class)) {
			try {
				return Integer.parseInt(value);
			} catch (Exception e) {
				return null;
			}
		} else if (referenceObject.getClass().equals(Float.class)) {
			try {
				return Float.parseFloat(value);
			} catch (Exception e) {
				return null;
			}
		} else if (referenceObject.getClass().equals(Double.class)) {
			try {
				return Double.parseDouble(value);
			} catch (Exception e) {
				return null;
			}
		} else if (referenceObject.getClass().equals(Boolean.class)) {
			if (value.equals("T") || value.equals("TRUE")) {
				return true;
			} else if (value.equals("F") || value.equals("FALSE")) {
				return false;
			} else {
				return null;
			}
		} else {
			throw new PPExecutionException("Parameter: " + name + PB.getString("parameterexistenz") + ". (unknown data type)");
		}
	}
	
	/**
	 * Returns content of a valid parameter or default object, if no valid content found
	 * 
	 * @param name					Name of parameter
	 * @param referenceAndDefault	Class of reference parameter
	 * 
	 * @return						Value of parameter or default object
	 */
	private Object getValidParameterWithDefault(String name, Object referenceAndDefault) {
		Object returnValue = null;
		
		try {
			returnValue = getValidParameter(name, referenceAndDefault);
		} catch (PPExecutionException e) {
			;
		}
		
		if (returnValue == null) {
			returnValue = referenceAndDefault;
		}
		
		return returnValue;
	}
}

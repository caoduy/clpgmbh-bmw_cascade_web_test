/*
 * Ping_1_0_F_Pruefprozedur.java
 *
 * Created on 19.02.12
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;

/**
 * F�r Sonderabl�ufe bei den MotorSG muss eine weitere FS PP erstellt werden, die eine FS Verarbeitung / Analyse auf Basis einer White und Black Liste erlaubt. <BR>
 * Alle White FS m�ssen dabei mindestens als Teilmenge der FS anliegen, jedoch darf keiner aus der Black Liste anliegen. Tritt dieser Fall auf wird das Pr�fprozedur-Ergebnis IO, ansonsten NIO.<BR>
 * <BR>
 * Anforder: Rainer Scheurich
 * Nachtrag: 23.07.2013
 * Bei der white Liste soll es m�glich sein, mehrere indiziert anzulegen. Diese sollen mit ODER / OR Beziehung einflie�en. Die Black-Liste bleibt singul�r.<BR>
 * - PP soll nur f�r ein SG eingesetzt werden, keine funktionale Abfrage <BR>
 * - ist f�r alle Motor SG relevant, Bsp.: Motor N20 -> MEVD1724 <BR>
 * - Unterst�tzung durch FS-Assistent wird aktuell nicht ben�tigt <BR>
 * <BR>
 * @author Buboltz <BR>
 * @version 1_0_F	28.07.2013  TB Erstimplementierung <BR>
 * @version 2_0_F	01.08.2013  TB FS WhiteListe kann Teilmenge der aktiven FS Liste sein + wenn WhiteList nicht trifft und jedoch FS-Fehler anliegen dann dies ebenfalls als NIO bewerten <BR>
 * @version 3_0_F   02.08.2013  TB Anforderung von Rainer: Pr�fergebnis soll invertiert werden (NIO -> IO bzw. IO -> NIO) <BR>
 */
public class DiagLeseFSAndCheckWhiteBlackLists_3_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagLeseFSAndCheckWhiteBlackLists_3_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DiagLeseFSAndCheckWhiteBlackLists_3_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "WHITELIST[1..N]", "BLACKLIST", "DEBUG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "SGBD" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 *
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt
	 *         hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		int i, j;
		int maxIndex = 0;
		boolean bRequiredArg, bOptionalArg;

		// Debug output on begin of checkArgs()
		if( bDebug ) {
			Enumeration<String> enu = getArgs().keys();

			System.out.println( "" );
			System.out.println( "DiagLeseFSAndCheckWhiteBlackLists PP: checkArgs in: " + this.getName() );
			System.out.println( "DiagLeseFSAndCheckWhiteBlackLists PP: All Arguments from this PP:" );
			while( enu.hasMoreElements() ) {
				// The actual argument
				String strGivenkey = enu.nextElement();
				String strActualArg = getArg( strGivenkey );
				System.out.println( strGivenkey + " = " + strActualArg );
			}
		}

		try {
			// 1. Check: Sind alle requiredArgs gesetzt
			String[] requiredArgs = getRequiredArgs();
			for( i = 0; i < requiredArgs.length; i++ ) {
				if( getArg( requiredArgs[i] ) == null )
					return false;
				if( getArg( requiredArgs[i] ).equals( "" ) == true )
					return false;
			}
			if( bDebug ) {
				System.out.println( "DiagLeseFSAndCheckWhiteBlackLists PP: Done 1. Check" );
			}

			// 2. Check: Sind �berfl�ssige Argumente gesetzt, d.h. Argumente, die weder required noch optional sind
			// Das Argument, dass mit "WHITELIST" beginnt, wird hierbei nicht analysiert, es wird
			// jedoch der maximale Index festgehalten. Die detaillierte Analyse erfolgt im 3-ten Check.
			Enumeration<String> enu = getArgs().keys();
			String[] optionalArgs = getOptionalArgs();
			String givenkey;
			String temp = "";
			while( enu.hasMoreElements() ) {
				givenkey = enu.nextElement();
				// Wenn Argument ohne Index
				if( (givenkey.startsWith( "WHITELIST" ) == false) ) {

					j = 0;
					bRequiredArg = false;
					while( (bRequiredArg == false) && (j < requiredArgs.length) ) {
						if( givenkey.equals( requiredArgs[j] ) == true )
							bRequiredArg = true;
						j++;
					}
					j = 0;
					bOptionalArg = false;
					while( (bOptionalArg == false) && (j < optionalArgs.length) ) {
						if( givenkey.equals( optionalArgs[j] ) == true )
							bOptionalArg = true;
						j++;
					}
					if( !(bRequiredArg || bOptionalArg) )
						return false;
					if( getArg( givenkey ).equals( "" ) == true )
						return false;
				}
				// Wenn Argument mit Index
				else {
					if( givenkey.startsWith( "WHITELIST" ) == true ) {
						temp = givenkey.substring( 9 );
					}
					try {
						j = Integer.parseInt( temp );
						if( j < 1 )
							return false;
						if( j > maxIndex )
							maxIndex = j;
					} catch( NumberFormatException e ) {
						return false;
					}
				}
			}
			if( bDebug ) {
				System.out.println( "DiagLeseFSAndCheckWhiteBlackLists PP: Done 2. Check" );
			}

			// 3. Check: Argment SGBD darf keine Alternativen enthalten
			enu = getArgs().keys();
			while( enu.hasMoreElements() ) {
				givenkey = enu.nextElement();
				if( (givenkey.equals( "SGBD" ) == true) || (givenkey.equals( "DEBUG" ) == true) ) {
					if( getArg( givenkey ).indexOf( ';' ) != -1 )
						return false;
				}
			}
			if( bDebug ) {
				System.out.println( "DiagLeseFSAndCheckWhiteBlackLists PP: Done 3. Check" );
			}

			// Tests bestanden, somit ok
			if( bDebug ) {
				System.out.println( "DiagLeseFSAndCheckWhiteBlackLists PP: End checkArgs() = true" );
			}
			return true;
		} catch( Exception e ) {
			// Debug Infos
			if( bDebug ) {
				e.printStackTrace();
			}

			return false;
		} catch( Throwable e ) {
			// Debug Infos
			if( bDebug ) {
				e.printStackTrace();
			}

			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String sgbd = null;
		Map<Integer, List<String>> mapFSWhiteLists = new HashMap<Integer, List<String>>();
		List<String> strFSBlackList = new ArrayList<String>();
		// aktuell anliegende FS
		List<String> strFSActiveList = new ArrayList<String>();
		Map<Integer, String> mapFSActiveDetail = new HashMap<Integer, String>();
		// EDIABAS
		EdiabasProxyThread myEdiabas = null;
		final String EDIABAS_JOB = "FS_LESEN";

		/***********************************************
		 * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
		 * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		 ***********************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		try {
			//Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG abfragen
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
				} else {
					// noch Pr�fstandvariable pr�fen, ob Debug
					try {
						Object pr_var_debug;

						pr_var_debug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
						if( pr_var_debug != null ) {
							if( pr_var_debug instanceof String ) {
								if( "TRUE".equalsIgnoreCase( (String) pr_var_debug ) ) {
									bDebug = true;
								} else {
									bDebug = false;
								}
							} else if( pr_var_debug instanceof Boolean ) {
								if( ((Boolean) pr_var_debug).booleanValue() ) {
									bDebug = true;
								} else {
									bDebug = false;
								}
							}
						} else {
							bDebug = false;
						}

					} catch( VariablesException e ) {
						bDebug = false;
					}
				}

				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// SGBD
				sgbd = extractValues( getArg( "SGBD" ) )[0];

				// WhiteLists
				int resCount = 1;
				while( (getArg( "WHITELIST" + resCount )) != null ) {
					mapFSWhiteLists.put( resCount, Arrays.asList( extractValues( getArg( "WHITELIST" + resCount ) ) ) );
					resCount++;
				}

				// Blacklist
				if( getArg( "BLACKLIST" ) != null ) {
					strFSBlackList = Arrays.asList( extractValues( getArg( "BLACKLIST" ) ) );
				}

				// debug
				if( bDebug ) {
					System.out.println( "DiagLeseFSAndCheckWhiteBlackLists PP: Parameter processing Check is done. (SGBD: <" + sgbd + ">)" );
				}

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );

				if( bDebug ) {
					e.printStackTrace();
				}

				throw new PPExecutionException();
			}

			// Der eigentliche Check
			// Ausf�hrung EDIABAS Job

			// EDIABAS besorgen
			myEdiabas = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();

			// EDIABAS Job ausf�hren
			String temp = myEdiabas.executeDiagJob( sgbd, EDIABAS_JOB, "", "" );
			if( temp.equals( "OKAY" ) == false ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, EDIABAS_JOB, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} else {
				result = new Ergebnis( "Status", "EDIABAS", sgbd, EDIABAS_JOB, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );
			}

			// Die Fehlerspeicherinfos analysieren
			for( int i = 1; i < (myEdiabas.getDiagJobSaetze() - 1); i++ ) {
				// Erfassung in Collection
				String strFSNr = myEdiabas.getDiagResultValue( i, "F_ORT_NR" );
				strFSActiveList.add( strFSNr );

				// Collection f�r Detailauswertung besorgen und bef�llen
				Integer iFSNr = new Integer( strFSNr );
				String strFSText = myEdiabas.getDiagResultValue( i, "F_ORT_TEXT" );
				mapFSActiveDetail.put( iFSNr, strFSText );

				// Debug Infos
				if( bDebug ) {
					System.out.println( "DiagLeseFSAndCheckWhiteBlackLists PP, Job:" + EDIABAS_JOB + ", FS-No:" + strFSNr + ", FS-Text:" + strFSText );
				}
			}

			// White- / Blacklisten Auswertung
			boolean bCheckWhiteBlackListsIO = false;
			List<String> strFSActiveCopyList = new ArrayList<String>( strFSActiveList );
			// pr�fe ob FS Eintr�ge der Black Liste in der aktiven FS Liste entalten sind?
			boolean bBlackListEntrysAreInActiveList = strFSActiveCopyList.removeAll( strFSBlackList );

			for( Map.Entry<Integer, List<String>> entryWhiteList : mapFSWhiteLists.entrySet() ) {
				boolean bWhiteListEntriesInBlackList = strFSActiveList.containsAll( entryWhiteList.getValue() );

				// White Listen Identit�t? f�r die anliegenden FS Eintr�ge, aber keine Blacklisten Treffer?
				if( bWhiteListEntriesInBlackList && (bBlackListEntrysAreInActiveList || strFSBlackList.isEmpty()) ) {
					bCheckWhiteBlackListsIO = true;

					break;
				}

				// FS liegen an, jedoch trifft keiner aus der Whiteliste und keiner aus der Blackliste
				if( !bWhiteListEntriesInBlackList && !strFSActiveList.isEmpty() && (!bBlackListEntrysAreInActiveList || strFSBlackList.isEmpty()) ) {
					bCheckWhiteBlackListsIO = true;

					break;
				}

				// FS liegen an, jedoch trifft keiner aus der Whiteliste und aber Blackliste trifft
				if( !bWhiteListEntriesInBlackList && !strFSActiveList.isEmpty() && bBlackListEntrysAreInActiveList ) {
					bCheckWhiteBlackListsIO = true;

					break;
				}
			}

			// Gesamt IO / NIO dokumentieren
			if( bCheckWhiteBlackListsIO || strFSActiveList.isEmpty() ) {
				// FS IO dokumentieren
				for( Map.Entry<Integer, String> entryFSActiveDetail : mapFSActiveDetail.entrySet() ) {
					result = new Ergebnis( "FS", "EDIABAS", sgbd, EDIABAS_JOB, "", "F_ORT_NR", entryFSActiveDetail.getKey() + " (HEX: " + Integer.toHexString( entryFSActiveDetail.getKey() ).toUpperCase() + ")", "", "", "0", "", "", "", entryFSActiveDetail.getValue(), "", Ergebnis.FT_IO );
					ergListe.add( result );

					// Debug Infos
					if( bDebug ) {
						System.out.println( "DiagLeseFSAndCheckWhiteBlackLists PP, Active FS entries, FS-No:" + entryFSActiveDetail.getKey() );
					}
				}

				result = new Ergebnis( "FS", "EDIABAS", sgbd, EDIABAS_JOB, "", "STATUS", "IO", "IO", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );
			} else {
				// FS NIO dokumentieren
				for( Map.Entry<Integer, String> entryFSActiveDetail : mapFSActiveDetail.entrySet() ) {
					result = new Ergebnis( "FS", "EDIABAS", sgbd, EDIABAS_JOB, "", "F_ORT_NR", entryFSActiveDetail.getKey() + " (HEX: " + Integer.toHexString( entryFSActiveDetail.getKey() ).toUpperCase() + ")", "", "", "0", "", "", "", entryFSActiveDetail.getValue(), "", Ergebnis.FT_NIO );
					ergListe.add( result );

					// Debug Infos
					if( bDebug ) {
						System.out.println( "DiagLeseFSAndCheckWhiteBlackLists PP, Active FS entries, FS-No:" + entryFSActiveDetail.getKey() );
					}
				}

				result = new Ergebnis( "FS", "EDIABAS", sgbd, EDIABAS_JOB, "", "STATUS", "NIO", "IO", "", "0", "", "", "", "ERROR", PB.getString( "toleranzFehler1" ), Ergebnis.FT_NIO );
				ergListe.add( result );

				status = STATUS_EXECUTION_ERROR;
			}

			// Debug Infos
			if( bDebug ) {
				System.out.println( "DiagLeseFSAndCheckWhiteBlackLists PP: Finished, Execution Status:" + (bCheckWhiteBlackListsIO ? "IO" : "NIO") );
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;

			// Debug Infos
			if( bDebug ) {
				e.printStackTrace();
			}
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;

			// Debug Infos
			if( bDebug ) {
				e.printStackTrace();
			}
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;

			// Debug Infos
			if( bDebug ) {
				e.printStackTrace();
			}
		}

		//Status setzen
		setPPStatus( info, status, ergListe );
	}
}

/**
 * SignalLampe_1_0_F_Pruefprozedur.java <br>
 * <br>
 * V 1: Implementierung <br>
 *  <br>
 * @author BMW AG TI-430 Drexel <br>
 * @version 15.04.2008 <br>
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.signallamp.SignalLamp;
import com.bmw.cascade.pruefstand.pruefumfang.*;

/**
 * Pr�fprozedur f�r die Ansteuerung der SignalLampe <br>
 *
 * @param SIGNALLAMPE <br>
 * 		true <br>
 *      	Flash-Tailer-Programmierung <br>
 *
 * @author BMW AG TI-430 Drexel <br>
 * @version 15.04.2008 <br>
 */
public class SignalLampe_1_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	static final long serialVersionUID = 1L;
	/**
	 * Globale Variablen
	 *
	 * @author BMW AG TI-430 Drexel <br>
	 * @version 15.04.2008 <br>
	 */
	static final String VERSION = "0.01";
    
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public SignalLampe_1_0_F_Pruefprozedur() {}
	/**
	 * erzeugt eine neue Pruefprozedur.
	 *
	 * @param pruefling
	 *            Klasse des zugeh. Pr�flings
	 * @param pruefprozName
	 *            Name der Pr�fprozedur
	 * @param hasToBeExecuted
	 *            Ausf�hrbedingung f�r Fehlerfreiheit
	 */
    public SignalLampe_1_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    /**
     * initialsiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /**
     * liefert die optionalen Argumente
     */
    public String[] getOptionalArgs() {
        String[] args = new String[0];
        return args;
    }
    
    /**
     * liefert die zwingend erforderlichen Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = {"SIGNALLAMPE"};
        return args;
    }
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
     */
    public boolean checkArgs() {
		String signalLampe;
        
        try{
			if (!super.checkArgs()) return false;

			signalLampe=getArg(getRequiredArgs()[0]);
            if (signalLampe==null) return false;
           	if (!signalLampe.equalsIgnoreCase("GREEN") && 
           		!signalLampe.equalsIgnoreCase("GR�N") && 
           		!signalLampe.equalsIgnoreCase("GRUEN") && 
           		!signalLampe.equalsIgnoreCase("YELLOW") && 
           		!signalLampe.equalsIgnoreCase("GELB") && 
           		!signalLampe.equalsIgnoreCase("RED") && 
           		!signalLampe.equalsIgnoreCase("ROT") && 
           		!signalLampe.equalsIgnoreCase("OFF") && 
               	!signalLampe.equalsIgnoreCase("AUS")) return false;
           	
			return true;
        } catch (Exception e) {
            return false;
        } catch (Throwable e) {
            return false;
        }
    }
    
	/**
	 * f�hrt die Pr�fprozedur aus
	 *
	 * @param info
	 * Information zur Ausf�hrung
	 */
    public void execute( ExecutionInfo info ) {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
		// Devices
		SignalLamp signalLamp = null;

		// Variablen allgemein
		String signalLampe;

        try {
            // Parameter holen
            try {
				if (checkArgs() == false) throw new PPExecutionException(PB.getString("parameterexistenz"));
				signalLampe=getArg(getRequiredArgs()[0]);
            } catch (PPExecutionException e) {
                if (e.getMessage() != null)
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }

            // Signallampen-Device holen
			try {
				signalLamp = getPr�flingLaufzeitUmgebung().getDeviceManager().getSignalLamp();
				if( signalLamp != null ) {
					signalLamp.LightsOff();
		           	if (signalLampe.equalsIgnoreCase("GREEN")) signalLamp.GreenLightOn();
		           	if (signalLampe.equalsIgnoreCase("GR�N")) signalLamp.GreenLightOn();
		           	if (signalLampe.equalsIgnoreCase("GRUEN")) signalLamp.GreenLightOn();
		           	if (signalLampe.equalsIgnoreCase("YELLOW")) signalLamp.YellowLightOn();
		           	if (signalLampe.equalsIgnoreCase("GELB")) signalLamp.YellowLightOn();
		           	if (signalLampe.equalsIgnoreCase("RED")) signalLamp.RedLightOn();
		           	if (signalLampe.equalsIgnoreCase("ROT")) signalLamp.RedLightOn();
					signalLamp.FreeceLights(true);
				}
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "SignalLampe", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "SignalLampe", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "SignalLampe", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// UserDialog-Device freigeben
			if( signalLamp != null ) {
				try {
					getPr�flingLaufzeitUmgebung().getDeviceManager().releaseSignalLamp();
				} catch( Exception e ) {
					if( e instanceof DeviceLockedException )
						result = new Ergebnis( "SignalLampe", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "SignalLampe", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
			}
            
            // IO Ergebnis erzeugen
    		if( status == STATUS_EXECUTION_OK ) {
    			result = new Ergebnis( "SignalLampe", "", "", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
    			ergListe.add( result );
    		}
            
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }

		// Status setzen
		setPPStatus( info, status, ergListe );
    }
}

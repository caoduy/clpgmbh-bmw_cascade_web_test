/*
 * MPS3Init_Cards
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import com.bmw.appframework.logging.*;
import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.mps3.*;
import com.bmw.cascade.util.logging.*;
import com.bmw.cascade.pruefstand.*;

/**
 * Diese Pr�fprozedut initialisiert die Messkarten im Multifunktionspr�fstand. 
 * 
 * @author BMW TI-431 Burger
 * @version V_1_0 11.11.2008 AB Implementierung <br>
 * @version V_2_0 14.01.2001 AB Diverse �nderungen <br>
 * @version V_7_0 16.06.2009 AB Fehleraufl�sungen hinzugef�gt <br>
 * @version V_8_0 26.06.2009 AB static finals entfernt <br>
 * @version	V_9_0 13.07.2009 AB releaseMPS3Card () ausgef�hrt <br>
 * @version	V_10_0 14.07.2009 AB bugfixing <br>
 * @version V_11_0 09.08.2009 AB Wartezeiten optimiert <br>
 * @version V_12_0 14.08.2009 AB Timeouts und Wartezeiten angepasst <br> 
 * @version V_13_0 26.08.2009 AB Deutsche Ergebnisse implementiert <br>
 * @version V_14_0 17.09.2009 AB Close card in Close channel ge�ndert
 * 								 Ergebnisse im NIO-Zweig erg�nzt
 * 								 Pause gefixt <br> 
 * @version V_15_0 29.09.2009 AB F-Version <br>
 * @version V_16_0 07.10.2009 AB Cancel implementiert <br>
 * @version V_17_0 07.10.2009 AB F-Version <br>
 * @version V_18_0 16.10.2009 AB Timeouts erh�ht <br>
 * @version V_19_0 09.11.2009 AB Fehlermeldung bei Nachrichtentimeout eingebaut <br>
 * @version V_20_0 09.11.2009 AB F-Version <br>
 * @version V_21_0 29.01.2010 AB Fehlermeldungen angepasst <br>
 * @version V_22_0 09.08.2010 AB F-Version <br>
 * @version V_23_0 09.08.2010 AB Fehlermeldung NOT-Aus erg�nzt <br>
 * @version V_24_0 19.08.2010 AB Fehlerstrings korrigiert <br>
 * @version V_25_0 03.05.2011 TM Optionales Argument 'Component' hinzugef�gt. <br>
 * @version V_26_0 14.06.2011 TM ADD: Optionales Argument 'AFFECTED_COMPONENT' f�r System-Fehler. <br>
 * @version V_27_0 01.08.2011 TM F-Version
 * @version V_28_0 06.07.2015 TM CHA: Added 'user dialogue active' check to user dialogue cancelling request
 * @version V29_0_T 04.08.2015 TM T version
 * @version V30_0_F 07.08.2015 TM F version
 */
public class MPS3DigitalIO_30_0_F_Pruefprozedur extends AbstractBMWPruefprozedur 
{
	static final long serialVersionUID = 1L;
	
	int i_Debug = 0;
	
	String s_AffectedComponent = "";

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public MPS3DigitalIO_30_0_F_Pruefprozedur() 
	{
	}

	private Logger pruefstandLogger = CascadeLogging.getLogger( "PruefstandLogger" );
	
	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public MPS3DigitalIO_30_0_F_Pruefprozedur ( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) 
	{
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() 
	{		
		super.attributeInit();	
	}

	/**
	 * liefert die optionalen Argumente
	 * 
     * JP1: Jumper 1 T / F, TRUE / FALSE
     * JP2: Jumper 2 T / F, TRUE / FALSE
     * JP3: Jumper 3 T / F, TRUE / FALSE
     * JP4: Jumper 4 T / F, TRUE / FALSE
     * DI1: Digitaleingang 1 T / F, TRUE / FALSE
     * DI2: Digitaleingang 2 T / F, TRUE / FALSE     
     * TIMEOUT: Timeout, bis Messwert erreicht
     * DEBUG: Debug-Level 0, 1, 2, 3 (0 = off) 
     * PAUSE: Wartezeit nach der Ausf�hrung
     * AWT_TEXT: Text f�r AWT
     * AWT_TITLE: Titel f�r AWT
     * AWT_TIME: Timeout f�r AWT in ms 
     * AWT_STYLE: 1 = Status, 2 = Error, 99 = Alarm, Rest = Standard
     * FWT_TEXT: Text f�r FWT
     * FWT_TITLE: Titel f�r FWT
     * FWT_TIME: Timeout f�r FWT in ms 
     * QWT_TEXT: Text f�r QWT
     * QWT_TITLE: Titel f�r QWT
     * QWT_TIME: Timeout f�r QWT in ms 
     * QWT_STYLE: 1 = Status, 2 = Error, 99 = Alarm, Rest = Standard
     * COMPONENT: Parametrierbarer Hinweistext f�r 'Gefordertes Bitmuster passt nicht'
     * AFFECTED_COMPONENT: Name der zu pr�fenden Komponente
	 */
	public String[] getOptionalArgs() 
	{
		String[] args = 
		{
				"JP1",
				"JP2",
				"JP3",
				"JP4",
				"DI1",
				"DI2",
				"TIMEOUT",
				"DEBUG",
				"PAUSE",
				"AWT_TEXT",
				"AWT_TITLE",
				"AWT_STYLE",
				"AWT_TIME",
				"AWT_CANCEL",
				"FWT_TEXT",
				"FWT_TITLE",
				"FWT_TIME",
				"QWT_TEXT",
				"QWT_TITLE",
				"QWT_STYLE",
				"QWT_TIME",
				"COMPONENT",
				"AFFECTED_COMPONENT"
	    };
		
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 * 
     * TAG: Bezeichnung der anzusteuernden Messkarte wie in Pr�fstandskonfig.
	 */
	public String[] getRequiredArgs() 
	{
		String[] args = 
		{
				"TAG"												
	    };
		
		return args;
	}

	/**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die parent-Methode aufgrund
     * der offenen Anzahl an Results
	 */	
	public boolean checkArgs() 
	{		
		boolean b_NoError = true;
		
		if (getArg ("TAG").length() <= 0)
			b_NoError = false;

		return b_NoError;
    }
	
	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) 
	{	
		//Result vars
		Vector v_ErgListe = new Vector();	
		int i_Status = STATUS_EXECUTION_ERROR;
		
		//Hardware vars
		DeviceManager c_DevMan = this.getPr�flingLaufzeitUmgebung().getDeviceManager();
		CardURI c_Card = null;
		Helper c_Helper = new Helper ();
		
		//Message vars
		MsgDefs c_DefMsg = new MsgDefs ();
		TcpClient c_Channel = null;
		MsgReceiver c_Receiver = null;
    	
    	//System vars
		Hashtable ht_Args = getArgs();
		Enumeration en_Keys = ht_Args.keys();		
		String s_Arg = null;        
		String s_Val = null;
		boolean b_NoExecError = true;
		boolean b_NoCancel = true;
		
		//local measuring vars
		String s_Tag = "";
		String s_Component = "";
		int i_JumperMask = 0;
		int i_JumperValue = 0;
		int i_JumperResult = 0;
		int i_JumperError = 0;
		int i_DiginMask = 0;
		int i_DiginValue = 0;
		int i_DiginResult = 0;
		int i_DiginError = 0;		
    	
    	//timing vars
        long l_StopTime = 0;		
    	int i_Timeout = 0;
    	int i_Pause = 0;
    	boolean b_Receiving = true;
    	
    	//local AWT vars
    	String s_AwtText = "";
    	String s_AwtTitle = PB.getString("anweisung");	
    	int i_AwtTime = -1;
    	int i_AwtStyle = 0;
    	boolean b_AwtCancel = false;
    	boolean b_AwtBusy = false;
    	
    	//local fwt vars
    	String s_FwtText = "";
    	String s_FwtTitle = PB.getString("frage");
    	int i_FwtTime = -1;
    	boolean b_FwtBusy = false;
    	boolean b_FwtResult = false;
    	
    	//local qwt vars
    	String s_QwtText = "";
    	String s_QwtTitle = PB.getString("meldung");
    	int i_QwtTime = -1;
    	int i_QwtStyle = 0;
    	boolean b_QwtBusy = false; 
    	
		//clear result list
		v_ErgListe.clear();
		//check args
		//checkArgs();
		//init logger
		c_Helper.logInit(i_Debug);	
		
		c_Helper.logDebug(2, "�berpr�fe Parameter", "check parameters", this.getClass());
		
    	if (checkArgs () == false)
    	{
    		c_Helper.logDebug(0, "Mindestens ein zwingender Parameter fehlt.", "At least one required parameter is not supplied.", this.getClass());    		
    		pruefstandLogger.logC( LogLevel.SEVERE, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "At least one required parameter is not supplied.");
    		if (isDE())
    			i_Status = setSystemError (MsgDefs.ERR_WRONG_ARGS, "Konfiguration Pr�fling pr�fen", "Karte (Parameter) erforderlich", "Mindestens ein erforderlicher Parameter fehlt.", v_ErgListe);	
    		else
    			i_Status = setSystemError (MsgDefs.ERR_WRONG_ARGS, "Check configuration of pruefling", "Card (Parameter) required", "At least one required parameter is not supplied.", v_ErgListe);
    		setPPStatus (info, i_Status, v_ErgListe);
    		return;
    	}
		
    	if (isDE())
			s_Component = "Gefordertes Bitmuster passt nicht.";
		else
			s_Component = "Requested Bit-Pattern does not match.";
    	
    	while (en_Keys.hasMoreElements())
		{
			//get name and value from next parameter
			s_Arg = en_Keys.nextElement().toString();
			//s_Val = getDynamicAttribute(en_Args.nextElement().toString());
			s_Val = getDynamicAttribute(ht_Args.get(s_Arg).toString());//.nextElement().toString());
			
			c_Helper.logDebug(2, "Arg: " + s_Arg + " Val: " + s_Val, "Arg: " + s_Arg + "Val: " + s_Val, this.getClass());
		
			//check parameter "DEBUG"
			if (s_Arg.equalsIgnoreCase("DEBUG"))
			{
				//set to level 0, if no value supplied or set to "false"
				if ((s_Val == null) || (s_Val.equalsIgnoreCase("false")))
					i_Debug = 0;
				else
					//set to level 1, if supplied "true"
					if (s_Val.equalsIgnoreCase("true"))
						i_Debug = 1;
					else						
					{
						try
						{
							//try to set desired level
							i_Debug = Integer.parseInt(s_Val);
						}
						//catch numberformatexception
						catch (NumberFormatException e)
						{
							i_Debug = 0;
							pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"DEBUG\" has wrong parameter format. Accepts only integer.", e );
						}
					}				
			}
					
			//Get timeout, if supplied
			if (s_Arg.equalsIgnoreCase("TIMEOUT"))
			{
				try
				{
					//try to set desired level
					i_Timeout = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_Timeout = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"TIMEOUT\" has wrong parameter format. Accepts only integer.", e );
				}	
			}
			
			//Get pause, if supplied
			if (s_Arg.equalsIgnoreCase("PAUSE"))
			{
				try
				{
					//try to set desired level
					i_Pause = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_Pause = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"PAUSE\" has wrong parameter format. Accepts only integer.", e );
				}	
			}
			
			//get AWT vals
			if (s_Arg.equalsIgnoreCase("AWT_TEXT"))
				s_AwtText = s_Val;
			
			if (s_Arg.equalsIgnoreCase("AWT_TITLE"))
				s_AwtTitle = s_Val;			

			if (s_Arg.equalsIgnoreCase("AWT_STYLE"))
			{
				try
				{
					//try to set desired time
					i_AwtStyle = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_AwtStyle = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"AWT_STYLE\" has wrong parameter format. Accepts only integer.", e );
				}					
			}
			
			if (s_Arg.equalsIgnoreCase("AWT_CANCEL"))
			{
				if (s_Val.equalsIgnoreCase("T") || s_Val.equalsIgnoreCase("TRUE") || s_Val.equalsIgnoreCase("1"))
					b_AwtCancel = true;
			}
			
			if (s_Arg.equalsIgnoreCase("AWT_TIME"))
			{
				try
				{
					if (Integer.parseInt(s_Val) < 1000)
						i_AwtTime = -1;
					else
					//try to get awt timeout
						i_AwtTime = Integer.parseInt(s_Val) / 1000;
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_AwtTime = -1;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"AWT_TIME\" has wrong parameter format. Accepts only integer.", e );
				}				
			}
			
			//get FWT vals
			if (s_Arg.equalsIgnoreCase("FWT_TEXT"))
				s_FwtText = s_Val;
			
			if (s_Arg.equalsIgnoreCase("FWT_TITLE"))
				s_FwtTitle = s_Val;			

			if (s_Arg.equalsIgnoreCase("FWT_TIME"))
			{
				try
				{
					if (Integer.parseInt(s_Val) < 1000)
						i_FwtTime = -1;
					else
					//try to get awt timeout
						i_FwtTime = Integer.parseInt(s_Val) / 1000;
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_FwtTime = -1;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"FWT_TIME\" has wrong parameter format. Accepts only integer.", e );
				}				
			}
			
			//get QWT vals
			if (s_Arg.equalsIgnoreCase("QWT_TEXT"))
				s_QwtText = s_Val;
			
			if (s_Arg.equalsIgnoreCase("QWT_TITLE"))
				s_QwtTitle = s_Val;			

			if (s_Arg.equalsIgnoreCase("QWT_STYLE"))
			{
				try
				{
					//try to set desired time
					i_QwtStyle = Integer.parseInt(s_Val);
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_QwtStyle = 0;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"QWT_STYLE\" has wrong parameter format. Accepts only integer.", e );
				}					
			}
			
			if (s_Arg.equalsIgnoreCase("QWT_TIME"))
			{
				try
				{
					if (Integer.parseInt(s_Val) < 1000)
						i_QwtTime = -1;
					else
					//try to get awt timeout
						i_QwtTime = Integer.parseInt(s_Val) / 1000;
				}
				//catch numberformatexception
				catch (NumberFormatException e)
				{
					i_QwtTime = -1;
					pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClass().getName(), "checkArgs()", "\"QWT_TIME\" has wrong parameter format. Accepts only integer.", e );
				}				
			}
			
			// Replace component if set
			if (s_Arg.toUpperCase().startsWith("COMPONENT"))
				s_Component = s_Val;
			
			if (s_Arg.equalsIgnoreCase("AFFECTED_COMPONENT"))
				s_AffectedComponent = /*"Affected Component: " + */s_Val + ": ";
			
			//Add card to list, if supplied
			if (s_Arg.toUpperCase().startsWith("TAG"))
				s_Tag = s_Val;
			
			//String must be MKnm_Vx or MKnm_Vxx
			if (s_Arg.equalsIgnoreCase("JP1"))
			{
				if ((s_Val.equalsIgnoreCase("T")) || (s_Val.equalsIgnoreCase("TRUE")))
				{
					i_JumperMask = i_JumperMask | 0x1;
					i_JumperValue = i_JumperValue | 0x1;
				}
				if ((s_Val.equalsIgnoreCase("F")) || (s_Val.equalsIgnoreCase("FALSE")))
				{
					i_JumperMask = i_JumperMask | 0x1;
					//i_JumperValue = i_JumperValue & ~0x1;
					i_JumperResult = i_JumperResult | 0x1;
				}
			}

			//String must be MKnm_Vx or MKnm_Vxx
			if (s_Arg.equalsIgnoreCase("JP2"))
			{
				if ((s_Val.equalsIgnoreCase("T")) || (s_Val.equalsIgnoreCase("TRUE")))
				{
					i_JumperMask = i_JumperMask | 0x2;
					i_JumperValue = i_JumperValue | 0x2;
				}
				if ((s_Val.equalsIgnoreCase("F")) || (s_Val.equalsIgnoreCase("FALSE")))
				{
					i_JumperMask = i_JumperMask | 0x2;
					//i_JumperValue = i_JumperValue & ~0x2;
					i_JumperResult = i_JumperResult | 0x2;
				}
			}

			//String must be MKnm_Vx or MKnm_Vxx
			if (s_Arg.equalsIgnoreCase("JP3"))
			{
				if ((s_Val.equalsIgnoreCase("T")) || (s_Val.equalsIgnoreCase("TRUE")))
				{
					i_JumperMask = i_JumperMask | 0x4;
					i_JumperValue = i_JumperValue | 0x4;
				}
				if ((s_Val.equalsIgnoreCase("F")) || (s_Val.equalsIgnoreCase("FALSE")))
				{
					i_JumperMask = i_JumperMask | 0x4;
					//i_JumperValue = i_JumperValue & ~0x4;
					i_JumperResult = i_JumperResult | 0x4;
				}
			}

			//String must be MKnm_Vx or MKnm_Vxx
			if (s_Arg.equalsIgnoreCase("JP4"))
			{
				if ((s_Val.equalsIgnoreCase("T")) || (s_Val.equalsIgnoreCase("TRUE")))
				{
					i_JumperMask = i_JumperMask | 0x8;
					i_JumperValue = i_JumperValue | 0x8;
				}
				if ((s_Val.equalsIgnoreCase("F")) || (s_Val.equalsIgnoreCase("FALSE")))
				{
					i_JumperMask = i_JumperMask | 0x8;
					//i_JumperValue = i_JumperValue & ~0x8;
					i_JumperResult = i_JumperResult | 0x8;
				}
			}
			
			//String must be MKnm_Vx or MKnm_Vxx
			if (s_Arg.equalsIgnoreCase("DI1"))
			{
				if ((s_Val.equalsIgnoreCase("T")) || (s_Val.equalsIgnoreCase("TRUE")))
				{
					i_DiginMask = i_DiginMask | 0x1;
					i_DiginValue = i_DiginValue | 0x1;
				}
				if ((s_Val.equalsIgnoreCase("F")) || (s_Val.equalsIgnoreCase("FALSE")))
				{
					i_DiginMask = i_DiginMask | 0x1;
					//i_DiginValue = i_DiginValue & ~0x1;
					i_DiginResult = i_DiginResult | 0x1;
				}
			}
			
			//String must be MKnm_Vx or MKnm_Vxx
			if (s_Arg.equalsIgnoreCase("DI2"))
			{
				if ((s_Val.equalsIgnoreCase("T")) || (s_Val.equalsIgnoreCase("TRUE")))
				{
					i_DiginMask = i_DiginMask | 0x2;
					i_DiginValue = i_DiginValue | 0x2;
				}
				if ((s_Val.equalsIgnoreCase("F")) || (s_Val.equalsIgnoreCase("FALSE")))
				{
					i_DiginMask = i_DiginMask | 0x2;
					//i_DiginValue = i_DiginValue & ~0x2;
					i_DiginResult = i_DiginResult | 0x2;
				}
			}			
		}    	

    	try
    	{
        	//Connect to measuring channel
        	c_Card = (CardURI) c_DevMan.getMPS3Card(s_Tag);    	
    		c_Card.setDebug(i_Debug);
    		c_Card.connectChannel(Integer.toString(IPDefs.DIGIN), "5000");
    		c_Channel = c_Card.getClient(Integer.toString(IPDefs.DIGIN));    		    		
		}
		catch (Exception e)
		{
			if (isDE())
				i_Status = setSystemError (MsgDefs.ERR_CHANNEL, "Konfiguration Pr�fstand pr�fen", "", "Fehler beim �ffnen von Messkanal: " + IPDefs.DIGIN + " auf Karte: " + s_Tag, v_ErgListe);
			else
				i_Status = setSystemError (MsgDefs.ERR_CHANNEL, "Check configuration of pr�fstand", "", "Error while opening channel: " + IPDefs.DIGIN + " at card: " + s_Tag, v_ErgListe);
			pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_DEVICE, this.getClass().getName(), "Error while opening channel: " + IPDefs.DIGIN + " at card: " + s_Val, "Error while Opening.", e );
			setPPStatus (info, i_Status, v_ErgListe);	
			return;
		}			 	
        
        //show AWT
        if (s_AwtText.length() > 0)
        {
        	c_Helper.logDebug(2, "Zeige AWT", "show AWT", this.getClass());
        	
            try 
            {
           		getPr�flingLaufzeitUmgebung().getUserDialog().setAllowCancel(b_AwtCancel);
           		
                if(i_AwtStyle == 99)
                	getPr�flingLaufzeitUmgebung().getUserDialog().displayMessage(s_AwtTitle, s_AwtText, i_AwtTime);
                else 
                	if(i_AwtStyle == 1)
                		getPr�flingLaufzeitUmgebung().getUserDialog().displayStatusMessage(s_AwtTitle, s_AwtText, i_AwtTime);
                else 
                	if(i_AwtStyle == 2)
                		getPr�flingLaufzeitUmgebung().getUserDialog().displayAlertMessage(s_AwtTitle, s_AwtText, i_AwtTime);
                else
                    getPr�flingLaufzeitUmgebung().getUserDialog().displayUserMessage(s_AwtTitle, s_AwtText, i_AwtTime);

                b_AwtBusy = true;            
            } 
            catch (Exception e) 
            {
            	c_Helper.logDebug(0, s_AwtText + " " + e, s_AwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while opening AWT-Dialog: " + s_AwtText, e );            	
            }   
        } 
        
        //show FWT
        int i_FwtTemp = 0;
        if (s_FwtText.length() > 0)
        {
        	c_Helper.logDebug(2, "Zeige FWT", "show FWT", this.getClass());
        	
            try 
            {            	
            	i_FwtTemp = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputDigital(s_FwtTitle, s_FwtText, i_FwtTime);
            	
                //Get FWT result
                if (i_FwtTemp == getPr�flingLaufzeitUmgebung().getUserDialog().YES_KEY)
                	b_FwtResult = true;
                else if (i_FwtTemp == getPr�flingLaufzeitUmgebung().getUserDialog().NO_KEY)
                	b_FwtResult = false;              	

                b_FwtBusy = true;            
            } 
            catch (Exception e) 
            {
            	c_Helper.logDebug(0, s_FwtText + " " + e, s_FwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while opening FWT-Dialog: " + s_FwtText, e );            	
            }   
        }
        
        //show QWT
        if (s_QwtText.length() > 0)
        {
        	c_Helper.logDebug(2, "Zeige QWT", "show QWT", this.getClass());
        	
            try 
            {
                if( i_QwtStyle == 1 )
                    getPr�flingLaufzeitUmgebung().getUserDialog().requestStatusMessage(s_QwtTitle, s_QwtText, i_QwtTime);
                else if( i_QwtStyle == 2 )
                    getPr�flingLaufzeitUmgebung().getUserDialog().requestAlertMessage(s_QwtTitle, s_QwtText, i_QwtTime);
                else
                    getPr�flingLaufzeitUmgebung().getUserDialog().requestUserMessage(s_QwtTitle, s_QwtText, i_QwtTime);            	

                b_QwtBusy = true;            
            } 
            catch (Exception e) 
            {
            	c_Helper.logDebug(0, s_QwtText + " " + e, s_QwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while opening QWT-Dialog: " + s_QwtText, e );            	
            }   
        }         
        
        //timoeut surveyed delays within the execution code
        if (i_Timeout > 0)
        {
        	//Measure until result is "green" or timeout expired
        	c_Helper.logDebug(2, "Starte Timeout", "start timeout", this.getClass());
        	
        	boolean b_Measuring = true;
        	l_StopTime = System.currentTimeMillis() + i_Timeout;
        	
        	while ((l_StopTime > System.currentTimeMillis()) && b_Measuring && b_NoCancel)
        	{
            	b_Receiving = true;
            	long l_StopLocal;
            	int i_Data[] = {c_DefMsg.ACT_GET_DATA};            	
            	
            	c_Helper.logDebug(2, "Starte Messung", "Start Measure", this.getClass());
            	            	
            	if (i_DiginMask != 0)
            	{
                	if (c_Channel != null)
                		c_Channel.setTxMsg(c_DefMsg.ID_CTRL_DIGIN, i_Data, null, null);
                	else
                		b_NoExecError = false;
            		
                	i_Data[0] = c_DefMsg.ACT_GET_DATA;
                	b_Receiving = true;
                	l_StopLocal = System.currentTimeMillis() + 6000;
                	while (b_Receiving && (l_StopLocal > System.currentTimeMillis()))
                	{
                		if (c_Channel != null)
                			c_Receiver = c_Channel.getRxMsgByID(c_DefMsg.ID_STAT_DIGIN, true);
                    	else
                    		b_NoExecError = false;
                		
                		if (c_Receiver == null)
                			c_Helper.timer(100);
                		else
                		{
                			i_DiginResult = c_Receiver.words[0];
                			if (c_Receiver.words.length > 1)
                			{
                				i_DiginError = c_Receiver.words[1];
                				
                				if (i_DiginError > 0)
                					b_Measuring = false;
                			}
                			b_Receiving = false;
                		}
                	}            		
            	}
            	
            	if (i_JumperMask != 0)
            	{
                	i_Data[0] = c_DefMsg.ACT_GET_DATA;
                	if (c_Channel != null)
                		c_Channel.setTxMsg(c_DefMsg.ID_CTRL_JUMP, i_Data, null, null);
                	else
                		b_NoExecError = false;        	
                	
                	b_Receiving = true;
                	l_StopLocal = System.currentTimeMillis() + 6000;
                	while (b_Receiving && (l_StopLocal > System.currentTimeMillis()))
                	{
                		if (c_Channel != null)
                			c_Receiver = c_Channel.getRxMsgByID(c_DefMsg.ID_STAT_JUMP, true);
                    	else
                    		b_NoExecError = false;
                		
                		if (c_Receiver == null)
                			c_Helper.timer(100);
                		else
                		{
                			i_JumperResult = c_Receiver.words[0];
                			if (c_Receiver.words.length > 1)
                			{
                				i_JumperError = c_Receiver.words[1];
                				
                				if (i_JumperError > 0)
                					b_Measuring = false;
                			}
                			b_Receiving = false;
                		}
                	}             		
            	}
            	
    			if (((i_DiginResult & i_DiginMask) == i_DiginValue) && ((i_JumperResult & i_JumperMask) == i_JumperValue))
    				b_Measuring = false;				
    			else
        	    	if (i_Pause > 0)
        	    	{
        	        	c_Helper.logDebug(2, "Starte Pause " + i_Pause + " ms", "start pause "  + i_Pause + " ms", this.getClass());
        	            try 
        	            {
        	            	Thread.sleep(i_Pause);
        	            } 
        	            catch( InterruptedException e ) 
        	            {
        	            }        	
        	    	}
        	    	else        	    		
        	    		c_Helper.timer(250);
    			
            	try
            	{
            		if (b_AwtBusy | b_FwtBusy | b_QwtBusy) {
            			b_NoCancel = !getPr�flingLaufzeitUmgebung().getUserDialog().isCancelled();
            		}
            	}
            	catch (Exception e)
            	{            	
            	}    			
        	}        	
        }
        else
        {
        	b_Receiving = true;
        	long l_StopLocal = System.currentTimeMillis() + 500;
        	int i_Data[] = {c_DefMsg.ACT_GET_DATA};

        	c_Helper.logDebug(2, "Starte Messung", "Start Measure", this.getClass());
        	            	
        	if (i_DiginMask != 0)
        	{
            	if (c_Channel != null)
            		c_Channel.setTxMsg(c_DefMsg.ID_CTRL_DIGIN, i_Data, null, null);
            	else
            		b_NoExecError = false;
        		
            	i_Data[0] = c_DefMsg.ACT_GET_DATA;
            	b_Receiving = true;
            	l_StopLocal = System.currentTimeMillis() + 6000;
            	while (b_Receiving && (l_StopLocal > System.currentTimeMillis()))
            	{
            		if (c_Channel != null)
            			c_Receiver = c_Channel.getRxMsgByID(c_DefMsg.ID_STAT_DIGIN, true);
                	else
                		b_NoExecError = false;
            		
            		if (c_Receiver == null)
            			c_Helper.timer(100);
            		else
            		{
            			i_DiginResult = c_Receiver.words[0];
            			if (c_Receiver.words.length > 1)
            			{
            				i_DiginError = c_Receiver.words[1];
            			}
            			b_Receiving = false;
            		}
            	}            		
        	}
        	
        	if (i_JumperMask != 0)
        	{
            	i_Data[0] = c_DefMsg.ACT_GET_DATA;
            	if (c_Channel != null)
            		c_Channel.setTxMsg(c_DefMsg.ID_CTRL_JUMP, i_Data, null, null);
            	else
            		b_NoExecError = false;        	
            	
            	b_Receiving = true;
            	l_StopLocal = System.currentTimeMillis() + 6000;
            	while (b_Receiving && (l_StopLocal > System.currentTimeMillis()))
            	{
            		if (c_Channel != null)
            			c_Receiver = c_Channel.getRxMsgByID(c_DefMsg.ID_STAT_JUMP, true);
                	else
                		b_NoExecError = false;
            		
            		if (c_Receiver == null)
            			c_Helper.timer(100);
            		else
            		{
            			i_JumperResult = c_Receiver.words[0];
            			if (c_Receiver.words.length > 1)
            			{
            				i_JumperError = c_Receiver.words[1];
            			}
            			b_Receiving = false;
            		}
            	}             		
        	}
        	
        	if (i_Pause > 0)
        	{
            	c_Helper.logDebug(2, "Starte Pause " + i_Pause + " ms", "start pause "  + i_Pause + " ms", this.getClass());
                try 
                {
                	Thread.sleep(i_Pause);
                } 
                catch( InterruptedException e ) 
                {
                }        	
        	}        	
        }
       
        //close AWT
        if (b_AwtBusy)
        {
        	c_Helper.logDebug(2, "Schlie�e AWT", "close AWT", this.getClass());
        	
        	try 
        	{
        		getPr�flingLaufzeitUmgebung().releaseUserDialog();
            }
        	catch (Exception e) 
        	{
            	c_Helper.logDebug(0, s_AwtText + " " + e, s_AwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while closing AWT-Dialog: " + s_AwtText, e );            	        		
        	}        	
        	
        	b_AwtBusy = false;
        }		                     
		
        //close FWT
        if (b_FwtBusy)
        {
        	c_Helper.logDebug(2, "Schlie�e FWT", "close FWT", this.getClass());
        	
        	try 
        	{
        		getPr�flingLaufzeitUmgebung().releaseUserDialog();
            }
        	catch (Exception e) 
        	{
            	c_Helper.logDebug(0, s_FwtText + " " + e, s_FwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while closing FWT-Dialog: " + s_FwtText, e );            	        		
        	}        	
        	
        	b_FwtBusy = false;
        }       
        
        //close QWT
        if (b_QwtBusy)
        {
        	c_Helper.logDebug(2, "Schlie�e QWT", "close QWT", this.getClass());
        	
        	try 
        	{
        		getPr�flingLaufzeitUmgebung().releaseUserDialog();
            }
        	catch (Exception e) 
        	{
            	c_Helper.logDebug(0, s_QwtText + " " + e, s_QwtText + " " + e, this.getClass());
            	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while closing QWT-Dialog: " + s_QwtText, e );            	        		
        	}        	
        	
        	b_QwtBusy = false;
        }                						              				        	

        if (!b_NoCancel)
        {
        	if (isDE())
        		i_Status = setMeasureError (0, 0, 0, 0, "", "", "User hat Pr�fung abgebrochen.", v_ErgListe);
        	else
        		i_Status = setMeasureError (0, 0, 0, 0, "", "", "User cancelled teststep.", v_ErgListe);
        }
        else
        {
    		if (b_NoExecError)
    		{
    			if (((i_DiginResult & i_DiginMask) == i_DiginValue) && ((i_JumperResult & i_JumperMask) == i_JumperValue))
    				i_Status = setMeasureOK (0, 0, 0, v_ErgListe);
    			else if (((i_DiginError & MsgDefs.ERR_EMSTOP) > 0) || ((i_JumperError & MsgDefs.ERR_EMSTOP) > 0))
    			{
					if (isDE())
						setSystemError(MsgDefs.ERR_EMSTOP, "NOT-Aus freigeben und best�tigen", "Ein NOT-Aus ist aufgetreten", "NOT-Aus", v_ErgListe);					
					else
						setSystemError(MsgDefs.ERR_EMSTOP, "Release condition and acknowledge", "An Emergency stop occured", "Emergency stop", v_ErgListe);    				
    			}
    			else
    			{
    				if (isDE())
    					i_Status = setMeasureError (0, 0, 0, 0, "", s_Component, "Ergebnis ung�ltig.", v_ErgListe);
    				else
    					i_Status = setMeasureError (0, 0, 0, 0, "", s_Component, "Result not valid.", v_ErgListe);    			    				
    			}
    		}
    		else
    		{
    			c_Helper.logDebug(0, "Mit Ausf�hrungsfehler beendet.", "Finished with execution errors.", this.getClass());
    			if (isDE())
    				i_Status = setSystemError(MsgDefs.ERR_HW, "Pr�fe Parameter, Karte und Spannungsversorgung", "Karten- oder Kommunikationsfehler", "Ausf�hrungsfehler", v_ErgListe);
    			else
    				i_Status = setSystemError(MsgDefs.ERR_HW, "Check parameters, card and power-supply", "Card or communication failed", "Execution error", v_ErgListe);			
    		}        	
        }
        
        if (b_Receiving)
        {
        	if (isDE())
        		i_Status = setMeasureError (0, 0, 0, 0, "Messung wiederholen", "Kommunikationsfehler", "Fehler beim Nachrichtenempfang", v_ErgListe);
        	else
        		i_Status = setMeasureError (0, 0, 0, 0, "Repeat measurement", "Communication error", "Error while receiving message", v_ErgListe);
        	
        	c_Helper.logDebug(0, "Fehler beim Nachrichtenempfang", "Error while receiving message", this.getClass());
        	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error while receiving message");            	        		        	
        }
        
		try
		{
			c_Card.closeChannel(Integer.toString(IPDefs.DIGIN), true);
		}		
		catch (Exception e)
		{
        	c_Helper.logDebug(0, "Fehler beim Schlie�en des Kanals: " + e.getMessage(), "Error while closing channel: " + e.getMessage(), this.getClass());
        	pruefstandLogger.logC( LogLevel.WARNING, LogCategory.LOG_CATEGORY_PRUEFPROZEDUR, this.getClassName(), "execute()", "Error ehile closing channel: " + e.getMessage());            	        		        				
		}
		
		//Set Result
		setPPStatus (info, i_Status, v_ErgListe);
	}

	/**
	 * Setzt das Pr�fungsergebnis auf Systemfehler und generiert entsprechende Eintr�ge.
	 * 
	 * @param errCode 		spezifischer Fehlercode
	 * @param awt 			spezifischer Anweisungstext
	 * @param hwt 			spezifischer Hinweistext
	 * @param errt 			spezifischer Fehlertext
	 * @param ergebnisListe zu beschreibende ErgListe
	 * @return status		status als int
	 */
	private int setSystemError (int errCode, String awt, String hwt, String errt, Vector ergListe)
	{
		if (s_AffectedComponent != null || s_AffectedComponent.length() > 0)
			ergListe.add (new  Ergebnis(Integer.toString(errCode), this.getPPName(), "", "", "", "", "", "", "", "", "", "", awt, s_AffectedComponent + errt, hwt, Ergebnis.FT_NIO));
		else
			ergListe.add (new  Ergebnis(Integer.toString(errCode), this.getPPName(), "", "", "", "", "", "", "", "", "", "", awt, errt, hwt, Ergebnis.FT_NIO));
		return STATUS_EXECUTION_ERROR;		
	}

	/**
	 * Setzt das Pr�fungsergebnis auf Messung IO und generiert entsprechende Eintr�ge.
	 * 
	 * @param value 		finale(r) Messwert(e)
	 * @param min			Minimalwert
	 * @param max			Maximalwert
	 * @param ergebnisListe zu beschreibende ErgListe
	 * @return status		status als int 
	 */
	private int setMeasureOK (int value, int min, int max, Vector ergListe)
	{
		if (isDE())
			ergListe.add (new Ergebnis("0", this.getPPName(), "", "", "", "Ergebnis IO", Integer.toString(value), Integer.toString(min), Integer.toString(max), "", "", "", "", "", "", Ergebnis.FT_IO));
		else
			ergListe.add (new Ergebnis("0", this.getPPName(), "", "", "", "Result ok", Integer.toString(value), Integer.toString(min), Integer.toString(max), "", "", "", "", "", "", Ergebnis.FT_IO));
		return STATUS_EXECUTION_OK;
	}

	/**
	 * Setzt das Pr�fungsergebnis auf Messung NIO und generiert entsprechende Eintr�ge.
	 * 
	 * @param errCode 	spezifischer Fehlercode
	 * @param value 	finaler Messwert
	 * @param min			Minimalwert
	 * @param max			Maximalwert
	 * @param awt 		spezifischer Anweisungstext
	 * @param hwt 		spezifischer Hinweistext
	 * @param errt 		spezifischer Fehlertext
	 * @param ergebnisListe zu beschreibende ErgListe
	 * @return status		status als int 
	 */
	private int setMeasureError (int errCode, int value, int min, int max, String awt, String hwt, String errt, Vector ergListe)
	{
		if (isDE())			
			ergListe.add (new Ergebnis(Integer.toString(errCode), this.getPPName(), "", "", "", "Ergebnis NIO", Integer.toString(value), Integer.toString(min), Integer.toString(max), "", "", "", awt, errt, hwt, Ergebnis.FT_NIO));
		else
			ergListe.add (new Ergebnis(Integer.toString(errCode), this.getPPName(), "", "", "", "Result out of range", Integer.toString(value), Integer.toString(min), Integer.toString(max), "", "", "", awt, errt, hwt, Ergebnis.FT_NIO));
		return STATUS_EXECUTION_ERROR;
	}	

	/**
	 * L�st in @-geklammerte Attribute dynamisch (zur Laufzeit) auf.
	 *  
	 * @param input	input-String
	 * @return		gefundener Wert. input ohne @-Klammerung, dynamisch geholte mit Klammerung.
	 */
	private String getDynamicAttribute (String input)
	{
		String result = input;
		Helper funcs = new Helper ();
		
		funcs.logInit(i_Debug);		
		
		funcs.logDebug(2, "Start getDynamicAttribute. . .", "Start getDynamicAttribute . . .", this.getClass());
		if (input.startsWith("@"))
		{
			funcs.logDebug(2, "Erstes Token gefunden", "Found first Token", this.getClass());
			if (input.endsWith("@"))
			{
				funcs.logDebug(2, "Zweites Token gefunden", "Found second Token", this.getClass());
				if (input.length() >= 3)
					try
					{
						funcs.logDebug(2, "L�nge g�ltig. Hole dynamisches Attribut. . .", "Length valid. fetch dynmaic attribute. . .", this.getClass());
						result = (String) getPr�fling().getAllAttributes().get(input.substring(1, input.length() - 1));
						funcs.logDebug(2, "Dynamisches Attribut aufgel�st: " + result, "Resolved dynamic attribute: " + result, this.getClass());
					}
					catch (Exception e)
					{
						//@todo: wenn ein fehler auftritt nicht versuchen mit einem beliebigen text weiterzuarbeiten. sondern exception!
						result = input.substring(0, input.length() - 1);						
						funcs.logDebug(0, "Fehler beim dynamischen Aufl�sen. Es wird zur�ck gegeben: " + result, "Error during dynamic lookup. It will be returned: " + result, this.getClass());						
					}					
				else
				{
					result = null;
					funcs.logDebug(0, "L�nge ung�ltig. Es wird \"null\" zur�ck gegeben.", "Length invalid. \"null\" is returned.", this.getClass());
				}
			}
			else
			{
				result = input.substring(1, input.length());
				funcs.logDebug(0, "Zweites \"@\" nicht gefunden. Es wird zur�ck gegeben: " + result, "Did not find second \"@\". It will be returned: " + result, this.getClass());
			}
		}
		else
			if (input.endsWith("@"))
			{
				result = input.substring(0, input.length() - 1);
				funcs.logDebug(0, "Erstes \"@\" nicht gefunden. Es wird zur�ck gegeben: " + result, "Did not find first \"@\". It will be returned: " + result, this.getClass());
			}				
			else
			{
				funcs.logDebug(2, "Kein \"@\" gefunden. Statisches Attribut. Value: " + result, "No \"@\" found. Static attribute. Value: " + result, this.getClass());
				result = input;
			}
		
		return result;
	}
	
    /**
     * Liefert true zur�ck, wenn deutsche Texte verwendet werden sollen.
     * @result True bei deutsche texte, false sonst.
     */
    private static boolean isDE() 
    {
        try 
        {
            if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true ) return true;
            else return false;
        } 
        catch (Exception e) 
        {
            return false;   //default is english
        }
    }
}

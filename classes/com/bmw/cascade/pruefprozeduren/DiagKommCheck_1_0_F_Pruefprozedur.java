/*
 * DiagKommCheck_1_0_F_Pruefprozedur.java
 *
 * Created on 23.02.00
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;


import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.ediabas.*;
import com.bmw.cascade.pruefstand.devices.*;



/**
 * Implementierung der Pr�fprozedur, die per Diagnose pr�ft, ob die Kommunikation zu einem Steuerger�t funktioniert.
 * Dabei werden s�mtliche EdiabasExceptions ge-catched, d.h. diese f�hren nicht zu einem NIO.
 * Diese Bewertung kann pollend erfolgen, d.h. �ber das optionale Argument TIMEOUT kann gesteuert werden, wie
 * lange (Maximalzeit) die Bewertung erfolgt. �ber das optionale Argument OK_TIME kann angegeben werden, wie lange
 * der Status OK gehalten werden muss, damit dieser Status g�ltig ist. Optional ist die Ausgabe einer Werkeranweisung
 * m�glich.
 * @author Winkler
 * @version not testet
 */
public class DiagKommCheck_1_0_F_Pruefprozedur extends AbstractBMWPruefprozedur 
{

  	static final long serialVersionUID = 1L;
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public DiagKommCheck_1_0_F_Pruefprozedur() {}

  /**
   * erzeugt eine neue Pruefprozedur mit obigem Verhalten.
   * @param pruefling Klasse des zugeh. Pr�flings
   * @param pruefprozName Name der Pr�fprozedur
   * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
   */
  public DiagKommCheck_1_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) 
  {
    super( pruefling, pruefprozName, hasToBeExecuted );
    attributeInit();
  }  
  

  /**
   * initialsiert die Argumente
   */
  protected void attributeInit() {
    super.attributeInit();
  }

  /**
   * liefert die optionalen Argumente
   */
  public String[] getOptionalArgs() {
    String[] args = {"TIMEOUT", "OK_TIME", "PAUSE", "AWT"};
    return args;
  }

  /**
   * liefert die zwingend erforderlichen Argumente
   */
  public String[] getRequiredArgs() {
    String[] args = {"SGBD", "KOMM"};
    return args;
  }

  /**
   * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die parent-Methode aufgrund
   * der offenen Anzahl an Results
   */
   public boolean checkArgs() {
    boolean ok;
    long l;
    int maxIndex = 0;
    boolean exist;

    try{
      ok = super.checkArgs();
      if( ok == false ) return false;
      if( getArg(getRequiredArgs()[0]).indexOf(';') != -1 )  return false;
      try {         
        l = Long.parseLong( getArg(getRequiredArgs()[1]) );
        if( (l < 0) || (l > 1) ) return false;
        for( int i = 0; i < 3; i++ ) {
          if( getArg(getOptionalArgs()[i]) != null ) {
            l = Long.parseLong( getArg(getOptionalArgs()[i]) );
            if( l < 0 ) return false;
          }
        }
        return true;
      } catch (NumberFormatException e) {
        return false;
      }  
    } catch (Exception e) {
      return false;
    } catch (Throwable e) {
      return false;
    }                     
  }


  /**
   * f�hrt die Pr�fprozedur aus
   * @param info Information zur Ausf�hrung
   */
  public void execute( ExecutionInfo info )
  {
    // immer notwendig
    Ergebnis result;
    Vector ergListe = new Vector();
    int status = STATUS_EXECUTION_OK;

    // spezifische Variablen
    String temp, sgbd, awt;
    long endeZeit, okTime, pause;
    boolean kommHasToWork;
    long deltaT, firstOkTime;
    int resAnzahl;
    int i, j;
    boolean kommOk;
    boolean udInUse = false;

    try {
      //Parameter holen 
      try {
        if( checkArgs() == false ) throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
        sgbd   = extractValues( getArg( getRequiredArgs()[0] ) )[0];
        //Komm-Sollstatus
        if( Integer.parseInt( getArg(getRequiredArgs()[1]) ) == 0 )
          kommHasToWork = false;
        else
          kommHasToWork = true;
        //Timeout
        temp = getArg( getOptionalArgs()[0] );
        if( temp == null ) endeZeit = System.currentTimeMillis();
        else               endeZeit = System.currentTimeMillis() + Long.parseLong(temp);
        //OK_Time
        temp = getArg( getOptionalArgs()[1] );
        if( temp == null ) okTime = -1;
        else               okTime = Long.parseLong(temp);
        //Pause
        temp = getArg( getOptionalArgs()[2] );
        if( temp == null ) pause = 0;
        else               pause = Long.parseLong(temp);
        //Anweisungstext
        awt = getArg( getOptionalArgs()[3] );
        if( awt == null ) awt="";
        else awt = PB.getString( awt );
      } catch (PPExecutionException e) {
        if (e.getMessage() != null)
          result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
        else
          result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
        ergListe.add(result);
        throw new PPExecutionException();
      }

      //Die Ausf�hrungs- und Analyseschleife
      String job    = "IDENT";
      String jobpar = "";
      String jobres = "JOB_STATUS";
      firstOkTime = Long.MAX_VALUE;

      do {
        //Ausf�hrung
        ergListe = new Vector();
        status = STATUS_EXECUTION_OK;
        kommOk = false;
        try {
          temp = Ediabas.executeDiagJob(sgbd,job,jobpar,jobres);
          if( temp.equals("OKAY") == true) kommOk = true;
        } catch( ApiCallFailedException e ) {
        } catch( EdiabasResultNotFoundException e ) {
        }

        //Analyse
        if( kommOk != kommHasToWork ) {
          status = STATUS_EXECUTION_ERROR;
          firstOkTime = Long.MAX_VALUE;
          if( kommHasToWork == true )
            result = new Ergebnis("KommStatus", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", "FAILED", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
          else
            result = new Ergebnis("KommStatus", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", "OKAY", "FAILED", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
          ergListe.add(result);
        } else {
          if( kommHasToWork == true )
            result = new Ergebnis("KommStatus", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
          else
            result = new Ergebnis("KommStatus", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", "FAILED", "FAILED", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
          ergListe.add(result);
          //OK, nun Analyse OK-Zeit
          if( okTime > 0 ) { //Unter Umst�nden Zeitdauer noch nicht erf�llt
            if( firstOkTime > System.currentTimeMillis() ) {
              // Letzte Ausf�hrung war NIO
              firstOkTime = System.currentTimeMillis();
              result = new Ergebnis( "OK-Zeit", "", "", "", "", "Dauer", "0", ""+okTime, ""+Long.MAX_VALUE, "0", "", "", "", PB.getString( "okZeitUnterschritten" ), "", Ergebnis.FT_NIO );
              status = STATUS_EXECUTION_ERROR;
            } else {
              //Letzte bzw. die letzten Ausf�hrung waren IO
              deltaT = System.currentTimeMillis() - firstOkTime;
              if( deltaT < okTime ) {
                status = STATUS_EXECUTION_ERROR;
                result = new Ergebnis( "OK-Zeit", "", "", "", "", "Dauer", ""+deltaT, ""+okTime, ""+Long.MAX_VALUE, "0", "", "", "", PB.getString( "okZeitUnterschritten" ), "", Ergebnis.FT_NIO );
              } else {
                result = new Ergebnis( "OK-Zeit", "", "", "", "", "Dauer", ""+deltaT, ""+okTime, ""+Long.MAX_VALUE, "0", "", "", "", "", "", Ergebnis.FT_IO );
              }
            }  
            ergListe.add(result);
          }
        }

        if( (status != STATUS_EXECUTION_OK) && (endeZeit > System.currentTimeMillis()) ) {
          //Nur wenn Loop nicht zuende
          if( (awt.equals("") == false) && (udInUse == false) ) {
            //nur wenn was auszugeben ist und dieses noch nicht ausgegeben wurde
            try {
              getPr�flingLaufzeitUmgebung().getUserDialog().displayMessage( PB.getString( "anweisung" ), awt, -1 );
              udInUse = true;
            } catch (Exception e) {
              if (e instanceof DeviceLockedException)
                result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
              else if (e instanceof DeviceNotAvailableException)
                result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
              else
                result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
              ergListe.add(result);
              throw new PPExecutionException();
            }
          }
        }

        //Pause falls gew�nscht und Status NIO
        if( (status != STATUS_EXECUTION_OK) && (pause > 0) ) {
          try {
            Thread.sleep(pause);
          } catch( InterruptedException e ) {
          }
        }
      } while( (endeZeit > System.currentTimeMillis()) && (status != STATUS_EXECUTION_OK) );

    } catch (PPExecutionException e) {
      status = STATUS_EXECUTION_ERROR;
    } catch (Exception e) {
      result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
      ergListe.add(result);
      status = STATUS_EXECUTION_ERROR;
    } catch (Throwable e) {
      result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
      ergListe.add(result);
      status = STATUS_EXECUTION_ERROR;
    }                     

    //Freigabe der benutzten Devices
    if( udInUse == true ) {
      try {
        getPr�flingLaufzeitUmgebung().releaseUserDialog();
      } catch (Exception e) {
        if (e instanceof DeviceLockedException)
          result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
        else
          result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
        ergListe.add(result);
        status = STATUS_EXECUTION_ERROR;
      }
    }
    
    //Status setzen
    setPPStatus( info, status, ergListe );

  }

  
}


package com.bmw.cascade.pruefprozeduren;

import java.util.Vector;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;

/**
 * Implementierung einer Pr�fprozedur zum Konvertieren Hex-Werte zu Dezimal-Werte.<BR>
 * @author Ke Wang, Claudia wolf<BR>
 * @version Implementierung <BR>
 * @version 1_0_F	12.04.2016  KW Erstimplementierung <BR>
 * @version 2_0_T	19.07.2017  CW Erweiterung um den Parameter SIGNED. Ist dieser true so wird der eingegebene
 * 									String wie eine Hexadezimalzahl mit Vorzeichen behandelt <BR>
 * @version 3_0_F	21.07.2017  CW Freigabe 2_0_T <BR>
 */
public class HexDecConvertor_3_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	// gibt an, ob Debug-Ausgaben erfolgen sollen
	boolean DEBUG = false;

	// Systemsprache DE
	private final boolean DE = checkDE(); // Systemsprache DE wenn true

	/** Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public HexDecConvertor_3_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public HexDecConvertor_3_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 * @return Optionale Argumente als String Array
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "SIGNED" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 * @return Zwingende Argumente als String Array
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "VALUE[1..N]" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 *
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt
	 *         hat, ansonsten <code>true</code>.
	 */
	@Override
	public boolean checkArgs() {
		// Obligatorisch.
		// 1. Check: Sind alle requiredArgs gesetzt
		String[] requiredArgs = getRequiredArgs();
		for( int i = 0; i < requiredArgs.length; i++ ) {
			if( getArg( "VALUE" + (i + 1) ) == null )
				return false;
			if( getArg( "VALUE" + (i + 1) ).equals( "" ) == true )
				return false;
		}
		return true;
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		//HexDecConvertor Parametervariablen
		String HEX_CHARACTERS = "0123456789ABCDEF";
		String sCurrentResultLine;
		int resAnzahl = 0;
		long convertedValue = 0;
		int inValueLen = 0;
		boolean signed = false;

		try {
			if( DEBUG ) {
				System.out.println( "--------------------------" );
			}
			if( DEBUG ) {
				System.out.println( "Start PP Test" );
			}

			//Parameter-Check
			if( checkArgs() == false ) {
				throw new PPExecutionException( PB.getString( "Parametrierfehler!" ) );
			}

			//Ist SIGNED=true
			if( getArg( "SIGNED" ) != null && (getArg( "SIGNED" ).equalsIgnoreCase( "true" )) ) {
				signed = true;
			}

			//required Args holen
			while( (sCurrentResultLine = getArg( "VALUE" + (resAnzahl + 1) )) != null ) {

				// Unterst�tzung @-Parameter
				sCurrentResultLine = extractValues( sCurrentResultLine )[0];

				inValueLen = sCurrentResultLine.length();

				if( DEBUG ) {
					System.out.println( "incoming Value : " + sCurrentResultLine );
				}

				long val = 0;
				if( signed ) {//Wert behandeln als ob eine signed Hex-Zahl vorliegt
					sCurrentResultLine = sCurrentResultLine.toUpperCase();
					short signedValue = (short) Integer.parseInt( sCurrentResultLine, 16 );
					convertedValue = signedValue;
				} else {//Wert behandeln als ob eine unsigned hex-zahl vorliegt
					sCurrentResultLine = sCurrentResultLine.toUpperCase();

					//alternativ zu dieser For-Schleife k�nnte man auch folgendes tun:
					//convertedValue = Integer.parseInt( sCurrentResultLine , 16)
					for( int i = 0; i < inValueLen; i++ ) {
						char c = sCurrentResultLine.charAt( i );
						int d = HEX_CHARACTERS.indexOf( c );
						val = 16 * val + d;
					}
					convertedValue = val;
				}

				if( DEBUG ) {
					System.out.println( "convertedValue : " + convertedValue );
				}
				result = new Ergebnis( "CONVERTEDVALUE" + (resAnzahl + 1), "CONVERTOR", "", "", "", "CONVERTEDVALUE" + (resAnzahl + 1), new Long( convertedValue ).toString(), "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );

				resAnzahl++;
			}

		} catch( PPExecutionException ppe ) {
			if( DEBUG ) {
				ppe.printStackTrace();
			}
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "PPExecutionException", ppe.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			if( DEBUG ) {
				e.printStackTrace();
			}
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ) + e.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( DEBUG ) {
				t.printStackTrace();
			}
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ) + t.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}
		setPPStatus( info, status, ergListe );
	}
}

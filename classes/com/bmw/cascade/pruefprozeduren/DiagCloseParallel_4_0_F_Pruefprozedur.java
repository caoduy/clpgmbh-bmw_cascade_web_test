/*
 * DiagClose_19_0_F_Pruefprozedur.java
 *
 * Created on 06.09.06
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;


import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.devices.DeviceInfo;
import com.bmw.cascade.pruefstand.devices.DeviceInfo.DeviceInfoDetail;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.devices.ediabas.*;
import com.bmw.cascade.util.*;
//import com.bmw.cascade.util.logging.CascadeLogManager;
import com.bmw.cascade.util.logging.CascadeLogging;
import com.bmw.appframework.logging.*;

/**
 * Beendet die Diagnoseverbindung und gibt somit EDIABAS frei
 *
 * @author Winkler, Mueller, Schumann
 * @version Implementierung <br>
 * 			21.08.2008 CS 2_0_T Erweiterung, sodass die Devices IFH- und API-Trace FGNR spezifisch zugeschalten werden k�nnen	
 * 			29.07.2014 CW 3_0_T  Parametergewinnnung angepasst, sodass Parameter nur �ber CASCADE Standard-Methoden geholt werden
 * 			29.07.2014 CW 4_0_F  Freigabe 2_0_T und 3_0_T
 *
 */
public class DiagCloseParallel_4_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    	static final long serialVersionUID = 1L;
    	//static final String PARALLEL = "PARALLEL";
    	//static final String TAG = "TAG";
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public DiagCloseParallel_4_0_F_Pruefprozedur() {}

  /**
     * erzeugt eine neue Pruefprozedur.
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public DiagCloseParallel_4_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    /**
     * initialsiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /**
     * liefert die optionalen Argumente
     */
    public String[] getOptionalArgs() {
        String[] args = { "EXTERN", "TAG"};
        return args;
    }
    
    /**
     * liefert die zwingend erforderlichen Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = new String[]{"PARALLEL"};
        return args;
    }
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
     */
    public boolean checkArgs() {
        try {
            return super.checkArgs();
        } catch (Exception e) {
            return false;
        } catch (Throwable e) {
            return false;
        }
    }
    
    
    /**
     * f�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        // spezifische Variablen
        String temp = null;
        String tracePath = null;
        boolean extern = false;
        
        EdiabasProxyThread ediabas = null;
        EdiabasProxyThread ept[] = null;
        boolean parallel = false;
        String tag = null;
        
        /***********************************************
         * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
         * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
         ***********************************************/
        try {
            UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
            myAnalyser.LogSetTestStepName(this.getName());
            getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
        } catch (Exception e) {
            try {
                getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
            }
            catch (Exception e1) {}
        }
        
        try {
        	
            try {
                //Parameter holen
                if( checkArgs() == false ) throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
                
				//Parallelmode an/aus
				parallel = Boolean.valueOf(getArg("PARALLEL")).booleanValue();
                
                if ( getArg(getOptionalArgs()[0]) != null ) {
                    if ( getArg(getOptionalArgs()[0]).equals("TRUE") ) extern = true;
                }
                
                if ( getArg("TAG") != null ) {
                    tag = getArg("TAG");
                }
                
            } catch (PPExecutionException e) {
                if (e.getMessage() != null)
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            if(parallel){
				ept = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabasParallel_ALL();
				if (ept==null || ept.length < 1){
				   // kein Device f�r Paralleldiagnose gefunden
					result = new Ergebnis( "ExecFehler", "EDIABAS_PARALLEL", "", "", "", "", "", "", "", "0", "", "", "", "Exception: No EdiabasParallel device available", "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw (new Exception ("No EdiabasParallel device available!"));
				}

				
            }
            else{
            	ept = new EdiabasProxyThread[]{getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas(tag)};
            }
            
			//ein beliebiges ediabas ausw�hlen
			ediabas = ept[0];
			
            
            if ( extern==false )
                //Initialisierung unter Interface-Bestimmung, wird nur ben�tigt, wenn EXTERN=FALSE, da sonst eh kein Funk angepackt wird
            {
                try {
                    temp = ediabas.executeDiagJob("UTILITY", "INTERFACE", "", "");
                    if( temp.equals("OKAY") == false) {
                        result = new Ergebnis( "DiagFehler", "EDIABAS", "UTILITY", "INTERFACE", "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
                        ergListe.add(result);
                        throw new PPExecutionException();
                    }
                    temp = ediabas.getDiagResultValue("TYP").toUpperCase();
                    result = new Ergebnis( "Interface", "EDIABAS", "UTILITY", "INTERFACE", "", "TYP", temp, temp, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                    ergListe.add(result);
                } catch( ApiCallFailedException e ) {
                    result = new Ergebnis( "DiagFehler", "EDIABAS", "UTILITY", "INTERFACE", "", "TYP", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
                    ergListe.add(result);
                    throw new PPExecutionException();
                } catch( EdiabasResultNotFoundException e ) {
                    if (e.getMessage() != null)
                        result = new Ergebnis( "DiagFehler", "EDIABAS", "UTILITY", "INTERFACE", "", "TYP", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                    else
                        result = new Ergebnis( "DiagFehler", "EDIABAS", "UTILITY", "INTERFACE", "", "TYP", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
                    ergListe.add(result);
                    throw new PPExecutionException();
                }
            }
            
            
            
            // wird nur gemacht wenn FUNK vorhanden ist UND falls EXTERN=FALSE
            // notwendig um EDIABAS freizugeben (z.B. NFS wo Funk immer noch notwendig ist)
            if ( extern == false ) {
                if ( temp.equals("FUNK") == true ) {
                    // Satistik Zaehler im Master auslesen
                    try {
                        temp = ediabas.executeDiagJob("IFR", "STATUS_STATISTIK_MASTER", "", "");
                        if( temp.equals("OKAY") == false) {
                            result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "STATUS_STATISTIK_MASTER", "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_IGNORE );
                            ergListe.add(result);
                        } else {
                            temp = ediabas.getDiagResultValue("STAT_ABBRUCH");
                            result = new Ergebnis( "Diagnose", "EDIABAS", "IFR", "STATUS_STATISTIK_MASTER", "", "STAT_ABBRUCH", temp, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                            ergListe.add(result);
                            temp = ediabas.getDiagResultValue("STAT_WIEDERHOLUNGEN");
                            result = new Ergebnis( "Diagnose", "EDIABAS", "IFR", "STATUS_STATISTIK_MASTER", "", "STAT_WIEDERHOLUNGEN", temp, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                            ergListe.add(result);
                            temp = ediabas.getDiagResultValue("STAT_VERBUNDENER_SLAVE");
                            result = new Ergebnis( "Diagnose", "EDIABAS", "IFR", "STATUS_STATISTIK_MASTER", "", "STAT_VERBUNDENER_SLAVE", temp, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                            ergListe.add(result);
                        }
                    } catch ( ApiCallFailedException e ) {
                        result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "STATUS_STATISTIK_MASTER", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_IGNORE );
                        ergListe.add(result);
                    } catch( EdiabasResultNotFoundException e ) {
                        if (e.getMessage() != null)
                           result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "STATUS_STATISTIK_MASTER", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_IGNORE );
                        else
                           result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "STATUS_STATISTIK_MASTER", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_IGNORE );
                        ergListe.add(result);
                    } catch ( Exception e ){
                    	CascadeLogging.getLogger().log( LogLevel.WARNING, "Funkverhau nicht in Ordnung!!! / Wrong version for radio adaptor software!!!", e);
                        e.printStackTrace();
                    } catch (Throwable t){
                    	CascadeLogging.getLogger().log( LogLevel.WARNING, "Funkverhau nicht in Ordnung!!! / Wrong version for radio adaptor software!!!", t);
                        t.printStackTrace();
                    }
                    //Funk beenden
                    try {
                        temp = ediabas.executeDiagJob("IFR", "ENDE_PRUEFUNG", "", "");
                        if( temp.equals("OKAY") == false) {
                            result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "ENDE_PRUEFUNG", "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
                            ergListe.add(result);
                            throw new PPExecutionException();
                        }
                    } catch( ApiCallFailedException e ) {
                        result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "ENDE_PRUEFUNG", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
                        ergListe.add(result);
                        throw new PPExecutionException();
                    } catch( EdiabasResultNotFoundException e ) {
                        if (e.getMessage() != null)
                            result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "ENDE_PRUEFUNG", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                        else
                            result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "ENDE_PRUEFUNG", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
                        ergListe.add(result);
                        throw new PPExecutionException();
                    }
                } //Ende Funk
                
            }
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
        // Lese den Tracepath aus - wird ben�tigt, um die Traces bei N.i.o - Pr�fungen zu bearbeiten
        try {
            tracePath=ediabas.apiGetConfig("TracePath");
        }catch (Exception e) {
        	CascadeLogging.getLogger().log( LogLevel.WARNING, PB.getString(" Exception during tracesave: "), e);
        }catch (Throwable t) {
        	CascadeLogging.getLogger().log( LogLevel.WARNING, PB.getString(" Throwable during tracesave: "), t);
        }
        
        for (int i = 0; i < ept.length; i++){
        	ediabas = ept[i];
        	try {
        		ediabas.apiEnd();
        	} catch (NoClassDefFoundError e) {
        		result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Exception: EDIABAS DLL (apijav32.dll) ", "", Ergebnis.FT_NIO_SYS );
        		ergListe.add(result);
        	} catch (Exception e) {
        		result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Exception: apiend()", e.toString(), Ergebnis.FT_NIO_SYS );
        		ergListe.add(result);
        	}
        }
        
        if (status == STATUS_EXECUTION_OK) {
			result = new Ergebnis( "Close", "EDIABAS", "", "", "", "STATUS", "OK", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );

        }else {
			result = new Ergebnis( "Close", "EDIABAS", "", "", "", "STATUS", "NOK", "", "", "", "", "", "", "", "", Ergebnis.FT_NIO );
			ergListe.add( result );       	
        }
        
        
        // Bearbeitung der EDIABAS-Traces
        if (extern==false) {
            try {
                // API-Trace?
            	DeviceInfo apiTrace = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabasApiTrace();
                if (tracePath!=null){
                    try {
                    	processTrace(tracePath, apiTrace); 
                    }catch(Exception acfe) {
                    	CascadeLogging.getLogger().log( LogLevel.WARNING, PB.getString(" Exception during tracesave: "), acfe);
                    	
                    }
                }
            } catch (Exception e) {
                // keine Fehlerbehandlung, Trace war nicht aktiviert
            } catch (Throwable t) {
                // keine Fehlerbehandlung
            }
            
            try {
                // IFH-Trace?
            	DeviceInfo ifhTrace = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabasIfhTrace();
                if (tracePath!=null)
                    try {
                    	processTrace(tracePath, ifhTrace);
                    }catch(Exception acfe) {
                    	CascadeLogging.getLogger().log( LogLevel.WARNING, PB.getString(" Exception during tracesave: "),acfe);
                    }
            } catch (Exception e) {
                // keine Fehlerbehandlung, Trace war nicht aktiviert
            } catch (Throwable t) {
            	// keine Fehlerbehandlung
            }
        }
        
        try {
            // cascade aufr�umen
        	getPr�flingLaufzeitUmgebung().getDeviceManager().releaseEdiabas();
            getPr�flingLaufzeitUmgebung().getDeviceManager().releaseEdiabasParallel_ALL();
            getPr�flingLaufzeitUmgebung().setPruefstandVariable(VariableModes.TEMPORARY, "DIAG_PARALLEL", Boolean.FALSE );
        } catch (Exception e) {
            // keine Fehlerbehandlung, Trace war nicht aktiviert
        } catch (Throwable t) {
            
    		result = new Ergebnis( "ExecFehler", "EDIABASPARALLEL", "", "", "", "", "", "", "", "0", "", "", "", "Wrong Cascade version!", t.toString(), Ergebnis.FT_NIO_SYS );
    		ergListe.add(result);
        }
        
        
        
        
        setPPStatus( info, status, ergListe );
    }
    
    /**
	 * �berpr�ft wie der API- oder IFH-Trace konfiguriert wurde und erstellt aufgrund der Konfiguration
	 * Trace-Dateien.
	 * Konfiguration:
	 * 	- FGNR in der Tag-Spalte, einzeln oder im String durch ';' getrennt -> nur bei diesen FGNRs wird der Trace
	 * 	  generell angeschalten
	 *  - Tag-Spalte leer -> f�r alle Fahrzeuge wird ein Trace erstellt, wenn die Pr�fung n.i.O. war
	 * @param extern
	 * @param Debug
	 * @param trace - Api- oder Ifh-Trace
	 */
	private void processTrace(String tracePath, DeviceInfo trace) throws Exception{
		String fgnr="";
		ArrayList fgnrArray=null;

		try {
			//API oder IFH-Trace
			String traceFileName="";
			if(trace.getName().equalsIgnoreCase("EdiabasApiTrace")){
				traceFileName="api.trc";
			}else if(trace.getName().equalsIgnoreCase("EdiabasIfhTrace")){
				traceFileName="ifh.trc";
			}
			File traceFile=new File(tracePath,traceFileName);
			
			//�berpr�fen, ob FGNRs in der Tag-Spalte in der Konfiguration angegeben wurde
			for(int i=0;i<trace.getDetailCount();i++){
				DeviceInfoDetail detail = trace.getDetail(i);
				if(!detail.getTag().equalsIgnoreCase("")){
					fgnr = detail.getTag();
					StringTokenizer st = new StringTokenizer(fgnr,";");
					fgnrArray=new ArrayList(st.countTokens());
					while(st.countTokens()>0){
						fgnrArray.add(st.nextElement());
					}	
				}
			}
			
			//nur Trace-File erstellen, wenn entsprechende FGNR gepr�ft wird
			if(fgnr.equalsIgnoreCase(getPr�fling().getAuftrag().getFahrgestellnummer7())){
		            createTrace(traceFile);
			//wenn mehrere FGNRs konfiguriert wurden
			}else if(fgnrArray.size()>1){
				for(int i=0; i<fgnrArray.size();i++){
					fgnr = (String)fgnrArray.get(i);
					//nur Trace-File erstellen, wenn entsprechende FGNR gepr�ft wird
					if(fgnr.equalsIgnoreCase(getPr�fling().getAuftrag().getFahrgestellnummer7())){
						createTrace(traceFile);
						break;
					}
				}
			//FGNR wurde nicht konfiguriert - Trace-File erstellen, wenn Pr�fung n.i.O.
			}else if(fgnr.equalsIgnoreCase("")||this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getLastExecStatus() != STATUS_EXECUTION_OK ){
				createTrace(traceFile);
			//Pr�fung war i.O. -> Trace-File wird gel�scht
			}else{
				if (traceFile.exists()) traceFile.delete();
			}
		} catch( Exception acfe ) {
			CascadeLogging.getLogger().log( LogLevel.WARNING, getPr�fling().getAuftrag().getFahrgestellnummer7() + " Exception API/IFH-Trace Level " + trace.getPort() + ": ", acfe );
		}
	}

	/**
	 * Schiebt das Trace-File in den Cascade/trace/pr�fstand/error Ordner.
	 * @param traceFile
	 */
	private void createTrace(File traceFile) {
		File saveFile =new File(com.bmw.cascade.pruefstand.PB.getString("cascade.pruefstand.auftrag.reporterrorpath"),
		this.getPr�fling().getAuftrag().getFahrgestellnummer7()+ "_" + this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getName() + "_" +System.currentTimeMillis()+"_" + traceFile.getName().toUpperCase());
		FileUtils.moveFile(traceFile,saveFile);
	}

}


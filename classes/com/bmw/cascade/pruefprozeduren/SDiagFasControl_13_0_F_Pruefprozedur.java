package com.bmw.cascade.pruefprozeduren;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.pruefprozeduren.PPExecutionException;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.fascontrol.*;


/**
 * Implementierung der Pr�fprozedur zur Ablaufsteuerung am FAS Pr�fstand
 * (Fahrerassistenzsystem)<BR>
 * Created on 18.05.06 <BR>
 * <BR>
 * 
 * @author S. Pichler (SP), R. Schollmeier (RS), K. Petersen (KP) - BMW AG, TI-432 <BR> 
 * 		   P. Jorge (PJ) - IndustrieHansa AG, TI-432 <BR>
 * 		   U. Plath (UP) - BMW AG, TP-432<br>
 * 			
 * @version 1_0 SP 30.05.2006 Ersterstellung, Grundstruktur zur
 *          Prozess-Ausf�hrung angelegt.<BR>
 * @version 2_0 SP 14.06.2006 Anpassung bei der Berechnung des Symmetriewinkels,
 *          Plausibilit�tstest (z) hinzugef�gt.<BR>
 * @version 3_0 RS, SP 20.07.2006 Prozeduren zur Ansteuerung des ACC Spiegels
 *          hinzugefuegt, Optionalen Parameter TAG_DEVICE_NAME zur Wahl des
 *          entsprechenden Devices hinzugefuegt.
 * @version 4_0 PJ 30.08.2006
 * 			Added support for @construct argument values through the added extractOptArgs private method.
 * 			General review of the code and rewrite of some parts.   
 * 			Debug flag added as optional parameter. Log function added.
 * 			Updated SET_VALUE arg range (0..6) on job POSITION_GANTRY_FRONT. 
 * 			Height plausibility test corrected. 
 * @version 5_0 PJ 13.09.2006
 * 			Added support for the new return value "RFKZIEL" in the required jobs.
 * @version 6_0 PJ 26.09.2006
 * 			Change job TEST_STAND_CALIBRATION to TEST_STAND_SHORT_VERIFICATION.
 * 			Correction of result string blank space bug.
 * 			Height measurement values made available to CASCADE environment as float values rounded 
 * 			to the 2nd decimal place.   
 * @version 7_0 PJ 19.10.2006
 * 			Added optional parameter CHECK_MACHINE to check machine mode & error - default is TRUE.
 * 			Added optional parameter SIMULATION to use in simulation mode - default is FALSE.
 * 			Added round function to round a float value to n decimal places. 
 * 			All height measurement values are now rounded to the 2nd decimal place.
 * 			Added optional parameter POLLING_TIME_MS to control the polling time in the WAIT_FOR_SWITCH_ACTIVATION 
 * 			and WAIT_FOR_VEHICLE_INSIDE_TEST_STAND jobs.
 * 			Improved the APDM error messages.
 * @version 8_0 PJ 17.06.2006
 * 			Added MittenVersatz computation in job FZG_VERMESSUNG.
 * 			Added the STAND_DIALOG, POSITION_GANTRY_REAR, POSITION_GANTRY_REAR_HOME, ACTIVATE_GANTRY_REAR_TARGET jobs 
 * 			and changed some old jobs' names. Each JOB - each check args and execute functions.
 * 			Structured APDM results implementation and input arguments names.  
 * 			New CASCADE interface implemented.
 * @version 9_0 UP 17.02.2009
 * 			Added additional argument "FLA" in Job ACTIVATE_GANTRY_FRONT_TARGET for introduction in new F25-DAS-Stand.
 * @version 10_0 UP 08.07.2009
 * 			Corrected a bug in the test stand calibration procedure where ACC wouldn't calibrate.
 * @version 11_0 UP 11.12.2009
 * 			Changed behavior of detecting manual mode on FAS rig. Now continuation is possible.
 * @version 12_0 UP 12.02.2015
 * 			Added function for parameter 'MESSAGE' that automatically resolves @-constructs
 */
public class SDiagFasControl_13_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	static final String VERSION = "Final_v12_25jan10";

	
	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public SDiagFasControl_13_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
     *
	 * @param pruefling
	 *            Klasse des zugeh. Prueflings
	 * @param pruefprozName
	 *            Name der Pruefprozedur
	 * @param hasToBeExecuted
	 *            Ausfuehrbedingung f�r Fehlerfreiheit
	 */
	public SDiagFasControl_13_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
		super(pruefling, pruefprozName, hasToBeExecuted);
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = {
				"JOB"
		};
		return args;
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = {
				"ID",
				"CMD",
				"MIN",
				"MAX",
				"X",
				"Y",
				"Z",
				"PHI",
				"THETA",
				"TIMEOUT",
				"WHEEL_BASE",
				"TOL_ACC",
				"TOL_GF",
				"TOL_GR",
				"TOL_HEIGHTS",
				"TOL_SYMMETRY",
				"POLLING_TIME",
				"MESSAGE",
				"DEVICE_TAG_NAME",
				"CHECK_MACHINE",
				"SIMULATION",
				"DEBUG"
		};
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 * �berschreibt die parent-Methode aufgrund der offenen Anzahl an Results
	 * 
	 * @return true, wenn �berpruefung der Argumente erfolgreich
	 */
	public boolean checkArgs() 
	{
		String job = getArg("JOB");
		boolean debug = (getArg("DEBUG") != null) && (getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
		
		try {
			if (!super.checkArgs()) {
				throw new Exception("super.checkArgs() failed!");
			}
						
			// POSITION_GANTRY_FRONT_HOME
			if (job.equalsIgnoreCase("POSITION_GANTRY_FRONT_HOME")) {
				check_POSITION_GANTRY_FRONT_HOME();
			}
			// POSITION_GANTRY_FRONT
			else if (job.equalsIgnoreCase("POSITION_GANTRY_FRONT")) {
				check_POSITION_GANTRY_FRONT();
			}
			// ACTIVATE_GANTRY_FRONT_TARGET
			else if (job.equalsIgnoreCase("ACTIVATE_GANTRY_FRONT_TARGET")) {
				check_ACTIVATE_GANTRY_FRONT_TARGET();
			}
			// ROTATE_ACC
			else if (job.equalsIgnoreCase("ROTATE_ACC")) {
				check_ROTATE_ACC();
			}
			// CONTROL_CENTRATION
			else if (job.equalsIgnoreCase("CONTROL_CENTRATION")) {
				check_CONTROL_CENTRATION();
			}
			// CONTROL_NIVI_WIRE
			else if (job.equalsIgnoreCase("CONTROL_NIVI_WIRE")) {
				check_CONTROL_NIVI_WIRE();
			}
			// CONTROL_TARGET_ILLUMINATION
			else if (job.equalsIgnoreCase("CONTROL_TARGET_ILLUMINATION")) {
				check_CONTROL_TARGET_ILLUMINATION();
			}
			// CONTROL_SWITCH_ILLUMINATION
			else if (job.equalsIgnoreCase("CONTROL_SWITCH_ILLUMINATION")) {
				check_CONTROL_SWITCH_ILLUMINATION();
			}
			// CONTROL_HC2_DOPPLER
			else if (job.equalsIgnoreCase("CONTROL_HC2_DOPPLER")) {
				check_CONTROL_HC2_DOPPLER();
			}
			// WAIT_SWITCH_ACTIVATION
			else if (job.equalsIgnoreCase("WAIT_SWITCH_ACTIVATION")) {
				check_WAIT_SWITCH_ACTIVATION();
			}
			// WAIT_VEHICLE_INSIDE_STAND
			else if (job.equalsIgnoreCase("WAIT_VEHICLE_INSIDE_STAND")) {
				check_WAIT_VEHICLE_INSIDE_STAND();
			}
			// MEASURE_VEHICLE
			else if (job.equalsIgnoreCase("MEASURE_VEHICLE")) {
				check_MEASURE_VEHICLE();
			}
			// MEASURE_STAND
			else if (job.equalsIgnoreCase("MEASURE_STAND")) {
				check_MEASURE_STAND();
			}
			// STAND_TYPE
			else if (job.equalsIgnoreCase("STAND_TYPE")) {
				check_STAND_TYPE();
			}
			// STAND_DIALOG
			else if (job.equalsIgnoreCase("STAND_DIALOG")) {
				check_STAND_DIALOG();
			}
			// POSITION_GANTRY_REAR_HOME
			else if (job.equalsIgnoreCase("POSITION_GANTRY_REAR_HOME")) {
				check_POSITION_GANTRY_REAR_HOME();
			}
			// POSITION_GANTRY_REAR
			else if (job.equalsIgnoreCase("POSITION_GANTRY_REAR")) {
				check_POSITION_GANTRY_REAR();
			}
			// ACTIVATE_GANTRY_REAR_TARGET
			else if (job.equalsIgnoreCase("ACTIVATE_GANTRY_REAR_TARGET")) {
				check_ACTIVATE_GANTRY_REAR_TARGET();
			}
			// job undefined
			else {
				throw new Exception("job "+ job +" undefined!");
			}
			return true;
			
		} catch (NumberFormatException e) {
			log(debug, "Error", "checking args, NumberFormatException: "+ e.getMessage()); 
			return false;
		} catch (Exception e) {
			log(debug, "Error", "checking args, Exception: "+ e.getMessage()); 
			return false;
		}
	}
	
	/**
	 * fuehrt die Pruefprozedur aus
	 * 
	 * @param info,
	 *            Information zur Ausfuehrung
	 */
	public void execute(ExecutionInfo info) 
	{
		// CASCADE parameters
		String job = getArg("JOB");
		String fasTag = (getArg("DEVICE_TAG") != null) ? getArg("DEVICE_TAG") : null;
		String message = (getArg("MESSAGE") != null) ? getArg("MESSAGE") : null;
		boolean debug = ((getArg("DEBUG") != null) && (getArg("DEBUG").equalsIgnoreCase("TRUE"))) ? true : false;
		boolean checkMachine = ((getArg("CHECK_MACHINE") != null) && (getArg("CHECK_MACHINE").equalsIgnoreCase("FALSE"))) ? false : true;
		boolean simulation = ((getArg("SIMULATION") != null) && (getArg("SIMULATION").equalsIgnoreCase("TRUE"))) ? true : false;
		// aux
		int status = STATUS_EXECUTION_OK;
		int dllResult = 0;
		Vector resList = new Vector();
		UserDialog ud = null;
		FasControl fas = null;
		String deviceName = "";
		boolean german = System.getProperty("user.language").equalsIgnoreCase("de");
		String resAdvise = german ? "Bitte den Pr�fstandsbetreuer benachrichtigen" : "Please inform the person responsible for the test stand";
		
		try {
			// -------------------------------------------------------------------------------------------
        	// check args
			// -------------------------------------------------------------------------------------------
			log(debug, "Var", "---> "+ getClassName() +" ("+ VERSION +") [sim: "+ String.valueOf(simulation) +", mach: "+ String.valueOf(checkMachine) +"]"); 
			if (!checkArgs()) {
        		throw new PPExecutionException(PB.getString("parameterexistenz"));
        	}
    		log(debug, "Ok", "check args ok."); 
    		// -------------------------------------------------------------------------------------------
        	// get devices - UserDialog & FasControl
			// -------------------------------------------------------------------------------------------
			try {
				// UserDialog
				// ----------------
				deviceName = "UserDialog";
				ud = getPr�flingLaufzeitUmgebung().getUserDialog();					
				if (ud == null) {
					throw new Exception("null");
				}
				log(debug, "Ok", "got "+ deviceName +" device.");
				
				// FasControl
				// ----------------
				if (!simulation) {
					deviceName = "FasControl";
					fas = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getFasControl(fasTag);
					if (fas == null) {
						throw new Exception("null");
					}
					log(debug, "Ok", "got "+ deviceName +" device.");
				}
		    		
			} catch (DeviceLockedException e) {
				log(debug, "Error", "getting "+ deviceName +" device, DeviceLockedException: "+ e.getMessage());
				throw new PPExecutionException(deviceName +" "+ PB.getString("nichtVerfuegbar"));
			} catch (DeviceNotAvailableException e) {
				log(debug, "Error", "getting "+ deviceName +" device, DeviceNotAvailableException: "+ e.getMessage());
				throw new PPExecutionException(deviceName +" "+ PB.getString("nichtVerfuegbar"));
			} catch (Exception e) {
				log(debug, "Error", "getting "+ deviceName +" device, Exception: "+ e.getMessage());
				throw new PPExecutionException(deviceName +" "+ PB.getString("nichtVerfuegbar"));
			}	
			// -------------------------------------------------------------------------------------------
			// machine checks
			// -------------------------------------------------------------------------------------------
			if (checkMachine) {
				String dlgTitle = "";
				String dlgMsg = "";
				String exceptionMsg = "";
				try {
    				// check machine errors...
					log(debug, "Ok", "checking machine errors...");
					dllResult = (simulation) ? 0 : fas.machineError();
					if (dllResult != 0) { 
						// NIO - Machine error!
						if (dllResult == 1) {
				    		dlgTitle = german ? "Pr�fstandsfehler" : "Test stand error"; 
				    		dlgMsg = fas.errorInfo() + (german ? "\nBitte den Pr�fumfang abbrechen (Taste F7)..." : "\nPlease cancel the test sequence (press F7)...");
						}
						exceptionMsg = (dllResult == 1) ? "MachineError" : getDllErrorMsg(String.valueOf(dllResult));
			    		log(debug, "Error", "checking machine errors: "+ exceptionMsg +" ("+ String.valueOf(dllResult) +")");			    		
			    		throw new Exception(exceptionMsg);
					}
					// check machine operating mode...	
					log(debug, "Ok", "Checking machine operating mode...");
					
					dllResult = (simulation) ? 2 : fas.machineOpMode();
					if (dllResult != 2) {
						// NIO - Machine not in automatic operating mode!
						if (dllResult == 1) {
				    		dlgTitle = german ? "Pr�fstand im Handmodus" : "Test stand in manual mode"; 
				    		dlgMsg = german ? "Bitte den Pr�fstand in den Automatikmodus schalten!" : "Please switch the test stand to automatic mode!";
						}
						// Show error, let user klick away message and try again
						ud.requestAlertMessage(dlgTitle, dlgMsg, 0);
						
						dllResult = (simulation) ? 2 : fas.machineOpMode();
						if (dllResult != 2) {
							exceptionMsg = (dllResult == 1) ? "Test stand in manual mode" : getDllErrorMsg(String.valueOf(dllResult));
							log(debug, "Error", "Checking machine operating mode: "+ exceptionMsg +" ("+ String.valueOf(dllResult) +")");
							throw new Exception(exceptionMsg);
						}
					} else {
						log(debug, "Ok", "Machine checks ok.");
					}						    		
    			} catch (Exception e) {
    				if (!dlgTitle.equalsIgnoreCase("") && !dlgMsg.equalsIgnoreCase("")) {
    					if (!this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().isStopped()) {
        					// show error
        					//ud.requestAlertMessage(dlgTitle, dlgMsg, 0);
            				// stop PU!
    						this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().stopExecution();
    						log(debug, "Error", "...PU stopped! "+ e.getMessage());
    					}	
    				}
					throw new PPExecutionException(e.getMessage());
				}
    		}	 			
			// -------------------------------------------------------------------------------------------
    		// job execution
			// -------------------------------------------------------------------------------------------
			log(debug, "Ok", "executing job... "+ job);
			
			try {
				if (message != null) {
					// show job message
					message = extractArg( "MESSAGE" );
					ud.displayStatusMessage("FAS", message, (simulation ? 2 : -1));
				}
			} catch (InformationNotAvailableException e) {
				log(debug, "Error", "getting value of " + message + " , Exception: "+ e.getMessage());
				throw new PPExecutionException(PB.getString( "infoNichtGefunden" ) + " ('" + message +"')");
			}
			
			// ---------------------
			// POSITION_GANTRY_FRONT_HOME
			// ---------------------
			// Vorderes Gantry in Home Position verfahren
			if (job.equalsIgnoreCase("POSITION_GANTRY_FRONT_HOME")) {
				status = execute_POSITION_GANTRY_FRONT_HOME(fas, resList);
			}
			// ---------------------
			// POSITION_GANTRY_FRONT
			// ---------------------
			// Target am vorderen Gantry positionieren
			else if (job.equalsIgnoreCase("POSITION_GANTRY_FRONT")) {
				status = execute_POSITION_GANTRY_FRONT(fas, resList);
			}
			// ---------------------
			// ACTIVATE_GANTRY_FRONT_TARGET
			// ---------------------
			// Schwenkt den vorderen Portalarm so, dass das angegebene
			// Target nach vorne gerichtet ist
			else if (job.equalsIgnoreCase("ACTIVATE_GANTRY_FRONT_TARGET")) {
				status = execute_ACTIVATE_GANTRY_FRONT_TARGET(fas, resList);
			}
			// ---------------------
			// ROTATE_ACC
			// ---------------------
			// ACC Spiegel um PHI und THETA absolut/relativ in Hundertstel-Grad
			// schwenken
			else if (job.equalsIgnoreCase("ROTATE_ACC")) {
				status = execute_ROTATE_ACC(fas, resList);
			}
			// ---------------------
			// CONTROL_CENTRATION
			// ---------------------
			// Fahrzeug (de-)zentrieren
			else if (job.equalsIgnoreCase("CONTROL_CENTRATION")) {
				status = execute_CONTROL_CENTRATION(fas, resList);
			}
			// ---------------------
			// CONTROL_NIVI_WIRE
			// ---------------------
			// NiVi Heizdraht ein-/ausschalten
			else if (job.equalsIgnoreCase("CONTROL_NIVI_WIRE")) {
				status = execute_CONTROL_NIVI_WIRE(fas, resList);
			}
			// ---------------------
			// CONTROL_TARGET_ILLUMINATION
			// ---------------------
			// TLC/RVC Target-Beleuchtung ein-/ausschalten
			else if (job.equalsIgnoreCase("CONTROL_TARGET_ILLUMINATION")) {
				status = execute_CONTROL_TARGET_ILLUMINATION(fas, resList);
			}
			// ---------------------
			// CONTROL_SWITCH_ILLUMINATION
			// ---------------------
			// Taster-Beleuchtung steuern
			else if (job.equalsIgnoreCase("CONTROL_SWITCH_ILLUMINATION")) {
				status = execute_CONTROL_SWITCH_ILLUMINATION(fas, resList);
			}
			// ---------------------
			// CONTROL_HC2_DOPPLER
			// ---------------------
			else if (job.equalsIgnoreCase("CONTROL_HC2_DOPPLER")) {
				status = execute_CONTROL_HC2_DOPPLER(fas, resList);
			}
			// ---------------------
			// WAIT_SWITCH_ACTIVATION
			// ---------------------
			// Auf Taster-Bet�tigung warten
			else if (job.equalsIgnoreCase("WAIT_SWITCH_ACTIVATION")) {
				status = execute_WAIT_SWITCH_ACTIVATION(fas, resList);
			}
			// ---------------------
			// WAIT_VEHICLE_INSIDE_STAND
			// ---------------------
			// Wartet auf eingef�rdertes Fahrzeug
			else if (job.equalsIgnoreCase("WAIT_VEHICLE_INSIDE_STAND")) {
				status = execute_WAIT_VEHICLE_INSIDE_STAND(fas, resList);
			}
			// ---------------------
			// MEASURE_VEHICLE
			// ---------------------
			// Fahrzeugvermessung
			else if (job.equalsIgnoreCase("MEASURE_VEHICLE")) {
				status = execute_MEASURE_VEHICLE(fas, resList);
			}
			// ---------------------
			// MEASURE_STAND
			// ---------------------
			// Pr�fstandnachmessung/Pr�fstandkurznachmessung
			else if (job.equalsIgnoreCase("MEASURE_STAND")) {
				status = execute_MEASURE_STAND(fas, resList);
			}				
			// ---------------------
			// STAND_TYPE
			// ---------------------
			// Check test stand type
			else if (job.equalsIgnoreCase("STAND_TYPE")) {
				status = execute_STAND_TYPE(fas, resList);
			}
			// ---------------------
			// STAND_DIALOG
			// ---------------------
			// Test stand switches dialog
			else if (job.equalsIgnoreCase("STAND_DIALOG")) {
				status = execute_STAND_DIALOG(fas, resList);
			}
			// ---------------------
			// POSITION_GANTRY_REAR_HOME
			// ---------------------
			// Hinteres Gantry in Home Position verfahren
			else if (job.equalsIgnoreCase("POSITION_GANTRY_REAR_HOME")) {
				status = execute_POSITION_GANTRY_REAR_HOME(fas, resList);
			}
			// ---------------------
			// POSITION_GANTRY_REAR
			// ---------------------
			// Target am hinteren Gantry positionieren
			else if (job.equalsIgnoreCase("POSITION_GANTRY_REAR")) {
				status = execute_POSITION_GANTRY_REAR(fas, resList);
			}
			// ---------------------
			// ACTIVATE_GANTRY_REAR_TARGET
			// ---------------------
			// Schwenkt den vorderen Portalarm so, dass das angegebene
			// Target nach vorne gerichtet ist
			else if (job.equalsIgnoreCase("ACTIVATE_GANTRY_REAR_TARGET")) {
				status = execute_ACTIVATE_GANTRY_REAR_TARGET(fas, resList);
			}
			
	    } catch (PPExecutionException e) {
	    	// NIO
	    	status = STATUS_EXECUTION_ERROR;
	        resList.add( new Ergebnis("ExecError", "FasControl", job, "PPExecutionException", (e.getMessage()!= null ? e.getMessage() : ""), "", "", "", "", "0", "", "", "", PB.getString("ausfuehrungsfehler"), resAdvise, Ergebnis.FT_NIO) );
			log(debug, "Error", "executing procedure, PPExecutionException: "+ e.getMessage());
			e.printStackTrace();
	    } catch (Exception e) {
	    	// NIO_SYS
	        status = STATUS_EXECUTION_ERROR;
	        resList.add( new Ergebnis("ExecError", "FasControl", job, "Exception", (e.getMessage()!= null ? e.getMessage() : ""), "", "", "", "", "0", "", "", "", PB.getString("unerwarteterLaufzeitfehler"), resAdvise, Ergebnis.FT_NIO_SYS) );
			log(debug, "Error", "executing procedure, Exception: "+ e.getMessage());
			e.printStackTrace();
	    } catch (Throwable e) {
	    	// NIO_SYS
	        status = STATUS_EXECUTION_ERROR;
	        resList.add( new Ergebnis("ExecError", "FasControl", job, "Throwable", (e.getMessage()!= null ? e.getMessage() : ""), "", "", "", "", "0", "", "", "", PB.getString("unerwarteterLaufzeitfehler"), resAdvise, Ergebnis.FT_NIO_SYS) );
			log(debug, "Error", "executing procedure, Throwable: "+ e.getMessage());
			e.printStackTrace();
	    }
	    // -------------------------------------------------------------------------------------------
    	// release devices - UserDialog & FasControl
		// -------------------------------------------------------------------------------------------
		try {
			// UserDialog
			// ----------------
			if (ud != null) {
				deviceName = "UserDialog";
				this.getPr�flingLaufzeitUmgebung().releaseUserDialog();
				log(debug, "Ok", "released "+ deviceName +" device.");
			}
			// HeadlightAiming
			// ----------------
			if (!simulation && fas != null) {
				deviceName = "FasControl";
				this.getPr�flingLaufzeitUmgebung().getDeviceManager().releaseFasControl(fasTag);
				fas = null;
				log(debug, "Ok", "released "+ deviceName +" device.");
			}			
			
		} catch (DeviceLockedException e) {
			log(debug, "Error", "releasing "+ deviceName +" device, DeviceLockedException: "+ e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			log(debug, "Error", "releasing "+ deviceName +" device, Exception: "+ e.getMessage());
			e.printStackTrace();
		}
		// -------------------------------------------------------------------------------------------
    	// set and log results
		// -------------------------------------------------------------------------------------------
		setPPStatus(info, status, resList);
		log(debug, "Var", "Execution status: "+ (status == STATUS_EXECUTION_OK ? "Ok" : "Error"));
		for (int i=0; i<resList.size(); i++) {
			log(debug, "Var", "APDM result "+ i +":\n"+ ((Ergebnis)resList.get(i)).toString());
		}
	}

	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//										CHECK JOBS 	
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * <b>check_POSITION_GANTRY_FRONT_HOME</b>
	 * <br>
	 */
	private void check_POSITION_GANTRY_FRONT_HOME() throws Exception
	{
		// nothing to be tested...
	}
	
	/**
	 * <b>check_POSITION_GANTRY_FRONT</b>
	 * <br>
	 * - req: ID, X, Y, Z
	 * <br>
	 */
	private void check_POSITION_GANTRY_FRONT() throws Exception
	{
		final String[] ID = {/*0*/"No_Offset", /*1*/"Acc", /*2*/"Nivi", /*3*/"Tlc_Up", /*4*/"Tlc_Down", 
	         		/*FIZ:*/ /*5*/"Fla", /*6*/"Rvc", /*7*/"Hc2"}; 

		// ID defined ?
		if (getOptionInt(ID, extractArg("ID")) == -1) {
			throw new Exception("ID invalid! "+ extractArg("ID") +" (opts: "+ getOptionsString(ID) +")");
		}	
		// X int ?
		new Integer(extractArg("X")).intValue(); 
		// Y int ?
		new Integer(extractArg("Y")).intValue(); 
		// Z int ?
		new Integer(extractArg("Z")).intValue(); 
	}
	
	/**
	 * <b>check_ACTIVATE_GANTRY_FRONT_TARGET</b>
	 * <br>
	 * - req: CMD
	 * <br>
	 */
	private void check_ACTIVATE_GANTRY_FRONT_TARGET() throws Exception
	{
		final String[] CMD = {"", "Acc", "Nivi_Tlc", "Fla"};

		// CMD defined ? 
		if (getOptionInt(CMD, extractArg("CMD")) == -1) {
			throw new Exception("CMD invalid! "+ extractArg("CMD") +" (opts: "+ getOptionsString(CMD) +")");
		}	
	}
	
	/**
	 * <b>check_ROTATE_ACC</b>
	 * <br>
	 * - req: ID, PHI, THETA
	 * <br>
	 */
	private void check_ROTATE_ACC() throws Exception
	{
		final String[] ID = {"Relative", "Absolut"};

		// ID defined ?
		if (getOptionInt(ID, extractArg("ID")) == -1) {
			throw new Exception("ID invalid! "+ extractArg("ID") +" (opts: "+ getOptionsString(ID) +")");
		}	
		// PHI int ?
		new Integer(extractArg("PHI")).intValue();
		// THETA int ?
		new Integer(extractArg("THETA")).intValue();
	}
	
	/**
	 * <b>check_CONTROL_CENTRATION</b>
	 * <br>
	 * - req: CMD
	 * <br>
	 */
	private void check_CONTROL_CENTRATION() throws Exception
	{
		final String[] CMD = {"", "Uncenter", "Center"};

		// CMD defined ? 
		if (getOptionInt(CMD, extractArg("CMD")) == -1) {
			throw new Exception("CMD invalid! "+ extractArg("CMD") +" (opts: "+ getOptionsString(CMD) +")");
		}	
	}
	
	/**
	 * <b>check_CONTROL_NIVI_WIRE</b>
	 * <br>
	 * - req: CMD, MIN, MAX, (TIMEOUT)
	 * <br>
	 */
	private void check_CONTROL_NIVI_WIRE() throws Exception
	{
		final String[] CMD = {"", "On", "Off"};

		// CMD defined ? 
		if (getOptionInt(CMD, extractArg("CMD")) == -1) {
			throw new Exception("CMD invalid! "+ extractArg("CMD") +" (opts: "+ getOptionsString(CMD) +")");
		}
		// MIN float ?
		new Float(extractArg("MIN")).floatValue();
		// MAX float ?
		new Float(extractArg("MAX")).floatValue();
		// MIN <= MAX ?
		if (new Float(extractArg("MIN")).floatValue() > new Float(extractArg("MAX")).floatValue()) 
			throw new Exception("MIN = "+ extractArg("MIN") +" > MAX = "+ extractArg("MAX"));  
		// TIMEOUT long > 0 ?
		if (extractArg("TIMEOUT") != null && new Long(extractArg("TIMEOUT")).longValue() <= 0L) {
			throw new Exception("TIMEOUT out of range! (>0): " + extractArg("TIMEOUT"));
		}	
	}
	
	/**
	 * <b>check_CONTROL_TARGET_ILLUMINATION</b>
	 * <br>
	 * - req: ID, CMD
	 * <br>
	 */
	private void check_CONTROL_TARGET_ILLUMINATION() throws Exception
	{
		final String[] ID = {"Tlc", "Rvc"}; 
		final String[] CMD = {"", "On", "Off"}; 

		// ID defined ?
		if (getOptionInt(ID, extractArg("ID")) == -1) {
			throw new Exception("ID invalid! "+ extractArg("ID") +" (opts: "+ getOptionsString(ID) +")");
		}
		// CMD defined ? 
		if (getOptionInt(CMD, extractArg("CMD")) == -1) {
			throw new Exception("CMD invalid! "+ extractArg("CMD") +" (opts: "+ getOptionsString(CMD) +")");
		}	
	}
	
	/**
	 * <b>check_CONTROL_SWITCH_ILLUMINATION</b>
	 * <br>
	 * - req: ID, CMD
	 * <br>
	 */
	private void check_CONTROL_SWITCH_ILLUMINATION() throws Exception
	{
		final String[] ID = {"Start", "Wiederholung", "Abbruch", "Quittieren"}; 
		final String[] CMD = {"Off", "Blink", "On"}; 

		// ID defined ?
		if (getOptionInt(ID, getArg("ID")) == -1) {
			throw new Exception("ID invalid! "+ extractArg("ID") +" (opts: "+ getOptionsString(ID) +")");
		}	
		// CMD defined ?
		if (getOptionInt(CMD, getArg("CMD")) == -1) {
			throw new Exception("CMD invalid! "+ extractArg("CMD") +" (opts: "+ getOptionsString(CMD) +")");
		}	
	}
	
	/**
	 * <b>check_CONTROL_HC2_DOPPLER</b>
	 * <br>
	 * - req: CMD
	 * <br>
	 */
	private void check_CONTROL_HC2_DOPPLER() throws Exception
	{
		final String[] CMD = {"", "On", "Off"};

		// CMD defined ? 
		if (getOptionInt(CMD, extractArg("CMD")) == -1) {
			throw new Exception("CMD invalid! "+ extractArg("CMD") +" (opts: "+ getOptionsString(CMD) +")");
		}	
	}
	
	/**
	 * <b>check_WAIT_SWITCH_ACTIVATION</b>
	 * <br>
	 * - req: ID, CMD, TIMEOUT
	 * <br>
	 * - opt: POLLING_TIME
	 * <br>
	 */
	private void check_WAIT_SWITCH_ACTIVATION() throws Exception
	{
		final String[] ID = {"Start", "Wiederholung", "Abbruch", "Quittieren"}; 
		final String[] CMD = {"Low", "High"}; 

		// ID defined ?
		if (getOptionInt(ID, getArg("ID")) == -1) {
			throw new Exception("ID invalid! " + getArg("ID") +" (opts: "+ getOptionsString(ID) +")");
		}	
		// CMD defined ?
		if (getOptionInt(CMD, getArg("CMD")) == -1) {
			throw new Exception("CMD invalid! ("+ CMD.toString() +"): " + getArg("CMD") +" (opts: "+ getOptionsString(CMD) +")");
		}	
		// TIMEOUT long > 0 ?
		if (new Long(extractArg("TIMEOUT")).longValue() <= 0L) { 
			throw new Exception("TIMEOUT out of range! (>0): " + extractArg("TIMEOUT"));
		}
		// POLLING_TIME long > 0 ?
		if (extractArg("POLLING_TIME") != null && new Long(extractArg("POLLING_TIME")).longValue() <= 0L) { 
			throw new Exception("POLLING_TIME out of range! (>0): " + extractArg("POLLING_TIME"));
		}
	}
	
	/**
	 * <b>check_WAIT_VEHICLE_INSIDE_STAND</b>
	 * <br>
	 * - req: TIMEOUT
	 * <br>
	 * - opt: POLLING_TIME
	 * <br>
	 */
	private void check_WAIT_VEHICLE_INSIDE_STAND() throws Exception
	{
		// TIMEOUT long > 0 ?
		if (new Long(extractArg("TIMEOUT")).longValue() <= 0L) {
			throw new Exception("TIMEOUT out of range! (>0): " + extractArg("TIMEOUT"));
		}	
		// POLLING_TIME long > 0 ?
		if (extractArg("POLLING_TIME") != null && new Long(extractArg("POLLING_TIME")).longValue() <= 0L) { 
			throw new Exception("POLLING_TIME out of range! (>0): " + extractArg("POLLING_TIME"));
		}
	}
	
	/**
	 * <b>check_MEASURE_VEHICLE</b>
	 * <br>
	 * - req: WHEEL_BASE
	 * <br>
	 */
	private void check_MEASURE_VEHICLE() throws Exception
	{
		// WHEEL_BASE int > 0 ?
		if (new Integer(extractArg("WHEEL_BASE")).intValue() <= 0) {
			throw new Exception("WHEEL_BASE out of range! (>0): " + extractArg("WHEEL_BASE"));
		}	
	}
	
	/**
	 * <b>check_MEASURE_STAND</b>
	 * <br>
	 * - req: ID
	 * <br>
	 * - opt: TOL_ACC, TOL_GF, TOL_GR, TOL_HEIGHTS, TOL_SYMMETRY
	 * <br>
	 */
	private void check_MEASURE_STAND() throws Exception
	{
		final String[] ID = {"Verification", "Calibration"};
		
		// ID defined ?
		if (getOptionInt(ID, getArg("ID")) == -1) {
			throw new Exception("ID invalid! " + getArg("ID") +" (opts: "+ getOptionsString(ID) +")");
		}	
		// TOL_ACC float ?
		if (extractArg("TOL_ACC") != null) {
			new Float(extractArg("TOL_ACC")).floatValue(); 
		}
		// TOL_GF float ?
		if (extractArg("TOL_GF") != null) {
			new Float(extractArg("TOL_GF")).floatValue(); 
		}
		// TOL_GR float ?
		if (extractArg("TOL_GR") != null) {
			new Float(extractArg("TOL_GR")).floatValue(); 
		}
		// TOL_HEIGHTS float ?
		if (extractArg("TOL_HEIGHTS") != null) {
			new Float(extractArg("TOL_SYMMETRY")).floatValue(); 
		}
		// TOL_SYMMETRY float ?
		if (extractArg("TOL_SYMMETRY") != null) {
			new Float(extractArg("TOL_SYMMETRY")).floatValue();
		}
	}
	
	/**
	 * <b>check_STAND_TYPE</b>
	 * <br>
	 * - req: ID
	 * <br>
	 */
	private void check_STAND_TYPE() throws Exception
	{
		final String[] ID = {"Basic", "High"}; 

		// ID defined ?
		if (getOptionInt(ID, extractArg("ID")) == -1) {
			throw new Exception("ID invalid! "+ extractArg("ID") +" (opts: "+ getOptionsString(ID) +")");
		}
	}
	
	/**
	 * <b>check_STAND_RESOURCE</b>
	 * <br>
	 * - req: ID, CMD, TIMEOUT, DEVICE_TAG_NAME, (POLLING_TIME)
	 * <br>
	 */
	private void check_STAND_RESOURCE() throws Exception
	{
		final String[] ID = {"Diagnose"}; 
		final String[] CMD = {"Block", "Free"};
		
		// ID defined ?
		if (getOptionInt(ID, extractArg("ID")) == -1) {
			throw new Exception("ID invalid! "+ extractArg("ID") +" (opts: "+ getOptionsString(ID) +")");
		}
		// CMD defined ? 
		if (getOptionInt(CMD, extractArg("CMD")) == -1) {
			throw new Exception("CMD invalid! "+ extractArg("CMD") +" (opts: "+ getOptionsString(CMD) +")");
		}					
		// DEVICE_TAG_NAME empty ?
		if (extractArg("DEVICE_TAG_NAME").equalsIgnoreCase("")) {
			throw new Exception("DEVICE_TAG_NAME empty!");
		}
		// TIMEOUT long > 0 ?
		if (new Long(extractArg("TIMEOUT")).longValue() <= 0L) {
			throw new Exception("TIMEOUT out of range! (>0): "+ extractArg("TIMEOUT"));
		}	
		// POLLING_TIME long > 0 ?
		if (extractArg("POLLING_TIME") != null && new Long(extractArg("POLLING_TIME")).longValue() <= 0L) { 
			throw new Exception("POLLING_TIME out of range! (>0): " + extractArg("POLLING_TIME"));
		}
	}
	
	/**
	 * <b>check_STAND_DIALOG</b>
	 * <br>
	 * - req: MESSAGE, (ID ;split), (TIMEOUT)
	 * <br>
	 * - opt: POLLING_TIME
	 * <br>
	 */
	private void check_STAND_DIALOG() throws Exception
	{
		final String[] ID = {"Start", "Wiederholung", "Abbruch", "Quittieren"}; 
		
		// MESSAGE defined ?
		new String(getArg("MESSAGE"));
		
		// ID (;split) defined ?
		if (getArg("ID") == null) { 					
			// no dialog switches set -> timeout required !
			// ---------------------------------------------
			// TIMEOUT long > 0 ?
			if (new Long(extractArg("TIMEOUT")).longValue() <= 0L) {
				throw new Exception("TIMEOUT out of range! (>0): "+ extractArg("TIMEOUT"));
			}
		} else { 													
			// dialog switches set -> timeout optional !
			// ---------------------------------------------
			String[] dlgSwitches = getArg("ID").split(";");
			// 1 <= number of IDs <= 2 ?
			if (dlgSwitches.length < 1 || dlgSwitches.length > 2) {
				throw new Exception("dialog switches number! (allowed: 1..2  specified: "+ String.valueOf(dlgSwitches.length) +")");
			}	
			for (int i=0; i<dlgSwitches.length; i++) {
				// ID defined ?
				if (getOptionInt(ID, dlgSwitches[i]) == -1) {
					throw new Exception("dialog switches invalid! "+ dlgSwitches[i] +" (opts: "+ getOptionsString(ID) +")");
				}	
			}
			// TIMEOUT long > 0 ?
			if ((extractArg("TIMEOUT") != null) && (new Long(extractArg("TIMEOUT")).longValue() <= 0L)) { 
				throw new Exception("TIMEOUT out of range! (>0): " + extractArg("TIMEOUT"));
			}	
		}
		// POLLING_TIME long > 0 ?
		if ((extractArg("POLLING_TIME") != null) && (new Long(extractArg("POLLING_TIME")).longValue() <= 0L)) {
			throw new Exception("POLLING_TIME out of range! (>0): " + extractArg("POLLING_TIME"));
		}	
	}

	/**
	 * <b>check_POSITION_GANTRY_REAR_HOME</b>
	 * <br>
	 */
	private void check_POSITION_GANTRY_REAR_HOME() throws Exception
	{
		// nothing to be tested...
	}
	
	/**
	 * <b>check_POSITION_GANTRY_REAR</b>
	 * <br>
	 * - req: ID, X, Y, Z
	 * <br>
	 */
	private void check_POSITION_GANTRY_REAR() throws Exception
	{
		final String[] ID = {"No_Offset", "Hc2", "Rvc"};

		// ID defined ?
		if (getOptionInt(ID, extractArg("ID")) == -1) {
			throw new Exception("ID invalid! "+ extractArg("ID") +" (opts: "+ getOptionsString(ID) +")");
		}	
		// X int ?
		new Integer(extractArg("X")).intValue(); 
		// Y int ?
		new Integer(extractArg("Y")).intValue(); 
		// Z int ?
		new Integer(extractArg("Z")).intValue(); 
	}
	
	/**
	 * <b>check_ACTIVATE_GANTRY_REAR_TARGET</b>
	 * <br>
	 * - req: CMD
	 * <br>
	 */
	private void check_ACTIVATE_GANTRY_REAR_TARGET() throws Exception
	{
		final String[] CMD = {"", "Hc2", "Rvc"};
		
		// CMD defined ? 
		if (getOptionInt(CMD, extractArg("CMD")) == -1) {
			throw new Exception("CMD invalid! "+ extractArg("CMD") +" (opts: "+ getOptionsString(CMD) +")");
		}					
	}
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//										EXECUTE JOBS 	
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * <b>execute_POSITION_GANTRY_FRONT_HOME</b>
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @return execution status
	 */
	private int execute_POSITION_GANTRY_FRONT_HOME(FasControl fas, Vector resList) throws PPExecutionException
	{
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "PositionGFHomeOk";
			String resError = "";
			String dllResult = "IO";
			String dlgTitle = (System.getProperty("user.language").equalsIgnoreCase("de")) ? "Achtung!" : "Attention!";
			String dlgText = (System.getProperty("user.language").equalsIgnoreCase("de")) ? "Bitte, das RVC-Ziel abh�ngen!" : "Please, remove the RVC-Target!";

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? "IO" : fas.posGFHome();					
			log(debug, "Var", "dllResult = "+ dllResult);
			int count = 0;
			while ((dllResult.equalsIgnoreCase("RFKZIEL") || dllResult.equalsIgnoreCase("NIO_RVC_TARGET")) && count < 3) {
				// warn the operator to remove the RVC target
				testStandDialog(fas, debug, simulation, dlgTitle, dlgText, -1, 250, "START", null); // blocks inside
				dllResult = (simulation) ? "IO" : fas.posGFHome();
				log(debug, "Var", "dllResult("+ String.valueOf(count) +") = " +dllResult);
				count++;
			}
			
			// check dll result...
			if (!dllResult.equalsIgnoreCase("IO")) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "PositionGFHomeNOk";
				resError = (dllResult.equalsIgnoreCase("RFKZIEL")) ? "RvcTargetNotRemoved" : getDllErrorMsg(dllResult);
				log(debug, "Error", resError + " (dllResult: "+ dllResult +")");
			}
			resList.add( new Ergebnis("StatusPositionGFHome", "FasControl", job, "", "", resName, dllResult, "IO", "IO", "0", "", "", "", resError, "", resType) );

			// --------------------------------------------------------------
			return status;
		
		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}	

	/**
	 * <b>execute_POSITION_GANTRY_FRONT</b>
	 * <br>
	 * - req: ID, X, Y, Z
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @return execution status
	 */
	private int execute_POSITION_GANTRY_FRONT(FasControl fas, Vector resList) throws PPExecutionException
	{
		final String[] ID = {/*0*/"No_Offset", /*1*/"Acc", /*2*/"Nivi", /*3*/"Tlc_Up", /*4*/"Tlc_Down", 
		         	/*FIZ:*/ /*5*/"Fla", /*6*/"Rvc", /*7*/"Hc2"}; 
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			int id = getOptionInt(ID, getArg("ID"));
			int x = new Integer(extractArg("X")).intValue(); //mm
			int y = new Integer(extractArg("Y")).intValue(); //mm
			int z = new Integer(extractArg("Z")).intValue(); //mm
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "PositionGF"+ ID[id] +"Ok";
			String resError = "";
			String dllResult = "IO";
			String dlgTitle = (System.getProperty("user.language").equalsIgnoreCase("de")) ? "Achtung!" : "Attention!";
			String dlgText = (System.getProperty("user.language").equalsIgnoreCase("de")) ? "Bitte, das RVC-Ziel abh�ngen!" : "Please, remove the RVC-Target!";
			
			// --------------------------------------------------------------
			// set vars...
			resList.add( new Ergebnis("GF_OFFSET", "FasControl", job, "", "", "GF_OFFSET", String.valueOf(id), "0", String.valueOf(ID.length - 1), "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
			resList.add( new Ergebnis("GF_POSX", "FasControl", job, "", "", "GF_POSX", String.valueOf(x), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
			resList.add( new Ergebnis("GF_POSY", "FasControl", job, "", "", "GF_POSY", String.valueOf(y), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
			resList.add( new Ergebnis("GF_POSZ", "FasControl", job, "", "", "GF_POSZ", String.valueOf(z), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
			log(debug, "Var", "set GF_OFFSET = "+ String.valueOf(id));
			log(debug, "Var", "set GF_POSX = "+ String.valueOf(x));
			log(debug, "Var", "set GF_POSY = "+ String.valueOf(y));
			log(debug, "Var", "set GF_POSZ = "+ String.valueOf(z));

			// execute dll function...
			dllResult = (simulation) ? "IO" : fas.posGFTarget(id, x, y, z);					
			log(debug, "Var", "dllResult = "+ dllResult);
			int count = 0;
			while ((dllResult.equalsIgnoreCase("RFKZIEL") || dllResult.equalsIgnoreCase("NIO_RVC_TARGET")) && count < 3) {
				// warn the operator to remove the RVC target
				testStandDialog(fas, debug, simulation, dlgTitle, dlgText, -1, 250, "START", null); // blocks inside
				dllResult = (simulation) ? "IO" : fas.posGFHome();
				log(debug, "Var", "dllResult("+ String.valueOf(count) +") = "+ dllResult);
				count++;
			}
			
			// check dll result...
			if (!dllResult.equalsIgnoreCase("IO")) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "PositionGF"+ ID[id] +"NOk";
				resError = (dllResult.equalsIgnoreCase("RFKZIEL")) ? "RvcTargetNotRemoved" : getDllErrorMsg(dllResult);
				log(debug, "Error", resError +" (dllResult: "+ String.valueOf(dllResult) +")");
			}
			resList.add( new Ergebnis("StatusPositionGF", "FasControl", job, "", "", resName, String.valueOf(dllResult), "IO", "IO", "0", "", "", "", resError, "", resType) );			

			// --------------------------------------------------------------
			return status;
			
		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}

	/**
	 * <b>execute_ACTIVATE_GANTRY_FRONT_TARGET</b>
	 * <br>
	 * - req: CMD
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @return execution status
	 */
	private int execute_ACTIVATE_GANTRY_FRONT_TARGET(FasControl fas, Vector resList) throws PPExecutionException
	{
		final String[] CMD = {"", "Acc", "Nivi_Tlc", "Fla"};
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			int cmd = getOptionInt(CMD, getArg("CMD"));
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "ActivateGFTarget"+ CMD[cmd] +"Ok";
			String resError = "";
			int dllResult = 0;
	
			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? cmd : fas.gfSetTargetActive(cmd);
			log(debug, "Var", "dllResult = "+ String.valueOf(dllResult));

			// check job result...
			if (dllResult != cmd) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "ActivateGFTarget"+ CMD[cmd] +"NOk";
				resError = getDllErrorMsg(String.valueOf(dllResult));
				log(debug, "Error", resError +" (dllResult: "+ String.valueOf(dllResult) +")");
			}	
			resList.add( new Ergebnis("StatusActivateGFTarget", "FasControl", job, "", "", resName, String.valueOf(dllResult), String.valueOf(cmd), String.valueOf(cmd), "0", "", "", "", resError, "", resType) );

			// --------------------------------------------------------------
			return status;

		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}

	/**
	 * <b>execute_ROTATE_ACC</b>
	 * <br>
	 * - req: ID, PHI, THETA
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @param rotationType - Absolut or Relativ
	 * @return execution status
	 */
	private int execute_ROTATE_ACC(FasControl fas, Vector resList) throws PPExecutionException
	{
		final String[] ID = {"Relative", "Absolut"};
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			int type = getOptionInt(ID, getArg("ID"));
			int phi = new Integer(extractArg("PHI")).intValue(); 	 //�
			int theta = new Integer(extractArg("THETA")).intValue(); //�
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "AccRotate"+ ID[type] +"Ok";
			String resError = "";
			String dllResult = "IO";
			String dlgTitle = (System.getProperty("user.language").equalsIgnoreCase("de")) ? "Achtung!" : "Attention!";
			String dlgText = (System.getProperty("user.language").equalsIgnoreCase("de")) ? "Bitte, das RVC-Ziel abh�ngen!" : "Please, remove the RVC-Target!";

			// --------------------------------------------------------------
			// set vars...
			resList.add( new Ergebnis("ACC_PHI", "FasControl", job, "", "", "ACC_PHI", String.valueOf(phi), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
			resList.add( new Ergebnis("ACC_THETA", "FasControl", job, "", "", "ACC_THETA", String.valueOf(theta), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
			log(debug, "Var", "set ACC_PHI = "+ String.valueOf(phi));
			log(debug, "Var", "set ACC_THETA = "+ String.valueOf(theta));
			
			// execute dll function...	
			if (simulation) {
				dllResult = "IO";
			} else {
				dllResult = (type == 1) ? fas.accSchwenkenAbsolut(phi, theta) : fas.accSchwenkenRelativ(phi, theta);
			}	
			log(debug, "Var", "dllResult = "+ dllResult);
			int count = 0;
			while ((dllResult.equalsIgnoreCase("RFKZIEL") || dllResult.equalsIgnoreCase("NIO_RVC_TARGET")) && count < 3) {
				// warn the operator to remove the RVC target
				testStandDialog(fas, debug, simulation, dlgTitle, dlgText, -1, 250, "START", null); // blocks inside
				dllResult = (simulation) ? "IO" : fas.posGFHome();
				log(debug, "Var", "dllResult("+ String.valueOf(count) +") = "+ dllResult);
				count++;
			}
			
			// check dll result...
			if (!dllResult.equalsIgnoreCase("IO")) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "AccRotate"+ ID[type] +"NOk";
				resError = (dllResult.equalsIgnoreCase("RFKZIEL")) ? "RvcTargetNotRemoved" : getDllErrorMsg(dllResult);
				log(debug, "Error", resError + " (dllResult: "+ String.valueOf(dllResult) +")");
			}
			resList.add( new Ergebnis("StatusAccRotate", "FasControl", job, "", "", resName, dllResult, "IO", "IO", "0", "", "", "", resError, "", resType) );
			
			// --------------------------------------------------------------
			return status;
			
		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}
	
	/**
	 * <b>execute_CONTROL_CENTRATION</b>
	 * <br>
	 * - req: CMD
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @return execution status
	 */
	private int execute_CONTROL_CENTRATION(FasControl fas, Vector resList) throws PPExecutionException
	{
		final String[] CMD = {"", "Uncenter", "Center"};
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			int cmd = getOptionInt(CMD, getArg("CMD"));
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "Centration"+ CMD[cmd] +"Ok";
			String resError = "";
			int dllResult = 0;
			
			// --------------------------------------------------------------
			// execute dll function...
			dllResult = simulation ? cmd : fas.centration(cmd);
			log(debug, "Var", "dllResult = "+ String.valueOf(dllResult));

			// check dll result...		
			if (dllResult != cmd) { 
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "Centration"+ CMD[cmd] +"NOk";
				resError = getDllErrorMsg(String.valueOf(dllResult));
				log(debug, "Error", resError +" (dllResult: "+ String.valueOf(dllResult) +")");
			}	
			resList.add( new Ergebnis("StatusCentration", "FasControl", job, "", "", resName, String.valueOf(dllResult), String.valueOf(cmd), String.valueOf(cmd), "0", "", "", "", resError, "", resType) );			

			// --------------------------------------------------------------
			return status;
			
		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}

	/**
	 * <b>execute_CONTROL_NIVI_WIRE</b>
	 * <br>
	 * - req: CMD, MIN, MAX, (TIMEOUT) 
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @return execution status
	 */
	private int execute_CONTROL_NIVI_WIRE(FasControl fas, Vector resList) throws PPExecutionException
	{
		final String[] CMD = {"", "On", "Off"};
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			int cmd = getOptionInt(CMD, getArg("CMD")); 
			float min = new Float(extractArg("MIN")).floatValue();
			float max = new Float(extractArg("MAX")).floatValue();
			long timeout = (extractArg("TIMEOUT") != null) ? new Long(extractArg("TIMEOUT")).longValue() : 1000; //ms
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "NiviWire"+ CMD[cmd] +"Ok";
			String resError = "";
			float dllResult = 0.0f; //A

			log(debug, "Var", "timeout = "+ String.valueOf(timeout));
			
			// --------------------------------------------------------------
			// execute dll function... 
			dllResult = (simulation) ? 1f : fas.niviHeatedWire(cmd);
			log(debug, "Var", "dllResult[niviHeatedWire"+ CMD[cmd] +"] = "+ String.valueOf(dllResult));
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				// ignore
			}
			dllResult = (simulation) ? 1f : fas.niviHeatedWire(0);
			log(debug, "Var", "dllResult[niviHeatedWireRead] = "+ String.valueOf(dllResult));
			
			// check job result...
			if (dllResult < min || dllResult > max) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "NiviWire"+ CMD[cmd] +"NOk";
				resError = (dllResult < 0) ? getDllErrorMsg(String.valueOf(dllResult)) : "NiViWireValueOutOfRange";
				log(debug, "Error", resError + " (dllResult: "+ String.valueOf(dllResult) +")");
			}
			resList.add( new Ergebnis("StatusNiviWire", "FasControl", job, "Timeout: "+ timeout, "", resName, String.valueOf(dllResult), String.valueOf(min), String.valueOf(max), "0", "", "", "", resError, "", resType) );
			
			// --------------------------------------------------------------
			return status;
			
		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}
	
	/**
	 * <b>execute_CONTROL_TARGET_ILLUMINATION</b>
	 * <br>
	 * - req: ID, CMD
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @return execution status
	 */
	private int execute_CONTROL_TARGET_ILLUMINATION(FasControl fas, Vector resList) throws PPExecutionException
	{
		final String[] ID = {"Tlc", "Rvc"};
		final String[] CMD = {"", "On", "Off"};
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			int id = getOptionInt(ID, getArg("ID")); 
			int cmd = getOptionInt(CMD, getArg("CMD")); 
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = ID[id] +"Illumination"+ CMD[cmd] +"Ok";
			String resError = "";
			int dllResult = 0;

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ?  cmd : fas.lightingTarget(id, cmd);
			log(debug, "Var", "dllResult= " + String.valueOf(dllResult));

			// check job result...
			if (dllResult != cmd) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = ID[id] +"Illumination"+ CMD[cmd] +"NOk";
				resError = getDllErrorMsg(String.valueOf(dllResult));
				log(debug, "Error", resError + " (dllResult: "+ String.valueOf(dllResult) +")");
			}	
			resList.add( new Ergebnis("StatusTargetIllumination", "FasControl", job, "", "", resName, String.valueOf(dllResult), String.valueOf(cmd), String.valueOf(cmd), "0", "", "", "", resError, "", resType) );

			// --------------------------------------------------------------
			return status;
			
		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}
	
	/**
	 * <b>execute_CONTROL_SWITCH_ILLUMINATION</b>
	 * <br>
	 * - req: ID, CMD
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @return execution status
	 */
	private int execute_CONTROL_SWITCH_ILLUMINATION(FasControl fas, Vector resList) throws PPExecutionException
	{
		final String[] ID = {"START", "WIEDERHOLUNG", "ABBRUCH", "QUITTIEREN"}; 
		final String[] CMD = {"Off", "Blink", "On"};
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			int id = getOptionInt(ID, getArg("ID")); 
			int cmd = getOptionInt(CMD, getArg("CMD")); 
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = ID[id] +"Illumination"+ CMD[cmd] +"Ok";
			String resError = "";
			int dllResult = 0;

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? cmd : fas.controlSwitchIllumination(ID[id], cmd);
			log(debug, "Var", "dllResult = "+ String.valueOf(dllResult));

			// check dll result...
			if (dllResult != cmd) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = ID[id] +"Illumination"+ CMD[cmd] +"NOk";
				resError = getDllErrorMsg(String.valueOf(dllResult));
				log(debug, "Error", resError +" (dllResult: "+ String.valueOf(dllResult) +")");
			}
			resList.add( new Ergebnis("StatusSwitchIllumination", "FasControl", job, "", "", resName, String.valueOf(dllResult), String.valueOf(cmd), String.valueOf(cmd), "0", "", "", "", resError, "", resType) );

			// --------------------------------------------------------------
			return status;
			
		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}
	
	/**
	 * <b>execute_CONTROL_HC2_DOPPLER</b>
	 * <br>
	 * - req: CMD
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @return execution status
	 */
	private int execute_CONTROL_HC2_DOPPLER(FasControl fas, Vector resList) throws PPExecutionException
	{
		final String[] CMD = {"", "On", "Off"};
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			int cmd = getOptionInt(CMD, getArg("CMD"));
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "Hc2Doppler"+ CMD[cmd] +"Ok";
			String resError = "";
			int dllResult = 0;
			
			// --------------------------------------------------------------
			// execute dll function...
			dllResult = simulation ? cmd : fas.hc2Doppler(cmd);
			log(debug, "Var", "dllResult = "+ String.valueOf(dllResult));

			// check dll result...		
			if (dllResult != cmd) { 
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "Hc2Doppler"+ CMD[cmd] +"NOk";
				resError = getDllErrorMsg(String.valueOf(dllResult));
				log(debug, "Error", resError +" (dllResult: "+ String.valueOf(dllResult) +")");
			}	
			resList.add( new Ergebnis("StatusHc2Doppler", "FasControl", job, "", "", resName, String.valueOf(dllResult), String.valueOf(cmd), String.valueOf(cmd), "0", "", "", "", resError, "", resType) );			

			// --------------------------------------------------------------
			return status;
			
		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}

	/**
	 * <b>execute_WAIT_SWITCH_ACTIVATION</b>
	 * <br>
	 * - req: ID, CMD, TIMEOUT
	 * <br>
	 * - opt: POLLING_TIME
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @return execution status
	 */
	private int execute_WAIT_SWITCH_ACTIVATION(FasControl fas, Vector resList) throws PPExecutionException
	{
		final String[] ID = {"START", "WIEDERHOLUNG", "ABBRUCH", "QUITTIEREN"}; 
		final String[] CMD = {"Low", "High"};
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			int id = getOptionInt(ID, getArg("ID")); 
			int activationInt = getOptionInt(CMD, getArg("CMD")); 
			long timeout = new Long(extractArg("TIMEOUT")).longValue(); //ms
			long pollingTime = (extractArg("POLLING_TIME") != null) ? new Long(extractArg("POLLING_TIME")).longValue() : 250; //ms  
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = ID[id] +"Mon"+ CMD[activationInt] +"ActivationOk";
			String resError = "";
			int dllResult = 0;
			long startTime = System.currentTimeMillis();
			long actualTime = System.currentTimeMillis();
		
			// --------------------------------------------------------------
			// start monitoring
			log(debug, "Var", "activation = "+ String.valueOf(activationInt) +", timeout = "+ String.valueOf(timeout) +", polling = "+ String.valueOf(pollingTime));
			dllResult = (simulation) ? activationInt : fas.switchMonitoring(ID[id], 1);
			if (dllResult == 1) {
				// IO
				resList.add( new Ergebnis("StatusSwitchMonActivation", "FasControl", job, "", "", resName, String.valueOf(dllResult), "1", "1", "0", "", "", "", resError, "", resType) );
				// execute dll function... (polling cycle)
				dllResult = (activationInt == 1) ? 0 : 1;
				int count = 0;
				while (((actualTime - startTime) <= timeout) && (dllResult != activationInt)) {
					dllResult = (simulation) ? activationInt : fas.switchActivated(ID[id]);
					log(debug, "Var", "dllResult("+ count +") = " + String.valueOf(dllResult));
					try {
						Thread.sleep(pollingTime);
					} catch (InterruptedException e) {
						// ignore
					}
					actualTime = System.currentTimeMillis();
					count++;
				}

				// check job result...
				resName = ID[id] + CMD[activationInt] +"ActivationOk";
				if (dllResult != activationInt) {
					// NIO
					status = STATUS_EXECUTION_ERROR;
					resType = Ergebnis.FT_IO;
					resName = ID[id] + CMD[activationInt] +"ActivationNOk";
					resError = (dllResult >= 0) ? "Switch"+ CMD[activationInt] +"ActivationTimeout" : getDllErrorMsg(String.valueOf(dllResult));
					log(debug, "Error", resError + " (dllResult: "+ String.valueOf(dllResult) +")");
				}
				resList.add( new Ergebnis("StatusSwitchActivation", "FasControl", job, "", "", resName, String.valueOf(dllResult), String.valueOf(activationInt), String.valueOf(activationInt), "0", "", "", "", resError, "", resType) );

				// end monitoring
				if (!simulation) {
					fas.switchMonitoring(ID[id], 0); 
				}
			
			} else {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = ID[id] +"Mon"+ CMD[activationInt] +"ActivationNOk";
				resError = getDllErrorMsg(String.valueOf(dllResult));
				log(debug, "Error", "starting switch "+ ID[id] +" monitoring, "+ resError +" (dllResult = " + String.valueOf(dllResult)+ ")");
				resList.add( new Ergebnis("StatusSwitchMonActivation", "FasControl", job, "", "", resName, String.valueOf(dllResult), "1", "1", "0", "", "", "", resError, "", resType) );
			}
			
			// --------------------------------------------------------------
			return status;
			
		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}
	
	/**
	 * <b>execute_WAIT_VEHICLE_INSIDE_STAND</b>
	 * <br>
	 * - req: ID, CMD, TIMEOUT
	 * <br>
	 * - opt: POLLING_TIME
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @return execution status
	 */
	private int execute_WAIT_VEHICLE_INSIDE_STAND(FasControl fas, Vector resList) throws PPExecutionException
	{
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			long timeout = new Long(extractArg("TIMEOUT")).longValue(); //ms
			long pollingTime = (extractArg("POLLING_TIME") != null) ? new Long(extractArg("POLLING_TIME")).longValue() : 250; //ms  
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "WaitVehicleInsideStandOk";
			String resError = "";
			int dllResult = 0;
			long startTime = System.currentTimeMillis();
			long actualTime = System.currentTimeMillis();
			
			// --------------------------------------------------------------
			// execute dll function... (polling cycle)
			log(debug, "Var", "timeout = "+ String.valueOf(timeout) +", polling = "+ String.valueOf(pollingTime));
			int count = 0;
			while (((actualTime - startTime) <= timeout) && (dllResult != 1)) {
				dllResult = (simulation) ? 1 : fas.vehicleInsideTestStand();
				log(debug, "Var", "dllResult("+ count +") = " + String.valueOf(dllResult));
				try {
					Thread.sleep(pollingTime);
				} catch (InterruptedException e) {
					// ignore
				}
				actualTime = System.currentTimeMillis();
				count++;
			}
			
			// check job result...
			if (dllResult != 1) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "WaitVehicleInsideStandNOk";
				resError = (dllResult == 2) ? "NoVehicleInsideTimeout" : getDllErrorMsg(String.valueOf(dllResult));
				log(debug, "Error", resError +" (dllResult: "+ String.valueOf(dllResult) +")");
			}
			resList.add( new Ergebnis("StatusWaitVehicleInsideStand", "FasControl", job, "", "", resName, String.valueOf(dllResult), "1", "1", "0", "", "", "", resError, "", resType) );

			// --------------------------------------------------------------
			return status;
			
		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}
	
	/**
	 * <b>execute_MEASURE_VEHICLE</b> 
	 * <br>
	 * - req: WHEEL_BASE
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @return execution status
	 */
	private int execute_MEASURE_VEHICLE(FasControl fas, Vector resList) throws PPExecutionException
	{
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			int wheelBase = new Integer(extractArg("WHEEL_BASE")).intValue();
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "";
			String resError = "";
			String dllResult = "IO";
			float yFL = 0, yFR = 0, yRL = 0, yRR = 0; //mm
			float zFL = 0, zFR = 0, zRL = 0, zRR = 0; //mm
			float plausib = 0.0f; //mm
			float symmetryAngle = 0.0f, middleOffset = 0.0f; //� //mm 
			float heightFront = 0.0f, heightRear = 0.0f, heightLeft = 0.0f, heightRight = 0.0f, height = 0.0f; //mm
			
			// --------------------------------------------------------------
			// execute dll function...
			log(debug, "Var", "wheelBase = "+ String.valueOf(wheelBase));
			if (simulation) {
				dllResult = "<Y_FL>98.7<\\Y_FL><Y_FR>95.56<\\Y_FR><Y_RL>97.98<\\Y_RL><Y_RR>92.45<\\Y_RR><Z_FL>150.56<\\Z_FL><Z_FR>140.56<\\Z_FR><Z_RL>132.45<\\Z_RL><Z_RR>130.56<\\Z_RR>";
			} else {
				dllResult = fas.vehMeasurement(wheelBase);
				dllResult = dllResult.trim();
			}
			log(debug, "Var", "dllResult = " + dllResult);
			
			// check dll result...
			if (dllResult.startsWith("<Y_FL>")) {
				// Y measures (rounded with 2 decimal places)	
				yFL = rnd(new Float(paramXmlDeserialization(dllResult, "Y_FL")).floatValue(), 2);
				yFR = rnd(new Float(paramXmlDeserialization(dllResult, "Y_FR")).floatValue(), 2);
				yRL = rnd(new Float(paramXmlDeserialization(dllResult, "Y_RL")).floatValue(), 2);
				yRR = rnd(new Float(paramXmlDeserialization(dllResult, "Y_RR")).floatValue(), 2);
				// Z measures (rounded with 2 decimal places)	
				zFL = rnd(new Float(paramXmlDeserialization(dllResult, "Z_FL")).floatValue(), 2);
				zFR = rnd(new Float(paramXmlDeserialization(dllResult, "Z_FR")).floatValue(), 2);
				zRL = rnd(new Float(paramXmlDeserialization(dllResult, "Z_RL")).floatValue(), 2);
				zRR = rnd(new Float(paramXmlDeserialization(dllResult, "Z_RR")).floatValue(), 2);
				// set vars...
				resList.add( new Ergebnis("VEHICLE_WIDTH_FL", "FasControl", job, "", "", "VEHICLE_WIDTH_FL", String.valueOf(yFL), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
				resList.add( new Ergebnis("VEHICLE_WIDTH_FR", "FasControl", job, "", "", "VEHICLE_WIDTH_FR", String.valueOf(yFR), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
				resList.add( new Ergebnis("VEHICLE_WIDTH_RL", "FasControl", job, "", "", "VEHICLE_WIDTH_RL", String.valueOf(yRL), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
				resList.add( new Ergebnis("VEHICLE_WIDTH_RR", "FasControl", job, "", "", "VEHICLE_WIDTH_RR", String.valueOf(yRR), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
				resList.add( new Ergebnis("VEHICLE_HEIGHT_FL", "FasControl", job, "", "", "VEHICLE_HEIGHT_FL", String.valueOf(zFL), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
				resList.add( new Ergebnis("VEHICLE_HEIGHT_FR", "FasControl", job, "", "", "VEHICLE_HEIGHT_FR", String.valueOf(zFR), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
				resList.add( new Ergebnis("VEHICLE_HEIGHT_RL", "FasControl", job, "", "", "VEHICLE_HEIGHT_RL", String.valueOf(zRL), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
				resList.add( new Ergebnis("VEHICLE_HEIGHT_RR", "FasControl", job, "", "", "VEHICLE_HEIGHT_RR", String.valueOf(zRR), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
				log(debug, "Var", "set VEHICLE_WIDTH_FL = "+ String.valueOf(yFL));
				log(debug, "Var", "set VEHICLE_WIDTH_FR = "+ String.valueOf(yFR));
				log(debug, "Var", "set VEHICLE_WIDTH_RL = "+ String.valueOf(yRL));
				log(debug, "Var", "set VEHICLE_WIDTH_RR = "+ String.valueOf(yRR));
				log(debug, "Var", "set VEHICLE_HEIGHT_FL = "+ String.valueOf(zFL));
				log(debug, "Var", "set VEHICLE_HEIGHT_FR = "+ String.valueOf(zFR));
				log(debug, "Var", "set VEHICLE_HEIGHT_RL = "+ String.valueOf(zRL));
				log(debug, "Var", "set VEHICLE_HEIGHT_RR = "+ String.valueOf(zRR));
				// IO
				resList.add( new Ergebnis("StatusVehicleMeasurement", "FasControl", job, "", "", "VehicleMeasurementOk", dllResult, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO) );
				log(debug, "Ok", "vehicle measurement completed.");
				
				// Plausibility test: abs(zFL - zFR - zRL + zRR) < 15
				plausib = new Float(Math.abs(zFL - zFR + zRR - zRL)).floatValue();
				log(debug, "Var", "plausib = abs(zFL - zFR + zRR - zRL) = "+ String.valueOf(plausib) +" mm");
				if (plausib < 15.0f) {
					// IO
					resList.add( new Ergebnis("StatusVehicleHeightsPlausibility", "FasControl", job, "", "", "VehicleHeightsPlausibilityOk", String.valueOf(plausib), "-15", "15", "0", "", "", "", "", "", Ergebnis.FT_IO) );
					log(debug, "Ok", "height measurement plausible. Tolerance criterion: abs(zFL - zFR + zRR - zRL) < 15mm, Result = "+ String.valueOf(plausib) +" mm");
					// composed measures
					symmetryAngle = new Float(Math.toDegrees(Math.atan((yFL - yFR - yRL + yRR) / (2 * wheelBase)))).floatValue();
					middleOffset = rnd( ( ((yFL - yFR)/2) + ((yRL - yRR)/2) )/2, 2); // middleOffset = [(Breite VL-Breite VR)/2 + (Breite HL ? Breite HR)/2]/2 
					heightFront = rnd( ((zFL + zFR)/2), 2);
					heightRear = rnd( ((zRL + zRR)/2 ), 2);
					heightLeft =  rnd( ((zFL + zRL)/2), 2);
					heightRight = rnd( ((zFR + zRR)/2), 2);
					height = rnd( ((zFL + zFR + zRL + zRR + heightFront + heightRear + heightLeft + heightRight)/8), 2); 
					// set vars...
					resList.add( new Ergebnis("VEHICLE_SYMMETRY_ANGLE", "FasControl", job, "", "", "VEHICLE_SYMMETRY_ANGLE", String.valueOf(symmetryAngle), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
					resList.add( new Ergebnis("VEHICLE_MIDDLE_OFFSET", "FasControl", job, "", "", "VEHICLE_MIDDLE_OFFSET", String.valueOf(middleOffset), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
					resList.add( new Ergebnis("VEHICLE_HEIGHT_FRONT", "FasControl", job, "", "", "VEHICLE_HEIGHT_FRONT", String.valueOf(heightFront), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
					resList.add( new Ergebnis("VEHICLE_HEIGHT_REAR", "FasControl", job, "", "", "VEHICLE_HEIGHT_REAR", String.valueOf(heightRear), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
					resList.add( new Ergebnis("VEHICLE_HEIGHT_LEFT", "FasControl", job, "", "", "VEHICLE_HEIGHT_LEFT", String.valueOf(heightLeft), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
					resList.add( new Ergebnis("VEHICLE_HEIGHT_RIGHT", "FasControl", job, "", "", "VEHICLE_HEIGHT_RIGHT", String.valueOf(heightRight), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
					resList.add( new Ergebnis("VEHICLE_HEIGHT", "FasControl", job, "", "", "VEHICLE_HEIGHT", String.valueOf(height), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
					log(debug, "Var", "set VEHICLE_SYMMETRY_ANGLE = "+ String.valueOf(symmetryAngle));
					log(debug, "Var", "set VEHICLE_MIDDLE_OFFSET = "+ String.valueOf(middleOffset));
					log(debug, "Var", "set VEHICLE_HEIGHT_FRONT = "+ String.valueOf(heightFront));
					log(debug, "Var", "set VEHICLE_HEIGHT_REAR = "+ String.valueOf(heightRear));
					log(debug, "Var", "set VEHICLE_HEIGHT_LEFT = "+ String.valueOf(heightLeft));
					log(debug, "Var", "set VEHICLE_HEIGHT_RIGHT = "+ String.valueOf(heightRight));
					log(debug, "Var", "set VEHICLE_HEIGHT = "+ String.valueOf(height));
					log(debug, "Var", "----------------");
					log(debug, "Ok", "vehicle measurement completed.");

					//--------------- Temp ---------------
					resList.add( new Ergebnis("SymmetrieWinkel", "SDiagFasControl", job, "", "", "SymmetrieInGrad", String.valueOf(symmetryAngle), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO) );
					resList.add( new Ergebnis("HoeheVorderachse", "SDiagFasControl", job, "", "", "HoeheVorderachseInMm", String.valueOf(heightFront), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO) );
					resList.add( new Ergebnis("HoeheHinterachse", "SDiagFasControl", job, "", "", "HoeheHinterachseInMm", String.valueOf(heightRear), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO) );
					resList.add( new Ergebnis("HoeheFzgLinks", "SDiagFasControl", job, "", "", "HoeheFahrzeugLinksMm", String.valueOf(heightLeft), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO) );
					resList.add( new Ergebnis("HoeheFzgRechts", "SDiagFasControl", job, "", "", "HoeheFahrzeugRechtsMm", String.valueOf(heightRight), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO) );
					resList.add( new Ergebnis("HoeheFzgGesamt", "SDiagFasControl", job, "", "", "HoeheFahrzeugGesamtMm", String.valueOf(height), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO) );
					log(debug, "Var", "tmp set SymmetrieWinkel = "+ String.valueOf(symmetryAngle));
					log(debug, "Var", "tmp set HoeheVorderachse = "+ String.valueOf(heightFront));
					log(debug, "Var", "tmp set HoeheHinterachse = "+ String.valueOf(heightRear));
					log(debug, "Var", "tmp set HoeheFzgLinks = "+ String.valueOf(heightLeft));
					log(debug, "Var", "tmp set HoeheFzgRechts = "+ String.valueOf(heightRight));
					log(debug, "Var", "tmp set HoeheFzgGesamt = "+ String.valueOf(height));
					//------------------------------------				
					
				} else {
					// NIO
					status = STATUS_EXECUTION_ERROR;
					resType = Ergebnis.FT_NIO;
					resName = "VehicleHeightsPlausibilityNOk";
					resError = "VehicleHeightsNotPlausibel";
					resList.add( new Ergebnis("StatusVehicleHeightsPlausibility", "FasControl", job, "", "", resName, String.valueOf(plausib), "-15", "15", "0", "", "", "", resError, "", resType) );
					log(debug, "Error", "height measurement NOT plausible! Tolerance criterion: abs(zFL - zFR + zRR - zRL) < 15mm, Result = "+ String.valueOf(plausib) +" mm");
				}
				
			} else {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "VehicleMeasurementNOk";
				resError = getDllErrorMsg(dllResult);
				resList.add( new Ergebnis("StatusVehicleMeasurement", "FasControl", job, "", "", resName, dllResult, "", "", "0", "", "", "", resError, "", resType) );
				log(debug, "Error", resError +" (dllResult: "+ dllResult +")");
			}

			// --------------------------------------------------------------
			return status;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new PPExecutionException(e.getMessage());
		}
	}
	
	/**
	 * <b>execute_MEASURE_STAND</b>
	 * <br>
	 * - req: ID
	 * <br>
	 * - opt: TOL_ACC, TOL_GF, TOL_GR, TOL_HEIGHTS, TOL_SYMMETRY 
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @return execution status
	 */
	private int execute_MEASURE_STAND(FasControl fas, Vector resList) throws PPExecutionException
	{
		final String[] ID = {"Verification", "Calibration"};
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			int id = getOptionInt(ID, getArg("ID")); 
			float tolAcc = (extractArg("TOL_ACC") != null) ? Math.abs(new Float(extractArg("TOL_ACC")).floatValue()) : -99999;
			float tolGF = (extractArg("TOL_GF") != null) ? Math.abs(new Float(extractArg("TOL_GF")).floatValue()) : -99999;
			float tolGR = (extractArg("TOL_GR") != null) ? Math.abs(new Float(extractArg("TOL_GR")).floatValue()) : -99999;
			float tolHeights = (extractArg("TOL_HEIGHTS") != null) ? Math.abs(new Float(extractArg("TOL_HEIGHTS")).floatValue()) : -99999;
			float tolSymmetry = (extractArg("TOL_SYMMETRY") != null) ? Math.abs(new Float(extractArg("TOL_SYMMETRY")).floatValue()) : -99999;
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resId = "";
			String resName = "";
			String resError = "";
			String dllResult = "IO";
		
			// --------------------------------------------------------------
			// execute dll function...
			log(debug, "Var", ID[id] +": Acc("+ String.valueOf(tolAcc) +"), GF("+ String.valueOf(tolGF) +"), GR("+ String.valueOf(tolGR) +"), Heights("+ String.valueOf(tolHeights) +"), Symmetry("+ String.valueOf(tolSymmetry) +")");
			if (simulation) {
				dllResult = "<ACC>\"0.02,0.43,-2.32,1.21,-0.86\";\"0.02,0.43,-2.32,1.21,-0.86\"<\\ACC><PORTAL_VO>0.00;-1.23;0.95<\\PORTAL_VO><PORTAL_HI>0.00;-1.23;0.95<\\PORTAL_HI><HOEHEN>0.02;0.00;0.01;-0.03<\\HOEHEN><SYMMETRY>0.03<\\SYMMETRY>";
			} else {
				dllResult = fas.testStandMeasurement(id == 0 ? true : false, 
													 tolAcc != -99999 ? true : false, 
													 tolGF != -99999 ? true : false, 
													 tolGR != -99999 ? true : false, 
													 tolHeights != -99999 ? true : false, 
													 tolSymmetry != -99999 ? true : false);
				dllResult = dllResult.trim();
			}
			log(debug, "Var", "dllResult = " + dllResult);
			
			// check dll result...
			if ((dllResult.startsWith("<ACC>")) || (dllResult.startsWith("<PORTAL_VO>")) || 
				(dllResult.startsWith("<PORTAL_HI>")) || (dllResult.startsWith("<HOEHEN>")) || 
				(dllResult.startsWith("<SYMMETRIE>"))) {
				// IO
				resList.add( new Ergebnis("StatusStandMeasurement", "FasControl", job, "", "", "StandMeasurement"+ ID[id] +"Ok", dllResult, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO) );
				log(debug, "Ok", "test stand "+ ID[id] +" measurement completed.");
				//------------------
				// Acc
				//------------------
				if (tolAcc != -99999) {
					String ergAcc = paramXmlDeserialization(dllResult, "ACC");
					if (ergAcc.equalsIgnoreCase("")) {
						throw new Exception("element <ACC></ACC> empty!");
					}
					StringTokenizer tokenTaker = new StringTokenizer(ergAcc, "\";,");
					int i = 1;
					while (tokenTaker.hasMoreElements()) {
						float valueAcc = new Float(tokenTaker.nextToken()).floatValue();
						// set var
						resType = Ergebnis.FT_IO;
						resId = "STAND_ACC_ANGLE"+ (i<=5 ? "Y"+ String.valueOf(i) : "Z"+ String.valueOf(i-5));
						resName = resId;
						resError = "";
						if (i != 1 && Math.abs(valueAcc) > tolAcc) {
							// NIO
							status = STATUS_EXECUTION_ERROR;
							resType = Ergebnis.FT_NIO;
							resError = "OutOfTolerance";
							log(debug, "Error", resError +" ("+ valueAcc +" > "+ tolAcc +")");
						}
						resList.add( new Ergebnis(resId, "FasControl", job, "", "", resName, String.valueOf(valueAcc), String.valueOf(-1*tolAcc), String.valueOf(tolAcc), "0", "", "", "", resError, "", resType) );
						log(debug, "Var", "set "+ resId +" = "+ String.valueOf(valueAcc));
						i++;						
					}
				}
				//------------------
				// GF
				//------------------
				if (tolGF != -99999) {
					String ergPortalVo = paramXmlDeserialization(dllResult, "PORTAL_VO");
					if (ergPortalVo.equalsIgnoreCase("")) {
						throw new Exception("element <PORTAL_VO></PORTAL_VO> empty!");
					}
					StringTokenizer tokenTaker = new StringTokenizer(ergPortalVo, ";");
					String[] opt = {"", "X", "Y", "Z"};
					int i = 1;
					while (tokenTaker.hasMoreElements()) {						
						float valueGF = new Float(tokenTaker.nextToken()).floatValue();
						// set var
						resType = Ergebnis.FT_IO;
						resId = "STAND_GF_POS"+ opt[i];
						resName = resId;
						resError = "";
						if (i != 1 && Math.abs(valueGF) > tolGF) {
							// NIO
							status = STATUS_EXECUTION_ERROR;
							resType = Ergebnis.FT_NIO;
							resError = "OutOfTolerance";
							log(debug, "Error", resError +" ("+ valueGF +" > "+ tolGF +")");
						}
						resList.add( new Ergebnis(resId, "FasControl", job, "", "", resName, String.valueOf(valueGF), String.valueOf(-1*tolGF), String.valueOf(tolGF), "0", "", "", "", resError, "", resType) );						
						log(debug, "Var", "set "+ resId +" = "+ String.valueOf(valueGF));
						i++;
					}	
				}
				//------------------
				// GR
				//------------------
				if (tolGR != -99999 && isHighStand(fas, simulation)) {
					String strPortalHi = paramXmlDeserialization(dllResult, "PORTAL_HI");
					log(true, "Var", "Rear portal present! (strPortalHi = "+ strPortalHi +")");
					if (strPortalHi.equalsIgnoreCase("")) {
						throw new Exception("element <PORTAL_HI></PORTAL_HI> empty!");
					}
					String[] opt = {"X", "Y", "Z"};
					StringTokenizer tokenTaker = new StringTokenizer(strPortalHi, ";");
					int i = 0;
					while (tokenTaker.hasMoreElements() && i < opt.length) {						
						float valueGR = new Float(tokenTaker.nextToken()).floatValue();
						// set var
						resType = Ergebnis.FT_IO;
						resId = "STAND_GR_POS"+ opt[i];
						resName = resId;
						resError = "";
						if (valueGR != Float.NaN && Math.abs(valueGR) > tolGR) {
							// NIO
							status = STATUS_EXECUTION_ERROR;
							resType = Ergebnis.FT_NIO;
							resError = "OutOfTolerance";
							log(debug, "Error", resError +" ("+ valueGR +" > "+ tolGR +")");
						}
						resList.add( new Ergebnis(resId, "FasControl", job, "", "", resName, String.valueOf(valueGR), String.valueOf(-1*tolGR), String.valueOf(tolGR), "0", "", "", "", resError, "", resType) );						
						log(debug, "Var", "set "+ resId +" = "+ String.valueOf(valueGR));
						i++;
					}
				}
				//------------------
				// Heights 
				//------------------
				log(true, "Info", "hasHeightMeasurement: " + hasHeightMeasurement(fas, simulation));
				if (tolHeights != -99999 && hasHeightMeasurement(fas, simulation)) {
					String ergHoehen = paramXmlDeserialization(dllResult, "HOEHEN");
					if (ergHoehen.equalsIgnoreCase("")) {
						throw new Exception("element <HOEHEN></HOEHEN> empty!");
					}
					StringTokenizer tokenTaker = new StringTokenizer(ergHoehen, ";");
					String[] opt = {"", "FL", "FR", "RL", "RR"};
					int i = 1;
					while (tokenTaker.hasMoreElements()) {
						float valueHeight = new Float(tokenTaker.nextToken()).floatValue();
						// set var
						resType = Ergebnis.FT_IO;
						resId = "STAND_HEIGHT_"+ opt[i];
						resName = resId;
						resError = "";
						if (Math.abs(valueHeight) > tolHeights) {
							// NIO
							status = STATUS_EXECUTION_ERROR;
							resType = Ergebnis.FT_NIO;
							resError = "OutOfTolerance";
							log(debug, "Error", resError +" ("+ valueHeight +" > "+ tolHeights +")");
						}
						resList.add( new Ergebnis(resId, "FasControl", job, "", "", resName, String.valueOf(valueHeight), String.valueOf(-1*tolHeights), String.valueOf(tolHeights), "0", "", "", "", resError, "", resType) );						
						log(debug, "Var", "set "+ resId +" = "+ String.valueOf(valueHeight));
						i++;						
					}
				}
				//------------------
				// Symmetry
				//------------------
				if (tolSymmetry != -99999 && hasHeightMeasurement(fas, simulation)) {
					String ergSymmetrie = paramXmlDeserialization(dllResult, "SYMMETRIE");
					if (ergSymmetrie.equalsIgnoreCase("")) {
						throw new Exception("element <SYMMETRIE></SYMMETRIE> empty!");
					}
					StringTokenizer tokenTaker = new StringTokenizer(ergSymmetrie, ";");
					while (tokenTaker.hasMoreElements()) {
						float valueSymmetry = new Float(tokenTaker.nextToken()).floatValue();
						// set var
						resType = Ergebnis.FT_IO;
						resId = "STAND_SYMMETRY_ANGLE";
						resName = resId;
						resError = "";
						if (Math.abs(valueSymmetry) > tolSymmetry) {
							// NIO
							status = STATUS_EXECUTION_ERROR;
							resType = Ergebnis.FT_NIO;
							resError = "OutOfTolerance";
							log(debug, "Error", resError +" ("+ valueSymmetry +" > "+ tolSymmetry +")");
						}
						resList.add( new Ergebnis(resId, "FasControl", job, "", "", resName, String.valueOf(valueSymmetry), String.valueOf(-1*tolSymmetry), String.valueOf(valueSymmetry), "0", "", "", "", resError, "", resType) );						
						log(debug, "Var", "set "+ resId +" = "+ String.valueOf(valueSymmetry));
					}					
				}
				
			} else { 
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "StandMeasurement"+ ID[id] +"NOk";
				resError = getDllErrorMsg(dllResult);
				resList.add( new Ergebnis("StatusStandMeasurement", "FasControl", job, "", "", resName, dllResult, "", "", "0", "", "", "", resError, "", resType) );
				log(debug, "Error", resError +" (dllResult: "+ dllResult +")");
			}	

			// --------------------------------------------------------------
			return status;
			
		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}
	
	/**
	 * <b>execute_STAND_TYPE</b>
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @return execution status
	 */
	private int execute_STAND_TYPE(FasControl fas, Vector resList) throws PPExecutionException
	{
		final String[] ID = {"Basic", "High"}; 
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			int id = getOptionInt(ID, getArg("ID")); 
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "StandType"+ ID[id] +"Ok";
			String resError = "";
			int dllResult = 0;

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? 1 : fas.hc2Doppler(0);					
			log(debug, "Var", "dllResult[hc2DopplerRead] = "+ dllResult);
			
			// check dll result...
			if ( (ID[id].equalsIgnoreCase("Basic") && dllResult != -5) ||
				 (ID[id].equalsIgnoreCase("High") && dllResult <= 0) ){
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "StandType"+ ID[id] +"NOk";
				resError = getDllErrorMsg(String.valueOf(dllResult));
				log(debug, "Error", resError + " (dllResult: "+ dllResult +")");
			}
			resList.add( new Ergebnis("StatusStandType", "FasControl", job, "", "", resName, String.valueOf(dllResult), "", "", "0", "", "", "", resError, "", resType) );

			// --------------------------------------------------------------
			return status;
		
		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}	

	/**
	 * <b>execute_STAND_DIALOG</b>
	 * <br>
	 * - req: MESSAGE, (ID ;split), (TIMEOUT)
	 * <br>
	 * - opt: POLLING_TIME
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @return execution status
	 */
	private int execute_STAND_DIALOG(FasControl fas, Vector resList) throws PPExecutionException
	{
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			String dlgText = extractArg("MESSAGE"); // can have several '@var' blocks inside 
			String dlgSwitchesStr = (extractArg("ID") != null) ? getArg("ID") : ""; // can have 1..2 switch names inside
			long pollingTime = (extractArg("POLLING_TIME") != null) ? new Long(extractArg("POLLING_TIME")).longValue() : 250;  
			long timeout = (extractArg("TIMEOUT") != null) ? new Long(extractArg("TIMEOUT")).longValue() : -1;
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "StandDialogOk";
			String resError = "";
			int dlgResult = 0;
			long startTime = System.currentTimeMillis();
			long actualTime = System.currentTimeMillis();

			// --------------------------------------------------------------
			// parse dlg switches (;split)
			String[] subSwitches = {};
			if (!dlgSwitchesStr.equalsIgnoreCase("")) {
				subSwitches = dlgSwitchesStr.split(";");
			}
			log(debug, "Ok", "parsed dlgSwitches [OK -> "+ (subSwitches.length > 0 ? subSwitches[0] : "") +" / NOK -> "+ (subSwitches.length > 1 ? subSwitches[1] : "") +"]");
			log(debug, "Ok", "parsed dlgText ["+ dlgText +"]");
			
			// execute dlg function...
			dlgResult = testStandDialog(fas, debug, simulation, "FAS", dlgText, timeout, pollingTime, (subSwitches.length > 0 ? subSwitches[0] : ""), (subSwitches.length > 1 ? subSwitches[1] : "")); 
			log(debug, "Var", "dlgResult = "+ String.valueOf(dlgResult));
			
			// check dlg result...
			if (dlgResult >= 0 && dlgResult <= 2) {
				// IO
				resList.add( new Ergebnis("StatusStandDialog", "FasControl", job, "", "", "StandDialogOk", String.valueOf(dlgResult), "0", "2", "0", "", "", "", "", "", Ergebnis.FT_IO) );
				log(debug, "Ok", "test stand dialog executed.");
				// Conditions:
				// . 0  buttons (ok)
				// . >1 buttons (ok only if returns 'pressed button 1')
				resType = Ergebnis.FT_IO;
				resName = "StandDialogUserResponseOk";
				resError = "";
				if (subSwitches.length > 0 && dlgResult != 1) {
					// NIO
					status = STATUS_EXECUTION_ERROR;
					resType = Ergebnis.FT_NIO;
					resName = "StandDialogUserResponseNOk";
					resError = (dlgResult == 0) ? "NoSwitchPressedTimeout" : "PressedSwitch"+ subSwitches[1];
					log(debug, "Error", resError +" (dlgResult: "+ String.valueOf(dlgResult) +")");
				}
				resList.add( new Ergebnis("StatusStandDialogUserResponse", "FasControl", job, "", "", resName, String.valueOf(dlgResult), (subSwitches.length == 0 ? "0" : "1"), (subSwitches.length == 0 ? "0" : "1"), "0", "", "", "", resError, "", resType) );

			} else {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resError = "DllSwitchError"; 
				resList.add( new Ergebnis("StatusStandDialog", "FasControl", job, "", "", "StandDialogNOk", String.valueOf(dlgResult), "0", "2", "0", "", "", "", resError, "", Ergebnis.FT_NIO) );
				log(debug, "Error", resError +" (dlgResult: "+ dlgResult +")");
			}

			// --------------------------------------------------------------
			return status;
			
		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}

	/**
	 * <b>execute_POSITION_GANTRY_REAR_HOME</b>
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @return execution status
	 */
	private int execute_POSITION_GANTRY_REAR_HOME(FasControl fas, Vector resList) throws PPExecutionException
	{
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "PositionGRHomeOk";
			String resError = "";
			String dllResult = "IO";

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? "IO" : fas.posGRHome();					
			log(debug, "Var", "dllResult = "+ dllResult);
			
			// check dll result...
			if (!dllResult.equalsIgnoreCase("IO")) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "PositionGRHomeNOk";
				resError = getDllErrorMsg(dllResult);
				log(debug, "Error", resError +" (dllResult: "+ dllResult +")");
			}
			resList.add( new Ergebnis("StatusPositionGRHome", "FasControl", job, "", "", resName, dllResult, "IO", "IO", "0", "", "", "", resError, "", resType) );

			// --------------------------------------------------------------
			return status;
		
		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}	

	/**
	 * <b>execute_POSITION_GANTRY_REAR</b>
	 * <br>
	 * - req: ID, X, Y, Z
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @return execution status
	 */
	private int execute_POSITION_GANTRY_REAR(FasControl fas, Vector resList) throws PPExecutionException
	{
		final String[] ID = {"No_Offset", "Hc2", "Rvc"};
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			int id = getOptionInt(ID, getArg("ID"));
			int x = new Integer(extractArg("X")).intValue(); //mm
			int y = new Integer(extractArg("Y")).intValue(); //mm
			int z = new Integer(extractArg("Z")).intValue(); //mm
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "PositionGR"+ ID[id] +"Ok";
			String resError = "";
			String dllResult = "IO";
			
			// --------------------------------------------------------------
			// set vars...
			resList.add( new Ergebnis("GR_OFFSET", "FasControl", job, "", "", "GR_OFFSET", String.valueOf(id), "0", String.valueOf(ID.length), "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
			resList.add( new Ergebnis("GR_POSX", "FasControl", job, "", "", "GR_POSX", String.valueOf(x), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
			resList.add( new Ergebnis("GR_POSY", "FasControl", job, "", "", "GR_POSY", String.valueOf(y), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
			resList.add( new Ergebnis("GR_POSZ", "FasControl", job, "", "", "GR_POSZ", String.valueOf(z), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE) );
			log(debug, "Var", "set GR_OFFSET = "+ String.valueOf(id));
			log(debug, "Var", "set GR_POSX = "+ String.valueOf(x));
			log(debug, "Var", "set GR_POSY = "+ String.valueOf(y));
			log(debug, "Var", "set GR_POSZ = "+ String.valueOf(z));
			
			// execute dll function...
			dllResult = (simulation) ? "IO" : fas.posGRTarget(id, x, y, z);					
			log(debug, "Var", "dllResult = "+ dllResult);
			
			// check dll result...
			if (!dllResult.equalsIgnoreCase("IO")) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "PositionGR"+ ID[id] +"NOk";
				resError = getDllErrorMsg(dllResult);
				log(debug, "Error", resError +" (dllResult: "+ String.valueOf(dllResult) +")");
			}
			resList.add( new Ergebnis("StatusPositionGR", "FasControl", job, "", "", resName, String.valueOf(dllResult), "IO", "IO", "0", "", "", "", resError, "", resType) );			

			// --------------------------------------------------------------
			return status;
			
		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}

	/**
	 * <b>execute_ACTIVATE_GANTRY_REAR_TARGET</b>
	 * <br>
	 * - req: CMD
	 * <br>
	 * @param fas - FasControl device
	 * @param resList - APDM results list
	 * @return execution status
	 */
	private int execute_ACTIVATE_GANTRY_REAR_TARGET(FasControl fas, Vector resList) throws PPExecutionException
	{
		final String[] CMD = {"", "Hc2", "Rvc"};
		try {
			// CASCADE parameters
			String job = getArg("JOB");
			int cmd = getOptionInt(CMD, getArg("CMD"));
			boolean debug = (getArg("DEBUG") != null && getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;
			boolean simulation = (getArg("SIMULATION") != null && getArg("SIMULATION").equalsIgnoreCase("TRUE")) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "ActivateGRTarget"+ CMD[cmd] +"Ok";
			String resError = "";
			int dllResult = 0;
	
			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? cmd : fas.grSetTargetActive(cmd);
			log(debug, "Var", "dllResult = "+ String.valueOf(dllResult));

			// check job result...
			if (dllResult != cmd) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "ActivateGRTarget"+ CMD[cmd] +"NOk";
				resError = getDllErrorMsg(String.valueOf(dllResult));
				log(debug, "Error", resError +" (dllResult: "+ String.valueOf(dllResult) +")");
			}	
			resList.add( new Ergebnis("StatusActivateGRTarget", "FasControl", job, "", "", resName, String.valueOf(dllResult), String.valueOf(cmd), String.valueOf(cmd), "0", "", "", "", resError, "", resType) );

			// --------------------------------------------------------------
			return status;

		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}

	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//											UTILS 	
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	/**
	 * <b>getMachineError</b>
	 * <br>
	 * Checks if the machine is in a error mode. If it is, warns the user to reset the error within 20 seconds.
	 * <br>
	 * @param fas - FasControl device
	 * @param debug - debug flag
	 * @param simulation - simulation flag
	 * @return the dll result int code
	 *
	private int getMachineError(FasControl fas, boolean debug, boolean simulation) throws PPExecutionException
	{
		// aux
		int dllResult = 0;
		UserDialog myUd = null;
		
		try {
			dllResult = (simulation) ? 0 : fas.machineError();
			log(debug, "Var", "checkMachineError = " + String.valueOf(dllResult));
			
			// warning dialog cycle
			int sec = 20;
			myUd = getPr�flingLaufzeitUmgebung().getUserDialog();
			myUd.setAllowCancel(false);
			while ((dllResult > 0) && (sec >= 0)) { 
				// in error or emergency...
				if (System.getProperty("user.language").equalsIgnoreCase("de")) {
					myUd.displayAlertMessage("Pr�fstandsfehler", fas.errorInfo() + "\nAbbruch in + " + sec + " Sekunden...", -1);
				} else {
					myUd.displayAlertMessage("Test stand error", fas.errorInfo() + "\nAbort in + " + sec + " seconds...", -1);
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// ignore
				}
				dllResult = (simulation) ? 0 : fas.machineError();
				log(debug, "Var", "MachineError (sec = "+ String.valueOf(sec) +") = " + String.valueOf(dllResult));
				sec--;
			}
			if (myUd != null) {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
				myUd = null;
			}
			return dllResult;
			
		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}

	/**
	 * <b>getMachineOpMode</b>
	 * <br>
	 * Checks if the machine is in manual/auto mode. If it's in manual mode, warns the user to change within 20 seconds.
	 * <br>
	 * @param fas - FasControl device
	 * @param debug - debug flag
	 * @param simulation - simulation flag
	 * @return the dll result int code
	 *
	private int getMachineOpMode(FasControl fas, boolean debug, boolean simulation) throws PPExecutionException
	{
		// aux
		int dllResult = 0;
		UserDialog myUd = null;
		
		try {
			dllResult = (simulation) ? 2 : fas.machineOpMode();
			log(debug, "Var", "checkMachineOpMode = " + String.valueOf(dllResult));

			// warning dialog cycle
			int sec = 20;
			myUd = getPr�flingLaufzeitUmgebung().getUserDialog();
			myUd.setAllowCancel(false);
			while ((dllResult == 1) && (sec >= 0)) {
				// in handmode...
				if (System.getProperty("user.language").equalsIgnoreCase("de")) {
					myUd.displayAlertMessage("Pr�fstand im Handmode", "Bitte in Automatikmodus wechseln\nAbbruch in + " + sec + " Sekunden...", -1);
				} else {
					myUd.displayAlertMessage("Test stand in hand mode", "Please change to auto mode\nAbort in + " + sec + " seconds...", -1);
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// ignore
				}
				dllResult = (simulation) ? 2 : fas.machineOpMode();
				log(debug, "Var", "MachineOpMode (sec = "+ String.valueOf(sec) +") = " + String.valueOf(dllResult));
				sec--;
			}
			if (myUd != null) {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
				myUd = null;
			}		
			return dllResult;

		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}
*/	
	/**
	 * <b>getDllErrorMsg</b>
	 * <br> 
	 * Gets the dll error message matching the given dll result code.
	 * <br>
	 * @param dllJobResult
	 * @return
	 */
	private String getDllErrorMsg(String dllJobResult)
	{
		String errorMsg = "";
		dllJobResult = dllJobResult.trim();
		dllJobResult = dllJobResult.toUpperCase();
		boolean isInt = false;
		try {
			new Integer(dllJobResult).intValue();
			isInt = true;
		} catch (Exception e) {
			// ignore		
		}
			
		// NIO_NETWORK
		if (dllJobResult.equalsIgnoreCase("NIO_NETWORK") || dllJobResult.equalsIgnoreCase("-1")) {
			errorMsg = "DllNetworkError";
		
		// NIO_TARGET_NOT_ACTIVE	
		} else if (dllJobResult.equalsIgnoreCase("NIO_TARGET_NOT_ACTIVE") || dllJobResult.equalsIgnoreCase("-2")) {
			errorMsg = "DllTargetNotActiveError";
		
		// NIO_MECHANICAL_LIMITATIONS	
		} else if (dllJobResult.equalsIgnoreCase("NIO_MECHANICAL_LIMITATIONS") || dllJobResult.equalsIgnoreCase("-3")) {
			errorMsg = "DllMechanicalLimitsError";
		
		// NIO_TIMEOUT_xxx (temp TIMEOUT in DGF)	
		} else if (dllJobResult.startsWith("NIO_TIMEOUT") || dllJobResult.startsWith("TIMEOUT") || dllJobResult.equalsIgnoreCase("-4")) {
			String sTimeout = ""; 
			if (!dllJobResult.equalsIgnoreCase("-4")) {
				int beginIndex = dllJobResult.lastIndexOf("_") + 1;
				int endIndex = dllJobResult.length();
				if ((beginIndex != -1) && (beginIndex < endIndex)) {
					sTimeout = dllJobResult.substring(beginIndex, endIndex);
				}	
			}
			errorMsg = "DllTimeout"+ sTimeout +"Error";
			
		// NIO_NOT_IMPLEMENTED	
		} else if (dllJobResult.startsWith("NIO_NOT_IMPLEMENTED") || dllJobResult.equalsIgnoreCase("-5")) {
			errorMsg = "DllNotImplemented";
		
		// ...UNDEFINED	
		} else if ((isInt && new Integer(dllJobResult).intValue() < 0) || (dllJobResult.startsWith("NIO"))) {
			errorMsg = "DllUndefinedError";
/*		
		// ...INVALID	
		} else {
			errorMsg = "DllInvalidResultError";
*/			
		}
		return errorMsg;
	}
	
	/**
	 * <b>paramXmlDeserialization</b>
	 * <br>
	 * @param serializedXmlString - kompletter Ergebnisstring
	 * @param paramName - zu suchender Parameter
	 * @return paramValue
	 */
	private String paramXmlDeserialization(String serializedXmlString, String paramName) 
	{
		boolean found = false;
		String temp = "", value = "";
		StringTokenizer tokenTaker = new StringTokenizer(serializedXmlString);
		while (tokenTaker.hasMoreTokens() && found == false) {
			temp = tokenTaker.nextToken("<>");
			if (temp.equalsIgnoreCase(paramName)) {
				found = true;
				value = tokenTaker.nextToken("<>");
			}
			tokenTaker.nextToken(">"); //nur das gr��er-Zeichen, sonst funktioniert es nicht
		}
		return (value);
	}
	/**
	 * <b>extractArg</b>
	 * <br>
	 * Extract the 'CASCADEconstruct' value if present in the given argument.
	 * <br>
	 * @param debug is the debug flag
	 * @return the arg extracted value string 
	 * @throws InformationNotAvailableException if a '@contruct' value is no available
	 *
	private String extractArg(String argName) throws InformationNotAvailableException 
	{
		String argValue = getArg(argName);
		if (argValue == null) {
			// NULL value
			return null;
		}
		if (argValue.indexOf("@") != -1) {	
			// @construct value
			argValue = getPPResult(argValue.toUpperCase());
		}
		return argValue;
	}
*/
	/**
	 * <b>extractArg</b>
	 * <br>
	 * Extract the 'CASCADEconstruct' value if present in the given argument.
	 * <br>
	 * @param debug is the debug flag
	 * @return the arg extracted value string 
	 * @throws InformationNotAvailableException if a '@contruct' value is no available
	 */
	private String extractArg(String argName) throws InformationNotAvailableException 
	{
		String argValue = getArg(argName);

		if (argValue == null) {
			// NULL value
			return null;
		}
		if (argValue.indexOf("@") != -1) {	
			//@construct value(s)
			String[] argSubValues = argValue.split(" ");
			argValue = "";
			for (int i=0; i<argSubValues.length; i++) {
				if (argSubValues[i].indexOf("@") != -1) {
					// extract @construct var value
					argSubValues[i] = getPPResult(argSubValues[i].toUpperCase().trim());
				}
				argValue += (i == 0 ? "" : " ") + argSubValues[i];
			}
		}
		return argValue;
	}
	
	/**
	 * <b>log</b>
	 * <br>
	 * Logs a message if the given debug flag is active and all 'Error' messages.
	 * <br>
	 * @param debug - the debug flag
	 * @param type - the type of message to display
	 * @param text - the message text
	 */
	private void log(boolean debug, String type, String text)
	{
		if (type.equalsIgnoreCase("Error") || debug) { 
			System.out.println("["+ getLocalName() +"] ["+ type +"] "+ text);
		}
	}

	/**
	 * <b>rnd</b> 
	 * <br>
	 * Rounds a float value to n decimal places.
	 * <br>
	 * @param value the float value to round
	 * @param decPlaces the number of decimal places to use while rounding
	 * @return the rounded float value
	 */
	private float rnd(float value, int decPlaces)
	{
		float tmp = 1;
		for (int i = 1; i <= decPlaces; i++)
			tmp *= 10;
		return (float)(Math.rint((float)value*tmp))/tmp;
	}
	
	/**
	 * <b>testStandDialog</b> 
	 * <br>
	 * Shows a dialog with the specified message in the screen and with the specified Pruefstand buttons  
	 * to interact with. Can be used with 0, 1 or 2 buttons with or without message dialog.
	 * <br>
	 * @param fas - FasControl device object
	 * @param debug - Debug flag
	 * @param simulation - Simulation flag for simulation purposes.
	 * @param title - Dialog's title. Default is "FAS Dialog".
	 * @param text - Dialog's text. If null/"" no dialog will be shown.
	 * @param timeout - Timeout for the dialog (ms). if < 0 no timeout will be used.
	 * @param polling - Polling time (ms) or -1 to use the default value of 250ms.
	 * @param switchName1 - Name of the pruefstand button 1 or null/"" for no button.
	 * @param switchName2 - Name of the pruefstand button 2 or null/"" for no button.		 
	 * @return 
	 * 		2 : pressed button 2<br> 
	 * 		1 : pressed button 1<br>
	 * 		0 : no button pressed<br> 
	 * 	   -1 : error on button 1<br>
	 * 	   -2 : error on button 2<br>	
	 */
	private int testStandDialog(FasControl fas, boolean debug, boolean simulation, String title, String text, long timeout, long polling, String switchName1, String switchName2) throws PPExecutionException
	{
		long startTime = System.currentTimeMillis();
		long actualTime = System.currentTimeMillis();
		long pollingTime = (polling != -1) ? polling : 250;
		int dllResult = -1;
		UserDialog ud = null;
		boolean displayDlg = (text != null) ? true : false;  
		
		title =  (title != null) ? title : "FAS Dialog"; 
		
		try {
			// start monitoring the specified buttons
			// b1
			if (switchName1 != null && !switchName1.equalsIgnoreCase("")) {
				dllResult = (simulation) ? 1 : fas.switchMonitoring(switchName1, 1); 
				if (dllResult != 1) {
					log(debug, "Error", "turning on switch "+ switchName1 +", "+ getDllErrorMsg(String.valueOf(dllResult)) +" (dllResult = "+ String.valueOf(dllResult) +")");			
					return -1; // Error starting mon1
				}
				dllResult = (simulation) ? 2 : fas.controlSwitchIllumination(switchName1, 2);
				log(debug, "Ok", "turned on switch "+ switchName1);			
				// b2
				if (switchName2 != null && !switchName2.equalsIgnoreCase("")) {
					dllResult = (simulation) ? 1 : fas.switchMonitoring(switchName2, 1);
					if (dllResult != 1) {
						log(debug, "Error", "turning on switch "+ switchName2 +", "+ getDllErrorMsg(String.valueOf(dllResult)) +" (dllResult = "+ String.valueOf(dllResult) +")");			
						return -2; // Error starting mon2
					}
					dllResult = (simulation) ? 1 : fas.controlSwitchIllumination(switchName2, 2);
					log(debug, "Ok", "turned on switch "+ switchName2);			
				}	
			}
			// build buttons line
			final int SIZE_BUTTONS_LINE = 50; //33; //"123456789012345678901234567890123"; 
			String buttonsLine = "";  
			int ins1 = -1;
			int ins2 = -1;
			if (switchName1 != null && !switchName1.equalsIgnoreCase("")) {
				// define insertion points
				if (switchName2 != null && !switchName2.equalsIgnoreCase("")) {
					ins1 = Math.round(   (SIZE_BUTTONS_LINE/3) - ((switchName1.length() + 2)/2) );
					ins2 = Math.round( 2*(SIZE_BUTTONS_LINE/3) - ((switchName2.length() + 2)/2) );
				} else {
					ins1 = Math.round(   (SIZE_BUTTONS_LINE/2) - ((switchName1.length() + 2)/2) );
				}
				// insert button names centered
				while (buttonsLine.length() < SIZE_BUTTONS_LINE) {
					if (buttonsLine.length() == ins1) {
						buttonsLine = buttonsLine +"["+ switchName1 +"]";
					} else if (buttonsLine.length() == ins2) {
						buttonsLine = buttonsLine +"["+ switchName2 +"]";
					} else {
						buttonsLine = buttonsLine +" ";
					}
				}
			}
			if (displayDlg) {
				text = text +"\n\n"+ buttonsLine;
				// get user Dialog
				ud = getPr�flingLaufzeitUmgebung().getUserDialog();
				ud.setAllowCancel(false);
				log(debug, "Ok", "got user dialog.");			
			}
			
			// polling cycle
			int pressed = 0;
			log(debug, "Ok", "starting switches polling...");			
			while (pressed == 0) {
//				log(debug, "Var", "cycle "+ String.valueOf(count));			
				if (timeout > 0 && (actualTime - startTime) > timeout) {
					break;
				}
				if (displayDlg) {
					ud.displayStatusMessage(title, text, -1);
				}
				// mon buttons
				// b1
				if (switchName1 != null && !switchName1.equalsIgnoreCase("")) { 
					dllResult = (simulation) ? 0 : fas.switchActivated(switchName1);
					if (dllResult == 1) {
						pressed = 1; // Button 1 pressed
						log(debug, "Ok", "pressed switch "+ switchName1);			
					} else if (dllResult == 0) {
						// b2
						if (switchName2 != null && !switchName2.equalsIgnoreCase("")) {
							dllResult = (simulation) ? 0 : fas.switchActivated(switchName2);
							if (dllResult == 1) {
								pressed = 2; // Button 2 pressed
								log(debug, "Ok", "pressed switch "+ switchName2);			
							} else if (dllResult < 0) {
								// NIO
								pressed = -2;
								log(debug, "Error", "checking pressed switch "+ switchName2 +", "+ getDllErrorMsg(String.valueOf(dllResult)) +" (dllResult = "+ String.valueOf(dllResult) +")");
							}
						}
					} else {
						// NIO
						pressed = -1;
						log(debug, "Error", "checking pressed switch "+ switchName1 +", "+ getDllErrorMsg(String.valueOf(dllResult)) +" (dllResult = "+ String.valueOf(dllResult) +")");
					}	
				}
				// sleep
				try {
					Thread.sleep(pollingTime);
				} catch (InterruptedException e) {
					// ignore
				}
				actualTime = System.currentTimeMillis();
			}
			
			if (displayDlg) {
				// hide dialog text
				try {
					getPr�flingLaufzeitUmgebung().releaseUserDialog();
					log(debug, "Ok", "released user dialog.");			
				} catch (DeviceLockedException e) {
					log(debug, "Error", "releasing user dialog, Exception: "+ e.getMessage());
				}
			}
			// end monitoring the specified buttons
			// b1
			if (switchName1 != null && !switchName1.equalsIgnoreCase("")) { 
				dllResult = (simulation) ? 0 : fas.switchMonitoring(switchName1, 0);
				dllResult = (simulation) ? 0 : fas.controlSwitchIllumination(switchName1, 0);
				log(debug, "Ok", "turned off switch "+ switchName1);			
				// b2
				if (switchName2 != null && !switchName2.equalsIgnoreCase("")) {
					dllResult = (simulation) ? 0 : fas.switchMonitoring(switchName2, 0);
					dllResult = (simulation) ? 0 : fas.controlSwitchIllumination(switchName2, 0);
					log(debug, "Ok", "turned off switch "+ switchName2);			
				}	
			}
			return pressed;
			
		} catch (Exception e) {
			throw new PPExecutionException(e.getMessage());
		}
	}
		
	/**
	 * <b>getOptionInt</b> 
	 * <br>
	 * Gets the option int value from the given array of options.
	 * <br>
	 * @param opts - the options array
	 * @param arg - option string or null
	 * @return the int value or -1 when not found
	 */
	private int getOptionInt(String[] opts, String arg)
	{
		if (arg == null) {
			// NULL value
			return -1;
		}	
		for (int i=0; i<opts.length; i++) {
			if (!opts[i].equalsIgnoreCase("") && opts[i].equalsIgnoreCase(arg)) {
				// not empty and equal
				return i;
			}
		}
		return -1;
	}

	/**
	 * <b>getOptionsString</b> 
	 * <br>
	 * Gets the array of options as a printable string.
	 * <br>
	 * @param opts - the options array
	 * @return the Sarray of options as a printable string
	 */
	private String getOptionsString(String[] opts)
	{
		String optsStr = "";
		for (int i=0; i<opts.length; i++) {
			optsStr += opts[i] +" ";
		}
		return optsStr;
	}

	/**
	 * <b>fasResourcePool</b>
	 * <br>
	 * 
	 * <br>
	 * @param action - GET, RELEASE
	 * @param tag
	 * @param resource
	 * @return Ok or ErrorMsg
	 */
	private String fasResourcePoolManager(boolean debug, String tag, long timeout, long polling, String action, String resource) throws Exception
	{
		final String OK = "Ok";
		final String ERROR_FILE_CREATE = "ErrorResourceFileCreate";
		final String ERROR_FILE_READ = "ErrorResourceFileRead";
		final String ERROR_FILE_WRITE = "ErrorResourceFileWrite";
		final String ERROR_NOT_OWNER = "ErrorResourceNotOwned";
		final String ERROR_GET_TIMEOUT = "ErrorResourceGetTimeout";
		
		File poolFile = new File(CascadeProperties.getCascadeHome() 
				+ File.separator +"database"
				+ File.separator +"pruefstand"
				+ File.separator +"FasResourcePool.properties");
		
		// check existence/create pool file
		if (!poolFile.exists()) {
			if (!poolFile.createNewFile()) {
				log(debug, "Error", "creating FasResourcePool file: "+ poolFile.getAbsolutePath());
				return ERROR_FILE_CREATE;
			}
			log(debug, "Ok", "created FasResourcePool file: "+ poolFile.getAbsolutePath());
		}
		
		// perform action 
		long startTime = System.currentTimeMillis();
		long actualTime = System.currentTimeMillis();
		String tmp = "";
		
		// Get the resource
		// --------------------
		if (action.equalsIgnoreCase("Get")) {
			while ((actualTime - startTime) <= timeout && !tmp.equalsIgnoreCase("")) {
				try {
					Thread.sleep(polling);
				} catch (InterruptedException e) {
					// ignore
				}
				actualTime = System.currentTimeMillis();
				if (fasResourcePoolIO(poolFile, "READ", resource, tmp) == 0) { // check resource availability
					if (tmp.equalsIgnoreCase("")) {
						if (fasResourcePoolIO(poolFile, "WRITE", resource, tag) == 0) { // possess resource
							return OK;
						} else {
							return ERROR_FILE_WRITE;
						}
					}
				} else {
					return ERROR_FILE_READ; 
				}
					
			}			
			if (!tmp.equalsIgnoreCase("")) {
				return ERROR_GET_TIMEOUT;
			}
			
		// Release the resource
		// --------------------
		} else {
			if (fasResourcePoolIO(poolFile, "READ", resource, tmp) == 0) { // check resource possession
				if (tmp.equalsIgnoreCase(tag)) {
					if (fasResourcePoolIO(poolFile, "WRITE", resource, "") == 0) { // free resource possession
						return OK;
					} else {
						return ERROR_FILE_WRITE;
					}
				} else {
					return ERROR_NOT_OWNER;
				}
			} else {
				return ERROR_FILE_READ;
			}
		}
		
		return OK;
	}

	/**
	 * <b>fasResourcePoolIO</b>
	 * <br>
	 * 
	 * <br>
	 * @param action - READ, WRITE
	 * @param tag
	 * @param resource
	 * @return Ok or ErrorMsg
	 */
	private synchronized int fasResourcePoolIO(File propFile, String action, String resource, String value)
	{
		Properties prop = new Properties();
		try {
			if (action.equalsIgnoreCase("READ")) {
				// READ
				prop.load(new FileInputStream(propFile));
				value = prop.getProperty(resource, "");
				
			} else if (action.equalsIgnoreCase("WRITE")) {
				// WRITE
				prop.setProperty(resource, value);
				prop.store(new FileOutputStream(propFile), "");
			}
			return 0;
			
		} catch (Exception e) {
			return -1;
		}
		
	}
		
	/**
	 * <b>isHighStand</b>
	 * <br> 
	 * Checks the pruefstand's type.
	 * <br>
	 * @param dllJobResult
	 * @return true if it's high and false if it's basic
	 */
	private boolean isHighStand(FasControl fas, boolean simulation) throws Exception
	{
		int dllResult = (simulation) ? 1 : fas.hc2Doppler(0);					
		if (dllResult >= 0) {
			return true;
		} else if (dllResult == -5) {
			return false;
		} else {
			throw new Exception(getDllErrorMsg(String.valueOf(dllResult)));
		}
	}

	/**
	 * <b>hasHeightMeasurement</b>
	 * <br> 
	 * Checks the availability of height measurement lasers.
	 * <br>
	 * @param dllJobResult
	 * @return true if it's available and false if it's not
	 */
	private boolean hasHeightMeasurement(FasControl fas, boolean simulation) throws Exception
	{
		String dllResult = (simulation) ? "0" : fas.vehMeasurement(1);
		System.out.println("vehMeasurement: " + dllResult);
		if (dllResult.compareTo("NIO_NOT_IMPLEMENTED") == 0)
			return false;
		else
			return true;
		
	}
}

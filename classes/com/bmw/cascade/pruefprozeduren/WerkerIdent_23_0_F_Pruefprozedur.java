/*
 * WerkerIdent
 *
 * Created on 26.03.04
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.visualisierung.*;
import com.bmw.cascade.pruefstand.devices.uianalyse.*;

/**
 * Implementierung der Pr�fprozedur, die dem Werker M�glichkeit gibt, seine Pr�fernummer einzugeben.
 * @author Z. Wen ESG Elektroniksystem- und Logistik-GmbH
 * @version <br>
 *      V0_0_3_TA               We  Neue Implementierung <br>
 *      V0_0_4_FA               We  FA-Version <br>
 *      V0_0_5_TA               We  mehrere Kommentare <br>
 *      V0_0_6_TA               We  Verbesserung bei der Eingabe der Werkernummer <br>
 *      V0_0_7_TA 21.09.2004    We  Am Ende der Ausf�hrung die Pr�fnummer wird gespeichert <br>
 *      V0_0_8_TA 26.10.2004    We  Beim Werkabbruch wird die Pr�fung abgebrochen <br>
 *      V0_0_9_FA 26.10.2004    We  Werkernummer ist zwingend einzugeben <br>
 *      21_0_F 	  22.01.2014    MS  Exceptionhandling verbessert <br>
 *      22_0_T 	  25.11.2015    PR  Kein Aufruf der Methode UIAnalyserEcos.setWid() mehr.<br>
 *      23_0_F 	  25.11.2015    PR  F-Version mit Code der letzten T-Version.<br>
 */
public class WerkerIdent_23_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    	static final long serialVersionUID = 1L;
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public WerkerIdent_23_0_F_Pruefprozedur() {}

  //Werker-Nummer
    //Wenn man sich ausloggen will, muss man 000 als Werker-Nummer eingeben
    private static String wid = "000";
    
    /**
     * erzeugt eine neue Pruefprozedur.
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public WerkerIdent_23_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    /**
     * initialsiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /**
     * liefert die optionalen Argumente
     */
    public String[] getOptionalArgs() {
        String[] args = new String[0];
        return args;
    }
    
    /**
     * liefert die zwingend erforderlichen Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = new String[0];
        return args;
    }
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
     */
    public boolean checkArgs() {
        boolean ok;
        //Parameterpr�fung
        try{
            ok = super.checkArgs();
            return ok;
        } catch (Exception e) {
            return false;
        } catch (Throwable e) {
            return false;
        }
    }
    
    /**
     * f�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        // spezifische Variablen
        String pruefstandnr;            //Pr�fstand-Name
        int answer;                     //Antwort von Dialogbox
        UserDialog myDialog = null;     //Variable f�r Dialogbox
        
        /***********************************************
         * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
         * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
         ***********************************************/
        try {
            UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
            myAnalyser.LogSetTestStepName(this.getName());
            getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
        } catch (Exception e) {
            try {
                getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
            }
            catch (Exception e1) {}
        }

        try {
            //�berpr�fung der Parameter
            try {
                if( checkArgs() == false ) throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
            } catch (PPExecutionException e) {
                //Parametrierfehler
                e.printStackTrace();
                if (e.getMessage() != null)
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "parametrierfehler" ), Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "parametrierfehler" ), Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            //Holen des Devices Dialogbox
            try {
                myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
            } catch (Exception e) {
                //Systemfehler, muss man genau analysieren
                e.printStackTrace();
                result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException("Get UserDialog");
            }
            
            //Holen des Pr�fstandsnamens
            pruefstandnr = getPr�flingLaufzeitUmgebung().getName();
            StringTokenizer toker = new StringTokenizer(pruefstandnr, "@");
            while (toker.hasMoreTokens()){
                pruefstandnr = toker.nextToken();
            }
            
            //Holen der Werker-Nummer
            if(wid.equalsIgnoreCase("000")){    //Wenn keine g�ltige Werkernummer vorhanden ist, wird neue Werker-Nummer geholt
                wid = myDialog.requestUserInput(PB.getString( "pruefstandInfo" ), PB.getString( "pruefstandNummer" ) + ": " + pruefstandnr + "\r\n" + PB.getString( "pruefernummerEingabe" ), 0);
                getPr�flingLaufzeitUmgebung().releaseUserDialog();
                wid = wid.trim();
                while(wid.compareTo("") == 0){  //Wenn keine Werkernummer eingegeben ist
                    wid = myDialog.requestUserInput(PB.getString( "pruefstandInfo" ), PB.getString( "pruefstandNummer" ) + ": " + pruefstandnr + "\r\n" + PB.getString( "pruefernummerEingabe" ), 0);
                    getPr�flingLaufzeitUmgebung().releaseUserDialog();
                    wid = wid.trim();
                }
                if(wid.equalsIgnoreCase("000")){//Wenn man sich doch ausloggen will, wird die Pr�fung abgebrochen
                    getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().stopExecution();
                }
            } else {    //Eine g�ltige Werkernummer ist vorhanden, werden Pr�fstand-Name und Werker-Nummer als Information ausgegeben
                answer = myDialog.requestUserInputDigital( PB.getString( "pruefstandInfo" ), PB.getString( "pruefstandNummer" ) + ": " + pruefstandnr + "\r\n\r\n" + PB.getString( "pruefernummer" ) + ": " + wid, 0 );
                getPr�flingLaufzeitUmgebung().releaseUserDialog();
                if (answer == getPr�flingLaufzeitUmgebung().getUserDialog().NO_KEY) {   //Wenn man sich doch ummelden will, wird neue Werker-Nummer geholt
                    wid = myDialog.requestUserInput(PB.getString( "pruefstandInfo" ), PB.getString( "pruefstandNummer" ) + ": " + pruefstandnr + "\r\n" + PB.getString( "pruefernummerEingabe" ), 0);
                    getPr�flingLaufzeitUmgebung().releaseUserDialog();
                    wid = wid.trim();
                    while(wid.compareTo("") == 0){  //Wenn keine Werkernummer eingegeben ist
                        wid = myDialog.requestUserInput(PB.getString( "pruefstandInfo" ), PB.getString( "pruefstandNummer" ) + ": " + pruefstandnr + "\r\n" + PB.getString( "pruefernummerEingabe" ), 0);
                        getPr�flingLaufzeitUmgebung().releaseUserDialog();
                        wid = wid.trim();
                    }
                    if(wid.equalsIgnoreCase("000")){//Wenn man sich doch ausloggen will, wird die Pr�fung abgebrochen
                        getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().stopExecution();
                    }
                }
            }
            //Die eingegebene Werker-Nummer wird in Ergebnis abgespeichert
            result = new Ergebnis( PB.getString( "pruefernummer" ), PB.getString("Werker"), "", "", "", PB.getString( "pruefernummer" ), wid, "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
            ergListe.add(result);
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            //Systemfehler, muss man genau analysieren
            e.printStackTrace();
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
            //Systemfehler, muss man genau analysieren
            e.printStackTrace();
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
               
        //Device Dialgobox wird freigegeben
        try {
            getPr�flingLaufzeitUmgebung().releaseUserDialog();
        } catch (Exception e) {
            //Systemfehler, muss man genau analysieren
            e.printStackTrace();
            if (e instanceof DeviceLockedException)
                result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
            else
                result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "release", Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
        //Status setzen
        setPPStatus( info, status, ergListe );
        
    }
    
}


/*
 * EWSSynchro_???_Pruefprozedur.java
 *
 * Created on 11.02.2005
 *
 * *
 * Created:          11.02.2005
 * Last modified:    See history
 *
 * Author:           Peter Winklhofer, BMW TI-430
 * Contact:          Tel.: +49/89/382-52493 (Winklhofer)
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.lang.reflect.Method;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.util.dom.*;
import com.bmw.cascade.*;

//new imports for DOM-Parser
import javax.xml.parsers.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;


/*
 * EXAMPLE for old format (Version 1)
 *  <?xml version="1.0" encoding="iso-8859-1" ?> <ewsinfo
 * vin="WBAEE111119046000"> <ewsecu> <ewsecutype>CAS3</ewsecutype> </ewsecu>
 * <ewssynchro> <ewsinterface>EWS4</ewsinterface>
 * <ewscode>0123456789ABCDEF0123456789ABCDEF</ewscode> </ewssynchro> </ewsinfo>
 * 
 * EXAMPLE for new format (Version 4)
 * <?xml version="1.0" encoding="iso-8859-1"?>
 * <immobiliser vin="WBAWB735X7PV82000">
 *	<ewsInfo  creationDateTime="2007-01-30T08:33:58" version="4.0">
 *		<ewsEcu>
 *			<ewsEcuType>CAS4</ewsEcuType>
 *		</ewsEcu>
 *		<ewsSynchro>
 *			<ewsInterface>EWS4</ewsInterface>
 *			<ewsCode encryption="AES" ecuType="DMEDDE1">1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF</ewsCode>
 *			<ewsCode encryption="STD" ecuType="EGS">F4804706</ewsCode>
 *		</ewsSynchro>
 *	</ewsInfo>
 * </immobiliser>
 */

/**
 * Diese Pr�fprozedur umfasst alle EWS4 aktionen, die mit der Dokumentation bzw.
 * der Generierung des SecretKeys in Verbindung stehen. 
 * Sie dient auch zum Erzeugen einer neuen Dokumentation von EWS-Steuerger�t (z.B.CAS2, CAS3, CAS4) 
 * und Schnittstellen-Typ bei EWS3. <BR>
 * ENGLISH: <BR>
 * This Pr�fprozedur is for all EWS4 processes, that have to do with
 * documentation / generation of the secretkey. 
 * It also is used to create a new documentation of the ewsecutype (e.g. CAS2, CAS3, CAS4)
 * and the ewsinterface when using EWS3. 
 * The required Argument OP decides wich operation will be performed.
 * Each Operation may require individual optional Args. 
 * The following operations are supported:<br>
 * GENERATE, READXML, CLEAR, DOKU, DOMACCESS. <BR>
 * Description of the optional Arguments: <BR> 
 * <b>DEBUG</b>: If 'TRUE' or value greater 0 the debugging-mode is enabled. Available for all OPs. <BR>
 * <b>EWSECUTYPE</b>: Possible values are EWS3, EWS4.3, EWS4.4, CASE65, CASE60, CAS2, CAS4, FEM. Available for GENERATE. <BR> 
 * <b>EWSINTERFACE</b>: Possible values are EWS3, EWS4, EWS4EGS. Available for GENERATE. <BR>
 * <b>GEN_DEFAULTCODE</b>: When 'TRUE' the default developement secretkey will be used. Available for GENERATE (with EWSINTERFACE = EWS4). <BR>
 * <b>STORAGE</b>: With this optional Argument you may specify another storage PL then the current PL. Available for all OPs. <BR>
 * <b>DOKU_PARTITION</b>: With this optional Argument you may specify the maximum size of a EWSSYNC.DAT-file (local history). Default is unlimited. Available for DOKU. <BR>
 * <b>READ_SOURCE1</b>: Specifies the first source where a XML-dataset (vinE.xml) is searched. <BR> 
 * Possible values for READ_SOURCEx are SERVER, PREUFSTAND_DOM (same as LOCAL) and DOM. Required for READXML.
 * <b>READ_SOURCE2</b>: Specifies the second source where a XML-dataset is searched. Available for READXML. <BR>
 * <b>READ_SOURCE3</b>: Specifies the third source where a XML-dataset is searched. Available for READXML. <BR>
 * <b>READ_OP</b>: Specifies what is done with the read dataset. Possible modes are READ, DSEXISTIO, DSEXISTNIO. Available for READXML. <BR>
 * <b>DOKU_MODE</b>: The following Modes are supported NORMAL (send it to CASCDE-Server, PATH (store it to given PATH), ALL (both NORMAL and PATH). Default is Normal. <br>
 * <b>DOKU_PATH</b>: In conjunction with DOKU_MODE=PATH this parameter may define another path than the default one in package-properties or cas.properties.<br>
 * <b>DOMACCESS_OP</b>: Specifies the sub-operation of DOMACCESS. Possible modes are LOG_ON, LOG_OFF, TEST, LOG_ON_WITH_TEST. Required for OP=DOMACCESS. <BR>
 * <b>DOMACCESS_TIMEOUT</b>: Specifies the timeout(minutes) for keeping the last combination of userId and Password (without any dom-access). Zero minutes will force userId and passoword always to be typed in. Max. 60 minutes. Default: 0 minutes. Available for all OPs that access DOM. <br>
 * <b>FORMAT_VERSION</b>: Specifies the version of the xml-format. Possible values are: 1, 1.0, 4, 4.0. Default is old format (1.0). Available for GENERATE.<br>
 * <b>ENC_ALGORITHM</b>: Specifies the encryption algorithm dom-datasets. STD->Standard-Encryption, PLAIN->no encryption. 60 minutes. Default: STD. Available for DOKU. <br>
 * <b>DOM_URL</b>: Specifies the URL to the DOM-Database for accessing stored datasets. Default: 'http://www6.muc:5358/wsvh/vehicleImmobilizer?vin='. <br>
 * 
 * @author Peter Winklhofer, TI-431
 * @version 0_0_1_FA PW 15.02.2005 Erstellung (Winklhofer) <BR>
 *          0_0_2_TA PW 17.03.2005 Bug-Fix readXML <BR>
 *          0_0_3_FA PW 22.03.2005 FA-Version <BR>
 *          0_0_4_FA PW 12.05.2005 Bug-Fix DOM Transfer <BR>
 *          4_0_F conversion to cascade 2.0 by kernteam <BR>
 *          5_0_T PW 28.06.2005 adaption to new ewsinfo.dtd version 1.0 (ewswcuinterface changed to ewsinterface) <BR>
 *          6_0_F PW 28.06.2005 FA-Version <BR>
 *          7_0_T PW 03.10.2005 Added support for reading from DOM Added doku local when reading from DOM Added support for DOKU_MODE and DOKU_PATH or PS-Variable CAS_DOKU_PATH <BR>
 *          8_0_F PW 06.03.2006 Vorab F-Version f�r Ersatzplatz-Test implements DOM-Modus <BR>
 *          9_0_T FD 20.03.2006 Use requestUserInputPassword if possible, requestUserInput otherwise <BR>
 *          10_0_F FD 04.09.2006 F-Version <BR>
 *          11_0_T PW 20.02.2007 Erweiterungen f�r L6, Neu: READ_SOURCEx=PREUFSTAND_DOM (identisch zu LOCAL) <BR>
 *          12_0_F FD 13.03.2007 F-Version <BR>
 *          13_0_T PW 22.10.2007 Anpassungen an finales DOM-Format f�r L6 <BR>
 *          14_0_F PW 24.10.2007 F-Version <BR>
 *          15_0_T_FD 07.04.2008 Bugfixes beim Parsen des neuen Formats (vehicleImmobilizer) <BR>
 *                               Bugfix beim XML Generieren: Jahreszahl enthielt 5 statt 4 Ziffern <BR>                         
 *                               Default Webservice zum Holen des Datensatzes von DOM: http://www6.muc:5358/wsvh/vehicleImmobilizer?vin= <BR>
 *          16_0_T FD 21.04.2008 Ref-Schema synchronisationInfo Version 1.02 -> 1.03 <BR>
 *          17_0_F FD 21.04.2008 F-Version <BR>
 *          18_0_T FD 21.04.2008 Ref-Schema vehicleImmobilizer Version 1.00 -> 1.01 <BR>
 *          19_0_F FD 21.04.2008 F-Version <BR>
 *          20_0_F AU 21.10.2009 EWSEcuType FEM hinzugef�gt
 *          21_0_T FD 23.10.2009 "_A" an Splittet-Filename angeh�ngt (f�r Backuptool)
 *          22_0_F FD 23.10.2009 F-Version <BR>
 */
public class EWSSynchro_22_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public EWSSynchro_22_0_F_Pruefprozedur() {
	}
	
	//Konstanten
	/**
	 * number of rands to be used
	 */
	private static final int NUM_RANDS = 3;

	//Instanz-variablen
	/**
	 * Debug-Level (0=Debug is off).
	 */
	private int debug = 0;

	/**
	 * Last used random-generator
	 */
	private int lastUsedRandom = 0;

	/**
	 * Randoms for generating random numbers
	 */
	private Random rands[] = new Random[NUM_RANDS];

	/**
	 * Buffer for generating hex-strings
	 */
	StringBuffer randBufferRes = new StringBuffer(30);

	/**
	 * Buffer for generating hex-strings of length 2
	 */
	StringBuffer randBuffer2 = new StringBuffer(4);

	/**
	 * Defines the PL that stores the dynamic attributes.
	 * Needed until a new mechanism is integrated into the cascade kernel.
	 * Default: current PL.
	 */
	String Storage = null;

	/**
	 * User-ID for logon to the DOM-Database
	 */
	private String domUser = null;

	/**
	 * Password for logon to the DOM-Database
	 */
	private String domPwd = null;

	/**
	 * URL for accessing the DOM-Database
	 */
	private String domUrl = null;
	
	//Public static definitionen
	/**
	 * Default-URL for accessing the DOM-Database
	 */
	public static final String DEFAULT_DOM_URL = "http://www6.muc:5358/wsvh/vehicleImmobilizer?vin=";
	// URL to test-web-site: http://wwwint.muc:5358/wsvh/index.html
	// URL to test-service ewsInfo:
	// http://wwwint.muc:5358/wsvh/ewsInfo?vin=01234567899046000

	/**
	 * Label of the dynamic attribute that stores the user-ID for DOM logon.
	 */
	public static final String DOM_USER_PS_VAR = "DOM_ACCESS_USER";

	/**
	 * Label of the dynamic attribute that stores the password for DOM logon.
	 */
	public static final String DOM_PWD_PS_VAR = "DOM_ACCESS_PWD";

	/**
	 * Label of the dynamic attribute that stores the timestamp of the last DOM access.
	 */
	public static final String DOM_TIME_PS_VAR = "DOM_ACCESS_TIME";

	/**
	 * Possible operationmodes for EWSSynchro GENERATE, READXML, READ (same as READXML), CLEAR, DOKU, DOMACCESS.
	 */
	public static final String Operations[][] = { { "GENERATE", "G" },
			{ "READXML", "X" }, { "READ", "X" }, { "CLEAR", "C" },
			{ "DOKU", "D" }, { "DOMACCESS", "M" } };

	/**
	 * Possible EWSECUTypes. Currently avaiable: EWS3, EWS4.3, EWS4.4, CASE65, CASE60, CAS2, CAS3, CAS4, FEM
	 */
	public static final String EWSECUTypes[] = { "EWS3", "EWS4.3", "EWS4.4",
			"CASE65", "CASE60", "CAS2", "CAS3", "CAS4", "FEM" };

	/**
	 * Possible EWSInterfaceTypes: EWS3, EWS4, EWS4EGS
	 */
	public static final String EWSInterfaces[] = { "EWS3", "EWS4", "EWS4EGS" };	
	
	/**
	 * Encryption algorithm used
	 * PLAIN->no encryption
	 * STD->Standard encryption (default)
	 * AES->AES encryption (not implemented yet)
	 */
	public static final String EncAlgorithms[] = {"PLAIN", "STD", "AES"};
	
	/**
	 * Possible DatasetSources for OP=READXML. Currently available: 
	 * SERVER, PRUEFSTAND_DOM, LOCAL (same as PRUEFSTAND_DOM), LOKAL (same as PRUEFSTAND_DOM), DOM. <BR>
	 * For special use-cases: MOVE, SERVERMOVE (same as MOVE)
	 */
	public static final String DatasetSources[][] = { { "MOVE", "M" },
			{ "SERVERMOVE", "M" }, { "SERVER", "S" },
			{ "PRUEFSTAND_DOM", "L" }, { "LOCAL", "L" }, { "LOKAL", "L" },
			{ "DOM", "D" } };

	/**
	 * Possible operationmodes for READXML.
	 * Currently available: READ, DSEXISTIO, DSEXISTNIO
	 */
	public static final String ReadOperations[][] = { { "READ", "R" },
			{ "DSEXISTIO", "E" }, { "DSEXISTNIO", "N" } };

	/**
	 * Possible DOKU_MODEs.<br>
	 * Currently available modes (default is Normal):<br>
	 * NORMAL: Send created XML-file to the CASCADE-server.<br>
	 * PATH: Store XML-file to a separate path (used at sparepartcenter).<br>
	 * ALL: Use both NORMAL and PATH at the same time.<br>
	 * The following matrix describes the destinations for the Xml-dataset: <br>
	 * <table border="1"> 
	 * <tr>
	 * 	<th></th>
	 * 	<th colspan="3">>Destinations</th>
	 * </tr>
	 * <tr>
	 * 	<th>DOKU_MODE=</th>
	 * 	<th>LOCAL</th>
	 * 	<th>DOM /<br>CASCADE SERVER</th>
	 * 	<th>PATH</th>
	 * </tr>
	 * <tr>
	 * 	<td>NORMAL</td>
	 * 	<td>YES</td>
	 * 	<td>YES</td>
	 * 	<td>NO</td>
	 * </tr>
	 * <tr>
	 * 	<td>PATH</td>
	 * 	<td>YES</td>
	 * 	<td>NO</td>
	 * 	<td>YES</td>
	 * </tr>
	 * <tr>
	 * 	<td>ALL</td>
	 * 	<td>YES</td>
	 * 	<td>YES</td>
	 * 	<td>YES</td>
	 * </tr>
	 * </table> 
	 * <b>Destination LOCAL</b>: write a line to ewssync.dat<br>
	 * <b>Destination DOM</b>: Write a xml-file to database/pruefstand/dom. It will be automatically moved to the servers daten$/dom/temp/vin7/.<br>
	 * <b>Destination PATH</b>: Write a xml-file to a configureable Path (optional DOKU_PATH).<br> 
	 */
	public static final String DokuModes[][] = { { "NORMAL", "N" },
			{ "PATH", "P" }, { "ALL", "A" } };

	/**
	 * possible operationmodes for DOMACCESS:
	 * LOG_ON, LOG_OFF, LOG_ON_WITH_TEST, TEST
	 */
	public static final String DomAccessOperations[][] = { { "LOG_ON", "N" },
			{ "LOG_OFF", "F" }, { "LOG_ON_WITH_TEST", "W" }, { "TEST", "T" } };

	/**
	 * Label of the dynamic attribute that stores the ewsecutype. Example values: "EWS3","CAS2","CAS3"
	 */
	public static final String LABEL_EWS_ECUTYPE = "EWS_ECUTYPE";

	/**
	 * Label of the dynamic attribute that stores the ewsinterface. Example value: "EWS3", "EWS4",
	 * "EWS4EGS"
	 */
	public static final String LABEL_EWS_INTERFACE = "EWS_INTERFACE";

	/**
	 * Label of the dynamic attribute that stores the ews4-secretkey in HEX. Example value:
	 * "0123456789ABCDEF0123456789ABCDEF" Random.
	 */
	public static final String LABEL_EWS_CODE = "EWS_CODE";

	/**
	 * Label of the dynamic attribute that stores the EGS-ISN in HEX. Example value:
	 * "01234567" Random.
	 */
	public static final String LABEL_EGS_CODE = "EGS_ISN";
	
	/**
	 * Developement Startvalue "0123456789ABCDEF0123456789ABCDEF" bei SA910
	 * (Entwicklungsfzg.), bei SA911 (Entwicklungsfzg.)
	 */
	public static final String DEFAULT_EWS_CODE = "2E69942790DD7AE27A9F338EA76C490B";

	/**
	 * Filename used for local documentation (history) of datasets.
	 */
	public static final String DOKU_LOCAL_FILENAME = "EWSSYNC.dat";

	/**
	 * Default path used for local documentation (history) of datasets
	 */
	public static final String DEFAULT_DOKU_LOCAL_PATH = "C:/";

	/**
	 * Property defining the path used for local documentation (history) of datasets.
	 * PB.getString() is used.
	 */
	public static final String DOKU_LOCAL_PATH_PROPERTY = "casTracePath";

	/**
	 * Prefix for the local filename (history) in partition mode
	 */
	public static final String DOKU_LOCAL_PREFIX = "EWSSYNC";

	/**
	 * extension for the local filename (history) in partition mode
	 */
	public static final String DOKU_LOCAL_EXTENSION = "DAT";

	/**
	 * Dummy for not set dynamic attributes.
	 */
	public static final String DYN_DUMMY = "UNKNOWN";

	/**
	 * Max DomAccessTimeout in minutes.
	 */
	public static final int DOMACCESS_TIMEOUT_MAX = 60;

	/**
	 * Default DomAccessTimeout in minutes.
	 */
	public static final int DOMACCESS_TIMEOUT_DEFAULT = 0;

	/**
	 * Default DomAccessTimeout when OP=DOMACCESS in minutes
	 */
	public static final int DOMACCESS_TIMEOUT_DEFAULT_FOR_DOMACCESS = 2;

	//Private definitionen f�r externe/interne Werte
	/**
	 * EWS_ECUTYPE storage type: String example values: "EWS3","CAS2","CAS3"
	 */
	private String Ews_EcuType = null;

	/**
	 * EWS_INTERFACE storage type: String example value: "EWS3", "EWS4",
	 * "EWS4EGS"
	 */
	private String Ews_Interface = null;

	/**
	 * EWS_CODE storage type: HEX example value:
	 * "0123456789ABCDEF0123456789ABCDEF" Random
	 */
	private String Ews_Code = null;

	/**
	 * EGS_ISN storage type: HEX example value:
	 * "01234567" Random
	 */
	private String Egs_Code = null;
	
	/**
	 * Encryption algorithm used
	 * PLAIN->no encryption
	 * STD->Standard encryption (default)
	 * AES->AES encryption (not implemented yet)
	 */
	private String EncAlgorithm = null;
	
	// private definitionen f�r rein interene Werte
	/**
	 * Operation to be executed.
	 */
	private char Op = ' ';

	/**
	 * Should the default start code be used (for development only)
	 */
	private boolean GenDefaultCode = false;

	/**
	 * Should the dataset be checked before storing it (to XML or locally)
	 */
	private boolean CheckDataset = true;

	/**
	 * Should the local documentation file be splitted into partitions (spilt size is given or 0).
	 */
	private int DokuPartition = 0;

	/**
	 * ReadSource1 for Datasets.
	 */
	private char DatasetSource1 = ' ';

	/**
	 * ReadSource2 for Datasets.
	 */
	private char DatasetSource2 = ' ';

	/**
	 * ReadSource3 for Datasets.
	 */
	private char DatasetSource3 = ' ';

	/**
	 * Operation for READXML.
	 */
	private char ReadOp = ' ';

	/**
	 * Operation for DOMACCESS
	 */
	private char DomAccessOp = ' ';

	/**
	 * Location for storing the xml-file when DOKU_MODE=PATH. If null use
	 * defaults (properties).
	 */
	private String DokuPath = null;

	/**
	 * Timeout for keeping DOM-userId and -password valid. Default: 0 Minutes.
	 */
	private int DomAccessTimeout = DOMACCESS_TIMEOUT_DEFAULT;

	/**
	 * Defines if the xml-file will be sent to the CASCADE-Server (and later to DOM).
	 * Possible DOKU_MODEs.<br>
	 * Currently available modes (default is Normal):<br>
	 * NORMAL: Send created XML-file to the CASCADE-server.<br>
	 * PATH: Store XML-file to a separate path (used at sparepartcenter).<br>
	 * ALL: Use both NORMAL and PATH at the same time.<br>
	 * The following matrix describes the destinations for the Xml-dataset: <br>
	 * <table border="1"> 
	 * <tr>
	 * 	<th></th>
	 * 	<th colspan="3">>Destinations</th>
	 * </tr>
	 * <tr>
	 * 	<th>DOKU_MODE=</th>
	 * 	<th>LOCAL</th>
	 * 	<th>DOM /<br>CASCADE SERVER</th>
	 * 	<th>PATH</th>
	 * </tr>
	 * <tr>
	 * 	<td>NORMAL</td>
	 * 	<td>YES</td>
	 * 	<td>YES</td>
	 * 	<td>NO</td>
	 * </tr>
	 * <tr>
	 * 	<td>PATH</td>
	 * 	<td>YES</td>
	 * 	<td>NO</td>
	 * 	<td>YES</td>
	 * </tr>
	 * <tr>
	 * 	<td>ALL</td>
	 * 	<td>YES</td>
	 * 	<td>YES</td>
	 * 	<td>YES</td>
	 * </tr>
	 * </table> 
	 * <b>Destination LOCAL</b>: write a line to ewssync.dat<br>
	 * <b>Destination DOM</b>: Write a xml-file to database/pruefstand/dom. It will be automatically moved to the servers daten$/dom/temp/vin7/.<br>
	 * <b>Destination PATH</b>: Write a xml-file to a configureable Path (optional DOKU_PATH).<br> 
	 */
	private boolean DokuToDom = true;

	/**
	 * Defines if the xml-file will be sent to a separate path (used at sparepartcenter).
	 * Possible DOKU_MODEs.<br>
	 * Currently available modes (default is Normal):<br>
	 * NORMAL: Send created XML-file to the CASCADE-server.<br>
	 * PATH: Store XML-file to a separate path (used at sparepartcenter).<br>
	 * ALL: Use both NORMAL and PATH at the same time.<br>
	 * The following matrix describes the destinations for the Xml-dataset: <br>
	 * <table border="1"> 
	 * <tr>
	 * 	<th></th>
	 * 	<th colspan="3">>Destinations</th>
	 * </tr>
	 * <tr>
	 * 	<th>DOKU_MODE=</th>
	 * 	<th>LOCAL</th>
	 * 	<th>DOM /<br>CASCADE SERVER</th>
	 * 	<th>PATH</th>
	 * </tr>
	 * <tr>
	 * 	<td>NORMAL</td>
	 * 	<td>YES</td>
	 * 	<td>YES</td>
	 * 	<td>NO</td>
	 * </tr>
	 * <tr>
	 * 	<td>PATH</td>
	 * 	<td>YES</td>
	 * 	<td>NO</td>
	 * 	<td>YES</td>
	 * </tr>
	 * <tr>
	 * 	<td>ALL</td>
	 * 	<td>YES</td>
	 * 	<td>YES</td>
	 * 	<td>YES</td>
	 * </tr>
	 * </table> 
	 * <b>Destination LOCAL</b>: write a line to ewssync.dat<br>
	 * <b>Destination DOM</b>: Write a xml-file to database/pruefstand/dom. It will be automatically moved to the servers daten$/dom/temp/vin7/.<br>
	 * <b>Destination PATH</b>: Write a xml-file to a configureable Path (optional DOKU_PATH).<br> 
	 */
	private boolean DokuToPath = false;

	/**
	 * Erzeugt eine neue Pruefprozedur, die auf Einhaltung bzw. auf Ausschlu�
	 * der zu analysierenden Werte pr�ft.
	 * 
	 * @param tempPruefling
	 *            Klasse des zugeh. Pr�flings
	 * @param tempPruefprozName
	 *            Name der Pr�fprozedur
	 * @param tempHasToBeExecuted
	 *            Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public EWSSynchro_22_0_F_Pruefprozedur(Pruefling tempPruefling,
			String tempPruefprozName, Boolean tempHasToBeExecuted) {
		super(tempPruefling, tempPruefprozName, tempHasToBeExecuted);
		resetVariables();
		attributeInit();
	}

	/**
	 * Initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Set all instance-variables to null / set all references in arrays to null
	 */
	private void resetVariables() {
		if (debug > 2)
			System.out.println("CASPlusDATA.resetVariables");
		Ews_EcuType = null;
		Ews_Interface = null;
		Ews_Code = null;
		Egs_Code = null;

		// internal status-variables
		// operation
		Op = ' ';
		// should the default start code be used (for development only)
		GenDefaultCode = false;
		// should the dataset be checked before storing it (to XML or locally)
		CheckDataset = true;
		// should the local documentation file be splitted into partitions
		DokuPartition = 0;
		// ReadSource1 for Datasets
		DatasetSource1 = ' ';
		// ReadSource2 for Datasets
		DatasetSource2 = ' ';
		// ReadSource3 for Datasets
		DatasetSource3 = ' ';
		// operation for READXML
		ReadOp = ' ';
		//Defines if the xml-file will be sent to the CASCADE-Server (and later to DOM).
		DokuToDom = true;
		//Defines if the xml-file will be sent to a separate path (used at sparepartcenter).
		DokuToPath = false;
	}

	/**
	 * Liefert die optionalen Argumente.<br> 
	 * DEBUG: Debug on/off (TRUE,FALSE, Level 0,1,2,...)<br>
	 */
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG", "EWSECUTYPE", "EWSINTERFACE",
				"GEN_DEFAULTCODE", "STORAGE", "DOKU_PARTITION", "READ_SOURCE1",
				"READ_SOURCE2", "READ_SOURCE3", "READ_OP", "DOKU_PATH",
				"DOKU_MODE", "DOMACCESS_OP", "DOMACCESS_TIMEOUT", "ENC_ALGORITHM", "DOM_URL"};
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.<br> 
	 * OP: Operation (GENERATE) <br>
	 * CASTYPE: Modus/Baureihe (PL2) <br>
	 */
	public String[] getRequiredArgs() {
		String[] args = { "OP" };
		return args;
	}

	/**
	 * Pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 * �berschreibt die parent-Methode aufgrund der offenen Anzahl an Results
	 */
	public boolean checkArgs() {
		String value;
		char op = ' ';
		char s1 = ' ', s2 = ' ', s3 = ' ';
		int i;
		boolean ok;

		// Optinal Arg DEBUG 
		// =====================================================
		
		//Debug: true is mapped to 2.
		value = getArg("DEBUG");
		if ((value == null) || (value.length() == 0)) {
			debug = 0;
		} else if ("TRUE".equalsIgnoreCase(value)) {
			debug = 2;
		} else if ("FALSE".equalsIgnoreCase(value)) {
			debug = 0;
		} else {
			try {
				debug = Integer.parseInt(value);
				if (debug < 0)
					debug = 0;
			} catch (NumberFormatException nfe) {
				System.out.println("EWSSynchro.checkArgs: DEBUG invalid");
				return false;
			}
		}
		if (debug > 0)
			System.out.println("EWSSynchro.checkArgs:  DEBUG enabled! Level:" + debug);
		if (debug > 0)
			System.out.println("EWSSynchro.checkArgs: Start");
		if (!super.checkArgs()) {
			if (debug > 0)
				System.out.println("EWSSynchro.checkArgs: super.checkArgs() failed");
			return false;
		}
		if (debug > 1)
			System.out.println("EWSSynchro.checkArgs: super.checkArgs() okay");

		// required Args
		// =====================================================

		// Check OP, OP must be defined in Operations
		value = getArg("OP");
		if (value == null) {
			if (debug > 0)
				System.out.println("EWSSynchro.checkArgs: OP not defined");
			return false; // required arg is not present
		}
		ok = false;
		//Loop over all available Operartions
		for (i = 0; i < Operations.length; i++) {
			//Is the current Argumentvalue equal to the Optionname
			if (value.equalsIgnoreCase(Operations[i][0])) {
				ok = true; // operation has been found in available Operations
				op = Operations[i][1].charAt(0); // Assign Optioncharacter to op
			}
		}
		if (!ok) { 
			// current Argumentvalue is not contained in available options
			if (debug > 0)
				System.out.println("EWSSynchro.checkArgs: OP '" + value + "' not known");
			return false;
		}
		if (debug > 1)
			System.out.println("EWSSynchro.checkArgs: OP '" + value + "' is okay");

		// optional Args (may also be required depending on OP)
		// =====================================================

		// DEBUG, already handled above
		// Check EWSECUTYPE
		value = getArg("EWSECUTYPE");
		if (value != null) {
			// we have a EWSECUTYPE
			if (op != 'G') {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: EWSECUTYPE '"
							+ value + "' not allowed when OP!=GENERATE");
				return false; // only allowed when OP=GENERATE
			}
			// only values in EWSECUTypes allowed
			ok = false;
			//Loop over all available EWSECUTypes
			for (i = 0; i < EWSECUTypes.length; i++) {
				//Is the current Argumentvalue equal to the EWSECUType?
				if (value.equalsIgnoreCase(EWSECUTypes[i])) {
					ok = true; // EWSECUType has been found in available EWSECUTypes
				}
			}
			if (!ok) { // current Argumentvalue is not contained in available EWSECUTypes
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: EWSECUTYPE '"
							+ value + "' not known");
				return false;
			}
		} else {
			//We have NO EWSECUTYPE
			if (op == 'G') {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: EWSECUTYPE needed when OP==GENERATE");
				return false; // needed when OP=GENERATE
			}

		}
		if (debug > 1)
			System.out.println("EWSSynchro.checkArgs: EWSECUTYPE '" + value + "' is okay");
		// Check EWSINTERFACE
		value = getArg("EWSINTERFACE");
		if (value != null) {
			// we have a EWSINTERFACE
			if (op != 'G') {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: EWSINTERFACE '"
							+ value + "' not allowed when OP!=GENERATE");
				return false; // only allowed when OP=GENERATE
			}
			//Only values in EWSInterfaces allowed
			ok = false;
			//Loop over all available EWSInterfaces
			for (i = 0; i < EWSInterfaces.length; i++) {
				//Is the current Argumentvalue equal to the EWSInterface
				if (value.equalsIgnoreCase(EWSInterfaces[i])) {
					ok = true; // EWSInterface has been found in available EWSInterfaces
				}
			}
			if (!ok) { // current Argumentvalue is not contained in available EWSInterfaces
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: EWSINTERFACE '"
							+ value + "' not known");
				return false;
			}
		} else {
			// we have NO EWSINTERFACE
			if (op == 'G') {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: EWSINTERFACE needed when OP==GENERATE");
				return false; // needed when OP=GENERATE
			}

		}
		if (debug > 1)
			System.out.println("EWSSynchro.checkArgs: EWSINTERFACE '" + value + "' is okay");
		// Check ENC_ALGORITHM
		value = getArg("ENC_ALGORITHM");
		if (value != null) {
			// we have a ENC_ALGORITHM
			if (op != 'D') {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: ENC_ALGORITHM '"
							+ value + "' not allowed when OP!=DOKU");
				return false; // only allowed when OP=DOKU
			}
			// only values in EncAlgorithms allowed
			ok = false;
			//Loop over all available EncAlgorithms
			for (i = 0; i < EncAlgorithms.length; i++) {
				//Is the current Argumentvalue equal to the EWSECUType?
				if (value.equalsIgnoreCase(EncAlgorithms[i])) {
					ok = true; // EncAlgorithm has been found in available EncAlgorithms
				}
			}
			if (!ok) { // current Argumentvalue is not contained in available EncAlgorithms
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: EncAlgorithms '"
							+ value + "' not known");
				return false;
			}
		} else {
			//We have NO EncAlgorithms use defaut
		}
		if (debug > 1)
			System.out.println("EWSSynchro.checkArgs: ENC_ALGORITHM '" + value + "' is okay");
		
		//GEN_DEFAULTCODE
		value = getArg("GEN_DEFAULTCODE");
		if (value != null) {
			// we have a GEN_DEFAULTCODE
			if (op != 'G') {
				if (debug > 0)
					System.out
							.println("EWSSynchro.checkArgs: GEN_DEFAULTCODE '"
									+ value + "' not allowed when OP!=GENERATE");
				return false; // only allowed when OP=GENERATE
			}
			// only TRUE or FALSE allowed
			if ((!value.equalsIgnoreCase("TRUE"))
					&& (!value.equalsIgnoreCase("FALSE"))) {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: GEN_DEFAULTCODE '" + value + "' not TRUE/FALSE");
				return false;
			}
		}
		if (debug > 1)
			System.out.println("EWSSynchro.checkArgs: GEN_DEFAULTCODE '" + value + "'");
		
		// DOKU_MODE
		value = getArg("DOKU_MODE");
		if (value != null) {
			// we have a DOKU_MODE
			if (op != 'D') {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: DOKU_MODE '" + value + "' not allowed when OP!=DOKU");
				return false; // only allowed when OP=DOKU
			}
			// check value
			ok = false;
			for (i = 0; i < DokuModes.length; i++) {
				if (value.equalsIgnoreCase(DokuModes[i][0])) {
					ok = true;
				}
			}
			if (!ok) {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: DOKU_MODE '" + value + "' not allowed");
				return false;
			}
		}
		if (debug > 1)
			System.out.println("EWSSynchro.checkArgs: DOKU_MODE '" + value + "'");
		
		// DOKU_PATH
		value = getArg("DOKU_PATH");
		if (value != null) {
			// we have a DOKU_PATH
			if ((op != 'D')) {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: DOKU_PATH '" + value + "' not allowed when OP!=DOKU");
				return false; // only allowed when OP=DOKU
			}
		}
		if (debug > 1)
			System.out.println("EWSSynchro.checkArgs: DOKU_PATH '" + value + "'");
		
		// DOKU_PARTITION
		value = getArg("DOKU_PARTITION");
		if (value != null) {
			// we have a DOKU_PARTITION
			if (op != 'D') {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: DOKU_PARTITION '"
							+ value + "' not allowed when OP!=DOKU");
				return false; // only allowed when OP=DOKU
			}
			try {
				int ivalue = Integer.parseInt(value);
				if (ivalue < 0) {
					if (debug > 0)
						System.out.println("EWSSynchro.checkArgs: DOKU_PARTITION '" + value + "' is out of range");
					return false;
				}
			} catch (NumberFormatException n) {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: DOKU_PARTITION '" + value + "' not numeric");
				return false;
			}
		}
		if (debug > 1)
			System.out.println("EWSSynchro.checkArgs: DOKU_PARTITION '" + value + "'");
		// READ_OP
		value = getArg("READ_OP");
		if (value != null) {
			// we have a READ_OP
			if (op != 'X') {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: READ_OP '" + value + "' not allowed when OP!=READXML");
				return false; // only allowed when OP=READXML
			}
			// check if the value is in the list of read-operations
			ok = false;
			for (i = 0; i < ReadOperations.length; i++) {
				if (value.equalsIgnoreCase(ReadOperations[i][0])) {
					ok = true;
				}
			}
			if (!ok) {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: READ_OP '"
							+ value + "' unknown read operationmode");
				return false; // no known read operation
			}
		}
		if (debug > 1)
			System.out.println("EWSSynchro.checkArgs: READ_OP '" + value + "'");
		
		// READ_SOURCE1
		value = getArg("READ_SOURCE1");
		if (value != null) {
			// we have a READ_SOURCE1
			if (op != 'X') {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: READ_SOURCE1 '"
							+ value + "' not allowed when OP!=READXML");
				return false; // only allowed when OP=READXML
			}
			// check if the value is in the list of sources
			ok = false;
			for (i = 0; i < DatasetSources.length; i++) {
				if (value.equalsIgnoreCase(DatasetSources[i][0])) {
					ok = true;
					s1 = DatasetSources[i][1].charAt(0);
				}
			}
			if (!ok) {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: READ_SOURCE1 '"
							+ value + "' unknown source");
				return false; // only allowed when OP=READXML
			}
		} else {
			// we have NO READ_SOURCE1
			if (op == 'X') {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: READ_SOURCE1 needed when OP==READXML");
				return false; // only allowed when OP=READXML
			}
		}
		if (debug > 1)
			System.out.println("EWSSynchro.checkArgs: READ_SOURCE1 '" + value + "'");
		
		// READ_SOURCE2
		value = getArg("READ_SOURCE2");
		if (value != null) {
			// we have a READ_SOURCE2
			if (op != 'X') {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: READ_SOURCE2 '"
							+ value + "' not allowed when OP!=READXML");
				return false; // only allowed when OP=READXML
			}
			// check if the value is in the list of sources
			ok = false;
			for (i = 0; i < DatasetSources.length; i++) {
				if (value.equalsIgnoreCase(DatasetSources[i][0])) {
					ok = true;
					s2 = DatasetSources[i][1].charAt(0);
				}
			}
			if (!ok) {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: READ_SOURCE2 '"
							+ value + "' unknown source");
				return false; // only allowed when OP=READXML
			}
			if (s1 == s2) {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: READ_SOURCE2 '"
							+ value + "' same source multiply used");
				return false; // only allowed when OP=READXML
			}
		}
		if (debug > 1)
			System.out.println("EWSSynchro.checkArgs: READ_SOURCE2 '" + value + "'");
		
		// READ_SOURCE3
		value = getArg("READ_SOURCE3");
		if (value != null) {
			// we have a READ_SOURCE3
			if (op != 'X') {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: READ_SOURCE3 '"
							+ value + "' not allowed when OP!=READXML");
				return false; // only allowed when OP=READXML
			}
			// check if the value is in the list of sources
			ok = false;
			for (i = 0; i < DatasetSources.length; i++) {
				if (value.equalsIgnoreCase(DatasetSources[i][0])) {
					ok = true;
					s3 = DatasetSources[i][1].charAt(0);
				}
			}
			if (!ok) {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: READ_SOURCE3 '"
							+ value + "' unknown source");
				return false; // only allowed when OP=READXML
			}
			if ((s1 == s3) || (s2 == s3)) {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: READ_SOURCE3 '"
							+ value + "' same source multiply used");
				return false; // only allowed when OP=READXML
			}
			if (s2 == ' ') {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: READ_SOURCE3 '" + value
							+ "' only allowed when READ_SOURCE2 is present");
				return false; // only allowed when OP=READXML
			}
		}
		if (debug > 1)
			System.out.println("EWSSynchro.checkArgs: READ_SOURCE3 '" + value + "'");
		
		// DOMACCESS_OP
		value = getArg("DOMACCESS_OP");
		if (value != null) {
			// we have a DOMACCESS_OP
			if (op != 'M') {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: DOMACCESS_OP '"
							+ value + "' not allowed when OP!=DOMACCESS");
				return false; // only allowed when OP=DOMACCESS_OP
			}
			// check if the value is in the list of domAccess-operations
			ok = false;
			for (i = 0; i < DomAccessOperations.length; i++) {
				if (value.equalsIgnoreCase(DomAccessOperations[i][0])) {
					ok = true;
				}
			}
			if (!ok) {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: DOMACCESS_OP '"
							+ value + "' unknown domAccess operationmode");
				return false; // no known domAccess operation
			}
		} else {
			// we have no DOMACCESS_OP
			if (op == 'M') {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: DOMACCESS_OP needed when OP==DOMACCESS");
				return false; // needed when OP=DOMACCESS_OP
			}
		}
		if (debug > 1)
			System.out.println("EWSSynchro.checkArgs: DOMACCESS_OP '" + value + "'");
		
		// DOMACCESS_TIMEOUT
		value = getArg("DOMACCESS_TIMEOUT");
		if (value != null) {
			// we have a DOMACCESS_TIMEOUT
			try {
				int ivalue = Integer.parseInt(value);
				if (ivalue < 0) {
					if (debug > 0)
						System.out.println("EWSSynchro.checkArgs: DOMACCESS_TIMEOUT '"
								+ value + "' is out of range");
					return false;
				}
			} catch (NumberFormatException n) {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: DOMACCESS_TIMEOUT '"
									+ value + "' not numeric");
				return false;
			}
		}
		if (debug > 1)
			System.out.println("EWSSynchro.checkArgs: DOMACCESS_TIMEOUT '" + value + "'");
		//DOM_URL
		value = getArg("DOM_URL");
		if (value != null) {
			// we have a DOM_URL
			if ((op != 'X') && (op != 'M')) {
				if (debug > 0)
					System.out
							.println("EWSSynchro.checkArgs: DOM_URL '"
									+ value + "' not allowed when OP!=READXML and OP!=DOMACCESS");
				return false; // only allowed when OP=READXML or OP=DOMACCESS
			}
			// length at least 6 characters 'http://x
			if (value.trim().length() < 7) {
				if (debug > 0)
					System.out.println("EWSSynchro.checkArgs: DOM_URL '" + value + "' to short");
				return false;
			}
		}
		if (debug > 1)
			System.out.println("EWSSynchro.checkArgs: DOM_URL '" + value + "'");
		if (debug > 0)
			System.out.println("EWSSynchro.checkArgs: End");
		return true; // no required Arguments, any Arguments allowed
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info
	 *            Information zur Ausf�hrung
	 */
	public void execute(ExecutionInfo info) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		Vector localErgListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// reset all instance-variables/arrays
		resetVariables();
		// init random
		initRandom();

		// Method variables
		String value; // for storing the value of a pair key/value
		// loop variables
		int i;

		try {
			// Parameter check
			// ----------------
			if (!checkArgs()) {
				// Pass a new exception upwards (PPExecutionException -> error in parameters!)
				result = new Ergebnis("ExecFehler", "Prameter", "", "", "", "",
						"", "", "", "0", "", "", "", PB.getString("parameterexistenz"),
						"Parameterexistenz", Ergebnis.FT_NIO_SYS);
				ergListe.add(result);
				throw new PPExecutionException(PB.getString("parameterexistenz"));
			}
			// Parameter auswerten/holen
			try {
				// Debug
				value = getArg("DEBUG");
				if ((value == null) || (value.length() == 0)) {
					debug = 0;
				} else if ("TRUE".equalsIgnoreCase(value)) {
					debug = 2;
				} else if ("FALSE".equalsIgnoreCase(value)) {
					debug = 0;
				} else {
					try {
						debug = Integer.parseInt(value);
						if (debug < 0)
							debug = 0;
					} catch (NumberFormatException nfe) {
						System.out.println("EWSSynchro.execute: DEBUG invalid");
						throw new PPExecutionException(PB.getString("parameterexistenz"));
					}
				}
				if (debug > 0)
					System.out.println("EWSSynchro.execute:  DEBUG enabled! Level:" + debug);
				if (debug > 0)
					System.out.println("EWSSynchro.execute: Start");
				// required Args
				// =====================================================
				// OP, OP must be defined in Operations
				value = getArg("OP");
				for (i = 0; i < Operations.length; i++) {
					if (value.equalsIgnoreCase(Operations[i][0])) {
						Op = Operations[i][1].charAt(0);
					}
				}
				if (debug > 0)
					System.out.println("EWSSynchro.execute: OP '" + Op + "'");

				// optional Args (may also be required depending on OP)
				// =====================================================
				// EWSECUTYPE
				value = getArg("EWSECUTYPE");
				if (value != null) {
					for (i = 0; i < EWSECUTypes.length; i++) {
						if (value.equalsIgnoreCase(EWSECUTypes[i])) {
							Ews_EcuType = EWSECUTypes[i];
						}
					}
				} else {
					Ews_EcuType = null;
				}
				if (debug > 0)
					System.out.println("EWSSynchro.execute: EWSECUTYPE '" + Ews_EcuType + "'");
				// EWSINTERFACE
				value = getArg("EWSINTERFACE");
				if (value != null) {
					for (i = 0; i < EWSInterfaces.length; i++) {
						if (value.equalsIgnoreCase(EWSInterfaces[i])) {
							Ews_Interface = EWSInterfaces[i];
						}
					}
				} else {
					Ews_Interface = null;
				}
				if (debug > 0)
					System.out.println("EWSSynchro.execute: EWSECUTYPE '" + Ews_Interface + "'");
				
				// STORAGE
				if (getArg("STORAGE") != null) {
					Storage = getArg("STORAGE");
					if (debug > 1)
						System.out.println("EWSSynchro.execute: STORAGE '" + Storage + "'");
				} else {
					Storage = null;
				}
				// GEN_DEFAULTCODE
				value = getArg("GEN_DEFAULTCODE");
				if ((value != null) && ("TRUE".equalsIgnoreCase(value))) {
					// we have a GEN_DEFAULTCODE and the value is TRUE
					GenDefaultCode = true;
				} else {
					GenDefaultCode = false;
				}
				if (debug > 1)
					System.out.println("EWSSynchro.execute: GEN_DEFAULTCODE '"
							+ GenDefaultCode + "'");
				/**
				 * Possible DOKU_MODEs.<br>
				 * Currently available modes (default is Normal):<br>
				 * NORMAL: Send created XML-file to the CASCADE-server.<br>
				 * PATH: Store XML-file to a separate path (used at sparepartcenter).<br>
				 * ALL: Use both NORMAL and PATH at the same time.
				 */
				value = getArg("DOKU_MODE");
				if (value != null) {
					// we have a DOKU_MODE
					// determine value
					char dokumode = ' ';
					for (i = 0; i < DokuModes.length; i++) {
						if (value.equalsIgnoreCase(DokuModes[i][0])) {
							dokumode = DokuModes[i][1].charAt(0);
						}
					}
					// set all to false
					DokuToDom = false;
					DokuToPath = false;
					// select options to activate depending on dokumode
					switch (dokumode) {
					case 'N':
					case 'n':
						// Normal
						DokuToDom = true;
						break;
					case 'P':
					case 'p':
						// Path (for sparepart center)
						DokuToPath = true;
						break;
					case 'A':
					case 'a':
						// All (=Local, Dom and Path)
						DokuToDom = true;
						DokuToPath = true;
						break;
					default:
						// default is Local and Dom (=NORMAL)
						DokuToDom = true;
					}
				} else {
					DokuToDom = true;
					DokuToPath = false;
				}
				// DOKU_PATH
				DokuPath = null;
				value = getArg("DOKU_PATH");
				if ((value != null) && (value.length() > 0)) {
					DokuPath = value;
				} else {
					DokuPath = null;
				}
				// DOKU_PARTITION
				value = getArg("DOKU_PARTITION");
				if (value != null) {
					// we have a DOKU_PARTITION
					try {
						DokuPartition = Integer.parseInt(value);
					} catch (NumberFormatException n) {
						throw new PPExecutionException("DOKU_PARTITION invalid");
					}
				} else {
					// we have no DOKU_PARTITION
					DokuPartition = 0;
				}
				if (debug > 1)
					System.out.println("EWSSynchro.execute: DOKU_PARTITION '" + value + "'");
				// READ_OP must be defined in ReadOperations
				value = getArg("READ_OP");
				if (value != null) {
					// check if the value is in the list of read-operations
					for (i = 0; i < ReadOperations.length; i++) {
						if (value.equalsIgnoreCase(ReadOperations[i][0])) {
							ReadOp = ReadOperations[i][1].charAt(0);
						}
					}
				} else {
					ReadOp = ' '; // Default not specified
				}
				if (debug > 1)
					System.out.println("EWSSynchro.execute: READ_OP '" + value + "'");
				// READ_SOURCE1, READ_SOURCE must be defined in DatasetSources
				value = getArg("READ_SOURCE1");
				if (value != null) {
					for (i = 0; i < DatasetSources.length; i++) {
						if (value.equalsIgnoreCase(DatasetSources[i][0])) {
							DatasetSource1 = DatasetSources[i][1].charAt(0);
						}
					}
				} else {
					DatasetSource1 = ' '; // default no source selected
				}
				if (debug > 1)
					System.out.println("EWSSynchro.execute: READ_SOURCE1 '" + value + "'");
				// READ_SOURCE2, READ_SOURCE must be defined in DatasetSources
				value = getArg("READ_SOURCE2");
				if (value != null) {
					for (i = 0; i < DatasetSources.length; i++) {
						if (value.equalsIgnoreCase(DatasetSources[i][0])) {
							DatasetSource2 = DatasetSources[i][1].charAt(0);
						}
					}
				} else {
					DatasetSource2 = ' '; // default no source selected
				}
				if (debug > 1)
					System.out.println("EWSSynchro.execute: READ_SOURCE2 '" + value + "'");
				// READ_SOURCE3, READ_SOURCE must be defined in DatasetSources
				value = getArg("READ_SOURCE3");
				if (value != null) {
					for (i = 0; i < DatasetSources.length; i++) {
						if (value.equalsIgnoreCase(DatasetSources[i][0])) {
							DatasetSource3 = DatasetSources[i][1].charAt(0);
						}
					}
				} else {
					DatasetSource3 = ' '; // default no source selected
				}
				if (debug > 1)
					System.out.println("EWSSynchro.execute: READ_SOURCE3 '"
							+ value + "'");
				// DOMACCESS_TIMEOUT
				value = getArg("DOMACCESS_TIMEOUT");
				if (value != null) {
					// we have a DOMACCESS_TIMEOUT
					try {
						DomAccessTimeout = Integer.parseInt(value);
					} catch (NumberFormatException n) {
						throw new PPExecutionException("DOMACCESS_TIMEOUT invalid");
					}
				} else {
					// we have no DOMACCESS_TIMEOUT
					if (Op != 'M') {
						// Default 0 Minutes.
						DomAccessTimeout = DOMACCESS_TIMEOUT_DEFAULT; 
					} else {
						// Default 2 Minutes.
						DomAccessTimeout = DOMACCESS_TIMEOUT_DEFAULT_FOR_DOMACCESS; 
					}
				}
				if (DomAccessTimeout < 0)
					DomAccessTimeout = 0;
				if (DomAccessTimeout > DOMACCESS_TIMEOUT_MAX)
					DomAccessTimeout = DOMACCESS_TIMEOUT_MAX;
				if (debug > 1)
					System.out.println("EWSSynchro.execute: DOMACCESS_TIMEOUT '" + value + "'");
				// DOMACCESS_OP must be defined in ReadOperations
				value = getArg("DOMACCESS_OP");
				if (value != null) {
					// check if the value is in the list of read-operations
					for (i = 0; i < DomAccessOperations.length; i++) {
						if (value.equalsIgnoreCase(DomAccessOperations[i][0])) {
							DomAccessOp = DomAccessOperations[i][1].charAt(0);
						}
					}
				} else {
					DomAccessOp = ' '; // Default not specified
				}
				if (debug > 1)
					System.out.println("EWSSynchro.execute: DOMACCESS_OP '" + value + "'");
				// ENC_ALGORITHM
				value = getArg("ENC_ALGORITHM");
				if (value != null) {
					for (i = 0; i < EncAlgorithms.length; i++) {
						if (value.equalsIgnoreCase(EncAlgorithms[i])) {
							EncAlgorithm = EncAlgorithms[i];
						}
					}
				} else {
					EncAlgorithm = "STD";	//default use standard-encryption
				}
				if (debug > 0)
					System.out.println("EWSSynchro.execute: ENC_ALGORITHM '" + EncAlgorithm + "'");
				// DOM_URL
				value = getArg("DOM_URL");
				if (value != null) {
					domUrl =  value.trim();
					if (debug > 0)
						System.out.println("EWSSynchro.execute: DOM_URL '" + domUrl + "'");
				} else {
					domUrl =  DEFAULT_DOM_URL;	//DEFAULT URL
				}

			} catch (Exception e) {
				if (debug > 0)
					System.out.println("EWSSynchro.execute: Error during extracting arguments");
				result = new Ergebnis("ExecFehler", "Prameter", "", "", "", "",
						"", "", "", "0", "", "", "", PB.getString("parameterexistenz"),
						"Parameterexistenz", Ergebnis.FT_NIO_SYS);
				ergListe.add(result);
				e.printStackTrace(System.out);
				throw new PPExecutionException(PB.getString("parameterexistenz"));
			}

			// execution try / catch
			try {
				// try / catch for section EXECUTE
				// **************************************
				localErgListe = new Vector(); // for storing errors in this
				// local section
				try {
					// Possible Ops are
					// G->GENERATE, X->READXML
					// C->CLEAR, D->DOKU
					switch (Op) {
					// GENERATE
					case 'G':
					case 'g':
						this.generate(); // no errors may occur
						this.exportDynamicAttributes();
						break;
					// READXML
					// ReadOP: R->READ, E->DSEXISTIO, N->DSEXISTNIO
					case 'X':
					case 'x':
						this.readXML(localErgListe); // also a Checkdataset
						// is done
						if ((localErgListe.size() != 0)
								&& containsError(localErgListe)) {
							if (debug > 0)
								System.out.println("EWSSynchro.execute: readXML: "
										+ localErgListe.size() + " error(s): " + localErgListe);
							throw new PPExecutionException("EWSSynchro.execute: readXML failed");
						}
						this.exportDynamicAttributes();
						break;
					// CLEAR
					case 'C':
					case 'c':
						// Do not handle errors here -> localErgListe
						this.importDynamicAttributes(localErgListe);
						this.clear();
						this.exportDynamicAttributes();
						break;
					// DOKU
					case 'D':
					case 'd':
						// Do not handle errors here -> localErgListe
						this.importDynamicAttributes(localErgListe);
						if ((localErgListe.size() != 0)
								&& containsError(localErgListe)) {
							if (debug > 0)
								System.out.println("EWSSynchro.execute: importDynamicAttributes: "
										+ localErgListe.size() + " error(s): " + localErgListe);
							throw new PPExecutionException("EWSSynchro.execute: importDynamicAttributes failed");
						}
						this.dokuDataset(localErgListe);
						if ((localErgListe.size() != 0)
								&& containsError(localErgListe)) {
							if (debug > 0)
								System.out.println("EWSSynchro.execute: dokuDataset: "
									+ localErgListe.size() + " error(s): " + localErgListe);
							throw new PPExecutionException("EWSSynchro.execute: dokuDataset failed");
						}
						break;
					// DOMACCESS
					case 'M':
					case 'm':
						this.domAccessMain(localErgListe);
						break;
					default:
						throw new PPExecutionException("EWSSynchro.execute: Operation '" + Op + "' not supported (yet)");
					}
					if (debug > 2)
						System.out.println("EWSSynchro.execute: EXECUTE ErgListe: " + localErgListe.toString());
				} catch (PPExecutionException ppe) {
					if (debug > 0)
						System.out.println("EWSSynchro.execute: Error during execution");
					if (localErgListe.size() != 0)
						ergListe.addAll(localErgListe);
					ppe.printStackTrace(System.out);
					if (!containsError(localErgListe)) {
						result = new Ergebnis("ExecFehler", "", "", "", "", "",
								"", "", "", "0", "", "", "",
								PB.getString("unerwarteterLaufzeitfehler"),"EXCEPTION in section EXECUTE: "
								+ ppe.getMessage(), Ergebnis.FT_NIO_SYS);
						ergListe.add(result);
					}
					throw ppe;
				}
				if (localErgListe.size() != 0)
					ergListe.addAll(localErgListe);
				if (containsError(localErgListe)) {
					// create NIO-Ergebnis
					result = new Ergebnis("ExecFehler", "", "", "", "", "", "",
							"", "", "0", "", "", "", PB.getString("unerwarteterLaufzeitfehler"),
							"ERROR in section EXECUTE", Ergebnis.FT_NIO_SYS);
					ergListe.add(result);
					throw new PPExecutionException("EWSSynchro.execute: Operation '" + Op
									+ "': Error during execution");
				} else {
					// create IO-Ergebnis
					result = new Ergebnis("Execution", "", "", "", "", "", "",
							"", "", "0", "", "", "", "", "Section EXECUTE IO", Ergebnis.FT_IO);
					ergListe.add(result);
				}
			} catch (PPExecutionException ppe) {
				if (debug > 0)
					System.out.println("EWSSynchro.execute: Error during execution");
				throw ppe;
			}

		} catch (PPExecutionException ppee) { // main try-catch
			status = STATUS_EXECUTION_ERROR;
			if (ergListe.size() == 0) {
				// no results, so also no NIO results produced so far
				result = new Ergebnis("ExecFehler", "", "", "", "", "", "", "",
						"", "0", "", "", "", PB.getString("unerwarteterLaufzeitfehler"),
						"PPExecutionException: " + ppee.getMessage(),Ergebnis.FT_NIO_SYS);
				ergListe.add(result);
			}
			ppee.printStackTrace();
		} catch (Exception ex) {
			result = new Ergebnis("ExecFehler", "", "", "", "", "", "", "", "","0", "", "", "",
					PB.getString("unerwarteterLaufzeitfehler"), "Throwable: "
					+ ex.getMessage(), Ergebnis.FT_NIO_SYS);
			ergListe.add(result);
			status = STATUS_EXECUTION_ERROR;
			ex.printStackTrace();
		} catch (Throwable t) {
			result = new Ergebnis("ExecFehler", "", "", "", "", "", "", "", "","0", "", "", "",
					PB.getString("unerwarteterLaufzeitfehler"), "Throwable: "
					+ t.getMessage(), Ergebnis.FT_NIO_SYS);
			ergListe.add(result);
			status = STATUS_EXECUTION_ERROR;
			t.printStackTrace();
		}

		// Status setzen
		if (debug > 2)
			System.out.println("EWSSynchro.execute: " + ergListe.size()
					+ " result(s) during execution ");
		if (debug > 2)
			printResults(ergListe);
		setPPStatus(info, status, ergListe);
		if (debug > 0)
			System.out.println("EWSSynchro.execute: End");
	}

	/**
	 * Export all internal variables and put them into the dynamic attributes
	 */
	private void exportDynamicAttributes() throws PPExecutionException {

		// EWS_ECUTYPE
		// storage type: String
		// example values: "EWS3","CAS2","CAS3"
		if (Ews_EcuType != null) {
			setDynamicAttribute(LABEL_EWS_ECUTYPE, Ews_EcuType);
		} else {
			// not present, so set it to UNKNOWN
			setDynamicAttribute(LABEL_EWS_ECUTYPE, DYN_DUMMY);
		}
		// EWS_INTERFACE
		// storage type: String
		// example value: "EWS3", "EWS4", "EWS4EGS"
		if (Ews_Interface != null) {
			setDynamicAttribute(LABEL_EWS_INTERFACE, Ews_Interface);
		} else {
			setDynamicAttribute(LABEL_EWS_INTERFACE, DYN_DUMMY);
		}
		// EWS_CODE
		// storage type: HEX
		// example value: "0123456789ABCDEF0123456789ABCDEF"
		// Random
		if (Ews_Code != null) {
			setDynamicAttribute(LABEL_EWS_CODE, Ews_Code);
		} else {
			setDynamicAttribute(LABEL_EWS_CODE, DYN_DUMMY);
		}
		// EGS_ISN
		// storage type: HEX
		// example value: "01234567"
		// Random
		if (Egs_Code != null) {
			setDynamicAttribute(LABEL_EGS_CODE, Egs_Code);
		} else {
			setDynamicAttribute(LABEL_EGS_CODE, DYN_DUMMY);
		}

	}

	/**
	 * Import all dynamic attributes and put them into internal variables create
	 * a result and add it to errors for every undefined or empty attribute
	 * EXCEPT the following: DOKU_TRANSPONDERLAYOUT, DOKU_LOCATION,
	 * CBS_KEY_VARIANT, CBS_VERSION, NUM_KEYS, NUM_KEYS_INITIAL, FBD_STATUS_FBD
	 */
	private void importDynamicAttributes(Vector errors)
			throws PPExecutionException {
		Ergebnis result;

		// EWS_ECUTYPE
		// storage type: String
		// example values: "EWS3","CAS2","CAS3"
		Ews_EcuType = getDynamicAttribute(LABEL_EWS_ECUTYPE);
		if ((Ews_EcuType == null) || (Ews_EcuType.length() == 0)) {
			result = new Ergebnis("ExecFehler", "importDynamicAttributes", "",
					"", "", LABEL_EWS_ECUTYPE, "null", "", "", "0", "", "", "",
					PB.getString("unerwarteterLaufzeitfehler"),"Dynamic Attribute not defined", Ergebnis.FT_NIO_SYS);
			errors.add(result);
		}
		if (DYN_DUMMY.equalsIgnoreCase(Ews_EcuType)) {
			// Set internal variable to null, if the attribute has been 'UNKNOWN'
			Ews_EcuType = null;
		}
		// EWS_INTERFACE
		// storage type: String
		// example value: "EWS3", "EWS4", "EWS4EGS"
		Ews_Interface = getDynamicAttribute(LABEL_EWS_INTERFACE);
		if ((Ews_Interface == null) || (Ews_Interface.length() == 0)) {
			result = new Ergebnis("ExecFehler", "importDynamicAttributes", "",
					"", "", LABEL_EWS_INTERFACE, "null", "", "", "0", "", "",
					"", PB.getString("unerwarteterLaufzeitfehler"),
					"Dynamic Attribute not defined", Ergebnis.FT_NIO_SYS);
			errors.add(result);
		}
		if (DYN_DUMMY.equalsIgnoreCase(Ews_Interface)) {
			// Set internal variable to null, if the attribute has been 'UNKNOWN'
			Ews_Interface = null;
		}
		// EWS_CODE
		// storage type: HEX
		// example value: "0123456789ABCDEF0123456789ABCDEF"
		// Random
		Ews_Code = getDynamicAttribute(LABEL_EWS_CODE);
		if ((Ews_Code == null) || (Ews_Code.length() == 0)) {
			result = new Ergebnis("ExecFehler", "importDynamicAttributes", "",
					"", "", LABEL_EWS_CODE, "null", "", "", "0", "", "", "", PB
							.getString("unerwarteterLaufzeitfehler"),
					"Dynamic Attribute not defined", Ergebnis.FT_NIO_SYS);
			errors.add(result);
		}
		if (DYN_DUMMY.equalsIgnoreCase(Ews_Code)) {
			// Set internal variable to null, if the attribute has been 'UNKNOWN'
			Ews_Code = null;
		}
		// EGS_ISN
		// storage type: HEX
		// example value: "01234567"
		// Random
		Egs_Code = getDynamicAttribute(LABEL_EGS_CODE);
		if ((Egs_Code == null) || (Egs_Code.length() == 0)) {
			result = new Ergebnis("ExecFehler", "importDynamicAttributes", "",
					"", "", LABEL_EGS_CODE, "null", "", "", "0", "", "", "", PB
							.getString("unerwarteterLaufzeitfehler"),
					"Dynamic Attribute not defined", Ergebnis.FT_NIO_SYS);
			errors.add(result);
		}
		if (DYN_DUMMY.equalsIgnoreCase(Egs_Code)) {
			// Set internal variable to null, if the attribute has been 'UNKNOWN'
			Egs_Code = null;
		}
	}

	/**
	 * Fetch the XML-Dataset-File from the server using OLD TRANSFER (RMI
	 * supports no move). Note the file is moved, not copied.
	 */
	private StringBuffer moveDatasetFromServer() throws PPExecutionException {

		StringBuffer buffer = null;

		// Transfer information from Server or the Pr�fstand using Datahandler device
		try {
			buffer = new StringBuffer(new String(DomPruefstand.getDomData(
					getPr�fling().getAuftrag().getFahrgestellnummer7(), "E")));
		} catch (Exception e) {
			if (debug > 0)
				System.out.println("EWSSynchro.moveDatasetFromServer: File Transfer failed. Exception:");
			if (debug > 0)
				e.printStackTrace(System.out);
			throw new PPExecutionException("EWSSynchro.moveDatasetFromServer: File Transfer failed");
		}
		if (buffer == null)
			throw new PPExecutionException("EWSSynchro.moveDatasetFromServer: File Transfer failed, no data available!");

		// save the buffer locally (the content is not modified) in case
		// something goes wrong during test and no doku is executed anymore
		try {
			DomPruefstand.writeDomData(getPr�fling().getAuftrag()
					.getFahrgestellnummer7(), "E", buffer.toString().getBytes());
			if (debug > 0)
				System.out.println("EWSSynchro.moveDatasetFromServer: XML saved locally");
		} catch (Exception e) {
			if (debug > 0)
				System.out.println("EWSSynchro.moveDatasetFromServer: error saving local XML");
			e.printStackTrace(System.out);
			throw new PPExecutionException();
		}

		if (debug > 0)
			System.out.println("EWSSynchro.moveDatasetFromServer: moved "
					+ buffer.length() + " bytes of data");
		return buffer;
	}

	/**
	 * Fetch the XML-Dataset-File from the server using RMI. If this does not
	 * work try to read it over direkt file-access. The file is only read, NOT
	 * moved.
	 */
	private StringBuffer readDatasetFromServer() throws PPExecutionException {

		StringBuffer buffer = null;
		;
		byte rawData[] = null;

		// Transfer information from Server via RMI
		try {
			rawData = null;
			// call the rmi-method
			rawData = DomPruefstand.readDomData(getPr�fling().getAuftrag().getFahrgestellnummer7(), "E");
			if ((rawData != null) && (rawData.length > 1)) {
				// Convert the byte-array to a String
				String strData = new String(rawData);
				if ((strData != null) && (strData.length() > 1)) {
					// Convert the String to a StringBuffer
					buffer = new StringBuffer(strData.trim());
				}
			}
		} catch (Exception e) {
			if (debug > 0)
				System.out.println("EWSSynchro.readDatasetFromServer: RMI Transfer failed");
			buffer = null;
		}
		// Transfer information from Server via direct file-access when RMI failed
		if (buffer == null) {
			try {
				// Construct Path
				// String serverPath =
				// com.bmw.cascade.extern.roh.dom.PB.getString("cascade.datatransfer.server.dom.temppath");
				String serverPath = com.bmw.cascade.util.dom.DomTransferConstants.SERVER_TEMP_DIRECTORY;
				if (debug > 0)
					System.out.println("EWSSynchro.readDatasetFromServer: serverPath = '"
									+ serverPath + "'");
				File sourcePath = new File(serverPath, getPr�fling()
						.getAuftrag().getFahrgestellnummer7());
				if (debug > 0)
					System.out.println("EWSSynchro.readDatasetFromServer: serverPathFile = '"
									+ sourcePath + "'");
				// Construct Path and Filename
				File myFile = new File(sourcePath, getPr�fling().getAuftrag()
						.getFahrgestellnummer7() + "E.XML");
				if (debug > 0)
					System.out.println("EWSSynchro.readDatasetFromServer: serverFile = '"
									+ myFile + "'");
				// create a FileInputStream for file
				FileInputStream fis = new FileInputStream(myFile);
				rawData = new byte[(int) (myFile.length())];
				if (fis.read(rawData, 0, ((int) myFile.length())) != ((int) myFile.length())) {
					throw new PPExecutionException();
				}
				// close the file
				fis.close();

				if ((rawData != null) && (rawData.length > 1)) {
					// Convert the byte-array to a String
					String strData = new String(rawData);
					if ((strData != null) && (strData.length() > 1)) {
						// Convert the String to a StringBuffer
						buffer = new StringBuffer(strData.trim());
					}
				}
			} catch (Exception e) {
				if (debug > 0)
					System.out.println("EWSSynchro.readDatasetFromServer: File Transfer failed");
				buffer = null;
				throw new PPExecutionException("EWSSynchro.readDatasetFromServer: File Transfer failed");
			}
		}

		if (debug > 0)
			System.out.println("EWSSynchro.readDatasetFromServer: read "
					+ buffer.length() + " bytes of data");
		return buffer;
	}

	/**
	 * Fetch the XML-Dataset-File from local distribution directory
	 */
	private StringBuffer readDatasetFromLocalDatabasePruefstandDom()
			throws PPExecutionException {

		StringBuffer buffer = null;

		if (buffer == null) {
			// try to read local database/pruefstand/dom
			try {
				String localPath = com.bmw.cascade.util.dom.DomTransferConstants.PRUEFSTAND_DIRECTORY;
				if (debug > 0)
					System.out.println("EWSSynchro.readDatasetFromLocalDatabasePruefstandDom: localPath = '"
						+ localPath + "'");
				File localDir = new File(localPath);
				if (localDir.exists()) {
					// database/pruefstand/dom directory exists
					File localFile = new File(localDir, getPr�fling()
							.getAuftrag().getFahrgestellnummer7() + "E" + ".xml");
					if (debug > 0)
						System.out.println("EWSSynchro.readDatasetFromLocalDatabasePruefstandDom: localFile = '"
										+ localFile + "'");
					if (localFile.exists() && localFile.canRead()) {
						// datasetfile exists so try to read it
						FileReader in = new FileReader(localFile); // create a filereader
						int size = (int) localFile.length(); // determine filesize
						char[] cbuff = new char[size]; // create a char-array usinf filesize
						in.read(cbuff); // read filesize bytes
						in.close(); // close filereader
						buffer = new StringBuffer(); // empty buffer
						buffer.append(cbuff); // convert char-array to StringBuffer
					} else {
						// file not exists or is not readable
						if (debug > 0)
							System.out.println("EWSSynchro.readDatasetFromLocalDatabasePruefstandDom: file '"
									+ localFile + "' not exists or is not readable");
					}
				} else {
					if (debug > 0)
						System.out.println("EWSSynchro.readDatasetFromLocalDatabasePruefstandDom: Local Read failed database/pruefstand/dom not exists");
					throw new PPExecutionException("EWSSynchro.readDatasetFromLocalDatabasePruefstandDom: File does not exist or is not readable");
				}
			} catch (Exception x) {
				if (debug > 0)
					System.out.println("EWSSynchro.readDatasetFromLocalDatabasePruefstandDom: Local Read failed");
				if (debug > 0)
					x.printStackTrace(System.out);
				buffer = null;
			}
		} //end of: try to read local database/pruefstand/dom
		// not read dataset yet?
		if (buffer == null) {
			// try to read local database/pruefstand/dom/sent
			try {
				String localSentPath = com.bmw.cascade.util.dom.DomTransferConstants.PRUEFSTAND_SENT_DIRECTORY;
				if (debug > 0)
					System.out.println("EWSSynchro.readDatasetFromLocalDatabasePruefstandDom: localSentPath = '"
									+ localSentPath + "'");
				File localSentDir = new File(localSentPath);
				if (localSentDir.exists()) {
					// database/pruefstand/dom directory exists
					File localSentFile = new File(localSentDir, getPr�fling()
							.getAuftrag().getFahrgestellnummer7() + "E" + ".xml");
					if (debug > 0)
						System.out.println("EWSSynchro.readDatasetFromLocalDatabasePruefstandDom: localSentFile = '"
										+ localSentFile + "'");
					if (localSentFile.exists() && localSentFile.canRead()) {
						// datasetfile exists so try to read it
						FileReader in = new FileReader(localSentFile); // create a filereader
						int size = (int) localSentFile.length(); // determine filesize
						char[] cbuff = new char[size]; // create a char-array usinf filesize
						in.read(cbuff); // read filesize bytes
						in.close(); // close filereader
						buffer = new StringBuffer(); // empty buffer
						buffer.append(cbuff); // convert char-array to StringBuffer
					} else {
						// file not exists or is not readable
						if (debug > 0)
							System.out.println("EWSSynchro.readDatasetFromLocalDatabasePruefstandDom: file '"
								+ localSentFile + "' not exists or is not readable");
					}
				} else {
					if (debug > 0)
						System.out.println("EWSSynchro.readDatasetFromLocalDatabasePruefstandDom: Local Read failed in database/pruefstand/dom/sent");
					throw new PPExecutionException("EWSSynchro.readDatasetFromLocalDatabasePruefstandDom: File does not exist or is not readable");
				}
			} catch (Exception x) {
				if (debug > 0)
					System.out.println("EWSSynchro.readDatasetFromLocalDatabasePruefstandDom: Local Read failed");
				if (debug > 0)
					x.printStackTrace(System.out);
				buffer = null;
				throw new PPExecutionException("EWSSynchro.readDatasetFromLocalDatabasePruefstandDom: File Transfer failed");
			}
		}
		if (debug > 0)
			System.out.println("EWSSynchro.readDatasetFromLocalDatabasePruefstandDom: read "
							+ buffer.length() + " bytes of data");
		return buffer;
	}

	private class DOMAccessException extends PPExecutionException {
		static final long serialVersionUID = 1L;

		int s;
		boolean authError = false;

		DOMAccessException(String msg, int status) {
			super(msg);
			s = status;
		}

		DOMAccessException(String msg, int status, boolean authenticationError) {
			super(msg);
			s = status;
			authError = authenticationError;
		}
		
		DOMAccessException(int status) {
			super("DOMAccess-Error: Status returned: "
					+ org.apache.commons.httpclient.HttpStatus
							.getStatusText(status));
			s = status;
		}

		String getStatusText() {
			return org.apache.commons.httpclient.HttpStatus.getStatusText(s);
		}
		
		boolean isAuthenticationError() {
			return authError;
		}
	}

	private class DOMAccess {

		DOMAccess() {
		}

		/**
		 * @throws DOMAccessException
		 *             when status is UNAUTHORIZED
		 * @throws IOException
		 * @throws HttpException
		 *             return null if HTTP status not OK
		 */
		public String getContent(String getUrl, String userId, String password)
				throws org.apache.commons.httpclient.HttpException,
				IOException, DOMAccessException {
			String res = null;
			int status;
			org.apache.commons.httpclient.HttpClient client = new org.apache.commons.httpclient.HttpClient();
			if (userId != null && password != null) {
				client.getState().setAuthenticationPreemptive(true);
				org.apache.commons.httpclient.Credentials credentials = new org.apache.commons.httpclient.UsernamePasswordCredentials(userId, password);
				client.getState().setCredentials(null, null, credentials);
			}
			org.apache.commons.httpclient.methods.GetMethod getMethod = new org.apache.commons.httpclient.methods.GetMethod(getUrl);
			try {
				status = client.executeMethod(getMethod);
				if (status == org.apache.commons.httpclient.HttpStatus.SC_OK) {
					res = getMethod.getResponseBodyAsString();
				} else {
					res = null;
				}
			} finally {
				getMethod.releaseConnection();
			}
			System.out.println("DOMAccess.getContent: HTTP-Status is "
					+ org.apache.commons.httpclient.HttpStatus.getStatusText(status));
			if (status == org.apache.commons.httpclient.HttpStatus.SC_UNAUTHORIZED)
				throw new DOMAccessException("DOMAccess: Unauthorized!", status, true);
			return res;
		}

	}

	private void importDomUserPwd() {
		Object pr_var;
		try {
			pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.TEMPORARY, DOM_USER_PS_VAR);
			domUser = (String) pr_var;
		} catch (VariablesException e1) {
			domUser = null;
			domPwd = null;
			return;
		}
		if (debug > 1)
			System.out.println("EWSSynchro.importDomUserPwd: " + DOM_USER_PS_VAR + "=" + pr_var);
		try {
			pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable(
					VariableModes.TEMPORARY, DOM_PWD_PS_VAR);
			domPwd = (String) pr_var;
		} catch (VariablesException e1) {
			domUser = null;
			domPwd = null;
			return;
		}
		if (debug > 1)
			System.out.println("EWSSynchro.importDomUserPwd: " + DOM_PWD_PS_VAR
					+ "=" + ((domPwd == null) ? domPwd : "***"));
		try {
			pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.TEMPORARY, DOM_TIME_PS_VAR);
			if (debug > 1)
				System.out.println("EWSSynchro.importDomUserPwd: " + DOM_TIME_PS_VAR + "=" + pr_var);
			long current = System.currentTimeMillis();
			if (debug > 1)
				System.out.println("EWSSynchro.importDomUserPwd: " + "currentTime=" + current);
			long last = Long.parseLong((String) pr_var);
			if (debug > 1)
				System.out.println("EWSSynchro.importDomUserPwd: " + DOM_TIME_PS_VAR + "=" + (new Date(last)).toString());
			if (debug > 1)
				System.out.println("EWSSynchro.importDomUserPwd: " + "currentTime=" + (new Date(current)).toString());
			if ((current - last) > (DomAccessTimeout * 60 * 1000)) {
				if (debug > 1)
					System.out.println("EWSSynchro.importDomUserPwd: " + "DOM_ACCESSDATA_TIMEOUT!");
				domUser = null;
				domPwd = null;
				return;
			}
		} catch (VariablesException e1) {
			domUser = null;
			domPwd = null;
		}
		if (debug > 1)
			System.out.println("EWSSynchro.importDomUserPwd: " + DOM_TIME_PS_VAR + "=" + pr_var);

	}

	private void exportDomUserPwd() {
		try {
			getPr�flingLaufzeitUmgebung().setPruefstandVariable(VariableModes.TEMPORARY, DOM_USER_PS_VAR, domUser);
			getPr�flingLaufzeitUmgebung().setPruefstandVariable(VariableModes.TEMPORARY, DOM_PWD_PS_VAR, domPwd);
			long current = System.currentTimeMillis();
			String currentString = "" + current;
			getPr�flingLaufzeitUmgebung().setPruefstandVariable(VariableModes.TEMPORARY, DOM_TIME_PS_VAR, currentString);
			if (debug > 1)
				System.out.println("EWSSynchro.exportDomUserPwd: " + "currentTime=" + current);
			if (debug > 1)
				System.out.println("EWSSynchro.exportDomUserPwd: " + "currentTime=" + (new Date(current)).toString());
		} catch (Exception e) {
			if (debug > 1)
				System.out.println("EWSSynchro.exportDomUserPwd: exception during export: "
								+ e.getMessage());
			return;
		}
		if (debug > 1)
			System.out.println("EWSSynchro.exportDomUserPwd: done");

	}

	/**
	 * Handles the Sub-operation of operation DOMACCESS.
	 */
	private void domAccessMain(Vector ergs) throws PPExecutionException {
		// DomAccessOperations[][] =
		// {{"LOG_ON","N"},{"LOG_OFF","F"},{"LOG_ON_WITH_TEST","W"},{"TEST","T"}};
		Ergebnis result = null;
		switch (DomAccessOp) {
		case 'N':
		case 'n':
			// LOG_ON
			domAccessLogOn(ergs);
			break;
		case 'F':
		case 'f':
			// LOG_OFF
			domAccessLogOff(ergs);
			break;
		case 'W':
		case 'w':
			// LOG_ON_WITH_TEST
			domAccessLogOnWithTest(ergs);
			break;
		case 'T':
		case 't':
			// TEST
			importDomUserPwd();
			domAccessTest(ergs);
			break;
		default:
			result = new Ergebnis("DOMACCESS", "", "", "", "", "","UNKNOWN OP", "DOMACCESS_OP required", "", "0", "", "", "",
					"DomAccessOperation '" + DomAccessOp + "' not supported!","DomAccessOperation '" + DomAccessOp + "' not supported!",
					Ergebnis.FT_NIO_SYS);
			if (ergs != null)
				ergs.add(result);
			if (debug > 0)
				System.out.println("EWSSynchro.domAccessMain: DomAccessOperation '"
								+ DomAccessOp + "' not supported!");
			throw new PPExecutionException("EWSSynchro.domAccessMain: DomAccessOperation '"
							+ DomAccessOp + "' not supported!");
		}
	}

	/**
	 * Query UserId and Password without checking connectivity and accessRights.
	 * Export DomUser and DomPassword
	 */
	private void domAccessLogOn(Vector ergs) throws PPExecutionException {
		domUser = null;
		domPwd = null;
		Ergebnis result = null;
		inputDomUserPwd(ergs);
		if ((domUser == null) || (domPwd == null)) {
			domUser = null;
			domPwd = null;
			exportDomUserPwd();
			result = new Ergebnis("DOMACCESS","","","","","","","","","0","","","",
					"DomAccessOperation 'LOG_ON' failed!","DomAccessOperation 'LOG_ON' UserID and Password required!", Ergebnis.FT_NIO_SYS);
			if (ergs != null)
				ergs.add(result);
			if (debug > 0)
				System.out.println("EWSSynchro.domAccessLogOn: log on failed");
			throw new PPExecutionException("EWSSynchro.domAccessLogOn: log on failed");
		}
		exportDomUserPwd();
	}

	/**
	 * Reset UserId and Password. Export DomUser and DomPassword
	 */
	private void domAccessLogOff(Vector ergs) {
		Ergebnis result = null;
		domUser = null;
		domPwd = null;
		exportDomUserPwd();
		result = new Ergebnis("DOMACCESS", "", "", "", "", "", "", "", "", "0","", "", "", "DomAccessOperation 'LOG_OFF' okay",
				"DomAccessOperation 'LOG_OFF': USERID and PASSWORD reseted",Ergebnis.FT_IO);
		if (ergs != null)
			ergs.add(result);
	}

	/**
	 * Query UserId and Password Check conectivity and accessRights Export
	 * DomUser and DomPassword
	 */
	private void domAccessLogOnWithTest(Vector ergs)
			throws PPExecutionException {
		Ergebnis result = null;
		domUser = null;
		domPwd = null;
		inputDomUserPwd(ergs);
		if ((domUser == null) || (domPwd == null)) {
			domUser = null;
			domPwd = null;
			exportDomUserPwd();
			result = new Ergebnis("DOMACCESS","","","","","","","","","0","","","",
					"DomAccessOperation 'LOG_ON_WITH_TEST' failed!","DomAccessOperation 'LOG_ON_WITH_TEST' UserID and Password required!", Ergebnis.FT_NIO_SYS);
			if (ergs != null)
				ergs.add(result);
			if (debug > 0)
				System.out.println("EWSSynchro.domAccessLogOnWithTest: log on failed");
			throw new PPExecutionException("EWSSynchro.domAccessLogOnWithTest: log on failed");
		}
		try {
			domAccessTest(ergs);
		} catch (PPExecutionException ppe) {
			domUser = null;
			domPwd = null;
			exportDomUserPwd();
			throw ppe;
		}
		exportDomUserPwd();
	}

	/**
	 * Check connectivity with present User/Password Export DomUser and
	 * DomPassword
	 */
	private void domAccessTest(Vector ergs) throws PPExecutionException {
		Ergebnis result = null;
		// String line;
		String all;
		// BufferedReader in = null;
		String url = null;

		if ((domUser == null) || (domPwd == null)) {
			result = new Ergebnis("DOMACCESS", "", "", "", "", "", "", "", "", "0", "", "", "", "DomAccessOperation 'TEST' failed!",
					"DomAccessOperation 'TEST' UserID and Password required!", Ergebnis.FT_NIO_SYS);
			if (ergs != null)
				ergs.add(result);
			if (debug > 0)
				System.out.println("EWSSynchro.domAccessTest: no login informations available");
			throw new PPExecutionException("EWSSynchro.domAccessTest: no login informations available");
		}

		try {
			url = domUrl + "9046000"; // construct URL with dummmy VIN-Number
			all = (new DOMAccess()).getContent(url, domUser, domPwd);
			// did we receive anything
			if (all == null) {
				result = new Ergebnis("DOMACCESS", "", "", "", "", "", "", "", "", "0", "", "", "", "getContent returned null",
						"No answer received from DOM", Ergebnis.FT_NIO_SYS);
				if (ergs != null)
					ergs.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.domAccessTest: Data is null");
				throw new PPExecutionException("EWSSynchro.domAccessTest: Data is null");
			}
			// error-handling
			if (all.indexOf("<error>") >= 0) {
				String msg = "ERROR received";
				int m, n;
				m = 8 + all.indexOf("<!CDATA[");
				if (m >= 8) {
					n = all.indexOf("]]>", m);
					if (n >= 0) {
						// we found an error-message
						msg = all.substring(m, n).trim();
					}
				}
				result = new Ergebnis("DOMACCESS", "", "", "", "", "", "", "", "", "0", "", "", "", "getContent returned the error: "
								+ msg, "DOM returned the following error: " + msg, Ergebnis.FT_NIO_SYS);
				if (ergs != null)
					ergs.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.domAccessTest: " + msg);
				throw new PPExecutionException("EWSSynchro.domAccessTest: " + msg);
			}
		} catch (IOException i) {
			result = new Ergebnis("DOMACCESS", "", "", "", "", "", "", "", "", "0", "", "", "", "getContent produced an IOException: "
					+ i.getMessage(), "IOException during ReadDOM", Ergebnis.FT_NIO_SYS);
			if (ergs != null)
				ergs.add(result);
			if (debug > 0)
				System.out.println("EWSSynchro.domAccessTest: IOException unable to read dataset " + i.getMessage());
			throw new PPExecutionException("EWSSynchro.domAccessTest: IOException unable to read dataset " + i.getMessage());
		} catch (DOMAccessException dae) {
			if (debug > 1)
				dae.printStackTrace(System.out);
			result = new Ergebnis("DOMACCESS", "", "", "", "", "", "", "", "", "0", "", "", "", "DOMAccess failed: " + dae.getMessage(),
					"DOMAccess failed during ReadDOM", Ergebnis.FT_NIO_SYS);
			if (ergs != null)
				ergs.add(result);
			if (debug > 0)
				System.out.println("EWSSynchro.domAccessTest: DOMAccessException unable to read dataset " + dae.getMessage());
 			if (dae.isAuthenticationError()) {
 				//If an authetication error occurred, clear user and password
     			domUser = null;
     			domPwd = null;
     			exportDomUserPwd();
 			}
			throw dae;
		} catch (PPExecutionException ppe) {
			if (debug > 1)
				ppe.printStackTrace(System.out);
			result = new Ergebnis("DOMACCESS", "", "", "", "", "", "", "", "", "0", "", "", "",
					"getContent produced an PPExecutionException: " + ppe.getMessage(),
					"PPExecutionException during ReadDOM", Ergebnis.FT_NIO_SYS);
			if (ergs != null)
				ergs.add(result);
			if (debug > 0)
				System.out.println("EWSSynchro.domAccessTest: PPExecutionException unable to read dataset " + ppe.getMessage());
			throw ppe;
		} catch (Exception e) {
			e.printStackTrace(System.out);
			result = new Ergebnis("DOMACCESS", "", "", "", "", "", "", "", "", "0", "", "", "", "getContent produced an Exception: "
					+ e.getMessage(), "Exception during ReadDOM", Ergebnis.FT_NIO_SYS);
			if (ergs != null)
				ergs.add(result);
			if (debug > 0)
				System.out.println("EWSSynchro.domAccessTest: Exception unable to read dataset " + e.getMessage());
			throw new PPExecutionException("EWSSynchro.domAccessTest: Exception unable to read dataset " + e.getMessage());
		}
		if (all.length() == 0) {
			if (debug > 0)
				System.out.println("EWSSynchro.domAccessTest: Empty file ");
			throw new PPExecutionException("EWSSynchro.domAccessTest: Empty file ");
		}
	}

	/**
	 * Query the user for a UserID and a password. NOTE: No checks to the UserID
	 * and password are done! If UserID or Password is missing both will be set
	 * to null and an Exception is thrown!
	 */
	private void inputDomUserPwd(Vector ergs) throws PPExecutionException {
		Ergebnis result = null;
		int i;
		String DlgTitle, DlgText;
		Method met[];
		boolean UsePWDialog;

		try {
			UserDialog myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();

			UsePWDialog = false;
			met = myDialog.getClass().getDeclaredMethods();
			for (i = 0; i < met.length; i++) {
				if (met[i].getName().equals("requestUserInputPassword")) {
					UsePWDialog = true;
					break;
				}
			}

			if (isDE())
				myDialog.requestUserInput("DOM Login", "Bitte User-Kennung eingeben:", 0);
			else
				myDialog.requestUserInput("DOM Login", "Enter user-id:", 0);
			domUser = myDialog.getUserInput();
			if ((domUser == null) || (domUser.trim().length() == 0)) {
				System.out.println("EWSSynchro.inputDomUserPwd: User-id no input by user");
				domUser = null;
				domPwd = null;
				result = new Ergebnis("DOMACCESS","","","","","","","","","0","","","",
						"DomAccessOperation 'INPUT_USERID' failed!", "DomAccessOperation 'INPUT_USERID': USERID required for accessing DOM", Ergebnis.FT_NIO_SYS);
				if (ergs != null)
					ergs.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.inputDomUserPwd: DomAccessOperation 'INPUT_USERID' failed!");
				throw new PPExecutionException("EWSSynchro.inputDomUserPwd: DomAccessOperation 'INPUT_USERID' failed!");
			}

			if (isDE()) {
				DlgTitle = "DOM Login";
				DlgText = "Bitte Passwort eingeben:";
			} else {
				DlgTitle = "DOM Login";
				DlgText = "Enter password:";
			}

			if (UsePWDialog) {
				try {
					myDialog.requestUserInputPassword(DlgTitle, DlgText, 0);
				} catch (Exception e) {
					UsePWDialog = false;
				}
			}
			if (!UsePWDialog) {
				myDialog.requestUserInput(DlgTitle, DlgText, 0);
			}

			domPwd = myDialog.getUserInput();
			if ((domPwd == null) || (domPwd.trim().length() == 0)) {
				System.out.println("EWSSynchro.inputDomUserPwd: Password no input by user");
				domUser = null;
				domPwd = null;
				result = new Ergebnis("DOMACCESS","","","","","","","","","0","","","",
						"DomAccessOperation 'INPUT_PASSWORD' failed!", "DomAccessOperation 'INPUT_PASSWORD': PASSWORD required for accessing DOM",
						Ergebnis.FT_NIO_SYS);
				if (ergs != null)
					ergs.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.inputDomUserPwd: DomAccessOperation 'INPUT_PASSWORD' failed!");
				throw new PPExecutionException("EWSSynchro.inputDomUserPwd: DomAccessOperation 'INPUT_PASSWORD' failed!");
			}

		} catch (PPExecutionException ppe) {
			// Always release UserDialog
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch (Exception x) {
				System.out.println("EWSSynchro.inputDomUserPwd: exception during releaseUserDialog(): " + x.getMessage());
			}
			throw ppe;
		} catch (Exception e) {
			result = new Ergebnis("DOMACCESS","","","","","","","","","0","","","",
					"DomAccessOperation 'INPUT_AUTHORIZATION_INFORMATION' failed!", "DomAccessOperation 'INPUT_AUTHORIZATION_INFORMATION': USERID and PASSWORD required for accessing DOM", Ergebnis.FT_NIO_SYS);
			if (ergs != null)
				ergs.add(result);
			System.out.println("EWSSynchro.inputDomUserPwd: exception during user input: " + e.getMessage());
			domUser = null;
			domPwd = null;
			// Always release UserDialog
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch (Exception x) {
				System.out.println("EWSSynchro.inputDomUserPwd: exception during releaseUserDialog(): " + x.getMessage());
			}
			throw new PPExecutionException("EWSSynchro.inputDomUserPwd: exception during user input: " + e.getMessage());
		}
		// Always release UserDialog
		try {
			getPr�flingLaufzeitUmgebung().releaseUserDialog();
		} catch (Exception x) {
			System.out.println("EWSSynchro.inputDomUserPwd: exception during releaseUserDialog(): " + x.getMessage());
		}
		result = new Ergebnis("DOMACCESS","","","","","","","","","0","","","",
				"DomAccessOperation 'INPUT_AUTHORIZATION_INFORMATION' okay", "DomAccessOperation 'INPUT_AUTHORIZATION_INFORMATION': USERID and PASSWORD received", Ergebnis.FT_IO);
		if (ergs != null)
			ergs.add(result);
		if (debug > 0)
			System.out.println("EWSSynchro.inputDomUserPwd: DomAccessOperation 'INPUT_AUTHORIZATION_INFORMATION' okay");
	}

	/**
	 * Fetch the XML-Dataset-File from DOM
	 */
	private StringBuffer readDatasetFromDom(Vector ergs)
			throws PPExecutionException {
		Ergebnis result = null;
		StringBuffer buffer = new StringBuffer();
		// String line;
		String all;
		// BufferedReader in = null;
		String url = null;
		// URLConnection connection = null;

		// check domUser and domPwd
		importDomUserPwd();
		if ((domPwd == null) || (domPwd.trim().length() == 0)
				|| (domUser == null) || (domUser.trim().length() == 0)) {
			// no pwd or user
			if (debug > 1)
				System.out.println("EWSSynchro.readDatasetFromDom: NO User or NO Pwd");

			// prompt for user-Input
			inputDomUserPwd(ergs);
		}
		if ((domPwd == null) || (domPwd.trim().length() == 0)
				|| (domUser == null) || (domUser.trim().length() == 0)) {
			// still no pwd or user
			result = new Ergebnis("DOMACCESS","","","","","","","","","0","","","",
					"DomAccessOperation 'LOG_ON' failed!", "DomAccessOperation 'LOG_ON' UserID and Password required!", Ergebnis.FT_NIO_SYS);
			if (ergs != null)
				ergs.add(result);
			if (debug > 1)
				System.out.println("EWSSynchro.readDatasetFromDom: NO User or NO Pwd after user-input");
			throw new PPExecutionException("EWSSynchro.readDatasetFromDom: NO User or NO Pwd after user-input");
		}

		try {
			url = domUrl + getPr�fling().getAuftrag().getFahrgestellnummer(); // construct
			// URL
			all = (new DOMAccess()).getContent(url, domUser, domPwd);
			// did we receive anything
			if (all == null) {
				result = new Ergebnis("DOMACCESS", "", "", "", "", "", "", "","", "0", "", "", "", "getContent returned null",
						"No answer received from DOM", Ergebnis.FT_NIO_SYS);
				if (ergs != null)
					ergs.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.readDatasetFromDom: Data is null");
				throw new PPExecutionException("EWSSynchro.readDatasetFromDom: Data is null");
			}
			// error-handling
			if (all.indexOf("<error>") >= 0) {
				String msg = "ERROR received";
				int m, n;
				m = 8 + all.indexOf("<!CDATA[");
				if (m >= 8) {
					n = all.indexOf("]]>", m);
					if (n >= 0) {
						// we found an error-message
						msg = all.substring(m, n).trim();
					}
				}
				result = new Ergebnis("DOMACCESS", "", "", "", "", "", "", "", "", "0", "", "", "", "getContent returned the error: "
								+ msg, "DOM returned the following error: " + msg, Ergebnis.FT_NIO_SYS);
				if (ergs != null)
					ergs.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.readDatasetFromDom: " + msg);
				throw new PPExecutionException("EWSSynchro.readDatasetFromDom: " + msg);
			}
			buffer.append(all);
		} catch (IOException i) {
			result = new Ergebnis("DOMACCESS", "", "", "", "", "", "", "", "", "0", "", "", "", "getContent produced an IOException: "
							+ i.getMessage(), "IOException during ReadDOM", Ergebnis.FT_NIO_SYS);
			if (ergs != null)
				ergs.add(result);
			if (debug > 0)
				System.out.println("EWSSynchro.readDatasetFromDom: IOException unable to read dataset " + i.getMessage());
			throw new PPExecutionException("EWSSynchro.readDatasetFromDom: IOException unable to read dataset " + i.getMessage());
		} catch (DOMAccessException dae) {
			if (debug > 1)
				dae.printStackTrace(System.out);
			result = new Ergebnis("DOMACCESS", "", "", "", "", "", "", "", "", "0", "", "", "", "DOMAccess failed: " + dae.getMessage(),
					"DOMAccess failed during ReadDOM", Ergebnis.FT_NIO_SYS);
			if (ergs != null)
				ergs.add(result);
			if (debug > 0)
				System.out.println("EWSSynchro.readDatasetFromDom: DOMAccessException unable to read dataset " + dae.getMessage());
			throw dae;
		} catch (PPExecutionException ppe) {
			if (debug > 1)
				ppe.printStackTrace(System.out);
			result = new Ergebnis("DOMACCESS", "", "", "", "", "", "", "", "", "0", "", "", "",
					"getContent produced an PPExecutionException: " + ppe.getMessage(),
					"PPExecutionException during ReadDOM", Ergebnis.FT_NIO_SYS);
			if (ergs != null)
				ergs.add(result);
			if (debug > 0)
				System.out.println("EWSSynchro.readDatasetFromDom: PPExecutionException unable to read dataset " + ppe.getMessage());
			throw ppe;
		} catch (Exception e) {
			e.printStackTrace(System.out);
			result = new Ergebnis("DOMACCESS", "", "", "", "", "", "", "", "", "0", "", "", "", "getContent produced an Exception: "
					+ e.getMessage(), "Exception during ReadDOM", Ergebnis.FT_NIO_SYS);
			if (ergs != null)
				ergs.add(result);
			if (debug > 0)
				System.out.println("EWSSynchro.readDatasetFromDom: Exception unable to read dataset " + e.getMessage());
			throw new PPExecutionException("EWSSynchro.readDatasetFromDom: Exception unable to read dataset " + e.getMessage());
		}
		if (buffer.length() == 0) {
			result = new Ergebnis("DOMACCESS", "", "", "", "", "", "", "", "", "0", "", "", "", "getContent returned an empty answer",
					"Empty answer received from DOM", Ergebnis.FT_NIO_SYS);
			if (ergs != null)
				ergs.add(result);
			if (debug > 0)
				System.out.println("EWSSynchro.readDatasetFromDom: Empty file ");
			throw new PPExecutionException("EWSSynchro.readDatasetFromDom: Empty file ");
		}

		if (debug > 0)
			System.out.println("EWSSynchro.readDatasetFromDom: read " + buffer.length() + " bytes of data");
		// we received some data from DOM, so remind the password and user-id
		exportDomUserPwd();
		return buffer;
	}

	/**
	 * Read an XML-Dataset-File and extract all Informations Uses the
	 * instance-variable ReadOP: R->READ, E->DSEXISTIO, N->DSEXISTNIO
	 */
	private void readXML(Vector ergs) throws PPExecutionException {

		// method variables
		StringBuffer datasetBuffer = null; // for keeping the unparsed dataset
		Ergebnis result; // for creating results
		String checkedSources = ""; // list of visited sources
		boolean readFromDOM = false;

		// first fetch the dataset
		// ***********************
		// first source
		try {
			if (debug > 0)
				System.out.println("EWSSynchro.readXML: Start reading dataset from Source1 (Type=" + DatasetSource1 + ")");
			switch (DatasetSource1) {
			case 'M':
			case 'm':
				// Source server, with move
				checkedSources = checkedSources + "ServerMove";
				datasetBuffer = moveDatasetFromServer();
				result = new Ergebnis("Reading DOM-File", "", "", "", "", "", "", "", "", "0", "", "", "", "",
						"Read dataset from Source1 (ServerMove)", Ergebnis.FT_IO);
				ergs.add(result);
				break;
			case 'S':
			case 's':
				// Source server
				checkedSources = checkedSources + "Server";
				datasetBuffer = readDatasetFromServer();
				result = new Ergebnis("Reading DOM-File", "", "", "", "", "", "", "", "", "0", "", "", "", "",
						"Read dataset from Source1 (Server)", Ergebnis.FT_IO);
				ergs.add(result);
				break;
			case 'L':
			case 'l':
				// Source local database/pruefstand/dom
				checkedSources = checkedSources + "database_pruefstand_dom";
				datasetBuffer = readDatasetFromLocalDatabasePruefstandDom();
				result = new Ergebnis("Reading DOM-File", "", "", "", "", "", "", "", "", "0", "", "", "", "",
						"Read dataset from Source1 (Local)", Ergebnis.FT_IO);
				ergs.add(result);
				break;
			case 'D':
			case 'd':
				// Source DOM
				checkedSources = checkedSources + "DOM";
				datasetBuffer = readDatasetFromDom(ergs);
				result = new Ergebnis("Reading DOM-File", "", "", "", "", "", "", "", "", "0", "", "", "", "",
						"Read dataset from Source1 (Dom)", Ergebnis.FT_IO);
				ergs.add(result);
				if ((datasetBuffer != null) && (datasetBuffer.length() > 0)) {
					readFromDOM = true; // remember that we got the data from DOM
				}
				break;
			default:
				datasetBuffer = null;
			}
			if (debug > 0)
				System.out.println("EWSSynchro.readXML: Finished reading dataset from Source1 (Type=" + DatasetSource1 + ")");
		} catch (Exception e1) {
			// first source did not get the dataset
			datasetBuffer = null;
		}
		// check if to try second source
		if (((datasetBuffer == null) || (datasetBuffer.length() == 0))
				&& (DatasetSource2 != ' ')) {
			// second source
			try {
				if (debug > 0)
					System.out.println("EWSSynchro.readXML: Start reading dataset from Source2 (Type=" + DatasetSource2 + ")");
				switch (DatasetSource2) {
				case 'M':
				case 'm':
					// Source server, with move
					checkedSources = checkedSources + "ServerMove";
					datasetBuffer = moveDatasetFromServer();
					result = new Ergebnis("Reading DOM-File", "", "", "", "", "", "", "", "", "0", "", "", "", "",
							"Read dataset from Source2 (ServerMove)", Ergebnis.FT_IO);
					ergs.add(result);
					break;
				case 'S':
				case 's':
					// Source server
					checkedSources = checkedSources + ",Server";
					datasetBuffer = readDatasetFromServer();
					result = new Ergebnis("Reading DOM-File", "", "", "", "", "", "", "", "", "0", "", "", "", "",
							"Read dataset from Source2 (Server)", Ergebnis.FT_IO);
					ergs.add(result);
					break;
				case 'L':
				case 'l':
					// Source local database/pruefstand/dom
					checkedSources = checkedSources + ",database_pruefstand_dom";
					datasetBuffer = readDatasetFromLocalDatabasePruefstandDom();
					result = new Ergebnis("Reading DOM-File", "", "", "", "", "", "", "", "", "0", "", "", "", "",
							"Read dataset from Source2 (Local)", Ergebnis.FT_IO);
					ergs.add(result);
					break;
				case 'D':
				case 'd':
					// Source DOM
					checkedSources = checkedSources + ",DOM";
					datasetBuffer = readDatasetFromDom(ergs);
					result = new Ergebnis("Reading DOM-File", "", "", "", "", "", "", "", "", "0", "", "", "", "",
							"Read dataset from Source2 (Dom)", Ergebnis.FT_IO);
					ergs.add(result);
					if ((datasetBuffer != null) && (datasetBuffer.length() > 0)) {
						readFromDOM = true; // remember that we got the data from DOM
					}
					break;
				default:
					datasetBuffer = null;
				}
				if (debug > 0)
					System.out.println("EWSSynchro.readXML: Finished reading dataset from Source2 (Type=" + DatasetSource2 + ")");
			} catch (Exception e1) {
				// second source did not get the dataset
				datasetBuffer = null;
			}
		}
		// check if to try third source
		if (((datasetBuffer == null) || (datasetBuffer.length() == 0))
				&& (DatasetSource3 != ' ')) {
			// third source
			try {
				if (debug > 0)
					System.out.println("EWSSynchro.readXML: Start reading dataset from Source3 (Type=" + DatasetSource3 + ")");
				switch (DatasetSource3) {
				case 'M':
				case 'm':
					// Source server, with move
					checkedSources = checkedSources + "ServerMove";
					datasetBuffer = moveDatasetFromServer();
					result = new Ergebnis("Reading DOM-File", "", "", "", "", "", "", "", "", "0", "", "", "", "",
							"Read dataset from Source3 (ServerMove)", Ergebnis.FT_IO);
					ergs.add(result);
					break;
				case 'S':
				case 's':
					// Source server
					checkedSources = checkedSources + ",Server";
					datasetBuffer = readDatasetFromServer();
					result = new Ergebnis("Reading DOM-File", "", "", "", "", "", "", "", "", "0", "", "", "", "",
							"Read dataset from Source3 (Server)", Ergebnis.FT_IO);
					ergs.add(result);
					break;
				case 'L':
				case 'l':
					// Source local database/pruefstand/dom
					checkedSources = checkedSources + ",database_pruefstand_dom";
					datasetBuffer = readDatasetFromLocalDatabasePruefstandDom();
					result = new Ergebnis("Reading DOM-File", "", "", "", "", "", "", "", "", "0", "", "", "", "",
							"Read dataset from Source3 (Local)", Ergebnis.FT_IO);
					ergs.add(result);
					break;
				case 'D':
				case 'd':
					// Source DOM
					checkedSources = checkedSources + ",DOM";
					datasetBuffer = readDatasetFromDom(ergs);
					result = new Ergebnis("Reading DOM-File", "", "", "", "", "", "", "", "", "0", "", "", "", "",
							"Read dataset from Source3 (Dom)", Ergebnis.FT_IO);
					ergs.add(result);
					if ((datasetBuffer != null) && (datasetBuffer.length() > 0)) {
						readFromDOM = true; // remember that we got the data from DOM
					}
					break;
				default:
					datasetBuffer = null;
				}
				if (debug > 0)
					System.out.println("EWSSynchro.readXML: Finished reading dataset from Source3 (Type=" + DatasetSource3 + ")");
			} catch (Exception e1) {
				// third source did not get the dataset
				datasetBuffer = null;
			}
		}

		// decide if dataset must be present depending on ReadOp: ReadOP:
		// R->READ, E->DSEXISTIO, N->DSEXISTNIO
		switch (ReadOp) {
		case 'N': // DATASET must NOT exist
			if ((datasetBuffer != null) && (datasetBuffer.length() > 10)) { // DS
				// exists
				result = new Ergebnis("DSEXIST", "", "", "", "", "", "DS EXISTS", "DS NOT EXISTS", "", "0", "", "", "",
						"Dataset successfully read from " + checkedSources, "Dataset already exists", Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.readXML: Dataset already exists, Dataset successfully read from " + checkedSources);
				throw new PPExecutionException("EWSSynchro.readXML: Dataset already exists");
			} else { // DS not exists
				// check if the access to the file failed because of other reasons ???? -> TBD
				result = new Ergebnis("DSEXIST", "", "", "", "", "", "DS NOT EXISTS", "DS NOT EXISTS", "", "0", "", "", "",
						"", "", Ergebnis.FT_IO);
				ergs.add(result);
				filterErrors(ergs); // remove NIO-Results of DOM-Access if file locally found
				return; // IO so return
			}
		case 'E': // DATASET must exist
			if ((datasetBuffer == null) || (datasetBuffer.length() == 0)) { // DS not exists
				result = new Ergebnis("DSEXIST", "", "", "", "", "", "DS NOT EXISTS", "DS EXISTS", "", "0", "", "", "",
						"Unable to read dataset from " + checkedSources, "Dataset not available", Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.readXML: Dataset not exists, Error reading dataset from sources " + checkedSources);
				throw new PPExecutionException("EWSSynchro.readXML: Dataset not available");
			} else { // DS exists
				result = new Ergebnis("DSEXIST", "", "", "", "", "", "DS EXISTS", "DS EXISTS", "", "0", "", "", "", "", "", Ergebnis.FT_IO);
				ergs.add(result);
				// dataset will not be checked, so parsing is not needed
				return;
			}
			// break; //Not needed because a return is done before
		case 'R': // DATASET must exist for Read
		case ' ': // DATASET must exist for Read
		default:
			if ((datasetBuffer == null) || (datasetBuffer.length() == 0)) {
				result = new Ergebnis("Reading DOM-File failed", "", "", "", "", "", "", "", "", "0", "", "", "",
						"Unable to read dataset from " + checkedSources, "Read dataset failed", Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.readXML: Error reading dataset from sources " + checkedSources);
				throw new PPExecutionException("EWSSynchro.readXML: Dataset not available");
			} else {
				filterErrors(ergs); // remove NIO-Results of DOM-Access if file locally found
			}
			break;
		}
		
		//Separate method for Parsing introduced
		parseXML(datasetBuffer, ergs);

		// now all internal variables have been filled. Check if they seem to be correct.
		if (CheckDataset) {
			if (debug > 2)
				System.out.println("EWSSynchro.readXML: " + ergs.size()
						+ " error(s) before checkDataset: " + ergs);
			try {
				checkDataset(ergs);
				if (debug > 2)
					System.out.println("EWSSynchro.readXML: " + ergs.size() + " error(s) after checkDataset: " + ergs);
			} catch (PPExecutionException ppee) {
				if (debug > 2)
					System.out.println("EWSSynchro.readXML: " + ergs.size() + " error(s) after checkDataset: " + ergs);
				throw ppee;
			}
		}

		// Doku read DOM to local report
		if (readFromDOM) {

			// *************************
			// start document locally
			// **************************
			// date and time of sucumentation
			Date rightNow = new Date();
			StringBuffer dokuLocal = new StringBuffer(1000);
			dokuLocal.append("READDOM: ");
			dokuLocal.append(getPr�fling().getAuftrag().getFahrgestellnummer()); // append
			// FGNR
			dokuLocal.append(";");
			dokuLocal.append((this.Ews_Interface == null) ? "null" : this.Ews_Interface.toUpperCase()); // append
			// Ews_Interface
			dokuLocal.append(";");
			dokuLocal.append((this.Ews_EcuType == null) ? "null" : this.Ews_EcuType.toUpperCase()); // append Ews_EcuType
			dokuLocal.append(";");
			dokuLocal.append((this.Ews_Code == null) ? "null" : encrypt(this.Ews_Code).toUpperCase()); // append Ews_Code
			dokuLocal.append(";");
			dokuLocal.append(rightNow + "\r\n"); // append date and time
			if (debug > 2)
				System.out.println("EWSSynchro.readXML from DOM: DOKULOCAL = '" + dokuLocal + "'");
			// save dataset
			String localFileName = DOKU_LOCAL_FILENAME;
			String localFilePath = DEFAULT_DOKU_LOCAL_PATH;
			boolean fileCreated;
			boolean pathCreated;
			// get the filepath from package properties
			localFilePath = PB.getString(DOKU_LOCAL_PATH_PROPERTY); 
			// is the property present?
			if ((localFilePath == null)
					|| (DOKU_LOCAL_PATH_PROPERTY.equals(localFilePath))) {
				//property not present, so use default
				localFilePath = DEFAULT_DOKU_LOCAL_PATH;
				result = new Ergebnis("ExecFehler", "LOCAL_DOCU", "", "", "", DOKU_LOCAL_PATH_PROPERTY, "null", "", "", "0", "", "", "",
						"Warning: Path for Local Documentation not defined", "Local path not defined", Ergebnis.FT_IGNORE);
				ergs.add(result);
			}
			File localPath = new File(localFilePath);
			File localFile = new File(localPath, localFileName);
			try {
				// If not exists create the directory for the Cas-dat files
				pathCreated = localPath.mkdirs(); 
				// If not exists create the ewssync.dat file
				fileCreated = localFile.createNewFile(); 
				if (debug > 2)
					System.out.println("EWSSynchro.readXML from DOM: Doku local: pathCreated=" + pathCreated
									+ " fileCreated=" + fileCreated);

				/* Write the dataset */
				if (debug > 2) {
					// Fetch the absolute path including filename
					String localFileNameAbsolut = localFile.getAbsolutePath(); 
					System.out.println("EWSSynchro.readXML from DOM: Doku local: filename: " + localFileNameAbsolut); // Debug-message
				}
				// FileWriter for ewssync.dat (appendmode)
				FileWriter out = new FileWriter(localFile, true); 
				out.write(dokuLocal.toString()); // write buffer to cas.dat
				out.flush(); // flush the buffer
				out.close(); // close the file
				if (debug > 1)
					System.out.println("EWSSynchro.readXML from DOM: LOCAL succesfully saved");

			} catch (Exception e) {
				result = new Ergebnis("ExecFehler", "LOCAL_DOCU", "", "", "", "", "", "", "", "0", "", "", "",
						"Warning: unable to save local Documentation", "Local documentation failed " + e.getMessage(), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.readXML from DOM: error saving LOCAL ");
				e.printStackTrace(System.out);
				throw new PPExecutionException("EWSSynchro.readXML from DOM: Local documentation of dataset failed");
			}
			result = new Ergebnis("Exec", "LOCAL_DOCU", "", "", "", "","", "", "", "0", "", "", "", "Local Documentation", "", Ergebnis.FT_IO);
			ergs.add(result);

		}

	}

	/**
	 * Parse an XML and extract all Informations.
	 */
	private void parseXML(StringBuffer datasetBuffer, Vector ergs) throws PPExecutionException {
		
		//local Variables
		String datasetString; 
		int beginIndex, endIndex;
		Ergebnis result;
		
		// prepare the datasetBuffer for parsing
		// ***********************

		// remove (if already existing) the xml-header and the dtd-path in the XML-dataset
		if (debug > 2)
			System.out.println("EWSSynchro.parseXML: Start preparing dataset for parsing");
		datasetString = new String(datasetBuffer); // get a String-representation of the datasetBuffer
		//Remove xml-header <?xml...
		beginIndex = datasetString.indexOf("<?xml"); // look for DOCTYPE
		if (debug > 2)
			System.out.println("EWSSynchro.parseXML: preparing dataset for parsing: beginIndex of <?xml '" + beginIndex + "'");
		// Is xml-header existing existing?
		if (beginIndex != -1) {
			// yes: so remove the existing <?xml...
			endIndex = datasetString.indexOf("?>", beginIndex) + 2;
			if (debug > 2)
				System.out.println("EWSSynchro.parseXML: preparing dataset for parsing: endIndex of <?xml...?> '" + endIndex + "'");
			if ((endIndex == -1) || ((endIndex - beginIndex) < 3)) {
				// Error in DTD-definition
				System.out.println("EWSSynchro.parseXML: Error in xml-header <?xml...?> ("
								+ getPr�fling().getAuftrag().getFahrgestellnummer7()
								+ "E.XML" + ")");
				result = new Ergebnis("Reading XML-File failed", "", "", "", "", "", "", "", "", "0", "", "", "",
						"Error in xml-header <?xml...?>", PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML: xml-header <?xml...?> in dataset invalid");
			}
			datasetBuffer.replace(beginIndex, endIndex, "");	//Delete the xml-header
			datasetString = new String(datasetBuffer); // refresh datasetstring
		} else {
			// already no xml-header, so do nothing
		}
		//Remove DOCTYPE
		beginIndex = datasetString.indexOf("<!DOCTYPE"); // look for DOCTYPE
		if (debug > 2)
			System.out.println("EWSSynchro.parseXML: preparing dataset for parsing: beginIndex of <!DOCTYPE '" + beginIndex + "'");
		// Is DOCTYPE already existing?
		if (beginIndex != -1) {
			// yes: so remove the existing DOCTYPE
			endIndex = datasetString.indexOf(">", beginIndex) + 1; // +1 because the > is also replaced
			if (debug > 2)
				System.out.println("EWSSynchro.parseXML: preparing dataset for parsing: endIndex of <!DOCTYPE '" + endIndex + "'");
			if ((endIndex == -1) || ((endIndex - beginIndex) < 27)) {
				// Error in DTD-definition
				System.out.println("EWSSynchro.parseXML: Error in DTD-definition ("
								+ getPr�fling().getAuftrag().getFahrgestellnummer7()
								+ "E.XML" + ")");
				result = new Ergebnis("Reading XML-File failed", "", "", "", "", "", "", "", "", "0", "", "", "",
						"Error in DTD-definition", PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML: DTD-definition in dataset invalid");
			}
			datasetBuffer.replace(beginIndex, endIndex, "");	//Delete the DTD-definition
			datasetString = new String(datasetBuffer); // refresh datasetstring
		} else {
			// already no DOCTYPE, so do nothing
		}

		if (debug > 1)
			System.out.println("EWSSynchro.parseXML: Read XML-dataset:\n" + datasetString);

		// parse the datasetBuffer
		// ***********************
		beginIndex = datasetString.indexOf("vehicleImmobilizer"); // look for main-tag
		if (beginIndex != -1) {
			//new xml-format vehicleImmobliser
			parseXML_internal_vehicleImmobilizer(datasetString, ergs);
		} else {
			//old xml-format ewsInfo
			parseXML_internal_ewsinfo(datasetString, ergs);
		}
	}
	
	/**
	 * Parse an XML and extract all Informations.
	 */
	private void parseXML_internal_vehicleImmobilizer(String xml, Vector ergs) throws PPExecutionException {
		SXMLDocument myDoc = null;
		boolean parseNewFormat = false;			//true indicates we also have an egs_isn (for L6) no longer used
		String parseVin = null, parseECUType = null, parseInterface = null, parseCode = null, parseEgsCode = null;
		String parseCodeEnc = null;			//null indicates to use STD
		String parseEgsCodeEnc = null;		//null indicates to use STD
		Ergebnis result = null;
		
		try {
			if (debug > 0)
				System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: Start parsing XML-dataset");
			myDoc = sxmlParse( xml );
			if (debug > 2) 
				System.out.println("EwsSynchro.parseXML_internal_vehicleImmobilizer: parsedXML\n" + myDoc);
		} catch (Exception e) {
			throw new PPExecutionException("Error during parsing XML: original Message: " + e.getMessage());
		}
		
		// parse the data
		try {
			/*
			 * <?xml version="1.0" encoding="iso-8859-1"?>
			 * <vehicleImmobilizer xmlns="http://bmw.com/immobiliser" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://bmw.com/immobiliser ../vehicleImmobilizerV01_00.xsd" version="01.00" refSchema="vehicleImmobilizerV01_00.xsd">
			 *   <vehicle vin="WB12345668FF00001">
			 *     <coding>
			 *       <synchronisationInfo version="01.02" refSchema="synchronisationInfoV0102.xsd" creationDateTime="2007-09-18T16:13:29">
			 *         <immobiliserEcu ecuType="CAS4"/>
			 *         <interface interfaceType="EWS4EGS"/>
			 *         <synchronisedEcus>
			 *           <synchronisedEcu ecuType="DMEDDE1">
			 *             <syncCode encryption="STD">2D0F694BA587E1C32D0F694BA587E1C3</syncCode>
			 *           </synchronisedEcu>
			 *           <synchronisedEcu ecuType="EGS">
			 *             <syncCode encryption="STD">6879F1C2</syncCode>
			 *           </synchronisedEcu>
			 *         </synchronisedEcus>
			 *       </synchronisationInfo>
			 *     </coding>
			 *   </vehicle>
			 * </vehicleImmobilizer>
			 */
			SXMLNode mainNode = null;	//<vehicleImmobilizer>
			SXMLNode vehicleNode = null;	//<vehicle>
			SXMLAttribute vinAttr = null;	// vin=
			SXMLNode codingNode = null;	//<coding>
			SXMLNode syncInfoNode = null;	//<synchronisationInfo>
			SXMLNode currNode = null;	//<immobiliserEcu>, <interface>, <synchronisedEcus>
			SXMLNode syncEcuNode = null;	//<synchronisedEcu>
			SXMLNode syncCodeNode = null;	//<syncCode>
			SXMLAttribute casEcuAttr = null;	// ecuType=
			SXMLAttribute syncEcuAttr = null;	// ecuType=
			SXMLAttribute syncEncAttr = null;	// encryption=
			SXMLAttribute interfaceAttr = null;	// interfaceType=
			Vector vec = null, vec2=null, vec3=null;;

			//<vehicleImmobilizer>
			mainNode = myDoc.getMainNode();
			if (mainNode == null) {
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: no mainNode in xml"); 
			}
			if (!"vehicleImmobilizer".equalsIgnoreCase(mainNode.getName())) {
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: mainNode is not <vehicleImmobilizer>"); 
			}
			if (debug > 4)
				System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->mainNode: " + mainNode);
			
			//<vehicle>
			vec = mainNode.getChildNodes();
			if (debug > 4)
				System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->mainNode->#childs: " + ((mainNode.getChildNodes() == null) ? -1 : mainNode.getChildNodes().size()));
			if (vec == null || vec.size() == 0) {
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <vehicleImmobilizer> has no childs in xml"); 
			}
			if (vec.size() > 1) {
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <vehicleImmobilizer> has more than one childs in xml"); 
			}
			vehicleNode = (SXMLNode)vec.elementAt( 0 );
			if (debug > 4)
				System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->vehicleNode: " + vehicleNode);
			if (vehicleNode == null) {
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <vehicle> is missing in xml"); 
			}
			if (!"vehicle".equalsIgnoreCase(vehicleNode.getName())) {
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: 'vehicle' expected instead of: " + vehicleNode.getName());
			}
			//Attribute vin
			vec = vehicleNode.getAttributes();
			if (debug > 4)
				System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->vehicleNode->#attr: " + ((vehicleNode.getAttributes() == null) ? -1 : vehicleNode.getAttributes().size()));
			if (vec == null || vec.size() == 0) {
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <vehicle> attribute 'vin' is missing in xml"); 
			}
			if (vec.size() > 2) {
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <vehicle> has more than two attributes in xml"); 
			}
            
            parseVin=null;
            for (int i=0; i<vec.size();i++)
            {
    			vinAttr = (SXMLAttribute)vec.elementAt( i );
    			if (debug > 4)
    				System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->vinAttr: " + vinAttr);
    			if (vinAttr == null) 
                {
    				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <vehicle> attribute 'vin' is missing in xml"); 
    			}
    			if ("vin".equalsIgnoreCase(vinAttr.getName())) 
                {
                    parseVin = vinAttr.getValue();
                    if ((parseVin == null) || (parseVin.trim().length() == 0)) 
                    { 
                        // No attribute vin for <immobiliser>
                        throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error no attribute vin for element <immobiliser>");
                    }
    			}
            }
            if (parseVin==null)
            {
                throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: 'vin' not found");
            }
            
			//check length of vin
			if (parseVin.length() > 17) { 
				// VIN is max. 17 characters
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error parsing VIN: too long");
			}
			if (parseVin.length() < 7) { 
				// VIN is min. 7 characters
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error parsing VIN: too short");
			}
			if (debug > 0)
				System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: Parsed VIN=" + parseVin);
			
			//<coding>
			vec = vehicleNode.getChildNodes();
			if (debug > 4)
				System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->vehicleNode->#childs: " + ((vec == null) ? -1 : vec.size()));
			if (vec == null || vec.size() == 0) {
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <vehicle> has no childs in xml"); 
			}
			if (vec.size() > 2) {
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <vehicle> has more than two childs in xml"); 
			}
			//Annahme <coding> ist im ersten child
			codingNode = (SXMLNode)vec.elementAt( 0 );
			if (debug > 4)
				System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->codingNode: " + codingNode);
			if (codingNode == null) {
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <coding> is missing in xml"); 
			}
			if (!"coding".equalsIgnoreCase(codingNode.getName())) {
				//<coding> ist nicht das erste child
				if (vec.size() == 2) {
					//Zwei childs, probiere child 2
					codingNode = (SXMLNode)vec.elementAt( 1 );
					if (debug > 4)
						System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->codingNode2: " + codingNode);
				} else {
					//Kein zweites child, somit kein <coding>
					throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <coding> expected as child of <vehicle>");
				}
			}
			if (!"coding".equalsIgnoreCase(codingNode.getName())) {
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <coding> expected as child of <vehicle>");
			}

			//<synchronisationInfo>
			vec = codingNode.getChildNodes();
			if (debug > 4)
				System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->codingNode->#childs: " + ((vec == null) ? -1 : vec.size()));
			if (vec == null || vec.size() == 0) {
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <coding> has no childs in xml"); 
			}
			if (vec.size() > 3) {
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <coding> has more than three childs in xml"); 
			}
			//loop over childs and search for synchronisationInfo
			for (int i = 0; i < vec.size(); i++) {
				syncInfoNode = (SXMLNode)vec.elementAt( i );
				if (debug > 4)
					System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->syncInfoNode*: " + syncInfoNode);
				if ("synchronisationInfo".equalsIgnoreCase(syncInfoNode.getName())) {
					break;	//exit for loop
				}
			}
			if (!"synchronisationInfo".equalsIgnoreCase(syncInfoNode.getName())) {
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <synchronisationInfo> expected as child of <coding>");
			}
			if (debug > 4)
				System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->syncInfoNode: " + syncInfoNode);
			
			//<immobiliserEcu>, <interface>, <synchronisedEcus>
			vec = syncInfoNode.getChildNodes();
			if (debug > 4)
				System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->syncInfoNode->#childs: " + ((vec == null) ? -1 : vec.size()));
			if (vec == null || vec.size() == 0) {
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <synchronisationInfo> has no childs in xml"); 
			}
			if (vec.size() > 3) {
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <synchronisationInfo> has more than three childs in xml"); 
			}
			//loop over childs <synchronisationInfo> and handle childs
			for (int i = 0; i < vec.size(); i++) {
				currNode = (SXMLNode)vec.elementAt( i );
				if (debug > 4)
					System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->currNode*: " + currNode);
	
				if ("immobiliserEcu".equalsIgnoreCase(currNode.getName())) {
					//<immobiliserEcu>
					vec2 = currNode.getAttributes();
					if (debug > 4)
						System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->immobiliserEcu->#attr: " + ((vec2 == null) ? -1 : vec2.size()));
					if (vec2 == null || vec2.size() == 0) {
						throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <immobiliserEcu> attribute 'ecuType' is missing in xml"); 
					}
					if (vec2.size() > 1) {
						throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <immobiliserEcu> has more than one attribute in xml"); 
					}
					//ecuType=
					casEcuAttr = (SXMLAttribute)vec2.elementAt( 0 );
					if (debug > 4)
						System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->casEcuAttr: " + casEcuAttr);
					if (casEcuAttr == null) {
						throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <immobiliserEcu> attribute 'ecuType' is missing in xml"); 
					}
					if (!"ecuType".equalsIgnoreCase(casEcuAttr.getName())) {
						throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: 'ecuType' expected instead of: " + casEcuAttr.getName());
					}
					parseECUType = casEcuAttr.getValue();
					if ((parseECUType != null) && (parseECUType.length() > 6)) { 
						// <ewsEcuType> is max. 6 characters
						throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error parsing <immobiliserEcu ecuType=...>: too long");
					}
					if ((parseECUType != null) && (parseECUType.length() < 3)) { 
						// <ewsEcuType> is min. 3 characters
						throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error parsing <immobiliserEcu ecuType=...>: too short");
					}
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: Parsed ewsEcuType=" + parseECUType);
				} else if ("interface".equalsIgnoreCase(currNode.getName())) {
					//<interface>
					vec2 = currNode.getAttributes();
					if (debug > 4)
						System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->interface->#attr: " + ((vec2 == null) ? -1 : vec2.size()));
					if (vec2 == null || vec2.size() == 0) {
						throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <interface> attribute 'interfaceType' is missing in xml"); 
					}
					if (vec2.size() > 1) {
						throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <interface> has more than one attribute in xml"); 
					}
					//interfaceType=
					interfaceAttr = (SXMLAttribute)vec2.elementAt( 0 );
					if (debug > 4)
						System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->interfaceAttr: " + interfaceAttr);
					if (interfaceAttr == null) {
						throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <interface> attribute 'interfaceType' is missing in xml"); 
					}
					if (!"interfaceType".equalsIgnoreCase(interfaceAttr.getName())) {
						throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: 'interfaceType' expected instead of: " + interfaceAttr.getName());
					}
					parseInterface = interfaceAttr.getValue();
					if ((parseInterface != null) && (parseInterface.length() > 7)) { 
						// <ewsInterface> is max. 7 characters
						throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error parsing <interface interfaceType=...>: too long");
					}
					if ((parseInterface != null) && (parseInterface.length() < 4)) { 
						// <ewsEcuType> is min. 4 characters
						throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error parsing <interface interfaceType=...>: too short");
					}
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: Parsed interface=" + parseInterface);
				} else if ("synchronisedEcus".equalsIgnoreCase(currNode.getName())) {
					//<synchronisedEcus>
					vec2 = currNode.getChildNodes();
					if (debug > 4)
						System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->synchronisedEcus->#childs: " + ((vec2 == null) ? -1 : vec2.size()));
					if (vec2 == null || vec2.size() == 0) {
						throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <synchronisedEcus> has no childs in xml"); 
					}
					if (vec2.size() > 2) {
						throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <synchronisedEcus> has more than two childs in xml"); 
					}
					//loop over synchronisedEcus
					for (int j = 0; j < vec2.size(); j++) {
						syncEcuNode = (SXMLNode)vec2.elementAt( j );
						if (debug > 4)
							System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->syncEcuNode*: " + syncEcuNode);
						if (syncEcuNode == null) {
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <synchronisedEcu> is missing in xml"); 
						}
						if (!"synchronisedEcu".equalsIgnoreCase(syncEcuNode.getName())) {
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <synchronisedEcu> expected instead of: " + syncEcuNode.getName());
						}						
						
						// ecuType= DMEDDE1 oder EGS
						vec3 = syncEcuNode.getAttributes();
						if (debug > 4)
							System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->syncEcuNode->#attr: " + ((vec3 == null) ? -1 : vec3.size()));
						if (vec3 == null || vec3.size() == 0) {
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <synchronisedEcus> attribute 'ecuType' is missing in xml"); 
						}
						if (vec3.size() > 1) {
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <synchronisedEcus> has more than one attribute in xml"); 
						}
						syncEcuAttr = (SXMLAttribute)vec3.elementAt( 0 );
						if (debug > 4)
							System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->syncEcuAttr: " + syncEcuAttr);
						if (syncEcuAttr == null) {
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <synchronisedEcus> attribute 'ecuType' is missing in xml"); 
						}
						if (!"ecuType".equalsIgnoreCase(syncEcuAttr.getName())) {
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: 'ecuType' expected instead of: " + casEcuAttr.getName());
						}
						String syncCode = null;
						String syncEnc = null;
						String syncEcu = syncEcuAttr.getValue();
						if (debug > 0)
							System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: Parsed syncEcu=" + syncEcu);
						if (syncEcu == null) {
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <synchronisedEcus> attribute 'ecuType' is null in xml"); 
						}
						//Fallunterscheidung syncEcu erfolgt sp�ter
						
						//<syncCode>
						vec3 = syncEcuNode.getChildNodes();
						if (debug > 4)
							System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->syncEcuNode->#childs: " + ((vec3 == null) ? -1 : vec3.size()));
						if (vec3 == null || vec3.size() == 0) {
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <synchronisedEcu> has no childs in xml"); 
						}
						if (vec3.size() > 1) {
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <synchronisedEcu> has more than one childs in xml"); 
						}
						syncCodeNode = (SXMLNode) vec3.elementAt( 0 );
						if (debug > 4)
							System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->syncCodeNode: " + syncCodeNode);
						if (syncCodeNode == null) {
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <syncCode> is missing in xml"); 
						}
						if (!"syncCode".equalsIgnoreCase(syncCodeNode.getName())) {
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <syncCode> expected instead of: " + syncCodeNode.getName());
						}
						
						//<syncCode encryption=...>
						vec3 = syncCodeNode.getAttributes();
						if (debug > 4)
							System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->syncCodeNode->#attr: " + ((vec3 == null) ? -1 : vec3.size()));
						if (vec3 == null || vec3.size() == 0) {
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <syncCode> attribute 'encryption' is missing in xml"); 
						}
						if (vec3.size() > 1) {
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <syncCode> has more than one attribute in xml"); 
						}
						syncEncAttr = (SXMLAttribute)vec3.elementAt( 0 );
						if (debug > 4)
							System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: PARSE_XML->syncEncAttr: " + syncEncAttr);
						if (syncEncAttr == null) {
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <syncCode> attribute 'encryption' is missing in xml"); 
						}
						if (!"encryption".equalsIgnoreCase(syncEncAttr.getName())) {
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: 'encryption' expected instead of: " + casEcuAttr.getName());
						}
						syncEnc = syncEncAttr.getValue();
						if (debug > 0)
							System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: Parsed syncEnc=" + syncEnc);
						if ((syncEnc != null) && (syncEnc.length() > 5)) { 
							// encryption is max. 5 characters
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error parsing <syncCode encryption=...>: too long");
						}
						if ((syncEnc != null) && (syncEnc.length() < 3)) { 
							// encryption is min. 3 characters
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error parsing <syncCode encryption=...>: too short");
						}
						if ((syncEnc != null) && (syncEnc.trim().length() != 0) && !syncEnc.equalsIgnoreCase("STD") && !syncEnc.equalsIgnoreCase("AES") && !syncEnc.equalsIgnoreCase("PLAIN")) {
							//unknown encryption-algorithm
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Element <syncCode encryption=...>: " + syncEnc);
						}
						if (syncEnc.equalsIgnoreCase("AES")) {
							//AES not supported yet
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Element <syncCode encryption=...>: AES not supported yet");
						}
						
						//<syncCode> -> value
						syncCode = syncCodeNode.getValue();
						if (debug > 0)
							System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: Parsed syncCode=" + syncCode);
						
						//Fallunterscheidung syncEcu
						if ("DMEDDE".equalsIgnoreCase( syncEcu ) || "DMEDDE1".equalsIgnoreCase( syncEcu )) {
							//We have a DMEDDE SecretKey
							parseCode = syncCode;
							parseCodeEnc = syncEnc;
							if (debug > 0)
								System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: Parsed DMEDDE1-SK");
						} else if ("EGS".equalsIgnoreCase( syncEcu )) {
							//we have an ISN for EGS
							parseEgsCode = syncCode;
							parseEgsCodeEnc = syncEnc;
							parseNewFormat = true;
							if (debug > 0)
								System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: Parsed EGS_ISN");
						} else {
							throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <synchronisedEcus> attribute 'ecuType' is invalid in xml: " + syncEcu);
						}
						
					}
				} else {
					throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: <synchronisedEcus> has an unknown child in xml: " + currNode.getName());
				}
			}
			
			if ("EWS4EGS".equalsIgnoreCase(Ews_Interface)) {
				//new Format as DEFAULT
				parseNewFormat = true;
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: EWS4EGS -> parseNewFormat = true");
			}
					
		} catch (Exception x) {
			// Error while parsing XML-dataset
			String v7 = null;
			if (getPr�fling() != null) v7 = getPr�fling().getAuftrag().getFahrgestellnummer7();
			else v7 = "???????";
			System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error while parsing XML-dataset ("
				+ v7 + "E.XML" + ")");
			x.printStackTrace(System.out);
			result = new Ergebnis("Parsing DOM-File failed", "", "", "", "", "", "", "", "", "0", "", "", "",
					"Error while parsing XML-dataset " + x.getMessage(), PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
			ergs.add(result);
			throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: dataset invalid"); 
			// handled by the next outer catch in this pp
		}
		
		// extract and decrypt content
		// ***********************

		//check vin in dataset against vin in order
		String v7 = null, v17 = null;
		
		if (getPr�fling() != null) {
			//we have a Pr�fling, so we also have a vin7 and vin17
			v7 = getPr�fling().getAuftrag().getFahrgestellnummer7();
			v17 = getPr�fling().getAuftrag().getFahrgestellnummer();
			//check vin in dataset against the virtual car
			if (!v7.equalsIgnoreCase(parseVin)
					&& !v17.equalsIgnoreCase(parseVin)) {
				result = new Ergebnis("VIN", "", "", "", "", "", "", "", "", "0", "", "", "",
						"EWSSynchro VIN in dataset not VIN in virtual car", PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: VIN in dataset not VIN in virtual car");
			}
		} else {
			v7 = "???????";
			v17 = "?????????????????";
		}
		// EWS_ECUTYPE
		// storage type: String
		// example values: "EWS3","CAS2","CAS3"
		Ews_EcuType = parseECUType;
		// EWS_INTERFACE
		// storage type: String
		// example value: "EWS3", "EWS4", "EWS4EGS"
		Ews_Interface = parseInterface;
		// EWS_CODE
		// storage type: HEX
		// example value: "0123456789ABCDEF0123456789ABCDEF"
		// Random
		if (parseCode != null) {
			if (parseCode.length() != 32) {
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: EWSCode in dataset is invalid");
				result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EWSCode in dataset is invalid",
						PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error ewscode: wrong length");
			}
			if (!isHex(parseCode)) {
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: EWSCode in dataset is invalid");
				result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EWSCode in dataset is invalid",
						PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error ewscode: not hexadecimal");
			}
			if ((parseCodeEnc == null) || (parseCodeEnc.equalsIgnoreCase("STD"))) {
				//Standard-Encryption
				if (parseCode.length() > 32) { 
					// <ewscode> is max. 32 characters
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: EWSCode in dataset is invalid");
					result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EWSCode in dataset is invalid",
							PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error parsing <ewscode> for DME/DDE: too long");
				}
				if (parseCode.length() < 32) { 
					// <ewscode> is min. 32 characters
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: EWSCode in dataset is invalid");
					result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EWSCode in dataset is invalid",
							PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error parsing <ewscode> for DME/DDE: too short");
				}
				Ews_Code = decrypt(parseCode);
			} else if (parseCodeEnc.equalsIgnoreCase("PLAIN")) {
				//no Encryption
				if (parseCode.length() > 32) { 
					// <ewscode> is max. 32 characters
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: EWSCode in dataset is invalid");
					result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EWSCode in dataset is invalid",
							PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error parsing <ewscode> for DME/DDE: too long");
				}
				if (parseCode.length() < 32) { 
					// <ewscode> is min. 32 characters
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: EWSCode in dataset is invalid");
					result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EWSCode in dataset is invalid",
							PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error parsing <ewscode> for DME/DDE: too short");
				}
				Ews_Code = parseCode;
			} else if (parseCodeEnc.equalsIgnoreCase("AES")) {
				//AES not supported yet
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: EWSCode in dataset is invalid");
				result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EWSCode in dataset is invalid",
						PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Element <ewsCode> for DME/DDE: Atribute encryption: AES not supported yet");
			} else {
				//Unknown encryption
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: EWSCode in dataset is invalid");
				result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EWSCode in dataset is invalid",
						PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Element <ewsCode> for DME/DDE: Atribute encryption: Unknown mode");
			}
		} else {
			Ews_Code = null;
		}
		// EGS_ISN
		// storage type: HEX
		// example value: "01234567"
		// Random
		if (parseEgsCode != null) {
			if (parseEgsCode.length() != 8) {
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: EGS_ISN in dataset is invalid");
				result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EGSCode in dataset is invalid",
						PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error EGS_ISN: wrong length");
			}
			if (!isHex(parseEgsCode)) {
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: EGS_ISN in dataset is invalid");
				result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EGSCode in dataset is invalid",
						PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error EGS_ISN: not hexadecimal");
			}
			if ((parseEgsCodeEnc == null) || (parseEgsCodeEnc.equalsIgnoreCase("STD"))) {
				//Standard-Encryption
				if (parseEgsCode.length() > 8) { 
					// <ewscode> is max. 8 characters
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: EGS_ISN in dataset is invalid");
					result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EGSCode in dataset is invalid",
							PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error parsing <ewscode> for EGS: too long");
				}
				if (parseEgsCode.length() < 8) { 
					// <ewscode> is min. 8 characters
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: EGS_ISN in dataset is invalid");
					result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EGSCode in dataset is invalid",
							PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error parsing <ewscode> for EGS: too short");
				}
				Egs_Code = decrypt(parseEgsCode);
			} else if (parseEgsCodeEnc.equalsIgnoreCase("PLAIN")) {
				//no Encryption
				if (parseEgsCode.length() > 8) { 
					// <ewscode> is max. 8 characters
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: EGS_ISN in dataset is invalid");
					result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EGSCode in dataset is invalid",
							PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error parsing <ewscode> for EGS: too long");
				}
				if (parseEgsCode.length() < 8) { 
					// <ewscode> is min. 8 characters
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: EGS_ISN in dataset is invalid");
					result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EGSCode in dataset is invalid",
							PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Error parsing <ewscode> for EGS: too short");
				}
				Egs_Code = parseEgsCode;
			} else if (parseEgsCodeEnc.equalsIgnoreCase("AES")) {
				//AES not supported yet
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: EGS_ISN in dataset is invalid");
				result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EGSCode in dataset is invalid",
						PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Element <ewsCode> for EGS: Atribute encryption: AES not supported yet");
			} else {
				//Unknown encryption
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: EGS_ISN in dataset is invalid");
				result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EGSCode in dataset is invalid",
						PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_vehicleImmobilizer: Element <ewsCode> for EGS: Atribute encryption: Unknown mode");
			}
		} else {
			Egs_Code = null;
		}
		if (debug > 0) {
			System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: Parsed (decrypted) ewsCode=" + Ews_Code);
			System.out.println("EWSSynchro.parseXML_internal_vehicleImmobilizer: Parsed (decrypted) egsCode=" + Egs_Code);
		}			
		if (debug > 0)
			System.out .println("EWSSynchro.parseXML_internal_vehicleImmobilizer: Finished decryption after parsing");

		//throw new PPExecutionException("vehicleImmobilizer not implemented yet");
	}
	
	/**
	 * Parse an XML and extract all Informations.
	 */
	private void parseXML_internal_ewsinfo(String xml, Vector ergs) throws PPExecutionException {
		
		String xmlUC;
		boolean parseNewFormat = false;			//true indicates we also have an egs_isn (for L6) no longer used
		String parseVin = null, parseECUType = null, parseInterface = null, parseCode = null, parseEgsCode = null;
		String parseCodeEnc = null;			//null indicates to use STD
		String parseEgsCodeEnc = null;		//null indicates to use STD
		Ergebnis result;
		
		// parse the data
		try {
			if (debug > 0)
				System.out.println("EWSSynchro.parseXML_internal_ewsinfo: Start parsing XML-dataset");
			/*
			 * EXAMPLE for old format (Version 1)
			 * EXAMPLE <?xml version="1.0" encoding="iso-8859-1" ?> 
			 * <ewsinfo vin="WBAEE111119046000"> 
			 * 		<ewsecu> 
			 * 			<ewsecutype>CAS3</ewsecutype>
			 * 		</ewsecu> 
			 * 		<ewssynchro> 
			 * 			<ewsinterface>EWS4</ewsinterface>
			 * 			<ewscode>0123456789ABCDEF0123456789ABCDEF</ewscode>
			 * 		</ewssynchro> 
			 * </ewsinfo>
			 * 
			 * EXAMPLE for new format (Version 4)
			 * <?xml version="1.0" encoding="iso-8859-1"?>
			 * <immobiliser vin="WBAWB735X7PV82000">
			 *	<ewsInfo  creationDateTime="2007-01-30T08:33:58" version="4.0">
			 *		<ewsEcu>
			 *			<ewsEcuType>CAS4</ewsEcuType>
			 *		</ewsEcu>
			 *		<ewsSynchro>
			 *			<ewsInterface>EWS4EGS</ewsInterface>
			 *			<ewsCode encryption="AES" ecuType="DMEDDE1">1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF</ewsCode>
			 *			<ewsCode encryption="STD" ecuType="EGS">F4804706</ewsCode>
			 *		</ewsSynchro>
			 *	</ewsInfo>
			 * </immobiliser>
			 */
			// make it uppercase
			xmlUC = xml.toUpperCase();
			
			//Variablen
			Node tempNode=null;
			Element tempElem=null;
			NodeList tempNodeList=null;
			Element ewsEcuElem=null;
			Element ewsSynchroElem=null;
			
			//Create a DocumentBuilder (=parser instance)
			DocumentBuilder myDOMBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			//Craete a ByteArrayInputStream for the String
			ByteArrayInputStream myBIS = new ByteArrayInputStream(xmlUC.getBytes());
			//parse the document
			Document myDocument = myDOMBuilder.parse(myBIS);
			//fetch the root-element
			Element rootElem = myDocument.getDocumentElement();
			if (rootElem == null) { 
				// No root element
				throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error no root element present");
			}
			//detect if we have new or old format
			if(rootElem.getTagName().equalsIgnoreCase("IMMOBILISER")) {
				//new format detected
				parseNewFormat = true;
				//fetch the vin
				parseVin = rootElem.getAttribute("VIN");
				if ((parseVin == null) || (parseVin.trim().length() == 0)) { 
					// No attribute vin for <immobiliser>
					throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error no attribute vin for element <immobiliser>");
				}
				
				//set rootElem to ewsInfo
				NodeList ewsinfoNodeList = rootElem.getElementsByTagName("EWSINFO");
				tempNode=null;
				tempElem=null;
				if (debug > 1) System.out.println("IMMOBILISER->EWSINFO #Childs=" + ewsinfoNodeList.getLength());
				if (ewsinfoNodeList.getLength() == 1) {
					//exact one element <ewsInfo>
					tempNode = ewsinfoNodeList.item(0);
					//we have a element
					tempElem = (Element)tempNode;
					if (debug > 1) System.out.println("current node tagName =" + tempElem.getTagName());
					//Set new root element to ewsinfo
					rootElem = tempElem;
				} else if (ewsinfoNodeList.getLength() == 0) {
					//NO element <ewsInfo>
					//no occurance of <ewsInfo> inside <immobiliser>
					throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsInfo> NOT occured within <immobiliser>");
				} else {
					//more than one element <ewsInfo>
					throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsInfo> occured more than once within <immobiliser>");
				}
				//so now we have <ewsInfo> as rootElem
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_ewsinfo: Shifted rootElem to <ewsInfo>");
				
			} else if(rootElem.getTagName().equalsIgnoreCase("EWSINFO")) {
				//old format detected
				parseNewFormat = false;
				//fetch the vin
				parseVin = rootElem.getAttribute("VIN");
				if ((parseVin == null) || (parseVin.trim().length() == 0)) { 
					// No attribute vin for <ewsinfo>
					throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error no attribute vin for element <ewsinfo>");
				}
			} else {
				//Unknown root element
				throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Unknown root element present");
			}
			//check length of vin
			if (parseVin.length() > 17) { 
				// VIN is max. 17 characters
				throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing VIN: too long");
			}
			if (parseVin.length() < 7) { 
				// VIN is min. 7 characters
				throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing VIN: too short");
			}
			if (debug > 0)
				System.out.println("EWSSynchro.parseXML_internal_ewsinfo: Parsed VIN=" + parseVin);
			
			//parse <ewsEcu>
			//get a list of all nodes with tagName ewsEcu
			tempNodeList = rootElem.getElementsByTagName("EWSECU");
			tempNode=null;
			tempElem=null;
			if (debug > 1) System.out.println("EWSINFO->EWSECU #Childs=" + tempNodeList.getLength());
			if (tempNodeList.getLength() == 1) {
				//exact one element <ewsEcu>
				tempNode = tempNodeList.item(0);
				//we have a element
				tempElem = (Element)tempNode;
				if (debug > 1) System.out.println("current node tagName =" + tempElem.getTagName());
				ewsEcuElem = tempElem;
			} else if (tempNodeList.getLength() == 0) {
				//NO element <ewsEcu>
				ewsEcuElem = null;
				//throw new PPExecutionException("EWSSynchro.parseXML: Element <ewsEcu> NOT occured within <ewsInfo>");
			} else {
				//more than one element <ewsEcu>
				throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsEcu> occured more than once within <ewsInfo>");
			}
			
			if (ewsEcuElem != null) {
				//we have a <ewsEcu>
				//parse <ewsEcuType>
				//get a list of all nodes with tagName ewsEcuType
				tempNodeList = ewsEcuElem.getElementsByTagName("EWSECUTYPE");
				tempNode=null;
				tempElem=null;
				parseECUType = null;
				if (debug > 1) System.out.println("EWSECU->EWSECUTYPE #Childs=" + tempNodeList.getLength());
				if (tempNodeList.getLength() == 1) {
					//we have a element
					tempNode = tempNodeList.item(0);
					tempElem = (Element)tempNode;
					if (debug > 1) System.out.println("current node tagName =" + tempElem.getTagName());
					tempNodeList = tempElem.getChildNodes();
					if (debug > 1) System.out.println("EWSECUTYPE->VALUE #Childs=" + tempNodeList.getLength());
					if (tempNodeList.getLength() != 1) {
						//More than one child
						throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewsEcuType>: more or less than one child for value");
					}
					tempNode = tempNodeList.item(0);
					if (debug > 1) System.out.println("NodeName: " + tempNode.getNodeName());
					if (debug > 1) System.out.println("NodeType: " + tempNode.getNodeType());
					if (debug > 1) System.out.println("NodeValue: " + tempNode.getNodeValue());
					if (tempNode.getNodeType() != Node.TEXT_NODE) {
						//More than one child
						throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewsEcuType>: value-node not of type '#text'");
					}
					parseECUType = tempNode.getNodeValue();
					
					if ((parseECUType != null) && (parseECUType.length() > 6)) { 
						// <ewsEcuType> is max. 6 characters
						throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewsEcuType>: too long");
					}
					if ((parseECUType != null) && (parseECUType.length() < 3)) { 
						// <ewsEcuType> is min. 3 characters
						throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewsEcuType>: too short");
					}
				} else if (tempNodeList.getLength() == 0) {
					//NO element <ewsEcuType>
					parseECUType = null;
					//throw new PPExecutionException("EWSSynchro.parseXML: Element <ewsEcuType> NOT occured within <ewsEcu>");
				} else {
					//more than one element <ewsEcuType>
					throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsEcuType> occured more than once within <ewsEcu>");
				}
			} else {
				//we DON'T have a <ewsEcu>
				parseECUType = null;
			}
			if (debug > 0)
				System.out.println("EWSSynchro.parseXML_internal_ewsinfo: Parsed ewsEcuType=" + parseECUType);

			//parse <ewsSynchro>
			//get a list of all nodes with tagName ewsSynchro
			tempNodeList = rootElem.getElementsByTagName("EWSSYNCHRO");
			tempNode=null;
			tempElem=null;
			if (debug > 1) System.out.println("EWSINFO->EWSSYNCHRO #Childs=" + tempNodeList.getLength());
			if (tempNodeList.getLength() == 1) {
				tempNode = tempNodeList.item(0);
				//we have a element
				tempElem = (Element)tempNode;
				if (debug > 1) System.out.println("current node tagName =" + tempElem.getTagName());
				ewsSynchroElem = tempElem;
			} else if (tempNodeList.getLength() == 0) {
				//NO element <ewsSynchro>
				ewsSynchroElem = null;
				//throw new PPExecutionException("EWSSynchro.parseXML: Element <ewsSynchro> NOT occured within <ewsInfo>");
			} else {
				//more than one element <ewsSynchro>
				throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsSynchro> occured more than once within <ewsInfo>");
			}
			
			if (ewsSynchroElem != null) {
				//we have a <ewsSynchro>
				
				//parse <ewsInterface>
				//get a list of all nodes with tagName ewsInterface
				tempNodeList = ewsSynchroElem.getElementsByTagName("EWSINTERFACE");
				tempNode=null;
				tempElem=null;
				parseInterface = null;
				if (debug > 1) System.out.println("EWSSYNCHRO->EWSINTERFACE #Childs=" + tempNodeList.getLength());
				if (tempNodeList.getLength() == 1) {
					tempNode = tempNodeList.item(0);
					//we have a element
					tempElem = (Element)tempNode;
					if (debug > 1) System.out.println("current node tagName =" + tempElem.getTagName());
					
					tempNodeList = tempElem.getChildNodes();
					if (debug > 1) System.out.println("EWSINTERFACE->VALUE #Childs=" + tempNodeList.getLength());
					if (tempNodeList.getLength() != 1) {
						//More than one child
						throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewsInterface>: more or less than one child for value");
					}
					tempNode = tempNodeList.item(0);
					if (debug > 1) System.out.println("NodeName: " + tempNode.getNodeName());
					if (debug > 1) System.out.println("NodeType: " + tempNode.getNodeType());
					if (debug > 1) System.out.println("NodeValue: " + tempNode.getNodeValue());
					if (tempNode.getNodeType() != Node.TEXT_NODE) {
						//More than one child
						throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewsInterface>: value-node not of type '#text'");
					}
					parseInterface = tempNode.getNodeValue();
					//check value
					if ((parseInterface != null) && (parseInterface.length() > 7)) { 
						// <ewsInterface> is max. 7 characters
						throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewsInterface>: too long");
					}
					if ((parseInterface != null) && (parseInterface.length() < 4)) { 
						// <ewsEcuType> is min. 4 characters
						throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewsInterface>: too short");
					}
				} else if (tempNodeList.getLength() == 0) {
					//NO element <ewsInterface>
					parseInterface = null;
					//throw new PPExecutionException("EWSSynchro.parseXML: Element <ewsInterface> NOT occured within <ewsSynchro>");
				} else {
					//more than one element <ewsInterface>
					throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsInterface> occured more than once within <ewsSynchro>");
				}
				
				//parse <ewsCode>
				//get a list of all nodes with tagName ewsCode
				tempNodeList = ewsSynchroElem.getElementsByTagName("EWSCODE");
				tempNode=null;
				tempElem=null;
				parseCode = null;
				parseEgsCode = null;
				String tempEcuType = null;
				if (debug > 1) System.out.println("EWSSYNCHRO->EWSCODE #Childs=" + tempNodeList.getLength());
				if ((tempNodeList.getLength() == 1) || (tempNodeList.getLength() == 2)) {
					//we have one or TWO elements
					int indexEwsCode = -1;
					int indexEgsCode = -1;
					//decide which one is for DMEDDE1
					tempNode = tempNodeList.item(0);
					tempElem = (Element)tempNode;
					if (debug > 1) System.out.println("EWSCODE[0] NodeName: " + tempNode.getNodeName());
					if (debug > 1) System.out.println("EWSCODE[0] NodeType: " + tempNode.getNodeType());
					if (debug > 1) System.out.println("EWSCODE[0] NodeValue: " + tempNode.getNodeValue());
					//check the attribute ecuType=
					tempEcuType = tempElem.getAttribute("ECUTYPE");
					if (debug > 1) System.out.println("EWSCODE[0] ->ECUTYPE: " + tempEcuType);
					if ((tempEcuType == null) || (tempEcuType.trim().length() == 0) || (tempEcuType.equalsIgnoreCase( "DMEDDE1" ))) {
						//ewscode for sync with DME/DDE 1 is the first one
						indexEwsCode = 0;
						indexEgsCode = 1;
					} else if (tempNodeList.getLength() == 2) {
						//ewscode for sync with DME/DDE 1 is the second one and we have two
						indexEwsCode = 1;
						indexEgsCode = 0;
					} else {
						//no code for DME/DDE 1
						throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsCode> for DME/DDE 1 is missing in <ewsSynchro>");
					}
					
					//parse ewsCode for DMEDDE1
					tempNode = tempNodeList.item(indexEwsCode);
					tempElem = (Element)tempNode;
					if (debug > 1) System.out.println("EWSCODE[DMEDDE1] NodeName: " + tempNode.getNodeName());
					if (debug > 1) System.out.println("EWSCODE[DMEDDE1] NodeType: " + tempNode.getNodeType());
					if (debug > 1) System.out.println("EWSCODE[DMEDDE1] NodeValue: " + tempNode.getNodeValue());
					if (debug > 1) System.out.println("current node tagName =" + tempElem.getTagName());
					//check the attribute ecuType=
					tempEcuType = tempElem.getAttribute("ECUTYPE");
					if (debug > 1) System.out.println("EWSCODE[DMEDDE1] ->ECUTYPE: " + tempEcuType);
					if (debug > 1) System.out.println("current attribute ecuType =" + tempEcuType);
					if ((tempEcuType != null) && (tempEcuType.trim().length() != 0) && !tempEcuType.equalsIgnoreCase("DMEDDE1")) {
						//wrong attribute ECUTYPE
						throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsCode> for DME/DDE: Atribute ecuType is invalid: " + tempEcuType);
					}
					//check the attribute encryption=
					parseCodeEnc = tempElem.getAttribute("ENCRYPTION");
					if (debug > 1) System.out.println("current attribute encryption =" + parseCodeEnc);
					if ((parseCodeEnc != null) && (parseCodeEnc.trim().length() != 0) && !parseCodeEnc.equalsIgnoreCase("STD") && !parseCodeEnc.equalsIgnoreCase("AES") && !parseCodeEnc.equalsIgnoreCase("PLAIN")) {
						//wrong attribute ECUTYPE
						throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsCode> for DME/DDE: Atribute encryption is invalid: " + parseCodeEnc);
					}
					if (parseCodeEnc.equalsIgnoreCase("AES")) {
						//AES not supported yet
						throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsCode> for DME/DDE: Atribute encryption: AES not supported yet");
					}
					if (parseCodeEnc.trim().length() == 0) {
						//no encryption-attribute
						parseCodeEnc = null;
					}
					//fetch value
					if (debug > 1) System.out.println("EWSCODE->VALUE #Childs=" + tempElem.getChildNodes().getLength());
					if (tempElem.getChildNodes().getLength() != 1) {
						//More than one child
						throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewsCode> for DME/DDE: more or less than one child for value");
					}
					tempNode = tempElem.getChildNodes().item(0);
					if (debug > 1) System.out.println("NodeName: " + tempNode.getNodeName());
					if (debug > 1) System.out.println("NodeType: " + tempNode.getNodeType());
					if (debug > 1) System.out.println("NodeValue: " + tempNode.getNodeValue());
					if (tempNode.getNodeType() != Node.TEXT_NODE) {
						//More than one child
						throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewsCode> for DME/DDE: value-node not of type '#text'");
					}
					parseCode = tempNode.getNodeValue();
					
					if (tempNodeList.getLength() == 2) {
						//we have a second <ewsCode> for EGS
						//parse ewsCode for EGS
						tempNode = tempNodeList.item(indexEgsCode);
						tempElem = (Element)tempNode;
						if (debug > 1) System.out.println("EWSCODE[EGS] NodeName: " + tempNode.getNodeName());
						if (debug > 1) System.out.println("EWSCODE[EGS] NodeType: " + tempNode.getNodeType());
						if (debug > 1) System.out.println("EWSCODE[EGS] NodeValue: " + tempNode.getNodeValue());
						if (debug > 1) System.out.println("current node tagName =" + tempElem.getTagName());
						//check the attribute ecuType=
						tempEcuType = tempElem.getAttribute("ECUTYPE");
						if (debug > 1) System.out.println("EWSCODE[EGS] ->ECUTYPE: " + tempEcuType);
						if (debug > 1) System.out.println("current attribute ecuType =" + tempEcuType);
						if (!tempEcuType.equalsIgnoreCase("EGS")) {
							//wrong attribute ECUTYPE
							throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsCode> for EGS: Atribute ecuType is invalid: " + tempEcuType);
						}
						//check the attribute encryption=
						parseEgsCodeEnc = tempElem.getAttribute("ENCRYPTION");
						if (debug > 1) System.out.println("current attribute encryption =" + parseEgsCodeEnc);
						if ((parseEgsCodeEnc != null) && (parseEgsCodeEnc.trim().length() != 0) && !parseEgsCodeEnc.equalsIgnoreCase("STD") && !parseEgsCodeEnc.equalsIgnoreCase("AES") && !parseEgsCodeEnc.equalsIgnoreCase("PLAIN")) {
							//wrong attribute ECUTYPE
							throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsCode> for EGS: Atribute encryption is invalid: " + parseEgsCodeEnc);
						}
						if (parseEgsCodeEnc.equalsIgnoreCase("AES")) {
							//AES not supported yet
							throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsCode> for EGS: Atribute encryption: AES not supported yet");
						}
						if (parseEgsCodeEnc.trim().length() == 0) {
							//no encryption-attribute
							parseEgsCodeEnc = null;
						}
						//fetch value
						if (debug > 1) System.out.println("EWSCODE->VALUE #Childs=" + tempElem.getChildNodes().getLength());
						if (tempElem.getChildNodes().getLength() != 1) {
							//More than one child
							throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewsCode> for EGS: more or less than one child for value");
						}
						tempNode = tempElem.getChildNodes().item(0);
						if (debug > 1) System.out.println("NodeName: " + tempNode.getNodeName());
						if (debug > 1) System.out.println("NodeType: " + tempNode.getNodeType());
						if (debug > 1) System.out.println("NodeValue: " + tempNode.getNodeValue());
						if (tempNode.getNodeType() != Node.TEXT_NODE) {
							//More than one child
							throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewsCode> for EGS: value-node not of type '#text'");
						}
						parseEgsCode = tempNode.getNodeValue();
					} else {
						//no code for egs
						parseEgsCode = null;
					}
					
				} else if (tempNodeList.getLength() == 0) {
					//NO element <ewsCode>
					parseCode = null;
					parseEgsCode = null;
					//throw new PPExecutionException("EWSSynchro.parseXML: Element <ewsCode> NOT occured within <ewsSynchro>");
				} else {
					//more than two elements <ewsCode>
					throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsCode> occured more than once within <ewsSynchro>");
				}
				
				
			} else {
				//we DON'T have a <ewsSynchro>
				parseInterface = null;
				parseCode = null;
				parseEgsCode = null;
			}
			if (debug > 0) {
				System.out.println("EWSSynchro.parseXML_internal_ewsinfo: Parsed ewsInterface=" + parseInterface);
				System.out.println("EWSSynchro.parseXML_internal_ewsinfo: Parsed ewsCode=" + parseCode);
				System.out.println("EWSSynchro.parseXML_internal_ewsinfo: Parsed egsCode=" + parseEgsCode);
			}			
			if (debug > 0)
				System.out .println("EWSSynchro.parseXML_internal_ewsinfo: Finished parsing XML-dataset");
		} catch (Exception x) {
			// Error while parsing XML-dataset
			String v7 = null;
			if (getPr�fling() != null) v7 = getPr�fling().getAuftrag().getFahrgestellnummer7();
			else v7 = "???????";
			System.out.println("EWSSynchro.parseXML_internal_ewsinfo: Error while parsing XML-dataset ("
				+ v7 + "E.XML" + ")");
			x.printStackTrace(System.out);
			result = new Ergebnis("Parsing DOM-File failed", "", "", "", "", "", "", "", "", "0", "", "", "",
					"Error while parsing XML-dataset " + x.getMessage(), PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
			ergs.add(result);
			throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: dataset invalid"); 
			// handled by the next outer catch in this pp
		}

		// extract and decrypt content
		// ***********************

		//check vin in dataset against vin in order
		String v7 = null, v17 = null;
		if (getPr�fling() != null) {
			//we have a Pr�fling, so we also have a vin7 and vin17
			v7 = getPr�fling().getAuftrag().getFahrgestellnummer7();
			v17 = getPr�fling().getAuftrag().getFahrgestellnummer();
			//check vin in dataset against the virtual car
			if (!v7.equalsIgnoreCase(parseVin)
					&& !v17.equalsIgnoreCase(parseVin)) {
				result = new Ergebnis("VIN", "", "", "", "", "", "", "", "", "0", "", "", "",
						"EWSSynchro VIN in dataset not VIN in virtual car", PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: VIN in dataset not VIN in virtual car");
			}
		} else {
			v7 = "???????";
			v17 = "?????????????????";
		}
		// EWS_ECUTYPE
		// storage type: String
		// example values: "EWS3","CAS2","CAS3"
		Ews_EcuType = parseECUType;
		// EWS_INTERFACE
		// storage type: String
		// example value: "EWS3", "EWS4", "EWS4EGS"
		Ews_Interface = parseInterface;
		// EWS_CODE
		// storage type: HEX
		// example value: "0123456789ABCDEF0123456789ABCDEF"
		// Random
		if (parseCode != null) {
			if (parseCode.length() != 32) {
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_ewsinfo: EWSCode in dataset is invalid");
				result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EWSCode in dataset is invalid",
						PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error ewscode: wrong length");
			}
			if (!isHex(parseCode)) {
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_ewsinfo: EWSCode in dataset is invalid");
				result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EWSCode in dataset is invalid",
						PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error ewscode: not hexadecimal");
			}
			if ((parseCodeEnc == null) || (parseCodeEnc.equalsIgnoreCase("STD"))) {
				//Standard-Encryption
				if (parseCode.length() > 32) { 
					// <ewscode> is max. 32 characters
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_ewsinfo: EWSCode in dataset is invalid");
					result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EWSCode in dataset is invalid",
							PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewscode> for DME/DDE: too long");
				}
				if (parseCode.length() < 32) { 
					// <ewscode> is min. 32 characters
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_ewsinfo: EWSCode in dataset is invalid");
					result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EWSCode in dataset is invalid",
							PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewscode> for DME/DDE: too short");
				}
				Ews_Code = decrypt(parseCode);
			} else if (parseCodeEnc.equalsIgnoreCase("PLAIN")) {
				//no Encryption
				if (parseCode.length() > 32) { 
					// <ewscode> is max. 32 characters
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_ewsinfo: EWSCode in dataset is invalid");
					result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EWSCode in dataset is invalid",
							PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewscode> for DME/DDE: too long");
				}
				if (parseCode.length() < 32) { 
					// <ewscode> is min. 32 characters
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_ewsinfo: EWSCode in dataset is invalid");
					result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EWSCode in dataset is invalid",
							PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewscode> for DME/DDE: too short");
				}
				Ews_Code = parseCode;
			} else if (parseCodeEnc.equalsIgnoreCase("AES")) {
				//AES not supported yet
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_ewsinfo: EWSCode in dataset is invalid");
				result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EWSCode in dataset is invalid",
						PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsCode> for DME/DDE: Atribute encryption: AES not supported yet");
			} else {
				//Unknown encryption
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_ewsinfo: EWSCode in dataset is invalid");
				result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EWSCode in dataset is invalid",
						PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsCode> for DME/DDE: Atribute encryption: Unknown mode");
			}
		} else {
			Ews_Code = null;
		}
		// EGS_ISN
		// storage type: HEX
		// example value: "01234567"
		// Random
		if (parseEgsCode != null) {
			if (parseEgsCode.length() != 8) {
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_ewsinfo: EGS_ISN in dataset is invalid");
				result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EGSCode in dataset is invalid",
						PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error EGS_ISN: wrong length");
			}
			if (!isHex(parseEgsCode)) {
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_ewsinfo: EGS_ISN in dataset is invalid");
				result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EGSCode in dataset is invalid",
						PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error EGS_ISN: not hexadecimal");
			}
			if ((parseEgsCodeEnc == null) || (parseEgsCodeEnc.equalsIgnoreCase("STD"))) {
				//Standard-Encryption
				if (parseEgsCode.length() > 8) { 
					// <ewscode> is max. 8 characters
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_ewsinfo: EGS_ISN in dataset is invalid");
					result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EGSCode in dataset is invalid",
							PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewscode> for EGS: too long");
				}
				if (parseEgsCode.length() < 8) { 
					// <ewscode> is min. 8 characters
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_ewsinfo: EGS_ISN in dataset is invalid");
					result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EGSCode in dataset is invalid",
							PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewscode> for EGS: too short");
				}
				Egs_Code = decrypt(parseEgsCode);
			} else if (parseEgsCodeEnc.equalsIgnoreCase("PLAIN")) {
				//no Encryption
				if (parseEgsCode.length() > 8) { 
					// <ewscode> is max. 8 characters
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_ewsinfo: EGS_ISN in dataset is invalid");
					result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EGSCode in dataset is invalid",
							PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewscode> for EGS: too long");
				}
				if (parseEgsCode.length() < 8) { 
					// <ewscode> is min. 8 characters
					if (debug > 0)
						System.out.println("EWSSynchro.parseXML_internal_ewsinfo: EGS_ISN in dataset is invalid");
					result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EGSCode in dataset is invalid",
							PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Error parsing <ewscode> for EGS: too short");
				}
				Egs_Code = parseEgsCode;
			} else if (parseEgsCodeEnc.equalsIgnoreCase("AES")) {
				//AES not supported yet
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_ewsinfo: EGS_ISN in dataset is invalid");
				result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EGSCode in dataset is invalid",
						PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsCode> for EGS: Atribute encryption: AES not supported yet");
			} else {
				//Unknown encryption
				if (debug > 0)
					System.out.println("EWSSynchro.parseXML_internal_ewsinfo: EGS_ISN in dataset is invalid");
				result = new Ergebnis("Startvalue", "", "", "", "", "", "", "", "", "0", "", "", "", "EGSCode in dataset is invalid",
						PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				throw new PPExecutionException("EWSSynchro.parseXML_internal_ewsinfo: Element <ewsCode> for EGS: Atribute encryption: Unknown mode");
			}
		} else {
			Egs_Code = null;
		}
		if (debug > 0) {
			System.out.println("EWSSynchro.parseXML_internal_ewsinfo: Parsed (decrypted) ewsCode=" + Ews_Code);
			System.out.println("EWSSynchro.parseXML_internal_ewsinfo: Parsed (decrypted) egsCode=" + Egs_Code);
		}			
		if (debug > 0)
			System.out .println("EWSSynchro.parseXML_internal_ewsinfo: Finished decryption after parsing");
	}
	
	/**
	 * Generate all values and put them into internal variables Uses the
	 * instance-variables CasType, GenNumKeys, GenNoPocketKey, GenFbdType
	 */
	private void generate() throws PPExecutionException {
		// The following instance-variables are used as input, containsSA() or
		// getBaureihe() are NOT USED!!!
		// GenDefaultCode: When true the default code for EWS4 is used

		// EWS_FORMAT_VERSION
		// storage type: PLAIN
		// EWSFormatVersions: 1/1.0 (=old), 4/4.0 (=new)
		// Ews_Format_Version
		
		// EWS_ECUTYPE
		// storage type: String
		// example values: "EWS3","CAS2","CAS3"
		// Ews_EcuType already passed as argument

		// EWS_INTERFACE
		// storage type: String
		// example value: "EWS3", "EWS4", "EWS4EGS"
		// Ews_Interface already passed as argument

		// EWS_CODE
		// storage type: HEX
		// example value: "0123456789ABCDEF0123456789ABCDEF"
		// Random
		
		// EGS_ISN
		// storage type: HEX
		// example value: "01234567"
		// Random
		if ("EWS4".equalsIgnoreCase(Ews_Interface)) {
			if (GenDefaultCode) {
				// use the default code
				Ews_Code = encrypt(DEFAULT_EWS_CODE);
			} else {
				Ews_Code = this.getRandomHex(16);
			}
			//No EGS_ISN
			Egs_Code = null;
		} else if ("EWS4EGS".equalsIgnoreCase(Ews_Interface)) {
			//New: EWS4 with EGS_ISN from L6 on
			if (GenDefaultCode) {
				// use the default code
				Ews_Code = encrypt(DEFAULT_EWS_CODE);
				Egs_Code = "000055AA";
			} else {
				Ews_Code = this.getRandomHex(16);
				Egs_Code = this.getRandomHex(4);
			}
		} else {
			// no EWS4
			Ews_Code = null;
			//No EGS_ISN
			Egs_Code = null;
		}
	}

	/**
	 * create a XML-File containing all the information from the internal
	 * variables Ews_Interface, Ews_EcuType, Ews_Code
	 */
	private void dokuDataset(Vector ergs) throws PPExecutionException {
		Ergebnis result;
		boolean newFormat = false;	//by default use old format
		if ("EWS4EGS".equalsIgnoreCase(Ews_Interface)) {
			//new Format as DEFAULT
			newFormat = true;
		} else {
			//old Format as DEFAULT
			newFormat = false;
		}

		// now all internal variables have been filled. Check if they seem to be correct.
		if (CheckDataset) {
			if (debug > 2)
				System.out.println("EWSSynchro.dokuDataset: " + ergs.size()
						+ " error(s) before checkDataset: " + ergs);
			try {
				checkDataset(ergs);
				if (debug > 2)
					System.out.println("EWSSynchro.dokuDataset: " + ergs.size()
							+ " error(s) after checkDataset: " + ergs);
			} catch (PPExecutionException ppee) {
				if (debug > 2)
					System.out.println("EWSSynchro.dokuDataset: " + ergs.size()
							+ " error(s) after checkDataset: " + ergs);
				throw ppee;
			}
		}

		// date and time of documentation
		Date rightNow = new Date();

		// *************************
		// start document locally
		// **************************
		StringBuffer dokuLocal = new StringBuffer(1000);
		dokuLocal.append(getPr�fling().getAuftrag().getFahrgestellnummer()); // append FGNR
		dokuLocal.append(";");
		dokuLocal.append((this.Ews_Interface == null) ? "null" : this.Ews_Interface.toUpperCase()); // append Ews_Interface
		dokuLocal.append(";");
		dokuLocal.append((this.Ews_EcuType == null) ? "null" : this.Ews_EcuType.toUpperCase()); // append Ews_EcuType
		dokuLocal.append(";");
		dokuLocal.append("STD:");	//Encryption
		dokuLocal.append((this.Ews_Code == null) ? "null" : encrypt(this.Ews_Code).toUpperCase()); // append Ews_Code
		dokuLocal.append(";");
		dokuLocal.append("STD:");	//Encryption
		dokuLocal.append((this.Egs_Code == null) ? "null" : encrypt(this.Egs_Code).toUpperCase()); // append Ews_Code
		dokuLocal.append(";");
		dokuLocal.append(rightNow + "\r\n"); // append date and time
		if (debug > 2)
			System.out.println("EWSSynchro.dokuDataset: LOCAL = '" + dokuLocal + "'");
		// save dataset
		String localFileName = DOKU_LOCAL_FILENAME;
		String localFilePath = DEFAULT_DOKU_LOCAL_PATH;
		boolean fileCreated;
		boolean pathCreated;
		// get the filepath from package properties
		localFilePath = PB.getString(DOKU_LOCAL_PATH_PROPERTY); 
		// is the property present
		if ((localFilePath == null)
				|| (DOKU_LOCAL_PATH_PROPERTY.equals(localFilePath))) { 
			localFilePath = DEFAULT_DOKU_LOCAL_PATH;
			result = new Ergebnis("ExecFehler", "LOCAL_DOCU", "", "", "", DOKU_LOCAL_PATH_PROPERTY, "null", "", "", "0", "", "", "",
					"Warning: Path for Local Documentation not defined", "Local path not defined", Ergebnis.FT_IGNORE);
			ergs.add(result);
		}
		File localPath = new File(localFilePath);
		File localFile = new File(localPath, localFileName);
		try {
			// If not exists create the directory for the Cas-dat files
			pathCreated = localPath.mkdirs(); 
			// If not exists create the ewssync.dat file
			fileCreated = localFile.createNewFile(); 
			if (debug > 2)
				System.out.println("EWSSynchro.dokuDataset: Doku local: pathCreated=" + pathCreated 
								+ " fileCreated=" + fileCreated);

			/* Write the dataset */
			if (debug > 2) {
				// Fetch the absolute path including filename
				String localFileNameAbsolut = localFile.getAbsolutePath(); 
				System.out.println("EWSSynchro.dokuDataset: Doku local: filename: " + localFileNameAbsolut); // Debug-message
			}
			FileWriter out = new FileWriter(localFile, true); 
			// FileWriter for ewssync.dat (appendmode)
			out.write(dokuLocal.toString()); // write buffer to ewssync.dat
			out.flush(); // flush the buffer
			out.close(); // close the file
			if (debug > 1)
				System.out.println("EWSSynchro.dokuDataset: LOCAL succesfully saved");

			/*
			 * Check if the file has reached the maximum limit and partitionmode
			 * is enabled
			 */
			long fileSize = localFile.length(); // current filesize
			// is partitionmode on and the file too big
			if ((DokuPartition > 0) && (fileSize > DokuPartition)) { 
				// construct the new filename
				StringBuffer destname = new StringBuffer(DOKU_LOCAL_PREFIX); 
				// current day and year
				Calendar currDate = Calendar.getInstance(); 
				StringBuffer expand = null; // temp buffer
				// fetch year
				expand = new StringBuffer("" + currDate.get(Calendar.YEAR)); 
				while (expand.length() < 4)
					expand.insert(0, '0'); // expand to length 4
				destname.append(expand); // append year
				expand = new StringBuffer("" + (currDate.get(Calendar.MONTH) - Calendar.JANUARY + 1)); // fetch month
				while (expand.length() < 2)
					expand.insert(0, '0'); // expand to length 2
				destname.append(expand); // append month
				expand = new StringBuffer("" + currDate.get(Calendar.DAY_OF_MONTH)); // fetch day
				while (expand.length() < 2)
					expand.insert(0, '0'); // expand to length 2
				destname.append(expand); // append day
				expand = new StringBuffer("" + currDate.get(Calendar.HOUR_OF_DAY)); // fetch hour
				while (expand.length() < 2)
					expand.insert(0, '0'); // expand to length 2
				destname.append('_'); // append '_'
				destname.append(expand); // append hour
				expand = new StringBuffer("" + currDate.get(Calendar.MINUTE)); // fetch minute
				while (expand.length() < 2)
					expand.insert(0, '0'); // expand to length 2
				destname.append('_'); // append '_'
				destname.append(expand); // append minute
                destname.append("_A"); // append '_A' (Dummy Client Name)
				destname.append('.'); // append '.'
				destname.append(DOKU_LOCAL_EXTENSION); // append extension
				File dest = new File(localFile.getParentFile(), destname.toString()); // destination file
				if (!dest.exists()) { 
					// ONLY move if file does not already exist rename orig ewssync.dat to 20020805_13_30.dat
					localFile.renameTo(dest); // rename ewssync.dat
					System.out.println("EWSSynchro.dokuDataset: moved " + localFile + " to " + dest);
				}
			}

		} catch (Exception e) {
			result = new Ergebnis("ExecFehler", "LOCAL_DOCU", "", "", "", "", "", "", "", "0", "", "", "",
					"Warning: unable to save local Documentation", "Local documentation failed " + e.getMessage(), Ergebnis.FT_NIO_SYS);
			ergs.add(result);
			if (debug > 0)
				System.out.println("EWSSynchro.dokuDataset: error saving LOCAL ");
			e.printStackTrace(System.out);
			throw new PPExecutionException("EWSSynchro.dokuDataset: Local documentation of dataset failed");
		}
		result = new Ergebnis("ExecFehler", "LOCAL_DOCU", "", "", "", "", "", "", "", "0", "", "", "", "Local Documentation", "", Ergebnis.FT_IO);
		ergs.add(result);

		// *************************
		// start document to xml
		// *************************
		/*
		 * EXAMPLE <?xml version="1.0" encoding="iso-8859-1" ?> <ewsinfo
		 * vin="WBAEE111119046000"> <ewsecu> <ewsecutype>CAS3</ewsecutype>
		 * </ewsecu> <ewssynchro> <ewsecuinterface>EWS4</ewsecuinterface>
		 * <ewscode>0123456789ABCDEF0123456789ABCDEF</ewscode> </ewssynchro>
		 * </ewsinfo>
		 */
		if (debug > 0)
			System.out.println("EWSSynchro.dokuDataset: creating XML");
		StringBuffer xml = new StringBuffer();
		//check vin in dataset against vin in order
		String v17 = null;
		if (getPr�fling() != null) {
			//we have a Pr�fling, so we also have a vin7 and vin17
			v17 = getPr�fling().getAuftrag().getFahrgestellnummer();
		} else {
			v17 = "?????????????????";
		}
		if (!newFormat) {
			// create xml-file old format
			xml.append("<?xml version=\"1.0\"?>\r\n");
			xml.append("<!DOCTYPE EWSInfo SYSTEM \"file:///C:/ec-apps/cascade/dtd/EWSInfo.dtd\">\r\n");
			xml.append("<ewsinfo vin=\""
							+ v17 + "\">\r\n");
			if (Ews_EcuType != null) {
				xml.append("\t<ewsecu>\r\n");
				xml.append("\t\t<ewsecutype>" + Ews_EcuType.toUpperCase() + "</ewsecutype>\r\n");
				xml.append("\t</ewsecu>\r\n");
			}
			if ((Ews_Interface != null) || (Ews_Code != null)) {
				xml.append("\t<ewssynchro>\r\n");
				if (Ews_Interface != null) {
					xml.append("\t\t<ewsinterface>" + Ews_Interface.toUpperCase() + "</ewsinterface>\r\n");
				}
				if (Ews_Code != null) {
					xml.append("\t\t<ewscode>" + encrypt(Ews_Code).toUpperCase() + "</ewscode>\r\n");
				}
				xml.append("\t</ewssynchro>\r\n");
			}
			xml.append("</ewsinfo>\r\n");
		} else {
			// create xml-file new format
			xml.append("<?xml version=\"1.0\"  encoding=\"iso-8859-1\"?>\r\n");
			//vehicleImmobilizer
			xml.append("<vehicleImmobilizer xmlns=\"http://bmw.com/immobiliser\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://bmw.com/immobiliser ../vehicleImmobilizerV01_01.xsd\" version=\"01.01\" refSchema=\"vehicleImmobilizerV01_01.xsd\">\r\n");
			//vehicle
			xml.append("\t<vehicle vin=\"" + v17 + "\">\r\n");
			//coding
			xml.append("\t\t<coding>\r\n");
			//synchronisationInfo
			xml.append("\t\t\t<synchronisationInfo version=\"01.03\" refSchema=\"synchronisationInfoV0103.xsd\" ");
			//creationDateTime example 2007-01-30T08:33:58
			xml.append("creationDateTime=\"");
			DateFormat myDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss"); 
			String df = myDateFormat.format(rightNow);
			if (debug > 1) System.out.println("EWSSynchro.dokuDataset Datum: " + df);
			xml.append( df );
			xml.append("\">\r\n");		//add "> + linebreak
			//immobiliserEcu
			xml.append("\t\t\t\t<immobiliserEcu ecuType=\"");
			xml.append(Ews_EcuType.toUpperCase());
			xml.append("\"/>\r\n");		//add "/> + linebreak
			//interface
			xml.append("\t\t\t\t<interface interfaceType=\"");
			xml.append(Ews_Interface.toUpperCase());
			xml.append("\"/>\r\n");		//add "/> + linebreak
			//synchronisedEcus
			xml.append("\t\t\t\t<synchronisedEcus>\r\n");
			
			//synchronisedEcu for DME/DDE
			xml.append("\t\t\t\t\t<synchronisedEcu ecuType=\"DMEDDE1\">\r\n");
			//syncCode
			xml.append( "\t\t\t\t\t\t<syncCode encryption=\"" );	//encryption algorithm used
			xml.append( EncAlgorithm.toUpperCase() );
			xml.append("\">");
			if ((EncAlgorithm == null) || "STD".equalsIgnoreCase(EncAlgorithm)) {
				xml.append( encrypt(Ews_Code).toUpperCase() );	//encrypted code
			} else if ("PLAIN".equalsIgnoreCase(EncAlgorithm)) {
				xml.append( Ews_Code.toUpperCase() );	//encrypted code
			} else if ("AES".equalsIgnoreCase(EncAlgorithm)) {
				//AES not supported yet
				result = new Ergebnis("ExecFehler", "DomTransfer", "", "", "", "", "", "", "", "0", "", "", "", "FileWrite", "EncryptionAlgorithm AES not Supported yet", Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.dokuDataset: EncryptionAlgorithm AES not Supported yet");
				throw new PPExecutionException();
			} else {
				//UNKNOWN algorithm
				result = new Ergebnis("ExecFehler", "DomTransfer", "", "", "", "", "", "", "", "0", "", "", "", "FileWrite", "EncryptionAlgorithm unknown: " + EncAlgorithm, Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.dokuDataset: EncryptionAlgorithm unknown: " + EncAlgorithm);
				throw new PPExecutionException();
			}
			//END syncCode
			xml.append( "</syncCode>\r\n" );
			//END synchronisedEcu for DME/DDE
			xml.append("\t\t\t\t\t</synchronisedEcu>\r\n");
			
			if (Egs_Code != null) {
				//synchronisedEcu for DME/DDE
				xml.append("\t\t\t\t\t<synchronisedEcu ecuType=\"EGS\">\r\n");
				//syncCode
				xml.append( "\t\t\t\t\t\t<syncCode encryption=\"" );	//encryption algorithm used
				xml.append( EncAlgorithm.toUpperCase() );
				xml.append("\">");
				if ((EncAlgorithm == null) || "STD".equalsIgnoreCase(EncAlgorithm)) {
					xml.append( encrypt(Egs_Code).toUpperCase() );	//encrypted code
				} else if ("PLAIN".equalsIgnoreCase(EncAlgorithm)) {
					xml.append( Egs_Code.toUpperCase() );	//encrypted code
				} else if ("AES".equalsIgnoreCase(EncAlgorithm)) {
					//AES not supported yet
					result = new Ergebnis("ExecFehler", "DomTransfer", "", "", "", "", "", "", "", "0", "", "", "", "FileWrite", "EncryptionAlgorithm AES not Supported yet", Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					if (debug > 0)
						System.out.println("EWSSynchro.dokuDataset: EncryptionAlgorithm AES not Supported yet");
					throw new PPExecutionException();
				} else {
					//UNKNOWN algorithm
					result = new Ergebnis("ExecFehler", "DomTransfer", "", "", "", "", "", "", "", "0", "", "", "", "FileWrite", "EncryptionAlgorithm unknown: " + EncAlgorithm, Ergebnis.FT_NIO_SYS);
					ergs.add(result);
					if (debug > 0)
						System.out.println("EWSSynchro.dokuDataset: EncryptionAlgorithm unknown: " + EncAlgorithm);
					throw new PPExecutionException();
				}
				//END syncCode
				xml.append( "</syncCode>\r\n" );
				//END synchronisedEcu for EGS
				xml.append("\t\t\t\t\t</synchronisedEcu>\r\n");
			} //end ewsCode for EGS
			
			//END synchronisedEcus
			xml.append("\t\t\t\t</synchronisedEcus>\r\n");			
			//END synchronisationInfo
			xml.append("\t\t\t</synchronisationInfo>\r\n");
			//END coding
			xml.append("\t\t</coding>\r\n");
			//END vehicle
			xml.append("\t</vehicle>\r\n");
			//END vehicleImmobilizer
			xml.append("</vehicleImmobilizer>\r\n");
		}
		if (debug > 2)
			System.out.println("EWSSynchro.dokuDataset: XML = '" + xml + "'");
		if (DokuToDom) {
			try {
				DomPruefstand.writeDomData(getPr�fling().getAuftrag()
						.getFahrgestellnummer7(), "E", xml.toString().getBytes());
				if (debug > 0)
					System.out.println("EWSSynchro.dokuDataset: XML succesfully saved");
			} catch (Exception e) {
				result = new Ergebnis("ExecFehler", "DomTransfer", "", "", "", "", "", "", "", "0", "", "", "", "FileWrite", ""
								+ e.getMessage(), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.dokuDataset: error saving XML ");
				e.printStackTrace(System.out);
				throw new PPExecutionException();
			}
			result = new Ergebnis("EWSSynchro", "DomTransfer", "", "", "", "", "", "", "", "0", "", "", "", "FileWrite", "", Ergebnis.FT_IO);
			ergs.add(result);
		}
		if (DokuToPath) {
			// *************************
			// Doku to Path
			// *************************
			if (DokuPath == null) {
				Object pr_var;
				try {
					pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PS_CONFIG, "CAS_DOKU_PATH");
				} catch (VariablesException e1) {
					pr_var = null;
				}
				if (pr_var != null) {
					if (pr_var instanceof String) {
						DokuPath = (String) pr_var;
					}
				} else {
					System.out.println("WARNING: EWSSynchro.dokuDataset: DOKU_PATH not present AND PS-Variable CAS_DOKU_PATH not defined");
				}
			}

			try {
				// fetch doku path from cas.properties
				if (DokuPath == null)
					DokuPath = ResourceBundle.getBundle("com.bmw.cascade.cas").getString("casDokuPath");
			} catch (Exception e) {
				if (debug > 0)
					System.out.println("EWSSynchro.dokuDataset: Doku_Path not found in cas.properties");
				DokuPath = null;
			}
			if (DokuPath == null) {
				// Use default-path from package properties
				DokuPath = PB.getString("casDomServerPath");
				if ((DokuPath == null) && (debug > 0))
					System.out.println("EWSSynchro.dokuDataset: Doku_Path not found in package.properties");
			}
			if (DokuPath == null) {
				// use hard coded default
				DokuPath = "E:/cas_dom/";
				if (debug > 0)
					System.out.println("EWSSynchro.dokuDataset: Doku_Path not found using default " + DokuPath);
			}
			DokuPath.replace('\\', File.separatorChar);
			DokuPath.replace('/', File.separatorChar);
			if (!DokuPath.endsWith("" + File.separatorChar))
				DokuPath += File.separatorChar;
			if (debug > 0)
				System.out.println("EWSSynchro.dokuDataset: saving XML to path " + DokuPath);
			try {
				File myPath = new File(DokuPath);
				myPath.mkdirs();

				File myFile = new File(DokuPath + getPr�fling().getAuftrag().getFahrgestellnummer7() + "E.XML");
				FileOutputStream fos = new FileOutputStream(myFile, false);
				fos.write(xml.toString().getBytes());
				fos.close();
			} catch (Exception e) {
				result = new Ergebnis("ExecError", "DomTransfer", "", "", "", "", "", "", "", "0", "", "", "", "Writing dataset to "
								+ DokuPath + " failed", "" + e.getMessage(), Ergebnis.FT_NIO_SYS);
				ergs.add(result);
				if (debug > 0)
					System.out.println("CASPlusData.dokuDataset: error saving XML to path");
				if (debug > 0)
					e.printStackTrace(System.out);
				throw new PPExecutionException("Writing dataset failed");
			}
			result = new Ergebnis("Exececution", "PathTransfer", "", "", "", "", "", "", "", "0", "", "", "", "Writing dataset IO", "", Ergebnis.FT_IO);
			ergs.add(result);
		}

	}

	/**
	 * Check all values in the internal variables. Note: A import or readxml
	 * must be done before. Ews_Interface, Ews_EcuType, Ews_Code are checked The
	 * VIN is checked directly in readXML
	 */
	private void checkDataset(Vector errors) throws PPExecutionException {
		// method variables
		int i; // for loops
		boolean ok;
		Ergebnis result;

		// EWS_ECUTYPE
		// storage type: String
		// example values: "EWS3","CAS2","CAS3"
		if (Ews_EcuType != null) {
			ok = false;
			for (i = 0; i < EWSECUTypes.length; i++) { // Loop over all
				// available EWSECUTypes
				// Is the current Argumentvalue equal to the EWSECUType
				if (Ews_EcuType.equalsIgnoreCase(EWSECUTypes[i])) { 
					// EWSECUType has been found in available EWSECUTypes
					ok = true; 
				}
			}
			if (!ok) { 
				// current Argumentvalue is not contained in available EwsEcuTypes
				result = new Ergebnis("ExecFehler", "Checkdataset", "", "", "", "Ews_EcuType", Ews_EcuType, "", "", "0", "", "", "", PB.getString("unerwarteterLaufzeitfehler"), "Unknown Ews_EcuType", Ergebnis.FT_NIO_SYS);
				errors.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.checkDataset: Ergebnis " + result);
				throw new PPExecutionException("EWSSynchro.checkDataset: Ews_EcuType = '" + Ews_EcuType + "' unknown");
			}
		}
		// EWS_INTERFACE
		// storage type: String
		// example value: "EWS3", "EWS4", "EWS4EGS"
		if (Ews_Interface != null) {
			ok = false;
			// Loop over all available EWSInterfaces
			for (i = 0; i < EWSInterfaces.length; i++) { 
				// Is the current Argumentvalue equal to the EWSInterface
				if (Ews_Interface.equalsIgnoreCase(EWSInterfaces[i])) { 
					// EWSInterface has been found in available EWSInterfaces
					ok = true; 
				}
			}
			if (!ok) { 
				// current Argumentvalue is not contained in available EWSInterfaces
				result = new Ergebnis("ExecFehler", "Checkdataset", "", "", "", "Ews_Interface", Ews_Interface, "", "", "0", "", "",
						"", PB.getString("unerwarteterLaufzeitfehler"), "Unknown Ews_Interface", Ergebnis.FT_NIO_SYS);
				errors.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.checkDataset: Ergebnis " + result);
				throw new PPExecutionException("EWSSynchro.checkDataset: Ews_Interface = '" + Ews_Interface + "' unknown");
			}
		}
		// EWS_CODE
		// storage type: HEX
		// example value: "0123456789ABCDEF0123456789ABCDEF"
		// Random
		if (Ews_Code != null) {
			if (Ews_Code.length() != 32) {
				result = new Ergebnis("ExecFehler", "Checkdataset", "", "", "", "Ews_Code", Ews_Code, "", "", "0", "", "", "", PB.getString("unerwarteterLaufzeitfehler"),
						"Ews_Code lllegal length", Ergebnis.FT_NIO_SYS);
				errors.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.checkDataset: Ergebnis " + result);
				throw new PPExecutionException("EWSSynchro.checkDataset: Ews_Code = '" + Ews_Code + "' lllegal length");
			}
			if (!isHex(Ews_Code)) {
				result = new Ergebnis("ExecFehler", "Checkdataset", "", "", "", "Ews_Code", Ews_Code, "", "", "0", "", "", "", PB.getString("unerwarteterLaufzeitfehler"),
						"Ews_Code not hexadecimal", Ergebnis.FT_NIO_SYS);
				errors.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.checkDataset: Ergebnis " + result);
				throw new PPExecutionException("EWSSynchro.checkDataset: Ews_Code = '" + Ews_Code + "' not hexadecimal");
			}
		}
		// EGS_ISN
		// storage type: HEX
		// example value: "01234567"
		// Random
		if (Egs_Code != null) {
			if (Egs_Code.length() != 8) {
				result = new Ergebnis("ExecFehler", "Checkdataset", "", "", "", "Egs_Code", Egs_Code, "", "", "0", "", "", "", PB.getString("unerwarteterLaufzeitfehler"),
						"Egs_Code lllegal length", Ergebnis.FT_NIO_SYS);
				errors.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.checkDataset: Ergebnis " + result);
				throw new PPExecutionException("EWSSynchro.checkDataset: Egs_Code = '" + Egs_Code + "' lllegal length");
			}
			if (!isHex(Egs_Code)) {
				result = new Ergebnis("ExecFehler", "Checkdataset", "", "", "", "Egs_Code", Egs_Code, "", "", "0", "", "", "", PB.getString("unerwarteterLaufzeitfehler"),
						"Egs_Code not hexadecimal", Ergebnis.FT_NIO_SYS);
				errors.add(result);
				if (debug > 0)
					System.out.println("EWSSynchro.checkDataset: Ergebnis " + result);
				throw new PPExecutionException("EWSSynchro.checkDataset: Egs_Code = '" + Egs_Code + "' not hexadecimal");
			}
		}

		// check values against each other

		// EWS4 without Code???
		if (("EWS4".equalsIgnoreCase(Ews_Interface) || "EWS4EGS".equalsIgnoreCase(Ews_Interface)) && (Ews_Code == null)) {
			result = new Ergebnis("ExecFehler", "Checkdataset", "", "", "", "EWS4-Ews_Code", "null", "", "", "0", "", "", "", PB.getString("unerwarteterLaufzeitfehler"),
					"Ews_Code for EWS4 missing", Ergebnis.FT_NIO_SYS);
			errors.add(result);
			if (debug > 0)
				System.out.println("EWSSynchro.checkDataset: Ergebnis " + result);
			throw new PPExecutionException("EWSSynchro.checkDataset: Ews_Code = '" + Ews_Code + "' for EWS4 missing");
		}
		// EWS4EGS without Egs_ISN???
		if ("EWS4EGS".equalsIgnoreCase(Ews_Interface) && (Egs_Code == null)) {
			result = new Ergebnis("ExecFehler", "Checkdataset", "", "", "", "EWS4-EGS_ISN", "null", "", "", "0", "", "", "", PB.getString("unerwarteterLaufzeitfehler"),
					"Egs_ISN is missing", Ergebnis.FT_NIO_SYS);
			errors.add(result);
			if (debug > 0)
				System.out.println("EWSSynchro.checkDataset: Ergebnis " + result);
			throw new PPExecutionException("EWSSynchro.checkDataset: Egs_Isn = '" + Egs_Code + "' is missing");
		}
		// Code with EWS3???
		if ("EWS3".equalsIgnoreCase(Ews_Interface) && (Ews_Code != null)) {
			result = new Ergebnis("ExecFehler", "Checkdataset", "", "", "", "EWS3-Ews_Code", Ews_Code, "", "", "0", "", "", "", PB.getString("unerwarteterLaufzeitfehler"),
					"Ews_Code for EWS3 not allowed", Ergebnis.FT_NIO_SYS);
			errors.add(result);
			if (debug > 0)
				System.out.println("EWSSynchro.checkDataset: Ergebnis " + result);
			throw new PPExecutionException("EWSSynchro.checkDataset: Ews_Code = '" + Ews_Code + "' for EWS3 not allowed");
		}

	}

	/**
	 * Clear all values and put them into internal variables
	 */
	private void clear() throws PPExecutionException {

		// EWS_ECUTYPE
		// storage type: String
		// example values: "EWS3","CAS2","CAS3"
		// Ews_EcuType not secret

		// EWS_INTERFACE
		// storage type: String
		// example value: "EWS3", "EWS4", "EWS4EGS"
		// Ews_Interface not secret

		// EWS_CODE
		// storage type: HEX
		// example value: "0123456789ABCDEF0123456789ABCDEF"
		// Random
		Ews_Code = "SECRET VALUE";
		Egs_Code = "SECRET VALUE";
	}

	// Other Utility Methods
	// *******************************************

	/**
	 * Returns true if the Vector input contains results (Ergebnis) that are NIO
	 * or NIO_SYS
	 */
	private static boolean containsError(Vector input) {
		Iterator iter = input.iterator();
		while (iter.hasNext()) {
			Object co = iter.next();
			if (co instanceof Ergebnis) {
				if (((Ergebnis) co).getFehlerTyp().equalsIgnoreCase(Ergebnis.FT_NIO)
						|| ((Ergebnis) co).getFehlerTyp().equalsIgnoreCase(Ergebnis.FT_NIO_SYS))
					return true; // error detected
			}
		}
		return false; // by default no error
	}

	/**
	 * Sets all NIO- and NIOSYS-Results in the input-vector to Ignore.
	 */
	private static void filterErrors(Vector input) {
		Iterator iter = input.iterator();
		while (iter.hasNext()) {
			Object co = iter.next();
			if (co instanceof Ergebnis) {
				if (((Ergebnis) co).getFehlerTyp().equalsIgnoreCase(Ergebnis.FT_NIO)
						|| ((Ergebnis) co).getFehlerTyp().equalsIgnoreCase(Ergebnis.FT_NIO_SYS)) {
					// error detected
					// Set to ignore (F and S become A)
					((Ergebnis) co).setFehlerTyp(Ergebnis.FT_IGNORE); 
				}
			}
		}
	}

	/**
	 * Prints all contained Errors in short form
	 */
	private static void printResults(Vector input) {
		Iterator iter = input.iterator();
		int i = 1;
		while (iter.hasNext()) {
			Object co = iter.next();
			if (co instanceof Ergebnis) {
				Ergebnis erg = (Ergebnis) co;
				if (((Ergebnis) co).getFehlerTyp().equalsIgnoreCase(Ergebnis.FT_NIO)
						|| ((Ergebnis) co).getFehlerTyp().equalsIgnoreCase(Ergebnis.FT_NIO_SYS)) {
					System.out.println("ERGEBNIS" + i + ": NIO ID='"
							+ erg.getID() + "' TYP='" + erg.getFehlerTyp() + "'");
				} else {
					System.out.println("ERGEBNIS" + i + ": IO ID='"
							+ erg.getID() + "' TYP='" + erg.getFehlerTyp() + "'");
				}
			} else {
				System.out.println("ERGEBNIS" + i + ": UNKNOWN CLASS " + co);
			}
			i++;
		}
	}

	/**
	 * Method for encrypting a string. Used before writing it to the xml-file.
	 * the input must be a string containing hexadecimal string e.g. "12AB"
	 * 
	 * @param inputString
	 *            The string to encrypt.
	 * @return The encrypted string.
	 */
	private static String encrypt(String inputString)
			throws PPExecutionException {

		// gerade Anzahl Zeichen
		if ((inputString.length() % 2) != 0) {
			throw new PPExecutionException("EWSSynchro.encrypt: Format error");
		}
		String reverseString = (new StringBuffer(inputString)).reverse().toString();
		StringBuffer res = new StringBuffer();

		for (int j = 0; j < (reverseString.length()); j = j + 2) {
			try {
				int num1 = Integer.parseInt(reverseString.substring(j, j + 2), 16); // Zwei Hex-Zeichen in int wandeln
				int num2 = 211; // 211;
				String tempTwoChar = Integer.toHexString(num1 ^ num2).toUpperCase(); // XOR + toUpperCase
				// Auf L�nge zwei auff�llen
				if (tempTwoChar.length() == 1) {
					tempTwoChar = "0" + tempTwoChar;
				} 
				res.append(tempTwoChar); // Back to Hex
			} catch (Exception e) {
				throw new PPExecutionException("EWSSynchro.encrypt: Format error");
			}
		}
		return res.toString();

	}

	/**
	 * Method for decrypting a string. Used after reading it from the xml-file.
	 * the input must be a string containing hexadecimal string e.g. "12AB"
	 * 
	 * @param inputString
	 *            The string to decrypt.
	 * @return The decrypted string.
	 */
	private static String decrypt(String inputString)
			throws PPExecutionException {

		// gerade Anzahl Zeichen
		if ((inputString.length() % 2) != 0) {
			throw new PPExecutionException("EWSSynchro.decrypt: Format error");
		}
		StringBuffer res = new StringBuffer();
		for (int j = 0; j < (inputString.length()); j = j + 2) {
			try {
				// Zwei Hex-Zeichen in int wandeln
				int num1 = Integer.parseInt(inputString.substring(j, j + 2), 16); 
				int num2 = 211; // 211;
				String tempTwoChar = Integer.toHexString(num1 ^ num2).toUpperCase(); // XOR + toUpperCase
				// Auf L�nge zwei auff�llen
				if (tempTwoChar.length() == 1) {
					tempTwoChar = "0" + tempTwoChar;
				} 
				res.append(tempTwoChar); // Back to Hex
			} catch (Exception e) {
				throw new PPExecutionException("EWSSynchro.decrypt: Format error");
			}
		}
		return res.reverse().toString();

	}

	/**
	 * Returns true if the String only contains hexadecimal characters
	 */
	private static boolean isHex(String input) {
		char c;
		int j;
		for (j = 0; j < input.length(); j++) {
			c = input.charAt(j);
			if ((!Character.isDigit(c)) && // keine Zahl
					!((c >= 'a') && (c <= 'f')) && // nicht zwischen a und f
					!((c >= 'A') && (c <= 'F'))) { // nicht zwischen A und F
				return false;
			}
		}
		return true;
	}

	/**
	 * Liefert true zur�ck, wenn deutsche Texte verwendet werden sollen.
	 * 
	 * @return True bei deutsche texte, false sonst.
	 */
	private static boolean isDE() {
		try {
			if (CascadeProperties.getLanguage().equalsIgnoreCase("DE") == true)
				return true;
			else
				return false;
		} catch (Exception e) {
			return false; // default is english
		}
	}

	// Methods for generating random numbers
	// *******************************************

	/**
	 * Initialize random-generator rand
	 */
	private void initRandom() {
		long steuerSchlInt = 0; // Zahlenanteil des Steuerschl�ssels
		long fgnrInt = 0; // Zahlenanteil der Fahrgestellnummer
		long base = 0; // Zahlenbasis f�r neues Random
		StringBuffer buff = null;
		String temp;

		// getSteuerschl�ssel() and filter all non-numeric characters
		temp = getPr�fling().getAuftrag().getSteuerschl�ssel().trim(); // input for numberfilter
		buff = new StringBuffer(); // output for numberfilter
		// loop over all characters
		for (int k = 0; k < temp.length(); k++) {
			if (Character.isDigit(temp.charAt(k))) {
				// reverse order of filtered characters
				buff.insert(0, temp.charAt(k)); 
			}
		}
		if (buff.length() == 0)
			buff.append("67"); // string is not empty parse the String
		try {
			steuerSchlInt = System.currentTimeMillis()
					+ Long.parseLong(buff.toString()); // convert to integer + Systime
		} catch (Exception e) { // Exception abfangen
			steuerSchlInt = System.currentTimeMillis(); // default Systime
		}

		// random pause
		try {
			wait(10);
		} catch (Exception x) {
		} // wait max 10ms

		// getFahrgestellnummer7() and filter all non-numeric characters
		temp = getPr�fling().getAuftrag().getFahrgestellnummer7().trim(); // input for numberfilter
		buff = new StringBuffer(); // output for numberfilter
		// loop over all characters
		for (int k = 0; k < temp.length(); k++) {
			if (Character.isDigit(temp.charAt(k))) {
				// reverse order of filtered characters
				buff.insert(0, temp.charAt(k)); 
			}
		}
		if (buff.length() == 0)
			buff.append("453"); // string is not empty
		// parse the String
		try {
			fgnrInt = System.currentTimeMillis()
					+ Long.parseLong(buff.toString()); // convert to integer + Systime
		} catch (Exception e) { // Exception abfangen
			fgnrInt = System.currentTimeMillis(); // default Systime
		}

		// random pause
		try {
			wait(10);
		} catch (Exception x) {
		} // wait max 10ms

		// loop to create the random-generators
		for (int i = 0; i < NUM_RANDS; i++) {
			base = System.currentTimeMillis(); // Systime
			switch (i) {
			case 0:
				base = base + steuerSchlInt; // Systime + steuerschlInt
				break;
			case 1:
				base = base + fgnrInt + rands[0].nextInt(10000); // Systime + fgnrInt + rands[0].nextInt()
			default:
				try {
					wait(20);
				} catch (Exception x) {
				} // wait max 20ms
				base = base + System.currentTimeMillis()
						+ rands[i - 1].nextInt(1000 * i); // Systime + Systime + rands[i-1].nextInt()
			}
			rands[i] = new Random(base); // Zufallsgenerator Initialisieren
		}
	}

	/**
	 * Generate a random number between 0 and 254 (1 <= x <= 254)
	 * 
	 * @return the generated random number
	 */
	private int getRandomByte() {
		lastUsedRandom = (lastUsedRandom + 1) % NUM_RANDS;
		return (1 + rands[lastUsedRandom].nextInt(254)); // 1 - 254
	}

	/**
	 * Generate a random hex-string representing numBytes Bytes (length = 2 *
	 * numBytes)
	 * 
	 * @param numBytes
	 *            length in bytes
	 * @return the generated hex-string
	 */
	private String getRandomHex(int numBytes) {
		// clear the result-buffer
		if (randBufferRes.length() > 0)
			randBufferRes.delete(0, randBufferRes.length());
		// variable for counting the generated number of bytes
		int numBytesDone = 0;
		// clear the buffer for the hex-representation of a single byte
		if (randBuffer2.length() > 0)
			randBuffer2.delete(0, randBuffer2.length());

		while (numBytesDone < numBytes) {
			// clear the buffer for the hex-representation of a single byte
			if (randBuffer2.length() > 0)
				randBuffer2.delete(0, randBuffer2.length());
			// put the hex-representation of a byte into randBuffer2
			randBuffer2.append(Integer.toHexString(getRandomByte())); // 0x01 - 0xFE extend the length to two chars (1 <= x <= 9) e.g. '3'->'03'
			if (randBuffer2.length() < 2)
				randBuffer2.insert(0, '0');
			// append the hex-byte to the result
			randBufferRes.append(randBuffer2);
			// count up the number of generated bytes
			numBytesDone++;
		}
		return randBufferRes.toString().toUpperCase();
	}

	/**
	 * Assigns a given value to a specified dynamic attribute
	 * 
	 * @param label
	 *            the name of the dynamic attribute
	 * @param value
	 *            the value of the dynamic attribute
	 */
	private void setDynamicAttribute(String label, String value)
			throws PPExecutionException {
		setDynamicAttribute(label, value, Storage);
	}

	/**
	 * Assigns a given value to a specified dynamic attribute
	 * 
	 * @param label
	 *            the name of the dynamic attribute
	 * @param value
	 *            the value of the dynamic attribute
	 * @param storage defines the PL that will be used for storing dynamic attributes
	 */
	public void setDynamicAttribute(String label, String value, String storage)
			throws PPExecutionException {
		if (debug > 1)
			System.out.println("EWSSynchro: setDynamicAttribute(" + label + "," + value + "," + storage + ")");
		Pruefling pl; // reference to the storing PL
		if (storage != null) {
			// get the reference to the storing PL
			pl = getPr�fling().getAuftrag().getPr�fling(storage); 
		} else {
			// get the reference to the storing PL (use current PL)
			pl = getPr�fling(); 
		}
		if (pl == null) {
			// throw an exeception if storing PL is null
			if (debug > 1)
				System.out.println("EWSSynchro.setDynamicAttribute: Storage-PL not found");
			throw new PPExecutionException("EWSSynchro.setDynamicAttribute: Storage-PL not found");
		}
		// get the hashtable of the PL where the attributes are stored
		Map ht = pl.getAllAttributes();
		if (ht == null) {
			// thow an exception if ht is null
			if (debug > 1)
				System.out.println("EWSSynchro.setDynamicAttribute: Hashtable not found");
			throw new PPExecutionException("EWSSynchro.setDynamicAttribute: Hashtable not found");
		}
		ht.put(label, value); // put the pair (label/value) to the hashtable
	}

	/**
	 * Assigns an array of values to dynamic attributes with the name
	 * (labelPrefix + index)
	 * 
	 * @param labelPrefix
	 *            the common prefix for the names of the dynamic attributes
	 * @param valueArray
	 *            the array of values
	 * @param beginIndex
	 *            the first index that will be used for getting the value from
	 *            the array
	 * @param numEntries
	 *            the number of values that will be taken from the array
	 * @param storage
	 *            defines the PP that will be used for storing dynamic
	 *            attributes
	 */
	public void setDynamicAttributes(String labelPrefix, String[] valueArray,
			int beginIndex, int numEntries, String storage)
			throws PPExecutionException {
		int i; // counter for index
		// loop over all indices
		for (i = beginIndex; i < (beginIndex + numEntries); i++) {
			setDynamicAttribute(labelPrefix + i, valueArray[i], storage);
		}
	}

	/**
	 * Returns the value of a specified dynamic attribute
	 * 
	 * @param label
	 *            the name of the dynamic attribute
	 * @return value of the dynamic attribute or null
	 */
	private String getDynamicAttribute(String label)
			throws PPExecutionException {
		return getDynamicAttribute(label, Storage);
	}

	/**
	 * Returns the value of a specified dynamic attribute
	 * 
	 * @param label
	 *            the name of the dynamic attribute
	 * @param storage
	 *            defines the PL that will be used for storing dynamic
	 *            attributes
	 * @return value of the dynamic attribute or null
	 */
	public String getDynamicAttribute(String label, String storage)
			throws PPExecutionException {
		if (debug > 1)
			System.out.println("EWSSynchro: getDynamicAttribute(" + label + "," + storage + ")");
		Pruefling pl; // reference to the storing PL
		if (storage != null) {
			// get the reference to the storing PL
			pl = getPr�fling().getAuftrag().getPr�fling(storage); 
		} else {
			// get the reference to the storing PL (use current PL)
			pl = getPr�fling(); 
		}
		if (pl == null) {
			// throw an exeception if storing PL is null
			if (debug > 1)
				System.out.println("EWSSynchro.getDynamicAttribute: Storage-PL not found");
			throw new PPExecutionException("EWSSynchro.getDynamicAttribute: Storage-PL not found");
		}
		try {
			// get the hashtable of the pp where the arguments are stored
			Map ht = pl.getAllAttributes();
			if (ht == null) {
				// thow an exception if ht is null
				if (debug > 1)
					System.out.println("EWSSynchro.getDynamicAttribute: Hashtable not found");
				throw new PPExecutionException("EWSSynchro.getDynamicAttribute: Hashtable not found");
			}
			// get the value assigned to label (in the hashtable)
			Object temp = ht.get(label); 
			if (debug > 1)
				System.out.println("EWSSynchro.getDynamicAttribute: Value=" + temp);
			if (temp == null)
				return null; // return null if value is null
			else
				return (String) temp; // return value with typecast to String
		} catch (PPExecutionException ppe) {
			throw ppe;
		} catch (Exception e) {
			// throw an exception if something unknown/unexpected went wrong
			e.printStackTrace();
			throw new PPExecutionException("EWSSynchro.getDynamicAttribute: Unknown Exception");
		}
	}

	/**
	 * Assigns dynamic attributes with the name (labelPrefix + index) to the
	 * given array
	 * 
	 * @param labelPrefix
	 *            the common prefix for the names of the dynamic attributes
	 * @param valueArray
	 *            the array of values
	 * @param beginIndex
	 *            the first index that will be used for getting the value from
	 *            the array
	 * @param numEntries
	 *            the number of values that will be taken from the array
	 * @param storage
	 *            defines the PP that will be used for storing dynamic
	 *            attributes
	 */
	public void getDynamicAttributes(String labelPrefix, String[] valueArray,
			int beginIndex, int numEntries, String storage)
			throws PPExecutionException {
		int i; // counter for index
		// loop over all indices
		for (i = beginIndex; i < (beginIndex + numEntries); i++) {
			valueArray[i] = getDynamicAttribute(labelPrefix + i, storage);
		}
	}
	
	//********************************************************
	//CLASSES FOR XML-Parsing
	//********************************************************
	
	/**
	 * @author root
	 *
	 */
	private class SXMLAttribute {
		
		protected String name = null;
		protected String value = null;
		
		protected SXMLAttribute(String name, String value) throws SXMLException {
			setName(name);
			setValue(value);
		}
		
		protected void setName(String name) throws SXMLException {
			if (name == null) throw new SXMLException("Attribute-Name 'null' not allowed!");
			this.name = name;
		}
		
		protected void setValue(String value) throws SXMLException {
			if (value == null) throw new SXMLException("Attribute-Value 'null' not allowed!");
			this.value = value;
		}
		
		public String getName() {
			return this.name;
		}

		public String getValue() {
			return this.value;
		}
		
		public String toString() {
			return "Attribute '" + getName() + "' -> '" + getValue() + "'";
		}
	}
	/**
	 * @author root
	 *
	 */
	private class SXMLDocument {

		private SXMLNode mainNode = null;
		
		protected SXMLDocument() {
			mainNode = null;
		}
		
		protected void setMainNode(SXMLNode mainNode) throws SXMLException {
			if (mainNode == null) throw new SXMLException("Main Node 'null' not allowed!");
			this.mainNode = mainNode;
		}
		
		public SXMLNode getMainNode() {
			return this.mainNode;
		}
		
		public String toString() {
			if (mainNode == null) return "EMPTY";
			else return mainNode.toString();
		}

		public String toXMLString() {
			if (mainNode == null) return null;
			else return toXMLStringBuffer().toString(); 
		}
		
		public StringBuffer toXMLStringBuffer() {
			if (mainNode == null) return null;
			else return mainNode.toXMLStringBuffer(null, 0);
		}
		
	}
	/**
	 * @author root
	 *
	 */
	private class SXMLException extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public SXMLException(String message) {
			super(message);
		}
		
	}
	/**
	 * @author root
	 *
	 */
	private class SXMLNode {
		
		String nodeName = null;
		String nodeValue = null;
		Vector childNodes = null;
		Vector attributes = null;
		
		
		protected SXMLNode(String nodeName, String nodeValue, Vector childNodes, Vector attributes) {
			this.nodeName = nodeName;
			this.nodeValue = nodeValue;
			this.childNodes = childNodes;
			this.attributes = attributes;
		}

		protected SXMLNode(String nodeName, String nodeValue) {
			this.nodeName = nodeName;
			this.nodeValue = nodeValue;
			this.childNodes = null;
			this.attributes = null;
		}
		
		protected SXMLNode(String nodeName) {
			this.nodeName = nodeName;
			this.nodeValue = null;
			this.childNodes = null;
			this.attributes = null;
		}
		
		public int getNumChildNodes() {
			if (childNodes == null) return 0;
			return childNodes.size();
		}
		
		public int getNumAttributes() {
			if (attributes == null) return 0;
			return attributes.size();
		}
		
		public Vector getChildNodes() {
			return childNodes;
		}
		
		public Vector getAttributes() {
			return attributes;
		}
		
		protected void appendChildNode(SXMLNode newNode) throws SXMLException {
			if (newNode == null) throw new SXMLException("New Node 'null' can't be appended!");
			if (childNodes == null) childNodes = new Vector();
			childNodes.add(newNode);
		}
		
		protected void appendAttribute(SXMLAttribute newAttr) throws SXMLException {
			if (newAttr == null) throw new SXMLException("New Attribute 'null' can't be appended!");
			if (attributes == null) attributes = new Vector();
			attributes.add(newAttr);
		}
		
		protected void setName(String name) throws SXMLException {
			if (name == null) throw new SXMLException("Node-Name 'null' not allowed!");
			this.nodeName = name;
		}
		
		protected void setValue(String value) throws SXMLException {
			this.nodeValue = value;
		}
		
		public String getName() {
			return this.nodeName;
		}

		public String getValue() {
			return this.nodeValue;
		}
		
		public String toString() {
			StringBuffer res = new StringBuffer();
			res.append("<Node '" + getName() + "' Value '" + getValue() + "'\n");
			Vector attr = getAttributes();
			for (int i = 0; i < getNumAttributes(); i++) {
				res.append("  Attribute [" + attr.get(i) + "]\n");
			}
			Vector childs = getChildNodes();
			for (int i = 0; i < getNumChildNodes(); i++) {
				res.append("  Child[" + i + "]\n");
				res.append(childs.get(i).toString() + "\n");
			}
			res.append(">Node '" + getName() + "'");
			return res.toString();
		}
		
		public StringBuffer toXMLStringBuffer(StringBuffer buff, int level) {
			if (buff == null) buff = new StringBuffer();
			shiftLevel(buff, level);
			buff.append("<" + getName());
			Vector attr = getAttributes();
			for (int i = 0; i < getNumAttributes(); i++) {
				SXMLAttribute currAttr = (SXMLAttribute)attr.get(i);
				buff.append(" ");
				buff.append(currAttr.getName());
				buff.append("=\"");
				buff.append(currAttr.getValue());
				buff.append("\"");
			}
			buff.append(">");
			if (getValue() != null) buff.append(getValue());
			Vector childs = getChildNodes();
			for (int i = 0; i < getNumChildNodes(); i++) {
				SXMLNode currChild = (SXMLNode) childs.get(i);
				buff.append("\n");
				currChild.toXMLStringBuffer(buff, level+1);
			}
			if (getNumChildNodes() > 0) {
				buff.append("\n");
				shiftLevel(buff, level);
			}
			buff.append("</" + getName() + ">");
			return buff;
		}
		
	}
	
	//********************************************************
	//METHODS FOR XML-Parsing
	//********************************************************
	
	/**
	 * Parsed ein XML-Document, das als String �bergeben wird.
	 * @param xml Zu parsendes xml als String.
	 * @return Enth�lt das verabeitete Dokument in Form eines SXMLDocument-Objectes.
	 */		
	private SXMLDocument sxmlParse(String xml) throws Exception {

		SXMLDocument xmlDocument = null;

		try {
			Reader re = new StringReader(xml);
			InputSource is = new InputSource(re);
			DocumentBuilder myDOMBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			//StringBufferInputStream myStringIS = new StringBufferInputStream(xml);
			//Document myDocument = myDOMBuilder.parse(myStringIS);
			Document myDocument = myDOMBuilder.parse(is);

			//List Node-Types
			if (debug > 4) System.out.println("SXMLParser.parse: ATTRIBUTE_NODE " + Node.ATTRIBUTE_NODE);
			if (debug > 4) System.out.println("SXMLParser.parse: CDATA_SECTION_NODE " + Node.CDATA_SECTION_NODE);
			if (debug > 4) System.out.println("SXMLParser.parse: COMMENT_NODE " + Node.COMMENT_NODE);
			if (debug > 4) System.out.println("SXMLParser.parse: DOCUMENT_FRAGMENT_NODE " + Node.DOCUMENT_FRAGMENT_NODE);
			if (debug > 4) System.out.println("SXMLParser.parse: DOCUMENT_NODE " + Node.DOCUMENT_NODE);
			if (debug > 4) System.out.println("SXMLParser.parse: DOCUMENT_TYPE_NODE " + Node.DOCUMENT_TYPE_NODE);
			if (debug > 4) System.out.println("SXMLParser.parse: ELEMENT_NODE " + Node.ELEMENT_NODE);
			if (debug > 4) System.out.println("SXMLParser.parse: ENTITY_NODE " + Node.ENTITY_NODE);
			if (debug > 4) System.out.println("SXMLParser.parse: ENTITY_REFERENCE_NODE " + Node.ENTITY_REFERENCE_NODE);
			if (debug > 4) System.out.println("SXMLParser.parse: NOTATION_NODE " + Node.NOTATION_NODE);
			if (debug > 4) System.out.println("SXMLParser.parse: PROCESSING_INSTRUCTION_NODE " + Node.PROCESSING_INSTRUCTION_NODE);
			if (debug > 4) System.out.println("SXMLParser.parse: TEXT_NODE " + Node.TEXT_NODE);

			NodeList mainNodeList = myDocument.getChildNodes();
			if (debug > 4) System.out.println("SXMLParser.parse: main.GetLength:" + mainNodeList.getLength());
			if (mainNodeList.getLength() != 1) throw new SXMLException("Exactly one root element required!");

			//fetch main Node
			Node mainNode = mainNodeList.item(0);
			if (debug > 2) System.out.println("SXMLParser.parse: mainNode.Name:" + mainNode.getNodeName());

			//create empty xmlDocument
			xmlDocument = new SXMLDocument();
			xmlDocument.setMainNode(sxmlParseNode(mainNode));


		} catch(Exception e) {
			System.out.println("SXMLParser.parse: EXCEPTION:\n");
			e.printStackTrace(System.out);
			throw e;
		}
		return xmlDocument;
	}


	/**
	 * Methode zum Abarbeiten einer Node.
	 * @param
	 * @return Enth�lt den verarbeiteten Knoten und den Sub-Baum in Form eines SXMLNode-Objectes.
	 */
	private SXMLNode sxmlParseNode(Node parseNode) throws Exception {

		String pName = null;
		String pValue = null;
		SXMLNode sNode =  null;

		try {
			pName = parseNode.getNodeName();
			if (debug > 2) System.out.println("SXMLParser.parseNode: Node-Name: '" + pName + "'");
			if (pName == null) throw new SXMLException("Parsed Node-Name 'null'");
			if (pName.equalsIgnoreCase("#text")) throw new SXMLException("Parsed Node-Name must not be '#text'");
			if (parseNode.getNodeType() != Node.ELEMENT_NODE) throw new SXMLException("SXMLParser.parseNode: Node-Type ELEMENT_NODE expected for node");

			//create new SXMLNode
			sNode = new SXMLNode(pName);
			if (debug > 4) System.out.println("SXMLParser.parseNode: created SXMLNode '" + pName + "'");

			//getAttributes
			org.w3c.dom.NamedNodeMap parseAttr = parseNode.getAttributes();
			int parseNumAttr = 0;
			if (parseAttr != null) parseNumAttr = parseAttr.getLength();
			if (debug > 2) System.out.println("SXMLParser.parseNode: #Node-Attributes: '" + parseNumAttr + "'");

			//loop over attributes
			for (int i = 0; i < parseNumAttr; i++) {
				Node currentAttr = parseAttr.item(i);
				if (debug > 4) System.out.println("SXMLParser.parseNode: attribute " + i + ": Name -> '" + currentAttr.getNodeName() + "'");
				if (debug > 4) System.out.println("SXMLParser.parseNode: attribute " + i + ": Value -> '" + currentAttr.getNodeValue() + "'");
				if ((debug > 4) && (currentAttr.getNodeValue() != null)) System.out.println("SXMLParser.parseNode: attribute " + i + ": Value-Length -> '" + currentAttr.getNodeValue().trim().length() + "'");
				if (debug > 4) System.out.println("SXMLParser.parseNode: attribute " + i + ": Type -> '" + currentAttr.getNodeType() + "'");

				if (currentAttr.getNodeType() != Node.ATTRIBUTE_NODE) throw new SXMLException("SXMLParser.parseNode: Node-Type ATTRIBUTE_NODE expected for attribute " + i);
				String aName = currentAttr.getNodeName();
				String aValue = currentAttr.getNodeValue();
				if (aName == null) throw new SXMLException("SXMLParser.parseNode: Attribute-Name 'null' not allowed");			
				if (aName.startsWith("#")) throw new SXMLException("SXMLParser.parseNode: Attribute-Name '#...' not allowed");			
				if (aValue == null) throw new SXMLException("SXMLParser.parseNode: Attribute-Value 'null' not allowed");
				aValue = aValue.trim();

				SXMLAttribute sAttr = new SXMLAttribute(aName,aValue);
				if (debug > 4) System.out.println("SXMLParser.parseNode: created SXMLAttribute [" + sAttr + "]");

				sNode.appendAttribute(sAttr);
			}

			//getChildNodes
			NodeList parseChilds = parseNode.getChildNodes();
			int parseNumChilds = 0;
			if (parseChilds != null) parseNumChilds = parseChilds.getLength();
			if (debug > 2) System.out.println("SXMLParser.parseNode: #Node-Childs: '" + parseNumChilds + "'");

			//loop over child nodes
			for (int i = 0; i < parseNumChilds; i++) {
				Node currentChild = parseChilds.item(i);
				if (debug > 4) System.out.println("SXMLParser.parseNode: child " + i + ": Name -> '" + currentChild.getNodeName() + "'");
				if (debug > 4) System.out.println("SXMLParser.parseNode: child " + i + ": Value -> '" + currentChild.getNodeValue() + "'");
				if ((debug > 4) && (currentChild.getNodeValue() != null)) System.out.println("SXMLParser.parseNode: child " + i + ": Value-Length -> '" + currentChild.getNodeValue().trim().length() + "'");
				if (debug > 4) System.out.println("SXMLParser.parseNode: child " + i + ": Type -> '" + currentChild.getNodeType() + "'");

				//Switch over type
				switch(currentChild.getNodeType()) {
					case Node.ELEMENT_NODE:
						//we have a child
						if (debug > 2) System.out.println("SXMLParser.parseNode: parsing Child");
						SXMLNode sChild = sxmlParseNode(currentChild);
						sNode.appendChildNode(sChild);
						break;
					case Node.TEXT_NODE:
						//we have a value

						//ignore it if empty
						String val = currentChild.getNodeValue();
						if (val == null) throw new SXMLException("SXMLParser.parseNode: Value-Child 'null' not allowed");
						if (val.trim().length() == 0) {
							if (debug > 2) System.out.println("SXMLParser.parseNode: empty value for node ignored");
							break;
						}

						//check if we had a value before
						if (pValue != null) throw new SXMLException("SXMLParser.parseNode: Value-Child only one value-node allowed");

						//assign value to sNode
						sNode.setValue(val);
						if (debug > 2) System.out.println("SXMLParser.parseNode: value for node set '" + val + "'");
						break;
					default:
						throw new SXMLException("SXMLParser.parseNode: Child-Type '" + currentChild.getNodeType() + "' not supported/allowed");
				}
			}

		} catch(Exception e) {
			System.out.println("SXMLParser.parseNode: EXCEPTION:\n");
			e.printStackTrace(System.out);
			throw e;
		}

		return sNode;
	}

	/**
	 * Hilfsmethode um Baum-Strukturen durch Einr�ckungen geeignet darzustellen.
	 * @param buff Stringbuffer, an den pro Level Einr�cktife zwei leerzeichen angeh�ngt werden.
	 * @param level Einr�cktiefe 0 .. n.
	 */		
	protected static void shiftLevel(StringBuffer buff, int level) {
		for (int i = 0; i < level; i++) {
			buff.append("  ");
		}
	}
	
	
	public static void main(String args[]) {

		String test = "<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?> <ewsinfo vin=\"WBAEE111119046000\"><ewsecu><ewsecutype>CAS3</ewsecutype></ewsecu><ewssynchro><ewsinterface>EWS4</ewsinterface><ewscode>0123456789ABCDEF0123456789ABCDEF</ewscode></ewssynchro></ewsinfo>";
		test = test + "";
		//String test2 = "<?xml version=\"1.0 \" encoding=\"iso-8859-1\"?><immobiliser vin=\"WBAWB735X7PV82000\"><ewsInfo  creationDateTime=\"2007-01-30T08:33:58\" version=\"4.0\"><ewsEcu><ewsEcuType>CAS4</ewsEcuType></ewsEcu><ewsSynchro><ewsInterface>EWS4</ewsInterface><ewsCode encryption=\"AES\" ecuType=\"DMEDDE1\">1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF</ewsCode><ewsCode encryption=\"STD\" ecuType=\"EGS\">F4804706</ewsCode></ewsSynchro></ewsInfo></immobiliser>";
		String test2 = "<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?><immobiliser vin=\"WBAWB735X7PV82000\"><ewsInfo  creationDateTime=\"2007-01-30T08:33:58\" version=\"4.0\"><ewsEcu><ewsEcuType>CAS4</ewsEcuType></ewsEcu><ewsSynchro><ewsInterface>EWS4</ewsInterface><ewsCode encryption=\"STD\" ecuType=\"DMEDDE1\">1234567890ABCDEF1234567890ABCDEF</ewsCode><ewsCode encryption=\"STD\" ecuType=\"EGS\">F4804706</ewsCode></ewsSynchro></ewsInfo></immobiliser>";
		Vector ergs = new Vector();
		

		String oldEWS3 = "<?xml version=\"1.0\"?><!DOCTYPE EWSInfo SYSTEM \"file:///C:/ec-apps/cascade/dtd/EWSInfo.dtd\"><ewsinfo vin=\"WB12345688AB12345\"><ewsecu><ewsecutype>CAS3</ewsecutype></ewsecu><ewssynchro><ewsinterface>EWS3</ewsinterface></ewssynchro></ewsinfo>";
		String oldEWS4 = "<?xml version=\"1.0\"?><!DOCTYPE EWSInfo SYSTEM \"file:///C:/ec-apps/cascade/dtd/EWSInfo.dtd\"><ewsinfo vin=\"WB123456X8AB12346\"><ewsecu><ewsecutype>CAS3</ewsecutype></ewsecu><ewssynchro><ewsinterface>EWS4</ewsinterface><ewscode>2C3D0E1F6879D34A5BA4B58697E0F1C2</ewscode></ewssynchro></ewsinfo>";
		String oldEWS4EGS1 = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><immobiliser vin=\"WB12345618AB12347\"><ewsInfo creationDateTime=\"2007-10-24T11:08:24\" version=\"4.0\"><ewsEcu><ewsEcuType>CAS4</ewsEcuType></ewsEcu><ewsSynchro><ewsInterface>EWS4EGS</ewsInterface><ewsCode encryption=\"STD\" ecuType=\"DMEDDE1\">C0877AC97E33940C9471DD604982A7E5</ewsCode><ewsCode encryption=\"STD\" ecuType=\"EGS\">7986D3D3</ewsCode></ewsSynchro></ewsInfo></immobiliser>";
		String oldEWS4EGS2 = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><immobiliser vin=\"WB12345638AB12348\"><ewsInfo creationDateTime=\"2007-10-24T11:08:24\" version=\"4.0\"><ewsEcu><ewsEcuType>CAS4</ewsEcuType></ewsEcu><ewsSynchro><ewsInterface>EWS4EGS</ewsInterface><ewsCode encryption=\"STD\" ecuType=\"DMEDDE1\">2C3D0E1F6879D34A5BA4B58697E0F1C2</ewsCode><ewsCode encryption=\"STD\" ecuType=\"EGS\">97E0F1C2</ewsCode></ewsSynchro></ewsInfo></immobiliser>";
		String newEWS4EGS = "<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?><vehicleImmobilizer xmlns=\"http://bmw.com/immobiliser\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://bmw.com/immobiliser ../vehicleImmobilizerV01_00.xsd\" version=\"01.00\" refSchema=\"vehicleImmobilizerV01_00.xsd\"><vehicle vin=\"WB12345668FF00001\"><coding><synchronisationInfo version=\"01.03\" refSchema=\"synchronisationInfoV0103.xsd\" creationDateTime=\"2007-09-18T16:13:29\"><immobiliserEcu ecuType=\"CAS4\"/><interface interfaceType=\"EWS4EGS\"/><synchronisedEcus><synchronisedEcu ecuType=\"DMEDDE1\"><syncCode encryption=\"STD\">2D0F694BA587E1C32D0F694BA587E1C3</syncCode></synchronisedEcu><synchronisedEcu ecuType=\"EGS\"><syncCode encryption=\"STD\">6879F1C2</syncCode></synchronisedEcu></synchronisedEcus></synchronisationInfo></coding></vehicle></vehicleImmobilizer>";
		
		try {
			StringBuffer in = null;
			EWSSynchro_22_0_F_Pruefprozedur myPP = null;
			
			in = new StringBuffer(oldEWS3);
			ergs = new Vector();
			System.out.println("Parsing oldEWS3\n==============================================");
			myPP = new EWSSynchro_22_0_F_Pruefprozedur();
			myPP.debug = 5;
			myPP.parseXML(in, ergs);
			
			in = new StringBuffer(oldEWS4);
			ergs = new Vector();
			System.out.println("Parsing oldEWS4\n==============================================");
			myPP = new EWSSynchro_22_0_F_Pruefprozedur();
			myPP.debug = 5;
			myPP.parseXML(in, ergs);
			
			in = new StringBuffer(oldEWS4EGS1);
			ergs = new Vector();
			System.out.println("Parsing oldEWS4EGS1\n==============================================");
			myPP = new EWSSynchro_22_0_F_Pruefprozedur();
			myPP.debug = 5;
			myPP.parseXML(in, ergs);
			
			in = new StringBuffer(oldEWS4EGS2);
			ergs = new Vector();
			System.out.println("Parsing oldEWS4EGS2\n==============================================");
			myPP = new EWSSynchro_22_0_F_Pruefprozedur();
			myPP.debug = 5;
			myPP.parseXML(in, ergs);
			
			in = new StringBuffer(newEWS4EGS);
			ergs = new Vector();
			System.out.println("Parsing newEWS4EGS\n==============================================");
			myPP = new EWSSynchro_22_0_F_Pruefprozedur();
			myPP.debug = 5;
			myPP.parseXML(in, ergs);
			
			
		} catch (Exception e) {
			System.out.println("Parsing-Test-Failed");
			e.printStackTrace(System.out);
		}	
	}
}

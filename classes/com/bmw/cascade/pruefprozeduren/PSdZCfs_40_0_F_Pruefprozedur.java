/**
* PSdZCfs_40_0_F_Pruefprozedur.java <br>
 * <br>
 * V 6: Integration Standard-Interfaces EDICNET, FUNK, OMITEC <br>
 * V 7: MCD_PROJECT und VEHICLE_INFO jetzt auch Soll-I-Stufen abh�ngig <br>
 * V 8: Optimierung UserDialog, Vorbereitung CASCADE 2.2.3 <br>
 * V10: Umstellung auf CASCADE 2.2.4, PSdZ 3.2.x <br>
 *      Integration Multi-Instanz <br>
 *      I-Stufe ins Fahrzeug schreiben dazu <br>
 *      Progress �berarbeitet <br>
 *      Ber�cksichtigung Slave-Bootloader FLSL <br>
 *      Ber�cksichtigung Betriebsanleitungsdaten IBAD <br>
 * V11: Umstellung auf CASCADE 2.2.5 <br>
 *      neue PSdZ-Device Methode openVehicleConnectionByIStep eingebaut <br>
 * V12: ignoreErrors auf true gesetzt wegen PSdZ-Problem bei Flash-Test <br>
 * V13: ignoreErrors jetzt INI-Datei Eintrag <br>
 * V14: getTargetSelectorInfoForIStep Null-Pointer exception abgefangen <br>
 *      Bug behoben        : Steuerdatei Formatfehler in SGBMID nicht erkannt <br>
 *      Bug behoben        : TAL-�berpr�fung fehlerhaft bei HW-Tausch in Verbindung mit <br>
 *                           INI-Eintrag ALLOW_BOOTLOADER_UPDATE = FALSE <br>
 *                           INI-Eintrag ALLOW_IBA_UPDATE = FALSE <br>
 *      Verbesserungswunsch: Flash-Logbuch Abbruch durch Anwender vermerkt <br>
 *      Verbesserungswunsch: Programmierung trotzt fehlender ECUs <br>
 *                           neuer INI-Eintrag IGNORE_MISSING_ECUS <br>
 *      Verbesserungswunsch: Integration Funk 'nicht Punkt zu Punkt'-Verbindung <br>
 *                           f�r Gesamtfahrzeugprogrammierung <br>
 *                           Integration Fahrzeugsteuerschl�sselflag <br>
 *                           neuer INI-Eintrag USE_FZS <br>
 *      Verbesserungswunsch: Fzg-Log-Datei SVT nach Adresse sortiert <br>
 * V15: Verbesserungswunsch: neuer INI-Eintrag DEFAULT_ISTUFE <br>
 *                           Erste Verbindung zum Auslesen des Fahrzeugauftrags <br>
 *                           f�r Gesamtfahrzeugprogrammierung <br>
 *      Verbesserungswunsch: neuer INI-Eintrag PSDZ_EXCEPTIONS <br>
 *                           PSdZ Exceptions in der Protokollausgabe <br>
 * V20: Umstellung auf CASCADE 2.2.7, PSdZ 3.5.x <br>
 *      getTargetSelectorInfoForIStep entfallen <br>
 * V21: Verbesserungswunsch: Fortschrittsanzeige f�r Container-Import bei Close <br>
 *                           Fehlertexte jetzt aus der TAL <br>
 *                           VIN-Zuordnung bei Gesamtfahrzeug-Flash �ber ZGW_VIN_1 und ZGW_VIN_2 <br>
 *                           Texte bei Tal-Abarbeitung jetzt �bersetzt <br>
 *                           DebugLevel eingef�hrt; nur f�r Tests; INI-Eintrag DEBUG_LEVEL =  0x.. <br>
 * V22: Bug behoben        : checkSVTSVT �berpr�fung der SVTist ob das SG auch geantwortet hat <br>
 * V23: Bug behoben        : Im iO-Fall wird jetzt das Ergebnis Versionsinfo nicht mehr erzeugt <br>
 *      Verbesserungswunsch: VIN-Zuordnung bei Gesamtfahrzeug-Flash mit mehreren Fahrzeugen <br>
 *                           1. �ber ZGW_VIN_1 bzw. ZGW_VIN_2 <br>
 *                           2. �ber CASCADE Auftrag <br>
 *                           3. �ber Auswahlmen� <br>
 *      Verbesserungswunsch: Gesamtfahrzeugprogrammierung Konfigurationsdateien <br>
 *                           Mehrere Baureihen-Sektionen in einer Zeile erlaubt <br>
 * V24: Verbesserungswunsch: Programming Dependencies Checked Auswertung im Trailer-Mode dazu <br>
 *                           SVT-Ist Programming Dependencies Checked im Fehlerfalle in der Trace-Datei <br>
 *                           Zwangsprogrammierung des SGs, wenn Programming Dependencies Checked != 1 <br>
 *                           und SGs nicht in der TAL enthalten <br>
 * V25: Verbesserungswunsch: DEFAULT_ISTUFE jetzt Instanzabh�ngig <br>
 *                           checkTrailerTAL, getProgressBar BLUP, FLUP integriert <br>
 * V27: Umstellung auf CASCADE 3.0.x, PSdZ 3.8.x <br>
 *      setTestScreenID neu <br>
 *      Bug behoben        : isEmptyTAL Fehlermeldung: Adressen jetzt korrekt <br>
 *      Verbesserungswunsch: APDM_DETAIL f�r zus�tzliche Ergebnisse in APDM <br>
 *      Verbesserungswunsch: Integration ICOM <br>
 * V28: Integration HO-Modus mit V-KIS <br>
 *      PDX-Import-Anzeige aus close entfernt <br>
 * V29: Integration ModeSwitch f�r Flashmode-Umschaltung <br>
 * V30: Verbesserungswunsch: Flash-Trailer Integration Anliefer-Programmierung <br>
 *      Anforderung        : Fachteam Programmierung TI-533 Richard Wurm <br>
 *      Verbesserungswunsch: �berpr�fung auf zu viele SWFL bei Gesamtfahrzeugprogrammierung. SVT-Ist gegen SVT-Soll <br>
 *                           VERIFY_SVT zum Aktivieren der �berpr�fung <br>
 *      Anforderung        : TI-532 Richard Kolbeck <br>
 *      Bug behoben        : Benutzeranzeige bei Zwangstausch jetzt korrekt <br>
 *      Verbesserungswunsch: Trailerbetrieb: Bei der Absteckmeldung wird auch die Lieferumfang-Nummer angezeigt <br>
 * V31: Umstellung auf CASCADE 4.1.x, PSdZ 4.3.x <br>
 *      myTA.getException() deprecated ersetzt durch myTA.getExceptions() <br>
 *      Verbesserungswunsch: Trim bei ini-Eintr�gen <br>
 *      Verbesserungswunsch: Integration Baureihe Ixx, Kxx, Istufe Ixxx Kxxx <br>
 *      Verbesserungswunsch: Auftrags-ISTUFE hat vorrang vor Default-ISTUFE. <br>
 *                           Beim F20 ist der FZG-Auftrag in FEM 0x10 und REM 0x72, sonst in ZGW 0x10 und CAS 0x40 <br>
 *      Verbesserungswunsch: Keine Progressmessage Ausgabe von MCDDiagService Fehlern, da diese f�r den Anwender nicht verst�ndlich sind <br>
 *      Windows 7 Anpassung: PROGRESSMARKER von 127 auf 187 ge�ndert <br>
 *      Verbesserungswunsch: Trailermode: Workaround wegen Gateway-Routing-Problem wenn direkt ohne SUB-Gateway programmiert wird <br>
 * V32: Bug behoben        : Falscher Fehlertext bei getTALExecutionStatus Exception behoben <br>
 *      Anforderung        : Sonderbehandlung I-Stufe ins Fahrzeug schreiben: RR1 Serie II hat kein Backup <br>
 *      Anforderung        : openVehicleConnection(McdProject(),VehicleInfo()) deprecated. Ausgebaut <br>
 *      Anforderung        : APDM_DETAIL default-Wert jetzt true <br>
 *                           Baureihen 'Mxx','Gxx'und I-Stufenformat 'Sxxx' integriert. <br>
 * V33: Umstellung auf CASCADE 5.2.x, PSdZ 4.7.x <br>
 *                           Bei F025 Baureihenverbund grunds�tzlich wiederholtes OpenConnection bei FZG-Flash <br>
 *                           Wegen unterschiedlicher VCM-Backup-ECUs bei F25 bzw. F15 <br>
 * V34: Bug behoben        : Fehlerprotokoll unvollst�ndig wegen Exception wenn Abbruch durch PSdz aber nur Warn-Event erzeugt wird <br>
 *      Anforderung        : Integration M12 <br>
 *                           Sonderbehandlung I-Stufe ins Fahrzeug schreiben: M12 hat kein Backup <br>
 * V35: Anforderung        : Integration Bedarfgerechter Update BEGU, Prozessklasse SWFK dazu <br>
 *                         : Sonderbehandlung I-Stufe ins Fahrzeug schreiben: RR2, RR3 Serie II hat kein Backup <br>
 * V36: Umstellung auf CASCADE 6.0.x, PSdZ 4.9.x <br>
 *      getSteuerschl�ssel() deprecated ersetzt durch getSteuerschluessel() <br>
 *      Anforderung        : Integration GWTB-Update <br>
 *                           INI-Eintrag ALLOW_GWTB_UPDATE = FALSE <br>
 *                           Trailer Zusatzinfo in der lieferUmfangNummerListeInfo <br>
 * V37: Umstellung auf CASCADE 7.0.x, PSdZ 5.0.x <br>
 *      Anforderung        : clearPSdZExceptions deprecated, entfernt <br>
 *      Anforderung        : getVehicleFA deprecated, neue Methode getFAFromVehicle <br>
 *      Anforderung        : getPSdZExceptions deprecated, entfernt <br>
 *                           getPsdzExceptionFlag, psdzExceptionFlag, PSDZ_EXCEPTIONS ausgebaut <br>
 *      Anforderung        : startTALExecution deprecated, neue Parameter <br>
 *                           getStopOnError, stopOnError ausgebaut <br>
 *                           getDifferentialExecution, differentialExecution ausgebaut <br>
 *                           getIgnoreErrors, ignoreErrors, IGNORE_ERRORS ausgebaut <br>
 *      Anforderung        : Setzen der Modusumschaltung nur noch bei ENET <br>
 *                           explizites Ausschalten bei allen anderen Interfaces <br>
 * <br>
 * V38: Umstellung auf CASCADE 7.0.x, PSdZ 5.1.x <br>
 *      Bug behoben        : setCheckProgDeps nach TAL-Ermittlung wieder zur�ckstellen <br>
 *      Bug behoben        : UserDialog .pack dazu <br>
 *      Anforderung        : Aufgrund KIS-Fehler kann es dazu kommen, dass nach der Programmierung noch einmal programmiert werden soll. <br>
 *                           Es wird gepr�ft ob die neue SVK nicht weiter programmiert wird. <br>
 *                           INI-Eintrag TRAILER_KIS_VERIFY = TRUE <br>
 *                           verifySVKTrailer dazu <br>
 *      Verbesserungswunsch: Eigene Klasse CfsString f�r deutsche und englische Texte <br>
 *      Verbesserungswunsch: Fehlerausgabe entryECUsFT_NIO aktualisiert (�bernahme aus PSdZExecuteTal_36_0_F_Pruefprozedur) <br> 
 *  <br>
 * V39: Integration RR11 (vierstellige Baureihe ohne '0') <br>
 *  <br>
 * V40: ICOM setICOMLinkPropertiesToDCan f�r Motorrad und Notstrategie eingebaut <br>
 * @author BMW AG TI-545 Drexel <br>
 * @version 21.12.2016 <br>
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Properties;
import java.util.StringTokenizer;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.signallamp.SignalLamp;
import com.bmw.cascade.pruefstand.devices.printer.Printer;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.pruefstand.devices.psdz.PSdZ;
import com.bmw.cascade.pruefstand.devices.psdz.util.CascadeSVTReader;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeECU;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeExecutionStatus;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeFA;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeFilteredTACategory;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeFscTA;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeLogistischesTeil;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeProcessClasses;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeSGBMID;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeSVT;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTA;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTACategories;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTAL;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTALExecutionStatus;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTALFilter;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTALLine;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTALTACategory;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeKISWBVersion;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.CascadePSdZConditionsNotCorrectException;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.CascadePSdZRuntimeException;

/**
 * Pr�fprozedur f�r CFS Cascade Flash System f�r <br>
 * Gesamtfahrzeug-Programmierung <br>
 * und <br>
 * Flash-Tailer-Programmierung einzelner Steuerger�te <br>
 * Nachfolger von NFS <br>
 *
 * @param TRAILERMODE <br>
 * 		true <br>
 *      	Flash-Tailer-Programmierung <br>
 * 		false <br>
 * 			Gesamtfahrzeug-Programmierung <br>
 * @param SAMPLING <br>
 * 		true <br>
 * 			Stichproben-Programmierung bei Gesamtfahrzeug-Programmierung <br>
 * 				�berpr�fung eines Fahrzeugs auf Konsistenz mit <br>
 * 				Zwangs-Programmierung aller Steuerger�te auf sich selbst. <br>
 * 			Zwangs-Programmierung bei Flash-Tailer-Programmierung <br>
 * 		false <br>
 * 			Normalbetrieb <br>
 * @param EDICNET_IP_ADDRESS <br>
 *  	Optionaler Parameter <br>
 *      Veraltet. Ersetzt durch INTERFACE_IP_ADDRESS.<br>
 *  	Wenn gesetzt wird bei ENET EDICNET benutzt, <br>
 *  	bei ICOM zwingend erforderlich <br>
 * @param USE_FZS <br>
 *  	Optionaler Parameter <br>
 *  	Nur f�r Gesamtfahrzeugprogrammierung mit Funk-Interface <br>
 *      im 'nicht Punkt zu Punkt'-Betrieb <br>
 *      Zur Zuordnung des Funk-Interface wird der CASCADE-Auftrag <br>
 *      ben�tigt. <br>
 * 		true <br>
 * 			Es wird der CASCADE-Fahrzeugsteuerschl�ssel <br>
 *			zur Funk-Interface-Zuordnung benutzt. <br>
 * 		false <br>
 * 			Es wird die CASCADE-Fahrgestellnummer <br>
 *			zur Funk-Interface-Zuordnung benutzt. <br>
 * @param INTERFACE_IP_ADDRESS <br>
 *  	Optionaler Parameter <br>
 *      Ersetzt EDICNET_IP_ADDRESS.<br>
 *  	Wenn gesetzt wird bei ENET EDICNET benutzt, <br>
 *  	bei ICOM zwingend erforderlich <br>
 * @param ICOM_CAN_BRIDGE_ACTIVE <br>
 *  	Optionaler Parameter <br>
 *  	F�r PSdZ, ob im ICOM die CAN-Bridge aktiviert ist. <br>
 * 		true <br>
 * 			Im ICOM ist die CAN-Bridge aktiviert. <br>
 * 		false <br>
 * 			Im ICOM ist die CAN-Bridge nicht aktiv. <br>
 *
 * @author BMW AG TI-545 Drexel <br>
 * @version 21.12.2016 <br>
 */
public class PSdZCfs_40_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	static final long serialVersionUID = 1L;
	/**
	 * Globale Variablen
	 *
	 * @author BMW AG TI-545 Drexel <br>
	 * @version 09.03.2011 <br>
	 */
	static final String VERSION = "40.00";
	static final String NL = ";";
	static final String CRLF = "\r\n";
	static final String LF = "\n";
	static final String ZGW_EXTENSION = "_DIAGADR10";
	static final char PROGRESSMARKER = (char)187;    // '>>' f�r Progress
    // veraltet. ersetzt durch INTERFACE_NET_IP 
	static final String EDIC_NET_IP = "EDIC_NET_IP"; // Variable f�r EDIC Net IP Adresse aus Frontend
	static final String INTERFACE_NET_IP = "INTERFACE_NET_IP"; // Variable f�r ICOM Net IP Adresse aus Frontend
	static final String PSDZ__KIS_CALCULATION_LEVEL = "PSDZ__KIS_CALCULATION_LEVEL"; // Legt fest, welches KIS-Level von PSdZ angewendet wird
	static final int AUTOMATIC_MESSAGE_TIMEOUT = 3;  // in Sekunden
	static final int PSDZ_STATUS_REFRESH_TIME = 2000;// in Millisekunden !0 nicht erlaubt
	/**
	 * DE fuer Sprachermittlung
	 */
	private final boolean DE = checkDE(); // Systemsprache DE wenn true

    // Global um Informationen zwischen den Instanzen auszutauschen
	static String vehicleVinInUse[] = {null, "", ""};

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSdZCfs_40_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 *
	 * @param pruefling
	 *            Klasse des zugeh. Pr�flings
	 * @param pruefprozName
	 *            Name der Pr�fprozedur
	 * @param hasToBeExecuted
	 *            Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSdZCfs_40_0_F_Pruefprozedur(Pruefling pruefling,
			String pruefprozName, Boolean hasToBeExecuted) {
		super(pruefling, pruefprozName, hasToBeExecuted);
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "EDICNET_IP_ADDRESS", "USE_FZS", "INTERFACE_IP_ADDRESS", "ICOM_CAN_BRIDGE_ACTIVE"};
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "TRAILERMODE", "SAMPLING" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 */
	public boolean checkArgs() {
		String trailerMode;
		String sampling;
		String useFzs;
		String icomCanBridgeActive;

		try {
			if (!super.checkArgs()) return false;

			trailerMode=getArg(getRequiredArgs()[0]);
            if (trailerMode==null) return false;
           	if (!trailerMode.equalsIgnoreCase("TRUE") && !trailerMode.equalsIgnoreCase("FALSE")) return false;

           	sampling=getArg(getRequiredArgs()[1]);
            if (sampling==null) return false;
           	if (!sampling.equalsIgnoreCase("TRUE") && !sampling.equalsIgnoreCase("FALSE")) return false;

           	// getOptionalArgs()[0] keine �berpr�fung auf Plausibilit�t

           	useFzs=getArg(getOptionalArgs()[1]);
            if (useFzs!=null)
            	if (!useFzs.equalsIgnoreCase("TRUE") && !useFzs.equalsIgnoreCase("FALSE")) return false;

            // getOptionalArgs()[2] keine �berpr�fung auf Plausibilit�t

            icomCanBridgeActive=getArg(getOptionalArgs()[3]);
            if (icomCanBridgeActive!=null)
            	if (!icomCanBridgeActive.equalsIgnoreCase("TRUE") && !icomCanBridgeActive.equalsIgnoreCase("FALSE")) return false;

			return true;
		} catch (Exception e) {
			return false;
		} catch (Throwable e) {
			return false;
		}
	}

	/**
	 * CFS Cascade Flash System <br>
	 * Eigene Klasse f�r deutsche und englische Texte <br>
	 * @author BMW AG TI-545 Drexel <br>
	 * @version 05.11.2015 <br>
	 */
	final static class CfsString {
		public static HashMap<String, String> myString = new HashMap<String, String>();
		
		static {
			myString.clear();
			if (checkDE()) {
				// deutsch
				myString.put("cfs.addresshelp", "Adressformat 2-stellig hex '00' - 'FF'");
				myString.put("cfs.alarmmessage", "Alarmmeldung");
				myString.put("cfs.anlieferprogrammierungohne", "Partielle Programmierung ohne");
				myString.put("cfs.anlieferzustand", "Partielle Programmierung");
				myString.put("cfs.baureihe", "Baureihe");
				myString.put("cfs.baustand", "Baustand");
				myString.put("cfs.bootupdatenotallowederror", "Bootloader Update laut Konfiguration nicht zul�ssig");
				myString.put("cfs.cancel", "Abbr.");
				myString.put("cfs.checksgflashorsgignorhelp", "Bitte �berpr�fen Sie die Konfigurationsdateien SG Flash und SG Ignor");
				myString.put("cfs.checksgflashorsgnoflashhelp", "Bitte �berpr�fen Sie die Konfigurationsdateien SG Flash und SG NoFlash");
				myString.put("cfs.checksgignororsgnoflashhelp", "Bitte �berpr�fen Sie die Konfigurationsdateien SG Ignor und SG NoFlash");
				myString.put("cfs.checksgxxxxlist", "�berpr�fe SG Konfiguration");
				myString.put("cfs.checksvkconfigerror", "�berpr�fung der SVK gegen die Konfiguration fehlerhaft");
				myString.put("cfs.checksvksgverbauerror", "�berpr�fung der SVK gegen die SG Verbaukonfiguration fehlerhaft");
				myString.put("cfs.checksvksvkerror", "�berpr�fung SVK-Ist gegen die SVK-Soll fehlerhaft");
				myString.put("cfs.checksvtist", "�berpr�fe SVT-Ist");
				myString.put("cfs.checksvtisterror", "�berpr�fung der SVT-Ist fehlerhaft");
				myString.put("cfs.checksvtistneu", "�berpr�fe SVT-Ist-Neu");
				myString.put("cfs.checksvtistneuerror", "�berpr�fung der SVT-Ist-Neu fehlerhaft");
				myString.put("cfs.checksvtistsvtsoll", "�berpr�fe SVT-Ist gegen SVT-Soll");
				myString.put("cfs.checksvtsoll", "�berpr�fe SVT-Soll");
				myString.put("cfs.checksvtsollerror", "�berpr�fung der SVT-Soll fehlerhaft");
				myString.put("cfs.checksvtsvterror", "�berpr�fung SVT-Ist gegen die SVT-Soll fehlerhaft");
				myString.put("cfs.checktal", "�berpr�fe der TAL");
				myString.put("cfs.checktalerror", "�berpr�fung der TAL fehlerhaft");
				myString.put("cfs.configuration", "Konfiguration");
				myString.put("cfs.copy", "&Kopie");
				myString.put("cfs.debug", "Debug");
				myString.put("cfs.debugLevel", "Debug Level");
				myString.put("cfs.disconnect", "Bitte Steuerger�t abstecken");
				myString.put("cfs.disconnectecu", "Steuerger�t abstecken");
				myString.put("cfs.ecu", "Steuerger�t");
				myString.put("cfs.ecuaddressmultipleusedehelp", "Steuerger�teadresse in Steuerdatei �berpr�fen");
				myString.put("cfs.ecuaddressmultipleusederror", "Steuerger�teadresse mehrfach gefunden");
				myString.put("cfs.eculisteerror", "Ung�ltige Steuerger�teliste");
				myString.put("cfs.eculistehelp", "Sollte nie vorkommen. Steuerger�teliste ist leer oder null");
				myString.put("cfs.ecunamehelp", "Erlaubte Zeichen '0-9a-zA-Z' Sonderzeichen ' _/()'");
				myString.put("cfs.ecunotfounderror", "Steuerger�t nicht gefunden");
				myString.put("cfs.ecunotinsvtsollerror", "Widerspruch SG Verbaukonfiguration zu KIS SVT-Soll");
				myString.put("cfs.empty", "Leer");
				myString.put("cfs.emptyeculisteerror", "Leere Steuerger�teliste");
				myString.put("cfs.emptyistufenlisteerror", "Leere I-Stufen Liste");
				myString.put("cfs.emptysvklisteerror", "Leere SVK-Soll-Liste");
				myString.put("cfs.emptytal", "Leere TAL");
				myString.put("cfs.emptytallineerror", "Ung�ltige TAL-Zeile");
				myString.put("cfs.emptytallinehelp", "Sollte nie vorkommen. TAL-Zeile ist leer");
				myString.put("cfs.entryformathelp", "Format ECUn=EcuName,xx n=0..255 xx='00'-'FF'");
				myString.put("cfs.entrynotvaliderror", "Eintrag ung�ltig");
				myString.put("cfs.entwicklung", "Entwicklung");
				myString.put("cfs.eof", "eof");
				myString.put("cfs.ewnumber", "Anzahl E Wort");
				myString.put("cfs.fa", "Fahrzeugauftrag");
				myString.put("cfs.filechanged", "Datei ge�ndert");
				myString.put("cfs.flashstart", "Programmierung starten?");
				myString.put("cfs.gettalexecutionstatus", "PSdZ TAL Status lesen");
				myString.put("cfs.gettalexecutionstatuserror", "PSdZ TAL Status lesen fehlerhaft");
				myString.put("cfs.gwaddressmultipleusedehelp", "Gatewayadressen in Steuerdatei �berpr�fen");
				myString.put("cfs.gwaddressmultipleusederror", "Gatewayadresse mehrfach gefunden");
				myString.put("cfs.gwecuaddressmultipleusedehelp", "Gatewayadressen in Steuerdatei �berpr�fen");
				myString.put("cfs.gwecuaddressmultipleusederror", "Gatewayadresse und Steuerger�teadresse in Steuerdateien �berpr�fen");
				myString.put("cfs.gwlisteerror", "Ung�ltige Gatewayliste");
				myString.put("cfs.gwlistehelp", "Sollte nie vorkommen. Gatewayliste ist null");
				myString.put("cfs.gwnamehelp", "Erlaubte Zeichen '0-9a-zA-Z' Sonderzeichen ' _/()'");
				myString.put("cfs.gwnotfounderror", "Gateway nicht gefunden");
				myString.put("cfs.gwtbupdatenotallowederror", "GWTB Update laut Konfiguration nicht zul�ssig");
				myString.put("cfs.headlinesfa", "Fahrzeugauftrag");
				myString.put("cfs.headlinesgflashlist", "SG Flash Liste");
				myString.put("cfs.headlinesgignorlist", "SG Ignor Liste");
				myString.put("cfs.headlinesgnoflashlist", "SG NoFlash Liste");
				myString.put("cfs.headlinesgverbaulist", "Steuerger�te Verbauliste");
				myString.put("cfs.headlinesvtist", "SVT-Ist");
				myString.put("cfs.headlinesvtistneu", "SVT-Neu");
				myString.put("cfs.headlinesvtsoll", "SVT-Soll");
				myString.put("cfs.headlinetal", "TAL");
				myString.put("cfs.honumber", "Anzahl HO Wort");
				myString.put("cfs.ibaupdatenotallowederror", "IBA Update laut Konfiguration nicht zul�ssig");
				myString.put("cfs.ignore", "&Ignorieren");
				myString.put("cfs.ignored", "Ignoriert:");
				myString.put("cfs.ilevel", "I-Stufe");
				myString.put("cfs.inconsistentsgflashwithsgignorerror", "Widerspruch in der Konfiguration SG Flash mit SG Ignor");
				myString.put("cfs.inconsistentsgflashwithsgnoflasherror", "Widerspruch in der Konfiguration SG Flash mit SG NoFlash");
				myString.put("cfs.inconsistentsgignorwithsgnoflasherror", "Widerspruch in der Konfiguration SG Ignor mit SG NoFlash");
				myString.put("cfs.inline", "in Zeile");
				myString.put("cfs.instance", "Instanz");
				myString.put("cfs.instanceerror", "Instanzfehler");
				myString.put("cfs.instancehelp1", "Unterst�tzte Instanzen = 2");
				myString.put("cfs.instancehelp2", "RechnerName muss belegt sein");
				myString.put("cfs.invalidaddresserror", "Ung�ltige Adresse");
				myString.put("cfs.invalidcategoryerror", "Ung�ltige Kategorie");
				myString.put("cfs.invalidecunameerror", "Ung�ltiger Steuerger�tename");
				myString.put("cfs.invalidecunr", "Fehler in Steuerger�teanzahl");
				myString.put("cfs.invalidecunrhelp", "Eintrag ECUs=x mit ECU0 .. ECUx-1 in Steuerdatei �berpr�fen");
				myString.put("cfs.invalidfa", "Ung�ltiger Fahrzeugauftrag");
				myString.put("cfs.invalidformat", "Ung�ltiges Format");
				myString.put("cfs.invalidformathelp", "Bitte �berpr�fen Sie die Konfigurationsdatei");
				myString.put("cfs.invalidgwnameerror", "Ung�ltiger Gatewayname");
				myString.put("cfs.invalidgwnr", "Fehler in Gatewayanzahl");
				myString.put("cfs.invalidgwnrhelp", "Eintrag GWs=x mit GW0 .. GWx-1 in Steuerdatei �berpr�fen");
				myString.put("cfs.invalidilevel", "Ung�ltige I-Stufe");
				myString.put("cfs.invalidilevelhelp", "I-Stufe-Format 'F001-08-09-400'");
				myString.put("cfs.invalidkiswbversionerror", "Ung�ltige KIS WB Versionsliste");
				myString.put("cfs.invalidkiswbversionhelp", "Keine g�ltigen KIS WB Versionen verf�gbar");
				myString.put("cfs.invalidmcdproject", "Ung�ltiges MCD-Projekt");
				myString.put("cfs.invalidmcdprojecthelp", "MCD-Projekt-Format 'F001_08_09_400'");
				myString.put("cfs.invalidnameerror", "Ung�ltiger Rechnername");
				myString.put("cfs.invalidprocessclasserror", "Ung�ltige Prozessklasse");
				myString.put("cfs.invalidprogdeperror", "Ung�ltige Programming Dependencies Checked");
				myString.put("cfs.invalidselectedSGBMID", "Ung�ltige SGBMID Auswahl;Bitte w�hlen Sie die SWFL die nicht programmiert werden soll");
				myString.put("cfs.invalidseries", "Ung�ltige Baureihe");
				myString.put("cfs.invalidserieshelp", "Baureihenformat 'F01'");
				myString.put("cfs.invalidsgbmidlist", "Ung�ltige SGBMID Liste");
				myString.put("cfs.invalidvehicleinfo", "Ung�ltige Vehicle-Info");
				myString.put("cfs.invalidvehicleinfohelp", "Vehicle-Info-Format 'F001_08_09_400'");
				myString.put("cfs.invalidvin", "PSdZ Fahrgestellnummer lesen fehlerhaft");
				myString.put("cfs.ististufe", "Ist  I-Stufe");
				myString.put("cfs.keynotfounderror", "Schl�sselwort nicht gefunden");
				myString.put("cfs.keyonlycapitallettershelp", "Schl�sselwort nur in Gro�buchstaben");
				myString.put("cfs.kiscalculationlevelerror", "Ung�ltiger PSDZ__KIS_CALCULATION_LEVEL");
				myString.put("cfs.kiscalculationlevelhelp", "Bitte Pr�fstandsvariable PSDZ__KIS_CALCULATION_LEVEL �berpr�fen");
				myString.put("cfs.kislevelkiswberror", "Ung�ltige Kombination PSDZ__KIS_CALCULATION_LEVEL und KIS WB");
				myString.put("cfs.kislevelkiswbhelp", "Bitte KIS WB und Pr�fsandsvariable PSDZ__KIS_CALCULATION_LEVEL �berpr�fen");
				myString.put("cfs.kisverifywarning", "KIS �berpr�fung");
				myString.put("cfs.kisverifymessage", "Aufgrund KIS Datenfehler kann es vorkommen,;dass nach der Programmierung noch einmal programmiert werden soll.;Es wird gepr�ft ob die neue SVK nicht weiter programmiert wird.;Siehe PSdZ Trace KIS_SVKSOLL_1..., KIS_SVKSOLL_2...;;Bitte den Systembetreuer informieren.");				
				myString.put("cfs.lack", "Lack");
				myString.put("cfs.logfileelementerror", "Ung�ltiges Logdateielement");
				myString.put("cfs.logfileelementhelp", "Sollte nie vorkommen. Logdateielement ist null");
				myString.put("cfs.missing", "Fehlend:");
				myString.put("cfs.missingaddresserror", "Fehlende Adresse");
				myString.put("cfs.missingecuerror", "Unerwarteter SG-Einbau");
				myString.put("cfs.missingecunameerror", "Fehlender Steuerger�tename");
				myString.put("cfs.missinggwnameerror", "Fehlender Gatewayname");
				myString.put("cfs.modifytal", "TAL �ndern");
				myString.put("cfs.modifytalerror", "TAL �ndern Fehler");
				myString.put("cfs.msgerrordeinstall1", "Folgendes Steuerger�t sollte ausgebaut werden:");
				myString.put("cfs.msgerrordeinstall2", "Folgende Steuerger�te sollten ausgebaut werden:");
				myString.put("cfs.msgerrorinstall1", "Folgendes Steuerger�t sollte eingebaut werden:");
				myString.put("cfs.msgerrorinstall2", "Folgende Steuerger�te sollten eingebaut werden:");
				myString.put("cfs.msgerrormissing1", "Folgendes Steuerger�t fehlt:");
				myString.put("cfs.msgerrormissing2", "Folgende Steuerger�te fehlen:");
				myString.put("cfs.msgerrorreplace1", "Folgendes Steuerger�t sollte ausgetauscht werden:");
				myString.put("cfs.msgerrorreplace2", "Folgende Steuerger�te sollten ausgetauscht werden:");
				myString.put("cfs.msgflash1", "Folgendes Steuerger�t wird programmiert:");
				myString.put("cfs.msgflash2", "Folgende Steuerger�te werden programmiert:");
				myString.put("cfs.msgignore1", "Folgendes Steuerger�t wird ignoriert:");
				myString.put("cfs.msgignore2", "Folgende Steuerger�te werden ignoriert:");
				myString.put("cfs.msgmissing1", "Folgendes Steuerger�t fehlt:");
				myString.put("cfs.msgmissing2", "Folgende Steuerger�te fehlen:");
				myString.put("cfs.msgmustreplace1", "Folgendes Steuerger�t muss zwangsgetauscht werden:");
				myString.put("cfs.msgmustreplace2", "Folgende Steuerger�te m�ssen zwangsgetauscht werden:");
				myString.put("cfs.msgreplace1", "Folgendes Steuerger�t muss getauscht werden:");
				myString.put("cfs.msgreplace2", "Folgende Steuerger�te m�ssen getauscht werden:");
				myString.put("cfs.name", "Rechnername");
				myString.put("cfs.noeculisteerror", "Steuerger�teliste ist null");
				myString.put("cfs.noerror", "");
				myString.put("cfs.nohelp", "");
				myString.put("cfs.noistufenlisteerror", "Keine g�ltige I-Stufen Liste");
				myString.put("cfs.nomatchingilevel", "Keine passende I-Stufe");
				myString.put("cfs.nosgbmidlisteerror", "SGBMID Liste Fehler");
				myString.put("cfs.nosvklisteerror", "SVK-Soll-Liste ist null");
				myString.put("cfs.nozgwfounderror", "Kein Gateway gefunden");
				myString.put("cfs.nozgwfoundhelp", "Ethernetverbindung zum ZGW �berpr�fen");
				myString.put("cfs.number", "Anzahl");
				myString.put("cfs.oknosave", "OK oh&ne speichern");
				myString.put("cfs.polster", "Polster");
				myString.put("cfs.progdepchecked", "ProgDepChecked");
				myString.put("cfs.psdzcanceltalexecution", "PSdZ TAL abbrechen");
				myString.put("cfs.psdzclose", "PSdZ schlie�en");
				myString.put("cfs.psdzcloseerror", "PSdZ schlie�en fehlerhaft");
				myString.put("cfs.psdzclosevehicleconnection", "PSdZ Fahrzeugverbindung schlie�en");
				myString.put("cfs.psdzclosevehicleconnectionerror", "PSdZ Fahrzeugverbindung schlie�en fehlerhaft");
				myString.put("cfs.psdzerror", "PSdZ Fehler");
				myString.put("cfs.psdzexceptionsgeterror", "PSdZ get exception fehlerhaft");
				myString.put("cfs.psdzgenerateandstoreecusuccessors", "PSdZ Lieferumfangsnummernliste erzeugen");
				myString.put("cfs.psdzgenerateandstoreecusuccessorsemptyhashtableerror", "PSdZ Lieferumfangsnummernliste ist leer");
				myString.put("cfs.psdzgenerateandstoreecusuccessorserror", "PSdZ Lieferumfangsnummernliste erzeugen fehlerhaft");
				myString.put("cfs.psdzgenerateandstoreecusuccessorsinvalidlunerror", "PSdZ Ung�ltige Lieferumfangsnummer");
				myString.put("cfs.psdzgenerateandstoreecusuccessorsnohashtableerror", "PSdZ Lieferumfangsnummernliste ist null");
				myString.put("cfs.psdzgenerateandstoreecusuccessorsstorelunerror", "Lieferumfangsnummer speichern fehlerhaft");
				myString.put("cfs.psdzgenerateandstoresollverbauung", "PSdZ SVT-Soll setzen");
				myString.put("cfs.psdzgenerateandstoresollverbauungerror", "PSdZ SVT-Soll setzen fehlerhaft");
				myString.put("cfs.psdzgenerateandstoresollverbauungroh", "PSdZ SVT-Soll roh setzen");
				myString.put("cfs.psdzgenerateandstoresollverbauungroherror", "PSdZ SVT-Soll roh setzen fehlerhaft");
				myString.put("cfs.psdzgenerateandstoresvtist", "PSdZ SVT-Ist lesen");
				myString.put("cfs.psdzgenerateandstoresvtisterror", "PSdZ SVT-Ist lesen fehlerhaft");
				myString.put("cfs.psdzgenerateandstoresvtistneu", "PSdZ SVT-Neu lesen");
				myString.put("cfs.psdzgenerateandstoresvtistneuerror", "PSdZ SVT-Neu lesen fehlerhaft");
				myString.put("cfs.psdzgenerateandstoretal", "PSdZ TAL erzeugen");
				myString.put("cfs.psdzgenerateandstoretalerror", "PSdZ TAL erzeugen fehlerhaft");
				myString.put("cfs.psdzgetdeviceparamserror", "PSdZ Ger�teparameter fehlerhaft");
				myString.put("cfs.psdzgetdeviceparamsinterfacenotsupportederror", "PSdZ Ger�teparameter Diagnose-Interface nicht unterst�tzt");
				myString.put("cfs.psdzgetfafromvehicle", "PSdZ Fahrzeugauftrag lesen");
				myString.put("cfs.psdzgetfafromvehiclebackup", "PSdZ Fahrzeugauftrag Backup lesen");
				myString.put("cfs.psdzgetfafromvehicleerror", "PSdZ Fahrzeugauftrag lesen fehlerhaft");
				myString.put("cfs.psdzgetinstalledistufen", "PSdZ installierte I-Stufen lesen");
				myString.put("cfs.psdzgetinstalledistufenerror", "PSdZ installierte I-Stufen lesen fehlerhaft");
				myString.put("cfs.psdzgetistepfromvehicle", "PSdZ I-Stufe lesen");
				myString.put("cfs.psdzgetistepfromvehiclebackup", "PSdZ I-Stufe Backup lesen");
				myString.put("cfs.psdzgetistepfromvehicleerror", "PSdZ I-Stufe lesen fehlerhaft");
				myString.put("cfs.psdzgetsvtist", "PSdZ SVT-Ist holen");
				myString.put("cfs.psdzgetsvtisterror", "PSdZ SVT-Ist holen fehlerhaft");
				myString.put("cfs.psdzgetsvtistneu", "PSdZ SVT-Neu holen");
				myString.put("cfs.psdzgetsvtistneuerror", "PSdZ SVT-Neu holen fehlerhaft");
				myString.put("cfs.psdzgetsvtsoll", "PSdZ SVT-Soll holen");
				myString.put("cfs.psdzgetsvtsollerror", "PSdZ SVT-Soll holen fehlerhaft");
				myString.put("cfs.psdzgettal", "PSdZ TAL lesen");
				myString.put("cfs.psdzgettalerror", "PSdZ TAL lesen fehlerhaft");
				myString.put("cfs.psdzgetvehiclevinsfromb2v", "PSdZ Fahrgestellnummern lesen");
				myString.put("cfs.psdzgetvehiclevinsfromb2verror", "PSdZ Fahrgestellnummern lesen fehlerhaft");
				myString.put("cfs.psdzgetvinfromvehicle", "PSdZ Fahrgestellnummer lesen");
				myString.put("cfs.psdzgetvinfromvehiclebackup", "PSdZ Fahrgestellnummer Backup lesen");
				myString.put("cfs.psdzgetvinfromvehicleerror", "Ung�ltige fahrgestellnummer");
				myString.put("cfs.psdzhelp", "Sollte nie vorkommen. PSdZ ist null");
				myString.put("cfs.psdzinit", "PSdZ Initialisierung");
				myString.put("cfs.psdziniterror", "PSdZ Initialisierung fehlerhaft");
				myString.put("cfs.psdzopenvehicleconnection", "PSdZ Fahrzeugverbindung herstellen");
				myString.put("cfs.psdzopenvehicleconnectionerror", "PSdZ Fahrzeugverbindung herstellen fehlerhaft");
				myString.put("cfs.psdzsetistufesoll", "PSdZ I-Stufe setzen");
				myString.put("cfs.psdzsetistufesollerror", "PSdZ I-Stufe setzen fehlerhaft");
				myString.put("cfs.psdzsetsvtsoll", "PSdZ SVT-Soll setzen");
				myString.put("cfs.psdzsetsvtsollerror", "PSdZ SVT-Soll setzen fehlerhaft");
				myString.put("cfs.psdzstarttalexecution", "PSdZ TAL starten");
				myString.put("cfs.psdzstarttalexecutionerror", "PSdZ TAL starten fehlerhaft");
				myString.put("cfs.psdzupdateistep", "PSdZ I-Stufe schreiben");
				myString.put("cfs.psdzupdateistepeerror", "PSdZ I-Stufe schreiben fehlerhaft");
				myString.put("cfs.readerror", "Lesefehler");
				myString.put("cfs.replace", "Zu tauschen:");
				myString.put("cfs.replacenotallowederror", "Im Stichprobenbetrieb ist kein SG-Tausch erlaubt");
				myString.put("cfs.running", "L�uft...");
				myString.put("cfs.sanumber", "Anzahl SA");
				myString.put("cfs.savefzglog", "Speichere Fahrzeugprotokoll");
				myString.put("cfs.savetrace", "Trace speichern");
				myString.put("cfs.savetraceerror", "Trace speichern fehlerhaft");
				myString.put("cfs.selectoneSGBMID", "Partielle Programmierung;Bitte w�hlen Sie die SWFL die nicht programmiert werden soll");
				myString.put("cfs.selectonelieferumfangnummer", "Bitte eine Lieferumfangsnummer ausw�hlen");
				myString.put("cfs.selectonevin", "Bitte eine Fahrgestellnummer ausw�hlen");
				myString.put("cfs.selectwarning", "Auswahl Warnung");
				myString.put("cfs.service", "Service");
				myString.put("cfs.sgbmid", "SGBMID");
				myString.put("cfs.sgbmidnotfounderror", "SGBMID nicht gefunden");
				myString.put("cfs.sgflashlisteerror", "SG Flash Liste fehlerhaft");
				myString.put("cfs.sgflashlistehelp", "Bitte �berpr�fen Sie die SG Flash Konfigurationsdatei");
				myString.put("cfs.sgignorlisteerror", "SG Ignor Liste fehlerhaft");
				myString.put("cfs.sgignorlistehelp", "Bitte �berpr�fen Sie die SG Ignor Konfigurationsdatei");
				myString.put("cfs.sgnoflashlisteerror", "SG NoFlash Liste fehlerhaft");
				myString.put("cfs.sgnoflashlistehelp", "Bitte �berpr�fen Sie die SG NoFlash Konfigurationsdatei");
				myString.put("cfs.sollistufe", "Soll I-Stufe");
				myString.put("cfs.svkistlisteerror", "Ung�ltige SVK-Ist-Liste");
				myString.put("cfs.svkistlistehelp", "Sollte nie vorkommen. SVK-Ist-Liste ist null");
				myString.put("cfs.svklisteerror", "Ung�ltige SVK-Liste");
				myString.put("cfs.svklistehelp", "Sollte nie vorkommen. SVK-Liste ist null");
				myString.put("cfs.svksolllisteerror", "Ung�ltige SVK-Soll-Liste");
				myString.put("cfs.svksolllistehelp", "Sollte nie vorkommen. SVK-Soll-Liste ist null");
				myString.put("cfs.talerror", "Ung�ltige TAL");
				myString.put("cfs.talhelp", "Sollte nie vorkommen. TAL ist null");
				myString.put("cfs.tallineerror", "Ung�ltige TAL-Zeile");
				myString.put("cfs.tallinehelp", "Sollte nie vorkommen. TAL-Zeile ist null");
				myString.put("cfs.talnotemptyerror", "TAL nicht leer");
				myString.put("cfs.talnotemptyhelp", "Im Stichprobenbetrieb muss die TAL leer sein");
				myString.put("cfs.talta", "TAL Transaktion");
				myString.put("cfs.taltacategory", "TAL Transaktionskategorie");
				myString.put("cfs.tomuchzgwfounderror", "Zu viele Gateways gefunden");
				myString.put("cfs.tracefileelementerror", "Ung�ltiges Traceelement");
				myString.put("cfs.tracefileelementhelp", "Sollte nie vorkommen. Traceelement ist null");
				myString.put("cfs.typ", "Typschl�ssel");
				myString.put("cfs.unexpectedecuerror", "Unerwartetes Steuerger�t");
				myString.put("cfs.unexpectedinstalldeinstallerror", "Unerwarteter SG-Einbau oder SG-Ausbau");
				myString.put("cfs.unexpectedsgbmiderror", "Unerwartete SGBMID gefunden");
				myString.put("cfs.unknown", "unbekannt");
				myString.put("cfs.userbreakerror", "Abbruch durch Anwender");
				myString.put("cfs.verifysvkerror", "SVK-Soll - SVK-Ist Vergleichsfehler");
				myString.put("cfs.verifysvkprogdeperror", "SVK-Ist Programming Dependencies Checked Fehler");
				myString.put("cfs.verifysvt", "SVT-Soll - SVT-Ist Vergleich");
				myString.put("cfs.verifysvterror", "SVT-Soll - SVT-Ist Vergleichsfehler");
				myString.put("cfs.versionb2v", "B2V     Version");
				myString.put("cfs.versioncascade", "CASCADE Version");
				myString.put("cfs.versioncfs", "CFS     Version");
				myString.put("cfs.versionkis", "KIS AL  Version");
				myString.put("cfs.versionkiswb", "KIS WB  Version");
				myString.put("cfs.versionmcd", "MCD     Version");
				myString.put("cfs.versionpsdz", "PSDZ    Version");
				myString.put("cfs.versionjava", "JAVA    Version");
				myString.put("cfs.vin", "FGNR");
				myString.put("cfs.vinlengthis17help", "Fahrgestellnummer 17-stellig");
				myString.put("cfs.vinselect", "Fahrgestellnummer Auswahl");
				myString.put("cfs.waiting", "Warte...");
				myString.put("cfs.werk", "Werk");
				myString.put("cfs.writeerror", "Schreibfehler");
				myString.put("cfs.xmlformaterror", "TAL XML Format Fehler");
				myString.put("cfs.xmlformathelp", "Sollte nie vorkommen. Token swDeployTA nicht gefunden");
				myString.put("cfs.zgwinuseerror", "Zentrales Gateway bereits in Benutzung");
				// PSdZ Tal-Event-Texte");
				myString.put("cfs.prepareTALExecution_started", "Vorbereitungen TAL Abarbeitung gestartet");
				myString.put("cfs.prepareTALExecution_finished", "Vorbereitungen TAL Abarbeitung beendet");
				myString.put("cfs.prepareVehicleForFlash_started", "Vorbereitungen Fahrzeug Flash gestartet");
				myString.put("cfs.prepareVehicleForFlash_finished", "Vorbereitungen Fahrzeug Flash beendet");
				myString.put("cfs.TA_started", "TA gestartet");
				myString.put("cfs.TA_finished", "TA beendet");
				myString.put("cfs.prepareECUforBLUpdate_started", "Vorbereitungen ECU BL-Update gestartet");
				myString.put("cfs.prepareECUforBLUpdate_finished", "Vorbereitungen ECU BL-Update beendet");
				myString.put("cfs.authenticateECUforBLUpdate_started", "Authentisierung ECU BL-Update gestartet");
				myString.put("cfs.authenticateECUforBLUpdate_finished", "Authentisierung ECU BL-Update beendet");
				myString.put("cfs.performECUBLUpdate_started_with_protocol_UDS", "Ausf�hrung ECU BL-Update gestartet");
				myString.put("cfs.finalizeECUBLUpdate_started", "Nachbereitung ECU BL-Update gestartet");
				myString.put("cfs.finalizeECUBLUpdate_finished", "Nachbereitung ECU BL-Update beendet");
				myString.put("cfs.installECUBL_started", "Installation ECU Bootloader gestartet");
				myString.put("cfs.installECUBL_finished", "Installation ECU Bootloader beendet");
				myString.put("cfs.prepareECUforFlash_started", "Vorbereitung ECU Flash gestartet");
				myString.put("cfs.prepareECUforFlash_finished", "Vorbereitung ECU Flash beendet");
				myString.put("cfs.authenticateECUforFlash_started", "Authentisierung ECU Flash gestartet");
				myString.put("cfs.authenticateECUforFlash_finished", "Authentisierung ECU Flash beendet");
				myString.put("cfs.performEcuFlash_started_with_protocol_UDS", "Ausf�hrung ECU Flash gestartet");
				myString.put("cfs.finalizeECUFlash_started", "Nachbereitung ECU Flash gestartet");
				myString.put("cfs.finalizeECUFlash_error", "Nachbereitung ECU Flash fehlerhaft");
				myString.put("cfs.finalizeECUFlash_finished", "Nachbereitung ECU Flash beendet");
				myString.put("cfs.finalizeVehicleFlash_started", "Nachbereitung Fahrzeug Flash gestartet");
				myString.put("cfs.finalizeVehicleFlash_finished", "Nachbereitung Fahrzeug Flash beendet");
				myString.put("cfs.finalizeTALExecution_started", "Nachbereitung TAL Abarbeitung gestartet");
				myString.put("cfs.finalizeTALExecution_finished", "Nachbereitung TAL Abarbeitung beendet");
				myString.put("cfs.TAL_execution_finished", "TAL Abarbeitung beendet");
			} else {
				// englisch
				myString.put("cfs.addresshelp", "Address format 2-digit hex '00' - 'FF'");
				myString.put("cfs.alarmmessage", "Alarm message");
				myString.put("cfs.anlieferprogrammierungohne", "Partial ECU flash without");
				myString.put("cfs.anlieferzustand", "Partial ECU flash");
				myString.put("cfs.baureihe", "Series");
				myString.put("cfs.baustand", "Coding date");
				myString.put("cfs.bootupdatenotallowederror", "Bootloader update not allowed by configuration");
				myString.put("cfs.cancel", "Cancel");
				myString.put("cfs.checksgflashorsgignorhelp", "Please check ECU flash with ECU ignore configuration file");
				myString.put("cfs.checksgflashorsgnoflashhelp", "Please check ECU flash with ECU no flash configuration file");
				myString.put("cfs.checksgignororsgnoflashhelp", "Please check ECU ignore with ECU no flash configuration file");
				myString.put("cfs.checksgxxxxlist", "Check ECU configuration list");
				myString.put("cfs.checksvkconfigerror", "Verfication SVK towards configuration error");
				myString.put("cfs.checksvksgverbauerror", "Verfication SVK towards ECU built configuration error");
				myString.put("cfs.checksvksvkerror", "Check SVK-Ist SVK-Soll error");
				myString.put("cfs.checksvtist", "Verify SVT-Ist");
				myString.put("cfs.checksvtisterror", "Verfication SVT-Ist error");
				myString.put("cfs.checksvtistneu", "Verfiy SVT-Ist-new");
				myString.put("cfs.checksvtistneuerror", "Verfication SVT-Ist-new error");
				myString.put("cfs.checksvtistsvtsoll", "Check SVT-Ist SVT-Soll");
				myString.put("cfs.checksvtsoll", "Verfiy SVT-Soll");
				myString.put("cfs.checksvtsollerror", "Verfication SVT-Soll error");
				myString.put("cfs.checksvtsvterror", "Check SVT-Ist SVT-Soll error");
				myString.put("cfs.checktal", "Verify TAL");
				myString.put("cfs.checktalerror", "Verfication TAL error");
				myString.put("cfs.configuration", "Configuration");
				myString.put("cfs.copy", "Cop&y");
				myString.put("cfs.debug", "Debug");
				myString.put("cfs.debugLevel", "Debug Level");
				myString.put("cfs.disconnect", "Please disconnect ECU");
				myString.put("cfs.disconnectecu", "Disconnect ECU");
				myString.put("cfs.ecu", "ECU");
				myString.put("cfs.ecuaddressmultipleusedehelp", "Verify ECU address in control file");
				myString.put("cfs.ecuaddressmultipleusederror", "Multiple use of ECU adress found");
				myString.put("cfs.eculisteerror", "Invalid ECU list");
				myString.put("cfs.eculistehelp", "Should never occur. ECU list empty or null");
				myString.put("cfs.ecunamehelp", "Allowed characters '0-9a-zA-Z' Special characters ' _/()'");
				myString.put("cfs.ecunotfounderror", "ECU not found");
				myString.put("cfs.ecunotinsvtsollerror", "Inconsistent ECU built configuration with KIS SVT-Soll");
				myString.put("cfs.empty", "Empty");
				myString.put("cfs.emptyeculisteerror", "ECU list empty");
				myString.put("cfs.emptyistufenlisteerror", "Empty I-Level list error");
				myString.put("cfs.emptysvklisteerror", "SVK-Soll list empty");
				myString.put("cfs.emptytal", "TAL empty");
				myString.put("cfs.emptytallineerror", "Invalid line in TAL");
				myString.put("cfs.emptytallinehelp", "Should never occur. TAL line is empty");
				myString.put("cfs.entryformathelp", "Format ECUn=EcuName,xx n=0..255 xx='00'-'FF'");
				myString.put("cfs.entrynotvaliderror", "Invalid entry");
				myString.put("cfs.entwicklung", "Entwicklung");
				myString.put("cfs.eof", "eof");
				myString.put("cfs.ewnumber", "Nos. of E word");
				myString.put("cfs.fa", "Vehicle order");
				myString.put("cfs.filechanged", "File changed");
				myString.put("cfs.flashstart", "Start programming?");
				myString.put("cfs.gettalexecutionstatus", "PSdZ TAL read status");
				myString.put("cfs.gettalexecutionstatuserror", "PSdZ TAL read status error");
				myString.put("cfs.gwaddressmultipleusedehelp", "Verify gateway addresses in control file");
				myString.put("cfs.gwaddressmultipleusederror", "Multiple use of gateway address");
				myString.put("cfs.gwecuaddressmultipleusedehelp", "Verify gateway addresses in control file");
				myString.put("cfs.gwecuaddressmultipleusederror", "Verify gateway address and ECU address in control files");
				myString.put("cfs.gwlisteerror", "Invalid gateway list");
				myString.put("cfs.gwlistehelp", "Should never occur. Gateway list is null");
				myString.put("cfs.gwnamehelp", "Allowed characters '0-9a-zA-Z' Special characters ' _/()'");
				myString.put("cfs.gwnotfounderror", "Gateway not found");
				myString.put("cfs.gwtbupdatenotallowederror", "GWTB update not allowed by configuration");
				myString.put("cfs.headlinesfa", "Vehicle order");
				myString.put("cfs.headlinesgflashlist", "ECU flash list");
				myString.put("cfs.headlinesgignorlist", "ECU ignore list");
				myString.put("cfs.headlinesgnoflashlist", "ECU no flash list");
				myString.put("cfs.headlinesgverbaulist", "ECU built configuration list");
				myString.put("cfs.headlinesvtist", "SVT-Ist");
				myString.put("cfs.headlinesvtistneu", "SVT-New");
				myString.put("cfs.headlinesvtsoll", "SVT-Soll");
				myString.put("cfs.headlinetal", "TAL");
				myString.put("cfs.honumber", "Nos. of HO word");
				myString.put("cfs.ibaupdatenotallowederror", "IBA update not allowed by configuration");
				myString.put("cfs.ignore", "&Ignore");
				myString.put("cfs.ignored", "Ignored:");
				myString.put("cfs.ilevel", "I-Level");
				myString.put("cfs.inconsistentsgflashwithsgignorerror", "Inconsistent configuration ECU flash with ECU ignore");
				myString.put("cfs.inconsistentsgflashwithsgnoflasherror", "Inconsistent configuration ECU flash with ECU no flash");
				myString.put("cfs.inconsistentsgignorwithsgnoflasherror", "Inconsistent configuration ECU ignore with ECU no flash");
				myString.put("cfs.inline", "in line");
				myString.put("cfs.instance", "Instance");
				myString.put("cfs.instanceerror", "Instance error");
				myString.put("cfs.instancehelp1", "Supported instances = 2");
				myString.put("cfs.instancehelp2", "Computer name must be defined");
				myString.put("cfs.invalidaddresserror", "Invalid address");
				myString.put("cfs.invalidcategoryerror", "Invalid category");
				myString.put("cfs.invalidecunameerror", "Invalid ECU name");
				myString.put("cfs.invalidecunr", "Erroneous number of ECUs");
				myString.put("cfs.invalidecunrhelp", "Verfiy entry ECUs=x towards ECU0 .. ECUx-1 in control file");
				myString.put("cfs.invalidfa", "Invalid vehicle order");
				myString.put("cfs.invalidformat", "Invalid format");
				myString.put("cfs.invalidformathelp", "Please check configuration file");
				myString.put("cfs.invalidgwnameerror", "Invalid gateway name");
				myString.put("cfs.invalidgwnr", "Erroneous number of gateways");
				myString.put("cfs.invalidgwnrhelp", "Verfiy entry GWs=x towards GW0 .. GWx-1 in control file");
				myString.put("cfs.invalidilevel", "Invalid I-Level");
				myString.put("cfs.invalidilevelhelp", "I-Level format 'F001-08-09-400'");
				myString.put("cfs.invalidkiswbversionerror", "Invalid KIS WB version list");
				myString.put("cfs.invalidkiswbversionhelp", "No valid KIS WB versions available");
				myString.put("cfs.invalidmcdproject", "Invalid MCD project");
				myString.put("cfs.invalidmcdprojecthelp", "MCD project format 'F001_08_09_400'");
				myString.put("cfs.invalidnameerror", "Invalid computer name");
				myString.put("cfs.invalidprocessclasserror", "Invalid process class");
				myString.put("cfs.invalidprogdeperror", "Invalid programming dependencies checked");
				myString.put("cfs.invalidselectedSGBMID", "Invalid selected SGBMID;Please select the SWFL not to flash");
				myString.put("cfs.invalidseries", "Invalid series");
				myString.put("cfs.invalidserieshelp", "Series format 'F01'");
				myString.put("cfs.invalidsgbmidlist", "Invalid SGBMID list");
				myString.put("cfs.invalidvehicleinfo", "Invalid vehicle info");
				myString.put("cfs.invalidvehicleinfohelp", "Vehicle info format 'F001_08_09_400'");
				myString.put("cfs.invalidvin", "Invalid VIN");
				myString.put("cfs.ististufe", "Actual  I-Level");
				myString.put("cfs.keynotfounderror", "Key word not found");
				myString.put("cfs.keyonlycapitallettershelp", "Key words allowed in upcase characters only");
				myString.put("cfs.kiscalculationlevelerror", "Invalid PSDZ__KIS_CALCULATION_LEVEL");
				myString.put("cfs.kiscalculationlevelhelp", "Please check teststand variable PSDZ__KIS_CALCULATION_LEVEL");
				myString.put("cfs.kislevelkiswberror", "Invalid combination PSDZ__KIS_CALCULATION_LEVEL and KIS WB");
				myString.put("cfs.kislevelkiswbhelp", "Please check KIS WB and teststand variable PSDZ__KIS_CALCULATION_LEVEL");
  				myString.put("cfs.kisverifywarning", "Verify KIS");
  				myString.put("cfs.kisverifymessage", "Due to KIS data error, it can happen; that after programming to be programmed again.;It will be checked whether the new SVK will not be reprogrammed.;See PSdZ Traces KIS_SVKSOLL_1..., KIS_SVKSOLL_2...;;Please inform the system administrator.");
				myString.put("cfs.lack", "Paint code");
				myString.put("cfs.logfileelementerror", "Log file element error");
				myString.put("cfs.logfileelementhelp", "Should never occur. Log file element is null");
				myString.put("cfs.missing", "Missed:");
				myString.put("cfs.missingaddresserror", "Missing addresse");
				myString.put("cfs.missingecuerror", "Unexpected ECU install");
				myString.put("cfs.missingecunameerror", "Missing ECU name");
				myString.put("cfs.missinggwnameerror", "Missing gateway name");
				myString.put("cfs.modifytal", "Modify TAL");
				myString.put("cfs.modifytalerror", "Modify TAL error");
				myString.put("cfs.msgerrordeinstall1", "Following ECU should be deinstalled:");
				myString.put("cfs.msgerrordeinstall2", "Following ECUs should be deinstalled:");
				myString.put("cfs.msgerrorinstall1", "Following ECU should be installed:");
				myString.put("cfs.msgerrorinstall2", "Following ECUs should be installed:");
				myString.put("cfs.msgerrormissing1", "Following ECU missing:");
				myString.put("cfs.msgerrormissing2", "Following ECUs missing");
				myString.put("cfs.msgerrorreplace1", "Following ECU should be replaced:");
				myString.put("cfs.msgerrorreplace2", "Following ECUs should be replaced:");
				myString.put("cfs.msgflash1", "Following ECU will be flashed:");
				myString.put("cfs.msgflash2", "Following ECUs will be flashed:");
				myString.put("cfs.msgignore1", "Following ECU will be ignored:");
				myString.put("cfs.msgignore2", "Following ECUs will be ignored:");
				myString.put("cfs.msgmissing1", "Following ECU missing:");
				myString.put("cfs.msgmissing2", "Following ECUs missing:");
				myString.put("cfs.msgmustreplace1", "Following ECU has to be mandatory replaced:");
				myString.put("cfs.msgmustreplace2", "Following ECUs has to be mandatory replaced:");
				myString.put("cfs.msgreplace1", "Following ECU has to be replaced:");
				myString.put("cfs.msgreplace2", "Following ECUs has to be replaced:");
				myString.put("cfs.name", "Computer name");
				myString.put("cfs.noeculisteerror", "ECU list is null");
				myString.put("cfs.noerror", "");
				myString.put("cfs.nohelp", "");
				myString.put("cfs.noistufenlisteerror", "No I-Level list error");
				myString.put("cfs.nomatchingilevel", "No matching I-Level");
				myString.put("cfs.nosgbmidlisteerror", "SGBMID list error");
				myString.put("cfs.nosvklisteerror", "SVK-Soll list is null");
				myString.put("cfs.nozgwfounderror", "No ZGW found");
				myString.put("cfs.nozgwfoundhelp", "Check ZGW ethernet connection");
				myString.put("cfs.number", "Number");
				myString.put("cfs.oknosave", "OK &not saving");
				myString.put("cfs.polster", "Upholstery code");
				myString.put("cfs.progdepchecked", "ProgDepChecked");
				myString.put("cfs.psdzcanceltalexecution", "PSdZ cancel TAL");
				myString.put("cfs.psdzclose", "PSdZ close");
				myString.put("cfs.psdzcloseerror", "PSdZ close error");
				myString.put("cfs.psdzclosevehicleconnection", "PSdZ close vehicle connection");
				myString.put("cfs.psdzclosevehicleconnectionerror", "PSdZ close vehicle connection error");
				myString.put("cfs.psdzerror", "PSdZ error");
				myString.put("cfs.psdzexceptionsgeterror", "PSdZ get exceptions error");
				myString.put("cfs.psdzgenerateandstoreecusuccessors", "PSdZ generate logistic part number list");
				myString.put("cfs.psdzgenerateandstoreecusuccessorsemptyhashtableerror", "PSdZ logistic part number is empty");
				myString.put("cfs.psdzgenerateandstoreecusuccessorserror", "PSdZ generate logistic part number error");
				myString.put("cfs.psdzgenerateandstoreecusuccessorsinvalidlunerror", "PSdZ invalid logistic part number");
				myString.put("cfs.psdzgenerateandstoreecusuccessorsnohashtableerror", "PSdZ logistic part number list is null");
				myString.put("cfs.psdzgenerateandstoreecusuccessorsstorelunerror", "Storing logistic part number error");
				myString.put("cfs.psdzgenerateandstoresollverbauung", "PSdZ set SVT-Soll");
				myString.put("cfs.psdzgenerateandstoresollverbauungerror", "PSdZ set SVT-Soll error");
				myString.put("cfs.psdzgenerateandstoresollverbauungroh", "PSdZ set SVT-Soll raw");
				myString.put("cfs.psdzgenerateandstoresollverbauungroherror", "PSdZ set SVT-Soll raw error");
				myString.put("cfs.psdzgenerateandstoresvtist", "PSdZ read SVT-Ist");
				myString.put("cfs.psdzgenerateandstoresvtisterror", "PSdZ read SVT-Ist error");
				myString.put("cfs.psdzgenerateandstoresvtistneu", "PSdZ read SVT-new");
				myString.put("cfs.psdzgenerateandstoresvtistneuerror", "PSdZ read SVT-new error");
				myString.put("cfs.psdzgenerateandstoretal", "PSdZ generate TAL");
				myString.put("cfs.psdzgenerateandstoretalerror", "PSdZ generate TAL error");
				myString.put("cfs.psdzgetdeviceparamserror", "PSdZ get device params error");
				myString.put("cfs.psdzgetdeviceparamsinterfacenotsupportederror", "PSdZ get device params interface not supported error");
				myString.put("cfs.psdzgetfafromvehicle", "PSdZ get vehicle order from vehicle");
				myString.put("cfs.psdzgetfafromvehiclebackup", "PSdZ get vehicle order from vehicle backup");
				myString.put("cfs.psdzgetfafromvehicleerror", "PSdZ get vehicle order from vehicle error");
				myString.put("cfs.psdzgetinstalledistufen", "PSdZ get installed I-Levels");
				myString.put("cfs.psdzgetinstalledistufenerror", "PSdZ get installed I-Level error");
				myString.put("cfs.psdzgetistepfromvehicle", "PSdZ get I-Level from vehicle");
				myString.put("cfs.psdzgetistepfromvehiclebackup", "PSdZ get I-Level from vehicle backup");
				myString.put("cfs.psdzgetistepfromvehicleerror", "PSdZ get I-Level from vehicle error");
				myString.put("cfs.psdzgetsvtist", "PSdZ get SVT-Ist");
				myString.put("cfs.psdzgetsvtisterror", "PSdZ get SVT-Ist error");
				myString.put("cfs.psdzgetsvtistneu", "PSdZ get SVT-new");
				myString.put("cfs.psdzgetsvtistneuerror", "PSdZ get SVT-new error");
				myString.put("cfs.psdzgetsvtsoll", "PSdZ get SVT-Soll");
				myString.put("cfs.psdzgetsvtsollerror", "PSdZ get SVT-Soll error");
				myString.put("cfs.psdzgettal", "PSdZ read TAL");
				myString.put("cfs.psdzgettalerror", "PSdZ read TAL error");
				myString.put("cfs.psdzgetvehiclevinsfromb2v", "PSdZ read VIN");
				myString.put("cfs.psdzgetvehiclevinsfromb2verror", "PSdZ read VIN error");
				myString.put("cfs.psdzgetvinfromvehicle", "PSdZ get VIN from vehicle");
				myString.put("cfs.psdzgetvinfromvehiclebackup", "PSdZ get VIN from vehicle backup");
				myString.put("cfs.psdzgetvinfromvehicleerror", "PSdZ get VIN from vehicle");
				myString.put("cfs.psdzhelp", "Should never occur. PSdZ is null");
				myString.put("cfs.psdzinit", "PSdZ initialization");
				myString.put("cfs.psdziniterror", "PSdZ initialization error");
				myString.put("cfs.psdzopenvehicleconnection", "PSdZ open vehicle connection");
				myString.put("cfs.psdzopenvehicleconnectionerror", "PSdZ open vehicle connection error");
				myString.put("cfs.psdzsetistufesoll", "PSdZ set I-Level");
				myString.put("cfs.psdzsetistufesollerror", "PSdZ set I-Level error");
				myString.put("cfs.psdzsetsvtsoll", "PSdZ set SVT-Soll");
				myString.put("cfs.psdzsetsvtsollerror", "PSdZ set SVT-Soll error");
				myString.put("cfs.psdzstarttalexecution", "PSdZ start TAL");
				myString.put("cfs.psdzstarttalexecutionerror", "PSdZ start TAL error");
				myString.put("cfs.psdzupdateistep", "PSdZ update I-Level");
				myString.put("cfs.psdzupdateistepeerror", "PSdZ update I-Level error");
				myString.put("cfs.readerror", "Read error");
				myString.put("cfs.replace", "To replace:");
				myString.put("cfs.replacenotallowederror", "On sampling mode replace ECU not allowed");
				myString.put("cfs.running", "Running...");
				myString.put("cfs.sanumber", "Nos. of SA");
				myString.put("cfs.savefzglog", "Save vehicle log");
				myString.put("cfs.savetrace", "Save trace");
				myString.put("cfs.savetraceerror", "Save trace error");
				myString.put("cfs.selectoneSGBMID", "Partial ECU flash;Please select the SWFL not to flash");
				myString.put("cfs.selectonelieferumfangnummer", "Please select a logistic part number");
				myString.put("cfs.selectonevin", "Please select a VIN");
				myString.put("cfs.selectwarning", "Select warning");
				myString.put("cfs.service", "Service");
				myString.put("cfs.sgbmid", "SGBMID");
				myString.put("cfs.sgbmidnotfounderror", "SGBMID not found");
				myString.put("cfs.sgflashlisteerror", "ECU flash configuration list error");
				myString.put("cfs.sgflashlistehelp", "Please check ECU flash configuration file");
				myString.put("cfs.sgignorlisteerror", "ECU ignore configuration list error");
				myString.put("cfs.sgignorlistehelp", "Please check ECU ignore configuration file");
				myString.put("cfs.sgnoflashlisteerror", "ECU no flash configuration list error");
				myString.put("cfs.sgnoflashlistehelp", "Please check ECU no flash configuration file");
				myString.put("cfs.sollistufe", "Nominal I-Level");
				myString.put("cfs.svkistlisteerror", "Invalid SVK-Ist list");
				myString.put("cfs.svkistlistehelp", "Should never occur. SVK-Ist list is null");
				myString.put("cfs.svklisteerror", "Invalid SVK list");
				myString.put("cfs.svklistehelp", "Should never occur. SVK list is null");
				myString.put("cfs.svksolllisteerror", "Invalid SVK-Soll list");
				myString.put("cfs.svksolllistehelp", "Should never occur. SVK-Soll list is null");
				myString.put("cfs.talerror", "Invalid TAL");
				myString.put("cfs.talhelp", "Should never occur. TAL is null");
				myString.put("cfs.tallineerror", "Invalid TAL line");
				myString.put("cfs.tallinehelp", "Should never occur. TAL line is null");
				myString.put("cfs.talnotemptyerror", "TAL not empty");
				myString.put("cfs.talnotemptyhelp", "On sampling mode the TAL has to be empty");
				myString.put("cfs.talta", "TAL trans action");
				myString.put("cfs.taltacategory", "TAL trans action category");
				myString.put("cfs.tomuchzgwfounderror", "Too many gateways found");
				myString.put("cfs.tracefileelementerror", "Invalid trace element");
				myString.put("cfs.tracefileelementhelp", "Should never occur. Trace element is null");
				myString.put("cfs.typ", "Model code");
				myString.put("cfs.unexpectedecuerror", "Unexpected ECU");
				myString.put("cfs.unexpectedinstalldeinstallerror", "Unexpected ECU install or ECU deinstall");
				myString.put("cfs.unexpectedsgbmiderror", "Unexpected SGBMID found");
				myString.put("cfs.unknown", "Unknown");
				myString.put("cfs.userbreakerror", "Break by user");
				myString.put("cfs.verifysvkerror", "SVK-Soll - SVK-Ist comparison error");
				myString.put("cfs.verifysvkprogdeperror", "SVK-Ist programming dependencies checked error");
				myString.put("cfs.verifysvt", "SVT-Soll - SVT-Ist comparison");
				myString.put("cfs.verifysvterror", "SVT-Soll - SVT-Ist comparison error");
				myString.put("cfs.versionb2v", "B2V     version");
				myString.put("cfs.versioncascade", "CASCADE version");
				myString.put("cfs.versioncfs", "CFS     version");
				myString.put("cfs.versionkis", "KIS AL  version");
				myString.put("cfs.versionkiswb", "KIS WB  version");
				myString.put("cfs.versionmcd", "MCD     version");
				myString.put("cfs.versionpsdz", "PSDZ    version");
				myString.put("cfs.versionjava", "JAVA    version");
				myString.put("cfs.vin", "VIN");
				myString.put("cfs.vinlengthis17help", "VIN always 17-digits");
				myString.put("cfs.vinselect", "Select VIN");
				myString.put("cfs.waiting", "Waiting...");
				myString.put("cfs.werk", "Werk");
				myString.put("cfs.writeerror", "Write error");
				myString.put("cfs.xmlformaterror", "TAL XML Format error");
				myString.put("cfs.xmlformathelp", "Should never occur. Token swDeployTA not found");
				myString.put("cfs.zgwinuseerror", "Central gate way still in use");
				// PSdZ Tal-Event-Texte");
				myString.put("cfs.prepareTALExecution_started", "Prepare TAL execution started");
				myString.put("cfs.prepareTALExecution_finished", "Prepare TAL execution finished");
				myString.put("cfs.prepareVehicleForFlash_started", "Prepare vehicle for flash started");
				myString.put("cfs.prepareVehicleForFlash_finished", "Prepare vehicle for flash finished");
				myString.put("cfs.TA_started", "TA started");
				myString.put("cfs.TA_finished", "TA finished");
				myString.put("cfs.prepareECUforBLUpdate_started", "Prepare ECU for BL update started");
				myString.put("cfs.prepareECUforBLUpdate_finished", "Prepare ECU for BL update finished");
				myString.put("cfs.authenticateECUforBLUpdate_started", "Authenticate ECU for BL update started");
				myString.put("cfs.authenticateECUforBLUpdate_finished", "Authenticate ECU for BL update finished");
				myString.put("cfs.performECUBLUpdate_started_with_protocol_UDS", "Perform ECU BL update started");
				myString.put("cfs.finalizeECUBLUpdate_started", "Finalize ECU BL update started");
				myString.put("cfs.finalizeECUBLUpdate_finished", "Finalize ECU BL update finished");
				myString.put("cfs.installECUBL_started", "Install ECU BL started");
				myString.put("cfs.installECUBL_finished", "Install ECU BL finished");
				myString.put("cfs.prepareECUforFlash_started", "Prepare ECU for flash started");
				myString.put("cfs.prepareECUforFlash_finished", "Prepare ECU for flash finished");
				myString.put("cfs.authenticateECUforFlash_started", "Authenticate ECU for flash started");
				myString.put("cfs.authenticateECUforFlash_finished", "Authenticate ECU for flash finished");
				myString.put("cfs.performEcuFlash_started_with_protocol_UDS", "Perform ECU flash started");
				myString.put("cfs.finalizeECUFlash_started", "Finalize ECU flash started");
				myString.put("cfs.finalizeECUFlash_error", "Finalize ECU flash error");
				myString.put("cfs.finalizeECUFlash_finished", "Finalize ECU flash finished");
				myString.put("cfs.finalizeVehicleFlash_started", "Finalize vehicle flash started");
				myString.put("cfs.finalizeVehicleFlash_finished", "Finalize vehicle flash finished");
				myString.put("cfs.finalizeTALExecution_started", "Finalize TAL execution started");
				myString.put("cfs.finalizeTALExecution_finished", "Finalize TAL execution finished");
				myString.put("cfs.TAL_execution_finished", "TAL execution finished");
			}
		}
		
		public static String get( String key ) {
			try {
				String s = myString.get(key);
				if (s != null) return s;
				System.out.println("CFS: myString key not found : ->"+key+"<-");
				return key;
			} catch( Exception e ) {
				System.out.println("CFS: myString key not found : ->"+key+"<-");
				return key;
			}
		}
	}
	

	/**
	 * CFS Cascade Flash System <br>
	 * Eigene Klasse <br>
	 * Task zum dynamischen Anzeigen der Stoppuhr <br>
	 * @author BMW AG TI-545 Drexel <br>
	 * @version 27.06.2008 <br>
	 */
	private class StopWatchTask extends TimerTask {
		private String myHeadLine = null;
		private String myMessage = null;
		private UserDialog myUserDialog = null;
		private Cfs myCfs = null;
		private boolean myMessageFlag = false;

		/**
		 * Konstruktor f�r den Task
		 *
		 * @param mHeadLine
		 * @param mMessage
		 * @param myUserDialog
		 * @param myCfs
		 * @param myMessageFlag
		 */
		public StopWatchTask(String mHeadLine, String mMessage, UserDialog mUserDialog,  Cfs mCfs, boolean mMessageFlag) {
			myHeadLine    = mHeadLine;
			myMessage     = mMessage;
			myUserDialog  = mUserDialog;
			myCfs         = mCfs;
			myMessageFlag = mMessageFlag;
		}

		public void run() {
			String msg = null;
			int n = 0;
			try {
				msg = myMessage;
				if (msg == null) msg="";
				n = msg.indexOf("...");
				if (n != -1)
					msg = msg.substring(0, n+3) + myCfs.getStopWatch() + msg.substring(n+3);
				else
					msg = myCfs.getStopWatch() + ";" + msg;
				if (myMessageFlag)
					myUserDialog.displayMessage(myHeadLine, msg , -1 );
				else
					myUserDialog.displayAlertMessage(myHeadLine, msg , -1 );
			} catch (Exception e) {
				this.cancel();
			} catch (Throwable f) {
				this.cancel();
			}
		}
	}

	/**
	 * CFS Cascade Flash System <br>
	 * Eigene Klasse <br>
	 * @author BMW AG TI-545 Drexel <br>
	 * @version 17.07.2007 <br>
	 */
	private class ParseListeElement {
		private String name = null;
		private boolean negiert = false;

		ParseListeElement(String name, boolean negiert, boolean wildcard) {
			this.name=name;
			this.negiert=negiert;
		}
	}

	/**
	 * CFS Cascade Flash System <br>
	 * Eigene Klasse <br>
	 * @author BMW AG TI-545 Drexel <br>
	 * @version 17.07.2007 <br>
	 */
	private class ParseElement {
		String  istIStufe = null;
		String  sollIStufe = null;
		boolean autoIStufe = false;
		ArrayList<String> adressListe = new ArrayList<String>(); // Adress-Liste aus 2-stelligen Hex-String
		ArrayList<ParseListeElement> saListe = new ArrayList<ParseListeElement>();     // SA-Liste aus ParseListeElement
		ArrayList<ParseListeElement> typListe = new ArrayList<ParseListeElement>();    // TYP-Liste aus ParseListeElement
		ArrayList<ParseListeElement> sgbmIdListe = new ArrayList<ParseListeElement>(); // SGBMID-Liste aus ParseListeElement
	}

	/**
	 * CFS Cascade Flash System <br>
	 * Eigene Klasse <br>
	 * @author BMW AG TI-545 Drexel <br>
	 * @version 14.01.2010 <br>
	 */
	private class EcuListeElement {
		private String name = null;
		private int[] address = null;
		private int[] SVKIstIndex = null;
		private int[] programmingDependencies = null;
		private int[] SVKSollIndex = null;
		private String[] baseVariantName = null;
		private String[] variantName = null;
		private String[] lieferUmfangNummer = null;
		private String[] sgbmIdAnlieferzustand = null;

		public String toString() {
			int i;
			String temp=CRLF;
			temp=temp+"name=";
			if (name==null)
				temp=temp+"null"+CRLF;
			else
				temp=temp+name+CRLF;
			temp=temp+"address=";
			if (address==null)
				temp=temp+"null"+CRLF;
			else {
				temp=temp+"["+ address.length + "]{";
				for (i=0; i<address.length; i++) {
					if (i>0) temp=temp+",";
					temp=temp+"0x"+Integer.toHexString(address[i]);
				}
				temp=temp+"}"+CRLF;
			}
			temp=temp+"SVKIstIndex=";
			if (SVKIstIndex==null)
				temp=temp+"null"+CRLF;
			else {
				temp=temp+"["+ SVKIstIndex.length + "]{";
				for (i=0; i<SVKIstIndex.length; i++) {
					if (i>0) temp=temp+",";
					temp=temp+SVKIstIndex[i];
				}
				temp=temp+"}"+CRLF;
			}
			temp=temp+"programmingDependencies=";
			if (programmingDependencies==null)
				temp=temp+"null"+CRLF;
			else {
				temp=temp+"["+ programmingDependencies.length + "]{";
				for (i=0; i<programmingDependencies.length; i++) {
					if (i>0) temp=temp+",";
					temp=temp+"0x"+Integer.toHexString(programmingDependencies[i]);
				}
				temp=temp+"}"+CRLF;
			}
			temp=temp+"SVKSollIndex=";
			if (SVKSollIndex==null)
				temp=temp+"null"+CRLF;
			else {
				temp=temp+"["+ SVKSollIndex.length + "]{";
				for (i=0; i<SVKSollIndex.length; i++) {
					if (i>0) temp=temp+",";
					temp=temp+SVKSollIndex[i];
				}
				temp=temp+"}"+CRLF;
			}
			temp=temp+"baseVariantName=";
			if (baseVariantName==null)
				temp=temp+"null"+CRLF;
			else {
				temp=temp+"["+ baseVariantName.length + "]{";
				for (i=0; i<baseVariantName.length; i++) {
					if (i>0) temp=temp+",";
					if (baseVariantName[i]==null)
						temp=temp+"null";
					else
						temp=temp+baseVariantName[i];
				}
				temp=temp+"}"+CRLF;
			}
			temp=temp+"variantName=";
			if (variantName==null)
				temp=temp+"null"+CRLF;
			else {
				temp=temp+"["+ variantName.length + "]{";
				for (i=0; i<variantName.length; i++) {
					if (i>0) temp=temp+",";
					if (variantName[i]==null)
						temp=temp+"null";
					else
						temp=temp+variantName[i];
				}
				temp=temp+"}"+CRLF;
			}
			temp=temp+"lieferUmfangNummer=";
			if (lieferUmfangNummer==null)
				temp=temp+"null"+CRLF;
			else {
				temp=temp+"["+ lieferUmfangNummer.length + "]{";
				for (i=0; i<lieferUmfangNummer.length; i++) {
					if (i>0) temp=temp+",";
					if (lieferUmfangNummer[i]==null)
						temp=temp+"null";
					else
						temp=temp+lieferUmfangNummer[i];
				}
				temp=temp+"}"+CRLF;
			}
			temp=temp+"sgbmIdAnlieferzustand=";
			if (sgbmIdAnlieferzustand==null)
				temp=temp+"null"+CRLF;
			else {
				temp=temp+"["+ sgbmIdAnlieferzustand.length + "]{";
				for (i=0; i<sgbmIdAnlieferzustand.length; i++) {
					if (i>0) temp=temp+",";
					if (sgbmIdAnlieferzustand[i]==null)
						temp=temp+"null";
					else
						temp=temp+sgbmIdAnlieferzustand[i];
				}
				temp=temp+"}"+CRLF;
			}
			return temp;
		}
	}

	/**
	 * CFS Cascade Flash System <br>
	 * Eigene Klasse <br>
	 * @author BMW AG TI-545 Drexel <br>
	 * @version 14.01.2010 <br>
	 */
	private class TraceFileElement {
		private boolean okayFlag = false;
		private boolean abbruchFlag = false;
		private int address = 0;
		private long timeMillisGesamt = 0;
		private long timeMillisVorbereitungStart = 0;
		private long timeMillisVorbereitungFertig = 0;
		private long timeMillisNachbereitungStart = 0;
		private long timeMillisNachbereitungFertig = 0;
		private String vin = null;
		private String iStufe = null;
		private String name = null;
		private String baseVariantName = null;
		private String variantName = null;
		private String lieferUmfangNummer = null;
		private String sgbmIdAnlieferzustand = null;
		private String zusatzMessage = null;
		private String[] SGBMID = null;
		private CascadeTAL TAL = null;
		private long[] timeMillisTalLine = null;
		private boolean[] timeValidTalLine = null;
		private String[] lieferUmfangNummerTalLine = null;

		public String toString() {
			int i;
			String temp=CRLF;
			temp=temp+"okayFlag="+okayFlag+CRLF;
			temp=temp+"abbruchFlag="+abbruchFlag+CRLF;
			temp=temp+"address="+address+CRLF;
			temp=temp+CRLF;
			temp=temp+"timeMillisGesamt="+timeMillisGesamt+CRLF;
			temp=temp+"timeMillisVorbereitungStart="+timeMillisVorbereitungStart+CRLF;
			temp=temp+"timeMillisVorbereitungFertig="+timeMillisVorbereitungFertig+CRLF;
			if (timeMillisVorbereitungStart>0 && timeMillisVorbereitungFertig>0)
				temp=temp+"timeMillisVorbereitungDauer="+(timeMillisVorbereitungFertig-timeMillisVorbereitungStart)+CRLF;
			temp=temp+"timeMillisNachbereitungStart="+timeMillisNachbereitungStart+CRLF;
			temp=temp+"timeMillisNachbereitungFertig="+timeMillisNachbereitungFertig+CRLF;
			if (timeMillisNachbereitungStart>0 && timeMillisNachbereitungFertig>0)
				temp=temp+"timeMillisNachbereitungDauer="+(timeMillisNachbereitungFertig-timeMillisNachbereitungStart)+CRLF;
			temp=temp+CRLF;
			temp=temp+"vin="+vin+CRLF;
			temp=temp+"iStufe="+iStufe+CRLF;
			temp=temp+"name="+name+CRLF;
			temp=temp+"baseVariantName="+baseVariantName+CRLF;
			temp=temp+"variantName="+variantName+CRLF;
			temp=temp+"lieferUmfangNummer="+lieferUmfangNummer+CRLF;
			temp=temp+"zusatzMessage="+zusatzMessage+CRLF;
			temp=temp+CRLF;
			temp=temp+"SGBMID=";
			if (SGBMID==null)
				temp=temp+"null"+CRLF;
			else {
				temp=temp+"["+ SGBMID.length + "]{";
				for (i=0; i<SGBMID.length; i++) {
					if (i>0) temp=temp+",";
					temp=temp+SGBMID[i];
				}
				temp=temp+"}"+CRLF;
			}
			temp=temp+"TAL=";
			if (TAL==null)
				temp=temp+"null"+CRLF;
			else {
				if (TAL.getTalLines()==null)
					temp=temp+"null"+CRLF;
				else {
					temp=temp+"["+ TAL.getTalLines().length + "]{";
					for (i=0; i<TAL.getTalLines().length; i++) {
						if (i>0) temp=temp+",";
						temp=temp+TAL.getTalLines()[i].getDiagnosticAddress()+";";
						temp=temp+TAL.getTalLines()[i].getBaseVariantName()+";";
						temp=temp+TAL.getTalLines()[i].getStatus();
					}
					temp=temp+"}"+CRLF;
				}
			}
			temp=temp+"timeMillisTalLine=";
			if (timeMillisTalLine==null)
				temp=temp+"null"+CRLF;
			else {
				temp=temp+"["+ timeMillisTalLine.length + "]{";
				for (i=0; i<timeMillisTalLine.length; i++) {
					if (i>0) temp=temp+",";
					temp=temp+timeMillisTalLine[i];
				}
				temp=temp+"}"+CRLF;
			}
			temp=temp+"timeValidTalLine=";
			if (timeValidTalLine==null)
				temp=temp+"null"+CRLF;
			else {
				temp=temp+"["+ timeValidTalLine.length + "]{";
				for (i=0; i<timeValidTalLine.length; i++) {
					if (i>0) temp=temp+",";
					temp=temp+timeValidTalLine[i];
				}
				temp=temp+"}"+CRLF;
			}
			temp=temp+"lieferUmfangNummerTalLine=";
			if (lieferUmfangNummerTalLine==null)
				temp=temp+"null"+CRLF;
			else {
				temp=temp+"["+ lieferUmfangNummerTalLine.length + "]{";
				for (i=0; i<lieferUmfangNummerTalLine.length; i++) {
					if (i>0) temp=temp+",";
					temp=temp+lieferUmfangNummerTalLine[i];
				}
				temp=temp+"}"+CRLF;
			}
			return temp;
		}
	}

	/**
	 * CFS Cascade Flash System <br>
	 * Eigene Klasse <br>
	 * @author BMW AG TI-545 Drexel <br>
	 * @version 11.02.2010 <br>
	 */
	private class LogFileElement {
		private String fileName = null;
		private String fileNameVin = null;
		private String vehicleVin = null;
		private String istIStufe = null;
		private String sollIStufe = null;
		private String[] sgVerbauListe = null;
		private String[] sgIgnorListe = null;
		private String[] sgFlashListe = null;
		private String[] sgNoFlashListe = null;
		private CascadeFA FA = null;
		private CascadeSVT SVTIst = null;
		private CascadeSVT SVTSoll = null;
		private CascadeSVT SVTIstNeu = null;
		private CascadeTAL TAL = null;
		private CascadeLogistischesTeil[] bestellNrListe = null;
		private HashMap<String, String> programmingDependenciesListe = null;
		private HashMap<String, String> programmingDependenciesListeNeu = null;
	}

	/**
	 * CFS Cascade Flash System <br>
	 * Eigene Klasse <br>
	 * @author BMW AG TI-545 Drexel <br>
	 * @version 26.11.2015 <br>
	 */
	private class Cfs {
		// Konstanten
		private final String UNKNOWNISTUFE = "F000-00-00-000";
		private final String UNKNOWN = "???";
        private final String CONFIGPATH = "\\cfs\\config\\";
        // optional f�r die zweite Instanz
        private final String CONFIGPATH_2 = "\\cfs\\config_2\\";
		private final String TRACEPATH = "\\cfs\\trace\\";
		private final String PSDZ_SWxx_PATH = "\\PSdZ\\PSdZDaten\\psdzdata\\swe\\";
		private final String CFS_INI = "CFS.INI";
		private final String SG_LISTE_DAT = "SG_LISTE.DAT";
		private final String I_STUFE_DAT = "I_STUFE.DAT";
		private final String SG_VERBAU_DAT = "SG_VERBAU.DAT";
		private final String SG_IGNOR_DAT = "SG_IGNOR.DAT";
		private final String SG_FLASH_DAT = "SG_FLASH.DAT";
		private final String SG_NOFLASH_DAT = "SG_NOFLASH.DAT";
		// debug
		private final int debugPrintStackTrace = 1;
		private final int debugUpdateIStepinVCMorBackup = 2;
		private final int debugVerifySVT = 3;
		private final int debugShowStopWatch = 4;

		// noch Konstanten, k�nnten INI-Datei Eintr�ge werden
		private final int maxErrorRepeat = 1; // muss mindestens 1 sein

		// INI-Datei-Variablen
		private int debugLevel = 0;
		private int instanz = 0;
		private String rechnerName = null;
		private Properties iniFileProperties = new Properties();
		private boolean samplingFlag = false;
		private boolean trailerModeFlag = false;
		private boolean trailerMultiSelectFlag = false;
		private boolean trailerKisVerifyFlag=true;		
		private boolean apdmDetailFlag = true;
		private boolean useFzsFlag = false;
		private boolean parallelExecution = true;
		private boolean checkProgDeps = false;
		private boolean modeSwitch = true;
		private boolean verifySVTFlag = true;
		private boolean ignoreMissingEcus = false;
		private boolean allowBootLoaderUpdate = true;
		private boolean allowIBAUpdate = true;
		private boolean allowGWTBUpdate = true;
		private boolean automaticMode = false;
		private boolean automaticFindDataSgbmid = true;
		private boolean icomCanBridgeActiveFlag = false;
		private String zgwVin1 = null;
		private String zgwVin2 = null;
		private String interfaceIPAddress = null;
		private String kisCalculationLevel = "M"; // MONTAGEFORTSCHRITT
		private String defaultIStufe = null;

		// SG_LISTE-Datei-Variablen
		private Properties ecuListFileProperties = new Properties();
		private boolean trailerModeAnlieferzustandFlag = false;
		private String sollIStufe = null;
		private EcuListeElement[] ecuListe = null;
		private EcuListeElement[] gwListe = null;

		// SG_VERBAU-Datei-Variablen
		private ArrayList<String> sgVerbauListe = null;
		// SG_IGNOR-Datei-Variablen
		private ArrayList<String> sgIgnorListe = null;
		// SG_FLASH-Datei-Variablen
		private ArrayList<String> sgFlashListe = null;
		// SG_NOFLASH-Datei-Variablen
		private ArrayList<String> sgNoFlashListe = null;

		// TAL SG-Einbau-Liste
		private ArrayList<String> sgEinbauListe = null;
		// TAL SG-Ausbau-Liste
		private ArrayList<String> sgAusbauListe = null;
		// TAL SG-Zwangstausch-Liste
		private ArrayList<String> sgZwangsTauschListe = null;
		// TAL SG-Tausch-Liste
		private ArrayList<String> sgTauschListe = null;
		// TAL SG-Programmier-Liste
		private ArrayList<String> sgProgrammListe = null;

		// Progress
		private HashMap<?, ?> progressMap = null;

		// Stoppuhr
		private long startStopWatchTimeMillis = System.currentTimeMillis();
		private long deltaStopWatchTimeMillis = 0;

		// F�r Abfrage des letzten Fehlers oder letzte Hilfe
		private String lastError=CfsString.get("cfs.noerror");
		private String lastHelp=CfsString.get("cfs.nohelp");

		private Color backColor = new Color(255, 255, 128);
		private final int formatEinruecken = 19;
		private final int formatLaenge = formatEinruecken - 1;

		/**
		 * Liefert die Instanznummer <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 11.01.2007 <br>
		 */
		private int getInstanz() {
			return instanz;
		}

		/**
		 * Liefert die Fahrgestellnummer von ZGW1 <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 11.01.2007 <br>
		 */
		private String getZgwVin1() {
			return zgwVin1;
		}

		/**
		 * Liefert die Fahrgestellnummer von ZGW2 <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 11.01.2007 <br>
		 */
		private String getZgwVin2() {
			return zgwVin2;
		}

		/**
		 * Liefert die Default-I-Stufe <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 16.04.2008 <br>
		 */
		private String getDefaultIStufe() {
			return defaultIStufe;
		}

		/**
		 * Liefert die Soll-I-Stufe <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 11.01.2007 <br>
		 */
		private String getSollIStufe() {
			return sollIStufe;
		}

		/**
		 * Setzt die Soll-I-Stufe <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 08.05.2007 <br>
		 */
		private boolean setSollIStufe(String value) {
			if (isValidIStufe(value)) {
				sollIStufe=value;
				return true;
			}
			return false;
		}

		/**
		 * Liefert das samplingFlag <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 16.03.2007 <br>
		 */
		private boolean getSamplingFlag() {
			return samplingFlag;
		}

		/**
		 * Setzt das samplingFlag <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 16.03.2007 <br>
		 */
		private void setSamplingFlag(boolean value) {
			samplingFlag=value;
		}

		/**
		 * Liefert den KIS-Level <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 15.05.2009 <br>
		 */
		private String getKisCalculationLevel() {
			return kisCalculationLevel;
		}

		/**
		 * Setzt den KIS-Level <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 15.05.2009 <br>
		 */
		private void setKisCalculationLevel(String value) {
			if (value==null) {
				// M == MONTAGEFORTSCHRITT
				kisCalculationLevel="M";
			} else {
				if (value.length()==0) {
					// M == MONTAGEFORTSCHRITT
					kisCalculationLevel="M";
				} else {
					switch( value.toUpperCase().charAt(0) ) {
					// M == MONTAGEFORTSCHRITT
					case 'M':
						kisCalculationLevel = "M";
						break;
					// E == EINZELFLASH
					case 'E':
						kisCalculationLevel = "E";
						break;
					// G == GESAMTFLASH
					case 'G':
						kisCalculationLevel = "G";
						break;
					// default ist MONTAGEFORTSCHRITT
					default:
						kisCalculationLevel = "M";
					}
				}
			}
		}

		/**
		 * Liefert die Interface IP-Adresse <br>
		 * F�r EDICNET und ICOM <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 21.12.2016 <br>
		 */
		private String getInterfaceIPAddress() {
			return interfaceIPAddress;
		}

		/**
		 * Setzt die Interface IP-Adresse <br>
		 * F�r EDICNET und ICOM <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 21.12.2016 <br>
		 */
		private void setInterfaceIPAddress(String value) {
			interfaceIPAddress=value;
		}

		/**
		 * Liefert das icomCanBridgeActiveFlag <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 21.12.2016 <br>
		 */
		private boolean getIcomCanBridgeActiveFlag() {
			return icomCanBridgeActiveFlag;
		}

		/**
		 * Setzt das IcomCanBridgeActiveFlag <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 21.12.2016 <br>
		 */
		private void setIcomCanBridgeActiveFlag(boolean value) {
			icomCanBridgeActiveFlag=value;
		}
		
		/**
		 * Liefert das useFzsFlag <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 25.03.2008 <br>
		 */
		private boolean getUseFzsFlag() {
			return useFzsFlag;
		}

		/**
		 * Setzt das useFzsFlag <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 25.03.2008 <br>
		 */
		private void setUseFzsFlag(boolean value) {
			useFzsFlag=value;
		}

		/**
		 * Liefert das trailerModeFlag <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 08.05.2007 <br>
		 */
		private boolean getTrailerModeFlag() {
			return trailerModeFlag;
		}

		/**
		 * Liefert das trailerMultiSelectFlag <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 17.12.2014 <br>
		 */
		private boolean getTrailerMultiSelectFlag() {
			return trailerMultiSelectFlag;
		}

		/**
		 * Liefert das trailerKisVerifyFlag <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 05.11.2015 <br>
		 */
		private boolean getTrailerKisVerifyFlag() {
			return trailerKisVerifyFlag;
		}

		/**
		 * Liefert das trailerModeAnlieferzustandFlag <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 14.01.2010 <br>
		 */
		private boolean getTrailerModeAnlieferzustandFlag() {
			return trailerModeAnlieferzustandFlag;
		}

		/**
		 * Liefert das apdmDetailFlag <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 16.04.2009 <br>
		 */
		private boolean getApdmDetailFlag() {
			return apdmDetailFlag;
		}

		/**
		 * Liefert das verifySVTFlag <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 11.02.2010 <br>
		 */
		private boolean getVerifySVTFlag() {
			return verifySVTFlag;
		}

		/**
		 * Setzt das trailerModeFlag <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 08.05.2007 <br>
		 */
		private void setTrailerModeFlag(boolean value) {
			trailerModeFlag=value;
		}

		/**
		 * Liefert Automatik Mode <br>
		 * Wenn true werden UserDialoge nur f�r <br>
		 * AUTOMATIC_MESSAGE_TIMEOUT Sekunden angezeigt <br>
		 * Wenn false dauernd
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 07.02.2007 <br>
		 */
		private boolean getAutomaticMode() {
			return automaticMode;
		}

		/**
		 * Liefert Automatik Mode f�r Datenstandserkennung <br>
		 * f�r Trailer Anlieferprogrammierung <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 15.02.2010 <br>
		 */
		private boolean getAutomaticFindDataSgbmid() {
			return automaticFindDataSgbmid;
		}

		/**
		 * Liefert Steuerparamer f�r psdz startTALExecution <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 07.02.2007 <br>
		 */
		private boolean getParallelExecution() {
			return parallelExecution;
		}

		/**
		 * Liefert as Flag ignoreMissingEcus <br>
		 * Steuert ob bei fehlenden ECUs trotzdem programmiert wird <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 19.03.2008 <br>
		 */
		private boolean getIgnoreMissingEcus() {
			return ignoreMissingEcus;
		}

		/**
		 * Liefert Steuerparamer f�r psdz startTALExecution <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 07.02.2007 <br>
		 */
		private int getMaxErrorRepeat() {
			return maxErrorRepeat;
		}

		/**
		 * Liefert Steuerparamer f�r psdz Modeumschaltung <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 03.12.2009 <br>
		 */
		private boolean getModeSwitch() {
			return modeSwitch;
		}

		/**
		 * Liefert Steuerparamer f�r psdz setCheckProgDeps <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 21.11.2008 <br>
		 */
		private boolean getCheckProgDeps() {
			return checkProgDeps;
		}

		/**
		 * Liefert die Steuerger�te-Liste <br>
		 * 	String name <br>
		 * 	int[] address <br>
		 * 	int[] SVKIstIndex <br>
		 * 	int[] SVKSollIndex <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 11.01.2007 <br>
		 */
		private EcuListeElement[] getEcuListe() {
			return ecuListe;
		}

		/**
		 * Liefert die SG-Verbau-Liste <br>
		 * als 2-stellige Hex-String-Liste <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 30.05.2007 <br>
		 */
		private String[] getSgVerbauListe() {
			String[] temp=null;

			if (sgVerbauListe == null) return temp;
			if (sgVerbauListe.size() == 0) return temp;
			temp = new String[sgVerbauListe.size()];
			for ( int i=0; i<sgVerbauListe.size(); i++)
				temp[i]=(String)sgVerbauListe.get(i);
			return temp;
		}

		/**
		 * Liefert die SG-Ignor-Liste <br>
		 * als 2-stellige Hex-String-Liste <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 21.06.2007 <br>
		 */
		private String[] getSgIgnorListe() {
			String[] temp=null;

			if (sgIgnorListe == null) return temp;
			if (sgIgnorListe.size() == 0) return temp;
			temp = new String[sgIgnorListe.size()];
			for ( int i=0; i<sgIgnorListe.size(); i++)
				temp[i]=(String)sgIgnorListe.get(i);
			return temp;
		}

		/**
		 * Liefert die SGBMID-Liste aus der ecuListe <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 14.01.2010 <br>
		 */
		private String[] getSGBMIDListe(List<CascadeECU> ecuListe) {
			String[] temp=null;
			ArrayList<String> sgbmIdListe = new ArrayList<String>();

			sgbmIdListe = createSgbmIdListe(ecuListe);
			if ((sgbmIdListe != null) && (sgbmIdListe.size() > 0)) {
				temp = (String[])sgbmIdListe.toArray(new String[sgbmIdListe.size()]);
				Arrays.sort(temp);
			}
			return temp;
		}

		/**
		 * Liefert die SGBMIDSize-Liste <br>
		 * und h�ngt die Gr��e bei SWFL und SWFK in der SGBMID-Liste an <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 26.11.2015 <br>
		 */
		private long[] getSGBMIDSizeListe(String[] sgbmIdListe) {
			long[] sgbmIdSizeListe=null;
			int i;
			String dateiName = null;
			File file = null;

			if ((sgbmIdListe != null) && (sgbmIdListe.length > 0)) {
				sgbmIdSizeListe = new long[sgbmIdListe.length];
				for (i=0; i<sgbmIdListe.length; i++) {
					sgbmIdSizeListe[i]=0;
					if (isValidSgbmId(sgbmIdListe[i])) {
						if (sgbmIdListe[i].startsWith("SWFL") || sgbmIdListe[i].startsWith("SWFK")) {
							try {
								dateiName = CascadeProperties.getCascadeHome() + PSDZ_SWxx_PATH + (sgbmIdListe[i].startsWith("SWFL") ? "SWFL\\" : "SWFK\\") +
								            sgbmIdListe[i].substring( 0, 4) + "_" +
								            sgbmIdListe[i].substring( 5,13) + ".bin." +
								            sgbmIdListe[i].substring(14).replace('.','_');
								file = new File(dateiName);
								sgbmIdSizeListe[i] = file.length();
								sgbmIdListe[i] = sgbmIdListe[i] + "  (" + (sgbmIdSizeListe[i]/1024) + " kB)";
							} catch (Exception e) {
								// keine Fehlermeldung wenn nicht verf�gbar
							}
						}
					}
				}
			}
			return sgbmIdSizeListe;
		}

		/**
		 * Liefert die SGBMID des Datenstandes <br>
		 * Kriterium: mindestens 2 SWFL vorhanden <br>
		 * Die kleinste SWFL muss mindestens Faktor 2 kleiner als die anderen SWFL <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 15.02.2010 <br>
		 */
		private String getDataSgbmId( long[] sgbmIdSizeListe, String[] sgbmIdListe) {
			String dataSgbmId = null;
			int i;
			int index;
			long size;

			if (sgbmIdSizeListe == null) return dataSgbmId;
			if (sgbmIdSizeListe.length == 0) return dataSgbmId;
			if (sgbmIdListe == null) return dataSgbmId;
			if (sgbmIdListe.length == 0) return dataSgbmId;
			if (sgbmIdSizeListe.length != sgbmIdListe.length) return dataSgbmId;

			size=0;
			index = 0;
			for (i=0; i<sgbmIdSizeListe.length; i++) {
				if (sgbmIdSizeListe[i] > 0 && sgbmIdListe[i].startsWith("SWFL")) {
					if (size == 0) {
						index = i;
						size = sgbmIdSizeListe[i];     // Erste SGBMID
					} else {
						if (sgbmIdSizeListe[i] < size) {
							index = i;
							size = sgbmIdSizeListe[i]; // Kleinere SGBMID
						}
					}
				}
			}
			if (size > 0) {
				// Mindestabstand pr�fen
				for (i=0; i<sgbmIdSizeListe.length; i++) {
					if (sgbmIdSizeListe[i] > 0 && sgbmIdListe[i].startsWith("SWFL")) {
						if (i != index) {
							if (sgbmIdSizeListe[i] < size * 2) return null;
							dataSgbmId = sgbmIdListe[index].substring(0,25);
						}
					}
				}
			}
			return dataSgbmId;
		}

		/**
		 * Liefert die SG-Ignor-Liste abh�ngig von SVTIst und SVTSoll <br>
		 * als 2-stellige Hex-String-Liste <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 14.12.2007 <br>
		 */
		private String[] getSgIgnorListeSVT(List<CascadeECU> SVKIstListe, List<CascadeECU> SVKSollListe) {
			String[] temp=null;
			boolean usedSgAddress[] = new boolean[256];
			int address;
			int i;
			int n;

			if (sgIgnorListe == null) return temp;
			if (sgIgnorListe.size() == 0) return temp;

			for (i=0; i<256; i++) usedSgAddress[i]=false;
			if (SVKIstListe != null) {
				for (i=0; i<SVKIstListe.size(); i++) {
					address = ((CascadeECU)SVKIstListe.get(i)).getDiagnosticAddress().intValue();
					if (sgIgnorListe.contains(hexString(address,2))) {
						if (address>=0 && address<=255) usedSgAddress[address]=true;
					}
				}
			}
			if (SVKSollListe != null) {
				for (i=0; i<SVKSollListe.size(); i++) {
					address = ((CascadeECU)SVKSollListe.get(i)).getDiagnosticAddress().intValue();
					if (sgIgnorListe.contains(hexString(address,2))) {
						if (address>=0 && address<=255) usedSgAddress[address]=true;
					}
				}
			}
			n=0;
			for (i=0; i<256; i++) if (usedSgAddress[i]==true) n++;
			if (n==0) return temp;
			temp = new String[n];
			n=0;
			for (i=0; i<256; i++)
				if (usedSgAddress[i]==true) temp[n++]=hexString(i,2);
			return temp;
		}

		/**
		 * Liefert die SG-Flash-Liste <br>
		 * als 2-stellige Hex-String-Liste <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 21.06.2007 <br>
		 */
		private String[] getSgFlashListe() {
			String[] temp=null;

			if (sgFlashListe == null) return temp;
			if (sgFlashListe.size() == 0) return temp;
			temp = new String[sgFlashListe.size()];
			for ( int i=0; i<sgFlashListe.size(); i++)
				temp[i]=(String)sgFlashListe.get(i);
			return temp;
		}

		/**
		 * Liefert die SG-NoFlash-Liste <br>
		 * als 2-stellige Hex-String-Liste <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 21.06.2007 <br>
		 */
		private String[] getSgNoFlashListe() {
			String[] temp=null;

			if (sgNoFlashListe == null) return temp;
			if (sgNoFlashListe.size() == 0) return temp;
			temp = new String[sgNoFlashListe.size()];
			for ( int i=0; i<sgNoFlashListe.size(); i++)
				temp[i]=(String)sgNoFlashListe.get(i);
			return temp;
		}


		/**
		 * H�ngt zwei Listen zusammen <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 21.11.2008 <br>
		 */
		private String[] addListe(String[] liste1, String[] liste2) {
			int i;
			String[] temp=null;

			if (liste1==null) return temp;
			if (liste2==null) return temp;
			temp = new String[liste1.length + liste2.length];
			for (i=0; i<liste1.length; i++) {
				temp[i]=liste1[i];
			}
			for (i=0; i<liste2.length; i++) {
				temp[i+liste1.length]=liste2[i];
			}
			return temp;
		}



		/**
		 * Liefert true wenn bei Anlieferstandsprogrammieren, <br>
		 * der Fehler ausgeblendet werden soll <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 19.01.2010 <br>
		 */
		private boolean getFehlerAusblenden(CascadeTALLine TALLine) {
			boolean fehlerAusblendenFlag = false;

			if (!getTrailerModeAnlieferzustandFlag()) return fehlerAusblendenFlag;
			if (TALLine == null) return fehlerAusblendenFlag;
			if (TALLine.getExceptions() == null) return fehlerAusblendenFlag;
			if( TALLine.getExceptions().size() == 0 ) return fehlerAusblendenFlag;

			for(int k = 0; k < TALLine.getExceptions().size(); k++ ) {
				try {
					String temp = ((CascadePSdZRuntimeException)TALLine.getExceptions().get(k)).getPSdZMessage();
					if (temp.toLowerCase().indexOf("FinalizeECUFlash failed".toLowerCase()) != -1)
						if (temp.toLowerCase().indexOf("CheckProgrammingDependencies delivered incorrectResult".toLowerCase()) != -1) {
							fehlerAusblendenFlag = true;
							return fehlerAusblendenFlag;
						}
				} catch (Exception e) {
					// keine Fehlermeldung wenn nicht verf�gbar
				}
			}
			return fehlerAusblendenFlag;
		}

		/**
		 * Liefert die SG-ZwangsFlash-Liste <br>
		 * von SGs die bei Anlieferstandsprogrammieren, <br>
		 * auszublendende SGBMIDs enthalten <br>
		 * als 2-stellige Hex-String-Liste <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 26.02.2010 <br>
		 */
		private String[] getSgAnlieferstandFlashListe(List<CascadeECU> SVKIstListe, List<CascadeECU> SVKSollListe, EcuListeElement[] ecuListe) {
			String[] temp=null;
			int n;
			int swflIst = 0;
			int swflSoll = 0;
			boolean gefunden = false;
			boolean adrAnlieferstand[] = new boolean[256];

			if (!getTrailerModeAnlieferzustandFlag()) return temp;
			if (ecuListe==null) return temp;
			if (ecuListe.length==0) return temp;

			for (int i=0; i<adrAnlieferstand.length; i++) {
				adrAnlieferstand[i] = false;
			}
			n=0;
			for (int i=0; i<ecuListe.length; i++) {
				for (int k=0; k<ecuListe[i].address.length; k++) {
					// SGBM-IDs holen
					CascadeSGBMID[] SVKSollSGBMID=((CascadeECU)SVKSollListe.get(ecuListe[i].SVKSollIndex[k])).getSvk();
					String[] SGBMIDSoll = new String[SVKSollSGBMID.length];
					swflSoll = 0;
					for (int m=0; m<SGBMIDSoll.length; m++) {
						SGBMIDSoll[m] = SVKSollSGBMID[m].getProcessClass().toString() + "-" + SVKSollSGBMID[m].getId() + "-" +
						dezString(SVKSollSGBMID[m].getMainVersion(),3) + "." + dezString(SVKSollSGBMID[m].getSubVersion(),3) + "." + dezString(SVKSollSGBMID[m].getPatchVersion(),3);
						if (SGBMIDSoll[m].startsWith(CascadeProcessClasses.SWFL.toString())) {
							if (isValidSgbmId(ecuListe[i].sgbmIdAnlieferzustand[k])) {
								if (!SGBMIDSoll[m].equalsIgnoreCase(ecuListe[i].sgbmIdAnlieferzustand[k])) {
									swflSoll++;
								}
							} else {
								swflSoll++;
							}

						}
					}
					CascadeSGBMID[] SVKIstSGBMID=((CascadeECU)SVKIstListe.get(ecuListe[i].SVKIstIndex[k])).getSvk();
					String[] SGBMIDIst = new String[SVKIstSGBMID.length];
					swflIst = 0;
					for (int m=0; m<SGBMIDIst.length; m++) {
						SGBMIDIst[m] = SVKIstSGBMID[m].getProcessClass().toString() + "-" + SVKIstSGBMID[m].getId() + "-" +
						dezString(SVKIstSGBMID[m].getMainVersion(),3) + "." + dezString(SVKIstSGBMID[m].getSubVersion(),3) + "." + dezString(SVKIstSGBMID[m].getPatchVersion(),3);
						if (SGBMIDIst[m].startsWith(CascadeProcessClasses.SWFL.toString())) swflIst++;
					}
					// sgbmIdAnlieferzustand in der SVK-Ist suchen
					if (isValidSgbmId(ecuListe[i].sgbmIdAnlieferzustand[k])) {
						for (int m=0; m<SGBMIDIst.length; m++) {
							if (SGBMIDIst[m].equalsIgnoreCase(ecuListe[i].sgbmIdAnlieferzustand[k])) {
								// sgbmIdAnlieferzustand in der SVK-Ist gefunden
  								if (!adrAnlieferstand[ecuListe[i].address[k]]) {
  									n++;
  									adrAnlieferstand[ecuListe[i].address[k]] = true;
  								}
							}
						}
					}
					// �berpr�fung ob zu viele SWFL in SVK-Ist
					if (swflIst > swflSoll) {
						for (int m=0; m<SGBMIDIst.length; m++) {
							if (SGBMIDIst[m].startsWith(CascadeProcessClasses.SWFL.toString())) {
								gefunden = false;
								for (int l=0; l<SGBMIDSoll.length; l++) {
									if (SGBMIDIst[m].equalsIgnoreCase(SGBMIDSoll[l])) {
										gefunden = true;
										break;
									}
								}
								if (!gefunden) {
									// zu viele SWFL in SVK-Ist gefunden
	  								if (!adrAnlieferstand[ecuListe[i].address[k]]) {
	  									n++;
	  									adrAnlieferstand[ecuListe[i].address[k]] = true;
	  								}
								}
							}
						}
					}
				}
			}
			if (n == 0) return temp;
			temp = new String[n];
			n=0;
			for (int i=0; i<adrAnlieferstand.length; i++)
				if (adrAnlieferstand[i] == true) temp[n++]=hexString(i,2);
			return temp;
		}

		/**
		 * Liefert die SG-ZwangsFlash-Liste <br>
		 * von SGs deren CheckProgDep != 1 <br>
		 * die TAL aber f�r dieses SG leer ist <br>
		 * als 2-stellige Hex-String-Liste <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 20.01.2009 <br>
		 */
		private String[] getSgCheckProgDepFlashListe(CascadeTALLine[] TALLine, HashMap<String, String> programmingDependenciesListe) {
			return getSgCheckProgDepFlashListe(TALLine, programmingDependenciesListe, null);
		}

		private String[] getSgCheckProgDepFlashListe(CascadeTALLine[] TALLine, HashMap<String, String> programmingDependenciesListe, String[] ecuAddressList) {
			int i;
			int j;
			int n;
			String tmp;
			String[] temp=null;
			boolean adrProgDep[] = new boolean[256];

			if (programmingDependenciesListe==null) return temp;
			if (programmingDependenciesListe.size()==0)	return temp;
			// Alle SG-Adressen die einen CheckProgDep != 1 haben
			n = 0;
			for (i=0; i<adrProgDep.length; i++) {
				adrProgDep[i] = false;
				tmp = (String)programmingDependenciesListe.get(hexString(i,0));
				if (isValidSignedNr(tmp)) {
					if (Integer.parseInt(tmp) != 1) {
						boolean gefunden = true;
						if ((ecuAddressList != null) && (ecuAddressList.length > 0)) {
							gefunden = false;
							for (int k=0; k<ecuAddressList.length; k++) {
								if (Integer.decode("0x" + ecuAddressList[k]).intValue() == i) {
									gefunden = true;
									break;
								}
							}
						}
						if (gefunden) {
							adrProgDep[i] = true;
							n++;
						}
					}
				}
			}
			if (n == 0) return temp;
			// Alle SG-Adressen raus die in der TAL vorhanden sind
			if (TALLine != null) {
				for (i=0; i<TALLine.length; i++) {
					j = TALLine[i].getDiagnosticAddress().intValue();
					if (adrProgDep[j]== true) {
						adrProgDep[j]= false;
						n--;
					}
				}
			}
			if (n == 0) return temp;
			temp = new String[n];
			n=0;
			for (i=0; i<adrProgDep.length; i++)
				if (adrProgDep[i] == true) 	temp[n++]=hexString(i,2);
			return temp;
		}

		/**
		 * Liefert die SG-ZwangsTausch-Liste <br>
		 * Ist die SG-NoFlash-Liste ohne die Adressen, <br>
		 * die laut TAL nicht verbaut sind <br>
		 * als 2-stellige Hex-String-Liste <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 18.02.2010 <br>
		 */
		private String[] getSgZwangsTauschListe(CascadeTALLine[] TALLine) {
			int i;
			int j;
			String[] temp=null;

			if (sgNoFlashListe == null) return temp;
			if (sgNoFlashListe.size() == 0) return temp;
			if (TALLine != null) {
				sgZwangsTauschListe = new ArrayList<String>();
				for (i=0; i<sgNoFlashListe.size(); i++) {
					for (j=0; j<TALLine.length; j++) {
						if (( Integer.decode("0x" + ((String)sgNoFlashListe.get(i))).intValue()) == TALLine[j].getDiagnosticAddress().intValue()) {
							sgZwangsTauschListe.add(sgNoFlashListe.get(i));
							break;
						}
					}
				}
			}
			if (sgZwangsTauschListe == null) return temp;
			if (sgZwangsTauschListe.size() == 0) return temp;
			temp = new String[sgZwangsTauschListe.size()];
			for ( i=0; i<sgZwangsTauschListe.size(); i++)
				temp[i]=(String)sgZwangsTauschListe.get(i);
			return temp;
		}

		/**
		 * Liefert die SG-Einbau-Liste <br>
		 * als 2-stellige Hex-String-Liste <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 04.07.2007 <br>
		 */
		private String[] getSgEinbauListe() {
			String[] temp=null;

			if (sgEinbauListe == null) return temp;
			if (sgEinbauListe.size() == 0) return temp;
			temp = new String[sgEinbauListe.size()];
			for ( int i=0; i<sgEinbauListe.size(); i++)
				temp[i]=(String)sgEinbauListe.get(i);
			return temp;
		}

		/**
		 * Liefert die SG-Ausbau-Liste <br>
		 * als 2-stellige Hex-String-Liste <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 04.07.2007 <br>
		 */
		private String[] getSgAusbauListe() {
			String[] temp=null;

			if (sgAusbauListe == null) return temp;
			if (sgAusbauListe.size() == 0) return temp;
			temp = new String[sgAusbauListe.size()];
			for ( int i=0; i<sgAusbauListe.size(); i++)
				temp[i]=(String)sgAusbauListe.get(i);
			return temp;
		}

		/**
		 * Liefert die SG-Tausch-Liste <br>
		 * als 2-stellige Hex-String-Liste <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 04.07.2007 <br>
		 */
		private String[] getSgTauschListe() {
			String[] temp=null;

			if (sgTauschListe == null) return temp;
			if (sgTauschListe.size() == 0) return temp;
			temp = new String[sgTauschListe.size()];
			for ( int i=0; i<sgTauschListe.size(); i++)
				temp[i]=(String)sgTauschListe.get(i);
			return temp;
		}

		/**
		 * Liefert die SG-Programm-Liste <br>
		 * als 2-stellige Hex-String-Liste <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 04.07.2007 <br>
		 */
		private String[] getSgProgrammListe() {
			String[] temp=null;

			if (sgProgrammListe == null) return temp;
			if (sgProgrammListe.size() == 0) return temp;
			temp = new String[sgProgrammListe.size()];
			for ( int i=0; i<sgProgrammListe.size(); i++)
				temp[i]=(String)sgProgrammListe.get(i);
			return temp;
		}

		/**
		 * Erzeugt die Steuerger�te Adressliste <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 06.02.2007 <br>
		 */
		private String[] getEcuAddressList() {
			return getAddressList(ecuListe);
		}

		/**
		 * Erzeugt die Hex-String-Adressliste <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 12.04.2007 <br>
		 */
		private String[] getAddressList(EcuListeElement[] liste) {
			int i;
			int k;
			int n;

			if (liste == null) return new String[0];
			n = 0;
			for (i=0; i<liste.length; i++) {
				for (k=0; k<liste[i].address.length; k++) {
					n++;
				}
			}
			String[] addressList = new String[n];
			n = 0;
			for (i=0; i<liste.length; i++) {
				for (k=0; k<liste[i].address.length; k++) {
					addressList[n++]=Integer.toHexString(liste[i].address[k]);
				}
			}
			return addressList;
		}

		/**
		 * Letzte Fehlermeldung <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 15.12.2006 <br>
		 */
		private String getLastError() {
			return lastError;
		}

		/**
		 * Letzte Hilfemeldung <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 15.12.2006 <br>
		 */
		private String getLastHelp() {
			return lastHelp;
		}

		/**
		 * Erzeugt eine Pause von 50 - 5000 ms <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 11.01.2007 <br>
		 */
		private void pause(int dauer) {
			if (dauer<50) dauer=50;
			if (dauer>5000) dauer=5000;

			try {
	            Thread.sleep( dauer );
	        } catch( InterruptedException e ) {
	        }
		}

		/**
		 * Erzeugt einen hexString <br>
		 * Es wird ein digits-stelliger Hex-String mit f�hrenden Nullen erzeugt <br>
		 * digits = 1 .. 16 <br>
		 * Bei digits = 0 werden keine f�hrenden Nullen erzeugt <br>
		 *
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 07.02.2007 <br>
		 */
		private String hexString(long x, int digits) {
			if (digits < 0) digits=16;
			if (digits > 16) digits=16;

			String s=Long.toHexString(x).toUpperCase();
			if (digits >0) {
				s = "0000000000000000"+s;
				s = s.substring(s.length()-digits,s.length());
			}
			return s;
		}

		/**
		 * Erzeugt einen dezimal-String <br>
		 * Es wird ein digits-stelliger dezimal-String mit f�hrenden Nullen erzeugt <br>
		 * digits = 1 .. 20 <br>
		 * Bei digits = 0 oder x < 0 werden keine f�hrenden Nullen erzeugt <br>
		 *
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 15.02.2007 <br>
		 */
		private String dezString(long x, int digits) {
			if (digits < 0) digits=20;
			if (digits > 20) digits=20;

			String s = ""+x;
			if ((x>=0) && (digits >0)) {
				s = "00000000000000000000"+s;
				s = s.substring(s.length()-digits,s.length());
			}
			return s;
		}

		/**
		 * �berpr�ft einen Textstring auf Nummerformat. <br>
		 * 'x' x='0'..'9' <br>
		 *
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 05.09.2008 <br>
		 */
		private boolean isValidNr(String checkNr, int digits) {
			int i;

			if (checkNr == null)
				return false;
			if (checkNr.length() == 0)
				return false;
			if (digits < 0)
				return false;
			if (digits > 0) {
				if (checkNr.length() != digits)
					return false;
			}
			for (i=0; i<checkNr.length(); i++) {
				if ("0123456789".indexOf(checkNr.charAt(i)) == -1)
					return false;
			}
			return true;
		}

		/**
		 * �berpr�ft einen Textstring auf Nummerformat. <br>
		 * 'x' x='+','-','0'..'9' <br>
		 *
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 20.12.2006 <br>
		 */
		private boolean isValidSignedNr(String checkNr) {
			int i;

			if (checkNr == null)
				return false;
			if (checkNr.length() == 0)
				return false;

			if ("+-0123456789".indexOf(checkNr.charAt(0)) == -1)
				return false;
			for (i=1; i<checkNr.length(); i++) {
				if ("0123456789".indexOf(checkNr.charAt(i)) == -1)
					return false;
			}
			return true;
		}

		/**
		 * �berpr�ft einen Textstring auf g�ltige Lieferumfangsnummer <br>
		 *
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 16.01.2007 <br>
		 */
		private boolean isValidLieferUmfangNummer(String lieferUmfangNummer) {
			return isValidNr(lieferUmfangNummer,7);
		}

		/**
		 * �berpr�ft einen Textstring auf g�ltige positive Anzahl innerhalb min und max. <br>
		 *
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 20.12.2006 <br>
		 */
		private boolean isValidAnzahl(String checkAnzahl, int min, int max) {
			int n=0;
			if (isValidNr(checkAnzahl, 0)) {
				n = Integer.parseInt(checkAnzahl);
				if (n < min) return false;
				if (n > max) return false;
				return true;
			}
			return false;
		}

		/**
		 * �berpr�ft eine Textstringliste auf SA-Listenformat. <br>
		 * 'xxx' x='A'..'Z','0'..'9' <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 14.05.2007 <br>
		 */
		private boolean isValidSaListe(String[] checkSaListe) {
			int n;
			int i;

			if (checkSaListe == null)
				return false;
			for (n=0; n<checkSaListe.length; n++) {
				if (checkSaListe[n].length() != 3)
					return false;
				for (i=0; i<3; i++) {
					if ("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(checkSaListe[n].charAt(i)) == -1)
						return false;
				}
			}
			return true;
		}

		/**
		 * �berpr�ft eine Textstringliste auf HO-Listenformat. <br>
		 * 'xxxx' x='A'..'Z','0'..'9' <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 14.05.2007 <br>
		 */
		private boolean isValidHoListe(String[] checkHoListe) {
			int n;
			int i;

			if (checkHoListe == null)
				return false;
			for (n=0; n<checkHoListe.length; n++) {
				if (checkHoListe[n].length() != 4)
					return false;
				for (i=0; i<4; i++) {
					if ("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(checkHoListe[n].charAt(i)) == -1)
						return false;
				}
			}
			return true;
		}

		/**
		 * �berpr�ft eine Textstringliste auf E-Wort-Listenformat. <br>
		 * 'xxxx' x='A'..'Z','0'..'9' <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 14.05.2007 <br>
		 */
		private boolean isValidEwListe(String[] checkEwListe) {
			int n;
			int i;

			if (checkEwListe == null)
				return false;
			for (n=0; n<checkEwListe.length; n++) {
				if (checkEwListe[n].length() != 4)
					return false;
				for (i=0; i<4; i++) {
					if ("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(checkEwListe[n].charAt(i)) == -1)
						return false;
				}
			}
			return true;
		}

		/**
		 * �berpr�ft einen Textstring auf Typschl�sselformat. <br>
		 * 'xxxx' x='A'..'Z','0'..'9' <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 14.05.2007 <br>
		 */
		private boolean isValidTypSchluessel(String checkTypSchluessel) {
			int i;

			if (checkTypSchluessel == null)
				return false;
			if (checkTypSchluessel.length() != 4)
				return false;
			for (i=0; i<4; i++) {
				if ("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(checkTypSchluessel.charAt(i)) == -1)
					return false;
			}
			return true;
		}

		/**
		 * �berpr�ft einen Textstring auf Baureihenformat. <br>
		 * �berpr�ft einen Textstring auf Baureihenformat. <br>
		 * 'Xxx' oder 'Xxxx' X='A'..'Z' x='A'..'Z','0'..'9' <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 18.02.2016 <br>
		 */
		private boolean isValidBaureihe(String checkBaureihe) {
			int i;

			if( checkBaureihe == null )
				return false;
			if((checkBaureihe.length() != 3) && (checkBaureihe.length() != 4))
				return false;
			if( ("ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz").indexOf( checkBaureihe.charAt( 0 ) ) == -1 )
				return false;
			for( i = 1; i < checkBaureihe.length(); i++ ) {
				if( ("0123456789" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz").indexOf( checkBaureihe.charAt( i ) ) == -1 )
					return false;
			}
			return true;
		}

		/**
		 * Normalisiert einen Baureihenstring <br>
		 * Wenn Baureihenstringl�nge == 4 <br>
		 * dann wird die erste '0' entfernt <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 13.06.2007 <br>
		 */
		private String normalizeBaureihe(String inBaureihe) {
			if (inBaureihe.length() == 4) {
				if (inBaureihe.charAt(1) == '0') return inBaureihe.substring(0,1)+inBaureihe.substring(2);
				if (inBaureihe.charAt(2) == '0') return inBaureihe.substring(0,2)+inBaureihe.substring(3);
			}
			return inBaureihe;
		}

		/**
		 * �berpr�ft einen Textstring auf I-Stufenformat. <br>
		 * 'XXXX-xx-xx-xxx' <br>
		 * X='A'..'Z','0'..'9' <br>
		 * x='0'..'9' <br>
		 * Wenn wildCard true dann ist auch '?' erlaubt <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 29.03.2012 <br>
		 */
		private boolean isValidIStufe(String checkIStufe) {
			return isValidIStufe(checkIStufe, false);
		}

		private boolean isValidIStufe(String checkIStufe, boolean wildCard) {
			int i;
			String w="";

			if (checkIStufe == null)
				return false;
			if (checkIStufe.length() != 14)
				return false;
			if (wildCard) w="?";
			for (i=0; i<4; i++) {
				if (("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"+w).indexOf(checkIStufe.charAt(0)) == -1)
					return false;
			}
			for (i=4; i<14; i++) {
				if ((i == 4) || (i == 7) || (i == 10)) {
					if (checkIStufe.charAt(i) != '-')
						return false;
				} else {
					if (("0123456789"+w).indexOf(checkIStufe.charAt(i)) == -1)
						return false;
				}
			}
			return true;
		}

		/**
		 * CFS Trailer Konfiguration <br>
		 * �berpr�ft ob debug. <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 17.06.2008 <br>
		 */
		private boolean isDebug(int level) {
			if (debugLevel == 0)
				return false;
			if (level < 1)
				return false;
			if (level > 16)
				return false;
			if ((debugLevel & (1 << (level-1))) == 0)
				return false;
			return true;
		}

		/**
		 * CFS Trailer Konfiguration <br>
		 * �berpr�ft einen Textstring auf SG-Adressformat. <br>
		 * 'xx' x='0'..'9','A'..'F' <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 20.12.2006 <br>
		 */
		private boolean isValidAdresse(String checkAdresse) {
			if (checkAdresse == null)
				return false;
			if (checkAdresse.length() != 2)
				return false;
			if ("0123456789ABCDEF".indexOf(checkAdresse.charAt(0)) == -1)
				return false;
			if ("0123456789ABCDEF".indexOf(checkAdresse.charAt(1)) == -1)
				return false;
			return true;
		}

		/**
		 * �berpr�ft einen Textstring auf SGBD-ID-Format. <br>
		 * Steuerger�tebeschreibungsmodell <br>
		 * 'TTTT-HHHHHHHH-DDD.DDD.DDD'<br>
		 * TTTT = 'HWEL' | 'HWAP' | 'HWFR' | 'CAFD' | 'BTLD' | 'FLSL' | 'SWFK' | 'SWFL' | 'IBAD' | 'GWTB' <br>
		 * H ='0'..'9','A'..'F' <br>
		 * D ='0'..'9' <br>
		 * Wenn wildCard true dann ist auch '?' erlaubt <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 13.01.2014 <br>
		 */
		private boolean isValidSgbmId(String checkSgbmId) {
			return isValidSgbmId(checkSgbmId, false);
		}

		private boolean isValidSgbmId(String checkSgbmId, boolean wildCard) {
			int i;
			String w="";

			if (checkSgbmId == null)
				return false;
			if (checkSgbmId.length() != 25)
				return false;
			if (wildCard) w="?";
			if (!wildCard("HWEL",checkSgbmId.substring(0,4)).equals(checkSgbmId.substring(0,4)))
				if (!wildCard("HWAP",checkSgbmId.substring(0,4)).equals(checkSgbmId.substring(0,4)))
					if (!wildCard("HWFR",checkSgbmId.substring(0,4)).equals(checkSgbmId.substring(0,4)))
						if (!wildCard("CAFD",checkSgbmId.substring(0,4)).equals(checkSgbmId.substring(0,4)))
							if (!wildCard("BTLD",checkSgbmId.substring(0,4)).equals(checkSgbmId.substring(0,4)))
								if (!wildCard("FLSL",checkSgbmId.substring(0,4)).equals(checkSgbmId.substring(0,4)))
									if (!wildCard("SWFK",checkSgbmId.substring(0,4)).equals(checkSgbmId.substring(0,4)))
										if (!wildCard("SWFL",checkSgbmId.substring(0,4)).equals(checkSgbmId.substring(0,4)))
											if (!wildCard("IBAD",checkSgbmId.substring(0,4)).equals(checkSgbmId.substring(0,4)))
												if (!wildCard("GWTB",checkSgbmId.substring(0,4)).equals(checkSgbmId.substring(0,4)))
													return false;
			for (i=4; i<25; i++) {
				if ((i == 4) || (i == 13)) {
					if (("-").indexOf(checkSgbmId.charAt(i)) == -1)
						return false;
				} else {
					if ((i == 17) || (i == 21)) {
						if ((".").indexOf(checkSgbmId.charAt(i)) == -1)
							return false;
					} else {
						if (("0123456789"+(i<13?"ABCDEF":"")+w).indexOf(checkSgbmId.charAt(i)) == -1)
							return false;
					}
				}
			}
			return true;
		}

		/**
		 * �berpr�ft einen Fahrzeugauftrag auf plausibilit�t. <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 29.05.2007 <br>
		 */
		private boolean isValidFA( CascadeFA checkFA) {
			if (checkFA == null)											return false;
			if (!isValidBaureihe(normalizeBaureihe(checkFA.getBaureihe())))	return false;
			if (!isValidTypSchluessel(checkFA.getTypSchluessel()))			return false;
			if (!isValidSaListe(checkFA.getSaListe()))						return false;
			if (!isValidHoListe(checkFA.getHoListe()))						return false;
			if (!isValidEwListe(checkFA.getEwListe()))						return false;

			// keine Detail�berpr�fung, da aktuell keine Funktionalit�t dahinter steckt
			if (checkFA.getZeitkriterium() == null)							return false;
			if (checkFA.getLack() == null)									return false;
			if (checkFA.getPolster() == null)								return false;
			return true;
		}

		/**
		 * �berpr�ft eine Fahrgestellnummer auf plausibilit�t. <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 09.05.2007 <br>
		 */
		private boolean isValidVIN( String checkVIN) {
			if (checkVIN == null)
				return false;
			if (checkVIN.length() != 17)
				return false;
			return true;
		}

		/**
		 * �berpr�ft einen Textstring auf Gateway Text. <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 09.01.2007 <br>
		 */
		private boolean isValidGw(String checkGw) {
			return isValidEcu(checkGw);
		}

		/**
		 * �berpr�ft einen Textstring auf ECU Text. <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 20.12.2006 <br>
		 */
		private boolean isValidEcu(String checkEcu) {
			int i;

			if (checkEcu == null)
				return false;
			if (checkEcu.length() == 0)
				return false;
			if (checkEcu.charAt(0) == ' ')
				return false;
			for (i=0; i<checkEcu.length(); i++) {
				if ((" _/()" +
					"0123456789" +
					"ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
					"abcdefghijklmnopqrstuvwxyz").indexOf(checkEcu.charAt(i)) == -1)
					return false;
			}
			return true;
		}

		/**
		 * Erzeugt einen Wildcardstring <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 11.05.2007 <br>
		 */
		private String wildCard(String s, String mask) {
			String temp = "";
			int i;
			int k;

			if (s == null)
				return temp;
			temp = s;
			if (mask == null)
				return temp;
			k = temp.length();
			if (mask.length() < k)
				k = mask.length();
			for (i = 0; i < k; i++) {
				if (mask.charAt(i) == '?')
					temp = temp.substring(0, i) + "?" + temp.substring(i + 1);
			}
			return temp;
		}

		/**
		 * Normiert einen Versionsstring <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 15.05.2009 <br>
		 */
		private String normalizeVersion(String s) {
			String temp;
			int p1;
			int p2;
			int p3;

			if (s == null) return UNKNOWN;
			temp = s;
			p1 = s.indexOf("major:");
			if (p1 >= 0) {
				p2 = s.indexOf("minor:");
				p3 = s.indexOf("revision:");
				if ((p2 > p1+6) && (p3 > p2+6) && (s.length() > p3+9)) {
					try {
						temp = s.substring(p1+6, p2).trim() + "." +
							   s.substring(p2+6, p3).trim() + "." +
							   s.substring(p3+9    ).trim();
					} catch( Exception e ) {
						temp = s;
					}
				}
			}
			else
				temp = s.replace('_','.');
			return temp;
		}

		/**
		 * Normiert einen Textstring <br>
		 * Entfernt Kommentar beginnend mit ';' oder mit '#' <br>
		 * Ersetzt Tabulatorzeichen durch Leerzeichen <br>
		 * Entfernt links- und rechtsb�ndige Leerzeichen <br>
		 * Wandelt in Gro�buchstaben <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 11.05.2007 <br>
		 */
		private String normalizeLine(String s) {
			String temp;
			int p;

			temp = s;
			p = temp.indexOf(';');
			if (p >= 0) temp = temp.substring(0, p);
			p = temp.indexOf('#');
			if (p >= 0) temp = temp.substring(0, p);
			temp = temp.replace('\t', ' ');
			temp = temp.trim().toUpperCase();
			return temp;
		}

		/**
		 * Parst einen Textstring <br>
		 * R�ckgabe null, wenn Formatfehler <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 30.05.2007 <br>
		 */
		private ParseElement parseLine(String s) {
			boolean wildcard = false;
			boolean negiert = false;
			int i = 0;
			int p = 0;
			String temp = "";

			if (s == null) return null;
			if (s.length() == 0) return null;
			ParseElement parseElement = new ParseElement();
			parseElement.autoIStufe = false;
			parseElement.istIStufe = null;
			parseElement.sollIStufe = null;
			StringTokenizer tokenizer = new StringTokenizer(s);
			parseElement.istIStufe = tokenizer.nextToken();
			if (!isValidIStufe(parseElement.istIStufe, true)) return null;
			while (tokenizer.hasMoreTokens()) {
				temp = tokenizer.nextToken();
				if (isValidAdresse(temp)) {
					parseElement.adressListe.add(temp);
				} else {
					if (isValidSgbmId(temp,true)) {
						parseElement.sgbmIdListe.add(new ParseListeElement(temp,false,!isValidSgbmId(temp)));
					} else {
						if ((temp.charAt(0) == '!') && (isValidSgbmId(temp.substring(1),true))) {
							parseElement.sgbmIdListe.add(new ParseListeElement(temp.substring(1),true,!isValidSgbmId(temp.substring(1))));
						} else {
							switch (temp.charAt(0)) {
							case 'S': // SA Sxxx
								wildcard = false;
								negiert = false;
								if (temp.length() == 4) {
									p = 1;
								} else {
									if (temp.length() == 5) {
										if (temp.charAt(1) == '!') {
											p = 2;
											negiert = true;
										} else {
											return null;
										}
									} else {
										return null;
									}
								}
								for (i = 0; i < 3; i++) {
									if ("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(temp.charAt(p + i)) == -1) {
										if (temp.charAt(p + i) == '?') {
											wildcard = true;
										} else {
											return null;
										}
									}
								}
								parseElement.saListe.add(new ParseListeElement(temp.substring(p),negiert,wildcard));
								break;

							case 'T': // Typ Txxxx
								wildcard = false;
								negiert = false;
								if (temp.length() == 5) {
									p = 1;
								} else {
									if (temp.length() == 6) {
										if (temp.charAt(1) == '!') {
											p = 2;
											negiert = true;
										} else {
											return null;
										}
									} else {
										return null;
									}
								}
								for (i = 0; i < 4; i++) {
									if ("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(temp.charAt(p + i)) == -1) {
										if (temp.charAt(p + i) == '?') {
											wildcard = true;
										} else {
											return null;
										}
									}
								}
								parseElement.typListe.add(new ParseListeElement(temp.substring(p),negiert,wildcard));
								break;

							case 'A': // Automatic Mode
								if (!temp.equals("AUTO")) return null;
								parseElement.autoIStufe = true;
								break;

							case 'I': // Soll I-Stufe
								if (!isValidIStufe(temp.substring(1))) return null;
								parseElement.sollIStufe = temp.substring(1);
								break;

							default:
								return null;
							}
						}
					}
				}
			}
			return parseElement;
		}

		/**
		 * L�dt die I_STUFE_DAT-Datei <br>
		 * und bestimmt die sollIStufe
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 11.09.2008 <br>
		 */
		private boolean loadIStufeFile(String istIStufe, CascadeFA FA ) {
			boolean returnFlag = false;
			boolean belegtFlag = false;
			boolean readFlag = false;
			int i = 0;
			String dateiName = null;
			String textZeile = null;
			String temp = null;
			RandomAccessFile f = null;
			ParseElement parseElement = null;

			if (FA==null) {
				lastError=CfsString.get("cfs.invalidfa");
				return returnFlag;
			}
			if (!isValidBaureihe(normalizeBaureihe(FA.getBaureihe()))) {
				lastError=CfsString.get("cfs.invalidseries");
				return returnFlag;
			}

			sollIStufe=null;
			try {
				dateiName = CascadeProperties.getCascadeHome() + CONFIGPATH + I_STUFE_DAT;
				if ((instanz == 2) && (new File( CascadeProperties.getCascadeHome() + CONFIGPATH_2).exists()))
					dateiName = CascadeProperties.getCascadeHome() + CONFIGPATH_2 + I_STUFE_DAT;
				f = new RandomAccessFile(dateiName, "r");
				readFlag = false;
				A: while (true) {
					if (readFlag) {
						// bereits gelesen
						readFlag = false;
					} else {
						textZeile = f.readLine();
						i++;
						if (textZeile == null) break A;
					}
					textZeile = normalizeLine(textZeile);
					if ((textZeile.length()>0) && (textZeile.charAt(0) == '[') && (textZeile.indexOf("[" + normalizeBaureihe(FA.getBaureihe()) + "]") != -1)) {
						while (true) {
							textZeile = f.readLine();
							i++;
							if (textZeile == null) break A;
							textZeile = normalizeLine(textZeile);
							if (textZeile.length() > 0) {
								if (textZeile.charAt(0) == '[') {
									readFlag = true; // Zeile bereits und neue Sektion
									break;
								}
								parseElement=parseLine(textZeile);
								if (parseElement != null) {
									if (interpreteLine(parseElement,istIStufe,FA)) {
										belegtFlag = true;
										if (parseElement.autoIStufe == true) {
											if (istIStufe.equals(UNKNOWNISTUFE)) {
												sollIStufe=parseElement.sollIStufe;
											} else {
												sollIStufe=istIStufe;
											}
										} else {
											sollIStufe=parseElement.sollIStufe;
										}
										break A; // Erster Treffer
									}
								} else {
									lastError=dateiName + NL + CfsString.get("cfs.readerror")+ NL
									+ CfsString.get("cfs.invalidformat")+ NL
									+ CfsString.get("cfs.inline")+ ": "+ i;
									lastHelp=CfsString.get("cfs.invalidformathelp");
									f.close();
									return returnFlag;
								}
							}
						} // while
					}
				} // while
				f.close();
				if (belegtFlag) {
					if (isValidIStufe(sollIStufe)) {
						lastError=CfsString.get("cfs.noerror");
						lastHelp=CfsString.get("cfs.nohelp");
						returnFlag = true;
					} else {
						if (sollIStufe==null) {
							temp = CfsString.get("cfs.baureihe") + " " + normalizeBaureihe(FA.getBaureihe()) + NL ;
							if (istIStufe != null) {
								temp = temp + CfsString.get("cfs.ilevel") + " " + istIStufe + NL;
							}
							lastError=dateiName + NL + CfsString.get("cfs.readerror")+ NL
							+ temp
							+ CfsString.get("cfs.nomatchingilevel");
							lastHelp=CfsString.get("cfs.invalidilevelhelp");
						} else {
							lastError=dateiName + NL + CfsString.get("cfs.readerror")+ NL
							+ CfsString.get("cfs.invalidilevel");
							lastHelp=CfsString.get("cfs.invalidilevelhelp");
							sollIStufe=null;
						}
					}
				} else {
					sollIStufe=null;
					temp = CfsString.get("cfs.baureihe") + " " + normalizeBaureihe(FA.getBaureihe()) + NL ;
					if (istIStufe != null) {
						temp = temp + CfsString.get("cfs.ilevel") + " " + istIStufe + NL;
					}
					lastError=dateiName + NL + CfsString.get("cfs.readerror")+ NL
					+ temp
					+ CfsString.get("cfs.nomatchingilevel");
					lastHelp=CfsString.get("cfs.invalidilevelhelp");
				}
			} catch (Exception e) {
				sollIStufe=null;
				lastError=dateiName + NL + CfsString.get("cfs.readerror") + NL	+ e.getMessage();
				lastHelp=CfsString.get("cfs.nohelp");
			}
			return returnFlag;
		}

		/**
		 * L�dt die SG_Xxxx_DAT-Datei <br>
		 * und erzeugt die Steuerger�te-Adress-Liste
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 11.09.2008 <br>
		 */
		private ArrayList<String> loadSgXxxxFile(String SG_Xxxx_DAT, String istIStufe, CascadeFA FA, List<CascadeECU> ecuListe ) {
			boolean gefundenFlag = false;
			boolean readFlag = false;
			int i = 0;
			int k = 0;
			int n = 0;
			String dateiName = null;
			String textZeile = null;
			String temp = null;
			RandomAccessFile f = null;
			ParseElement parseElement = null;
			ArrayList<String> sgbmIdListe = new ArrayList<String>();
			ArrayList<String> sgXxxxListe = new ArrayList<String>();

			if (FA==null) {
				lastError=CfsString.get("cfs.invalidfa");
				return null;
			}
			if (!isValidBaureihe(normalizeBaureihe(FA.getBaureihe()))) {
				lastError=CfsString.get("cfs.invalidseries");
				return null;
			}

			if (ecuListe != null) {
				sgbmIdListe = createSgbmIdListe(ecuListe);
				if (sgbmIdListe==null) {
					lastError=CfsString.get("cfs.invalidsgbmidlist");
					return null;
				}
				if (sgbmIdListe.size()==0) {
					lastError=CfsString.get("cfs.invalidsgbmidlist");
					return null;
				}
			}

			try {
				dateiName = CascadeProperties.getCascadeHome() + CONFIGPATH + SG_Xxxx_DAT;
				if ((instanz == 2) && (new File( CascadeProperties.getCascadeHome() + CONFIGPATH_2).exists()))
					dateiName = CascadeProperties.getCascadeHome() + CONFIGPATH_2 + SG_Xxxx_DAT;
				f = new RandomAccessFile(dateiName, "r");
				readFlag = false;
				A: while (true) {
					if (readFlag) {
						// bereits gelesen
						readFlag = false;
					} else {
						textZeile = f.readLine();
						i++;
						if (textZeile == null) break A;
					}
					textZeile = normalizeLine(textZeile);
					if ((textZeile.length()>0) && (textZeile.charAt(0) == '[') && (textZeile.indexOf("[" + normalizeBaureihe(FA.getBaureihe()) + "]") != -1)) {
						while (true) {
							textZeile = f.readLine();
							i++;
							if (textZeile == null) break A;
							textZeile = normalizeLine(textZeile);
							if (textZeile.length() > 0) {
								if (textZeile.charAt(0) == '[') {
									readFlag = true; // Zeile gelesen und neue Sektion
									break;
								}
								parseElement=parseLine(textZeile);
								if (parseElement != null) {
									if (interpreteLine(parseElement,istIStufe,FA,sgbmIdListe)) {
										if (parseElement.adressListe != null) {
										  for (n=0; n<parseElement.adressListe.size(); n++) {
										  	gefundenFlag = false;
										  	temp = (String)parseElement.adressListe.get(n);
										  	for (k=0; k<sgXxxxListe.size(); k++) {
										  		if (temp.equalsIgnoreCase((String)sgXxxxListe.get(k))) {
										  			gefundenFlag = true;
										  			break;
										  		}
										  	}
										  	if (!gefundenFlag) sgXxxxListe.add(temp);
										  }
										}
									}
								} else {
									lastError=dateiName + NL + CfsString.get("cfs.readerror")+ NL
									+ CfsString.get("cfs.invalidformat")+ NL
									+ CfsString.get("cfs.inline")+ ": "+ i;
									lastHelp=CfsString.get("cfs.invalidformathelp");
									f.close();
									return null;
								}
							}
						} // while
					}
				} // while
				f.close();
				lastError=CfsString.get("cfs.noerror");
				lastHelp=CfsString.get("cfs.nohelp");
				return sgXxxxListe;
			} catch (Exception e) {
				lastError=dateiName + NL + CfsString.get("cfs.readerror") + NL	+ e.getMessage();
				lastHelp=CfsString.get("cfs.nohelp");
			}
			return null;
		}

		/**
		 * L�dt die SG_IGNOR_DAT-Datei <br>
		 * und erzeugt die Steuerger�te-Adress-Liste
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 22.11.2007 <br>
		 */
		private boolean loadSgIgnorFile(String istIStufe, CascadeFA FA, List<CascadeECU> ecuListe ) {
			sgIgnorListe = loadSgXxxxFile(SG_IGNOR_DAT, istIStufe, FA, ecuListe );
			if (sgIgnorListe == null) {
				sgIgnorListe = new ArrayList<String>();
				return false;
			}
			return true;
		}

		/**
		 * L�dt die SG_FLASH_DAT-Datei <br>
		 * und erzeugt die Steuerger�te-Adress-Liste
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 22.11.2007 <br>
		 */
		private boolean loadSgFlashFile(String istIStufe, CascadeFA FA, List<CascadeECU> ecuListe ) {
			sgFlashListe = loadSgXxxxFile(SG_FLASH_DAT, istIStufe, FA, ecuListe );
			if (sgFlashListe == null) {
				sgFlashListe = new ArrayList<String>();
				return false;
			}
			return true;
		}

		/**
		 * L�dt die SG_NOFLASH_DAT-Datei <br>
		 * und erzeugt die Steuerger�te-Adress-Liste
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 22.11.2007 <br>
		 */
		private boolean loadSgNoFlashFile(String istIStufe, CascadeFA FA, List<CascadeECU> ecuListe ) {
			sgNoFlashListe = loadSgXxxxFile(SG_NOFLASH_DAT, istIStufe, FA, ecuListe );
			if (sgNoFlashListe == null) {
				sgNoFlashListe = new ArrayList<String>();
				return false;
			}
			return true;
		}

		/**
		 * L�dt die SG_VERBAU_DAT-Datei <br>
		 * und erzeugt die Steuerger�te-Adress-Liste
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 22.11.2007 <br>
		 */
		private boolean loadSgVerbauFile(String istIStufe, CascadeFA FA ) {
			sgVerbauListe = loadSgXxxxFile(SG_VERBAU_DAT, istIStufe, FA, null );
			if (sgVerbauListe == null) {
				sgVerbauListe = new ArrayList<String>();
				return false;
			}
			return true;
		}

		/**
		 * Erzeugt die SG-Liste f�r Fzg-Log-Dateien <br>
		 * Im Fehlerfall ist die Liste leer <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 22.06.2007 <br>
		 */
		private ArrayList<String> createSgbmIdListe(List<CascadeECU> ecuListe ) {
			int j = 0;
			int k = 0;
			String temp = "";
			CascadeSGBMID[] sgbmid = null;
			CascadeECU ecu = null;
			ArrayList<String> sgbmIdListe = new ArrayList<String>(); // SGBMID-Liste aus SVT

			if (ecuListe != null) {
				for (j=0; j<ecuListe.size(); j++) {
					ecu = (CascadeECU)ecuListe.get(j);
					if (ecu != null) {
						sgbmid = ecu.getSvk();
						if (sgbmid != null) {
							for (k=0; k<sgbmid.length; k++) {
								temp = sgbmid[k].getProcessClass() + "-" +	sgbmid[k].getId() + "-" +
										dezString(sgbmid[k].getMainVersion() ,3) + "." + dezString(sgbmid[k].getSubVersion() ,3) + "." + dezString(sgbmid[k].getPatchVersion() ,3);
								sgbmIdListe.add( temp );
							}
						}
					}
				}
			}
			return sgbmIdListe;
		}

		/**
		 * Interpretiert einen Textstring <br>
		 * R�ckgabe null, wenn Formatfehler <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 22.11.2007 <br>
		 */
		private boolean interpreteLine(ParseElement parseElement, String istIStufe, CascadeFA FA) {
			return interpreteLine(parseElement, istIStufe, FA, null);
		}

		private boolean interpreteLine(ParseElement parseElement, String istIStufe, CascadeFA FA, ArrayList<String> sgbmIdListe) {
			boolean returnFlag = false;
			int i;
			int n;
			String temp;
			ParseListeElement parseListeElement;

			if (istIStufe==null) return false;
			if (FA==null) return false;
			if (parseElement==null) return false;
			temp = wildCard(istIStufe, parseElement.istIStufe);
			if (parseElement.istIStufe.equals(temp)) {
				returnFlag = true;
				for (i = 0; i < parseElement.typListe.size(); i++) {
					parseListeElement = (ParseListeElement)parseElement.typListe.get(i);
					temp = wildCard(FA.getTypSchluessel(), parseListeElement.name);
					if (parseListeElement.negiert) {
						if (parseListeElement.name.equals(temp))
							returnFlag = false;
					} else {
						if (!parseListeElement.name.equals(temp))
							returnFlag = false;
					}
					if (returnFlag == false)
						break;
				}
				if (returnFlag == true) {
					for (i = 0; i < parseElement.saListe.size(); i++) {
						parseListeElement = (ParseListeElement)parseElement.saListe.get(i);
						if (parseListeElement.negiert) {
							returnFlag = true;
							for (n = 0; n < FA.getSaListe().length; n++) {
								temp = wildCard(FA.getSaListe()[n],parseListeElement.name);
								if (parseListeElement.name.equals(temp)) {
									returnFlag = false;
									break;
								}
							}
						} else {
							returnFlag = false;
							for (n = 0; n < FA.getSaListe().length; n++) {
								temp = wildCard(FA.getSaListe()[n],parseListeElement.name);
								if (parseListeElement.name.equals(temp)) {
									returnFlag = true;
									break;
								}
							}
						}
						if (returnFlag == false)
							break;
					}
				}
				if (returnFlag == true) {
					if ((sgbmIdListe != null) && (sgbmIdListe.size() > 0)) {
						for (i = 0; i < parseElement.sgbmIdListe.size(); i++) {
							parseListeElement = (ParseListeElement)parseElement.sgbmIdListe.get(i);
							if (parseListeElement.negiert) {
								returnFlag = true;
								for (n = 0; n < sgbmIdListe.size(); n++) {
									temp = wildCard((String)sgbmIdListe.get(n),parseListeElement.name);
									if (parseListeElement.name.equals(temp)) {
										returnFlag = false;
										break;
									}
								}
							} else {
								returnFlag = false;
								for (n = 0; n < sgbmIdListe.size(); n++) {
									temp = wildCard((String)sgbmIdListe.get(n),parseListeElement.name);
									if (parseListeElement.name.equals(temp)) {
										returnFlag = true;
										break;
									}
								}
							}
						}
					}
				}
			}
			return returnFlag;
		}

		/**
		 * L�dt die INI-Datei <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 13.01.2014 <br>
		 */
		private boolean loadINIFile() {
			boolean returnFlag = false;
			String vin = null;
			String dateiName = null;
			String message = null;
			String helpmessage = null;
			String temp = null;

			zgwVin1=null;
			zgwVin2=null;
			try {
				/**
				 * Instanz pruefen <br>
				 */
				instanz=getPr�flingLaufzeitUmgebung().getNumber()+1;
				if ((instanz!=1) && (instanz!=2)) {
					lastError = CfsString.get("cfs.instanceerror")+ NL + CfsString.get("cfs.instance")+" = " + instanz;
					lastHelp = CfsString.get("cfs.instancehelp1");
					return returnFlag;
				}
				rechnerName=getPr�flingLaufzeitUmgebung().getName();
				if (rechnerName == null) {
					lastError = CfsString.get("cfs.invalidnameerror")+ NL + CfsString.get("cfs.name")+" = null";
					lastHelp = CfsString.get("cfs.instancehelp2");
					return returnFlag;
				}
				if (rechnerName.length() == 0) {
					lastError = CfsString.get("cfs.invalidnameerror")+ NL + CfsString.get("cfs.name")+" = null";
					lastHelp = CfsString.get("cfs.instancehelp2");
					return returnFlag;
				}

				dateiName = CascadeProperties.getCascadeHome() + CONFIGPATH + CFS_INI;
				if ((instanz == 2) && (new File( CascadeProperties.getCascadeHome() + CONFIGPATH_2).exists()))
					dateiName = CascadeProperties.getCascadeHome() + CONFIGPATH_2 + CFS_INI;
				iniFileProperties.clear();
				iniFileProperties.load(new FileInputStream(new File(dateiName)));

				/**
				 * Die Eintr�ge abholen <br>
				 */
				message="";
				helpmessage="";

				/**
				 * modeSwitch <br>
				 */
				temp = iniFileProperties.getProperty("MODE_SWITCH");
				if (temp != null) {
					modeSwitch=temp.trim().equalsIgnoreCase("TRUE");
				}
				temp = iniFileProperties.getProperty("MODE_SWITCH_" + instanz);
				if (temp != null) {
					modeSwitch=temp.trim().equalsIgnoreCase("TRUE");
				}
				/**
				 * checkProgDeps <br>
				 */
				temp = iniFileProperties.getProperty("CHECK_PROG_DEPS");
				if (temp != null) {
					checkProgDeps=temp.trim().equalsIgnoreCase("TRUE");
				}
				/**
				 * ignoreMissingECU <br>
				 */
				temp = iniFileProperties.getProperty("IGNORE_MISSING_ECUS");
				if (temp != null) {
					ignoreMissingEcus=temp.trim().equalsIgnoreCase("TRUE");
				}
				/**
				 * parallelExecution <br>
				 */
				temp = iniFileProperties.getProperty("PARALLEL_EXECUTION");
				if (temp != null) {
					parallelExecution=temp.trim().equalsIgnoreCase("TRUE");
				}
				/**
				 * allowBootLoaderUpdate <br>
				 */
				temp = iniFileProperties.getProperty("ALLOW_BOOTLOADER_UPDATE");
				if (temp != null) {
					allowBootLoaderUpdate=temp.trim().equalsIgnoreCase("TRUE");
				}
				/**
				 * allowIBAUpdate <br>
				 */
				temp = iniFileProperties.getProperty("ALLOW_IBA_UPDATE");
				if (temp != null) {
					allowIBAUpdate=temp.trim().equalsIgnoreCase("TRUE");
				}
				/**
				 * allowGWTBUpdate <br>
				 */
				temp = iniFileProperties.getProperty("ALLOW_GWTB_UPDATE");
				if (temp != null) {
					allowGWTBUpdate=temp.trim().equalsIgnoreCase("TRUE");
				}
				/**
				 * samplingFlag <br>
				 */
				temp = iniFileProperties.getProperty("FORCE_EXECUTION");
				if (temp != null) {
					samplingFlag=temp.trim().equalsIgnoreCase("TRUE");
				}
				/**
				 * debugLevel <br>
				 */
				debugLevel = 0;
				temp = iniFileProperties.getProperty("DEBUG_LEVEL");
				if (temp != null) {
					debugLevel=Integer.decode(temp.trim()).intValue();
				}
				/**
				 * trailerModeFlag <br>
				 */
				temp = iniFileProperties.getProperty("TRAILER_MODE");
				if (temp != null) {
					trailerModeFlag=temp.trim().equalsIgnoreCase("TRUE");
				}
				/**
				 * trailerMultiSelectFlag <br>
				 */
				temp = iniFileProperties.getProperty("TRAILER_MULTISELECT");
				if (temp != null) {
					trailerMultiSelectFlag=temp.trim().equalsIgnoreCase("TRUE");
				}
				/**
				 * trailerKisVerifyFlag <br>
				 */
				temp = iniFileProperties.getProperty("TRAILER_KIS_VERIFY");
				if (temp != null) {
					trailerKisVerifyFlag=temp.trim().equalsIgnoreCase("TRUE");
				}
				/**
				 * apdmDetailFlag <br>
				 */
				temp = iniFileProperties.getProperty("APDM_DETAIL");
				if (temp != null) {
					apdmDetailFlag=temp.trim().equalsIgnoreCase("TRUE");
				}
				/**
				 * verifySVTFlag <br>
				 */
				temp = iniFileProperties.getProperty("VERIFY_SVT");
				if (temp != null) {
					verifySVTFlag=temp.trim().equalsIgnoreCase("TRUE");
				}
				/**
				 * useFzsFlag <br>
				 */
				temp = iniFileProperties.getProperty("USE_FZS");
				if (temp != null) {
					useFzsFlag=temp.trim().equalsIgnoreCase("TRUE");
				}
				/**
				 * automaticMode <br>
				 */
				temp = iniFileProperties.getProperty("AUTOMATIC_MODE");
				if (temp != null) {
					automaticMode=temp.trim().equalsIgnoreCase("TRUE");
				}
				/**
				 * automaticDataSgbmid <br>
				 */
				temp = iniFileProperties.getProperty("AUTOMATIC_FIND_DATA_SGBMID");
				if (temp != null) {
					automaticFindDataSgbmid=temp.trim().equalsIgnoreCase("TRUE");
				}
				/**
				 * edicNetIPAddress <br>
				 * veraltet. ersetzt durch INTERFACE_IP_ADDRESS
				 */
				temp = iniFileProperties.getProperty("EDICNET_IP_ADDRESS_" + instanz);
				if (temp != null) {
					interfaceIPAddress=temp.trim();
				}
				/**
				 * interfaceIPAddress <br>
				 */
				temp = iniFileProperties.getProperty("INTERFACE_IP_ADDRESS_" + instanz);
				if (temp != null) {
					interfaceIPAddress=temp.trim();
				}
				/**
				 * icomCanBridgeActive <br>
				 */
				temp = iniFileProperties.getProperty("ICOM_CAN_BRIDGE_ACTIVE");
				if (temp != null) {
					icomCanBridgeActiveFlag=temp.trim().equalsIgnoreCase("TRUE");
				}
				/**
				 * defaultIStufe <br>
				 */
				defaultIStufe = null;
				temp = iniFileProperties.getProperty("DEFAULT_ISTUFE");
				if (temp != null) {
					temp=temp.trim();
					if (isValidIStufe(temp)) defaultIStufe=temp;
				}
				temp = iniFileProperties.getProperty("DEFAULT_ISTUFE_"+instanz);
				if (temp != null) {
					temp=temp.trim();
					if (isValidIStufe(temp)) defaultIStufe=temp;
				}
				/**
				 * zgwVin1 und zgwVin2 <br>
				 */
				vin=iniFileProperties.getProperty("ZGW_VIN_1");
				if (vin==null) {
					if (trailerModeFlag) {
						message=message+"ZGW_VIN_1 "+ CfsString.get("cfs.keynotfounderror")+NL;
						helpmessage=helpmessage+ CfsString.get("cfs.keyonlycapitallettershelp")+NL;
					}
				} else {
					vin=vin.trim();
					if (isValidVIN(vin)) {
						zgwVin1=vin;
					} else {
						message=message+"ZGW_VIN_1='"+vin+"' "+ CfsString.get("cfs.entrynotvaliderror")+NL;
 					    helpmessage=helpmessage+ CfsString.get("cfs.vinlengthis17help")+NL;
					}
				}
				vin=iniFileProperties.getProperty("ZGW_VIN_2");
				if (vin==null) {
					if (trailerModeFlag) {
						message=message+"ZGW_VIN_2 "+ CfsString.get("cfs.keynotfounderror")+NL;
						helpmessage=helpmessage+ CfsString.get("cfs.keyonlycapitallettershelp")+NL;
					}
				} else {
					vin=vin.trim();
					if (isValidVIN(vin)) {
						zgwVin2=vin;
					} else {
						message=message+"ZGW_VIN_2='"+vin+"' "+ CfsString.get("cfs.entrynotvaliderror")+NL;
					    helpmessage=helpmessage+ CfsString.get("cfs.vinlengthis17help")+NL;
					}
				}

				if ( message.length()!=0) {
					lastError=dateiName + NL + CfsString.get("cfs.readerror") + NL + message;
					if ( helpmessage.length()!=0)
						lastHelp=helpmessage;
					else
						lastHelp=CfsString.get("cfs.nohelp");
				} else {
					lastError=CfsString.get("cfs.noerror");
					lastHelp=CfsString.get("cfs.nohelp");
					returnFlag = true;
				}
			} catch (Exception e) {
				lastError = dateiName + NL + CfsString.get("cfs.readerror") + NL	+ e.getMessage();
				lastHelp=CfsString.get("cfs.nohelp");
			}
			return returnFlag;
		}

		/**
		 * L�dt die SG_LISTE-Datei <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 20.01.2010 <br>
		 */
		// Wegen warning Dead Code
		@SuppressWarnings("unused")
		private boolean loadEcuListFile() {
			boolean returnFlag = false;
			StringTokenizer tokenizer = null;
			String message = null;
			String helpmessage = null;
			String dateiName = null;
			String iStufe = null;
			String baureihe = null;
			String str = null;
			String ecuName = null;
			String ecuAddress = null;
			String gwName = null;
			String gwAddress = null;
			int n=0;
			int i=0;
			int j=0;
			int k=0;

			sollIStufe=null;
			try {
				dateiName = CascadeProperties.getCascadeHome() + CONFIGPATH + SG_LISTE_DAT;
				if ((instanz == 2) && (new File( CascadeProperties.getCascadeHome() + CONFIGPATH_2).exists()))
					dateiName = CascadeProperties.getCascadeHome() + CONFIGPATH_2 + SG_LISTE_DAT;
				ecuListFileProperties.clear();
				ecuListFileProperties.load(new FileInputStream(new File(dateiName)));
				trailerModeAnlieferzustandFlag = false;
				str=ecuListFileProperties.getProperty("ANLIEFERZUSTAND");
				if (str != null) {
					if (str.trim().equalsIgnoreCase("TRUE")) trailerModeAnlieferzustandFlag = true;
				}
				baureihe = ecuListFileProperties.getProperty("BAUREIHE");
				if (baureihe != null) baureihe = baureihe.trim().toUpperCase();
				if (isValidBaureihe(baureihe)) {
					iStufe = ecuListFileProperties.getProperty("ISTUFE");
					if (isValidIStufe(iStufe)) {
						sollIStufe=iStufe;
						if (isValidAnzahl(ecuListFileProperties.getProperty("GWS"),0,256)) {
							if (isValidAnzahl(ecuListFileProperties.getProperty("ECUS"),1,256)) {
								/**
								 * Die Eintr�ge abholen <br>
								 */
								message="";
								helpmessage="";
								/**
								 * Gateways <br>
								 */
								if ( message.length()==0) {
									n = Integer.parseInt(ecuListFileProperties.getProperty("GWS"));
									gwListe = new EcuListeElement[n];
									for (i=0; i<n; i++) {
										str = ecuListFileProperties.getProperty("GW" + i);
										if (str!=null) {
											tokenizer = new StringTokenizer(str,",");
											if (tokenizer!=null) {
												if (tokenizer.hasMoreTokens()) {
													gwName = tokenizer.nextToken();
													if (isValidGw(gwName)) {
														gwListe[i] = new EcuListeElement();
														gwListe[i].name = gwName;
														k = tokenizer.countTokens();
														if (k>0) {
															gwListe[i].address = new int[k];
															for (j=0; j<k; j++) {
																gwAddress = tokenizer.nextToken();
																if (isValidAdresse(gwAddress)) {
																	gwListe[i].address[j]=Integer.decode("0x" + gwAddress).intValue();
																} else {
																	message=message+"GW" + i + " "+ CfsString.get("cfs.invalidaddresserror")+NL;
																	helpmessage=helpmessage+ CfsString.get("cfs.addresshelp")+NL;
																}
															}
														} else {
															message=message+"GW" + i + " "+ CfsString.get("cfs.missingaddresserror")+NL;
															helpmessage=helpmessage+ CfsString.get("cfs.addresshelp")+NL;
														}
													} else {
														message=message+"GW" + i + " "+ CfsString.get("cfs.invalidgwnameerror")+NL;
														helpmessage=helpmessage+ CfsString.get("cfs.gwnamehelp")+NL;
													}
												} else {
													message=message+"GW" + i + " "+ CfsString.get("cfs.missinggwnameerror")+NL;
													helpmessage=helpmessage+ CfsString.get("cfs.gwnamehelp")+NL;
												}
  											} else {
												message=message+"GW" + i + " "+ CfsString.get("cfs.entrynotvaliderror")+NL;
												helpmessage=helpmessage+ CfsString.get("cfs.entryformathelp")+NL;
											}
										} else {
											message=message+"GW" + i + " "+ CfsString.get("cfs.keynotfounderror")+NL;
											helpmessage=helpmessage+ CfsString.get("cfs.keyonlycapitallettershelp")+NL;
										}
									}
								}
								/**
								 * Steuerger�te <br>
								 */
								if ( message.length()==0) {
									n = Integer.parseInt(ecuListFileProperties.getProperty("ECUS"));
									ecuListe = new EcuListeElement[n];
									for (i=0; i<n; i++) {
										str = ecuListFileProperties.getProperty("ECU" + i);
										if (str!=null) {
											tokenizer = new StringTokenizer(str,",");
											if (tokenizer!=null) {
												if (tokenizer.hasMoreTokens()) {
													ecuName = tokenizer.nextToken();
													if (isValidEcu(ecuName)) {
														ecuListe[i] = new EcuListeElement();
														ecuListe[i].name = ecuName;
														k = tokenizer.countTokens();
														if (k>0) {
															ecuListe[i].address = new int[k];
															for (j=0; j<k; j++) {
																ecuAddress = tokenizer.nextToken();
																if (isValidAdresse(ecuAddress)) {
																	ecuListe[i].address[j]=Integer.decode("0x" + ecuAddress).intValue();
																} else {
																	message=message+"ECU" + i + " "+ CfsString.get("cfs.invalidaddresserror")+NL;
																	helpmessage=helpmessage+ CfsString.get("cfs.addresshelp")+NL;
																}
															}
														} else {
															message=message+"ECU" + i + " "+ CfsString.get("cfs.missingaddresserror")+NL;
															helpmessage=helpmessage+ CfsString.get("cfs.addresshelp")+NL;
														}
													} else {
														message=message+"ECU" + i + " "+ CfsString.get("cfs.invalidecunameerror")+NL;
														helpmessage=helpmessage+ CfsString.get("cfs.ecunamehelp")+NL;
													}
												} else {
													message=message+"ECU" + i + " "+ CfsString.get("cfs.missingecunameerror")+NL;
													helpmessage=helpmessage+ CfsString.get("cfs.ecunamehelp")+NL;
												}
											} else {
												message=message+"ECU" + i + " "+ CfsString.get("cfs.entrynotvaliderror")+NL;
												helpmessage=helpmessage+ CfsString.get("cfs.entryformathelp")+NL;
											}
										} else {
											message=message+"ECU" + i + " "+ CfsString.get("cfs.keynotfounderror")+NL;
											helpmessage=helpmessage+ CfsString.get("cfs.keyonlycapitallettershelp")+NL;
										}
									}
								}
								/**
								 * Eindeutigkeit der Adressen testen <br>
								 */
								if ( message.length()==0) {
									int usedGwAddress[] = new int[256];
									int usedEcuAddress[] = new int[256];
									String temp;
									// Gateways
									if (gwListe.length > 0) {
										temp="";
										for (i=0; i<gwListe.length; i++) {
											for (j=0; j<gwListe[i].address.length; j++) {
												usedGwAddress[gwListe[i].address[j]]++;
											}
										}
										for (i=0; i<usedGwAddress.length; i++) {
											if (usedGwAddress[i]>1) {
												if ( temp.length()!=0) temp=temp+",";
												temp=temp+hexString(i,2);
											}
										}
										if ( temp.length()!=0) {
											message=message+CfsString.get("cfs.gwaddressmultipleusederror")+": "+temp+NL;
											helpmessage=helpmessage+CfsString.get("cfs.gwaddressmultipleusedehelp")+NL;
										}
									}
									// Steuerger�te
									temp="";
									for (i=0; i<ecuListe.length; i++) {
										for (j=0; j<ecuListe[i].address.length; j++) {
											usedEcuAddress[ecuListe[i].address[j]]++;
										}
									}
									for (i=0; i<usedEcuAddress.length; i++) {
										if (usedEcuAddress[i]>1) {
											if ( temp.length()!=0) temp=temp+",";
											temp=temp+hexString(i,2);
										}
									}
									if ( temp.length()!=0) {
										message=message+CfsString.get("cfs.ecuaddressmultipleusederror")+": "+temp+NL;
										helpmessage=helpmessage+CfsString.get("cfs.ecuaddressmultipleusedehelp")+NL;
									}
									// Gateways und Steuerger�te
									if (gwListe.length > 0) {
										temp="";
										for (i=0; i<usedGwAddress.length; i++) {
											if (usedGwAddress[i]*usedEcuAddress[i] !=0 ) {
												if ( temp.length()!=0) temp=temp+",";
												temp=temp+hexString(i,2);
											}
										}
										if ( temp.length()!=0) {
											message=message+CfsString.get("cfs.gwecuaddressmultipleusederror")+": "+temp+NL;
											helpmessage=helpmessage+CfsString.get("cfs.gwecuaddressmultipleusedehelp")+NL;
										}
									}
								}
								if ( message.length()!=0) {
									lastError=dateiName + NL + CfsString.get("cfs.readerror") + NL + message;
									if ( helpmessage.length()!=0)
										lastHelp=helpmessage;
									else
										lastHelp=CfsString.get("cfs.nohelp");
								} else {
									lastError=CfsString.get("cfs.noerror");
									lastHelp=CfsString.get("cfs.nohelp");
									returnFlag = true;
								}
							} else {
								lastError=dateiName + NL + CfsString.get("cfs.readerror")+ NL
								+ CfsString.get("cfs.invalidecunr");
								lastHelp=CfsString.get("cfs.invalidecunrhelp");
							}
						} else {
							lastError=dateiName + NL + CfsString.get("cfs.readerror")+ NL
							+ CfsString.get("cfs.invalidgwnr");
							lastHelp=CfsString.get("cfs.invalidgwnrhelp");
						}
					} else {
						lastError=dateiName + NL + CfsString.get("cfs.readerror")+ NL
						+ CfsString.get("cfs.invalidilevel");
						lastHelp=CfsString.get("cfs.invalidilevelhelp");
					}
				} else {
					lastError=dateiName + NL + CfsString.get("cfs.readerror") + NL
					+ CfsString.get("cfs.invalidseries");
					lastHelp=CfsString.get("cfs.invalidserieshelp");
				}
			} catch (Exception e) {
				lastError=dateiName + NL + CfsString.get("cfs.readerror") + NL	+ e.getMessage();
				lastHelp=CfsString.get("cfs.nohelp");
			}
			return returnFlag;
		}

		/**
		 * Liefert die SGBDID, die im Anlieferzustand nicht zu programmieren ist <br>
		 * f�r die Lieferumfangsnummer <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 14.01.2010 <br>
		 */
		private String getStoredSGBMID(String lieferUmfangNummer) {
			String returnString = null;

			if (isValidLieferUmfangNummer(lieferUmfangNummer)) {
				returnString = ecuListFileProperties.getProperty(lieferUmfangNummer);
				if (!isValidSgbmId(returnString)) {
					returnString = null;
				}
			}
			return returnString;
		}

		/**
		 * Speichert die SGBDID, die im Anlieferzustand nicht zu programmieren ist <br>
		 * f�r die Lieferumfangsnummer <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 14.01.2010 <br>
		 */
		private boolean storeSGBMID(String lieferUmfangNummer, String sgbmId) {
			boolean returnFlag = false;

			if (isValidLieferUmfangNummer(lieferUmfangNummer)) {
				if (isValidSgbmId(sgbmId)) {
					ecuListFileProperties.setProperty(lieferUmfangNummer,sgbmId);
					returnFlag = true;
				}
			}
			return returnFlag;
		}

		/**
		 * Liefert true wenn die Lieferumfangsnummer mit Adresse <br>
		 * gespeichert ist <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 17.12.2014 <br>
		 */
		private boolean isStoredLieferUmfangNummer(String sgAdresse, String lieferUmfangNummer, boolean trailerMultiSelectFlag) {
			boolean returnFlag = false;
			String temp = null;

			if (isValidAdresse(sgAdresse)) {
				if (isValidLieferUmfangNummer(lieferUmfangNummer)) {
					if (trailerMultiSelectFlag) {
						if (ecuListFileProperties.getProperty(sgAdresse+","+lieferUmfangNummer) != null) {
							returnFlag = true;
						}
					} else {
						temp = ecuListFileProperties.getProperty("LieferUmfangNummer("+sgAdresse+")");
						if (temp != null) {
							if (temp.equalsIgnoreCase(lieferUmfangNummer)) {
								returnFlag = true;
							}
						}
					}
				}
			}
			return returnFlag;
		}

		/**
		 * Speichert die Lieferumfangsnummer mit Adresse <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 17.12.2014 <br>
		 */
		private boolean storeLieferUmfangNummer(String sgAdresse, String lieferUmfangNummer, boolean trailerMultiSelectFlag) {
			boolean returnFlag = false;

			if (isValidAdresse(sgAdresse)) {
				if (isValidLieferUmfangNummer(lieferUmfangNummer)) {
					if (trailerMultiSelectFlag) {
						ecuListFileProperties.setProperty(sgAdresse+","+lieferUmfangNummer,new Date().toString().replace(':', '-'));
					} else {
						ecuListFileProperties.setProperty("LieferUmfangNummer("+sgAdresse+")",lieferUmfangNummer);
					}
					returnFlag = true;
				}
			}
			return returnFlag;
		}

		/**
		 * Speichert die SG_LISTE-Datei <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 18.01.2007 <br>
		 */
		private boolean saveEcuListFile() {
			boolean returnFlag = false;
			String dateiName = "";

			try {
				dateiName = CascadeProperties.getCascadeHome() + CONFIGPATH + SG_LISTE_DAT;
				if ((instanz == 2) && (new File( CascadeProperties.getCascadeHome() + CONFIGPATH_2).exists()))
					dateiName = CascadeProperties.getCascadeHome() + CONFIGPATH_2 + SG_LISTE_DAT;
				ecuListFileProperties.store(new FileOutputStream(new File(dateiName)), CfsString.get("cfs.filechanged"));
				returnFlag = true;
			} catch (Exception e) {
				lastError=dateiName + NL + CfsString.get("cfs.writeerror") + NL	+ e.getMessage();
				lastHelp=CfsString.get("cfs.nohelp");
			}
			return returnFlag;
		}

		/**
		 * �berpr�ft die SVT-Ist gegen die SVT-Soll Gesamtfahrzeugprogrammierung <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 13.12.2012 <br>
		 */
		private boolean verifySVTFzg(List<CascadeECU> SVKIstListe, List<CascadeECU> SVKSollListe, boolean skipMissingECUs) {
			boolean returnFlag = false;
			boolean gefunden = false;
			String message = null;
			String helpmessage = null;
			String temp = null;
			String temp2 = null;
			String name = null;
			int i=0;
			int j=0;
			int m=0;

			if (SVKIstListe==null) {
				lastError=CfsString.get("cfs.svkistlisteerror");
				lastHelp=CfsString.get("cfs.svkistlistehelp");
				return returnFlag;
			}

			if (SVKSollListe==null) {
				lastError=CfsString.get("cfs.svksolllisteerror");
				lastHelp=CfsString.get("cfs.svksolllistehelp");
				return returnFlag;
			}

			if (!checkSVTSVT(SVKIstListe,SVKSollListe,skipMissingECUs)) {
				return returnFlag;
			}

			message="";
			helpmessage="";
            // SVT-Soll gegen SVT-IST
			for (i=0; i<SVKSollListe.size(); i++) {
				name=UNKNOWN;
				temp=((CascadeECU)SVKSollListe.get(i)).getBaseVariantName();
				if (temp!=null) {
					if (temp.trim().length() > 0) name=temp;
				}
				temp=((CascadeECU)SVKSollListe.get(i)).getVariantName();
				if (temp!=null) {
					if (temp.trim().length() > 0) name=temp;
				}
				gefunden = false;
				for (j=0; j<SVKIstListe.size(); j++) {
					if (sgIgnorListe != null) {
						if (sgIgnorListe.contains(hexString(((CascadeECU)SVKSollListe.get(i)).getDiagnosticAddress().intValue(),2))) {
							gefunden = true;
							break;
						}
					}
					if (sgNoFlashListe != null) {
						if (sgNoFlashListe.contains(hexString(((CascadeECU)SVKSollListe.get(i)).getDiagnosticAddress().intValue(),2))) {
							gefunden = true;
							break;
						}
					}
					if (sgTauschListe != null) {
						if (sgTauschListe.contains(hexString(((CascadeECU)SVKSollListe.get(i)).getDiagnosticAddress().intValue(),2))) {
							gefunden = true;
							break;
						}
					}
					if (((CascadeECU)SVKSollListe.get(i)).getDiagnosticAddress().intValue() == ((CascadeECU)SVKIstListe.get(j)).getDiagnosticAddress().intValue()) {
						// �berpr�fen ob Steuerger�t auch geantwortet hat.
						// SVK ist leer wenn das SG in der white-List enthalten aber nicht antwortet
						if (((CascadeECU)SVKIstListe.get(j)).getSvk() != null ) {
							if (((CascadeECU)SVKIstListe.get(j)).getSvk().length > 0 ) {
								gefunden = true;
								// jetzt SWFL �berpr�fen
								CascadeSGBMID[] SVKSollSGBMID=((CascadeECU)SVKSollListe.get(i)).getSvk();
								ArrayList<String> SGBMIDSoll = new ArrayList<String>();
								for (m=0; m<SVKSollSGBMID.length; m++) {
									if(SVKSollSGBMID[m].getProcessClass().toString().equalsIgnoreCase("SWFL"))
										SGBMIDSoll.add(SVKSollSGBMID[m].getProcessClass().toString() + "-" + SVKSollSGBMID[m].getId() + "-" + dezString(SVKSollSGBMID[m].getMainVersion(),3) + "." + dezString(SVKSollSGBMID[m].getSubVersion(),3) + "." + dezString(SVKSollSGBMID[m].getPatchVersion(),3));
									if(SVKSollSGBMID[m].getProcessClass().toString().equalsIgnoreCase("SWFK"))
										SGBMIDSoll.add(SVKSollSGBMID[m].getProcessClass().toString() + "-" + SVKSollSGBMID[m].getId() + "-" + dezString(SVKSollSGBMID[m].getMainVersion(),3) + "." + dezString(SVKSollSGBMID[m].getSubVersion(),3) + "." + dezString(SVKSollSGBMID[m].getPatchVersion(),3));
								}
								CascadeSGBMID[] SVKIstSGBMID=((CascadeECU)SVKIstListe.get(j)).getSvk();
								ArrayList<String> SGBMIDIst = new ArrayList<String>();
								for (m=0; m<SVKIstSGBMID.length; m++) {
									if(SVKIstSGBMID[m].getProcessClass().toString().equalsIgnoreCase("SWFL"))
										SGBMIDIst.add(SVKIstSGBMID[m].getProcessClass().toString() + "-" + SVKIstSGBMID[m].getId() + "-" + dezString(SVKIstSGBMID[m].getMainVersion(),3) + "." + dezString(SVKIstSGBMID[m].getSubVersion(),3) + "." + dezString(SVKIstSGBMID[m].getPatchVersion(),3));
									if(SVKIstSGBMID[m].getProcessClass().toString().equalsIgnoreCase("SWFK"))
										SGBMIDIst.add(SVKIstSGBMID[m].getProcessClass().toString() + "-" + SVKIstSGBMID[m].getId() + "-" + dezString(SVKIstSGBMID[m].getMainVersion(),3) + "." + dezString(SVKIstSGBMID[m].getSubVersion(),3) + "." + dezString(SVKIstSGBMID[m].getPatchVersion(),3));
								}
								// Fehlende SWFL / SWFK in SVT-Ist
								temp="";
								for (m=0; m<SGBMIDSoll.size(); m++) {
									temp2 = (String)SGBMIDSoll.get(m);
									if (!SGBMIDIst.contains(temp2)) {
										temp = temp + " " + temp2;
									}
								}
								if (temp.length()!=0) {
									message=message+"ECU=" + name + " ADR=" + hexString(((CascadeECU)SVKSollListe.get(i)).getDiagnosticAddress().intValue(),2)
									+ " " + temp.trim() + " " + CfsString.get("cfs.sgbmidnotfounderror")+NL;
								}
								// Zu viele SWFL / SWFK in SVT-Ist
								temp="";
								for (m=0; m<SGBMIDIst.size(); m++) {
									temp2 = (String)SGBMIDIst.get(m);
//									if (!temp2.equalsIgnoreCase("SWFL-00000000-000.000.000") && !temp2.equalsIgnoreCase("SWFL-FFFFFFFF-FFF.FFF.FFF"))
									{
										if (!SGBMIDSoll.contains(temp2)) {
											temp = temp + " " + temp2;
										}
									}
								}
								if (temp.length()!=0) {
									message=message+"ECU=" + name + " ADR=" + hexString(((CascadeECU)SVKSollListe.get(i)).getDiagnosticAddress().intValue(),2)
									+ " " + temp.trim() + " " + CfsString.get("cfs.unexpectedsgbmiderror")+NL;
								}
								break;
							}
						}
					}
				}
				if (!gefunden) {
					if (!skipMissingECUs) {
						message=message+"ECU=" + name + " ADR=" + hexString(((CascadeECU)SVKSollListe.get(i)).getDiagnosticAddress().intValue(),2)
						+ " " + CfsString.get("cfs.ecunotfounderror")+NL;
					}
				}
			}

			if ( message.length()!=0) {
				lastError= CfsString.get("cfs.verifysvterror") + NL + message;
				if ( helpmessage.length()!=0)
					lastHelp=helpmessage;
				else
					lastHelp=CfsString.get("cfs.nohelp");
			} else {
				lastError=CfsString.get("cfs.noerror");
				lastHelp=CfsString.get("cfs.nohelp");
				returnFlag = true;
			}
			return returnFlag;
		}

		/**
		 * �berpr�ft die SVT-Ist gegen die SVT-Soll im TrailerMode <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 13.01.2014 <br>
		 */
		private boolean verifySVTTrailer(List<CascadeECU> SVKIstListe, List<CascadeECU> SVKSollListe) {
			boolean returnFlag = false;
			boolean gefunden = false;
			String temp = null;
			String message = null;
			String helpmessage = null;
			int i=0;
			int k=0;
			int m=0;
			int n=0;

			if (ecuListe==null) {
				lastError=CfsString.get("cfs.eculisteerror");
				lastHelp=CfsString.get("cfs.eculistehelp");
				return returnFlag;
			}
			if (ecuListe.length==0) {
				lastError=CfsString.get("cfs.eculisteerror");
				lastHelp=CfsString.get("cfs.eculistehelp");
				return returnFlag;
			}
			if (SVKIstListe==null) {
				lastError=CfsString.get("cfs.svkistlisteerror");
				lastHelp=CfsString.get("cfs.svkistlistehelp");
				return returnFlag;
			}

			if (SVKSollListe==null) {
				lastError=CfsString.get("cfs.svksolllisteerror");
				lastHelp=CfsString.get("cfs.svksolllistehelp");
				return returnFlag;
			}

			message="";
			helpmessage="";
			for (i=0; i<ecuListe.length; i++) {
				for (k=0; k<ecuListe[i].address.length; k++) {
					// SGBM-IDs holen
					CascadeSGBMID[] SVKSollSGBMID=((CascadeECU)SVKSollListe.get(ecuListe[i].SVKSollIndex[k])).getSvk();
					String[] SGBMIDSoll = new String[SVKSollSGBMID.length];
					for (m=0; m<SGBMIDSoll.length; m++) {
						SGBMIDSoll[m] = SVKSollSGBMID[m].getProcessClass().toString() + "-" + SVKSollSGBMID[m].getId() + "-" +
						dezString(SVKSollSGBMID[m].getMainVersion(),3) + "." + dezString(SVKSollSGBMID[m].getSubVersion(),3) + "." + dezString(SVKSollSGBMID[m].getPatchVersion(),3);
					}
					CascadeSGBMID[] SVKIstSGBMID=((CascadeECU)SVKIstListe.get(ecuListe[i].SVKIstIndex[k])).getSvk();
					String[] SGBMIDIst = new String[SVKIstSGBMID.length];
					for (m=0; m<SGBMIDIst.length; m++) {
						SGBMIDIst[m] = SVKIstSGBMID[m].getProcessClass().toString() + "-" + SVKIstSGBMID[m].getId() + "-" +
						dezString(SVKIstSGBMID[m].getMainVersion(),3) + "." + dezString(SVKIstSGBMID[m].getSubVersion(),3) + "." + dezString(SVKIstSGBMID[m].getPatchVersion(),3);
					}
					for (m=0; m<SGBMIDSoll.length; m++) {
						if (SGBMIDSoll[m].startsWith(CascadeProcessClasses.BTLD.toString()) ||
							SGBMIDSoll[m].startsWith(CascadeProcessClasses.FLSL.toString()) ||
							SGBMIDSoll[m].startsWith(CascadeProcessClasses.SWFK.toString()) ||
							SGBMIDSoll[m].startsWith(CascadeProcessClasses.SWFL.toString()) ||
							SGBMIDSoll[m].startsWith(CascadeProcessClasses.IBAD.toString()) ||
							SGBMIDSoll[m].startsWith(CascadeProcessClasses.GWTB.toString()) ) {
							gefunden = false;
							for (n=0; n<SGBMIDIst.length; n++) {
								if (SGBMIDSoll[m].equalsIgnoreCase(SGBMIDIst[n])) {
									gefunden = true;
									break;
								}
							}
							if ((getTrailerModeAnlieferzustandFlag()) && (SGBMIDSoll[m].equalsIgnoreCase(ecuListe[i].sgbmIdAnlieferzustand[k]))) {
								// Anlieferstandsprogrammierung und auszublendende SGBMID
								if (gefunden) {
									temp=((CascadeECU)SVKSollListe.get(ecuListe[i].SVKSollIndex[k])).getBaseVariantName();
									if (temp==null) temp="";
									message=message+"SVKIst[" + ecuListe[i].SVKSollIndex[k] + "]: "+temp+" ADR=" + hexString(ecuListe[i].address[k],2) + " SGBMID=" + SGBMIDIst[n]
									+ " " + CfsString.get("cfs.anlieferzustand") + " " + CfsString.get("cfs.verifysvkerror")+NL;
								}
							} else {
								if (!gefunden) {
									temp=((CascadeECU)SVKSollListe.get(ecuListe[i].SVKSollIndex[k])).getBaseVariantName();
									if (temp==null) temp="";
									message=message+"SVKSoll[" + ecuListe[i].SVKSollIndex[k] + "]: "+temp+" ADR=" + hexString(ecuListe[i].address[k],2) + " SGBMID=" + SGBMIDSoll[m]
									+ " " + CfsString.get("cfs.verifysvkerror")+NL;
								}
							}
						}
					}
					if (getTrailerModeAnlieferzustandFlag()) {
						// �berpr�fung ob zu viele SWFL in SVKIst
						for (m=0; m<SGBMIDIst.length; m++) {
							if (SGBMIDIst[m].startsWith(CascadeProcessClasses.SWFK.toString()) ||
								SGBMIDIst[m].startsWith(CascadeProcessClasses.SWFL.toString()) ) {
								gefunden = false;
								for (n=0; n<SGBMIDSoll.length; n++) {
									if (SGBMIDIst[m].equalsIgnoreCase(SGBMIDSoll[n])) {
										gefunden = true;
										break;
									}
								}
								if (!gefunden) {
									temp=((CascadeECU)SVKSollListe.get(ecuListe[i].SVKSollIndex[k])).getBaseVariantName();
									if (temp==null) temp="";
									message=message+"SVKSIst[" + ecuListe[i].SVKSollIndex[k] + "]: "+temp+" ADR=" + hexString(ecuListe[i].address[k],2) + " SGBMID=" + SGBMIDIst[m]
   									+ " " + CfsString.get("cfs.anlieferzustand") + " " + CfsString.get("cfs.verifysvkerror")+NL;
								}
							}
						}
					}
					if (!getTrailerModeAnlieferzustandFlag()) {
						// keine �berpr�fung bei Anlieferstandsprogrammierung
						if (ecuListe[i].programmingDependencies[k] != 1) {
							temp=((CascadeECU)SVKSollListe.get(ecuListe[i].SVKSollIndex[k])).getBaseVariantName();
							if (temp==null) temp="";
							message=message+"SVKSoll[" + ecuListe[i].SVKSollIndex[k] + "]: "+temp+" ADR=" + hexString(ecuListe[i].address[k],2) + " PROGDEP=0x"+hexString(ecuListe[i].programmingDependencies[k],2)
							+ " " + CfsString.get("cfs.verifysvkprogdeperror")+NL;
						}
					}
				}
			}
			if ( message.length()!=0) {
				lastError= CfsString.get("cfs.verifysvterror") + NL + message;
				if ( helpmessage.length()!=0)
					lastHelp=helpmessage;
				else
					lastHelp=CfsString.get("cfs.nohelp");
			} else {
				lastError=CfsString.get("cfs.noerror");
				lastHelp=CfsString.get("cfs.nohelp");
				returnFlag = true;
			}
			return returnFlag;
		}

		/**
		 * �berpr�ft zwei SVKs im TrailerMode <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 04.11.2015 <br>
		 */
		private boolean verifySVKTrailer(CascadeECU SVKIst, CascadeECU SVKVerify) {
			boolean returnFlag = false;
			boolean gefunden = false;
			int i=0;
			int k=0;
			int n=0;

			if (SVKIst.getDiagnosticAddress().intValue() == SVKVerify.getDiagnosticAddress().intValue()) {
				if (SVKIst.getSvk().length == SVKVerify.getSvk().length) {
					n=SVKIst.getSvk().length;
					if (n > 0) {
						// SGBM-IDs holen
						CascadeSGBMID[] SVKIstSGBMID= SVKIst.getSvk();
						String[] SGBMIDIst = new String[n];
						for (i=0; i<n; i++) {
							SGBMIDIst[i] = SVKIstSGBMID[i].getProcessClass().toString() + "-" + SVKIstSGBMID[i].getId() + "-" +
							dezString(SVKIstSGBMID[i].getMainVersion(),3) + "." + dezString(SVKIstSGBMID[i].getSubVersion(),3) + "." + dezString(SVKIstSGBMID[i].getPatchVersion(),3);
						}
						CascadeSGBMID[] SVKVerifySGBMID= SVKVerify.getSvk();
						String[] SGBMIDVerify = new String[n];
						for (i=0; i<n; i++) {
							SGBMIDVerify[i] = SVKVerifySGBMID[i].getProcessClass().toString() + "-" + SVKVerifySGBMID[i].getId() + "-" +
							dezString(SVKVerifySGBMID[i].getMainVersion(),3) + "." + dezString(SVKVerifySGBMID[i].getSubVersion(),3) + "." + dezString(SVKVerifySGBMID[i].getPatchVersion(),3);
						}
						for (i=0; i<n; i++) {
							gefunden = false;
							for (k=0; k<n; k++) {
								if (SGBMIDVerify[k].equalsIgnoreCase(SGBMIDIst[i])) {
									gefunden = true;
									break;
								}
							}
							if (!gefunden) break;
						}
						if (gefunden) returnFlag = true;
					}
				}
			}
			return returnFlag;
		}

		/**
		 * �bergibt die angeschlossenen ECUs <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 21.05.2010 <br>
		 */
		private String getConnectedEcus(List<CascadeECU> SVKListe) {
			String returnString = "";
			String temp = null;
			int i=0;
			int j=0;
			int k=0;

			if (ecuListe!=null) {
				if (ecuListe.length!=0) {
					if (SVKListe!=null) {
						for (i=0; i<ecuListe.length; i++) {
							for (k=0; k<ecuListe[i].address.length; k++) {
								for (j=0; j<SVKListe.size(); j++) {
									if ((ecuListe[i].address[k]) == ((CascadeECU)SVKListe.get(j)).getDiagnosticAddress().intValue()) {
										temp=((CascadeECU)SVKListe.get(j)).getBaseVariantName();
										if (temp==null) temp="ECU";
										if (ecuListe[i].lieferUmfangNummer[k] != null) {
											if (ecuListe[i].lieferUmfangNummer[k].length()>0) {
												temp = temp + " (" + ecuListe[i].lieferUmfangNummer[k] + ")";
											}
										}
										if (returnString.length()>0) returnString = returnString + NL;
										returnString = returnString + temp + " ADR=" + hexString(ecuListe[i].address[k],2);
										break;
									}
								}
							}
						}
					}
				}
			}
			return returnString;
		}

		/**
		 * �berpr�ft die SGFlashListe gegen die SgIgnorListe <br>
		 * �berpr�ft die SGFlashListe gegen die SgNoFlashListe <br>
		 * �berpr�ft die SgIgnorListe gegen die SGNoFlashListe <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 26.06.2007 <br>
		 */
		private boolean checkSgXxxxList() {
			boolean returnFlag = false;
			int i=0;
			int k=0;
			String message = null;
			String helpmessage = null;
			String temp = null;
			String[] sgIgnor = null;
			String[] sgFlash = null;
			String[] sgNoFlash = null;

			if (sgFlashListe == null) {
				lastError=CfsString.get("cfs.sgflashlisteerror");
				lastHelp=CfsString.get("cfs.sgflashlistehelp");
				return returnFlag;
			}
			if (sgNoFlashListe == null) {
				lastError=CfsString.get("cfs.sgnoflashlisteerror");
				lastHelp=CfsString.get("cfs.sgnoflashlistehelp");
				return returnFlag;
			}
			if (sgIgnorListe == null) {
				lastError=CfsString.get("cfs.sgignorlisteerror");
				lastHelp=CfsString.get("cfs.sgignorlistehelp");
				return returnFlag;
			}
			sgFlash = getSgFlashListe();
			sgNoFlash = getSgNoFlashListe();
			sgIgnor = getSgIgnorListe();
			message="";
			helpmessage="";
			// 1. Flash gegen Ignor pr�fen
			if ((sgFlash != null) &&  (sgIgnor != null)) {
				temp="";
				for (i=0; i<sgFlash.length; i++) {
					for (k=0; k<sgIgnor.length; k++) {
						if (sgFlash[i].equals(sgIgnor[k])) {
							// Widerspruch Zwangsflash und Ignorieren
							message=message+"ADR=" + sgFlash[i]	+ " " + CfsString.get("cfs.inconsistentsgflashwithsgignorerror")+NL;
							if (temp.length() == 0) temp = CfsString.get("cfs.checksgflashorsgignorhelp")+NL;
						}
					}
				}
				helpmessage=helpmessage + temp;
			}

			// 2. Flash gegen NoFlash pr�fen
			if ((sgFlash != null) &&  (sgNoFlash != null)) {
				temp="";
				for (i=0; i<sgFlash.length; i++) {
					for (k=0; k<sgNoFlash.length; k++) {
						if (sgFlash[i].equals(sgNoFlash[k])) {
							// Widerspruch Zwangsflash und Zwangstausch
							message=message+"ADR=" + sgFlash[i]	+ " " + CfsString.get("cfs.inconsistentsgflashwithsgnoflasherror")+NL;
							if (temp.length() == 0) temp = CfsString.get("cfs.checksgflashorsgnoflashhelp")+NL;
						}
					}
				}
				helpmessage=helpmessage + temp;
			}
			// 3. Ignor gegen NoFlash pr�fen
			if ((sgIgnor != null) &&  (sgNoFlash != null)) {
				temp="";
				for (i=0; i<sgIgnor.length; i++) {
					for (k=0; k<sgNoFlash.length; k++) {
						if (sgIgnor[i].equals(sgNoFlash[k])) {
							// Widerspruch Ignorieren und Zwangstausch
							message=message+"ADR=" + sgIgnor[i]	+ " " + CfsString.get("cfs.inconsistentsgignorwithsgnoflasherror")+NL;
							if (temp.length() == 0) temp = CfsString.get("cfs.checksgignororsgnoflashhelp")+NL;
						}
					}
				}
				helpmessage=helpmessage + temp;
			}
			if ( message.length()!=0) {
				lastError= CfsString.get("cfs.checksgxxxxlist") + NL + message;
				if ( helpmessage.length()!=0)
					lastHelp=helpmessage;
				else
					lastHelp=CfsString.get("cfs.nohelp");
			} else {
				lastError=CfsString.get("cfs.noerror");
				lastHelp=CfsString.get("cfs.nohelp");
				returnFlag = true;
			}
			return returnFlag;
		}

		/**
		 * �berpr�ft die SVT-Ist gegen SVT-Soll <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 10.07.2008 <br>
		 */
		private boolean checkSVTSVT(List<CascadeECU> SVKIstListe, List<CascadeECU> SVKSollListe, boolean skipMissingECUs) {
			boolean returnFlag = false;
			boolean gefunden = false;
			String message = null;
			String helpmessage = null;
			String temp = null;
			String name = null;
			int i=0;
			int j=0;

			if (SVKIstListe==null) {
				lastError=CfsString.get("cfs.svkistlisteerror");
				lastHelp=CfsString.get("cfs.svkistlistehelp");
				return returnFlag;
			}
			if (SVKSollListe==null) {
				lastError=CfsString.get("cfs.svksolllisteerror");
				lastHelp=CfsString.get("cfs.svksolllistehelp");
				return returnFlag;
			}

			message="";
			helpmessage="";
			if (!skipMissingECUs) {
				// alle Steuerger�te vorhanden
				for (i=0; i<SVKSollListe.size(); i++) {
					name=UNKNOWN;
					temp=((CascadeECU)SVKSollListe.get(i)).getBaseVariantName();
					if (temp!=null) {
						if (temp.trim().length() > 0) name=temp;
					}
					temp=((CascadeECU)SVKSollListe.get(i)).getVariantName();
					if (temp!=null) {
						if (temp.trim().length() > 0) name=temp;
					}
					gefunden = false;
					for (j=0; j<SVKIstListe.size(); j++) {
						if (sgIgnorListe != null) {
							if (sgIgnorListe.contains(hexString(((CascadeECU)SVKSollListe.get(i)).getDiagnosticAddress().intValue(),2))) {
								gefunden = true;
								break;
							}
						}
						if (((CascadeECU)SVKSollListe.get(i)).getDiagnosticAddress().intValue() == ((CascadeECU)SVKIstListe.get(j)).getDiagnosticAddress().intValue()) {
							// �berpr�fen ob Steuerger�t auch geantwortet hat.
							// SVK ist leer wenn das SG in der white-List enthalten aber nicht antwortet
							if (((CascadeECU)SVKIstListe.get(j)).getSvk() != null ) {
								if (((CascadeECU)SVKIstListe.get(j)).getSvk().length > 0 ) {
									gefunden = true;
									break;
								}
							}
						}
					}
					if (!gefunden) {
						message=message+"ECU=" + name + " ADR=" + hexString(((CascadeECU)SVKSollListe.get(i)).getDiagnosticAddress().intValue(),2)
						+ " " + CfsString.get("cfs.ecunotfounderror")+NL;
					}
				}
			}
			// zu viele Steuerger�te vorhanden
			for (i=0; i<SVKIstListe.size(); i++) {
				name=UNKNOWN;
				temp=((CascadeECU)SVKIstListe.get(i)).getBaseVariantName();
				if (temp!=null) {
					if (temp.trim().length() > 0) name=temp;
				}
				temp=((CascadeECU)SVKIstListe.get(i)).getVariantName();
				if (temp!=null) {
					if (temp.trim().length() > 0) name=temp;
				}
				gefunden = false;
				for (j=0; j<SVKSollListe.size(); j++) {
					if (sgIgnorListe != null) {
						if (sgIgnorListe.contains(hexString(((CascadeECU)SVKIstListe.get(i)).getDiagnosticAddress().intValue(),2))) {
							gefunden = true;
							break;
						}
					}
					if (((CascadeECU)SVKIstListe.get(i)).getDiagnosticAddress().intValue() == ((CascadeECU)SVKSollListe.get(j)).getDiagnosticAddress().intValue()) {
						gefunden = true;
						break;
					}
				}
				if (!gefunden) {
					message=message+"ECU=" + name + " ADR=" + hexString(((CascadeECU)SVKIstListe.get(i)).getDiagnosticAddress().intValue(),2)
					+ " " + CfsString.get("cfs.unexpectedecuerror")+NL;
				}
			}
			if ( message.length()!=0) {
				lastError= CfsString.get("cfs.checksvksvkerror") + NL + message;
				if ( helpmessage.length()!=0)
					lastHelp=helpmessage;
				else
					lastHelp=CfsString.get("cfs.nohelp");
			} else {
				lastError=CfsString.get("cfs.noerror");
				lastHelp=CfsString.get("cfs.nohelp");
				returnFlag = true;
			}
			return returnFlag;
		}

		/**
		 * �berpr�ft die SVT-Ist bzw SVT-Soll gegen die SGVerbau-Liste <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 27.10.2008 <br>
		 */
		private boolean checkSgVerbauSVT(boolean SVKIstFlag, List<CascadeECU> SVKListe) {
			boolean returnFlag = false;
			boolean gefunden = false;
			String message = null;
			String helpmessage = null;
			int i=0;
			int j=0;

			if (SVKListe==null) {
				lastError=CfsString.get("cfs.svklisteerror");
				lastHelp=CfsString.get("cfs.svklistehelp");
				return returnFlag;
			}

			if (sgVerbauListe == null) return true;
			if (sgVerbauListe.size() == 0) return true;

			message="";
			helpmessage="";
			// alle SG vorhanden
			for (i=0; i<sgVerbauListe.size(); i++) {
				gefunden = false;
				for (j=0; j<SVKListe.size(); j++) {
					if (( Integer.decode("0x" + ((String)sgVerbauListe.get(i))).intValue()) == ((CascadeECU)SVKListe.get(j)).getDiagnosticAddress().intValue()) {
						gefunden = true;
						break;
					}
				}
				if (!gefunden) {
					message=message+"ECU=? ADR=" + (String)sgVerbauListe.get(i)	+ " ";
					if (SVKIstFlag)
						message=message+CfsString.get("cfs.ecunotfounderror")+NL;
					else
						message=message+CfsString.get("cfs.ecunotinsvtsollerror")+NL;
				}
			}
			if ( message.length()!=0) {
				lastError= CfsString.get("cfs.checksvksgverbauerror") + NL + message;
				if ( helpmessage.length()!=0)
					lastHelp=helpmessage;
				else
					lastHelp=CfsString.get("cfs.nohelp");
			} else {
				lastError=CfsString.get("cfs.noerror");
				lastHelp=CfsString.get("cfs.nohelp");
				returnFlag = true;
			}
			return returnFlag;
		}

		/**
		 * �berpr�ft die SVT-Ist bzw SVT-Soll gegen die Konfiguration <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 17.09.2008 <br>
		 */
		private boolean checkTrailerSVT(boolean SVKIstFlag, List<CascadeECU> SVKListe, HashMap<String, String> programmingDependenciesListe) {
			boolean returnFlag = false;
			boolean gefunden = false;
			String tmp = "";
			String message = null;
			String helpmessage = null;
			String temp = null;
			int i=0;
			int j=0;
			int k=0;
			int usedAddress[] = new int[256];

			if (gwListe==null) {
				lastError=CfsString.get("cfs.gwlisteerror");
				lastHelp=CfsString.get("cfs.gwlistehelp");
				return returnFlag;
			}
			if (ecuListe==null) {
				lastError=CfsString.get("cfs.eculisteerror");
				lastHelp=CfsString.get("cfs.eculistehelp");
				return returnFlag;
			}
			if (ecuListe.length==0) {
				lastError=CfsString.get("cfs.eculisteerror");
				lastHelp=CfsString.get("cfs.eculistehelp");
				return returnFlag;
			}
			if (SVKListe==null) {
				lastError=CfsString.get("cfs.svklisteerror");
				lastHelp=CfsString.get("cfs.svklistehelp");
				return returnFlag;
			}

			message="";
			helpmessage="";
			// alle Gateways vorhanden
			if (gwListe.length > 0) {
				for (i=0; i<gwListe.length; i++) {
					if (SVKIstFlag)
						gwListe[i].SVKIstIndex = new int[gwListe[i].address.length];
					else
						gwListe[i].SVKSollIndex = new int[gwListe[i].address.length];
					gwListe[i].baseVariantName = new String[gwListe[i].address.length];
					gwListe[i].variantName = new String[gwListe[i].address.length];
					for (k=0; k<gwListe[i].address.length; k++) {
						gefunden = false;
						for (j=0; j<SVKListe.size(); j++) {
							if ((gwListe[i].address[k]) == ((CascadeECU)SVKListe.get(j)).getDiagnosticAddress().intValue()) {
								if (SVKIstFlag)
									gwListe[i].SVKIstIndex[k]=j;
								else
									gwListe[i].SVKSollIndex[k]=j;
								usedAddress[gwListe[i].address[k]]++;
								temp=((CascadeECU)SVKListe.get(j)).getBaseVariantName();
								if (temp!=null) {
									if (temp.trim().length() > 0) gwListe[i].baseVariantName[k]=temp;
								}
								temp=((CascadeECU)SVKListe.get(j)).getVariantName();
								if (temp!=null) {
									if (temp.trim().length() > 0) gwListe[i].variantName[k]=temp;
								}
								gefunden = true;
								break;
							}
						}
						if (!gefunden) {
							message=message+"GW="+gwListe[i].name + " ADR=" + hexString(gwListe[i].address[k],2)
							+ " " + CfsString.get("cfs.gwnotfounderror")+NL;
						}
					}
				}
			}
			// alle Steuerger�te vorhanden
			for (i=0; i<ecuListe.length; i++) {
				if (SVKIstFlag) {
					ecuListe[i].SVKIstIndex = new int[ecuListe[i].address.length];
					ecuListe[i].programmingDependencies = new int[ecuListe[i].address.length];
				} else {
					ecuListe[i].SVKSollIndex = new int[ecuListe[i].address.length];
				}
				ecuListe[i].baseVariantName = new String[ecuListe[i].address.length];
				ecuListe[i].variantName = new String[ecuListe[i].address.length];
				for (k=0; k<ecuListe[i].address.length; k++) {
					gefunden = false;
					for (j=0; j<SVKListe.size(); j++) {
						if ((ecuListe[i].address[k]) == ((CascadeECU)SVKListe.get(j)).getDiagnosticAddress().intValue()) {
							if (SVKIstFlag)
								ecuListe[i].SVKIstIndex[k]=j;
							else
								ecuListe[i].SVKSollIndex[k]=j;
							usedAddress[ecuListe[i].address[k]]++;
							temp=((CascadeECU)SVKListe.get(j)).getBaseVariantName();
							if (temp!=null) {
								if (temp.trim().length() > 0) ecuListe[i].baseVariantName[k]=temp;
							}
							temp=((CascadeECU)SVKListe.get(j)).getVariantName();
							if (temp!=null) {
								if (temp.trim().length() > 0) ecuListe[i].variantName[k]=temp;
							}
							gefunden = true;
							break;
						}
					}
					if (!gefunden) {
						message=message+"ECU="+ecuListe[i].name + " ADR=" + hexString(ecuListe[i].address[k],2)
						+ " " + CfsString.get("cfs.ecunotfounderror")+NL;
					}
					if (SVKIstFlag) {
						tmp="1";
						if (programmingDependenciesListe!=null && programmingDependenciesListe.size()!=0) {
							tmp = (String)programmingDependenciesListe.get(hexString(ecuListe[i].address[k],0));
						}
						if (isValidSignedNr(tmp)) {
							ecuListe[i].programmingDependencies[k] = Integer.parseInt(tmp);
						} else {
							if (tmp == null) tmp = UNKNOWN;
							if (tmp.length() == 0) tmp = UNKNOWN;
							message=message+"ECU="+ecuListe[i].name + " ADR=" + hexString(ecuListe[i].address[k],2)
							+ " " + CfsString.get("cfs.invalidprogdeperror")+NL;
						}
					}
//System.out.println("ecuListe["+i+"]="+ecuListe[i].toString());
				}
			}
			// noch Steuerger�te �brig
			for (i=0; i<SVKListe.size(); i++) {
				k=((CascadeECU)SVKListe.get(i)).getDiagnosticAddress().intValue();
				if (k>=0 && k<256) {
					if (usedAddress[k]==0) {
						temp=((CascadeECU)SVKListe.get(i)).getBaseVariantName();
						if (temp==null) temp="";
						message=message+"SVK[" + i + "]: "+temp+" ADR=" + hexString(k,2)
						+ " " + CfsString.get("cfs.unexpectedecuerror")+NL;
					}
				} else {
					// sollte nie kommen
					temp=((CascadeECU)SVKListe.get(i)).getBaseVariantName();
					if (temp==null) temp="";
					message=message+"SVK[" + i + "]: "+temp+" ADR=0x" + hexString(k,2)
					+ " " + CfsString.get("cfs.invalidaddresserror")+NL;
				}
			}
			if ( message.length()!=0) {
				lastError= CfsString.get("cfs.checksvkconfigerror") + NL + message;
				if ( helpmessage.length()!=0)
					lastHelp=helpmessage;
				else
					lastHelp=CfsString.get("cfs.nohelp");
			} else {
				lastError=CfsString.get("cfs.noerror");
				lastHelp=CfsString.get("cfs.nohelp");
				returnFlag = true;
			}
			return returnFlag;
		}

		/**
		 * �berpr�ft ob eine TAL leer ist <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 16.04.2009 <br>
		 */
		private boolean isEmptyTAL(CascadeTAL TAL) {
			String message = null;
			String temp = null;
			int i=0;
			int k=0;
			int index[] = new int[256];
			boolean returnFlag = false;
			CascadeTALLine[] TALLine = null;

			if (TAL==null) {
				lastError=CfsString.get("cfs.talerror");
				lastHelp=CfsString.get("cfs.talhelp");
				return returnFlag;
			}
			TALLine = TAL.getTalLines();
			if (TALLine==null) {
				lastError=CfsString.get("cfs.tallineerror");
				lastHelp=CfsString.get("cfs.tallinehelp");
				return returnFlag;
			}
 			if (TALLine.length!=0) {
 				message = CfsString.get("cfs.talnotemptyerror")+NL;

 				// Index l�schen
 				for (i=0; i<256; i++) {
 					index[i]=-1;
 				}
 				for (i=0; i<TALLine.length; i++) {
 					k=TALLine[i].getDiagnosticAddress().intValue();
 					if (index[k]==-1) index[k]=i;
 				}
 				for (i=0; i<256; i++) {
 					if (index[i]!=-1) {
						temp=TALLine[index[i]].getBaseVariantName();
						if (temp==null)	temp = UNKNOWN;
						temp = temp + " ";
						message = message + temp + "ADR=0x" + hexString(i,2) + NL;
 					}
 				}
 				lastError=message;
				lastHelp=CfsString.get("cfs.talnotemptyhelp");
				return returnFlag;
			}
			return true;
		}

		/**
		 * �berpr�ft den KIS-Level mit KIS-WB <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 15.05.2009 <br>
		 * @param string
		 */
		private boolean checkKisCalculationLevel(String kisCalculationLevel, String sollIStufe) {
			boolean returnFlag = false;
			int i=0;
			String baureihenverbund = null;
			CascadeKISWBVersion KisWBVersion = null;

			if (kisCalculationLevel==null) {
				lastError=CfsString.get("cfs.kiscalculationlevelerror");
				lastHelp=CfsString.get("cfs.kiscalculationlevelhelp");
				return returnFlag;
			}
			if (kisCalculationLevel.length()==0) {
				lastError=CfsString.get("cfs.kiscalculationlevelerror");
				lastHelp=CfsString.get("cfs.kiscalculationlevelhelp");
				return returnFlag;
			}
			if ("MEG".indexOf(kisCalculationLevel.charAt(0)) == -1) {
				lastError=CfsString.get("cfs.kiscalculationlevelerror");
				lastHelp=CfsString.get("cfs.kiscalculationlevelhelp");
				return returnFlag;
			}

			if (!isValidIStufe(sollIStufe)) {
				lastError = CfsString.get("cfs.invalidilevel");
				lastHelp = CfsString.get("cfs.invalidilevelhelp");
				return returnFlag;
			}

			if (PSdZ.versionInfo.getKisWBVersions()==null) {
				lastError = CfsString.get("cfs.invalidkiswbversionerror");
				lastHelp = CfsString.get("cfs.invalidkiswbversionhelp");
				return returnFlag;
			}

			if (PSdZ.versionInfo.getKisWBVersions().size()==0) {
				lastError = CfsString.get("cfs.invalidkiswbversionerror");
				lastHelp = CfsString.get("cfs.invalidkiswbversionhelp");
				return returnFlag;
			}

			lastError = CfsString.get("cfs.invalidkiswbversionerror");
			lastHelp = CfsString.get("cfs.invalidkiswbversionhelp");
			baureihenverbund = sollIStufe.substring(0,4);
			for (i=0; i<PSdZ.versionInfo.getKisWBVersions().size(); i++) {
				KisWBVersion = (CascadeKISWBVersion) PSdZ.versionInfo.getKisWBVersions().get(i);
				if (KisWBVersion==null) {
					lastError = CfsString.get("cfs.invalidkiswbversionerror");
					lastHelp = CfsString.get("cfs.invalidkiswbversionhelp");
					return returnFlag;
				}
				if (KisWBVersion.getBaureihenverbund().equalsIgnoreCase(baureihenverbund)) {
					switch( kisCalculationLevel.toUpperCase().charAt(0) ) {
					case 'M':
						if ((KisWBVersion.getVerwendungskennzeichen()==2) || (KisWBVersion.getVerwendungskennzeichen()==1)) {
							lastError=CfsString.get("cfs.noerror");
							lastHelp=CfsString.get("cfs.nohelp");
							returnFlag = true;
						} else {
							lastError = CfsString.get("cfs.kislevelkiswberror");
							lastHelp = CfsString.get("cfs.kislevelkiswbhelp");
						}
						break;
					case 'E':
						lastError = CfsString.get("cfs.kislevelkiswberror");
						lastHelp = CfsString.get("cfs.kislevelkiswbhelp");
						break;
					case 'G':
						if ((KisWBVersion.getVerwendungskennzeichen()==3) || (KisWBVersion.getVerwendungskennzeichen()==1)) {
							lastError=CfsString.get("cfs.noerror");
							lastHelp=CfsString.get("cfs.nohelp");
							returnFlag = true;
						} else {
							lastError = CfsString.get("cfs.kislevelkiswberror");
							lastHelp = CfsString.get("cfs.kislevelkiswbhelp");
						}
						break;

					}
					break;
				}
			}
			return returnFlag;
		}

		/**
		 * �berpr�ft die Gesamtfahrzeug-TAL auf Plausibilit�t <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 13.01.2014 <br>
		 */
		private boolean checkFzgTAL(CascadeTAL TAL) {
			boolean returnFlag = false;
			String message = null;
			String helpmessage = null;
			String temp = null;
			int i=0;
			int j=0;
			int k=0;
			boolean adrInstall[] = new boolean[256];
			boolean adrDeInstall[] = new boolean[256];
			boolean adrProgram[] = new boolean[256];
			CascadeTALLine[] TALLine = null;
			CascadeTACategories category = null;

			sgProgrammListe = new ArrayList<String>();
			sgEinbauListe = new ArrayList<String>();
			sgAusbauListe = new ArrayList<String>();
			sgTauschListe = new ArrayList<String>();

			if (TAL==null) {
				lastError=CfsString.get("cfs.talerror");
				lastHelp=CfsString.get("cfs.talhelp");
				return returnFlag;
			}
			TALLine = TAL.getTalLines();
			if (TALLine==null) {
				lastError=CfsString.get("cfs.tallineerror");
				lastHelp=CfsString.get("cfs.tallinehelp");
				return returnFlag;
			}
/*
 			if (TALLine.length==0) {
				lastError=myString.get("cfs.emptytallineerror");
				lastHelp=myString.get("cfs.emptytallinehelp");
				return returnFlag;
			}
*/
			message="";
			helpmessage="";

			// Install und Deinstall l�schen
			for (i=0; i<256; i++) {
				adrInstall[i]=false;
				adrDeInstall[i]=false;
			}
			for (i=0; i<TALLine.length; i++) {
				k=TALLine[i].getDiagnosticAddress().intValue();
				if (k>=0 && k<256) {
					for( j = 0; j < TALLine[i].getTALTACategories().length; j++ ) {
						category = TALLine[i].getTALTACategories()[j].getCategory();
						if (category == CascadeTACategories.HW_INSTALL) {
							adrInstall[k]=true;
						} else {
							if (category == CascadeTACategories.HW_DEINSTALL) {
								adrDeInstall[k]=true;
							} else {
								if (category == CascadeTACategories.SW_DEPLOY) {
									// Kein Flash wenn neue HW
									if (adrInstall[k]==false) {
										adrProgram[k]=true;
									}
								} else {
									if (category == CascadeTACategories.BL_FLASH) {
										// Kein Flash wenn neue HW
										if (adrInstall[k]==false) {
											if (allowBootLoaderUpdate) {
												adrProgram[k]=true;
											} else {
												temp=TALLine[i].getBaseVariantName();
												if (temp==null) temp="";
												message=message+"TALLine[" + i + "]: "+temp	+ " " +
												category.toString()+ " " +
												CfsString.get("cfs.bootupdatenotallowederror")+NL;
											}
										}
									} else {
										if (category == CascadeTACategories.IBA_DEPLOY) {
											// Kein IBA wenn neue HW
											if (adrInstall[k]==false) {
												if (allowIBAUpdate) {
													adrProgram[k]=true;
												} else {
													temp=TALLine[i].getBaseVariantName();
													if (temp==null) temp="";
													message=message+"TALLine[" + i + "]: "+temp	+ " " +
													category.toString()+ " " +
													CfsString.get("cfs.ibaupdatenotallowederror")+NL;
												}
											}
										} else {
											if (category == CascadeTACategories.GWTB_DEPLOY) {
												// Kein GWTB wenn neue HW
												if (adrInstall[k]==false) {
													if (allowGWTBUpdate) {
														adrProgram[k]=true;
													} else {
														temp=TALLine[i].getBaseVariantName();
														if (temp==null) temp="";
														message=message+"TALLine[" + i + "]: "+temp	+ " " +
														category.toString()+ " " +
														CfsString.get("cfs.gwtbupdatenotallowederror")+NL;
													}
												}
											} else {
												temp=TALLine[i].getBaseVariantName();
												if (temp==null) temp="";
												message=message+"TALLine[" + i + "]: "+temp	+ " " +
												category.toString()+ " " +
												CfsString.get("cfs.invalidcategoryerror")+NL;
											}
										}
									}
								}
							}
						}
					}
				} else {
					// sollte nie kommen
					temp=TALLine[i].getBaseVariantName();
					if (temp==null) temp="";
					message=message+"TALLine[" + i + "]: "+temp+" ADR=0x" + hexString(k,2)
					+ " " + CfsString.get("cfs.invalidaddresserror")+NL;
				}
			}
			if ( message.length()==0) {
				for (i=0; i<256; i++) {
					if (adrInstall[i]==true  && adrDeInstall[i]==false)
						sgEinbauListe.add(hexString(i,2));
					if (adrInstall[i]==false && adrDeInstall[i]==true)
						sgAusbauListe.add(hexString(i,2));
					if (adrInstall[i]==true  && adrDeInstall[i]==true)
						sgTauschListe.add(hexString(i,2));
					if (adrInstall[i]==false && adrDeInstall[i]==false && adrProgram[i]==true)
						sgProgrammListe.add(hexString(i,2));
				}
			}
			if ( message.length()!=0) {
				lastError= CfsString.get("cfs.checktalerror") + NL + message;
				if ( helpmessage.length()!=0)
					lastHelp=helpmessage;
				else
					lastHelp=CfsString.get("cfs.nohelp");
			} else {
				lastError=CfsString.get("cfs.noerror");
				lastHelp=CfsString.get("cfs.nohelp");
				returnFlag = true;
			}
			return returnFlag;
		}

		/**
		 * Ver�ndert die PSdZ-TAL f�r Trailer <br>
		 * Anlieferprogrammierung, <br>
		 * Workaround wegen Gateway-Routing-Problem wenn direkt ohne SUB-Gateway programmiert wird <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 26.11.2015 <br>
		 */
		private boolean modifyTAL(PSdZ psdz, boolean anlieferzustand, boolean installedECUList) {
			boolean returnFlag = false;
			int p1;
			int p2;
			int p3;
			String str = null;
			String dateiName = "";
			String temp;
	        File file = null;
	        BufferedWriter bufferedWriter = null;

			if (ecuListe==null) {
				lastError=CfsString.get("cfs.eculisteerror");
				lastHelp=CfsString.get("cfs.eculistehelp");
				return returnFlag;
			}
			if (ecuListe.length==0) {
				lastError=CfsString.get("cfs.eculisteerror");
				lastHelp=CfsString.get("cfs.eculistehelp");
				return returnFlag;
			}
			if (psdz==null) {
				lastError=CfsString.get("cfs.psdzerror");
				lastHelp=CfsString.get("cfs.psdzhelp");
				return returnFlag;
			}

			try {
				str = psdz.getTALReader().getPsdzTALasXML();
			} catch (Exception e) {
				str = null;
			}
			if (str==null) {
				lastError=CfsString.get("cfs.talerror");
				lastHelp=CfsString.get("cfs.talhelp");
				return returnFlag;
			}

			if (anlieferzustand) {
				for (int i=0; i<ecuListe.length; i++) {
					for (int k=0; k<ecuListe[i].address.length; k++) {
						if (isValidSgbmId(ecuListe[i].sgbmIdAnlieferzustand[k])) {
							p2 = str.indexOf(ecuListe[i].sgbmIdAnlieferzustand[k].substring(5, 13));
							if (p2 != -1) {
								p3 = str.indexOf("</swDeployTA>",p2);
								if (p3 == -1) {
									lastError=CfsString.get("cfs.xmlformaterror");
									lastHelp=CfsString.get("cfs.xmlformathelp");
									return returnFlag;
								} else {
									p1 = str.lastIndexOf("<swDeployTA ",p2);
									if (p1 == -1) {
										lastError=CfsString.get("cfs.xmlformaterror");
										lastHelp=CfsString.get("cfs.xmlformathelp");
										return returnFlag;
									} else {
										str = str.substring(0,p1) + str.substring(p3+13);
									}
								}
							}
						}
					}
				}
			}

			if (installedECUList) {
				for (int i=0; i<ecuListe.length; i++) {
					for (int k=0; k<ecuListe[i].address.length; k++) {
						// Sonderbehandlung bei SME 0x07 -> Gateway ist DME 0x12
						// Sonderbehandlung bei EKP 0x17 -> Gateway ist DME 0x12
						// Sonderbehandlung bei FRR 0x21 -> Gateway ist ICM 0x1C
						if ((ecuListe[i].address[k] == 0x07) ||	(ecuListe[i].address[k] == 0x17) ||
							(ecuListe[i].address[k] == 0x21)) {
							p2 = str.indexOf("<installedECUList_Ist>");
							if (p2 == -1) {
								lastError=CfsString.get("cfs.xmlformaterror");
								lastHelp=CfsString.get("cfs.xmlformathelp");
								return returnFlag;
							}
							p3 = str.indexOf("</installedECUList_Ist>",p2);
							if (p3 == -1) {
								lastError=CfsString.get("cfs.xmlformaterror");
								lastHelp=CfsString.get("cfs.xmlformathelp");
								return returnFlag;
							}
							if ((ecuListe[i].address[k] == 0x07) ||	(ecuListe[i].address[k] == 0x17)) {
								p1 = str.indexOf("diagnosticAddress=\"18\"",p2);
								temp="<ecuID diagnosticAddress=\"18\" baseVariantName=\"DME\"/>";
							} else {
								p1 = str.indexOf("diagnosticAddress=\"28\"",p2);
								temp="<ecuID diagnosticAddress=\"28\" baseVariantName=\"ICM\"/>";
							}
							if ((p1 == -1) || (p1 > p3)) {
								str = str.substring(0,p2+22) + temp + str.substring(p2+22);
							}
						}
					}
				}
			}

			try {
				dateiName = CascadeProperties.getCascadeHome() + TRACEPATH;
				dateiName = dateiName + rechnerName + "_" + instanz + "_TAL.XML";
				file = new File( dateiName );
				bufferedWriter = new BufferedWriter( new FileWriter( file, false));
				bufferedWriter.write(str);
				bufferedWriter.close();
			} catch (Exception e) {
				lastError=dateiName + NL + CfsString.get("cfs.writeerror") + NL	+ e.getMessage();
				lastHelp=CfsString.get("cfs.nohelp");
				return returnFlag;
			}
			try {
				psdz.readAndStoreTALfromFile(dateiName, true);
			} catch (Exception e) {
				lastError=CfsString.get("cfs.talerror");
				lastHelp=CfsString.get("cfs.talhelp");
				return returnFlag;
			}
			lastError=CfsString.get("cfs.noerror");
			lastHelp=CfsString.get("cfs.nohelp");
			returnFlag = true;
			return returnFlag;
		}

		/**
		 * �berpr�ft die Trailer-TAL auf Plausibilit�t <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 13.01.2014 <br>
		 */
		private boolean checkTrailerTAL(CascadeTAL TAL, List<CascadeECU> SVKIstListe, List<CascadeECU> SVKSollListe) {
			boolean returnFlag = false;
			boolean gefunden = false;
			String message = null;
			String helpmessage = null;
			String temp = null;
			String klasse = null;
			int i=0;
			int j=0;
			int k=0;
			int m=0;
			int n=0;
			int usedAddress[] = new int[256];
			CascadeTALLine[] TALLine = null;
			CascadeTACategories category = null;

			if (TAL==null) {
				lastError=CfsString.get("cfs.talerror");
				lastHelp=CfsString.get("cfs.talhelp");
				return returnFlag;
			}
			TALLine = TAL.getTalLines();
			if (TALLine==null) {
				lastError=CfsString.get("cfs.tallineerror");
				lastHelp=CfsString.get("cfs.tallinehelp");
				return returnFlag;
			}
/*
 			if (TALLine.length==0) {
				lastError=myString.get("cfs.emptytallineerror");
				lastHelp=myString.get("cfs.emptytallinehelp");
				return returnFlag;
			}
*/
			if (ecuListe==null) {
				lastError=CfsString.get("cfs.eculisteerror");
				lastHelp=CfsString.get("cfs.eculistehelp");
				return returnFlag;
			}
			if (ecuListe.length==0) {
				lastError=CfsString.get("cfs.eculisteerror");
				lastHelp=CfsString.get("cfs.eculistehelp");
				return returnFlag;
			}
			if (SVKIstListe==null) {
				lastError=CfsString.get("cfs.svkistlisteerror");
				lastHelp=CfsString.get("cfs.svkistlistehelp");
				return returnFlag;
			}

			if (SVKSollListe==null) {
				lastError=CfsString.get("cfs.svksolllisteerror");
				lastHelp=CfsString.get("cfs.svksolllistehelp");
				return returnFlag;
			}

			message="";
			helpmessage="";
			// alle Steuerger�te vorhanden
			for (i=0; i<ecuListe.length; i++) {
				for (k=0; k<ecuListe[i].address.length; k++) {
					gefunden = false;
					for (j=0; j<TALLine.length; j++) {
						if ((ecuListe[i].address[k]) == TALLine[j].getDiagnosticAddress().intValue()) {
							usedAddress[ecuListe[i].address[k]]++;
							gefunden = true;
							break;
						}
					}
					if (!gefunden) {
						// SG hat eventuell den richtigen Stand
						// SGBM-IDs holen
						CascadeSGBMID[] SVKSollSGBMID=((CascadeECU)SVKSollListe.get(ecuListe[i].SVKSollIndex[k])).getSvk();
						String[] SGBMIDSoll = new String[SVKSollSGBMID.length];
						for (m=0; m<SGBMIDSoll.length; m++) {
							SGBMIDSoll[m] = SVKSollSGBMID[m].getProcessClass().toString() + "-" + SVKSollSGBMID[m].getId() + "-" +
							dezString(SVKSollSGBMID[m].getMainVersion(),3) + "." + dezString(SVKSollSGBMID[m].getSubVersion(),3) + "." + dezString(SVKSollSGBMID[m].getPatchVersion(),3);
						}
						CascadeSGBMID[] SVKIstSGBMID=((CascadeECU)SVKIstListe.get(ecuListe[i].SVKIstIndex[k])).getSvk();
						String[] SGBMIDIst = new String[SVKIstSGBMID.length];
						for (m=0; m<SGBMIDIst.length; m++) {
							SGBMIDIst[m] = SVKIstSGBMID[m].getProcessClass().toString() + "-" + SVKIstSGBMID[m].getId() + "-" +
							dezString(SVKIstSGBMID[m].getMainVersion(),3) + "." + dezString(SVKIstSGBMID[m].getSubVersion(),3) + "." + dezString(SVKIstSGBMID[m].getPatchVersion(),3);
						}
						for (m=0; m<SGBMIDSoll.length; m++) {
							if (SGBMIDSoll[m].startsWith(CascadeProcessClasses.BTLD.toString()) ||
								SGBMIDSoll[m].startsWith(CascadeProcessClasses.FLSL.toString()) ||
								SGBMIDSoll[m].startsWith(CascadeProcessClasses.SWFK.toString()) ||
								SGBMIDSoll[m].startsWith(CascadeProcessClasses.SWFL.toString()) ||
								SGBMIDSoll[m].startsWith(CascadeProcessClasses.IBAD.toString()) ||
								SGBMIDSoll[m].startsWith(CascadeProcessClasses.GWTB.toString()) ) {
								gefunden = false;
								for (n=0; n<SGBMIDIst.length; n++) {
									if (SGBMIDSoll[m].equalsIgnoreCase(SGBMIDIst[n])) {
										gefunden = true;
										break;
									}
								}
								if (!gefunden) {
									message=message+"ECU="+ecuListe[i].name + " ADR=" + hexString(ecuListe[i].address[k],2)
									+ " " + CfsString.get("cfs.ecunotfounderror")+NL;
								}
							}
						}
					}
				}
			}
			// noch Steuerger�te �brig
			for (i=0; i<TALLine.length; i++) {
				k=TALLine[i].getDiagnosticAddress().intValue();
				if (k>=0 && k<256) {
					if (usedAddress[k]==0) {
						temp=TALLine[i].getBaseVariantName();
						if (temp==null) temp="";
						message=message+"TALLine[" + i + "]: "+temp+" ADR=" + hexString(k,2)
						+ " " + CfsString.get("cfs.unexpectedecuerror")+NL;
					}
				} else {
					// sollte nie kommen
					temp=TALLine[i].getBaseVariantName();
					if (temp==null) temp="";
					message=message+"TALLine[" + i + "]: "+temp+" ADR=0x" + hexString(k,2)
					+ " " + CfsString.get("cfs.invalidaddresserror")+NL;
				}
			}
			// TAL Aktionen und Prozessklassen g�ltig
			if ( message.length()==0) {
				for (i=0; i<TALLine.length; i++) {
					for( j = 0; j < TALLine[i].getTALTACategories().length; j++ ) {
						category = TALLine[i].getTALTACategories()[j].getCategory();
						if (category == CascadeTACategories.BL_FLASH) {
							if (allowBootLoaderUpdate) {
								for( k = 0; k < TALLine[i].getTALTACategories()[j].getTAs().length; k++ ) {
									klasse = TALLine[i].getTALTACategories()[j].getTAs()[k].getProcessClass();
									if ( !klasse.equalsIgnoreCase(CascadeProcessClasses.BTLD) &&
										 !klasse.equalsIgnoreCase(CascadeProcessClasses.FLSL) &&
										 !klasse.equalsIgnoreCase(CascadeProcessClasses.BLUP) &&
										 !klasse.equalsIgnoreCase(CascadeProcessClasses.FLUP)) {
										temp=TALLine[i].getBaseVariantName();
										if (temp==null) temp="";
										message=message+"TALLine[" + i + "]: "+temp	+ " " +
										category.toString()+ " " + klasse + " " +
										CfsString.get("cfs.invalidprocessclasserror")+NL;
									}
								}
							} else {
								temp=TALLine[i].getBaseVariantName();
								if (temp==null) temp="";
								message=message+"TALLine[" + i + "]: "+temp	+ " " +
								category.toString()+ " " +
								CfsString.get("cfs.bootupdatenotallowederror")+NL;
							}
						} else {
							if (category == CascadeTACategories.IBA_DEPLOY) {
								if (allowIBAUpdate) {
									for( k = 0; k < TALLine[i].getTALTACategories()[j].getTAs().length; k++ ) {
										klasse = TALLine[i].getTALTACategories()[j].getTAs()[k].getProcessClass();
										if ( !klasse.equalsIgnoreCase(CascadeProcessClasses.IBAD)) {
											temp=TALLine[i].getBaseVariantName();
											if (temp==null) temp="";
											message=message+"TALLine[" + i + "]: "+temp	+ " " +
											category.toString()+ " " + klasse + " " +
											CfsString.get("cfs.invalidprocessclasserror")+NL;
										}
									}
								} else {
									temp=TALLine[i].getBaseVariantName();
									if (temp==null) temp="";
									message=message+"TALLine[" + i + "]: "+temp	+ " " +
									category.toString()+ " " +
									CfsString.get("cfs.ibaupdatenotallowederror")+NL;
								}
							} else {
								if (category == CascadeTACategories.GWTB_DEPLOY) {
									if (allowGWTBUpdate) {
										for( k = 0; k < TALLine[i].getTALTACategories()[j].getTAs().length; k++ ) {
											klasse = TALLine[i].getTALTACategories()[j].getTAs()[k].getProcessClass();
											if ( !klasse.equalsIgnoreCase(CascadeProcessClasses.GWTB)) {
												temp=TALLine[i].getBaseVariantName();
												if (temp==null) temp="";
												message=message+"TALLine[" + i + "]: "+temp	+ " " +
												category.toString()+ " " + klasse + " " +
												CfsString.get("cfs.invalidprocessclasserror")+NL;
											}
										}
									} else {
										temp=TALLine[i].getBaseVariantName();
										if (temp==null) temp="";
										message=message+"TALLine[" + i + "]: "+temp	+ " " +
										category.toString()+ " " +
										CfsString.get("cfs.gwtbupdatenotallowederror")+NL;
									}
								} else {
									if (category == CascadeTACategories.SW_DEPLOY) {
										for( k = 0; k < TALLine[i].getTALTACategories()[j].getTAs().length; k++ ) {
											klasse = TALLine[i].getTALTACategories()[j].getTAs()[k].getProcessClass();
											if ( !klasse.equalsIgnoreCase(CascadeProcessClasses.SWFL) &&
												 !klasse.equalsIgnoreCase(CascadeProcessClasses.SWFK)) {
												temp=TALLine[i].getBaseVariantName();
												if (temp==null) temp="";
												message=message+"TALLine[" + i + "]: "+temp	+ " " +
												category.toString()+ " " + klasse + " " +
												CfsString.get("cfs.invalidprocessclasserror")+NL;
											}
										}
									} else {
										temp=TALLine[i].getBaseVariantName();
										if (temp==null) temp="";
										message=message+"TALLine[" + i + "]: "+temp	+ " " +
										category.toString()+ " " +
										CfsString.get("cfs.invalidcategoryerror")+NL;
									}
								}
							}
						}
					}
				}
			}
			if ( message.length()!=0) {
				lastError= CfsString.get("cfs.checktalerror") + NL + message;
				if ( helpmessage.length()!=0)
					lastHelp=helpmessage;
				else
					lastHelp=CfsString.get("cfs.nohelp");
			} else {
				lastError=CfsString.get("cfs.noerror");
				lastHelp=CfsString.get("cfs.nohelp");
				returnFlag = true;
			}
			return returnFlag;
		}

		/**
		 * Formatierung f�r Log-Datei ohne Zeilenumbruch <br>
		 * linksb�ndig mit L�nge len (1 bis max 30) <br>
		 * Bei len <= 0 keine Formatierung <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 13.06.2007 <br>
		 */
		private String logString(String in, int len ) {
			final String s30 = "                              ";
			if (in == null) in = "";
			if (len <= 0) return in;
			if (len > 30) len = 30;
			return (in + s30).substring(0,len);
		}

		/**
		 * Formatierung f�r Log-Datei mit Zeilenumbruch <br>
		 * linksb�ndig mit L�nge len (1 bis max 30) <br>
		 * Bei len = 0 keine Formatierung
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 13.06.2007 <br>
		 */
		private String logStringNL(String in, int len ) {
			return logString(in, len )+ CRLF;
		}

		/**
		 * Formatierung in Trace-Datei Format <br>
		 * linksb�ndig mit L�nge len (1 bis max 30) und abgeschlossen mit "; " <br>
		 * Bei len = 0 keine Formatierung
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 15.02.2007 <br>
		 */
		private String traceString(String in, int len ) {
			final String s30 = "                              ";
			if (in == null) in = "";
			if (len == 0) return in + "; ";
			if (len > 30) len = 30;
			return (in + s30).substring(0,len) + "; ";
		}

		/**
		 * Erzeugt den Gesamtfahrzeug-Log und speichert ihn in die Log-Datei <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 11.02.2010 <br>
		 */
		private boolean saveFzgLog(LogFileElement logFileElement, boolean appendFlag ) {
			boolean returnFlag = false;

			if (logFileElement==null) {
				lastError=CfsString.get("cfs.logfileelementerror");
				lastHelp=CfsString.get("cfs.logfileelementhelp");
				return returnFlag;
			}

			if (appendFlag) {
				if (!appendLogFile(logFileElement)) return returnFlag;
			} else {
				if (!saveLogFile(logFileElement)) return returnFlag;
			}
			returnFlag=true;
			return returnFlag;
		}

		/**
		 * Erzeugt den Gesamtfahrzeug-Trace und speichert ihn in die Trace-Datei <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 17.04.2009 <br>
		 */
		private boolean saveFzgTrace(TraceFileElement traceFileElement , Vector<Ergebnis> ergListe) {
			boolean returnFlag = false;
			boolean adrBelegt[] = new boolean[256];
			int i = 0;
			int j = 0;
			int m = 0;
			int n = 0;
			int p = 0;
			long timeMillisGesamtBackup = 0;
			long timeMillisDauer = 0;
			String temp = null;
			CascadeTALLine[] TALLine = null;

			if (traceFileElement==null) {
				lastError=CfsString.get("cfs.tracefileelementerror");
				lastHelp=CfsString.get("cfs.tracefileelementhelp");
				return returnFlag;
			}

			if (traceFileElement.TAL==null) {
				lastError=CfsString.get("cfs.talerror");
				lastHelp=CfsString.get("cfs.talhelp");
				return returnFlag;
			}
			TALLine = traceFileElement.TAL.getTalLines();
			if (TALLine==null) {
				lastError=CfsString.get("cfs.tallineerror");
				lastHelp=CfsString.get("cfs.tallinehelp");
				return returnFlag;
			}
/*
			if (TALLine.length==0) {
				lastError=myString.get("cfs.emptytallineerror");
				lastHelp=myString.get("cfs.emptytallinehelp");
				return returnFlag;
			}
*/
			if (TALLine.length == 0) {
				traceFileElement.SGBMID = new String[1];
				traceFileElement.SGBMID[0] = CfsString.get("cfs.emptytal");
				traceFileElement.address = -1;
				if (!saveTraceFile(traceFileElement, ergListe)) return returnFlag;
			} else {
				timeMillisGesamtBackup = traceFileElement.timeMillisGesamt;
				for (i=0; i<adrBelegt.length; i++) {
					adrBelegt[i] = false;
				}
				for (i=0; i<TALLine.length; i++) {
					adrBelegt[TALLine[i].getDiagnosticAddress().intValue()] = true;
				}
				for (i=0; i<adrBelegt.length; i++) {
					if (adrBelegt[i]) {
						timeMillisDauer = 0;
						if (traceFileElement.timeMillisVorbereitungStart > 0 &&
							traceFileElement.timeMillisVorbereitungFertig > 0 &&
							traceFileElement.timeMillisNachbereitungStart > 0 &&
							traceFileElement.timeMillisNachbereitungFertig > 0)
							timeMillisDauer = traceFileElement.timeMillisVorbereitungFertig - traceFileElement.timeMillisVorbereitungStart
							 				+ traceFileElement.timeMillisNachbereitungFertig - traceFileElement.timeMillisNachbereitungStart;
						traceFileElement.name = null;
						traceFileElement.baseVariantName=null;
						traceFileElement.variantName=null;
						traceFileElement.address = 0;
						traceFileElement.lieferUmfangNummer = null;
						traceFileElement.okayFlag = true;
						n=0;
						for (j=0; j<TALLine.length; j++) {
							if (i == TALLine[j].getDiagnosticAddress().intValue()) {
								if (traceFileElement.lieferUmfangNummerTalLine != null && traceFileElement.lieferUmfangNummerTalLine.length > j) {
									temp=traceFileElement.lieferUmfangNummerTalLine[j];
									if (temp!=null && traceFileElement.lieferUmfangNummer==null) {
										if (temp.trim().length() > 0) traceFileElement.lieferUmfangNummer = temp;
									}
								}
								if (traceFileElement.timeValidTalLine[j] == true) {
									if (timeMillisDauer > 0)
										timeMillisDauer = timeMillisDauer + traceFileElement.timeMillisTalLine[j];
								} else
									timeMillisDauer = 0;
								traceFileElement.address = i;
								if (TALLine[j].getStatus() != CascadeExecutionStatus.PROCESSED)
									traceFileElement.okayFlag = false;

								for (m=0; m<TALLine[j].getTALTACategories().length; m++) {
									n = n + TALLine[j].getTALTACategories()[m].getTAs().length;
								}
								temp=TALLine[j].getBaseVariantName();
								if (temp!=null && traceFileElement.baseVariantName==null) {
									if (temp.trim().length() > 0) traceFileElement.baseVariantName = temp;
								}
								temp=TALLine[j].getVariantName();
								if (temp!=null && traceFileElement.variantName==null) {
									if (temp.trim().length() > 0) traceFileElement.variantName = temp;
								}
							}
						}
						if (timeMillisDauer > 0)
							traceFileElement.timeMillisGesamt = timeMillisDauer;
						else
							traceFileElement.timeMillisGesamt = timeMillisGesamtBackup;
						if (n>0) {
							traceFileElement.SGBMID = new String[n];
							n=0;
							for (j=0; j<TALLine.length; j++) {
								if (i == TALLine[j].getDiagnosticAddress().intValue()) {
									for (m=0; m<TALLine[j].getTALTACategories().length; m++) {
										for (p=0;p<TALLine[j].getTALTACategories()[m].getTAs().length; p++) {
											temp = TALLine[j].getTALTACategories()[m].getTAs()[p].getProcessClass() + "-" +	TALLine[j].getTALTACategories()[m].getTAs()[p].getId() + "-" +
												dezString(TALLine[j].getTALTACategories()[m].getTAs()[p].getMainVersion() ,3) + "." +
												dezString(TALLine[j].getTALTACategories()[m].getTAs()[p].getSubVersion() ,3) + "." +
												dezString(TALLine[j].getTALTACategories()[m].getTAs()[p].getPatchVersion() ,3);
											traceFileElement.SGBMID[n]=temp;
											n++;
										}
									}
								}
							}
						} else {
							traceFileElement.SGBMID = new String[1];
							traceFileElement.SGBMID[0] = CfsString.get("cfs.emptytal");
						}
						if (!saveTraceFile(traceFileElement, ergListe)) return returnFlag;
					}
				}
			}
			returnFlag=true;
			return returnFlag;
		}

		/**
		 * Erzeugt den Trailer-Trace und speichert ihn in die Trace-Datei <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 17.04.2009 <br>
		 */
		private boolean saveTrailerTrace(TraceFileElement traceFileElement ) {
			boolean returnFlag = false;
			int i=0;
			int j=0;
			int k=0;
			int m=0;
			int n=0;
			int p=0;
			long timeMillisGesamtBackup = 0;
			long timeMillisDauer = 0;
			String temp = null;
			CascadeTALLine[] TALLine = null;

			if (traceFileElement==null) {
				lastError=CfsString.get("cfs.tracefileelementerror");
				lastHelp=CfsString.get("cfs.tracefileelementhelp");
				return returnFlag;
			}
			if (ecuListe==null) {
				lastError=CfsString.get("cfs.eculisteerror");
				lastHelp=CfsString.get("cfs.eculistehelp");
				return returnFlag;
			}
			if (ecuListe.length==0) {
				lastError=CfsString.get("cfs.eculisteerror");
				lastHelp=CfsString.get("cfs.eculistehelp");
				return returnFlag;
			}

			if (traceFileElement.TAL==null) {
				lastError=CfsString.get("cfs.talerror");
				lastHelp=CfsString.get("cfs.talhelp");
				return returnFlag;
			}
			TALLine = traceFileElement.TAL.getTalLines();
			if (TALLine==null) {
				lastError=CfsString.get("cfs.tallineerror");
				lastHelp=CfsString.get("cfs.tallinehelp");
				return returnFlag;
			}
/*
			if (TALLine.length==0) {
				lastError=myString.get("cfs.emptytallineerror");
				lastHelp=myString.get("cfs.emptytallinehelp");
				return returnFlag;
			}
*/
			timeMillisGesamtBackup = traceFileElement.timeMillisGesamt;
			for (i=0; i<ecuListe.length; i++) {
				for (k=0; k<ecuListe[i].address.length; k++) {
					timeMillisDauer = 0;
					if (traceFileElement.timeMillisVorbereitungStart > 0 &&
						traceFileElement.timeMillisVorbereitungFertig > 0 &&
						traceFileElement.timeMillisNachbereitungStart > 0 &&
						traceFileElement.timeMillisNachbereitungFertig > 0)
						timeMillisDauer = traceFileElement.timeMillisVorbereitungFertig - traceFileElement.timeMillisVorbereitungStart
						 				+ traceFileElement.timeMillisNachbereitungFertig - traceFileElement.timeMillisNachbereitungStart;
					traceFileElement.name = ecuListe[i].name;
					traceFileElement.baseVariantName=null;
					traceFileElement.variantName=null;
					traceFileElement.address = ecuListe[i].address[k];
					traceFileElement.lieferUmfangNummer = ecuListe[i].lieferUmfangNummer[k];
					traceFileElement.sgbmIdAnlieferzustand = ecuListe[i].sgbmIdAnlieferzustand[k];
					traceFileElement.okayFlag = true;
					n=0;
					for (j=0; j<TALLine.length; j++) {
						if ((ecuListe[i].address[k]) == TALLine[j].getDiagnosticAddress().intValue()) {
							if (traceFileElement.timeValidTalLine[j] == true) {
								if (timeMillisDauer > 0)
									timeMillisDauer = timeMillisDauer + traceFileElement.timeMillisTalLine[j];
							} else
								timeMillisDauer = 0;
							if (TALLine[j].getStatus() != CascadeExecutionStatus.PROCESSED) {
								if (TALLine[j].getStatus() == CascadeExecutionStatus.PROCESSEDWITHERROR) {
									if (!getFehlerAusblenden(TALLine[j])) traceFileElement.okayFlag = false;
								} else {
									traceFileElement.okayFlag = false;
								}
							}

							for (m=0; m<TALLine[j].getTALTACategories().length; m++) {
								n = n + TALLine[j].getTALTACategories()[m].getTAs().length;
							}
							temp=TALLine[j].getBaseVariantName();
							if (temp!=null && traceFileElement.baseVariantName==null) {
								if (temp.trim().length() > 0) traceFileElement.baseVariantName = temp;
							}
							temp=TALLine[j].getVariantName();
							if (temp!=null && traceFileElement.variantName==null) {
								if (temp.trim().length() > 0) traceFileElement.variantName = temp;
							}
						}
					}
					if (traceFileElement.baseVariantName==null) {
						temp=ecuListe[i].baseVariantName[k];
						if (temp != null) {
							if (temp.trim().length() > 0) traceFileElement.baseVariantName = temp;
						}
					}
					if (traceFileElement.variantName==null) {
						temp=ecuListe[i].variantName[k];
						if (temp != null) {
							if (temp.trim().length() > 0) traceFileElement.variantName = temp;
						}
					}
					if (timeMillisDauer > 0)
						traceFileElement.timeMillisGesamt = timeMillisDauer;
					else
						traceFileElement.timeMillisGesamt = timeMillisGesamtBackup;
					if (n>0) {
						traceFileElement.SGBMID = new String[n];
						n=0;
						for (j=0; j<TALLine.length; j++) {
							if ((ecuListe[i].address[k]) == TALLine[j].getDiagnosticAddress().intValue()) {
								for (m=0; m<TALLine[j].getTALTACategories().length; m++) {
									for (p=0;p<TALLine[j].getTALTACategories()[m].getTAs().length; p++) {
										temp = TALLine[j].getTALTACategories()[m].getTAs()[p].getProcessClass() + "-" +	TALLine[j].getTALTACategories()[m].getTAs()[p].getId() + "-" +
											dezString(TALLine[j].getTALTACategories()[m].getTAs()[p].getMainVersion() ,3) + "." +
											dezString(TALLine[j].getTALTACategories()[m].getTAs()[p].getSubVersion() ,3) + "." +
											dezString(TALLine[j].getTALTACategories()[m].getTAs()[p].getPatchVersion() ,3);
										traceFileElement.SGBMID[n]=temp;
										n++;
									}
								}
							}
						}
					} else {
						traceFileElement.SGBMID = new String[1];
						traceFileElement.SGBMID[0] = CfsString.get("cfs.emptytal");
					}
					if (!saveTraceFile(traceFileElement, null )) return returnFlag;
				}
			}
			returnFlag=true;
			return returnFlag;
		}

		/**
		 * Erzeugt die TAL-Line f�r Fzg-Log-Dateien <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 13.01.2014 <br>
		 */
		private String logTALLine(CascadeTALLine[] TALLine ) {
			int i = 0;
			int j = 0;
			int k = 0;
			String str = "";
			String tmp = "";
			String[] temp = null;

			if (TALLine != null) {
				if (TALLine.length > 0) {
					for (i=0; i<TALLine.length; i++) {
						str= str+ "0x"+hexString(TALLine[i].getDiagnosticAddress().longValue(),2) + " ";
						tmp = TALLine[i].getBaseVariantName();
						if (tmp == null) tmp = TALLine[i].getVariantName();
						if (tmp == null) tmp = "---";
						str = str + logString(tmp,formatLaenge-5+1)+" ";
						tmp="";
						if (TALLine[i].getTALTACategories().length > 0) {
							for( j = 0; j < TALLine[i].getTALTACategories().length; j++ ) {
								tmp = tmp + TALLine[i].getTALTACategories()[j].getCategory().toString();
								str = str+ logStringNL(tmp,0);
								if (TALLine[i].getTALTACategories()[j].getTAs().length > 0) {
									temp = new String[TALLine[i].getTALTACategories()[j].getTAs().length];
									for( k = 0; k < TALLine[i].getTALTACategories()[j].getTAs().length; k++ ) {
										temp[k]=TALLine[i].getTALTACategories()[j].getTAs()[k].getProcessClass() + "-" +
										TALLine[i].getTALTACategories()[j].getTAs()[k].getId() + "-" +
										dezString(TALLine[i].getTALTACategories()[j].getTAs()[k].getMainVersion() ,3) + "." +
										dezString(TALLine[i].getTALTACategories()[j].getTAs()[k].getSubVersion() ,3) + "." +
										dezString(TALLine[i].getTALTACategories()[j].getTAs()[k].getPatchVersion() ,3);
									}
									tmp="";
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("HWEL")) {
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("HWAP")) {
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("HWFR")) {
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									if (tmp.length() > 0) {
										str = str + logString("",formatEinruecken+1) + logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("BTLD")) {
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									if (tmp.length() > 0) {
										// Letztes "; " l�schen
										str = str + logString("",formatEinruecken+1) + logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("FLSL")) {
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									if (tmp.length() > 0) {
										// Letztes "; " l�schen
										str = str + logString("",formatEinruecken+1) + logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("SWFL")) {
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									if (tmp.length() > 0) {
										// Letztes "; " l�schen
										str = str+ logString("",formatEinruecken+1) + logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("SWFK")) {
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									if (tmp.length() > 0) {
										// Letztes "; " l�schen
										str = str+ logString("",formatEinruecken+1) + logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("IBAD")) {
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									if (tmp.length() > 0) {
										// Letztes "; " l�schen
										str = str + logString("",formatEinruecken+1) + logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("GWTB")) {
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									if (tmp.length() > 0) {
										// Letztes "; " l�schen
										str = str + logString("",formatEinruecken+1) + logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("CAFD")) {
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									if (tmp.length() > 0) {
										// Letztes "; " l�schen
										str = str+ logString("",formatEinruecken+1) + logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
									}
									// Rest
									for (k=0; k<temp.length; k++) {
										if (temp[k].length()>0) {
											tmp = tmp + temp[k] + "; ";
										}
									}
									if (tmp.length() > 0) {
										// Letztes "; " l�schen
										str = str+ logString("",formatEinruecken+1) + logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
									}
								} else {
									str = str + logString("",formatEinruecken+1) + logString(CfsString.get("cfs.talta"),formatLaenge) + ": " + logStringNL(CfsString.get("cfs.empty"),0);
								}
								tmp = logStringNL("",0) + logString("",formatEinruecken+1);
							}
						} else {
							str = str + logString(CfsString.get("cfs.taltacategory"),formatLaenge) + ": " + logStringNL(CfsString.get("cfs.empty"),0);
						}
						str = str + logStringNL("",0);
					}
				}
			} else {
				str = str + logString("",formatLaenge) + ": " + logStringNL(CfsString.get("cfs.empty"),0);
			}
			return str;
		}

		/**
		 * Erzeugt die SG-Liste f�r Fzg-Log-Dateien <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 13.01.2014 <br>
		 */
		private String logEcuListe(List<CascadeECU> ecuListe, CascadeLogistischesTeil[] bestellNrListe, HashMap<String, String> programmingDependenciesListe) {
			boolean sortError = false;
			int i = 0;
			int j = 0;
			int k = 0;
			int n = 0;
			int address = 0;
			int usedSgAddress[] = new int[256];
			String str = "";
			String tmp = "";
			String[] temp = null;
			CascadeSGBMID[] sgbmid = null;
			CascadeECU ecu = null;

			if (ecuListe != null) {
				sortError = false;
				for (i=0; i<256; i++) usedSgAddress[i]=-1;
				for (i=0; i<ecuListe.size(); i++) {
					ecu = (CascadeECU)ecuListe.get(i);
					if (ecu != null) {
						address = ecu.getDiagnosticAddress().intValue();
						if (address>=0 && address<=255) {
							usedSgAddress[address]=i;
						} else {
							sortError = true;
							break;
						}
					} else {
						sortError = true;
						break;
					}
				}
				if (sortError) {
					for (i=0; i<256; i++) usedSgAddress[i]=-1;
					for (i=0; i<ecuListe.size(); i++) usedSgAddress[i]=i;
				}

				for (i=0; i<256; i++) {
					j=usedSgAddress[i];
					if (j!=-1) {
						ecu = (CascadeECU)ecuListe.get(j);
						if (ecu != null) {
							str = str + "0x"+hexString(ecu.getDiagnosticAddress().longValue(),2) + " ";
							tmp = ecu.getBaseVariantName();
							if (tmp == null) tmp = ecu.getVariantName();
							if (tmp == null) tmp = "---";
							str = str + logString(tmp,formatLaenge-4)+" ";
							if (bestellNrListe != null) {
								if (j<bestellNrListe.length) {
									if (bestellNrListe[j] != null) {
										tmp = bestellNrListe[j].getSachNrBestellbaresTeilTAIS();
										if (tmp != null)
											str = str + logStringNL(tmp,0) + logString("",formatEinruecken+1);
									}
								}
							}
							if (programmingDependenciesListe != null) {
								tmp = (String)programmingDependenciesListe.get(hexString(ecu.getDiagnosticAddress().longValue(),0));
								if (isValidSignedNr(tmp)) {
									if (Integer.parseInt(tmp) != 1)
										str = str + logStringNL(CfsString.get("cfs.progdepchecked")+"=0x"+hexString(Integer.parseInt(tmp),2),0) + logString("",formatEinruecken+1);
								}

							}
							sgbmid = ecu.getSvk();
							if (sgbmid != null) {
								if (sgbmid.length > 0) {
									temp = new String[sgbmid.length];
									for (k=0; k<sgbmid.length; k++) {
										temp[k]=sgbmid[k].getProcessClass() + "-" +	sgbmid[k].getId() + "-" +
												dezString(sgbmid[k].getMainVersion() ,3) + "." + dezString(sgbmid[k].getSubVersion() ,3) + "." + dezString(sgbmid[k].getPatchVersion() ,3);
									}
									Arrays.sort(temp);
									tmp="";
									n=0;
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("HWEL")) {
											if (((n++)%4==0) && (tmp.length()>0)) tmp = tmp.trim() + logStringNL("",0) + logString("",formatEinruecken+1);
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("HWAP")) {
											if (((n++)%4==0) && (tmp.length()>0)) tmp = tmp.trim() + logStringNL("",0) + logString("",formatEinruecken+1);
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("HWFR")) {
											if (((n++)%4==0) && (tmp.length()>0)) tmp = tmp.trim() + logStringNL("",0) + logString("",formatEinruecken+1);
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									if (tmp.length() > 0) {
										str = str+ logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
										n=0;
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("BTLD")) {
											if (((n++)%4==0) && (tmp.length()>0)) tmp = tmp.trim() + logStringNL("",0) + logString("",formatEinruecken+1);
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									if (tmp.length() > 0) {
										// Letztes "; " l�schen
										str = str + logString("",formatEinruecken+1) + logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
										n=0;
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("FLSL")) {
											if (((n++)%4==0) && (tmp.length()>0)) tmp = tmp.trim() + logStringNL("",0) + logString("",formatEinruecken+1);
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									if (tmp.length() > 0) {
										// Letztes "; " l�schen
										str = str + logString("",formatEinruecken+1) + logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
										n=0;
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("SWFL")) {
											if (((n++)%4==0) && (tmp.length()>0)) tmp = tmp.trim() + logStringNL("",0) + logString("",formatEinruecken+1);
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									if (tmp.length() > 0) {
										// Letztes "; " l�schen
										str = str+ logString("",formatEinruecken+1) + logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
										n=0;
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("SWFK")) {
											if (((n++)%4==0) && (tmp.length()>0)) tmp = tmp.trim() + logStringNL("",0) + logString("",formatEinruecken+1);
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									if (tmp.length() > 0) {
										// Letztes "; " l�schen
										str = str+ logString("",formatEinruecken+1) + logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
										n=0;
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("IBAD")) {
											if (((n++)%4==0) && (tmp.length()>0)) tmp = tmp.trim() + logStringNL("",0) + logString("",formatEinruecken+1);
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									if (tmp.length() > 0) {
										// Letztes "; " l�schen
										str = str + logString("",formatEinruecken+1) + logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
										n=0;
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("GWTB")) {
											if (((n++)%4==0) && (tmp.length()>0)) tmp = tmp.trim() + logStringNL("",0) + logString("",formatEinruecken+1);
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									if (tmp.length() > 0) {
										// Letztes "; " l�schen
										str = str + logString("",formatEinruecken+1) + logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
										n=0;
									}
									for (k=0; k<temp.length; k++) {
										if (temp[k].toUpperCase().startsWith("CAFD")) {
											if (((n++)%4==0) && (tmp.length()>0)) tmp = tmp.trim() + logStringNL("",0) + logString("",formatEinruecken+1);
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									if (tmp.length() > 0) {
										// Letztes "; " l�schen
										str = str+ logString("",formatEinruecken+1) + logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
										n=0;
									}
									// Rest
									for (k=0; k<temp.length; k++) {
										if (temp[k].length()>0) {
											if (((n++)%4==0) && (tmp.length()>0)) tmp = tmp.trim() + logStringNL("",0) + logString("",formatEinruecken+1);
											tmp = tmp + temp[k] + "; ";
											temp[k]="";
										}
									}
									if (tmp.length() > 0) {
										// Letztes "; " l�schen
										str = str+ logString("",formatEinruecken+1) + logStringNL(tmp.substring(0,tmp.length()-2),0);
										tmp="";
										n=0;
									}
									str = str+ logStringNL("",0);
								} else {
									str = str + logString(CfsString.get("cfs.sgbmid"),0) + ": " + logStringNL(CfsString.get("cfs.empty"),0);
									str = str+ logStringNL("",0);
								}
							} else {
								str = str + logString(CfsString.get("cfs.sgbmid"),0) + ": " + logStringNL(CfsString.get("cfs.empty"),0);
								str = str+ logStringNL("",0);
							}
						} else {
							str = str + logString(CfsString.get("cfs.ecu"),0) + ": " + logStringNL(CfsString.get("cfs.empty"),0);
							str = str+ logStringNL("",0);
						}
					}
				}
			} else {
				str = str + logString("",formatLaenge) + ": " + logStringNL(CfsString.get("cfs.empty"),0);
			}
			if (str.endsWith(CRLF+CRLF)) str = str.substring(0,str.length()-2);
			return str;
		}

		/**
		 * Speichert die Log-Dateien <br>
		 * Wenn die VIN g�ltig ist wird Dateiname VIN.TRC <br>
		 * Wenn VIN.TRC bereits existiert wird Dateiname VIN_2.TRC <br>
		 * Dateiname ist Pr�fstand_Instanz.TMP <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 05.10.2015 <br>
		 */
		private boolean saveLogFile(LogFileElement logFileElement ) {
			boolean returnFlag = false;
			int i = 0;
			String dateiName = "";
			String str = "";
			String[] temp = null;
			CascadeKISWBVersion KisWBVersion = null;
			CascadeTALLine[] TALLine = null;
			SimpleDateFormat simpleDateFormat = null;
	        File file = null;
	        BufferedWriter bufferedWriter = null;
			Date date = new Date();

	        try {
				simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
				str = str + logStringNL(simpleDateFormat.format(date),0);
				str = str + logString(CfsString.get("cfs.name"),formatLaenge)           + ": " + logStringNL(rechnerName,0);
				str = str + logString(CfsString.get("cfs.instance"),formatLaenge)       + ": " + logStringNL(""+instanz,0);
				str = str + logString(CfsString.get("cfs.versioncfs"),formatLaenge)     + ": " + logStringNL(VERSION,0);
				str = str + logString(CfsString.get("cfs.versionjava"),formatLaenge)    + ": " + logStringNL(System.getProperty("java.version"),0);
				str = str + logString(CfsString.get("cfs.versioncascade"),formatLaenge) + ": " + logStringNL(CascadeProperties.getCascadeVersion(),0);
				str = str + logString(CfsString.get("cfs.versionpsdz"),formatLaenge)    + ": " + logStringNL(PSdZ.versionInfo.getPsdzVersion(),0);
				str = str + logString(CfsString.get("cfs.versionmcd"),formatLaenge)     + ": " + logStringNL(normalizeVersion(PSdZ.versionInfo.getMcdVersion()),0);
				str = str + logString(CfsString.get("cfs.versionkis"),formatLaenge)     + ": " + logStringNL(PSdZ.versionInfo.getKisAwlVersion(),0);
				for (i=0; i<PSdZ.versionInfo.getKisWBVersions().size(); i++) {
					KisWBVersion = (CascadeKISWBVersion) PSdZ.versionInfo.getKisWBVersions().get(i);
					if (KisWBVersion != null) {
						str = str + logString(CfsString.get("cfs.versionkiswb"),formatLaenge)   + ": ";
						str = str + logString(KisWBVersion.getBaureihenverbund(),5);
						str = str + logString(KisWBVersion.getHauptversion()+"."+KisWBVersion.getUnterversion()+"."+KisWBVersion.getPatchversion(),12);
						switch (KisWBVersion.getVerwendungskennzeichen()) {
						case 1:
							str = str + logString(CfsString.get("cfs.entwicklung"),0);
							break;
						case 2:
//							str = str + logString(myString.get("cfs.werk"),0);
							break;
						case 3:
							str = str + logString(CfsString.get("cfs.service"),0);
							break;
						}
						str = str.trim() + logStringNL("",0);
					}
				}
				str = str + logStringNL("",0);
				if (debugLevel != 0) {
					str = str + logString(CfsString.get("cfs.debugLevel"),formatLaenge) + ": " + logStringNL(hexString(debugLevel,4),0);
					str = str + logStringNL("",0);
				}

				str = str + logString(CfsString.get("cfs.vin"),formatLaenge)        + ": " + logStringNL(logFileElement.vehicleVin,0);
				str = str + logString(CfsString.get("cfs.ististufe"),formatLaenge)  + ": " + logStringNL(logFileElement.istIStufe,0);
				str = str + logString(CfsString.get("cfs.sollistufe"),formatLaenge) + ": " + logStringNL(logFileElement.sollIStufe,0);

				if (logFileElement.FA != null) {
					str = str + logStringNL(CfsString.get("cfs.headlinesfa"),0);
					str = str + logString(CfsString.get("cfs.baureihe"),formatLaenge)   + ": " + logStringNL(logFileElement.FA.getBaureihe(),0);
					str = str + logString(CfsString.get("cfs.typ"),formatLaenge)        + ": " + logStringNL(logFileElement.FA.getTypSchluessel(),0);
					str = str + logString(CfsString.get("cfs.baustand"),formatLaenge)   + ": " + logStringNL(logFileElement.FA.getZeitkriterium(),0);
					str = str + logString(CfsString.get("cfs.lack"),formatLaenge)       + ": " + logStringNL(logFileElement.FA.getLack(),0);
					str = str + logString(CfsString.get("cfs.polster"),formatLaenge)    + ": " + logStringNL(logFileElement.FA.getPolster(),0);
					temp = logFileElement.FA.getSaListe();
					if (temp!=null && temp.length > 0) {
						str = str + logString(CfsString.get("cfs.sanumber"),formatLaenge)   + ": " + logStringNL(""+temp.length,0);
						for (i=1; i<=temp.length; i++) {
							str = str + " " + logString(temp[i-1],4);
							if ((i%10==0) || (i==temp.length)) str = str + logStringNL("",0);
						}
					} else {
						str = str + logString(CfsString.get("cfs.sanumber"),formatLaenge)   + ": " + logStringNL("0",0);
					}
					temp = logFileElement.FA.getEwListe();
					if (temp!=null && temp.length > 0) {
						str = str + logString(CfsString.get("cfs.ewnumber"),formatLaenge)   + ": " + logStringNL(""+temp.length,0);
						for (i=1; i<=temp.length; i++) {
							str = str + " " + logString(temp[i-1],4);
							if ((i%10==0) || (i==temp.length)) str = str + logStringNL("",0);
						}
					} else {
						str = str + logString(CfsString.get("cfs.ewnumber"),formatLaenge)   + ": " + logStringNL("0",0);
						for (i=1; i<=temp.length; i++) {
							str = str + " " + logString(temp[i-1],4);
							if ((i%10==0) || (i==temp.length)) str = str + logStringNL("",0);
						}
					}
					temp = logFileElement.FA.getHoListe();
					if (temp!=null && temp.length > 0) {
						str = str + logString(CfsString.get("cfs.honumber"),formatLaenge)   + ": " + logStringNL(""+temp.length,0);

					} else {
						str = str + logString(CfsString.get("cfs.honumber"),formatLaenge)   + ": " + logStringNL("0",0);
					}
				} else {
					str = str + logString(CfsString.get("cfs.fa"),formatLaenge)         + ": " + logStringNL(CfsString.get("cfs.unknown"),0);
				}

				str = str + logStringNL("",0);
				str = str + logStringNL(CfsString.get("cfs.headlinesgverbaulist"),0);
				if (logFileElement.sgVerbauListe != null) {
					str = str + logString(CfsString.get("cfs.number"),formatLaenge)   + ": " + logStringNL(""+logFileElement.sgVerbauListe.length,0);
					for (i=1; i<=logFileElement.sgVerbauListe.length; i++) {
						str = str + " 0x" + logString(logFileElement.sgVerbauListe[i-1],2);
						if ((i%10==0) || (i==logFileElement.sgVerbauListe.length)) str = str + logStringNL("",0);
					}
				} else {
					str = str + logString("",formatLaenge) + ": " + logStringNL(CfsString.get("cfs.empty"),0);
				}

				str = str + logStringNL("",0);
				str = str + logStringNL(CfsString.get("cfs.headlinesgignorlist"),0);
				if (logFileElement.sgIgnorListe != null) {
					str = str + logString(CfsString.get("cfs.number"),formatLaenge)   + ": " + logStringNL(""+logFileElement.sgIgnorListe.length,0);
					for (i=1; i<=logFileElement.sgIgnorListe.length; i++) {
						str = str + " 0x" + logString(logFileElement.sgIgnorListe[i-1],2);
						if ((i%10==0) || (i==logFileElement.sgIgnorListe.length)) str = str + logStringNL("",0);
					}
				} else {
					str = str + logString("",formatLaenge) + ": " + logStringNL(CfsString.get("cfs.empty"),0);
				}

				str = str + logStringNL("",0);
				str = str + logStringNL(CfsString.get("cfs.headlinesgflashlist"),0);
				if (logFileElement.sgFlashListe != null) {
					str = str + logString(CfsString.get("cfs.number"),formatLaenge)   + ": " + logStringNL(""+logFileElement.sgFlashListe.length,0);
					for (i=1; i<=logFileElement.sgFlashListe.length; i++) {
						str = str + " 0x" + logString(logFileElement.sgFlashListe[i-1],2);
						if ((i%10==0) || (i==logFileElement.sgFlashListe.length)) str = str + logStringNL("",0);
					}
				} else {
					str = str + logString("",formatLaenge) + ": " + logStringNL(CfsString.get("cfs.empty"),0);
				}

				str = str + logStringNL("",0);
				str = str + logStringNL(CfsString.get("cfs.headlinesgnoflashlist"),0);
				if (logFileElement.sgNoFlashListe != null) {
					str = str + logString(CfsString.get("cfs.number"),formatLaenge)   + ": " + logStringNL(""+logFileElement.sgNoFlashListe.length,0);
					for (i=1; i<=logFileElement.sgNoFlashListe.length; i++) {
						str = str + " 0x" + logString(logFileElement.sgNoFlashListe[i-1],2);
						if ((i%10==0) || (i==logFileElement.sgNoFlashListe.length)) str = str + logStringNL("",0);
					}
				} else {
					str = str + logString("",formatLaenge) + ": " + logStringNL(CfsString.get("cfs.empty"),0);
				}

				str = str + logStringNL("",0);
				str = str + logStringNL(CfsString.get("cfs.headlinesvtist"),0);
				if (logFileElement.SVTIst != null) {
					str = str + logString(CfsString.get("cfs.number"),formatLaenge)   + ": " + logStringNL(""+logFileElement.SVTIst.getEcuList().size(),0);
					str = str + logString(logEcuListe(logFileElement.SVTIst.getEcuList(), null, logFileElement.programmingDependenciesListe),0);
				} else {
					str = str + logString("",formatLaenge) + ": " + logStringNL(CfsString.get("cfs.empty"),0);
				}

				str = str + logStringNL("",0);
				str = str + logStringNL(CfsString.get("cfs.headlinesvtsoll"),0);
				if (logFileElement.SVTSoll != null) {
					str = str + logString(CfsString.get("cfs.number"),formatLaenge)   + ": " + logStringNL(""+logFileElement.SVTSoll.getEcuList().size(),0);
					str = str + logString(logEcuListe(logFileElement.SVTSoll.getEcuList(), logFileElement.bestellNrListe, null),0);
				} else {
					str = str + logString("",formatLaenge) + ": " + logStringNL(CfsString.get("cfs.empty"),0);
				}

				str = str + logStringNL("",0);
				str = str + logStringNL(CfsString.get("cfs.headlinetal"),0);
				if (logFileElement.TAL != null) {
					TALLine = logFileElement.TAL.getTalLines();
					if (TALLine != null) {
						str = str + logString(CfsString.get("cfs.number"),formatLaenge)   + ": " + logStringNL(""+TALLine.length,0);
						str = str + logString(logTALLine(TALLine),0);
					} else {
						str = str + logString("",formatLaenge) + ": " + logStringNL(CfsString.get("cfs.empty"),0);
					}
				} else {
					str = str + logString("",formatLaenge) + ": " + logStringNL(CfsString.get("cfs.empty"),0);
				}
				str = str + logStringNL(CfsString.get("cfs.eof"),0);

				// Dateiname
				dateiName = CascadeProperties.getCascadeHome() + TRACEPATH;
				dateiName = dateiName + rechnerName + "_" + instanz + ".TMP";
				file = new File( dateiName );
		        bufferedWriter = new BufferedWriter( new FileWriter( file, false));
		        bufferedWriter.write(str);
		        bufferedWriter.close();
		        logFileElement.fileName = dateiName;

		        if (isValidVIN(logFileElement.vehicleVin)) {
					simpleDateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss_SSS_");
					dateiName = CascadeProperties.getCascadeHome() + TRACEPATH;
					dateiName = dateiName + simpleDateFormat.format(date) + logFileElement.vehicleVin + ".TRC";
					file = new File( dateiName );
			        bufferedWriter = new BufferedWriter( new FileWriter( file, false));
			        bufferedWriter.write(str);
			        bufferedWriter.close();
			        logFileElement.fileNameVin = dateiName;
		        }
		        returnFlag = true;
			} catch (Exception e) {
				lastError=dateiName + NL + CfsString.get("cfs.writeerror") + NL	+ e.getMessage();
				lastHelp=CfsString.get("cfs.nohelp");
			}
			return returnFlag;
		}

		/**
		 * H�ngt an die Log-Dateien die SVT-Ist neu an <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 11.02.2010 <br>
		 */
		private boolean appendLogFile(LogFileElement logFileElement ) {
			boolean returnFlag = false;
			String dateiName = "";
			String str = "";
			String tmp = "";
	        File file = null;
	        BufferedReader bufferedReader = null;
	        BufferedWriter bufferedWriter = null;

	        try {
				// Log einlesen und eof entfernen
	        	dateiName = logFileElement.fileName;
				file = new File( dateiName );
				bufferedReader = new BufferedReader( new FileReader(file));
				while ((tmp = bufferedReader.readLine()) != null) {
			       	if (!tmp.equalsIgnoreCase(CfsString.get("cfs.eof")))	str = str + logStringNL(tmp,0);
				}
				bufferedReader.close();

	        	// SVT-Ist neu anh�ngen
				str = str + logStringNL("",0);
				str = str + logStringNL(CfsString.get("cfs.headlinesvtistneu"),0);
				if (logFileElement.SVTIstNeu != null) {
					str = str + logString(CfsString.get("cfs.number"),formatLaenge)   + ": " + logStringNL(""+logFileElement.SVTIstNeu.getEcuList().size(),0);
					str = str + logString(logEcuListe(logFileElement.SVTIstNeu.getEcuList(), null, logFileElement.programmingDependenciesListeNeu),0);
				} else {
					str = str + logString("",formatLaenge) + ": " + logStringNL(CfsString.get("cfs.empty"),0);
				}
				str = str + logStringNL(CfsString.get("cfs.eof"),0);

				file = new File( dateiName );
		        bufferedWriter = new BufferedWriter( new FileWriter( file, false));
		        bufferedWriter.write(str);
		        bufferedWriter.close();

		        dateiName = logFileElement.fileNameVin;
				if (dateiName != null) {
					file = new File( dateiName );
			        bufferedWriter = new BufferedWriter( new FileWriter( file, false));
			        bufferedWriter.write(str);
			        bufferedWriter.close();
				}
		        returnFlag = true;
			} catch (Exception e) {
				if (dateiName == null) dateiName="null";
				lastError=dateiName + NL + CfsString.get("cfs.writeerror") + NL	+ e.getMessage();
				lastHelp=CfsString.get("cfs.nohelp");
			}
			return returnFlag;
		}

		/**
		 * Speichert eine Trace-Zeile in die Trace-Datei <br>
		 * Dateiname ist Pr�fstand_Instanz_Jahr_Woche.TRC <br>
		 *
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 13.01.2014 <br>
		 */
		private boolean saveTraceFile(TraceFileElement traceFileElement, Vector<Ergebnis> ergListe ) {
			boolean returnFlag = false;
			String dateiName = "";
			String str = "";
			String ergName = "";
			String ergWert = "";
			String ergHinweis = "";
			String ergType = Ergebnis.FT_IO;
			SimpleDateFormat simpleDateFormat = null;
	        File file = null;
	        BufferedWriter bufferedWriter = null;
			Date date = new Date();

	        try {
				// Dateiname
				simpleDateFormat = new SimpleDateFormat("yy_ww");
				dateiName = CascadeProperties.getCascadeHome() + TRACEPATH;
				dateiName = dateiName + rechnerName + "_" + instanz + "_";
				dateiName = dateiName + simpleDateFormat.format(date) + ".TRC";

				simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy; HH:mm:ss; ww; ");
				str = simpleDateFormat.format(date);
				if ((traceFileElement.timeMillisGesamt < 0) || (traceFileElement.timeMillisGesamt >= 86400000 )) {
					str = str + traceString("??:??:??",0);
				} else {
					long flashTime = traceFileElement.timeMillisGesamt / 1000; // in Sekunden
					long h = flashTime / 3600;
					long m = flashTime /   60 - h*  60;
					long s = flashTime        - h*3600 -m*60;
					str = str + traceString(dezString(h,2)+":"+dezString(m,2)+":"+dezString(s,2),0);
				}
				if (traceFileElement.abbruchFlag) {
					str = str + traceString(CfsString.get("cfs.cancel"),6);
					ergType = Ergebnis.FT_IGNORE;
				} else {
					if (traceFileElement.okayFlag) {
						str = str + traceString("  i.O.",6);
						ergType = Ergebnis.FT_IO;
					} else {
						str = str + traceString("n.i.O.",6);
						ergType = Ergebnis.FT_NIO;
					}
				}

				str = str + traceString(traceFileElement.vin,17);
				str = str + traceString(traceFileElement.iStufe,14);
				ergHinweis = traceFileElement.iStufe;
				if ((traceFileElement.variantName != null) &&  (traceFileElement.variantName.length() > 0)) {
					str = str + traceString(traceFileElement.variantName,15);
					ergName = ergName + "ECU:" + traceFileElement.variantName;
				} else {
					if ((traceFileElement.baseVariantName != null) &&  (traceFileElement.baseVariantName.length() > 0)) {
						str = str + traceString(traceFileElement.baseVariantName,15);
						ergName = ergName + "ECU:" + traceFileElement.baseVariantName;
					} else {
						str = str + traceString(traceFileElement.name,15);
						ergName = ergName + "ECU:" + traceFileElement.name;
					}
				}
				if ((traceFileElement.address < 0) || (traceFileElement.address > 255 )) {
					if (traceFileElement.address == -1) {
						str = str + traceString("  ",0);
						ergName = "     " + ergName;
					} else {
						str = str + traceString("??",0);
						ergName = "0x?? " + ergName;
					}
				} else {
					str = str + traceString(hexString(traceFileElement.address,2),2);
					ergName = "0x" + hexString(traceFileElement.address,2) + " " + ergName;
				}
				str = str + traceString(traceFileElement.lieferUmfangNummer,7);

				if (traceFileElement.SGBMID != null ) {
					int i;
					String temp = "";
					for (i=0; i<traceFileElement.SGBMID.length; i++) {
						if (traceFileElement.SGBMID[i]==null) traceFileElement.SGBMID[i]="";
					}
					for (i=0; i<traceFileElement.SGBMID.length; i++) {
						if (traceFileElement.SGBMID[i].toUpperCase().startsWith("HWEL")) {
							temp = temp + traceString(traceFileElement.SGBMID[i],25);
							traceFileElement.SGBMID[i]="";
						}
					}
					for (i=0; i<traceFileElement.SGBMID.length; i++) {
						if (traceFileElement.SGBMID[i].toUpperCase().startsWith("HWAP")) {
							temp = temp + traceString(traceFileElement.SGBMID[i],25);
							traceFileElement.SGBMID[i]="";
						}
					}
					for (i=0; i<traceFileElement.SGBMID.length; i++) {
						if (traceFileElement.SGBMID[i].toUpperCase().startsWith("HWFR")) {
							temp = temp + traceString(traceFileElement.SGBMID[i],25);
							traceFileElement.SGBMID[i]="";
						}
					}
					for (i=0; i<traceFileElement.SGBMID.length; i++) {
						if (traceFileElement.SGBMID[i].toUpperCase().startsWith("BTLD")) {
							temp = temp + traceString(traceFileElement.SGBMID[i],25);
							traceFileElement.SGBMID[i]="";
						}
					}
					for (i=0; i<traceFileElement.SGBMID.length; i++) {
						if (traceFileElement.SGBMID[i].toUpperCase().startsWith("FLSL")) {
							temp = temp + traceString(traceFileElement.SGBMID[i],25);
							traceFileElement.SGBMID[i]="";
						}
					}
					for (i=0; i<traceFileElement.SGBMID.length; i++) {
						if (traceFileElement.SGBMID[i].toUpperCase().startsWith("SWFL")) {
							temp = temp + traceString(traceFileElement.SGBMID[i],25);
							traceFileElement.SGBMID[i]="";
						}
					}
					for (i=0; i<traceFileElement.SGBMID.length; i++) {
						if (traceFileElement.SGBMID[i].toUpperCase().startsWith("SWFK")) {
							temp = temp + traceString(traceFileElement.SGBMID[i],25);
							traceFileElement.SGBMID[i]="";
						}
					}
					for (i=0; i<traceFileElement.SGBMID.length; i++) {
						if (traceFileElement.SGBMID[i].toUpperCase().startsWith("IBAD")) {
							temp = temp + traceString(traceFileElement.SGBMID[i],25);
							traceFileElement.SGBMID[i]="";
						}
					}
					for (i=0; i<traceFileElement.SGBMID.length; i++) {
						if (traceFileElement.SGBMID[i].toUpperCase().startsWith("GWTB")) {
							temp = temp + traceString(traceFileElement.SGBMID[i],25);
							traceFileElement.SGBMID[i]="";
						}
					}
					for (i=0; i<traceFileElement.SGBMID.length; i++) {
						if (traceFileElement.SGBMID[i].toUpperCase().startsWith("CAFD")) {
							temp = temp + traceString(traceFileElement.SGBMID[i],25);
							traceFileElement.SGBMID[i]="";
						}
					}
					// Rest
					for (i=0; i<traceFileElement.SGBMID.length; i++) {
						if (traceFileElement.SGBMID[i].length()>0) {
							if (traceFileElement.SGBMID.length==1 &&
								traceFileElement.SGBMID[0].equalsIgnoreCase(CfsString.get("cfs.emptytal")) &&
								traceFileElement.zusatzMessage != null &&
								traceFileElement.zusatzMessage.length() > 0)	{
								temp = temp + traceString(traceFileElement.SGBMID[i] + " (" + traceFileElement.zusatzMessage +")",0);
							} else {
								temp = temp + traceString(traceFileElement.SGBMID[i],25);
							}
						}
					}
					str = str + temp;
					ergWert = ergWert + temp;
				}
				if (traceFileElement.sgbmIdAnlieferzustand != null) {
					str = str + traceString(CfsString.get("cfs.anlieferprogrammierungohne")+" "+traceFileElement.sgbmIdAnlieferzustand,0);
				}

				// Letztes "; " durch "." ersetzen
				str = str.trim();
				if (str.charAt(str.length()-1) == ';')
					str = str.substring(0,str.length()-1);
				str = str.trim()+".";

				ergWert = ergWert.trim();
				if (ergWert.charAt(ergWert.length()-1) == ';')
					ergWert = ergWert.substring(0,ergWert.length()-1);
				ergWert = ergWert.trim()+".";

				// Detailinfos f�r APDM
				if (!getTrailerModeFlag() && getApdmDetailFlag() && ergListe!=null && !ergType.equalsIgnoreCase(Ergebnis.FT_IGNORE) ) {
					Ergebnis result = new Ergebnis( "PSdZCfs", "Result", "", "", "", ergName, ergWert, "", "", "0", "", "", "", "", ergHinweis, ergType );
					ergListe.add(result );
				}

				// File schreiben
				file = new File( dateiName );
		        bufferedWriter = new BufferedWriter( new FileWriter( file, true ));
		        bufferedWriter.write(str);
		        bufferedWriter.newLine();
		        bufferedWriter.close();

		        returnFlag = true;
			} catch (Exception e) {
				lastError=dateiName + NL + CfsString.get("cfs.writeerror") + NL	+ e.getMessage();
				lastHelp=CfsString.get("cfs.nohelp");
			}
			return returnFlag;
		}

		/**
		 * Setter  f�r die PSdz ProgressMap <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 12.12.2007 <br>
		 */
		private void setProgressMap(HashMap<?, ?> value) {
			if (value == null)
				progressMap = new HashMap<Object, Object>();
			else
				progressMap = value;
			return;
		}

		/**
		 * Liest die allgemeine Progress Message <br>
		 * und �bersetzt diese, wenn m�glich <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 03.03.2011 <br>
		 */
		private String getProgressMessage() {
			String temp="";
			String temp2="";

			if (progressMap == null) return "";
			temp = (String)progressMap.get("MESSAGE");
			if (temp == null) return "";
			temp = temp.trim();
			if (temp.length() == 0) return "";
			temp2 = CfsString.get("cfs."+temp.replace(' ', '_'));
			if (!temp2.startsWith("cfs.")) {
				temp = temp2;
			} else {
				// keine Ausgabe von MCDDiagService Fehlern
				if (temp.startsWith("MCDDiagService<id=")) {
					temp="";
				}
				else {
//					System.out.println("!!! CFS-Warnung: package(_de).progerties: nicht gefunden:'"+temp2+"'");
				}
			}
			return temp;
		}

		/**
		 * Liest den Progress eines Steuerger�tes <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 13.01.2014 <br>
		 */
		private String getProgressBar(String address, CascadeTACategories TACategories) {
			String temp="";
			String t1="xxxxxxxxxx".replace('x',PROGRESSMARKER);
			String t2="__________";
			int p;

			if (progressMap == null) return "";
			if (!isValidAdresse(address)) return "";
			temp = (String)progressMap.get(address);
			if (temp == null) return "";

			if (TACategories == CascadeTACategories.BL_FLASH) {
				if ((temp.toUpperCase().indexOf("BTLD_") == -1) &&
					(temp.toUpperCase().indexOf("FLSL_") == -1) &&
					(temp.toUpperCase().indexOf("FLUP_") == -1) &&
					(temp.toUpperCase().indexOf("BLUP_") == -1)) return "";
			} else {
				if (TACategories == CascadeTACategories.SW_DEPLOY) {
					if ((temp.toUpperCase().indexOf("SWFK_") == -1) &&
						(temp.toUpperCase().indexOf("SWFL_") == -1)) return "";
				} else {
					if (TACategories == CascadeTACategories.IBA_DEPLOY) {
						if (temp.toUpperCase().indexOf("IBAD_") == -1) return "";
					} else {
						if (TACategories == CascadeTACategories.GWTB_DEPLOY) {
							if (temp.toUpperCase().indexOf("GWTB_") == -1) return "";
						}
					}
				}
			}
			p = temp.indexOf("%"); // 0% bis 100%
			if (p<1) return "";
			if (p>3) return "";
			temp = temp.substring(0, p);
			p = (Integer.valueOf(temp)).intValue();
			if (p<0) return "";
			if (p>100) return "";
			if ( p < 10 ) temp = " " + temp;
			if ( p < 100 ) temp = " " + temp;
			temp =	" " + t1.substring(0, (p+9) / 10) +  t2.substring((p+9) / 10) + " " + temp + "%";
			return temp;
		}

		/**
		 * Kopiert einen Text in die Zwischenablage <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 17.07.2007 <br>
		 */
		private boolean copyToClipboard(String message) {
			if (message == null) return false;
	        try {
				Clipboard systemClipboard =	Toolkit.getDefaultToolkit().getSystemClipboard();
				Transferable transferableText =	new StringSelection(message.replace(NL.charAt(0),LF.charAt(0)).replace(PROGRESSMARKER,'x'));
				systemClipboard.setContents(transferableText,null);
			} catch (Exception e) {
				return false;
			} catch( Throwable e ) {
				return false;
			}
			return true;
		}

		/**
		 * Initialisiert den userDialog f�r formatierte Ausgabe <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 27.06.2008 <br>
		 */
		private boolean initDialog(UserDialog userDialog, boolean backColorFlag, boolean displayProgressFlag) {
			if (userDialog == null) return false;
			if (!displayProgressFlag) {
				userDialog.setAllowCancel( true );
			}
			if (backColorFlag) {
				userDialog.setBackgroundColor(backColor);
			}
			userDialog.setMonospacedMessage(true);
			userDialog.setLineWrap(false);
			userDialog.setMessageColumns(43);
			if (displayProgressFlag) {
				userDialog.setMessageBoxRows(6);
				userDialog.setDisplayProgress( true );
				userDialog.getProgressbar().setIndeterminate( true );
			} else {
				userDialog.setMessageBoxRows(7);
				userDialog.setDisplayProgress( false );
			}
			userDialog.pack();
			return true;
		}

		/**
		 * Startet die StoppUhr <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 20.06.2008 <br>
		 */
		private void startStopWatch() {
			startStopWatchTimeMillis = System.currentTimeMillis();
			deltaStopWatchTimeMillis = 0;
			return ;
		}

		/**
		 * Stoppt die StoppUhr <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 20.06.2008 <br>
		 */
		private void stopStopWatch() {
			deltaStopWatchTimeMillis = System.currentTimeMillis() - startStopWatchTimeMillis;
			return ;
		}

		/**
		 * L�sst die StoppUhr weiter laufen <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 20.06.2008 <br>
		 */
		private void continueStopWatch() {
			startStopWatchTimeMillis = System.currentTimeMillis() - deltaStopWatchTimeMillis;
			deltaStopWatchTimeMillis = 0;
			return ;
		}

		/**
		 * Liefert den StoppUhr String <br>
		 * @author BMW AG TI-545 Drexel <br>
		 * @version 20.06.2008 <br>
		 */
		private String getStopWatch() {
			long t = (System.currentTimeMillis()- startStopWatchTimeMillis) / 1000; // in Sekunden
			if ((t < 0) || (t >= 86400 )) return "";
			long h = t / 3600;
			long m = t /   60 - h*  60;
			long s = t        - h*3600 -m*60;
			return " "+dezString(h,2)+":"+dezString(m,2)+":"+dezString(s,2);
		}
	}

	/* ***************************** start of copied code ********************************************************************* */

	/**
	 * �bernahme aus PSdZ.java <br>
	 * @author BMW AG TI-545 Gampl, Buboltz <br>
	 * @version 06.11.2015 <br>
	 * <br>
	 * modifiziert <br>
	 * @param String vehicleVin hinzu <br>
	 * dateFormatPSdZXMLFilesOnDisk <br>
	 * PB.getString <br>
	 * <br>
	 * @author BMW AG TI-545 Drexel <br>
	 * @version 09.11.2015 <br>
	 */
	/**
	 * F�r das Loggen von PSdZ Files SVT's SVT_ist, SVT_soll + TAL
	 *
	 * @param strLog_Type SVTSoll, SVTIst, TAL
	 * @param strContent as XML, der entsprechende LogFile Content in XML
	 * @throws IOException bei Fehler
	 */
	private void logPSdZContent( String vehicleVin, String strLog_Type, String strContent ) throws IOException {
		SimpleDateFormat dateFormatPSdZXMLFilesOnDisk = new SimpleDateFormat( "yyyyMMdd'_'HHmmss'_'SSS" );

		// Fileobjekt generieren
		File f = new File( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.logDirectory" ) + System.getProperty( "file.separator" ) + dateFormatPSdZXMLFilesOnDisk.format( new Date( System.currentTimeMillis() ) ) + "_" + vehicleVin + "_" + strLog_Type + ".XML" );

		// DIR eventuell erzeugen inklusive SubDIRs
		if( !f.getParentFile().exists() )
			f.getParentFile().mkdirs();

		// JAVA ist schnell, pr�fe deshalb ob es das File eventuell schon gibt und warte dann 10 ms, um ein seperates File zu erzeugen
		while( f.exists() ) {
			try {
				Thread.sleep( 10 );
			} catch( InterruptedException e ) {
			}

			f = new File( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.logDirectory" ) + System.getProperty( "file.separator" ) + dateFormatPSdZXMLFilesOnDisk.format( new Date( System.currentTimeMillis() ) ) + "_" + vehicleVin + "_" + strLog_Type + ".XML" );
		}

		// Daten rausschreiben
		if( strContent != null && (!f.exists() || (f.exists() && f.canWrite())) ) {
			BufferedWriter out = new BufferedWriter( new FileWriter( f ) );
			out.write( strContent );
			out.close();
		}
	}

	/**
	 * �bernahme aus PSdZExecuteTal_36_0_F_Pruefprozedur <br>
	 * @author BMW AG TI-545 Gampl, Buboltz <br>
	 * @version 27.08.2015 <br>
	 * <br>
	 * modifiziert <br>
	 * @param psdz PSdz hinzu <br>
	 * PSdZExecuteTAL nach PSdZCfs ge�ndert <br>
	 * @author BMW AG TI-545 Drexel <br>
	 * @version 09.11.2015 <br>
	 */
	/**
	 * Tr�gt ECUs in die Ergebnisliste (APDM) ein
	 * 
	 * @param mErgListe Ergebnisliste
	 * @param mMap Map mit einzutragenden ECU's
	 * @param mTransact einzutragende Transaktion
	 * @param mResult einzutragendes Ergebnis
	 * @param mPsdzErrors zugeh�rige einzutragende Fehler
	 * @param mECUStatistics Map mit SG Antwortverhalten
	 * @throws CascadePSdZRuntimeException
	 */
	private void entryECUsDetails( Vector<Ergebnis> mErgListe, TreeMap<String, HashSet<String>> mMap, String mTransact, String mSResult, HashMap<String, List<Exception>> mPsdzErrors, HashMap<String, String> mECUStatistics , PSdZ psdz) throws CascadePSdZRuntimeException, CascadePSdZConditionsNotCorrectException {
		Ergebnis mResult;
		String key, fehlertext, antwortVerhalten;
		CascadePSdZRuntimeException cscRE;
		Exception ex;
		String action = DE ? "Aktion: " : "Action: ";
		String errorDescription = DE ? "Fehlerbeschreibung: " : "Error description: ";
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";
		String titleBestellTeilTAIS = DE ? "BestellNr: " : "OrderPartNo: "; // TAIS Nr. Bestellteil

		// Schleife �ber alle Eintr�ge der �bergebenen Map
		for( Iterator<Entry<String, HashSet<String>>> e = mMap.entrySet().iterator(); e.hasNext(); ) {
			Map.Entry<String, HashSet<String>> entry = e.next(); // z.B. SG:JBBF 0x00=[cdDeploy]
			cscRE = null;
			ex = null;

			String tempKey = (String) entry.getKey();

			// etwas aufw�ndigerer Code (anstatt einfacher Suche nach ":"), stellt aber sicher, dass Basisvariante sicher ermittelt wird, auch dann wenn "psdz.SG" zuk�nftig ":" enthalten sollte(!)
			String sgBaseVariantName = tempKey.substring( tempKey.indexOf( PB.getString( "psdz.SG" ) ) + PB.getString( "psdz.SG" ).length() + 1, tempKey.indexOf( "0x" ) - 1 ); // z.B. "JBBF"
			String sgDiagAddress = tempKey.substring( tempKey.indexOf( "0x" ) ); // z.B. "0x00"

			HashSet<String> myContent = new HashSet<String>();
			myContent = ((HashSet<String>) entry.getValue()); // z.B. [cdDeploy]

			Iterator<String> i = myContent.iterator();
			StringBuffer text = new StringBuffer();
			boolean hasNext = i.hasNext();
			Object o = null;

			while( hasNext ) {
				o = i.next();
				text.append( PB.getString( "psdz.tal." + String.valueOf( o ).toLowerCase().trim() ) );
				hasNext = i.hasNext();
				if( hasNext )
					text.append( ", " );
			}

			if( !mSResult.equalsIgnoreCase( Ergebnis.FT_IO ) ) {
				for( Iterator<String> j = myContent.iterator(); j.hasNext(); ) {

					String sgCat = ((String) j.next()).toLowerCase().trim(); // z.B. "cddeploy"

					for( int excep = 0; excep < (mPsdzErrors.get( sgDiagAddress + "_" + sgCat )).size(); excep++ ) {
						ex = (mPsdzErrors.get( sgDiagAddress + "_" + sgCat.toLowerCase().trim() )).get( excep );
						if( ex instanceof CascadePSdZRuntimeException ) {
							cscRE = (CascadePSdZRuntimeException) ex;
						}

						// den anderen Key (-> Diagnose-Adresse ohne 0x und ohne f�hrende Nullen) f�r die ECUStatistics holen (etwas aufwendig...)
						key = sgDiagAddress.charAt( 2 ) == '0' ? sgDiagAddress.substring( 3, 4 ) : sgDiagAddress.substring( 2, 4 );

						// der Fehlertext
						if( cscRE != null ) {
							fehlertext = action + cscRE.getStrErrorContextTranslated() + ", " + errorDescription + cscRE.getMessage() + ", (PSdZ ID: " + cscRE.getMessageId() + ")";
						} else {
							fehlertext = PB.getString( mTransact );
						}

						// SG Antwortverhalten nur eintragen, wenn SG nie geantwortet hat oder sporadisch
						if( mECUStatistics.get( key ).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.0" ) ) || ((String) mECUStatistics.get( key )).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.1" ) ) )
							antwortVerhalten = " (" + PB.getString( "psdz.SG" ) + " " + PB.getString( "psdz.antwortet" ) + " " + mECUStatistics.get( key ) + ")";
						else
							antwortVerhalten = "";

						// LOP 1436: Ausgabe um TAIS Teilenummer anreichern und zus�tzlich, wenn m�glich noch die TAIS Nummer des bestellbaren Teils mit ausgeben. Unterdr�cke die Bestellnummer, wenn diese gleich der Nummer des logTeils ist

						String logTeil = "";
						String bestellTeilTAIS = "";
						StringBuffer strBufNummernTAIS = new StringBuffer();

						if( psdz.getLogistischesTeil( key ) != null ) {
							logTeil = psdz.getLogistischesTeil( key ).getSachNrTAIS();
							strBufNummernTAIS.append( ", " + PB.getString( "psdz.text.TaisNummer" ) + logTeil );

							bestellTeilTAIS = psdz.getLogistischesTeil( key ).getSachNrBestellbaresTeilTAIS();
							strBufNummernTAIS.append( logTeil.equals( bestellTeilTAIS ) ? "" : " (" + titleBestellTeilTAIS + bestellTeilTAIS + ")" );
						}

						mResult = new Ergebnis( "PSdZCfs", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress + strBufNummernTAIS.toString(), text.toString(), "", "STATUS", statusNotOkay, "", "", "0", "", "", "", fehlertext + antwortVerhalten, "", mSResult );
						mErgListe.add( mResult );
					}
				}
			} else {
				mResult = new Ergebnis( "PSdZCfs", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, text.toString(), "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", mSResult );
				mErgListe.add( mResult );
			}
		}
	}

	/**
	 * �bernahme aus PSdZExecuteTal_36_0_F_Pruefprozedur <br>
	 * @author BMW AG TI-545 Gampl, Buboltz <br>
	 * @version 27.08.2015 <br>
	 * <br>
	 * modifiziert <br>
	 * @param psdz PSdz hinzu <br>
	 * PSdZExecuteTAL nach PSdZCfs ge�ndert <br>
	 * @author BMW AG TI-545 Drexel <br>
	 * @version 09.11.2015 <br>
	 */
	/**
	 * Eintrag des ersten allgemeinen Fehlers, der NIO-ECUs und der NotExecutable-ECUs ins APDM
	 * 
	 * @param gErgListe Ergebnisliste
	 * @param gMapNIO Map mit NIO-ECU's
	 * @param gMapNotExec Map mit NotExecutable-ECU's
	 * @param gPsdzErrors zugeh�rige einzutragende Fehler
	 * @param gECUStatistics Map mit SG Antwortverhalten
	 * @throws CascadePSdZRuntimeException
	 */
	private void entryECUsFT_NIO( Vector<Ergebnis> gErgListe, TreeMap<String, HashSet<String>> gMapNIO, TreeMap<String, HashSet<String>> gMapNotExec, HashMap<String, List<Exception>> gPsdzErrors, HashMap<String, String> gECUStatistics, PSdZ psdz ) throws CascadePSdZRuntimeException, CascadePSdZConditionsNotCorrectException {
		Ergebnis gResult;
		String action = DE ? "Aktion: " : "Action: ";
		String errorDescription = DE ? "Fehlerbeschreibung: " : "Error description: ";
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";

		// Haben wir irgendeinen allgemeinen Fehler ohne SG Adresse?
		if( gPsdzErrors.containsKey( "-1" ) ) {
			CascadePSdZRuntimeException cscRE = null;

			// Liste der Fehler durchgehen
			for( int e = 0; e < (gPsdzErrors.get( "-1" )).size(); e++ ) {
				Exception ex = (gPsdzErrors.get( "-1" )).get( e );

				if( ex instanceof CascadePSdZRuntimeException ) {
					cscRE = (CascadePSdZRuntimeException) ex;
				}

				gResult = new Ergebnis( "PSdZCfs", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", cscRE != null ? action + cscRE.getStrErrorContextTranslated() + ", " + errorDescription + cscRE.getMessage() + ", (PSdZ ID: " + cscRE.getMessageId() + ")" : ex.getMessage(), "", Ergebnis.FT_NIO );
				gErgListe.add( gResult );
			}
		}

		// NIO ECUs in Ergebnisliste (APDM) eintragen
		entryECUsDetails( gErgListe, gMapNIO, "psdz.tal.transactionNIO", Ergebnis.FT_NIO, gPsdzErrors, gECUStatistics, psdz );

		// NIO ECUs NotExecutable in Ergebnisliste (APDM) eintragen
		entryECUsDetails( gErgListe, gMapNotExec, "psdz.tal.transactionNOTExecutable", Ergebnis.FT_NIO, gPsdzErrors, gECUStatistics, psdz );
	}

	/**
	 * �bernahme aus PSdZExecuteTal_36_0_F_Pruefprozedur <br>
	 * @author BMW AG TI-545 Gampl, Buboltz <br>
	 * @version 27.08.2015 <br>
	 * <br>
	 * unver�ndert �bernommen <br>
	 * @author BMW AG TI-545 Drexel <br>
	 * @version 15.12.2014 <br>
	 */
	/**
	 * Aufbau einer HashMap aus den Exceptions der TAL (Aufbau: key = Steuerger�teadresse in hex + _ + TACategorie z.B. '0x00_cddeploy', wert = 'Exception'
	 * 
	 * @param mTALResult zu analysierende CascadeTAL
	 * @return HashMap aus input-CascadeTAL erzeugte HashMap
	 */
	private HashMap<String, List<Exception>> buildExcHashMap( CascadeTAL mTALResult ) {
		HashMap<String, List<Exception>> ret_val = new HashMap<String, List<Exception>>();

		// Exception der TAL ?
		if( mTALResult.getExceptions().size() > 0 )
			ret_val.put( "-1", mTALResult.getExceptions() );

		CascadeTALLine[] myLines = mTALResult.getTalLines();
		for( int i = 0; i < myLines.length; i++ ) {
			String sgAddressTemp = "0" + Integer.toHexString( myLines[i].getDiagnosticAddress().intValue() ).toUpperCase(); // z.B. "012"
			String sgDiagAddress = "0x" + sgAddressTemp.substring( sgAddressTemp.length() - 2, sgAddressTemp.length() ); // z.B. "0x12"
			String sgCat = "_" + myLines[i].getTALTACategories()[0].getCategory().toString().toLowerCase().trim(); // z.B. "_swdeploy"
			boolean foundException = false;

			// Exception in der TALLine?
			if( myLines[i].getExceptions().size() > 0 ) {
				ret_val.put( sgDiagAddress + sgCat, myLines[i].getExceptions() );
				foundException = true;
			}

			if( !foundException ) {
				for( int j = 0; j < myLines[i].getTALTACategories().length; j++ ) {
					CascadeTALTACategory myTALTACategory = myLines[i].getTALTACategories()[j];
					sgCat = "_" + myTALTACategory.getCategory().toString().toLowerCase().trim();
					// Pr�fung der TAs
					for( int k = 0; k < myTALTACategory.getTAs().length; k++ ) {
						CascadeTA myTA = myTALTACategory.getTAs()[k];
						// Exception in der Transaktion?
						if( myTA.getExceptions() != null ) {
							ret_val.put( sgDiagAddress + sgCat, myTA.getExceptions() );
						}
					}
					// Pr�fung der FscTAs					
					for( int k = 0; k < myTALTACategory.getFscTAs().length; k++ ) {
						CascadeFscTA myFscTA = myTALTACategory.getFscTAs()[k];
						// Exception in der FSC Transaktion?
						if( myFscTA.getExceptions() != null ) {
							ret_val.put( sgDiagAddress + sgCat, myFscTA.getExceptions() );
						}
					}
				}
			}
		}
		return ret_val;
	}
	/* ***************************** end of copied code *********************************************************************** */

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 *
	 * @param info
	 * Information zur Ausf�hrung
	 */
	public void execute(ExecutionInfo info) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// Devices
		UserDialog userDialog = null;
		SignalLamp signalLamp = null;
		PSdZ psdz = null;

		// Instanzkennung f�r Multiinstanz
		String testScreenID = getPr�flingLaufzeitUmgebung().getName();

		// Variablen allgemein
		int i;
		int j;
		int k;
		int m;
		int n;
		boolean printerFlag = false;
		boolean testFlag = true;
		boolean testFlagTemp = true;
		boolean initFlag = false;
		boolean userBreakFlag = false;
		boolean connectionFlag = false;
		String vehicleVinB2v = "";
		String vehicleVinFzg = "";
		String istIStufe = "";
		String sollIStufeBackup = "";
		String zusatzMessage = "";
		String temp = "";
		String[] deviceParams = null;
		CascadeFA FA = null;
		CascadeSVT SVTIst = null;
		String SVTIstXML = "";
		HashMap<String, String> programmingDependenciesListe = null;
		HashMap<String, String> programmingDependenciesListeNeu = null;
		CascadeSVT SVTIstNeu = null;
		CascadeSVT SVTSoll = null;
		CascadeLogistischesTeil[] bestellNrListe = null;
		CascadeTAL TAL = null;
		CascadeTALFilter TALFilter = null;
		CascadeFilteredTACategory filteredTACategory = null;
		List<CascadeSVTReader> SVKListeTrailer = null;
		CascadeSVTReader SVKTrailer = null;
		CascadeSVTReader SVKTrailerVerify = null;
		EcuListeElement[] ecuListe = null;
		TraceFileElement traceFileElement = null;
		LogFileElement logFileElement = null;
		// Adresslisten
		String[] ecuAddressList = null;
		String[] ecuIgnorList = null;
		String[] ecuFlashList = null;
		String[] ecuCheckProgDepFlashList = null;
		String[] ecuAnlieferstandFlashList = null;
//		String[] ecuNoFlashList = null;
		String[] ecuZwangsTauschList = null;
		String[] ecuEinbauList = null;
		String[] ecuAusbauList = null;
		String[] ecuTauschList = null;
		String[] ecuProgrammList = null;
		// Zwischenspeicher f�r Pr�fstandsvariablen f�r Modeumschaltung
		boolean modeSwitchFlag = false;
		Object PSDZ__TEMP_MODE_SWITCH_old = null;
		Object PSDZ__TEMP_MODE_SWITCH_GATEWAY_LIST_old = null;
		// Zwischenspeicher f�r Pr�fstandsvariablen f�r SollverbauRoh-Berechnung
		String PSDZ__KIS_CALCULATION_LEVEL_old = null;
		// Zwischenspeicher f�r PSdZ Zustand
		boolean PSDZCheckProgDepsFlag = true;

		// Verwaltung der verarbeiteten SG [IO, NIO, NOTEXECUTABLE] in sortierten Listen
		// mit Variable=ECU und Wert=TA, SWT Kategories als HashSet
		// Bsp.: Variable=0x29 ECU:DSC, Wert=cdDeploy, fscDeploy als HashSet
		// TreeMap<String, HashSet<String>> mapEcuIO = new TreeMap<String, HashSet<String>>();
		TreeMap<String, HashSet<String>> mapEcuNIO = new TreeMap<String, HashSet<String>>();
		TreeMap<String, HashSet<String>> mapEcuNotExecutable = new TreeMap<String, HashSet<String>>();

		Cfs cfs = new Cfs();
		// Benutztes Fahrzeug f�r die Instanz freigeben
		try {
			vehicleVinInUse[cfs.instanz] = "";
		} catch( Exception e ) {
			// keine Fehlermeldung wenn nicht erlaubte instanz
		}

		try {
			// Stoppuhr starten
			cfs.startStopWatch();
			// Parameter holen
			try {
				if (checkArgs() == false) throw new PPExecutionException(PB.getString("parameterexistenz"));
				cfs.setTrailerModeFlag(getArg(getRequiredArgs()[0]).equalsIgnoreCase("TRUE"));
	           	cfs.setSamplingFlag(getArg(getRequiredArgs()[1]).equalsIgnoreCase("TRUE"));

				String interfaceIPAddress=null;
				// veraltet. Ersetzt durch INTERFACE_IP_ADDRESS
	           	if( getArg(getOptionalArgs()[0]) != null ) {
					interfaceIPAddress = extractValues( getArg(getOptionalArgs()[0]))[0];
					if( interfaceIPAddress.startsWith( "_VARIABLE:" + EDIC_NET_IP ) )
						interfaceIPAddress = (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, EDIC_NET_IP );
		           	cfs.setInterfaceIPAddress(interfaceIPAddress);
				}

	           	// USE_FZS
	           	if( getArg(getOptionalArgs()[1]) != null ) {
	           		cfs.setUseFzsFlag(getArg(getOptionalArgs()[1]).equalsIgnoreCase("TRUE"));
	           	}

	           	// INTERFACE_IP_ADDRESS
	           	if( getArg(getOptionalArgs()[2]) != null ) {
					interfaceIPAddress = extractValues( getArg(getOptionalArgs()[2]))[0];
					if( interfaceIPAddress.startsWith( "_VARIABLE:" + INTERFACE_NET_IP ) )
						interfaceIPAddress = (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, INTERFACE_NET_IP );
		           	cfs.setInterfaceIPAddress(interfaceIPAddress);
				}

	            // ICOM_CAN_BRIDGE_ACTIVE
	           	if(getArg(getOptionalArgs()[3]) != null ) {
	           		cfs.setIcomCanBridgeActiveFlag(getArg(getOptionalArgs()[3]).equalsIgnoreCase("TRUE"));
	           	}
	           	
                // Pr�fstandsvariablen merken
				modeSwitchFlag = false;
	           	try {
	           		PSDZ__TEMP_MODE_SWITCH_old = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_MODE_SWITCH@" + testScreenID);
	           	} catch( Exception e ) {
	           		PSDZ__TEMP_MODE_SWITCH_old = null;
	           	}
	           	try {
	           		PSDZ__TEMP_MODE_SWITCH_GATEWAY_LIST_old = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_MODE_SWITCH_GATEWAY_LIST@" + testScreenID);
	           	} catch( Exception e ) {
	           		PSDZ__TEMP_MODE_SWITCH_GATEWAY_LIST_old  = null;
	           	}

			} catch (PPExecutionException e) {
				if (e.getMessage() != null)
					result = new Ergebnis("ParaFehler", "", "", "", "", "", "",	"", "", "0", "", "", "", PB.getString("parametrierfehler"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS);
				else
					result = new Ergebnis("ParaFehler", "", "", "", "", "", "",	"", "", "0", "", "", "", PB.getString("parametrierfehler"), "PPExecutionException", Ergebnis.FT_NIO_SYS);
				ergListe.add(result);
				throw new PPExecutionException();
			}

			// Drucker-Device holen und freigeben
			try {
				printerFlag = false;
				Printer printer = null;
				printer = getPr�flingLaufzeitUmgebung().getDeviceManager().getPrinter();
				if ( printer != null) {
					getPr�flingLaufzeitUmgebung().getDeviceManager().releasePrinter();
					printerFlag = true;
				}
			} catch (Exception e) {
				// keine Fehlermeldung wenn nicht verf�gbar
				printerFlag = false;
			}

			// Signallampen-Device holen
			try {
				signalLamp = getPr�flingLaufzeitUmgebung().getDeviceManager().getSignalLamp();
				if( signalLamp != null ) {
					signalLamp.LightsOff();
				}
			} catch (Exception e) {
				// keine Fehlermeldung wenn nicht verf�gbar
			}

			// UserDialog-Device holen
			try {
				userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// PSdZ-Device holen
			try {
				psdz = getPr�flingLaufzeitUmgebung().getDeviceManager().getPSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// INI-Datei lesen
			if (!cfs.loadINIFile()) {
				result = new Ergebnis( "CFS", "loadINIFile", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Diagnoseinterfaces abfragen
 			deviceParams = psdz.getDeviceParams();
			if (deviceParams == null) {
				result = new Ergebnis( "PSdZ", "getDeviceParams", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetdeviceparamserror"), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Setzen der Modusumschaltung
			try {
				modeSwitchFlag = true;
				if (deviceParams[0].equalsIgnoreCase("ENET")) {
					getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_MODE_SWITCH@" + testScreenID, new Boolean(cfs.getModeSwitch()) );
				} else {
					getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_MODE_SWITCH@" + testScreenID, new Boolean(false) );
				}

				getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_MODE_SWITCH_GATEWAY_LIST@" + testScreenID, new ArrayList<Object>());
			} catch( Exception e ) {
				// keine Fehlermeldung wenn nicht verf�gbar
			}

			// Trailer oder Gesamtfahrzeug
			if (testFlag) {
				if (cfs.getTrailerModeFlag() != true) {
// Bookmark Gesamtfahrzeug Anfang
					if (testFlag) {
						// Diagnoseinterfaces abfragen
						if (cfs.instanz == 1) {
							if (!deviceParams[0].equalsIgnoreCase("ENET") &&
							    !deviceParams[0].equalsIgnoreCase("ICOM") &&
							    !deviceParams[0].equalsIgnoreCase("STD:FUNK") &&
							    !deviceParams[0].equalsIgnoreCase("STD:OMITEC")) {
								testFlag=false;
								result = new Ergebnis( "PSdZ", "getDeviceParams", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetdeviceparamsinterfacenotsupportederror"), deviceParams[0]+ " <> ENET,ICOM,STD:FUNK,STD:OMITEC" , Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								throw new PPExecutionException();
							}
						} else {
							if (!deviceParams[0].equalsIgnoreCase("ENET") &&
								!deviceParams[0].equalsIgnoreCase("ICOM")) {
								testFlag=false;
								result = new Ergebnis( "PSdZ", "getDeviceParams", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetdeviceparamsinterfacenotsupportederror"), deviceParams[0]+ " <> ENET,ICOM" , Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								throw new PPExecutionException();
							}
						}
						if (deviceParams[0].equalsIgnoreCase("ICOM")) {
							if (cfs.getInterfaceIPAddress()==null) {
								testFlag=false;
								result = new Ergebnis("ParaFehler", "", "", "", "", "", "",	"", "", "0", "", "", "", PB.getString("parametrierfehler"), "INTERFACE_IP_ADDRESS (EDICNET_IP_ADDRESS)", Ergebnis.FT_NIO_SYS);
								ergListe.add( result );
								throw new PPExecutionException();
							}
						}
					}

					initFlag=false;
					if (testFlag) {
						// PSdZ Initialisierung aufrufen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzinit"),CfsString.get("cfs.running"), -1 );
							psdz.setTestScreenID(testScreenID);
							psdz.init();
							userDialog.reset();
							initFlag=true;
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "init", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdziniterror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "init", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdziniterror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "init", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdziniterror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "init", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdziniterror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						}
					}

					if (testFlag) {
						// PSdZ Installierte I-Stufen abfragen
						try {
							String iStufenListe[]=null;
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgetinstalledistufen"),CfsString.get("cfs.running"), -1 );
							iStufenListe=psdz.getInstalledIStufen();
							if (iStufenListe == null) {
								testFlag=false;
								result = new Ergebnis( "Cfs", "getInstalledIStufen", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.noistufenlisteerror"), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							} else {
								if (iStufenListe.length == 0) {
									testFlag=false;
									result = new Ergebnis( "Cfs", "getInstalledIStufen", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.emptyistufenlisteerror"), "", Ergebnis.FT_NIO );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								} else {
									// Auftrags I-Stufe, Auftrags I-Stufe Baureihenverbund, Default I-Stufe, erste I-Stufe
									// wird erst einmal als Soll-Istufe benutzt
									// Nach dem Auslesen der Fahrzeugdaten wird die richtige I-Stufe benutzt
                                    int iStufenIndex = -1;
                                    if (iStufenIndex == -1) {
                                        // Versuch mit Auftrags I-Stufe
                                        temp = getPr�fling().getAuftrag().getIStufe();
                                        for(i=0;i<iStufenListe.length;i++) {
                                        	if (iStufenListe[i].equalsIgnoreCase(temp)) {
                                        		iStufenIndex = i;
                                        		break;
                                        	}
                                        }
                                    }
                                    if (iStufenIndex == -1) {
                                        // Versuch mit Auftrags I-Stufe Baureihenverbund
                                        temp = getPr�fling().getAuftrag().getIStufeBRV();
                                        for(i=0;i<iStufenListe.length;i++) {
                                        	if (iStufenListe[i].substring( 0, 4 ).equalsIgnoreCase(temp)) {
                                        		iStufenIndex = i;
                                        		break;
                                        	}
                                        }
                                    }
                                    if (iStufenIndex == -1) {
                                        // Versuch mit Default I-Stufe
                                    	temp = cfs.getDefaultIStufe();
                                        for(i=0;i<iStufenListe.length;i++) {
                                        	if (iStufenListe[i].equalsIgnoreCase(temp)) {
                                        		iStufenIndex = i;
                                        		break;
                                        	}
                                        }
                                    }
									if (iStufenIndex == -1) {
										// Erste I-Stufe
										iStufenIndex = 0;
									}
                                    if (!cfs.setSollIStufe(iStufenListe[iStufenIndex])) {
										testFlag=false;
										result = new Ergebnis( "Cfs", "getInstalledIStufen", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.invalidilevel"), "", Ergebnis.FT_NIO );
										ergListe.add( result );
										status = STATUS_EXECUTION_ERROR;
									}
								}
							}
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getInstalledIStufen", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetinstalledistufenerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getInstalledIStufen", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetinstalledistufenerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getInstalledIStufen", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetinstalledistufenerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getInstalledIStufen", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetinstalledistufenerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						}
					}

					if (testFlag) {
						if (deviceParams[0].equalsIgnoreCase("ENET")) {
							if (cfs.getInterfaceIPAddress()!=null) {
								// EDICNET-IP-Address
								psdz.setEDICnetIPAddress(cfs.getInterfaceIPAddress());
								vehicleVinB2v = getPr�fling().getAuftrag().getFahrgestellnummer();
			  					psdz.setVehicleVin(vehicleVinB2v);
							} else {
								// PSdZ B2V Gateways abfragen und zuordnen
								try {
									userDialog.setDisplayProgress( true );
									userDialog.getProgressbar().setIndeterminate( true );
									userDialog.displayMessage( CfsString.get("cfs.psdzgetvehiclevinsfromb2v"), CfsString.get("cfs.running"), -1 );
									String vehicleVinsListe[]=null;
									vehicleVinsListe = psdz.getVehicleVinsFromB2V();
									userDialog.reset();
									if (vehicleVinsListe.length == 0) {
										testFlag=false;
										result = new Ergebnis( "PSdZ", "getVehicleVinsFromB2V", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.nozgwfounderror"), CfsString.get("cfs.nozgwfoundhelp"), Ergebnis.FT_NIO );
										ergListe.add( result );
										status = STATUS_EXECUTION_ERROR;
									} else {
										if (vehicleVinsListe.length == 1) {
											vehicleVinB2v = vehicleVinsListe[0];
										} else {
											boolean gefunden = false;
											for (i=0; i<vehicleVinsListe.length; i++) {
												if ((cfs.getInstanz()==1) && (vehicleVinsListe[i].equals(cfs.getZgwVin1()+ZGW_EXTENSION))) {
													vehicleVinB2v=vehicleVinsListe[i];
													gefunden=true;
													break;
												}
												if ((cfs.getInstanz()==2) && (vehicleVinsListe[i].equals(cfs.getZgwVin2()+ZGW_EXTENSION))) {
													vehicleVinB2v=vehicleVinsListe[i];
													gefunden=true;
													break;
												}
											}
											if (!gefunden) {
												for (i=0; i<vehicleVinsListe.length; i++) {
													if (vehicleVinsListe[i].equals(getPr�fling().getAuftrag().getFahrgestellnummer()+ZGW_EXTENSION)) {
														vehicleVinB2v=vehicleVinsListe[i];
														gefunden=true;
														break;
													}
												}
											}
											if (!gefunden) {
												boolean selectedFlag=true;
												int index[]=null;
												if( signalLamp != null ) {
													signalLamp.YellowLightOn();
												}
												cfs.stopStopWatch();
												do {
													selectedFlag=true;
													userDialog.reset();
													userDialog.setAllowCancel( true );
													userDialog.setBackgroundColor(cfs.backColor);
													index=userDialog.requestUserInputSingleChoice( CfsString.get("cfs.vinselect") , CfsString.get("cfs.selectonevin"), 0, vehicleVinsListe );
													if (!userDialog.isCancelled()) {
														if ((index != null) && (index.length == 1)) {
															vehicleVinB2v = vehicleVinsListe[index[0]];
														} else {
															selectedFlag=false;
															userDialog.reset();
															userDialog.setBackgroundColor(cfs.backColor);
															userDialog.displayMessage( CfsString.get("cfs.selectwarning"),CfsString.get("cfs.selectonevin") , AUTOMATIC_MESSAGE_TIMEOUT );
														}
													} else {
														userBreakFlag = true;
														testFlag=false;
														result = new Ergebnis( "CFS", "vinSelect", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.userbreakerror"), "", Ergebnis.FT_NIO );
														ergListe.add( result );
														status = STATUS_EXECUTION_ERROR;
													}
												} while (!selectedFlag);
												cfs.continueStopWatch();
												userDialog.reset();
												if( signalLamp != null ) {
													signalLamp.LightsOff();
												}
											}
										}
									}
								} catch ( Exception e ) {
									userDialog.reset();
									if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
									testFlag=false;
									if (e.getMessage() != null)
										result = new Ergebnis( "PSdZ", "getVehicleVinsFromB2V", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetvehiclevinsfromb2verror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
									else
										result = new Ergebnis( "PSdZ", "getVehicleVinsFromB2V", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetvehiclevinsfromb2verror"), "Exception", Ergebnis.FT_NIO_SYS );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								} catch( Throwable e ) {
									userDialog.reset();
									if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
									testFlag=false;
									if (e.getMessage() != null)
										result = new Ergebnis( "PSdZ", "getVehicleVinsFromB2V", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetvehiclevinsfromb2verror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
									else
										result = new Ergebnis( "PSdZ", "getVehicleVinsFromB2V", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetvehiclevinsfromb2verror"), "Throwable", Ergebnis.FT_NIO_SYS );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
								if (testFlag) {
									if (vehicleVinB2v.equalsIgnoreCase(vehicleVinInUse[cfs.instanz==1 ? 2 : 1])) {
										testFlag=false;
										result = new Ergebnis( "PSdZ", "getVehicleVinsFromB2V", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.zgwinuseerror"), vehicleVinB2v, Ergebnis.FT_NIO );
										ergListe.add( result );
										status = STATUS_EXECUTION_ERROR;
									} else {
										psdz.setVehicleVin(vehicleVinB2v);
										try {
											vehicleVinInUse[cfs.instanz] = vehicleVinB2v;
										} catch( Exception e ) {
											// keine Fehlermeldung wenn nicht erlaubte instanz
										}
									}
								}
							}
						}

						if (deviceParams[0].equalsIgnoreCase("ICOM")) {
							if (cfs.getInterfaceIPAddress()!=null) {
								// ICOM-IP-Address
								psdz.setEDICnetIPAddress(cfs.getInterfaceIPAddress());
								vehicleVinB2v = getPr�fling().getAuftrag().getFahrgestellnummer();
			  					psdz.setVehicleVin(vehicleVinB2v);
							}
						}

						if (deviceParams[0].equalsIgnoreCase("STD:FUNK")) {
							if (cfs.getUseFzsFlag())
								vehicleVinB2v = "CASCADEFZS" + getPr�fling().getAuftrag().getSteuerschluessel();
							else
								vehicleVinB2v = getPr�fling().getAuftrag().getFahrgestellnummer();
							psdz.setVehicleVin(vehicleVinB2v);
						}

						if (deviceParams[0].equalsIgnoreCase("STD:OMITEC")) {
							vehicleVinB2v = getPr�fling().getAuftrag().getFahrgestellnummer();
		  					psdz.setVehicleVin(vehicleVinB2v);
						}
					}

					// Erst mal die erste gefundenen I-Stufe
					if (testFlag) {
						psdz.setIStufeSoll(cfs.getSollIStufe());
					}

					connectionFlag=false;
					if (testFlag) {
						// Verbindung zum Fahrzeug herstellen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							temp = "Interface=" + deviceParams[0] + NL;
							if (deviceParams[0].equalsIgnoreCase("ENET")) {
								temp="";
								if (cfs.getInterfaceIPAddress()!=null) {
									// EDICNET-IP-Address
									temp = "Interface=EDICNET, " + cfs.getInterfaceIPAddress() + NL;
								}
							}
							if (deviceParams[0].equalsIgnoreCase("ICOM")) {
								temp = "Interface=ICOM"; 
								if (cfs.getIcomCanBridgeActiveFlag()) {
									temp = temp + "(D-CAN)";
									psdz.setICOMLinkPropertiesToDCan();
								} else {
									temp = temp + "(HSFZ)";
									psdz.setICOMLinkPropertiesToEthernet();
								}
								if (cfs.getInterfaceIPAddress()!=null) {
									// ICOM-IP-Address
									temp = temp + ", " + cfs.getInterfaceIPAddress();
								}
								temp = temp + NL;
							}
							if (deviceParams[0].equalsIgnoreCase("STD:FUNK")) {
								temp = "Interface=STD:FUNK, ";
								if (cfs.getUseFzsFlag())
									temp = temp + "FZS="+ getPr�fling().getAuftrag().getSteuerschluessel() + NL;
								else
									temp = temp + "VIN="+ getPr�fling().getAuftrag().getFahrgestellnummer() + NL;
							}
							sollIStufeBackup=cfs.getSollIStufe();
							userDialog.displayMessage( CfsString.get("cfs.psdzopenvehicleconnection"),temp + cfs.getSollIStufe() + NL + CfsString.get("cfs.running"), -1 );
							connectionFlag=true;
							psdz.openVehicleConnectionByIStep();
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							temp = "Interface=" + deviceParams[0];
							if (deviceParams[0].equalsIgnoreCase("ENET")) {
								if (cfs.getInterfaceIPAddress()!=null) {
									// EDICNET-IP-Address
									temp = "Interface=EDICNET, " + cfs.getInterfaceIPAddress();
								}
							}
							if (deviceParams[0].equalsIgnoreCase("ICOM")) {
								temp = "Interface=ICOM"; 
								if (cfs.getIcomCanBridgeActiveFlag()) {
									temp = temp + "(D-CAN)";
								} else {
									temp = temp + "(HSFZ)";
								}
								if (cfs.getInterfaceIPAddress()!=null) {
									// ICOM-IP-Address
									temp = temp + ", " + cfs.getInterfaceIPAddress();
								}
							}
							if (deviceParams[0].equalsIgnoreCase("STD:FUNK")) {
								temp = "Interface=STD:FUNK, ";
								if (cfs.getUseFzsFlag())
									temp = temp + "CASCADE-FZS="+ getPr�fling().getAuftrag().getSteuerschluessel();
								else
									temp = temp + "CASCADE-VIN="+ getPr�fling().getAuftrag().getFahrgestellnummer();
							}
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "openVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzopenvehicleconnectionerror"), "Exception "+temp+NL+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "openVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzopenvehicleconnectionerror"), "Exception "+temp , Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "openVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzopenvehicleconnectionerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "openVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzopenvehicleconnectionerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// Fahrzeugauftrag aus Fahrzeug auslesen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgetfafromvehicle"), CfsString.get("cfs.running"), -1 );
							FA = psdz.getFAFromVehicle(false,true);
							if (!cfs.isValidFA(FA)) {
								// Backup versuchen
								userDialog.displayMessage( CfsString.get("cfs.psdzgetfafromvehiclebackup"), CfsString.get("cfs.running"), -1 );
								FA = psdz.getFAFromVehicle(true,true);
								if (!cfs.isValidFA(FA)) {
									testFlag=false;
									result = new Ergebnis( "Cfs", "getFAFromVehicle", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.invalidfa"), "", Ergebnis.FT_NIO );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
							}
							logFileElement = new LogFileElement();
							logFileElement.FA = FA;
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getFAFromVehicle", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetfafromvehicleerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getFAFromVehicle", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetfafromvehicleerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getFAFromVehicle", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetfafromvehicleerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getFAFromVehicle", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetfafromvehicleerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// V33.01
						// Bei Baureihenverbund F025 trotzdem neu verbinden, wegen VCM-Backup unterschiedlich bei F25 und F15
						// Bei der ersten Verbindung ist die Baureihe noch unbekannt
						// Danach wird die Baureihe �ber Fahrzeugauftrag lesen ermittelt
						if (cfs.getSollIStufe().startsWith("F025")) {
							// Alte Verbindung zum Fahrzeug abbauen
							try {
								userDialog.setDisplayProgress( true );
								userDialog.getProgressbar().setIndeterminate( true );
								userDialog.displayMessage( CfsString.get("cfs.psdzclosevehicleconnection"),temp + sollIStufeBackup + NL + CfsString.get("cfs.running") + NL , -1 );
								psdz.closeVehicleConnection();
								connectionFlag=false;
								userDialog.reset();
							} catch ( Exception e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "PSdZ", "closeVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzclosevehicleconnectionerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "PSdZ", "closeVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzclosevehicleconnectionerror"), "Exception", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							} catch( Throwable e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "PSdZ", "closeVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzclosevehicleconnectionerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "PSdZ", "closeVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzclosevehicleconnectionerror"), "Throwable", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
							if (testFlag) {
								// Verbindung zum Fahrzeug herstellen
								try {
									userDialog.setDisplayProgress( true );
									userDialog.getProgressbar().setIndeterminate( true );
									temp = "Interface=" + deviceParams[0] + NL;
									if (deviceParams[0].equalsIgnoreCase("ENET")) {
										temp="";
										if (cfs.getInterfaceIPAddress()!=null) {
											// EDICNET-IP-Address
											temp = "Interface=EDICNET, " + cfs.getInterfaceIPAddress() + NL;
										}
									}
									if (deviceParams[0].equalsIgnoreCase("ICOM")) {
										temp = "Interface=ICOM"; 
										if (cfs.getIcomCanBridgeActiveFlag()) {
											temp = temp + "(D-CAN)";
											psdz.setICOMLinkPropertiesToDCan();
										} else {
											temp = temp + "(HSFZ)";
											psdz.setICOMLinkPropertiesToEthernet();
										}
										if (cfs.getInterfaceIPAddress()!=null) {
											// ICOM-IP-Address
											temp = temp + ", " + cfs.getInterfaceIPAddress();
										}
										temp = temp + NL;
									}
									if (deviceParams[0].equalsIgnoreCase("STD:FUNK")) {
										temp = "Interface=STD:FUNK, ";
										if (cfs.getUseFzsFlag())
											temp = temp + "FZS="+ getPr�fling().getAuftrag().getSteuerschluessel() + NL;
										else
											temp = temp + "VIN="+ getPr�fling().getAuftrag().getFahrgestellnummer() + NL;
									}
									sollIStufeBackup=cfs.getSollIStufe();
									userDialog.displayMessage( CfsString.get("cfs.psdzopenvehicleconnection"),temp + cfs.getSollIStufe() + NL + CfsString.get("cfs.running"), -1 );
									connectionFlag=true;
									psdz.openVehicleConnectionByIStep();
									userDialog.reset();
								} catch ( Exception e ) {
									userDialog.reset();
									temp = "Interface=" + deviceParams[0];
									if (deviceParams[0].equalsIgnoreCase("ENET")) {
										if (cfs.getInterfaceIPAddress()!=null) {
											// EDICNET-IP-Address
											temp = "Interface=EDICNET, " + cfs.getInterfaceIPAddress();
										}
									}
									if (deviceParams[0].equalsIgnoreCase("ICOM")) {
										temp = "Interface=ICOM"; 
										if (cfs.getIcomCanBridgeActiveFlag()) {
											temp = temp + "(D-CAN)";
										} else {
											temp = temp + "(HSFZ)";
										}
										if (cfs.getInterfaceIPAddress()!=null) {
											// ICOM-IP-Address
											temp = temp + ", " + cfs.getInterfaceIPAddress();
										}
										temp = temp + NL;
									}
									if (deviceParams[0].equalsIgnoreCase("STD:FUNK")) {
										temp = "Interface=STD:FUNK, ";
										if (cfs.getUseFzsFlag())
											temp = temp + "CASCADE-FZS="+ getPr�fling().getAuftrag().getSteuerschluessel();
										else
											temp = temp + "CASCADE-VIN="+ getPr�fling().getAuftrag().getFahrgestellnummer();
									}
									if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
									testFlag=false;
									if (e.getMessage() != null)
										result = new Ergebnis( "PSdZ", "openVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzopenvehicleconnectionerror"), "Exception "+temp+NL+e.getMessage(), Ergebnis.FT_NIO_SYS );
									else
										result = new Ergebnis( "PSdZ", "openVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzopenvehicleconnectionerror"), "Exception "+temp , Ergebnis.FT_NIO_SYS );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								} catch( Throwable e ) {
									userDialog.reset();
									if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
									testFlag=false;
									if (e.getMessage() != null)
										result = new Ergebnis( "PSdZ", "openVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzopenvehicleconnectionerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
									else
										result = new Ergebnis( "PSdZ", "openVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzopenvehicleconnectionerror"), "Throwable", Ergebnis.FT_NIO_SYS );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
							}
						}
					}

					if (testFlag) {
						// Fahrgestellnummer aus Fahrzeug auslesen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgetvinfromvehicle"), CfsString.get("cfs.running"), -1 );
							vehicleVinFzg = psdz.getVinFromVehicle(false);
							if (!cfs.isValidVIN(vehicleVinFzg)) {
								// Backup versuchen
								userDialog.displayMessage( CfsString.get("cfs.psdzgetvinfromvehiclebackup"), CfsString.get("cfs.running"), -1 );
								vehicleVinFzg = psdz.getVinFromVehicle(true);
								if (!cfs.isValidVIN(vehicleVinFzg)) {
									testFlag=false;
									result = new Ergebnis( "Cfs", "getVinFromVehicle", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.invalidvin"), "", Ergebnis.FT_NIO );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
							}
							logFileElement.vehicleVin = vehicleVinFzg;
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getVinFromVehicle", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetvinfromvehicleerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getVinFromVehicle", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetvinfromvehicleerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getVinFromVehicle", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetvinfromvehicleerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getVinFromVehicle", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetvinfromvehicleerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// I-Stufe aus Fahrzeug auslesen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgetistepfromvehicle"), CfsString.get("cfs.running"), -1 );
							istIStufe = psdz.getIStepFromVehicle(false);
							if (!cfs.isValidIStufe(istIStufe)) {
								// Backup versuchen
								userDialog.displayMessage( CfsString.get("cfs.psdzgetistepfromvehiclebackup"), CfsString.get("cfs.running"), -1 );
								istIStufe = psdz.getIStepFromVehicle(true);
								if (!cfs.isValidIStufe(istIStufe)) {
									istIStufe=cfs.UNKNOWNISTUFE;
								}
							}
							logFileElement.istIStufe = istIStufe;
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getIStepFromVehicle", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetistepfromvehicleerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getIStepFromVehicle", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetistepfromvehicleerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getIStepFromVehicle", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetistepfromvehicleerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getIStepFromVehicle", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetistepfromvehicleerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					// IStufen-Datei lesen
					if (testFlag) {
						if (cfs.loadIStufeFile(istIStufe, FA)) {
							logFileElement.sollIStufe = cfs.getSollIStufe();
						} else {
							testFlag=false;
							result = new Ergebnis( "CFS", "loadIStufeFile", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// Ziel-Istufe kann andere sein als die zuerst gew�hlte
						psdz.setIStufeSoll(cfs.getSollIStufe());
						boolean connectNewFlag = true;
						if (sollIStufeBackup.equalsIgnoreCase(cfs.getSollIStufe()))
							connectNewFlag=false;

						if (connectNewFlag)
						{
							// Alte Verbindung zum Fahrzeug abbauen
							try {
								userDialog.setDisplayProgress( true );
								userDialog.getProgressbar().setIndeterminate( true );
								userDialog.displayMessage( CfsString.get("cfs.psdzclosevehicleconnection"),temp + sollIStufeBackup + NL + CfsString.get("cfs.running") + NL , -1 );
								psdz.closeVehicleConnection();
								connectionFlag=false;
								userDialog.reset();
							} catch ( Exception e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "PSdZ", "closeVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzclosevehicleconnectionerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "PSdZ", "closeVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzclosevehicleconnectionerror"), "Exception", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							} catch( Throwable e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "PSdZ", "closeVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzclosevehicleconnectionerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "PSdZ", "closeVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzclosevehicleconnectionerror"), "Throwable", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}

							if (testFlag) {
								// Neue Verbindung zum Fahrzeug herstellen
								try {
									userDialog.setDisplayProgress( true );
									userDialog.getProgressbar().setIndeterminate( true );
									temp = "Interface=" + deviceParams[0] + NL;
									if (deviceParams[0].equalsIgnoreCase("ENET")) {
										temp="";
										if (cfs.getInterfaceIPAddress()!=null) {
											// EDICNET-IP-Address
											temp = "Interface=EDICNET, " + cfs.getInterfaceIPAddress() + NL;
										}
									}
									if (deviceParams[0].equalsIgnoreCase("ICOM")) {
										temp = "Interface=ICOM"; 
										if (cfs.getIcomCanBridgeActiveFlag()) {
											temp = temp + "(D-CAN)";
											psdz.setICOMLinkPropertiesToDCan();
										} else {
											temp = temp + "(HSFZ)";
											psdz.setICOMLinkPropertiesToEthernet();
										}
										if (cfs.getInterfaceIPAddress()!=null) {
											// ICOM-IP-Address
											temp = temp + ", " + cfs.getInterfaceIPAddress();
										}
										temp = temp + NL;
									}
									if (deviceParams[0].equalsIgnoreCase("STD:FUNK")) {
										temp = "Interface=STD:FUNK, ";
										if (cfs.getUseFzsFlag())
											temp = temp + "FZS="+ getPr�fling().getAuftrag().getSteuerschluessel() + NL;
										else
											temp = temp + "VIN="+ getPr�fling().getAuftrag().getFahrgestellnummer() + NL;
									}
									sollIStufeBackup=cfs.getSollIStufe();
									userDialog.displayMessage( CfsString.get("cfs.psdzopenvehicleconnection"),temp + cfs.getSollIStufe() + NL + CfsString.get("cfs.running"), -1 );
									connectionFlag=true;
									psdz.openVehicleConnectionByIStep();
									userDialog.reset();
								} catch ( Exception e ) {
									temp = "Interface=" + deviceParams[0];
									if (deviceParams[0].equalsIgnoreCase("ENET")) {
										if (cfs.getInterfaceIPAddress()!=null) {
											// EDICNET-IP-Address
											temp = "Interface=EDICNET, " + cfs.getInterfaceIPAddress();
										}
									}
									if (deviceParams[0].equalsIgnoreCase("ICOM")) {
										temp = "Interface=ICOM"; 
										if (cfs.getIcomCanBridgeActiveFlag()) {
											temp = temp + "(D-CAN)";
										} else {
											temp = temp + "(HSFZ)";
										}
										if (cfs.getInterfaceIPAddress()!=null) {
											// ICOM-IP-Address
											temp = temp + ", " + cfs.getInterfaceIPAddress();
										}
										temp = temp + NL;
									}
									if (deviceParams[0].equalsIgnoreCase("STD:FUNK")) {
										temp = "Interface=STD:FUNK, ";
										if (cfs.getUseFzsFlag())
											temp = temp + "CASCADE-FZS="+ getPr�fling().getAuftrag().getSteuerschluessel();
										else
											temp = temp + "CASCADE-VIN="+ getPr�fling().getAuftrag().getFahrgestellnummer();
									}
									userDialog.reset();
									if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
									testFlag=false;
									if (e.getMessage() != null)
										result = new Ergebnis( "PSdZ", "openVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzopenvehicleconnectionerror"), "Exception "+temp+NL+e.getMessage(), Ergebnis.FT_NIO_SYS );
									else
										result = new Ergebnis( "PSdZ", "openVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzopenvehicleconnectionerror"), "Exception "+temp, Ergebnis.FT_NIO_SYS );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								} catch( Throwable e ) {
									userDialog.reset();
									if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
									testFlag=false;
									if (e.getMessage() != null)
										result = new Ergebnis( "PSdZ", "openVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzopenvehicleconnectionerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
									else
										result = new Ergebnis( "PSdZ", "openVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzopenvehicleconnectionerror"), "Throwable", Ergebnis.FT_NIO_SYS );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
							}
						}
					}

					// �berpr�fung ob KIS-Level mit KIS-WB zusammen passen
					if (testFlag) {
						String kisCalculationLevel=null;
						try {
							kisCalculationLevel = (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, PSDZ__KIS_CALCULATION_LEVEL );
						} catch ( Exception e ) {
							kisCalculationLevel=null;
						}
						cfs.setKisCalculationLevel(kisCalculationLevel);

						if (!cfs.checkKisCalculationLevel(cfs.getKisCalculationLevel(), cfs.getSollIStufe())) {
							testFlag=false;
							result = new Ergebnis( "CFS", "checkKisCalculationLevel", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					// Fahrgestellnummer aus Fahrzeug setzen
					if (testFlag) {
	  					psdz.setVehicleVin(vehicleVinFzg);
					}

					// SG_VERBAU-Datei lesen
					if (testFlag) {
						if (cfs.loadSgVerbauFile(istIStufe, FA)) {
							logFileElement.sgVerbauListe = cfs.getSgVerbauListe();
						} else {
							testFlag=false;
							result = new Ergebnis( "CFS", "loadSgVerbauFile", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// SollverbauungRoh generieren
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgenerateandstoresollverbauungroh"),CfsString.get("cfs.running"), -1 );

				           	try {
				           		PSDZ__KIS_CALCULATION_LEVEL_old = (String)getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PS_CONFIG, "PSDZ__KIS_CALCULATION_LEVEL");
				           	} catch( Exception e ) {
				           		PSDZ__KIS_CALCULATION_LEVEL_old  = null;
				           	}
				           	getPr�flingLaufzeitUmgebung().setPruefstandVariable(VariableModes.PS_CONFIG, "PSDZ__KIS_CALCULATION_LEVEL", "VORSCHAU");
				           	// schneller als Montagefortschritt oder Gesamtflash
				           	psdz.generateAndStoreSollverbauungRoh();

				           	if (PSDZ__KIS_CALCULATION_LEVEL_old == null) {
			           			getPr�flingLaufzeitUmgebung().deletePruefstandVariable(VariableModes.PS_CONFIG, "PSDZ__KIS_CALCULATION_LEVEL");
				           	} else {
					           	getPr�flingLaufzeitUmgebung().setPruefstandVariable(VariableModes.PS_CONFIG, "PSDZ__KIS_CALCULATION_LEVEL", PSDZ__KIS_CALCULATION_LEVEL_old);
				           	}
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "generateAndStoreSollverbauungRoh", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresollverbauungroherror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "generateAndStoreSollverbauungRoh", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresollverbauungroherror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "generateAndStoreSollverbauungRoh", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresollverbauungroherror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "generateAndStoreSollverbauungRoh", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresollverbauungroherror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// SVT-IST generieren
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgenerateandstoresvtist"),CfsString.get("cfs.running"), -1 );
							psdz.generateAndStoreSVTIstCFS("", cfs.getSgVerbauListe(), false);
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "generateAndStoreSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresvtisterror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "generateAndStoreSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresvtisterror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "generateAndStoreSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresvtisterror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "generateAndStoreSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresvtisterror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// SVT-IST abholen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgetsvtist"), CfsString.get("cfs.running"), -1 );
							SVTIst = psdz.getSVTIst();
							logFileElement.SVTIst = SVTIst;
							logFileElement.programmingDependenciesListe = null;
							programmingDependenciesListe = null;
							if (SVTIst != null) {
								programmingDependenciesListe = (HashMap<String,String>)psdz.getProgDepCheckedperSVKinSVTist();
								if (programmingDependenciesListe != null) {
									if (programmingDependenciesListe.size() == 0) {
										programmingDependenciesListe = null;
									}
								}
								logFileElement.programmingDependenciesListe = programmingDependenciesListe;
							}
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtisterror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtisterror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtisterror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtisterror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// SVT-Ist gegen SGVerbauliste pr�fen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.checksvtist"), CfsString.get("cfs.running"), -1 );
							if (!cfs.checkSgVerbauSVT(true, SVTIst.getEcuList())) {
								testFlag=false;
								result = new Ergebnis( "CFS", "checkSgVerbauSVT(SVTIst)", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "checkSgVerbauSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtisterror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "checkSgVerbauSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtisterror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "checkSgVerbauSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtisterror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "checkSgVerbauSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtisterror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// Sollverbauung generieren
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgenerateandstoresollverbauung"),CfsString.get("cfs.running"), -1 );
							psdz.generateAndStoreSollverbauung();
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "generateAndStoreSollverbauung", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresollverbauungerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "generateAndStoreSollverbauung", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresollverbauungerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "generateAndStoreSollverbauung", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresollverbauungerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "generateAndStoreSollverbauung", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresollverbauungerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// SVT-Soll abholen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgetsvtsoll"), CfsString.get("cfs.running"), -1 );
							SVTSoll = psdz.getSVTSoll();
							logFileElement.SVTSoll = SVTSoll;
							List<CascadeECU> ecuList = SVTSoll.getEcuList();
							bestellNrListe = null;
							if (ecuList != null) {
								bestellNrListe = new CascadeLogistischesTeil[ecuList.size()];
								for (i=0; i<ecuList.size(); i++) {
									bestellNrListe[i] = psdz.getLogistischesTeil(cfs.hexString(((CascadeECU)ecuList.get(i)).getDiagnosticAddress().intValue(),2));
								}
							}
							logFileElement.bestellNrListe = bestellNrListe;
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getSVTSoll", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtsollerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getSVTSoll", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtsollerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getSVTSoll", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtsollerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getSVTSoll", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtsollerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// SVT-Soll gegen SGVerbauliste pr�fen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.checksvtsoll"), CfsString.get("cfs.running"), -1 );
							if (!cfs.checkSgVerbauSVT(false, SVTSoll.getEcuList())) {
								testFlag=false;
								result = new Ergebnis( "CFS", "checkSgVerbauSVT(SVTSoll)", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "checkSgVerbauSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtsollerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "checkSgVerbauSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtsollerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "checkSgVerbauSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtsollerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "checkSgVerbauSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtsollerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// Steuerdateien laden und pr�fen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.configuration"), CfsString.get("cfs.running"), -1 );
							// SG_IGNOR-Datei lesen
							if (testFlag) {
								if (cfs.loadSgIgnorFile(istIStufe, FA, SVTIst.getEcuList())) {
									logFileElement.sgIgnorListe = cfs.getSgIgnorListe();
								} else {
									testFlag=false;
									result = new Ergebnis( "CFS", "loadSgIgnorFile", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
							}
							// SG_FLASH-Datei lesen
							if (testFlag) {
								if (cfs.loadSgFlashFile(istIStufe, FA, SVTIst.getEcuList())) {
									logFileElement.sgFlashListe = cfs.getSgFlashListe();
								} else {
									testFlag=false;
									result = new Ergebnis( "CFS", "loadSgFlashFile", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
							}
							// SG_NOFLASH-Datei lesen
							if (testFlag) {
								if (cfs.loadSgNoFlashFile(istIStufe, FA, SVTIst.getEcuList())) {
									logFileElement.sgNoFlashListe = cfs.getSgNoFlashListe();
								} else {
									testFlag=false;
									result = new Ergebnis( "CFS", "loadSgNoFlashFile", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
							}
							if (testFlag) {
								if (!cfs.checkSgXxxxList()) {
									testFlag=false;
									result = new Ergebnis( "CFS", "checkSgXxxxList", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
							}
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "configuration", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtsollerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "configuration", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtsollerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "configuration", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtsollerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "configuration", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtsollerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// SVT-Ist gegen SVT-Soll
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.checksvtistsvtsoll"), CfsString.get("cfs.running"), -1 );
							if (!cfs.checkSVTSVT(SVTIst.getEcuList(),SVTSoll.getEcuList(),cfs.getIgnoreMissingEcus())) {
								testFlag=false;
								result = new Ergebnis( "CFS", "checkSVTSVT", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "checkSVTSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtsvterror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "checkSVTSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtsvterror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "checkSVTSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtsvterror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "checkSVTSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtsvterror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// TAL generieren
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgenerateandstoretal"),CfsString.get("cfs.running"), -1 );

							// PSdZ Zustand merken
							PSDZCheckProgDepsFlag=psdz.isCheckProgDeps();
							if (PSDZCheckProgDepsFlag != cfs.getCheckProgDeps()) {
								// Umschalten der PSdZ-Fehlerbehandlung
								psdz.setCheckProgDeps(cfs.getCheckProgDeps());
							}

							// Adresslisten belegen
							ecuAddressList = cfs.getSgVerbauListe();
							ecuIgnorList = cfs.getSgIgnorListe();
							ecuFlashList = cfs.getSgFlashListe();
							ecuCheckProgDepFlashList = null;

							// TAL Filter zur�cksetzen
							TALFilter = new CascadeTALFilter();
							filteredTACategory = null;

							// 1. Alles aus: Kein SG Freischalten, Individualdaten, Codieren, Hardware, Programmieren
							Object[] allCategories = CascadeTACategories.getAllCategories().toArray();
							for( i = 0; i < allCategories.length; i++ ) {
								filteredTACategory = new CascadeFilteredTACategory( (CascadeTACategories) allCategories[i]);
								filteredTACategory.setAffectAllEcus( true ); // alle
								filteredTACategory.setBlacklist( true );     // nicht
								TALFilter.addFilteredTACategory( filteredTACategory );
							}
							// 2. Jetzt die erlaubten Kategorien ein
							// Hardware Deinstall
							filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.HW_DEINSTALL);
							filteredTACategory.setAffectAllEcus( true ); // alle
							filteredTACategory.setBlacklist( false );    // muss
							TALFilter.addFilteredTACategory( filteredTACategory );
							// Hardware Install
							filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.HW_INSTALL);
							filteredTACategory.setAffectAllEcus( true ); // alle
							filteredTACategory.setBlacklist( false );    // muss
							TALFilter.addFilteredTACategory( filteredTACategory );
							// Programmieren Bootloader
							filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.BL_FLASH);
							filteredTACategory.setAffectAllEcus( true ); // alle
							filteredTACategory.setBlacklist( false );    // muss
							TALFilter.addFilteredTACategory( filteredTACategory );
							// Programmieren Software
							filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.SW_DEPLOY);
							filteredTACategory.setAffectAllEcus( true ); // alle
							filteredTACategory.setBlacklist( false );    // muss
							TALFilter.addFilteredTACategory( filteredTACategory );
							// Programmieren Betriebsanleitung
							filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.IBA_DEPLOY);
							filteredTACategory.setAffectAllEcus( true ); // alle
							filteredTACategory.setBlacklist( false );    // muss
							TALFilter.addFilteredTACategory( filteredTACategory );
							// Programmieren Gatewaytabelle
							filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.GWTB_DEPLOY);
							filteredTACategory.setAffectAllEcus( true ); // alle
							filteredTACategory.setBlacklist( false );    // muss
							TALFilter.addFilteredTACategory( filteredTACategory );
							// 3. Zu flashende SGs ein
							//    Aber nicht bei Stichprobe
							if (ecuFlashList != null && !cfs.getSamplingFlag()) {
								// Programmieren Bootloader
								if (cfs.allowBootLoaderUpdate==true) {
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.BL_FLASH);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( false );         // muss
									filteredTACategory.setForceExecution( true );     // Zwangsprogrammierung
									filteredTACategory.addEcusToList(ecuFlashList);   // zu flashende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
								}
								// Programmieren Software
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.SW_DEPLOY);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( false );         // muss
								filteredTACategory.addEcusToList(ecuFlashList);   // zu flashende SGS
								filteredTACategory.setForceExecution( true );     // Zwangsprogrammierung
								TALFilter.addFilteredTACategory( filteredTACategory );
								// Programmieren Betriebsanleitung
								if (cfs.allowIBAUpdate==true) {
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.IBA_DEPLOY);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( false );         // muss
									filteredTACategory.setForceExecution( true );     // Zwangsprogrammierung
									filteredTACategory.addEcusToList(ecuFlashList);   // zu flashende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
								}
								// Programmieren Gatewaytabelle
								if (cfs.allowGWTBUpdate==true) {
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.GWTB_DEPLOY);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( false );         // muss
									filteredTACategory.setForceExecution( true );     // Zwangsprogrammierung
									filteredTACategory.addEcusToList(ecuFlashList);   // zu flashende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
								}
							}
							// TAL erstmalig erzeugen
							psdz.generateAndStoreTAL(TALFilter,ecuAddressList,false);

							// Die SGs Zwangsprogrammieren,
							// deren CheckProgDep != 1,
							// die TAL aber f�r dieses SG leer ist
							TAL = psdz.getTAL();
							ecuCheckProgDepFlashList = cfs.getSgCheckProgDepFlashListe(TAL.getTalLines(), programmingDependenciesListe);
							// 4. Zu flashende SGs deren CheckProgDeps != 1
							if (ecuCheckProgDepFlashList != null) {
								// TAL-Filter nicht zur�cksetzen
								if (ecuFlashList != null && !cfs.getSamplingFlag()) {
									// ecuCheckProgDepFlashList + ecuFlashList zusammenh�ngen
									ecuCheckProgDepFlashList = cfs.addListe(ecuCheckProgDepFlashList,ecuFlashList);
								}
								// Programmieren Bootloader
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.BL_FLASH);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( false );         // muss
								filteredTACategory.setForceExecution( true );     // Zwangsprogrammierung
								filteredTACategory.addEcusToList(ecuCheckProgDepFlashList);   // zu flashende SGS
								TALFilter.addFilteredTACategory( filteredTACategory );
								// Programmieren Software
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.SW_DEPLOY);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( false );         // muss
								filteredTACategory.addEcusToList(ecuCheckProgDepFlashList);   // zu flashende SGS
								filteredTACategory.setForceExecution( true );     // Zwangsprogrammierung
								TALFilter.addFilteredTACategory( filteredTACategory );
								// Programmieren Betriebsanleitung
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.IBA_DEPLOY);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( false );         // muss
								filteredTACategory.setForceExecution( true );     // Zwangsprogrammierung
								filteredTACategory.addEcusToList(ecuCheckProgDepFlashList);   // zu flashende SGS
								TALFilter.addFilteredTACategory( filteredTACategory );
								// Programmieren Gatewaytabelle
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.GWTB_DEPLOY);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( false );         // muss
								filteredTACategory.setForceExecution( true );     // Zwangsprogrammierung
								filteredTACategory.addEcusToList(ecuCheckProgDepFlashList);   // zu flashende SGS
								TALFilter.addFilteredTACategory( filteredTACategory );
								// TAL nochmal neu erzeugen
								psdz.generateAndStoreTAL(TALFilter,ecuAddressList,false);
							}

							// Zu ignorierende SGs aus
							if (ecuIgnorList != null) {
								// TAL Filter zur�cksetzen
								TALFilter = new CascadeTALFilter();
								filteredTACategory = null;

								// Hardware Deinstall
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.HW_DEINSTALL);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( true );          // nicht
								filteredTACategory.addEcusToList(ecuIgnorList);   // zu ignorierende SGS
								TALFilter.addFilteredTACategory( filteredTACategory );
								// Hardware Install
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.HW_INSTALL);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( true );          // nicht
								filteredTACategory.addEcusToList(ecuIgnorList);   // zu ignorierende SGS
								TALFilter.addFilteredTACategory( filteredTACategory );
								// Programmieren Bootloader
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.BL_FLASH);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( true );          // nicht
								filteredTACategory.addEcusToList(ecuIgnorList);   // zu ignorierende SGS
								TALFilter.addFilteredTACategory( filteredTACategory );
								// Programmieren Software
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.SW_DEPLOY);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( true );          // nicht
								filteredTACategory.addEcusToList(ecuIgnorList);   // zu ignorierende SGS
								TALFilter.addFilteredTACategory( filteredTACategory );
								// Programmieren Betriebsanleitung
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.IBA_DEPLOY);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( true );          // nicht
								filteredTACategory.addEcusToList(ecuIgnorList);   // zu ignorierende SGS
								TALFilter.addFilteredTACategory( filteredTACategory );
								// Programmieren Gatewaytabelle
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.GWTB_DEPLOY);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( true );          // nicht
								filteredTACategory.addEcusToList(ecuIgnorList);   // zu ignorierende SGS
								TALFilter.addFilteredTACategory( filteredTACategory );
								// TAL weiter filtern
								psdz.filterTAL(null,TALFilter);
							}

							// Zu tauschende SGs aus
							// Aber nicht bei Stichprobe
							// TAL abholen
							TAL = psdz.getTAL();
							ecuZwangsTauschList = cfs.getSgZwangsTauschListe(TAL.getTalLines());
							if (ecuZwangsTauschList != null) {
								if (cfs.getSamplingFlag()) {
									testFlag=false;
									result = new Ergebnis( "CFS", "checkReplace", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.replacenotallowederror"), "", Ergebnis.FT_NIO );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								} else {
									// TAL Filter zur�cksetzen
									TALFilter = new CascadeTALFilter();
									filteredTACategory = null;

									// Hardware Deinstall
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.HW_DEINSTALL);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( true );          // nicht
									filteredTACategory.addEcusToList(ecuZwangsTauschList);  // zu tauschende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
									// Hardware Install
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.HW_INSTALL);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( true );          // nicht
									filteredTACategory.addEcusToList(ecuZwangsTauschList);  // zu tauschende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
									// Programmieren Bootloader
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.BL_FLASH);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( true );          // nicht
									filteredTACategory.addEcusToList(ecuZwangsTauschList);  // zu tauschende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
									// Programmieren Software
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.SW_DEPLOY);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( true );          // nicht
									filteredTACategory.addEcusToList(ecuZwangsTauschList);  // zu tauschende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
									// Programmieren Betriebsanleitung
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.IBA_DEPLOY);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( true );          // nicht
									filteredTACategory.addEcusToList(ecuZwangsTauschList);  // zu tauschende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
									// Programmieren Gatewaytabelle
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.GWTB_DEPLOY);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( true );          // nicht
									filteredTACategory.addEcusToList(ecuZwangsTauschList);  // zu tauschende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
									// TAL weiter filtern
									psdz.filterTAL(null,TALFilter);
								}
							}

							if (testFlag) {
								// TAL abholen
								TAL = psdz.getTAL();
								logFileElement.TAL = TAL;
								if (cfs.getSamplingFlag()) {
									// Stichprobe
									if (!cfs.isEmptyTAL(TAL)) {
										testFlag=false;
										result = new Ergebnis( "CFS", "isEmptyTAL", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
										ergListe.add( result );
										status = STATUS_EXECUTION_ERROR;
									} else {
										// TAL Filter zur�cksetzen
										TALFilter = new CascadeTALFilter();
										filteredTACategory = null;

										// 1. Alles aus: Kein SG Freischalten, Individualdaten, Codieren, Hardware, Programmieren
										allCategories = CascadeTACategories.getAllCategories().toArray();
										for( i = 0; i < allCategories.length; i++ ) {
											filteredTACategory = new CascadeFilteredTACategory( (CascadeTACategories) allCategories[i]);
											filteredTACategory.setAffectAllEcus( true ); // alle
											filteredTACategory.setBlacklist( true );     // nicht
											TALFilter.addFilteredTACategory( filteredTACategory );
										}
										// 2. Jetzt die erlaubten Kategorien ein
										// Hardware Deinstall
										filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.HW_DEINSTALL);
										filteredTACategory.setAffectAllEcus( true ); // alle
										filteredTACategory.setBlacklist( false );    // muss
										TALFilter.addFilteredTACategory( filteredTACategory );
										// Hardware Install
										filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.HW_INSTALL);
										filteredTACategory.setAffectAllEcus( true ); // alle
										filteredTACategory.setBlacklist( false );    // muss
										TALFilter.addFilteredTACategory( filteredTACategory );
										// Programmieren Bootloader
										filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.BL_FLASH);
										filteredTACategory.setAffectAllEcus( true ); // alle
										filteredTACategory.setBlacklist( false );    // muss
										if (cfs.allowBootLoaderUpdate==true) {
											filteredTACategory.setForceExecution( true ); // Zwangsprogrammierung
										}
										TALFilter.addFilteredTACategory( filteredTACategory );
										// Programmieren Software
										filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.SW_DEPLOY);
										filteredTACategory.setAffectAllEcus( true ); // alle
										filteredTACategory.setBlacklist( false );    // muss
										filteredTACategory.setForceExecution( true );// Zwangsprogrammierung
										TALFilter.addFilteredTACategory( filteredTACategory );
										// Programmieren Betriebsanleitung
										filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.IBA_DEPLOY);
										filteredTACategory.setAffectAllEcus( true ); // alle
										filteredTACategory.setBlacklist( false );    // muss
										if (cfs.allowIBAUpdate==true) {
											filteredTACategory.setForceExecution( true ); // Zwangsprogrammierung
										}
										TALFilter.addFilteredTACategory( filteredTACategory );
										// Programmieren Gatewaytabelle
										filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.GWTB_DEPLOY);
										filteredTACategory.setAffectAllEcus( true ); // alle
										filteredTACategory.setBlacklist( false );    // muss
										if (cfs.allowGWTBUpdate==true) {
											filteredTACategory.setForceExecution( true ); // Zwangsprogrammierung
										}
										TALFilter.addFilteredTACategory( filteredTACategory );
										// TAL erstmalig erzeugen
										psdz.generateAndStoreTAL(TALFilter,ecuAddressList,false);

										// Zu ignorierende SGs aus
										if (ecuIgnorList != null) {
											// TAL Filter zur�cksetzen
											TALFilter = new CascadeTALFilter();
											filteredTACategory = null;

											// Hardware Deinstall
											filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.HW_DEINSTALL);
											filteredTACategory.setAffectAllEcus( false );     // nicht alle
											filteredTACategory.setBlacklist( true );          // nicht
											filteredTACategory.addEcusToList(ecuIgnorList);   // zu ignorierende SGS
											TALFilter.addFilteredTACategory( filteredTACategory );
											// Hardware Install
											filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.HW_INSTALL);
											filteredTACategory.setAffectAllEcus( false );     // nicht alle
											filteredTACategory.setBlacklist( true );          // nicht
											filteredTACategory.addEcusToList(ecuIgnorList);   // zu ignorierende SGS
											TALFilter.addFilteredTACategory( filteredTACategory );
											// Programmieren Bootloader
											filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.BL_FLASH);
											filteredTACategory.setAffectAllEcus( false );     // nicht alle
											filteredTACategory.setBlacklist( true );          // nicht
											filteredTACategory.addEcusToList(ecuIgnorList);   // zu ignorierende SGS
											TALFilter.addFilteredTACategory( filteredTACategory );
											// Programmieren Software
											filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.SW_DEPLOY);
											filteredTACategory.setAffectAllEcus( false );     // nicht alle
											filteredTACategory.setBlacklist( true );          // nicht
											filteredTACategory.addEcusToList(ecuIgnorList);   // zu ignorierende SGS
											TALFilter.addFilteredTACategory( filteredTACategory );
											// Programmieren Betriebsanleitung
											filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.IBA_DEPLOY);
											filteredTACategory.setAffectAllEcus( false );     // nicht alle
											filteredTACategory.setBlacklist( true );          // nicht
											filteredTACategory.addEcusToList(ecuIgnorList);   // zu ignorierende SGS
											TALFilter.addFilteredTACategory( filteredTACategory );
											// Programmieren Gatewaytabelle
											filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.GWTB_DEPLOY);
											filteredTACategory.setAffectAllEcus( false );     // nicht alle
											filteredTACategory.setBlacklist( true );          // nicht
											filteredTACategory.addEcusToList(ecuIgnorList);   // zu ignorierende SGS
											TALFilter.addFilteredTACategory( filteredTACategory );
											// TAL weiter filtern
											psdz.filterTAL(null,TALFilter);
										}
										// TAL abholen
										TAL = psdz.getTAL();
										logFileElement.TAL = TAL;
									}
								}
							}

							// PSdZ Zustand wieder zur�ckstellen
							if (PSDZCheckProgDepsFlag != cfs.getCheckProgDeps()) {
								// Umschalten der PSdZ-Fehlerbehandlung
								psdz.setCheckProgDeps(PSDZCheckProgDepsFlag);
							}

							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "generateAndStoreTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoretalerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "generateAndStoreTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoretalerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "generateAndStoreTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoretalerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "generateAndStoreTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoretalerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// TAL pr�fen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.checktal"), CfsString.get("cfs.running"), -1 );
							if (cfs.checkFzgTAL(TAL)) {
								ecuEinbauList = cfs.getSgEinbauListe();
								ecuAusbauList = cfs.getSgAusbauListe();
								ecuTauschList = cfs.getSgTauschListe();
								temp = "";
								String temp2=null;
								String temp3=null;

								// Tausch nicht bei Stichprobe
								if (ecuTauschList!=null && ecuTauschList.length>0 && cfs.getSamplingFlag()) {
									testFlag=false;
									if (ecuTauschList.length == 1) {
										temp=temp+CfsString.get("cfs.msgerrorreplace1")+NL;
									} else {
										temp=temp+CfsString.get("cfs.msgerrorreplace2")+NL;
									}
									for (i=0; i<ecuTauschList.length; i++) {
										temp2=null;
										temp3=null;
										if (bestellNrListe != null) {
											for (k=0; k<bestellNrListe.length; k++) {
												if (bestellNrListe[k] != null) {
													if (cfs.hexString(bestellNrListe[k].getDiagnosticAddress().longValue(),2).equals(ecuTauschList[i])) {
														temp2=bestellNrListe[k].getBaseVariantName();
														temp3=bestellNrListe[k].getSachNrBestellbaresTeilTAIS();
														break;
													}
												}
											}
										}
										if (temp2 == null || temp2.length()==0) temp2=cfs.UNKNOWN;
										if (temp3 == null || temp3.length()==0) temp3=cfs.UNKNOWN;
										temp = temp + temp2+ " " + ecuTauschList[i] + "  "+ temp3 + NL;
									}
								}

								if (ecuAusbauList!=null && ecuAusbauList.length>0) {
									testFlag=false;
									if (ecuAusbauList.length == 1) {
										temp=temp+CfsString.get("cfs.msgerrordeinstall1")+NL;
									} else {
										temp=temp+CfsString.get("cfs.msgerrordeinstall2")+NL;
									}
									for (i=0; i<ecuAusbauList.length; i++) {
										temp2=null;
										temp3=null;
										if (bestellNrListe != null) {
											for (k=0; k<bestellNrListe.length; k++) {
												if (bestellNrListe[k] != null) {
													if (cfs.hexString(bestellNrListe[k].getDiagnosticAddress().longValue(),2).equals(ecuAusbauList[i])) {
														temp2=bestellNrListe[k].getBaseVariantName();
														temp3=bestellNrListe[k].getSachNrBestellbaresTeilTAIS();
														break;
													}
												}
											}
										}
										if (temp2 == null || temp2.length()==0) temp2=cfs.UNKNOWN;
										if (temp3 == null || temp3.length()==0) temp3=cfs.UNKNOWN;
										temp = temp + temp2+ " " + ecuAusbauList[i] + "  "+ temp3 + NL;
									}
								}
								if (!cfs.getIgnoreMissingEcus()) {
									if (ecuEinbauList!=null && ecuEinbauList.length>0) {
										testFlag=false;
										if (ecuEinbauList.length == 1) {
											temp=temp+CfsString.get("cfs.msgerrorinstall1")+NL;
										} else {
											temp=temp+CfsString.get("cfs.msgerrorinstall2")+NL;
										}
										for (i=0; i<ecuEinbauList.length; i++) {
											temp2=null;
											temp3=null;
											if (bestellNrListe != null) {
												for (k=0; k<bestellNrListe.length; k++) {
													if (bestellNrListe[k] != null) {
														if (cfs.hexString(bestellNrListe[k].getDiagnosticAddress().longValue(),2).equals(ecuEinbauList[i])) {
															temp2=bestellNrListe[k].getBaseVariantName();
															temp3=bestellNrListe[k].getSachNrBestellbaresTeilTAIS();
															break;
														}
													}
												}
											}
											if (temp2 == null || temp2.length()==0) temp2=cfs.UNKNOWN;
											if (temp3 == null || temp3.length()==0) temp3=cfs.UNKNOWN;
											temp = temp + temp2+ " " + ecuEinbauList[i] + "  "+ temp3 + NL;
										}
									}
								}
								if (!testFlag) {
									testFlag=false;
									result = new Ergebnis( "CFS", "checkTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.unexpectedinstalldeinstallerror"), temp, Ergebnis.FT_NIO );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
							} else {
								testFlag=false;
								result = new Ergebnis( "CFS", "checkTAL", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "checkTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checktalerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "checkTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checktalerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "checkTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checktalerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "checkTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checktalerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// TAL weiter filtern
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgenerateandstoretal"),CfsString.get("cfs.running"), -1 );

							// Adresslisten belegen
							ecuTauschList = cfs.getSgTauschListe();
							ecuEinbauList = cfs.getSgEinbauListe();

							// Zu tauschende SGs aus
							if (ecuTauschList!=null && ecuTauschList.length>0) {
								// TAL Filter zur�cksetzen
								TALFilter = new CascadeTALFilter();
								filteredTACategory = null;

								// Hardware Deinstall
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.HW_DEINSTALL);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( true );          // nicht
								filteredTACategory.addEcusToList(ecuTauschList);  // zu tauschende SGS
								TALFilter.addFilteredTACategory( filteredTACategory );
								// Hardware Install
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.HW_INSTALL);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( true );          // nicht
								filteredTACategory.addEcusToList(ecuTauschList);  // zu tauschende SGS
								TALFilter.addFilteredTACategory( filteredTACategory );
								// Programmieren Bootloader
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.BL_FLASH);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( true );          // nicht
								filteredTACategory.addEcusToList(ecuTauschList);  // zu tauschende SGS
								TALFilter.addFilteredTACategory( filteredTACategory );
								// Programmieren Software
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.SW_DEPLOY);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( true );          // nicht
								filteredTACategory.addEcusToList(ecuTauschList);  // zu tauschende SGS
								TALFilter.addFilteredTACategory( filteredTACategory );
								// Programmieren Betriebsanleitung
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.IBA_DEPLOY);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( true );          // nicht
								filteredTACategory.addEcusToList(ecuTauschList);  // zu tauschende SGS
								TALFilter.addFilteredTACategory( filteredTACategory );
								// Programmieren Gatewaytabelle
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.GWTB_DEPLOY);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( true );          // nicht
								filteredTACategory.addEcusToList(ecuTauschList);  // zu tauschende SGS
								TALFilter.addFilteredTACategory( filteredTACategory );
								// TAL weiter filtern
								psdz.filterTAL(null,TALFilter);
							}
							if (cfs.getIgnoreMissingEcus()) {
								// Fehlende SGs aus
								if (ecuEinbauList!=null && ecuEinbauList.length>0) {
									// TAL Filter zur�cksetzen
									TALFilter = new CascadeTALFilter();
									filteredTACategory = null;

									// Hardware Deinstall
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.HW_DEINSTALL);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( true );          // nicht
									filteredTACategory.addEcusToList(ecuEinbauList);  // fehlende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
									// Hardware Install
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.HW_INSTALL);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( true );          // nicht
									filteredTACategory.addEcusToList(ecuEinbauList);  // fehlende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
									// Programmieren Bootloader
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.BL_FLASH);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( true );          // nicht
									filteredTACategory.addEcusToList(ecuEinbauList);  // fehlende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
									// Programmieren Software
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.SW_DEPLOY);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( true );          // nicht
									filteredTACategory.addEcusToList(ecuEinbauList);  // fehlende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
									// Programmieren Betriebsanleitung
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.IBA_DEPLOY);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( true );          // nicht
									filteredTACategory.addEcusToList(ecuEinbauList);  // fehlende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
									// Programmieren Gatewaytabelle
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.GWTB_DEPLOY);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( true );          // nicht
									filteredTACategory.addEcusToList(ecuEinbauList);  // fehlende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
									// TAL weiter filtern
									psdz.filterTAL(null,TALFilter);
								}
							}
							// TAL abholen
							TAL = psdz.getTAL();
							logFileElement.TAL = TAL;
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "generateAndStoreTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoretalerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "generateAndStoreTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoretalerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "generateAndStoreTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoretalerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "generateAndStoreTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoretalerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (logFileElement != null) {
						// Log speichern
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.savefzglog"), CfsString.get("cfs.running"), -1 );
							if (!cfs.saveFzgLog(logFileElement,false)) {
								testFlag=false;
								result = new Ergebnis( "CFS", "saveFzgLog", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "saveFzgLog", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.savetraceerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "saveFzgLog", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.savetraceerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "saveFzgLog", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.savetraceerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "saveFzgLog", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.savetraceerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// Benutzeranzeige
						boolean viewFlag = false;
						temp = null;
						String temp2=null;
						String temp3=null;
						String temp4=null;
						CascadeECU ecu = null;
						List<CascadeECU> svk = null;
						int t=0;
						Date date = new Date();
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yy HH:mm");

						temp=cfs.logString(CfsString.get("cfs.vin"),4)+" "+cfs.logString(vehicleVinFzg,17)+cfs.logString("",6)+simpleDateFormat.format(date)+NL;
						temp=temp+CfsString.get("cfs.ilevel")+" "+cfs.getSollIStufe()+NL;
						zusatzMessage="";

						ecuProgrammList = cfs.getSgProgrammListe();
						if (ecuProgrammList != null && ecuProgrammList.length > 0) {
							viewFlag = true;
							temp=temp+""+NL;
							if (ecuProgrammList.length == 1)
								temp=temp+CfsString.get("cfs.msgflash1")+NL;
							else
								temp=temp+CfsString.get("cfs.msgflash2")+NL;
							if (SVTSoll != null)
								svk = SVTSoll.getEcuList();
							for (i=0; i<ecuProgrammList.length; i++) {
								temp2=null;
								temp3=null;
								temp4=null;
								if (svk != null) {
									for (k=0; k<svk.size(); k++) {
										ecu = (CascadeECU)svk.get(k);
										if (ecu != null) {
											if (cfs.hexString(ecu.getDiagnosticAddress().longValue(),2).equals(ecuProgrammList[i])) {
												temp2 = ecu.getBaseVariantName();
												if (bestellNrListe != null) {
													if (k<bestellNrListe.length) {
														if (bestellNrListe[k] != null) {
															temp3 = bestellNrListe[k].getSachNrBestellbaresTeilTAIS();
															temp4 = bestellNrListe[k].getBaseVariantName();
														}
													}
												}
												break;
											}
										}
									}
								}
								if (temp2 == null || temp2.length()==0) temp2=cfs.UNKNOWN;
								if (temp3 == null || temp3.length()==0) temp3=cfs.UNKNOWN;
								if (temp4 == null || temp4.length()==0) temp4=cfs.UNKNOWN;
								if (temp3.equalsIgnoreCase(cfs.UNKNOWN)) temp3=""; else temp3 = "  " + temp3;
								if (temp2.equalsIgnoreCase(temp4) || temp4.equalsIgnoreCase(cfs.UNKNOWN) )
									temp = temp + cfs.logString(temp2,14)+ " " + ecuProgrammList[i] + temp3 + NL;
								else
									temp = temp + cfs.logString(temp2+"("+temp4+")",14)+ " " + ecuProgrammList[i] + temp3 + NL;
							}
						}

						ecuTauschList = cfs.getSgTauschListe();
						if (ecuTauschList != null && ecuTauschList.length > 0) {
							zusatzMessage =  CfsString.get("cfs.replace");
							viewFlag = true;
							temp=temp+""+NL;
							if (ecuTauschList.length == 1)
								temp=temp+CfsString.get("cfs.msgreplace1")+NL;
							else
								temp=temp+CfsString.get("cfs.msgreplace2")+NL;
							if (SVTSoll != null)
								svk = SVTSoll.getEcuList();
							for (i=0; i<ecuTauschList.length; i++) {
								temp2=null;
								temp3=null;
								temp4=null;
								if (svk != null) {
									for (k=0; k<svk.size(); k++) {
										ecu = (CascadeECU)svk.get(k);
										if (ecu != null) {
											if (cfs.hexString(ecu.getDiagnosticAddress().longValue(),2).equals(ecuTauschList[i])) {
												temp2 = ecu.getBaseVariantName();
												if (bestellNrListe != null) {
													if (k<bestellNrListe.length) {
														if (bestellNrListe[k] != null) {
															temp3 = bestellNrListe[k].getSachNrBestellbaresTeilTAIS();
															temp4 = bestellNrListe[k].getBaseVariantName();
														}
													}
												}
												break;
											}
										}
									}
								}
								if (temp2 == null || temp2.length()==0) temp2=cfs.UNKNOWN;
								if (temp3 == null || temp3.length()==0) temp3=cfs.UNKNOWN;
								if (temp4 == null || temp4.length()==0) temp4=cfs.UNKNOWN;
								zusatzMessage = zusatzMessage + " " + temp2 + "," + ecuTauschList[i] + "," + temp3;
								if (temp3.equalsIgnoreCase(cfs.UNKNOWN)) temp3=""; else temp3 = "  " + temp3;
								if (temp2.equalsIgnoreCase(temp4) || temp4.equalsIgnoreCase(cfs.UNKNOWN) )
									temp = temp + cfs.logString(temp2,14)+ " " + ecuTauschList[i] + temp3 + NL;
								else
									temp = temp + cfs.logString(temp2+"("+temp4+")",14)+ " " + ecuTauschList[i] + temp3 + NL;
							}
						}

						ecuZwangsTauschList = cfs.getSgZwangsTauschListe(null);
						if (ecuZwangsTauschList != null && ecuZwangsTauschList.length > 0) {
							if (zusatzMessage.length()==0) zusatzMessage = CfsString.get("cfs.replace");
							viewFlag = true;
							temp=temp+""+NL;
							if (ecuZwangsTauschList.length == 1)
								temp=temp+CfsString.get("cfs.msgmustreplace1")+NL;
							else
								temp=temp+CfsString.get("cfs.msgmustreplace2")+NL;
							if (SVTSoll != null)
								svk = SVTSoll.getEcuList();
							for (i=0; i<ecuZwangsTauschList.length; i++) {
								temp2=null;
								temp3=null;
								temp4=null;
								if (svk != null) {
									for (k=0; k<svk.size(); k++) {
										ecu = (CascadeECU)svk.get(k);
										if (ecu != null) {
											if (cfs.hexString(ecu.getDiagnosticAddress().longValue(),2).equals(ecuZwangsTauschList[i])) {
												temp2 = ecu.getBaseVariantName();
												if (bestellNrListe != null) {
													if (k<bestellNrListe.length) {
														if (bestellNrListe[k] != null) {
															temp3 = bestellNrListe[k].getSachNrBestellbaresTeilTAIS();
															temp4 = bestellNrListe[k].getBaseVariantName();
														}
													}
												}
												break;
											}
										}
									}
								}
								if (temp2 == null || temp2.length()==0) temp2=cfs.UNKNOWN;
								if (temp3 == null || temp3.length()==0) temp3=cfs.UNKNOWN;
								if (temp4 == null || temp4.length()==0) temp4=cfs.UNKNOWN;
								zusatzMessage = zusatzMessage + " " + temp2 + "," + ecuZwangsTauschList[i] + "," + temp3;
								if (temp3.equalsIgnoreCase(cfs.UNKNOWN)) temp3=""; else temp3 = "  " + temp3;
								if (temp2.equalsIgnoreCase(temp4) || temp4.equalsIgnoreCase(cfs.UNKNOWN) )
									temp = temp + cfs.logString(temp2,14)+ " " + ecuZwangsTauschList[i] + temp3 + NL;
								else
									temp = temp + cfs.logString(temp2+"("+temp4+")",14)+ " " + ecuZwangsTauschList[i] + temp3 + NL;
							}
						}

						ecuIgnorList = cfs.getSgIgnorListeSVT(SVTIst.getEcuList(),SVTSoll.getEcuList());
						if (ecuIgnorList != null && ecuIgnorList.length > 0) {
							if (zusatzMessage.length()>0) zusatzMessage = zusatzMessage + " ";
							zusatzMessage = zusatzMessage + CfsString.get("cfs.ignored");
							// viewFlag = true;
							temp=temp+""+NL;
							if (ecuIgnorList.length == 1)
								temp=temp+CfsString.get("cfs.msgignore1")+NL;
							else
								temp=temp+CfsString.get("cfs.msgignore2")+NL;
							if (SVTSoll != null)
								svk = SVTSoll.getEcuList();
							for (i=0; i<ecuIgnorList.length; i++) {
								temp2=null;
								temp3=null;
								temp4=null;
								if (svk != null) {
									for (k=0; k<svk.size(); k++) {
										ecu = (CascadeECU)svk.get(k);
										if (ecu != null) {
											if (cfs.hexString(ecu.getDiagnosticAddress().longValue(),2).equals(ecuIgnorList[i])) {
												temp2 = ecu.getBaseVariantName();
												if (bestellNrListe != null) {
													if (k<bestellNrListe.length) {
														if (bestellNrListe[k] != null) {
															temp3 = bestellNrListe[k].getSachNrBestellbaresTeilTAIS();
															temp4 = bestellNrListe[k].getBaseVariantName();
														}
													}
												}
												break;
											}
										}
									}
								}
								if (temp2 == null || temp2.length()==0) temp2=cfs.UNKNOWN;
								if (temp3 == null || temp3.length()==0) temp3=cfs.UNKNOWN;
								if (temp4 == null || temp4.length()==0) temp4=cfs.UNKNOWN;
								zusatzMessage = zusatzMessage + " " + temp2 + "," + ecuIgnorList[i] + "," + temp3;
								if (temp3.equalsIgnoreCase(cfs.UNKNOWN)) temp3=""; else temp3 = "  " + temp3;
								if (temp2.equalsIgnoreCase(temp4) || temp4.equalsIgnoreCase(cfs.UNKNOWN) )
									temp = temp + cfs.logString(temp2,14)+ " " + ecuIgnorList[i] + temp3 + NL;
								else
									temp = temp + cfs.logString(temp2+"("+temp4+")",14)+ " " + ecuIgnorList[i] + temp3 + NL;
							}
						}

						if (cfs.getIgnoreMissingEcus()) {
							ecuEinbauList = cfs.getSgEinbauListe();
							if (ecuEinbauList != null && ecuEinbauList.length > 0) {
								zusatzMessage =  CfsString.get("cfs.missing");
								viewFlag = true;
								temp=temp+""+NL;
								if (ecuEinbauList.length == 1)
									temp=temp+CfsString.get("cfs.msgmissing1")+NL;
								else
									temp=temp+CfsString.get("cfs.msgmissing2")+NL;
								if (SVTSoll != null)
									svk = SVTSoll.getEcuList();
								for (i=0; i<ecuEinbauList.length; i++) {
									temp2=null;
									temp3=null;
									temp4=null;
									if (svk != null) {
										for (k=0; k<svk.size(); k++) {
											ecu = (CascadeECU)svk.get(k);
											if (ecu != null) {
												if (cfs.hexString(ecu.getDiagnosticAddress().longValue(),2).equals(ecuEinbauList[i])) {
													temp2 = ecu.getBaseVariantName();
													if (bestellNrListe != null) {
														if (k<bestellNrListe.length) {
															if (bestellNrListe[k] != null) {
																temp3 = bestellNrListe[k].getSachNrBestellbaresTeilTAIS();
																temp4 = bestellNrListe[k].getBaseVariantName();
															}
														}
													}
													break;
												}
											}
										}
									}
									if (temp2 == null || temp2.length()==0) temp2=cfs.UNKNOWN;
									if (temp3 == null || temp3.length()==0) temp3=cfs.UNKNOWN;
									if (temp4 == null || temp4.length()==0) temp4=cfs.UNKNOWN;
									zusatzMessage = zusatzMessage + " " + temp2 + "," + ecuEinbauList[i] + "," + temp3;
									if (temp3.equalsIgnoreCase(cfs.UNKNOWN)) temp3=""; else temp3 = "  " + temp3;
									if (temp2.equalsIgnoreCase(temp4) || temp4.equalsIgnoreCase(cfs.UNKNOWN) )
										temp = temp + cfs.logString(temp2,14)+ " " + ecuEinbauList[i] + temp3 + NL;
									else
										temp = temp + cfs.logString(temp2+"("+temp4+")",14)+ " " + ecuEinbauList[i] + temp3 + NL;
								}
							}
						}

						if (viewFlag) {
							if( signalLamp != null ) {
								signalLamp.YellowLightOn();
							}
							if (cfs.getAutomaticMode())
								t = AUTOMATIC_MESSAGE_TIMEOUT;
							else
								t = 0;
							cfs.stopStopWatch();
							do {
								userDialog.reset();
								if (printerFlag)
									userDialog.setAuxiliaryButtonVisibilityMask(UserDialog.OK_BUTTON_MASK | UserDialog.CANCEL_BUTTON_MASK | UserDialog.CONTINUE_BUTTON_MASK | UserDialog.PRINT_BUTTON_MASK);
								else
									userDialog.setAuxiliaryButtonVisibilityMask(UserDialog.OK_BUTTON_MASK | UserDialog.CANCEL_BUTTON_MASK | UserDialog.CONTINUE_BUTTON_MASK);
								userDialog.setContinueButtonText(CfsString.get("cfs.copy"));
								cfs.initDialog(userDialog,true,false);
								userDialog.displayMessage( CfsString.get("cfs.flashstart"), temp, t );
								if (userDialog.getLastKey() == UserDialog.CONTINUE_KEY)
									cfs.copyToClipboard(CfsString.get("cfs.flashstart")+LF+temp);
								if (t!=0) break;
							} while (userDialog.getLastKey() == UserDialog.CONTINUE_KEY);
							if( signalLamp != null ) {
								signalLamp.LightsOff();
							}
							if (userDialog.isCancelled()) {
								userBreakFlag = true;
								testFlag = false;
								result = new Ergebnis( "CFS", "flashstart", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.userbreakerror"), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
							cfs.continueStopWatch();
							userDialog.reset();
						}
					}

					if (testFlag) {
						// TraceElement belegen
						traceFileElement = new TraceFileElement();
						traceFileElement.timeMillisGesamt = System.currentTimeMillis();
						traceFileElement.timeMillisVorbereitungStart = System.currentTimeMillis();
						traceFileElement.timeMillisVorbereitungFertig = 0;
						traceFileElement.timeMillisNachbereitungStart = 0;
						traceFileElement.timeMillisNachbereitungFertig = 0;
						traceFileElement.iStufe=cfs.getSollIStufe();
						traceFileElement.zusatzMessage = zusatzMessage;
						traceFileElement.vin=vehicleVinFzg;
						traceFileElement.TAL=TAL;
						if (TAL != null && TAL.getTalLines() != null) {
							n = TAL.getTalLines().length;
							if (n > 0) {
								traceFileElement.timeValidTalLine = new boolean[n];
								traceFileElement.timeMillisTalLine = new long[n];
								traceFileElement.lieferUmfangNummerTalLine = new String[n];
								for (i=0; i<n; i++) {
									traceFileElement.timeValidTalLine[i] = false;
									traceFileElement.timeMillisTalLine[i] = traceFileElement.timeMillisVorbereitungStart;
									CascadeLogistischesTeil ptr = (psdz.getLogistischesTeil(cfs.hexString(TAL.getTalLines()[i].getDiagnosticAddress().intValue(),2)));
									if (ptr != null)
										traceFileElement.lieferUmfangNummerTalLine[i] = ptr.getSachNrBestellbaresTeilTAIS();
									else
										traceFileElement.lieferUmfangNummerTalLine[i] = null;
								}
							}
						}
					}

					if (testFlag) {
						// TAL abarbeiten
						java.util.Timer myTimer = new java.util.Timer();
						try {
							if (!cfs.isEmptyTAL(TAL)) {
								cfs.initDialog(userDialog,false,true);
								userDialog.displayMessage( CfsString.get("cfs.psdzstarttalexecution"),CfsString.get("cfs.running")+cfs.getStopWatch(), -1 );
								StopWatchTask  myStopWatchTask = null;
								myStopWatchTask = new StopWatchTask(CfsString.get("cfs.psdzstarttalexecution"), CfsString.get("cfs.running"), userDialog, cfs, true);
								myTimer.schedule( myStopWatchTask, 0, PSDZ_STATUS_REFRESH_TIME);
								psdz.startTALExecution( cfs.getParallelExecution(), cfs.getMaxErrorRepeat(), true ); // mit writeSvtSollToVCM
								myTimer.cancel();
								userDialog.reset();
							}
						} catch ( Exception e ) {
							myTimer.cancel();
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "startTALExecution", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzstarttalexecutionerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "startTALExecution", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzstarttalexecutionerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							myTimer.cancel();
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "startTALExecution", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzstarttalexecutionerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "startTALExecution", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzstarttalexecutionerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// Warten bis TAL abgearbeitet
						try {
							if (!cfs.isEmptyTAL(TAL)) {
								String message="";
								String message1="";
								String message2="";
								String message3="";
								String message4="";
								boolean errorFlag=false;
								boolean errorFlag2=false;
								message=CfsString.get("cfs.running")+cfs.getStopWatch();
								// Laufende Abfrage
								while( true ) {
									userDialog.setUserInteractionEnabled( false );
									if (userDialog.getLastKey() == UserDialog.CONTINUE_KEY)
										cfs.copyToClipboard(CfsString.get("cfs.gettalexecutionstatus")+LF+message);

									if( userDialog.isCancelled() == true ) {
										cfs.initDialog(userDialog,false,true);
										userDialog.displayAlertMessage( CfsString.get("cfs.psdzcanceltalexecution"),CfsString.get("cfs.running")+cfs.getStopWatch(), -1 );
										java.util.Timer myTimer = new java.util.Timer();
										try {
											StopWatchTask  myStopWatchTask = null;
											myStopWatchTask = new StopWatchTask(CfsString.get("cfs.psdzcanceltalexecution"), CfsString.get("cfs.running"), userDialog, cfs, false);
											myTimer.schedule( myStopWatchTask, 0, PSDZ_STATUS_REFRESH_TIME);
											psdz.cancelTALExecution();
											myTimer.cancel();
										} catch ( Exception e ) {
											myTimer.cancel();
										} catch( Throwable e ) {
											myTimer.cancel();
										}
										userBreakFlag = true;
										testFlag=false;
										result = new Ergebnis( "PSdZ", "getTALExecutionStatus", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.userbreakerror"), "", Ergebnis.FT_NIO );
										ergListe.add( result );
										status = STATUS_EXECUTION_ERROR;
										userDialog.reset();
										break;
									}

									userDialog.setAuxiliaryButtonVisibilityMask( UserDialog.CANCEL_BUTTON_MASK | UserDialog.CONTINUE_BUTTON_MASK );
									userDialog.setContinueButtonText(CfsString.get("cfs.copy"));
									cfs.initDialog(userDialog,false,false);
									if (!errorFlag2)
										userDialog.displayMessage(CfsString.get("cfs.gettalexecutionstatus"),message, -1 );
									else
										userDialog.displayAlertMessage(CfsString.get("cfs.gettalexecutionstatus"),message, -1 );

									CascadeTAL TALExecutionStatus = psdz.getTALExecutionStatus(PSDZ_STATUS_REFRESH_TIME, null);
									CascadeTALLine[] TALLine = TALExecutionStatus.getTalLines();
									temp = "";
									cfs.setProgressMap(psdz.getProgressMap());
									message = CfsString.get("cfs.running") + cfs.getStopWatch() + NL + cfs.getProgressMessage() + NL;
									message1 = "";
									message2 = "";
									message3 = "";
									message4 = "";
									errorFlag = false;
									errorFlag2 = false;
									for (i=0; i<TALLine.length; i++) {
										temp = cfs.logString(TALLine[i].getBaseVariantName(),12) + " " + cfs.hexString(TALLine[i].getDiagnosticAddress().longValue(),2);
										if ( TALLine[i].getTALTACategories().length > 0 )
											temp = temp + " " + (TALLine[i].getTALTACategories()[0].getCategory().toString()+"  ").substring(0,2).toUpperCase();
										else
											temp = temp + " ??"; // sollte nie kommen
										if ((TALLine[i].getStatus()==CascadeExecutionStatus.EXECUTABLE) ||
											(TALLine[i].getStatus()==CascadeExecutionStatus.PROCESSING)) {
											n = 0;
											m = 0;
											errorFlag=false;
											for( j = 0; j < TALLine[i].getTALTACategories().length; j++ ) {
												if ((TALLine[i].getTALTACategories()[j].getCategory() == CascadeTACategories.BL_FLASH) ||
													(TALLine[i].getTALTACategories()[j].getCategory() == CascadeTACategories.SW_DEPLOY) ||
													(TALLine[i].getTALTACategories()[j].getCategory() == CascadeTACategories.IBA_DEPLOY)||
													(TALLine[i].getTALTACategories()[j].getCategory() == CascadeTACategories.GWTB_DEPLOY)) {
													n=n+TALLine[i].getTALTACategories()[j].getTAs().length;
													for( k = 0; k < TALLine[i].getTALTACategories()[j].getTAs().length; k++ ) {
														if (TALLine[i].getTALTACategories()[j].getTAs()[k].getStatus() == CascadeExecutionStatus.PROCESSING ) m++;
														if (TALLine[i].getTALTACategories()[j].getTAs()[k].getStatus() == CascadeExecutionStatus.PROCESSED ) m++;
														if (TALLine[i].getTALTACategories()[j].getTAs()[k].getStatus() == CascadeExecutionStatus.PROCESSEDWITHERROR ) {
															m++;
															errorFlag=true;
															errorFlag2=true;
														}
													}
												}
											}
											if (m==0) {
												if (!traceFileElement.timeValidTalLine[i])
													traceFileElement.timeMillisTalLine[i] = System.currentTimeMillis(); // noch nicht angefangen
											} else {
												if (traceFileElement.timeMillisVorbereitungFertig == 0) traceFileElement.timeMillisVorbereitungFertig = System.currentTimeMillis(); // Vorbereitung beendet
											}
											traceFileElement.timeMillisNachbereitungStart = System.currentTimeMillis(); // Nachbereitung noch nicht angefangen
											temp = temp + " " + (m<10?" ":"") + m + "/" + n + (n<10?" ":"") + cfs.getProgressBar(cfs.hexString(TALLine[i].getDiagnosticAddress().longValue(),2),TALLine[i].getTALTACategories()[0].getCategory());
											if (errorFlag) temp = temp + " n.i.O.";
											if (temp.indexOf("%") > 0)
												message1 = message1 + temp + NL; // oben in Arbeit
											else
												message2 = message2 + temp + NL; // dann noch nicht in Arbeit
										} else {
											// fertig
											if (!traceFileElement.timeValidTalLine[i]) {
												traceFileElement.timeValidTalLine[i] = true;
												traceFileElement.timeMillisTalLine[i] = System.currentTimeMillis() - traceFileElement.timeMillisTalLine[i];
											}
											if (TALLine[i].getStatus()==CascadeExecutionStatus.PROCESSED) {
												message4 = message4 + temp + "   i.O." + NL; // Am Schluss fertig io
											} else {
												errorFlag2=true;
												if ((TALLine[i].getStatus()==CascadeExecutionStatus.PROCESSEDWITHERROR)||(TALLine[i].getStatus()==CascadeExecutionStatus.NOTEXECUTABLE)) {
													message3 = message3 + temp + " n.i.O." + NL; // dann fertig nio
												} else {
													message3 = message3 + temp + " "+TALLine[i].getStatus().toString() + NL;
												}
											}
										}
									}
									message = message + message1 + message2 + message3 + message4;

									if (!TALExecutionStatus.getStatus().getName().equals( CascadeTALExecutionStatus.RUNNING.getName())) {
										if (!TALExecutionStatus.getStatus().getName().equals( CascadeTALExecutionStatus.FINISHED.getName())) {
											testFlag=false;
											mapEcuNIO.clear();
											mapEcuNotExecutable.clear();
											for (i=0; i<TALLine.length; i++) {
												// BasisName des SG ermitteln ("ECU:'Basisname' 0x'hexcode'")
												String sgIDName = PB.getString( "psdz.SG" ) + ":" + TALLine[i].getBaseVariantName() + " 0x" + cfs.hexString(TALLine[i].getDiagnosticAddress().longValue(),2) ;
												for( j = 0; j < TALLine[i].getTALTACategories().length; j++ ) {
													if (TALLine[i].getStatus()==CascadeExecutionStatus.PROCESSEDWITHERROR) {
														HashSet<String> setTaSwtNIO = new HashSet<String>();
														// pr�fe ob schon eingetragen
														if (mapEcuNIO.containsKey(sgIDName)) setTaSwtNIO = (HashSet<String>)mapEcuNIO.get(sgIDName);
														setTaSwtNIO.add(TALLine[i].getTALTACategories()[j].getCategory().toString());
														mapEcuNIO.put(sgIDName,setTaSwtNIO);
													}
													if (TALLine[i].getStatus()==CascadeExecutionStatus.NOTEXECUTABLE) {
														HashSet<String> setTaSwtNotExecutable = new HashSet<String>();
														// pr�fe ob schon eingetragen
														if (mapEcuNotExecutable.containsKey(sgIDName)) setTaSwtNotExecutable = (HashSet<String>)mapEcuNotExecutable.get(sgIDName);
														setTaSwtNotExecutable.add(TALLine[i].getTALTACategories()[j].getCategory().toString());
														mapEcuNotExecutable.put(sgIDName,setTaSwtNotExecutable);
													}
												}
											}
											if ((mapEcuNIO.size() > 0) || (mapEcuNotExecutable.size() > 0)) {
												// PSdZ hole die aufgelaufenen Events
												HashMap<String, List<Exception>> psdzErrors = new HashMap<String, List<Exception>>( buildExcHashMap(TALExecutionStatus) );
												HashMap<String, String> psdzECUStatisticsHexStr = new HashMap<String, String>( psdz.getEcuStatisticsHexString() );
												// Fehlereintr�ge anlegen
												entryECUsFT_NIO( ergListe, mapEcuNIO, mapEcuNotExecutable, psdzErrors, psdzECUStatisticsHexStr, psdz );
												// Alle relevanten Fehler aus der TAL interpretiert
											} else {
												result = new Ergebnis( "PSdZ", "getTALExecutionStatus", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.gettalexecutionstatuserror"), TALExecutionStatus.getStatus().getName(), Ergebnis.FT_NIO );
												ergListe.add( result );
											}
											status = STATUS_EXECUTION_ERROR;
										}
										TALExecutionStatus = psdz.getTALExecutionStatus( 0, "STATUS");
										if (TALExecutionStatus != null)
											traceFileElement.TAL=TALExecutionStatus;
										break;
									}
								}
								userDialog.reset();
							}
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getTALExecutionStatus", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.gettalexecutionstatuserror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getTALExecutionStatus", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.gettalexecutionstatuserror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getTALExecutionStatus", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.gettalexecutionstatuserror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getTALExecutionStatus", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.gettalexecutionstatuserror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
						// TraceElement belegen
						if (traceFileElement.timeMillisNachbereitungStart != 0) traceFileElement.timeMillisNachbereitungFertig = System.currentTimeMillis(); // Nachbereitung beendet
						traceFileElement.timeMillisGesamt = System.currentTimeMillis() - traceFileElement.timeMillisGesamt;
					}

					if (cfs.getVerifySVTFlag()) {
						if (testFlag) {
							// SVT-IST-Neu generieren
							try {
								userDialog.setDisplayProgress( true );
								userDialog.getProgressbar().setIndeterminate( true );
								userDialog.displayMessage( CfsString.get("cfs.psdzgenerateandstoresvtistneu"),CfsString.get("cfs.running"), -1 );
								psdz.generateAndStoreSVTIstCFS("_NEU", cfs.getSgVerbauListe(), false);
								userDialog.reset();
							} catch ( Exception e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "PSdZ", "generateAndStoreSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresvtisterror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "PSdZ", "generateAndStoreSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresvtisterror"), "Exception", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							} catch( Throwable e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "PSdZ", "generateAndStoreSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresvtisterror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "PSdZ", "generateAndStoreSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresvtisterror"), "Throwable", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						}

						if (testFlag) {
							// SVT-IST-Neu zur Kontrolle abholen
							try {
								userDialog.setDisplayProgress( true );
								userDialog.getProgressbar().setIndeterminate( true );
								userDialog.displayMessage( CfsString.get("cfs.psdzgetsvtistneu"), CfsString.get("cfs.running"), -1 );
								SVTIstNeu = psdz.getSVTIst();
								logFileElement.SVTIstNeu = SVTIstNeu;
								logFileElement.programmingDependenciesListeNeu = null;
								programmingDependenciesListeNeu = null;
								if (SVTIstNeu != null) {
									programmingDependenciesListeNeu = (HashMap<String, String>)psdz.getProgDepCheckedperSVKinSVTist();
									if (programmingDependenciesListeNeu != null) {
										if (programmingDependenciesListeNeu.size() == 0) {
											programmingDependenciesListeNeu = null;
										}
									}
								}
								logFileElement.programmingDependenciesListeNeu = programmingDependenciesListeNeu;
								userDialog.reset();
							} catch ( Exception e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "PSdZ", "getSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtistneuerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "PSdZ", "getSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtistneuerror"), "Exception", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							} catch( Throwable e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "PSdZ", "getSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtistneuerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "PSdZ", "getSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtistneuerror"), "Throwable", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						}

						if (testFlag) {
							// SVT-Ist-Neu gegen SVT-Soll pr�fen
							try {
								userDialog.setDisplayProgress( true );
								userDialog.getProgressbar().setIndeterminate( true );
								userDialog.displayMessage( CfsString.get("cfs.verifysvt"), CfsString.get("cfs.running"), -1 );
								if (cfs.isDebug(cfs.debugVerifySVT)) {
									userDialog.displayMessage( CfsString.get("cfs.debug"), "verifySVT(SVTIstNeu,SVTSoll)", AUTOMATIC_MESSAGE_TIMEOUT );
								} else {
									if (!cfs.verifySVTFzg(SVTIstNeu.getEcuList(),SVTSoll.getEcuList(),cfs.getIgnoreMissingEcus())) {
										testFlag=false;
										result = new Ergebnis( "CFS", "verifySVT(SVTIstNeu,SVTSoll)", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
										ergListe.add( result );
										status = STATUS_EXECUTION_ERROR;
									}
								}
								userDialog.reset();
							} catch ( Exception e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "CFS", "verifySVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.verifysvterror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "CFS", "verifySVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.verifysvterror"), "Exception", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							} catch( Throwable e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "CFS", "verifySVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.verifysvterror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "CFS", "verifySVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.verifysvterror"), "Throwable", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						}

						if (logFileElement != null) {
							// Log speichern
							try {
								userDialog.setDisplayProgress( true );
								userDialog.getProgressbar().setIndeterminate( true );
								userDialog.displayMessage( CfsString.get("cfs.savefzglog"), CfsString.get("cfs.running"), -1 );
								if (!cfs.saveFzgLog(logFileElement,true)) {
									testFlag=false;
									result = new Ergebnis( "CFS", "saveFzgLog", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
								userDialog.reset();
							} catch ( Exception e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "CFS", "saveFzgLog", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.savetraceerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "CFS", "saveFzgLog", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.savetraceerror"), "Exception", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							} catch( Throwable e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "CFS", "saveFzgLog", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.savetraceerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "CFS", "saveFzgLog", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.savetraceerror"), "Throwable", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						}
					}

					if (cfs.getIgnoreMissingEcus()) {
						// fehlende ECUs
						ecuEinbauList = cfs.getSgEinbauListe();
						temp = "";
						String temp2=null;
						String temp3=null;

						if (ecuEinbauList!=null && ecuEinbauList.length>0) {
							testFlag=false;
							if (ecuEinbauList.length == 1) {
								temp=temp+CfsString.get("cfs.msgerrormissing1")+NL;
							} else {
								temp=temp+CfsString.get("cfs.msgerrormissing2")+NL;
							}
							for (i=0; i<ecuEinbauList.length; i++) {
								temp2=null;
								temp3=null;
								if (bestellNrListe != null) {
									for (k=0; k<bestellNrListe.length; k++) {
										if (bestellNrListe[k] != null) {
											if (cfs.hexString(bestellNrListe[k].getDiagnosticAddress().longValue(),2).equals(ecuEinbauList[i])) {
												temp2=bestellNrListe[k].getBaseVariantName();
												temp3=bestellNrListe[k].getSachNrBestellbaresTeilTAIS();
												break;
											}
										}
									}
								}
								if (temp2 == null || temp2.length()==0) temp2=cfs.UNKNOWN;
								if (temp3 == null || temp3.length()==0) temp3=cfs.UNKNOWN;
								temp = temp + temp2+ " " + ecuEinbauList[i] + "  "+ temp3 + NL;
							}
							testFlag=false;
							result = new Ergebnis( "CFS", "checkMissingEcus", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.missingecuerror"), temp, Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// I-Stufe ins Fahrzeug schreiben
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzupdateistep"), CfsString.get("cfs.running"), -1 );
							if (cfs.isDebug(cfs.debugUpdateIStepinVCMorBackup)) {
								userDialog.displayMessage( CfsString.get("cfs.debug"), "updateIStepinVCMorBackup", AUTOMATIC_MESSAGE_TIMEOUT );
							} else {
								// Sonderbehandlung: RR1, RR2, RR3 Serie II hat kein Backup
								// Sonderbehandlung: M12 hat kein Backup
								if (cfs.normalizeBaureihe(FA.getBaureihe()).equalsIgnoreCase("RR1") ||
									cfs.normalizeBaureihe(FA.getBaureihe()).equalsIgnoreCase("RR2") ||
									cfs.normalizeBaureihe(FA.getBaureihe()).equalsIgnoreCase("RR3") ||
									cfs.normalizeBaureihe(FA.getBaureihe()).equalsIgnoreCase("M12")) {
									psdz.updateIStepinVCMorBackup(false); // ohne Backup
								} else {
									psdz.updateIStepinVCMorBackup(true);  // mit Backup
								}
							}
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "updateIStepinVCMorBackup", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzupdateistepeerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "updateIStepinVCMorBackup", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzupdateistepeerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "updateIStepinVCMorBackup", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzupdateistepeerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "updateIStepinVCMorBackup", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzupdateistepeerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (traceFileElement != null) {
						// Trace speichern
						try {
							// TraceElement belegen
							traceFileElement.okayFlag = testFlag;
							traceFileElement.abbruchFlag = userBreakFlag;
							if (!cfs.saveFzgTrace(traceFileElement, ergListe)) {
								testFlag=false;
								result = new Ergebnis( "CFS", "saveTrace", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						} catch ( Exception e ) {
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "saveTrace", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.savetraceerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "saveTrace", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.savetraceerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "saveTrace", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.savetraceerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "saveTrace", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.savetraceerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}
// Bookmark Gesamtfahrzeug Ende
				} else {
// Bookmark Trailer Anfang
					if (testFlag) {
						// Diagnoseinterfaces abfragen
						if (!deviceParams[0].equalsIgnoreCase("ENET")) {
							testFlag=false;
							result = new Ergebnis( "PSdZ", "getDeviceParams", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetdeviceparamsinterfacenotsupportederror"), deviceParams[0]+ " <> ENET" , Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						}
					}
					// Trailer-Konfigurations-Datei lesen
					if (testFlag) {
						if (!cfs.loadEcuListFile()) {
							testFlag=false;
							result = new Ergebnis( "CFS", "loadEcuListFile", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						}
					}

					initFlag=false;
					if (testFlag) {
						// PSdZ Initialisierung aufrufen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzinit"),CfsString.get("cfs.running"), -1 );
							psdz.setTestScreenID(testScreenID);
							psdz.init();
							userDialog.reset();
							initFlag=true;
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "init", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdziniterror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "init", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdziniterror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "init", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdziniterror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "init", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdziniterror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						}
					}

					if (testFlag) {
						// Ziel-Istufe festlegen
						psdz.setIStufeSoll(cfs.getSollIStufe());
					}

					if (testFlag) {
						// PSdZ B2V Gateways abfragen und zuordnen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgetvehiclevinsfromb2v"), CfsString.get("cfs.running"), -1 );
							String vehicleVinsListe[]=null;
							vehicleVinsListe = psdz.getVehicleVinsFromB2V();
							userDialog.reset();
							boolean gefunden = false;
							for (i=0; i<vehicleVinsListe.length; i++) {
								if ((cfs.getInstanz()==1) && (vehicleVinsListe[i].equals(cfs.getZgwVin1()+ZGW_EXTENSION))) {
									vehicleVinB2v=vehicleVinsListe[i].substring(0,17);
									gefunden=true;
									break;
								}
								if ((cfs.getInstanz()==2) && (vehicleVinsListe[i].equals(cfs.getZgwVin2()+ZGW_EXTENSION))) {
									vehicleVinB2v=vehicleVinsListe[i].substring(0,17);
									gefunden=true;
									break;
								}
							}
							if ((!gefunden) && (cfs.getInstanz()==1)) {
								String errormessage=CfsString.get("cfs.tomuchzgwfounderror") + NL;
								for (i=0; i<vehicleVinsListe.length; i++) {
									if ((!vehicleVinsListe[i].equals(cfs.getZgwVin1()+ZGW_EXTENSION)) && (!vehicleVinsListe[i].equals(cfs.getZgwVin2()+ZGW_EXTENSION))) {
										errormessage=errormessage + vehicleVinsListe[i].substring(0,17) + NL;
										if (!gefunden) {
											// erstes Gateway gefunden, muss noch kein Fehler sein
											gefunden=true;
											vehicleVinB2v=vehicleVinsListe[i].substring(0,17);
										} else {
											// n�chstes Gateway gefunden, jetzt ist es ein Fehler
											testFlag=false;
										}
									}
								}
								if (!testFlag) {
									result = new Ergebnis( "PSdZ", "getVehicleVinsFromB2V", "", "", "", "", "", "", "", "0", "", "", "", errormessage, "", Ergebnis.FT_NIO );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
							}
							if (!gefunden) {
								testFlag=false;
								result = new Ergebnis( "PSdZ", "getVehicleVinsFromB2V", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.nozgwfounderror"), CfsString.get("cfs.nozgwfoundhelp"), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getVehicleVinsFromB2V", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetvehiclevinsfromb2verror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getVehicleVinsFromB2V", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetvehiclevinsfromb2verror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getVehicleVinsFromB2V", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetvehiclevinsfromb2verror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getVehicleVinsFromB2V", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetvehiclevinsfromb2verror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
						if (testFlag) {
		  					psdz.setVehicleVin(vehicleVinB2v);
						}
					}

					connectionFlag=false;
					if (testFlag) {
						// Verbindung zum Fahrzeug herstellen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzopenvehicleconnection"),temp + cfs.getSollIStufe() + NL + CfsString.get("cfs.running"), -1 );
							connectionFlag=true;
							psdz.openVehicleConnectionByIStep();
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "openVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzopenvehicleconnectionerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "openVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzopenvehicleconnectionerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "openVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzopenvehicleconnectionerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "openVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzopenvehicleconnectionerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// SVT-IST generieren
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgenerateandstoresvtist"),CfsString.get("cfs.running"), -1 );
							psdz.generateAndStoreSVTIstCFS("", null, false);
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "generateAndStoreSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresvtisterror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "generateAndStoreSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresvtisterror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "generateAndStoreSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresvtisterror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "generateAndStoreSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresvtisterror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// SVT-IST abholen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgetsvtist"), CfsString.get("cfs.running"), -1 );
							SVTIst = psdz.getSVTIst();
							SVTIstXML = "";
							programmingDependenciesListe = null;
							if (SVTIst != null) {
								SVTIstXML = psdz.getPSdZSVTistAsXML();
								programmingDependenciesListe = (HashMap<String, String>)psdz.getProgDepCheckedperSVKinSVTist();
								if (programmingDependenciesListe != null) {
									if (programmingDependenciesListe.size() == 0) {
										programmingDependenciesListe = null;
									}
								}
							}
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtisterror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtisterror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtisterror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtisterror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// SVT-Ist gegen Trailer-Konfiguration pr�fen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.checksvtist"), CfsString.get("cfs.running"), -1 );
							if (!cfs.checkTrailerSVT(true, SVTIst.getEcuList(), programmingDependenciesListe)) {
								testFlag=false;
								result = new Ergebnis( "CFS", "checkSVT(SVTIst)", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "checkSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtisterror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "checkSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtisterror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "checkSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtisterror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "checkSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtisterror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// Lieferumfangsnummern holen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgenerateandstoreecusuccessors"), CfsString.get("cfs.running"), -1 );
							ecuListe = cfs.getEcuListe();
							userDialog.reset();
							if (ecuListe == null) {
								testFlag=false;
								result = new Ergebnis( "Cfs", "getEcuListe", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.noeculisteerror"), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							} else {
								if (ecuListe.length == 0) {
									testFlag=false;
									result = new Ergebnis( "Cfs", "getEcuListe", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.emptyeculisteerror"), "", Ergebnis.FT_NIO );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
							}
							if (testFlag) {
								SVKListeTrailer = new ArrayList<CascadeSVTReader>();
								boolean saveFlag=false;
								userBreakFlag = false;
								for (i=0; i<ecuListe.length; i++) {
									if (userBreakFlag) break;
									ecuListe[i].lieferUmfangNummer = new String[ecuListe[i].address.length];
									ecuListe[i].sgbmIdAnlieferzustand = new String[ecuListe[i].address.length];
									for (k=0; k<ecuListe[i].address.length; k++) {
										if (userBreakFlag) break;
										userDialog.setDisplayProgress( true );
										userDialog.getProgressbar().setIndeterminate( true );
										userDialog.displayMessage( CfsString.get("cfs.psdzgenerateandstoreecusuccessors"), CfsString.get("cfs.running")+ NL + "ECU=" + ecuListe[i].name + NL + " ADR=" + cfs.hexString(ecuListe[i].address[k],2) , -1 );
										try {
											psdz.generateAndStoreEcuSuccessors(ecuListe[i].address[k]);
											Hashtable<String, Object> ht=null;
											Enumeration<String> en=null;
											String[] lieferUmfangNummerListe=null;
											ecuListe[i].lieferUmfangNummer[k]="";
											testFlagTemp = true;
											ht=psdz.getEcuSuccessors();
											if (ht == null) {
												testFlag=false;
												testFlagTemp=false;
												result = new Ergebnis( "PSdZ", "generateAndStoreEcuSuccessors", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoreecusuccessorserror"), "ECU="+ecuListe[i].name + " ADR=" + cfs.hexString(ecuListe[i].address[k],2)+NL+ CfsString.get("cfs.psdzgenerateandstoreecusuccessorsnohashtableerror"), Ergebnis.FT_NIO_SYS );
												ergListe.add( result );
												status = STATUS_EXECUTION_ERROR;
											} else {
												if (ht.size() == 0) {
													testFlag=false;
													testFlagTemp=false;
													result = new Ergebnis( "PSdZ", "generateAndStoreEcuSuccessors", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoreecusuccessorserror"), "ECU="+ecuListe[i].name + " ADR=" + cfs.hexString(ecuListe[i].address[k],2)+NL+ CfsString.get("cfs.psdzgenerateandstoreecusuccessorsemptyhashtableerror"), Ergebnis.FT_NIO );
													ergListe.add( result );
													status = STATUS_EXECUTION_ERROR;
												}
											}
											if (testFlagTemp) {
												int index[]=null;
												String[] tempListe = new String[ht.size()];
												en = ht.keys();
												n=0;

												while ( en.hasMoreElements() ) {
													tempListe[n] = (String) en.nextElement();
													if (!tempListe[n].contains("_INFO")) { // Zusatzinfos ausfiltern
														if (!cfs.isValidLieferUmfangNummer(tempListe[n])) {
															testFlag=false;
															testFlagTemp=false;
															result = new Ergebnis( "PSdZ", "generateAndStoreEcuSuccessors", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoreecusuccessorserror"), "ECU="+ecuListe[i].name + " ADR=" + cfs.hexString(ecuListe[i].address[k],2)+ NL + "="+tempListe[n]+ NL + CfsString.get("cfs.psdzgenerateandstoreecusuccessorsinvalidlunerror"), Ergebnis.FT_NIO_SYS );
															ergListe.add( result );
															status = STATUS_EXECUTION_ERROR;
														}
														n++;
													}
												}
												lieferUmfangNummerListe = new String[n];
												for (n=0; n<lieferUmfangNummerListe.length; n++) {
													lieferUmfangNummerListe[n]=tempListe[n];
												}
												String[] lieferUmfangNummerListeInfo = new String[n];
												for (n=0; n<lieferUmfangNummerListeInfo.length; n++) {
													temp = (String)ht.get(lieferUmfangNummerListe[n]+"_INFO"); // Info abholen
													if (temp == null) temp="";
													temp = temp.trim();
													if (temp.length()>0) temp = "  "+ temp;
													lieferUmfangNummerListeInfo[n]=lieferUmfangNummerListe[n] + temp;
												}

												if (testFlagTemp) {
													boolean gefunden=false;
													for (n=0; n<lieferUmfangNummerListe.length; n++) {
														if (cfs.isStoredLieferUmfangNummer(cfs.hexString(ecuListe[i].address[k],2),lieferUmfangNummerListe[n],cfs.getTrailerMultiSelectFlag())) {
															ecuListe[i].lieferUmfangNummer[k]=lieferUmfangNummerListe[n];
															gefunden=true;
															break;
														}
													}
													if (!gefunden) {
														boolean selectedFlag=true;
														if( signalLamp != null ) {
															signalLamp.YellowLightOn();
														}
														cfs.stopStopWatch();
														do {
															selectedFlag=true;
															userDialog.reset();
															userDialog.setAllowCancel( true );
															userDialog.setAuxiliaryButtonVisibilityMask(UserDialog.OK_BUTTON_MASK | UserDialog.CANCEL_BUTTON_MASK | UserDialog.CONTINUE_BUTTON_MASK);
															userDialog.setContinueButtonText(CfsString.get("cfs.oknosave"));
															userDialog.setBackgroundColor(cfs.backColor);
															index=userDialog.requestUserInputSingleChoice( "ECU="+ecuListe[i].name + " ADR=" + cfs.hexString(ecuListe[i].address[k],2) , CfsString.get("cfs.selectonelieferumfangnummer"), 0, lieferUmfangNummerListeInfo );
															if (!userDialog.isCancelled()) {
																if ((index != null) && (index.length == 1)) {
																	ecuListe[i].lieferUmfangNummer[k]=lieferUmfangNummerListe[index[0]];
																	if (userDialog.getLastKey()== UserDialog.OK_KEY) {
																		if (cfs.storeLieferUmfangNummer(cfs.hexString(ecuListe[i].address[k],2),ecuListe[i].lieferUmfangNummer[k], cfs.getTrailerMultiSelectFlag())) {
																			saveFlag=true;
																		} else {
																			testFlag=false;
																			testFlagTemp=false;
																			result = new Ergebnis( "PSdZ", "generateAndStoreEcuSuccessors", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoreecusuccessorserror"), "ECU="+ecuListe[i].name + " ADR=" + cfs.hexString(ecuListe[i].address[k],2)+ NL + "NR="+ecuListe[i].lieferUmfangNummer[k] + NL + CfsString.get("cfs.psdzgenerateandstoreecusuccessorsstorelunerror"), Ergebnis.FT_NIO_SYS );
																			ergListe.add( result );
																			status = STATUS_EXECUTION_ERROR;
																		}
																	}
																} else {
																	selectedFlag=false;
																	if (lieferUmfangNummerListe.length == 0) break;
																	userDialog.reset();
																	userDialog.setBackgroundColor(cfs.backColor);
																	userDialog.displayMessage( CfsString.get("cfs.selectwarning"),CfsString.get("cfs.selectonelieferumfangnummer") , AUTOMATIC_MESSAGE_TIMEOUT );
																}
															} else {
																userBreakFlag = true;
																testFlagTemp=false;
																testFlag=false;
																result = new Ergebnis( "PSdZ", "generateAndStoreEcuSuccessors", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.userbreakerror"), "ECU="+ecuListe[i].name + " ADR=" + cfs.hexString(ecuListe[i].address[k],2), Ergebnis.FT_NIO );
																ergListe.add( result );
																status = STATUS_EXECUTION_ERROR;
															}
														} while (!selectedFlag);
														cfs.continueStopWatch();
														userDialog.reset();
														if( signalLamp != null ) {
															signalLamp.LightsOff();
														}
													}
													if (testFlagTemp) {
														SVKTrailer = (CascadeSVTReader)(ht.get(ecuListe[i].lieferUmfangNummer[k]));
														if (cfs.getTrailerKisVerifyFlag()) {
															// Aufgrund KIS-Fehler kann es dazu kommen, dass nach der Programmierung noch einmal programmiert werden soll.
															// Es wird gepr�ft ob die neue SVK nicht weiter programmiert wird.
															try {
																psdz.setPSdZSVTist(SVKTrailer.getPsdzSVTasXML());
																psdz.generateAndStoreEcuSuccessors(ecuListe[i].address[k]);
																SVKTrailerVerify = (CascadeSVTReader)(psdz.getEcuSuccessors().get(ecuListe[i].lieferUmfangNummer[k]));
																psdz.setPSdZSVTist(SVTIstXML);
																// Jetzt m�sste SVKTrailer und SVKTrailerVerify identisch sein
																if (!cfs.verifySVKTrailer(SVKTrailer.getCascadeSVT().getEcuList().get(0), SVKTrailerVerify.getCascadeSVT().getEcuList().get(0))) {
																	// Warnung Doppelprogrammierung
																	// LOG anlegen
																	String[] psdzDeviceParams = psdz.getDeviceParams();
																	if (psdzDeviceParams != null) {
																		if (psdzDeviceParams.length > 3 ? Boolean.valueOf(psdzDeviceParams[3]).booleanValue() : false) {
																			psdz.logPSdZContent("KIS_SVKSOLL_1_" + ecuListe[i].lieferUmfangNummer[k] , SVKTrailer.getPsdzSVTasXML());
																			psdz.logPSdZContent("KIS_SVKSOLL_2_" + ecuListe[i].lieferUmfangNummer[k] , SVKTrailerVerify.getPsdzSVTasXML());
																		}
																	}
																	if( signalLamp != null ) {
																		signalLamp.YellowLightOn();
																	}
																	cfs.stopStopWatch();
																	userDialog.reset();
																	userDialog.setAuxiliaryButtonVisibilityMask(UserDialog.OK_BUTTON_MASK);
																	userDialog.setBackgroundColor(cfs.backColor);
																	userDialog.setMessageColumns(38);
																	userDialog.setMessageBoxRows(7);
																	userDialog.pack();
																	userDialog.displayMessage( CfsString.get("cfs.kisverifywarning") ,CfsString.get("cfs.kisverifymessage") , cfs.getAutomaticMode() ? AUTOMATIC_MESSAGE_TIMEOUT : 0 );
																	cfs.continueStopWatch();
																	if( signalLamp != null ) {
																		signalLamp.LightsOff();
																	}
																	SVKTrailer = SVKTrailerVerify;
																}
															} catch (Exception e) {
																// keine Fehlermeldung wenn nicht verf�gbar
															}
															// SVK f�r die Lieferumfangsnummer in Liste eintragen
															SVKListeTrailer.add(SVKTrailer);
														}
													}
												}
												if (testFlagTemp) {
													if (cfs.getTrailerModeAnlieferzustandFlag()) {
														ecuListe[i].sgbmIdAnlieferzustand[k] = cfs.getStoredSGBMID(ecuListe[i].lieferUmfangNummer[k]);
														if (ecuListe[i].sgbmIdAnlieferzustand[k] == null) {
															String[] sgbmIdListe = null;
															sgbmIdListe = cfs.getSGBMIDListe(SVKTrailer.getCascadeSVT().getEcuList());
															if (sgbmIdListe == null) {
																testFlagTemp=false;
																testFlag=false;
																result = new Ergebnis( "PSdZ" , "generateAndStoreEcuSuccessors", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.nosgbmidlisteerror"), "", Ergebnis.FT_NIO );
																ergListe.add( result );
																status = STATUS_EXECUTION_ERROR;
															}
															if (testFlagTemp) {
																boolean selectedFlag=true;
																boolean AutomaticFindDataSgbmidFlag=false;
																long[] sgbmIdSizeListe = null;
																sgbmIdSizeListe = cfs.getSGBMIDSizeListe(sgbmIdListe);

																if (cfs.getAutomaticFindDataSgbmid()) {
																	if (sgbmIdSizeListe != null && sgbmIdSizeListe.length > 0) {
																		// Suche Datenstand
																		ecuListe[i].sgbmIdAnlieferzustand[k] = cfs.getDataSgbmId(sgbmIdSizeListe,sgbmIdListe);
																		if (ecuListe[i].sgbmIdAnlieferzustand[k] != null) {
																			AutomaticFindDataSgbmidFlag=true;
																			if (saveFlag) {
																				if (!cfs.storeSGBMID(ecuListe[i].lieferUmfangNummer[k],ecuListe[i].sgbmIdAnlieferzustand[k])) {
																					testFlag=false;
																					testFlagTemp=false;
																					result = new Ergebnis( "PSdZ", "generateAndStoreEcuSuccessors", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoreecusuccessorserror"), "ECU="+ecuListe[i].name + " ADR=" + cfs.hexString(ecuListe[i].address[k],2)+ NL + "NR="+ecuListe[i].lieferUmfangNummer[k] + NL + CfsString.get("cfs.psdzgenerateandstoreecusuccessorsstorelunerror"), Ergebnis.FT_NIO_SYS );
																					ergListe.add( result );
																					status = STATUS_EXECUTION_ERROR;
																				}
																			}
																		}
																	}
																}

																if (testFlagTemp) {
																	if (!AutomaticFindDataSgbmidFlag) {
																		// Keine automatische Auswahl m�glich
																		// Userabfrage
																		if (ecuListe[i].sgbmIdAnlieferzustand[k] == null) {
																			if( signalLamp != null ) {
																				signalLamp.YellowLightOn();
																			}
																			cfs.stopStopWatch();
																			do {
																				selectedFlag=true;
																				userDialog.reset();
																				userDialog.setAllowCancel( true );
																				userDialog.setAuxiliaryButtonVisibilityMask(UserDialog.OK_BUTTON_MASK | UserDialog.CANCEL_BUTTON_MASK | UserDialog.CONTINUE_BUTTON_MASK);
																				userDialog.setContinueButtonText(CfsString.get("cfs.oknosave"));
																				userDialog.setBackgroundColor(cfs.backColor);
																				index=userDialog.requestUserInputSingleChoice( "ECU="+ecuListe[i].name + " ADR=" + cfs.hexString(ecuListe[i].address[k],2) , CfsString.get("cfs.selectoneSGBMID"), 0, sgbmIdListe );
																				if (!userDialog.isCancelled()) {
																					if ((index != null) && (index.length == 1)) {
																						if (sgbmIdListe[index[0]].startsWith("SWFL")) {
																							ecuListe[i].sgbmIdAnlieferzustand[k]=sgbmIdListe[index[0]].substring(0,25);
																							if (userDialog.getLastKey()== UserDialog.OK_KEY) {
																								if (cfs.storeSGBMID(ecuListe[i].lieferUmfangNummer[k],ecuListe[i].sgbmIdAnlieferzustand[k])) {
																									saveFlag=true;
																								} else {
																									testFlag=false;
																									testFlagTemp=false;
																									result = new Ergebnis( "PSdZ", "generateAndStoreEcuSuccessors", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoreecusuccessorserror"), "ECU="+ecuListe[i].name + " ADR=" + cfs.hexString(ecuListe[i].address[k],2)+ NL + "NR="+ecuListe[i].lieferUmfangNummer[k] + NL + CfsString.get("cfs.psdzgenerateandstoreecusuccessorsstorelunerror"), Ergebnis.FT_NIO_SYS );
																									ergListe.add( result );
																									status = STATUS_EXECUTION_ERROR;
																								}
																							}
																						} else {
																							selectedFlag=false;
																							if (sgbmIdListe.length == 0) break;
																							userDialog.reset();
																							userDialog.setBackgroundColor(cfs.backColor);
																							userDialog.displayMessage( CfsString.get("cfs.selectwarning"),CfsString.get("cfs.invalidselectedSGBMID") , AUTOMATIC_MESSAGE_TIMEOUT );
																						}

																					} else {
																						selectedFlag=false;
																						if (sgbmIdListe.length == 0) break;
																						userDialog.reset();
																						userDialog.setBackgroundColor(cfs.backColor);
																						userDialog.displayMessage( CfsString.get("cfs.selectwarning"),CfsString.get("cfs.selectoneSGBMID") , AUTOMATIC_MESSAGE_TIMEOUT );
																					}
																				} else {
																					userBreakFlag = true;
																					testFlagTemp=false;
																					testFlag=false;
																					result = new Ergebnis( "PSdZ", "generateAndStoreEcuSuccessors", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.userbreakerror"), "ECU="+ecuListe[i].name + " ADR=" + cfs.hexString(ecuListe[i].address[k],2), Ergebnis.FT_NIO );
																					ergListe.add( result );
																					status = STATUS_EXECUTION_ERROR;
																				}
																			} while (!selectedFlag);
																			cfs.continueStopWatch();
																			userDialog.reset();
																			if( signalLamp != null ) {
																				signalLamp.LightsOff();
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										} catch ( Exception e ) {
											testFlag=false;
											result = new Ergebnis( "PSdZ", "generateAndStoreEcuSuccessors", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoreecusuccessorserror"), "ECU="+ecuListe[i].name + " ADR=" + cfs.hexString(ecuListe[i].address[k],2)+NL+"Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
											ergListe.add( result );
											status = STATUS_EXECUTION_ERROR;
										}
									}
								}
								if (testFlag) {
									if (saveFlag) {
										if (!cfs.saveEcuListFile()) {
											testFlag=false;
											result = new Ergebnis( "CFS", "saveEcuListFile", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO_SYS );
											ergListe.add( result );
											status = STATUS_EXECUTION_ERROR;
										}
									}
								}
							}
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "generateAndStoreEcuSuccessors", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoreecusuccessorserror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "generateAndStoreEcuSuccessors", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoreecusuccessorserror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "generateAndStoreEcuSuccessors", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoreecusuccessorserror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "generateAndStoreEcuSuccessors", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoreecusuccessorserror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// Benutzeranzeige
						temp = null;
						String temp2=null;
						int t=0;

						temp="ECU            ADR NR"+NL;
						for (i=0; i<ecuListe.length; i++) {
							temp2=null;
							for (k=0; k<ecuListe[i].address.length; k++) {
								if (temp2==null) temp2=ecuListe[i].variantName[k];
								if (temp2==null) temp2=ecuListe[i].baseVariantName[k];
								if (temp2==null) temp2=ecuListe[i].name;
								if (temp2==null) temp2="?";
								temp = temp + cfs.logString(temp2,14) + " " + cfs.hexString(ecuListe[i].address[k],2) + "  " + ecuListe[i].lieferUmfangNummer[k];
								if (cfs.isValidSgbmId(ecuListe[i].sgbmIdAnlieferzustand[k])) temp = temp + "  " + CfsString.get("cfs.anlieferzustand");
								temp = temp + NL;
							}
						}

						if( signalLamp != null ) {
							signalLamp.YellowLightOn();
						}
						if (cfs.getAutomaticMode())
							t = AUTOMATIC_MESSAGE_TIMEOUT;
						else
							t = 0;
						cfs.stopStopWatch();
						do {
							userDialog.reset();
							if (printerFlag)
								userDialog.setAuxiliaryButtonVisibilityMask(UserDialog.OK_BUTTON_MASK | UserDialog.CANCEL_BUTTON_MASK | UserDialog.CONTINUE_BUTTON_MASK | UserDialog.PRINT_BUTTON_MASK);
							else
								userDialog.setAuxiliaryButtonVisibilityMask(UserDialog.OK_BUTTON_MASK | UserDialog.CANCEL_BUTTON_MASK | UserDialog.CONTINUE_BUTTON_MASK);
							userDialog.setContinueButtonText(CfsString.get("cfs.copy"));
							cfs.initDialog(userDialog,true,false);
							userDialog.displayMessage( CfsString.get("cfs.flashstart"), temp, t );
							if (userDialog.getLastKey() == UserDialog.CONTINUE_KEY)
								cfs.copyToClipboard(CfsString.get("cfs.flashstart")+LF+temp);
							else
								if (t!=0) break;
						} while (userDialog.getLastKey() == UserDialog.CONTINUE_KEY);
						if( signalLamp != null ) {
							signalLamp.LightsOff();
						}
						if (userDialog.isCancelled()) {
							userBreakFlag = true;
							testFlag = false;
							result = new Ergebnis( "CFS", "flashstart", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.userbreakerror"), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
						cfs.continueStopWatch();
						userDialog.reset();
					}

					if (testFlag) {
						// SVT-Soll erzeugen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzsetsvtsoll"), CfsString.get("cfs.running"), -1 );
							if (SVKListeTrailer == null) {
								testFlag=false;
								result = new Ergebnis( "Cfs", "setSVTSoll", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.nosvklisteerror"), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							} else {
								if (SVKListeTrailer.size() == 0) {
									testFlag=false;
									result = new Ergebnis( "Cfs", "setSVTSoll", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.emptysvklisteerror"), "", Ergebnis.FT_NIO );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
							}
							if (testFlag) {
								psdz.setSVTSoll(SVKListeTrailer);
							}
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "setSVTSoll", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzsetsvtsollerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "setSVTSoll", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzsetsvtsollerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "setSVTSoll", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzsetsvtsollerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "setSVTSoll", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzsetsvtsollerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// SVT-Soll abholen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgetsvtsoll"), CfsString.get("cfs.running"), -1 );
							SVTSoll = psdz.getSVTSoll();
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getSVTSoll", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtsollerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getSVTSoll", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtsollerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getSVTSoll", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtsollerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getSVTSoll", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtsollerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// SVT-Soll gegen Trailer-Konfiguration pr�fen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.checksvtsoll"), CfsString.get("cfs.running"), -1 );
							if (!cfs.checkTrailerSVT(false, SVTSoll.getEcuList(), null)) {
								testFlag=false;
								result = new Ergebnis( "CFS", "checkSVT(SVTSoll)", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "checkSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtsollerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "checkSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtsollerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "checkSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtsollerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "checkSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtsollerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// TAL generieren
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgenerateandstoretal"),CfsString.get("cfs.running"), -1 );

							// Abschalten der PSdZ-Fehlerbehandlung
							psdz.setCheckProgDeps(cfs.getCheckProgDeps());

							// Adresslisten erzeugen
							ecuAddressList = cfs.getEcuAddressList();
							ecuCheckProgDepFlashList = null;
							ecuAnlieferstandFlashList = null;

							// TAL Filter setzen
							TALFilter = new CascadeTALFilter();
							filteredTACategory = null;

							// 1. Kein SG Freischalten, Individualdaten, Codieren, Hardware, Programmieren
							Object[] allCategories = CascadeTACategories.getAllCategories().toArray();
							for( i = 0; i < allCategories.length; i++ ) {
								filteredTACategory = new CascadeFilteredTACategory( (CascadeTACategories) allCategories[i]);
								filteredTACategory.setAffectAllEcus( true ); // alle
								filteredTACategory.setBlacklist( true );     // nicht
								TALFilter.addFilteredTACategory( filteredTACategory );
							}

							// 2. ECU behandeln
							if (ecuAddressList.length > 0) {
								// Hardware
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.HW_DEINSTALL);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( false );         // muss
								filteredTACategory.addEcusToList(ecuAddressList); // nur ECUs
								TALFilter.addFilteredTACategory( filteredTACategory );

								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.HW_INSTALL);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( false );         // muss
								filteredTACategory.addEcusToList(ecuAddressList); // nur ECUs
								TALFilter.addFilteredTACategory( filteredTACategory );

								// Programmieren
								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.BL_FLASH);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( false );         // muss
								filteredTACategory.addEcusToList(ecuAddressList); // nur ECUs
								// Stichprobe
								if (cfs.getSamplingFlag()==true) {
									if (cfs.allowBootLoaderUpdate==true) {
										filteredTACategory.setForceExecution( true ); // Zwangsprogrammierung
									}
								}
								TALFilter.addFilteredTACategory( filteredTACategory );

								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.SW_DEPLOY);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( false );         // muss
								filteredTACategory.addEcusToList(ecuAddressList); // nur ECUs
								// Stichprobe
								if (cfs.getSamplingFlag()==true) {
									filteredTACategory.setForceExecution( true ); // Zwangsprogrammierung
								}
								TALFilter.addFilteredTACategory( filteredTACategory );

								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.IBA_DEPLOY);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( false );         // muss
								filteredTACategory.addEcusToList(ecuAddressList); // nur ECUs
								// Stichprobe
								if (cfs.getSamplingFlag()==true) {
									if (cfs.allowIBAUpdate==true) {
										filteredTACategory.setForceExecution( true ); // Zwangsprogrammierung
									}
								}
								TALFilter.addFilteredTACategory( filteredTACategory );

								filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.GWTB_DEPLOY);
								filteredTACategory.setAffectAllEcus( false );     // nicht alle
								filteredTACategory.setBlacklist( false );         // muss
								filteredTACategory.addEcusToList(ecuAddressList); // nur ECUs
								// Stichprobe
								if (cfs.getSamplingFlag()==true) {
									if (cfs.allowGWTBUpdate==true) {
										filteredTACategory.setForceExecution( true ); // Zwangsprogrammierung
									}
								}
								TALFilter.addFilteredTACategory( filteredTACategory );
							}
							// TAL erzeugen
							psdz.generateAndStoreTAL(TALFilter,ecuAddressList,false);

							// Die SGs Zwangsprogrammieren,
							// deren CheckProgDep != 1,
							// die TAL aber f�r dieses SG leer ist
							// Nicht bei Stichprobe, da dort alle SG
							// Zwangsgeflasht werden
							if (cfs.getSamplingFlag()!=true) {
								TAL = psdz.getTAL();
								ecuCheckProgDepFlashList = cfs.getSgCheckProgDepFlashListe(TAL.getTalLines(), programmingDependenciesListe, ecuAddressList);
								// 3. Zu flashende SGs deren CheckProgDeps != 1
								if (ecuCheckProgDepFlashList != null) {
									// TAL-Filter nicht zur�cksetzen

									// Programmieren Bootloader
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.BL_FLASH);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( false );         // muss
									filteredTACategory.setForceExecution( true );     // Zwangsprogrammierung
									filteredTACategory.addEcusToList(ecuCheckProgDepFlashList);   // zu flashende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
									// Programmieren Software
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.SW_DEPLOY);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( false );         // muss
									filteredTACategory.addEcusToList(ecuCheckProgDepFlashList);   // zu flashende SGS
									filteredTACategory.setForceExecution( true );     // Zwangsprogrammierung
									TALFilter.addFilteredTACategory( filteredTACategory );
									// Programmieren Betriebsanleitung
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.IBA_DEPLOY);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( false );         // muss
									filteredTACategory.setForceExecution( true );     // Zwangsprogrammierung
									filteredTACategory.addEcusToList(ecuCheckProgDepFlashList);   // zu flashende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
									// Programmieren Gatewaytabelle
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.GWTB_DEPLOY);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( false );         // muss
									filteredTACategory.setForceExecution( true );     // Zwangsprogrammierung
									filteredTACategory.addEcusToList(ecuCheckProgDepFlashList);   // zu flashende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
									// TAL nochmal neu erzeugen
									psdz.generateAndStoreTAL(TALFilter,ecuAddressList,false);
								}
							}

							// Die SGs Zwangsprogrammieren,
							// die bei Anlieferstandsprogrammieren,
							// auszublendende SGBMIDs enthalten
							if (cfs.getTrailerModeAnlieferzustandFlag()) {
								TAL = psdz.getTAL();
								ecuAnlieferstandFlashList = cfs.getSgAnlieferstandFlashListe(SVTIst.getEcuList(), SVTSoll.getEcuList(), ecuListe);
								// 4. Zu flashende SGs deren auszublendende SGBMID enthalten ist
								if (ecuAnlieferstandFlashList != null) {
									// TAL-Filter nicht zur�cksetzen

									// Programmieren Bootloader
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.BL_FLASH);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( false );         // muss
									filteredTACategory.setForceExecution( true );     // Zwangsprogrammierung
									filteredTACategory.addEcusToList(ecuAnlieferstandFlashList);   // zu flashende SGS
									TALFilter.addFilteredTACategory( filteredTACategory );
									// Programmieren Software
									filteredTACategory = new CascadeFilteredTACategory( CascadeTACategories.SW_DEPLOY);
									filteredTACategory.setAffectAllEcus( false );     // nicht alle
									filteredTACategory.setBlacklist( false );         // muss
									filteredTACategory.addEcusToList(ecuAnlieferstandFlashList);   // zu flashende SGS
									filteredTACategory.setForceExecution( true );     // Zwangsprogrammierung
									TALFilter.addFilteredTACategory( filteredTACategory );
									// TAL nochmal neu erzeugen
									psdz.generateAndStoreTAL(TALFilter,ecuAddressList,false);
								}
							}
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "generateAndStoreTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoretalerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "generateAndStoreTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoretalerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "generateAndStoreTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoretalerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "generateAndStoreTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoretalerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// TAL abholen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.psdzgettal"),CfsString.get("cfs.running"), -1 );
							TAL = psdz.getTAL();
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgettalerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgettalerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgettalerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgettalerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// TAL pr�fen
						try {
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( true );
							userDialog.displayMessage( CfsString.get("cfs.checktal"), CfsString.get("cfs.running"), -1 );
							if (!cfs.checkTrailerTAL(TAL, SVTIst.getEcuList(), SVTSoll.getEcuList())) {
								testFlag=false;
								result = new Ergebnis( "CFS", "checkTAL", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
							userDialog.reset();
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "checkTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checktalerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "checkTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checktalerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "checkTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checktalerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "checkTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checktalerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// Bei Anlieferzustand programmieren
						if (cfs.getTrailerModeAnlieferzustandFlag() || cfs.getParallelExecution()) {
							if (!cfs.isEmptyTAL(TAL)) {
								try {
									userDialog.setDisplayProgress( true );
									userDialog.getProgressbar().setIndeterminate( true );
									userDialog.displayMessage( CfsString.get("cfs.modifytal"),CfsString.get("cfs.running"), -1 );
									if (cfs.modifyTAL(psdz, cfs.getTrailerModeAnlieferzustandFlag(), cfs.getParallelExecution())) {
										// TAL aktualisieren
										TAL = psdz.getTAL();
										// LOG anlegen
										String[] psdzDeviceParams = psdz.getDeviceParams();
										if (psdzDeviceParams != null) {
											if (psdzDeviceParams.length > 3 ? Boolean.valueOf(psdzDeviceParams[3]).booleanValue() : false) {
												logPSdZContent(vehicleVinB2v, "TAL_NEU",psdz.getTALReader().getPsdzTALasXML());
											}
										}
									} else {
										testFlag=false;
										result = new Ergebnis( "CFS", "modifyTAL", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
										ergListe.add( result );
										status = STATUS_EXECUTION_ERROR;
									}
									userDialog.reset();
								} catch ( Exception e ) {
									userDialog.reset();
									if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
									testFlag=false;
									if (e.getMessage() != null)
										result = new Ergebnis( "CFS", "modifyTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.modifytalerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
									else
										result = new Ergebnis( "CFS", "modifyTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.modifytalerror"), "Exception", Ergebnis.FT_NIO_SYS );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								} catch( Throwable e ) {
									userDialog.reset();
									if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
									testFlag=false;
									if (e.getMessage() != null)
										result = new Ergebnis( "CFS", "modifyTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.modifytalerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
									else
										result = new Ergebnis( "CFS", "modifyTAL", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.modifytalerror"), "Throwable", Ergebnis.FT_NIO_SYS );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
							}
						}
					}

					if (testFlag) {
						// TraceElement belegen
						traceFileElement = new TraceFileElement();
						traceFileElement.timeMillisGesamt = System.currentTimeMillis();
						traceFileElement.timeMillisVorbereitungStart = System.currentTimeMillis();
						traceFileElement.timeMillisVorbereitungFertig = 0;
						traceFileElement.timeMillisNachbereitungStart = 0;
						traceFileElement.timeMillisNachbereitungFertig = 0;
						traceFileElement.iStufe=cfs.getSollIStufe();
						traceFileElement.vin=vehicleVinB2v;
						traceFileElement.TAL=TAL;
						if (TAL != null && TAL.getTalLines() != null) {
							n = TAL.getTalLines().length;
							if (n > 0) {
								traceFileElement.timeValidTalLine = new boolean[n];
								traceFileElement.timeMillisTalLine = new long[n];
								for (i=0; i<n; i++) {
									traceFileElement.timeValidTalLine[i] = false;
									traceFileElement.timeMillisTalLine[i] = traceFileElement.timeMillisVorbereitungStart;
								}
							}
						}
					}

					if (testFlag) {
						// TAL abarbeiten
						java.util.Timer myTimer = new java.util.Timer();
						try {
							if (!cfs.isEmptyTAL(TAL)) {
								cfs.initDialog(userDialog,false,true);
								userDialog.displayMessage( CfsString.get("cfs.psdzstarttalexecution"),CfsString.get("cfs.running")+cfs.getStopWatch(), -1 );
								StopWatchTask  myStopWatchTask = null;
								myStopWatchTask = new StopWatchTask(CfsString.get("cfs.psdzstarttalexecution"), CfsString.get("cfs.running"), userDialog, cfs, true);
								myTimer.schedule( myStopWatchTask, 0, PSDZ_STATUS_REFRESH_TIME);
								psdz.startTALExecution( cfs.getParallelExecution(), cfs.getMaxErrorRepeat(), false ); // ohne writeSvtSollToVCM
								myTimer.cancel();
								userDialog.reset();
							}
						} catch ( Exception e ) {
							myTimer.cancel();
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "startTALExecution", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzstarttalexecutionerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "startTALExecution", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzstarttalexecutionerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							myTimer.cancel();
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "startTALExecution", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzstarttalexecutionerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "startTALExecution", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzstarttalexecutionerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						// Warten bis TAL abgearbeitet
						try {
							if (!cfs.isEmptyTAL(TAL)) {
								String message="";
								String message1="";
								String message2="";
								String message3="";
								String message4="";
								boolean errorFlag=false;
								boolean errorFlag2=false;
								message=CfsString.get("cfs.running");
								// Laufende Abfrage
								while( true ) {
									userDialog.setUserInteractionEnabled( false );
									if (userDialog.getLastKey() == UserDialog.CONTINUE_KEY)
										cfs.copyToClipboard(CfsString.get("cfs.gettalexecutionstatus")+LF+message);

									if( userDialog.isCancelled() == true ) {
										cfs.initDialog(userDialog,false,true);
										userDialog.displayAlertMessage( CfsString.get("cfs.psdzcanceltalexecution"),CfsString.get("cfs.running")+cfs.getStopWatch(), -1 );
										java.util.Timer myTimer = new java.util.Timer();
										try {
											StopWatchTask  myStopWatchTask = null;
											myStopWatchTask = new StopWatchTask(CfsString.get("cfs.psdzcanceltalexecution"), CfsString.get("cfs.running"), userDialog, cfs, false);
											myTimer.schedule( myStopWatchTask, 0, PSDZ_STATUS_REFRESH_TIME);
											psdz.cancelTALExecution();
											myTimer.cancel();
										} catch ( Exception e ) {
											myTimer.cancel();
										} catch( Throwable e ) {
											myTimer.cancel();
										}
										userBreakFlag = true;
										testFlag=false;
										result = new Ergebnis( "PSdZ", "getTALExecutionStatus", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.userbreakerror"), "", Ergebnis.FT_NIO );
										ergListe.add( result );
										status = STATUS_EXECUTION_ERROR;
										userDialog.reset();
										break;
									}

									userDialog.setAuxiliaryButtonVisibilityMask( UserDialog.CANCEL_BUTTON_MASK | UserDialog.CONTINUE_BUTTON_MASK );
									userDialog.setContinueButtonText(CfsString.get("cfs.copy"));
									cfs.initDialog(userDialog,false,false);
									if (!errorFlag2)
										userDialog.displayMessage(CfsString.get("cfs.gettalexecutionstatus"),message, -1 );
									else {
										if (cfs.getTrailerModeAnlieferzustandFlag())
											userDialog.displayMessage(CfsString.get("cfs.gettalexecutionstatus"),message, -1 );
										else
											userDialog.displayAlertMessage(CfsString.get("cfs.gettalexecutionstatus"),message, -1 );
									}

									CascadeTAL TALExecutionStatus = psdz.getTALExecutionStatus(PSDZ_STATUS_REFRESH_TIME, null);
									CascadeTALLine[] TALLine = TALExecutionStatus.getTalLines();
									temp = "";
									cfs.setProgressMap(psdz.getProgressMap());
									message = CfsString.get("cfs.running") + cfs.getStopWatch() + NL + cfs.getProgressMessage() + NL;
									message1 = "";
									message2 = "";
									message3 = "";
									message4 = "";
									errorFlag = false;
									errorFlag2 = false;
									for (i=0; i<TALLine.length; i++) {
										temp = cfs.logString(TALLine[i].getBaseVariantName(),12) + " " + cfs.hexString(TALLine[i].getDiagnosticAddress().longValue(),2);
										if ( TALLine[i].getTALTACategories().length > 0 )
											temp = temp + " " + (TALLine[i].getTALTACategories()[0].getCategory().toString()+"  ").substring(0,2).toUpperCase();
										else
											temp = temp + " ??"; // sollte nie kommen
										if ((TALLine[i].getStatus()==CascadeExecutionStatus.EXECUTABLE) ||
											(TALLine[i].getStatus()==CascadeExecutionStatus.PROCESSING)) {
											n = 0;
											m = 0;
											errorFlag=false;
											for( j = 0; j < TALLine[i].getTALTACategories().length; j++ ) {
												if ((TALLine[i].getTALTACategories()[j].getCategory() == CascadeTACategories.BL_FLASH) ||
													(TALLine[i].getTALTACategories()[j].getCategory() == CascadeTACategories.SW_DEPLOY) ||
													(TALLine[i].getTALTACategories()[j].getCategory() == CascadeTACategories.IBA_DEPLOY)||
													(TALLine[i].getTALTACategories()[j].getCategory() == CascadeTACategories.GWTB_DEPLOY)) {
													n=n+TALLine[i].getTALTACategories()[j].getTAs().length;
													for( k = 0; k < TALLine[i].getTALTACategories()[j].getTAs().length; k++ ) {
														if (TALLine[i].getTALTACategories()[j].getTAs()[k].getStatus() == CascadeExecutionStatus.PROCESSING ) m++;
														if (TALLine[i].getTALTACategories()[j].getTAs()[k].getStatus() == CascadeExecutionStatus.PROCESSED ) m++;
														if (TALLine[i].getTALTACategories()[j].getTAs()[k].getStatus() == CascadeExecutionStatus.PROCESSEDWITHERROR ) {
															m++;
															errorFlag=true;
															errorFlag2=true;
														}
													}
												}
											}
											if (m==0) {
												if (!traceFileElement.timeValidTalLine[i])
													traceFileElement.timeMillisTalLine[i] = System.currentTimeMillis(); // noch nicht angefangen
											} else {
												if (traceFileElement.timeMillisVorbereitungFertig == 0) traceFileElement.timeMillisVorbereitungFertig = System.currentTimeMillis(); // Vorbereitung beendet
											}
											traceFileElement.timeMillisNachbereitungStart = System.currentTimeMillis(); // Nachbereitung noch nicht angefangen
											temp = temp + " " + (m<10?" ":"") + m + "/" + n + (n<10?" ":"") + cfs.getProgressBar(cfs.hexString(TALLine[i].getDiagnosticAddress().longValue(),2),TALLine[i].getTALTACategories()[0].getCategory());
											if (errorFlag) {
												if (cfs.getTrailerModeAnlieferzustandFlag())
													temp = temp + ""; // keine n.i.O. Ausgabe
												else
													temp = temp + " n.i.O.";
											}

											if (temp.indexOf("%") > 0)
												message1 = message1 + temp + NL; // oben in Arbeit
											else
												message2 = message2 + temp + NL; // dann noch nicht in Arbeit
										} else {
											// fertig
											if (!traceFileElement.timeValidTalLine[i]) {
												traceFileElement.timeValidTalLine[i] = true;
												traceFileElement.timeMillisTalLine[i] = System.currentTimeMillis() - traceFileElement.timeMillisTalLine[i];
											}
											if (TALLine[i].getStatus()==CascadeExecutionStatus.PROCESSED) {
												message4 = message4 + temp + "   i.O." + NL; // Am Schluss fertig io
											} else {
												errorFlag2=true;
												if ((TALLine[i].getStatus()==CascadeExecutionStatus.PROCESSEDWITHERROR)||(TALLine[i].getStatus()==CascadeExecutionStatus.NOTEXECUTABLE)) {
													if (cfs.getTrailerModeAnlieferzustandFlag())
														message3 = message3 + temp + NL; // keine n.i.O. Ausgabe
													else
														message3 = message3 + temp + " n.i.O." + NL; // dann fertig nio
												} else {
													message3 = message3 + temp + " "+TALLine[i].getStatus().toString() + NL;
												}
											}
										}
									}
									message = message + message1 + message2 + message3 + message4;

									if (!TALExecutionStatus.getStatus().getName().equals( CascadeTALExecutionStatus.RUNNING.getName())) {
										if (!TALExecutionStatus.getStatus().getName().equals( CascadeTALExecutionStatus.FINISHED.getName())) {
											mapEcuNIO.clear();
											mapEcuNotExecutable.clear();
											for (i=0; i<TALLine.length; i++) {
												// BasisName des SG ermitteln ("ECU:'Basisname' 0x'hexcode'")
												String sgIDName = PB.getString( "psdz.SG" ) + ":" + TALLine[i].getBaseVariantName() + " 0x" + cfs.hexString(TALLine[i].getDiagnosticAddress().longValue(),2) ;
												for( j = 0; j < TALLine[i].getTALTACategories().length; j++ ) {
													if (TALLine[i].getStatus()==CascadeExecutionStatus.PROCESSEDWITHERROR) {
														if (!cfs.getFehlerAusblenden(TALLine[i])) {
															HashSet<String> setTaSwtNIO = new HashSet<String>();
															// pr�fe ob schon eingetragen
															if (mapEcuNIO.containsKey(sgIDName)) setTaSwtNIO = (HashSet<String>)mapEcuNIO.get(sgIDName);
															setTaSwtNIO.add(TALLine[i].getTALTACategories()[j].getCategory().toString());
															mapEcuNIO.put(sgIDName,setTaSwtNIO);
														}
													}
													if (TALLine[i].getStatus()==CascadeExecutionStatus.NOTEXECUTABLE) {
														HashSet<String> setTaSwtNotExecutable = new HashSet<String>();
														// pr�fe ob schon eingetragen
														if (mapEcuNotExecutable.containsKey(sgIDName)) setTaSwtNotExecutable = (HashSet<String>)mapEcuNotExecutable.get(sgIDName);
														setTaSwtNotExecutable.add(TALLine[i].getTALTACategories()[j].getCategory().toString());
														mapEcuNotExecutable.put(sgIDName,setTaSwtNotExecutable);
													}
												}
											}
											if ((mapEcuNIO.size() > 0) || (mapEcuNotExecutable.size() > 0)) {
												// PSdZ hole die aufgelaufenen Events
												HashMap<String, List<Exception>> psdzErrors = new HashMap<String, List<Exception>>( buildExcHashMap(TALExecutionStatus) );
												HashMap<String, String> psdzECUStatisticsHexStr = new HashMap<String, String>( psdz.getEcuStatisticsHexString() );
												// Fehlereintr�ge anlegen
												entryECUsFT_NIO( ergListe, mapEcuNIO, mapEcuNotExecutable, psdzErrors, psdzECUStatisticsHexStr, psdz );
												// Alle relevanten Fehler aus der TAL interpretiert
												testFlag=false;
												status = STATUS_EXECUTION_ERROR;
											} else {
												if (cfs.getTrailerModeAnlieferzustandFlag()) {
													// Fehler ausgeblendet
												} else {
													testFlag=false;
													result = new Ergebnis( "PSdZ", "getTALExecutionStatus", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.gettalexecutionstatuserror"), TALExecutionStatus.getStatus().getName(), Ergebnis.FT_NIO );
													ergListe.add( result );
													status = STATUS_EXECUTION_ERROR;
												}
											}
										}
										TALExecutionStatus = psdz.getTALExecutionStatus( 0, "STATUS");
										if (TALExecutionStatus != null)
											traceFileElement.TAL=TALExecutionStatus;
										break;
									}
								}
								userDialog.reset();
							}
							// TraceElement belegen
							if (traceFileElement.timeMillisNachbereitungStart != 0) traceFileElement.timeMillisNachbereitungFertig = System.currentTimeMillis(); // Nachbereitung beendet
							traceFileElement.timeMillisGesamt = System.currentTimeMillis() - traceFileElement.timeMillisGesamt;
						} catch ( Exception e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getTALExecutionStatus", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.gettalexecutionstatuserror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getTALExecutionStatus", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.gettalexecutionstatuserror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							userDialog.reset();
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "PSdZ", "getTALExecutionStatus", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.gettalexecutionstatuserror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "PSdZ", "getTALExecutionStatus", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.gettalexecutionstatuserror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (cfs.getVerifySVTFlag()) {
						if (testFlag) {
							// SVT-IST-Neu generieren
							try {
								userDialog.setDisplayProgress( true );
								userDialog.getProgressbar().setIndeterminate( true );
								userDialog.displayMessage( CfsString.get("cfs.psdzgenerateandstoresvtistneu"),CfsString.get("cfs.running"), -1 );
								psdz.generateAndStoreSVTIstCFS("_NEU", null, false);
								userDialog.reset();
							} catch ( Exception e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "PSdZ", "generateAndStoreSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresvtistneuerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "PSdZ", "generateAndStoreSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresvtistneuerror"), "Exception", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							} catch( Throwable e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "PSdZ", "generateAndStoreSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresvtistneuerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "PSdZ", "generateAndStoreSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgenerateandstoresvtistneuerror"), "Throwable", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						}

						if (testFlag) {
							// SVT-IST-Neu zur Kontrolle abholen
							try {
								userDialog.setDisplayProgress( true );
								userDialog.getProgressbar().setIndeterminate( true );
								userDialog.displayMessage( CfsString.get("cfs.psdzgetsvtistneu"), CfsString.get("cfs.running"), -1 );
								SVTIstNeu = psdz.getSVTIst();
								programmingDependenciesListe = null;
								if (SVTIstNeu != null) {
									programmingDependenciesListe = (HashMap<String, String>)psdz.getProgDepCheckedperSVKinSVTist();
									if (programmingDependenciesListe != null) {
										if (programmingDependenciesListe.size() == 0) {
											programmingDependenciesListe = null;
										}
									}
								}
								userDialog.reset();
							} catch ( Exception e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "PSdZ", "getSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtistneuerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "PSdZ", "getSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtistneuerror"), "Exception", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							} catch( Throwable e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "PSdZ", "getSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtistneuerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "PSdZ", "getSVTIst", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzgetsvtistneuerror"), "Throwable", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						}

						if (testFlag) {
							// SVT-Ist-Neu gegen Trailer-Konfiguration pr�fen
							try {
								userDialog.setDisplayProgress( true );
								userDialog.getProgressbar().setIndeterminate( true );
								userDialog.displayMessage( CfsString.get("cfs.checksvtistneu"), CfsString.get("cfs.running"), -1 );
								if (!cfs.checkTrailerSVT(true, SVTIstNeu.getEcuList(), programmingDependenciesListe)) {
									testFlag=false;
									result = new Ergebnis( "CFS", "checkSVT(SVTIstNeu)", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
								userDialog.reset();
							} catch ( Exception e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "CFS", "checkSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtistneuerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "CFS", "checkSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtistneuerror"), "Exception", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							} catch( Throwable e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "CFS", "checkSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtistneuerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "CFS", "checkSVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.checksvtistneuerror"), "Throwable", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						}

						if (testFlag) {
							// SVT-Ist-Neu gegen SVT-Soll pr�fen
							try {
								userDialog.setDisplayProgress( true );
								userDialog.getProgressbar().setIndeterminate( true );
								userDialog.displayMessage( CfsString.get("cfs.verifysvt"), CfsString.get("cfs.running"), -1 );
								if (cfs.isDebug(cfs.debugVerifySVT)) {
									userDialog.displayMessage( CfsString.get("cfs.debug"), "verifySVT(SVTIstNeu,SVTSoll)", AUTOMATIC_MESSAGE_TIMEOUT );
								} else {
									if (!cfs.verifySVTTrailer(SVTIstNeu.getEcuList(),SVTSoll.getEcuList())) {
										testFlag=false;
										result = new Ergebnis( "CFS", "verifySVT(SVTIstNeu,SVTSoll)", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
										ergListe.add( result );
										status = STATUS_EXECUTION_ERROR;
									}
								}
								userDialog.reset();
							} catch ( Exception e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "CFS", "verifySVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.verifysvterror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "CFS", "verifySVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.verifysvterror"), "Exception", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							} catch( Throwable e ) {
								userDialog.reset();
								if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
								testFlag=false;
								if (e.getMessage() != null)
									result = new Ergebnis( "CFS", "verifySVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.verifysvterror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "CFS", "verifySVT", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.verifysvterror"), "Throwable", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						}
					}

					if (traceFileElement != null) {
						// Trace speichern
						try {
							// TraceElement belegen
							traceFileElement.okayFlag = testFlag;
							traceFileElement.abbruchFlag = userBreakFlag;
							if (!cfs.saveTrailerTrace(traceFileElement)) {
								testFlag=false;
								result = new Ergebnis( "CFS", "saveTrace", "", "", "", "", "", "", "", "0", "", "", "", cfs.getLastError(), cfs.getLastHelp(), Ergebnis.FT_NIO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
							}
						} catch ( Exception e ) {
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "saveTrace", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.savetraceerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "saveTrace", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.savetraceerror"), "Exception", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable e ) {
							if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
							testFlag=false;
							if (e.getMessage() != null)
								result = new Ergebnis( "CFS", "saveTrace", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.savetraceerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "CFS", "saveTrace", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.savetraceerror"), "Throwable", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					if (testFlag) {
						if (!cfs.getAutomaticMode()) {
							// Steuerger�te abstecken
							try {
								userDialog.setDisplayProgress( true );
								userDialog.getProgressbar().setIndeterminate( true );
								userDialog.setAllowCancel( true );
								userDialog.setCancelButtonText(CfsString.get("cfs.ignore"));
								userDialog.setBackgroundColor(cfs.backColor);
								if( signalLamp != null ) {
									signalLamp.YellowLightOn();
								}
								userDialog.displayMessage( CfsString.get("cfs.disconnectecu"),CfsString.get("cfs.waiting"), -1 );
								String ecus = null;
								cfs.stopStopWatch();
								while( true ) {
									psdz.generateAndStoreSVTIstCFS(null, null, false); // keine Trace Datei anlegen
									ecus = cfs.getConnectedEcus(psdz.getSVTIst().getEcuList());
									if (ecus == null) break;
									if (ecus.length() == 0) break;
									userDialog.setDisplayProgress( true );
									userDialog.getProgressbar().setIndeterminate( true );
									userDialog.setAllowCancel( true );
									userDialog.setCancelButtonText(CfsString.get("cfs.ignore"));
									userDialog.setBackgroundColor(cfs.backColor);
									userDialog.displayMessage( CfsString.get("cfs.disconnectecu"),CfsString.get("cfs.disconnect") + NL + ecus + NL + CfsString.get("cfs.waiting"), -1 );
									if (userDialog.isCancelled()) break;
									cfs.pause(1000);
								}
								userDialog.reset();
							} catch ( Exception e ) {
								userDialog.reset();
								// keine Fehlermeldung
							} catch( Throwable e ) {
								userDialog.reset();
								// keine Fehlermeldung
							}
							if( signalLamp != null ) {
								signalLamp.LightsOff();
							}
							cfs.continueStopWatch();
						}
					}
// Bookmark Trailer Ende
				}
			}

			if (connectionFlag) {
				// Verbindung zum Fahrzeug abbauen
				try {
					userDialog.setDisplayProgress( true );
					userDialog.getProgressbar().setIndeterminate( true );
					if (testFlag)
						userDialog.displayMessage( CfsString.get("cfs.psdzclosevehicleconnection"),CfsString.get("cfs.running"), -1 );
					else
						userDialog.displayAlertMessage( CfsString.get("cfs.psdzclosevehicleconnection"),CfsString.get("cfs.running"), -1 );
					psdz.closeVehicleConnection();
					connectionFlag=false;
					userDialog.reset();
				} catch ( Exception e ) {
					userDialog.reset();
					if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
					testFlag=false;
					if (e.getMessage() != null)
						result = new Ergebnis( "PSdZ", "closeVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzclosevehicleconnectionerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "PSdZ", "closeVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzclosevehicleconnectionerror"), "Exception", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				} catch( Throwable e ) {
					userDialog.reset();
					if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
					testFlag=false;
					if (e.getMessage() != null)
						result = new Ergebnis( "PSdZ", "closeVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzclosevehicleconnectionerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "PSdZ", "closeVehicleConnection", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzclosevehicleconnectionerror"), "Throwable", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
			}

			if (initFlag) {
				// PSdZ Close aufrufen
				try {
					userDialog.setDisplayProgress( true );
					userDialog.getProgressbar().setIndeterminate( true );
	                if (testFlag)
	                	userDialog.displayMessage( CfsString.get("cfs.psdzclose"),CfsString.get("cfs.running"), -1 );
	                else
	                	userDialog.displayAlertMessage( CfsString.get("cfs.psdzclose"),CfsString.get("cfs.running"), -1 );
					psdz.close();
					userDialog.reset();
				} catch ( Exception e ) {
					userDialog.reset();
					if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
					testFlag=false;
					if (e.getMessage() != null)
						result = new Ergebnis( "PSdZ", "close", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzcloseerror"), "Exception "+e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "PSdZ", "close", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzcloseerror"), "Exception", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				} catch( Throwable e ) {
					userDialog.reset();
					if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
					testFlag=false;
					if (e.getMessage() != null)
						result = new Ergebnis( "PSdZ", "close", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzcloseerror"), "Throwable "+e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "PSdZ", "close", "", "", "", "", "", "", "", "0", "", "", "", CfsString.get("cfs.psdzcloseerror"), "Throwable", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
			}
		} catch( PPExecutionException ppe ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			if (cfs.isDebug(cfs.debugPrintStackTrace)) e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// PSdZ-Device freigeben
		if( psdz != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releasePSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		if (cfs.isDebug(cfs.debugShowStopWatch)) {
			int t=0;
			if (cfs.getAutomaticMode())
				t = AUTOMATIC_MESSAGE_TIMEOUT;
			else
				t = 0;
			userDialog.requestStatusMessage("StopWatch",cfs.getStopWatch(),t);
		}

		// UserDialog-Device freigeben
		if( userDialog != null ) {
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		if( signalLamp != null ) {
			if( status == STATUS_EXECUTION_OK ) {
				signalLamp.GreenLightOn();
			} else {
				signalLamp.RedLightOn();
			}
		}

		// Signallampen-Device freigeben
		if( signalLamp != null ) {
			try {
				signalLamp.FreeceLights(true);
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseSignalLamp();
			} catch (Exception e) {
				// keine Fehlermeldung wenn nicht verf�gbar
			}
		}

		// Benutztes Fahrzeug f�r die Instanz freigeben
		try {
			vehicleVinInUse[cfs.instanz] = "";
		} catch( Exception e ) {
			// keine Fehlermeldung wenn nicht erlaubte instanz
		}

		// Pr�fstandsvariablen wieder auf alte Werte setzen
		try {
			if (modeSwitchFlag == true)
			{
				if (PSDZ__TEMP_MODE_SWITCH_old == null)
					getPr�flingLaufzeitUmgebung().deletePruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_MODE_SWITCH@" + testScreenID);
				else
					getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_MODE_SWITCH@" + testScreenID, PSDZ__TEMP_MODE_SWITCH_old );
				if (PSDZ__TEMP_MODE_SWITCH_GATEWAY_LIST_old == null)
					getPr�flingLaufzeitUmgebung().deletePruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_MODE_SWITCH_GATEWAY_LIST@" + testScreenID);
				else
					getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_MODE_SWITCH_GATEWAY_LIST@" + testScreenID, PSDZ__TEMP_MODE_SWITCH_GATEWAY_LIST_old);
			}
		} catch( Exception e ) {
			// keine Fehlermeldung wenn nicht verf�gbar
		}

		// Bei Fehler: Versionsinfo in Ergebnisliste vorne eintragen
		if( status != STATUS_EXECUTION_OK ) {
			temp="";
			if (deviceParams != null) {
				temp = "Interface=" + deviceParams[0] + NL;
				if (deviceParams[0].equalsIgnoreCase("ENET")) {
					temp="";
					if (cfs.getInterfaceIPAddress()!=null) {
						// EDICNET-IP-Address
						temp = "Interface=EDICNET, " + cfs.getInterfaceIPAddress() + NL;
					}
				}
				if (deviceParams[0].equalsIgnoreCase("ICOM")) {
					temp = "Interface=ICOM"; 
					if (cfs.getIcomCanBridgeActiveFlag()) {
						temp = temp + "(D-CAN)";
					} else {
						temp = temp + "(HSFZ)";
					}
					if (cfs.getInterfaceIPAddress()!=null) {
						// ICOM-IP-Address
						temp = temp + ", " + cfs.getInterfaceIPAddress();
					}
					temp = temp + NL;
				}
			}
			result = new Ergebnis("PSdZCfs", "PSdZCfs V"+VERSION, "", "", "", "", "",	"", "", "0", "", "", "", "", temp, Ergebnis.FT_NIO);
			ergListe.add( 0, result);
		}

		//Status setzen
		setPPStatus( info, status, ergListe );
	}
}

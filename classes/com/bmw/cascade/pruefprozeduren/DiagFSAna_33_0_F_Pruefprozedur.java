/*
 * DiagFSAna_V0_0_3_FU_Pruefprozedur.java Created on 01.07.02
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.Enumeration;
import java.util.Map;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultSet;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;

/**
 * Implementierung der Pr�fprozedur, die f�r vorgegebene Fehlerorte Bedingungen �hnlich auf
 * Einhaltung �berpr�ft. Die Bedingungen basieren hierbei auf bereits ermittelte
 * Fehlerspeicherinhalte (SGBD und JOB sind nicht als Attribute gesetzt worden) oder auf einem neu
 * auszuf�hrenden Diagnosejob (SGBD und JOB m�ssen spezifiziert sein).
 * 
 * @author Winkler, Crichton BMW AG
 * @version V0_0_3_FU ??.??.???? HJW Implementierung 
 *          V0_0_4_TA 18.06.2004 NDC Pr�fprozedur umstruktuiert 
 *          V0_0_5_FA 06.07.2004 NDC Freigabe 0_0_4_TA 
 *          V0_0_6_FA 06.07.2004 NDC Version kurzzeitig zur�ckgesetzt nach Fehler
 *          V0_0_7_FA 07.07.2004 NDC Bugfix auf Basis V0_0_5_FA 
 *          V0_0_8_FA 31.01.2005 NDC Bugfix: Auslesen des Hex-Codes gilt nicht f�r alle SGs 
 *          V_9_0_T 14.10.2008 MBa Erweiterung Paralleldiagnose, Umstellung auf EdiabasProxyThread,
 *          Warnungen entfernt 
 *          V_10_0_T 06.10.2010 FGE Korrektur DebugMode und Verhinderung des Aufrufs Ediabas.apiJob
 *          mit Parameter null 
 *          V_11_0_F 11.10.2010 FGE Freigabe F_Version
 *          mit Parameter null 
 *          V_12_0_T 31.05.2012 CWO Bef�higung f�r UDS Steuerger�te
 *          V_13_0_F 01.08.2012 CWO Bef�higung f�r UDS Steuerger�te
 *          V_14_0_T 21.08.2012 CWO BugFix: FS_LESEN wurde mehrmals durchgef�hrt - damit kam es zu Inkonsistenzen
 *          V_15_0_F 12.11.2012 CWO Freigabe
 *          V_16_0_T 24.03.2014 CWO Erweiterung HWTs pro Result
 *          V_17_0_F 28.03.2014 CWO Freigabe
 *          V_18_0_T 19.05.2014 CWO Bugfix wenn kein HWT parametriert wird
 *          V_19_0_F 19.05.2014 CWO Freigabe
 *          V_20_0_T 05.10.2015 TBU @-Operator bei den �bergabeparametern (SGBD, JOB, JOBPAR) eingebaut (LOP 1996), Generics 
 *          V_21_0_F 18.11.2015 FSC Freigabe 
 *          V_22_0_T 15.12.2015 PRE Bugfix. Vermeidung �bergabe von null bei extractValues vs. optionalen Argumenten.
 *          V_23_0_F 15.12.2015 PRE Freigabe
 *          V_24_0_T 17.12.2015 PRE Letzte Umsetzung war fehlerhaft und wurde korrigiert.
 *          V_25_0_F 07.01.2016 PRE Freigabe
 *          V_26_0_T 14.01.2016 PRE Die indizierten HWTs (HWT[1..N]) werden nun �berall angezogen. Ferner wurde die Ermittlung der Hex-Werte verbessert. 
 *          V_27_0_F 05.02.2016 PRE Freigabe
 *			V_28_0_F 10.02.2016 PRE Zwei Ergebnisse in Absprache mit Rene Gelfert von FT_IGNORE auf FT_NIO gesetzt.
 *          V_29_0_F 08.03.2016 PRE Freigabe 
 *          V_30_0_T 13.05.2016 PRE Problem bei Angabe von mehreren semikolongetrennten Job-Parametern beseitigt (es wurde nur der erste Parameter verwendet).          
 *          V_31_0_F 18.05.2016 PRE Freigabe
 *          V_32_0_T 03.07.2017 MKe Es besteht nun die M�glichkeit die ID einer Umweltbedingung dem Result zu �bergeben. Mit der ID wird dann der dazugeh�rige F_UWx_TEXT ausgewertet.
 *          V_33_0_F 26.07.2017 MKe F-Version
 */
public class DiagFSAna_33_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	// Debug standardm��ig True damit Fehler bei der initialisierung der Debug variable �ber eine
	// Pr�fstandsvariable geloggt werden
	boolean debug = true;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagFSAna_33_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DiagFSAna_33_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 * 
	 * @return die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "SGBD", "JOB", "JOBPAR", "RESULT[2..N]", "MIN[2..N]", "MAX[1..N]", "TAG", "HWT[1..N]" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 * 
	 * @return die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "FORT", "RESULT1", "MIN1" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 * 
	 * @return true wenn alles i.O. sonst false
	 */
	@Override
	public boolean checkArgs() {
		// double d;
		int i, j;
		int maxIndex = 0;
		boolean exist;

		try {
			// 1. Check: Sind alle requiredArgs (sinnvoll) gesetzt
			String temp;
			String[] requiredArgs = getRequiredArgs();
			for( i = 0; i < requiredArgs.length; i++ ) {
				temp = getArg( requiredArgs[i] );
				if( temp == null )
					return false;
				if( temp.equals( "" ) == true )
					return false;
				if( i == 0 ) {
					try {
						temp = extractErrorNumbers( temp );
					} catch( NumberFormatException e ) {
						return false;
					}
				}
			}
			// 2. Check: Entweder SGBD == JOB == JOBPAR == null oder (SGBD != null)&&(JOB != null)
			if( (getArg( getOptionalArgs()[0] ) != null) || (getArg( getOptionalArgs()[1] ) != null) || (getArg( getOptionalArgs()[2] ) != null) ) {
				if( (getArg( getOptionalArgs()[0] ) == null) || (getArg( getOptionalArgs()[1] ) == null) )
					return false;
			}
			// 3. Check: Sind �berfl�ssige Argumente gesetzt, d.h. Argumente, die weder required
			// noch optional sind
			// Gesetzte Argumente, die mit "RESULT", "MIN" oder "MAX" beginnen, werden hierbei nicht
			// analysiert, es wird
			// jedoch der maximale Index festgehalten. Die detaillierte Analyse erfolgt im 4-ten
			// Check.
			Enumeration<String> enu = getArgs().keys();
			String[] optionalArgs = getOptionalArgs();
			String givenkey;
			while( enu.hasMoreElements() ) {
				givenkey = (String) enu.nextElement();
				if( (givenkey.startsWith( "RESULT" ) == false) && (givenkey.startsWith( "MIN" ) == false) && (givenkey.startsWith( "MAX" ) == false) && givenkey.startsWith( "HWT" ) == false ) {
					exist = false;
					j = 0;
					while( (exist == false) && (j < requiredArgs.length) ) {
						if( givenkey.equals( requiredArgs[j] ) == true )
							exist = true;
						j++;
					}
					j = 0;
					while( (exist == false) && (j < optionalArgs.length) ) {
						if( givenkey.equals( optionalArgs[j] ) == true )
							exist = true;
						j++;
					}
					if( exist == false )
						return false;
					if( getArg( givenkey ).equals( "" ) == true )
						return false;
				} else {
					if( givenkey.startsWith( "RESULT" ) == true ) {
						temp = givenkey.substring( 6 );
					} else {
						temp = givenkey.substring( 3 );
					}
					try {
						j = Integer.parseInt( temp );
						if( j < 1 )
							return false;
						if( j > maxIndex )
							maxIndex = j;
					} catch( NumberFormatException e ) {
						return false;
					}
				}
			}
			// 4. Check: Sind die Result- und Min-Argumente plausibel, Max-Argumente sind optional
			// und werden daher gefordert.
			// Existieren jedoch Max-Werte, wird die Anzahl sowie die Konvertierung von Minwerten
			// und Maxwerten zu Zahlen gepr�ft,
			// sofern es sich um kein Result einer anderen PP handelt
			String[] mins, maxs;
			for( i = 1; i <= maxIndex; i++ ) {
				temp = getArg( "RESULT" + i );
				if( temp == null )
					return false;
				if( temp.indexOf( ';' ) != -1 )
					return false;
				if( (i > 1) && (temp.equals( getArg( "RESULT1" ) ) == true) )
					return false;
				temp = getArg( "MIN" + i );
				if( temp == null )
					return false;
				if( getArg( "MAX" + i ) != null ) {
					mins = splitArg( getArg( "MIN" + i ) );
					maxs = splitArg( getArg( "MAX" + i ) );
					if( mins.length != maxs.length )
						return false;
					for( j = 0; j < mins.length; j++ ) {
						try {
							if( mins[j].indexOf( '@' ) == -1 )
								Double.parseDouble( mins[j] );
							if( maxs[j].indexOf( '@' ) == -1 )
								Double.parseDouble( maxs[j] );
						} catch( NumberFormatException e ) {
							return false;
						}
					}
				}
			}
			// 5. Check: Argmente SGBD, JOB, RESULT[1..N] d�rfen keine Alternativen enthalten
			enu = getArgs().keys();
			while( enu.hasMoreElements() ) {
				givenkey = (String) enu.nextElement();
				if( (givenkey.equals( "SGBD" ) == true) || (givenkey.equals( "JOB" ) == true) || (givenkey.startsWith( "RESULT" ) == true) ) {
					if( getArg( givenkey ).indexOf( ';' ) != -1 )
						return false;
				}
			}
			// Tests bestanden, somit ok
			return true;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// Debug setzen
		try // Pr�fstandvariabel auslesen, ob Debug
		{
			Object pr_var_debug;

			pr_var_debug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
			if( pr_var_debug != null ) {
				if( pr_var_debug instanceof String ) {
					if( "TRUE".equalsIgnoreCase( (String) pr_var_debug ) ) {
						debug = true;
					} else {
						debug = false;
					}
				} else if( pr_var_debug instanceof Boolean ) {
					if( ((Boolean) pr_var_debug).booleanValue() ) {
						debug = true;
					} else {
						debug = false;
					}
				}
			} else {
				debug = false;
			}

		} catch( VariablesException e ) {
			writeDebug( "FS-ANA: cannot read VariableModes.PSCONFIG, DEBUG, set Debug false" );
			debug = false;
		}

		try {
			String sgbd = getArg( getOptionalArgs()[0] ) != null ? extractValues( getArg( getOptionalArgs()[0] ) )[0] : null;
			String job = getArg( getOptionalArgs()[1] ) != null ? extractValues( getArg( getOptionalArgs()[1] ) )[0] : null;
			if( sgbd == null ) {
				writeDebug( "SGBD == null, Fehlerspeicheranalyse notwendig" );
				status = executeErrorMemoryAnalyse( ergListe );
			} else if( job == null ) {
				writeDebug( "JOB == null, Fehlerspeicheranalyse notwendig" );
				status = executeErrorMemoryAnalyse( ergListe );
			} else if( job.equalsIgnoreCase( "FS_LESEN" ) || job.equalsIgnoreCase( "IS_LESEN" ) ) {
				writeDebug( "JOB == FS_LESEN oder IS_LESEN, Fehlerspeicheranalyse notwendig" );
				status = executeErrorMemoryAnalyse( ergListe );
			} else {
				writeDebug( "Standardanalyse notwendig" );
				status = executeStandardAnalyse( ergListe );
			}
		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ) + " " + e.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ) + " " + e.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		writeDebug( "Gesamtstatus: " + status );
		setPPStatus( info, status, ergListe );
	}

	/**
	 * Hilfsmethode
	 * 
	 * @param ergListe der Ergebnisliste
	 * @return der PP Status z.B. STATUS_EXECUTION_OK
	 * @throws PPExecutionException bei Fehler
	 */
	private int executeErrorMemoryAnalyse( Vector<Ergebnis> ergListe ) throws PPExecutionException {
		String sgbd, job, jobpar, jobres;
		String temp, temp1, temp2;
		String[] results = null;
		String[] hwts = null;
		String[] mins, maxs;
		String[] ignoreNumbers = new String[0];
		double d;
		int resAnzahl;
		boolean ok;
		Ergebnis result;
		int status = STATUS_EXECUTION_OK;

		// MBa Paralleldiagnose:
		boolean parallel = false;
		String tag = null;
		EdiabasProxyThread ept = null;
		DeviceManager devMan = null;

		// Parameter holen
		try {
			if( checkArgs() == false )
				throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			// Fehler die auszublenden sind
			temp = getArg( getRequiredArgs()[0] );
			if( temp != null ) {
				temp = extractErrorNumbers( temp );
				ignoreNumbers = temp.split( ";" );
			}
			// Diagnose Job Info
			sgbd = getArg( getOptionalArgs()[0] ) != null ? extractValues( getArg( getOptionalArgs()[0] ) )[0] : null;
			job = getArg( getOptionalArgs()[1] ) != null ? extractValues( getArg( getOptionalArgs()[1] ) )[0] : null;
			jobpar = getArg( getOptionalArgs()[2] ) != null ? toString( extractValues( getArg( getOptionalArgs()[2] ) ), ";" ) : null;
			if( sgbd == null )
				sgbd = getAttribut( "ERROR_SGBD" );
			if( job == null )
				job = getAttribut( "ERROR_JOB" );
			if( jobpar == null )
				jobpar = "";

			if( sgbd == null || job == null ) {
				result = new Ergebnis( "ParaFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "", "", "", "", "FS-Analyse: " + PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Ergebnis Info
			jobres = "";
			resAnzahl = 0;
			while( (temp = getArg( "RESULT" + (resAnzahl + 1) )) != null ) {
				jobres = jobres + ";" + extractValues( temp )[0];
				resAnzahl++;
			}
			if( resAnzahl > 0 ) {
				jobres = jobres.substring( 1 );
				results = splitArg( jobres ); // Links sind in jobres bereits aufgel�st
				if( results.length != resAnzahl )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) + "1" );
				for( int i = 1; i <= resAnzahl; i++ ) {
					mins = extractValues( getArg( "MIN" + i ) );
					if( getArg( "MAX" + i ) != null ) {
						maxs = extractValues( getArg( "MAX" + i ) );
						for( int j = 0; j < mins.length; j++ ) {
							try {
								Double.parseDouble( mins[j] );
								Double.parseDouble( maxs[j] );
							} catch( NumberFormatException e ) {
								throw new PPExecutionException( PB.getString( "parameterexistenz" ) + "2" );
							}
						}
					}
				}
			} else {
				jobres = "JOB_STATUS";
			}

			// Hwts
			hwts = getHwts( resAnzahl );

		} catch( PPExecutionException e ) {
			if( e.getMessage() != null )
				result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			throw new PPExecutionException();
		}

		// MBa: Paralleldiagnose >>>>>
		try // Pr�fstandvariabel auslesen, ob Paralleldiagnose
		{
			Object pr_var;

			pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
			if( pr_var != null ) {
				if( pr_var instanceof String ) {
					if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
						parallel = true;
					}
				} else if( pr_var instanceof Boolean ) {
					if( ((Boolean) pr_var).booleanValue() ) {
						parallel = true;
					}
				}
			}
		} catch( VariablesException e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			throw new PPExecutionException();
		}

		// Devicemanager
		devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();

		if( getArg( "TAG" ) != null )
			tag = getArg( "TAG" );

		if( parallel ) // Ediabas holen
		{
			try {
				ept = devMan.getEdiabasParallel( tag, sgbd );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Wrong Cascade configuration for parallel diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				throw new PPExecutionException();
			} catch( Throwable ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				throw new PPExecutionException();
			}
		} else {
			try {
				ept = devMan.getEdiabas( tag );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				throw new PPExecutionException();
			}
		}

		// <<<<< Paralleldiagnose

		// Ausf�hrung

		try {
			System.out.println( "asdf" );
			writeDebug( "Execute Ediabas JOB=" + job + ", SGBD=" + sgbd + ", JOBPAR=" + jobpar );

			temp = ept.executeDiagJob( sgbd, job, jobpar, "" );
			if( temp.equals( "OKAY" ) == false ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} else {
				result = new Ergebnis( "Status", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );
			}
			int initialJobSaetze = (ept.getDiagJobSaetze() - 1);
			String[] fOrtNrArray = new String[initialJobSaetze - 1];
			for( int jobSatz = 1; jobSatz < initialJobSaetze; jobSatz++ ) {

				// Alle Fehlerspeicherinfos (tempor�r) speichern
				ept.getResult();

				//temp = ept.executeDiagJob( sgbd, job, jobpar, "" );
				fOrtNrArray[jobSatz - 1] = ept.getDiagResultValue( jobSatz, "F_ORT_NR" );

			}
			// String errorType = Ergebnis.FT_NIO;

			for( int fOrtNrCount = 0; fOrtNrCount < fOrtNrArray.length; fOrtNrCount++ ) {
				for( int ignoreCount = 0; ignoreCount < ignoreNumbers.length; ignoreCount++ ) {
					if( ignoreNumbers[ignoreCount].equalsIgnoreCase( fOrtNrArray[fOrtNrCount] ) ) {
						// Falls Fehler vorhanden und ausgeblendet werden soll analysieren
						for( int resultCount = 0; resultCount < resAnzahl; resultCount++ ) {
							try {

								//Fehlerschranken holen
								mins = extractValues( getArg( "MIN" + (resultCount + 1) ) );
								temp1 = mins[0];
								for( int j = 1; j < mins.length; j++ ) {
									temp1 = temp1 + ";" + mins[j];
								}
								if( getArg( "MAX" + (resultCount + 1) ) != null ) {
									maxs = extractValues( getArg( "MAX" + (resultCount + 1) ) );
									temp2 = maxs[0];
									for( int j = 1; j < maxs.length; j++ ) {
										temp2 = temp2 + ";" + maxs[j];
									}
								} else {
									maxs = null;
									temp2 = "";
								}

								//F_Version=2 -> KWP-SG, F_Version=3 -> UDS-SG
								if( ept.getDiagResultValue( 1, "F_VERSION" ).equalsIgnoreCase( "2" ) ) {
									try {
										temp = ept.getDiagResultValue( fOrtNrCount + 1, results[resultCount] );
									} catch( Exception e ) {
										//wurde als Result eine Umweltbedingung parametriert, so wird ein
										//Fehler geworfen -> temp=""
										temp = "";
									}

									String fOrtText = ept.getDiagResultValue( 1, "F_ORT_TEXT" );
									String fHexCode = null;
									try {
										fHexCode = ept.getDiagResultValue( 1, "F_HEX_CODE" );
									} catch( Exception ernfe ) {
										// Nicht alle Steuerger�te unterst�tzen diese Zusatzinformation, also Fehler
										// ignorieren
										fHexCode = "?";
									}

									//Ergebnis Anhand der Fehlerschranken bewerten
									ok = false;
									if( maxs == null ) {
										for( int j = 0; j < mins.length; j++ ) {
											if( mins[j].equalsIgnoreCase( temp ) == true )
												ok = true;
										}
									} else {
										d = Double.parseDouble( temp );
										for( int j = 0; j < mins.length; j++ ) {
											if( (Double.parseDouble( mins[j] ) <= d) && (Double.parseDouble( maxs[j] ) >= d) )
												ok = true;
										}
									}
									// Ergebnisanalyse dokumentieren
									if( ok == false ) {
										writeDebug( "Analyse niO " + results[resultCount] );
										status = STATUS_EXECUTION_ERROR;
										result = new Ergebnis( results[resultCount], "EDIABAS", sgbd, job, jobpar, results[resultCount], temp, temp1, temp2, "0", "", "", "", "FS-Analyse", fOrtNrArray[fOrtNrCount] + " (HEX " + toHex( fOrtNrArray[fOrtNrCount], fHexCode ) + ")", Ergebnis.FT_NIO );
										ergListe.add( result );
										result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", fOrtNrArray[fOrtNrCount] + " (HEX " + toHex( fOrtNrArray[fOrtNrCount], fHexCode ) + ")", "", "", "0", "", "", "", fOrtText, hwts[resultCount], Ergebnis.FT_NIO );
										ergListe.add( result );
									} else {
										writeDebug( "Analyse iO " + results[resultCount] );
										result = new Ergebnis( results[resultCount], "EDIABAS", sgbd, job, jobpar, results[resultCount], temp, temp1, temp2, "0", "", "", "", "FS-Analyse", fOrtNrArray[fOrtNrCount] + " (HEX " + toHex( fOrtNrArray[fOrtNrCount], fHexCode ) + ")", Ergebnis.FT_IO );
										ergListe.add( result );
										result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", fOrtNrArray[fOrtNrCount] + " (HEX " + toHex( fOrtNrArray[fOrtNrCount], fHexCode ) + ")", "", "", "0", "", "", "", fOrtText, hwts[resultCount], Ergebnis.FT_IGNORE );
										ergListe.add( result );
									}

								} else {
									//das zu lesende Element stammt aus einer UDS-SGBD --> Alternativabfrage

									temp = ept.executeDiagJob( sgbd, "FS_LESEN_DETAIL", fOrtNrArray[fOrtNrCount], "" );

									if( temp.equals( "OKAY" ) == false ) { // Es sind Fehler bei der Ausf�hrung aufgetreten
										result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, "FS_LESEN_DETAIL", "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
										ergListe.add( result );
										throw new PPExecutionException();
									} else {
										result = new Ergebnis( "Status", "EDIABAS", sgbd, "FS_LESEN_DETAIL", "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
										ergListe.add( result );
									}

									String fOrtText = ept.getDiagResultValue( 1, "F_ORT_TEXT" );
									String fHexCode = null;
									try {
										fHexCode = ept.getDiagResultValue( 1, "F_HEX_CODE" );
									} catch( Exception ernfe ) {
										// Nicht alle Steuerger�te unterst�tzen diese Zusatzinformation, also Fehler
										// ignorieren
										fHexCode = "?";
									}

									int ergSaetze = ept.getDiagJobSaetze(); // Anzahl der gefunden Fehler
									EdiabasResultSet[] ediabasResultSets = ept.getResult().getResultSets();
									for( int i = 1; i < ergSaetze - 1; i++ ) {
										//if( i == jobSatz ) {
										EdiabasResultSet ediabasResultSet = ediabasResultSets[i];

										if( isNumeric( results[resultCount] ) ) {
											for( Object obj : ediabasResultSet.entrySet() ) {
												Map.Entry entry = (Map.Entry) obj;
												String key = entry.getKey().toString();
												String value = entry.getValue().toString();
												int index = value.indexOf( ":" );
												if( value.substring( index + 2, value.length() ).equalsIgnoreCase( results[resultCount] ) && key.contains( "F_UW" ) && key.contains( "_NR" ) ) {
													String uw_number = key.substring( 4, 5 );
													results[resultCount] = "F_UW" + uw_number + "_TEXT";
												}
											}
										}
										if( ediabasResultSet.containsKey( results[resultCount] ) ) {
											temp = ediabasResultSet.get( results[resultCount] ).stringValue().substring( ediabasResultSet.get( results[resultCount] ).stringValue().indexOf( ":" ) + 1 ).trim();//f_uw_anz: 9
											//break;

											// Ergebnis Anhand der Fehlerschranken bewerten
											ok = false;
											if( maxs == null ) {
												for( int j = 0; j < mins.length; j++ ) {
													if( mins[j].equalsIgnoreCase( temp ) == true )
														ok = true;
												}
											} else {
												d = Double.parseDouble( temp );
												for( int j = 0; j < mins.length; j++ ) {
													if( (Double.parseDouble( mins[j] ) <= d) && (Double.parseDouble( maxs[j] ) >= d) )
														ok = true;
												}
											}
											// Ergebnisanalyse dokumentieren
											if( ok == false ) {
												writeDebug( "Analyse niO " + results[resultCount] );
												status = STATUS_EXECUTION_ERROR;
												result = new Ergebnis( results[resultCount], "EDIABAS", sgbd, job, jobpar, results[resultCount], temp, temp1, temp2, "0", "", "", "", "FS-Analyse", fOrtNrArray[fOrtNrCount] + " (HEX " + toHex( fOrtNrArray[fOrtNrCount], fHexCode ) + ")", Ergebnis.FT_NIO );
												ergListe.add( result );
												result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", fOrtNrArray[fOrtNrCount] + " (HEX " + toHex( fOrtNrArray[fOrtNrCount], fHexCode ) + ")", "", "", "0", "", "", "", fOrtText, hwts[resultCount], Ergebnis.FT_NIO );
												ergListe.add( result );
											} else {
												writeDebug( "Analyse iO " + results[resultCount] );
												result = new Ergebnis( results[resultCount], "EDIABAS", sgbd, job, jobpar, results[resultCount], temp, temp1, temp2, "0", "", "", "", "FS-Analyse", fOrtNrArray[fOrtNrCount] + " (HEX " + toHex( fOrtNrArray[fOrtNrCount], fHexCode ) + ")", Ergebnis.FT_IO );
												ergListe.add( result );
												result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", fOrtNrArray[fOrtNrCount] + " (HEX " + toHex( fOrtNrArray[fOrtNrCount], fHexCode ) + ")", "", "", "0", "", "", "", fOrtText, hwts[resultCount], Ergebnis.FT_IGNORE );
												ergListe.add( result );
											}

										}

									}
								}

							} catch( ApiCallFailedException e ) {
								if( e.getMessage() != null )
									result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, results[resultCount], "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, results[resultCount], "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								throw new PPExecutionException();
							} catch( EdiabasResultNotFoundException e ) {
								if( results[resultCount] == null )
									results[resultCount] = "null";
								if( e.getMessage() != null )
									result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, results[resultCount], "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, results[resultCount], "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								throw new PPExecutionException();
							} catch( PPExecutionException e ) {
								if( results[resultCount] == null )
									results[resultCount] = "null";
								if( e.getMessage() != null )
									result = new Ergebnis( "ParaFehler", "EDIABAS", sgbd, job, jobpar, results[resultCount], "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "ausfuehrungsfehler" ) + " Parameter", e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "ParaFehler", "EDIABAS", sgbd, job, jobpar, results[resultCount], "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "ausfuehrungsfehler" ) + " Parameter", "", Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								throw new PPExecutionException();
							}
						}
					}
				}
			}

		} catch( ApiCallFailedException e ) {
			result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ept.apiErrorCode() + ": " + ept.apiErrorText(), Ergebnis.FT_NIO );
			ergListe.add( result );
			throw new PPExecutionException();
		} catch( EdiabasResultNotFoundException e ) {
			if( e.getMessage() != null )
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			throw new PPExecutionException();
		}

		// MBa: Paralleldiagnose
		if( parallel ) // && ept!=null)
		{
			try {
				devMan.releaseEdiabasParallel( ept, tag, sgbd );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable t ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;

			}
		}
		return status;
	}

	private boolean isNumeric( String str ) {
		try {
			Double.parseDouble( str );
		} catch( NumberFormatException nfe ) {
			return false;
		}
		return true;
	}

	/**
	 * Hilfsmethode
	 * 
	 * @param ergListe der Ergebnisliste
	 * @return der PP Status z.B. STATUS_EXECUTION_OK
	 * @throws PPExecutionException bei Fehler
	 */
	private int executeStandardAnalyse( Vector<Ergebnis> ergListe ) throws PPExecutionException {
		String sgbd, job, jobpar, jobres;
		String temp, temp1, temp2;
		String[] results = null;
		String[] hwts = null;
		String[] mins, maxs;
		String[] ignoreNumbers = new String[0];
		double d;
		int resAnzahl;
		boolean ok;
		Ergebnis result;
		int status = STATUS_EXECUTION_OK;

		// MBa Paralleldiagnose:
		boolean parallel = false;
		String tag = null;
		EdiabasProxyThread ept = null;
		DeviceManager devMan = null;

		// Parameter holen
		try {
			if( checkArgs() == false )
				throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			// Fehler die auszublenden sind
			temp = getArg( getRequiredArgs()[0] );
			if( temp != null ) {
				temp = extractErrorNumbers( temp );
				ignoreNumbers = temp.split( ";" );
			}
			// Diagnose Job Info
			sgbd = getArg( getOptionalArgs()[0] ) != null ? extractValues( getArg( getOptionalArgs()[0] ) )[0] : null;
			job = getArg( getOptionalArgs()[1] ) != null ? extractValues( getArg( getOptionalArgs()[1] ) )[0] : null;
			jobpar = getArg( getOptionalArgs()[2] ) != null ? toString( extractValues( getArg( getOptionalArgs()[2] ) ), ";" ) : null;
			if( jobpar == null )
				jobpar = "";
			// Ergebnis Info
			jobres = "";
			resAnzahl = 0;
			while( (temp = getArg( "RESULT" + (resAnzahl + 1) )) != null ) {
				jobres = jobres + ";" + extractValues( temp )[0];
				resAnzahl++;
			}
			if( resAnzahl > 0 ) {
				jobres = jobres.substring( 1 );
				results = splitArg( jobres ); // Links sind in jobres bereits aufgel�st
				if( results.length != resAnzahl )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) + "1" );
				for( int i = 1; i <= resAnzahl; i++ ) {
					mins = extractValues( getArg( "MIN" + i ) );
					if( getArg( "MAX" + i ) != null ) {
						maxs = extractValues( getArg( "MAX" + i ) );
						for( int j = 0; j < mins.length; j++ ) {
							try {
								d = Double.parseDouble( mins[j] );
								d = Double.parseDouble( maxs[j] );
							} catch( NumberFormatException e ) {
								throw new PPExecutionException( PB.getString( "parameterexistenz" ) + "2" );
							}
						}
					}
				}
			} else {
				jobres = "JOB_STATUS";
			}

			// Hwts
			hwts = getHwts( resAnzahl );

		} catch( PPExecutionException e ) {
			if( e.getMessage() != null )
				result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			throw new PPExecutionException();
		}

		// MBa: Paralleldiagnose >>>>>
		try // Pr�fstandvariabel auslesen, ob Paralleldiagnose
		{
			Object pr_var;

			pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
			if( pr_var != null ) {
				if( pr_var instanceof String ) {
					if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
						parallel = true;
					}
				} else if( pr_var instanceof Boolean ) {
					if( ((Boolean) pr_var).booleanValue() ) {
						parallel = true;
					}
				}
			}
		} catch( VariablesException e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			throw new PPExecutionException();
		}

		// Devicemanager
		devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();

		if( getArg( "TAG" ) != null )
			tag = getArg( "TAG" );

		if( parallel ) // Ediabas holen
		{
			try {
				ept = devMan.getEdiabasParallel( tag, sgbd );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Wrong Cascade configuration for parallel diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				return status;
			} catch( Throwable ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				return status;
			}
		} else {
			try {
				ept = devMan.getEdiabas( tag );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				return status;
			}
		}

		// <<<<< Paralleldiagnose

		// Diagnose Job ausf�hren
		try {
			writeDebug( "Executing Diag Job, " + sgbd + ", " + job + ", " + jobpar + ", " + jobres );
			temp = ept.executeDiagJob( sgbd, job, jobpar, jobres );
			if( temp.equals( "OKAY" ) == false ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			}
		} catch( ApiCallFailedException e ) {
			result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "diagnosefehler" ), ept.apiErrorCode() + ": " + ept.apiErrorText(), Ergebnis.FT_NIO );
			ergListe.add( result );
			throw new PPExecutionException();
		} catch( EdiabasResultNotFoundException e ) {
			if( e.getMessage() != null )
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			throw new PPExecutionException();
		}

		// Diagnose Ergebnis(se) analysieren
		for( int i = 1; i <= resAnzahl; i++ ) {
			try {
				temp = ept.getDiagResultValue( results[i - 1] );
				mins = extractValues( getArg( "MIN" + i ) );
				temp1 = mins[0];
				for( int j = 1; j < mins.length; j++ ) {
					temp1 = temp1 + ";" + mins[j];
				}
				if( getArg( "MAX" + i ) != null ) {
					maxs = extractValues( getArg( "MAX" + i ) );
					temp2 = maxs[0];
					for( int j = 1; j < maxs.length; j++ ) {
						temp2 = temp2 + ";" + maxs[j];
					}
				} else {
					maxs = null;
					temp2 = "";
				}
				ok = false;
				if( maxs == null ) {
					for( int j = 0; j < mins.length; j++ ) {
						if( mins[j].equalsIgnoreCase( temp ) == true )
							ok = true;
					}
				} else {
					d = Double.parseDouble( temp );
					for( int j = 0; j < mins.length; j++ ) {
						if( (Double.parseDouble( mins[j] ) <= d) && (Double.parseDouble( maxs[j] ) >= d) )
							ok = true;
					}
				}
				// Ergebnisanalyse dokumentieren
				if( ok == false ) {
					writeDebug( "Analyse niO " + results[i - 1] );
					status = STATUS_EXECUTION_ERROR;
					result = new Ergebnis( results[i - 1], "EDIABAS", sgbd, job, jobpar, results[i - 1], temp, temp1, temp2, "0", "", "", "", "FS-Analyse", hwts[i - 1], Ergebnis.FT_NIO );
				} else {
					writeDebug( "Analyse iO " + results[i - 1] );
					result = new Ergebnis( results[i - 1], "EDIABAS", sgbd, job, jobpar, results[i - 1], temp, temp1, temp2, "0", "", "", "", "FS-Analyse", hwts[i - 1], Ergebnis.FT_IO );
				}
				ergListe.add( result );
			} catch( ApiCallFailedException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, results[i - 1], "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, results[i - 1], "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				if( results[i - 1] == null )
					results[i - 1] = "null";
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, results[i - 1], "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, results[i - 1], "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( PPExecutionException e ) {
				if( results[i - 1] == null )
					results[i - 1] = "null";
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "EDIABAS", sgbd, job, jobpar, results[i - 1], "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "ausfuehrungsfehler" ) + " Parameter", e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "EDIABAS", sgbd, job, jobpar, results[i - 1], "", "", "", "0", "", "", "", "FS-Analyse: " + PB.getString( "ausfuehrungsfehler" ) + " Parameter", "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
		}

		// Nach Analyse relevante Fehler dokumentieren
		try {
			// EDIABAS Fehlerspeicher Job ausf�hren

			sgbd = getAttribut( "ERROR_SGBD" );
			job = getAttribut( "ERROR_JOB" );

			if( sgbd == null || job == null ) {
				result = new Ergebnis( "ParaFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "", "", "", "", "FS-Analyse: " + PB.getString( "parametrierfehler" + " no call FS_LESEN " ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			writeDebug( "Executing Diag Job, " + sgbd + ", " + job );
			temp = ept.executeDiagJob( sgbd, job, "", "" );
			if( temp.equals( "OKAY" ) == false ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} else {
				result = new Ergebnis( "Status", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );
			}
			// Alle Fehlerspeicherinfos (tempor�r) speichern
			for( int i = 1; i < (ept.getDiagJobSaetze() - 1); i++ ) {
				String fOrtNr = ept.getDiagResultValue( i, "F_ORT_NR" );
				String fOrtText = ept.getDiagResultValue( i, "F_ORT_TEXT" );
				String fHexCode = ept.getDiagResultValue( i, "F_HEX_CODE" );
				String errorType = Ergebnis.FT_NIO;
				// Falls Fehler ausgeblendet werden soll, FT_IGNORE setzen ansonsten direkt
				// dokumentieren
				for( int j = 0; j < ignoreNumbers.length; j++ ) {
					if( ignoreNumbers[j].equalsIgnoreCase( fOrtNr ) ) {
						if( status == STATUS_EXECUTION_OK )
							errorType = Ergebnis.FT_IGNORE;
						result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", fOrtNr + " (HEX " + toHex( fOrtNr, fHexCode ) + ")", "", "", "0", "", "", "", fOrtText, "", errorType );
						ergListe.add( result );
					}
				}
			}
		} catch( ApiCallFailedException e ) {
			result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ept.apiErrorCode() + ": " + ept.apiErrorText(), Ergebnis.FT_NIO );
			ergListe.add( result );
			throw new PPExecutionException();
		} catch( EdiabasResultNotFoundException e ) {
			if( e.getMessage() != null )
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			throw new PPExecutionException();
		}

		// MBa: Paralleldiagnose
		if( parallel ) // && ept!=null)
		{
			try {
				devMan.releaseEdiabasParallel( ept, tag, sgbd );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable t ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;

			}
		}
		return status;
	}

	/**
	 * Schreibt ein Debugausgabe im ScreenLog
	 * 
	 * @param info Text
	 */
	private void writeDebug( String info ) {
		if( debug )
			System.out.println( "DiagFSAna: " + getPr�fling().getName() + "." + getName() + " " + info );
	}

	/**
	 * Hilfsmethode f�r Argument IGNORE: L�st den �bergebenen String auf
	 * 
	 * @param in der Argument IGNORE
	 * @return vollst�ndige String (zB "1-3" wird als "1;2;3" zur�ckgeliefert)
	 */
	private String extractErrorNumbers( String in ) {
		String out;
		String sub, subSub;
		int[] posArray;
		int counter = 1;
		// int Pos = 0;
		int min, max;
		int i, j;

		for( i = 0; i < in.length(); i++ ) {
			if( in.charAt( i ) == ';' )
				counter++;
		}

		posArray = new int[counter + 1];
		j = 0;
		posArray[j++] = -1;
		for( i = 0; i < in.length(); i++ ) {
			if( in.charAt( i ) == ';' )
				posArray[j++] = i;
		}
		posArray[j++] = in.length();

		out = ";";
		for( i = 0; i < counter; i++ ) {
			sub = (in.substring( posArray[i] + 1, posArray[i + 1] )).trim();
			if( sub.compareTo( "" ) != 0 ) {
				j = sub.indexOf( "-" );
				if( (j < 1) || (j == (sub.length() - 1)) )
					out = out + sub + ";";
				else {
					subSub = sub.substring( 0, j );
					min = Integer.parseInt( subSub );
					subSub = sub.substring( j + 1, sub.length() );
					max = Integer.parseInt( subSub );
					for( j = min; j <= max; j++ ) {
						out = out + j + ";";
					}
				}
			}
		}

		return out;
	}

	/**
	 * Liefert den �bergebenen Wert in hexadezimal zur�ck (formatiert in 
	 * Zweiergruppen). Falls dies nicht m�glich ist, wird der spezifizierte 
	 * Default-Wert (unformatiert) zur�ckgegeben.
	 * 
	 * Hinweis: Der Einsatz dieser Methode war n�tig, da F_HEX_CODE eine zu
	 * lange Zahl (mit zus�tzlichen Stellen) liefert.  
	 * 
	 * @param strValue Eingangswert, der in hexadezimaler Form zur�ckzugeben 
	 * 		  ist. 
	 * @param defaultOnError Im Fehlerfall zur�ckzugebender Wert. 
	 * @return Die hexadezimale Form des Eingangswertes.  
	 */
	private String toHex( String strValue, String defaultOnError ) {
		try {
			StringBuilder hexValue = new StringBuilder( Long.toHexString( Long.parseLong( strValue ) ) );
			if( (hexValue.length() & 1) != 0 ) //die Anzahl der Stellen muss geradzahlig sein, daher ggf. f�hrende 0 erg�nzen 
				hexValue.insert( 0, "0" );
			for( int i = hexValue.length() - 2; i > 0; i -= 2 )
				//Zahl in Zweiergruppen aufteilen
				hexValue.insert( i, " " );
			return hexValue.toString().toUpperCase();
		} catch( Exception e ) {
			return defaultOnError;
		}
	}

	/**
	 * Bezieht die HWT-Argumente, welche pro Result angelegt wurden. Eventuell
	 * fehlende HWTs (mehr Results als HWTs) werden mit "" aufgef�llt.
	 * 
	 * @param resAnzahl Anzahl der Results.
	 * @return Die ermittelten HWTs.
	 * @throws PPExecutionException Bei einem Fehler.
	 */
	private String[] getHwts( int resAnzahl ) throws PPExecutionException {
		String[] hwts = null;
		String temp, jobhwt = "";
		int hwtAnzahl = 0;
		while( (temp = getArg( "HWT" + (hwtAnzahl + 1) )) != null ) {
			jobhwt = jobhwt + ";" + extractValues( temp )[0];
			hwtAnzahl++;
		}
		if( hwtAnzahl > 0 ) {
			jobhwt = jobhwt.substring( 1 );
			hwts = splitArg( jobhwt );
			if( hwts.length != hwtAnzahl ) {
				throw new PPExecutionException( PB.getString( "parameterexistenz" ) + "3" );
			}
		}

		if( hwtAnzahl != resAnzahl ) {
			//hwts zur Not mit Leerzeilen bef�llen
			String[] hwtsCopy = hwts;
			hwts = new String[resAnzahl];
			for( int i = 0; i < hwts.length; i++ ) {
				if( hwtsCopy != null && i < hwtsCopy.length ) {
					hwts[i] = hwtsCopy[i];
				} else {
					hwts[i] = "";
				}

			}
		}

		return hwts;
	}

	/**
	 * Erzeugt aus dem �bergebenen Array einen String, bei dem die Elemente
	 * mit dem spezifizierten Separator getrennt werden.
	 * 
	 * @param array Das zu konvertierende Array.
	 * @param separator Das Trennzeichen.
	 * @return Die erzeugte String-Repr�sentation des Arrays.
	 */
	private String toString( Object[] array, String separator ) {
		StringBuilder builder = new StringBuilder();

		if( array == null )
			return "";

		if( separator == null )
			separator = "";

		for( int i = 0; i < array.length; i++ ) {
			if( i != 0 )
				builder.append( separator );
			builder.append( array[i] != null ? array[i].toString() : "" );
		}

		return builder.toString();
	}

}

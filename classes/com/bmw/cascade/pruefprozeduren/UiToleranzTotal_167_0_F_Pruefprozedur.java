/**
 * UiToleranzTotal Created on 16.10.02
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.uianalyse.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.*;

/**
 * Pr�fprozedur, die auf eine Grenzwerteinhaltung des Strom- oder Leistungsverlaufes innerhalb eines
 * Timeouts wartet. Wie lange die Grenzwerteinhaltung erf�llt sein soll, ist parametrierbar.
 * Ebenfalls kann ein Referenzwert �bergeben werden. Parallel kann eine Benutzeranweisung ausgegeben
 * werden oder eine Trigger-PP ausgef�hrt werden, beide haben das Ziel, den Strom- oder
 * Leistungswert in den entsprechenden Bereich zu bringen.
 * 
 * @author <br>
 *         Atell (BMW AG / TI-430) <br>
 *         Wen (ESG Elektroniksystem- und Logistik-GmbH / TI-431) <br>
 *         Mellersh (BMW AG / TI-431) <br>
 *         Crichton (BMW AG / TI-430) <br>
 * @version <br>
 *          V0_5_1_FA We Die Parameter TRIGGER_TYPE und MESS_TYPE als optionale Parameter definiert
 *          Grenzfrequenz f�r Grundwerterfassung auf 500 Hz defaultm��ig gesetzt <br>
 *          V0_5_2_TA We Die Einheit von Messgr��en wurde gel�scht f�r eine bessere Auswertung <br>
 *          V0_5_3_TA We Die Einheit von Messgr��en wurde an ResultName angef�gt <br>
 *          V0_5_4_FA We Keine �nderung zu V0_5_3_TA <br>
 *          V0_5_6_FA We Der Weiterschaltwert basiert jetzt auch auf den Messwert (optional) <br>
 *          V0_5_9_FA We Abbruchm�glichkeit f�r Siemens-Handterminal <br>
 *          V0_6_1_FA We Zus�tzliche Pr�fung der Stromwerte in der Totzeit <br>
 *          V0_6_3_TA We Unterst�tzt parallelen Ablauf f�r Status-Abfrage <br>
 *          V0_6_4_FA NC Freigabe V0_6_3_TA <br>
 *          V0_6_5_TA We Optionale Parameter WAIT_TRIGGER_PP wurde hinzugef�gt <br>
 *          V0_6_7_TA We 10ms Wartezeit f�r Ausf�hrung von TRIGGER_PP <br>
 *          V0_6_8_FA We Freigabe V0_6_7_TA <br>
 *          V0_7_1_FA We Unterst�tzt beide Richtungen f�r Stromzange <br>
 *          V0_7_2_TA We Fehlerbehandlungen werden verbessert <br>
 *          V0_7_3_TA We Fehlerbehandlungen werden verbessert <br>
 *          V0_7_4_FA Me FA-Version <br>
 *          V0_7_6_TA We AWT wird am Anfang dargestellt <br>
 *          V0_7_7_FA Me FA-Version <br>
 *          V0_7_8_FA Me Fehler bei Argumentenkontrolle mit i & j korrigiert <br>
 *          V0_7_9_TA Me Fehler Trigger_PP vor Grundwert behoben <br>
 *          V0_8_0_FA Me FA-Version <br>
 *          V0_8_1_TA We TA-Version von V0_8_0_FA <br>
 *          V0_8_2_TA We Warte 30 ms f�r Initialisierung des Median-Filters von Econszange <br>
 *          V0_8_3_FA We FA-Version <br>
 *          V0_8_4_TA We AWT f�r Ruhestrommessung wird auch ausgegeben <br>
 *          V0_8_5_TA We Dateiname-Eingabe f�r Sonderfunktion 84 <br>
 *          V0_8_6_FA Me FA-Version <br>
 *          V0_8_7_TA We Neue Grundwert-Ermittlung <br>
 *          V0_8_8_TA We Bugfix <br>
 *          V0_8_9_TA We Bugfix <br>
 *          V0_9_0_FA We pp = null am Ende der Pruefprozedur gesetzt <br>
 *          V0_9_1_FA We myPruefprozedur = null am Ende des EcosThread gesetzt <br>
 *          V0_9_2_TA We Grenzfrequenz fuer Grundwertermittlung von 500 auf 200 geaendert
 *          (Mittelwertfilter) <br>
 *          V0_9_3_TA We Grenzfrequenz fuer Grundwertermittlung von 200 auf 500 geaendert <br>
 *          Die wichtigen Parameter wurde in Log-Datei abgespeichert <br>
 *          V0_9_4_FA We FA-Version <br>
 *          V0_9_5_TA We Grenzfrequenz fuer Grundwertermittlung von 500 auf 200 geaendert
 *          (Mittelwertfilter) <br>
 *          V0_9_6_FA Me FA-Version <br>
 *          V0_9_7_TA We Parameternamen von TRIG_FREQ, MESS_FREQ und BASE_FREQ wurden ge�ndert <br>
 *          V0_9_8_FA Me FA-Version <br>
 *          V0_9_9_TA We Der Grundwert wird einfach durch nicht gefilterte Rohwerte ermittelt <br>
 *          V1_0_0_TA We FREQ_BASE ist von 200 auf 500 ge�ndert <br>
 *          V1_0_1_FA Me FA-Version <br>
 *          V1_0_2_TA We OK_TIME_START wurde durch BASE_VALUE_UPDATE_TIME ersetzt, OK_TIME_END wurde
 *          gel�scht <br>
 *          V1_0_3_TA We Textausgabe am Handterminal w�hrend der Strommessung wurde verbessert <br>
 *          V1_0_4_FA Me FA-Version <br>
 *          V1_0_5_TA We Statt Messwerttimer wurde Systemtimer (sleep( totzeit ) in der Totzeit
 *          verwendet <br>
 *          V1_0_6_FA We In der Totzeit wurde die Stromdaten auch eingelesen <br>
 *          V1_0_7_FA We In der Totzeit wurde keine zus�tzliche Meldung ausgegeben <br>
 *          V1_0_8_FA 11.10.2004 We Keine Meldung ausgibt wenn keine Daten von Stromzange bekommt <br>
 *          V1_0_9_FA 12.10.2004 We Daten werden beim Ruhestrommessung gel�scht (CleanStream
 *          Folgefehler) <br>
 *          V1_1_0_FA 13.10.2004 We Status f�r falsche Triggerung wurde ge�ndert <br>
 *          V1_1_1_FA 15.10.2004 We Am Anfang wird Messung zur�ckgesetzt mit reset(false) <br>
 *          V1_1_2_FA 18.10.2004 We Messwert bei der Spitzenwertmessung ist der Mittelwert aller
 *          Werte in der Messzeit <br>
 *          V1_1_3_FA 18.10.2004 We 30ms f�r Initialisierung f�r Median-Filter wurde ausgenommen <br>
 *          V1_1_4_TA 19.10.2004 We optionale Parameter LOGGER_ENABLE wurde hinzugef�gt <br>
 *          V1_1_5_TA 19.10.2004 We Konzept von Spitzenwertmessung wurde korrigiert <br>
 *          V1_1_6_FA 20.10.2004 We FA-Version <br>
 *          V1_1_7_TA 20.10.2004 We Messfilter wurde in der Totzeit initialisiert <br>
 *          V1_1_8_FA 21.10.2004 We FA-Version <br>
 *          V1_1_9_TA 22.10.2004 We Bugfix f�r Initialisierung des Messfilters nach der Totzeit <br>
 *          V1_2_0_FA 22.10.2004 We FA-Version <br>
 *          V1_2_3_TA 04.02.2004 Gehring, Aufruf einer neuen wait-Methode zur Ermittlung der Totzeit
 *          (Stromwerte werden jetzt auch innerhalb der Totzeit normiert) <BR>
 *          Konstante FehlerText hinzu, damit dem Ergebnis-Objekt im FT_NIO-Fall immer ein
 *          FehlerText uebergeben wird <BR>
 *          V1_2_7_TA 21.02.2005 Gehring Konstanter FehlerText durch PB.getString("pollingAbbruch")
 *          ersetzt. <BR>
 *          V1_2_8_TA 28.02.2005 Siemens minimale bzw. maximale Wert bei Grundwertermittlung. <BR>
 *          V1_2_9_TA 28.02.2005 Siemens Filterung ein f�r Grundwertermittlung. <BR>
 *          V1_3_0_TA 01.03.2005 Siemens Abspeicherung 1. Grundwert ge�ndert. <BR>
 *          V133_T 02.05.2005 Siemens / Gehring Test mit der neuen Versionsnummerierung <BR>
 *          V134_F 03.05.2005 Siemens / Gehring F-Version Freigabe <BR>
 *          V156_T 30.05.2007 BRAINBOW GmbH, Volker Funke / Entsch�rfung des problematischen Umgangs
 *          mit dem UserDialog (UserSelectionThread) <BR>
 *          V157_F 11.06.2007 Braunstorfer Erstellung F-Version <BR>
 *          V158_T 12.06.2008 Schumann APDM Ergebnis angepasst <BR>
 *          V159_F 16.06.2008 Schumann <BR>
 *          V160_T 16.06.2008 Gebauer Anpassung der Parameter MIN_GET, MAX_GET, START_TRIGGER_LEVEL
 *          und END_TRIGER_LEVEL f�r �bergabe referenzierte Ergebnisse (@) <BR>
 *          V161_F 15.11.2010 Gebauer F_Version <BR>
 *          V162_T 02.04.2012 Gebauer Fehlerbehebung Freigabe Devices bei einem Abbruch
 *          V163_F 03.09.2012 Gebauer Fehlerbehebung Freigabe Devices bei einem Abbruch
 *          V164_T 14.01.2013 Gebauer Timeouterweiterung f�r Messungen ohne Trigger-PP
 *          V165_F 14.01.2013 Gebauer F_Version
 *          V166_F 07.01.2015 TB Fehler beim internen Ausf�hren eines Diagnoseschrittes �ber den internen ECOS Thread beeinflussen jetzt das Gesamtergebnis dieser PP vgl. LOP 1880<BR>
 *          V167_F 18.02.2015 TB F_Version nach RM Test im W7<BR> 
 */

public class UiToleranzTotal_167_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * Execution status OK vorintialisieren
	 */
	private int status = STATUS_EXECUTION_OK;
	
	private boolean DEBUG = false;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public UiToleranzTotal_167_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public UiToleranzTotal_167_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 * 
	 * @return Stringvektor der optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "VTYP", "START_TRIGGER_LEVEL", "END_TRIGGER_LEVEL", "END_TRIGGER_REF", "REF", "BASE_VALUE_UPDATE_TIME", "WAIT", "WAIT_END", "TRIGGER_PP", "PUNKT", "MODE", "TIMEOUT", "CYCLETIME", "START_TRIG_DIR", "END_TRIG_DIR", "BASE_VALUE_STORAGE_WRITE", "BASE_VALUE_READ", "BASE_VALUE_STORAGE_READ", "WW_IS_MW", "BASE_TYPE", "BASE_FILTER", "TRIGGER_TYPE", "MESS_TYPE", "STATUS_PP", "WAIT_TRIGGER_PP", "LOGGER_ENABLE" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 * 
	 * @return Stringvektor der ben�tigten Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "RESULT1", "MIN_GET1", "MAX_GET1", "DAUER", "TRIGGER_FILTER", "MESS_FILTER", "AWT", "HWT" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 * 
	 * @return true, wenn die Ueberpruefung der zwingenden Argumente ok ist
	 */
	public boolean checkArgs() {
		String temp;
		int i, j;
		float f;
		boolean ok;

		try {
			ok = super.checkArgs();
			if( ok == true ) {
				// Parameterpr�fung, ob alle zwingenden Parameter belegt sind.

				// RESULT1 nur Strommessung, Leistungsmessung oder
				// Spitzenwertmessung (nur f�r
				// Strommessung)
				temp = getArg( getRequiredArgs()[0] ).toUpperCase();
				if( (temp.equals( "I" ) == false) && (temp.equals( "P" ) == false) && (temp.equals( "MAX" ) == false) )
					return false;

				// MIN_GET1 Der Parameter soll von Typ Integer sein
				temp = extractValues( getArg( getRequiredArgs()[1] ) )[0];

				try {
					j = Integer.parseInt( temp );
				} catch( NumberFormatException e ) {
					return false;
				}
				// MAX_GET1 Der Parameter soll von Typ Integer sein
				temp = extractValues( getArg( getRequiredArgs()[2] ) )[0];
				try {
					j = Integer.parseInt( temp );
				} catch( NumberFormatException e ) {
					return false;
				}
				// DAUER Der Parameter soll von Typ Integer sein
				try {
					j = Integer.parseInt( getArg( getRequiredArgs()[3] ) );
					if( j < 0 )
						return false;
				} catch( NumberFormatException e ) {
					return false;
				}

				// TRIGGER_FILTER Der Parameter soll von Typ Float sein
				try {
					f = Float.parseFloat( getArg( getRequiredArgs()[4] ) );
					if( f < 0 )
						return false;
				} catch( NumberFormatException e ) {
					return false;
				}

				// MESS_FILTER Der Parameter soll von Typ Float sein
				try {
					f = Float.parseFloat( getArg( getRequiredArgs()[5] ) );
					if( f < 0 )
						return false;
				} catch( NumberFormatException e ) {
					return false;
				}

				// AWT
				if( getArg( getRequiredArgs()[6] ) == null )
					return false;

				// HWT
				if( getArg( getRequiredArgs()[7] ) == null )
					return false;

				// G�ltigkeitspr�fung der Belegung aller optionalen Parameter
				// Au�er Trigger_PP, STATUS_PP und BASE_FILTER sollen alle
				// Parameterwerte von Typ
				// Integer sein.
				// (mehr Infos s. Online-Doc)
				for( i = 0; i < 25; i++ ) {
					temp = getArg( getOptionalArgs()[i] );
					if( temp != null ) {
						temp = extractValues( temp )[0];
						try {
							if( i != 8 && i != 23 && i != 20 ) {
								j = Integer.parseInt( temp );
							}
						} catch( NumberFormatException e ) {
							return false;
						}
					}
				}

				// END_TRIGGER_REF
				// END_TRIGGER_REF = 0 (DEFAULT): auf den Messwert
				// END_TRIGGER_REF = 1: auf den Grundwert vor dem Start-Trigger
				temp = getArg( getOptionalArgs()[3] );
				if( temp != null ) {
					if( (temp.equals( "0" ) == false) && (temp.equals( "1" ) == false) )
						return false;
				}

				// PUNKT
				// F�r Spitzenwertmessung darf dieser Parameter nicht gesetzt
				// werden.
				if( getArg( getOptionalArgs()[9] ) != null ) {
					temp = getArg( getRequiredArgs()[0] ).toUpperCase();
					if( (temp.equals( "MAX" ) == true) )
						return false;
				}

				// MODE muss nur den Wert 0, 1, 2, 3 annehmen
				// mode == 0 Trigger Messung Trigger
				// mode == 1 Trigger Messung Best�tigung
				// mode == 2 Trigger Messung automatischweitergehen
				// mode == 3 Ruhestrommessung
				temp = getArg( getOptionalArgs()[10] );
				if( temp != null ) {
					if( (temp.equals( "0" ) == false) && (temp.equals( "1" ) == false) && (temp.equals( "2" ) == false) && (temp.equals( "3" ) == false) )
						return false;
				}

				// START_TRIG_DIR
				// START_TRIG_DIR = 0 (DEFAULT): f�r steigende Werte
				// START_TRIG_DIR = 1: f�r fallende Werte
				temp = getArg( getOptionalArgs()[13] );
				if( temp != null ) {
					if( (temp.equals( "0" ) == false) && (temp.equals( "1" ) == false) )
						return false;
				}

				// END_TRIG_DIR
				// END_TRIG_DIR = 0 (DEFAULT): f�r fallende Werte
				// END_TRIG_DIR = 1: f�r steigende Werte
				temp = getArg( getOptionalArgs()[14] );
				if( temp != null ) {
					if( (temp.equals( "0" ) == false) && (temp.equals( "1" ) == false) )
						return false;
				}

				// BASE_VALUE_STORAGE_WRITE
				// Grundwertspeicher nimmt nur den Wert von 1 bis 13 an
				// F�r Spitzenwertmessung darf dieser Parameter nicht gesetzt
				// werden
				temp = getArg( getOptionalArgs()[15] );
				if( temp != null ) {
					if( Integer.parseInt( temp ) < 1 || Integer.parseInt( temp ) > 13 )
						return false;
					temp = getArg( getRequiredArgs()[0] ).toUpperCase();
					if( (temp.equals( "MAX" ) == true) )
						return false;
				}

				// BASE_VALUE_READ
				// 0 Grundwert lesen, Messen und Schaltwert
				// 1 Grundwert lesen, Messen
				// 2 Grundwert lesen, Schaltwert
				// 3 kein Grundwert lesen
				temp = getArg( getOptionalArgs()[16] );
				if( temp != null ) {
					if( Integer.parseInt( temp ) < 0 || Integer.parseInt( temp ) > 3 )
						return false;
				}

				// BASE_VALUE_STORAGE_READ
				// Nur 15 Speicherzellen f�r Grundwert
				temp = getArg( getOptionalArgs()[17] );
				if( temp != null ) {
					if( Integer.parseInt( temp ) < 0 || Integer.parseInt( temp ) > 15 )
						return false;
				}

				// WW=MW (Weiterschaltwert == Messwert bei der
				// Weiterschaltbedingung))
				// WW_IS_MW = 0 (DEFAULT): diese Option wird nicht
				// eingeschaltet.
				// WW_IS_MW = 1: diese Option wird eingeschaltet.
				temp = getArg( getOptionalArgs()[18] );
				if( temp != null ) {
					if( (temp.equals( "0" ) == false) && (temp.equals( "1" ) == false) )
						return false;
				}

				// BASE_FILTER Der Parameter soll von Typ Float sein
				temp = getArg( getOptionalArgs()[20] );
				if( temp != null ) {
					try {
						f = Float.parseFloat( temp );
						if( f < 0 )
							return false;
					} catch( NumberFormatException e ) {
						return false;
					}
				}

				// TRIGGER_PP und STATUS_PP Die beiden Parameter d�rfen nicht
				// gleichzeitig belegt
				// werden
				if( getArg( getOptionalArgs()[8] ) != null && getArg( getOptionalArgs()[23] ) != null )
					return false;
			}
			return ok;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		/** 1-Variablen Deklarieren und Initialisieren* */
		// Immer notwendig
		Ergebnis result;
		Ergebnis result_mode_0 = null; // result fuer mode 0 wird am ende geschriben nur wenn die pp
		// nicht abgebrochen ist
		Vector ergListe = new Vector();
		
		// Wichtige Konstanten
		final int TYP_TRIGGER_PP = 1; // Diagnosetyp
		final int TYP_STATUS_PP = 2; // Diagnosetyp
		final int TYP_NON_PP = 0; // Diagnosetyp
		// Grundwertermittlung
		final int TRIGGER_PP_WAIT = 125; // Wartezeit bevor TRIGGER_PP
		// losschickt
		final int BASE_VALUE_UPDATE_TIME = 30; // Zeitabstand f�r Aktualisierung
		// des Grundwertes

		// spezifische Variablen
		Pruefprozedur pp; // Pr�fprozedur f�r Diagnose
		UIAnalyserEcos myUI = null; // Device UIAnalyser
		BoundInfo measure = null; // Objekt f�r das Ergebnis einer Messung
		UserDialog myDialog = null; // Device Dialogbox
		EcosThread ppThread = null; // Thread der Pr�fprozedur f�r Diagnose
		UserSelectionThread selectionThread = null; // Thread f�r AWT
		boolean ECOS_TRACE_TEST_STEP = false; // bestimmt ob ein einzelnes Trace
		boolean ECOS_TRACE_CONTINUOUS = false;// aufgezeichnet wird
		String ECOS_OFFSET_ZANGE_UMSCHLOSSEN = null;

		// Variablen f�r gesamte Parameter
		int dauer = 0, vTypValue = 0, refValue = 0, istValue = 0, startTriggerValue = 0, endTriggerValue = 0, endTriggerRef = 0, minValue = 0, maxValue = 0, okTime = 0, totZeit = 0, anzahl = 0, timeout = 0, WW_is_MW = 0, minGet1 = 0, maxGet1 = 0;
		int type_mess = 0, type_trigger = 0, type_base = 0, abschaltZeit = 0, mode = 0, Zykluszeit = 0, minZyklus = 0, maxZyklus = 0, start_trig_dir = 0, end_trig_dir = 0, grundwert_storage_write = 0, grundwert_read = 0, grundwert_storage_read = 0, wait_trig_pp = TRIGGER_PP_WAIT;
		int maximum = 0, minimum = 0;
		float cutoff_mess = 0, cutoff_trigger = 0, cutoff_base = 0;
		String resultName; // Variable f�r Parameter RESULT1
		String awt = null, hwt = null; // AWT und HWT
		boolean normalize = false; // VTYP
		int loggerEnable = 1; // LOGGER_ENABLE

		// Andere nutzbare Variablen
		boolean udInUse = false; // Ob eine Meldung auf dem Handterminal
		// dargestellt wird
		boolean cancelled = false; // Ob der Pr�fschritt abgebrochen wird
		boolean absolute = false; // Ob ein Referenzwert vordefiniert wird
		boolean zuWiederholen = false; // Ob falsche Triggerung aufgetaucht ist
		boolean typWiederholen = false; // Ob die Messung wegen falscher
		// Triggerung wiederholt
		// werden muss (nur manueller Verbraucher mit negativer
		// Flanke)
		String[] choices = null; // Eine Liste von (Einstellige-)
		// Auswahlm�glichkeiten, das
		// Handterminal wird nur die Eingabe von diesen Charakter
		// akzeptieren (Expertmode)
		String resultEinheit = null; // Einheit f�r Strom- bzw. Leistungsmessung
		String nokTaste = null; // Definition der NOK-Taste f�r Expertmode
		String temp; // Tempor�re Variable
		int loopIndex = 0; // Wie viele Zahlen werden schon f�r Expertmode
		// gedr�ckt
		int diagnoseTyp = TYP_NON_PP; // Diagnosetyp (TRIGGER_PP oder STATUS_PP)
		long start, end; // Zur Definition eines Time-outs
		String Parameters = null; // Zur Speicherung aller Parameter
		final boolean OFFSETIO = true, OFFSETNIO = false;
		boolean offsetStatus = OFFSETIO;
		boolean excecuteagain = false;
		/** 1-Variablen Deklarieren und Initialisieren end* */
		do {
			// Reinit wegen excecute again.
			// ergListe = new Vector(); //war auskommentiert, vermutlich Ursache f�r Probleme mit
			// Offsetermittlung bei 150_0_F

			status = STATUS_EXECUTION_OK;
			myUI = null; // Device UIAnalyser
			measure = null; // Objekt f�r das Ergebnis einer Messung
			myDialog = null; // Device Dialogbox
			ppThread = null; // Thread der Pr�fprozedur f�r Diagnose
			selectionThread = null; // Thread f�r AWT
			ECOS_TRACE_TEST_STEP = false; // bestimmt ob ein einzelnes Trace
			ECOS_TRACE_CONTINUOUS = false;// aufgezeichnet wird
			ECOS_OFFSET_ZANGE_UMSCHLOSSEN = null;
			dauer = 0;
			vTypValue = 0;
			refValue = 0;
			istValue = 0;
			startTriggerValue = 0;
			endTriggerValue = 0;
			endTriggerRef = 0;
			minValue = 0;
			maxValue = 0;
			okTime = 0;
			totZeit = 0;
			anzahl = 0;
			timeout = 0;
			WW_is_MW = 0;
			minGet1 = 0;
			maxGet1 = 0;
			type_mess = 0;
			type_trigger = 0;
			type_base = 0;
			abschaltZeit = 0;
			mode = 0;
			Zykluszeit = 0;
			minZyklus = 0;
			maxZyklus = 0;
			start_trig_dir = 0;
			end_trig_dir = 0;
			grundwert_storage_write = 0;
			grundwert_read = 0;
			grundwert_storage_read = 0;
			wait_trig_pp = TRIGGER_PP_WAIT;
			maximum = 0;
			minimum = 0;
			cutoff_mess = 0;
			cutoff_trigger = 0;
			cutoff_base = 0;
			awt = null;
			hwt = null; // AWT und HWT
			normalize = false; // VTYP
			loggerEnable = 0; // LOGGER_ENABLE
			udInUse = false; // Ob eine Meldung auf dem Handterminal
			// dargestellt wird
			cancelled = false; // Ob der Pr�fschritt abgebrochen wird
			absolute = false; // Ob ein Referenzwert vordefiniert wird
			zuWiederholen = false; // Ob falsche Triggerung aufgetaucht ist
			typWiederholen = false; // Ob die Messung wegen falscher
			// Triggerung wiederholt
			// werden muss (nur manueller Verbraucher mit negativer
			// Flanke)
			choices = null; // Eine Liste von (Einstellige-)
			// Auswahlm�glichkeiten, das
			// Handterminal wird nur die Eingabe von diesen Charakter
			// akzeptieren (Expertmode)
			resultEinheit = null; // Einheit f�r Strom- bzw. Leistungsmessung
			nokTaste = null; // Definition der NOK-Taste f�r Expertmode
			loopIndex = 0; // Wie viele Zahlen werden schon f�r Expertmode
			// gedr�ckt
			diagnoseTyp = TYP_NON_PP; // Diagnosetyp (TRIGGER_PP oder STATUS_PP)
			Parameters = null; // Zur Speicherung aller Parameter
			offsetStatus = OFFSETIO;
			excecuteagain = false;

			try {
				/** 2-�berprufung und Zuweisung der zwingende und optionale Parameter* */
				try {
					// �berpr�fung der Parameter
					if( checkArgs() == false )
						throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

					// Holen des Wertes eines Parameters

					try {// Holen der Pr�fstandsvariable ECOS_PPS_DEBUG
						DEBUG = ((Boolean) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "ECOS_PPS_DEBUG" )).booleanValue();
					} catch( Exception e ) {
						if( e instanceof VariablesException ) {
							// Falls keine Pr�fstandsvariable angegeben ist, oder es tritt beim
							// Abholen ein Fehler auf
							// TransientenModus inaktiv
							DEBUG = false;
						} else {
							throw new PPExecutionException();
						}
					}// Ende Holen der Pr�fstandsvariablen ECOS_PPS_DEBUG
						// DEBUG = true;

					// RESULT1
					resultName = getArg( getRequiredArgs()[0] ).toUpperCase();

					// MIN_GET1
					minGet1 = Integer.parseInt( extractValues( getArg( getRequiredArgs()[1] ) )[0] );

					// MAX_GET1
					maxGet1 = Integer.parseInt( extractValues( getArg( getRequiredArgs()[2] ) )[0] );

					// DAUER
					dauer = Integer.parseInt( getArg( getRequiredArgs()[3] ) );

					// TRIGGER_FILTER
					cutoff_trigger = Float.parseFloat( getArg( getRequiredArgs()[4] ) );

					// MESS_FILTER
					cutoff_mess = Float.parseFloat( getArg( getRequiredArgs()[5] ) );

					// AWT
					awt = PB.getString( getArg( getRequiredArgs()[6] ) );

					// HWT
					hwt = PB.getString( getArg( getRequiredArgs()[7] ) );

					// VTYP
					temp = getArg( getOptionalArgs()[0] );
					if( temp == null )
						normalize = false; // Ohne Normierung
					else {
						vTypValue = Integer.parseInt( temp );
						normalize = true; // Mit Normierung
					}

					// START_TRIGGER_LEVEL
					temp = getArg( getOptionalArgs()[1] );
					if( temp != null ) {
						temp = extractValues( temp )[0];
						startTriggerValue = Integer.parseInt( temp );
						minZyklus = startTriggerValue;
					}

					// END_TRIGGER_LEVEL
					temp = getArg( getOptionalArgs()[2] );
					if( temp != null ) {
						temp = extractValues( temp )[0];
						endTriggerValue = Integer.parseInt( temp );
						maxZyklus = endTriggerValue;
					}

					// REF
					if( getArg( getOptionalArgs()[4] ) != null ) {
						refValue = Integer.parseInt( getArg( getOptionalArgs()[4] ) );
						absolute = true;
					}

					// OK_TIME_START
					temp = getArg( getOptionalArgs()[5] );
					if( temp == null )
						okTime = BASE_VALUE_UPDATE_TIME;
					else
						okTime = Integer.parseInt( temp );

					// WAIT
					if( getArg( getOptionalArgs()[6] ) != null )
						totZeit = Integer.parseInt( getArg( getOptionalArgs()[6] ) );

					// WAIT_END
					if( getArg( getOptionalArgs()[7] ) != null )
						abschaltZeit = Integer.parseInt( getArg( getOptionalArgs()[7] ) );

					// PUNKT
					if( getArg( getOptionalArgs()[9] ) != null )
						anzahl = Integer.parseInt( getArg( getOptionalArgs()[9] ) );

					// Weiterschaltbedingungen
					if( getArg( getOptionalArgs()[10] ) != null )
						mode = Integer.parseInt( getArg( getOptionalArgs()[10] ) );

					// TRIGGER_PP oder STATUS_PP
					if( getArg( getOptionalArgs()[8] ) != null ) {
						diagnoseTyp = TYP_TRIGGER_PP;
						temp = getArg( getOptionalArgs()[8] );
						if( temp.indexOf( '.' ) == -1 )
							pp = getPr�fling().getPr�fprozedur( temp );
						else
							pp = getPr�fling().getAuftrag().getPr�fprozedur( temp );
						if( pp == null )
							throw new PPExecutionException( PB.getString( "ppNichtGefunden" ) );
					} else if( getArg( getOptionalArgs()[23] ) != null ) {
						diagnoseTyp = TYP_STATUS_PP;
						temp = getArg( getOptionalArgs()[23] );
						if( temp.indexOf( '.' ) == -1 )
							pp = getPr�fling().getPr�fprozedur( temp );
						else
							pp = getPr�fling().getAuftrag().getPr�fprozedur( temp );
						if( pp == null )
							throw new PPExecutionException( PB.getString( "ppNichtGefunden" ) );
					} else {
						diagnoseTyp = TYP_NON_PP;
						pp = null;
					}

					// TIMEOUT
					if( getArg( getOptionalArgs()[11] ) != null )
						timeout = Integer.parseInt( getArg( getOptionalArgs()[11] ) );

					// CYCLETIME
					if( getArg( getOptionalArgs()[12] ) != null )
						Zykluszeit = Integer.parseInt( getArg( getOptionalArgs()[12] ) );

					// START_TRIG_DIR
					if( getArg( getOptionalArgs()[13] ) != null )
						start_trig_dir = Integer.parseInt( getArg( getOptionalArgs()[13] ) );

					// END_TRIG_DIR
					if( getArg( getOptionalArgs()[14] ) != null )
						end_trig_dir = Integer.parseInt( getArg( getOptionalArgs()[14] ) );

					// BASE_VALUE_STORAGE_WRITE
					if( getArg( getOptionalArgs()[15] ) != null )
						grundwert_storage_write = Integer.parseInt( getArg( getOptionalArgs()[15] ) );

					// BASE_VALUE_READ
					if( getArg( getOptionalArgs()[16] ) != null )
						grundwert_read = Integer.parseInt( getArg( getOptionalArgs()[16] ) );

					// BASE_VALUE_STORAGE_READ
					if( getArg( getOptionalArgs()[17] ) != null )
						grundwert_storage_read = Integer.parseInt( getArg( getOptionalArgs()[17] ) );

					// WW=MW
					if( getArg( getOptionalArgs()[18] ) != null )
						WW_is_MW = Integer.parseInt( getArg( getOptionalArgs()[18] ) );

					// BASE_TYPE
					if( getArg( getOptionalArgs()[19] ) != null )
						type_base = Integer.parseInt( getArg( getOptionalArgs()[19] ) );

					// BASE_FILTER
					if( getArg( getOptionalArgs()[20] ) != null )
						cutoff_base = Float.parseFloat( getArg( getOptionalArgs()[20] ) );
					else
						// Defaultm�ssig BaseFilter gleich Triggerfilter
						cutoff_base = cutoff_trigger;

					// TRIGGER_TYPE
					if( getArg( getOptionalArgs()[21] ) != null )
						type_trigger = Integer.parseInt( getArg( getOptionalArgs()[21] ) );

					// MESS_TYPE
					if( getArg( getOptionalArgs()[22] ) != null )
						type_mess = Integer.parseInt( getArg( getOptionalArgs()[22] ) );

					// WAIT_TRIGGER_PP
					if( getArg( getOptionalArgs()[24] ) != null )
						wait_trig_pp = Integer.parseInt( getArg( getOptionalArgs()[24] ) );

					// LOGGER_ENABLE
					if( getArg( getOptionalArgs()[25] ) != null )
						loggerEnable = Integer.parseInt( getArg( getOptionalArgs()[25] ) );

					// Ergebnisvektor, vorherige Ergebnisse der ErgListe m�ssen auf IGNORE
					// gesetzt werden, da sonst Gesamtergebnis NIO wg. vorhergehender Fehler
					for( int i = 0; i < ergListe.size(); i++ ) {
						result = (Ergebnis) ergListe.get( i );
						result.setFehlerTyp( Ergebnis.FT_IGNORE );
						ergListe.setElementAt( result, i );
					}

				} catch( PPExecutionException e ) {
					// Parametrierfehler
					e.printStackTrace();
					if( e.getMessage() != null )
						result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "parametrierfehler" ), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "parametrierfehler" ), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}
				/** 2-�berprufung und Zuweisung der zwingende und optionale Parameter end* */
				/**
				 * 3-Bewertung von Pr�fstandvariablen ECOS_TRACE_TEST_STEP und
				 * ECOS_TRACE_CONTINUOUS*
				 */
				try {// Holen der Pr�fstandsvariablen ECOS_TRACE_TEST_STEP
					ECOS_TRACE_TEST_STEP = ((Boolean) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "ECOS_TRACE_TEST_STEP" )).booleanValue();
				} catch( Exception e ) {
					if( e instanceof VariablesException ) {
						// Falls keine Pr�fstandsvariable angegeben ist, oder es tritt beim Abholen
						// ein Fehler auf
						// TransientenModus inaktiv
						ECOS_TRACE_TEST_STEP = false;
					} else {
						throw new PPExecutionException();
					}
				}// Ende Holen der Pr�fstandsvariablen ECOS_TRACE_TEST_STEP

				try {// Holen der Pr�fstandsvariablen ECOS_TRACE_CONTINUOUS
					ECOS_TRACE_CONTINUOUS = ((Boolean) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "ECOS_TRACE_CONTINUOUS" )).booleanValue();
				} catch( Exception e ) {
					if( e instanceof VariablesException ) {
						// Falls keine Pr�fstandsvariable angegeben ist, oder es tritt beim Abholen
						// ein Fehler auf
						// TransientenModus inaktiv
						ECOS_TRACE_CONTINUOUS = false;
					} else {
						throw new PPExecutionException();
					}
				}// Ende Holen der Pr�fstandsvariablen ECOS_TRACE_CONTINUOUS

				// ECOS_TRACE_CONTINUOUS ist dominant
				if( ECOS_TRACE_CONTINUOUS ) {
					ECOS_TRACE_TEST_STEP = false;
				}
				// ECOS_OFFSET_ZANGE_UMSCHLOSSEN
				try {
					ECOS_OFFSET_ZANGE_UMSCHLOSSEN = ((String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "ECOS_OFFSET_ZANGE_UMSCHLOSSEN" ));
					if( DEBUG ) {
						System.out.println( "ECOS_OFFSET_ZANGE_UMSCHLOSSEN: " + ECOS_OFFSET_ZANGE_UMSCHLOSSEN );
					}
				} catch( Exception e ) {
					if( e instanceof VariablesException ) {
						ECOS_OFFSET_ZANGE_UMSCHLOSSEN = null;
					} else {
						throw new PPExecutionException();
					}
				}// Ende Holen der Pr�fstandsvariablen ECOS_OFFSET_ZANGE_UMSCHLOSSEN

				/**
				 * 3-Bewertung von Pr�fstandvariablen ECOS_TRACE_TEST_STEP und ECOS_TRACE_CONTINUOUS
				 * end*
				 */
				/**
				 * 4-Holen des Devices UIAnalyser und Abstimmen dessen Parametern f�r
				 * Transientenaufnahme*
				 */
				try {
					// Holen des Devices UIAnalyser
					if( DEBUG ) {
						System.out.println( "UiToleranzTotal: UiAnalyser get." );
					}
					myUI = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();

					// parallelen Thread stoppen
					myUI.startMess();

					// Negative Stromwerte werden nicht zugelassen
					myUI.reset( false );

					if( !(ECOS_TRACE_TEST_STEP) ) {// Pr�fschrittlog ausschalten falls
						// ECOS_TRACE_TEST_STEP=false
						myUI.setLog( false );
						loggerEnable = 0;
					}

					myUI.setVerbraucher( this.getName() ); // Verbrauchername wird �bergegeben
					if( DEBUG ) {
						System.out.println( "UiToleranzTotal: " + this.getName() );
					}
					// wenn die Funktion f�r die Transientenaufnahme eingeschaltet ist
					if( myUI.getLog() == true || loggerEnable == 1 ) {
						// Erzeugt einen LOG-Dateiname mit FzgstNr-7
						if( loggerEnable == 1 ) {
							myUI.setLog( true );
						}
						Date date = new Date();
						SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy_MM_dd" );
						// SimpleDateFormat dateTimeFormat = new
						// SimpleDateFormat("yyyyMMdd_HHmmss");
						String FileName = CascadeProperties.getCascadeHome() + File.separator + "trace" + File.separator + "ecos" + File.separator + dateFormat.format( date ) + File.separator + // Tag/Ordner
						getPr�fling().getAuftrag().getFahrgestellnummer7();
						// + "_" + dateTimeFormat.format(date);
						myUI.prepareLog( FileName, false );
					}

					// Erstellt die Parameterliste
					Parameters = "RESULT1             \t" + resultName + "\nMIN_GET1            \t" + minGet1 + "\nMAX_GET1            \t" + maxGet1 + "\nDAUER               \t" + dauer;

					if( totZeit != 0 )
						Parameters = Parameters + "\nWAIT                \t" + totZeit;
					if( getArg( getOptionalArgs()[20] ) != null )
						Parameters = Parameters + "\nBASE_FILTER         \t" + (int) cutoff_base;
					Parameters = Parameters + "\nTRIGGER_FILTER      \t" + (int) cutoff_trigger;
					Parameters = Parameters + "\nMESS_FILTER         \t" + (int) cutoff_mess;

					if( startTriggerValue != 0 )
						Parameters = Parameters + "\nSTART_TRIGGER_LEVEL \t" + startTriggerValue;
					if( endTriggerValue != 0 )
						Parameters = Parameters + "\nEND_TRIGGER_LEVEL   \t" + endTriggerValue;
					// END_TRIGGER_REF
					temp = getArg( getOptionalArgs()[3] );
					if( temp != null ) {
						endTriggerRef = Integer.parseInt( temp );
						Parameters = Parameters + "\nEND_TRIGGER_REF     \t" + endTriggerRef;
					}

					// START_TRIG_DIR
					if( getArg( getOptionalArgs()[13] ) != null )
						Parameters = Parameters + "\nSTART_TRIG_DIR     \t" + start_trig_dir;

					// END_TRIG_DIR
					if( getArg( getOptionalArgs()[14] ) != null )
						Parameters = Parameters + "\nEND_TRIG_DIR       \t" + end_trig_dir;

					if( getArg( getOptionalArgs()[10] ) != null )
						Parameters = Parameters + "\nMODE                \t" + mode;

					// CYCLETIME
					if( getArg( getOptionalArgs()[12] ) != null )
						Parameters = Parameters + "\nCYCLETIME          \t" + Zykluszeit;

					if( anzahl != 0 )
						Parameters = Parameters + "\nPUNKT               \t" + anzahl;

					// VTYP
					if( getArg( getOptionalArgs()[0] ) != null )
						Parameters = Parameters + "\nVTYP          \t" + vTypValue;

					// TRIGGER_PP oder STATUS_PP
					if( getArg( getOptionalArgs()[8] ) != null ) {
						Parameters = Parameters + "\nTRIGGER_PP " + pp;
					}
					if( getArg( getOptionalArgs()[23] ) != null ) {
						Parameters = Parameters + "\nSTATUS_PP " + pp;
					}

					// WAIT_TRIGGER_PP
					if( getArg( getOptionalArgs()[21] ) != null )
						Parameters = Parameters + "\nWAIT_TRIGGER_PP    \t" + wait_trig_pp;

					// REF
					if( getArg( getOptionalArgs()[4] ) != null ) {
						Parameters = Parameters + "\nREF           \t" + refValue;
						// Parameters = Parameters + "\nAbsolute \t" + absolute;
					}

					// BASE_VALUE_UPDATE_TIME oder OK_TIME_START
					if( getArg( getOptionalArgs()[5] ) != null )
						Parameters = Parameters + "\nBASE_VALUE_UPDATE_TIME       \t" + okTime;

					// BASE_VALUE_STORAGE_WRITE
					if( getArg( getOptionalArgs()[15] ) != null )
						Parameters = Parameters + "\nBASE_VALUE_STORAGE_WRITE \t" + grundwert_storage_write;

					// BASE_VALUE_STORAGE_READ
					if( getArg( getOptionalArgs()[17] ) != null )
						Parameters = Parameters + "\nBASE_VALUE_STORAGE_READ \t" + grundwert_storage_read;

					// BASE_VALUE_READ
					if( getArg( getOptionalArgs()[16] ) != null )
						Parameters = Parameters + "\nBASE_VALUE_READ    \t" + grundwert_read;

					// TIMEOUT
					if( getArg( getOptionalArgs()[11] ) != null )
						Parameters = Parameters + "\nTIMEOUT            \t" + timeout;

					// WAIT_END
					if( getArg( getOptionalArgs()[7] ) != null )
						Parameters = Parameters + "\nWAIT_END            \t" + abschaltZeit;

					// WW=MW
					if( getArg( getOptionalArgs()[18] ) != null )
						Parameters = Parameters + "\nWW_IS_MW           \t" + WW_is_MW;

					// LOGGER_ENABLE
					if( getArg( getOptionalArgs()[22] ) != null )
						Parameters = Parameters + "\nLOGGER_ENABLE      \t" + loggerEnable;

					// AWT
					Parameters = Parameters + "\nAWT " + awt;

					// HWT
					Parameters = Parameters + "\nHWT " + hwt;

					myUI.writeParameters( Parameters );
				} catch( Exception e ) {
					// Systemfehler, muss man genau analysieren
					e.printStackTrace();
					if( e instanceof DeviceLockedException )
						result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
					else if( e instanceof DeviceNotAvailableException )
						result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "DeviceNotAvailableException", Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "Exception", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}
				/**
				 * 4-Holen des Devices UIAnalyser und Abstimmen dessen Parametern f�r
				 * Transientenaufnahme end*
				 */
				/** 5-Holen des NOK-Taste des Handterminals f�r weitere N�tzen* */
				// Holen der NOK-Taste des Handterminals
				try {
					nokTaste = getPr�flingLaufzeitUmgebung().getDeviceManager().getHandterminal().getNokTaste();
					getPr�flingLaufzeitUmgebung().getDeviceManager().releaseHandterminal();
				} catch( Exception e ) {
					nokTaste = " ";
				}
				/** 5-Holen des NOK-Taste des Handterminals f�r weitere N�tzen end* */

				// mode == 0 Trigger Messung Trigger
				// mode == 1 Trigger Messung Best�tigung
				// mode == 2 Trigger Messung automatischweitergehen
				// mode == 3 Ruhestrommessung
				/** 6-Handlung f�r Start Trigger falls Trigger Messung (nicht Ruhestrommessung)* */
				if( !myUI.getLastReadError() ) {

					if( DEBUG ) {
						System.out.println( "UiToleranzTotal: Batterieadapter angeschlossen." );
					}

					if( mode == 0 || mode == 1 || mode == 2 ) {
						// Stellt Triggerfilter ein
						myUI.messFilter.setFilter( cutoff_trigger, type_trigger );

						// Ob die Messung wegen falschen Triggerung wiederholt werden muss (nur
						// manueller
						// Verbraucher mit negative Flanke)
						if( mode == 0 && pp == null )
							typWiederholen = true;

						do {

							myUI.baseFilter.setFilter( cutoff_base, UIFilter.LOW_PASS );

							// AWT wird ausgegeben. Falls Diagnose-Pr�fprozedur vorhanden ist, wird
							// sie ausgef�hrt.
							repeatLoop: do {
								// Definition der aktiven Tasten f�r Expertmode
								// NOK-Taste kann man jederzeit dr�cken (Abbruch)
								if( loopIndex == 0 ) {
									choices = new String[] { nokTaste, "8" }; // Am
									// Anfang ist nur Taste 8 aktivert
								}
								try {
									myDialog = getPr�flingLaufzeitUmgebung().getUserDialog(); // Holen
									// des
									// Devices
									// Dialogbox
									selectionThread = new UserSelectionThread( myDialog, PB.getString( "anweisung" ), awt, choices );
									selectionThread.start(); // AWT wird ausgegeben im
									// eigenen Prozess
									udInUse = true;
								} catch( Exception e ) {
									// Systemfehler, muss man genau analysieren
									e.printStackTrace();
									result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
									ergListe.add( result );
									throw new PPExecutionException( "Get UserDialog" );
								}

								// Grundwert bestimmen
								try {
									myUI.initGrundWertPuffer( false );
									myUI.grundWert = 0; // Initialisiert den Grundwert
									if( absolute == false ) { // Wenn keine absolute
										// Messung ist, wird der
										// erste Grundwert ermittelt
										if( normalize == false ) {
											if( resultName.equals( "P" ) == true )
												measure = myUI.getPowerBaseValueBoundInfo( true, BASE_VALUE_UPDATE_TIME );
											else
												measure = myUI.getCurrentBaseValueBoundInfo( true, BASE_VALUE_UPDATE_TIME );
										} else {
											if( resultName.equals( "P" ) == true )
												measure = myUI.getNormalizedPowerBaseValueBoundInfo( UIAnalyser.NORM_VOLTAGE, vTypValue, true, BASE_VALUE_UPDATE_TIME );
											else
												measure = myUI.getNormalizedCurrentBaseValueBoundInfo( UIAnalyser.NORM_VOLTAGE, vTypValue, true, BASE_VALUE_UPDATE_TIME );
										}

										myUI.grundWert = measure.getAverageValue();

									} else { // Wenn absolute Messung ist, wird der
										// Referenzwert als
										// Grundwert abgespeichert
										myUI.refWert = refValue;
									}
								} catch( CurrentProbeException e ) {
									// Keine Daten kommen f�r zwei Sekunden lang aus dem
									// COM-Port
									e.printStackTrace();
									throw new CurrentProbeException( "No Data from Serial Port" );
								}/*
									* catch (UIOffsetErrorException e) { // Offsetmessung fehlgeschlage
									* oder noch aktiv e.printStackTrace(); throw e; }
									*/catch( Exception e ) {
									// Systemfehler, muss man genau analysieren
									e.printStackTrace();
									result = new Ergebnis( "Start Value", "UIAnalyser", "", "", BASE_VALUE_UPDATE_TIME + "ms", "", "", "", "", "", "", "", "", e.getMessage(), "", Ergebnis.FT_NIO_SYS );
									ergListe.add( result );
									if( e instanceof UIOffsetErrorException )
										throw e;
									throw new PPExecutionException( "Get Device UIAnalyser and start value" );
								}

								// Trigger_PP starten
								try {
									if( diagnoseTyp == TYP_TRIGGER_PP ) { // Wenn
										// TRIGGER_PP
										// vorhanden
										// ist,
										// wird sie sofort ausgef�hrt
										ppThread = new EcosThread( info, pp, wait_trig_pp ); // TRIGGER_PP
										// wird als
										// eigener
										// Prozess
										// ausgef�hrt
										ppThread.start();
									}
								} catch( Exception e ) {
									// Systemfehler, muss man genau analysieren
									e.printStackTrace();
									result = new Ergebnis( "ExecFehler", "EcosThread Trigger PP start", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
									ergListe.add( result );
									throw new PPExecutionException( "Ecos Thread Trigger PP start" );
								}

								// Warte auf das Erreichen des Einschaltwertes
								try {
									if( timeout == 0 )
										timeout = 3600000; // Timeout ist einstellbar
									start = System.currentTimeMillis();
									end = 0;
									if( diagnoseTyp == TYP_NON_PP || diagnoseTyp == TYP_STATUS_PP ) { // F�r
										// manuellen
										// Verbraucher
										if( normalize == false ) { // Die Werte sind
											// nicht normalisiert
											if( resultName.equals( "P" ) == true ) { // Leistungsmessung
												while( (end < timeout) && selectionThread.getSelection() == null ) { // Nur
													// wenn
													// man das
													// Programm
													// nicht
													// abgebrochen
													// hat
													// System.out.println("++++++++++++
													// Waiting for trigger
													// ++++++++++++");
													if( myUI.waitForPowerBoundNoTime( startTriggerValue, 2, okTime, absolute, start_trig_dir, end_trig_dir, grundwert_read, grundwert_storage_read ) == true ) {
														break;
													} else {
														end = System.currentTimeMillis() - start;
													}
												}
											} else { // Strommessung
												while( (end < timeout) && selectionThread.getSelection() == null ) { // Nur
													// wenn
													// man
													// das
													// Programm
													// nicht
													// abgebrochen
													// hat
													if( myUI.waitForCurrentBoundNoTime( startTriggerValue, 2, okTime, absolute, start_trig_dir, end_trig_dir, grundwert_read, grundwert_storage_read ) == true ) {
														break;
													} else {
														end = System.currentTimeMillis() - start;
													}
												}
											}
										} else { // Die Werte sind normalisiert
											if( resultName.equals( "P" ) == true ) { // Leistungsmessung
												while( (end < timeout) && selectionThread.getSelection() == null ) {
													if( myUI.waitForNormalizedPowerBoundNoTime( UIAnalyser.NORM_VOLTAGE, vTypValue, startTriggerValue, 2, okTime, absolute, start_trig_dir, end_trig_dir, grundwert_read, grundwert_storage_read ) == true ) {
														break;
													} else {
														end = System.currentTimeMillis() - start;
													}
												}
											} else { // Strommessung
												while( (end < timeout) && selectionThread.getSelection() == null ) {
													if( myUI.waitForNormalizedCurrentBoundNoTime( UIAnalyser.NORM_VOLTAGE, vTypValue, startTriggerValue, 2, okTime, absolute, start_trig_dir, end_trig_dir, grundwert_read, grundwert_storage_read ) == true ) {
														break;
													} else {
														end = System.currentTimeMillis() - start;
													}
												}
											}
										}
									} else { // F�r durch TRIGGER_PP angesteuerten Verbraucher
										if( timeout == 0 )
											timeout = 3600000; // Timeout ist einstellbar
										start = System.currentTimeMillis();
										end = 0;
										if( normalize == false ) {
											if( resultName.equals( "P" ) == true ) {
												while( (end < timeout) && (selectionThread.getSelection() == null) ) {
													if( myUI.waitForPowerBoundNoTime( startTriggerValue, 2, okTime, absolute, start_trig_dir, end_trig_dir, grundwert_read, grundwert_storage_read ) == true ) {
														break;
													} else {
														end = System.currentTimeMillis() - start;
													}
												}
												if( end >= timeout ) {
													status = STATUS_EXECUTION_ERROR;
												}
											} else {
												while( (end < timeout) && (selectionThread.getSelection() == null) ) {
													if( myUI.waitForCurrentBoundNoTime( startTriggerValue, 2, okTime, absolute, start_trig_dir, end_trig_dir, grundwert_read, grundwert_storage_read ) == true ) {
														break;
													} else {
														end = System.currentTimeMillis() - start;
													}
												}
												if( end >= timeout ) {
													status = STATUS_EXECUTION_ERROR;
												}
											}
										} else {
											if( resultName.equals( "P" ) == true ) {
												while( (end < timeout) && (selectionThread.getSelection() == null) ) {
													if( myUI.waitForNormalizedPowerBoundNoTime( UIAnalyser.NORM_VOLTAGE, vTypValue, startTriggerValue, 2, okTime, absolute, start_trig_dir, end_trig_dir, grundwert_read, grundwert_storage_read ) == true ) {
														break;
													} else {
														end = System.currentTimeMillis() - start;
													}
												}
												if( end >= timeout ) {
													status = STATUS_EXECUTION_ERROR;
												}
											} else {
												while( (end < timeout) && (selectionThread.getSelection() == null) ) {
													if( myUI.waitForNormalizedCurrentBoundNoTime( UIAnalyser.NORM_VOLTAGE, vTypValue, startTriggerValue, 2, okTime, absolute, start_trig_dir, end_trig_dir, grundwert_read, grundwert_storage_read ) == true ) {
														break;
													} else {
														end = System.currentTimeMillis() - start;
													}
												}
												if( end >= timeout ) {
													status = STATUS_EXECUTION_ERROR;
												}
											}
										}
										if( status != STATUS_EXECUTION_OK ) {
											result = new Ergebnis( "Diagnostics", "TRIGGER_PP", "", "", "", pp.getPr�fling().getName() + "." + pp.getName(), "" + status, "" + STATUS_EXECUTION_OK, "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
											if( myDialog.isCancelled() == true ) { // Wenn Wecker
												// Abbruchtaste
												// dr�ckt
												ergListe = new Vector();
												result = new Ergebnis( PB.getString( "messung" ), PB.getString( "Werker" ), "", "", "", "", "", "", "", "", "", PB.getString( "werkerAbbruch" ), "", "", "", Ergebnis.FT_NIO );
												ergListe.add( result );
												excecuteagain = false;
												throw new PPExecutionException();
											} else if( mode != 0 ) {
												ergListe.add( result );
											} else {
												result_mode_0 = result;
											}
											throw new PPExecutionException();
										}
									}
								} catch( CurrentProbeException e ) {
									// Keine Daten kommt f�r zwei Sekunden lang aus dem
									// COM-Port
									e.printStackTrace();
									throw new CurrentProbeException( "No Data from Serial Port" );
								}/*
									* catch (UIOffsetErrorException e) { // Offsetmessung fehlgeschlage
									* oder noch aktiv e.printStackTrace(); throw e; }
									*/catch( Exception e ) {
									// Systemfehler, muss man genau analysieren
									e.printStackTrace();
									result = new Ergebnis( "Trigger Measurement", "UIAnalyser", "", "", "", "", "", "", "", "", "", "", "", e.getMessage(), "", Ergebnis.FT_NIO );
									ergListe.add( result );
									if( e instanceof UIOffsetErrorException )
										throw e;
									throw new PPExecutionException( "Error in trigger measurement" );
								}

								// Expertmode
								if( udInUse == true ) {
									if( loopIndex == 0 ) { // Beim ersten Tastendruck
										if( selectionThread.getSelection() != null ) {
											try {
												if( selectionThread.getSelection().equalsIgnoreCase( nokTaste ) ) { // Wenn
													// NOK
													// Taste
													// gedr�ckt
													// wird,
													// wird
													// das
													// Programm
													// abgebrochen
													cancelled = true;
													status = STATUS_EXECUTION_ERROR;
													getPr�flingLaufzeitUmgebung().releaseUserDialog();
													udInUse = false;
													break;
												} else if( selectionThread.getSelection().equalsIgnoreCase( "8" ) ) { // Wenn
													// Taste
													// 8
													// gedr�ckt
													// wird,
													// wird
													// auf
													// weiteren
													// Tastendruck
													// gewartet
													loopIndex = 1;
													getPr�flingLaufzeitUmgebung().releaseUserDialog();
													udInUse = false;
													continue repeatLoop;
												}
												cancelled = true;
												status = STATUS_EXECUTION_ERROR;
												getPr�flingLaufzeitUmgebung().releaseUserDialog();
												udInUse = false;
											} catch( Exception e ) {
												// Systemfehler, muss man genau
												// analysieren
												e.printStackTrace();
												result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
												ergListe.add( result );
												throw new PPExecutionException( "Get UserDialog" );
											}
										}
										break;
									}
								}
							} while( true ); // repeatLoop

							/*
							 * Ab Version 156: Ab hier wird der UserSelectionThread nicht mehr
							 * ben�tigt. Daher schliessen wir einen eventuell noch offenen
							 * UserDialog aus dem parallel laufenden UserSelectionThread. Damit
							 * beheben wir ein Fehlerszenario, welches unregelm��ig auftritt: Ein
							 * releaseUserDialog, gefolgt von einem getUserDialog bewirkt ein
							 * "Entriegeln" des UserSelectionThreads und ein zeitlich verz�gertes
							 * clearScreen auf dem Handterminal. Daher wird eine Meldung, die
							 * abgesetzt wird, durch ein zeitlich verz�gertes clearScreen wieder
							 * gel�scht.
							 */
							getPr�flingLaufzeitUmgebung().releaseUserDialog();
							try {
								selectionThread.join( 500 ); // wir warten auf das Ende des
								// SelectionThreads
							} catch( InterruptedException iex ) {
								// wird ignoriert
							}
							if( (status == STATUS_EXECUTION_OK) && (totZeit != 0) && typWiederholen == true ) {
								try {
									myUI.messFilter.setFilter( cutoff_mess, type_mess );

									zuWiederholen = myUI.getRepeatInfo( true, totZeit, normalize, UIAnalyser.NORM_VOLTAGE, vTypValue );
									if( zuWiederholen == true ) {
										try {
											loopIndex = 0;
											getPr�flingLaufzeitUmgebung().releaseUserDialog();
											udInUse = false;
											result = new Ergebnis( "Falsche Triggerung", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", "Error Trigger", "", Ergebnis.FT_IGNORE );
											if( myDialog.isCancelled() == true ) { // Wenn Wecker
												// Abbruchtaste
												// dr�ckt
												ergListe = new Vector();
												result = new Ergebnis( PB.getString( "messung" ), PB.getString( "Werker" ), "", "", "", "", "", "", "", "", "", PB.getString( "werkerAbbruch" ), "", "", "", Ergebnis.FT_NIO );
												ergListe.add( result );
												excecuteagain = false;
												throw new PPExecutionException();
											} else if( mode != 0 ) {
												ergListe.add( result );
											} else {
												result_mode_0 = result;
											}
										} catch( Exception e ) {
											// Systemfehler, muss man genau analysieren
											e.printStackTrace();
											result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
											ergListe.add( result );
											throw new PPExecutionException( "Get UserDialog" );
										}
									}
								} catch( CurrentProbeException e ) {
									// Keine Daten kommt f�r zwei Sekunden lang aus dem COM-Port
									e.printStackTrace();
									throw new CurrentProbeException( "No Data from Serial Port" );
								} catch( Exception e ) {
									// Systemfehler, muss man genau analysieren
									e.printStackTrace();
									result = new Ergebnis( "Waiting Deadtime", "UIAnalyser", "", "", totZeit + "ms", "", "", "", "", "", "", "", "", e.getMessage(), "", Ergebnis.FT_NIO );
									ergListe.add( result );
									if( e instanceof UIOffsetErrorException )
										throw e;
									throw new PPExecutionException( "waiting deadtime" );
								}
							}
						} while( status == STATUS_EXECUTION_OK && zuWiederholen == true );
						// Bei der Falschen Triggerung wird der Grundwert erneut ermittelt
					}
					/**
					 * 6-Handlung f�r Start Trigger falls Trigger Messung (nicht Ruhestrommessung)
					 * end*
					 */

					/**
					 * 7-Zur�ck setzen von Filter Parameter falls manueller Verbraucher mit
					 * Endtrigger und keine Totzeit*
					 */
					if( status == STATUS_EXECUTION_OK ) {
						if( (totZeit == 0) || (typWiederholen == false) ) {
							try {
								myUI.messFilter.setFilter( cutoff_mess, type_mess ); // Setzt den
								// messFilter
								// zur�ck
								/*
								 * Es ist nicht mehr n�tig. if (myUI.getLog() == true) {
								 * myUI.setlog_messFilter(cutoff_mess, type_mess); }
								 */
							} catch( Exception e ) {
								// Systemfehler, muss man genau analysieren
								e.printStackTrace();
								result = new Ergebnis( "Waiting Deadtime", "UIAnalyser", "", "", totZeit + "ms", "", "", "", "", "", "", "", "", e.getMessage(), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								throw new PPExecutionException( "waiting deadtime" );
							}
						}
					}
					/**
					 * 7-Zur�ck setzen von Filter Parameter falls manueller Verbraucher mit
					 * Endtrigger und keine Totzeit end*
					 */

					/**
					 * 8-Falls Ruhestrommessung, zeige die richtige Anweisung, merke dass User
					 * Dialog belegt ist*
					 */
					// Ruhestrommessung
					if( mode == 3 ) {
						if( DEBUG ) {
							System.out.println( "UiToleranzTotal: Ruhestrommessung od. mode 3." + "\n" );
						}
						try {
							// AWT wird auf dem Bildschirm dargestellt
							myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
							myDialog.displayMessage( PB.getString( "anweisung" ), awt, -1 );
							udInUse = true;
						} catch( Exception e ) {
							// Systemfehler, muss man genau analysieren
							e.printStackTrace();
							result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException( "Get UserDialog" );
						}
					}
					/**
					 * 8-Falls Ruhestrommessung, zeige die richtige Anweisung, merke dass User
					 * Dialog belegt ist end*
					 */

					/** 9-Warte f�r Totziet* */
					// Totzeit f�r Verbraucher wenn typWiederholen == false ist
					if( (status == STATUS_EXECUTION_OK) && (totZeit != 0) && typWiederholen == false ) {
						try {
							myUI.wait( true, totZeit, normalize, UIAnalyser.NORM_VOLTAGE, vTypValue );
						} catch( CurrentProbeException e ) {
							// Keine Daten kommt f�r zwei Sekunden lang aus dem COM-Port
							e.printStackTrace();
							throw new CurrentProbeException( "No Data from Serial Port" );
						}/*
							* catch (UIOffsetErrorException e) { // Offsetmessung fehlgeschlage oder
							* noch aktiv e.printStackTrace(); throw e; }
							*/catch( Exception e ) {
							// Systemfehler, muss man genau analysieren
							e.printStackTrace();
							result = new Ergebnis( "Waiting Deadtime", "UIAnalyser", "", "", totZeit + "ms", "", "", "", "", "", "", "", "", e.getMessage(), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							if( e instanceof UIOffsetErrorException )
								throw e;
							throw new PPExecutionException( "waiting deadtime" );
						}
					}
					/** 9-Warte f�r Totziet end* */

					/** 10-Starte die Diagnosejob wenn STATUS_PP belegt ist* */
					// Wenn STATUS_PP belegt ist, wird sie ausgef�hrt
					if( status == STATUS_EXECUTION_OK ) {
						if( diagnoseTyp == TYP_STATUS_PP ) {
							ppThread = new EcosThread( info, pp, 0 );
							ppThread.start();
						}
					}
					/** 10-Starte die Diagnosejob wenn STATUS_PP belegt ist end* */

					/***********************************************************************************************************************************************************************************
					 * 11-If BASE_VALUE_STORAGE_READ == 15, Grundwert in Speicherzelle 14 f�r
					 * n�chste Pr�fschritt speichern (Dynamischer Grundwertspeicher)
					 **********************************************************************************************************************************************************************************/
					// Grundwert wird in Speicherzell 14 kopiert
					if( status == STATUS_EXECUTION_OK ) {
						if( grundwert_storage_read == 15 ) {
							myUI.Grundwertspeicher[13] = myUI.grundWert;
						}
					}
					/***********************************************************************************************************************************************************************************
					 * 11-If BASE_VALUE_STORAGE_READ == 15, Grundwert in Speicherzelle 14 f�r
					 * n�chste Pr�fschritt speichern (Dynamischer Grundwertspeicher) end
					 **********************************************************************************************************************************************************************************/
					// Ab hier f�ngt die richtige Messung an
					if( status == STATUS_EXECUTION_OK ) {
						if( resultName.equals( "P" ) == true ) {
							resultEinheit = "mW";
						} else {
							resultEinheit = "mA";
						}
						// Mittelwertmessung
						if( anzahl == 0 ) {
							/** 12-Mittelwertmessung* */
							if( DEBUG ) {
								System.out.println( "UiToleranzTotal: Mittelwertmessung." + "\n" );
							}
							try {
								if( normalize == false ) {
									if( resultName.equals( "P" ) == true )
										measure = myUI.getPowerBoundInfo( true, dauer );
									else
										measure = myUI.getCurrentBoundInfo( true, dauer );
								} else {
									if( resultName.equals( "P" ) == true )
										measure = myUI.getNormalizedPowerBoundInfo( UIAnalyser.NORM_VOLTAGE, vTypValue, true, dauer );
									else
										measure = myUI.getNormalizedCurrentBoundInfo( UIAnalyser.NORM_VOLTAGE, vTypValue, true, dauer );
								}
							} catch( CurrentProbeException e ) {
								// Keine Daten kommt f�r zwei Sekunden lang aus dem
								// COM-Port
								e.printStackTrace();
								throw new CurrentProbeException( "No Data from Serial Port" );
							}/*
								* catch (UIOffsetErrorException e) { // Offsetmessung fehlgeschlage
								* oder noch aktiv e.printStackTrace(); throw e; }
								*/catch( Exception e ) {
								// Systemfehler, muss man genau analysieren
								e.printStackTrace();
								result = new Ergebnis( "Average Value Measurement", "UIAnalyser", "", "", dauer + "ms", "", "", "", "", "", "", "", "", e.getMessage(), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								if( e instanceof UIOffsetErrorException )
									throw e;
								throw new PPExecutionException( "The average value measurement " );
							}

							/* The real analysis */
							if( status == STATUS_EXECUTION_OK ) {
								try {
									if( (mode != 3) && (absolute == false) ) { // Keine
										// Ruhestrommessung
										// und
										// keine absolute Messung
										if( grundwert_read == 2 || grundwert_read == 3 ) { // Grundwert
											// lesen,
											// Schaltwert
											// oder kein
											// Grundwert
											// lesen
											refValue = 0; // Setzt Grundwert auf 0
										} else {
											if( (grundwert_storage_read == 15) || (grundwert_storage_read == 0) ) { // BASE_VALUE_STORAGE_READ
												// ist
												// nicht
												// belegt
												refValue = myUI.grundWert;
											} else { // BASE_VALUE_STORAGE_READ ist
												// belegt
												// (1<grundwert_storage_read <=14)
												refValue = myUI.Grundwertspeicher[grundwert_storage_read - 1];
											}
										}
									}
									// Holen der Werte von Toleranzbereich
									minValue = minGet1 + refValue;
									maxValue = maxGet1 + refValue;

									// Holen des Ist-Wertes
									istValue = myUI.getLastExecutionInfo().getSampledValue();
									maximum = measure.getMaxValue();
									minimum = measure.getMinValue();

									// Ist-Wert wird abgespeichert
									myUI.messWert = istValue;

									// Soll- und Ist-Wert-Verglich
									if( resultName.equals( "MAX" ) == true ) {
										if( (minimum < minValue) || (maximum > maxValue) ) { // Ist-Wert
											// nicht i.O.
											if( minimum < minValue ) {
												if( minimum < refValue ) {
													result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", resultName + "(" + resultEinheit + ")", "" + (minimum - refValue), "" + (minValue - refValue), "" + (maxValue - refValue), "", "", "", awt, PB.getString( "messkleingrund" ), hwt, Ergebnis.FT_NIO );
												} else {
													result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", resultName + "(" + resultEinheit + ")", "" + (minimum - refValue), "" + (minValue - refValue), "" + (maxValue - refValue), "", "", "", awt, (minValue - refValue) + "<" + (minimum - refValue) + ">" + (maxValue - refValue), hwt, Ergebnis.FT_NIO );
												}
											} else {
												if( maximum < refValue ) {
													result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", resultName + "(" + resultEinheit + ")", "" + (maximum - refValue), "" + (minValue - refValue), "" + (maxValue - refValue), "", "", "", awt, PB.getString( "messkleingrund" ), hwt, Ergebnis.FT_NIO );
												} else {
													result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", resultName + "(" + resultEinheit + ")", "" + (maximum - refValue), "" + (minValue - refValue), "" + (maxValue - refValue), "", "", "", awt, (minValue - refValue) + "<" + (maximum - refValue) + ">" + (maxValue - refValue), hwt, Ergebnis.FT_NIO );
												}
											}
											status = STATUS_EXECUTION_ERROR;
										} else { // Ist-Wert i.O.
											result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", resultName + "(" + resultEinheit + ")", "" + (istValue - refValue), "" + (minValue - refValue), "" + (maxValue - refValue), "", "", "", "", "", "", Ergebnis.FT_IO );
										}
									} else {
										// Wenn BASE_VALUE_STORAGE_WRITE belegt ist,
										// wird Ist-Wert in einem
										// Grundwertspeicher abgelegt
										if( grundwert_storage_write != 0 ) {
											myUI.Grundwertspeicher[grundwert_storage_write - 1] = istValue;
										}
										if( (istValue < minValue) || (istValue > maxValue) ) { // Ist-Wert
											// nicht i.O.
											if( istValue < refValue ) {
												result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", resultName + "(" + resultEinheit + ")", "" + (istValue - refValue), "" + (minValue - refValue), "" + (maxValue - refValue), "", "", "", awt, PB.getString( "messkleingrund" ), hwt, Ergebnis.FT_NIO );
											} else {
												result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", resultName + "(" + resultEinheit + ")", "" + (istValue - refValue), "" + (minValue - refValue), "" + (maxValue - refValue), "", "", "", awt, (minValue - refValue) + "<" + (istValue - refValue) + ">" + (maxValue - refValue), hwt, Ergebnis.FT_NIO );
											}
											status = STATUS_EXECUTION_ERROR;
										} else { // Ist-Wert i.O.
											result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", resultName + "(" + resultEinheit + ")", "" + (istValue - refValue), "" + (minValue - refValue), "" + (maxValue - refValue), "", "", "", "", "", "", Ergebnis.FT_IO );
										}
									}
									if( myDialog.isCancelled() == true ) { // Wenn Wecker
										// Abbruchtaste dr�ckt
										ergListe = new Vector();
										result = new Ergebnis( PB.getString( "messung" ), PB.getString( "Werker" ), "", "", "", "", "", "", "", "", "", PB.getString( "werkerAbbruch" ), "", "", "", Ergebnis.FT_NIO );
										ergListe.add( result );
										excecuteagain = false;
										throw new PPExecutionException();
									} else if( mode != 0 ) {
										ergListe.add( result );
									} else {
										result_mode_0 = result;
										if( DEBUG ) {
											System.out.println( "UiToleranzTotal mod 0 Mittelwertmessung: " + result_mode_0.getErgebnis() );
										}
									}
								} catch( Exception e ) {
									// Systemfehler, muss man genau analysieren
									e.printStackTrace();
									result = new Ergebnis( "Average Value Measurement", "UIAnalyser", "", "", dauer + "ms", "", "", "", "", "", "", "", "", e.getMessage(), "", Ergebnis.FT_NIO );
									ergListe.add( result );
									throw new PPExecutionException( "The real measurement " );
								}
							}
							/** 12-Mittelwertmessung end* */
						} else {
							/** 13-Mehrpunktmessung* */
							if( DEBUG ) {
								System.out.println( "UiToleranzTotal: Mehrpunktmessung." + "\n" );
							}
							// mehr Punktenmessung
							try {
								if( (mode != 3) && (absolute == false) ) { // Keine
									// Ruhestrommessung
									// und
									// keine absolute Messung
									if( grundwert_read == 2 || grundwert_read == 3 ) { // Grundwert
										// lesen,
										// Schaltwert oder
										// kein Grundwert
										// lesen
										refValue = 0; // Setzt Grundwert auf 0
									} else {
										if( (grundwert_storage_read == 15) || (grundwert_storage_read == 0) ) { // BASE_VALUE_STORAGE_READ
											// ist
											// nicht
											// belegt
											refValue = myUI.grundWert;
										} else { // BASE_VALUE_STORAGE_READ ist belegt
											// (1<grundwert_storage_read <=14)
											refValue = myUI.Grundwertspeicher[grundwert_storage_read - 1];
										}
									}
								}

								minValue = minGet1 + refValue;
								maxValue = maxGet1 + refValue;

								// Mehrpunktmessung
								if( normalize == false ) {
									if( resultName.equals( "P" ) == true )
										myUI.waitForPowerBoundSpecial( maxValue, minValue, anzahl, dauer );
									else
										myUI.waitForCurrentBoundSpecial( maxValue, minValue, anzahl, dauer );
								} else {
									if( resultName.equals( "P" ) == true )
										myUI.waitForNormalizedPowerBoundSpecial( UIAnalyser.NORM_VOLTAGE, vTypValue, maxValue, minValue, anzahl, dauer );
									else
										myUI.waitForNormalizedCurrentBoundSpecial( UIAnalyser.NORM_VOLTAGE, vTypValue, maxValue, minValue, anzahl, dauer );
								}
								// Holen des Ist-Wertes
								istValue = myUI.getLastExecutionInfo().getSampledValue();
								// Ist-Wert wird abgespeicher
								myUI.messWert = istValue;
								// Soll- und Ist-Wert-Vergleich
								if( (istValue < minValue) || (istValue > maxValue) ) { // Ist-Wert
									// nicht
									// i.O.
									if( istValue < refValue ) {
										result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", resultName + "(" + resultEinheit + ")", "" + (istValue - refValue), "" + (minValue - refValue), "" + (maxValue - refValue), "", "", "", awt, PB.getString( "messkleingrund" ), hwt, Ergebnis.FT_NIO );
									} else {
										result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", resultName + "(" + resultEinheit + ")", "" + (istValue - refValue), "" + (minValue - refValue), "" + (maxValue - refValue), "", "", "", awt, (minValue - refValue) + "<" + (istValue - refValue) + ">" + (maxValue - refValue), hwt, Ergebnis.FT_NIO );
									}
									status = STATUS_EXECUTION_ERROR;
									if( grundwert_storage_write != 0 ) { // Wenn
										// BASE_VALUE_STORAGE_WRITE
										// belegt ist, wird der Mittelwert
										// der absoluten Messwerte der
										// letzten Abtastung in einem
										// Grundwertspeicher abgelegt
										istValue = myUI.getLastExecutionInfo().getRequiredMin();
										myUI.Grundwertspeicher[grundwert_storage_write - 1] = istValue;
									}
								} else { // Ist-Wert i.O.
									if( grundwert_storage_write != 0 ) { // Wenn
										// BASE_VALUE_STORAGE_WRITE
										// belegt ist, wird der Messwert in
										// einem Grundwertspeicher abgelegt
										myUI.Grundwertspeicher[grundwert_storage_write - 1] = istValue;
									}
									result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", resultName + "(" + resultEinheit + ")", "" + (istValue - refValue), "" + (minValue - refValue), "" + (maxValue - refValue), "", "", "", "", "", "", Ergebnis.FT_IO );
								}
								if( myDialog.isCancelled() == true ) { // Wenn Wecker Abbruchtaste
									// dr�ckt
									ergListe = new Vector();
									result = new Ergebnis( PB.getString( "messung" ), PB.getString( "Werker" ), "", "", "", "", "", "", "", "", "", PB.getString( "werkerAbbruch" ), "", "", "", Ergebnis.FT_NIO );
									ergListe.add( result );
									excecuteagain = false;
									throw new PPExecutionException();
								} else if( mode != 0 ) {
									ergListe.add( result );
								} else {
									result_mode_0 = result;
									if( DEBUG ) {
										System.out.println( "UiToleranzTotal mod 0 Mehrpunktmessung: " + result_mode_0.getErgebnis() );
									}
								}
							} catch( CurrentProbeException e ) {
								// Keine Daten kommt f�r zwei Sekunden lang aus dem
								// COM-Port
								e.printStackTrace();
								throw new CurrentProbeException( "No Data from Serial Port" );
							}/*
								* catch (UIOffsetErrorException e) { // Offsetmessung fehlgeschlage
								* oder noch aktiv e.printStackTrace(); throw e; }
								*/catch( Exception e ) {
								// Systemfehler, muss man genau analysieren
								e.printStackTrace();
								result = new Ergebnis( "Multipoint Measurement", "UIAnalyser", "", "", dauer + "ms; " + anzahl + "points", "", "", "", "", "", "", "", "", e.getMessage(), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								if( e instanceof UIOffsetErrorException )
									throw e;
								throw new PPExecutionException( "The multipoint measurement " );
							}
						}
						/** 13-Mehrpunktmessung end* */
					}

					/** 14-Handlung f�r end Trigger falls mode == 0* */
					if( mode == 0 ) { // Eine negative Flanke wird erwartet
						if( DEBUG ) {
							System.out.println( "UiToleranzTotal: mod 0." );
						}
						if( status == STATUS_EXECUTION_OK ) {
							if( diagnoseTyp == TYP_NON_PP || diagnoseTyp == TYP_STATUS_PP ) { // Nur
								// f�r
								// manuellen
								// Verbraucher
								if( udInUse == true ) {
									try {
										// Device Dialogbox wird freigegeben
										getPr�flingLaufzeitUmgebung().releaseUserDialog();
										// AWT (Messung i.O.) wird ausgegeben
										myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
										myDialog.setAllowCancel( true );
										myDialog.displayUserMessage( PB.getString( "anweisung" ), awt + ";" + PB.getString( "verbraucherAusschalten" ), -1 );
									} catch( Exception e ) {
										// Systemfehler, muss man genau analysieren
										e.printStackTrace();
										result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
										ergListe.add( result );
										throw new PPExecutionException( "Get UserDialog" );
									}
								}
							}
							try {
								// Holen des Wertes des Parameters END_TRIGGER_REF
								temp = getArg( getOptionalArgs()[3] );
								if( temp != null ) {
									endTriggerRef = Integer.parseInt( temp );
								}

								okTime = BASE_VALUE_UPDATE_TIME;
								timeout = 3000; // Wenn keine fallende Flanke auftaucht,
								// warte nur 3
								// Sekunden
								myUI.initGrundWertPuffer( true );
								myUI.grundWertSave = myUI.grundWert;
								start = System.currentTimeMillis();
								end = 0;
								if( Zykluszeit == 0 ) { // f�r nicht Zyklusverbraucher,
									// pr�ft ob der
									// ausgeschaltet ist
									if( normalize == false ) { // Nicht normiert
										if( resultName.equals( "P" ) == true ) { // Leistungsmessung
											if( WW_is_MW == 1 ) {
												myUI.messFilter.setFilter( cutoff_mess, type_mess );
											} else {
												myUI.messFilter.setFilter( cutoff_trigger, type_trigger );
											}
											while( (end < timeout) && (myDialog.isCancelled() == false) ) {
												// System.out.println("++++++++++++
												// Waiting for trigger
												// ++++++++++++");
												/*
												 * if (WW_is_MW == 1) {
												 * myUI.messFilter.setFilter(cutoff_mess,
												 * type_mess); } else {
												 * myUI.messFilter.setFilter(cutoff_trigger,
												 * type_trigger); }
												 */if( myUI.waitForPowerBoundNoTime( endTriggerValue, endTriggerRef, okTime, absolute, start_trig_dir, end_trig_dir, grundwert_read, grundwert_storage_read ) == true ) {
													break;
												} else {
													end = System.currentTimeMillis() - start;
												}
											}

											if( end >= timeout ) { // Wenn Timeout abgelaufen ist,
												// weiteren AWT ausgegeben
												try {
													getPr�flingLaufzeitUmgebung().releaseUserDialog();
													myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
													myDialog.requestUserMessage( PB.getString( "anweisung" ), awt, 0 );
												} catch( Exception e ) {
													// Systemfehler, muss man genau
													// analysieren
													e.printStackTrace();
													result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
													ergListe.add( result );
													throw new PPExecutionException( "Get UserDialog" );
												}
											}
										} else { // Strommessung
											if( WW_is_MW == 1 ) {
												myUI.messFilter.setFilter( cutoff_mess, type_mess );
											} else {
												myUI.messFilter.setFilter( cutoff_trigger, type_trigger );
											}
											while( (end < timeout) && (myDialog.isCancelled() == false) ) {
												// System.out.println("++++++++++++
												// Waiting for trigger
												// ++++++++++++");
												/*
												 * if (WW_is_MW == 1) {
												 * myUI.messFilter.setFilter(cutoff_mess,
												 * type_mess); } else {
												 * myUI.messFilter.setFilter(cutoff_trigger,
												 * type_trigger); }
												 */if( myUI.waitForCurrentBoundNoTime( endTriggerValue, endTriggerRef, okTime, absolute, start_trig_dir, end_trig_dir, grundwert_read, grundwert_storage_read ) == true ) {
													break;
												} else {
													end = System.currentTimeMillis() - start;
												}
											}

											if( end >= timeout ) { // Wenn Timeout
												// abgelauften ist,
												// wird
												// weiteren AWT ausgegeben
												try {
													getPr�flingLaufzeitUmgebung().releaseUserDialog();
													myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
													myDialog.requestUserMessage( PB.getString( "anweisung" ), awt, 0 );
												} catch( Exception e ) {
													// Systemfehler, muss man genau
													// analysieren
													e.printStackTrace();
													result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
													ergListe.add( result );
													throw new PPExecutionException( "Get UserDialog" );
												}
											}
										}
									} else { // Normiert
										if( resultName.equals( "P" ) == true ) { // Leistungsmessung
											if( WW_is_MW == 1 ) {
												myUI.messFilter.setFilter( cutoff_mess, type_mess );
											} else {
												myUI.messFilter.setFilter( cutoff_trigger, type_trigger );
											}
											while( (end < timeout) && (myDialog.isCancelled() == false) ) {
												// System.out.println("++++++++++++
												// Waiting for trigger
												// ++++++++++++");
												/*
												 * if (WW_is_MW == 1) {
												 * myUI.messFilter.setFilter(cutoff_mess,
												 * type_mess); } else {
												 * myUI.messFilter.setFilter(cutoff_trigger,
												 * type_trigger); }
												 */if( myUI.waitForNormalizedPowerBoundNoTime( UIAnalyser.NORM_VOLTAGE, vTypValue, endTriggerValue, endTriggerRef, okTime, absolute, start_trig_dir, end_trig_dir, grundwert_read, grundwert_storage_read ) == true ) {
													break;
												} else {
													end = System.currentTimeMillis() - start;
												}
											}

											if( end >= timeout ) { // Wenn Timeout
												// abgelauften ist,
												// wird
												// weiteren AWT ausgegeben
												try {
													getPr�flingLaufzeitUmgebung().releaseUserDialog();
													myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
													myDialog.requestUserMessage( PB.getString( "anweisung" ), awt, 0 );
												} catch( Exception e ) {
													// Systemfehler, muss man genau
													// analysieren
													e.printStackTrace();
													result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
													ergListe.add( result );
													throw new PPExecutionException( "Get UserDialog" );
												}
											}
										} else { // Strommessung
											if( WW_is_MW == 1 ) {
												myUI.messFilter.setFilter( cutoff_mess, type_mess );
											} else {
												myUI.messFilter.setFilter( cutoff_trigger, type_trigger );
											}
											while( (end < timeout) && (myDialog.isCancelled() == false) ) {
												// System.out.println("++++++++++++
												// Waiting for trigger
												// ++++++++++++");
												/*
												 * if (WW_is_MW == 1) {
												 * myUI.messFilter.setFilter(cutoff_mess,
												 * type_mess); } else {
												 * myUI.messFilter.setFilter(cutoff_trigger,
												 * type_trigger); }
												 */if( myUI.waitForNormalizedCurrentBoundNoTime( UIAnalyser.NORM_VOLTAGE, vTypValue, endTriggerValue, endTriggerRef, okTime, absolute, start_trig_dir, end_trig_dir, grundwert_read, grundwert_storage_read ) == true ) {
													break;
												} else {
													end = System.currentTimeMillis() - start;
												}
											}

											if( end >= timeout ) { // Wenn Timeout
												// abgelauften ist,
												// wird
												// weiteren AWT ausgegeben
												try {
													getPr�flingLaufzeitUmgebung().releaseUserDialog();
													myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
													myDialog.requestUserMessage( PB.getString( "anweisung" ), awt, 0 );
												} catch( Exception e ) {
													// Systemfehler, muss man genau
													// analysieren
													e.printStackTrace();
													result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
													ergListe.add( result );
													throw new PPExecutionException( "Get UserDialog" );
												}
											}
										}
									}
								} else {
									// f�r Zyklusverbraucher, pr�ft ob der ausgeschaltet
									// ist
									if( Zykluszeit > timeout ) {
										timeout = 3 * Zykluszeit;
									}
									if( normalize == false ) { // Nicht normiert
										if( resultName.equals( "P" ) == true ) { // Leistungsmessung
											while( (timeout > 0) && (myDialog.isCancelled() == false) ) {
												// System.out.println("++++++++++++
												// Waiting for switching
												// off the consumer ++++++++++++");
												if( myUI.waitForPowerBoundCycle( maxZyklus, minZyklus, Zykluszeit, endTriggerRef ) == true ) {
													break;
												} else {
													timeout = timeout - Zykluszeit;
												}
												if( timeout <= 0 ) { // Wenn Timeout
													// abgelauften ist,
													// wird
													// weiteren AWT ausgegeben
													try {
														getPr�flingLaufzeitUmgebung().releaseUserDialog();
														myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
														myDialog.requestUserMessage( PB.getString( "anweisung" ), awt, 0 );
													} catch( Exception e ) {
														// Systemfehler, muss man genau
														// analysieren
														e.printStackTrace();
														result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
														ergListe.add( result );
														throw new PPExecutionException( "Get UserDialog" );
													}
												}
											}
										} else { // Strommessung
											while( (timeout > 0) && (myDialog.isCancelled() == false) ) {
												// System.out.println("++++++++++++
												// Waiting for switching
												// off the consumer ++++++++++++");
												if( myUI.waitForCurrentBoundCycle( maxZyklus, minZyklus, Zykluszeit, endTriggerRef ) == true ) {
													break;
												} else {
													timeout = timeout - Zykluszeit;
												}
												if( timeout <= 0 ) { // Wenn Timeout
													// abgelauften ist,
													// wird
													// weiteren AWT ausgegeben
													try {
														getPr�flingLaufzeitUmgebung().releaseUserDialog();
														myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
														myDialog.requestUserMessage( PB.getString( "anweisung" ), awt, 0 );
													} catch( Exception e ) {
														// Systemfehler, muss man genau
														// analysieren
														e.printStackTrace();
														result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
														ergListe.add( result );
														throw new PPExecutionException( "Get UserDialog" );
													}
												}
											}
										}
									} else { // Normiert
										if( resultName.equals( "P" ) == true ) { // Leistungsmessung
											while( (timeout > 0) && (myDialog.isCancelled() == false) ) {
												// System.out.println("++++++++++++
												// Waiting for switching
												// off the consumer ++++++++++++");
												if( myUI.waitForNormalizedPowerBoundCycle( UIAnalyser.NORM_VOLTAGE, vTypValue, maxZyklus, minZyklus, Zykluszeit, endTriggerRef ) == true ) {
													break;
												} else {
													timeout = timeout - Zykluszeit;
												}
												if( timeout <= 0 ) { // Wenn Timeout
													// abgelauften ist,
													// wird
													// weiteren AWT ausgegeben
													try {
														getPr�flingLaufzeitUmgebung().releaseUserDialog();
														myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
														myDialog.requestUserMessage( PB.getString( "anweisung" ), awt, 0 );
													} catch( Exception e ) {
														// Systemfehler, muss man genau
														// analysieren
														e.printStackTrace();
														result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
														ergListe.add( result );
														throw new PPExecutionException( "Get UserDialog" );
													}
												}
											}
										} else { // Strommessung
											while( (timeout > 0) && (myDialog.isCancelled() == false) ) {
												// System.out.println("++++++++++++
												// Waiting for switching
												// off the consumer ++++++++++++");
												if( myUI.waitForNormalizedCurrentBoundCycle( UIAnalyser.NORM_VOLTAGE, vTypValue, maxZyklus, minZyklus, Zykluszeit, endTriggerRef ) == true ) {
													break;
												} else {
													timeout = timeout - Zykluszeit;
												}
												if( timeout <= 0 ) { // Wenn Timeout
													// abgelauften ist,
													// wird
													// weiteren AWT ausgegeben
													try {
														getPr�flingLaufzeitUmgebung().releaseUserDialog();
														myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
														myDialog.requestUserMessage( PB.getString( "anweisung" ), awt, 0 );
													} catch( Exception e ) {
														// Systemfehler, muss man genau
														// analysieren
														e.printStackTrace();
														result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
														ergListe.add( result );
														throw new PPExecutionException( "Get UserDialog" );
													}
												}
											}
										}
									}
								}
							} catch( CurrentProbeException e ) {
								// Keine Daten kommt f�r zwei Sekunden lang aus dem
								// COM-Port
								e.printStackTrace();
								throw new CurrentProbeException( "No Data from Serial Port" );
							}/*
								* catch (UIOffsetErrorException e) { // Offsetmessung fehlgeschlage
								* oder noch aktiv e.printStackTrace(); throw e; }
								*/catch( Exception e ) {
								// Systemfehler, muss man genau analysieren
								e.printStackTrace();
								result = new Ergebnis( "End Trigger Measurement", "UIAnalyser", "", "", "", "", "", "", "", "", "", "", "", e.getMessage(), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								if( e instanceof UIOffsetErrorException )
									throw e;
								throw new PPExecutionException( "The end measurement" );
							}
						}
						if( result_mode_0 != null ) {
							ergListe.add( result_mode_0 );
						}
					}
					/** 14-Handlung f�r end Trigger falls mode == 0 end* */

					/** 15-�berpr�fen ob User Dialog abgebrochen ist* */
					// Wenn das Programm abgebrochen wird
					if( udInUse == true ) {
						try {
							if( myDialog.isCancelled() == true ) {
								ergListe = new Vector();
								cancelled = true;
								status = STATUS_EXECUTION_ERROR;
								getPr�flingLaufzeitUmgebung().releaseUserDialog();
								udInUse = false;
							}
						} catch( Exception e ) {
							// Systemfehler, muss man genau analysieren
							e.printStackTrace();
							result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException( "Get UserDialog" );
						}
					}
					/** 15-�berpr�fen ob User Dialog abgebrochen ist end* */

					/** 16-Falls der Pr�fprozedur abgebrochen, Ergebnis zur�ckgeben* */
					// Das Ergebnis wird zur�ckgegeben
					if( cancelled == true ) {
						status = STATUS_EXECUTION_ERROR;
						result = new Ergebnis( PB.getString( "messung" ), PB.getString( "Werker" ), "", "", "", "", "", "", "", "", "", "U = " + myUI.getActUI().uValue + "mV  I = " + myUI.getActUI().iValue + "mA  " + PB.getString( "werkerAbbruch" ), "", PB.getString( "pollingAbbruch" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
					}
					/** 16-Falls der Pr�fprozedur abgebrochen, Ergebnis zur�ckgeben end* */

					/** 17-Wenn mode == 1 auf Best�tigung warten* */
					// Wenn mode gleich 1 (Trigger, Messung, Best�tigung) ist, wird eine
					// Meldung nach der
					// Messung noch ausgegeben
					if( (mode == 1) && (status == STATUS_EXECUTION_OK) ) {
						// Vorschlag f�r �nderung in Ausgabe nach dem Trigger
						// awt = PB.getString("pruefschrittIo") + ";" + PB.getString("weiterGehen");
						awt = PB.getString( "messung" ) + " OK" + ";" + PB.getString( "weiterGehen" );
						try {
							getPr�flingLaufzeitUmgebung().releaseUserDialog();
							// Vorschlag f�r �nderung in Ausgabe nach dem Trigger
							/*
							 * getPr�flingLaufzeitUmgebung().getUserDialog()
							 * .requestUserMessage(PB.getString("ecosMessung"), awt, 0);
							 */
							getPr�flingLaufzeitUmgebung().getUserDialog().requestUserMessage( "ECOS-" + PB.getString( "messung" ), awt, 0 );
						} catch( Exception e ) {
							// Systemfehler, muss man genau analysieren
							e.printStackTrace();
							result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException( "Get UserDialog" );
						}
					}
					/** 17-Wenn mode == 1 auf Best�tigung warten end* */

					/** 18-Wenn WAIT_END belegt ist, die Messung verl�ngern* */
					// Wenn WAIT_END belegt ist
					if( (status == STATUS_EXECUTION_OK) && (abschaltZeit != 0) ) {
						try {
							measure = myUI.getCurrentBoundInfo( true, abschaltZeit );
						} catch( CurrentProbeException e ) {
							// Keine Daten kommt f�r zwei Sekunden lang aus dem COM-Port
							e.printStackTrace();
							throw new CurrentProbeException( "No Data from Serial Port" );
						}/*
							* catch (UIOffsetErrorException e) { // Offsetmessung fehlgeschlage oder
							* noch aktiv e.printStackTrace(); throw e; }
							*/catch( Exception e ) {
							e.printStackTrace();
							result = new Ergebnis( "Waiting End Deadtime", "UIAnalyser", "", "", "", "", "", "", "", "", "", "", "", e.getMessage(), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							if( e instanceof UIOffsetErrorException )
								throw e;
							throw new PPExecutionException( "waiting deadtime" );
						}
					}
					/** 18-Wenn WAIT_END belegt ist, die Messung verl�ngern end* */
					/***********************************************************************************************************************************************************************************
					 * 19-Wenn Diagnosejob nicht zu ende ist warten, und in zwischen die Meldung
					 * "Warte auf Diagnose" einblenden
					 **********************************************************************************************************************************************************************************/
					// Warte bis EcosThread beendet
					// Eine Meldung wird zwischen ausgegeben (Warte auf Diagnose)
					if( status == STATUS_EXECUTION_OK && ppThread != null && ppThread.isAlive() ) {
						try {
							getPr�flingLaufzeitUmgebung().releaseUserDialog();
							myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
							myDialog.displayUserMessage( PB.getString( "meldung" ), PB.getString( "warteaufppThread" ), -1 );
						} catch( Exception e ) {
							// Systemfehler, muss man genau analysieren
							e.printStackTrace();
							result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException( "Get UserDialog" );
						}
						udInUse = true;
					}
					// Warten bis Diagnosejob zum Ende f�hrt
					while( ppThread != null && ppThread.isAlive() ) {
						try {
							Thread.sleep( 10 );
						} catch( Exception e ) {
							// Systemfehler, muss man genau analysieren
							e.printStackTrace();
						}
					}
					/***********************************************************************************************************************************************************************************
					 * 19-Wenn Diagnosejob nicht zu ende ist warten, und in zwischen die Meldung
					 * "Warte auf Diagnose" einblenden end
					 **********************************************************************************************************************************************************************************/
					// Wenn der Status von STATUS_PP n.i.O. ist, wird der Pr�fschritt
					// als n.i.O. bewertet
					// und beendet
					/**
					 * 20-Wenn Diagnosejob STATUS_PP ist und Fehler aufgetreten ist, die richtige
					 * Fehlermeldung vorbreiten*
					 */
					if( diagnoseTyp == TYP_STATUS_PP && pp.getExecStatus() == STATUS_EXECUTION_ERROR ) { // ob
						// der
						// Diagnosejob
						// f�r
						// Statusabfrage
						// richtig
						// ausgef�hrt
						// wurde
						StringBuffer fehlerText = new StringBuffer();
						try {
							Ergebnis lastResult = pp.getLastErgebnis();
							if( lastResult.getFehlerText().length() > 0 )
								fehlerText.append( lastResult.getFehlerText() ).append( " " );
							if( lastResult.getHinweisText().length() > 0 )
								fehlerText.append( lastResult.getHinweisText() ).append( " " );
						} catch( Exception e ) {
							// Systemfehler, muss man genau analysieren
							e.printStackTrace();
						}
						result = new Ergebnis( "Diagnostics", "STATUS_PP", "", "", "", "", "", "", "", "0", "", "", "", fehlerText.toString(), "", Ergebnis.FT_NIO );
						if( myDialog.isCancelled() == true ) { // Wenn Werker Abbruchtaste dr�ckt
							// wird andere ergebnisse
							// weggeworfen
							ergListe = new Vector();
							result = new Ergebnis( PB.getString( "messung" ), PB.getString( "Werker" ), "", "", "", "", "", "", "", "", "", PB.getString( "werkerAbbruch" ), "", "", "", Ergebnis.FT_NIO );
							ergListe.add( result );
							excecuteagain = false;
							throw new PPExecutionException();
						} else {
							ergListe.add( result );
						}
						throw new PPExecutionException( "STATUS_PP" );
					}
					/**
					 * 20-Wenn Diagnosejob STATUS_PP ist und Fehler aufgetreten ist, die richtige
					 * Fehlermeldung vorbreiten end*
					 */
					/** 21-Zugriff von Diagnosejob ausl�schen* */
					pp = null;
					/** 21-Zugriff von Diagnosejob ausl�schen end* */
				}
			} catch( CurrentProbeException e ) {
				result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", "", "", "", "", "", "", "", "", PB.getString( "keinedaten" ) + ";" + PB.getString( "zangeio" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				// e.printStackTrace();
				if( DEBUG ) {
					System.out.println( "UiToleranzTotal: CurrentProbeException, excecute again." );
				}
				excecuteagain = true;
			} catch( PPExecutionException e ) {
				status = STATUS_EXECUTION_ERROR;
				// Fehler damit Ende PP
				break;
			} catch( Exception e ) {
				// Systemfehler, muss man genau analysieren
				if( e instanceof UIOffsetErrorException ) {
					result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", "", "", "", "", "", "", "", "", e.getMessage(), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					offsetStatus = OFFSETNIO;
				} else {
					e.printStackTrace();
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
				// Fehler damit Ende PP
				break;
			} catch( Throwable e ) {
				// Systemfehler, muss man genau analysieren
				e.printStackTrace();
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				// Fehler damit Ende PP
				break;
			}
			// Kontrollieren ob Stromdatten kommen.
			if( myUI.getLastReadError() ) {
				if( DEBUG ) {
					System.out.println( "UiToleranzTotal: Batterieadapter ist nicht angeschlossen, stop mess." );
				}
				/*
				 * try{ myUI.writeErgebnis(1); }catch(Exception e){ }
				 */myUI.stopMess();
				awt = PB.getString( "keinedaten" ) + ";" + PB.getString( "zangeio" );
				try {
					try {
						getPr�flingLaufzeitUmgebung().releaseUserDialog(); // Device Dialgobox wird
						// freigegeben
						myDialog = getPr�flingLaufzeitUmgebung().getUserDialog(); // Holen der
						// Dialogbox
						myDialog.setAllowCancel( true ); // Abbruchtaste definiert f�r Dialogbox
						myDialog.displayUserMessage( PB.getString( "zange" ), awt, -1 ); // AWT wird
						// auf
						// dem
						// Bildschirm
						// dargestellt
					} catch( Exception exp1 ) {
						// Systemfehler, muss man genau analysieren
						exp1.printStackTrace();
						result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", exp1.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						throw new PPExecutionException( "Get UserDialog" );
					}
					while( myUI.getLastReadError() ) {
						if( myDialog.isCancelled() == true ) { // Wenn Wecker Abbruchtaste dr�ckt
							ergListe = new Vector();
							result = new Ergebnis( PB.getString( "messung" ), PB.getString( "Werker" ), "", "", "", "", "", "", "", "", "", PB.getString( "werkerAbbruch" ), "", "", "", Ergebnis.FT_NIO );
							ergListe.add( result );
							excecuteagain = false;
							throw new PPExecutionException();
						}
						Thread.sleep( 50 );
					}
					Thread.sleep( 50 );
					if( !myUI.getActUI().isOffsetIO() ) {
						offsetStatus = OFFSETNIO;
					} else {
						result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", PB.getString( "batterieSpannung" ) + "(mV)", "" + myUI.getActUI().uValue, "", "", "", "", "", "", "Offset", "", Ergebnis.FT_IO );
						ergListe.add( result );
						int minOffsetToleranz = 0 - myUI.getMaxOffsetToleranz();
						result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", "Offset (mA)", "" + myUI.getActUI().oValue, "" + minOffsetToleranz, "" + myUI.getMaxOffsetToleranz(), "", "", "", "", "Offset", "", Ergebnis.FT_IO );
						ergListe.add( result );
						result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", "Max. Off. Diff. (mA)", "" + myUI.getActUI().oDifferenz, "0", "" + myUI.getMaxOffsetFluct(), "", "", "", "", "Offset", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
					if( DEBUG ) {
						System.out.println( "UiToleranzTotal: Batterieadapter erneut angeschlossen, Offsetstatus:" + myUI.getActUI().oStatus );
					}
				} catch( PPExecutionException e ) {
					status = STATUS_EXECUTION_ERROR;

				} catch( Exception e ) {
					// Systemfehler, muss man genau analysieren
					e.printStackTrace();
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
			}

			if( ECOS_OFFSET_ZANGE_UMSCHLOSSEN != null ) {
				if( excecuteagain && myUI.getActUI().isOffsetIO() && !myUI.getLastReadError() ) {
					offsetStatus = OFFSETIO;
					// awt = PB.getString("ZangeUmgeschlossen");
					awt = ECOS_OFFSET_ZANGE_UMSCHLOSSEN;
					try {
						getPr�flingLaufzeitUmgebung().releaseUserDialog();
						getPr�flingLaufzeitUmgebung().getUserDialog().requestUserMessage( PB.getString( "zange" ), awt, 0 );
					} catch( Exception e ) {
						// Systemfehler, muss man genau analysieren
						e.printStackTrace();
						result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				}
			}

			/** 22- Wenn Offset Fehler aufgetretten ist die Pr�fprozedur neu starten* */
			if( offsetStatus == OFFSETNIO ) {
				try {
					if( DEBUG ) {
						System.out.println( "UiToleranzTotal: Offset error." + "\n" );
					}
					String title = PB.getString( "zange" );
					awt = PB.getString( "OffsetNIO" ) + "," + PB.getString( "BatterieadaprerErneutAnschliessen" );
					try {
						getPr�flingLaufzeitUmgebung().releaseUserDialog();
						myDialog = getPr�flingLaufzeitUmgebung().getUserDialog(); // Holen der
						// Dialogbox
						myDialog.setAllowCancel( true ); // Abbruchtaste definiert f�r Dialogbox
						myDialog.displayUserMessage( title, awt, -1 ); // AWT wird auf dem
						// Bildschirm dargestellt
						udInUse = false;
					} catch( Exception e ) {
						// Systemfehler, muss man genau analysieren
						e.printStackTrace();
						result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						throw new PPExecutionException( "Get UserDialog" );
					}
					boolean newStatus = true;
					int newOffsetStatus = myUI.getActUI().oStatus;
					int oldOffsetStatus = myUI.getActUI().oStatus;
					// udInUse = false;
					while( newOffsetStatus != UIMultizet.OFFSET_IO ) {
						if( newOffsetStatus == UIMultizet.OFFSET_ERR_FLUCT || newOffsetStatus == UIMultizet.OFFSET_ERR_HIGH || newOffsetStatus == UIMultizet.OFFSET_ERR_LOW || newOffsetStatus == UIMultizet.OFFSET_NOTREADY ) {
							awt = PB.getString( "OffsetNIO" ) + " " + PB.getString( "BatterieadaprerErneutAnschliessen" );
						} else if( newOffsetStatus == UIMultizet.OFFSET_RUNNING ) {
							awt = PB.getString( "offsetAbgleichLaeuft" );
						}
						if( newStatus ) {
							try {
								getPr�flingLaufzeitUmgebung().releaseUserDialog(); // Device
								// Dialgobox
								// wird
								// freigegeben
								myDialog = getPr�flingLaufzeitUmgebung().getUserDialog(); // Holen
								// der
								// Dialogbox
								myDialog.setAllowCancel( true ); // Abbruchtaste definiert f�r
								// Dialogbox
								myDialog.displayUserMessage( title, awt, -1 ); // AWT wird auf dem
								// Bildschirm
								// dargestellt
							} catch( Exception exp1 ) {
								// Systemfehler, muss man genau analysieren
								exp1.printStackTrace();
								result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", exp1.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								throw new PPExecutionException( "Get UserDialog" );
							}
						}
						if( myDialog.isCancelled() == true ) { // Wenn Wecker Abbruchtaste dr�ckt
							ergListe = new Vector();
							result = new Ergebnis( PB.getString( "messung" ), PB.getString( "Werker" ), "", "", "", "", "", "", "", "", "", PB.getString( "werkerAbbruch" ), "", "", "", Ergebnis.FT_NIO );
							ergListe.add( result );
							excecuteagain = false;
							throw new PPExecutionException();
						}
						oldOffsetStatus = newOffsetStatus;
						Thread.sleep( 250 ); // Warte immer 250 Millisekunden
						newOffsetStatus = myUI.getActUI().oStatus;
						if( newOffsetStatus == oldOffsetStatus ) {
							newStatus = false;
						} else {
							newStatus = true;
						}
						if( newStatus && (newOffsetStatus != UIMultizet.OFFSET_RUNNING) && (newOffsetStatus != UIMultizet.OFFSET_IO) ) {
							int minOffsetToleranz = 0 - myUI.getMaxOffsetToleranz();
							result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", "Offset (mA)", "" + myUI.getActUI().oValue, "" + minOffsetToleranz, "" + myUI.getMaxOffsetToleranz(), "", "", "", "", "Offsetstatus " + newOffsetStatus, "", Ergebnis.FT_IO );
							ergListe.add( result );
							result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", "Max. Off. Diff. (mA)", "" + myUI.getActUI().oDifferenz, "0", "" + myUI.getMaxOffsetFluct(), "", "", "", "", "Offsetstatus " + newOffsetStatus, "", Ergebnis.FT_IO );
							ergListe.add( result );
						}
					}
					if( ECOS_OFFSET_ZANGE_UMSCHLOSSEN != null ) {
						awt = ECOS_OFFSET_ZANGE_UMSCHLOSSEN;
						try {
							getPr�flingLaufzeitUmgebung().releaseUserDialog();
							getPr�flingLaufzeitUmgebung().getUserDialog().requestUserMessage( PB.getString( "zange" ), awt, 0 );
						} catch( Exception e ) {
							// Systemfehler, muss man genau analysieren
							e.printStackTrace();
							result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException( "Get UserDialog" );
						}
					}
					getPr�flingLaufzeitUmgebung().releaseUserDialog(); // Device Dialgobox wird
					// freigegeben
					udInUse = false;
					result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", PB.getString( "batterieSpannung" ) + "(mV)", "" + myUI.getActUI().uValue, "", "", "", "", "", "", "Offset", "", Ergebnis.FT_IO );
					ergListe.add( result );
					int minOffsetToleranz = 0 - myUI.getMaxOffsetToleranz();
					result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", "Offset (mA)", "" + myUI.getActUI().oValue, "" + minOffsetToleranz, "" + myUI.getMaxOffsetToleranz(), "", "", "", "", "Offset", "", Ergebnis.FT_IO );
					ergListe.add( result );
					result = new Ergebnis( PB.getString( "messung" ), "UIAnalyser", "", "", "", "Max. Off. Diff. (mA)", "" + myUI.getActUI().oDifferenz, "0", "" + myUI.getMaxOffsetFluct(), "", "", "", "", "Offset", "", Ergebnis.FT_IO );
					ergListe.add( result );
					// int answer = 0;
					// awt = PB.getString(getArg(getRequiredArgs()[6]));
					awt = PB.getString( "PruefsystemBereit" );
					if( DEBUG ) {
						System.out.println( "UiToleranzTotal: Offset IO " + awt );
					}
					try {
						getPr�flingLaufzeitUmgebung().releaseUserDialog();
						getPr�flingLaufzeitUmgebung().getUserDialog().requestUserMessage( PB.getString( "anweisung" ), awt, 0 );
						/*
						 * if(answer == UserDialog.YES_KEY){ excecuteagain = true; }
						 */
					} catch( Exception e ) {
						// Systemfehler, muss man genau analysieren
						e.printStackTrace();
						result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						throw new PPExecutionException( "Get UserDialog" );
					}
					excecuteagain = true;
				} catch( PPExecutionException e ) {
					status = STATUS_EXECUTION_ERROR;
				} catch( Exception e ) {
					// Systemfehler, muss man genau analysieren
					e.printStackTrace();
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				} catch( Throwable e ) {
					// Systemfehler, muss man genau analysieren
					e.printStackTrace();
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
			}
			/**
			 * 23-Log Datei von UiAnalyser schliessen UiAnalyser freigeben und dessen Zugriff
			 * l�schen*
			 */
			// Freigabe der benutzten Devices
			if( myUI != null ) {
				if( DEBUG ) {
					System.out.println( "UiToleranzTotal: UiAnalyser freigeben." + "\n" );
				}
				try {
					if( myUI.getLog() == true ) { // Log-Datei wird geschlossen
						if( loggerEnable == 1 ) {
							myUI.setLog( false );
						}
						myUI.exitLog();
					}
				} catch( DeviceIOException e ) {
					// Systemfehler, muss man genau analysieren
					e.printStackTrace();
					result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "DeviceIOException", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
				/*
				 * if(status != STATUS_EXECUTION_OK){ try{ myUI.writeErgebnis(1); }catch(Exception
				 * e){ } }
				 */// parallelen Thread wieder Starten
				myUI.stopMess();

				try {
					// Device UIAnalyser wird freigegeben
					getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
					myUI = null;
				} catch( Exception e ) {
					// Systemfehler, muss man genau analysieren
					e.printStackTrace();
					if( e instanceof DeviceLockedException )
						result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "release", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
				myUI = null;
			}

			/** 24-User Dialog freigeben* */
			// Device Dialogbox wird freigegeben
			if( udInUse == true ) {
				try {
					getPr�flingLaufzeitUmgebung().releaseUserDialog();
					myDialog = null;
				} catch( Exception e ) {
					// Systemfehler, muss man genau analysieren
					e.printStackTrace();
					if( e instanceof DeviceLockedException )
						result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "release", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
			}
			/** 24-User Dialog freigeben end* */
		} while( excecuteagain );

		/**
		 * 23-Log Datei von UiAnalyser schliessen UiAnalyser freigeben und dessen Zugriff
		 * l�schen*
		 */
		// Bei einen Abbruch nicht geschlossene Devices sauber beenden
		if( myUI != null ) {
			try {
				if( myUI.getLog() == true ) { // Log-Datei wird geschlossen
					if( loggerEnable == 1 ) {
						myUI.setLog( false );
					}
					myUI.exitLog();
				}
			} catch( DeviceIOException e ) {
				// Systemfehler, muss man genau analysieren
				e.printStackTrace();
				result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "DeviceIOException", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}

			myUI.stopMess();

			try {
				// Device UIAnalyser wird freigegeben
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e ) {
				// Systemfehler, muss man genau analysieren
				e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
			myUI = null;
		}

		/** 24-User Dialog freigeben* */
		// Device Dialogbox wird freigegeben
		if( (myDialog != null) && (udInUse == true) ) {
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( Exception e ) {
				// Systemfehler, muss man genau analysieren
				e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
		/** 24-User Dialog freigeben end* */

		// Status setzen
		/** 25-Den Status von Pr�fprozedur setzen* */
		setPPStatus( info, status, ergListe );
		/** 25-Den Status von Pr�fprozedur setzen end* */
	}

	// Der eigene Prozess f�r Diagnose-Job
	private class EcosThread extends Thread {
		ExecutionInfo info;

		Pruefprozedur myPruefprozedur;

		long wait_pp;

		public EcosThread( ExecutionInfo info, Pruefprozedur pp, int wait_trig_pp ) {
			this.info = info;
			this.myPruefprozedur = pp;
			this.wait_pp = wait_trig_pp;
		}

		public void run() {
			try {
				if( wait_pp != 0 )
					Thread.sleep( wait_pp ); // Wenn der Parameter WAIT_TRIGGER_PP
				// belegt ist
				myPruefprozedur.setPr�flingLaufzeitUmgebung( getPr�flingLaufzeitUmgebung() );
				myPruefprozedur.execute( info );
				
				// Das PP Ergebnis des externen PP Calls im Not OK Fall weiterreichen
				if ( myPruefprozedur.getExecStatus() != STATUS_EXECUTION_OK ) {
					status = myPruefprozedur.getExecStatus();
				}

				myPruefprozedur = null;
			} catch( Exception e ) {
				// Systemfehler, muss man genau analysieren
				e.printStackTrace();
			}
		}
	}

	// Der eigene Prozess f�r Dialogbox
	private class UserSelectionThread extends Thread {
		private UserDialog threadDialog = null;

		private String selection = null;

		private String title = null;

		private String message = null;

		private String[] choices = null;

		public UserSelectionThread( UserDialog ud, String title, String message, String[] choices ) {
			this.threadDialog = ud;
			this.title = title;
			this.message = message;
			this.choices = choices;
		}

		public void run() {
			selection = threadDialog.requestUserSelection( title, message, 0, choices );
		}

		public String getSelection() {
			return selection;
		}
	}

}

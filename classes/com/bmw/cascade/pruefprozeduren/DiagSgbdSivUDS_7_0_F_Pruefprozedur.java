/*
 * DiagSgbdSivUDS_1_0_F_Pruefprozedur.java
 *
 * Created on 14.07.06
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.ediabas.*;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;

/**
 * Implementierung der Pr�fprozedur, die per Diagnose �ber eine Gruppendatei die aktuelle
 * SGBD_Variante ermittelt und diese u.U. diversen Soll-Varianten gegen�berstellt. Ist die
 * �bergebene Sollvariante identisch der Gruppe, unterbleibt ausschliesslich die Bewertung.
 * Ist die Bewertung ok oder wurde explizit keine Bewertung gew�nscht, wird die ermittelte SGBD
 * als Attribut im Pr�fling eingetragen und s�mtliche Pr�fprozeduren des Prueflings upgedatet. 
 *
 * @version V_1_0_F 14.07.2006  TB   Ersterstellung auf Basis DiagSgbdSiv + E65 Sachen raus + neuer Weg Ediabas zu holen<BR>
 *          V_2_0_F 07.09.2006  TB BugFix ECOS <BR>
 *          V_3_0_F 08.09.2006  TB Verschoenerung ECOS <BR>
 *          V_4_0_T 18.11.2012	MS Erweiterung Diag_Parallel <BR>
 *          V_5_0_F 25.01.2017  MKe F-Version <BR>
 *          V_6_0_T 20.04.2017  MKe Freigabe der Parallel Diagnose am Ende der execute Methode geschoben <BR>
 *          V_7_0_F 20.04.2017  MKe F-Version <BR>

 */
public class DiagSgbdSivUDS_7_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagSgbdSivUDS_7_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DiagSgbdSivUDS_7_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = new String[0];
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "Gruppe", "SollSGBD" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	public boolean checkArgs() {
		boolean ok;
		try {
			ok = super.checkArgs();
			if( ok == true ) {
				if( getArg( getRequiredArgs()[0] ).indexOf( ';' ) != -1 )
					return false;
			}
			return ok;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String gruppe = "";
		String[] sollSgbden;
		String temp = "";
		int i;

		//MSa Paralleldiagnose:
		boolean parallel = false; // MSa, paralleldiagnose
		String tag = null; // MSa, Paralleldiagnose

		// MBa: Paralleldiagnose >>>>>
		try // Pr�fstandvariabel auslesen, ob Paralleldiagnose
		{
			Object pr_var;

			pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
			if( pr_var != null ) {
				if( pr_var instanceof String ) {
					if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
						parallel = true;
					}
				} else if( pr_var instanceof Boolean ) {
					if( ((Boolean) pr_var).booleanValue() ) {
						parallel = true;
					}
				}
			}
		} catch( VariablesException e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
		}

		if( getArg( "TAG" ) != null )
			tag = getArg( "TAG" );

		EdiabasProxyThread myEdiabas = null;

		/***********************************************
		 * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
		 * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		 ***********************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		try {
			//Parameter holen 
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				gruppe = getArg( getRequiredArgs()[0] );
				sollSgbden = extractValues( getArg( getRequiredArgs()[1] ) );
			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// neuer Weg Ediabas zu holen
			if( parallel )
				myEdiabas = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabasParallel( tag, gruppe );
			else
				myEdiabas = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();

			try {
				//Ausf�hrung
				myEdiabas.apiJob( gruppe, "INITIALISIERUNG", "", "VARIANTE" ); //Achtung Jobstatus existiert nicht!!!
				temp = myEdiabas.getDiagResultValue( 0, "VARIANTE" ).toUpperCase();
			} catch( ApiCallFailedException e ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", gruppe, "INITIALISIERUNG", "", "VARIANTE", temp, "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), myEdiabas.apiErrorCode() + ": " + myEdiabas.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", gruppe, "INITIALISIERUNG", "", "VARIANTE", temp, "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", gruppe, "INITIALISIERUNG", "", "VARIANTE", temp, "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//Analyse nur wenn SollSgbden != Gruppe
			if( (sollSgbden.length != 1) || (sollSgbden[(sollSgbden.length) - 1].equals( gruppe ) == false) ) {
				status = STATUS_EXECUTION_ERROR;
				i = 0;
				while( (i < sollSgbden.length) && (status == STATUS_EXECUTION_ERROR) ) {
					if( temp.equals( sollSgbden[i] ) == true )
						status = STATUS_EXECUTION_OK;
					i++;
				}
			}

			//Doku
			String temp1 = "";
			if( sollSgbden.length > 0 )
				temp1 = sollSgbden[0];
			for( i = 1; i < sollSgbden.length; i++ ) {
				temp1 = temp1 + ";" + sollSgbden[i];
			}
			if( status == STATUS_EXECUTION_OK )
				result = new Ergebnis( "VARIANTE", "EDIABAS", gruppe, "INITIALISIERUNG", "", "VARIANTE", temp, temp1, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			else
				result = new Ergebnis( "VARIANTE", "EDIABAS", gruppe, "INITIALISIERUNG", "", "VARIANTE", temp, temp1, "", "0", "", "", "", PB.getString( "variantenfehler" ), PB.getString( "sgTauschen" ), Ergebnis.FT_NIO );
			ergListe.add( result );

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		if( parallel ) //&& ept!=null)
		{
			try {
				this.getPr�flingLaufzeitUmgebung().getDeviceManager().releaseEdiabasParallel( myEdiabas, tag, gruppe );
			} catch( DeviceLockedException ex ) {
				result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", ex.getMessage(), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Unexpected runtime error at releasing the parallel diagnosis", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable ex ) {
				result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Unexpected runtime error at releasing the parallel diagnosis", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		setPPStatus( info, status, ergListe );
	}

}

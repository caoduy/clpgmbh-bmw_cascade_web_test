package com.bmw.cascade.pruefprozeduren;

import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.devices.DeviceExecutionException;
import com.bmw.cascade.devices.DynamicDevice;
import com.bmw.cascade.devices.DynamicDeviceListener;
import com.bmw.cascade.devices.DynamicDeviceManager;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.server.PB;
import com.bmw.cascade.util.dynamicdevices.PropertyObject;
import com.bmw.cascade.util.dynamicdevices.WrongTypeException;

/**
 * Implementation of a LIN window lifter (IFE) movement
 * 
 * @author Torsten Mager, GEFASOFT Engineering GmbH, BMW TI-545
 * @version V1_0_F 19.03.2014 TM Implementierung
 * @version V2_0_0 14.05.2014 TM Fixed released user dialog bug; coding style
 * @version V3_0_0 14.07.2014 TM Coding style
 * @version V4_0_0 22.09.2014 TM Removed parameters 'rear' and 'istep', added parameters 'type' and 'component'
 * @version V5_0_0 24.09.2014 TM Added user dialogue, added parameters 'dlg_caption' and 'dlgtext_send_params'
 * @version V6_0_0 13.10.2014 TM Added IFE answer logging
 * @version V7_0_0 28.10.2014 TM Added user dialogue release
 * @version V8_0_F 17.02.2015 TM Changed error from system to normal for cancelling and measure errors
 * @version V9_0_F 14.07.2015 TM Added 'user dialogue active' check to user dialogue cancelling request
 * @version V10_0_T 04.08.2015 TM T version
 * @version V11_0_F 07.08.2015 TM F version
 */
public class MPS3DynamicIfe_11_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	static final long serialVersionUID = 1L;
	
	private PropertyObject myPO = new PropertyObject("");
	
	private UserDialog userdialog;
	String dlgCaption;
	
	/**
	 * Default constructor, only for de-seriaslisation
	 */
	public MPS3DynamicIfe_11_0_F_Pruefprozedur() {
		
	}
	
	/**
	 * Creates a new test procedure
	 * 
	 * @param pruefling			Class of related Pruefling
	 * @param pruefprozName		Name of test procedure
	 * @param hasToBeExecuted	Execution condition for absence of errors
	 */
	public MPS3DynamicIfe_11_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
		super(pruefling, pruefprozName, hasToBeExecuted);
		attributeInit();
	}
	
	/**
	 * Initialises arguments
	 */
	protected void attributeInit() {
		super.attributeInit();
	}
	
	/**
	 * Provides optional arguments
	 */
	public String[] getOptionalArgs() {
		String[] args = {"COMPONENT", "DLG_CAPTION", "DLGTXT_SEND_PARAMS"/*, "DEBUG"*/};
		
		return args;
	}
	
	/**
	 * Provides mandatory arguments
	 */
	public String[] getRequiredArgs() {
		String[] args = {"TAG", "DIGIN", "ACTION", "DIRECTION", "VOLTAGE", "CURR_LIM", "REPETITIONS", "TIME_DOWN", "TIME_UP", "PAUSE", "FRAMELESS", "GAP", "TYPE", "TERMINATION", "BUTTON"};
		
		return args;
	}
	
	/**
	 * Checks arguments regarding existence and value, as far as possible
	 */
	public boolean checkArgs() {
		boolean ok;
		
		try {
			ok = super.checkArgs();
			return ok;
		} catch (Exception e) {
			return false;
		} catch (Throwable e) {
			return false;
		}
	}
	
	/**
	 * Executes test procedure
	 * 
	 * @param info	Information for execution
	 */
	public void execute(ExecutionInfo info) {
		DynamicDeviceManager ddm = null;
		DynamicDevice cardDevice = null;
		
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_ERROR;
		
		//TODO: remove isDe or rebuild
		boolean isDe = CascadeProperties.getLanguage().equalsIgnoreCase("DE");
		
		Logger mps3Logger = null;
		
		String tag				= null;
		int digin				= 0;
		int action				= 0;
		int direction			= 0;
		int voltage				= 0;
		int current				= 0;
		int count				= 0;
		int down				= 0;
		int up					= 0;
		int pause				= 0;
		boolean frameless		= false;
		int gap					= 0;
		int type				= 0;
		boolean termination		= false;
		int button				= 0;
		//int debug				= 0;
		
		dlgCaption				= null;
		String dlgTxtSendParams	= null;
		
		String compText			= null;
		
		//Required
		tag = getArg("TAG").trim().toUpperCase();
		digin = Integer.parseInt(getArg("DIGIN").trim().toUpperCase());
		action = Integer.parseInt(getArg("ACTION").trim().toUpperCase());
		direction = Integer.parseInt(getArg("DIRECTION").trim().toUpperCase());
		voltage = Integer.parseInt(getArg("VOLTAGE").trim().toUpperCase());
		current = Integer.parseInt(getArg("CURR_LIM").trim().toUpperCase());
		count = Integer.parseInt(getArg("REPETITIONS").trim().toUpperCase());
		down = Integer.parseInt(getArg("TIME_DOWN").trim().toUpperCase());
		up = Integer.parseInt(getArg("TIME_UP").trim().toUpperCase());
		pause = Integer.parseInt(getArg("PAUSE").trim().toUpperCase());
		frameless = ((Boolean) getValidParameterWithDefault("FRAMELESS", false));
		gap = Integer.parseInt(getArg("GAP").trim().toUpperCase());
		type = Integer.parseInt(getArg("TYPE").trim().toUpperCase());
		termination = ((Boolean) getValidParameterWithDefault("TERMINATION", false));
		button = Integer.parseInt(getArg("BUTTON").trim().toUpperCase());
		
		//Optional
		if (isDe) {
			dlgCaption = (String) getValidParameterWithDefault("DLG_CAPTION", "IFE.");
		} else {
			dlgCaption = (String) getValidParameterWithDefault("DLG_CAPTION", "IFE.");
		}
		if (isDe) {
			dlgTxtSendParams = (String) getValidParameterWithDefault("DLGTXT_SEND_PARAMS", "IFE: Parameter senden...");
		} else {
			dlgTxtSendParams = (String) getValidParameterWithDefault("DLGTXT_SEND_PARAMS", "IFE: Sending parameters...");
		}
		/*
		debug = ((Integer) getValidParameterWithDefault("DEBUG", (int) -1)).intValue();
		if (debug == -1) {
			if (((Boolean) getValidParameterWithDefault("DEBUG", false)).booleanValue()) {
				debug = 1;
			} else {
				debug = 0;
			}
		}
		*/
		
		if (isDe) {
			compText = (String) getValidParameterWithDefault("COMPONENT", "Fehler w�hrend IFE-Kommunikation.");
		} else {
			compText = (String) getValidParameterWithDefault("COMPONENT", "Error during IFE communication.");
		}
		
		ddm = getPr�flingLaufzeitUmgebung().getDynamicDeviceManager();
		try {
			cardDevice = ddm.requestDevice("MPS3", "IfeDevice");
			if (cardDevice == null) {
				if (isDe) {
					throw new DeviceExecutionException("Kann Device nicht holen");
				} else {
					throw new DeviceExecutionException("Cannot get Device");
				}
			}
		} catch (Exception ex) {
			Ergebnis apdmErg = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
			apdmErg.setWerkzeug("MPS3, LinDevice");
			apdmErg.setFehlerTyp(Ergebnis.FT_NIO_SYS);
			apdmErg.setFehlerText(ex.getMessage());
			ergListe.add(apdmErg);
			setPPStatus(info, status, ergListe);
			return;
		}
		
		MPS3Listener listener = new MPS3Listener(myPO);
		cardDevice.setDeviceListener(listener);
		
		Set<String> methods = cardDevice.getMethodNames();
		if (!methods.contains("connect") || !methods.contains("communicate") || !methods.contains("closeChannel") || !methods.contains("getLogger")) {
			String err = null;
			
			if (isDe) {
				err = "Devicemethoden fehlen";
			} else {
				err = "Device doesn't contain all required methods";
			}
			
			Ergebnis apdmErg = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
			apdmErg.setWerkzeug("MPS3, IfeDevice");
			apdmErg.setFehlerTyp(Ergebnis.FT_NIO_SYS);
			apdmErg.setFehlerText(err);
			ergListe.add(apdmErg);
			setPPStatus(info, status, ergListe);
			return;
		}
		
		String cardId = null;
		boolean resHasErrors;
		boolean isCancelled = false;
		List<PropertyObject> resErrors;
		int[] resMessage;
		String resMessageAsString = new String();
		
		try {
			List<PropertyObject> result = null;
			String ifeChannel = null;
			
			//Get Logger
			result = cardDevice.execute("getLogger");
			mps3Logger = Logger.getLogger(result.get(0).asString());
			
			//Connect
			result = cardDevice.execute("connect", new PropertyObject(tag));
			ifeChannel = result.get(0).asString();
			mps3Logger.log(Level.INFO, "Card connected");
			
			userdialog = null;
			try {
				if (getPr�flingLaufzeitUmgebung() != null) {
					userdialog = getPr�flingLaufzeitUmgebung().getUserDialog();
					if (userdialog != null) {
						userdialog.setAllowCancel(true);
						userdialog.displayMessage(dlgCaption, dlgTxtSendParams, -1);
					}
				}
			} catch (Exception e) {
				String err = null;
				
				if (isDe) {
					err = "Kann Userdialog nicht anzeigen";
				} else {
					err = "Cannot show userdialog";
				}
				
				Ergebnis apdmErg = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
				apdmErg.setWerkzeug("MPS3, IfeDevice");
				apdmErg.setFehlerTyp(Ergebnis.FT_NIO_SYS);
				apdmErg.setFehlerText(err);
				ergListe.add(apdmErg);
				setPPStatus(info, status, ergListe);
				return;
			}
			
			//Communication
			result = cardDevice.execute("communicate", new PropertyObject(ifeChannel), new PropertyObject(digin), new PropertyObject(action), new PropertyObject(direction), new PropertyObject(voltage),
												new PropertyObject(current), new PropertyObject(count), new PropertyObject(down), new PropertyObject(up), new PropertyObject(pause), new PropertyObject(frameless),
												new PropertyObject(gap), new PropertyObject(type), new PropertyObject(termination), new PropertyObject(button));
			//Close
			cardDevice.execute("closeChannel", new PropertyObject(tag));
			
			//Get results
			Ergebnis apdmRes;
			cardId = PropertyObject.getValueFromList(result, "cardId").asString();
			resHasErrors = PropertyObject.getValueFromList(result, "hasErrors").asBoolean();
			resErrors = PropertyObject.getValueFromList(result, "errors").asList();
			
			resMessage = PropertyObject.getValueFromList(result, "message").asIntArray();
			for (int i=0; i<resMessage.length; i++) {
				if (i == 0) {
					resMessageAsString = ((Integer)resMessage[i]).toString();
				} else {
					resMessageAsString += " | " + ((Integer)resMessage[i]).toString();
				}
			}
			mps3Logger.log(Level.INFO, "IFE Antwort: " + resMessageAsString);
			
			if (resHasErrors) {
				for (int i=0; i<resErrors.size(); i++) {
					String err = resErrors.get(i).asString();
					mps3Logger.log(Level.WARNING, "MeasureError: " + err);
					
					if (isDe && (err.equalsIgnoreCase("Zeit�berschreitung beim Empfang der IFE-Antwort."))) {
						err = compText + ": " + err;
					} else if (err.equalsIgnoreCase("Timeout while receiving IFE answer.")) {
						err = compText + ": " + err;
					}
					
					apdmRes = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
					apdmRes.setWerkzeug("MPS3, IfeDevice");
					apdmRes.setParameter1(cardId);
					apdmRes.setFehlerTyp(Ergebnis.FT_NIO);
					apdmRes.setFehlerText(err);
					//apdmRes.setHinweisText(compText);
					ergListe.add(apdmRes);
				}
			}
			
			if ((userdialog != null) && (userdialog.isCancelled())) {
				isCancelled = true;
				
				apdmRes = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
				apdmRes.setWerkzeug("MPS3, IfeDevice");
				apdmRes.setParameter1(cardId);
				apdmRes.setFehlerTyp(Ergebnis.FT_NIO);
				if (isDe) {
					apdmRes.setFehlerText("Pr�fung wurde abgebrochen.");
				} else {
					apdmRes.setFehlerText("Test was cancelled.");
				}
				//apdmRes.setHinweisText(compText);
				ergListe.add(apdmRes);
				
				status = STATUS_EXECUTION_ABORTED_BY_USER;
			}
			
			//Evaluate
			if (!resHasErrors && !isCancelled) {
				boolean error = false;
				
				mps3Logger.log(Level.INFO, "Test finished. Evaluation tolerances.");
				
				//TODO: error texts -> array?!?
				
				//Response
				boolean err = false;
				apdmRes = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
				apdmRes.setWerkzeug("MPS3, IfeDevice");
				apdmRes.setParameter1(cardId);
				if (isDe) {
					apdmRes.setErgebnis("IFE - Antwortnachricht");
				} else {
					apdmRes.setErgebnis("IFE - answer message");
				}
				apdmRes.setErgebnisWert(resMessageAsString);
				
				if (err) {
					error = true;
				} else {
					apdmRes.setFehlerTyp(Ergebnis.FT_IO);
				}
				
				//apdmRes.setFehlerTyp(Ergebnis.FT_IGNORE);
				ergListe.add(apdmRes);
				
				if (!error) {
					status = STATUS_EXECUTION_OK;
				}
			}
		} catch (Exception e) {
			try {
				cardDevice.execute("closeChannel", new PropertyObject(tag));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			if (mps3Logger != null) {
				mps3Logger.log(Level.WARNING, "Caught Exeption: " + e.getMessage());
			}
			
			Ergebnis apdmErg = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
			apdmErg.setWerkzeug("MPS3, IfeDevice");
			apdmErg.setFehlerTyp(Ergebnis.FT_NIO_SYS);
			ergListe.add(apdmErg);
		}
		
		try {
			if ((userdialog != null) && (userdialog.isShowing())) {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		setPPStatus(info, status, ergListe);
	}
	
	/**
	* Listener which can be handed over to a Dynamic Device.
	* It's used for getting news inside of the test procedure
	* after the state of the Dynamic Device has changed.
	*/
	public class MPS3Listener implements DynamicDeviceListener {
		/**
		* Constructor
		* 
		* @param po		PropertyObject
		*/
		public MPS3Listener(PropertyObject po) {
			myPO = po;
		}
		
		/**
		* Called via invocation of method "fireStateChanged" at Dynamic Device
		*/
		public void stateChanged( PropertyObject changedObject ) {
			try {
				myPO = changedObject;
				if (myPO.isString()) {
					userdialog. displayMessage(dlgCaption, myPO.asString(), -1);
				}
			} catch (WrongTypeException e) {
				//Nothing to do, can't happen
			}
		}
		
		/**
		* Getter for dynamically changing property object
		* 
		* @return myPO		PropertyObject
		*/
		public PropertyObject getMyPO() {
			return myPO;
		}
	}
	
	/**
	 * Checks for a valid parameter
	 * 
	 * @param name				Name of parameter
	 * @param referenceObject	Class of parameter
	 * 
	 * @throws PPExecutionException 
	 */
	private boolean hasValidParameter(String name, Object referenceObject) throws PPExecutionException {
		String value = getArg(name);
		
		if ((value == null) || (value.length() == 0)) {
			return false;
		}
		
		value = value.trim().toUpperCase();
		
		//TODO: to be completed
		if (referenceObject.getClass().equals(String.class)) {
			return true;
		} else if (referenceObject.getClass().equals(Byte.class)) {
			try {
				Byte.parseByte(value);
			} catch (Exception e) {
				return false;
			}
			
			return true;
		} else if (referenceObject.getClass().equals(Integer.class)) {
			try {
				Integer.parseInt(value);
			} catch (Exception e) {
				return false;
			}
			
			return true;
		} else if (referenceObject.getClass().equals(Float.class)) {
			try {
				Float.parseFloat(value);
			} catch (Exception e) {
				return false;
			}
			
			return true;
		} else if (referenceObject.getClass().equals(Double.class)) {
			try {
				Double.parseDouble(value);
			} catch (Exception e) {
				return false;
			}
			
			return true;
		} else if (referenceObject.getClass().equals(Boolean.class)) {
			if (value.equals("T") || value.equals("TRUE") || value.equals("F") || value.equals("FALSE")) {
				return true;
			} else {
				return false;
			}
		} else {
			throw new PPExecutionException("Parameter: " + name + PB.getString("parameterexistenz") + ". (unknown data type)");
		}
	}
	
	/**
	 * Returns content of a valid parameter
	 * 
	 * @param name				Name of parameter
	 * @param referenceObject	Class of parameter
	 * 
	 * @return					Value of parameter
	 * @throws PPExecutionException 
	 */
	private Object getValidParameter(String name, Object referenceObject) throws PPExecutionException {
		String value = getArg(name);
		
		if (!hasValidParameter(name, referenceObject)) {
			return null;
		}
		
		value = value.trim().toUpperCase();
		
		if (referenceObject.getClass().equals(String.class)) {
			return value;
		} else if (referenceObject.getClass().equals(Byte.class)) {
			try {
				return Byte.parseByte(value);
			} catch (Exception e) {
				return null;
			}
		} else if (referenceObject.getClass().equals(Integer.class)) {
			try {
				return Integer.parseInt(value);
			} catch (Exception e) {
				return null;
			}
		} else if (referenceObject.getClass().equals(Float.class)) {
			try {
				return Float.parseFloat(value);
			} catch (Exception e) {
				return null;
			}
		} else if (referenceObject.getClass().equals(Double.class)) {
			try {
				return Double.parseDouble(value);
			} catch (Exception e) {
				return null;
			}
		} else if (referenceObject.getClass().equals(Boolean.class)) {
			if (value.equals("T") || value.equals("TRUE")) {
				return true;
			} else if (value.equals("F") || value.equals("FALSE")) {
				return false;
			} else {
				return null;
			}
		} else {
			throw new PPExecutionException("Parameter: " + name + PB.getString("parameterexistenz") + ". (unknown data type)");
		}
	}
	
	/**
	 * Returns content of a valid parameter or default object, if no valid content found
	 * 
	 * @param name					Name of parameter
	 * @param referenceAndDefault	Class of reference parameter
	 * 
	 * @return						Value of parameter or default object
	 */
	private Object getValidParameterWithDefault(String name, Object referenceAndDefault) {
		Object returnValue = null;
		
		try {
			returnValue = getValidParameter(name, referenceAndDefault);
		} catch (PPExecutionException e) {
			;
		}
		
		if (returnValue == null) {
			returnValue = referenceAndDefault;
		}
		
		return returnValue;
	}
}

package com.bmw.cascade.pruefprozeduren;

import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.devices.DeviceExecutionException;
import com.bmw.cascade.devices.DynamicDevice;
import com.bmw.cascade.devices.DynamicDeviceManager;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.server.PB;
import com.bmw.cascade.util.dynamicdevices.PropertyObject;

/**
 * Implementation of KLINE communication
 * 
 * @author Torsten Mager, GEFASOFT Engineering GmbH, BMW TI-545
 * @version V1_0_F 21.11.2011 TM First implementation
 * @version V3_0_F 14.05.2014 TM Fixed released user dialogue bug; coding style
 * @version V4_0_F 14.07.2014 TM Coding style
 * @version V5_0_F 08.12.2014 TM Added optional parameter COMPONENT
 * @version V6_0_F 17.02.2015 TM Changed error from system to normal for cancelling and measure errors
 * @version V7_0_F 14.07.2015 TM Deleted user dialogue cancelling and releasing requests
 * @version V8_0_T 04.08.2015 TM T version
 * @version V9_0_F 07.08.2015 TM F version
 */
public class MPS3DynamicKLine_9_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	static final long serialVersionUID = 1L;
	
	/**
	 * Default constructor, only for de-seriaslisation
	 */
	public MPS3DynamicKLine_9_0_F_Pruefprozedur() {
		
	}
	
	/**
	 * Creates a new test procedure
	 * 
	 * @param pruefling			Class of related Pruefling
	 * @param pruefprozName		Name of test procedure
	 * @param hasToBeExecuted	Execution condition for absence of errors
	 */
	public MPS3DynamicKLine_9_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
		super(pruefling, pruefprozName, hasToBeExecuted);
		attributeInit();
	}
	
	/**
	 * Initialises arguments
	 */
	protected void attributeInit() {
		super.attributeInit();
	}
	
	/**
	 * Provides optional arguments
	 */
	public String[] getOptionalArgs() {
		String[] args = {/*"DEBUG",*/"APDM_CUSTOM_ERRORTEXT", "APDM_TEST_DESCRIPTION", "COMPONENT"};
		
		return args;
	}
	
	/**
	 * Provides mandatory arguments
	 */
	public String[] getRequiredArgs() {
		String[] args = {"TAG", "MKHWID", "TIMEOUT", "CURR_LIMIT", "CURR_DELAY", "CURR_TIME", "BAUDRATE", "TERMINATION"};
		
		return args;
	}
	
	/**
	 * Checks arguments regarding existence and value, as far as possible
	 */
	public boolean checkArgs() {
		boolean ok;
		
		try {
			ok = super.checkArgs();
			return ok;
		} catch (Exception e) {
			return false;
		} catch (Throwable e) {
			return false;
		}
	}
	
	/**
	 * Executes test procedure
	 * 
	 * @param info	Information for execution
	 */
	public void execute(ExecutionInfo info) {
		DynamicDeviceManager ddm = null;
		DynamicDevice cardDevice = null;
		
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_ERROR;
		
		//TODO: remove isDe or rebuild
		boolean isDe = CascadeProperties.getLanguage().equalsIgnoreCase("DE");
		
		Logger mps3Logger = null;
		
		String tag				= null;
		String mkhwid			= null;
		int timeout				= 0;
		int baudrate			= 0;
		boolean termination		= false;
		int currLim				= 0;
		int currTime			= 0;
		int currDelay			= 0;
		String compText			= null;
		//int debug				= 0;
		
		//Required
		tag = getArg("TAG").trim().toUpperCase();
		mkhwid = getArg("MKHWID").trim().toUpperCase();
		timeout = Integer.parseInt(getArg("TIMEOUT").trim().toUpperCase());
		baudrate = Integer.parseInt(getArg("BAUDRATE").trim().toUpperCase());
		termination = ((Boolean) getValidParameterWithDefault("TERMINATION", false));
		
		// Optional
		/*
		debug = ((Integer) getValidParameterWithDefault("DEBUG", (int) -1)).intValue();
		if (debug == -1) {
			if (((Boolean) getValidParameterWithDefault("DEBUG", false)).booleanValue()) {
				debug = 1;
			} else {
				debug = 0;
			}
		}
		*/
		
		currLim = ((Integer) getValidParameterWithDefault("CURR_LIMIT", (int) 3000)).intValue();
		currTime = ((Integer) getValidParameterWithDefault("CURR_TIME", (int) 500)).intValue();
		currDelay = ((Integer) getValidParameterWithDefault("CURR_DELAY", (int) 0)).intValue();
		
		if (isDe) {
			compText = (String) getValidParameterWithDefault("COMPONENT", "Fehler w�hrend KLINE-Kommunikation.");
		} else {
			compText = (String) getValidParameterWithDefault("COMPONENT", "Error during KLINE communication.");
		}
		
		ddm = getPr�flingLaufzeitUmgebung().getDynamicDeviceManager();
		try {
			cardDevice = ddm.requestDevice("MPS3", "KLineDevice");
			if (cardDevice == null) {
				if (isDe) {
					throw new DeviceExecutionException("Kann Device nicht holen");
				} else {
					throw new DeviceExecutionException("Cannot get Device");
				}
			}
		} catch (Exception ex) {
			Ergebnis apdmErg = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
			apdmErg.setWerkzeug("MPS3, KLineDevice");
			apdmErg.setFehlerTyp(Ergebnis.FT_NIO_SYS);
			apdmErg.setFehlerText(ex.getMessage());
			ergListe.add(apdmErg);
			setPPStatus(info, status, ergListe);
			return;
		}
		
		Set<String> methods = cardDevice.getMethodNames();
		if (!methods.contains("connect") || !methods.contains("communicate") || !methods.contains("closeChannel") || !methods.contains("getLogger")) {
			String err = null;
			
			if (isDe) {
				err = "Devicemethoden fehlen";
			} else {
				err = "Device doesn't contain all required methods";
			}
			
			Ergebnis apdmErg = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
			apdmErg.setWerkzeug("MPS3, KLineDevice");
			apdmErg.setFehlerTyp(Ergebnis.FT_NIO_SYS);
			apdmErg.setFehlerText(err);
			ergListe.add(apdmErg);
			setPPStatus(info, status, ergListe);
			return;
		}
		
		String cardId = null;
		boolean resHasErrors;
		boolean isCancelled = false;
		List<PropertyObject> resErrors;
		int resCurrent;
		int resTime;
		int[] resMessage;
		String resMessageAsString = null;
		
		try {
			List<PropertyObject> result = null;
			String klineChannel = null;
			
			//Get Logger
			result = cardDevice.execute("getLogger");
			mps3Logger = Logger.getLogger(result.get(0).asString());
			
			//Connect
			result = cardDevice.execute("connect", new PropertyObject(tag));
			klineChannel = result.get(0).asString();
			mps3Logger.log(Level.INFO, "Card connected");
			
			//Communicate
			result = cardDevice.execute("communicate", new PropertyObject(klineChannel), new PropertyObject(mkhwid), new PropertyObject(timeout), new PropertyObject(currLim), new PropertyObject(currDelay),
												new PropertyObject(currTime), new PropertyObject(baudrate), new PropertyObject(termination));
			
			//Close
			cardDevice.execute("closeChannel", new PropertyObject(tag));
			
			//Get results
			Ergebnis apdmRes;
			cardId = PropertyObject.getValueFromList(result, "cardId").asString();
			resHasErrors = PropertyObject.getValueFromList(result, "hasErrors").asBoolean();
			resErrors = PropertyObject.getValueFromList(result, "errors").asList();
			resCurrent = PropertyObject.getValueFromList(result, "current").asInt();
			resTime = PropertyObject.getValueFromList(result, "time").asInt();
			resMessage = PropertyObject.getValueFromList(result, "message").asIntArray();
			for (int i=0; i<resMessage.length; i++) {
				if (i == 0) {
					resMessageAsString = ((Integer)resMessage[i]).toString();
				} else {
					resMessageAsString +=  " | " + ((Integer)resMessage[i]).toString();
				}
			}
			
			if (resHasErrors) {
				for (int i=0; i<resErrors.size(); i++) {
					String err = resErrors.get(i).asString();
					mps3Logger.log(Level.WARNING, "MeasureError: " + err);
					
					if (isDe && (err.equalsIgnoreCase("Zeit�berschreitung beim Empfang der KLINE-Antwort."))) {
						err = compText + ": " + err;
					} else if (err.equalsIgnoreCase("Timeout while receiving KLINE answer.")) {
						err = compText + ": " + err;
					}
					
					apdmRes = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
					apdmRes.setWerkzeug("MPS3, KLineDevice");
					apdmRes.setParameter1(cardId);
					apdmRes.setFehlerTyp(Ergebnis.FT_NIO);
					apdmRes.setFehlerText(err);
					//apdmRes.setHinweisText(compText);
					ergListe.add(apdmRes);
				}
			}
			
			//Evaluate
			if (!resHasErrors && !isCancelled) {
				mps3Logger.log(Level.INFO, "Test finished. Evaluation tolerances.");
				
				//Time
				apdmRes = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
				apdmRes.setWerkzeug("MPS3, KLineDevice");
				apdmRes.setParameter1(cardId);
				if (isDe) {
					apdmRes.setErgebnis("Gemessene Zeit (ms)");
				} else {
					apdmRes.setErgebnis("Measured time (ms)");
				}
				apdmRes.setErgebnisWert(Integer.toString(resTime));
				apdmRes.setFehlerTyp(Ergebnis.FT_IO);
				ergListe.add(apdmRes);
				
				//Current
				apdmRes = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
				apdmRes.setWerkzeug("MPS3, KLineDevice");
				apdmRes.setParameter1(cardId);
				if (isDe) {
					apdmRes.setErgebnis("Gemessener Strom (mA)");
				} else {
					apdmRes.setErgebnis("Measured current (mA)");
				}
				apdmRes.setErgebnisWert(Integer.toString(resCurrent));
				apdmRes.setFehlerTyp(Ergebnis.FT_IO);
				ergListe.add(apdmRes);
				
				//Response
				apdmRes = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
				apdmRes.setWerkzeug("MPS3, KLineDevice");
				apdmRes.setParameter1(cardId);
				if (isDe) {
					apdmRes.setErgebnis("KLine-Antwortnachricht");
				} else {
					apdmRes.setErgebnis("KLine-Answermessage");
				}
				apdmRes.setErgebnisWert(resMessageAsString);
				apdmRes.setFehlerTyp(Ergebnis.FT_IO);
				ergListe.add(apdmRes);
				
				status = STATUS_EXECUTION_OK;
			}
		} catch (Exception e) {
			try {
				cardDevice.execute("closeChannel", new PropertyObject(tag));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			if (mps3Logger != null) {
				mps3Logger.log(Level.WARNING, "Caught Exeption: " + e.getMessage());
			}
			
			Ergebnis apdmErg = new Ergebnis("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
			apdmErg.setWerkzeug("MPS3, KLineDevice");
			apdmErg.setFehlerTyp(Ergebnis.FT_NIO_SYS);
			ergListe.add(apdmErg);
		}
		
		setPPStatus(info, status, ergListe);
	}
	
	/**
	 * Checks for a valid parameter
	 * 
	 * @param name				Name of parameter
	 * @param referenceObject	Class of parameter
	 * 
	 * @throws PPExecutionException
	 */
	private boolean hasValidParameter(String name, Object referenceObject) throws PPExecutionException {
		String value = getArg(name);
		
		if ((value == null) || (value.length() == 0)) {
			return false;
		}
		
		value = value.trim().toUpperCase();
		
		//TODO: to be completed
		if (referenceObject.getClass().equals(String.class)) {
			return true;
		} else if (referenceObject.getClass().equals(Integer.class)) {
			try {
				Integer.parseInt(value);
			} catch (Exception e) {
				return false;
			}
			
			return true;
		} else if (referenceObject.getClass().equals(Float.class)) {
			try {
				Float.parseFloat(value);
			} catch (Exception e) {
				return false;
			}
			
			return true;
		} else if (referenceObject.getClass().equals(Double.class)) {
			try {
				Double.parseDouble(value);
			} catch (Exception e) {
				return false;
			}
			
			return true;
		} else if (referenceObject.getClass().equals(Boolean.class)) {
			if (value.equals("T") || value.equals("TRUE") || value.equals("F") || value.equals("FALSE")) {
				return true;
			} else {
				return false;
			}
		} else {
			throw new PPExecutionException("Parameter: " + name + PB.getString("parameterexistenz") + ". (unknown data type)");
		}
	}
	
	/**
	 * Returns content of a valid parameter
	 * 
	 * @param name				Name of parameter
	 * @param referenceObject	Class of parameter
	 * 
	 * @return					Value of parameter
	 * @throws PPExecutionException
	 */
	private Object getValidParameter(String name, Object referenceObject) throws PPExecutionException {
		String value = getArg(name);
		
		if (!hasValidParameter (name, referenceObject)) {
			return null;
		}
		
		value = value.trim().toUpperCase();
		
		if (referenceObject.getClass().equals(String.class)) {
			return value;
		} else if (referenceObject.getClass().equals(Integer.class)) {
			try {
				return Integer.parseInt(value);
			} catch (Exception e) {
				return null;
			}
		} else if (referenceObject.getClass().equals(Float.class)) {
			try {
				return Float.parseFloat(value);
			} catch (Exception e) {
				return null;
			}
		} else if (referenceObject.getClass().equals(Double.class)) {
			try {
				return Double.parseDouble(value);
			} catch (Exception e) {
				return null;
			}
		} else if (referenceObject.getClass().equals(Boolean.class)) {
			if (value.equals("T") || value.equals("TRUE")) {
				return true;
			} else if (value.equals("F") || value.equals("FALSE")) {
				return false;
			} else {
				return null;
			}
		} else {
			throw new PPExecutionException("Parameter: " + name + PB.getString("parameterexistenz") + ". (unknown data type)");
		}
	}
	
	/**
	 * Returns content of a valid parameter or default object, if no valid content found
	 * 
	 * @param name					Name of parameter
	 * @param referenceAndDefault	Class of reference parameter
	 * 
	 * @return						Value of parameter or default object
	 */
	private Object getValidParameterWithDefault(String name, Object referenceAndDefault) {
		Object returnValue = null;
		
		try {
			returnValue = getValidParameter(name, referenceAndDefault);
		} catch (PPExecutionException e) {
			;
		}
		
		if (returnValue == null) {
			returnValue = referenceAndDefault;
		}
		
		return returnValue;
	}
}

package com.bmw.cascade.pruefprozeduren;

import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;

/**
 * PP zur Ermittlung einer beliebigen Pr�fstandsvariable, n�tigenfalls auch rekursiv.<BR>
 * <BR>
 * @author 	Buboltz BMW AG; F. Sch�nert BMW AG <BR>
 * @version	1_0_F 09.03.2014 TB Ersterstellung. <BR>
 * @version 2_0_T 23.11.2015 FS Bugfix: Beziehen von intern erweiterten Variablen repariert. <BR>
 * @version 3_0_F 23.11.2015 FS Freigabe Version 2_0_T. <BR>
 * @version 4_0_T 16.12.2015 PR Beim Schrieben der Ergebnisse (jene, welche die gelesene Variable enthalten) ist der Result-Name = Result-ID. <BR>
 * @version 5_0_F 16.12.2015 PR Freigabe Version 4_0_T. <BR>
 * @version 6_0_T 22.06.2016 PR Reparatur des Rekursionsmechanismus. <BR>
 * @version 7_0_F 27.07.2015 PR Freigabe Version 6_0_T. <BR>
 * @version 8_0_T 14.02.2017 TB LOP 2162 BugFix Bei PP: GetPSVariable wird wieder zus�tzlich der Resultname als "RESULT" mit weggeschrieben. Dies wird u.a. f�r die Werkeridentifikation von APDM so erwartet. <BR>
 * @version 9_0_F 14.02.2017 TB F-Version <BR>
 * @version 10_0_T 16.02.2017 MKe Der Name des Werkers wurde bei der Ergebniserzeugung aus der Min Spalte entfernt  <BR>
 * @version 11_0_F 24.03.2017 MKe F-version  <BR>
 */
public class GetPSVariable_11_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public GetPSVariable_11_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public GetPSVariable_11_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "RECURSIVE", "DEBUG" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "NAME" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	@Override
	public boolean checkArgs() {
		// Obligatorisch.
		return super.checkArgs();
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// �bergabeparameter
		String name = null;
		boolean recursive = true;
		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen

		try {
			// Argumente einlesen und checken
			try {
				// DEBUG abfragen
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
				} else {
					// noch Pr�fstandvariable pr�fen, ob Debug
					try {
						Object pr_var_debug;

						pr_var_debug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
						if( pr_var_debug != null ) {
							if( pr_var_debug instanceof String ) {
								if( "TRUE".equalsIgnoreCase( (String) pr_var_debug ) ) {
									bDebug = true;
								} else {
									bDebug = false;
								}
							} else if( pr_var_debug instanceof Boolean ) {
								if( ((Boolean) pr_var_debug).booleanValue() ) {
									bDebug = true;
								} else {
									bDebug = false;
								}
							}
						} else {
							bDebug = false;
						}

					} catch( VariablesException e ) {
						bDebug = false;
					}
				}

				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// Name der PS Variable, als zwingendes Argument direkt als Name oder �ber Refernzoperator
				name = extractValues( getArg( "NAME" ) )[0];

				// Info JOB, als zwingendes Argument und �ber Refernzoperator verf�gbar machen
				if( getArg( "RECURSIVE" ) != null ) {
					recursive = "TRUE".equalsIgnoreCase( extractValues( getArg( "RECURSIVE" ) )[0] );
				}

				// Debug
				if( bDebug ) {
					System.out.println( "PP GetPSVariable, Parameter: name = <" + name + ">" );
					System.out.println( "PP GetPSVariable, Parameter: recurive = <" + recursive + ">" );
				}
			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Variable aufl�sen
			Object value = getVariableValue( name );
			if( recursive ) {
				Object temp;
				while( value != null && value instanceof java.lang.String ) {
					if( bDebug )
						System.out.println( "PP GetPSVariable, calculated value:" + value );

					temp = getVariableValue( (String) value );
					if( temp != null )
						value = temp; //als Variablenwert �bernehmen, der eventl. ein weiterer Variablenname ist	 
					else
						break; //Ende, letzter Variablenwert ist der finale Wert						 
				}
			}

			// dokumentieren als Variablenname vgl. LOP 2018
			if( value != null ) {
				result = new Ergebnis( name, "GetPSVariable", "", "", "", name, value.toString(), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			} else {
				result = new Ergebnis( name, "GetPSVariable", "", "", "", name, "null", "not null", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			}
			ergListe.add( result );

			// dokumentieren als "RESULT" vgl. LOP 2162
			if( value != null ) {
				result = new Ergebnis( name, "GetPSVariable", "", "", "", "RESULT", value.toString(), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			} else {
				result = new Ergebnis( name, "GetPSVariable", "", "", "", "RESULT", "null", "not null", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			}
			ergListe.add( result );

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "GetPSVariable", "", "", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}
		setPPStatus( info, status, ergListe );
	}

	private Object getVariableValue( String name ) {
		Object value = null;
		try {
			value = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, name );
		} catch( Exception x ) {
			// Nichts
		}
		try {
			value = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, name );
		} catch( Exception x ) {
			// Nichts
		}
		try {
			value = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PERSISTENT, name );
		} catch( Exception x ) {
			// Nichts
		}
		return value;

	}
}

/*
 * DiagToleranzLoop_x_y_F_Pruefprozedur.java
 *
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;

/**
 * DiagToleranzLoop_Vx_x_x_xx_Pruefprozedur
 * Implementierung der Pr�fprozedur, die f�r eine vorgegebene Anzahl an Durchl�ufen per Diagnose diverse
 * Einzelinformationen abholt. Werden kewine Results angegeben, besteht die Einzelinformation ausschlie�lich aus
 * der Kontrolle des Jobstatus (d.h. n-malige Ausf�hrung eines Jobs). Sind Results angegeben, dann m�ssen dies
 * Zahlenwerte sein, deren Mittelwert, den Minimal- und den Maximalwert ermittelt und anschlie�end
 * bewertet werden (auf Einhaltung der Grenzen bzw. auf Nichteinhaltung).
 * @author BMW TI-430 Schaller, BMW TI-431 Semmler
 *
 * @version 0_0_1  14.09.2000  RS  Ersterstellung aus DiagToleranz
 *          0_0_3  20.03.2002  RS  Umstellung des Attribut INVERT auf optionales Argument, Generierung als TA
 *          0_0_4  21.03.2002  RS  TA --> FA
 *          0_0_5  09.04.2002  RS  Ergebnisausgabe-Anpassung nach AVG(), MIN() oder MAX(); Alternativ-Toleranzen deleted
 *          0_0_6  10.04.2002  RS  TA --> FA
 *          0_0_7  27.02.2003  RS  "schoener wohnen" im Sourcecode
 *          0_0_8  25.11.2003  AS  Bug-Fix: Darstellung negativer Werte (TA)
 *          0_0_9  25.11.2003  AS  wie 0_0_7 (Fall-Back-Solution)
 *	    	0_1_0  01.07.2004  AS  TA --> FA
 *	    	0_1_1  04.08.2004  AS  Bug-Fix: vollst�ndiges Schreiben des virt. Fzg. auch bei Toleranz NIO
 *	    	0_1_2  14.04.2008  IB  Hinweistext(e) HWT hinzugef�gt 
 *			0_1_3  06.05.2008  AU  Hinweistext(e) HWT hinzugef�gt
 *		    14_0_T 30.11.2015  PR  Statt Double.parseDouble rufen wir eine eigene Parse-Methode auf, welche auch mit hexadezimalen Zahlen umgehen kann. Anforderung resultiert aus neuem Sachnummernformat.
 *			15_0_F 30.11.2015  PR  T -> F
 *			16_0_T 03.08.2016  TB  Erweiterungen f�r Paralleldiagnose analog zur DiagToleranzPP (LOP 2015), Umstellung auf EdiabasProxy, �berfl�ssiges Import entfernt + Generics eingebaut + Compiler Warnungen behoben<BR>
 *			17_0_F 25.01.2017  MKe F-Version
 *          18_0_T 30.08.2017  TB  LOP 2245: LOOP Schleifenz�hler soll bzgl. Paralleldiagnosetests den @-Operator unterst�tzen. (Ebenfalls f�r PAUSE Parameter mit eingebaut)<BR>
 *          19_0_F 31.08.2017  TB  F-Version<BR>
 */
public class DiagToleranzLoop_19_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagToleranzLoop_19_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur, die auf Einhaltung bzw. auf Ausschlu� der zu analysierenden Werte pr�ft.
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DiagToleranzLoop_19_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
		setAttribut( "INVERT", "FALSE" );
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "JOBPAR", "RESULT[1..N]", "MIN[1..N]", "MAX[1..N]", "PAUSE", "INVERT", "HWT", "HWT[1..N]", "TAG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "SGBD", "JOB", "LOOPS" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die parent-Methode aufgrund
	 * der offenen Anzahl an Results
	 */
	@Override
	public boolean checkArgs() {
		int i, j;
		int maxIndex = 0;
		boolean exist;

		try {
			// 1. Check: Sind alle requiredArgs gesetzt
			String[] requiredArgs = getRequiredArgs();
			for( i = 0; i < requiredArgs.length; i++ ) {
				if( getArg( requiredArgs[i] ) == null )
					return false;
				if( getArg( requiredArgs[i] ).equals( "" ) == true )
					return false;
			}
			// 2. Check: Sind �berfl�ssige Argumente gesetzt, d.h. Argumente, die weder required noch optional sind
			// Gesetzte Argumente, die mit "RESULT", "MIN" oder "MAX" beginnen, werden hierbei nicht analysiert, es wird
			// jedoch der maximale Index festgehalten. Die detaillierte Analyse erfolgt im 3-ten Check.
			Enumeration<?> enu = getArgs().keys();
			String[] optionalArgs = getOptionalArgs();
			String givenkey;
			String temp;
			while( enu.hasMoreElements() ) {
				givenkey = (String) enu.nextElement();
				if( (givenkey.startsWith( "RESULT" ) == false) && (givenkey.startsWith( "MIN" ) == false) && (givenkey.startsWith( "MAX" ) == false) && ((givenkey.startsWith( "HWT" ) == false) || (givenkey.equals( "HWT" ) == true)) ) {
					exist = false;
					j = 0;
					while( (exist == false) && (j < requiredArgs.length) ) {
						if( givenkey.equals( requiredArgs[j] ) == true )
							exist = true;
						j++;
					}
					j = 0;
					while( (exist == false) && (j < optionalArgs.length) ) {
						if( givenkey.equals( optionalArgs[j] ) == true )
							exist = true;
						j++;
					}
					if( exist == false )
						return false;
					if( getArg( givenkey ).equals( "" ) == true )
						return false;
				} else {
					if( givenkey.startsWith( "RESULT" ) == true ) {
						temp = givenkey.substring( 6 );
					} else {
						temp = givenkey.substring( 3 );
					}
					try {
						j = Integer.parseInt( temp );
						if( j < 1 )
							return false;
						if( j > maxIndex )
							maxIndex = j;
					} catch( NumberFormatException e ) {
						return false;
					}
				}
			}
			// 3. Check: Sind die Result-, Min- und Max-Argumente existent und plausibel
			// (Min und Max sind Zahlen bzw. Result einer anderen Pr�fprozedur)
			String mins, maxs;
			for( i = 1; i <= maxIndex; i++ ) {
				temp = getArg( "RESULT" + i );
				if( temp == null )
					return false;
				if( temp.indexOf( ';' ) != -1 )
					return false;
				if( (i > 1) && (temp.equals( getArg( "RESULT1" ) ) == true) )
					return false;
				temp = getArg( "MIN" + i );
				if( temp == null )
					return false;
				if( temp.indexOf( ';' ) != -1 )
					return false;
				temp = getArg( "MAX" + i );
				if( temp == null )
					return false;
				if( temp.indexOf( ';' ) != -1 )
					return false;
				mins = getArg( "MIN" + i );
				maxs = getArg( "MAX" + i );
				try {
					if( mins.indexOf( '@' ) == -1 )
						parseDouble( mins );
					if( maxs.indexOf( '@' ) == -1 )
						parseDouble( maxs );
				} catch( NumberFormatException e ) {
					return false;
				}
			}
			//4. Check: LOOPS mu� Integer gr��er 0 sein
			try {
				if( Long.parseLong( extractValues( getArg( requiredArgs[2] ) )[0] ) < 1 )
					return false;
			} catch( NumberFormatException e ) {
				return false;
			}
			//5. Check: Falls PAUSE gesetzt, mu� positiver Integer sein
			if( getArg( optionalArgs[4] ) != null ) {
				String pause = extractValues( getArg( optionalArgs[4] ) )[0];
				try {
					if( Long.parseLong( pause ) < 0 )
						return false;
				} catch( NumberFormatException e ) {
					return false;
				}
			}
			//6. Check: Falls INVERT gesetzt, mu� true oder false sein
			if( getArg( optionalArgs[5] ) != null ) {
				try {
					if( (getArg( optionalArgs[5] ).equalsIgnoreCase( "TRUE" ) == false) && (getArg( optionalArgs[5] ).equalsIgnoreCase( "FALSE" ) == false) )
						return false;
				} catch( Exception e ) {
					return false;
				}
			}
			// 7. Check: Argmente SGBD, JOB, RESULT[1..N] d�rfen keine Alternativen enthalten
			enu = getArgs().keys();
			while( enu.hasMoreElements() ) {
				givenkey = (String) enu.nextElement();
				if( (givenkey.equals( "SGBD" ) == true) || (givenkey.equals( "JOB" ) == true) || (givenkey.startsWith( "RESULT" ) == true) ) {
					if( getArg( givenkey ).indexOf( ';' ) != -1 )
						return false;
				}
			}
			// Tests bestanden, somit ok
			return true;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String temp, temp1, temp2;
		String sgbd = null, job, jobpar, jobres;
		String hwt;
		String[] results = null;
		String mins, maxs;
		String[] hwts = null;
		double[] resultValues = null;
		short[] how = null;

		double d;
		long schleifen, pause;
		int resAnzahl;
		int i, j;
		boolean ok;

		boolean invert = false;
		boolean useDebug = true;

		EdiabasProxyThread ediabas = null;

		//Paralleldiagnose
		boolean parallel = false;
		String tag = null;
		DeviceManager devMan = null;

		try {
			//Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				if( useDebug )
					System.out.println( "Checkargs ok" );
				sgbd = extractValues( getArg( getRequiredArgs()[0] ) )[0];
				job = getArg( getRequiredArgs()[1] );
				jobpar = getArg( getOptionalArgs()[0] );
				if( jobpar == null )
					jobpar = "";
				jobres = "";
				resAnzahl = 0;
				while( (temp = getArg( "RESULT" + (resAnzahl + 1) )) != null ) {
					temp = temp.toUpperCase().trim();
					if( ((temp.startsWith( "MIN(" ) == true) || (temp.startsWith( "MAX(" ) == true)) && (temp.endsWith( ")" ) == true) )
						temp = temp.substring( 4, temp.length() - 1 );
					jobres = jobres + ";" + extractValues( temp )[0];
					resAnzahl++;
				}
				if( resAnzahl > 0 ) {
					how = new short[resAnzahl]; //Merken, ob Average, Min oder Max-Bewertung
					for( i = 0; i < resAnzahl; i++ ) {
						temp = getArg( "RESULT" + (i + 1) ).toUpperCase().trim();
						if( ((temp.startsWith( "MIN(" ) == true) || (temp.startsWith( "MAX(" ) == true)) && (temp.endsWith( ")" ) == true) ) {
							if( temp.startsWith( "MIN(" ) == true )
								how[i] = 1;
							else
								how[i] = 2;
						} else {
							how[i] = 0;
						}
					}
					resultValues = new double[resAnzahl]; //Entsprechende Vorbelegung
					for( i = 0; i < resAnzahl; i++ ) {
						if( how[i] == 0 )
							resultValues[i] = 0.0;
						else if( how[i] == 1 )
							resultValues[i] = Double.MAX_VALUE;
						else
							resultValues[i] = Double.MIN_VALUE;
					}
					if( resAnzahl > 0 ) {
						jobres = jobres.substring( 1 );
						results = splitArg( jobres ); //Links sind in jobres bereits aufgel�st
						if( results.length != resAnzahl )
							throw new PPExecutionException( PB.getString( "parameterexistenz" ) + "1" );
						for( i = 1; i <= resAnzahl; i++ ) {
							mins = getArg( "MIN" + i );
							maxs = getArg( "MAX" + i );
							try {
								d = parseDouble( mins );
								d = parseDouble( maxs );
							} catch( NumberFormatException e ) {
								throw new PPExecutionException( PB.getString( "parameterexistenz" ) + "2" );
							}

						}
					}
				}
				//Loops
				schleifen = Long.parseLong( extractValues( getArg( getRequiredArgs()[2] ) )[0] );
				//Pause
				pause = 0;
				temp = getArg( getOptionalArgs()[4] );
				if( temp != null )
					pause = Long.parseLong( extractValues( temp )[0] );
				//Hinweistext
				hwt = getArg( getOptionalArgs()[6] );
				if( hwt == null )
					hwt = "";
				else
					hwt = PB.getString( hwt );
				// mehrere Hinweistexte einlesen, wenn vorhanden
				hwts = new String[resAnzahl];
				for( i = 0; i < resAnzahl; i++ ) {
					hwts[i] = getArg( "HWT" + (i + 1) );
					System.out.println( getArg( "HWT" + (i + 1) ) );
				}
				//Invert
				if( (getAttribut( "INVERT" ) != null) && (getAttribut( "INVERT" ).equalsIgnoreCase( "TRUE" )) )
					invert = true;
				temp = getArg( getOptionalArgs()[5] );
				if( (temp != null) && temp.equalsIgnoreCase( "TRUE" ) )
					invert = true;
				if( (temp != null) && temp.equalsIgnoreCase( "FALSE" ) )
					invert = false;
			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Paralleldiagnose, Pr�fstandvariabel auslesen, ob Paralleldiagnose 
			try {
				Object pr_var;

				pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
				if( pr_var != null ) {
					if( pr_var instanceof String ) {
						if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
							parallel = true;
						}
					} else if( pr_var instanceof Boolean ) {
						if( ((Boolean) pr_var).booleanValue() ) {
							parallel = true;
						}
					}
				}
			} catch( VariablesException e ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				throw new PPExecutionException();
			}

			// Devicemanager
			devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();

			if( getArg( "TAG" ) != null )
				tag = getArg( "TAG" );

			if( parallel ) // Ediabas holen
			{
				try {
					ediabas = devMan.getEdiabasParallel( tag, sgbd );
				} catch( Exception ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Wrong Cascade configuration for parallel diagnosys", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				} catch( Throwable ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				}
			} else {
				try {
					ediabas = devMan.getEdiabas( tag );
				} catch( Exception ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();

				}
			}

			// <<<<< Paralleldiagnose            

			//Die Ausf�hrungsschleife
			temp = "";
			i = 0;
			try {
				for( i = 0; i < schleifen; i++ ) {
					temp = ediabas.executeDiagJob( sgbd, job, jobpar, jobres );
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "" + i, "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					} else if( (resAnzahl == 0) && (i == (schleifen - 1)) ) {
						result = new Ergebnis( "Status", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "" + i, "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
					for( j = 0; j < resAnzahl; j++ ) {
						temp = ediabas.getDiagResultValue( results[j] );
						d = parseDouble( temp );
						// Korrigieren der Vorbelegung bzgl. negativer Zahlen!!! (SS)
						if( i == 0 ) {
							if( how[j] == 0 ) {
								resultValues[j] = (d / (double) schleifen);
							} else {
								resultValues[j] = d;
							}
						} else { // Ende Korrektur
							if( how[j] == 0 ) {
								resultValues[j] = resultValues[j] + (d / (double) schleifen);
							} else if( how[j] == 1 ) {
								if( d < resultValues[j] )
									resultValues[j] = d;
							} else {
								if( d > resultValues[j] )
									resultValues[j] = d;
							}
						}
					}
					if( pause > 0 ) {
						try {
							Thread.sleep( pause );
						} catch( InterruptedException e ) {
						}
					}
				}
			} catch( ApiCallFailedException e ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, temp, "", "", "", "" + i, "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, temp, "", "", "", "" + i, "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, temp, "", "", "", "" + i, "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//Analyse
			status = STATUS_EXECUTION_OK;
			for( i = 0; i < resAnzahl; i++ ) {

				mins = getArg( "MIN" + (i + 1) );
				temp1 = mins;

				maxs = getArg( "MAX" + (i + 1) );
				temp2 = maxs;

				ok = false;

				if( (parseDouble( mins ) <= resultValues[i]) && (parseDouble( maxs ) >= resultValues[i]) )
					ok = true;

				temp = results[i];
				if( how[i] == 0 )
					temp = "AVG(" + results[i] + ")";
				else if( how[i] == 1 )
					temp = "MIN(" + results[i] + ")";
				else
					temp = "MAX(" + results[i] + ")";
				if( (ok == true) && (invert == false) ) {
					//Toleranz, somit IO
					result = new Ergebnis( results[i], "EDIABAS", sgbd, job, jobpar, temp, "" + resultValues[i], temp1, temp2, "" + (schleifen - 1), "", "", "", "", "", Ergebnis.FT_IO );
				} else if( (ok == false) && (invert == true) ) {
					//Invertierte Toleranz, somit IO
					result = new Ergebnis( results[i], "EDIABAS", sgbd, job, jobpar, temp, "" + resultValues[i], "!(" + temp1 + ")", "!(" + temp2 + ")", "" + (schleifen - 1), "", "", "", "", "", Ergebnis.FT_IO );
				} else if( (ok == false) && (invert == false) ) {
					//Toleranz, somit NIO
					String hwtDummy;
					if( hwts[i] != null )
						hwtDummy = hwts[i];
					else
						hwtDummy = hwt;
					result = new Ergebnis( results[i], "EDIABAS", sgbd, job, jobpar, temp, "" + resultValues[i], temp1, temp2, "" + (schleifen - 1), "", "", "", PB.getString( "toleranzFehler3" ), hwtDummy, Ergebnis.FT_NIO );
					status = STATUS_EXECUTION_ERROR;
				} else {
					//Invertierte Toleranz mit ok == true, somit NIO
					String hwtDummy;
					if( hwts[i] != null )
						hwtDummy = hwts[i];
					else
						hwtDummy = hwt;
					result = new Ergebnis( results[i], "EDIABAS", sgbd, job, jobpar, temp, "" + resultValues[i], "!(" + temp1 + ")", "!(" + temp2 + ")", "" + (schleifen - 1), "", "", "", PB.getString( "toleranzFehler4" ), hwtDummy, Ergebnis.FT_NIO );
					status = STATUS_EXECUTION_ERROR;
				}
				ergListe.add( result );
				// A. Semmler, TI-431, Auskommentierung, damit PP nicht abgebrochen wird, wenn Toleranz NIO ist
				// if( status == STATUS_EXECUTION_ERROR ) break;
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Paralleldiagnose
		if( parallel ) {
			try {
				devMan.releaseEdiabasParallel( ediabas, tag, sgbd );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable t ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		// Status setzen
		setPPStatus( info, status, ergListe );
	}

	/** 
	 * Pr�ft, ob der �bergebene Wert eine Zahl im hexadezimalen Format ist.
	 * Dies ist immer dann der Fall, wenn die Buchstaben a-f/A-F im Wert 
	 * vorkommen. Achtung: Nat�rlich kann eine Zahl nur aus Ziffern bestehen
	 * und trotzdem hex sein. In dem Fall geht die Methode von einer Zahl im 
	 * dezimalen Format aus, da eine Unterscheidung zwischen hex und dez nicht
	 * m�glich ist (Unsch�rfe).  
	 *  
	 * @param value Der zu pr�fende Wert.
	 * @return <code>True</code>, wenn die Zahl hexadezimal ist, andernfalls
	 * 		   <code>false</code>. Letztes ist auch der Fall, wenn es sich 
	 * 		   um reinen Text handelt.
	 */
	private boolean isHex( String value ) {
		try {
			Double.parseDouble( value ); //pr�fen, ob wir als Double parsen k�nnen
			return false;
		} catch( NumberFormatException e1 ) { //wenn nicht, dann pr�fen, ob wir als hex parsen k�nnen
			try {
				Long.parseLong( value.replaceAll( " ", "" ), 16 ); //eventuelle Leerzeichen, die als Trennzeichen fungieren (z. B. "A2 FF 7E") entfernen
				return true;
			} catch( NumberFormatException e2 ) {
				return false;
			}
		}
	}

	/** 
	 * Parst den �bergebenen Wert und wandelt ihn in ein Double um. Hierbei
	 * wird automatisch auf verschiedene Zahlenformate R�cksicht genommen.
	 *  
	 * @param value Der zu parsende Wert.
	 * @return Der ermittelte Double-Wert.
	 * @throws NumberFormatException Wenn kein Parsen m�glich ist, da entweder
	 * 		   reiner Text vorliegt oder das Zahlenformat unbekannt ist.  
	 */
	private double parseDouble( String value ) throws NumberFormatException {
		if( isHex( value ) )
			return (double) Long.parseLong( value.replaceAll( " ", "" ), 16 ); //eventuelle Leerzeichen, die als Trennzeichen fungieren (z. B. "A2 FF 7E") entfernen
		else
			return Double.parseDouble( value );
	}

}

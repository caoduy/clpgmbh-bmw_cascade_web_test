package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;

/** Implementierung der Pruefprozedur, die Werte in einem *.ini file �ndert.<BR>
 * <BR>
 * Created on 23.08.04<BR>
 * <BR>
 * Bei Aenderung Fr. Bla� TI-431 Tel. 28158 informieren!<BR>
 * <BR>
 * Bei s�mtlichen Parametern k�nnen Pr�fstandsvariablen �bergeben werden, 
 * welche dann den jeweiligen Wert spezifizieren. In dem Fall muss vor 
 * dem Namen der Variablen ein $ geschrieben werden (wenn die Variable z. B. 
 * VAR_INT hei�t, ist $VAR_INT zu schreiben. Falls der Wert selbst mit einem 
 * $ beginnt, muss diesem ebenfalls ein $ vorangestellt werden ($$), hierbei 
 * w�rde sp�ter das erste $ automatisch entfernt und der Wert auch als Wert 
 * behandelt werden. Hinweis: Dies impliziert, dass Pr�fstandsvariablen nicht 
 * mit einem $ beginnen d�rfen.<BR>
 * <BR>
 * @author BMW TI-431 Bla�<BR>
 * @version V0_0_1  23.08.2004  TB  Implementierung<BR>
 * 			V2_0_F	04.11.2007	PR  Parametrierbarkeit mittels Pr�fstandsvariablen erg�nzt<BR>
 * 			V3_0_F	03.06.2008	CS  APDM Ergebnis Format angepasst.<BR>
 * 			V4_0_F	07.10.2008	CS  Freigabe 3_0_F.<BR>
 * 			V5_0_T  12.05.2016	FS  Optionaler ACCESS_TIMEOUT f�r Dateizugriff. Default: 10 Sekunden.<BR>
 * 			V6_0_T  25.05.2016	MK	Bugfix bez�glich des optionalen Parameters ACCESS_TIMEOUT.<BR>
 *          V7_0_T  07.06.2016	MK	Default Timeout auf 5 Sekunden angepasst.<BR> 
 *          V8_0_F  07.06.2016	MK	Produktive Freigabe Version V_7_0_T.<BR>  
 */
public class DateiIniWrite_8_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	static final long DEFAULT_TIMEOUT = 5;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DateiIniWrite_8_0_F_Pruefprozedur() {
	}

	/**
	   * erzeugt eine neue Pruefprozedur.
	   * @param pruefling Klasse des zugeh. Prueflings
	   * @param pruefprozName Name der Pruefprozedur
	   * @param hasToBeExecuted Ausfuehrbedingung f�r Fehlerfreiheit
	   */
	public DateiIniWrite_8_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "ACCESS_TIMEOUT" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "DIR", "FILENAME", "EXTENSION", "SECTION", "NAME", "WERT" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die parent-Methode aufgrund
	 * der offenen Anzahl an Results
	 */
	public boolean checkArgs() {
		int i, j;
		boolean exist;
		int maxIndex = 0;

		try {
			// 1. Check: Sind alle requiredArgs gesetzt
			String[] requiredArgs = getRequiredArgs();
			for( i = 0; i < requiredArgs.length; i++ ) {
				if( getArg( requiredArgs[i] ) == null )
					return false;
				if( getArg( requiredArgs[i] ).equals( "" ) == true )
					return false;
			}

			// 2. Check: Sind �berfl�ssige Argumente gesetzt, d.h. Argumente, die weder required noch optional sind
			// Ein gesetztes Argument, das mit LINE wird nicht analysiert, es wird aber der maximale Index festgehalten.
			// Die detaillierte Analyse erfolgt im 3-ten Check.
			Enumeration enu = getArgs().keys();
			String[] optionalArgs = getOptionalArgs();
			String givenkey;
			String temp;
			while( enu.hasMoreElements() ) {
				givenkey = (String) enu.nextElement();
				if( (givenkey.startsWith( "DIR" ) == false) && (givenkey.startsWith( "FILENAME" ) == false) && (givenkey.startsWith( "EXTENSION" ) == false) && (givenkey.startsWith( "SECTION" ) == false) && (givenkey.startsWith( "NAME" ) == false) && (givenkey.startsWith( "VALUE" ) == false) ) {
					exist = false;
					j = 0;
					while( (exist == false) && (j < requiredArgs.length) ) {
						if( givenkey.equals( requiredArgs[j] ) == true )
							exist = true;
						j++;
					}
					j = 0;
					while( (exist == false) && (j < optionalArgs.length) ) {
						if( givenkey.equals( optionalArgs[j] ) == true )
							exist = true;
						j++;
					}
					if( exist == false )
						return false;
					if( getArg( givenkey ).equals( "" ) == true )
						return false;
				}
			}

			// Tests bestanden, somit ok
			return true;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * fuehrt die Pruefprozedur aus
	 * @param info Information zur Ausfuehrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String filename, extension, dir, section, name, wert = null;
		long timeout = DEFAULT_TIMEOUT;

		try {
			//Parameter holen
			try {
				//Parameterpruefung
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				//Zwingenden singulaeren Parameter DIR holen
				if( getArg( getRequiredArgs()[0] ).indexOf( '@' ) != -1 )
					dir = getPPResult( getArg( getRequiredArgs()[0] ) ).trim();
				else
					dir = getArg( getRequiredArgs()[0] ).trim();
				dir = processPrefix( dir );
				if( dir == null )
					throw new PPExecutionException( "Pruefstand variable not found!" );

				//Zwingenden singulaeren Parameter NAME holen
				if( getArg( getRequiredArgs()[1] ).indexOf( '@' ) != -1 )
					filename = getPPResult( getArg( getRequiredArgs()[1] ) ).trim();
				else
					filename = getArg( getRequiredArgs()[1] ).trim();
				filename = processPrefix( filename );
				if( filename == null )
					throw new PPExecutionException( "Pruefstand variable not found!" );

				//Zwingenden singulaeren Parameter EXTENSION holen
				if( getArg( getRequiredArgs()[2] ).indexOf( '@' ) != -1 )
					extension = getPPResult( getArg( getRequiredArgs()[2] ) ).trim();
				else
					extension = getArg( getRequiredArgs()[2] ).trim();
				extension = processPrefix( extension );
				if( extension == null )
					throw new PPExecutionException( "Pruefstand variable not found!" );

				//Zwingenden singulaeren Parameter SECTION holen
				if( getArg( getRequiredArgs()[3] ).indexOf( '@' ) != -1 )
					section = getPPResult( getArg( getRequiredArgs()[3] ) ).trim();
				else
					section = getArg( getRequiredArgs()[3] ).trim();
				section = processPrefix( section );
				if( section == null )
					throw new PPExecutionException( "Pruefstand variable not found!" );

				//Zwingenden singulaeren Parameter NAME holen
				if( getArg( getRequiredArgs()[4] ).indexOf( '@' ) != -1 )
					name = getPPResult( getArg( getRequiredArgs()[4] ) ).trim();
				else
					name = getArg( getRequiredArgs()[4] ).trim();
				name = processPrefix( name );
				if( name == null )
					throw new PPExecutionException( "Pruefstand variable not found!" );

				//Zwingenden singulaeren Parameter WERT holen
				if( getArg( getRequiredArgs()[5] ).indexOf( '@' ) != -1 )
					wert = getPPResult( getArg( getRequiredArgs()[5] ) ).trim();
				else
					wert = getArg( getRequiredArgs()[5] ).trim();
				wert = processPrefix( wert );
				if( wert == null )
					throw new PPExecutionException( "Pruefstand variable not found!" );

				//Optionalen Parameter TIMEOUT holen
				if( getArg( getOptionalArgs()[0] ) != null ) {
					timeout = Long.parseLong( getArg( getOptionalArgs()[0] ).trim() );
				}

			} catch( PPExecutionException e ) {
				e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			try {
				//Seperator am Ende kuerzen
				if( dir.endsWith( "\\" ) || (dir.endsWith( "/" )) )
					dir = dir.substring( 0, dir.length() - 1 );
				if( dir.endsWith( "\\\\" ) )
					dir = dir.substring( 0, dir.length() - 2 );

				//Fileobjekt generieren
				File f = new File( dir + System.getProperty( "file.separator" ) + filename + "." + extension );
				BufferedReader in;
				BufferedWriter out;
				Vector zeilen = new Vector();

				// Pr�fe Dateiexistenz in Executorthread um einen Timeout zu erm�glichen
				ExecutorService exSrv = Executors.newSingleThreadExecutor();
				ExecutorCompletionService<Boolean> completionService = new ExecutorCompletionService<Boolean>( exSrv );
				completionService.submit( new CheckFileExistsWriteable( f ) );
				Future<Boolean> fileExistsWriteable = completionService.poll( timeout, TimeUnit.SECONDS );
				if( fileExistsWriteable != null && fileExistsWriteable.get() == true ) {
					exSrv.shutdown();
					//Daten lesen
					in = new BufferedReader( new FileReader( f ) );
					while( in.ready() ) {
						zeilen.add( in.readLine() );
					}
					in.close();

					//Ergebnis Doc
					result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE READ", "TRUE", "TRUE", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );

					//Datei umschreiben
					try {
						String block_name = "[" + section.trim().toUpperCase() + "]";
						String id_name = name.trim().toUpperCase();
						boolean searchPasskey = true;
						boolean foundBlock = false;
						String aktZeile = "";

						for( int i = 0; (i < zeilen.size()) && (searchPasskey == true); i++ ) {
							aktZeile = zeilen.get( i ).toString().trim().toUpperCase();
							if( aktZeile != null ) {
								if( foundBlock == true ) {
									// Passkey ?
									if( aktZeile.startsWith( id_name ) ) {

										zeilen.set( i, id_name + " = " + wert );
										searchPasskey = false;
									}
								} else {
									if( aktZeile.startsWith( block_name ) )
										foundBlock = true;
								}
							}
						}
					} catch( Exception e ) {
						throw new PPExecutionException();
					}

					//Daten rausschreiben
					out = new BufferedWriter( new FileWriter( f ) );
					for( int i = 0; i < zeilen.size(); i++ ) {
						out.write( zeilen.get( i ).toString() );
						out.newLine();
					}
					out.close();

					//Ergebnis Doc
					result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE WRITTEN", "TRUE", "TRUE", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );

				} else {
					//Ergebnis Doc
					result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE WRITE EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					exSrv.shutdown();
					throw new PPExecutionException();
				}
			} catch( CancellationException e ) {
				result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "CANCELLATION_EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( InterruptedException e ) {
				result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "INTERRUPTED_EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( ExecutionException e ) {
				// An execution exception of CheckFileExists can only be due to a security exception in file.exists()
				result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "SECURITY_EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( IOException e ) {
				e.printStackTrace();
				result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE IO EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( SecurityException e ) {
				e.printStackTrace();
				result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE WRITE SECURITY EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		setPPStatus( info, status, ergListe );
	}

	/**
	 * Verabreitet einen Wert weiter, wenn dieser mit einem $ begonnen hat. In
	 * dem Fall muss es sich um den Namen einer Pr�fstandsvariablen handeln,
	 * deren Wert zur�ckgegeben wird. Falls nach dem f�hrenden $ ein weiteres 
	 * folgt, dann diente das erste nur zur Maskierung, so dass der um das erste 
	 * $ bereinigte Wert �bergeben wird.
	 * 
	 * @param value Wert.
	 * @return Weiterverarbeiteter R�ckgabewert. Bei <code>null</code> wurde 
	 * 		   die spezifizierte Pr�fstandsvariable nicht gefunden.
	 */
	private String processPrefix( String value ) {
		String VAR_INDICATOR = "$";

		// �berpr�fe die G�ltigkeit des �bergebenen Parameters.
		if( value == null )
			return null;

		// Beginnen wir mit einem $?
		if( value.startsWith( VAR_INDICATOR ) ) {
			value = value.substring( 1 ); //erstes $ abschneiden

			// Handelt es sich um ein $ welches mit einem f�hrenden $ maskiert 
			// wurde?
			if( !value.startsWith( VAR_INDICATOR ) ) //Kein weiteres $ an Anfang enthalten?
				try {
					value = (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, value ); //beziehe Inhalt der Pr�fstandsvariablen		
				} catch( Exception x ) {
					value = null;
				} //nein, dann Wert aus der Pr�fstandsvariablen beziehen
		}

		return value;
	}

	/**
	 *  Die Klasse CheckFileExistsWriteable erm�glicht ein threadbasiertes �berpr�fen auf Dateiexistenz und Schreibrechten.
	 * 
	 * @author Sch�nert Fabian, TI-545 BMW AG
	 *
	 */
	class CheckFileExistsWriteable implements Callable {

		private File file;

		public CheckFileExistsWriteable( File file ) {
			this.file = file;
		}

		@Override
		public Boolean call() throws Exception {
			return (file.exists() && file.canRead() && file.canWrite());
		}
	}

}

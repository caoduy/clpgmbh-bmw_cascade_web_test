package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import java.lang.Math;
import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import javax.swing.border.*;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.*;

/** SDiagEPSinit Pruefprozedur<BR>
 * <BR>
 * Created on 23.02.2007<BR>
 * <BR>
 * EPS initizialization process implementation.<BR>
 * The current angle is shown in a graphic interface for better understanding.<BR>
 * The moment value is evaluated within the specified areas while there's no timeout.<BR>
 * <BR>
 * @author PJ Pedro Jorge (IndustrieHansa GmbH), Ti-432, BMW AG<BR>
 * @author UP Ulf Plath, TI-544, BMW AG<BR>
 *
 * @version  1_0_F 29.01.2007 PJ AFS initialization process with graphical user interface<BR>
 * @version  2_0_F 12.02.2007 PJ Small corrections<BR>
 * @version  3_0_F 26.02.2007 PJ Improved the visibility of the "turn left/right" messages.<BR>
 * 								 Added optional parameter FREEZE_TIME.<BR>
 * @version  4_0_T 15.06.2007 PJ Added max angle and max moment limits to the test.
 * 								 Can only execute one EDIABAS job/cycle. Values to read were priorized 
 * 								 accordingly to the importance and the current test.
 * 								 Added the possibility to monitor the angular velocity.
 * 								 Added the possibility to change the message texts.
 * 								 Default polling time set to 0 ms. 
 * @version  5_0_T 24.07.2007 PJ Added SGBD related parameters for each diagnose value to read out.
 * 								 When the SGBD and the SGBD_JOB arguments are equal the results will be retrieved in only one job call.  
 * 								 Parameter names changed.
 * 								 Added average velocity monitoring only when VELOCITY_MAX and no VELOCITY sgbd 
 * 								 parameters are given. 
 * @version  6_0_T 11.09.2006 PJ Final test version.  
 * @version  7_0_F 14.09.2006 PJ Changed to an F version.
 * @version  8_0_T 04.02.2009 UP Corrected bug where the arrows for 'right' were pointing to the left, testing version for plant Munich
 * @version  9_0_T 11.03.2009 UP Introduced EdiabasProxyThread instead of calling Ediabas directly for every job
 * @version 10_0_F 26.03.2009 UP Final version of v9_0_T
 * @version 11_0_F 14.07.2009 UP Changed handling of an ApiCallFailedException that led to an ArrayIndex-Exception
 * @version 12_0_T 03.09.2009 UP Added additional output at EDIABAS calls to localize an EDIABAS result error
 * @version 13_0_F 16.09.2009 UP Final version of version 12_0_T
 * @version 14_0_F 22.09.2009 UP Small bugfix of version 13_0_F (wrong source code has been imported to Cascade)
 * @version 15_0_F 27.01.2010 UP Changed behaviour at end of cycle. Angle and moment have to remain stable inside tolerances
 *                               before the calibration job starts.
 * @version 16_0_F 14.10.2010 UP Deactivation of automatic abort in case the steering velocity exceeds threshold value for steering velocity
 * @version 17_0_F 16.10.2012 UP The PP now also checks specific initialization bits in the ECU to control the process
 * @version 18_0_F 20.08.2015 UP Roll-back to contents of version 16 (exact copy) to avoid version number confusion (LOP No. 1967)
 * 
 */
public class SDiagEPSinit_18_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	// application states
	private static final int MIDDLE2LEFT = 0;
	private static final int LEFT2RIGHT = 1;
	private static final int RIGHT2MIDDLE = 2;
	private static final int END = 3;
	private static final int TEST_MOMENT = 0;
	private static final int TEST_ANGLE = 1;
	// diagnose values
	private static final int MOMENT = 0;
	private static final int ANGLE = 1;
	private static final int VELOCITY = 2;
	// area positions
	private static final int OTHER = -1;
	private static final int MIDDLE = 0;
	private static final int LEFT = 1;
	private static final int RIGHT = 2;
	// angle/moment grafic consts
	private static final int AXIS_WIDTH = 1; //px (graph axis width)
	private static final int BAR_WIDTH = 40; //px (graph bar width)
	private static final Color COLOR_BORDER = Color.ORANGE; // borders
	private static final Color COLOR_BACKGROUND = Color.BLACK; // background
	private static final Color COLOR_AREA = Color.BLUE; // area
	private static final Color COLOR_BAR_DEFAULT = Color.ORANGE; // angle bar default
	private static final Color COLOR_BAR_ACTIVE = Color.GREEN; // angle bar inside area

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public SDiagEPSinit_18_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public SDiagEPSinit_18_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialisiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 * @return Stringvektor der ben�tigten Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "MOMENT_SGBD", "MOMENT_SGBD_JOB", "MOMENT_SGBD_RESULT", "MOMENT_REF", "MOMENT_MIN", "MOMENT_MAX", "ANGLE_SGBD", "ANGLE_SGBD_JOB", "ANGLE_SGBD_RESULT", "ANGLE_REF", "ANGLE_MIN", "ANGLE_MAX", "TIMEOUT" };
		return args;
	}

	/**
	 * liefert die optionalen Argumente
	 * @return Stringvektor der optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "MOMENT_SGBD_ARGUMENTS", // DEFAULT = "" 
		"MOMENT_RANGE", // DEFAULT = 2*MOMENT_MAX
		"ANGLE_SGBD_ARGUMENTS", // DEFAULT = "" 
		"ANGLE_RANGE", // DEFAULT = 2*ANGLE_MAX
		"VELOCITY_SGBD", // DEAFULT = "";
		"VELOCITY_SGBD_JOB", // DEAFULT = "";
		"VELOCITY_SGBD_ARGUMENTS", // DEAFULT = "";
		"VELOCITY_SGBD_RESULT", // DEAFULT = "";
		"VELOCITY_MAX", // DEFAULT = 0 �/s (not monitored)
		"TEXT_MSG_0", // DEFAULT = original texts 
		"TEXT_MSG_1", // DEFAULT = original texts 
		"TEXT_MSG_2", // DEFAULT = original texts 
		"TEXT_MSG_3", // DEFAULT = original texts 
		"TEXT_MSG_4", // DEFAULT = original texts 
		"TEXT_MSG_5", // DEFAULT = original texts 
		"TEXT_SIZE", // DEFAULT = 70 
		"POLLING_TIME", // DEFAULT = 0 ms
		"FREEZE_TIME", // DEFAULT = 1000 ms
		"END_TIME_ERROR", // DEFAULT = 3000 ms
		"END_TIME_OK", // DEFAULT = 1000 ms
		"DEBUG", // DEFAULT = FALSE 
		"SIMULATION" // DEFAULT = FALSE 
		};
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 * @return true, wenn die Ueberpruefung der zwingenden Argumente ok ist
	 */
	public boolean checkArgs() {
		try {
			//if (!super.checkArgs())
			//   throw new Exception("super.check args failed!");

			//---------------------- req: MOMENT_SGBD, MOMENT_SGBD_JOB, MOMENT_SGBD_RESULT, MOMENT_REF, MOMENT_MIN, MOMENT_MAX, 
			//							  ANGLE_SGBD, ANGLE_SGBD_JOB, ANGLE_SGBD_RESULT, ANGLE_REF, ANGLE_MIN, ANGLE_MAX,
			//							  TIMEOUT	
			// TIMEOUT > 0
			if( new Long( extractArg( "TIMEOUT" ) ).longValue() <= 0L )
				throw new Exception( "TIMEOUT out of range! (>0): " + extractArg( "TIMEOUT" ) );
			// MOMENT_REF > 0 ?
			if( new Float( extractArg( "MOMENT_REF" ) ).floatValue() <= 0f )
				throw new Exception( "MOMENT_REF out of range! (>0): " + extractArg( "MOMENT_REF" ) );
			// MOMENT_MIN > 0 ?
			if( new Float( extractArg( "MOMENT_MIN" ) ).floatValue() <= 0f )
				throw new Exception( "MOMENT_MIN out of range! (>0): " + extractArg( "MOMENT_MIN" ) );
			// MOMENT_MAX > 0 ?
			if( new Float( extractArg( "MOMENT_MAX" ) ).floatValue() <= 0f )
				throw new Exception( "MOMENT_MAX out of range! (>0): " + extractArg( "MOMENT_MAX" ) );
			// MOMENT_MAX > MOMENT_MIN ?
			if( new Float( extractArg( "MOMENT_MAX" ) ).floatValue() <= new Float( extractArg( "MOMENT_MIN" ) ).floatValue() )
				throw new Exception( "MOMENT_MAX <= MOMENT_MIN!: " + extractArg( "MOMENT_MAX" ) + " < " + extractArg( "MOMENT_MIN" ) );
			// MOMENT_RANGE/2 >= MOMENT_MAX ?
			if( extractArg( "MOMENT_RANGE" ) != null ) {
				if( new Float( extractArg( "MOMENT_RANGE" ) ).floatValue() / 2 < new Float( extractArg( "MOMENT_MAX" ) ).floatValue() )
					throw new Exception( "MOMENT_RANGE/2 < MOMENT_MAX!: " + extractArg( "MOMENT_RANGE" ) + "/2 < " + extractArg( "MOMENT_MAX" ) );
			}
			// ANGLE_REF > 0 ?
			if( new Float( extractArg( "ANGLE_REF" ) ).floatValue() <= 0f )
				throw new Exception( "ANGLE_REF out of range! (>0): " + extractArg( "ANGLE_REF" ) );
			// ANGLE_MIN > 0 ?
			if( new Float( extractArg( "ANGLE_MIN" ) ).floatValue() <= 0f )
				throw new Exception( "ANGLE_MIN out of range! (>0): " + extractArg( "ANGLE_MIN" ) );
			// ANGLE_MAX > 0 ?
			if( new Float( extractArg( "ANGLE_MAX" ) ).floatValue() <= 0f )
				throw new Exception( "ANGLE_MAX out of range! (>0): " + extractArg( "ANGLE_MAX" ) );
			// ANGLE_MAX > ANGLE_MIN ?
			if( new Float( extractArg( "ANGLE_MAX" ) ).floatValue() <= new Float( extractArg( "ANGLE_MIN" ) ).floatValue() )
				throw new Exception( "ANGLE_MAX <= ANGLE_MIN!: " + extractArg( "ANGLE_MAX" ) + " < " + extractArg( "ANGLE_MIN" ) );
			// ANGLE_RANGE/2 >= ANGLE_MAX ?
			if( extractArg( "ANGLE_RANGE" ) != null ) {
				if( new Float( extractArg( "ANGLE_RANGE" ) ).floatValue() / 2 < new Float( extractArg( "ANGLE_MAX" ) ).floatValue() )
					throw new Exception( "ANGLE_RANGE/2 < ANGLE_MAX!: " + extractArg( "ANGLE_RANGE" ) + "/2 < " + extractArg( "ANGLE_MAX" ) );
			}

			//---------------------- opt: MOMENT_RANGE, MOMENT_SGBD_ARGUMENTS,
			//							  ANGLE_RANGE, ANGLE_SGBD_ARGUMENTS, 
			//							  VELOCITY_SGBD, VELOCITY_SGBD_JOB, VELOCITY_SGBD_ARGUMENTS, VELOCITY_SGBD_RESULT, VELOCITY_MAX,
			//							  TEXT_SIZE, POLLING_TIME, END_TIME, FREEZE_TIME
			// MOMENT_RANGE > 0 ?
			if( extractArg( "MOMENT_RANGE" ) != null && new Float( extractArg( "MOMENT_RANGE" ) ).floatValue() <= 0f )
				throw new Exception( "MOMENT_RANGE out of range! (>0): " + extractArg( "MOMENT_RANGE" ) );
			// ANGLE_RANGE > 0 ?
			if( extractArg( "ANGLE_RANGE" ) != null && new Float( extractArg( "ANGLE_RANGE" ) ).floatValue() <= 0f )
				throw new Exception( "ANGLE_RANGE out of range! (>0): " + extractArg( "ANGLE_RANGE" ) );
			// VELOCITY_MAX > 0 ?
			if( extractArg( "VELOCITY_MAX" ) != null && new Integer( extractArg( "VELOCITY_MAX" ) ).intValue() <= 0 )
				throw new Exception( "VELOCITY_MAX out of range! (>0): " + extractArg( "VELOCITY_MAX" ) );
			// VELOCITY_SGBD, VELOCITY_SGBD_JOB, VELOCITY_SGBD_RESULT && VELOCITY_MAX defined ?
			if( extractArg( "VELOCITY_SGBD" ) != null && extractArg( "VELOCITY_SGBD_JOB" ) != null && extractArg( "VELOCITY_SGBD_RESULT" ) != null && extractArg( "VELOCITY_MAX" ) == null )
				throw new Exception( "VELOCITY_MAX must be present if VELOCITY diagnose jobs are given! (null)" );
			// TEXT_SIZE > 0
			if( extractArg( "TEXT_SIZE" ) != null && new Integer( extractArg( "TEXT_SIZE" ) ).intValue() <= 0 )
				throw new Exception( "TEXT_SIZE out of range! (>0): " + extractArg( "TEXT_SIZE" ) );
			// POLLING_TIME > 0
			if( extractArg( "POLLING_TIME" ) != null && new Long( extractArg( "POLLING_TIME" ) ).longValue() <= 0L )
				throw new Exception( "POLLING_TIME out of range! (>0): " + extractArg( "POLLING_TIME" ) );
			// FREEZE_TIME > 0
			if( extractArg( "FREEZE_TIME" ) != null && new Long( extractArg( "FREEZE_TIME" ) ).longValue() <= 0L )
				throw new Exception( "FREEZE_TIME out of range! (>0): " + extractArg( "FREEZE_TIME" ) );
			// END_TIME_OK > 0
			if( extractArg( "END_TIME_OK" ) != null && new Long( extractArg( "END_TIME_OK" ) ).longValue() <= 0L )
				throw new Exception( "END_TIME_OK out of range! (>0): " + extractArg( "END_TIME_OK" ) );
			// END_TIME_ERROR > 0
			if( extractArg( "END_TIME_ERROR" ) != null && new Long( extractArg( "END_TIME_ERROR" ) ).longValue() <= 0L )
				throw new Exception( "END_TIME_ERROR out of range! (>0): " + extractArg( "END_TIME_ERROR" ) );

			return true;

		} catch( Exception e ) {
			log( true, "Error", "checking args, Exception: " + e.getMessage() );
			return false;
		} catch( Throwable e ) {
			log( true, "Error", "checking args, Throwable: " + e.getMessage() );
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		boolean debug = ((getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ))) ? true : false;
		boolean simulation = ((getArg( "SIMULATION" ) != null) && (getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" ))) ? true : false;
		// swings
		JFrame frame = null;
		Font stdFont = null;
		JPanel topPanel = null; // TOP panel
		JTextArea msgArea = null;
		JPanel middlePanel = null; // MIDDLE panel
		JLabel imgLabel = null;
		JPanel bottomPanel = null; // BOTTOM panel
		JPanel anglePanel = null;
		JLabel angleLabel = null;
		JPanel testPanel = null;
		JLabel codeLabel = null;
		JSlider angleSlider = null;
		JSlider momentSlider = null;
		JPanel momentPanel = null;
		JLabel momentLabel = null;

		// aux
		int status = STATUS_EXECUTION_OK;
		int diagAction = 0;
		int appState = MIDDLE2LEFT, appTest = TEST_MOMENT;
		int stableState = 4;
		int middleAngCnt = 0;
		int middleMomCnt = 0;
		int token = 0;
		Vector resList = new Vector();
		BufferedImage img = null;
		String diagStatus = "";
		long actualTime = System.currentTimeMillis(), startTime = actualTime;
		long angleOldTime = actualTime;
		float tmp = 0.0f;
		float angleOld = 0.0f;
		boolean start = true;

		// EDIABAS-Device holen
		EdiabasProxyThread ediabas = null;
		try {
			ediabas = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();
		} catch( Exception e ) {
			log( debug, "Error", "Fehler beim Holen des EDIABAS-Device. " + e.getMessage() );
		}

		int[] resToRead = { -1, -1, -1 };
		int lang = System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) ? 1 : 0;
		String[] sgbd = { "", "", "" };
		String[] sgbdJob = { "", "", "" };
		String[] sgbdArgs = { "", "", "" };
		String[] sgbdRes = { "", "", "" };
		float[] range = { 0.0f, 0.0f, 0.0f };
		float[] ref = { 0.0f, 0.0f, 0.0f };
		float[] min = { 0.0f, 0.0f, 0.0f };
		float[] max = { 0.0f, 0.0f, 0.0f };
		float[] value = { 0.0f, 0.0f, 0.0f };
		String[][] msg = {
				/*0*/{ "Turn slowly complete to...\n           <<<  LEFT  <<<", "Anschlaglenken nach...\n              <<<  LINKS  <<<" }, //(pos 0 -> MIDDLE2LEFT)
				/*1*/{ "Turn slowly complete to...\n          >>>  RIGHT  >>>", "Anschlaglenken nach...\n             >>>  RECHTS  >>>" }, //(pos 0 -> MIDDLE2LEFT)
				/*2*/{ "Back to the middle\n release steering wheel", "Zur�ck zur Mitte\n und Lenkrad loslassen" }, //(pos 2 -> RIGHT2MIDDLE)
				/*3*/{ "EPS initialization succeeded!\nPlease align the steering wheel", "EPS-Initialisierung erfolgt!\nBitte Lenkrad geradestellen" },
				/*4*/{ "EPS initialization time\n    expired!", "   EPS-Initialisierungszeit ist\n  abgelaufen!" },
				/*5*/{ "Steering velocity too high!\nPlease turn more slowly!", "Lenkgeschwindigkeit zu hoch\nBitte langsamer lenken!" }, };

		try {
			// --------------------------------------------------------------------------------------------------
			// check/extract args
			// --------------------------------------------------------------------------------------------------
			log( debug, "Info", "This is PP " + this.getPPName() + " version " + this.getVersion() );
			if( !checkArgs() ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resList.add( new Ergebnis( "StatusEpsInit", "SDiagEPSinit", "", "", "", "EpsInitNOk", "NOk", "Ok", "Ok", "0", "", "", "", PB.getString( "parameterexistenz" ), "", Ergebnis.FT_NIO ) );
				log( debug, "Error", "check args failed." );
				throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			}
			log( debug, "Ok", "check args ok." );

			// --------------------------------------------------------------------------------------------------
			// CASCADE parameters
			// --------------------------------------------------------------------------------------------------
			sgbd[MOMENT] = extractArg( "MOMENT_SGBD" );
			sgbdJob[MOMENT] = extractArg( "MOMENT_SGBD_JOB" );
			sgbdRes[MOMENT] = extractArg( "MOMENT_SGBD_RESULT" );
			ref[MOMENT] = new Float( extractArg( "MOMENT_REF" ) ).floatValue();
			min[MOMENT] = new Float( extractArg( "MOMENT_MIN" ) ).floatValue();
			max[MOMENT] = new Float( extractArg( "MOMENT_MAX" ) ).floatValue();
			sgbd[ANGLE] = extractArg( "ANGLE_SGBD" );
			sgbdJob[ANGLE] = extractArg( "ANGLE_SGBD_JOB" );
			sgbdRes[ANGLE] = extractArg( "ANGLE_SGBD_RESULT" );
			ref[ANGLE] = new Float( extractArg( "ANGLE_REF" ) ).floatValue();
			min[ANGLE] = new Float( extractArg( "ANGLE_MIN" ) ).floatValue();
			max[ANGLE] = new Float( extractArg( "ANGLE_MAX" ) ).floatValue();
			long timeoutTime = new Long( extractArg( "TIMEOUT" ) ).longValue();
			range[MOMENT] = (extractArg( "MOMENT_RANGE" ) != null) ? new Float( extractArg( "MOMENT_RANGE" ) ).floatValue() : 2 * max[MOMENT];
			sgbdArgs[MOMENT] = (extractArg( "MOMENT_SGBD_ARGUMENTS" ) != null) ? extractArg( "MOMENT_SGBD_ARGUMENTS" ) : "";
			range[ANGLE] = (extractArg( "ANGLE_RANGE" ) != null) ? new Float( extractArg( "ANGLE_RANGE" ) ).floatValue() : 2 * max[ANGLE];
			sgbdArgs[ANGLE] = (extractArg( "ANGLE_SGBD_ARGUMENTS" ) != null) ? extractArg( "ANGLE_SGBD_ARGUMENTS" ) : "";
			sgbd[VELOCITY] = (extractArg( "VELOCITY_SGBD" ) != null) ? extractArg( "VELOCITY_SGBD" ) : "";
			sgbdJob[VELOCITY] = (extractArg( "VELOCITY_SGBD_JOB" ) != null) ? extractArg( "VELOCITY_SGBD_JOB" ) : "";
			sgbdArgs[VELOCITY] = (extractArg( "VELOCITY_SGBD_ARGUMENTS" ) != null) ? extractArg( "VELOCITY_SGBD_ARGUMENTS" ) : "";
			sgbdRes[VELOCITY] = (extractArg( "VELOCITY_SGBD_RESULT" ) != null) ? extractArg( "VELOCITY_SGBD_RESULT" ) : "";
			max[VELOCITY] = (extractArg( "VELOCITY_MAX" ) != null) ? new Float( extractArg( "VELOCITY_MAX" ) ).floatValue() : 0; // �/s
			long pollingTime = (extractArg( "POLLING_TIME" ) != null) ? new Long( extractArg( "POLLING_TIME" ) ).longValue() : 0; //ms
			long freezeTime = (extractArg( "FREEZE_TIME" ) != null) ? new Long( extractArg( "FREEZE_TIME" ) ).longValue() : 1000; //ms
			long endTimeOk = (extractArg( "END_TIME_OK" ) != null) ? new Long( extractArg( "END_TIME_OK" ) ).longValue() : 1000; //ms
			long endTimeErr = (extractArg( "END_TIME_ERROR" ) != null) ? new Long( extractArg( "END_TIME_ERROR" ) ).longValue() : 3000; //ms
			int textSize = (extractArg( "TEXT_SIZE" ) != null) ? new Integer( extractArg( "TEXT_SIZE" ) ).intValue() : 70;

			for( int i = 0; i < msg.length; i++ ) {
				if( extractArg( "TEXT_MSG_" + String.valueOf( i ) ) != null && !extractArg( "TEXT_MSG_" + String.valueOf( i ) ).equalsIgnoreCase( "" ) ) {
					msg[i][lang] = extractArg( "TEXT_MSG_" + String.valueOf( i ) );
				}
			}

			// --------------------------------------------------------------------------------------------------
			// swings init
			// --------------------------------------------------------------------------------------------------
			Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
			stdFont = new Font( null, Font.BOLD, textSize );
			frame = new JFrame( lang == 1 ? "EPS-Initialisierung" : "EPS Initialization" );
			frame.setExtendedState( JFrame.MAXIMIZED_BOTH );
			frame.setSize( screenDim );
			frame.setLocation( screenDim.width / 2 - frame.getWidth() / 2, screenDim.height / 2 - frame.getHeight() / 2 );
			frame.setDefaultCloseOperation( WindowConstants.DO_NOTHING_ON_CLOSE );
			// ----------------------------------------------------------- TOP panel
			msgArea = new JTextArea( 2, 1 );
			msgArea.setFont( stdFont );
			msgArea.setText( msg[MIDDLE2LEFT][lang] ); // init in MIDDLE2LEFT
			msgArea.setEditable( false );
			msgArea.setAlignmentX( Container.CENTER_ALIGNMENT );
			msgArea.setAlignmentY( Container.CENTER_ALIGNMENT );
			msgArea.setLineWrap( true );
			msgArea.setOpaque( false );
			topPanel = new JPanel( new BorderLayout() );
			topPanel.setPreferredSize( new Dimension( screenDim.width, screenDim.height / 4 ) );
			topPanel.setBorder( BorderFactory.createEtchedBorder( EtchedBorder.RAISED ) );
			topPanel.setBackground( Color.LIGHT_GRAY );
			topPanel.setOpaque( true );
			topPanel.add( msgArea );
			frame.getContentPane().add( topPanel, BorderLayout.PAGE_START );
			// ----------------------------------------------------------- MIDDLE panel
			img = new BufferedImage( screenDim.width, screenDim.height / 2, BufferedImage.TYPE_INT_RGB );
			img = initImage( img, convDeg2Pix( img, range[ANGLE], ref[ANGLE] ), convDeg2Pix( img, range[ANGLE], min[ANGLE] ), convDeg2Pix( img, range[ANGLE], max[ANGLE] ) );
			imgLabel = new JLabel( new ImageIcon( img ), SwingConstants.CENTER );
			imgLabel.setBorder( BorderFactory.createLineBorder( COLOR_BORDER ) );
			middlePanel = new JPanel( new BorderLayout() );
			middlePanel.setPreferredSize( new Dimension( screenDim.width, screenDim.height / 2 ) );
			middlePanel.add( imgLabel );
			frame.getContentPane().add( middlePanel, BorderLayout.CENTER );
			// ----------------------------------------------------------- BOTTOM panel			
			angleLabel = new JLabel( "0�", SwingConstants.CENTER );
			angleLabel.setFont( stdFont );
			angleLabel.setOpaque( false );
			anglePanel = new JPanel( new BorderLayout() );
			anglePanel.setPreferredSize( new Dimension( screenDim.width / 3, screenDim.height / 4 ) );
			anglePanel.setBorder( BorderFactory.createEtchedBorder() );
			anglePanel.setOpaque( false );
			anglePanel.add( angleLabel );
			// 
			codeLabel = new JLabel( getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7(), SwingConstants.CENTER );
			codeLabel.setFont( stdFont );
			codeLabel.setPreferredSize( new Dimension( screenDim.width / 3, screenDim.height / 8 ) );
			codeLabel.setOpaque( false );
			angleSlider = new JSlider( SwingConstants.HORIZONTAL, -1 * Math.round( range[ANGLE] / 2 ), Math.round( range[ANGLE] / 2 ), 0 );
			angleSlider.setPreferredSize( new Dimension( screenDim.width / 6, screenDim.height / 32 ) );
			angleSlider.setPaintTicks( true );
			angleSlider.setMajorTickSpacing( 100 );
			angleSlider.setVisible( simulation );
			angleSlider.setEnabled( simulation );
			angleSlider.setOpaque( false );
			momentSlider = new JSlider( SwingConstants.HORIZONTAL, -1 * Math.round( range[MOMENT] / 2 ), Math.round( range[MOMENT] / 2 ), 0 );
			momentSlider.setPreferredSize( new Dimension( screenDim.width / 6, screenDim.height / 32 ) );
			momentSlider.setVisible( simulation );
			momentSlider.setEnabled( simulation );
			momentSlider.setOpaque( false );
			testPanel = new JPanel( new FlowLayout( FlowLayout.CENTER ) );
			testPanel.setPreferredSize( new Dimension( screenDim.width / 3, screenDim.height / 4 ) );
			testPanel.setBorder( BorderFactory.createEtchedBorder() );
			testPanel.setOpaque( false );
			testPanel.add( codeLabel );
			testPanel.add( angleSlider );
			testPanel.add( momentSlider );
			// 
			momentLabel = new JLabel( "0Nm", SwingConstants.CENTER );
			momentLabel.setFont( stdFont );
			momentLabel.setOpaque( false );
			momentPanel = new JPanel( new BorderLayout() );
			momentPanel.setPreferredSize( new Dimension( screenDim.width / 3, screenDim.height / 4 ) );
			momentPanel.setBorder( BorderFactory.createEtchedBorder() );
			momentPanel.setOpaque( false );
			momentPanel.add( momentLabel );
			//
			bottomPanel = new JPanel();
			bottomPanel.setLayout( new BoxLayout( bottomPanel, BoxLayout.X_AXIS ) );
			bottomPanel.setPreferredSize( new Dimension( screenDim.width, screenDim.height / 4 ) );
			bottomPanel.setOpaque( true );
			bottomPanel.setBackground( Color.LIGHT_GRAY );
			bottomPanel.add( anglePanel );
			bottomPanel.add( testPanel );
			bottomPanel.add( momentPanel );
			frame.getContentPane().add( bottomPanel, BorderLayout.PAGE_END );
			// -----------------------------------------------------------
			frame.show();
			log( debug, "Ok", "GUI created and shown." );

			// --------------------------------------------------------------------------------------------------
			// EPS initialization process cycle begin
			// --------------------------------------------------------------------------------------------------
			while( true ) {
				actualTime = System.currentTimeMillis();
				//				try { // Sleep for testing
				//					Thread.sleep(200);
				//				} catch(InterruptedException e) {
				//					e.printStackTrace();
				//				}

				// ---------------------
				// read angle, moment (& velocity)
				// ---------------------
				if( simulation ) {
					//
					// read simulation slider 
					//
					angleOld = value[ANGLE];
					value[ANGLE] = (float) -1 * angleSlider.getValue();
					value[MOMENT] = (float) -1 * momentSlider.getValue();
					value[VELOCITY] = 1000 * (value[ANGLE] - angleOld) / (actualTime - angleOldTime); // �/s
					angleOldTime = actualTime;

				} else {
					//
					// attribute the ediabas token (0..5)
					//
					switch( token ) {
						case 2:
							diagAction = (appTest == TEST_MOMENT) ? ANGLE : (!sgbd[VELOCITY].equalsIgnoreCase( "" ) ? VELOCITY : ANGLE);
							token++;
							break;
						case 5:
							diagAction = (!sgbd[VELOCITY].equalsIgnoreCase( "" ) ? VELOCITY : ANGLE);
							token = 0;
							break;
						default:
							diagAction = (appTest == TEST_MOMENT) ? MOMENT : ANGLE;
							token++;
							break;
					}

					// execute (the specified) diagnose job
					int resRead = -1;
					try {
						//log(debug, "Info", "Trying to execute EDIABAS job with SGBD " + sgbd[diagAction] + " and job " + sgbdJob[diagAction] + " and arguments " + sgbdArgs[diagAction] + ".");
						diagStatus = ediabas.executeDiagJob( sgbd[diagAction], sgbdJob[diagAction], sgbdArgs[diagAction], "" );
						if( diagStatus.equals( "OKAY" ) == false ) {
							// NIO
							status = STATUS_EXECUTION_ERROR;
							resList.add( new Ergebnis( "DiagError", "EDIABAS", sgbd[diagAction], sgbdJob[diagAction], sgbdArgs[diagAction], "", diagStatus, "OKAY", "OKAY", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO ) );
							log( debug, "Error", "executing EDIABAS, sgbd: " + sgbd[diagAction] + ", sgbdJob: " + sgbdJob[diagAction] + ", sgbdArgs: " + sgbdArgs[diagAction] + ", diagStatus: " + diagStatus );
							throw new PPExecutionException( PB.getString( "diagnosefehler" ) );
						}
						// reads all the possible EDIABAS results with the given SGBD and JOB. 
						resToRead = diagResToRead( sgbd, sgbdJob, sgbdArgs, diagAction );
						for( int i = 0; i < resToRead.length; i++ ) {
							if( resToRead[i] != -1 ) {
								resRead = resToRead[i];
								tmp = value[resRead];
								//log(debug, "Info", "Trying to read EDIABAS result "+ sgbdRes[resRead] + ".");
								value[resRead] = Float.parseFloat( ediabas.getDiagResultValue( sgbdRes[resRead] ) );
								// update specific things
								if( resRead == ANGLE ) {
									angleOld = tmp;
									if( start ) {
										// if we don't start with the angle in the middle
										angleOld = value[ANGLE];
										start = false;
									}
									// compute average velocity (�/s)
									if( sgbd[VELOCITY].equalsIgnoreCase( "" ) ) {
										value[VELOCITY] = 1000 * (value[ANGLE] - angleOld) / (actualTime - angleOldTime); // �/s
									}
									angleOldTime = actualTime;
								}
							}
						}

					} catch( ApiCallFailedException e ) {
						// NIO
						status = STATUS_EXECUTION_ERROR;
						resList.add( new Ergebnis( "DiagError", "EDIABAS", sgbd[diagAction], sgbdJob[diagAction], sgbdArgs[diagAction], "", diagStatus, "OKAY", "OKAY", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO ) );
						log( debug, "Error", "executing EDIABAS, sgbd: " + sgbd[diagAction] + ", sgbdJob: " + sgbdJob[diagAction] + ", sgbdArgs: " + sgbdArgs[diagAction] + ", diagStatus: " + diagStatus );
						e.printStackTrace();
						throw new PPExecutionException( PB.getString( "diagnosefehler" ) );
					} catch( EdiabasResultNotFoundException e ) {
						// NIO
						status = STATUS_EXECUTION_ERROR;
						resList.add( new Ergebnis( "DiagError", "EDIABAS", sgbd[resRead], sgbdJob[resRead], sgbdArgs[resRead], sgbdRes[resRead], diagStatus, "", "", "0", "", "", "", PB.getString( "diagnosefehler" ) + "->" + (e.getMessage() != null ? e.getMessage() : ""), "", Ergebnis.FT_NIO_SYS ) );
						log( debug, "Error", "executing EDIABAS, sgbd: " + sgbd[resRead] + ", sgbdJob: " + sgbdJob[resRead] + ", sgbdArgs: " + sgbdArgs[resRead] + ", diagStatus: " + diagStatus + ", sgbdRes: " + sgbdRes[resRead] + ", EdiabasResultNotFoundException: " + e.getMessage() );
						e.printStackTrace();
						throw new PPExecutionException( PB.getString( "diagnosefehler" ) );
					} catch( Exception e ) {
						// NIO
						status = STATUS_EXECUTION_ERROR;
						resList.add( new Ergebnis( "Error", "EDIABAS", sgbd[diagAction], sgbdJob[diagAction], sgbdArgs[diagAction], "", diagStatus, "OKAY", "OKAY", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO ) );
						log( debug, "Error", "General exception occurred while executing EDIABAS, sgbd: " + sgbd[diagAction] + ", sgbdJob: " + sgbdJob[diagAction] + ", sgbdArgs: " + sgbdArgs[diagAction] + ", diagStatus: " + diagStatus );
						e.printStackTrace();
						throw new PPExecutionException( PB.getString( "diagnosefehler" ) );

					}
				}

				// ---------------------
				// state tasks
				// ---------------------

				// ------------
				// END	
				// ------------
				if( appState == END ) {
					// check end time
					if( actualTime - startTime > (status == STATUS_EXECUTION_OK ? endTimeOk : endTimeErr) ) {
						break;
					}

				} else {
					// Timeout check
					if( actualTime - startTime > timeoutTime ) {
						// NIO
						status = STATUS_EXECUTION_ERROR;
						resList.add( new Ergebnis( "StatusEpsInit", "SDiagEPSinit", "", "", "", "EpsInitIncomplete", String.valueOf( actualTime - startTime ) + " ms", "0 ms", String.valueOf( timeoutTime ) + " ms", "0", "", "", "", "Timeout", "", Ergebnis.FT_NIO ) );
						log( debug, "Error", "EPS initialization incomplete. -> timeout (" + String.valueOf( actualTime - startTime ) + " ms)" );
						// ... -> END
						startTime = actualTime;
						// state update (exit)
						appState = END;
						// gui update (exit)
						msgArea.setText( msg[4][lang] );
						topPanel.setBackground( Color.RED );
						anglePanel.setOpaque( false );
						momentPanel.setOpaque( false );
						bottomPanel.setBackground( Color.RED );

						// Velocity check
					} else if( max[VELOCITY] != 0 && (value[VELOCITY] < -max[VELOCITY] || value[VELOCITY] > max[VELOCITY]) ) {
						// NIO
						//						status = STATUS_EXECUTION_ERROR;
						//			        	resList.add( new Ergebnis("StatusEpsInit", "SDiagEPSinit", sgbd[VELOCITY], sgbdJob[VELOCITY], sgbdArgs[VELOCITY], "EpsInitIncomplete", String.valueOf(value[VELOCITY]) +" �/s", String.valueOf(-1*max[VELOCITY]) +" �/s", String.valueOf(max[VELOCITY]) +" �/s", "0", "", "", "", "SteeringVelocity", "", Ergebnis.FT_NIO) );
						//						log(debug, "Error", "EPS initialization incomplete. -> velocity ("+ String.valueOf(value[VELOCITY]) +" �/s)");
						//						// ... -> END
						//						startTime = actualTime;									
						//						// state update (exit)
						//						appState = END;
						//						// gui update (exit)
						msgArea.setText( msg[5][lang] );
						topPanel.setBackground( Color.RED );
						anglePanel.setOpaque( false );
						momentPanel.setOpaque( false );
						bottomPanel.setBackground( Color.RED );
						appSleep( 2 * freezeTime );

						// ------------
						// MIDDLE2LEFT
						// ------------
					} else if( appState == MIDDLE2LEFT ) {
						// reset GUI in case steering velocity has been too high
						msgArea.setText( msg[0][lang] );
						topPanel.setBackground( Color.LIGHT_GRAY );
						anglePanel.setOpaque( false );
						momentPanel.setOpaque( false );
						bottomPanel.setBackground( Color.LIGHT_GRAY );
						// ------------
						// 1. TESTMOMENT
						// ------------						
						if( appTest == TEST_MOMENT && getArea( ref[MOMENT], min[MOMENT], max[MOMENT], value[MOMENT] ) == LEFT ) {
							// IO
							resList.add( new Ergebnis( "EPS_MOMENT_LEFT", "SDiagEPSinit", sgbd[MOMENT], sgbdJob[MOMENT], sgbdArgs[MOMENT], "EPS_MOMENT_LEFT", String.valueOf( value[MOMENT] ), String.valueOf( min[MOMENT] ), String.valueOf( max[MOMENT] ), "0", "", "", "", "", "", Ergebnis.FT_IO ) );
							log( debug, "Ok", "EPS left moment ok. moment = " + String.valueOf( value[MOMENT] ) + "Nm [" + min[MOMENT] + ", " + max[MOMENT] + "]" );
							//
							appTest = TEST_ANGLE;
							momentPanel.setOpaque( true );
							momentPanel.setBackground( Color.YELLOW );

							// ------------
							// 2. TESTANGLE
							// ------------
						} else if( appTest == TEST_ANGLE && getArea( ref[ANGLE], min[ANGLE], max[ANGLE], value[ANGLE] ) == LEFT ) {
							// IO
							resList.add( new Ergebnis( "EPS_ANGLE_LEFT", "SDiagEPSinit", sgbd[ANGLE], sgbdJob[ANGLE], sgbdArgs[ANGLE], "EPS_ANGLE_LEFT", String.valueOf( value[ANGLE] ), String.valueOf( min[ANGLE] ), String.valueOf( max[ANGLE] ), "0", "", "", "", "", "", Ergebnis.FT_IO ) );
							log( debug, "Ok", "EPS left angle ok. angle = " + String.valueOf( value[ANGLE] ) + "� [" + min[ANGLE] + ", " + max[ANGLE] + "]" );
							// ... -> LEFT2RIGHT
							// state update
							appState = LEFT2RIGHT;
							appTest = TEST_MOMENT;
							// gui update
							msgArea.setText( msg[LEFT2RIGHT][lang] );
							topPanel.setBackground( Color.YELLOW );
							anglePanel.setOpaque( false );
							momentPanel.setOpaque( false );
							bottomPanel.setBackground( Color.YELLOW );
							appSleep( freezeTime );
							topPanel.setBackground( Color.LIGHT_GRAY );
							bottomPanel.setBackground( Color.LIGHT_GRAY );
						}

						// ------------
						// LEFT2RIGHT	
						// ------------
					} else if( appState == LEFT2RIGHT ) {
						// reset GUI in case steering velocity has been too high
						msgArea.setText( msg[1][lang] );
						topPanel.setBackground( Color.LIGHT_GRAY );
						anglePanel.setOpaque( false );
						momentPanel.setOpaque( false );
						bottomPanel.setBackground( Color.LIGHT_GRAY );
						// ------------
						// 1. TESTMOMENT
						// ------------
						if( appTest == TEST_MOMENT && getArea( ref[MOMENT], min[MOMENT], max[MOMENT], value[MOMENT] ) == RIGHT ) {
							// IO
							resList.add( new Ergebnis( "EPS_MOMENT_RIGHT", "SDiagEPSinit", sgbd[MOMENT], sgbdJob[MOMENT], sgbdArgs[MOMENT], "EPS_MOMENT_RIGHT", String.valueOf( value[MOMENT] ), String.valueOf( -1 * max[MOMENT] ), String.valueOf( -1 * min[MOMENT] ), "0", "", "", "", "", "", Ergebnis.FT_IO ) );
							log( debug, "Ok", "EPS right moment ok. moment = " + String.valueOf( value[MOMENT] ) + "Nm [" + String.valueOf( -1 * max[MOMENT] ) + ", " + String.valueOf( -1 * min[MOMENT] ) + "]" );
							//
							appTest = TEST_ANGLE;
							momentPanel.setOpaque( true );
							momentPanel.setBackground( Color.YELLOW );

							// ------------
							// 2. TESTANGLE
							// ------------
						} else if( appTest == TEST_ANGLE && getArea( ref[ANGLE], min[ANGLE], max[ANGLE], value[ANGLE] ) == RIGHT ) {
							// IO
							resList.add( new Ergebnis( "EPS_ANGLE_RIGHT", "SDiagEPSinit", sgbd[ANGLE], sgbdJob[ANGLE], sgbdArgs[ANGLE], "EPS_ANGLE_RIGHT", String.valueOf( value[ANGLE] ), String.valueOf( -1 * max[ANGLE] ), String.valueOf( -1 * min[ANGLE] ), "0", "", "", "", "", "", Ergebnis.FT_IO ) );
							log( debug, "Ok", "EPS right angle ok. angle = " + String.valueOf( value[ANGLE] ) + "� [" + String.valueOf( -1 * max[ANGLE] ) + ", " + String.valueOf( -1 * min[ANGLE] ) + "]" );
							// ... -> RIGHT2MIDDLE
							// state update
							appState = RIGHT2MIDDLE;
							appTest = TEST_ANGLE;
							// gui update
							msgArea.setText( msg[RIGHT2MIDDLE][lang] );
							topPanel.setBackground( Color.YELLOW );
							anglePanel.setOpaque( false );
							momentPanel.setOpaque( false );
							bottomPanel.setBackground( Color.YELLOW );
							appSleep( freezeTime );
							topPanel.setBackground( Color.LIGHT_GRAY );
							bottomPanel.setBackground( Color.LIGHT_GRAY );
						}

						// ------------
						// RIGHT2MIDDLE	
						// ------------
					} else if( appState == RIGHT2MIDDLE ) {
						// reset GUI in case steering velocity has been too high
						msgArea.setText( msg[2][lang] );
						topPanel.setBackground( Color.LIGHT_GRAY );
						anglePanel.setOpaque( false );
						momentPanel.setOpaque( false );
						bottomPanel.setBackground( Color.LIGHT_GRAY );
						// ------------
						// 1. TESTANGLE 
						// ------------
						if( appTest == TEST_ANGLE ) {
							if( getArea( ref[ANGLE], min[ANGLE], max[ANGLE], value[ANGLE] ) == MIDDLE ) {
								// IO
								middleAngCnt++; // count times during which the angle was okay
							} else {
								middleAngCnt = 0;
								middleMomCnt = 0;
							}
							appTest = TEST_MOMENT;
							// ------------
							// 2. TESTMOMENT
							// ------------
						} else if( appTest == TEST_MOMENT ) {
							if( getArea( ref[MOMENT], min[MOMENT], max[MOMENT], value[MOMENT] ) == MIDDLE ) {
								// IO
								middleMomCnt++; // count times during which the moment was okay

								if( middleAngCnt >= stableState && middleMomCnt >= stableState ) {
									// Angle IO
									resList.add( new Ergebnis( "EPS_ANGLE_MIDDLE", "SDiagEPSinit", sgbd[ANGLE], sgbdJob[ANGLE], sgbdArgs[ANGLE], "EPS_ANGLE_MIDDLE", String.valueOf( value[ANGLE] ), String.valueOf( -1 * ref[ANGLE] ), String.valueOf( ref[ANGLE] ), "0", "", "", "", "", "", Ergebnis.FT_IO ) );
									log( debug, "Ok", "EPS middle angle ok. angle = " + String.valueOf( value[ANGLE] ) + "� [" + String.valueOf( -1 * ref[ANGLE] ) + ", " + String.valueOf( ref[ANGLE] ) + "]" );
									anglePanel.setOpaque( true );
									anglePanel.setBackground( Color.YELLOW );
									// Moment IO
									resList.add( new Ergebnis( "EPS_MOMENT_MIDDLE", "SDiagEPSinit", sgbd[MOMENT], sgbdJob[MOMENT], sgbdArgs[MOMENT], "EPS_MOMENT_MIDDLE", String.valueOf( value[MOMENT] ), String.valueOf( -1 * ref[MOMENT] ), String.valueOf( ref[MOMENT] ), "0", "", "", "", "", "", Ergebnis.FT_IO ) );
									log( debug, "Ok", "EPS middle moment ok. moment = " + String.valueOf( value[MOMENT] ) + "Nm [" + String.valueOf( -1 * ref[MOMENT] ) + ", " + String.valueOf( ref[MOMENT] ) + "]" );

									// IO
									status = STATUS_EXECUTION_OK;
									resList.add( new Ergebnis( "StatusEpsInit", "SDiagEPSinit", "", "", "", "EpsInitComplete", "Ok", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
									log( debug, "Ok", "EPS initialization complete. -> exit" );
									// ... -> END
									startTime = actualTime;
									// state update (exit)
									appState = END;
									// gui update (exit)
									msgArea.setText( msg[3][lang] );
									topPanel.setBackground( Color.GREEN );
									anglePanel.setOpaque( false );
									momentPanel.setOpaque( false );
									bottomPanel.setBackground( Color.GREEN );
								}
							} else {
								middleMomCnt = 0;
								middleAngCnt = 0;
							}
							appTest = TEST_ANGLE;
						}
					}
				}

				// ---------------------
				// update GUI
				// ---------------------
				angleLabel.setText( String.valueOf( rnd( value[ANGLE], 2 ) ) + "�" );
				momentLabel.setText( String.valueOf( rnd( value[MOMENT], 2 ) ) + "Nm" );
				if( angleOldTime == actualTime ) {
					// update image (with inverted values)
					img = drawImage( img, convDeg2Pix( img, range[ANGLE], ref[ANGLE] ), convDeg2Pix( img, range[ANGLE], min[ANGLE] ), convDeg2Pix( img, range[ANGLE], max[ANGLE] ), convDeg2Pix( img, range[ANGLE], -1 * angleOld ), convDeg2Pix( img, range[ANGLE], -1 * value[ANGLE] ) );
					imgLabel.setIcon( new ImageIcon( img ) );
					//log(debug, "Ok", "image refreshed.");	
				}
				appSleep( pollingTime );
			}
			// --------------------------------------------------------------------------------------------------
			log( debug, "Ok", "EPS initialization cycle ended." );

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
			log( debug, "Error", "executing procedure, PPExecutionException: " + e.getMessage() );
		} catch( Exception e ) {
			status = STATUS_EXECUTION_ERROR;
			resList.add( new Ergebnis( "ExecError", "SDiagEPSinit", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS ) );
			log( debug, "Error", "executing procedure, Exception: " + e.getMessage() );
			e.printStackTrace();
		} catch( Throwable e ) {
			status = STATUS_EXECUTION_ERROR;
			resList.add( new Ergebnis( "ExecError", "SDiagEPSinit", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS ) );
			log( debug, "Error", "executing procedure, Throwable: " + e.getMessage() );
			e.printStackTrace();
		}

		// --------------------------------------------------------------------------------------------------
		// close GUI & set results
		// --------------------------------------------------------------------------------------------------
		if( frame != null ) {
			frame.dispose();
			frame = null;
			log( debug, "Ok", "GUI disposed." );
		}
		for( int i = 0; i < resList.size(); i++ ) {
			log( debug, "Var", "APDM result " + i + ":\n" + ((Ergebnis) resList.get( i )).toString() );
		}
		setPPStatus( info, status, resList );
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//											UTILS
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * <b>initImage</b>
	 * <br>
	 * @param img
	 * @param pxAbsRef - within [0, img.getWidth()/2] px
	 * @param pxAbsMin - within [0, img.getWidth()/2] px
	 * @param pxAbsMax - within [0, img.getWidth()/2] px
	 * @return 
	 */
	private static BufferedImage initImage( BufferedImage img, int pxAbsRef, int pxAbsMin, int pxAbsMax ) {
		Color color = COLOR_BACKGROUND;
		int pxOffset = Math.round( img.getWidth() / 2 );
		int currArea = 0;

		for( int y = 0; y < img.getHeight(); y++ ) {
			// draw the background
			for( int x = 0; x < img.getWidth(); x++ ) {
				currArea = getArea( pxAbsRef, pxAbsMin, pxAbsMax, x - pxOffset );
				// border
				if( isBorder( pxAbsRef, pxAbsMin, pxAbsMax, x - pxOffset ) ) {
					color = COLOR_BORDER;
					// special areas
				} else if( currArea == LEFT || currArea == MIDDLE || currArea == RIGHT ) {
					color = COLOR_AREA;
					// background 
				} else {
					color = COLOR_BACKGROUND;
				}
				img.setRGB( x, y, color.getRGB() );
			}
		}
		return img;
	}

	/**
	 * <b>drawImage</b>
	 * <br>
	 * @param img
	 * @param pxAbsRef - within [0, img.getWidth()/2] px
	 * @param pxAbsMin - within [0, img.getWidth()/2] px
	 * @param pxAbsMax - within [0, img.getWidth()/2] px
	 * @param pxAngleOld - within [-img.getWidth()/2, img.getWidth()/2] px
	 * @param pxAngle - within [-img.getWidth()/2, img.getWidth()/2] px
	 * @return 
	 */
	private BufferedImage drawImage( BufferedImage img, int pxAbsRef, int pxAbsMin, int pxAbsMax, int pxAngleOld, int pxAngle ) {
		Color color = COLOR_BACKGROUND;
		int pxOffset = Math.round( img.getWidth() / 2 );
		int pxBarHalfWidth = Math.round( BAR_WIDTH / 2 );
		int pxAxisHalfWidth = Math.round( AXIS_WIDTH );
		int currArea = 0;

		for( int y = 0; y < img.getHeight(); y++ ) {
			//
			// Re-draw the background behind the bar
			//
			for( int x = pxAngleOld + pxOffset - pxBarHalfWidth; x < pxAngleOld + pxOffset + pxBarHalfWidth; x++ ) {
				// only within GUI x range
				if( x >= 0 && x < img.getWidth() ) {
					currArea = getArea( pxAbsRef, pxAbsMin, pxAbsMax, x - pxOffset );

					// border
					if( isBorder( pxAbsRef, pxAbsMin, pxAbsMax, x - pxOffset ) ) {
						color = COLOR_BORDER;

						// special areas
					} else if( currArea == LEFT || currArea == MIDDLE || currArea == RIGHT ) {
						color = COLOR_AREA;

						// background
					} else {
						color = COLOR_BACKGROUND;
					}
					img.setRGB( x, y, color.getRGB() );
				}
			}
			//
			// draw the angle bar
			//
			currArea = getArea( pxAbsRef, pxAbsMin, pxAbsMax, pxAngle );
			for( int x = pxAngle + pxOffset - pxBarHalfWidth; x < pxAngle + pxOffset + pxBarHalfWidth; x++ ) {
				// only within GUI x range
				if( x > 0 && x < img.getWidth() ) {

					// angle bar axis
					if( x >= pxAngle + pxOffset - pxAxisHalfWidth && x < pxAngle + pxOffset + pxAxisHalfWidth ) {
						color = COLOR_BACKGROUND;

						// angle bar inside area
					} else if( currArea == LEFT || currArea == MIDDLE || currArea == RIGHT ) {
						color = COLOR_BAR_ACTIVE;

						// angle bar
					} else {
						color = COLOR_BAR_DEFAULT;
					}
					img.setRGB( x, y, color.getRGB() );
				}
			}
		}
		return img;
	}

	/**
	 * <b>convDeg2Pix</b>
	 * <br>
	 * Convert degrees [-ANGLE_RANGE/2, ANGLE_RANGE/2] to pixels [-img.getWidth()/2, img.getWidth()/2]<br>
	 * <br>
	 * @param img
	 * @param angleRange
	 * @param angleValue
	 * @return
	 */
	private static int convDeg2Pix( BufferedImage img, float angleRange, float angleValue ) {
		return Math.round( img.getWidth() * angleValue / angleRange );
	}

	/**
	 * <b>getArea </b> 
	 * <br>
	 * Gets the area for the given value.
	 * <br>
	 * @param absRef - middle area limit (abs value) [0, ANGLE_RANGE/2] or [0, img.getWidth()/2]
	 * @param absMin - side areas min limit (abs value) [0, ANGLE_RANGE/2] or [0, img.getWidth()/2]
	 * @param absMax - side areas max limit (abs value) [0, ANGLE_RANGE/2] or [0, img.getWidth()/2]
	 * @param value - value within [-ANGLE_RANGE/2, ANGLE_RANGE/2] or [-img.getWidth()/2, img.getWidth()/2] 
	 * @return
	 * . -1 (OTHER area)
	 * .  0 (MIDDLE area) when between [-absRef,  absRef]
	 * .  1 (LEFT area)	  when between [ absMin,  absMax]	
	 * .  2 (RIGHT area)  when between [-absMax, -absMin]
	 */
	private static int getArea( float absRef, float absMin, float absMax, float value ) {
		// MIDDLE area
		if( value >= -absRef && value <= absRef ) {
			return MIDDLE;
			// LEFT SIDE area
		} else if( value >= absMin && value <= absMax ) {
			return LEFT;
			// RIGHT SIDE area
		} else if( value >= -absMax && value <= -absMin ) {
			return RIGHT;
			// OTHER area
		} else {
			return OTHER;
		}
	}

	/**
	 * <b>isBorder</b> 
	 * <br>
	 * Checks if the given value position is a border/limit position. 
	 * <br>
	 * @param absLimitRef
	 * @param absLimitMin
	 * @param absLimitMax
	 * @param value 
	 * @return
	 */
	private static boolean isBorder( int absRef, int absMin, int absMax, int value ) {
		return (value == 0 || value == -absRef || value == absRef || value == -absMin || value == absMin || value == -absMax || value == absMax);
	}

	/**
	 * <b>rnd </b> 
	 * <br>
	 * Rounds a float value to n decimal places.
	 * <br>
	 * @param value - the float value to round
	 * @param decPlaces - the number of decimal places to use while rounding
	 * @return the rounded float value
	 */
	private static double rnd( double value, int decPlaces ) {
		float tmp = 1;
		for( int i = 1; i <= decPlaces; i++ )
			tmp *= 10;
		return (Math.rint( value * tmp )) / tmp;
	}

	/**
	 * <b>extractArg</b>
	 * <br>
	 * Extract one or several(split with ;) 'CASCADEconstruct' values if present in the given arguments.
	 * <br>
	 * @param debug is the debug flag
	 * @return the arg extracted value string 
	 * @throws InformationNotAvailableException if a '@contruct' value is no available
	 */
	private String extractArg( String argName ) throws InformationNotAvailableException {
		String argValue = getArg( argName );

		if( argValue == null ) {
			// NULL value
			return null;
		}
		if( argValue.indexOf( "@" ) != -1 ) {
			// @construct value
			String[] argSubValues = argValue.split( ";" );
			argValue = "";
			for( int i = 0; i < argSubValues.length; i++ ) {
				if( argSubValues[i].indexOf( "@" ) != -1 ) {
					// extract @construct var value
					argSubValues[i] = getPPResult( argSubValues[i].toUpperCase().trim() );
				}
				argValue += (i == 0 ? "" : " ") + argSubValues[i];
			}
		}
		return argValue;
	}

	/**
	 * <b>appSleep</b>
	 * <br>
	 * Sleeps for the specified time in milliseconds
	 * <br>
	 * @param ms - sleeping time in milliseconds
	 */
	private void appSleep( long ms ) {
		try {
			Thread.sleep( ms );
		} catch( Exception e ) {
			//..
		}
	}

	/**
	 * <b>log </b> 
	 * <br>
	 * Logs a message if the given debug flag is active or if it is an 'Error'
	 * message. 
	 * <br>
	 * @param debug - the debug flag
	 * @param type - the type of message to display
	 * @param text - the message text
	 */
	private void log( boolean debug, String type, String text ) {
		if( type.equalsIgnoreCase( "Error" ) || debug ) {
			System.out.println( "[" + getLocalName() + "] [" + type + "] " + text );
		}
	}

	/**
	 * <b>diagResToRead </b> 
	 * <br>
	 * @param sgbd
	 * @param sgbdJob
	 * @param diagAction
	 * @return An array with the results that should be read. -1 to ignore  
	 * 
	 */
	private int[] diagResToRead( String[] sgbd, String[] sgbdJob, String[] sgbdArgs, int diagAction ) {
		int[] resToRead = { -1, -1, -1 };
		for( int i = 0; i < sgbd.length; i++ ) {
			if( sgbd[i].equalsIgnoreCase( sgbd[diagAction] ) && sgbdJob[i].equalsIgnoreCase( sgbdJob[diagAction] ) && sgbdArgs[i].equalsIgnoreCase( sgbdArgs[diagAction] ) ) {
				resToRead[i] = i;
			}
		}
		return resToRead;
	}
}

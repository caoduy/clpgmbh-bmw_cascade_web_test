package com.bmw.cascade.pruefprozeduren;

import java.util.HashMap;
import java.util.List;
import java.util.ListResourceBundle;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.devices.DeviceExecutionException;
import com.bmw.cascade.devices.DynamicDevice;
import com.bmw.cascade.devices.DynamicDeviceManager;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.util.dynamicdevices.PropertyObject;

/**
 * CASCADE Job AscotLED
 * 
 *	@author Michael Scholler, remes GmbH
 *
 *	@version 19_0_F 2018-02-12
 *	- MS: production release
 *	@version 18_0_T 2018-02-08
 *	- MS: inserted CancelJob to abort on Timeout and UserDialogCanceled
 *	- MS: replaced STATUS_EXECUTION_ABORTED_BY_USER result by STATUS_EXECUTION_ERROR when PP is canceled in user dialog
 *	@version 17_0_F 2017-12-04
 *	- MS: production release
 *	@version 16_0_T 2017-11-22
 *	- MS: inserted CancelJob to abort on Timeout and UserDialogCanceled
 *	@version 15_0_F 2017-10-18
 *	- MS: new parameter AUTOADDRESS_MODE
 *	@version 14_0_F	2017-10-09
 *	- OR: Fix wrong key text in bundle translation HashMap
 *	@version 13_0_F	2017-10-09
 *	- OR: Auto repeat auto addressing when LED not responding
 *  - OR: Add global auto repeat controllable by parameter MAXREPETITIONS
 *  - OR: Start broadcast after auto addressing
 *	@version 12_0_F	2017-10-05
 *	- OR: Cleanup/optimize IO Error and ErrorMemory results
 *  - OR: Fix issue that job result is OK even that there are communication errors
 *	@version 11_0_F	2017-07-31
 *	- MS: new timeout calculation (cf. ASCOTBase.java)
 *	- MS: interval for broadcast changed to 0.500s
 *	@version 10_0_F	2017-05-05
 *	- MS: casting Long to Integer for PropertyObject encapsulation
 *	- MS: new parameter SWITCHONLED to light LED during test 
 *	- MS: parameter LINDELAY, POWERONDELAY as Double in [s]
 *	- MS: formating of measurement value using (Locale)null for dot as decimal separator 
 *	@version 9_0_F	2017-04-20
 *	- MS: new parameter LINDELAY
 *	- MS: timeout and dialog_timeout in [s]
 *	- MS: formating of measurement value according to given precision(minimum=3)
 *	@version 8_0_F	2017-04-10
 *	- MS: formating of measurement value according to given precision(default=3)
 *	@version 7_0_F	2017-04-03
 *	- MS: production release
 *	- MS: JobValue[~] will be added to results
 *	@version 6_0_T 2017-03-27
 *	- MS: dialog can be canceled and will abort execute()
 *	- MS: changed result handling
 *	- MS: changed result handling
 *	- MS: introduced parameter PowerOnDelay	
 *	@version 5_0_T 2017-03-16
 *	- MS: adapted to template ASCOTBase
 *	@version 4_0_F 2017-03-03
 *	- MS: fixed error in result handling
 *	@version 3_0_F 2017-03-02
 *	- BMW: rollback from 1_0_F
 *	@version 2_0_F 2017-03-01
 * 	- MS: changed of parsing parameter DEBUG
 * 	- MS: display UserDialog while executing total sequence not only while a single step
 *	- MS: changed result handling
 *	@version 1_0_F 2017-02-21
 * 	- MS: initial release
 */

public class AscotLED_19_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	static final long serialVersionUID = 0x08122972300000L;
	private String Werkzeug = "ASCOT";
	private String jobName	= "AscotLED";
	private Boolean checkAscotParameters = false;
	private Double      jobCancelWaitTime = 1.0;

	private UserDialog	userDialog	= null;
	private Boolean		debug		= false;
	private Double		timeout		= 30.000;
	private String		component   = "";

	private String linInterface;
	private String linVersion;

	/**
	 * Default constructor, only for de-seriaslisation
	 */
	public AscotLED_19_0_F_Pruefprozedur() {
	}

	/**
	 * Creates a new test procedure
	 * 
	 * @param pruefling			Class of related Pruefling
	 * @param pruefprozName		Name of test procedure
	 * @param hasToBeExecuted	Execution condition for absence of errors
	 */
	public AscotLED_19_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
		super(pruefling, pruefprozName, hasToBeExecuted);
		attributeInit();
	}

	/**
	 * Initialises arguments
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

   /**
 	* Provides mandatory arguments
 	*/
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "PwrPin", "GndPin", "LinPin" };
		return args;
	}
	/** 
	 * Provides optional arguments
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { 
			"DEBUG", "DELAY", "PAUSE", "TIMEOUT", "COMPONENT", 
			"DIALOG_TITLE",	"DIALOG_TEXT", "DIALOG_CANCEL", "DIALOG_STYLE", "DIALOG_TIME",
			"PowerSource", "Voltage", "CurrentLimit", "Interface", "LinVersion",
			"POWERONDELAY",	"LINDELAY", "QUANTITY", "LIGHT_SWORD", "SWITCHONLED", "MAXREPETITIONS", "AUTOADDRESS_MODE" };
		return args;
	}

	/**
     *	Pr�ft die Argumente auf Existenz und Wert.
     *
	 * @return <code>false</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
     */
	@Override
	public boolean checkArgs() {
	   boolean ok = super.checkArgs();   	// obligatorische Existenzpr�fung 
	   return ok;
	}

	private void exportArgs() {
		declareParameter("DEBUG",			"",				ParameterType.Boolean,	false );
		declareParameter("DELAY",			"",				ParameterType.Double,	0.0 );
		declareParameter("PAUSE",			"",				ParameterType.Double,	0.0 );
		declareParameter("TIMEOUT",			"",				ParameterType.Double,	timeout );
		declareParameter("COMPONENT",		"",				ParameterType.String,	"" );

		declareParameter("DIALOG_TITLE",	"",				ParameterType.String,	"" );
		declareParameter("DIALOG_TEXT",		"",				ParameterType.String,	"" );
		declareParameter("DIALOG_CANCEL",	"",				ParameterType.Boolean,	false );
		declareParameter("DIALOG_STYLE",	"",				ParameterType.Long,		0 );
		declareParameter("DIALOG_TIME",		"",				ParameterType.Double,	-1.0 );

		// job specific parameter
		declareParameter("PowerSource",		"PowerSource", 	ParameterType.String,	"300mA Pwr");
		declareParameter("Voltage",			"Voltage", 		ParameterType.Double,	13.0);
		declareParameter("CurrentLimit",	"CurrentLimit", ParameterType.Double,	0.3);
		declareParameter("PwrPin",			"PwrPin", 		ParameterType.Long,		null);
		declareParameter("GndPin",			"GndPin", 		ParameterType.Long,		null);
		declareParameter("LinPin",			"LinPin", 		ParameterType.Long,		null);
		declareParameter("Interface",		"Interface",	ParameterType.String,	"LIN1");
		declareParameter("LinVersion",		"LinVersion", 	ParameterType.String,	"V20");

		declareParameter("LINDELAY",		"", 			ParameterType.Double,	0.020);
		declareParameter("POWERONDELAY", 	"",				ParameterType.Double,	0.000);
		declareParameter("QUANTITY", 		"",				ParameterType.Long,		26);
		declareParameter("LIGHT_SWORD", 	"",				ParameterType.Long,		0);
		declareParameter("SWITCHONLED",		"",				ParameterType.Boolean,	true);
		declareParameter("MAXREPETITIONS", 	"",				ParameterType.Long, 	2);
		declareParameter("AUTOADDRESS_MODE","",				ParameterType.Long, 	0x02);
	}

	/**
	 * Executes test procedure
	 * 
	 * @param info	Information for execution
	 */
	public void execute(ExecutionInfo info) {
		Vector<Ergebnis> ergebnisListe = new Vector<Ergebnis>();
		HashMap<String, Object> jobParameter = new HashMap<String, Object>();
		JobResult jobResult;
		XREF<Integer> status = new XREF<Integer>(STATUS_EXECUTION_ERROR);
		boolean copyResultOnSuccess = false;

		Double delay;
		Double pause;

		String  dialogTitle;
		String  dialogText;
		Boolean dialogCancel;
		Integer dialogStyle;
		Double	dialogTime;

		Integer pwrPin;
		Integer gndPin;
		Integer linPin;

		String powerSource;
		Double voltage;
		Double currentLimit;
		Double powerOnDelay;
		Double linDelay;
		Integer quantity;
		Integer lightSword;
		Boolean switchOnLed;
		Integer maxRepetitions;

		if(jobName.isEmpty()) {
			addResultsException(ergebnisListe, new Exception(bundle.getString("NoJobName")));
			setPPStatus(info, STATUS_EXECUTION_ERROR, ergebnisListe);
			return;
		}

		try {  // special handling for debug flag
			String DEBUG = getArg("DEBUG").toUpperCase();
			if(DEBUG.startsWith("T") || DEBUG.startsWith("Y") || DEBUG.startsWith("J")) {
				debug=true;
			} else if(Double.parseDouble(DEBUG)>0)	{
				debug=true;
			}
		} catch (Exception e) {
			debug=false;
		}

		if(debug) {
			System.out.println(String.format("<%s> %s.%s.%s[%s] -> %s v%s [%s] => %s()", 
					this.getPr�fling().getAuftrag().getFahrgestellnummer7(),
					this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getName(), this.getName(),this.getPr�fling().getName(),this.getPr�fling().getClassName(), 
					this.getPPName(), this.getVersion(), this.getClassName(), jobName
			));
		}

		//Aufruf der Argumentpr�fung  
		try {
			exportArgs();
			if (checkArgs() == false) throw new PPExecutionException( PB.getString("parameterexistenz" ));
			parseArgs();

			delay     			= (Double)  getParameter("DELAY");
			pause      			= (Double)  getParameter("PAUSE");
//			timeout 			= (Double)  getParameter("TIMEOUT");
			timeout 			= (Double)  /*<*CascadeTimeout*>*/timeout;
			component 			= (String)  getParameter("COMPONENT");

			dialogTitle    		= (String)  getParameter("DIALOG_TITLE");
			dialogText     		= (String)  getParameter("DIALOG_TEXT");
			dialogCancel   		= (Boolean) getParameter("DIALOG_CANCEL");
			dialogStyle    		= (Integer) getParameter("DIALOG_STYLE");
			dialogTime     		= (Double)  getParameter("DIALOG_TIME");

			pwrPin 				= (Integer) getParameter("PwrPin");
			gndPin 				= (Integer) getParameter("GndPin");
			linPin 				= (Integer) getParameter("LinPin");

			powerSource			= (String)  getParameter("PowerSource");
			voltage      		= (Double)  getParameter("Voltage");
			currentLimit 		= (Double)  getParameter("CurrentLimit");

			linInterface 		= (String)  getParameter("Interface");
			linVersion   		= (String)  getParameter("LinVersion");

			powerOnDelay		= (Double)  getParameter("POWERONDELAY");
			linDelay			= (Double)  getParameter("LINDELAY");
			quantity   			= (Integer) getParameter("QUANTITY");
			lightSword 			= (Integer) getParameter("LIGHT_SWORD");
			switchOnLed			= (Boolean)	getParameter("SWITCHONLED");
			maxRepetitions		= (Integer) getParameter("MAXREPETITIONS");
			autoAddressMode		= (Integer) getParameter("AUTOADDRESS_MODE");
			
		} catch (PPExecutionException e) {
			addResultsException(ergebnisListe, e);
			setPPStatus(info, STATUS_EXECUTION_ERROR, ergebnisListe);
			return;
		}

		try {
			if((!dialogTitle.isEmpty()) || (!dialogText.isEmpty())) {
				if (getPr�flingLaufzeitUmgebung() != null) {
					userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
					if (userDialog != null) {
						userDialog.setAllowCancel(dialogCancel);

						switch( dialogStyle ) {
							case 1:
								userDialog.displayStatusMessage(dialogTitle, dialogText, (int)Math.round(dialogTime*1000));
								break;
							case 2:
								userDialog.displayAlertMessage(dialogTitle, dialogText, (int)Math.round(dialogTime*1000));
								break;
							case 99:
								userDialog.displayMessage(dialogTitle, dialogText, (int)Math.round(dialogTime*1000));
								break;
							default:
								userDialog.displayUserMessage(dialogTitle, dialogText, (int)Math.round(dialogTime*1000));
								break;
						}
					}
				}
			}
		} catch (Exception e) {
			addResultsException(ergebnisListe, new Exception(bundle.getString("UserDialogOpenFail")+": "+e.getMessage()));
			setPPStatus(info, STATUS_EXECUTION_ERROR, ergebnisListe);
			return;
		}

		if (delay > 0) {
			try {
				Thread.sleep(Math.round(delay));
			}
			catch( InterruptedException e ) {
			}        	
		}

		boolean switchOffPowerSource = false;
		boolean switchOffPwrPin = false;
		boolean switchOffGndPin = false;
		boolean switchOffLinPin = false;

		try {
			/* start procedure */

			// switch power on
			System.out.println(jobName+": switch power on: Source="+powerSource+", pwrPin="+pwrPin+", gndPin="+gndPin+", voltage="+voltage+", currentLimit="+currentLimit);
			jobParameter.clear();
			jobParameter.put("PowerSource", powerSource);
			jobParameter.put("Voltage", voltage);
			jobParameter.put("CurrentLimit", currentLimit);
			jobParameter.put("PwrPin", pwrPin);
			jobParameter.put("GndPin", gndPin);
			jobResult = executeJob("AscotSetPowerSourceOn", jobParameter, ergebnisListe, copyResultOnSuccess, status);
			if (!jobResult.getExecutionOk()) {
				String msg = bundle.getString("UnableToPowerOn");
				System.out.println(msg);
				throw new PPExecutionException(msg);
			}
			switchOffPowerSource = jobResult.getReturnParameterAsBoolean("SwitchOffPowerSource");
			switchOffPwrPin = jobResult.getReturnParameterAsBoolean("SwitchOffPwrPin");
			switchOffGndPin = jobResult.getReturnParameterAsBoolean("SwitchOffGndPin");

			// setup LIN interface
			System.out.println(jobName+": connect "+linInterface+": busPin="+linPin);
			jobParameter.clear();
			jobParameter.put("FunctionName", linInterface);
			jobParameter.put("Pin", linPin);
			jobParameter.put("State", true);
			jobResult = executeJob("AscotSetFunctionPin", jobParameter, ergebnisListe, copyResultOnSuccess, status);
			if (!jobResult.getExecutionOk()) {
				String msg = bundle.getString("UnableToConnectLIN");
				System.out.println(msg);
				throw new PPExecutionException(msg);
			}
			switchOffLinPin = jobResult.getReturnValueAsBoolean();

			Thread.sleep(Math.round(powerOnDelay*1000));
			
			int Channel = 0;
			int NumOfChannels = quantity;
			int Lichtschwert = lightSword;
			int LedStatus[]				= { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
											0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
											0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
											0x00, 0x00, 	//LEDS
											0x00, 0x00, };	//Lichtswert

			int Data[] 					= { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

			boolean successful;
			XREF<JobResult> ergebnis = new XREF<JobResult>();
			
			if(LedStatus.length<MAXMODULES+2) {
				String msg = "LedStatus["+LedStatus.length+"] has wrong size! Must be "+MAXMODULES;
				System.out.println(msg);
				throw new PPExecutionException(msg);
			}
			String oldComponent = component;
			if (oldComponent ==null || oldComponent.length() == 0)
				oldComponent = bundle.getString("NotConnectedMesage");

			for (int repetition = 0; repetition <= maxRepetitions; ++repetition) {
				// reset results, status ,...
				for (int n = 0; n < LedStatus.length; ++n)
					LedStatus[n] = 0x00;
				status.setValue(STATUS_EXECUTION_ERROR);
				ergebnisListe.clear();
				if (repetition > 0)
					System.out.println("AscotLED: Full repeat #" + repetition);
				
				// perform auto addressing for LED modules
				successful = doAutoAddressing(true, NumOfChannels, Lichtschwert, linDelay, ergebnisListe, status);
				
				
				if (successful && switchOnLed) {
					// send cyclic broadcast
					System.out.println(jobName+": setup broadcast");
					jobParameter.clear();
					jobParameter.put("Interface", linInterface);
					jobParameter.put("Id", "LinLedBroadcast");
					jobParameter.put("LinId", 1);
					jobParameter.put("Interval", 500);
					jobParameter.put("Data", "ff ff ff 02 fd fe fe fe");
					component = "LIN - Set broadcast";
					jobResult = executeJob("AscotLinSetCyclicMessage", jobParameter, ergebnisListe, copyResultOnSuccess, status);
					if (!jobResult.getExecutionOk()) {
						String msg = bundle.getString("UnableToSetupCyclicMessage");
						System.out.println(msg);
						// try again
						ergebnisListe.clear();
						jobResult = executeJob("AscotLinSetCyclicMessage", jobParameter, ergebnisListe, copyResultOnSuccess, status);
						successful = jobResult.getExecutionOk();
					}
					if (successful) {
						// start cyclic broadcast
						System.out.println(jobName+": start broadcast");
						jobParameter.clear();
						jobParameter.put("Interface", linInterface);
						jobParameter.put("Id", "LinLedBroadcast");
						component = "LIN - Start broadcast";
						jobResult = executeJob("AscotLinStartCyclicMessage", jobParameter, ergebnisListe, copyResultOnSuccess, status);
						if (!jobResult.getExecutionOk()) {
							String msg = bundle.getString("UnableToStartCyclicMessage");
							System.out.println(msg);
							// try again
							ergebnisListe.clear();
							jobResult = executeJob("AscotLinStartCyclicMessage", jobParameter, ergebnisListe, copyResultOnSuccess, status);
							successful = jobResult.getExecutionOk();
						}
					}
				}

				if (successful) {
					System.out.println(jobName+": read status");
					for (Channel = 0; Channel < NumOfChannels; Channel++) {
						LedStatus[Channel]=1;
						component = "LED "+ (Channel+1) + " " + oldComponent;
						boolean success = linReceive(LED_BASE + Channel, Data, FRAMELEN, ergebnis);
						System.out.println(jobName+": read status channel="+Channel+": success="+successful);
						if (success) {
							LedStatus[Channel] = 2;
							addResultsErgebnis(ergebnisListe, "LED"+Channel, bundle.getString("Success"), Ergebnis.FT_IO);
						} else {
							successful = false;
							addResultsErgebnisse(ergebnisListe, "LED"+Channel, ergebnis.getValue().ergebnisse);
						}
						ergebnis.getValue().ergebnisse.clear();
						Thread.sleep(Math.round(linDelay*1000));
					}
					Channel += 1;
					if (1 == Lichtschwert) {
						LedStatus[Channel]=1;
						component = "Lichtschwert 1 " + oldComponent;
						boolean success = linReceive(LED_BASE + Channel, Data, FRAMELEN, ergebnis);
						System.out.println(jobName+": read status channel="+Channel+" Lichtschwert=1: success="+successful);
						if (success) {
							LedStatus[LedStatus.length-2] = 2;
							addResultsErgebnis(ergebnisListe, "Lichtschwert1", bundle.getString("Success"), Ergebnis.FT_IO);
						} else {
							successful = success;
							addResultsErgebnisse(ergebnisListe, "Lichtschwert1", ergebnis.getValue().ergebnisse);
						}
						ergebnis.getValue().ergebnisse.clear();
						Thread.sleep(Math.round(linDelay*1000));
					}
					Channel += 1;
					if (2 == Lichtschwert) {
						LedStatus[Channel]=1;
						component = "Lichtschwert 2 " + oldComponent;
						boolean success = linReceive(LED_BASE + Channel, Data, FRAMELEN, ergebnis);
						System.out.println(jobName+": read status channel="+Channel+" Lichtschwert=2: success="+successful);
						if (success) {
							LedStatus[LedStatus.length-1] = 2;
							addResultsErgebnis(ergebnisListe, "Lichtschwert2", bundle.getString("Success"), Ergebnis.FT_IO);
						} else {
							successful = success;
							addResultsErgebnisse(ergebnisListe, "Lichtschwert2", ergebnis.getValue().ergebnisse);
						}
						ergebnis.getValue().ergebnisse.clear();
						Thread.sleep(Math.round(linDelay*1000));
					}
				}
				
				if (successful)
					break;
				else if (switchOnLed)
					stopBroadcast();
			}
			
			// clear addresses for LED modules
			doAutoAddressing(false, NumOfChannels, Lichtschwert, linDelay, ergebnisListe, status);

			successful=true;

			// check for errors in results
			for (int i = 0; i < ergebnisListe.size(); ++i) {
				if(ergebnisListe.get(i).getFehlerTyp() == Ergebnis.FT_NIO || ergebnisListe.get(i).getFehlerTyp() == Ergebnis.FT_NIO_SYS) {
					successful = false;
					break;
				}
			}
			
			// check for errors in LED modules
			for(int i=0; i<NumOfChannels; i++) {
				if(LedStatus[i]<2) {
					successful=false;
					break;
				}
			}
			if(Lichtschwert==1 && LedStatus[LedStatus.length-2]<2) {
				successful=false;
			}
			if(Lichtschwert==2 && LedStatus[LedStatus.length-1]<2) {
				successful=false;
			}
			
			if(successful) {
				System.out.println(jobName+": finished successful");
				//ergebnisListe.add(new Ergebnis("", Werkzeug, "", "", "", "", "", "", "", "", "", "", "", bundle.getString("Success"), "", Ergebnis.FT_IO));
				status.setValue(STATUS_EXECUTION_OK);
			} else {
				System.out.println(jobName+": finished with errors");
				//ergebnisListe.add(new Ergebnis("", Werkzeug, "", "", "", "", "", "", "", "", "", "", "", bundle.getString("Fail"), "", Ergebnis.FT_NIO));
				status.setValue(STATUS_EXECUTION_ERROR);
			} 
			/* end procedure */
		} catch (Exception e) {
			addResultsException(ergebnisListe, e);
			status.setValue(STATUS_EXECUTION_ERROR);
		} finally {
			try {
				if(switchOnLed) {
					stopBroadcast();
				}
				// switch off LIN
				if (switchOffLinPin) {
					System.out.println(jobName+": switch lin off");
					jobParameter.clear();
					jobParameter.put("FunctionName", linInterface);
					jobParameter.put("Pin", linPin);
					jobParameter.put("State", false);
					jobResult = executeJob("AscotSetFunctionPin", jobParameter, ergebnisListe, copyResultOnSuccess, status);
				}

				// switch off power
				System.out.println(jobName+": switch power off");
				jobParameter.clear();
				jobParameter.put("PowerSource", powerSource);
				jobParameter.put("PwrPin", pwrPin);
				jobParameter.put("GndPin", gndPin);
				jobParameter.put("SwitchOffPowerSource", switchOffPowerSource);
				jobParameter.put("SwitchOffPwrPin", switchOffPwrPin);
				jobParameter.put("SwitchOffGndPin", switchOffGndPin);
				jobResult = executeJob("AscotSetPowerSourceOff", jobParameter, ergebnisListe, copyResultOnSuccess, status);

			} catch (Exception e) {
				addResultsException(ergebnisListe, e);
				status.setValue(STATUS_EXECUTION_ERROR);
			}
		}

		if (pause > 0) {
            try {
            	Thread.sleep(Math.round(pause));
            }
            catch( InterruptedException e ) {
            }        	
    	}

		//cleanup
		try {
	        //close dialog
			if ((userDialog != null) && (userDialog.isShowing())) {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			}
		} catch (Exception e) {
			addResultsException(ergebnisListe, e);
		}

		if(ergebnisListe.size()==0) {
			addResultsErgebnis(ergebnisListe, bundle.getString("Result"),
				status.getValue()==STATUS_EXECUTION_OK?bundle.getString("Success"):bundle.getString("Fail"), 
				status.getValue()==STATUS_EXECUTION_OK?Ergebnis.FT_IO:Ergebnis.FT_NIO );
		}
		setPPStatus(info, status.getValue(), ergebnisListe);
		if(debug) {
			String results = "\n";
			for(Ergebnis e : ergebnisListe) {
				results += String.format("\t%s %-8.8s %1.1s  %-40.40s=%-30.30s [%-8.8s,%-8.8s] frage=\"%s\" antwort=\"%s\" anweisung=\"%s\" fehler=\"%s\" hinweis=\"%s\" p1=%s p2=%s p3=%s\n", 
						e.getID(), e.getWerkzeug(), e.getFehlerTyp(),
						e.getErgebnis(), e.getErgebnisWert(), e.getMinWert(), e.getMaxWert(),
						e.getFrageText(), e.getAntwortText(), e.getAnweisText(), e.getFehlerText(), e.getHinweisText(),
						e.getParameter1(), e.getParameter2(), e.getParameter3()
				);
			}
			System.out.println(String.format("%s.%s Results = {%s} finished [%d=%s]",	this.getPr�fling().getName(), this.getName(), results, status.getValue(), statusAsString(status.getValue())));
		}
	}

	private void stopBroadcast() {
		// stop cyclic broadcast
		Vector<Ergebnis> ergebnisListe = new Vector<Ergebnis>();
		XREF<Integer> status = new XREF<Integer>(STATUS_EXECUTION_ERROR);
		HashMap<String, Object> jobParameter = new HashMap<String, Object>();
		
		System.out.println(jobName+": stop broadcast");
		jobParameter.clear();
		jobParameter.put("Interface", linInterface);
		jobParameter.put("Id", "LinLedBroadcast");
		JobResult jobResult = executeJob("AscotLinRemoveCyclicMessage", jobParameter, ergebnisListe, false, status);
		if (!jobResult.getExecutionOk()) {
			String msg = bundle.getString("UnableToRemoveCyclicMessage");
			System.out.println(msg);
			// try again but ignore any errors then
			executeJob("AscotLinRemoveCyclicMessage", jobParameter, ergebnisListe, false, status);
		}
	}
	
	
	private static int FRAMELEN = 8;
	private static int LED_BASE = 0x20;
	private static int MAXMODULES = 26;

	private static int MasterRequest = 0x3c;
	//private static int SlaveResponse = 0x3d;

	private Integer autoAddressMode = 0x02;
	private Integer autoAddressMode_index = 6;
	
	private int FrameAutoAdressStart[]			= { 0x7f, 0x06, 0xb5, 0xff, 0x7f, 0x01, 0x02, 0xff };
	private int StoreNAD[] 						= { 0x7f, 0x06, 0xb5, 0xff, 0x7f, 0x03, 0x02, 0xff };
	private int FrameAutoAdressEnd[]			= { 0x7f, 0x06, 0xb5, 0xff, 0x7f, 0x04, 0x02, 0xff };
	private int FrameAutoAdress[]				= { 0x7f, 0x06, 0xb5, 0xff, 0x7f, 0x02, 0x02, 0xff };
	private static int WriteFrameID1[]			= { 0xff, 0x06, 0xb1, 0xff, 0x7f, 0x02, 0x30, 0xff };
	private static int WriteFrameID2[]			= { 0xff, 0x06, 0xb1, 0xff, 0x7f, 0x01, 0x30, 0xff };
	private static int WriteFrameID3[]			= { 0xff, 0x06, 0xb1, 0xff, 0x7f, 0x03, 0x30, 0xff };

	private static int FrameID1[] = { 0x03, 0xc4, 0x85, 0x06, 0x47, 0x08, 0x49, 0xca, 
									  0x8b, 0x4c, 0x0d, 0x8e, 0xcf, 0x50, 0x11, 0x92, 
									  0xd3, 0x14, 0x55, 0xd6, 0x97, 0xd8, 0x99, 0x1a,
									  0x5b, 0x9c };
	private static int FrameID2[] = { 0x20, 0x61, 0xe2, 0xa3, 0x64, 0x25, 0xa6, 0xe7, 
									  0xa8, 0xe9, 0x6a, 0x2b, 0xec, 0xad, 0x2e, 0x6f, 
									  0xf0, 0xb1, 0x32, 0x73, 0xb4, 0xf5, 0x76, 0x37, 
									  0x78, 0x39 };
	private static int FrameID3[] = { 0xc1, 0xc1, 0xc1, 0xc1, 0xc1, 0xc1, 0xc1, 0xc1, 
									  0xc1, 0xc1, 0xc1, 0xc1, 0xc1, 0xc1, 0xc1, 0xc1, 
									  0xc1, 0xc1, 0xc1, 0xc1, 0xc1, 0xc1, 0xc1, 0xc1, 
									  0xc1, 0xc1 };

	
	private boolean doAutoAddressing(boolean setAddress, Integer numOfChannels, Integer lichtschwert, Double linDelay, 
			Vector<Ergebnis> extErgebnisListe, XREF<Integer> status) throws Exception {
		Integer channel;
		int data[] 					= { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
		boolean successful = false;
		Vector<Ergebnis> ergebnisListe = new Vector<Ergebnis>(); 
		Integer extStatus = status.getValue();

		System.out.println(jobName+": numberOfChannels="+numOfChannels);
		System.out.println(jobName+": lichtSchwert="+lichtschwert);
		System.out.println(jobName+": AutoAddressMode="+autoAddressMode);

		
		for (int repeat = 0; repeat < 2; ++repeat) {
			// reset results and status of repetition
			ergebnisListe.clear();
			status.setValue(extStatus);
			
			String oldComponent = component; 
			
			try {
				// send wakeup frame (broadcast with no actual data)
				component = "LIN - Wakeup";
				for (int i = 0; i < FRAMELEN; i++) {
					data[i] = 0;
				}
				successful = linSend(1, data, FRAMELEN, ergebnisListe, false, status);
				System.out.println(jobName+": wakeup success="+successful);
				// wait 100ms after wakeup call
				Thread.sleep(100);
								
				// Send Begin of auto addressing
				if (successful) {
					component = "LIN - StartAutoAddressung";
					FrameAutoAdressStart[autoAddressMode_index] = autoAddressMode;
					successful = linSend(MasterRequest, FrameAutoAdressStart, FRAMELEN, ergebnisListe, false, status);
					System.out.println(jobName+": AutoAddressStart success="+successful);
					Thread.sleep(Math.round(linDelay*1000));
				}
				
				// send NAD addresses
				if (successful) {
					for (int i = 0; i < FRAMELEN; i++) {
						data[i] = FrameAutoAdress[i];
					}
					for (channel = numOfChannels; channel > 0; channel--) {
						component = "LIN - Set NAD " + channel;
						data[FRAMELEN - 1] = setAddress ? channel : 1;
						data[autoAddressMode_index] = autoAddressMode;
						successful = linSend(MasterRequest, data, FRAMELEN, ergebnisListe, false, status);
						System.out.println(jobName+": AutoAddress channel="+channel+" success="+successful);
						Thread.sleep(Math.round(linDelay*1000));
					}
				}
		
				// store NADs
				if (successful) {
					component = "LIN - Store NADs";
					StoreNAD[autoAddressMode_index] = autoAddressMode;
					successful = linSend(MasterRequest, StoreNAD, FRAMELEN, ergebnisListe, false, status);
					System.out.println(jobName+": StoreNAD success="+successful);
					Thread.sleep(Math.round(linDelay*1000));
				}
		
				// Send End of auto addressing
				if (successful) {
					component = "LIN - EndOfAutoAddressing";
					FrameAutoAdressEnd[autoAddressMode_index] = autoAddressMode;
					successful = linSend(MasterRequest, FrameAutoAdressEnd, FRAMELEN, ergebnisListe, false, status);
					System.out.println(jobName+": AutoAddressEnd success="+successful);
					Thread.sleep(Math.round(linDelay*1000));
				}
		
				if (setAddress) {
					// Send first frame id
					if (successful) {
						System.out.println(jobName+": WriteFrameID1");
						for (int i = 0; i < FRAMELEN; i++) {
							data[i] = WriteFrameID1[i];
						}
						for (channel = 0; channel < numOfChannels; channel++) {
							component = "LIN - WriteFrameId 1 to " + channel;
							data[0] = channel + 1;
							data[7] = FrameID1[channel];
							successful = linSend(MasterRequest, data, FRAMELEN, ergebnisListe, false, status);
							System.out.println(jobName+": WriteFrameID1 channel="+channel+" success="+successful);
							Thread.sleep(Math.round(linDelay*1000));
						}
						if (1 == lichtschwert) {
							component = "LIN - WriteFrameId 1 to LS1";
							data[0] = 0x19;
							data[7] = 0x5b;
							successful = linSend(MasterRequest, data, FRAMELEN, ergebnisListe, false, status);
							System.out.println(jobName+": WriteFrameID1 Lichtschwert=1 success="+successful);
							Thread.sleep(Math.round(linDelay*1000));
						} else if (2 == lichtschwert) {
							component = "LIN - WriteFrameId 1 to LS2";
							data[0] = 0x1a;
							data[7] = 0x9c;
							successful = linSend(MasterRequest, data, FRAMELEN, ergebnisListe, false, status);
							System.out.println(jobName+": WriteFrameID1 Lichtschwert=2 success="+successful);
							Thread.sleep(Math.round(linDelay*1000));
						}
					}
			
					// Send second frame id
					if (successful) {
						System.out.println(jobName+": WriteFrameID2");
						for (int i = 0; i < FRAMELEN; i++) {
							data[i] = WriteFrameID2[i];
						}
						for (channel = 0; channel < numOfChannels; channel++) {
							component = "LIN - WriteFrameId 2 to " + channel;
							data[0] = channel + 1;
							data[7] = FrameID2[channel];
							successful = linSend(MasterRequest, data, FRAMELEN, ergebnisListe, false, status);
							System.out.println(jobName+": WriteFrameID2 channel="+channel+" success="+successful);
							Thread.sleep(Math.round(linDelay*1000));
						}
						if (1 == lichtschwert) {
							component = "LIN - WriteFrameId 2 to LS1";
							data[0] = 0x19;
							data[7] = 0x78;
							successful = linSend(MasterRequest, data, FRAMELEN, ergebnisListe, false, status);
							System.out.println(jobName+": WriteFrameID2 Lichtschwert=1 success="+successful);
							Thread.sleep(Math.round(linDelay*1000));
						} else if (2 == lichtschwert) {
							component = "LIN - WriteFrameId 2 to LS2";
							data[0] = 0x1a;
							data[7] = 0x39;
							successful = linSend(MasterRequest, data, FRAMELEN, ergebnisListe, false, status);
							System.out.println(jobName+": WriteFrameID2 Lichtschwert=2 success="+successful);
							Thread.sleep(Math.round(linDelay*1000));
						}
					}
			
					// Send third frame id
					if (successful) {
						System.out.println(jobName+": WriteFrameID3");
						for (int i = 0; i < FRAMELEN; i++) {
							data[i] = WriteFrameID3[i];
						}
						for (channel = 0; channel < numOfChannels; channel++) {
							component = "LIN - WriteFrameId 3 to " + channel;
							data[0] = channel + 1;
							data[7] = FrameID3[channel];
							successful = linSend(MasterRequest, data, FRAMELEN, ergebnisListe, false, status);
							System.out.println(jobName+": WriteFrameID3 channel="+channel+" success="+successful);
							Thread.sleep(Math.round(linDelay*1000));
						}
						if (1 == lichtschwert) {
							component = "LIN - WriteFrameId 3 to LS1";
							data[0] = 0x19;
							data[7] = 0xc1;
							successful = linSend(MasterRequest, data, FRAMELEN, ergebnisListe, false, status);
							System.out.println(jobName+": WriteFrameID3 Lichtschwert=1 success="+successful);
							Thread.sleep(Math.round(linDelay*1000));
						} else if (2 == lichtschwert) {
							component = "LIN - WriteFrameId 3 to LS2";
							data[0] = 0x1a;
							data[7] = 0xc1;
							successful = linSend(MasterRequest, data, FRAMELEN, ergebnisListe, false, status);
							System.out.println(jobName+": WriteFrameID3 Lichtschwert=2 success="+successful);
							Thread.sleep(Math.round(linDelay*1000));
						}
					}
				}
			}
			catch (Exception e) {
				successful = false;
				if (repeat >= 2)
					throw e;
			}
			finally {
				component = oldComponent;
			}
			if(successful)
				break;
		}
		// copy results
		for(int n = 0; n < ergebnisListe.size(); ++n) {
			extErgebnisListe.addElement(ergebnisListe.get(n));
		}
		return successful;
	}
	
	private boolean linSend(int id, int data[], int len, XREF<JobResult> result) {
		System.out.println(jobName+"::linSend(id="+id+",data["+data.length+"]={"+HexString.intArrayToHex(data)+"},len="+len+")");
		if ((id < 0x00) || (id > 0xff)) {
			System.out.println(jobName+"::linSend(): wrong id 0x00<"+id+"<0xff");
			return false;
		}
		if ((len < 0x0000) || (len > 0xffff)) {
			System.out.println(jobName+"::linSend(): wrong len 0x0000<"+len+"<0xffff");
			return false;
		}
		if (data.length != len) {
			System.out.println(jobName+"::linSend(): wrong data length "+data.length+"!="+len);
			return false;
		}

		HashMap<String, Object> parameter = new HashMap<String, Object>();
		parameter.clear();
		parameter.put("Interface", linInterface);
		parameter.put("Id", id);
		parameter.put("Data", HexString.intArrayToHex(data));
		Vector<Ergebnis> ergebnisListe = new Vector<Ergebnis>();
		Boolean copyResultOnSuccess = true;
		XREF<Integer> status = new XREF<Integer>();
		result.setValue(executeJob("AscotLinSendFrame", parameter, ergebnisListe, copyResultOnSuccess, status));

		if (!result.getValue().getExecutionOk())
			return false;

		return true;
	}
	private boolean linSend(int id, int data[], int len, Vector<Ergebnis> ergebnisListe, Boolean copyResultOnSuccess, XREF<Integer> status) {
		System.out.println(jobName+"::linSend(id="+id+",data["+data.length+"]={"+HexString.intArrayToHex(data)+"},len="+len+")");
		if ((id < 0x00) || (id > 0xff)) {
			System.out.println(jobName+"::linSend(): wrong id 0x00<"+id+"<0xff");
			return false;
		}
		if ((len < 0x0000) || (len > 0xffff)) {
			System.out.println(jobName+"::linSend(): wrong len 0x0000<"+len+"<0xffff");
			return false;
		}
		if (data.length != len) {
			System.out.println(jobName+"::linSend(): wrong data length "+data.length+"!="+len);
			return false;
		}

		HashMap<String, Object> parameter = new HashMap<String, Object>();
		parameter.clear();
		parameter.put("Interface", linInterface);
		parameter.put("Id", id);
		parameter.put("Data", HexString.intArrayToHex(data));

		JobResult result = executeJob("AscotLinSendFrame", parameter, ergebnisListe, copyResultOnSuccess, status);

		if (!result.getExecutionOk())
			return false;

		return true;
	}

	private boolean linReceive(int id, int data[], int len, XREF<JobResult> result) {
		if ((id < 0x00) || (id > 0xff)) {
			System.out.println(jobName+"::linSend(): wrong id 0x00<"+id+"<0xff");
			return false;
		}
		if ((len < 0x0000) || (len > 0xffff)) {
			System.out.println(jobName+"::linSend(): wrong len 0x0000<"+len+"<0xffff");
			return false;
		}
		if (data.length != len) {
			System.out.println(jobName+"::linSend(): wrong data length "+data.length+"!="+len);
			return false;
		}
		for(int i=0; i<data.length; i++) {
			data[i] = 0;
		}

		HashMap<String, Object> parameter = new HashMap<String, Object>();
		parameter.clear();
		parameter.put("Interface", linInterface);
		parameter.put("Id", id);
		parameter.put("Length", len);
		parameter.put("Timeout", 20);
		Vector<Ergebnis> ergebnisListe = new Vector<Ergebnis>();
		Boolean copyResultOnSuccess=false;
		XREF<Integer> status = new XREF<Integer>();
		
		result.setValue(executeJob("AscotLinRequestFrame", parameter, ergebnisListe, copyResultOnSuccess, status));
		boolean success = result.getValue().getExecutionOk();

		if(success) {
			byte d[] = HexString.hexToArray(result.getValue().getReturnValueAsString());
			for(int i=0; i<d.length; i++) {
				data[i] = d[i];
			}
		}
		System.out.println(jobName+"::linReceive(id="+id+",data["+data.length+"]={"+HexString.intArrayToHex(data)+"},len="+len+") = "+success);
		return success;		
	}
	private boolean linReceive(int id, int data[], int len, Vector<Ergebnis> ergebnisListe, Boolean copyResultOnSuccess, XREF<Integer> status) {
		if ((id < 0x00) || (id > 0xff)) {
			System.out.println(jobName+"::linSend(): wrong id 0x00<"+id+"<0xff");
			return false;
		}
		if ((len < 0x0000) || (len > 0xffff)) {
			System.out.println(jobName+"::linSend(): wrong len 0x0000<"+len+"<0xffff");
			return false;
		}
		if (data.length != len) {
			System.out.println(jobName+"::linSend(): wrong data length "+data.length+"!="+len);
			return false;
		}
		for(int i=0; i<data.length; i++) {
			data[i] = 0;
		}

		HashMap<String, Object> parameter = new HashMap<String, Object>();
		parameter.clear();
		parameter.put("Interface", linInterface);
		parameter.put("Id", id);
		parameter.put("Length", len);
		parameter.put("Timeout", 20);

		JobResult result = executeJob("AscotLinRequestFrame", parameter, ergebnisListe, copyResultOnSuccess, status);
		boolean success = result.getExecutionOk();

		if(success) {
			byte d[] = HexString.hexToArray(result.getReturnValueAsString());
			for(int i=0; i<d.length; i++) {
				data[i] = d[i];
			}
		}
		System.out.println(jobName+"::linReceive(id="+id+",data["+data.length+"]={"+HexString.intArrayToHex(data)+"},len="+len+") = "+success);
		return success;
	}

	/**
	 * Executes test procedure
	 * 
	 * @param jobName	job to execute
	 * @param parameter	job parameter
	 * @return		ASCOTJobResult
	 */
	private JobResult executeJob( String jobName, HashMap<String, Object> parameter ) {
		DynamicDeviceManager ddm = null;
		DynamicDevice ascotDevice = null;
		
		JobResult result = new JobResult();
		result.status = STATUS_EXECUTION_ERROR;

		if(debug) {
			addResultsErgebnis(result.ergebnisse, "JobName", jobName, Ergebnis.FT_IO);
		}

		HashMap<String, PropertyObject> jobParameter = new HashMap<String, PropertyObject>();
		for( String s : parameter.keySet() ) {
			if(s=="") continue;
			if(debug) {
				addResultsErgebnis(result.ergebnisse, "Parameter["+s+"]", parameter.get(s).toString(), Ergebnis.FT_IO);
			}
			try {
				if(checkAscotParameters) {
					Parameter par = null; 
					if(parameters.containsKey(s.toUpperCase())) {
						par = parameters.get(s.toUpperCase());
					}
					if(par==null)
						throw new Exception(String.format(bundle.getString("ParameterUnknown__sName"), s));
					if(par._ascotName=="")
						continue;
					jobParameter.put( par._ascotName, getObjectAsPropertyObject(parameter.get(s)) );
				} else {
					jobParameter.put( s, getObjectAsPropertyObject(parameter.get(s)) );
				}
			} catch ( Exception e ) {
				addResultsException( result.ergebnisse, e );
				return result;
			}
		}
		
		try {
			ddm = getPr�flingLaufzeitUmgebung().getDynamicDeviceManager();
			ascotDevice = ddm.requestDevice( Werkzeug, Werkzeug );
			if ( ascotDevice == null ) {
				throw new DeviceExecutionException ( bundle.getString("RequestDeviceFail") );
			}
		} catch (Exception e) {
			addResultsException( result.ergebnisse, e );
			return result;
		}

		Set<String> methods = ascotDevice.getMethodNames();
		if(!methods.contains("StartJob") 
		|| !methods.contains("GetJobStatus") 
		|| !methods.contains("GetJobResults") 
		|| !methods.contains("GetJobValues")
		|| !methods.contains("CancelJob")) {
			addResultsError( result.ergebnisse, bundle.getString("GetMethodNamesFail") );
			return result;
		}
		
		try {
			int jobToken = 0;
			int jobStatus = 0;
			List<PropertyObject> jobResult;

			// StartJob
			long stopTime = System.currentTimeMillis() + Math.round(timeout*1000);
			jobResult  = ascotDevice.execute( "StartJob", new PropertyObject(jobName), new PropertyObject(jobParameter) );
			jobToken = jobResult.get(0).asInt();

			do { // GetJobStatus
				Thread.sleep(1);
				jobResult = ascotDevice.execute( "GetJobStatus", new PropertyObject(jobToken) );
				jobStatus = jobResult.get(0).asInt();

				if(userDialog!=null && userDialog.isCancelled()) {
					jobStatus = JobStatus.Canceled;
					ascotDevice.execute("CancelJob", new PropertyObject(jobToken), new PropertyObject(jobCancelWaitTime));
					break;
				}
				if( (timeout > 0) && (System.currentTimeMillis() > stopTime) ) {
					jobStatus = JobStatus.Timeout;
					ascotDevice.execute("CancelJob", new PropertyObject(jobToken), new PropertyObject(jobCancelWaitTime));
					break;
				}
			} while( jobStatus == JobStatus.Running );
			
			switch( jobStatus ) {
				case JobStatus.Succeeded:
				case JobStatus.Failed:
//						addResultsErgebnis( result.ergebnisse, bundle.getString("Result"), 
//								jobStatus==JobStatus.Succeeded?bundle.getString("Success"):bundle.getString("Fail"), 
//								jobStatus==JobStatus.Succeeded?Ergebnis.FT_IO:Ergebnis.FT_NIO );

						// GetJobResult
						jobResult = ascotDevice.execute( "GetJobResults", new PropertyObject(jobToken) );
						if( jobResult != null ) {
							for( PropertyObject po : jobResult ) {
								switch( po.getType() ) {
								case BOOLEAN:
									addResultsErgebnis( result.ergebnisse, bundle.getString("Result"), ((Boolean)(po.asBoolean())).toString(), Ergebnis.FT_IO );
									break;
								case INTEGER:
									addResultsErgebnis( result.ergebnisse, bundle.getString("Result"), ((Integer)(po.asInt())).toString(), Ergebnis.FT_IO );
									break;
								case DOUBLE:
									addResultsErgebnis( result.ergebnisse, bundle.getString("Result"), ((Double)(po.asDouble())).toString(), Ergebnis.FT_IO );
									break;
								case STRING:
									addResultsErgebnis( result.ergebnisse, bundle.getString("Result"), ((String)(po.asString())).toString(), Ergebnis.FT_IO );
									break;
								case MAP:
									Map<String, PropertyObject> map = po.asMap();
									Ergebnis ergebnis = new Ergebnis("", Werkzeug, "", "", "", "", "", "", "", "", "", "", "", "", "", Ergebnis.FT_IO ); 
		
									String unit = "";
									if(map.containsKey("Unit")) 			unit			=" ["+map.get("Unit").asString()+"]";
									Integer precision = 3;
									if(map.containsKey("Precision"))		precision		= Math.max(map.get("Precision").asInt(), precision);
									String desiredValue = "";
									if(map.containsKey("DesiredValue"))		desiredValue	= map.get("DesiredValue").asString();
		
									if(map.containsKey("Name"))				ergebnis.setErgebnis(map.get("Name").asString()+unit);
									if(map.containsKey("Repetitions"))		ergebnis.setAnzWiederholungen(((Integer)map.get("Repetitions").asInt()).toString());
									if(map.containsKey("InfoText"))			ergebnis.setHinweisText(map.get("InfoText").asString());
									if(map.containsKey("Value")) {
										PropertyObject value = map.get("Value");
										if(value.isString()) {
											ergebnis.setErgebnisWert(map.get("Value").asString());
										} else
										if(value.getType() == PropertyObject.PropertyDataType.DOUBLE) {
											Double v = value.asDouble(); 
											ergebnis.setErgebnisWert(String.format((Locale)null, "%."+precision+"f", v));
										}
									}
									if(map.containsKey("MinValue"))			ergebnis.setMinWert(((Double)map.get("MinValue").asDouble()).toString());
									if(map.containsKey("MaxValue"))			ergebnis.setMaxWert(((Double)map.get("MaxValue").asDouble()).toString());
									if(map.containsKey("ResultType"))		ergebnis.setFehlerTyp(map.get("ResultType").asString());
									else									ergebnis.setFehlerTyp(jobStatus==JobStatus.Succeeded?Ergebnis.FT_IO:Ergebnis.FT_NIO);
									if(map.containsKey("ErrorMessage"))		ergebnis.setFehlerText(map.get("ErrorMessage").asString()
																			+(((ergebnis.getFehlerTyp()!=Ergebnis.FT_IO) && (component!=""))?(" : "+component):("")));
									result.ergebnisse.add(ergebnis);
//									if(debug) {
//										System.out.println(String.format("%s.%s Return: %s=%s [%s,%s] %s %s -> %s (%s)",
//												this.getPr�fling().getName(), this.getPPName(),
//												ergebnis.getErgebnis(),
//												ergebnis.getErgebnisWert(),
//												ergebnis.getMinWert(), ergebnis.getMaxWert(),
//												ergebnis.getFehlerTyp(),
//												ergebnis.getAnzWiederholungen(),
//												ergebnis.getFehlerText(), ergebnis.getHinweisText()
//										));
//									}
									break;
								case LIST:
									List<PropertyObject> list = po.asList();
									for(PropertyObject lpo : list) {
										addResultsErgebnis(result.ergebnisse, bundle.getString("Result")+"{}", lpo.getValue(), Ergebnis.FT_IO);
									}
									break;
								default:
									throw new Exception("ResultsValue: unknown data type "+po.getType().toString());
								}//switch(Type)
							}//for(jobResults)
						}//if(jobResult)
					
						// GetJobValues
						jobResult = ascotDevice.execute("GetJobValues", new PropertyObject(jobToken));
						if(jobResult != null) {
							for(PropertyObject po : jobResult) {
								Object jobValues = getPropertyObjectAsObject(po);
		
								if (jobValues instanceof Map) {
									Map<String, PropertyObject> map = po.asMap();
									for(String key : map.keySet()) {
										PropertyObject mpo = map.get(key);  
										if(debug) {
											addResultsErgebnis(result.ergebnisse, bundle.getString("Result")+"["+key+"]", mpo.getValue(), Ergebnis.FT_IO);
										}
										if(key=="~") {
											addResultsErgebnis(result.ergebnisse, bundle.getString("Result"), mpo.getValue(), Ergebnis.FT_IO);
										}
										result.jobResults.put(key, getPropertyObjectAsObject(mpo));
									}
								}
								else if (jobValues instanceof List) {
									List<PropertyObject> list = po.asList();
									for(PropertyObject lpo : list) {
										if(debug) {
											addResultsErgebnis(result.ergebnisse, bundle.getString("Result")+"{}", lpo.getValue(), Ergebnis.FT_IO);
										}
									}
								}
								else {
									if(debug) {
										addResultsErgebnis(result.ergebnisse, bundle.getString("Result"), jobValues.toString(), Ergebnis.FT_IO);
									}
								}//if(Type)
							}//for(jobResult)
						}//if(jobResult)
		
						result.status = ((jobStatus==JobStatus.Succeeded)?(STATUS_EXECUTION_OK):(STATUS_EXECUTION_ERROR));
					break;
				case JobStatus.Canceled:
						addResultsErgebnis(result.ergebnisse, bundle.getString("Fail"), bundle.getString("UserDialogCancel"), Ergebnis.FT_NIO);
						result.status = STATUS_EXECUTION_ERROR;
					break;
				case JobStatus.Timeout:
						addResultsError(result.ergebnisse, bundle.getString("Timeout")+" ["+timeout+" s]" );
						result.status = STATUS_EXECUTION_ERROR;
					break;
				default:
						addResultsError(result.ergebnisse, bundle.getString("UnknownStatus")+" ["+jobStatus+"]" );
						result.status = STATUS_EXECUTION_ERROR;
					break;
			} //switch(jobStatus)
		} catch (Exception e) {
			addResultsException(result.ergebnisse, e);
			result.status = STATUS_EXECUTION_ERROR;
			return result;
		}
		return result;
	}

	/**
	 * Executes test procedure
	 * 
	 * @param jobName				job to execute
	 * @param parameter				job parameter
	 * @param ergebnisListe			reference to result set to add results
	 * @param copyResultOnSuccess	should results be added if job succeeds
	 * @param status				reference to status
	 * @return					ASCOTJobResult
	 */
	private JobResult executeJob(String jobName, HashMap<String, Object> parameter, Vector<Ergebnis> ergebnisListe, Boolean copyResultOnSuccess, XREF<Integer> status) {
		JobResult result = executeJob(jobName, parameter);

		if((!result.getExecutionOk()) || copyResultOnSuccess) {
			result.copyJobErgebnisse(ergebnisListe);
		}
		if((!result.getExecutionOk())) {
			status.setValue(result.status);
		}
		return result;
	}

	/** 
     * export parameter
     * 
     * @param cascadeName  Name of parameter in CASCADE context
     * @param ascotName  Name of parameter in ASCOT context (@note this parameter is not use anymore!)
     * @param type  Type of parameter cf. AbstractBMWPruefprozedur$ParameterType
     * @param value Value of parameter if null parameter is required else given value is default value and can be overridden by CASCADE
     */
	private HashMap<String, Parameter> parameters = new HashMap<String, Parameter>();
	private void declareParameter(String cascadeName, String ascotName, byte type, Object value) {
		parameters.put(cascadeName.toUpperCase(), new Parameter(cascadeName, ascotName, new ParameterType(type), value));
	}
	private void exportParameter(String cascadeName, String ascotName, byte type, Object value) {
		parameters.put(cascadeName.toUpperCase(), new Parameter(cascadeName, cascadeName, new ParameterType(type), value));
	}
	private Object getParameter(String name) throws PPExecutionException {
		String arg = name.toUpperCase();
		if(parameters.containsKey(arg)) {
			Parameter par = parameters.get(arg);
			return par.getValue();
		}
		throw new PPExecutionException(PB.getString("parametrierfehler")+": "+String.format(bundle.getString("ParameterUnknown__sName"), name));
	}

	/**
	 * Checks if parameters can be parses as their given type 
	 * @return	true if all parameters can be parsed else false
	 * @throws PPExecutionException
	 */
	private boolean parseArgs() throws PPExecutionException {
		boolean ok=true;

		for(Object o : getArgs().keySet()) { // all given parameters
			if (debug) {
				System.out.println("\t" + o + "=" + getArgs().get(o));
			}
		}

		for (String param : getRequiredArgs()) {
			if(!parameters.containsKey(param.toUpperCase()))
				throw new PPExecutionException(PB.getString("parametrierfehler") + ": "
						+ String.format(bundle.getString("ParameterIsNotDeclared__sName"), param));
		}
		for (String param : getOptionalArgs()) {
			if(!parameters.containsKey(param.toUpperCase()))
				throw new PPExecutionException(PB.getString("parametrierfehler") + ": "
						+ String.format(bundle.getString("ParameterIsNotDeclared__sName"), param));			
		}

		for(String param : parameters.keySet()) {
			boolean found = false;
			for(String s : getRequiredArgs()) {
				if(param.toUpperCase().compareTo(s.toUpperCase())==0) {
					found = true;
					break;
				}
			}
			for(String s : getOptionalArgs()) {
				if(param.toUpperCase().compareTo(s.toUpperCase())==0) {
					found = true;
					break;
				}
			}
			if(found==false) {
				throw new PPExecutionException(PB.getString("parametrierfehler") + ": "
						+ String.format(bundle.getString("ParameterIsNotListedAsCascadeArg__sName"), param));				
			}
		}
		
		String dump = String.format("%s.%s Parameter = {\n", this.getPr�fling().getName(), this.getName());

		int req = 0;
		int opt = 0;
		int def = 0;

		for(String param : parameters.keySet()) {
			Parameter par = parameters.get(param.toUpperCase());
			par.parseArgument();
			boolean io=par.checkValue();
			if (io == false) {
				throw new PPExecutionException(PB.getString("parametrierfehler") + ": " 
						+ String.format(bundle.getString("ParameterCannotBeParsed__sName_sType_sValue"), param, par.getType(), par.getArgument()));
			}
			dump += String.format("\t%s\n", par);
			ok &= io;
			switch(par.getFlag()) {
			case 'r':req++; break;
			case 'o':opt++; break;
			case 'd':def++; break;
			}
		}
		dump += "}";
		if (debug) {
			System.out.println(dump + "\trequired[" + req + "], optional[" + opt + "/" + (opt + def) + "]");
		}
		return ok;
	}

	/**
	 * converts PropertyObject to String
	 * 
	 * @param po				PropertyObject
	 * @return					String representation of PropertyObject
	 */
	private String getPropertyObjectAsString(PropertyObject po) {
		try {
			return getPropertyObjectAsObject(po).toString();
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	/**
	 * converts PropertyObject to Object of its type
	 * 
	 * @param po				PropertyObject
	 * @return					Object of PropertyObject
	 * @throws Exception
	 */
	private Object getPropertyObjectAsObject(PropertyObject po) throws Exception {
		switch(po.getType()) {
		case BOOLEAN:
			return po.asBoolean();
		case INTEGER:
			return po.asInt();
		case DOUBLE:
			return po.asDouble();
		case STRING:
			return po.asString();
		case MAP:
			return po.asMap();
		case LIST:
			return po.asList();
		default:
			throw new Exception("getPropertyObjectAsObject: unknown data type "+po.getType().toString());
		}
	}
	/**
	 * converts PropertyObject to Object of its type
	 * 
	 * @param 	o				Object
	 * @return					PropertyObject
	 * @throws Exception
	 */
	private PropertyObject getObjectAsPropertyObject(Object o) throws Exception {
		if(o==null) throw new Exception("getObjectAsPropertyObject: Object is null");
		if(o instanceof Boolean)
			return new PropertyObject((Boolean)o);
		if(o instanceof Long)
			return new PropertyObject(((Long)o).intValue());
		if(o instanceof Integer)
			return new PropertyObject((Integer)o);
		if(o instanceof Double)
			return new PropertyObject((Double)o);
		if(o instanceof Float)
			return new PropertyObject((Double)o);
		if(o instanceof String)
			return new PropertyObject((String)o);
		throw new Exception("getObjectAsPropertyObject: unknown data type "+o.getClass().getName()+"="+o.toString());
	}
	
	/**
	 * add result to result set
	 * 
	 * @param	ergebnisListe		reference to result set
	 * @param	name				result name
	 * @param	value				result value
	 * @param	typ					result type
	 */
	private void addResultsErgebnis(Vector<Ergebnis> ergebnisListe, String name, String value, String typ) {
		addResultsErgebnis(ergebnisListe, name, value, "", typ);
	}
	/**
	 * add result to result set
	 * 
	 * @param	ergebnisListe		reference to result set
	 * @param	name				result name
	 * @param	value				result value
	 * @param	typ					result type
	 */
	private void addResultsErgebnis(Vector<Ergebnis> ergebnisListe, String name, String value, String hinweisText, String typ) {
		ergebnisListe.add(new Ergebnis(
				"",									// ID
				Werkzeug,							// werkzeug
				"",									// parameter_1 
				"",									// parameter_2
				"",									// parameter_3
				name,								// ergebnis
				value,								// ergebnisWert
				"",									// minWert
				"",									// maxWert
				"",									// wiederholungen
				"",									// frageText
				"",									// antwortText
				"",									// anweisText
				"",									// fehlerText
				hinweisText,						// hinweisText
				typ									// fehlerTyp
			)
		);
	}
	/**
	 * add result to result set
	 * 
	 * @param	ergebnisListe		reference to result set
	 * @param	name				result name
	 * @param	value				result value
	 * @param	typ					result type
	 */
	private void addResultsErgebnisse(Vector<Ergebnis> ergebnisListe, String name, Vector<Ergebnis> source) {
		for(int n = 0; n < source.size(); ++n) {
			Ergebnis src = source.get(n);
		
			ergebnisListe.add(new Ergebnis(
					"",									// ID
					Werkzeug,							// werkzeug
					src.getParameter1(),				// parameter_1 
					src.getParameter2(),				// parameter_2
					src.getParameter3(),				// parameter_3
					name,								// ergebnis
					src.getErgebnisWert(),				// ergebnisWert
					src.getMinWert(),					// minWert
					src.getMaxWert(),					// maxWert
					src.getAnzWiederholungen(),			// wiederholungen
					src.getFrageText(),					// frageText
					src.getAntwortText(),				// antwortText
					src.getAnweisText(),				// anweisText
					src.getFehlerText(),				// fehlerText
					src.getHinweisText(),				// hinweisText
					src.getFehlerTyp()					// fehlerTyp
				)
			);
		}
	}
	/**
	 * add debug info of an object to result set
	 * 
	 * @param	ergebnisListe		reference to result set
	 * @param	name				local name of object
	 * @param	object				reference to object
	 */
	private void addResultsDebugInfo(Vector<Ergebnis> ergebnisListe, String name, Object object) {
		addResultsDebugInfo(ergebnisListe, name+"["+object.getClass().getName()+"]="+object.toString());
	}
	/**
	 * add debug info string to result set
	 * 
	 * @param	ergebnisListe		reference to result set
	 * @param	message				text message
	 */
	private void addResultsDebugInfo(Vector<Ergebnis> ergebnisListe, String message) {
		Exception e = new Exception();
		String pos = "";
		StackTraceElement[] stack = e.getStackTrace();
		for(int i=0; i<stack.length; i++) {
			String entry =stack[i].toString();
			if(entry.contains("addResultsDebugInfo(")) continue;
			
			pos = entry;
			break;
		}
		
		ergebnisListe.add(new Ergebnis(
				"",						// ID
				Werkzeug,				// werkzeug
				"",						// parameter_1 
				"",						// parameter_2
				"",						// parameter_3
				"DEBUG INFO",			// ergebnis
				message,				// ergebnisWert
				"",						// minWert
				"",						// maxWert
				"",						// wiederholungen
				"",						// frageText
				"",						// antwortText
				"",						// anweisText
				"",						// fehlerText
				pos,					// hinweisText
				Ergebnis.FT_IO			// fehlerTyp
			)
		);
	}
	/**
	 * add error message to result set
	 * 
	 * @param	ergebnisListe		reference to result set
	 * @param	message				text message
	 */
	private void addResultsError(Vector<Ergebnis> ergebnisListe, String message) {
		Exception e = new Exception();
		String pos = "";
		StackTraceElement[] stack = e.getStackTrace();
		for(int i=0; i<stack.length; i++) {
			String entry =stack[i].toString();
			if(entry.contains("addResultsError(")) continue;
			
			pos = entry;
			break;
		}

		ergebnisListe.add(new Ergebnis(
				"",									// ID
				Werkzeug,							// werkzeug
				"",									// parameter_1 
				"",									// parameter_2
				"",									// parameter_3
				"ERROR",							// ergebnis
				"",									// ergebnisWert
				"",									// minWert
				"",									// maxWert
				"",									// wiederholungen
				"",									// frageText
				"",									// antwortText
				"",									// anweisText
				message,							// fehlerText
				pos,								// hinweisText
				Ergebnis.FT_NIO						// fehlerTyp
			)
		);
	}
	private void addResultsSystemError(Vector<Ergebnis> ergebnisListe, String message) {
		Exception e = new Exception();
		String pos = "";
		StackTraceElement[] stack = e.getStackTrace();
		for(int i=0; i<stack.length; i++) {
			String entry =stack[i].toString();
			if(entry.contains("addResultsError(")) continue;
			
			pos = entry;
			break;
		}

		ergebnisListe.add(new Ergebnis(
				"",									// ID
				Werkzeug,							// werkzeug
				"",									// parameter_1 
				"",									// parameter_2
				"",									// parameter_3
				"ERROR",							// ergebnis
				"",									// ergebnisWert
				"",									// minWert
				"",									// maxWert
				"",									// wiederholungen
				"",									// frageText
				"",									// antwortText
				"",									// anweisText
				message,							// fehlerText
				pos,								// hinweisText
				Ergebnis.FT_NIO_SYS					// fehlerTyp
			)
		);
	}
	/**
	 * add debug info of exception to result set
	 * 
	 * @param	ergebnisListe		reference to result set
	 * @param	exception			reference to exception
	 */
	private void addResultsException(Vector<Ergebnis> ergebnisListe, Throwable exception) {
		ergebnisListe.add(new Ergebnis(
				"",									// ID
				Werkzeug,							// werkzeug
				"",									// parameter_1 
				"",									// parameter_2
				"",									// parameter_3
				"Exception",						// ergebnis
				exception.getClass().getName(),		// ergebnisWert
				"",									// minWert
				"",									// maxWert
				"",									// wiederholungen
				"",									// frageText
				"",									// antwortText
				"",									// anweisText
				exception.getMessage(),				// fehlerText
				getStackTrace(exception),			// hinweisText
				Ergebnis.FT_NIO_SYS					// fehlerTyp
			)
		);
	}

	/**
	 * get stack trace as string
	 * 
	 * @param	exception
	 * @return	string representation of stack trace
	 */
	private static String getStackTrace(Throwable exception) {
		StringBuilder sb = new StringBuilder();
		for (StackTraceElement element : exception.getStackTrace()) {
			sb.append("\n");
			sb.append(element.toString());
		}
		sb.append("\n");
		return sb.toString();
	}
	/**
	 * get line number of calling line
	 *
	 * @return string with class name calling function name and line number
	 */
	private static String getCurrentLine () {
        StackTraceElement ste = Thread.currentThread().getStackTrace()[2];
        String where = ste.getClassName() + "::" + ste.getMethodName() + "(" + ste.getLineNumber() + ")";
        return where;
    }

	private String statusAsString(int status) {
		switch(status) {
		case -1:
			/** The element is undefined */
			return "STATUS_UNDEFINED";// = -1;
		case 0:
			/** The element has not been executed */
			return "STATUS_NOT_EXECUTED";// = 0;
		case 1:
			/** The element is being executed */
			return "STATUS_RUNNING";// = 1;
		case 2:
			/** The element has been executed and was OK */
			return "STATUS_EXECUTION_OK";// = 2;
		case 3:
			/** The element has been executed and was not OK */
			return "STATUS_EXECUTION_ERROR";// = 3;
		case 4:
			/** The element execution was cancelled by a user */
			return "STATUS_EXECUTION_ABORTED_BY_USER";// = 4;
		case 5:
			/** The element has been executed more than once (retried) */
			return "STATUS_EXECUTION_RETRY";// = 5;
		case 6:
			/** The element has been executed and was not OK but the error will be ignored (eg. OK_ON_ERROR) */
			return "STATUS_EXECUTION_ERROR_IGNORE";// = 6;
		case 100:
			/** The element is disabled */
			return "STATUS_DISABLED";// = 100;
		default:
			return "unkown";
		}
	}

	private Boolean False = false;
	private Boolean True  = true;

	protected class ParameterType {
		static final long serialVersionUID = 1L;

		public static final byte String = 0;
		public static final byte Boolean = 1;
		public static final byte Long = 2;
		public static final byte Double = 3;
		public static final byte None = (byte)0xff;
		

		public byte value;
		public ParameterType() {
			value = None;
		}
		public ParameterType(byte b) {
			this();
			value = b;
		}
		
		public String toString() {
			switch(value) {
			case ParameterType.Boolean:
				return "Boolean";
			case ParameterType.Long:
				return "Integer";
			case ParameterType.Double:
				return "Double";
			case ParameterType.String:
				return "String";
			case ParameterType.None:
				return "None";
			default:
				return "unknown";
			}
		}
	}
	protected class Parameter {
		static final long serialVersionUID = 1L;

		private String _cascadeName;
		private String _ascotName;
		private ParameterType _type;
		private Object _default;
		private Object _value;
		private String _argument;
		private Boolean _parsed;
		
		public Parameter(String cascadeName, String ascotName, ParameterType type, Object defaultValue) {
			_cascadeName	= cascadeName;
			_ascotName 		= ascotName;
			_type 			= type;
			_default		= defaultValue;
			_value			= _default;
			_argument		= "";
			_parsed			= false;
		}
		
		public String			getName() 		{ return _cascadeName; }
		public String			getArgument() 	{ return _argument; }
		public String 			getAscotName() 	{ return _ascotName; }
		public ParameterType	getType() 		{ return _type; }
		public Object 			getDefault() 	{ return _default; }
		public Object getValue() throws PPExecutionException {
			if(_parsed) return _value;
			parseArgument();
			return _value;
		}
		
		private Object getStringAsObject(String s) throws PPExecutionException {
			try {
				switch(_type.value) {
				case ParameterType.Boolean:
					try {
						if(s.startsWith("T") || s.startsWith("Y") || s.startsWith("J")) {
							_value=true;
						} else
						if(Boolean.parseBoolean(s)==true) {
							_value=true;
						} else
						if(Double.parseDouble(s)>0)	{
							_value=true;
						}
					} catch (Exception e) {
						_value=false;
					}
					return _value;
				case ParameterType.Long:
					return Integer.parseInt(s);
				case ParameterType.Double:
					return Double.parseDouble(s);
				case ParameterType.String:
					return s.trim();
				default:
					throw new PPExecutionException(PB.getString("parametrierfehler")+": "+String.format(bundle.getString("ParameterTypeUnknown__sName_sType"), _cascadeName, _type));
				}
			} catch(Exception e) {
				throw new PPExecutionException(PB.getString("parametrierfehler")+": "+String.format(bundle.getString("ParameterCannotBeParsed__sName_sType_sValue"), _cascadeName, _type, s));
			}
		}
		
		private String readArgument() {
			_argument = getArg(_cascadeName.toUpperCase());
			if(_argument!=null) { 
				_argument = _argument.trim();
			}
			return _argument;
		}
		public boolean parseArgument() throws PPExecutionException {
			if(_parsed) return _parsed; // already done;
			
			if(readArgument()!=null) {
				_value = getStringAsObject(_argument);
			} else {
				_value = _default;
			}
			_parsed=true;
			return _parsed;
		}

		public boolean checkValue() throws PPExecutionException {
			return parseArgument();
		}
		
		public boolean hasDefault() {
			return _default!=null;
		}
		
		public boolean isRequired() {
			return !isOptional();
		}
		public boolean isOptional() {
			return hasDefault();
		}
		public boolean isGiven() {
			Object arg = getArg(_cascadeName.toUpperCase());
			return arg!=null;
		}
		
		private char getFlag() {
			if(isRequired())	return 'r';
			if(isGiven())		return 'o';
			return 'd';
		}
		
		public String toString() {
			try {
//				return String.format("[%c]%-40.40s = %-40.40s [%-7.7s](%-40.40s)-> %-40.40s  [%2.2s]", getFlag(), _cascadeName, _argument, _type, _default, _value, (checkValue() ? "OK" : "!!"));
				return String.format("[%c]%-40.40s = %-40.40s [%-7.7s]-> %-40.40s  [%2.2s]", getFlag(), _cascadeName, _argument, _type, _value, (checkValue() ? "OK" : "!!"));
			} catch (Exception e) {
				return String.format("%s[%s]=%s", _cascadeName, _type, _default);
			}
		}
	}
	
	protected class JobStatus {
		public static final byte Running = 0;
		public static final byte Succeeded = 1;
		public static final byte Failed = 2;
		public static final byte Unknown = 3;
		public static final byte Canceled = (byte)0xfe;
		public static final byte Timeout = (byte)0xff;
		
		public byte status;
		public JobStatus() {
			status = 3;
		}
		public JobStatus(byte s) {
			status = s;
		}
		
		public String toString() {
			switch(status) {
			case Running:
				return "Running";
			case Succeeded:
				return "Succeeded";
			case Failed:
				return "Failed";
			default:
			case Unknown:
				return "Unknown";
			case Canceled:
				return "Canceled";
			case Timeout:
				return "Timeout";
			}
		}
	}
	protected class JobResult {
		static final long serialVersionUID = 1L;
		public JobResult(){
			ergebnisse = new Vector<Ergebnis>();
			jobResults = new HashMap<String, Object>();
		}
		
		public Vector<Ergebnis> ergebnisse;
		public JobStatus jobStatus;
		public HashMap<String, Object> jobResults;
		public int status;

		public String toString() {
			String sErgebnisse = "{\n";
			for(Ergebnis erg : ergebnisse) {
				sErgebnisse += erg+"\n\n";
			}
			sErgebnisse += "}";
			String sJobResults = "{\n";
			for(String key : jobResults.keySet()) {
				sJobResults += "\t"+key+"="+jobResults.get(key)+"\n";
			}
			sJobResults += "}";
			return String.format("{ jobStatus=%s, jobResults=%s,\nergebnisse=%s,\n status=%d }",
					jobStatus, sJobResults, sErgebnisse, status);
		}
		
		public Boolean getExecutionOk() {
			return status == AbstractBMWPruefprozedur.STATUS_EXECUTION_OK;
		}
		
		public void copyJobErgebnisse(Vector<Ergebnis> target)
		{
			for(int e = 0; e < ergebnisse.size(); ++e)
				target.add(ergebnisse.get(e));
		}
		
		public Boolean getReturnParameterAsBoolean(String name) {
			return (Boolean) this.jobResults.get(name);
		}
		public Long getReturnParameterAsLong(String name) {
			return (Long) this.jobResults.get(name);
		}
		public Integer getReturnParameterAsInteger(String name) {
			//return getReturnParameterAsLong(name).intValue();
			return (Integer) this.jobResults.get(name);
		}
		public Double getReturnParameterAsDouble(String name) {
			return (Double) this.jobResults.get(name);
		}
		public String getReturnParameterAsString(String name) {
			return (String) this.jobResults.get(name);
		}

		public Boolean getReturnValueAsBoolean() {
			return getReturnParameterAsBoolean("~");
		}
		public Long getReturnValueAsLong() {
			return getReturnParameterAsLong("~");
		}
		public Integer getReturnValueAsInteger() {
			return getReturnParameterAsInteger("~");
		}
		public Double getReturnValueAsDouble() {
			return getReturnParameterAsDouble("~");
		}
		public String getReturnValueAsString() {
			return getReturnParameterAsString("~");
		}
	}

	protected class XREF<T> {
	       public XOBJ<T> Obj = null;
	       public XREF(T value) 
	       {
	           Obj = new XOBJ<T>(value);
	       }
	       public XREF() 
	       {
	         Obj = new XOBJ<T>();
	       }  
	       public XOUT<T> Out()
	       {
	           return(Obj.Out());       
	       }
	       public XREF<T> Ref()
	       {
	           return(this);
	       }
	       
	       public T getValue() {
	    	   return Obj.Value;
	       }
	       public void setValue(T value) {
	    	   Obj.Value = value;
	       }
	};
	protected class XOUT<T> {
	     public XOBJ<T> Obj = null;
	     public XOUT(T value) 
	     {
	         Obj = new XOBJ<T>(value);
	     }
	     public XOUT() 
	     {
	       Obj = new XOBJ<T>();
	     }  
	     public XOUT<T> Out()
	     {
	         return(this);           
	     }
	     public XREF<T> Ref()
	     {
	         return(Obj.Ref());       
	     }

	     public T getValue() {
	  	   return Obj.Value;
	     }
	     public void setValue(T value) {
	  	   Obj.Value = value;
	     }
	};
	protected class XOBJ<T> {

		  public T Value;

		  public  XOBJ() {

		  }    
		  public XOBJ(T value) {
		      this.Value = value;
		  }
		  //
		  // Method: Ref()
		  // Purpose: returns a Reference Parameter object using the XOBJ value
		  //    
		  public XREF<T> Ref()
		  {
		      XREF<T> ref = new XREF<T>();
		      ref.Obj = this;
		      return(ref);
		  }
		  //
		  // Method: Out()
		  // Purpose: returns an Out Parameter Object using the XOBJ value
		  //
		  public XOUT<T> Out()
		  {
		      XOUT<T> out = new XOUT<T>();
		      out.Obj = this;
		      return(out);
		  }    
		  //
		  // Method get()
		  // Purpose: returns the value 
		  // Note: Because this is combersome to edit in the code,
		  // the Value object has been made public
		  //    
		  public T get() {
		      return Value;
		  }
		  //
		  // Method get()
		  // Purpose: sets the value
		  // Note: Because this is combersome to edit in the code,
		  // the Value object has been made public
		  //
		  public void set(T anotherValue) {
		      Value = anotherValue;
		  }


		  @Override
		  public String toString() {
		      return Value.toString();
		  }

		  @Override
		  public boolean equals(Object obj) {
		      return Value.equals(obj);
		  }

		  @Override
		  public int hashCode() {
		      return Value.hashCode();
		  }
		}	

	protected static class HexString {
		//http://stackoverflow.com/questions/9655181/how-to-convert-a-byte-array-to-a-hex-string-in-java
		
		final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

		public static String bytesToHex(byte[] bytes) {
			char[] hexChars = new char[bytes.length * 2];
			for (int j = 0; j < bytes.length; j++) {
				int v = bytes[j] & 0xFF;
				hexChars[j * 2] = hexArray[v >>> 4];
				hexChars[j * 2 + 1] = hexArray[v & 0x0F];
			}
			return new String(hexChars);
		}

		public static String byteArrayToHex(byte[] a) {
			StringBuilder sb = new StringBuilder(a.length * 2);
			for (byte b : a)
				sb.append(String.format("%02x", b & 0xff));
			return sb.toString();
		}
		public static String intArrayToHex(int[] a) {
			StringBuilder sb = new StringBuilder(a.length * 2);
			for (int b : a)
				sb.append(String.format("%02x ", b & 0xff));
			return sb.toString().trim();
		}

		static final String HEXES = "0123456789ABCDEF";

		public static String getHex(byte[] raw) {
			if (raw == null) {
				return null;
			}
			final StringBuilder hex = new StringBuilder(2 * raw.length);
			for (final byte b : raw) {
				hex.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt((b & 0x0F)));
			}
			return hex.toString();
		}
		public static String getHex(int[] raw) {
			if (raw == null) {
				return null;
			}
			final StringBuilder hex = new StringBuilder(2 * raw.length);
			for (final int b : raw) {
				hex.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt((b & 0x0F)));
			}
			return hex.toString();
		}
		
		public static byte[] hexToArray(String hexString) {
			String[] hex = "ff ff 09 c4".split(" ");
			byte[] data = new byte[hex.length];
			
			for(int i=0; i<hex.length; i++) {
				int b = Integer.parseInt(hex[i],16);
				data[i]=(byte)(b&0xff);
			}
			return data;
		}

	}
	
	// f�r die �bersetzung
	private ResourceBundle bundle = ResourceBundle.getBundle( Resources.class.getName() );

	/**
	 * RessourceBundle default en / English
	 * @author Michael Scholler, remes GmbH
	 */
	public static class Resources extends ListResourceBundle {
		@Override
		protected Object[][] getContents() {
			return new Object[][] {
				// LOCALIZE THIS
				{ "Timeout", "Action takes to long"},
				{ "Exception", "Exception: "},
				{ "RequestDeviceFail", "Cannot get Device"},
				{ "RequestDeviceException", "Exception while getting ASCOT DynamicDevice: "},
				{ "GetMethodNamesFail", "Device doesn't contain all required methods"},
				{ "Result", "Result"},
				{ "Success", "Success"},
				{ "Fail", "Fail"},
				{ "UnknownStatus", "Unknown status"},
				{ "NoJobName", "You must privide a job name!" },
				{ "UserDialogOpenFail","Cannot show userdialog"},
				{ "UserDialogCloseFail","Cannot close userdialog"},
				{ "UserDialogCancel","test step canceled by user"},
				{ "WaitingForUserAction", "ASCOT: Waiting for action..."},
				{ "ParameterCannotBeParsed__sName_sType_sValue", "%s[%s] = \"%s\" cannot be parsed!"},
				{ "ParameterIsNotDeclared__sName", "%s is not declared as parameter!"},
				{ "ParameterIsNotListedAsCascadeArg__sName", "%s is not listed as required or optional argument!"},
				{ "ParameterUnknown__sName", "unknown parameter: %s"},
				{ "ParameterTypeUnknown__sName_sType", "unknown type for parameter: %s[%s]"},
				{ "UnableToPowerOn", "unable to power on"},
				{ "UnableToConnectLIN", "unable to connect to lin"},
				{ "UnableToSetupCyclicMessage", "unable to setup cyclic message"},
				{ "UnableToStartCyclicMessage", "unable to start cyclic message"},
				{ "UnableToRemoveCyclicMessage",  "unable to remove cyclic message"},
				{ "NotConnectedMesage", "not connected or defect" },
				// END OF MATERIAL TO LOCALIZE
			};
		}
	}

	/**
	 * RessourceBundle de / Deutsch
	 * @author Michael Scholler, remes GmbH
	 */
	public static class Resources_de extends ListResourceBundle {
		@Override
		protected Object[][] getContents() {
			return new Object[][] {
				// LOCALIZE THIS
				{ "Timeout", "Aktion dauert zu lange"},
				{ "Exception", "Ausnahme: "},
				{ "RequestDeviceFail", "Kann Device nicht holen"},
				{ "RequestDeviceException", "Exception w�hrend Anforderung des ASCOT DynamicDevice: "},
				{ "GetMethodNamesFail", "Devicemethoden fehlen"},
				{ "Result", "Ergebnis"},
				{ "Success", "Erfolgreich"},
				{ "Fail", "Fehlgeschlagen"},
				{ "UnknownStatus", "Unbekanter Status"},
				{ "NoJobName", "Sie m�ssen einen JobNamen angeben!" },
				{ "UserDialogOpenFail","Userdialog kann nicht angezeigt werden"},
				{ "UserDialogCloseFail","Userdialog kann nicht beendet werden"},
				{ "UserDialogCancel","Pr�fschritt von Benutzer abgebrochen"},
				{ "WaitingForUserAction", "ASCOT: Warte auf Benutzer-Interaktion..."},
				{ "ParameterCannotBeParsed__sName_sType_sValue", "%s[%s] = \"%s\" kann nicht verarbeitet werden!"},
				{ "ParameterIsNotDeclared__sName", "%s wird nicht als Parameter deklariert!"},
				{ "ParameterIsNotListedAsCascadeArg__sName", "%s wird nicht als ben�tigtes oder optionales Argument gelistet!"},
				{ "ParameterUnknown__sName", "Unbekannter Parameter: %s"},
				{ "ParameterTypeUnknown__sName_sType", "Unbekannter ParameterType: %s[s]"},
				{ "UnableToPowerOn", "Netzteil kann nicht aufgeschalten werden"},
				{ "UnableToConnectLIN", "LIN kann nicht aufgeschalten werden"},
				{ "UnableToSetupCyclicMessage", "Zyklische Nachricht kann nicht initialisiert werden"},
				{ "UnableToStartCyclicMessage", "Zyklischer Versand der Nachricht kann nicht gestartet werden"},
				{ "UnableToRemoveCyclicMessage",  "Zyklischer Versand der Nachricht kann nicht entfernt werden"},
				{ "NotConnectedMesage", "nicht verbunden oder defekt" },
				// END OF MATERIAL TO LOCALIZE
			};
		}
	}
}

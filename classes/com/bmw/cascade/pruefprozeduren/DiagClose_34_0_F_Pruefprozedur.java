/*
 * DiagClose_19_0_F_Pruefprozedur.java
 *
 * Created on 06.09.06
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import com.bmw.appframework.logging.LogLevel;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.DeviceInfo;
import com.bmw.cascade.pruefstand.devices.DeviceInfo.DeviceInfoDetail;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.ediabas.*;
import com.bmw.cascade.util.*;
import com.bmw.cascade.util.logging.CascadeLogging;

/**
 * Beendet die Diagnoseverbindung und gibt somit EDIABAS frei
 *
 * @author Winkler, Mueller, Schumann, Rettig
 * @version Implementierung <br>
 * 			21.08.2008 CS 20_0_T Erweiterung, sodass die Devices IFH- und API-Trace FGNR spezifisch zugeschalten werden k�nnen	
 *			21.10.2008 PR 20_1_T STATUS_EXECUTION_RETRY und STATUS_EXECUTION_IGNORE f�hren nun nicht mehr zum Speichern von API/IFH-Traces			
 *			30.10.2008 CS 22_0_T Generelles Anschalten von IFH/API-Trace und weiterhin FGNR spezifische Traces; Beinhaltet auch 21_0_T	
 *			04.11.2008 CS 23_0_T Konfiguration �ber Tag, da Port Konfiguration das Trace-Level angibt	
 *			27.11.2008 CS 24_0_T Konfiguration �ber Port, da in Tag beim Multiinstanzbetrieb die TestScreens zugeordnet werden
 *			21.01.2009 CS 25_0_F Release-Version von Version 24_0_T bevor das TraceFile erzeugt wird
 *			20.12.2010 CW 27_0_T 200ms sleep bevor das TraceFile erzeugt wird
 *			20.12.2010 CW 28_0_F Freigabe 27_0_T
 *			16.12.2011 CW 29_0_T EdiabasAPI- und IFH-Trace wird im n.i.O.-Fall aufgrund der Fehleranzahl des dar�ber liegenden Blockes 
 *								 und zus�tzlich aufgrund des Execution-Status des PUs erzeugt
 *			16.12.2011 CW 30_0_F Freigabe 29_0_T
 *			04.02.2015 MS 34_0_F Umstellung create Trace; Abfrage des PU-Status �ber http-Request
 */
public class DiagClose_34_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagClose_34_0_F_Pruefprozedur() {
	}

	/**
	   * erzeugt eine neue Pruefprozedur.
	   * @param pruefling Klasse des zugeh. Pr�flings
	   * @param pruefprozName Name der Pr�fprozedur
	   * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	   */
	public DiagClose_34_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "EXTERN" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = new String[0];
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	public boolean checkArgs() {
		try {
			return super.checkArgs();
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String temp = null;
		String tracePath = null;
		boolean extern = false;

		/***********************************************
		 * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
		 * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		 ***********************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		try {
			try {
				//Parameter holen
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				if( getArg( getOptionalArgs()[0] ) != null ) {
					if( getArg( getOptionalArgs()[0] ).equals( "TRUE" ) )
						extern = true;
				}

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			if( extern == false )
			//Initialisierung unter Interface-Bestimmung, wird nur ben�tigt, wenn EXTERN=FALSE, da sonst eh kein Funk angepackt wird
			{
				try {
					temp = Ediabas.executeDiagJob( "UTILITY", "INTERFACE", "", "" );
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", "UTILITY", "INTERFACE", "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					}
					temp = Ediabas.getDiagResultValue( "TYP" ).toUpperCase();
					result = new Ergebnis( "Interface", "EDIABAS", "UTILITY", "INTERFACE", "", "TYP", temp, temp, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				} catch( ApiCallFailedException e ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", "UTILITY", "INTERFACE", "", "TYP", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), Ediabas.apiErrorCode() + ": " + Ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				} catch( EdiabasResultNotFoundException e ) {
					if( e.getMessage() != null )
						result = new Ergebnis( "DiagFehler", "EDIABAS", "UTILITY", "INTERFACE", "", "TYP", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "DiagFehler", "EDIABAS", "UTILITY", "INTERFACE", "", "TYP", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}
			}

			// wird nur gemacht wenn FUNK vorhanden ist UND falls EXTERN=FALSE
			// notwendig um EDIABAS freizugeben (z.B. NFS wo Funk immer noch notwendig ist)
			if( extern == false ) {
				if( temp.equals( "FUNK" ) == true ) {
					// Satistik Zaehler im Master auslesen
					try {
						temp = Ediabas.executeDiagJob( "IFR", "STATUS_STATISTIK_MASTER", "", "" );
						if( temp.equals( "OKAY" ) == false ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "STATUS_STATISTIK_MASTER", "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_IGNORE );
							ergListe.add( result );
						} else {
							temp = Ediabas.getDiagResultValue( "STAT_ABBRUCH" );
							result = new Ergebnis( "Diagnose", "EDIABAS", "IFR", "STATUS_STATISTIK_MASTER", "", "STAT_ABBRUCH", temp, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
							temp = Ediabas.getDiagResultValue( "STAT_WIEDERHOLUNGEN" );
							result = new Ergebnis( "Diagnose", "EDIABAS", "IFR", "STATUS_STATISTIK_MASTER", "", "STAT_WIEDERHOLUNGEN", temp, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
							temp = Ediabas.getDiagResultValue( "STAT_VERBUNDENER_SLAVE" );
							result = new Ergebnis( "Diagnose", "EDIABAS", "IFR", "STATUS_STATISTIK_MASTER", "", "STAT_VERBUNDENER_SLAVE", temp, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						}
					} catch( ApiCallFailedException e ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "STATUS_STATISTIK_MASTER", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), Ediabas.apiErrorCode() + ": " + Ediabas.apiErrorText(), Ergebnis.FT_IGNORE );
						ergListe.add( result );
					} catch( EdiabasResultNotFoundException e ) {
						if( e.getMessage() != null )
							result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "STATUS_STATISTIK_MASTER", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_IGNORE );
						else
							result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "STATUS_STATISTIK_MASTER", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_IGNORE );
						ergListe.add( result );
					} catch( Exception e ) {
						CascadeLogging.getLogger().log( LogLevel.WARNING, "Funkverhau nicht in Ordnung!!! / Wrong version for radio adaptor software!!!", e.getMessage() );
						e.printStackTrace();
					} catch( Throwable t ) {
						CascadeLogging.getLogger().log( LogLevel.WARNING, "Funkverhau nicht in Ordnung!!! / Wrong version for radio adaptor software!!!", t.getMessage() );
						t.printStackTrace();
					}
					//Funk beenden
					try {
						temp = Ediabas.executeDiagJob( "IFR", "ENDE_PRUEFUNG", "", "" );
						if( temp.equals( "OKAY" ) == false ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "ENDE_PRUEFUNG", "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						}
					} catch( ApiCallFailedException e ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "ENDE_PRUEFUNG", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), Ediabas.apiErrorCode() + ": " + Ediabas.apiErrorText(), Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						throw new PPExecutionException();
					} catch( EdiabasResultNotFoundException e ) {
						if( e.getMessage() != null )
							result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "ENDE_PRUEFUNG", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
						else
							result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "ENDE_PRUEFUNG", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						throw new PPExecutionException();
					}
				} //Ende Funk

			}
		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Lese den Tracepath aus - wird ben�tigt, um die Traces bei N.i.o - Pr�fungen zu bearbeiten
		try {
			tracePath = Ediabas.apiGetConfig( "TracePath" );
		} catch( Exception e ) {
			CascadeLogging.getLogger().log( LogLevel.WARNING, PB.getString( " Exception during tracesave: " ), e.getMessage() );
		} catch( Throwable t ) {
			CascadeLogging.getLogger().log( LogLevel.WARNING, PB.getString( " Throwable during tracesave: " ), t.getMessage() );
		}

		try {
			Ediabas.apiEnd();
		} catch( NoClassDefFoundError e ) {
			result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Exception: EDIABAS DLL (apijav32.dll) ", "", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Exception: apiend()", e.toString(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
		}

		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "Close", "EDIABAS", "", "", "", "STATUS", "OK", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );

		} else {
			result = new Ergebnis( "Close", "EDIABAS", "", "", "", "STATUS", "NOK", "", "", "", "", "", "", "", "", Ergebnis.FT_NIO );
			ergListe.add( result );
		}

		// Bearbeitung der EDIABAS-Traces
		if( extern == false ) {
			try {
				// API-Trace?
				DeviceInfo apiTrace = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabasApiTrace();
				if( tracePath != null ) {
					try {
						processTrace( tracePath, apiTrace );
					} catch( Exception acfe ) {
						CascadeLogging.getLogger().log( LogLevel.WARNING, PB.getString( " Exception during tracesave: " ), acfe.getMessage() );

					}
				}
			} catch( Exception e ) {
				// keine Fehlerbehandlung, Trace war nicht aktiviert
			} catch( Throwable t ) {
				// keine Fehlerbehandlung
			}

			try {
				// IFH-Trace?
				DeviceInfo ifhTrace = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabasIfhTrace();
				if( tracePath != null )
					try {
						processTrace( tracePath, ifhTrace );
					} catch( Exception acfe ) {
						CascadeLogging.getLogger().log( LogLevel.WARNING, PB.getString( " Exception during tracesave: " ), acfe.getMessage() );
					}
			} catch( Exception e ) {
				// keine Fehlerbehandlung, Trace war nicht aktiviert
			} catch( Throwable t ) {
				// keine Fehlerbehandlung
			}
		}

		setPPStatus( info, status, ergListe );
	}

	/**
	 * �berpr�ft wie der API- oder IFH-Trace konfiguriert wurde und erstellt aufgrund der Konfiguration
	 * Trace-Dateien.
	 * Konfiguration:
	 * - verketteteter String in der Port Spalte gibt die Kongiguration an
	 * - erstes Element des verketteten Stings:	Trace-Level (1-5)
	 * - zweites Element des verketteten Strings: Traces immer erw�nscht konfigurieren mit ok, Traces nur im n.i.O. Fall erw�nscht mit nok
	 * - drittes Element des verketteten Strings: FGNR, einzeln oder im String durch ';' getrennt -> nur bei diesen FGNRs wird der Trace
	 * 	  generell angeschalten
	 *  - Port-Spalte leer -> f�r alle Fahrzeuge wird ein Trace erstellt, wenn die Pr�fung n.i.O. war
	 *  Beispielkonfiguration: 	3;ok -> Traces werden immer f�r alle Fahrzeuge erstellt mit Trace-Level 3
	 *  						3;AB12345;ED98765 -> Traces nur erstellen f�r FGNRs: AB12345 und ED98765
	 *  						3 -> Traces werden nur im n.i.O. Fall erstellt
	 * @param tracePath
	 * @param trace - Api- oder Ifh-Trace
	 */
	private void processTrace( String tracePath, DeviceInfo trace ) throws Exception {
		String configurationString = "";
		ArrayList configurationArray = null;

		try {
			//API oder IFH-Trace
			String traceFileName = "";
			if( trace.getName().equalsIgnoreCase( "EdiabasApiTrace" ) ) {
				traceFileName = "api.trc";
			} else if( trace.getName().equalsIgnoreCase( "EdiabasIfhTrace" ) ) {
				traceFileName = "ifh.trc";
			}
			File traceFile = new File( tracePath, traceFileName );

			//�berpr�fen, ob Konfiguration in der Port-Spalte angegeben wurde
			//und abspeichern der Konf. im configurationArray
			for( int i = 0; i < trace.getDetailCount(); i++ ) {
				DeviceInfoDetail detail = trace.getDetail( i );
				if( !detail.getPort().equalsIgnoreCase( "" ) ) {
					configurationString = detail.getPort();
					StringTokenizer st = new StringTokenizer( configurationString, ";" );
					configurationArray = new ArrayList( st.countTokens() );
					while( st.countTokens() > 0 ) {
						configurationArray.add( st.nextElement() );
					}
				}
			}

			String traceCreate = "nOK";
			String fgnr = "";

			//auslesen des configurationArray und zuordnen zu den entsprechenden
			//Variablen
			for( int i = 0; (configurationArray != null && i < configurationArray.size()); i++ ) {
				if( i == 1 ) {
					traceCreate = (String) configurationArray.get( i );
					break;
				}
			}

			//Abfrage des PU-Status
			boolean puStatus = httpRequestPUStatus();

			//es wird immer ein Trace-File erw�nscht
			if( traceCreate.equalsIgnoreCase( "OK" ) ) {
				System.out.println( "Trace-File wird immer kopiert" );
				createTrace( traceFile );
			}
			//wenn eine oder mehrere FGNRs konfiguriert wurden
			else if( (traceCreate.length() == getPr�fling().getAuftrag().getFahrgestellnummer7().length()) && configurationArray != null && configurationArray.size() > 1 ) {
				boolean traceNotCreated = true;
				for( int i = 1; i < configurationArray.size(); i++ ) {
					fgnr = (String) configurationArray.get( i );
					//nur Trace-File erstellen, wenn entsprechende FGNR gepr�ft wird
					if( fgnr.equalsIgnoreCase( getPr�fling().getAuftrag().getFahrgestellnummer7() ) ) {
						createTrace( traceFile );
						traceNotCreated = false;
						break;
					}
				}
				//falls Trace nicht erstellt wurde, muss es gel�scht werden
				if( traceNotCreated ) {
					if( traceFile.exists() )
						traceFile.delete();
				}

				//FGNR wurde nicht konfiguriert - Trace-File erstellen, wenn Pr�fung n.i.O.				
			} else if( (configurationString.equalsIgnoreCase( "" ) || configurationArray.size() == 1) && (puStatus || this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getLastExecStatus() == 3) ) {
				createTrace( traceFile );
				//Pr�fung war i.O. -> Trace-File wird gel�scht --> alter Weg
			} else {
				if( traceFile.exists() )
					traceFile.delete();
			}

		} catch( Exception acfe ) {
			CascadeLogging.getLogger().logC( LogLevel.WARNING, getPr�fling().getAuftrag().getFahrgestellnummer7() + " Exception API/IFH-Trace Level " + trace.getPort() + ": ", acfe.getMessage() );
		}
	}

	/**
	 * Schiebt das Trace-File in den Cascade/trace/pr�fstand/error Ordner.
	 * @param traceFile
	 */
	private void createTrace( File traceFile ) {
		try {			
			File saveFile = new File( com.bmw.cascade.pruefstand.PB.getString( "cascade.pruefstand.auftrag.reporterrorpath" ), this.getPr�fling().getAuftrag().getFahrgestellnummer7() + "_" + this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getName() + "_" + System.currentTimeMillis() + "_" + traceFile.getName().toUpperCase() );
			try {
				Thread.sleep( 100 );
			} catch( InterruptedException e1 ) {
				e1.printStackTrace();
			}
			FileUtils.moveFile( traceFile, saveFile );
		} catch( Exception e ) {
			e.printStackTrace();
		}

	}

	private boolean httpRequestPUStatus() throws IOException {
		try {
			String url = "http://localhost:8086/testscreen-" + (getPr�flingLaufzeitUmgebung().getNumber() + 1); //ist Multi-Instanz-f�hig

			URL obj = new URL( url );
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setConnectTimeout( 5000 ); //in ms
			con.setRequestMethod( "GET" ); //ist eigentlich Default 		

			BufferedReader in = new BufferedReader( new InputStreamReader( con.getInputStream() ) );
			String inputLine;
			StringBuffer response = new StringBuffer();
			while( (inputLine = in.readLine()) != null )
				response.append( inputLine );
			in.close();
			int firstIndex = response.toString().indexOf( "testState" ) + 11;
			int lastIndex = response.toString().indexOf( "testProgress" ) - 2;
			String inhalt = response.toString().substring( firstIndex, lastIndex );

			con.disconnect();

			if( inhalt.equalsIgnoreCase( "NIO" ) ) {
				return true;
			} else {
				return false;
			}
		} catch( Exception e ) {
			return false;
		}

	}

}

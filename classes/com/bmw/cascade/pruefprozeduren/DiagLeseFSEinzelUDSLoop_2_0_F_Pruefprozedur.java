package com.bmw.cascade.pruefprozeduren;

import java.util.Enumeration;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultSet;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;

/**
 * Pr�fprozedur, welche in einer bestimmte Zeitspanne (=timeout), alle x Sekunden (=warte) �berpr�ft, ob ein bestimmter Fehlerort (=FCode1..n) vorhanden ist.
 * Ist der Fehlerort vorhanden, wird die Pr�fprozedur mit n.i.O. bewertet.
 * Ist der Fehlerort nicht vorhanden, wird die Pr�fprozedur mit i.O. bewertet.
 * @author Claudia Wolf
 * @version 1_0_F  08.07.10  CW  Erstinitialisierung. Erkl�rung siehe oben.
 */
public class DiagLeseFSEinzelUDSLoop_2_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	Ergebnis result;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagLeseFSEinzelUDSLoop_2_0_F_Pruefprozedur() {
	}

	/**
	  * erzeugt eine neue Pruefprozedur.
	  * @param pruefling Klasse des zugeh. Pr�flings
	  * @param pruefprozName Name der Pr�fprozedur
	  * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	  */
	public DiagLeseFSEinzelUDSLoop_2_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "CANCEL" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "SGBD", "JOB", "FCODE[1..N]", "TIMEOUT", "WARTE" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	public boolean checkArgs() {
		String temp;
		boolean ok;

		try {
			//auskommentieren, da diese Methode false liefert aufgrund von FCODE[1..N]
			//ok = super.checkArgs();
			ok=true;
			if( ok == true ) {
				//SGBD
				temp = getArg( getRequiredArgs()[0] );
				if( temp == null ) {
					return false;
				}
				//Jobs
				temp = getArg( getRequiredArgs()[1] );
				if( temp == null ) {
					return false;
				}

				// Einlesen der Anzahl der FehlerCodes
				int FCodeAnzahl = 0;

				while( (getArg( "FCODE" + (FCodeAnzahl + 1) )) != null ) {
					FCodeAnzahl++;
				}
				//FCode muss Zahl sein
				for( int i = 0; i < FCodeAnzahl; i++ ) {
					temp = getArg( "FCODE" + (i + 1) );
					try {
						Integer.parseInt( temp );
					} catch( NumberFormatException e ) {
						throw e;
					}
				}

				//timeout muss eine Zahl sein
				temp = getArg( getRequiredArgs()[2 + FCodeAnzahl] );
				try {
					Integer.parseInt( temp );
				} catch( NumberFormatException e ) {
					throw e;
				}
				//warte muss eine Zahl sein
				temp = getArg( getRequiredArgs()[3 + FCodeAnzahl] );
				try {
					Integer.parseInt( temp );
				} catch( NumberFormatException e ) {
					throw e;
				}
			}
			return ok;
		} catch( Exception e ) {
			e.printStackTrace();
			return false;
		} catch( Throwable e ) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String sgbd;
		String job;
		String timeout;
		String warte;

		int FCodeAnzahl = 0; //  Variable fuer die Anzahl angegeben FCodes
		String[] fCodes = null;
		String cancel;

		try {
			try {
				//Parameter holen
				if( checkArgs() == false )
					throw new PPExecutionException( "Parameterexistenz" );

				sgbd = getArg( "SGBD" );
				job = getArg( "JOB" );

				while( (getArg( "FCODE" + (FCodeAnzahl + 1) )) != null ) {
					FCodeAnzahl++;
				}
				fCodes = new String[FCodeAnzahl];

				//FCodes bef�llen
				for( int i = 0; i < FCodeAnzahl; i++ ) {
					fCodes[i] = getArg( "FCODE" + (i + 1) );
				}
				timeout = getArg( "TIMEOUT" );
				warte = getArg( "WARTE" );

				//Werkfrage mit M�glichkeit zum Abbruch der Pr�fung
				cancel = getArg( getOptionalArgs()[0] );
				if( cancel == null )
					cancel = "false";

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			
			boolean codeVorhanden=false;
			long start = System.currentTimeMillis();
			long end = start + new Long(timeout).longValue();

			boolean cancelUD = false;
			if(cancel.equalsIgnoreCase( "true" )){
				//timeout in Sekunden umwandeln
				int tout = new Integer(timeout).intValue();
				tout = tout/1000;
				
				int answer = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputDigital( PB.getString( "abbruchDurchWerker" ), PB.getString( "abbruchDurchWerker" ), tout );
				//YES
				if(answer==1){
					cancelUD = true;
					result = new Ergebnis( PB.getString( "abbruchDurchWerker" ), "Userdialog", "", "", "", "", "", "", "", "0", PB.getString( "abbruchDurchWerker" ), "YES", "", PB.getString("werkerfrageAbbruch"), "", Ergebnis.FT_NIO );
					ergListe.add(result);
					status = STATUS_EXECUTION_ERROR; // Job ist fehlgeschlagen
				}
				
			}
			
			//Solange der timeout noch nicht vorr�ber ist und der Pr�fschritt nicht abgebrochen wurde
			while((end-start)>0 && !cancelUD){
				start = System.currentTimeMillis();
				
				//solange die poll zeit noch nicht vorr�ber ist
				long startPoll = System.currentTimeMillis();
				long endPoll = startPoll + new Long(warte).longValue();
				while((endPoll-startPoll)>0){

					//aktuelle Ediabas Fehler Codes holen
					String[] ediabasErrorCodes = getErrorCodes(sgbd, job, ergListe);
					for (int i=0;i<fCodes.length;i++){
						codeVorhanden=false;
						for(int j=0;j<ediabasErrorCodes.length;j++){
							if(fCodes[i].equals( ediabasErrorCodes[j] )){
								codeVorhanden=true;
							}
						}
						//wenn der FCode vorhanden ist, wird die restliche Warte-Zeit geschlafen um
						//anschlie�end wieder zu �berpr�fen, ob der FCode immer noch vorhanden ist
						if(codeVorhanden){
							System.out.println("Code Vorhanden");
							startPoll = System.currentTimeMillis();
							if((endPoll-startPoll)>0){
								Thread.sleep( endPoll-startPoll );
								startPoll = System.currentTimeMillis();
							}
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "ERRORCODE", fCodes[i], "ErrorCode should not exist", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
				            ergListe.add(result);
							break;
						}else{
							result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, "", "ERRORCODE", fCodes[i], "OKAY", "", "0", "", "", "", "", PB.getString("errorNumber") + PB.getString( "nichtVerfuegbar" ), Ergebnis.FT_IO );
				            ergListe.add(result);
						}
					}
					//nach dem Sleep oder dem i.O. Ergebnis die Warte-Schleife unterbrechen
					break;

				}
				//wenn der FCode nicht vorhanden ist, kann der Pr�fschritt mit i.O. beendet werden
				if(!codeVorhanden){			
		            status = STATUS_EXECUTION_OK; // Job ist i.O.
					break;
				}
				
			}
			//wenn nach dem Timeout, der Fehlercode immer noch vorhanden ist, so wird der Pr�fschritt mit n.i.O. bewertet
			if(codeVorhanden){
	            status = STATUS_EXECUTION_ERROR; // Job ist fehlgeschlagen
			}
            
		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "ausfuehrungsstatusNIO" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		setPPStatus( info, status, ergListe );

	}

	/**
	 * Gibt alle auftretenden F-Ort-Nummern zur�ck.
	 * @param sgbd
	 * @param job
	 * @param ergListe
	 * @return alle auftretenden Fehler-Ort-Nummern beim angegebenen Job @job
	 * @throws PPExecutionException
	 */
	public String[] getErrorCodes( String sgbd, String job, Vector ergListe ) throws PPExecutionException {
		String fCodesEdiabas[] = null;
		String tempTestErg = "";
		int FCodeAnzahlEdiabas = 0;
		EdiabasProxyThread ediabas = null;
		try{
			ediabas = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();
			//vorhandene F-Codes z.B. mit job1 'IS_LESEN' holen
			String temp = ediabas.executeDiagJob( sgbd, job, "", "" );
			if( temp.equals( "OKAY" ) == false ) { // Es sind Fehler bei der Ausf�hrung aufgetreten
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} else {
				result = new Ergebnis( "Status", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );
			}
			int ergSaetze = ediabas.getDiagJobSaetze(); // Anzahl der gefunden Fehler
			EdiabasResultSet[] ediabasResultSets = ediabas.getResult().getResultSets();
			for( int i = 0; i < ergSaetze; i++ ) {
				EdiabasResultSet ediabasResultSet = ediabasResultSets[i];
				Enumeration results = ediabasResultSet.elements();
				for( ; results.hasMoreElements(); ) {
					Object testErg = (Object) results.nextElement();
					tempTestErg = testErg.toString();
					//findet die Anzahl der Ergebniss�tze
					if( tempTestErg.startsWith( "SAETZE" ) ) {
						int saetze = Integer.valueOf( tempTestErg.substring( tempTestErg.indexOf( ":" ) + 1, tempTestErg.length() ).trim() ).intValue();
						fCodesEdiabas = new String[saetze - 1];
					}
					//speichert die FehlerOrtNummern
					if( tempTestErg.startsWith( "F_ORT_NR" ) ) {
						fCodesEdiabas[FCodeAnzahlEdiabas] = tempTestErg.substring( tempTestErg.indexOf( ":" ) + 1, tempTestErg.length() ).trim();
						FCodeAnzahlEdiabas++;
					}
				}
			}
			
		}catch( ApiCallFailedException e ) {
			System.out.println("Api call failed");
			e.printStackTrace();
			result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
			ergListe.add( result );
			throw new PPExecutionException();
		} catch( EdiabasResultNotFoundException e ) {
			if( e.getMessage() != null )
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			throw new PPExecutionException();
		} catch( DeviceLockedException e ) {
			e.printStackTrace();
			result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), PB.getString( "lockEcu" ), Ergebnis.FT_NIO );
			ergListe.add( result );
			throw new PPExecutionException();
		} catch( DeviceNotAvailableException e ) {
			e.printStackTrace();
			result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "Device is not available", Ergebnis.FT_NIO );
			ergListe.add( result );
			throw new PPExecutionException();
		} 
		return fCodesEdiabas;
	}
	
}

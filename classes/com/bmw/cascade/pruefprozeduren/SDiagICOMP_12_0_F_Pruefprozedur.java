package com.bmw.cascade.pruefprozeduren;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import sun.misc.BASE64Decoder;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * SDiagICOMP_Pruefprozedur.java <BR>
 * <BR>
 * Created on 13.10.2010 <BR>
 * <BR>
 * bei Aenderung Hr. Haase TI-538 Tel.: 43876 informieren! <BR>
 * <BR>
 * Implementierung f�r unter anderem UPDATE und CONFIG f�r ICOM P �ber Netzwerk<BR>
 * <BR>
 * 
 * @author Buboltz, Haase Wolf <BR>
 * @version 1_0_F 22.10.2009 DH Ersterstellung<BR>
 * 			2_0_F 23.10.2009 JS/DH Verschl�sselung eingebaut<BR>
 * 			3_0_F 01.12.2009 DH Bugfix in DEBUG-Mode<BR>
 *          4_0_F 11.03.2010 DH JobResult f�r APDM<BR>
 *          5_0_F 11.04.2010 DH ProgressBar hinzugef�gt<BR>
 *          6_0_F 16.04.2010 DH @-Operatorverwendung und mehrere Argumente durch ; getrennt m�glich gemacht<BR>
 *          7_0_F 13.10.2010 DH PU_NAME als Ergebnis ins virtuelle Fahrzeug eintragen<BR>
 *          8_0_T 29.07.2014 CW	Parametergewinnnung angepasst, sodass Parameter nur �ber CASCADE Standard-Methoden geholt werden<BR>
 *          9_0_F 29.07.2014 CW Freigabe 8_0_T<BR>
 *          10_0_T 19.05.2014 TB LOP 1944: Folegeprobleme im Rahmen von Wartungsarbeiten am NAS Filer f�r das W2 f�hrten dazu, dass der ICOM Tracefile Share nicht oder tempor�r nicht mehr erreichbar war.<BR>
 *                                         Dies f�hrte in Folge dazu, dass der entsprechende EDIABAS Job im ICOM-P Interface hing und nicht mehr zur�ckkehrte was letztendlich ebenfalls zu einem H�ngen des CASCADE Pr�fstandes f�hrte.<BR>
 *                                         Als Sofortmassnahme soll in der zugeh�rigen CASCADE Pr�fprozedur vor dem Aufruf des ICOM-P Jobes der Share �berpr�ft werden. Kleinere �berarbeitungen und Kosmetik.<BR>
 *                                         PP wird auch f�r andere ICOMP Themen genutzt. Deshalb Einf�hrung eines weiteren opt. Arguments und nur wenn dieses aktiviert ist, dann machen den Share Check.<BR>
 *			11_0_T 22.08.2016  TB  Erweiterungen f�r Paralleldiagnose analog zur DiagToleranzPP (LOP 2015), Umstellung auf EdiabasProxy, �berfl�ssiges Import entfernt + Generics eingebaut + Compiler Warnungen behoben<BR>
 *			12_0_F 25.01.2017  MKe  F-Version <BR>
 */
public class SDiagICOMP_12_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/** Debugschalter */
	boolean Debug = false;

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean isDE() {
		try {
			if( System.getProperty( "user.language" ).equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public SDiagICOMP_12_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public SDiagICOMP_12_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialisiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 * 
	 * @return Stringvektor der optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG", "ENCODED", "DISPLAY_PROGRESS", "NAME_PROGRESS", "CHECK_SHARE", "TAG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 * 
	 * @return Stringvektor der ben�tigten Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "SGBD", "JOB", "JOBPAR" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 * 
	 * @see com.bmw.cascade.pruefprozeduren.Pruefprozedur#checkArgs()
	 */
	@Override
	public boolean checkArgs() {
		// Obligatorisch.
		return super.checkArgs();
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {

		// spezifische Variablen
		String temp = null;
		String sgbd = null;
		String sgbdJob = null;
		String sgbdJobPara = null;
		boolean displayProgress = false;
		String nameProgress = "SDiagICOMP";
		boolean encoded = false;
		boolean checkShare = false; // bei true pr�fe den remote Share vorher
		String KOMMA = "'KOMMA'";
		UserDialog userDialog = null;

		EdiabasProxyThread ediabas = null;

		//Paralleldiagnose
		boolean parallel = false;
		String tag = null;
		DeviceManager devMan = null;

		final boolean DE = isDE(); // Systemsprache DE wenn true

		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		try {
			//Pr�fen ob Argumente stimmen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// Debug-Info ermitteln
				// DEBUG Parameter
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) || getArg( "DEBUG" ).equalsIgnoreCase( "DEBUGON" ) ) {
						Debug = true;
					}
				} else {
					// alternativ DEBUG �ber Pr�fstandsvariable noch auswerten
					try {
						Object objDebug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
						if( objDebug != null ) {
							if( objDebug instanceof String && "TRUE".equalsIgnoreCase( (String) objDebug ) )
								Debug = true;
							else if( objDebug instanceof Boolean && ((Boolean) objDebug).booleanValue() )
								Debug = true;
						}
					} catch( VariablesException e ) {
					}
				}

				if( getArg( "CHECK_SHARE" ) != null && getArg( "CHECK_SHARE" ).equalsIgnoreCase( "TRUE" ) ) {
					checkShare = true;
				}

				if( Debug ) {
					System.out.println( "\n\n\n Aktuelle Pr�fprozedur: SDiagICOMP_11_0_F_Pruefprozedur" );
					System.out.println( "SDiagICOMP PP Debug = ON" );
				}

				//Encoding sgbdJobPara
				if( getArg( "ENCODED" ) != null && getArg( "ENCODED" ).equalsIgnoreCase( "TRUE" ) ) {
					encoded = true;
					if( Debug )
						System.out.println( "SDiagICOMP PP encoded = " + encoded );
				}
				//Progressbar
				if( getArg( "DISPLAY_PROGRESS" ) != null && getArg( "DISPLAY_PROGRESS" ).equalsIgnoreCase( "TRUE" ) ) {
					displayProgress = true;
					if( Debug )
						System.out.println( "SDiagICOMP PP displayProgress = " + displayProgress );

					if( getArg( "NAME_PROGRESS" ) != null ) {
						nameProgress = getArg( "NAME_PROGRESS" ).toUpperCase();
						if( Debug )
							System.out.println( "SDiagICOMP PP nameProgress = " + nameProgress );
					}
				}

				// PU-Name abgelegt in virtuellem Fahrzeug
				result = new Ergebnis( "PU_NAME", "", "", "", "", "PU_NAME", "" + getPr�flingLaufzeitUmgebung().getPr�fumfangName(), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );

				sgbd = extractValues( getArg( getRequiredArgs()[0] ) )[0];
				if( Debug )
					System.out.println( "SDiagICOMP PP sgbd: " + sgbd );

				sgbdJob = extractValues( getArg( getRequiredArgs()[1] ) )[0];
				if( Debug )
					System.out.println( "SDiagICOMP PP sgbdJob: " + sgbdJob );

				// sgbdJobPara holen und bearbeiten f�r @-Operator
				//Anzahl Argumente bestimmen
				String[] allArgs = splitArg( (getArg( getRequiredArgs()[2] )).trim() );
				int iNumArgs = allArgs.length;

				//zusammensetzen sgbbdJobPara-String f�r �bergabe.
				StringBuffer myPara = new StringBuffer( "" );
				for( int i = 0; i < iNumArgs; i++ ) {

					if( i != 0 ) // beim ersten Durchlauf kein Trennzeichen setzen
						myPara.append( ";" );
					myPara.append( extractValues( (getArg( getRequiredArgs()[2] )).trim() )[i] );
					if( Debug )
						System.out.println( "SDiagICOMP PP sgbdJobPara zusammensetzen iNumArgs=" + iNumArgs + ": " + myPara.toString() );
				}

				sgbdJobPara = myPara.toString();

				if( Debug )
					System.out.println( "SDiagICOMP PP sgbdJobPara vor KOMMA und BASE64: " + sgbdJobPara );

				// wenn BASE64 decodiert dann decodieren
				if( encoded ) {
					BASE64Decoder b64Dec = new BASE64Decoder();
					sgbdJobPara = (new String( b64Dec.decodeBuffer( sgbdJobPara ) )).trim();
					if( Debug )
						System.out.println( "SDiagICOMP PP sgbdJobPara nach BASE64: " + sgbdJobPara );
				}

				// Komma Substituierung
				sgbdJobPara = sgbdJobPara.replaceAll( KOMMA, "," );

				if( Debug )
					System.out.println( "SDiagICOMP PP sgbdJobPara nach replaceAll: " + sgbdJobPara );

			} catch( Exception e ) {
				e.printStackTrace();
				result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Package.properties: " + PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Ausf�hren parametrierter SGBD-Job
			try {

				if( displayProgress == true ) {
					userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
					// Informiere den Benutzer
					userDialog.setDisplayProgress( true );
					userDialog.setProgressIndeterminate( true );
					userDialog.displayMessage( "Status " + nameProgress, ";" + "\n--> BITTE WARTEN.\n--> PLEASE WAIT.", -1 );
					if( Debug )
						System.out.println( "SDiagICOMP PP Progressbar is running!!!" );
				}

				// Paralleldiagnose, Pr�fstandvariabel auslesen, ob Paralleldiagnose 
				try {
					Object pr_var;

					pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
					if( pr_var != null ) {
						if( pr_var instanceof String ) {
							if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
								parallel = true;
							}
						} else if( pr_var instanceof Boolean ) {
							if( ((Boolean) pr_var).booleanValue() ) {
								parallel = true;
							}
						}
					}
				} catch( VariablesException e ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				}

				// Devicemanager
				devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();

				if( getArg( "TAG" ) != null )
					tag = getArg( "TAG" );

				if( parallel ) // Ediabas holen
				{
					try {
						ediabas = devMan.getEdiabasParallel( tag, sgbd );
					} catch( Exception ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Wrong Cascade configuration for parallel diagnosys", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();
					} catch( Throwable ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();
					}
				} else {
					try {
						ediabas = devMan.getEdiabas( tag );
					} catch( Exception ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();

					}
				}
				// <<<<< Paralleldiagnose            

				// Remote Share vorher pr�fen, wenn dieser Check �ber den �bergabeparameter entsprechend konfiguriert wurde
				if( checkShare ) {
					// Lese den EDIABAS remote Pfad aus dem ICOM aus
					// nach Info von Manuel sind Job und Result fix
					executeEdiabasJob( ediabas, sgbd, "STATUS_TRACE_GET_CONFIG", "" );
					String fileName = readEdiabasResult( ediabas, "STAT_REMOTEURL" );

					// Der Filename enth�lt im Gutfall nicht nur den Sharenamen im Linuxformat, sondern hat das Format: "'f�hrende Daten' + Leerzeichen + Share", Share Format z.B.: //smuc1451.muc/icomp_files$/trace
					int index = fileName.indexOf( " " );
					if( index != -1 ) {
						fileName = fileName.substring( index + 1, fileName.length() ).trim();
					}

					// Pr�fung der Verf�gbarkeit des ermittelten Files / Shares
					ExecutorService exSrv = Executors.newFixedThreadPool( 1 );
					ExecutorCompletionService<Boolean> completionService = new ExecutorCompletionService<Boolean>( exSrv );
					completionService.submit( new CheckRemoteFileCallable( fileName ) );
					Boolean remoteFileOK = false;
					try {
						Future<Boolean> futureResult = completionService.poll( 5, TimeUnit.SECONDS );
						remoteFileOK = futureResult.get();
					} catch( Exception e ) {
						// Sicherheitshalber alle Exceptions bearbeiten (NPE, InterruptedException, ...)
						if( Debug ) {
							e.printStackTrace();
						}
					}
					exSrv.shutdown();

					// Doku
					if( remoteFileOK ) {
						result = new Ergebnis( "ICOMP", "FILE", fileName, "", "", DE ? "PFAD" : "PATH", DE ? "ERREICHBAR" : "AVAILABLE", DE ? "ERREICHBAR" : "AVAILABLE", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					} else {
						result = new Ergebnis( "ICOMP", "FILE", fileName, "", "", DE ? "PFAD" : "PATH", DE ? "NICHT ERREICHBAR" : "NOT AVAILABLE", DE ? "ERREICHBAR" : "AVAILABLE", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					}

				}

				// Ggf. den weiteren EDIABAS Job ausf�hren; im Normalfall immer bei aktivierten "CHECK_SHARE" nur nach IO Pr�fung des Shares 
				temp = executeEdiabasJob( ediabas, sgbd, sgbdJob, sgbdJobPara );

				if( temp.equals( "OKAY" ) ) {
					result = new Ergebnis( "DiagStatus", "EDIABAS", sgbd, sgbdJob, sgbdJobPara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				} else {
					result = new Ergebnis( "DiagStatus", "EDIABAS", sgbd, sgbdJob, sgbdJobPara, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
			} catch( ApiCallFailedException e ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, sgbdJob, sgbdJobPara, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, sgbdJob, sgbdJobPara, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, sgbdJob, sgbdJobPara, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException e ) {
			if( Debug )
				e.printStackTrace();
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			if( Debug )
				e.printStackTrace();

			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			e.printStackTrace();
		} catch( Throwable e ) {
			if( Debug )
				e.printStackTrace();

			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Freigabe inkl. Reset des benutzten UserDialoges
		if( displayProgress == true ) {
			// Freigabe inkl. Reset
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( Exception e ) {
				if( Debug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		// Paralleldiagnose
		if( parallel ) {
			try {
				devMan.releaseEdiabasParallel( ediabas, tag, sgbd );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable t ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		// Ergebnisstatus setzen
		setPPStatus( info, status, ergListe );

	} // End of execute()

	/** Ediabasjob ausf�hren und Ergebnis STATUS=OKAY pr�fen
	 * 
	 * @param ediabas Ediabas
	 * @param SGBD Ediabas SGBD
	 * @param JobName Name des EdiabasJobs
	 * @param Args RequestParameter
	 * @return Ediabas Jobstatus
	 * @throws Exception Exception bei Fehler
	 */
	private String executeEdiabasJob( EdiabasProxyThread ediabas, String SGBD, String JobName, String Args ) throws Exception {
		String Status;
		String ERRORTXT = isDE() ? "Fehler bei " + SGBD + "/" + JobName + "/executeDiagJob (" + Args + ")" : "Error during " + JobName + "/executeJob (" + Args + ")";

		if( Debug )
			System.out.println( "SDiagICOMP PP executing from SGBD: " + SGBD + ": Job: " + JobName + ": with Argument: " + Args );

		try {
			Status = ediabas.executeDiagJob( SGBD, JobName, Args, "" );
		} catch( ApiCallFailedException e ) {
			throw new ApiCallFailedException( "EDIABASERROR: " + ERRORTXT + ", API-Error: " + ediabas.apiErrorText(), e.getErrorCode(), e.getErrorMessage() );
		} catch( EdiabasResultNotFoundException e ) {
			throw new IOException( "EDIABASERROR: " + ERRORTXT + ", API-Error: EdiabasResultNotFoundException" );
		}

		if( !Status.equalsIgnoreCase( "OKAY" ) ) {
			throw new IOException( "EDIABASERROR: " + ERRORTXT + ", STATUS: " + Status );
		}

		return Status;
	}

	/** EDIABAS Result lesen
	 * 
	 * @param ediabas Ediabas
	 * @param Resultname Result das ausgelesen werden soll
	 * @return EDIABAS Result oder null
	 * @throws Exception Exception bei Fehler
	 */
	private String readEdiabasResult( EdiabasProxyThread ediabas, String Resultname ) throws Exception {
		String Result = null;
		String ERRORTXT = isDE() ? "Lese EDIABAS Result: " + Resultname : "Reading EDIABAS result: " + Resultname;

		try {
			// Wenn Result ausgelesen werden soll, dann versuche das
			if( Resultname != null ) {
				return ediabas.getDiagResultValue( Resultname );
			}
		} catch( ApiCallFailedException e ) {
			throw new ApiCallFailedException( "EDIABASERROR: " + ERRORTXT + ", API-Error: " + ediabas.apiErrorText(), e.getErrorCode(), e.getErrorMessage() );
		} catch( EdiabasResultNotFoundException e ) {
			throw new IOException( "EDIABASERROR: " + ERRORTXT + ", API-Error: EdiabasResultNotFoundException" );
		}

		return Result;
	}

	/**
	 * Verf�gbarkeitscheck des Trace Shares in Form einer eigenen Callable Klasse
	 */
	private class CheckRemoteFileCallable implements Callable<Boolean> {
		private File file;

		/**
		 * Konstruktor Methode
		 * @param fileName
		 */
		public CheckRemoteFileCallable( String fileName ) {
			this.file = new File( fileName.replace( '/', '\\' ) ); // Linux Slashes auf Windows Format bringen, e.q. (//smuc1451.muc/icomp_files$/trace --> \\smuc1451.muc\icomp_files$\trace)  
		}

		/**
		 * Eigentliche �berpr�fung des Shares (Die Methoden exists() und canRead() k�nnen potentiell dauern)
		 *
		 * @return Boolean.TRUE, wenn das File Objekt existiert und darauf lesend zugegriffen werden kann
		 * @throws Exception wenn das Ergebnis nicht ermittelt werden konnte
		 */
		@Override
		public Boolean call() throws Exception {
			return file.exists() && file.canRead() ? Boolean.TRUE : Boolean.FALSE;
		}

	}

} // End of Class

/*
 * Ping_1_0_F_Pruefprozedur.java
 *
 * Created on 19.02.12
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.net.InetAddress;
import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;

/**
 * Implementierung der Pr�fprozedur, die einen Rechner pingt und abh�ngig davon IO oder NIO wird <BR>
 * - Bei der Ping �berpr�fung ist default der check �ber natives ausf�hren des ping Kommandos <BR>
 * - Alternativ kan der Check �ber Java isReachable Check erfolgen <BR>
 * @author Buboltz <BR>
 * @version Implementierung <BR>
 * @version 1_0_F	19.02.2012  TB Erstimplementierung <BR>
 *          2_0_T   24.02.2014  TB Anzahl der Pings parametrierbar, Umstellung auf ProzessBuilder und bei NIO den Fehler vom externen Prozess mit erfassen und loggen. <BR>
 *          3_0_F   03.03.2014  TB F-Version <BR>
 */
public class Ping_3_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public Ping_3_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public Ping_3_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "METHOD", "NATIVE_PING_COUNT", "DEBUG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "HOSTNAME" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 *
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt
	 *         hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		if( !super.checkArgs() )
			return false;

		return true;
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String hostname; // der zu pingende Hostname, vollqualifiziert oder als IP Adresse

		final int PING_NATIVE = 0;
		final int PING_JAVA = 1;
		int iPingMethod = PING_NATIVE; // Auswahl der Ping Methode ("java" == "1" entspricht ping Java, andere Werte = 0 entsprechen der Methode ping native),

		long lNativePingCount = 1; // Anzahl der Pings, die erlaubte Anzahl sind 1 bis 4.294.967.295

		try {
			//Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG abfragen
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
				} else {
					// noch Pr�fstandvariable pr�fen, ob Debug
					try {
						Object pr_var_debug;

						pr_var_debug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
						if( pr_var_debug != null ) {
							if( pr_var_debug instanceof String ) {
								if( "TRUE".equalsIgnoreCase( (String) pr_var_debug ) ) {
									bDebug = true;
								} else {
									bDebug = false;
								}
							} else if( pr_var_debug instanceof Boolean ) {
								if( ((Boolean) pr_var_debug).booleanValue() ) {
									bDebug = true;
								} else {
									bDebug = false;
								}
							}
						} else {
							bDebug = false;
						}

					} catch( VariablesException e ) {
						bDebug = false;
					}
				}

				// hostname
				hostname = extractValues( getArg( "HOSTNAME" ) )[0];

				// Methode f�r Ping Check, default ist der java check, isReachable
				if( getArg( "METHOD" ) != null ) {
					if( extractValues( getArg( "METHOD" ) )[0].equalsIgnoreCase( "JAVA" ) || extractValues( getArg( "METHOD" ) )[0].equalsIgnoreCase( "" + PING_JAVA ) )
						iPingMethod = PING_JAVA;
				}

				// Anzahl der nativen Pings, falls diese konfiguriert wurden. Erlaubte Werte sind 1 <= NATIVE_PING_COUNT <= 4.294.967.295.
				if( getArg( "NATIVE_PING_COUNT" ) != null ) {
					// Wenn Ping Zahl nicht verarbeitbar ist, oder diese au�erhalb des erlaubten Bereiches liegt
					try {
						lNativePingCount = Long.parseLong( extractValues( getArg( "NATIVE_PING_COUNT" ) )[0] );
					} catch( NumberFormatException nfe ) {
						throw new PPExecutionException( "NumberFormatException" );
					} finally {
						if( lNativePingCount < 1 || lNativePingCount > 4294967295l )
							throw new PPExecutionException( "OutOfRange" );
					}
				}

				// debug
				if( bDebug )
					System.out.println( "Ping PP: Parameter Check is done. (Hostname: <" + hostname + ">, Method: <" + iPingMethod + ">)" );

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Der eigentliche PING Check
			boolean pingStatus = false;

			switch( iPingMethod ) {
				case PING_JAVA:
					pingStatus = pingJava( hostname );
					result = new Ergebnis( "Status", "PING", hostname, "", "", "PING_STATUS", "" + pingStatus, "true", "", "0", "", "", "", "", "", pingStatus ? Ergebnis.FT_IO : Ergebnis.FT_NIO );
					ergListe.add( result );
					break;
				case PING_NATIVE:
					pingStatus = pingNative( hostname, lNativePingCount );
					result = new Ergebnis( "Status", "PING", hostname, "", "", "PING_STATUS", "" + pingStatus, "true", "", "0", "", "", "", pingStatus ? "" : resultPingNative, "", pingStatus ? Ergebnis.FT_IO : Ergebnis.FT_NIO );
					ergListe.add( result );
					break;
			}

			if( bDebug )
				System.out.println( "Ping PP: Result Ping (Hostname: <" + hostname + ">, Result: <" + pingStatus + ">)" );

			if( !pingStatus )
				status = STATUS_EXECUTION_ERROR;

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} finally {
			resultPingNative = null; // Objekt (Membervariable) auf jeden Fall wieder freigeben
		}

		//Status setzen
		setPPStatus( info, status, ergListe );
	}

	// Membervariable f�r Datenaustausch
	private String resultPingNative = "";

	/**
	 * �berpr�fung des Host �ber natives pingen
	 * @param host der zu pingende Host
	 * @param lNativePingCount, Anzahl der nativen Pings 1 bis 4.294.967.295
	 * @return true, wenn ping funktioniert hat
	 */
	public boolean pingNative( String host, long lNativePingCount ) {
		boolean isReachable = false;
		BufferedReader reader = null;
		StringBuffer sbInput = new StringBuffer();

		try {
			// Als externen Prozess ausf�hren
			ProcessBuilder pb = new ProcessBuilder( "ping", "-n", "" + lNativePingCount, "-w", lNativePingCount == 1 ? "1000" : "3000", host );
			Process process = pb.start();
			int exitValue = process.waitFor();

			// den InputStream einlesen, Umgebung ist Windows, deshalb Windows default Codierung CP850 annehmen
			reader = new BufferedReader( new InputStreamReader( process.getInputStream(), "CP850" ) );
			String line;
			while( (line = reader.readLine()) != null ) {
				sbInput.append( line );
			}

			// Zuweisung f�r die weitere Verarbeitung
			if( exitValue == 0 ) {
				isReachable = true;
				resultPingNative = "IO: " + sbInput.toString();
			} else {
				resultPingNative = "NIO: " + sbInput.toString();
			}
		} catch( Exception e ) {
			if( bDebug ) {
				System.out.println( "Ping PP: Exception begin!" );
				e.printStackTrace();
				System.out.println( "Ping PP: Exception end!" );
			}
		} finally {
			try {
				reader.close();
			} catch( Exception e ) {
				// alle Exceptions ignorieren
			}
		}

		return isReachable;
	}

	/**
	 * �berpr�fung des Host �ber Java is reachable
	 * @param host der zu pingende Host
	 * @return true, wenn ping funktioniert hat
	 */
	public boolean pingJava( String host ) {
		try {
			return InetAddress.getByName( host ).isReachable( 2000 );
		} catch( IOException e ) {
			if( bDebug ) {
				System.out.println( "Ping PP: Exception begin!" );
				e.printStackTrace();
				System.out.println( "Ping PP: Exception end!" );
			}
		}
		return false;
	}

	/**
	 * Testmethode
	 * @param args
	 */
	public static void main( String args[] ) {
		long currentTime;
		Ping_3_0_F_Pruefprozedur pp = new Ping_3_0_F_Pruefprozedur();

		String hostname = "xmuc283894"; // sollte gehen FIZ Testmaschine (IP: 10.253.117.140)
		String hostnameIP = "160.50.6.80"; // sollte gehen FIZ ZS (IP: 160.50.6.80)

		String hostname_geht_nicht = "xmuc283894.muc.w200"; // geht nicht

		currentTime = System.currentTimeMillis();
		System.out.println( "PingNative, Host should work: <" + hostname + ">, Ping: <" + pp.pingNative( hostname, 1 ) + ">, runs... " + (System.currentTimeMillis() - currentTime) + "ms" );
		System.out.println( pp.resultPingNative );
		currentTime = System.currentTimeMillis();
		System.out.println( "PingNative, Host with IP address should work: <" + hostnameIP + ">, Ping: <" + pp.pingNative( hostnameIP, 1 ) + ">, runs... " + (System.currentTimeMillis() - currentTime) + "ms" );
		System.out.println( pp.resultPingNative );
		currentTime = System.currentTimeMillis();
		System.out.println( "PingNative, Host should not work: <" + hostname_geht_nicht + ">, Ping: <" + pp.pingNative( hostname_geht_nicht, 1 ) + ">, runs... " + (System.currentTimeMillis() - currentTime) + "ms" );
		System.out.println( pp.resultPingNative );

		System.out.println();

		currentTime = System.currentTimeMillis();
		System.out.println( "PingJava, Host should work: <" + hostname + ">, Ping: <" + pp.pingJava( hostname ) + ">, runs... " + (System.currentTimeMillis() - currentTime) + "ms" );
		currentTime = System.currentTimeMillis();
		System.out.println( "PingJava, Host with IP address should work: <" + hostnameIP + ">, Ping: <" + pp.pingJava( hostnameIP ) + ">, runs... " + (System.currentTimeMillis() - currentTime) + "ms" );
		currentTime = System.currentTimeMillis();
		System.out.println( "PingJava, Host should not work: <" + hostname_geht_nicht + ">, Ping: <" + pp.pingJava( hostname_geht_nicht ) + ">, runs... " + (System.currentTimeMillis() - currentTime) + "ms" );
	}
}

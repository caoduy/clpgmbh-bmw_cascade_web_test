/*
 * DiagGetConfig_1_0_F_Pruefprozedur.java
 *
 * Created on 20.08.14
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;

import com.bmw.cascade.pruefstand.ediabas.Ediabas;
import com.bmw.cascade.pruefstand.ediabas.ApiCallFailedException;

;

/**
 * Pr�fprozedur, welche die Ediabas-Konfiguration abfr�gt. Ermittelter Wert
 * sowie der Name des abgefragten Parameters werden als Ergebnis gespeichert. 
 *
 * Zwingende Argumente:
 *
 * NAME: Name der abzufragenden Konfigurations-Einstellung.
 *
 * Optionale Argumente: keine
 *
 * @author Stephan Schubert, TI-545<BR>
 * @version 0_0_1_FA PR 20.08.2014 Initiale Implementierung<BR>
 */
public class DiagGetConfig_1_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur f�r die Deserialisierung
	 */
	public DiagGetConfig_1_0_F_Pruefprozedur() {
	}

	/**
	   * Konstruktor.
	   *
	   * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	   * @param pruefprozName Name der Pr�fprozedur
	   * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	   */
	public DiagGetConfig_1_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 *
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = {};
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 *
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = { "NAME" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 *
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt
	 *         hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		if( !super.checkArgs() )
			return false;

		// �berpr�fe die Argumente.
		String name = getArg( "NAME" );
		if( name == null || name.length() == 0 )
			return false;

		return true;
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 *
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		//eigene Variablen
		String name = null;
		String value = null;

		try {

			//Parameter holen und Argumente pruefen
			try {
				// hat der allgemeine Check fehlgeschlagen???
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				// hole den Namen
				if( getArg( getRequiredArgs()[0] ).indexOf( '@' ) != -1 )
					name = getPPResult( getArg( getRequiredArgs()[0] ) );
				else
					name = getArg( getRequiredArgs()[0] );

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );

				ergListe.add( result );
				throw new PPExecutionException();
			}

			//Den Parameternamen als eigenes APDM Result ablegen.
			result = new Ergebnis( "NAME", "EDIABAS", "", "", "", "PARAMETER_NAME", name, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );

			// Jetzt die EDIABAS Konfiguration abfragen
			try {
				value = Ediabas.apiGetConfig( name );

				result = new Ergebnis( "VALUE", "EDIABAS", name, "", "", "PARAMETER_VALUE", value, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );
			} catch( ApiCallFailedException e ) {
				result = new Ergebnis( "Ediabas-Fehler", "", "", "", "", "", "", "", "", "", "", "", "", e.toString(), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO );
			e.printStackTrace();
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		setPPStatus( info, status, ergListe );
	}

}

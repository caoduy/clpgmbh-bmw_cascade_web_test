/*
 * UiCalibration
 *
 * Created on 16.07.03
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.visualisierung.*;
import com.bmw.cascade.pruefstand.devices.uianalyse.*;

/**
 * Implementierung der Pr�fprozedur, die von Funkadapter gesendete Strom- und Spannungswerte anzeigt.
 *
 * @author ESG Elektroniksystem- und Logistik-GmbH Z. Wen
 * @version V0_0_1  16.07.2003  Implementierung
 * @version V0_0_2 Bug fixed
 * @version V0_0_3 Fehlermeldungausgabe verbessert
 * @version V0_0_4 15.10.2004 Fehlermeldungausgabe verbessert
 */
public class UiCalibration_4_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    	static final long serialVersionUID = 1L;
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public UiCalibration_4_0_F_Pruefprozedur() {}

  /**
     * erzeugt eine Pruefprozedur.
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public UiCalibration_4_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    /**
     * initialsiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /**
     * liefert die optionalen Argumente
     * @return Stringvektor der optionalen Argumente
     */
    public String[] getOptionalArgs() {
        String[] args = new String[0];
        return args;
    }
    
    /**
     * liefert die zwingend erforderlichen Argumente
     * @return Stringvektor der ben�tigten Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = new String[0];
        return args;
    }
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
     * @return true, wenn die Ueberpruefung der zwingenden Argumente ok ist
     */
    public boolean checkArgs() {
        boolean ok;
        
        try{
            ok = super.checkArgs();
            return ok;
        } catch (Exception e) {
            return false;
        } catch (Throwable e) {
            return false;
        }
    }
    
    /**
     * f�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        // spezifische Variablen
        UIAnalyser myUI = null;     //Variable f�r UIAnalyser
        UserDialog myDialog = null; //Variable f�r UserDialog
        UIPair pair = null;         //Variable f�r UIPair
        
        try {
            try {
                //�berpr�fung der Parameter
                if( checkArgs() == false ) throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
            } catch (PPExecutionException e) {
                //Parametrierfehler
                e.printStackTrace();
                if (e.getMessage() != null)
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "parametrierfehler" ), Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "parametrierfehler" ), Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            //Holen des Devices UIAnalyser
            try {
                myUI = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyser();
            } catch (Exception e) {
                //Systemfehler, muss man genau analysieren
                e.printStackTrace();
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
                else if (e instanceof DeviceNotAvailableException)
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "DeviceNotAvailableException", Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "Exception", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            //Holen des Devices UserDialog
            try {
                myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
            } catch (Exception e) {
                //Systemfehler, muss man genau analysieren
                e.printStackTrace();
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "", "", "", "", e.getMessage(), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
                else if (e instanceof DeviceNotAvailableException)
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "", "", "", "", e.getMessage(), "DeviceNotAvailableException", Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "", "", "", "", e.getMessage(), "Exception", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException("Get UserDialog");
            }
            
            //Show multimeter
            try{
                myDialog.setAllowCancel(true);
                myUI.reset(true);               //negative Stromwerte sind zugelassen
                do {
                    try{
                        pair = myUI.recordUIPair();
                    } catch(Exception e) {
                        pair = null;
                    }
                    if (pair == null){  //Keine Daten vorhanden
                        getPr�flingLaufzeitUmgebung().getUserDialog().displayMessage( "Multimeter", PB.getString( "batterieSpannung" ) + " = " + PB.getString( "Unbekannt" ) + "       " + PB.getString( "batterieStrom" ) + " = " + PB.getString( "Unbekannt" ), -1 );
                    } else {            //UIPair vorhanden
                        getPr�flingLaufzeitUmgebung().getUserDialog().displayMessage( "Multimeter", PB.getString( "batterieSpannung" ) + " = " + pair.uValue + " mV       " + PB.getString( "batterieStrom" ) + " = " + pair.iValue + " mA", -1 );
                    }
                    Thread.sleep(250);
                } while ( myDialog.isCancelled() == false );
            } catch (Exception e) {
                //Systemfehler, muss man genau analysieren
                e.printStackTrace();
                result = new Ergebnis( "Multimeter", "UIAnalyser", "", "", "", "", "", "", "", "", "", "", "", e.getMessage(), "", Ergebnis.FT_NIO );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            result = new Ergebnis( "Status", "System", "", "", "", "", "", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
            ergListe.add(result);
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            //Systemfehler, muss man genau analysieren
            e.printStackTrace();
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
            //Systemfehler, muss man genau analysieren
            e.printStackTrace();
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
        //Freigabe der verwendeten Devices
        if( myUI != null ) {
            try {
                //Device UIAnalyser wird freigegeben
                getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyser();
            } catch (Exception e) {
                //Systemfehler, muss man genau analysieren
                e.printStackTrace();
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ExecFehler", "UIAnalyser", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "release", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                status = STATUS_EXECUTION_ERROR;
            }
            myUI = null;
            try {
                //Device Dialgobox wird freigegeben
                getPr�flingLaufzeitUmgebung().releaseUserDialog();
            } catch (Exception e) {
                //Systemfehler, muss man genau analysieren
                e.printStackTrace();
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "", "", "", "", e.getMessage(), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
                else if (e instanceof DeviceNotAvailableException)
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "", "", "", "", e.getMessage(), "DeviceNotAvailableException", Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "", "", "", "", e.getMessage(), "Exception", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                status = STATUS_EXECUTION_ERROR;
            }
        }
        //Status setzen
        setPPStatus( info, status, ergListe );
    }
}

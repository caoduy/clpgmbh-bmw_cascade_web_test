package com.bmw.cascade.pruefprozeduren;

import java.util.StringTokenizer;
import java.util.Vector;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.io.*;

import javax.imageio.ImageIO;

import com.bmw.cascade.pruefprozeduren.PPExecutionException;
import com.bmw.cascade.auftrag.*;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.headlightaiming.*;
import com.bmw.cascade.pruefstand.devices.serial.*;

/**
 * Implementierung der Pr�fprozedur zur Ablaufsteuerung am
 * Scheinwerfereinstellstand (HeadlightAiming)<BR>
 * Created on 22.03.07 <BR>
 * <BR>
 * 
 * @author P. Jorge (PJ) - [IndustrieHansa], BMW AG, TI-432<BR>
 *         U. Plath (UP) - TI-544, BMW AG<BR>
 *         P. Rettig (PR) - Gefasoft Engineering GmbH
 * 
 * @version 1_0 PJ 22.03.2007 Erstellung und Implementierung der ersten Jobs<BR>
 *          2_0 PJ 02.05.2007 Kleinere �nderungen<BR>
 *          4_0 PJ 18.06.2007 Kleinere �nderungen<BR>
 *          5_0 PJ 24.08.2007 Fehlernachricht im Falle eines Maschinenfehlers<BR>
 *          oder Hand-Modus Stop PU entfernt. APDM-Ergebnisse aus MEASURE_STAND und PERFORM_AIMING_PROCESS
 *          angepasst<BR>
 *          6_0 PJ 26.03.2008 APDM-Ergebnisse aus PERFORM_AIMING_PROCESS f�r
 *          vor/nach Einstellung angepasst. TimeCriteria-Attribut ge�ndert zu
 *          -MonthYear- in Auftragsdaten. Auffangmethode f�r DISCONNECTED-Status
 *          der Anlage implementiert<BR>
 *          7_0 UP 26.11.2008 motorSport-Kriterium mit in Auftragsdaten-�bergabe
 *          eingebunden<BR>
 *          8_0 UP 31.05.2011 Anpassung an aktuelle Schnittstellen-Spezifikation
 *          0.18, Integration der Anbindung der LET-Einstellanlagen �ber
 *          serielle Schnittstelle<BR>
 *          9_0 UP 12.07.2011 �nderung der Akzeptanz von Maschinen-Status
 *          (vorher: = IO/IDLE, jetzt beginnt mit IO/IDLE) Bugfix in der
 *          XML-Deserialisierung<BR>
 *          10_0 UP 07.09.2011 Bugfix in Ermittlung des Zeitkriteriums aus Auftragsdaten<BR>
 *          11_0 UP 13.09.2011 Freigabeversion mit �nderungen aus v10<BR>
 *          12_0 UP 14.09.2011 Testversion mit Bugfix in XML-Deserialisierung<BR>
 *          13_0 UP 15.09.2011 Freigabeversion mit �nderungen aus v12<BR>
 *          14_0 UP 09.11.2011 Anpassung an interface spec 0.20 mit Funktion iDisplayMessage
 *          und Ausgabe der Lichtintensit�t in APDM<BR>
 *          15_0 UP 25.08.2012 Optionale Reduzierung der Datenabfragen in
 *          Einstellschritten �ber REDUCE_APDM_OUTPUT, Unterscheidung beim SW-Einstellen
 *          nach "NOK/Next Step" und "Cancel", Checksummenberechnung bei Steuerung
 *          einer LET-Anlage �ber RS232<BR>
 *          16_0 UP 15.02.2013 Wartezeiten von 100ms nach jedem DLL-Aufruf in den Einstell- und
 *          Positionierjobs hinzugef�gt<br>
 *          17_0 UP 20.02.2013 Wiederverbindung bei Status "DISCONNECTED" auch f�r LET verf�gbar
 *          gemacht ('contains' statt 'startsWith'), Weiterentwicklung des Parameters REDUCE_APDM_OUTPUT
 *          mit verschiedenen Stufen, neuer Parameter NOK_HANDLING steuert, ob die Tasten "Abbruch"
 *          und "NIO" bzw. "Next Step" unterschiedlich behandelt werden<br>
 *          18_0 UP 03.03.2013 Aktivierung des Jobs DISPLAY_MESSAGE zur Anzeige von
 *          MA-Hinweisen in der SWE-Software<br>
 *          19_0 UP 17.05.2013 Beim Aufruf von SETUP_SET wird jetzt auch der Fahrzeygtypschl�ssel
 *          �bertragen, ein Wiederverbindungsversuch wird jetzt auch bei "not connected" gestartet<br>
 *          20_0 UP 04.09.2014 Das engineSeries-Kriterium wird bei SETUP_SET mit �bertragen<br>
 *          21_0 UP 08.09.2014 T-Version mit den Inhalten aus Version 20_F<br>
 *          22_0 PR 09.10.2014 Falls getMotorBaureihe keinen Wert liefert, dann diesen aus jener DriveUnit
 *          beziehen, welche den Verbrennungsmotor beschreibt<br>
 *          23_0 UP 16.10.2014 Die Wartezeit nach der Pr�fstands-Statusabfrage zu Beginn
 *          jedes Schrittes wurde entfernt<br>
 *          24_0 UP 25.11.2014 die Night-Vision-Funktionen f�r den FIZ-Pr�fstand hinzugef�gt<br>
 *          25_0 UP 05.12.2014 F-Version mit den �nderungen aus den vorhergehenden Testversionen
 *          (DriveUnit, Nivi-Jobs, Zeitersparnis<br>
 *          26_0 UP 12.02.2015 Fehlerbehebung bei der Schleife zur Erfassung des Pr�fstandszustands
 *          27_0 MS 05.07.2016 Erweiterung der Funktionen um MRR und LC. Neuer Parameter DEVICE_TAG
 *          28_0 MS 05.07.2016 F-Version
 *          29_0 MS 16.08.2016 Erweiterung um Funktion START_LC_ADJUSTEMENT
 *          30_0 MS 16.08.2016 F-Version
 *          31_0 MS 18.01.2017 @-Parameter bei execute_DISPLAY_MESSAGE
 *          32_0 MS 18.01.2017 T-Version
 *          33_0 MS 18.01.2017 F-Version
 *          34_0 MS 28.09.2017 Changed methods to execute the jobs parallel
 *          35_0 MS 28.09.2017 F-Version
 */
public class SDiagHla_35_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	// consts
	static final int SRC = 0;
	static final int POS = 1;

	/**
	 * DefaultKonstruktor, nur f�r die Deserialisierung
	 */
	public SDiagHla_35_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pr�fprozedur.
	 * 
	 * @param pruefling
	 *            Klasse des zugeh. Pr�flings
	 * @param pruefprozName
	 *            Name der Pr�fprozedur
	 * @param hasToBeExecuted
	 *            Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public SDiagHla_35_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "JOB" };
		return args;
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "ID", "CMD", "X", "Y", "Z", "PHI", "THETA", "LIGHT", "WORK_DIRECTORY", "MESSAGE", "THRUST_ANGLE", "VSN", "TIMEOUT_SEC", "REDUCE_APDM_OUTPUT", "NOK_HANDLING", "BUTTONS", "SIMULATION", "DEBUG", "DEVICE_TAG" };
		return args;
	}

	/**
	 * pr�ft - soweit m�glich - die Argumente auf Existenz und Wert.
	 * �berschreibt die parent-Methode aufgrund der offenen Anzahl an Results
	 * 
	 * @return true, wenn �berpr�fung der Argumente erfolgreich
	 */
	public boolean checkArgs() {
		try {
			if( !super.checkArgs() ) {
				throw new Exception( "super.checkArgs failed!" );
			}
			String job = getArg( "JOB" );

			// SETUP
			if( job.equalsIgnoreCase( "SETUP" ) ) {
				check_SETUP();
			}
			// READ_DEFAULT_VALUE
			else if( job.equalsIgnoreCase( "READ_DEFAULT_VALUE" ) ) {
				check_READ_DEFAULT_VALUE();
			}
			// READ_IMAGE_VALUE
			else if( job.equalsIgnoreCase( "READ_IMAGE_VALUE" ) ) {
				check_READ_IMAGE_VALUE();
			}
			// POSITION_LIGHT_BOX_HOME
			else if( job.equalsIgnoreCase( "POSITION_LIGHT_BOX_HOME" ) ) {
				check_POSITION_LIGHT_BOX_HOME();
			}
			// POSITION_LIGHT_BOX
			else if( job.equalsIgnoreCase( "POSITION_LIGHT_BOX" ) ) {
				check_POSITION_LIGHT_BOX();
			}
			// POSITION_ACC_MIRROR
			else if( job.equalsIgnoreCase( "POSITION_ACC_MIRROR" ) ) {
				check_POSITION_ACC_MIRROR();
			}
			// PERFORM_AIMING_PROCESS
			else if( job.equalsIgnoreCase( "PERFORM_AIMING_PROCESS" ) ) {
				check_PERFORM_AIMING_PROCESS();
			}
			// CONTROL_CENTRATION
			else if( job.equalsIgnoreCase( "CONTROL_CENTRATION" ) ) {
				check_CONTROL_CENTRATION();
			}
			// CONTROL_TRAFFIC_LIGHTS
			else if( job.equalsIgnoreCase( "CONTROL_TRAFFIC_LIGHTS" ) ) {
				check_CONTROL_TRAFFIC_LIGHTS();
			}
			// CONTROL_VISUALIZATION
			else if( job.equalsIgnoreCase( "CONTROL_VISUALIZATION" ) ) {
				check_CONTROL_VISUALIZATION();
			}
			// MEASURE_VEHICLE
			else if( job.equalsIgnoreCase( "MEASURE_VEHICLE" ) ) {
				check_MEASURE_VEHICLE();
			}
			// MEASURE_VEHICLE
			else if( job.equalsIgnoreCase( "START_HEIGHT_MEASUREMENT" ) ) {
				check_START_HEIGHT_MEASUREMENT();
			}
			// MEASURE_STAND
			else if( job.equalsIgnoreCase( "MEASURE_STAND" ) ) {
				check_MEASURE_STAND();
			}
			// POSITION_MIRROR_HOME
			else if( job.equalsIgnoreCase( "POSITION_MIRROR_HOME" ) ) {
				check_POSITION_MIRROR_HOME();
			}
			// SET_THRUST_ANGLE
			else if( job.equalsIgnoreCase( "SET_THRUST_ANGLE" ) ) {
				check_SET_THRUST_ANGLE();
			}
			// DISPLAY_MESSAGE
			else if( job.equalsIgnoreCase( "DISPLAY_MESSAGE" ) ) {
				check_DISPLAY_MESSAGE();
			}
			// COLLECT_SERIAL_DATA
			else if( job.equalsIgnoreCase( "LET_SERIAL_HLA" ) ) {
				check_LET_SERIAL_HLA();
			}
			// SIMULATE_RS232_HLA
			else if( job.equalsIgnoreCase( "SIMULATE_SERIAL_HLA" ) ) {
				check_SIMULATE_SERIAL_HLA();
			}
			// ACTIVATE_NIVI_TARGET
			else if( job.equalsIgnoreCase( "ACTIVATE_NIVI_TARGET" ) ) {
				check_ACTIVATE_NIVI_TARGET();
			}
			// POSITION_NIVI_TARGET
			else if( job.equalsIgnoreCase( "POSITION_NIVI_TARGET" ) ) {
				check_POSITION_NIVI_TARGET();
			}
			// POSITION_NIVI_TARGET_HOME
			else if( job.equalsIgnoreCase( "POSITION_NIVI_TARGET_HOME" ) ) {
				check_POSITION_NIVI_TARGET_HOME();
			}
			// CONTROL_MOVEABLE_TRACKS
			else if( job.equalsIgnoreCase( "CONTROL_MOVEABLE_TRACKS" ) ) {
				check_ACTIVATE_MOVEABLE_TRACKS();
			}
			// DRIVE_LC_SCREWDRIVER_AUTO
			else if( job.equalsIgnoreCase( "DRIVE_LC_SCREWDRIVER_AUTO" ) ) {
				check_DRIVE_LC_SCREWDRIVER_AUTO();
			}
			// DRIVE_LC_SCREWDRIVER_MANUAL
			else if( job.equalsIgnoreCase( "DRIVE_LC_SCREWDRIVER_MANUAL" ) ) {
				check_DRIVE_LC_SCREWDRIVER_MANUAL();
			}
			// DRIVE_LCPB_AUTO
			else if( job.equalsIgnoreCase( "DRIVE_LCPB_AUTO" ) ) {
				check_DRIVE_LCPB_AUTO();
			}
			// DRIVE_LCPB_MANUAL
			else if( job.equalsIgnoreCase( "DRIVE_LCPB_MANUAL" ) ) {
				check_DRIVE_LCPB_MANUAL();
			}
			// START_LC_ADJUSTEMENT
			else if( job.equalsIgnoreCase( "START_LC_ADJUSTEMENT" ) ) {
				check_START_LC_ADJUSTEMENT();
			}
			// DRIVE_LC_HOME
			else if( job.equalsIgnoreCase( "DRIVE_LC_HOME" ) ) {
				check_DRIVE_LC_HOME();
			}
			// CONTROL_HHG
			else if( job.equalsIgnoreCase( "CONTROL_HHG" ) ) {
				check_CONTROL_HHG();
			}
			// START_ACC_ADJUSTEMENT
			else if( job.equalsIgnoreCase( "START_ACC_ADJUSTEMENT" ) ) {
				check_START_ACC_ADJUSTEMENT();
			}
			// DRIVE_HHG_HOME
			else if( job.equalsIgnoreCase( "DRIVE_HHG_HOME" ) ) {
				check_DRIVE_HHG_HOME();
			}
			// job undefined
			else {
				throw new Exception( "job " + job + " undefined!" );
			}
			return true;

		} catch( NumberFormatException e ) {
			log( true, "Error", "checking args, NumberFormatException: " + e.getMessage() );
			return false;
		} catch( Exception e ) {
			log( true, "Error", "checking args, Exception: " + e.getMessage() );
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info
	 *            , Information zur Ausf�hrung
	 * 
	 */
	public void execute( ExecutionInfo info ) {
		// CASCADE parameters
		String job = getArg( "JOB" );
		String message = (getArg( "MESSAGE" ) != null) ? getArg( "MESSAGE" ) : null;
		boolean loadHla = true;
		boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
		String deviceTag = (getArg("DEVICE_TAG") != null) ? getArg("DEVICE_TAG") : "";
		boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
		boolean lcCheck = true;

		// aux
		int status = STATUS_EXECUTION_OK;
		Vector resList = new Vector();
		UserDialog ud = null;
		int statusTimeoutSec = 40;

		if( job.equalsIgnoreCase( "LET_SERIAL_HLA" ) ) {
			loadHla = false;
		}

		HeadlightAiming hla = null;

		boolean german = System.getProperty( "user.language" ).equalsIgnoreCase( "de" ) ? true : false;
		String resAdvise = german ? "Bitte den Pruefstandsbetreuer benachrichtigen" : "Please inform the person responsible for the test stand";
		String deviceName = "";
		String dllResult = "";

		try {
			// -------------------------------------------------------------------------------------------
			// check args
			// -------------------------------------------------------------------------------------------
			log( debug, "Var", "---> " + getClassName() + this.getVersion() + " [sim: " + String.valueOf( simulation ) + "]" );
			if( !checkArgs() ) {
				throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			}
			log( debug, "Ok", "check args ok." );

			// -------------------------------------------------------------------------------------------
			// get devices - UserDialog & HeadlightAiming
			// -------------------------------------------------------------------------------------------
			try {
				// UserDialog
				// ----------------
				deviceName = "UserDialog";
				ud = getPr�flingLaufzeitUmgebung().getUserDialog();
				if( ud == null ) {
					throw new Exception( "null" );
				}
				log( debug, "Ok", "got " + deviceName + " device." );

				// HeadlightAiming
				// ----------------
				if( !simulation && loadHla ) {
					deviceName = "HeadlightAiming";
					if (deviceTag != ""){
						hla = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getHLA( deviceTag );
					}else{
						hla = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getHLA();
					}
					if( hla == null ) {
						throw new Exception( "null" );
					}
					log( debug, "Ok", "got " + deviceName + " device." );
				}

			} catch( DeviceLockedException e ) {
				log( debug, "Error", "getting " + deviceName + " device, DeviceLockedException: " + e.getMessage() );
				throw new PPExecutionException( deviceName + " " + PB.getString( "nichtVerfuegbar" ) );
			} catch( DeviceNotAvailableException e ) {
				log( debug, "Error", "getting " + deviceName + " device, DeviceNotAvailableException: " + e.getMessage() );
				throw new PPExecutionException( deviceName + " " + PB.getString( "nichtVerfuegbar" ) );
			} catch( Exception e ) {
				log( debug, "Error", "getting " + deviceName + " device, Exception: " + e.getMessage() );
				throw new PPExecutionException( deviceName + " " + PB.getString( "nichtVerfuegbar" ) );
			}

			// -------------------------------------------------------------------------------------------
			// check stand status
			// -------------------------------------------------------------------------------------------
			try {
				int sec = statusTimeoutSec;

				// cycle for status check
				while( sec >= 0 && !dllResult.startsWith( "IO" ) && !dllResult.startsWith( "IDLE" ) && !lcCheck ) {
					// don't sleep before first attempt
					if( sec < statusTimeoutSec ) {
						try {
							System.out.println( "Waiting for test stand to be ready..." );
							Thread.sleep( 1000 );
						} catch( InterruptedException e ) {
							e.printStackTrace();
						}
					}
					dllResult = (simulation || !loadHla) ? "IO" : hla.status().toUpperCase();
					log( debug, "Var", "HLA-Stand status(" + sec + " sec) = HLA: "+hla.toString()+", Result:" + dllResult );

					// check for general error
					if( dllResult.startsWith( "ERROR" ) ) {
						throw new Exception( "MachineStatusError: " + dllResult );
						
					// check for general error
					}else if( dllResult.startsWith( "BUSY" ) && dllResult.contains("MANUAL") ) {
						throw new Exception( "MachineStatusError: " + dllResult );
						// Exception beim Status "BUSY[manual mode]

						// check for Disconnection
					} else if( dllResult.contains( "DISCONNECTED" ) || dllResult.contains( "NOT CONNECTED" ) ) {
						// refresh device connection on first attempt
						if( sec == statusTimeoutSec ) {
							log( debug, "Ok", "Attempting to reconnect..." );
							this.getPr�flingLaufzeitUmgebung().getDeviceManager().releaseHLA();
							hla = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getHLA();
							log( debug, "Ok", "Device refreshed." );
						} else {
							throw new Exception( "MachineStatusError: " + dllResult );
						}
					}
					if (deviceTag.equalsIgnoreCase("LC")){
						if ((dllResult.startsWith("BUSY"))&&(dllResult.contains("LC")))
							lcCheck=false;
					}
					log( debug, "Ok", "LCCheck = " + lcCheck );
					sec--;
				}
				// check status after time expired
				if( !dllResult.startsWith( "IO" ) && !dllResult.startsWith( "IDLE" ) && !lcCheck ) {
					throw new Exception( "MachineStatusTimeout: " + dllResult );
				}
				log( debug, "Ok", "stand status ok: " + dllResult );

			} catch( Exception e ) {
				log( debug, "Error", "checking stand status, Exception: " + e.getMessage() );
				// show error (5 sec)
				String dlgTitle = german ? "Pruefstandsfehler" : "Test stand error";
				String dlgMsg = e.getMessage();
				ud.displayAlertMessage( dlgTitle, dlgMsg, 5 );
				throw new PPExecutionException( e.getMessage() );
			}

			// -------------------------------------------------------------------------------------------
			// job execution
			// -------------------------------------------------------------------------------------------
			log( debug, "Ok", "executing job... " + job );
			if( message != null ) {
				// show job message
				ud.displayStatusMessage( "HLA", message, -1 );
			}
			// ---------------------
			// SETUP
			// ---------------------
			if( job.equalsIgnoreCase( "SETUP" ) ) {
				status = execute_SETUP( hla, resList );
			}
			// ---------------------
			// READ_DEFAULT_VALUE
			// ---------------------
			else if( job.equalsIgnoreCase( "READ_DEFAULT_VALUE" ) ) {
				status = execute_READ_DEFAULT_VALUE( hla, resList );
			}
			// ---------------------
			// READ_IMAGE_VALUE
			// ---------------------
			else if( job.equalsIgnoreCase( "READ_IMAGE_VALUE" ) ) {
				status = execute_READ_IMAGE_VALUE( hla, resList );
			}
			// ---------------------
			// POSITION_LIGHT_BOX_HOME
			// ---------------------
			else if( job.equalsIgnoreCase( "POSITION_LIGHT_BOX_HOME" ) ) {
				status = execute_POSITION_LIGHT_BOX_HOME( hla, resList );
			}
			// ---------------------
			// POSITION_LIGHT_BOX
			// ---------------------
			else if( job.equalsIgnoreCase( "POSITION_LIGHT_BOX" ) ) {
				status = execute_POSITION_LIGHT_BOX( hla, resList );
			}
			// ---------------------
			// POSITION_ACC_MIRROR
			// ---------------------
			else if( job.equalsIgnoreCase( "POSITION_ACC_MIRROR" ) ) {
				status = execute_POSITION_ACC_MIRROR( hla, resList );
			}
			// ---------------------
			// PERFORM_AIMING_PROCESS
			// ---------------------
			else if( job.equalsIgnoreCase( "PERFORM_AIMING_PROCESS" ) ) {
				status = execute_PERFORM_AIMING_PROCESS( hla, resList );
			}
			// ---------------------
			// CONTROL_CENTRATION
			// ---------------------
			// Fahrzeug (de-)zentrieren
			else if( job.equalsIgnoreCase( "CONTROL_CENTRATION" ) ) {
				status = execute_CONTROL_CENTRATION( hla, resList );
			}
			// ---------------------
			// CONTROL_VISUALIZATION
			// ---------------------
			else if( job.equalsIgnoreCase( "CONTROL_VISUALIZATION" ) ) {
				status = execute_CONTROL_VISUALIZATION( hla, resList );
			}
			// ---------------------
			// CONTROL_TRAFFIC_LIGHTS
			// ---------------------
			else if( job.equalsIgnoreCase( "CONTROL_TRAFFIC_LIGHTS" ) ) {
				status = execute_CONTROL_TRAFFIC_LIGHTS( hla, resList );
			}
			// ---------------------
			// START_HEIGHT_MEASUREMENT
			// ---------------------
			else if( job.equalsIgnoreCase( "START_HEIGHT_MEASUREMENT" ) ) {
				status = execute_START_HEIGHT_MEASUREMENT( hla, resList );
			}
			// ---------------------
			// MEASURE_VEHICLE
			// ---------------------
			else if( job.equalsIgnoreCase( "MEASURE_VEHICLE" ) ) {
				status = execute_MEASURE_VEHICLE( hla, resList );
			}
			// ---------------------
			// MEASURE_STAND
			// ---------------------
			else if( job.equalsIgnoreCase( "MEASURE_STAND" ) ) {
				status = execute_MEASURE_STAND( hla, resList );
			}
			// ---------------------
			// POSITION_MIRROR_HOME
			// ---------------------
			else if( job.equalsIgnoreCase( "POSITION_MIRROR_HOME" ) ) {
				status = execute_POSITION_MIRROR_HOME( hla, resList );
			}
			// ---------------------
			// SET_THRUST_ANGLE
			// ---------------------
			else if( job.equalsIgnoreCase( "SET_THRUST_ANGLE" ) ) {
				status = execute_SET_THRUST_ANGLE( hla, resList );
			}
			// ---------------------
			// DISPLAY_MESSAGE
			// ---------------------
			else if( job.equalsIgnoreCase( "DISPLAY_MESSAGE" ) ) {
				status = execute_DISPLAY_MESSAGE( hla, resList );
			}
			// ---------------------
			// LET_SERIAL_HLA
			// ---------------------
			else if( job.equalsIgnoreCase( "LET_SERIAL_HLA" ) ) {
				status = execute_LET_SERIAL_HLA( debug, hla, resList );
			}
			// ---------------------
			// ACTIVATE_NIVI_TARGET
			// ---------------------
			else if( job.equalsIgnoreCase( "ACTIVATE_NIVI_TARGET" ) ) {
				status = execute_ACTIVATE_NIVI_TARGET( hla, resList );
			}
			// ---------------------
			// POSITION_NIVI_TARGET
			// ---------------------
			else if( job.equalsIgnoreCase( "POSITION_NIVI_TARGET" ) ) {
				status = execute_POSITION_NIVI_TARGET( hla, resList );
			}
			// ---------------------
			// POSITION_NIVI_TARGET_HOME
			// ---------------------
			else if( job.equalsIgnoreCase( "POSITION_NIVI_TARGET_HOME" ) ) {
				status = execute_POSITION_NIVI_TARGET_HOME( hla, resList );
			}
			// ---------------------
			// CONTROL_MOVEABLE_TRACKS
			// ---------------------
			else if( job.equalsIgnoreCase( "CONTROL_MOVEABLE_TRACKS" ) ) {
				status = execute_ACTIVATE_MOVEABLE_TRACKS( hla, resList );
			}
			// ---------------------
			// DRIVE_LC_SCREWDRIVER_AUTO
			// ---------------------
			else if( job.equalsIgnoreCase( "DRIVE_LC_SCREWDRIVER_AUTO" ) ) {
				status = execute_DRIVE_LC_SCREWDRIVER_AUTO( hla, resList );
			}
			// ---------------------
			// DRIVE_LC_SCREWDRIVER_MANUAL
			// ---------------------
			else if( job.equalsIgnoreCase( "DRIVE_LC_SCREWDRIVER_MANUAL" ) ) {
				status = execute_DRIVE_LC_SCREWDRIVER_MANUAL( hla, resList );
			}
			// ---------------------
			// DRIVE_LCPB_AUTO
			// ---------------------
			else if( job.equalsIgnoreCase( "DRIVE_LCPB_AUTO" ) ) {
				status = execute_DRIVE_LCPB_AUTO( hla, resList );
			}
			// ---------------------
			// DRIVE_LCPB_MANUAL
			// ---------------------
			else if( job.equalsIgnoreCase( "DRIVE_LCPB_MANUAL" ) ) {
				status = execute_DRIVE_LCPB_MANUAL( hla, resList );
			}
			// ---------------------
			// START_LC_ADJUSTEMENT
			// ---------------------
			else if( job.equalsIgnoreCase( "START_LC_ADJUSTEMENT" ) ) {
				status = execute_START_LC_ADJUSTEMENT( hla, resList );
			}
			// ---------------------
			// DRIVE_LC_HOME
			// ---------------------
			else if( job.equalsIgnoreCase( "DRIVE_LC_HOME" ) ) {
				status = execute_DRIVE_LC_HOME( hla, resList );
			}
			// ---------------------
			// CONTROL_HHG
			// ---------------------
			else if( job.equalsIgnoreCase( "CONTROL_HHG" ) ) {
				status = execute_CONTROL_HHG( hla, resList );
			}
			// ---------------------
			// START_ACC_ADJUSTEMENT
			// ---------------------
			else if( job.equalsIgnoreCase( "START_ACC_ADJUSTEMENT" ) ) {
				status = execute_START_ACC_ADJUSTEMENT( hla, resList );
			}
			// ---------------------
			// DRIVE_HHG_HOME
			// ---------------------
			else if( job.equalsIgnoreCase( "DRIVE_HHG_HOME" ) ) {
				status = execute_DRIVE_HHG_HOME( hla, resList );
			}
		} catch( PPExecutionException e ) {
			// NIO
			status = STATUS_EXECUTION_ERROR;
			resList.add( new Ergebnis( "ExecError", "HeadlightAiming", job, "PPExecutionException", (e.getMessage() != null ? e.getMessage() : ""), "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), resAdvise, Ergebnis.FT_NIO ) );
			log( debug, "Error", "executing procedure, PPExecutionException: " + e.getMessage() );
		} catch( Exception e ) {
			// NIO_SYS
			status = STATUS_EXECUTION_ERROR;
			resList.add( new Ergebnis( "ExecError", "HeadlightAiming", job, "Exception", (e.getMessage() != null ? e.getMessage() : ""), "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler" ), resAdvise, Ergebnis.FT_NIO_SYS ) );
			log( debug, "Error", "executing procedure, Exception: " + e.getMessage() );
		} catch( Throwable e ) {
			// NIO_SYS
			status = STATUS_EXECUTION_ERROR;
			resList.add( new Ergebnis( "ExecError", "HeadlightAiming", job, "Throwable", (e.getMessage() != null ? e.getMessage() : ""), "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler" ), resAdvise, Ergebnis.FT_NIO_SYS ) );
			log( debug, "Error", "executing procedure, Throwable: " + e.getMessage() );
		}

		// -------------------------------------------------------------------------------------------
		// release devices - UserDialog & HeadlightAiming
		// -------------------------------------------------------------------------------------------
		try {
			// UserDialog
			// ----------------
			if( ud != null ) {
				deviceName = "UserDialog";
				this.getPr�flingLaufzeitUmgebung().releaseUserDialog();
				log( debug, "Ok", "released " + deviceName + " device." );
			}
			// HeadlightAiming
			// ----------------
			if( !simulation && loadHla && hla != null ) {
				deviceName = "HeadlightAiming";
				this.getPr�flingLaufzeitUmgebung().getDeviceManager().releaseHLA();
				hla = null;
				log( debug, "Ok", "released " + deviceName + " device." );
			}

		} catch( DeviceLockedException e ) {
			log( debug, "Error", "releasing " + deviceName + " device, DeviceLockedException: " + e.getMessage() );
		} catch( Exception e ) {
			log( debug, "Error", "releasing " + deviceName + " device, Exception: " + e.getMessage() );
		}

		// -------------------------------------------------------------------------------------------
		// set and log results
		// -------------------------------------------------------------------------------------------
		log( debug, "Var", "Execution status: " + (status == STATUS_EXECUTION_OK ? "Ok" : "Error") );
		for( int i = 0; i < resList.size(); i++ ) {
			log( debug, "Var", "APDM result " + i + ":\n" + ((Ergebnis) resList.get( i )).toString() );
		}
		setPPStatus( info, status, resList );
	}

	// ////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// CHECK JOBS
	//
	// ////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * <b>check_SETUP</b> <br>
	 * - req: CMD <br>
	 */
	private void check_SETUP() throws Exception {
		final String[] CMD = { "", "Set", "Unset" };

		// CMD defined ?
		if( getOptionInt( CMD, extractArg( "CMD" ) ) == -1 ) {
			throw new Exception( "CMD invalid! " + extractArg( "CMD" ) + " (opts: " + getOptionsString( CMD ) + ")" );
		}
	}

	/**
	 * <b>check_READ_DEFAULT_VALUE</b> <br>
	 * - req: ID, LIGHT <br>
	 */
	private void check_READ_DEFAULT_VALUE() throws Exception {
		final String[] ID = { "Target", "Tolerance", "Offset", "AlgoInfo", "ScrewDriver", "PosLightBox" };
		final String[] LIGHT = { "HL_Left", "HL_Right", "FL_Left", "FL_Right", "HB_Left", "HB_Right", "GZA_Left", "GZA_Right" };

		// ID defined ?
		if( getOptionInt( ID, extractArg( "ID" ) ) == -1 ) {
			throw new Exception( "ID invalid! " + extractArg( "ID" ) + " (opts: " + getOptionsString( ID ) + ")" );
		}
		// LIGHT defined ?
		if( getOptionInt( LIGHT, extractArg( "LIGHT" ) ) == -1 ) {
			throw new Exception( "LIGHT invalid! " + extractArg( "LIGHT" ) + " (opts: " + getOptionsString( LIGHT ) + ")" );
		}
	}

	/**
	 * <b>check_READ_IMAGE_VALUE</b> <br>
	 * - req: ID, LIGHT <br>
	 */
	private void check_READ_IMAGE_VALUE() throws Exception {
		final String[] ID = { "ImageInfo", "ImageGradient", "ImageColor" };
		final String[] LIGHT = { "HL_Left", "HL_Right", "FL_Left", "FL_Right", "HB_Left", "HB_Right", "GZA_Left", "GZA_Right" };

		// ID defined ?
		if( getOptionInt( ID, extractArg( "ID" ) ) == -1 ) {
			throw new Exception( "ID invalid! " + extractArg( "ID" ) + " (opts: " + getOptionsString( ID ) + ")" );
		}
		// LIGHT defined ?
		if( getOptionInt( LIGHT, extractArg( "LIGHT" ) ) == -1 ) {
			throw new Exception( "LIGHT invalid! " + extractArg( "LIGHT" ) + " (opts: " + getOptionsString( LIGHT ) + ")" );
		}
	}

	/**
	 * <b>check_POSITION_LIGHT_BOX_HOME</b> <br>
	 */
	private void check_POSITION_LIGHT_BOX_HOME() throws Exception {
		// nothing to be tested...
	}
	
	/**
	 * <b>check_START_HEIGHT_MEASUREMENT</b> <br>
	 */
	private void check_START_HEIGHT_MEASUREMENT() throws Exception {
		// nothing to be tested...
	}

	/**
	 * <b>check_POSITION_LIGHT_BOX</b> <br>
	 * - req: ID, (LIGHT), (X), (Y), (Z) <br>
	 */
	private void check_POSITION_LIGHT_BOX() throws Exception {
		final String[] ID = { "Auto", "Manual" };
		final String[] LIGHT = { "HL_Left", "HL_Right", "FL_Left", "FL_Right", "HB_Left", "HB_Right", "GZA_Left", "GZA_Right" };

		// ID defined ?
		if( getOptionInt( ID, extractArg( "ID" ) ) == -1 ) {
			throw new Exception( "ID invalid! " + extractArg( "ID" ) + " (opts: " + getOptionsString( ID ) + ")" );
		}
		if( extractArg( "ID" ).equalsIgnoreCase( "Auto" ) ) {
			// LIGHT defined ?
			if( getOptionInt( LIGHT, extractArg( "LIGHT" ) ) == -1 ) {
				throw new Exception( "LIGHT invalid! " + extractArg( "LIGHT" ) + " (opts: " + getOptionsString( LIGHT ) + ")" );
			}
		} else {
			// X float ?
			new Float( extractArg( "X" ) ).floatValue();
			// Y float ?
			new Float( extractArg( "Y" ) ).floatValue();
			// Z float ?
			new Float( extractArg( "Z" ) ).floatValue();
		}
	}

	/**
	 * <b>check_CONTROL_CENTRATION</b> <br>
	 * - req: CMD <br>
	 */
	private void check_CONTROL_CENTRATION() throws Exception {
		final String[] CMD = { "", "Uncenter", "CenterNormal", "CenterStrong" };

		// CMD defined ?
		if( getOptionInt( CMD, extractArg( "CMD" ) ) == -1 ) {
			throw new Exception( "CMD invalid! " + extractArg( "CMD" ) + " (opts: " + getOptionsString( CMD ) + ")" );
		}
	}

	/**
	 * <b>check_CONTROL_TRAFFIC_LIGHTS</b> <br>
	 * - req: CMD <br>
	 */
	private void check_CONTROL_TRAFFIC_LIGHTS() throws Exception {
		final String[] CMD = { "", "Green", "Yellow", "Red" };

		// CMD defined ?
		if( getOptionInt( CMD, extractArg( "CMD" ) ) == -1 ) {
			throw new Exception( "CMD invalid! " + extractArg( "CMD" ) + " (opts: " + getOptionsString( CMD ) + ")" );
		}
	}

	/**
	 * <b>check_CONTROL_VISUALIZATION</b> <br>
	 * - req: CMD <br>
	 */
	private void check_CONTROL_VISUALIZATION() throws Exception {
		final String[] CMD = { "", "Off", "On" };

		// CMD defined ?
		if( getOptionInt( CMD, extractArg( "CMD" ) ) == -1 ) {
			throw new Exception( "CMD invalid! " + extractArg( "CMD" ) + " (opts: " + getOptionsString( CMD ) + ")" );
		}
	}

	/**
	 * <b>check_POSITION_ACC_MIRROR</b> <br>
	 * - req: X, Y, Z, PHI, THETA <br>
	 */
	private void check_POSITION_ACC_MIRROR() throws Exception {
		// X float ?
		new Float( extractArg( "X" ) ).floatValue();
		// Y float ?
		new Float( extractArg( "Y" ) ).floatValue();
		// Z float ?
		new Float( extractArg( "Z" ) ).floatValue();
		// PHI float ?
		new Float( extractArg( "PHI" ) ).floatValue();
		// THETA float ?
		new Float( extractArg( "THETA" ) ).floatValue();
	}

	/**
	 * <b>check_PERFORM_AIMING_PROCESS</b> <br>
	 * - req: ID, CMD, LIGHT, - opt: WORK_DIRECTORY <br>
	 */
	private void check_PERFORM_AIMING_PROCESS() throws Exception {
		final String[] ID = { "Auto", "Manual" };
		final String[] CMD = { "Side", "Height", "Both" };
		final String[] LIGHT = { "HL_Left", "HL_Right", "FL_Left", "FL_Right", "HB_Left", "HB_Right", "GZA_Left", "GZA_Right" };

		// ID defined ?
		if( getOptionInt( ID, getArg( "ID" ) ) == -1 ) {
			throw new Exception( "ID invalid! " + getArg( "ID" ) + " (opts: " + getOptionsString( ID ) + ")" );
		}
		// CMD defined ?
		if( getOptionInt( CMD, extractArg( "CMD" ) ) == -1 ) {
			throw new Exception( "CMD invalid! " + extractArg( "CMD" ) + " (opts: " + getOptionsString( CMD ) + ")" );
		}
		// LIGHT defined ?
		if( getOptionInt( LIGHT, extractArg( "LIGHT" ) ) == -1 ) {
			throw new Exception( "LIGHT invalid! " + extractArg( "LIGHT" ) + " (opts: " + getOptionsString( LIGHT ) + ")" );
		}
		// REDUCE_APDM_OUTPUT properly defined ?
		if( extractArg( "REDUCE_APDM_OUTPUT" ) != null ) {
			int reduceApdmOutput = new Float( extractArg( "REDUCE_APDM_OUTPUT" ) ).intValue();
			if( reduceApdmOutput < 0 || reduceApdmOutput > 2 )
				throw new Exception( "Parameter REDUCE_APDM_OUTPUT only accepts 0, 1 or 2 as values." );
		}

		if( extractArg( "WORK_DIRECTORY" ) != null ) {
			// WORK_DIRECTORY valid ?
			File workDir = new File( extractArg( "WORK_DIRECTORY" ) );
			if( !workDir.exists() ) {
				// create...
				if( !workDir.mkdir() ) {
					throw new Exception( "WORK_DIRECTORY invalid! (unable to create...): " + extractArg( "WORK_DIRECTORY" ) );
				}
				log( true, "Ok", "work dir created. " + workDir.getAbsolutePath() );
			} else {
				// already exists...
				if( !workDir.isDirectory() ) {
					throw new Exception( "WORK_DIRECTORY invalid! (not a directory...): " + extractArg( "WORK_DIRECTORY" ) );
				}
				if( !workDir.canWrite() ) {
					throw new Exception( "WORK_DIRECTORY invalid! (not writable...): " + extractArg( "WORK_DIRECTORY" ) );
				}
			}
		}
	}

	/**
	 * <b>check_MEASURE_VEHICLE</b> <br>
	 */
	private void check_MEASURE_VEHICLE() throws Exception {
		// nothing to be tested...
	}

	/**
	 * <b>check_MEASURE_STAND</b> <br>
	 */
	private void check_MEASURE_STAND() throws Exception {
		// nothing to be tested...
	}

	/**
	 * <b>check_POSITION_MIRROR_HOME</b> <br>
	 */
	private void check_POSITION_MIRROR_HOME() throws Exception {
		// nothing to be tested...
	}

	/**
	 * <b>check_SET_THRUST_ANGLE</b> <br>
	 */
	private void check_SET_THRUST_ANGLE() throws Exception {
		// Thrust Angle float ?
		new Float( extractArg( "THRUST_ANGLE" ) ).floatValue();
	}

	/**
	 * <b>check_DISPLAY_MESSAGE</b> <br>
	 */
	private void check_DISPLAY_MESSAGE() throws Exception {
		// Message defined ?
		if( getArg( "MESSAGE" ) == null ) {
			throw new Exception( "No message text defined!" );
		}

		// Buttons properly defined ?
		int buttons = new Integer( extractArg( "BUTTONS" ) ).intValue();
		if( buttons <= 0 || buttons >= 8 ) {
			throw new Exception( "Parameter BUTTONS only accepts values 1..7)." );
		}
	}

	private void check_LET_SERIAL_HLA() throws Exception {
		new Integer( extractArg( "VSN" ) ).intValue();
		if( extractArg( "VSN" ).length() > 3 )
			throw new Exception( "Argument 'VSN' is too long (max. length three digits)." );
		if( extractArg( "TIMEOUT_SEC" ) != null )
			new Integer( extractArg( "TIMEOUT_SEC" ) ).intValue();
	}

	private void check_SIMULATE_SERIAL_HLA() throws Exception {
		// nothing to be tested...
	}

	private void check_ACTIVATE_NIVI_TARGET() throws Exception {
		final String[] CMD = { "", "Deactivate", "Activate" };

		// CMD defined ?
		if( getOptionInt( CMD, extractArg( "CMD" ) ) == -1 ) {
			throw new Exception( "CMD invalid! " + extractArg( "CMD" ) + " (opts: " + getOptionsString( CMD ) + ")" );
		}
	}

	private void check_POSITION_NIVI_TARGET() throws Exception {
		// X float ?
		new Float( extractArg( "X" ) ).floatValue();
		// Y float ?
		new Float( extractArg( "Y" ) ).floatValue();
		// Z float ?
		new Float( extractArg( "Z" ) ).floatValue();
	}

	private void check_POSITION_NIVI_TARGET_HOME() throws Exception {
		// nothing to be tested...
	}
	
	private void check_ACTIVATE_MOVEABLE_TRACKS() throws Exception {
		final String[] CMD = { "Close", "Open"};
	
		// CMD defined ?
				if( getOptionInt( CMD, extractArg( "CMD" ) ) == -1 ) {
					throw new Exception( "CMD invalid! " + extractArg( "CMD" ) + " (opts: " + getOptionsString( CMD ) + ")" );
				}
	}
	
	private void check_DRIVE_LC_SCREWDRIVER_AUTO() throws Exception {
			
		// CMD defined ?
				if( extractArg( "CMD" ) == null ) {
					throw new Exception( "CMD invalid! " + extractArg( "CMD" ) );
				}
	}
	
	private void check_DRIVE_LC_SCREWDRIVER_MANUAL() throws Exception {
		
		// CMD defined ?
		if( extractArg( "CMD" ) == null ) {
			throw new Exception( "CMD invalid! " + extractArg( "CMD" ) );
		}
		
		// X float ?
		new Float( extractArg( "X" ) ).floatValue();
		// Y float ?
		new Float( extractArg( "Y" ) ).floatValue();
		// Z float ?
		new Float( extractArg( "Z" ) ).floatValue();
	}
	
	private void check_DRIVE_LCPB_AUTO() throws Exception {
		
		// CMD defined ?
				if( extractArg( "CMD" ) == null ) {
					throw new Exception( "CMD invalid! " + extractArg( "CMD" ) );
				}
	}
	
	private void check_DRIVE_LCPB_MANUAL() throws Exception {
		
		// CMD defined ?
		if( extractArg( "CMD" ) == null ) {
			throw new Exception( "CMD invalid! " + extractArg( "CMD" ) );
		}
		
		// X float ?
		new Float( extractArg( "X" ) ).floatValue();
		// Y float ?
		new Float( extractArg( "Y" ) ).floatValue();
		// Z float ?
		new Float( extractArg( "Z" ) ).floatValue();
	}
	
	private void check_DRIVE_LC_HOME() throws Exception {
		// nothing to be tested...
	}
	
	private void check_DRIVE_HHG_HOME() throws Exception {
		// nothing to be tested...
	}
	
	private void check_CONTROL_HHG() throws Exception {
		// X float ?
		new Float( extractArg( "X" ) ).floatValue();
		// Y float ?
		new Float( extractArg( "Y" ) ).floatValue();
		// Z float ?
		new Float( extractArg( "Z" ) ).floatValue();
	}
	
	private void check_START_ACC_ADJUSTEMENT() throws Exception {
		// X float ?
		new Float( extractArg( "THETA" ) ).floatValue();
	}
	
	private void check_START_LC_ADJUSTEMENT() throws Exception {
		// CMD defined ?
		if( extractArg( "CMD" ) == null ) {
			throw new Exception( "CMD invalid! " + extractArg( "CMD" ) );
		}
	}

	// ////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// EXECUTE JOBS
	//
	// ////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * <b>execute_SETUP</b> <br>
	 * - req: CMD, TIMEOUT <br>
	 * 
	 * @param ud
	 *            - UserDialog device
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_SETUP( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			String cmd = getArg( "CMD" );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = cmd + "_SetupOk";
			String resError = "";
			String dllResult = "";
			String orderData = "";
			String orderSaSx = "";

			// --------------------------------------------------------------
			// build auftrag xml data
			orderData = getOrderData( debug, this.getPr�fling().getAuftrag() );
			orderSaSx = getOrderSaSx( debug, this.getPr�fling().getAuftrag() );

			// execute dll function...
			dllResult = (simulation) ? "IO" : (cmd.equalsIgnoreCase( "Set" )) ? hla.selectParamSet( orderData, orderSaSx ) : hla.deselectParam();
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( !dllResult.equalsIgnoreCase( "IO" ) ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = cmd + "_SetupNOk";
				resError = dllResult;
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}
			resList.add( new Ergebnis( "StatusSetup", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), "IO", "IO", "0", "", "", "", resError, "", resType ) );

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_READ_DEFAULT_VALUE</b> <br>
	 * - req: ID, LIGHT <br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_READ_DEFAULT_VALUE( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		final String[] DIRECTION = { "SIDE", "HEIGHT" };
		final String[] AXIS = { "Y", "Z" };
		final String[] DETAIL = { "MIN", "MAX" };
		final String[] XMLTAG_ALGO = { "METHOD", "PARAMETER" };
		final String[] XMLTAG_SCREW = { "PORT", "TORQUE_MAX", "TORQUE_MIN", "SPEED_MAX", "SPEED_MIN", "TURN_OFF_POINT", "RAMP", "ROTATION_DIRECTION" };
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			String id = getArg( "ID" );
			String[] light = getArg( "LIGHT" ).split( "_" ); // 0 -> lightSrc, 1 ->
			// lightPos
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IGNORE;
			String resName = "";
			String resValue = "";
			String dllResult = "";

			// --------------------------------------------------------------
			// execute dll function...
			// -> AlgoInfo...
			if( id.equalsIgnoreCase( "AlgoInfo" ) ) {
				dllResult = (simulation) ? paramXmlSerialization( XMLTAG_ALGO, "STRING" ) : String.valueOf( hla.readAlgorithmInfo( light[SRC], light[POS] ) );
				for( int p = 0; p < XMLTAG_ALGO.length; p++ ) {
					resName = "DEF_" + light[SRC] + "_" + light[POS] + "_" + id + "_" + XMLTAG_ALGO[p];
					resValue = paramXmlDeserialization( dllResult, XMLTAG_ALGO[p] );
					if( resValue != null && !resValue.equalsIgnoreCase( "" ) ) {
						resList.add( new Ergebnis( resName, "HeadlightAiming", job, "", "", resName, resValue, "", "", "0", "", "", "", "", "", resType ) );
						log( debug, "Var", "set " + resName + " = " + resValue );
					}
				}
				// -> Tolerance...
			} else if( id.equalsIgnoreCase( "Tolerance" ) ) {
				for( int d = 0; d < DIRECTION.length; d++ ) {
					for( int t = 0; t < DETAIL.length; t++ ) {
						dllResult = (simulation) ? "0.1" : String.valueOf( hla.readToleranceValue( light[SRC], light[POS], DIRECTION[d], DIRECTION[t] ) );
						resName = "DEF_" + light[SRC] + "_" + light[POS] + "_" + id + "_" + AXIS[d] + "_" + DETAIL[t];
						resValue = dllResult;
						if( resValue != null && !resValue.equalsIgnoreCase( "" ) ) {
							resList.add( new Ergebnis( resName, "HeadlightAiming", job, "", "", resName, resValue, "", "", "0", "", "", "", "", "", resType ) );
							log( debug, "Var", "set " + resName + " = " + resValue );
						}
					}
				}
				// -> ...
			} else {
				for( int d = 0; d < DIRECTION.length; d++ ) {
					// -> ScrewDriver...
					if( id.equalsIgnoreCase( "ScrewDriver" ) ) {
						dllResult = (simulation) ? paramXmlSerialization( XMLTAG_SCREW, "FLOAT" ) : String.valueOf( hla.readScrewDriverParam( light[SRC], light[POS], DIRECTION[d] ) );
						for( int p = 0; p < XMLTAG_SCREW.length; p++ ) {
							resName = "DEF_" + light[SRC] + "_" + light[POS] + "_" + id + "_" + AXIS[d] + "_" + XMLTAG_SCREW[p];
							resValue = paramXmlDeserialization( dllResult, XMLTAG_SCREW[p] );
							if( resValue != null && !resValue.equalsIgnoreCase( "" ) ) {
								resList.add( new Ergebnis( resName, "HeadlightAiming", job, "", "", resName, resValue, "", "", "0", "", "", "", "", "", resType ) );
								log( debug, "Var", "set " + resName + " = " + resValue );
							}
						}
						// ->...
					} else {
						// -> Target...
						if( id.equalsIgnoreCase( "Target" ) ) {
							dllResult = (simulation) ? "0.0" : String.valueOf( hla.readTargetValue( light[SRC], light[POS], DIRECTION[d] ) );
							// -> Offset...
						} else if( id.equalsIgnoreCase( "Offset" ) ) {
							dllResult = (simulation) ? "0.2" : String.valueOf( hla.readOffsetValue( light[SRC], light[POS], DIRECTION[d] ) );
							// -> PosLightBox...
						} else if( id.equalsIgnoreCase( "PosLightBox" ) ) {
							dllResult = (simulation) ? "0.5" : String.valueOf( hla.readPresetLightBoxPos( light[SRC], light[POS], AXIS[d] ) );
						}
						resName = "DEF_" + light[SRC] + "_" + light[POS] + "_" + id + "_" + AXIS[d];
						resValue = dllResult;
						if( resValue != null && !resValue.equalsIgnoreCase( "" ) ) {
							resList.add( new Ergebnis( resName, "HeadlightAiming", job, "", "", resName, resValue, "", "", "0", "", "", "", "", "", resType ) );
							log( debug, "Var", "set " + resName + " = " + resValue );
						}
					}
				}
			}

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_READ_IMAGE_VALUE</b> <br>
	 * - req: ID, LIGHT <br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_READ_IMAGE_VALUE( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		final String[] XMLTAG_INFO = { "HV_POS_SIDE", "HV_POS_HEIGHT", "HV_DIFF_SIDE", "HV_DIFF_HEIGHT", "HOTSPOT_POS_SIDE", "HOTSPOT_POS_HEIGHT", "FOGLAMP_HEIGHT" };
		final String[] XMLTAG_GRADIENT = { "GRAD_40", "GRAD_25", "GRAD_10", "GRAD_HV", "GRAD_POS_40_HEIGHT", "GRAD_POS_25_HEIGHT", "GRAD_POS_10_HEIGHT", "GRAD_POS_HV_HEIGHT" };
		final String[] XMLTAG_COLOR = { "COLOR_AVG" };
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			String id = getArg( "ID" );
			String light = getArg( "LIGHT" );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IGNORE;
			String resName = "";
			String resValue = "";
			String dllResult = "";
			String[] xmlTag = null;

			// --------------------------------------------------------------
			// execute dll function...
			// -> ImageInfo...
			if( id.equalsIgnoreCase( "ImageInfo" ) ) {
				xmlTag = XMLTAG_INFO;
				dllResult = (simulation) ? paramXmlSerialization( XMLTAG_INFO, "STRING" ) : hla.readImageInfo( 1 );
				// -> ImageGradient...
			} else if( id.equalsIgnoreCase( "ImageGradient" ) ) {
				xmlTag = XMLTAG_GRADIENT;
				dllResult = (simulation) ? paramXmlSerialization( XMLTAG_GRADIENT, "STRING" ) : hla.readGradient();
				// -> ImageColor...
			} else if( id.equalsIgnoreCase( "ImageColor" ) ) {
				xmlTag = XMLTAG_COLOR;
				dllResult = (simulation) ? paramXmlSerialization( XMLTAG_COLOR, "STRING" ) : hla.readColor();
			}

			// set vars...
			for( int p = 0; p < xmlTag.length; p++ ) {
				resName = light + "_" + xmlTag[p] + (id.equalsIgnoreCase( "ImageInfo" ) ? "%" : "");
				resValue = paramXmlDeserialization( dllResult, xmlTag[p] );
				if( resValue != null && !resValue.equalsIgnoreCase( "" ) ) {
					resList.add( new Ergebnis( resName, "HeadlightAiming", job, "", "", resName, resValue, "", "", "0", "", "", "", "", "", resType ) );
					log( debug, "Var", "set " + resName + " = " + resValue );
				}
			}

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_POSITION_LIGHT_BOX_HOME</b> <br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_POSITION_LIGHT_BOX_HOME( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "PosLightBoxHomeOk";
			String resError = "";
			String dllResult = "IO";

			// --------------------[positionLB]----------------------
			// execute dll function...
			dllResult = (simulation) ? "IO" : hla.driveLightBoxAuto( "", "HOME_POSITION" ); // select light?
			log( debug, "Var", "dllResult[positionLB] = " + dllResult );

			// check dll result...
			if( !dllResult.equalsIgnoreCase( "IO" ) ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "PosLightBoxHomeNOk";
				resError = dllResult;
				log( debug, "Error", resError + " (dllResult[positionLB] = " + dllResult + ")" );
			}
			resList.add( new Ergebnis( "StatusPosLightBoxHome", "HeadlightAiming", job, "AUTO", "HOME", resName, dllResult, "IO", "IO", "0", "", "", "", resError, "", resType ) );

			// ------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}
	
	
	/**
	 * <b>execute_START_HEIGHT_MEASUREMENT</b> <br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_START_HEIGHT_MEASUREMENT( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "StartHeightMeasurementOk";
			String resError = "";
			int dllResult = 0 ;

			// --------------------[positionLB]----------------------
			// execute dll function...
			dllResult = (simulation) ? 0 : hla.startHeightMeasurement(); // select light?			
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( dllResult != 1 ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "StartHeightMeasurementNOk";
				resError = String.valueOf( dllResult );
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}
			resList.add( new Ergebnis( "StatusStartHeightMeasurement", "HeadlightAiming", job, "AUTO", "HOME", resName, String.valueOf( dllResult ), "IO", "IO", "0", "", "", "", resError, "", resType ) );

			// ------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_POSITION_LIGHT_BOX</b> <br>
	 * - req: ID, (LIGHT), (Y), (Z) <br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_POSITION_LIGHT_BOX( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		final String[] AXIS = { "Y", "Z" };
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			String id = getArg( "ID" );
			String[] light = (getArg( "LIGHT" ) != null) ? getArg( "LIGHT" ).split( "_" ) : null;
			float x = (extractArg( "X" ) != null) ? new Float( extractArg( "X" ) ).floatValue() : Float.NaN; // mm
			float y = (extractArg( "Y" ) != null) ? new Float( extractArg( "Y" ) ).floatValue() : Float.NaN; // mm
			float z = (extractArg( "Z" ) != null) ? new Float( extractArg( "Z" ) ).floatValue() : Float.NaN; // mm
			boolean apdmOutput = (getArg( "REDUCE_APDM_OUTPUT" ) != null && getArg( "REDUCE_APDM_OUTPUT" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String posID = (id.equalsIgnoreCase( "AUTO" ) ? light[SRC] + "_" + light[POS] : "MANUAL");
			String resType = Ergebnis.FT_IO;
			String resName = posID + "_PosLightBoxOk";
			String resError = "";
			String dllResult = "IO";

			// --------------------[positionLB]----------------------
			// execute dll function...
			dllResult = (simulation) ? "IO" : id.equalsIgnoreCase( "AUTO" ) ? hla.driveLightBoxAuto( light[SRC], light[POS] ) : hla.driveLightBoxManual( y, z );
			log( debug, "Var", "dllResult[positionLB] = " + dllResult );

			// check dll result...
			if( !dllResult.equalsIgnoreCase( "IO" ) ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = posID + "_PosLightBoxNOk";
				resError = dllResult;
				log( debug, "Error", resError + " (dllResult[positionLB] = " + String.valueOf( dllResult ) + ")" );
			}
			resList.add( new Ergebnis( "StatusPosLightBox", "HeadlightAiming", job, "", "", resName, dllResult, "IO", "IO", "0", "", "", "", resError, "", resType ) );

			// ------------------[getPositionLB]---------------------
			// execute dll function...
			for( int a = 0; a < AXIS.length; a++ ) {
				dllResult = String.valueOf( (simulation) ? a : hla.getPositionLightBox( AXIS[a] ) );
				log( debug, "Var", "dllResult[getPositionLB] = " + dllResult );
				resName = posID + "_LIGHT_BOX_POS_" + AXIS[a];
				if( !apdmOutput )
					resList.add( new Ergebnis( resName, "HeadlightAiming", job, "", "", resName, dllResult, "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
				log( debug, "Var", "set " + resName + " = " + dllResult );
			}

			// ------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_POSITION_ACC_MIRROR</b> <br>
	 * - req: X, Y, Z, PHI, THETA <br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_POSITION_ACC_MIRROR( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			int x = new Integer( extractArg( "X" ) ).intValue(); // mm
			int y = new Integer( extractArg( "Y" ) ).intValue(); // mm
			int z = new Integer( extractArg( "Z" ) ).intValue(); // mm
			int phi = new Integer( extractArg( "PHI" ) ).intValue(); // �
			int theta = new Integer( extractArg( "THETA" ) ).intValue(); // �
			boolean apdmOutput = (getArg( "REDUCE_APDM_OUTPUT" ) != null && getArg( "REDUCE_APDM_OUTPUT" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "PosAccMirrorOk";
			String resError = "";
			String dllResult = "IO";

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? "IO" : hla.controlMirror( x, y, z, phi, theta );
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( !dllResult.equalsIgnoreCase( "IO" ) ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "PosAccMirrorNOk";
				resError = dllResult;
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}
			resList.add( new Ergebnis( "StatusPosAccMirror", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), "IO", "IO", "0", "", "", "", resError, "", resType ) );

			// set vars...
			if( !apdmOutput ) {
				resList.add( new Ergebnis( "ACC_MIRROR_POS_X", "HeadlightAiming", job, "", "", "ACC_MIRROR_POS_X", String.valueOf( x ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
				resList.add( new Ergebnis( "ACC_MIRROR_POS_Y", "HeadlightAiming", job, "", "", "ACC_MIRROR_POS_Y", String.valueOf( y ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
				resList.add( new Ergebnis( "ACC_MIRROR_POS_Z", "HeadlightAiming", job, "", "", "ACC_MIRROR_POS_Z", String.valueOf( z ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
				resList.add( new Ergebnis( "ACC_MIRROR_POS_PHI", "HeadlightAiming", job, "", "", "ACC_MIRROR_POS_PHI", String.valueOf( phi ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
				resList.add( new Ergebnis( "ACC_MIRROR_POS_THETA", "HeadlightAiming", job, "", "", "ACC_MIRROR_POS_THETA", String.valueOf( theta ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
			}

			log( debug, "Var", "set ACC_MIRROR_POS_X = " + String.valueOf( x ) );
			log( debug, "Var", "set ACC_MIRROR_POS_Y = " + String.valueOf( y ) );
			log( debug, "Var", "set ACC_MIRROR_POS_Z = " + String.valueOf( z ) );
			log( debug, "Var", "set ACC_MIRROR_POS_PHI = " + String.valueOf( phi ) );
			log( debug, "Var", "set ACC_MIRROR_POS_THETA = " + String.valueOf( theta ) );

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_PERFORM_AIMING_PROCESS</b> <br>
	 * - req: ID, CMD <br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_PERFORM_AIMING_PROCESS( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		final String[] XMLTAG_INFO = { "FOGLAMP_HEIGHT", "HOTSPOT_POS_HEIGHT", "HOTSPOT_POS_SIDE", "HV_DIFF_HEIGHT", "HV_DIFF_SIDE", "HV_POS_HEIGHT", "HV_POS_SIDE", "LIGHT_INTENSITY" };
		final String[] XMLTAG_ALGO = { "METHOD", "PARAMETER" };
		final String[] DIRECTION = { "SIDE", "HEIGHT" };
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			String id = getArg( "ID" ); // Auto, Manual
			String cmd = getArg( "CMD" ); // Side, Height, Both
			String[] light = getArg( "LIGHT" ).split( "_" );
			String workDir = (getArg( "WORK_DIRECTORY" ) != null) ? getArg( "WORK_DIRECTORY" ) : null;
			int reduceApdmOutput = (getArg( "REDUCE_APDM_OUTPUT" ) != null) ? new Integer( extractArg( "REDUCE_APDM_OUTPUT" ) ).intValue() : 1;
			boolean nokHandling = (getArg( "NOK_HANDLING" ) != null && getArg( "NOK_HANDLING" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "";
			String resValue = "";
			String resError = "";
			String dllResult = "";

			// -----------------------[aimingProcess]-----------------------
			// perform aiming process
			log( debug, "Ok", "Aiming process(" + id + ")... " + light[SRC] + "_" + light[POS] );
			resName = light[SRC] + "_" + light[POS] + "_AimingOk";
			if( id.equalsIgnoreCase( "Auto" ) ) {
				// ---------
				// AUTO
				// ---------
				// perform automatic aiming process (without screw driver)
				// must activateLightBox and readValues until they are in
				// tolerance.
				dllResult = "NIO";
				log( debug, "Error", "Method not yet implemented in the PP..." );

			} else {
				// ---------
				// MANUAL
				// ---------
				// execute dll function...
				dllResult = (simulation) ? "IO" : hla.autoAimingProcess( light[SRC], light[POS], cmd );
				log( debug, "Var", "dllResult[manualAimingProcess] = " + String.valueOf( dllResult ) );
			}
			// check dll result...
			// different handling for NOK/Next Step and Cancel
			if( !dllResult.equalsIgnoreCase( "IO" ) ) {
				if( nokHandling && dllResult.equalsIgnoreCase( "NIO" ) ) { // Continue process in pruefumfang
					status = STATUS_EXECUTION_ERROR_IGNORE;
					resType = Ergebnis.FT_IGNORE;
				} else { // all other results lead to an error, e.g. "CANCEL"
					status = STATUS_EXECUTION_ERROR;
					resType = Ergebnis.FT_NIO;
				}
				resName = light[SRC] + "_" + light[POS] + "_AimingNOk";
				resError = dllResult;
				log( debug, "Error", resError + " (dllResult[manualAimingProcess]: " + String.valueOf( dllResult ) + ")" );

			}
			resList.add( new Ergebnis( "StatusAimingProcess", "HeadlightAiming", job, "", "", resName + " (" + id + ")", String.valueOf( dllResult ), "IO", "IO", "0", "", "", "", resError, "", resType ) );

			// ------------------------[readValues]-------------------------
			if( reduceApdmOutput <= 1 ) {
				// read pre-aiming values
				// ----------------------
				log( debug, "Ok", "Reading values before aiming... " + light[SRC] + "_" + light[POS] );
				// set imageInfo vars... (before aiming)
				dllResult = (simulation) ? paramXmlSerialization( XMLTAG_INFO, "STRING" ) : hla.readImageInfo( 0 );
				log( debug, "Var", "dllResult[readOldImageInfo] = " + String.valueOf( dllResult ) );
				for( int p = 0; p < XMLTAG_INFO.length; p++ ) {
					resName = light[SRC] + "_" + light[POS] + "_" + XMLTAG_INFO[p];
					resValue = paramXmlDeserialization( dllResult, XMLTAG_INFO[p] );
					if( resValue != null && !resValue.equalsIgnoreCase( "" ) ) {
						resList.add( new Ergebnis( resName, "HeadlightAiming", job, "", "", resName + " before aiming (%)", resValue, "", "", "0", "", "", "", "", "", resType ) );
						log( debug, "Var", "set " + resName + " = " + resValue );
					}
				}

				// read pos-aiming values
				// ----------------------
				log( debug, "Ok", "Reading values after aiming... " + light[SRC] + "_" + light[POS] );
				// set imageInfo vars... (after aiming)
				dllResult = (simulation) ? paramXmlSerialization( XMLTAG_INFO, "STRING" ) : hla.readImageInfo( 1 );
				log( debug, "Var", "dllResult[readNewImageInfo] = " + String.valueOf( dllResult ) );

				if( reduceApdmOutput == 0 ) {
					// read tolerance values
					float sideTolMin = (simulation) ? -2.1f : hla.readToleranceValue( light[SRC], light[POS], "SIDE", "MIN" );
					float sideTolMax = (simulation) ? -1.9f : hla.readToleranceValue( light[SRC], light[POS], "SIDE", "MAX" );
					float heightTolMin = (simulation) ? -0.1f : hla.readToleranceValue( light[SRC], light[POS], "HEIGHT", "MIN" );
					float heightTolMax = (simulation) ? 0.1f : hla.readToleranceValue( light[SRC], light[POS], "HEIGHT", "MAX" );
					log( debug, "Var", "Tolerances: side[" + sideTolMin + "," + sideTolMax + "] height[" + heightTolMin + "," + heightTolMax + "]" );

					// de-seralize DLL return value
					for( int p = 0; p < XMLTAG_INFO.length; p++ ) {
						resName = light[SRC] + "_" + light[POS] + "_" + XMLTAG_INFO[p];
						resValue = paramXmlDeserialization( dllResult, XMLTAG_INFO[p] );
						if( resValue != null && !resValue.equalsIgnoreCase( "" ) ) {
							if( XMLTAG_INFO[p].equalsIgnoreCase( "HV_DIFF_SIDE" ) ) {
								resList.add( new Ergebnis( resName, "HeadlightAiming", job, "", "", resName + " after aiming (%)", resValue, String.valueOf( sideTolMin ), String.valueOf( sideTolMax ), "0", "", "", "", "", "", resType ) );
							} else if( XMLTAG_INFO[p].equalsIgnoreCase( "HV_DIFF_HEIGHT" ) ) {
								resList.add( new Ergebnis( resName, "HeadlightAiming", job, "", "", resName + " after aiming (%)", resValue, String.valueOf( heightTolMin ), String.valueOf( heightTolMax ), "0", "", "", "", "", "", resType ) );
							} else {
								resList.add( new Ergebnis( resName, "HeadlightAiming", job, "", "", resName + " after aiming (%)", resValue, "", "", "0", "", "", "", "", "", resType ) );
							}
							log( debug, "Var", "set " + resName + " = " + resValue );
						}
					}
				} else {
					// reduceApdmOutput == 1
					//log post-aming values w/o tolerance information
					for( int p = 0; p < XMLTAG_INFO.length; p++ ) {
						resName = light[SRC] + "_" + light[POS] + "_" + XMLTAG_INFO[p];
						resValue = paramXmlDeserialization( dllResult, XMLTAG_INFO[p] );
						if( resValue != null && !resValue.equalsIgnoreCase( "" ) ) {
							if( XMLTAG_INFO[p].equalsIgnoreCase( "HV_DIFF_SIDE" ) ) {
								resList.add( new Ergebnis( resName, "HeadlightAiming", job, "", "", resName + " after aiming (%)", resValue, "", "", "0", "", "", "", "", "", resType ) );
							} else if( XMLTAG_INFO[p].equalsIgnoreCase( "HV_DIFF_HEIGHT" ) ) {
								resList.add( new Ergebnis( resName, "HeadlightAiming", job, "", "", resName + " after aiming (%)", resValue, "", "", "0", "", "", "", "", "", resType ) );
							} else {
								resList.add( new Ergebnis( resName, "HeadlightAiming", job, "", "", resName + " after aiming (%)", resValue, "", "", "0", "", "", "", "", "", resType ) );
							}
							log( debug, "Var", "set " + resName + " = " + resValue );
						}
					}
				}

				if( reduceApdmOutput == 0 ) {
					// set algoInfo vars...
					dllResult = (simulation) ? paramXmlSerialization( XMLTAG_ALGO, "STRING" ) : String.valueOf( hla.readAlgorithmInfo( light[SRC], light[POS] ) );
					log( debug, "Var", "dllResult[readAlgorithmInfo] = " + String.valueOf( dllResult ) );
					for( int p = 0; p < XMLTAG_ALGO.length; p++ ) {
						resName = light[SRC] + "_" + light[POS] + "_ALGO_" + XMLTAG_ALGO[p];
						resValue = paramXmlDeserialization( dllResult, XMLTAG_ALGO[p] );
						if( resValue != null && !resValue.equalsIgnoreCase( "" ) ) {
							resList.add( new Ergebnis( resName, "HeadlightAiming", job, "", "", resName + " preset", resValue, "", "", "0", "", "", "", "", "", resType ) );
							log( debug, "Var", "set " + resName + " = " + resValue );
						}
					}
					// set offset vars...
					for( int d = 0; d < DIRECTION.length; d++ ) {
						resName = light[SRC] + "_" + light[POS] + "_OFFSET_" + DIRECTION[d];
						resValue = String.valueOf( (simulation) ? 0.1f : hla.readOffsetValue( light[SRC], light[POS], DIRECTION[d] ) );
						if( resValue != null && !resValue.equalsIgnoreCase( "" ) ) {
							resList.add( new Ergebnis( resName, "HeadlightAiming", job, "", "", resName + " preset (%)", resValue, "", "", "0", "", "", "", "", "", resType ) );
							log( debug, "Var", "set " + resName + " = " + resValue );
						}
					}
					// set target vars...
					for( int d = 0; d < DIRECTION.length; d++ ) {
						resName = light[SRC] + "_" + light[POS] + "_NOMINAL_" + DIRECTION[d];
						resValue = String.valueOf( (simulation) ? 0.1f : hla.readTargetValue( light[SRC], light[POS], DIRECTION[d] ) );
						if( resValue != null && !resValue.equalsIgnoreCase( "" ) ) {
							resList.add( new Ergebnis( resName, "HeadlightAiming", job, "", "", resName + " preset (%)", resValue, "", "", "0", "", "", "", "", "", resType ) );
							log( debug, "Var", "set " + resName + " = " + resValue );
						}
					}
				}
			}

			// -----------------------[storeImage]--------------------------
			// store aiming image
			if( workDir != null ) {
				resType = Ergebnis.FT_IO;
				resName = light[SRC] + "_" + light[POS] + "_StoreImageOk";
				resError = "";
				String imgFilename = workDir + File.separator + String.valueOf( System.currentTimeMillis() ) + "_" + this.getPr�fling().getAuftrag().getFahrgestellnummer7() + "_" + light[SRC] + "_" + light[POS] + ".jpg";

				// execute dll function...
				log( debug, "Ok", "Storing aiming image... " + light[SRC] + "_" + light[POS] );
				if( simulation ) {
					// create screenshot & save to PNG file
					File imgFile = new File( imgFilename );
					ImageIO.write( new Robot().createScreenCapture( new Rectangle( Toolkit.getDefaultToolkit().getScreenSize() ) ), "png", imgFile );
					dllResult = "IO";
				} else {
					dllResult = hla.storeImage( imgFilename );
				}
				log( debug, "Var", "dllResult[storeImage] = " + String.valueOf( dllResult ) );

				// check dll result...
				if( !dllResult.equalsIgnoreCase( "IO" ) ) {
					// NIO
					status = STATUS_EXECUTION_ERROR;
					resType = Ergebnis.FT_NIO;
					resName = light[SRC] + "_" + light[POS] + "_StoreImageNOk";
					resError = dllResult; // getDllErrorMsg(String.valueOf(dllResult));
					log( debug, "Error", resError + " (dllResult[storeImage] = " + String.valueOf( dllResult ) + ")" );
				}
				resList.add( new Ergebnis( "StatusStoreImage", "HeadlightAiming", job, imgFilename, "", resName, dllResult, "IO", "IO", "0", "", "", "", resError, "", resType ) );
			}

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_CONTROL_CENTRATION</b> <br>
	 * - req: CMD <br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_CONTROL_CENTRATION( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		final String[] CMD = { "", "Uncenter", "CenterNormal", "CenterStrong" };
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			int cmd = getOptionInt( CMD, getArg( "CMD" ) );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = CMD[cmd] + "_CentrationOk";
			String resError = "";
			int dllResult = 0;

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = simulation ? cmd : hla.centration( cmd );
			log( debug, "Var", "dllResult = " + String.valueOf( dllResult ) );

			// check dll result...
			if( (dllResult != cmd) ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = CMD[cmd] + "_CentrationNOk";
				resError = String.valueOf( dllResult );
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}
			resList.add( new Ergebnis( "StatusCentration", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), String.valueOf( cmd ), String.valueOf( cmd ), "0", "", "", "", resError, "", resType ) );

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_CONTROL_TRAFFIC_LIGHTS</b> <br>
	 * - req: CMD <br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_CONTROL_TRAFFIC_LIGHTS( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		final String[] CMD = { "", "Green", "Yellow", "Red" };
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			int cmd = getOptionInt( CMD, getArg( "CMD" ) );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = CMD[cmd] + "_TrafficLightsOk";
			String resError = "";
			int dllResult = 0;

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? cmd : hla.trafficLights( cmd );
			log( debug, "Var", "dllResult = " + String.valueOf( dllResult ) );

			// check job result...
			if( (dllResult != cmd) ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = CMD[cmd] + "_TrafficLightsNOk";
				resError = String.valueOf( dllResult );
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}
			resList.add( new Ergebnis( "StatusTrafficLights", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), String.valueOf( cmd ), String.valueOf( cmd ), "0", "", "", "", resError, "", resType ) );

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_CONTROL_VISUALIZATION</b> <br>
	 * - req: CMD <br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_CONTROL_VISUALIZATION( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		final String[] CMD = { "", "Off", "On" };
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			int cmd = getOptionInt( CMD, getArg( "CMD" ) );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = CMD[cmd] + "_VisualizationOk";
			String resError = "";
			int dllResult = 0;

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? cmd : hla.showVisualization( cmd, 0, 0 );
			log( debug, "Var", "dllResult = " + String.valueOf( dllResult ) );

			// check dll result...
			if( dllResult != cmd ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = CMD[cmd] + "_VisualizationNOk";
				resError = String.valueOf( dllResult );
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}
			resList.add( new Ergebnis( "StatusVisualization", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), String.valueOf( cmd ), String.valueOf( cmd ), "0", "", "", "", resError, "", resType ) );

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_MEASURE_VEHICLE</b> <br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_MEASURE_VEHICLE( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "";
			String resError = "";
			float zFL = 0, zFR = 0, zRL = 0, zRR = 0; // mm
			float orientation = 0.0f; // �
			float plausib = 0.0f; // mm
			float heightFront = 0.0f, heightRear = 0.0f, heightLeft = 0.0f, heightRight = 0.0f, height = 0.0f; // mm

			// --------------------------------------------------------------
			// vehicle measurements
			zFL = (simulation) ? 98.7f : rnd( hla.getVehicleHeight( "LEFT_FRONT" ), 2 );
			zFR = (simulation) ? 98.9f : rnd( hla.getVehicleHeight( "RIGHT_FRONT" ), 2 );
			zRL = (simulation) ? 97.8f : rnd( hla.getVehicleHeight( "LEFT_REAR" ), 2 );
			zRR = (simulation) ? 99.0f : rnd( hla.getVehicleHeight( "RIGHT_REAR" ), 2 );
			orientation = (simulation) ? 0.01f : rnd( hla.getVehicleOrientation(), 6 );
			log( debug, "Var", "--- vehicle measures ---" );
			// IO
			resList.add( new Ergebnis( "StatusVehicleMeasurement", "HeadlightAiming", job, "", "", "VehicleMeasurementOk", "IO", "IO", "IO", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
			log( debug, "Ok", "vehicle measurement completed." );
			// set vars...
			resList.add( new Ergebnis( "VEHICLE_HEIGHT_FL", "HeadlightAiming", job, "", "", "VEHICLE_HEIGHT_FL", String.valueOf( zFL ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
			resList.add( new Ergebnis( "VEHICLE_HEIGHT_FR", "HeadlightAiming", job, "", "", "VEHICLE_HEIGHT_FR", String.valueOf( zFR ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
			resList.add( new Ergebnis( "VEHICLE_HEIGHT_RL", "HeadlightAiming", job, "", "", "VEHICLE_HEIGHT_RL", String.valueOf( zRL ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
			resList.add( new Ergebnis( "VEHICLE_HEIGHT_RR", "HeadlightAiming", job, "", "", "VEHICLE_HEIGHT_RR", String.valueOf( zRR ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
			resList.add( new Ergebnis( "VEHICLE_ORIENTATION", "HeadlightAiming", job, "", "", "VEHICLE_ORIENTATION", String.valueOf( orientation ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
			log( debug, "Var", "set VEHICLE_HEIGHT_FL = " + String.valueOf( zFL ) );
			log( debug, "Var", "set VEHICLE_HEIGHT_FR = " + String.valueOf( zFR ) );
			log( debug, "Var", "set VEHICLE_HEIGHT_RL = " + String.valueOf( zRL ) );
			log( debug, "Var", "set VEHICLE_HEIGHT_RR = " + String.valueOf( zRR ) );
			log( debug, "Var", "set VEHICLE_ORIENTATION = " + String.valueOf( orientation ) );

			// Heights plausibility test: abs(zFL - zFR - zRL + zRR)
			plausib = Math.abs( zFL - zFR + zRR - zRL );
			log( debug, "Var", "plausib = abs(zFL - zFR + zRR - zRL) = " + String.valueOf( plausib ) + " mm" );
			if( plausib < 15.0f ) {
				// IO
				resList.add( new Ergebnis( "StatusVehicleHeightsPlausibility", "HeadlightAiming", job, "", "", "VehicleHeightsPlausibilityOk", String.valueOf( plausib ), "-15", "15", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
				log( debug, "Ok", "height measurement plausible. Tolerance criterion: abs(zFL - zFR + zRR - zRL) < 15mm, Result = " + String.valueOf( plausib ) + " mm" );
				// computed measures
				heightFront = rnd( (zFL + zFR) / 2, 2 );
				heightRear = rnd( (zRL + zRR) / 2, 2 );
				heightLeft = rnd( (zFL + zRL) / 2, 2 );
				heightRight = rnd( (zFR + zRR) / 2, 2 );
				height = rnd( ((zFL + zFR + zRL + zRR + heightFront + heightRear + heightLeft + heightRight) / 8), 2 );
				// set vars...
				resList.add( new Ergebnis( "VEHICLE_HEIGHT_FRONT", "HeadlightAiming", job, "", "", "VEHICLE_HEIGHT_FRONT", String.valueOf( heightFront ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
				resList.add( new Ergebnis( "VEHICLE_HEIGHT_REAR", "HeadlightAiming", job, "", "", "VEHICLE_HEIGHT_REAR", String.valueOf( heightRear ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
				resList.add( new Ergebnis( "VEHICLE_HEIGHT_LEFT", "HeadlightAiming", job, "", "", "VEHICLE_HEIGHT_LEFT", String.valueOf( heightLeft ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
				resList.add( new Ergebnis( "VEHICLE_HEIGHT_RIGHT", "HeadlightAiming", job, "", "", "VEHICLE_HEIGHT_RIGHT", String.valueOf( heightRight ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
				resList.add( new Ergebnis( "VEHICLE_HEIGHT", "HeadlightAiming", job, "", "", "VEHICLE_HEIGHT", String.valueOf( height ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
				log( debug, "Var", "set VEHICLE_HEIGHT_FRONT = " + String.valueOf( heightFront ) );
				log( debug, "Var", "set VEHICLE_HEIGHT_REAR = " + String.valueOf( heightRear ) );
				log( debug, "Var", "set VEHICLE_HEIGHT_LEFT = " + String.valueOf( heightLeft ) );
				log( debug, "Var", "set VEHICLE_HEIGHT_RIGHT = " + String.valueOf( heightRight ) );
				log( debug, "Var", "set VEHICLE_HEIGHT = " + String.valueOf( height ) );
				log( debug, "Var", "----------------------" );
				log( debug, "Ok", "vehicle measurement completed." );

			} else {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "VehicleHeightsPlausibilityNOk";
				resError = "VehicleHeightsNotPlausibel";
				resList.add( new Ergebnis( "StatusVehicleHeightsPlausibility", "HeadlightAiming", job, "", "", resName, String.valueOf( plausib ), "-15", "15", "0", "", "", "", resError, "", resType ) );
				log( debug, "Error", "height measurement NOT plausible! Tolerance criterion: abs(zFL - zFR + zRR - zRL) < 15mm, Result = " + String.valueOf( plausib ) + " mm" );
			}

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_MEASURE_STAND</b> <br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_MEASURE_STAND( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		String[] XMLTAG_STAND_POS = { // only the positions
		"LEFTBOX_LEFTPOS_Y", "LEFTBOX_LEFTPOS_Z", "LEFTBOX_RIGHTPOS_Y", "LEFTBOX_RIGHTPOS_Z", "RIGHTBOX_LEFTPOS_Y", "RIGHTBOX_LEFTPOS_Z", "RIGHTBOX_RIGHTPOS_Y", "RIGHTBOX_RIGHTPOS_Z" };
		String[] XMLTAG_STAND_TYPE = { // only the type of values
		"REF", "NEW" };
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			String id = getArg( "ID" );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "StandVerificationOk";
			String resValue = "IO";
			String resError = "";
			String dllResult = "";

			// --------------------------------------------------------------
			// execute dll function...
			if( simulation ) {
				dllResult = "<STATUS_IO>IO</STATUS_IO><CALIB_EXECUTED_1>Y</CALIB_EXECUTED_1><TOLERANCE_1>1.5</TOLERANCE_1><REF_LEFTBOX_LEFTPOS_Y_1>10.04</REF_LEFTBOX_LEFTPOS_Y_1><REF_LEFTBOX_LEFTPOS_Z_1>11.23</REF_LEFTBOX_LEFTPOS_Z_1><REF_LEFTBOX_RIGHTPOS_Y_1>9.67</REF_LEFTBOX_RIGHTPOS_Y_1><REF_LEFTBOX_RIGHTPOS_Z_1>9.98</REF_LEFTBOX_RIGHTPOS_Z_1><NEW_LEFTBOX_LEFTPOS_Y_1>10.05</NEW_LEFTBOX_LEFTPOS_Y_1><NEW_LEFTBOX_LEFTPOS_Z_1>10.02</NEW_LEFTBOX_LEFTPOS_Z_1><NEW_LEFTBOX_RIGHTPOS_Y_1>10.06</NEW_LEFTBOX_RIGHTPOS_Y_1><NEW_LEFTBOX_RIGHTPOS_Z_1>8.90</NEW_LEFTBOX_RIGHTPOS_Z_1>";
			} else {
				dllResult = hla.testStandCalibration( false ); // through CASCADE
				// only the
				// stand
				// verification
				// is allowed
				dllResult = dllResult.trim();
			}
			log( debug, "Var", "dllResult = " + dllResult );
			
			if (dllResult.equalsIgnoreCase("IO")) {
				status = STATUS_EXECUTION_OK;
				return status;
			}
			
			if (dllResult.equalsIgnoreCase("NIO")) {
				status = STATUS_EXECUTION_ERROR;
				return status;
			}

			// get status
			String mStatus = paramXmlDeserialization( dllResult, "STATUS_IO" );

			// extract measurement values...
			String sName, sValue = "";
			float mTol, mVal, mValRef, mValMin, mValMax = 0f;
			if( paramXmlDeserialization( dllResult, "CALIB_EXECUTED_1" ).equalsIgnoreCase( "Y" ) ) {
				if( !paramXmlDeserialization( dllResult, "TOLERANCE_1" ).equalsIgnoreCase( "" ) ) {
					mTol = new Float( paramXmlDeserialization( dllResult, "TOLERANCE_1" ) ).floatValue();
					// set vars
					for( int typ = 0; typ < XMLTAG_STAND_TYPE.length; typ++ ) {
						for( int pos = 0; pos < XMLTAG_STAND_POS.length; pos++ ) {
							sName = XMLTAG_STAND_TYPE[typ] + "_" + XMLTAG_STAND_POS[pos] + "_1";
							sValue = paramXmlDeserialization( dllResult, sName );
							if( sValue != null && !sValue.equalsIgnoreCase( "" ) ) {
								if( typ == 0 ) {
									// REF...
									resList.add( new Ergebnis( sName, "HeadlightAiming", job, "", "", sName, sValue, "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
								} else {
									// NEW...
									mVal = new Float( sValue ).floatValue();
									mValRef = new Float( paramXmlDeserialization( dllResult, "REF_" + XMLTAG_STAND_POS[pos] + "_1" ) ).floatValue();
									mValMin = mValRef - mTol;
									mValMax = mValRef + mTol;
									if( mVal >= mValMin && mVal <= mValMax ) {
										// IO
										resList.add( new Ergebnis( sName, "HeadlightAiming", job, "", "", sName, sValue, String.valueOf( mValMin ), String.valueOf( mValMax ), "0", "", "", "", "", "", Ergebnis.FT_IO ) );
									} else {
										// NIO - OutOfTolerance!
										status = STATUS_EXECUTION_ERROR;
										resList.add( new Ergebnis( sName, "HeadlightAiming", job, "", "", sName, sValue, String.valueOf( mValMin ), String.valueOf( mValMax ), "0", "", "", "", "OutOfTolerance", "", Ergebnis.FT_NIO ) );
									}
								}
								log( debug, "Var", "set " + sName + " = " + sValue );
							}
						}
					}
					// check final status
					if( status != STATUS_EXECUTION_OK || !mStatus.equalsIgnoreCase( "IO" ) ) {
						// NIO - StatusNotOk!
						status = STATUS_EXECUTION_ERROR;
						resType = Ergebnis.FT_NIO;
						resName = "StandVerificationNOk";
						resValue = mStatus;
						resError = "StatusNotOk";
						log( debug, "Error", resError + " (mStatus = " + mStatus + ")" );
					}

				} else {
					// NIO - ToleranceMissing!
					status = STATUS_EXECUTION_ERROR;
					resType = Ergebnis.FT_NIO;
					resName = "StandVerificationNOk";
					resValue = "NIO";
					resError = "ToleranceMissing";
					log( debug, "Error", resError + " (dllResult = " + dllResult + ")" );
				}

			} else {
				// NIO - NotExecuted!
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "StandVerificationNOk";
				resValue = "NIO";
				resError = "NotExecuted";
				log( debug, "Error", resError + " (dllResult = " + dllResult + ")" );
			}

			resList.add( new Ergebnis( "StatusStandVerification", "HeadlightAiming", job, "", "", resName, mStatus, "IO", "IO", "0", "", "", "", resError, "", resType ) );

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_POSITION_MIRROR_HOME</b> <br>
	 * 
	 * @param hla
	 * @param resList
	 * @return execution status
	 * @throws PPExecutionException
	 */
	private int execute_POSITION_MIRROR_HOME( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "AccMirrorHomeOk";
			String resError = "";
			String dllResult = "IO";

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? "IO" : hla.driveMirrorHome();
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( !dllResult.equalsIgnoreCase( "IO" ) ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "AccMirrorHomeNOk";
				resError = dllResult;
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}
			resList.add( new Ergebnis( "StatusAccMirrorHome", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), "IO", "IO", "0", "", "", "", resError, "", resType ) );
			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_SET_THRUST_ANGLE</b><br>
	 * 
	 * @param hla
	 * @param resList
	 * @return execution status
	 * @throws PPExecutionException
	 */
	private int execute_SET_THRUST_ANGLE( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			float thAngle = (simulation == true) ? 0.5f : new Float( extractArg( "THRUST_ANGLE" ) ).floatValue();
			String resType = Ergebnis.FT_IO;
			String resName = "SetThrustAngleOk";
			String resError = "";

			String dllResult = "IO";

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? "IO" : hla.setThrustAngle( thAngle );
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( !dllResult.equalsIgnoreCase( "IO" ) ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "SetThrustAngleNOk";
				resError = dllResult;
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}
			resList.add( new Ergebnis( "SetThrustAngle", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ) + " (" + String.valueOf( thAngle ) + ")", "IO", "IO", "0", "", "", "", resError, "", resType ) );
			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_DISPLAY_MESSAGE</b><br>
	 * 
	 * @param hla
	 * @param resList
	 * @return execution status
	 * @throws PPExecutionException
	 */
	private int execute_DISPLAY_MESSAGE( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String message = extractArg( "MESSAGE" );
			int buttons = new Integer( extractArg( "BUTTONS" ) ).intValue();

			String resType = Ergebnis.FT_IO;
			String resName = "DisplayMessageOk";
			String resError = "";

			int dllResult = 0;

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? 0 : hla.displayMessage( message, buttons );

			log( debug, "Var", "dllResult = " + dllResult );

			switch( dllResult ) {

				case 1: // user pressed OK
					status = STATUS_EXECUTION_OK;
					resType = Ergebnis.FT_IO;
					resName = "DisplayMessageOk";

					resList.add( new Ergebnis( "DisplayMessage", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ) + " (" + message + ")", "IO", "IO", "0", "", "", "", "", "", resType ) );
					break;
				case 2: // user pressed NOK
					status = STATUS_EXECUTION_ERROR;
					resType = Ergebnis.FT_NIO;
					resName = "DisplayMessageNOk";
					resError = String.valueOf( dllResult );

					resList.add( new Ergebnis( "DisplayMessage", "HeadlightAiming", job, "", "", resName, "Button NOK" + " (" + message + ")", "IO", "IO", "0", "", "", "", "User pressed NOK", "", resType ) );
					break;
				case 4: // user pressed REPEAT
					status = STATUS_EXECUTION_ERROR;
					resType = Ergebnis.FT_NIO;
					resName = "DisplayMessageNOk";
					resError = String.valueOf( dllResult );

					resList.add( new Ergebnis( "DisplayMessage", "HeadlightAiming", job, "", "", resName, "Button REPEAT" + " (" + message + ")", "IO", "IO", "0", "", "", "", "User pressed REPEAT", "", resType ) );
					break;

				default: // NIO
					status = STATUS_EXECUTION_ERROR;
					resType = Ergebnis.FT_NIO;
					resName = "DisplayMessageNOk";
					resError = String.valueOf( dllResult );
					log( debug, "Error", "dllResult = " + resError + ")" );
					resList.add( new Ergebnis( "DisplayMessage", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ) + " (" + message + ")", "IO", "IO", "0", "", "", "", "No Button pressed (" + resError + ")", "", resType ) );
					break;
			}
			// --------------------------------------------------------------
			return status;
		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_LET_SERIAL_HLA</b><br>
	 * 
	 * Funktion zum Ansteuern des manuellen Lichtsammelkastens
	 * von L.E.T. �ber die Serielle Schnittstelle
	 * 
	 * @param debug
	 * @param hla
	 * @param resList
	 * @return execution status
	 * @throws PPExecutionException
	 */
	private int execute_LET_SERIAL_HLA( boolean debug, HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		try {
			// Variablen
			int status = STATUS_EXECUTION_ERROR;
			int i = 0;
			int resultSets = 0;
			int timeoutSec = getArg( "TIMEOUT_SEC" ) == null ? 120 : Integer.parseInt( getArg( "TIMEOUT_SEC" ) );

			String vehSelNum = getArg( "VSN" );
			String dataString = new String();
			String errorString = new String();
			String resultString = new String();
			String evalString = new String();
			String evalResultAPDM = new String();
			String fgnr = new String();

			char stx = 0x02;
			char etx = 0x03;
			char ack = 0x06;
			char nak = 0x15;
			char etb = 0x17;
			char chk = 0x00;

			byte[] sendBuffer = new byte[48];
			byte[] receiveBuffer = new byte[256];

			// Objekt zur Ansteuerung der seriellen Schnittstelle
			SerialGeneric serSchni = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getSerialGeneric();

			fgnr = this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer();

			// Argument VSN (steuert die Programmauswahl am Einstellger�t) ggf.
			// mit Nullen auff�llen
			log( debug, "Info", "VSN: " + vehSelNum );
			try {
				if( vehSelNum.length() == 1 )
					vehSelNum = new String( "00" + vehSelNum );
				if( vehSelNum.length() == 2 )
					vehSelNum = new String( "0" + vehSelNum );
			} catch( Exception e ) {
				vehSelNum = "001";
				log( debug, "Error", "Auff�llen der VSN ist fehlgeschlagen" );
			}

			// Port COM1 �ffnen
			serSchni.open( 9600, 0, 0, 8, 1, 0 );

			// erste Nachricht zusammensetzen
			chk = getCheckSum( stx + "VSNS" + fgnr + vehSelNum + etb );
			dataString = stx + "VSNS" + fgnr + vehSelNum + etb + chk + etx;
			sendBuffer = dataString.getBytes();
			serSchni.writeData( sendBuffer );
			log( debug, "Info", "Sent: " + dataString );

			// auf Antwort warten
			while( i <= 10 ) {
				Thread.sleep( 1000 );
				receiveBuffer = serSchni.readData();

				// bei Antwort Schleife abbrechen und weitermachen
				if( receiveBuffer != null ) {
					log( debug, "Info", "Rcvd: " + new String( receiveBuffer ) );
					break;
				}

				// bei Timeout Schnittstelle schlie�en und aussteigen
				if( i >= 10 ) {
					serSchni.close();
					this.getPr�flingLaufzeitUmgebung().getDeviceManager().releaseSerialGeneric();
					resList.add( new Ergebnis( "Serial_Headlight_Aiming", "HeadlightAiming", "", "", "", "", "", "", "", "0", "", "", "", "Keine Antwort vom Einstellger�t nach Senden der FGNR.", "Verbindung zum Einstellger�t pr�fen.", Ergebnis.FT_NIO ) );
					throw new PPExecutionException( "No response from light box after sending of VIN." );
				}

				i++;
			}

			// Status des Einstellger�ts pr�fen
			if( receiveBuffer != null && receiveBuffer[5] != ack ) {
				switch( receiveBuffer[5] ) {
					case 0x06:
						errorString = "No error (ACK).";
						break;
					case 0x15:
						errorString = "Vehicle selection not accepted or communication error (NAK).";
						break;
					case 0x11:
						errorString = "Vehicle selection not possible (BUSY).";
						break;
					case 0x45:
						errorString = "Selected vehicle not in data base (E).";
						break;
					default:
						errorString = "An undefined error occured.";
						break;
				}

				serSchni.close();
				this.getPr�flingLaufzeitUmgebung().getDeviceManager().releaseSerialGeneric();
				throw new PPExecutionException( errorString );
			} else {
				log( debug, "OK", "Vehicle selection okay, no error (ACK)." );
			}

			// Empfangsspeicher zur�cksetzen
			receiveBuffer = null;

			// Auf Einstelldaten warten
			while( i <= timeoutSec ) {
				Thread.sleep( 1000 );
				receiveBuffer = serSchni.readData();

				if( receiveBuffer != null ) {
					log( debug, "Info", "Rcvd: " + new String( receiveBuffer ) );
					break;
				}

				if( i >= timeoutSec ) {
					serSchni.close();
					this.getPr�flingLaufzeitUmgebung().getDeviceManager().releaseSerialGeneric();
					resList.add( new Ergebnis( "Serial_Headlight_Aiming", "HeadlightAiming", "", "", "", "", "", "", "", "0", "", "", "", "Keine Antwort vom Einstellger�t nach Freigabe der Einstellung.", "Verbindung zum Einstellger�t pr�fen.", Ergebnis.FT_NIO ) );
					throw new PPExecutionException( "No response from light box after release of adjustment." );
				}
				i++;
			}

			resultString = new String( receiveBuffer );

			// Checksumme pr�fen
			dataString = resultString.substring( 0, resultString.length() - 2 );

			if( getCheckSum( dataString ) == resultString.charAt( resultString.length() - 2 ) ) {
				// Empfang ohne Fehler

				// Anzahl auszuwertender Datens�tze feststellen
				resultSets = Integer.parseInt( resultString.substring( 28, 29 ) );

				// Empfangene Daten aufbereiten und
				// APDM-Datens�tze zusammensetzen
				resList.add( new Ergebnis( "Serial_Headlight_Aiming", "HeadlightAiming", "", "", "", "VIN", resultString.substring( 6, 23 ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
				resList.add( new Ergebnis( "Serial_Headlight_Aiming", "HeadlightAiming", "", "", "", "VSN", resultString.substring( 24, 27 ), "000", "999", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
				resList.add( new Ergebnis( "Serial_Headlight_Aiming", "HeadlightAiming", "", "", "", "Lamp count", String.valueOf( resultSets ), "1", "6", "0", "", "", "", "", "", Ergebnis.FT_IO ) );

				if( resultString.substring( 30, 31 ).equalsIgnoreCase( "P" ) ) {
					evalString = "Passed";
					evalResultAPDM = Ergebnis.FT_IO;
				} else {
					evalString = "Failed";
					evalResultAPDM = Ergebnis.FT_NIO;
				}

				resList.add( new Ergebnis( "Serial_Headlight_Aiming", "HeadlightAiming", "", "", "", "Overall evaluation", evalString, "Passed", "Passed", "0", "", "", "", "", "", evalResultAPDM ) );

				// Schleife mehrfach durchlaufen, falls mehrere Lampen eingestellt
				// wurden
				for( i = 1; i <= resultSets; i++ ) {

					if( resultString.substring( 62, 63 ).equalsIgnoreCase( "P" ) ) {
						evalString = "Passed";
						evalResultAPDM = Ergebnis.FT_IO;
					} else {
						evalString = "Failed";
						evalResultAPDM = Ergebnis.FT_NIO;
					}

					resList.add( new Ergebnis( "Serial_Headlight_Aiming", "HeadlightAiming", "", "", "", "Side Lamp " + i + " before adjustment", resultString.substring( 34, 40 ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
					resList.add( new Ergebnis( "Serial_Headlight_Aiming", "HeadlightAiming", "", "", "", "Height Lamp " + i + " before adjustment", resultString.substring( 41, 47 ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
					resList.add( new Ergebnis( "Serial_Headlight_Aiming", "HeadlightAiming", "", "", "", "Side Lamp " + i + " after adjustment", resultString.substring( 48, 54 ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
					resList.add( new Ergebnis( "Serial_Headlight_Aiming", "HeadlightAiming", "", "", "", "Height Lamp " + i + " after adjustment", resultString.substring( 55, 61 ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
					resList.add( new Ergebnis( "Serial_Headlight_Aiming", "HeadlightAiming", "", "", "", "Evaluation Lamp " + i, evalString, "Passed", "Passed", "0", "", "", "", "", "", evalResultAPDM ) );
					resList.add( new Ergebnis( "Serial_Headlight_Aiming", "HeadlightAiming", "", "", "", "Error code Lamp " + i, resultString.substring( 64, 67 ), "0", "0", "0", "", "", "", "", "", Ergebnis.FT_IO ) );

					// String vorne abschneiden vor neuer Auswertung, um
					// Berechnungen einzusparen
					resultString = resultString.substring( 36 );
				}

				// Datenempfang best�tigen
				dataString = stx + "RSPA" + ack + etx;
				sendBuffer = dataString.getBytes();
				log( debug, "Info", "Sent: " + dataString );
				serSchni.writeData( sendBuffer );
			} else {
				// Empfang fehlgeschlagen
				dataString = stx + "RSPA" + nak + etx;
				sendBuffer = dataString.getBytes();
				log( debug, "Info", "Sent: " + dataString );
				serSchni.writeData( sendBuffer );
			}

			// Serielle Schnittstelle wieder freigeben
			serSchni.close();
			this.getPr�flingLaufzeitUmgebung().getDeviceManager().releaseSerialGeneric();
			status = STATUS_EXECUTION_OK;
			return status;
		} catch( Exception e ) {
			e.printStackTrace();
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_ACTIVATE_NIVI_TARGET</b> <br>
	 * - req: X, Y, Z<br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_ACTIVATE_NIVI_TARGET( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		final String[] CMD = { "", "Deactivate", "Activate" };
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			int cmd = getOptionInt( CMD, getArg( "CMD" ) );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;

			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "SwitchNiviTargetOk";
			String resError = "";
			int dllResult = 0;

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? cmd : hla.activateNightVision( cmd );
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( dllResult != cmd ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = CMD[cmd] + "_NiviTargetNOk";
				resError = String.valueOf( dllResult );
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}

			resList.add( new Ergebnis( "StatusNiviTarget", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), String.valueOf( cmd ), String.valueOf( cmd ), "0", "", "", "", resError, "", resType ) );

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_POSITION_NIVI_TARGET</b> <br>
	 * - req: X, Y, Z<br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_POSITION_NIVI_TARGET( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			int x = new Integer( extractArg( "X" ) ).intValue(); // mm
			int y = new Integer( extractArg( "Y" ) ).intValue(); // mm
			int z = new Integer( extractArg( "Z" ) ).intValue(); // mm
			boolean apdmOutput = (getArg( "REDUCE_APDM_OUTPUT" ) != null && getArg( "REDUCE_APDM_OUTPUT" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "PosNiviTargetOk";
			String resError = "";
			String dllResult = "IO";

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? "IO" : hla.controlNightVision( x, y, z );
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( !dllResult.equalsIgnoreCase( "IO" ) ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "PosNiviTargetNOk";
				resError = dllResult;
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}
			resList.add( new Ergebnis( "StatusPosNiviTarget", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), "IO", "IO", "0", "", "", "", resError, "", resType ) );

			// set vars...
			if( !apdmOutput ) {
				resList.add( new Ergebnis( "NIVI_TARGET_POS_X", "HeadlightAiming", job, "", "", "NIVI_TARGET_POS_X", String.valueOf( x ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
				resList.add( new Ergebnis( "NIVI_TARGET_POS_Y", "HeadlightAiming", job, "", "", "NIVI_TARGET_POS_Y", String.valueOf( y ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
				resList.add( new Ergebnis( "NIVI_TARGET_POS_Z", "HeadlightAiming", job, "", "", "NIVI_TARGET_POS_Z", String.valueOf( z ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IGNORE ) );
			}

			log( debug, "Var", "set ACC_MIRROR_POS_X = " + String.valueOf( x ) );
			log( debug, "Var", "set ACC_MIRROR_POS_Y = " + String.valueOf( y ) );
			log( debug, "Var", "set ACC_MIRROR_POS_Z = " + String.valueOf( z ) );

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	/**
	 * <b>execute_POSITION_NIVI_TARGET_HOME</b> <br>
	 * 
	 * @param hla
	 * @param resList
	 * @return execution status
	 * @throws PPExecutionException
	 */
	private int execute_POSITION_NIVI_TARGET_HOME( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "NiviTargetHomeOk";
			String resError = "";
			String dllResult = "IO";

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? "IO" : hla.driveNightVisionHome();
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( !dllResult.equalsIgnoreCase( "IO" ) ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "NiviTargetHomeNOk";
				resError = dllResult;
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}
			resList.add( new Ergebnis( "StatusNiviTargetHome", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), "IO", "IO", "0", "", "", "", resError, "", resType ) );
			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}
	
	/**
	 * <b>execute_ACTIVATE_MOVEABLE_TRACKS</b> <br>
	 * - req: openTracks<br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_ACTIVATE_MOVEABLE_TRACKS( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		final String[] CMD = { "Close", "Open"};
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			int cmd = getOptionInt( CMD, getArg( "CMD" ) );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;

			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "SwitchMoveableTracksOk";
			String resError = "";
			int dllResult = 0;

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? 0 : hla.controlMoveableTracks( cmd );
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( dllResult != 1 ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = CMD[cmd] + "_SwitchMoveableTracksNOk";
				resError = String.valueOf( dllResult );
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}

			resList.add( new Ergebnis( "ActivateMoveableTracks", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), String.valueOf( cmd ), String.valueOf( cmd ), "0", "", "", "", resError, "", resType ) );

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}
	
	/**
	 * <b>execute_DRIVE_LC_SCREWDRIVER_AUTO</b> <br>
	 * - req: Position Side<br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_DRIVE_LC_SCREWDRIVER_AUTO( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			String cmd = getArg( "CMD" );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;

			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "DriveLCScrewdriverAutoOk";
			String resError = "";
			int dllResult = 0;

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? 0 : hla.driveLCScrewDriverAuto( cmd );
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( dllResult != 1 ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = cmd + "_DriveLCScrewdriverAutoNOk";
				resError = String.valueOf( dllResult );
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}

			resList.add( new Ergebnis( "DriveLCScrewdriverAuto", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), String.valueOf( cmd ), String.valueOf( cmd ), "0", "", "", "", resError, "", resType ) );

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}
	
	/**
	 * <b>execute_DRIVE_LC_SCREWDRIVER_MANUAL</b> <br>
	 * - req: Position Side , x , y , z <br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_DRIVE_LC_SCREWDRIVER_MANUAL( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			String cmd = getArg( "CMD" );
			int x = new Integer( extractArg( "X" ) ).intValue(); // mm
			int y = new Integer( extractArg( "Y" ) ).intValue(); // mm
			int z = new Integer( extractArg( "Z" ) ).intValue(); // mm
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;

			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "DriveLCScrewdriverManualOk";
			String resError = "";
			int dllResult = 0;

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? 0 : hla.driveLCScrewdriverManual( cmd, x, y, z );
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( dllResult != 1 ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = cmd + "_DriveLCScrewdriverManualNOk";
				resError = String.valueOf( dllResult );
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}

			resList.add( new Ergebnis( "DriveLCScrewdriverManual", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), String.valueOf( cmd ), String.valueOf( cmd ), "0", "", "", "", resError, "", resType ) );

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}
	
	/**
	 * <b>execute_DRIVE_LCPB_AUTO</b> <br>
	 * - req: Position Side<br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_DRIVE_LCPB_AUTO( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			String cmd = getArg( "CMD" );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;

			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "DriveLCPBAutoOk";
			String resError = "";
			int dllResult = 0;

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? 0 : hla.driveLCPBAuto( cmd );
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( dllResult != 1 ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = cmd + "_DriveLCPBAutoNOk";
				resError = String.valueOf( dllResult );
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}

			resList.add( new Ergebnis( "DriveLCPBAuto", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), String.valueOf( cmd ), String.valueOf( cmd ), "0", "", "", "", resError, "", resType ) );

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}
	
	/**
	 * <b>execute_DRIVE_LCPB_MANUAL</b> <br>
	 * - req: Position Side , x , y , z <br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_DRIVE_LCPB_MANUAL( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			String cmd = getArg( "CMD" );
			int x = new Integer( extractArg( "X" ) ).intValue(); // mm
			int y = new Integer( extractArg( "Y" ) ).intValue(); // mm
			int z = new Integer( extractArg( "Z" ) ).intValue(); // mm
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;

			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "DriveLCPBManualOk";
			String resError = "";
			int dllResult = 0;

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? 0 : hla.driveLCPBManual( cmd, x, y, z );
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( dllResult != 1 ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = cmd + "_DriveLCPBManualNOk";
				resError = String.valueOf( dllResult );
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}

			resList.add( new Ergebnis( "DriveLCPBManual", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), String.valueOf( cmd ), String.valueOf( cmd ), "0", "", "", "", resError, "", resType ) );

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}
	
	/**
	 * <b>execute_START_LC_ADJUSTEMENT</b> <br>
	 * - Position Side <br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_START_LC_ADJUSTEMENT( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			String cmd = getArg( "CMD" );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;

			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "StartLCAdjustementOk";
			String resError = "";
			String dllResult = "IO";

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? "IO" : hla.startLCAdjustment( cmd );
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( !dllResult.equalsIgnoreCase( "IO" ) ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "StartLCAdjustementNOk";
				resError = String.valueOf( dllResult );
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}

			resList.add( new Ergebnis( "StartLCAdjustement", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), "IO", "IO", "0", "", "", "", resError, "", resType ) );

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}
	
	/**
	 * <b>execute_DRIVE_LC_HOME</b> <br>
	 * 
	 * @param hla
	 * @param resList
	 * @return execution status
	 * @throws PPExecutionException
	 */
	private int execute_DRIVE_LC_HOME( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "LCHomeOk";
			String resError = "";
			int dllResult = 0;

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? 0 : hla.driveLCHome();
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( dllResult != 1 ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "LCHomeNOk";
				resError = String.valueOf( dllResult );
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}
			resList.add( new Ergebnis( "StatusLCHome", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), "IO", "IO", "0", "", "", "", resError, "", resType ) );
			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}
	
	/**
	 * <b>execute_CONTROL_HHG</b> <br>
	 * - req: x , y , z <br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_CONTROL_HHG( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			int x = new Integer( extractArg( "X" ) ).intValue(); // mm
			int y = new Integer( extractArg( "Y" ) ).intValue(); // mm
			int z = new Integer( extractArg( "Z" ) ).intValue(); // mm
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;

			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "ControlHHGOk";
			String resError = "";
			String dllResult = "IO";

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? "IO" : hla.controlHHG( x, y, z );
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( !dllResult.equalsIgnoreCase( "IO" ) ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "ControlHHGNOk";
				resError = String.valueOf( dllResult );
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}

			resList.add( new Ergebnis( "ControlHHG", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), "IO", "IO", "0", "", "", "", resError, "", resType ) );

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}
	
	/**
	 * <b>execute_START_ACC_ADJUSTEMENT</b> <br>
	 * - req: turnScrew <br>
	 * 
	 * @param hla
	 *            - HeadlightAiming device
	 * @param resList
	 *            - APDM results list
	 * @return execution status
	 */
	private int execute_START_ACC_ADJUSTEMENT( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			int theta = new Integer( extractArg( "THETA" ) ).intValue(); // Grad
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;

			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "StartACCAdjustementOk";
			String resError = "";
			String dllResult = "IO";

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? "IO" : hla.startACCAdjustment( theta );
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( !dllResult.equalsIgnoreCase( "IO" ) ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "StartACCAdjustementNOk";
				resError = String.valueOf( dllResult );
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}

			resList.add( new Ergebnis( "StartACCAdjustement", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), "IO", "IO", "0", "", "", "", resError, "", resType ) );

			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}
	
	/**
	 * <b>execute_DRIVE_HHG_HOME</b> <br>
	 * 
	 * @param hla
	 * @param resList
	 * @return execution status
	 * @throws PPExecutionException
	 */
	private int execute_DRIVE_HHG_HOME( HeadlightAiming hla, Vector resList ) throws PPExecutionException {
		try {
			// CASCADE parameters
			String job = getArg( "JOB" );
			boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			boolean simulation = (getArg( "SIMULATION" ) != null && getArg( "SIMULATION" ).equalsIgnoreCase( "TRUE" )) ? true : false;
			// aux
			int status = STATUS_EXECUTION_OK;
			String resType = Ergebnis.FT_IO;
			String resName = "HHGHomeOk";
			String resError = "";
			int dllResult = 0;

			// --------------------------------------------------------------
			// execute dll function...
			dllResult = (simulation) ? 0 : hla.driveHHGHome();
			log( debug, "Var", "dllResult = " + dllResult );

			// check dll result...
			if( dllResult != 1 ) {
				// NIO
				status = STATUS_EXECUTION_ERROR;
				resType = Ergebnis.FT_NIO;
				resName = "HHGHomeNOk";
				resError = String.valueOf( dllResult );
				log( debug, "Error", resError + " (dllResult = " + String.valueOf( dllResult ) + ")" );
			}
			resList.add( new Ergebnis( "StatusHHGHome", "HeadlightAiming", job, "", "", resName, String.valueOf( dllResult ), "IO", "IO", "0", "", "", "", resError, "", resType ) );
			// --------------------------------------------------------------
			return status;

		} catch( Exception e ) {
			throw new PPExecutionException( e.getMessage() );
		}
	}

	// ////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// UTILS
	//
	// ////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * <b>paramXmlSerialization</b> <br>
	 * Gets the serialized xml string for the given parameter strings. <br>
	 * 
	 * @param parameter
	 *            strings
	 * @param parameter
	 *            values type (SHORT/INT/FLOAT/DOUBLE/STRING)
	 * @return the xml serialized string
	 */
	private String paramXmlSerialization( String[] params, String paramsType ) {
		String xmlStr = "";
		String paramValue = "";
		for( int t = 0; t < params.length; t++ ) {
			if( paramsType.equalsIgnoreCase( "SHORT" ) || paramsType.equalsIgnoreCase( "INT" ) ) {
				// Short/Int value
				paramValue = String.valueOf( t );
			} else if( paramsType.equalsIgnoreCase( "FLOAT" ) || paramsType.equalsIgnoreCase( "DOUBLE" ) ) {
				// Float/Double value
				paramValue = "0." + String.valueOf( t );
			} else {
				// String value
				paramValue = "Value" + t;
			}
			xmlStr += "<" + params[t] + ">" + paramValue + "</" + params[t] + ">";
		}
		return xmlStr;
	}

	/**
	 * <b>paramXmlDeserialization</b> <br>
	 * 
	 * @param serializedXmlString
	 *            - kompletter Ergebnisstring
	 * @param paramName
	 *            - zu suchender Parameter
	 * @return paramValue
	 */
	private String paramXmlDeserialization( String serializedXmlString, String paramName ) {
		boolean found = false;
		String temp = "", value = "";

		// Alle Leerzeichen entfernen
		StringTokenizer st = new StringTokenizer( serializedXmlString, " ", false );
		String sNoSpace = "";
		while( st.hasMoreElements() )
			sNoSpace += st.nextElement();

		try {
			StringTokenizer tokenTaker = new StringTokenizer( sNoSpace );
			while( tokenTaker.hasMoreTokens() && found == false ) {
				temp = tokenTaker.nextToken( "<>" );
				if( temp.equalsIgnoreCase( paramName ) ) {
					found = true;
					value = tokenTaker.nextToken( "<>" );
				}
				tokenTaker.nextToken( ">" );
			}
		} catch( Exception e ) {
			log( true, "Error", "(Ignored) extracting " + paramName + ", Exception: " + e.getMessage() );
			value = "";
		}
		return (value);
	}

	/**
	 * Gets the order data string.
	 * 
	 * @param debug
	 * @param auftrag
	 * @return order data string
	 */
	private String getOrderData( boolean debug, Auftrag auftrag ) throws Exception {
		String month = (auftrag.getZeitkriteriumMonatInt() < 10 ? "0" : "") + String.valueOf( auftrag.getZeitkriteriumMonatInt() );
		String year = (auftrag.getZeitkriteriumJahrInt() < 10 ? "0" : "") + String.valueOf( auftrag.getZeitkriteriumJahrInt() );
		String orderData = " countryVariant=\"" + auftrag.getLaenderVariante() + "\"" + " driveType=\"" + auftrag.getFahrerPosition() + "\"" + " engineSeries=\"" + getMotorBaureihe() + "\"" + " fzs=\"" + auftrag.getSteuerschluessel() + "\"" + " headlightCode=\"" + auftrag.getScheinwerfer() + "\"" + " longVIN=\"" + auftrag.getFahrgestellnummer() + "\"" + " motorSport=\"" + auftrag.getMotorSport() + "\"" + " series=\"" + auftrag.getBaureihe() + "\"" + " shortVIN=\"" + auftrag.getFahrgestellnummer7() + "\"" + " timeCriteria=\"" + month + year + "\"" + "typeKey=\"" + auftrag.getTypschluessel() + "\"";
		log( debug, "Var", "orderData => " + orderData );
		return orderData;
	}

	/**
	 * Gets the order SaSx string.
	 * 
	 * @param debug
	 * @param auftrag
	 * @return SaSx string
	 */
	private String getOrderSaSx( boolean debug, Auftrag auftrag ) throws Exception {
		// add SAs string
		String[] saCodes = this.getPr�fling().getAuftrag().getSAs();
		String orderSaSx = "Sa=\"";
		for( int i = 0; i < saCodes.length; i++ ) {
			orderSaSx += (i == 0 ? "" : ",") + saCodes[i];
		}
		orderSaSx += "\"";

		// add SXs string
		String[] sxCodes = this.getPr�fling().getAuftrag().getSXs();
		orderSaSx += " Sx=\"";
		for( int i = 0; i < sxCodes.length; i++ ) {
			orderSaSx += (i == 0 ? "" : ",") + sxCodes[i];
		}
		orderSaSx += "\"";

		log( debug, "Var", "orderSaSx => " + orderSaSx );
		return orderSaSx;
	}

	/**
	 * <b>extractArg</b> <br>
	 * Extract the 'CASCADE-construct' value if present in the given argument. <br>
	 * 
	 * @param debug
	 *            is the debug flag
	 * @return the arg extracted value string
	 * @throws InformationNotAvailableException
	 *             if a '@contruct' value is no available
	 */
	private String extractArg( String argName ) throws InformationNotAvailableException {
		String argValue = getArg( argName );

		if( argValue == null ) {
			// NULL value
			return null;
		}
		if( argValue.indexOf( "@" ) != -1 ) {
			// @construct value
			argValue = getPPResult( argValue.toUpperCase() );
		}
		return argValue;
	}

	/**
	 * <b>log</b> <br>
	 * Logs a message if the given debug flag is active and all 'Error'
	 * messages. <br>
	 * 
	 * @param debug
	 *            - the debug flag
	 * @param type
	 *            - the type of message to display
	 * @param text
	 *            - the message text
	 */
	private void log( boolean debug, String type, String text ) {
		if( type.equalsIgnoreCase( "Error" ) || debug ) {
			System.out.println( "[" + getLocalName() + "] [" + type + "] " + text );
		}
	}

	/**
	 * <b>rnd</b> <br>
	 * Rounds a float value to n decimal places. <br>
	 * 
	 * @param value
	 *            the float value to round
	 * @param decPlaces
	 *            the number of decimal places to use while rounding
	 * @return the rounded float value
	 */
	private float rnd( float value, int decPlaces ) {
		float tmp = 1;
		for( int i = 1; i <= decPlaces; i++ )
			tmp *= 10;
		return (float) (Math.rint( (float) value * tmp )) / tmp;
	}

	/**
	 * <b>getOptionInt</b> <br>
	 * Gets the option int value from the given array of options. <br>
	 * 
	 * @param opts
	 *            - the options array
	 * @param arg
	 *            - option string or null
	 * @return the int value or -1 when not found
	 */
	private int getOptionInt( String[] opts, String arg ) {
		if( arg == null ) {
			// NULL value
			return -1;
		}
		for( int i = 0; i < opts.length; i++ ) {
			if( !opts[i].equalsIgnoreCase( "" ) && opts[i].equalsIgnoreCase( arg ) ) {
				// not empty and equal
				return i;
			}
		}
		return -1;
	}

	/**
	 * <b>getOptionsString</b> <br>
	 * Gets the array of options as a printable string. <br>
	 * 
	 * @param opts
	 *            - the options array
	 * @return the Sarray of options as a printable string
	 */
	private String getOptionsString( String[] opts ) {
		String optsStr = "";
		for( int i = 0; i < opts.length; i++ ) {
			optsStr += opts[i] + " ";
		}
		return optsStr;
	}

	/**
	 * <b>getCheckSum</b><br>
	 * 
	 * @param telegram
	 * @return checksum of telegram provided (XOR)
	 */
	private char getCheckSum( String telegram ) {
		int i = 0;
		char chk = 0x00;

		for( i = 0; i < telegram.length(); i++ ) {
			chk ^= telegram.charAt( i );
		}

		return chk;
	}

	/**
	 * Bezieht die Motorbaureihe aus den Auftragsdaten. Schaut als Erstes in der
	 * "alten Welt" (via getMotorBaureihe) nach und falls diese nichts liefert,
	 * wird in den DriveUnits nach einem Verbrennungsmotor gesucht und dieser
	 * befragt. 
	 * @return Die ermittelte Motorbaureihe. 
	 */
	private String getMotorBaureihe() {
		Auftrag auftrag = getPr�fling().getAuftrag();
		String engineSeries = auftrag.getMotorBaureihe();

		if( engineSeries == null || engineSeries.trim().length() == 0 ) //falls nicht gefunden, dann in den DriveUnits nachsehen
			for( int i = 0; i < auftrag.getDriveUnitEnginesCountInt(); i++ ) {
				String driveUnitDerivative = auftrag.getDriveUnitEngine_Derivative( "" + i ); //ermittle Antriebsart
				if( "O".equalsIgnoreCase( driveUnitDerivative ) || "D".equalsIgnoreCase( driveUnitDerivative ) ) { //nur DriveUnits mit Verbrennungsmotor ber�cksichtigen
					String driveUnitEngineSeries = auftrag.getDriveUnitEngine_Series( "" + i );
					if( driveUnitEngineSeries != null && driveUnitEngineSeries.trim().length() != 0 ) {
						engineSeries = driveUnitEngineSeries;
						break;
					}
				}
			}

		return engineSeries;
	}

}

package com.bmw.cascade.pruefprozeduren;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Vector;

import javax.swing.JLabel;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultSet;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.DisplayArea;

/**
 * Pr�fprozedur, welche alle Detailinformationen zu den gefundenen F_ORT_NR ausgibt.
 * @author Schumann, Saller, Buboltz
 * @version 1_0_F  06.08.08  CS  Erstinitialisierung. PP bietet die M�glichkeit alle Details zum Infospeicher bzw. zum Fehlerspeicher
 * 								 auszugeben. Dazu sind bei den Paramtern die Jobs IS_LESEN und IS_LESEN_DETAIL oder
 * 								 FS_LESEN und FS_LESEN_DETAIL anzugeben.
 * @version 2_0_T  10.11.11  MS  Zusammenfassung der beiden PP DiagLeseISEinzel und DiagLeseISEinzelUDS. Die Funktionalti�t der DiagLeseISEinzel
 * 								 wurde �bernommen. Er k�nnen sowohl alle IS bzw FS als auch einzelne IS und FS ausgelesen werden
 * @version 3_0_F  10.11.11  MS  2_0_T als F-Version �bernommen
 * 
 * @version 4_0_T  24.11.11  MS  Erweiterung um DEBUG-Ausgaben zur Fehleranalyse
 * 
 * @version 5_0_F  24.11.11  MS  4_0_T als F-Version �bernommen
 * 
 * @version 6_0_T  29.11.11  MS  Bugfix der Version 5_0_F
 * 
 * @version 7_0_F  29.11.11  MS  6_0_T als F-Version �bernommen
 * 
 * @version 8_0_T  05.12.11  MS  Erweiterung zum Auslesen der richtigen Werten bei vorbelegten Variablen (VARIANTE@SGBD_SIV)
 * 
 * @version 9_0_F  05.12.11  MS  8_0_T als F-Version �bernommen
 * 
 * @version 10_0_T 22.08.16  TB  Erweiterungen f�r Paralleldiagnose analog zur DiagToleranzPP (LOP 2015), Generics eingebaut + Compiler Warnungen behoben<BR>
 * 
 * @version 11_0_F 25.01.17  MKe F-Version 
 */
public class DiagLeseISEinzelUDS_11_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	Ergebnis result;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagLeseISEinzelUDS_11_0_F_Pruefprozedur() {
	}

	/**
	  * erzeugt eine neue Pruefprozedur.
	  * @param pruefling Klasse des zugeh. Pr�flings
	  * @param pruefprozName Name der Pr�fprozedur
	  * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	  */
	public DiagLeseISEinzelUDS_11_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "VISUAL", "DEBUG", "FCODE[1..N]", "TAG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "SGBD", "JOB1", "JOB2" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	@Override
	public boolean checkArgs() {
		String temp;
		String temp2;
		boolean ok, exist;
		int j;

		try {
			ok = true;
			// 1. Check: Sind alle requiredArgs gesetzt
			String[] requiredArgs = getRequiredArgs();
			for( int i = 0; i < requiredArgs.length; i++ ) {
				if( getArg( requiredArgs[i].toUpperCase() ) == null )
					ok = false;
				if( getArg( requiredArgs[i].toUpperCase() ).equals( "" ) == true )
					ok = false;
			}
			// 2. Check: Sind �berfl�ssige Argumente gesetzt, d.h. Argumente, die weder required noch
			// optional sind
			Enumeration<?> enu = getArgs().keys();
			String[] optionalArgs = getOptionalArgs();
			String givenkey;
			while( enu.hasMoreElements() ) {
				givenkey = (String) enu.nextElement();
				exist = false;
				j = 0;
				while( (exist == false) && (j < requiredArgs.length) ) {
					if( givenkey.equalsIgnoreCase( requiredArgs[j] ) == true )
						exist = true;
					j++;
				}
				j = 0;
				while( (exist == false) && (j < optionalArgs.length) ) {
					//Falls optionales Argument mit FCODE beginnt, dann nicht analysieren und auf true setzen
					if( givenkey.startsWith( "FCODE" ) ) {
						exist = true;
					}
					if( givenkey.equalsIgnoreCase( optionalArgs[j] ) == true )
						exist = true;
					j++;
				}
				if( exist == false )
					ok = false;
				if( getArg( givenkey ).equals( "" ) == true )
					ok = false;
			}

			if( ok == true ) {
				//SGBD
				temp = getArg( getRequiredArgs()[0] );
				if( temp == null ) {
					return false;
				}
				//Jobs - es m�ssen immer 2 Jobs angegeben werden
				temp = getArg( getRequiredArgs()[1] );
				if( temp == null ) {
					return false;
				}

				temp2 = getArg( getRequiredArgs()[2] );
				if( temp2 == null ) {
					return false;
				}

				//IS_LESEN und FS_LESEN d�rfen nicht vermischt werden
				if( temp.equalsIgnoreCase( "IS_LESEN" ) && !temp2.equalsIgnoreCase( "IS_LESEN_DETAIL" ) ) {
					return false;
				}

				if( temp.equalsIgnoreCase( "FS_LESEN" ) && !temp2.equalsIgnoreCase( "FS_LESEN_DETAIL" ) ) {
					return false;
				}
			}
			return ok;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String temp;
		String sgbd = null;
		String job1;
		String job2;
		int ergSaetze = 0;

		int FCodeAnzahl = 0; //  Variable fuer die Anzahl angegeben FCodes
		String argFCodes; // Wert des aktuellen FCodes
		String[] listFCodes = null; // Die angegeben FCodes werden in diesem String abgelegt
		String[] fCodes = null;
		String argument;
		String argument_result;
		String ergebnis;
		String visual;
		EdiabasResultSet[] ediabasResultSets;
		EdiabasProxyThread ediabas = null;

		//Paralleldiagnose
		boolean parallel = false;
		String tag = null;
		DeviceManager devMan = null;

		boolean debug = false;

		try {
			try {
				//Debug-Schalter setzen
				if( getArg( getOptionalArgs()[1] ).equalsIgnoreCase( "TRUE" ) ) {
					debug = true;
				}
			} catch( Exception e1 ) {
				//keine Aktion notwendig, da debug optional ist
			}

			try {

				// Einlesen der Anzahl der FehlerCodes
				FCodeAnzahl = 0;
				while( (argFCodes = getArg( "FCODE" + (FCodeAnzahl + 1) )) != null )
					FCodeAnzahl++;
				if( debug ) {
					System.out.println( "Anzahl vorgegebener FCodes: " + FCodeAnzahl );
				}
				// Einlesen der Anzahl der angegeben Fehler-Codes und Ablage aller Fehler-Codes
				//System.out.println("Anzahl FCODES " + String.valueOf(FCodeAnzahl));
				if( FCodeAnzahl > 0 ) {
					listFCodes = new String[FCodeAnzahl];
					for( int i = 0; i < FCodeAnzahl; i++ ) {
						listFCodes[i] = getArg( "FCODE" + (i + 1) );
						if( debug ) {
							System.out.println( "Aktueller FCode " + String.valueOf( getArg( "FCODE" + (i + 1) ) ) );
							System.out.println( "Arg " + String.valueOf( argFCodes ) );
							System.out.println( "i " + String.valueOf( i ) );
						}
					}

					fCodes = new String[FCodeAnzahl];
					//einzelne F_ORT_NR extrahieren
					for( int k = 0; k < listFCodes.length; k++ ) {

						fCodes[k] = listFCodes[k];
					}
				}

				//Parameter holen
				if( checkArgs() == false )
					throw new PPExecutionException( "Parameterexistenz" );

				sgbd = extractValues( getArg( getRequiredArgs()[0] ) )[0];
				job1 = getArg( getRequiredArgs()[1] );
				job2 = getArg( getRequiredArgs()[2] );

				//Visualisierung der Ergebnisse?
				visual = getArg( getOptionalArgs()[0] );
				if( visual == null )
					visual = "0";

			} catch( PPExecutionException e ) {
				//e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			try {
				// Paralleldiagnose, Pr�fstandvariabel auslesen, ob Paralleldiagnose 
				try {
					Object pr_var;

					pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
					if( pr_var != null ) {
						if( pr_var instanceof String ) {
							if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
								parallel = true;
							}
						} else if( pr_var instanceof Boolean ) {
							if( ((Boolean) pr_var).booleanValue() ) {
								parallel = true;
							}
						}
					}
				} catch( VariablesException e ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				}

				// Devicemanager
				devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();

				if( getArg( "TAG" ) != null )
					tag = getArg( "TAG" );

				if( parallel ) // Ediabas holen
				{
					try {
						ediabas = devMan.getEdiabasParallel( tag, sgbd );
					} catch( Exception ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Wrong Cascade configuration for parallel diagnosys", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();
					} catch( Throwable ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();
					}
				} else {
					try {
						ediabas = devMan.getEdiabas( tag );
					} catch( Exception ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();

					}
				}
				// <<<<< Paralleldiagnose            

				if( FCodeAnzahl < 1 ) {
					if( debug ) {
						System.out.println( "Job " + job1 + " wird ausgef�hrt!" );
					}
					//F-Codes z.B. mit job1 'IS_LESEN' holen
					int saetze = 0;
					temp = ediabas.executeDiagJob( sgbd, job1, "", "" );
					if( temp.equals( "OKAY" ) == false ) { // Es sind Fehler bei der Ausf�hrung aufgetreten
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job1, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					} else {
						result = new Ergebnis( "Status", "EDIABAS", sgbd, job1, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
					ergSaetze = ediabas.getDiagJobSaetze(); // Anzahl der gefunden Fehler
					ediabasResultSets = ediabas.getResult().getResultSets();
					for( int i = 0; i < ergSaetze; i++ ) {
						EdiabasResultSet ediabasResultSet = ediabasResultSets[i];
						Enumeration<?> results = ediabasResultSet.elements();
						for( ; results.hasMoreElements(); ) {
							Object testErg = (Object) results.nextElement();
							String tempTestErg = testErg.toString();
							if( tempTestErg.startsWith( "SAETZE" ) ) {
								saetze = Integer.valueOf( tempTestErg.substring( tempTestErg.indexOf( ":" ) + 1, tempTestErg.length() ).trim() ).intValue();
								fCodes = new String[saetze - 1];
							}
							if( tempTestErg.startsWith( "F_ORT_NR" ) ) {
								fCodes[FCodeAnzahl] = tempTestErg.substring( tempTestErg.indexOf( ":" ) + 1, tempTestErg.length() ).trim();
								FCodeAnzahl++;
							}
						}
					}
				}
				//wird ben�tigt f�r die Visualisierung der Ergebnisse
				Set<String> visualizationSet = new LinkedHashSet<String>();
				//f�r alle FCODE[1..n]
				for( int k = 0; k < fCodes.length; k++ ) {
					//Ausf�hrung des Ediabas job1s
					if( debug ) {
						System.out.println( "Job " + job2 + " mit dem Fehlercode " + fCodes[k] + " wird ausgef�hrt!" );
					}
					temp = ediabas.executeDiagJob( sgbd, job2, fCodes[k], "" );
					if( debug ) {
						System.out.println( "Ausf�hrung temp: " + temp );
					}

					if( temp.equals( "OKAY" ) == false ) { // Es sind Fehler bei der Ausf�hrung aufgetreten
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job2, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					} else {
						result = new Ergebnis( "Status", "EDIABAS", sgbd, job2, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
					visualizationSet.add( "F_Ort_Nr: " + fCodes[k] );
					//hier werden alle Ergebnisse aus dem job ausgelesen und abgespeichert
					ergSaetze = ediabas.getDiagJobSaetze(); // Anzahl der gefunden Fehler
					ediabasResultSets = ediabas.getResult().getResultSets();
					int tempLimit;

					for( int i = 0; i < ergSaetze - 2; i++ ) {
						EdiabasResultSet ediabasResultSet = ediabasResultSets[i + 1];
						Enumeration<?> enumResults = ediabasResultSet.elements();
						for( ; enumResults.hasMoreElements(); ) {
							Object testErg = (Object) enumResults.nextElement();
							ergebnis = testErg.toString();
							tempLimit = ergebnis.indexOf( ":" );
							argument = ergebnis.substring( 0, tempLimit - 1 );
							argument_result = ergebnis.substring( tempLimit + 1, ergebnis.length() ).trim();
							result = new Ergebnis( "Status", "EDIABAS", sgbd, job2, fCodes[k], argument + ", Satz " + (i + 1), argument_result, "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
							status = STATUS_EXECUTION_OK;
							visualizationSet.add( "F_Ort_Nr: " + fCodes[k] + " hat in Satz " + (i + 1) + " folgendes Ergebnis: " + ergebnis );
						}
					}
				}

				//Visualisierung der Ergebnisse?
				if( visual.equals( "1" ) ) {

					DisplayArea da = getPr�flingLaufzeitUmgebung().getDisplayArea( "Ergebnisse des Jobs " + job2, visualizationSet.size(), 1 );
					String[] testarray = new String[visualizationSet.size()];
					int z = 0;
					for( Iterator<String> listIter = visualizationSet.iterator(); listIter.hasNext(); ) {
						String single_result = (String) listIter.next();
						da.add( new JLabel( single_result ) );
						testarray[z] = single_result;
						z++;
						if( z % 250 == 0 ) { //nach 250 Eintr�gen neue DisplayArea erzeugen, da sonstnicht mehr alle Eintr�ge korrekt angezeigt werden k�nnen
							da.revalidate();
							da = getPr�flingLaufzeitUmgebung().getDisplayArea( "Ergebnisse des Jobs " + job2, visualizationSet.size(), 1 );
						}
					}
					//da.revalidate();
					//getPr�flingLaufzeitUmgebung().getUserDialog().displayStatusMessage("Ergebnisse des Jobs " + job1, testarray[z]);
				}

			} catch( ApiCallFailedException e ) {
				e.printStackTrace();
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job2, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job2, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job2, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Paralleldiagnose
		if( parallel ) {
			try {
				devMan.releaseEdiabasParallel( ediabas, tag, sgbd );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable t ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		setPPStatus( info, status, ergListe );

	}

}

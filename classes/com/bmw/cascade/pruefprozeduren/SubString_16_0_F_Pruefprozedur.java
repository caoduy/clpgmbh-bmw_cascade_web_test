package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;

/**
 * Bes. <br>
 * Implementierung einer Pr�fprozedur zur Bearbeitung eines 
 * Eingabestrings - Aus dem Inputstring wird Stringausschnitt
 * an parametrierten Stellen extrahiert.<br>
 * 
 * @author Staudinger, TI-431 <br>
 * @version 0.0.1.FA 28.02.2005 LS Ersterstellung <BR>
 *          2.0.F    03.04.2006 TB Zus�tzlicher optionaler Parameter 'COMPARE_STRING' <BR>
 *          3.0.T    26.03.2012 CW 'COMPARE_STRING' kann jetzt auch mit mehreren Strings bef�llt werden <BR>
 *			4.0.F    26.03.2012 CW 'COMPARE_STRING' kann jetzt auch mit mehreren Strings bef�llt werden, sobald einer
 *									dieser Strings auf den InString matcht, liefert die PP ein i.O. als Ergebnis <BR>
 *			5.0.T    17.04.2012 CW BugFix: Die Funktion Substring hat mit Version 3 und 4 immer Fehler geliefert - wurde gefixt<BR>
 *			6.0.F    17.04.2012 CW BugFix: Die Funktion Substring hat mit Version 3 und 4 immer Fehler geliefert - wurde gefixt<BR>
 *			7.0.T    07.05.2015 FS BugFix: Ausgabe von Stracktraces bei PP Fehler nur noch bei gesetztem DEBUG Flag<BR>
 *			8.0.F    16.11.2015 FS BugFix: Ausgabe von Stracktraces bei PP Fehler nur noch bei gesetztem DEBUG Flag<BR>
 *          9.0.T    16.11.2015 KW Optionale Argumente "HWT" hinzugef�gt<BR>   
 *          10.0.F   18.11.2015 KW Optionale Argumente "HWT" hinzugef�gt<BR>  
 *      	11.0.T   20.02.2017 MKe Zus�tzlicher Parameter Contains und Checkall hinzugef�gt durchzuf�hren.<BR>  
 *      	12.0.F   20.02.2017 MKe F-Version <BR>  
 *      	13.0.T   14.06.2017 MKe Ergebniserzeugung angepasst <BR>  
 *      	14.0.F   14.06.2017 MKe F-Version <BR>  
 *      	15.0.T   07.09.2017 MKe Bugfix beim NIO Ergebnis: Fehlerhafte Arraykonvertierung <BR>  
 *      	16.0.F   07.09.2017 MKe F-Version <BR>  
 */

public class SubString_16_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public SubString_16_0_F_Pruefprozedur() {
	}

	boolean DEBUG = false;

	/**
	 * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	 * 
	 * @param pruefling
	 *            Klasse des zugeh. Pr�flings 
	 * @param pruefprozName
	 *            Name der Pr�fprozedur
	 * @param hasToBeExecuted
	 *            Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public SubString_16_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 * 
	 * @return Stringarray der optionalen Argumente
	 */

	public String[] getOptionalArgs() {
		String[] args = { "StartIndex", "StopIndex", "CompareString", "HWT", "Contains", "Checkall" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 * 
	 * @return
	 */
	public String[] getRequiredArgs() {
		String[] args = { "InString" };
		return args;
	}

	/**
	 * prueft - soweit als m�glich - die Argumente auf Existenz und Wert
	 * 
	 * @return true, wenn die Ueberpruefung der zwingenden Argumente ok ist
	 */
	public boolean checkArgs() {
		try {
			return super.checkArgs();
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info
	 *            Information zur Ausf�hrung
	 */

	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		//SubString Parametervariablen
		String inString = null;
		String outString = null;
		String[] cmpString = null;
		int inStringLen = 0;
		int startIndex = 0;
		int stopIndex = 0;
		String hwt = "";
		String temp;
		String check_mode = "";
		boolean contains = false;
		boolean checkall = false;

		try {
			if( DEBUG )
				System.out.println( "--------------------------" );
			if( DEBUG )
				System.out.println( "Start PP Test" );
			//Parameter-Check
			if( checkArgs() == false )
				throw new PPExecutionException( PB.getString( "Parametrierfehler!" ) );

			//required Args holen
			inString = extractValues( getArg( getRequiredArgs()[0] ) )[0];
			inStringLen = inString.length();

			result = new Ergebnis( "InString", "CASCADE", "", "", "", "", inString, "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );

			if( DEBUG )
				System.out.println( "InString : " + inString );

			//optional Args holen
			if( getArg( getOptionalArgs()[0] ) != null )
				startIndex = Integer.parseInt( extractValues( getArg( getOptionalArgs()[0] ) )[0] );

			if( DEBUG )
				System.out.println( "StartIndex : " + startIndex );

			if( getArg( getOptionalArgs()[1] ) != null )
				stopIndex = Integer.parseInt( extractValues( getArg( getOptionalArgs()[1] ) )[0] );
			else
				stopIndex = inStringLen;

			if( DEBUG )
				System.out.println( "StopIndex : " + stopIndex );

			if( getArg( getOptionalArgs()[2] ) != null )
				cmpString = extractValues( getArg( getOptionalArgs()[2] ) );

			if( DEBUG )
				System.out.println( "CompareString : " + cmpString );

			if( getArg( getOptionalArgs()[3] ) != null )
				hwt = new String( getArg( getOptionalArgs()[3] ) ).toString();

			if( DEBUG )
				System.out.println( "hwt : " + hwt );

			if( getArg( getOptionalArgs()[4] ) != null ) {
				temp = new String( getArg( getOptionalArgs()[4] ) ).toString();
				if( (temp != null) && temp.equalsIgnoreCase( "TRUE" ) ) {
					contains = true;
				}
			}

			if( DEBUG )
				System.out.println( "Contains : " + contains );

			if( getArg( getOptionalArgs()[5] ) != null ) {
				temp = new String( getArg( getOptionalArgs()[5] ) ).toString();
				if( (temp != null) && temp.equalsIgnoreCase( "TRUE" ) ) {
					checkall = true;
				}
			}
			if( DEBUG )
				System.out.println( "Checkall : " + checkall );

			//Outstring - Entspricht InString mittels Indizes beschnitten 
			outString = inString.substring( startIndex, stopIndex );

			if( DEBUG )
				System.out.println( "OutString : " + outString );
			result = new Ergebnis( "OutString", "CASCADE", "", "", "", "", outString, new Integer( startIndex ).toString(), new Integer( stopIndex ).toString(), "", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );

			boolean resultCMP = false;
			boolean mismatch = false;

			//Vergleiche den SubString mit dem CompareString ignoriere Gro�- / Kleinschreibung
			if( cmpString != null && outString != null ) {
				for( int i = 0; i < cmpString.length; i++ ) {
					if( contains == true ) {
						check_mode = "contains";
						if( outString.contains( cmpString[i] ) ) {
							result = new Ergebnis( "Status", "System", "", "SubString", outString, "CompareContains", cmpString[i], "TRUE", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
							resultCMP = true;
							// nach dem ersten gefunden Ergebnis wird abgebrochen sofern checkall nicht true ist
							if( !checkall ) {
								break;
							}
						} else {
							result = new Ergebnis( "Status", "System", "", "SubString", outString, "CompareContains", cmpString[i], "FALSE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_IO );
							ergListe.add( result );
							mismatch = true;
						}
					} else {
						check_mode = "equal";
						if( outString.equalsIgnoreCase( cmpString[i] ) ) {
							result = new Ergebnis( "Status", "System", "", "SubString", outString, "CompareEqualsIgnoreCase", cmpString[i], "TRUE", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
							resultCMP = true;
							// nach dem ersten gefunden Ergebnis wird abgebrochen sofern checkall nicht true ist
							if( !checkall ) {
								break;
							}
						} else {
							result = new Ergebnis( "Status", "System", "", "SubString", outString, "CompareEqualsIgnoreCase", cmpString[i], "FALSE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_IO );
							ergListe.add( result );
							mismatch = true;

						}
					}
				}
			}

			//wenn nicht alle compare string  �bereinstimmen und checkall  mit true parametiert wurde dann resultCMP=false
			if( checkall && mismatch ) {
				resultCMP = false;
			}

			//wenn es keinen cmpString gibt, dann ist resultCMP=true
			if( cmpString == null ) {
				resultCMP = true;
			}

			if( !resultCMP ) {
				result = new Ergebnis( "Status", "System", "", check_mode, inString, "NoAnalogyFoundWith", outString, Arrays.toString( cmpString ), "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), hwt, Ergebnis.FT_NIO );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}

		} catch( PPExecutionException ppe ) {
			if( DEBUG )
				ppe.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "PPExecutionException", ppe.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			if( DEBUG )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ) + e.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( DEBUG )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ) + t.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}
		setPPStatus( info, status, ergListe );
	}
}

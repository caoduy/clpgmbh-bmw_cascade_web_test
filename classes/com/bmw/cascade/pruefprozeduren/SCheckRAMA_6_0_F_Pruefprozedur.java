package com.bmw.cascade.pruefprozeduren;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Pr�fprozedur zur optimierten Fahrzeugauswahl f�r den RAMA Fahrwerksstand.<BR>
 * <BR>
 * Funktion:<BR>
 * Auf Basis der Host/Share-Information des RAMA Fahrwerksstandes wird von dort aus einem Unterverzeichnis ein quasi ini basiertes Eingangsfile eingelesen.<BR>
 * Das INI-File enth�lt Vorgabeinformationen (Spurcode) von Fahrzeugen die an einer bestimmten Linie noch gepr�ft werden m�ssen.<BR>
 * Entspricht das aktuell am CASCADE Pr�fstand gepr�fte Fahrzeug dem Spurcodekriterium soll einmal f�r den Werker eine Info-Ausgabe erfolgen:<BR>
 * "Bitte Fahrwerkstand Linie: {0} anfahren! Fahrzeug f�r Referenzmessung abstellen!"<BR>
 * <BR>
 * Anzahl der Stationen und '0:=nicht relevant' und '1:=relevant' Markierungen m�ssen �bereinstimmen<BR>
 * Bsp. f�r Ini-File: RAMA_Orderfile.txt<BR>
 *  "Spurcode";"501";"502";"503";"504"
 *  "20";1;1;1;0
 *  "30";0;1;0;0
 *  "AD";0;0;0;0
 *  "XX";1;0;1;0
 *  "YY";0;0;1;1
 * <BR> 
 * In diesem Fall soll auf dem Fahrwerksstand ein einfaches Best�tigungsfile abgelegt werden.<BR>
 * Weiterhin erfolgt eine einfache APDM Doku.<BR>
 * <BR>
 * Hinweis:<BR>
 * Die Pr�fprozedur wird aufgrund Anwenderwunsch: Thomas Deisinger W2 in keinem Fall NIO.<BR>
 * <BR>
 * @author BMW TI-545 Thomas Buboltz (TB)<BR>
 * @version 1_0_F 17.04.2016 TB Implementierung<BR>
 *          2_0_T 30.04.2016 TB Erweiterungswunsch, wenn mehr als eine Fahrwerks-Anlage angefahren werden kann, dann dies mit ausgeben. 'Linie: 1, 2 oder 3'<BR>
 *          3_0_T 09.05.2016 TB Erweiterungswunsch, anderes Fileeingangsformat auswerten und die Ergebnis-Datei bereits vor der Benutzerinteraktion abspeichern. <BR>
 *          4_0_F 10.05.2016 TB F-Version. <BR>
 *          5_0_T 29.06.2016 TB Erweiterungswunsch von T. Deisinger: Die UserDialog Message soll kuerzer im Text dargestellt werden (Verbesserung Handterminal-Anzeige). <BR>
 *          6_0_F 30.06.2016 TB F-Version. <BR>
 */
public class SCheckRAMA_6_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public SCheckRAMA_6_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public SCheckRAMA_6_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "HOSTNAME_RAMA" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert!!!
	 */
	@Override
	public boolean checkArgs() {
		// Obligatorisch.
		return super.checkArgs();
	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( System.getProperty( "user.language" ).equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/*
	 * Systemsprache DE = true, wenn DE
	 */
	private final boolean DE = checkDE();

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// Parameter
		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
		String hostnameRAMA = null;
		final long RAMA_TIMEOUT_IN_MS = 5000; //maximaler Timeout f�r die Verf�gbarkeit der Remote RAMA Maschine und ihrer Shares

		// spezifische Variablen
		final String RAMA_ORDER = File.separator + "Referencemeasurement$" + File.separator + "RAMA_Order" + File.separator + "RAMA_Orderfile.txt";
		final String RAMA_RESULT = File.separator + "Referencemeasurement$";

		File orderFileRAMA = null, resultFileRAMA = null;

		// UserDialog und dbzgl. Anzeigen
		UserDialog userDialog = null;
		final String UD_TITLE = DE ? "Pr�fe RAMA" : "Check RAMA";
		final String UD_MESSAGE_CHECK_REFERENCE_HOST = DE ? "Pr�fe RAMA Referenzrechner..." : "Check RAMA reference host...";
		final String UD_MESSAGE_WORKER_INFORMATION = DE ? "Bitte FWS Linie: {0} anfahren! Fahrzeug f�r Referenzmessung abstellen!" : "Please bring vehicle to WAM station: {0}! Vehicle is reserved for reference check!";

		try {
			//Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG Parameter
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( !getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				} else {
					// alternativ DEBUG �ber Pr�fstandsvariable noch auswerten
					try {
						Object objDebug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
						if( objDebug != null ) {
							if( objDebug instanceof String && "TRUE".equalsIgnoreCase( (String) objDebug ) )
								bDebug = true;
							else if( objDebug instanceof Boolean && ((Boolean) objDebug).booleanValue() )
								bDebug = true;
						}
					} catch( VariablesException e ) {
						// Nothing, bDebug ist false vorbelegt
					}
				}

				// hostnameRAMA
				if( getArg( "HOSTNAME_RAMA" ) != null ) {
					hostnameRAMA = extractValues( getArg( "HOSTNAME_RAMA" ) )[0];
				}

				// Debug-Ausgabe
				if( bDebug ) {
					System.out.println( "PP SCheckRAMA: PARAMETERCHECK, Parameter: DEBUG=<" + bDebug + ">, HOSTNAME_RAMA=<" + hostnameRAMA + ">" );
				}

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// UserDialog holen und Anwender informieren
			userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
			userDialog.displayMessage( UD_TITLE, UD_MESSAGE_CHECK_REFERENCE_HOST, -1 );

			// Pr�fung der Verf�gbarkeit des ermittelten Files / Shares
			ExecutorService exSrv = Executors.newFixedThreadPool( 1 );
			ExecutorCompletionService<Boolean> completionService = new ExecutorCompletionService<Boolean>( exSrv );
			completionService.submit( new CheckRemoteFileCallable( orderFileRAMA = new File( File.separator + File.separator + hostnameRAMA, RAMA_ORDER ), resultFileRAMA = new File( File.separator + File.separator + hostnameRAMA, RAMA_RESULT ) ) );
			Boolean remoteFilesOK = false;
			try {
				Future<Boolean> futureResult = completionService.poll( RAMA_TIMEOUT_IN_MS, TimeUnit.MILLISECONDS );
				remoteFilesOK = futureResult.get();
			} catch( Exception e ) {
				// Sicherheitshalber alle Exceptions bearbeiten (NPE, InterruptedException, ...)
				if( bDebug ) {
					e.printStackTrace();
				}
			}
			exSrv.shutdown();

			// Debug-Ausgabe
			if( bDebug ) {
				System.out.println( "PP SCheckRAMA: orderFileRAMA=<" + orderFileRAMA + ">, resultFileRAMA=<" + resultFileRAMA + ">, remoteDirs=<" + (remoteFilesOK ? "OK" : "NOK!!!") + ">" );
			}

			String stationInfoForAPDM = "EMPTY";
			String shortVIN = this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7();

			// weitere Aktivit�ten nur machen, wenn die RAMA Referenzanlage und ihre Verzeichnisse verf�gbar sind..
			if( remoteFilesOK ) {
				// Spurcode des aktuellen Fahrzeuges
				String spurCode = this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getSpur().toUpperCase();

				String station = checkAndGetStationForWheelAlignmentCode( readOrderFileRAMA( orderFileRAMA, bDebug ), spurCode, bDebug );

				if( station != null ) {
					// Best�tigungsfile f�r die weitere Auswertung auf der RAMA Anlage ablegen
					writeResultFileRAMA( resultFileRAMA, shortVIN, spurCode, station, bDebug );

					// Info f�r APDM Fehlerprotokoll
					stationInfoForAPDM = station;

					// Benutzerinteraktion
					userDialog.reset();
					userDialog.requestUserMessage( UD_TITLE, MessageFormat.format( UD_MESSAGE_WORKER_INFORMATION, station ), 0 );
				}
			}

			// Debug-Ausgabe
			if( bDebug ) {
				System.out.println( "PP SCheckRAMA: Vehicle with shortVIN=<" + shortVIN + ">, is reserved for wheelAlignement Test at station=<" + stationInfoForAPDM + ">" );
			}

			//IO-Ablauf als einfachen APDM Status ggf. mit der konkret anzufahrenden Fahrwerksstation festhalten
			result = new Ergebnis( "SCheckRAMAStation", "", "", "", "", "STATION", stationInfoForAPDM, stationInfoForAPDM, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );

		} catch( PPExecutionException e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// UserDialog freigeben
		if( userDialog != null ) {
			try {
				this.getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( DeviceLockedException e ) {
			}
		}

		// PP Status setzen
		setPPStatus( info, status, ergListe );
	}

	/**
	 * Methode zum Einlesen des quasi Ini-Files, hier: 'RAMA_Orderfile.txt'
	 * @param orderFileRAMA, Eingangs File-Objekt
	 * @param bDebug, Debugschalter vgl. opt. PP Parameter
	 * @return Collection mit den Inhalten der Eingangsdatei ohne die kommentierende erste Zeile
	 */
	private HashMap<String, IniLine> readOrderFileRAMA( final File orderFileRAMA, final boolean bDebug ) {
		BufferedReader in = null;
		HashMap<String, IniLine> iniData = new HashMap<String, IniLine>();

		try {
			in = new BufferedReader( new FileReader( orderFileRAMA ) );

			String line = null;
			ArrayList<String> lineNames = new ArrayList<String>();
			int i = 0;
			while( (line = in.readLine()) != null ) {
				line = line.replaceAll( "\"", "" ); // Teilweise enthalten die Strings im Eingangsfile Anf�hrungszeichen. Diese zuerst gesamthaft entfernen.

				ArrayList<String> lineargs = new ArrayList<String>( Arrays.asList( line.split( ";" ) ) );

				//In der ersten Zeile z.B. ("Spurcode";"501";"502";"503";"504") sind Informationen �ber die Linien Bezeichnungen enthalten. Diese abspalten und merken. 
				if( lineargs.get( 0 ).toUpperCase().indexOf( "SPURCODE" ) != -1 ) {
					lineargs.remove( 0 );
					lineNames = lineargs;
				} else {
					// die weiteren Zeilen mit dem Key: Spurcode f�r die weitere Auswertung speichern
					iniData.put( lineargs.get( 0 ), new IniLine( lineNames, lineargs ) );
				}

				//Debug-Ausgabe
				if( bDebug ) {
					System.out.println( "PP SCheckRAMA: Method: readOrderFileRAMA, Read Input File, Line " + ++i + ": " + line );
				}
			}
		} catch( IOException e ) {
			if( bDebug )
				e.printStackTrace();
		} finally {
			if( in != null )
				try {
					in.close();
				} catch( IOException e ) {
				}
		}

		return iniData;
	}

	/**
	 * Datenstruktur zum Kapseln der Informationen in einer Zeile der RAMA Eingangsdatei
	 */
	private class IniLine {
		ArrayList<String> lineNames = null;

		String spurcode = null;
		ArrayList<String> markedLines = null;

		/**
		 * Konstruktor
		 * 
		 * @param lineNames, FWS Linien Namen
		 * @param lineArgs, die weiteren Eingangsparameter pro Zeile  
		 */
		public IniLine( ArrayList<String> lineNames, ArrayList<String> lineArgs ) {
			this.lineNames = lineNames;

			this.spurcode = lineArgs.get( 0 ).toUpperCase();
			lineArgs.remove( 0 ); //f�hrende Spurcode Info entfernen
			this.markedLines = lineArgs;
		}

		/**
		 * R�ckgabe einer IniLine Datenstruktur
		 *
		 * @return  IniLine Datenstruktur als Text
		 */
		@Override
		public String toString() {
			StringBuffer lineInfo = new StringBuffer( "" );
			for( int i = 0; i < lineNames.size(); i++ ) {
				lineInfo.append( lineNames.get( i ) + ":" + markedLines.get( i ) + " " );
			}
			return "wheelalignementcode:=" + spurcode + ", lines:=" + lineInfo.toString();
		}

	}

	/**
	 * Methode zum Ermitteln der anzufahrenden Fahrwerksstation
	 * 
	 * @param iniData, Datenstruktur der RAMA Eingangsdatei
	 * @param spurCode, SpurCode des akt. gepr�ften Fahrzeuges
	 * @param bDebug, Debugschalter vgl. opt. PP Parameter
	 * @return null oder gefundene RAMA Station oder Stationen
	 */
	private String checkAndGetStationForWheelAlignmentCode( HashMap<String, IniLine> iniData, String spurCode, boolean bDebug ) {
		StringBuffer sbStation = new StringBuffer( "" );
		final String DELIMITER = ", ";
		for( Entry<String, IniLine> entry : iniData.entrySet() ) {
			// Debug?
			if( bDebug ) {
				System.out.println( "PP SCheckRAMA: Method: checkAndGetStationForWheelAlignmentCode, InitLine=<" + entry.getValue().toString() + ">, (wheelAlignmentCodeFromVehicele=" + spurCode + ")" );
			}

			// Check! Bei richtigem Spurcode und entsprechenden '1' Marker dann addiere die Linen Bezeichnung hinzu.
			if( entry.getValue().spurcode.equals( spurCode ) ) {
				for( int i = 0; i < entry.getValue().markedLines.size(); i++ ) {
					if( entry.getValue().markedLines.get( i ).equals( "1" ) ) {
						sbStation.append( entry.getValue().lineNames.get( i ) );
						sbStation.append( DELIMITER );
					}
				}
			}
		}

		String station = sbStation.toString();
		return "".equals( station ) ? null : replaceLastToken( station.substring( 0, station.lastIndexOf( DELIMITER ) ), DELIMITER, DE ? " oder " : " or " );
	}

	/**
	 * Methode zum Ersetzen eines letzten Suchtreffers f�r das Eingangstoken
	 * 
	 * @param text, Eingangstext
	 * @param token, Suchtoken
	 * @param replacement, zu ersetzende Zeichenfolge
	 * @return R�ckgabetext als entsprechend angepasste Zeichenkette
	 */
	private String replaceLastToken( String text, String token, String replacement ) {
		return text.replaceFirst( "(?s)(.*)" + token, "$1" + replacement );
	}

	/**
	 * Schreiben des Best�tigungs-Files auf der RAMA Anlage
	 * 
	 * @param resultFileRAMA, Ziel-Ergebnisdatei
	 * @param shortVIN, kurze FGNR des gepr�ften Fahrzeuges
	 * @param spurCode, akt. Spurcode des gepr�ften Fahrzeuges
	 * @param station, anzufahrende Fahrwerksstation
	 * @param bDebug, Debugschalter vgl. opt. PP Parameter
	 */
	private void writeResultFileRAMA( File resultFileRAMA, String shortVIN, String spurCode, String station, boolean bDebug ) {
		long timestamp = System.currentTimeMillis();
		File resultFileTmp = new File( resultFileRAMA, File.separator + "RAMA_Result" + File.separator + "RAMA_Result_" + shortVIN + "_" + timestamp + ".txt.tmp" );
		File resultFile = new File( resultFileRAMA, File.separator + "RAMA_Result" + File.separator + "RAMA_Result_" + shortVIN + "_" + timestamp + ".txt" );
		BufferedWriter bw = null;

		try {
			bw = new BufferedWriter( new FileWriter( resultFileTmp ) );
			bw.write( "shortVIN;wheelAlignmentCode;stations" );
			bw.write( System.getProperty( "line.separator" ) );
			bw.write( shortVIN + ";" + spurCode + ";" + station.replaceAll( ", ", "," ).replaceAll( DE ? " oder " : " or ", "," ) );
		} catch( IOException e ) {
			if( bDebug ) {
				e.printStackTrace();
			}
		} finally {
			if( bw != null ) {
				try {
					bw.close();
				} catch( IOException e ) {
				}
			}
		}

		try {
			resultFileTmp.renameTo( resultFile );
		} catch( Exception e ) {
			if( bDebug ) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Verf�gbarkeitscheck der Remote File(s) in Form einer eigenen Callable Klasse
	 */
	private class CheckRemoteFileCallable implements Callable<Boolean> {
		private File orderFileRAMA = null;
		private File resultFileRAMA = null;

		/**
		 * Konstruktor Methode
		 * @param orderFileRAMA, File f�r die RAMA-Eingabedatei
		 * @param resultFileRAMA, File der RAMA-Ergebnisdatei
		 */
		public CheckRemoteFileCallable( File orderFileRAMA, File resultFileRAMA ) {
			this.orderFileRAMA = orderFileRAMA;
			this.resultFileRAMA = resultFileRAMA;
		}

		/**
		 * Eigentliche �berpr�fung des Shares (Die Methoden exists() und canWrite() k�nnen potentiell dauern)
		 *
		 * @return Boolean.TRUE, wenn das File Objekt existiert und darauf schreibend zugegriffen werden kann
		 * @throws Exception wenn das Ergebnis nicht ermittelt werden konnte
		 */
		@Override
		public Boolean call() throws Exception {
			return orderFileRAMA.exists() && orderFileRAMA.canWrite() && resultFileRAMA.exists() && resultFileRAMA.canWrite() ? Boolean.TRUE : Boolean.FALSE;
		}
	}
}

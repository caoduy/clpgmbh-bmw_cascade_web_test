package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/** Implementierung der Pruefprozedur, die auf die Existenz einer Datei wartet.<BR>
 * Anschlie�end werden die angegebenen Parameter ausgelesen und bewertet.<BR>
 * <BR>
 * Created on 14.06.04<BR>
 * <BR>
 * bei Aenderung Hr. Pichler TI-432 Tel. 68609 informieren!<BR>
 * <BR>
 * @author BMW TI-432 Pichler<BR>
 * @version V0_0_1  14.06.04  SP  Implementierung<BR>
 * @version V0_0_2  15.06.04  SP  Ueberpr�fung, ob Parameter vorhanden eingefuegt.<BR>
 * @version V0_0_3  17.06.04  SP  Benutzerbildschirm mit Abbruchm�glichkeit hinzugefuegt.<BR>
 * @version V4_0_T  31.03.16  MK  Zus�tzliches APDM Ergebnis hinzugef�gt falls versucht wird eine komplett leere Datei auszulesen.<BR>
 * @version V5_0_F  31.03.16  MK  Freigabe Version 4_0_T.<BR>
 * @version V6_0_T  12.05.16  FS  Optionaler ACCESS_TIMEOUT Parameter f�r Dateizugriff. Default: 10 Sekunden.<BR>
 * @version V7_0_T  25.05.16  MK  Bugfix bez�glich des optionalen Parameters ACCESS_TIMEOUT.<BR>
 * @version V8_0_T  07.06.16  MK  Default Timeout auf 5 Sekunden angepasst.<BR> 
 * @version V9_0_F  07.06.16  MK  Produktive Freigabe Version V_8_0_T.<BR> 
 * @version V10_0_T 24.05.17  TB  LOP 2201: PP DateiWaiterReader so erweitern, dass @-Operator bei der �bergabe des TimeOut m�glich ist.<BR> 
 * @version V11_0_T 29.05.17  TB  LOP 2201: f�r weitere Parameter den @-Operator eingebaut.<BR> 
 * @version V12_0_F 31.05.17  TB  F-Version.<BR> 
 * @version V13_0_T 12.06.17  TB  LOP 2206: Einbau EN-Nationalisierung f�r den Countdown Text. Generics.<BR> 
 * @version V14_0_F 12.06.17  TB  F-Version.<BR> 
 * @version V15_0_T 29.06.17  TB  LOP 2213: Einbau des ppt. Argument AWT. Wenn dies konfiguriert ist, wird dieser Text anstatt des Default Countdown Textes angezeigt.<BR> 
 * @version V16_0_F 29.06.17  TB  F-Version.<BR> 
 */
public class DateiWaiterReader_16_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	static final long DEFAULT_TIMEOUT = 5L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DateiWaiterReader_16_0_F_Pruefprozedur() {
	}

	/**
	   * erzeugt eine neue Pruefprozedur.
	   * @param pruefling Klasse des zugeh. Prueflings
	   * @param pruefprozName Name der Pruefprozedur
	   * @param hasToBeExecuted Ausfuehrbedingung f�r Fehlerfreiheit
	   */
	public DateiWaiterReader_16_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "DELETE_FILE", "TRACE", "ACCESS_TIMEOUT", "AWT" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "DATEI", "MAX_WAIT_IN_SEC", "PARAM_TO_READ", "VALUE_TO_GET" };
		return args;
	}

	/**
	 * Prueft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 */
	@Override
	public boolean checkArgs() {
		return super.checkArgs();
	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( System.getProperty( "user.language" ).equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * fuehrt die Pruefprozedur aus
	 * @param info Information zur Ausfuehrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;
		super.attributeInit();

		// spezifische Variablen
		String datei = null;
		String paramToRead = null;
		String valueToGet = null;
		String awt = null;
		File f = null;
		int waitTimeInSec = 0;
		boolean deleteFile = false;
		boolean trace = false;
		UserDialog myDialog = null;
		long timeout = DEFAULT_TIMEOUT;
		final boolean DE = checkDE(); // Systemsprache DE wenn true

		try {
			//Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( "Parameterexistenz" );

				//Optionalen Parameter 0: "DELETE_FILE" pr�fen
				if( getArg( "DELETE_FILE" ) != null ) {
					if( (extractValues( getArg( "DELETE_FILE" ) )[0]).toUpperCase().equals( "TRUE" ) ) {
						deleteFile = true;
					}
				}

				//Optionalen Parameter 1: "TRACE" pr�fen
				if( getArg( "TRACE" ) != null ) {
					if( (extractValues( getArg( "TRACE" ) )[0]).toUpperCase().equals( "TRUE" ) ) {
						trace = true;
					}
				}

				//Optionalen Parameter 2: "ACCESS_TIMEOUT" "checken
				if( getArg( "ACCESS_TIMEOUT" ) != null ) {
					timeout = Long.parseLong( extractValues( getArg( "ACCESS_TIMEOUT" ).trim() )[0] );

					//negative Werte auf DEFAULT_TIMEOUT begrenzen
					if( timeout < 0 ) {
						timeout = DEFAULT_TIMEOUT;
					}
				}

				//Optionalen Parameter 3: "AWT" "checken
				if( getArg( "AWT" ) != null ) {
					awt = extractValues( getArg( "AWT" ) )[0];
				}

				//Required parameter 0: "DATEI" einlesen
				datei = extractValues( getArg( "DATEI" ) )[0];
				f = new File( datei );

				//Required parameter 1: "MAX_WAIT_IN_SEC" einlesen
				waitTimeInSec = Integer.parseInt( extractValues( getArg( "MAX_WAIT_IN_SEC" ) )[0] );
				//negative Werte auf 0 begrenzen
				if( waitTimeInSec < 0 ) {
					waitTimeInSec = 0;
				}

				//Required parameter 2: "PARAM_TO_READ" einlesen
				paramToRead = extractValues( getArg( "PARAM_TO_READ" ) )[0];

				//Required parameter 3: "VALUE_TO_GET" einlesen
				valueToGet = extractValues( getArg( "VALUE_TO_GET" ) )[0];
			} catch( Exception e ) {
				e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "PARAFEHLER", "", "", "", "", "", "", "", "", "0", "", "", "", "PARAMETRIERFEHLER", e.getMessage(), Ergebnis.FT_NIO_SYS );
				else {
					result = new Ergebnis( "PARAFEHLER", "", "", "", "", "", "", "", "", "0", "", "", "", "PARAMETRIERFEHLER", "", Ergebnis.FT_NIO_SYS );
				}
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//Datei auf Existenz pruefen, ggfs. Wartezeit abwarten
			if( trace == true ) {
				System.out.println( "Datei�berpr�fung startet! Datei = " + datei );
			}
			try {
				boolean exist = false;
				boolean cancelled = false;

				myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
				myDialog.setAllowCancel( true );

				// Pr�fe Existenz des Parent Folders in Executorthread um einen Timeout zu erm�glichen
				ExecutorService exSrv = Executors.newSingleThreadExecutor();
				ExecutorCompletionService<Boolean> completionService = new ExecutorCompletionService<Boolean>( exSrv );
				completionService.submit( new CheckFileExists( f.getParentFile() ) );
				Future<Boolean> fileExists = completionService.poll( timeout, TimeUnit.SECONDS );
				if( fileExists == null || fileExists.get() != true ) {
					result = new Ergebnis( "STATUS", "SYSTEM", datei, "", "", "ACCESS FILE EXCEPTION", ".", "", "", "", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					exSrv.shutdown();
					throw new PPExecutionException();
				}
				exSrv.shutdown();

				while( exist == false && waitTimeInSec > 0 && cancelled == false ) {
					exist = f.exists();
					try {
						if( !myDialog.isCancelled() ) {
							if( awt != null ) {
								myDialog.displayMessage( DE ? "Info" : "Information", awt, -1 );
							} else {
								myDialog.displayMessage( DE ? "Info" : "Information", DE ? "Warte " + waitTimeInSec + " Sekunden auf Dateiexistenz" : "Waiting " + waitTimeInSec + "s for file existence", -1 );
							}
						}
						if( myDialog.isCancelled() ) {
							if( trace == true ) {
								System.out.println( "Abbruch!!" );
							}
							cancelled = true;
							result = new Ergebnis( "STATUS", "SYSTEM", datei, "", "", "NUTZERABBRUCH", ".", "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
							myDialog.close();
							try {
								getPr�flingLaufzeitUmgebung().releaseUserDialog();
							} catch( Exception e ) {
								if( e instanceof DeviceLockedException ) {
									result = new Ergebnis( "EXECFEHLER", "USERDIALOG", "", "", "", "", "", "", "", "0", "", "", "", "DEVICELOCKEDEXCEPTION", "", Ergebnis.FT_NIO_SYS );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								} else {
									result = new Ergebnis( "EXECFEHLER", "USERDIALOG", "", "", "", "", "", "", "", "0", "", "", "", "EXCEPTION", "REALEASE", Ergebnis.FT_NIO_SYS );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
							}
							throw new PPExecutionException();
						}
						if( exist == false && cancelled == false ) {
							Thread.sleep( 1000 );
							waitTimeInSec--;
							if( trace == true ) {
								System.out.println( "Warte...!" );
							}
						}
					} catch( InterruptedException e ) {
						throw new Exception( "Wait time interrupted" );
					}
				}

				if( waitTimeInSec == 0 ) {
					myDialog.close();
					try {
						getPr�flingLaufzeitUmgebung().releaseUserDialog();
					} catch( Exception e ) {
						if( e instanceof DeviceLockedException ) {
							result = new Ergebnis( "EXECFEHLER", "USERDIALOG", "", "", "", "", "", "", "", "0", "", "", "", "DEVICELOCKEDEXCEPTION", "", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} else {
							result = new Ergebnis( "EXECFEHLER", "USERDIALOG", "", "", "", "", "", "", "", "0", "", "", "", "EXCEPTION", "REALEASE", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}
					if( trace == true ) {
						System.out.println( "Zeit�berschreitung beim Warten auf Dateiexistenz!\nWartezeit: " + waitTimeInSec + " Sekunden" );
					}
					result = new Ergebnis( "STATUS", "SYSTEM", datei, "", "", "ZEITUEBERSCHREITUNG", "DATEIEXISTENZ", "0S", waitTimeInSec + "S", "", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}

				if( f.exists() ) {
					// *******Auslesen des Parameters!!!!!!!!!!!!!!!!!!
					try {
						if( trace == true ) {
							System.out.println( "Datei existiert...Suche nach Parameter" );
						}
						FileInputStream fiStream = new FileInputStream( f );
						InputStreamReader isReader = new InputStreamReader( fiStream );
						BufferedReader bufread = new BufferedReader( isReader );
						bufread.mark( 100 );
						String selectedParam = new String( "[" + paramToRead + "]" );
						String temp = null;
						String temp2 = null;
						boolean found = false;
						FileInputStream fiStream2 = new FileInputStream( f );
						InputStreamReader isReader2 = new InputStreamReader( fiStream2 );
						BufferedReader bufread2 = new BufferedReader( isReader2 );
						if( bufread2.readLine() == null ) {
							result = new Ergebnis( "STATUS", "SYSTEM", datei, "", "", "DATEI_LEER", "N.I.O.", "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						}
						while( (found != true) && ((temp = bufread.readLine()) != null) ) {
							if( trace == true ) {
								System.out.println( "Parameter ist:" + temp );
								System.out.println( "Parameter soll:" + selectedParam );
							}
							if( (temp.toUpperCase().equals( selectedParam.toUpperCase() )) || (temp.toUpperCase().equals( (selectedParam + "\n").toUpperCase() )) ) {
								result = new Ergebnis( "STATUS", "SYSTEM", datei, "", "", "PARAMETER_GEFUNDEN", "I.O.", "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
								if( trace == true ) {
									System.out.println( "Parameter gefunden...Lese Wert aus!" );
								}
								found = true;
								temp2 = bufread.readLine();
								if( trace == true ) {
									System.out.println( "Wert ist:" + temp2 );
									System.out.println( "Wert soll:" + valueToGet );
								}
								if( temp2.toUpperCase().equals( valueToGet.toUpperCase() ) || temp2.toUpperCase().equals( (valueToGet + "\n").toUpperCase() ) ) {
									if( trace == true ) {
										System.out.println( "Werte identisch!" );
									}
									result = new Ergebnis( "STATUS", "SYSTEM", datei, paramToRead, "", "WERT_VERGLEICH", temp2, "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
									ergListe.add( result );
									status = STATUS_EXECUTION_OK;
								} else {
									result = new Ergebnis( "STATUS", "SYSTEM", datei, paramToRead, "", "WERT_VERGLEICH", temp2, valueToGet, "", "", "", "", "", "", "", Ergebnis.FT_IO );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
							}
							if( found == false ) {
								result = new Ergebnis( "STATUS", "SYSTEM", datei, "", "", "PARAMETER_GEFUNDEN", "N.I.O.", "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR;
								if( trace == true ) {
									System.out.println( "Parameter konnte nicht gefunden werden" );
								}
							}
						}
						//Dateizugriff bereinigen!
						bufread.close();
						isReader.close();
						fiStream.close();
						bufread2.close();
						//L�schen der Datei, wenn entsprechender Parameter gesetzt!
						if( deleteFile ) {
							boolean deleteOk = f.delete();
							if( trace == true ) {
								System.out.println( "L�schen der Datei erfolgreich? " + deleteOk );
							}
							if( deleteOk ) {
								result = new Ergebnis( "STATUS", "SYSTEM", datei, "", "", "DATEI_LOESCHEN", "I.O.", "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
								if( trace == true ) {
									System.out.println( "Datei wurde gel�scht!" );
								}
							} else {
								result = new Ergebnis( "STATUS", "SYSTEM", datei, "", "", "DATEI_LOESCHEN", "N.I.O.", "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
								if( trace == true ) {
									System.out.println( "Datei konnte nicht gel�scht werden!" );
								}
							}
						} else if( trace == true ) {
							System.out.println( "Datei wurde nicht gel�scht!" );
						}
					} catch( IOException e ) {
						e.printStackTrace();
						throw new Exception( "IO Exception" );
					}
				}
			} catch( SecurityException e ) {
				e.printStackTrace();
				result = new Ergebnis( "STATUS", "SYSTEM", datei, "", "", "SECURITY_EXCEPTION", "", "", "", "", "", "", "", "", "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				throw new PPExecutionException();
			}
		} catch( PPExecutionException e ) {
			e.printStackTrace();
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "EXECFEHLER", "", "", "", "", "", "", "", "", "0", "", "", "", "EXCEPTION", e.getMessage(), Ergebnis.FT_NIO_SYS );
			if( trace == true ) {
				System.out.println( "Exception" );
			}
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;

		} catch( Throwable t ) {
			t.printStackTrace();
			result = new Ergebnis( "EXECFEHLER", "", "", "", "", "", "", "", "", "0", "", "", "", "THROWABLE", t.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}
		setPPStatus( info, status, ergListe );
	}

	/**
	 *  Die Klasse CheckFileExists erm�glicht ein threadbasiertes �berpr�fen auf Dateiexistenz.
	 * 
	 * @author Sch�nert Fabian, TI-545 BMW AG
	 *
	 */
	class CheckFileExists implements Callable<Boolean> {

		private File file;

		public CheckFileExists( File file ) {
			this.file = file;
		}

		@Override
		public Boolean call() throws Exception {
			return file.exists();
		}
	}
}

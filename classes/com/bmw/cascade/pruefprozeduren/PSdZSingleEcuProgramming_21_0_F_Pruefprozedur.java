package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import java.util.Map.Entry;

import com.bmw.appframework.logging.LogLevel;
import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.psdz.PSdZ;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeConfigError;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeECU;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeExecutionStatus;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeFilteredTACategory;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTA;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTACategories;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTAL;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTALExecutionStatus;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTALFilter;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTALLine;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTALTACategory;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.CascadePSdZArgumentException;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.CascadePSdZConditionsNotCorrectException;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.CascadePSdZRuntimeException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.util.logging.CascadeLogging;

/**
 * Implementierung einer Pr�fprozedur, die es erm�glicht ein- oder mehere ECU zu programmieren. <BR>
 * Diese PP setzt voraus, dass eine SVTist und eine SVTsoll (Sollverbauung vorhanden ist)! <BR>
 * Dazu kann beispielsweise die PP PSdZGetSVTs benutzt werden. <BR>
 * <BR>
 * W�hrend der TAL Ausf�hrung wird ein Benutzerdialog eingeblendet, der die aktuell <BR>
 * behandelten SG und die noch zu behandelnden SG anzeigt. <BR>
 * Der Benutzer hat die M�glichkeit die Behandlung der TAL abzubrechen. <BR>
 * Die Behandlung wird zum n�chsten m�glichen Zeitpunkt abgebrochen. (PSdZ Verantwortung)<BR>
 * <BR>
 * 
 * @author Thomas Buboltz, BMW AG; Dieter Busetti, GEFASOFT AG; Martin Gampl, BMW AG <BR>
 *         <BR>
 * @version 01.04.2007 TB Ersterstellung <BR>
 *          12.04.2007 TB DEU / ENG Texte hinzu und include- / exclude Liste + "*" Platzhalter <BR>
 *          16.04.2007 TB Zaehlen der SG Anzahl korrigiert (SG und nicht Aktionen) <BR>
 *          21.04.2007 TB SG aus der Sollverbauung verwenden <BR>
 *          25.04.2007 TB SVTSoll auch bei den Stern Platzhaltern <BR>
 *          06.06.2007 TB �berarbeitung SG Zaehlung (Hintergund: Wir bekommen jede PSdZ TAL mit, SGBM IDs k�nnen unterschiedlich sein) <BR>
 *          06.06.2007 TB �berfl�ssigen Code f�r die Auswertung auskommentiert <BR>
 *          01.07.2007 TB �nderungen aufgrund TAL Filter �nderung, neuer optionaler Parameter BTL_FLASH (default = false [no Bootloader Flash]) <BR>
 *          03.07.2007 TB Robustheit bei der Parametrierung verbessert <BR>
 *          08.07.2007 TB Hotfix bei der Filterung (einstellig / zweistellig) <BR>
 *          26.10.2007 TB IO / NIO Steuerger�te in APDM eintragen und ECU Z�hlung BugFix <BR>
 *          03.01.2008 DB Einbau "testScreenID" in Debug-Ausgaben <BR>
 *          22.01.2008 TB !!! inkompatibel mit Cascade Versionen < 2.2.5!! Einbau Erkennung KIS Doppeltreffer, bei weicher KIS -> diese f�hren zu PP NIO <BR>
 *          17.02.2008 TB KIS Doppeltreffer immer als APDM Fehler ausgeben, ECU IO / NIO Daten f�r APDM erweitert um Prozessklassen Info, BugFix Include / Exclude Listen Behandlung <BR>
 *          28.04.2008 DB Neueste Fehlertexte <BR>
 *          07.05.2008 DB Detailmeldungen je ECU anzeigen <BR>
 *          18.05.2008 TB ECU Statistik dazu <BR>
 *          27.05.2008 TB antortet immer und unbekannt ausgeblendet (Wunsch DGF) <BR> 
 *          02.06.2008 DB Fehlertexte aus der TAL, Ersetzen deprecated-Methode 'psdz.generateAndStoreSollverbauungTAL(...)' durch 'psdz.generateAndStoreTAL(...)' <BR> 
 *          18.06.2008 DB Performanceoptimierung durch Verlagerung von Code <BR> 
 *          24.06.2008 DB �nderung holen des "testScreenID" <BR>
 *          13.07.2009 TB Logging der finalen TAL als "TALSTATUS" XML File im LogVerzeichnis <BR>
 *          08.09.2009 PR Probleme mit UserDialog dadurch gel�st, dass pro Dialog eine neue Instanz angelegt wird (keine Wiederverwendung) <BR>
 *          20.10.2009 TB Konfigurierbare Modusumschaltung eingebaut 2 neue optionale Args ["MODE_SWITCH_GATEWAY_LIST", "MODE_SWITCH"]<BR>
 *          01.08.2012 BL Dynamic Retry Funktion eingebaut
 *          06.11.2012 BL Dynamic Retry Funktion korrigiert
 *          08.03.2013 BL Dynamic Retry Funktion optimiert
 *          16.04.2014 MG 
 *          - Fehlerausgaben in Pr�fungsprotokoll verbessert. Durch Nutzung des neuen TAL-Schemas ab PSdZ 4.9 erfolgt nun zus�tzlich die Ausgabe �ber die genaue Fehlerstelle im Ablauf (Aktion). <BR>
 *          - Ausgabe der TAIS-Teilenummer des logistischen Teils, sowie, falls abweichend davon, zus�tzlich Ausgabe der TAIS-Teilenummer des bestellbaren Teils (analog wie in PP PSdZExecuteTAL) <BR>
 *          - Optionale Parameter DIFFERENTIAL_EXECUTION (nicht mehr genutzt seit CASCADE 5.1.0/PSdZ 4.6.1) und STOP_ON_ERROR (nicht mehr genutzt seit CASCADE 4.2.0/PSdZ 4.4.2) wurden entfernt <BR>
 *          - Optionaler Parameter IGNORE_ERRORS wird in PP noch aus Gr�nden der Abw�rtskompatibilit�t mit aktuellem PW unterst�tzt, jedoch ab CASCADE 6.1.0/PSdZ 4.9.1 nicht mehr genutzt <BR>
 *          - APDM-Ausgaben strukturiert. Es erfolgt immer (d.h. f�r IO-TAL , NIO-TAL ggf. mit Wiederholung, Abbruch durch Werker) zun�chst die Ausgabe der IO-Steuerger�te, danach die Ausgabe der NIO-Steuerger�te und danach der Gesamtstatus der TAL. <BR>
 *          - Ausbau der deprecated Methoden/deprecated TAL-Status <BR>
 *          - Generics hinzu <BR>
 *          04.06.2014 MG F-Version
 *          
 */
public class PSdZSingleEcuProgramming_21_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	
	/**
	 * PSdZ als globale Variable
	 */
	private PSdZ psdz = null;

	/**
	 * DE fuer Sprachermittlung
	 */
	private final boolean DE = checkDE(); // Systemsprache DE wenn true

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSdZSingleEcuProgramming_21_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSdZSingleEcuProgramming_21_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "VEHICLE_STATE_INCLUDE_LIST", "VEHICLE_STATE_EXCLUDE_LIST", "INCLUDE_LIST", "EXCLUDE_LIST", "BTL_FLASH", "FORCE_EXECUTION", "PARALLEL_EXECUTION", "MAX_ERROR_REPEAT", "IGNORE_ERRORS", "MODE_SWITCH_GATEWAY_LIST", "MODE_SWITCH", "DEBUG", "DEBUG_PERFORM", "DYNAMIC_RETRIES" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = {};
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		if( !super.checkArgs() )
			return false;
		return true;
	}

	/**
	 * Berechne auf Basis von einem Eingangsarray ein Ausgangsarray, wobei ein * Platzhalter f�r SG Adressen verwendet werden kann. i.d.R. sind dies immer 2 Listen als erstes die include- und als zweites die exclude List Ist kein * Platzhalter in den Eingangslisten, ist das Ausgangsarray 1:1 dem Eingangsarray. Bei Verwendung eines Platzhalters wird die jeweilige Liste mit allen SG Adressen (au�er die in der STAR_IGNORE_LIST) aufgef�llt.
	 * 
	 * @param AusgangsArray mit zwei StringArrayListen (1. Liste = *includeList, 2. Liste = *excludeList) - eine Liste kann einen * Platzhalter beinhalten
	 * @param cPlaceholder Marker welche Liste den * Platzhalter beinhaltet, i.d.R nur die Werte 'i', 'e' oder 'n'
	 * @param strTextForLogging Information f�rs Logging
	 * @param bDebug, true im Debugmodus
	 * @return AusgangsArray mit zwei StringArrayListen (1. Liste = *includeList, 2. Liste = *excludeList) - R�ckgabewert jeweils 1:1 oder auf Basis des Platzhalters
	 */
	private String[][] calculatePlaceholderForLists( String[][] inputList, char cPlaceholder, String strTextForLogging, boolean bDebug ) {
		String[][] outputList = { {}, {} };
		ArrayList<String> maskList = new ArrayList<String>();
		// df- funkt. Adr neues Bordnetz; ef-funkt. Adr altes Bordnetz; f1, f4, f5 Testeradressen
		final String[] STAR_IGNORE_LIST = { "df", "ef", "f1", "f4", "f5" }; // IGNORE Liste f�r die Platzhalter-Verwendung, hier k�nnen z.B. die funktionalen ECU Adressen stehen, ACHTUNG: hier z.B. nur "ef" und nicht "EF" eintragen
		final List<String> STAR_IGNORE_LIST_AS_LIST = Arrays.asList( STAR_IGNORE_LIST );

		// Berechnung der ECU Maske, bei Verwendung eines Platzhalters '*' bei der vehicleStateIncludeList oder der vehicleStateExcludeList
		switch( cPlaceholder ) {
			// include Liste mit '*'
			case 'i':
				List<String> vehicleStateExcludeListAsList = Arrays.asList( inputList[1] );

				for( int i = 0; i < 256; i++ ) {
					if( !vehicleStateExcludeListAsList.contains( Integer.toHexString( i ) ) && !vehicleStateExcludeListAsList.contains( Integer.toHexString( i ).toUpperCase() ) && !STAR_IGNORE_LIST_AS_LIST.contains( Integer.toHexString( i ) ) )
						maskList.add( Integer.toHexString( i ) );
				}
				outputList[0] = (String[]) maskList.toArray( new String[maskList.size()] );

				if( bDebug )
					System.out.println( "'" + strTextForLogging + "' MaskList bei IncludeList: " + maskList.toString() );

				break;

			// exclude Liste mit '*'
			case 'e':
				List<String> vehicleStateIncludeListAsList = Arrays.asList( inputList[0] );

				for( int i = 0; i < 256; i++ ) {
					if( !vehicleStateIncludeListAsList.contains( Integer.toHexString( i ) ) && !vehicleStateIncludeListAsList.contains( Integer.toHexString( i ).toUpperCase() ) && !STAR_IGNORE_LIST_AS_LIST.contains( Integer.toHexString( i ) ) )
						maskList.add( Integer.toHexString( i ) );
				}
				outputList[1] = (String[]) maskList.toArray( new String[maskList.size()] );

				if( bDebug )
					System.out.println( "'" + strTextForLogging + "' MaskList bei ExcludeList: " + maskList.toString() );

				break;

			// 1:1
			default:
				outputList = inputList;
		}
		return outputList;
	}

	/**
	 * Pr�fe und modifiziere ein Eingangssarray aus ECU Hex Adressen.
	 * Ignoriere den '*' als m�glichen Platzhalter, der ebenfalls in der Liste auftauchen kann
	 * 
	 * @param strInputArray mit ECU SG Adressen in hexadezimaler Form
	 * @return modifiziertes Eingangsarray aus ECU Hex Adressen mit Kleinbuchstaben
	 */
	private String[] checkAndModifyEcuHexArray( String[] strInputArray ) {
		for( int i = 0; i < strInputArray.length; i++ ) {
			if( !strInputArray[i].equalsIgnoreCase( "*" ) )
				strInputArray[i] = Integer.toHexString( new Integer( Integer.parseInt( strInputArray[i], 16 ) ).intValue() ).toLowerCase();
		}

		return strInputArray;
	}

	/**
	 * Tr�gt ECUs in die Ergebnisliste (APDM) ein
	 * 
	 * @param mErgListe Ergebnisliste
	 * @param mMap Map mit einzutragenden ECU's
	 * @param mTransact einzutragende Transaktion
	 * @param mResult einzutragendes Ergebnis
	 * @param mPsdzErrors zugeh�rige einzutragende Fehler
	 * @param mECUStatistics Map mit SG Antwortverhalten
	 */
	private void entryECUsDetails( Vector<Ergebnis> mErgListe, TreeMap<String, HashSet<String>> mMap, String mTransact, String mSResult, HashMap<String, List<Exception>> mPsdzErrors, HashMap<String, String> mECUStatistics ) {
		Ergebnis mResult;
		String key, fehlertext, antwortVerhalten;
		CascadePSdZRuntimeException cscRE;
		Exception ex;
		String action = DE ? "Aktion: " : "Action: ";
		String errorDescription = DE ? "Fehlerbeschreibung: " : "Error description: ";
		// TODO: Lokalisierung des Fehler-Kontextes in der CRE (TALLINE, PREPARATION, AUTHENTICATION, FINALIZATION); Kann ab fl�chiger Nutzung von CASCADE 7.0 wieder ausgebaut werden, da in CASCADE 7.0 Fehler-Kontexte vollst�ndig lokalisiert sind
		String talLine = DE ? "Steuerger�tebehandlung" : "ECU handling";
		String preparation = DE ? "Vorbereitung" : "Preparation";
		String authentication = DE ? "Authentifizierung" : "Authentication";		
		String finalization = DE ? "Nachbereitung" : "Finalization";
		// TAIS Nr BestellTeil
		String titleBestellTeilTAIS = DE ? "BestellNr: " : "OrderPartNo: ";

		// Schleife �ber alle Eintr�ge der �bergebenen Map
		for( Iterator<Entry<String, HashSet<String>>> e = mMap.entrySet().iterator(); e.hasNext(); ) {
			Map.Entry<String, HashSet<String>> entry = e.next();
			cscRE = null;
			ex = null;

			HashSet<String> myContent = new HashSet<String>();
			myContent = ((HashSet<String>) entry.getValue());

			Iterator<String> i = myContent.iterator();
			StringBuffer text = new StringBuffer();
			boolean hasNext = i.hasNext();
			Object o = null;
			
			while( hasNext ) {
				o = i.next();
				text.append( PB.getString( "psdz.tal." + String.valueOf( o ).toLowerCase().trim() ) );
				hasNext = i.hasNext();
				if( hasNext )
					text.append( ", " );
			}

			if( !mSResult.equalsIgnoreCase( Ergebnis.FT_IO ) ) {
				for( Iterator<String> j = myContent.iterator(); j.hasNext(); ) {
					String sgCat = (String) j.next();
					
					for( int excep = 0; excep < (mPsdzErrors.get( ((String) entry.getKey()).substring( 0, 4 ) + "_" + sgCat.toLowerCase().trim() )).size(); excep++ ) {
						ex = (mPsdzErrors.get( ((String) entry.getKey()).substring( 0, 4 ) + "_" + sgCat.toLowerCase().trim() )).get( excep );
					if( ex instanceof CascadePSdZRuntimeException ) {
							cscRE = (CascadePSdZRuntimeException) ex;
					}
					
					// TODO: Lokalisierung des Fehler-Kontextes in der CRE (TALLINE, PREPARATION, AUTHENTICATION, FINALIZATION); Kann ab fl�chiger Nutzung von CASCADE 7.0 wieder ausgebaut werden, da in CASCADE 7.0 Fehler-Kontexte vollst�ndig lokalisiert sind 
					String errorContextTranslated = cscRE.getStrErrorContextTranslated();
					if ( errorContextTranslated == "TALLINE" )  {
						errorContextTranslated = talLine;
					} else if ( errorContextTranslated == "PREPARATION" ) {
						errorContextTranslated = preparation;
					} else if ( errorContextTranslated == "AUTHENTICATION" ) {
						errorContextTranslated = authentication;
					} else if ( errorContextTranslated == "FINALIZATION" ) {
						errorContextTranslated = finalization;
					} else {
						// Fehler-Kontext ist bereits lokalisiert (CASCADE 7.0) bzw. keine der o.g. Lokalisierungen zutreffend
					}

					// den anderen Key f�r die ECUStatistics holen (etwas aufwendig..)
					key = ((String) entry.getKey()).charAt( 2 ) == '0' ? ((String) entry.getKey()).substring( 3, 4 ) : ((String) entry.getKey()).substring( 2, 4 );
	
					// der Fehlertext
					fehlertext = cscRE != null ? action + errorContextTranslated + ", " + errorDescription + cscRE.getMessage() + ", (PSdZ ID: " + cscRE.getMessageId() + ")" : PB.getString( mTransact );
	
					// SG Antwortverhalten nur eintragen, wenn SG nie geantwortet hat oder sporadisch
					if( mECUStatistics.get( key ).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.0" ) ) || ((String) mECUStatistics.get( key )).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.1" ) ) )
						antwortVerhalten = " (" + PB.getString( "psdz.SG" ) + " " + PB.getString( "psdz.antwortet" ) + " " + mECUStatistics.get( key ) + ")";
					else
						antwortVerhalten = "";
						
					// LOP 1436: Ausgabe um TAIS Teilenummer anreichern und zus�tzlich, wenn m�glich noch die TAIS Nummer des bestellbaren Teils mit ausgeben (Unterdr�cke die Bestellnummer, wenn diese gleich der Nummer des logTeils ist)
					String logTeil = "";
					String bestellTeilTAIS = "";
					StringBuffer strBufNummernTAIS = new StringBuffer();
					try {
						logTeil = psdz.getLogistischesTeil( key ).getSachNrTAIS();
						strBufNummernTAIS.append( ", " + PB.getString( "psdz.text.TaisNummer" ) + logTeil );
					} catch( Exception f ) {
					}
					try {
						bestellTeilTAIS = psdz.getLogistischesTeil( key ).getSachNrBestellbaresTeilTAIS();
						strBufNummernTAIS.append( logTeil.equals( bestellTeilTAIS ) ? "" : " (" + titleBestellTeilTAIS + bestellTeilTAIS + ")" );
					} catch( Exception f ) {
					}

					mResult = new Ergebnis( "PSdZSingleEcuProgramming", "Result", "", "", "", (String) entry.getKey() + strBufNummernTAIS.toString(), text.toString(), "", "", "0", "", "", "", fehlertext + antwortVerhalten, "", mSResult );
					mErgListe.add( mResult );
					}
				}
			} else {
				mResult = new Ergebnis( "PSdZSingleEcuProgramming", "Result", "", "", "", (String) entry.getKey(), text.toString(), "", "", "0", "", "", "", "", "", mSResult );
				mErgListe.add( mResult );
			}
		}
	}

	/**
	 * Eintrag des ersten allgemeinen Fehlers, der NIO-ECUs und der NotExecutable-ECUs ins APDM
	 * 
	 * @param gErgListe Ergebnisliste
	 * @param gMapNIO Map mit NIO-ECU's
	 * @param gMapNotExec Map mit NotExecutable-ECU's
	 * @param gPsdzErrors zugeh�rige einzutragende Fehler
	 * @param gECUStatistics Map mit SG Antwortverhalten
	 */
	private void entryECUsFT_NIO( Vector<Ergebnis> gErgListe, TreeMap<String, HashSet<String>> gMapNIO, TreeMap<String, HashSet<String>> gMapNotExec, HashMap<String, List<Exception>> gPsdzErrors, HashMap<String, String> gECUStatistics ) {
		Ergebnis gResult;
		String action = DE ? "Aktion: " : "Action: ";
		String errorDescription = DE ? "Fehlerbeschreibung: " : "Error description: ";
		String technicalAction = DE ? "Technische Aktion" : "Technical action";
		
		// Haben wir irgendeinen allgemeinen Fehler ohne SG Adresse?
		if( gPsdzErrors.containsKey( "-1" ) ) {
			CascadePSdZRuntimeException cscRE = null;
			
			// Liste der Fehler durchgehen
			for ( int e = 0; e < (gPsdzErrors.get( "-1" )).size(); e++ ) {
				Exception ex = (gPsdzErrors.get( "-1" )).get( e );
				
				if( ex instanceof CascadePSdZRuntimeException ) {
					cscRE = (CascadePSdZRuntimeException) ex;
				}
				
				// TODO: Lokalisierung des Fehler-Kontextes in der CRE (TechnicalTALExecutionAction); Kann ab fl�chiger Nutzung von CASCADE 7.0 wieder ausgebaut werden, da in CASCADE 7.0 Fehler-Kontexte vollst�ndig lokalisiert sind 
				String errorContextTranslated = cscRE.getStrErrorContextTranslated();
				if ( errorContextTranslated == "TechnicalTALExecutionAction" )  {
					errorContextTranslated = technicalAction;
				} else {
					// Fehler-Kontext ist bereits lokalisiert (CASCADE 7.0) bzw. keine der o.g. Lokalisierungen zutreffend
				}
				
				gResult = new Ergebnis( "PSdZSingleEcuProgramming", "Result", "", "", "", "SG ALL", "", "", "", "0", "", "", "", cscRE != null ? action + errorContextTranslated + ", " + errorDescription + cscRE.getMessage() + ", (PSdZ ID: " + cscRE.getMessageId() + ")" : ex.getMessage(), "", Ergebnis.FT_NIO );
				gErgListe.add( gResult );
			}
		}

		// NIO ECUs in Ergebnisliste (APDM) eintragen
		entryECUsDetails( gErgListe, gMapNIO, "psdz.tal.transactionNIO", Ergebnis.FT_NIO, gPsdzErrors, gECUStatistics );

		// NIO ECUs NotExecutable in Ergebnisliste (APDM) eintragen
		entryECUsDetails( gErgListe, gMapNotExec, "psdz.tal.transactionNOTExecutable", Ergebnis.FT_NIO, gPsdzErrors, gECUStatistics );
	}

	/**
	 * Aufbau einer HashMap aus den Exceptions der TAL (Aufbau: key = Steuerger�teadresse in hex + _ + TACategorie
	 * z.B. '0x00_cdceploy', wert = 'Exception'
	 * 
	 * @param mTALResult zu analysierende CascadeTAL
	 * @return HashMap aus input-CascadeTAL erzeugte HashMap
	 */
	private HashMap<String, List<Exception>> buildExcHashMap( CascadeTAL mTALResult ) {
		HashMap<String, List<Exception>> ret_val = new HashMap<String, List<Exception>>();

		// Exception der TAL ?
		if( mTALResult.getExceptions().size() > 0 ) 
			// TODO: Nutzung von getStatusRelevantExceptions ab CASCADE 7.0.0
			ret_val.put( "-1", mTALResult.getExceptions() );

		CascadeTALLine[] myLines = mTALResult.getTalLines();
		for( int i = 0; i < myLines.length; i++ ) {
			String sgAdress = "0" + Integer.toHexString( myLines[i].getDiagnosticAddress().intValue() ).toUpperCase();
			String sgID = "0x" + sgAdress.substring( sgAdress.length() - 2, sgAdress.length() );
			String sgCat = "_" + myLines[i].getTALTACategories()[0].getCategory().toString().toLowerCase().trim();
			boolean foundException = false;
			
			// Exception in der TALLine?
			if( myLines[i].getExceptions().size() > 0 ) {
				// TODO: Nutzung von getStatusRelevantExceptions ab CASCADE 7.0.0
				ret_val.put( sgID + sgCat, myLines[i].getExceptions() );
				foundException = true;
			}

			if( !foundException ) {
				for( int j = 0; j < myLines[i].getTALTACategories().length; j++ ) {
					CascadeTALTACategory myTALTACategory = myLines[i].getTALTACategories()[j];
					sgCat = "_" + myTALTACategory.getCategory().toString().toLowerCase().trim();
					// Pr�fung der TAs
					for( int k = 0; k < myTALTACategory.getTAs().length; k++ ) {
						CascadeTA myTA = myTALTACategory.getTAs()[k];
						// Exception in der Transaktion?
						if( myTA.getExceptions() != null ) {
							// TODO: Nutzung von getStatusRelevantExceptions ab CASCADE 7.0.0
							ret_val.put( sgID + sgCat, myTA.getExceptions() );
						}
					}
					// da TAL-Filterung ausschlie�lich auf BL_FLASH und SW_DEPLOY erfolgt, muss keine Pr�fung der FscTAs erfolgen
				}
			}
		}
		return ret_val;
	}

	/**
	 * Extrahiert aus mProgressMap den %-Wert des Schl�ssels "msgAdress", ansonsten null
	 * 
	 * @param mProgressMap HashMap mit Fortschrittsangaben der ECUs
	 * @return String mit %-Wert des ECUs mit Schl�ssel "msgAdress" oder 0 oder null
	 */
	private String getProgressECU( HashMap<?, ?> mProgressMap, String msgAdress ) {
		String result = null;
		if( mProgressMap == null )
			return result;
		result = (String) mProgressMap.get( msgAdress );
		if( result == null )
			return null;
		int p = result.indexOf( "%" ); // 0% bis 100%
		if( p < 1 )
			return "0";
		if( p > 3 )
			return "0";
		result = result.substring( 0, p );
		return result;
	}

	/**
	 * Bereitet die Eintr�ge der TreeMap zur Anzeige im UserDialog vor
	 * Die TreeMap beinhaltet unter Schl�ssel z.B. "0x00 ECU:JBBF" eine weitere HashMap mit den Eintr�gen:
	 * Schl�ssel "0" : String z.B. "0x00 ECU:JBBF : xxx %" (xxx Gesamtfortschritt des ECU)
	 * Schl�ssel "blupdate" bzw. "swdeploy" : String z.B. "blupdate = xxx" (xxx kann folgende Werte annehmen: 
	 * ??? noch offen; transactionIO, transactionNIO oder transactionNOTExecutable)
	 * 
	 * @param hMap HashMap
	 * @return String zur Anzeige im UserDialog
	 */
	private String getAnzeige( TreeMap<String, HashMap<String, String>> mMap ) {
		String result = "";
		if( mMap.size() > 0 ) {
			for( Iterator<String> i = mMap.keySet().iterator(); i.hasNext(); ) {
				HashMap<String, String> iMap = new HashMap<String, String>();
				iMap = mMap.get( i.next() );
				Object[] resIMap = iMap.values().toArray();
				for( int j = 0; j < resIMap.length; j++ ) {
					String res = (String) resIMap[j];
					int k = res.indexOf( "=" );
					if( k != -1 ) {
						String t1 = PB.getString( "psdz.tal." + res.substring( 0, k ).toLowerCase().trim() ) + " = ";
						String t2 = res.substring( k + 1, res.length() ).trim();
						if( t2.equals( "???" ) ) {
							res = t1 + t2 + ";";
						} else {
							res = t1 + ";" + PB.getString( "psdz.tal." + t2 ) + ";";
						}
					}
					result = result + res;
				}
			}
		}
		return result;
	}

	/**
	 * Aufbau von drei TreeMaps aus den Ergebnissen der TAL (Aufbau: key = Steuerger�teadresse in hex + ' ' + 'SG:' +
	 * Basisvariantenname, wert = TALCategorie
	 * z.B. '0x00 SG:JBBF', wert = 'cddeploy'
	 * 
	 * @param mTALResult zu analysierende CascadeTAL
	 * @param mEcuIO aus input-CascadeTAL erzeugte TreeMap mit CascadeExcecutionStatus = PROCESSED
	 * @param mEcuNIO aus input-CascadeTAL erzeugte TreeMap mit CascadeExcecutionStatus = PROCESSEDWITHERROR
	 * @param mEcuNotExecutable aus input-CascadeTAL erzeugte TreeMap mit CascadeExcecutionStatus = NOTEXECUTABLE
	 */
	private void buildMaps( CascadeTAL mTALResult, TreeMap<String, HashSet<String>> mEcuIO, TreeMap<String, HashSet<String>> mEcuNIO, TreeMap<String, HashSet<String>> mEcuNotExecutable ) {
		// Leeren der Maps
		mEcuIO.clear();
		mEcuNIO.clear();
		mEcuNotExecutable.clear();
		
		// Ergebnisanalyse
		for( int i = 0; i < mTALResult.getTalLines().length; i++ ) {
			// BasisName des SG ermitteln (0x'hexcode' + " ECU:'Basisname')
			String sgAdress = "0" + Integer.toHexString( mTALResult.getTalLines()[i].getDiagnosticAddress().intValue() ).toUpperCase();
			String sgIDName = "0x" + sgAdress.substring( sgAdress.length() - 2, sgAdress.length() ) + " " + PB.getString( "psdz.SG" ) + ":" + mTALResult.getTalLines()[i].getBaseVariantName();

			// Status der einzelnen TAs und SWTs holen und merken in IO und NIO Listen
			for( int j = 0; j < mTALResult.getTalLines()[i].getTALTACategories().length; j++ ) {
				// IO TAL-Lines analysieren und TalLine-Kategorien merken
				// Es werden der TAL-Line Status analysiert und nicht der Status der TALTACategories, damit Fehler, die nach der Transaktion auftreten (-> FINALIZATION) auch erfasst werden 
				if( mTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.PROCESSED ) || mTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.INACTIVE ) ) {
					HashSet<String> setTalTaCategoriesIO = new HashSet<String>();
					// pr�fe ob schon eingetragen
					if( mEcuIO.containsKey( sgIDName ) )
						setTalTaCategoriesIO = mEcuIO.get( sgIDName );

					setTalTaCategoriesIO.add( mTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString() );
					mEcuIO.put( sgIDName, setTalTaCategoriesIO );
				}

				// NIO TAL-Lines analysieren und TalLine-Kategorien merken (z.B. blFlash, swDeploy)
				if( mTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.PROCESSEDWITHERROR ) ) {
					HashSet<String> setTalTaCategoriesNIO = new HashSet<String>();
					// pr�fe ob schon eingetragen
					if( mEcuNIO.containsKey( sgIDName ) )
						setTalTaCategoriesNIO = mEcuNIO.get( sgIDName );

					setTalTaCategoriesNIO.add( mTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString() );
					mEcuNIO.put( sgIDName, setTalTaCategoriesNIO );
				}

				// NotExcecutable (auch NIO !)
				if( mTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.NOTEXECUTABLE ) ) {
					HashSet<String> setTalTaCategoriesNotExecutable = new HashSet<String>();
					// pr�fe ob schon eingetragen
					if( mEcuNotExecutable.containsKey( sgIDName ) )
						setTalTaCategoriesNotExecutable = mEcuNotExecutable.get( sgIDName );

					setTalTaCategoriesNotExecutable.add( mTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString() );
					mEcuNotExecutable.put( sgIDName, setTalTaCategoriesNotExecutable );
				}
			}
		}
	}
	
	/** Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; // default english
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// vehicleState Listen, Steuerung von phys. Nachadressierung �bers PSdZ [0 - includeListe, 1 - excludeListe]
		String[][] vehicleState_include_exclude_List = { {}, {} };

		// incl. / excl. Listen, f�r Steuerung von Einschlu� / Ausschlu� von SG f�r die TAL Filterung
		// [0 - includeListe, 1 - excludeListe]
		String[][] include_exclude_List = { {}, {} };

		// Mittels eines '*' beim ersten Element in der vehicleStateIncludeList oder vehicleStateExcludeList
		// werden in der jeweils anderen Liste alle ECUs eingetragen ohne die ECUs in der List wo kein Platzhalter steht und ohne die ECUs
		// aus der IGNORE_LIST.
		char cPlatzhalter_vehicleState_inc_ex_Lists = 'n'; // Verwaltung des '*' Platzhalters: 'n'-nichts oder ohne Platzhalter, 'i'-include Liste hat Platzhalter, 'e'-exclude Liste hat Platzhalter
		char cPlatzhalter_inc_ex_Lists = 'n'; // Verwaltung des '*' Platzhalters: 'n'-nichts oder ohne Platzhalter, 'i'-include Liste hat Platzhalter, 'e'-exclude Liste hat Platzhalter

		boolean bForceExecution = true; // programmieren erzwingen, wir wollen alle SG angezeigt bekommen - deshalb hier per default auf true


		final int I_TAL_POLLTIME = 1000; // alle I_TAL_POLLTIME in ms TAL Status �berpr�fen

		UserDialog userDialog = null;

		boolean bBTL_Flash = false; // Voreinstellung: BootloaderFlash per default NICHT erlauben
		boolean bParallelExecution = true; // Voreinstellung: parallele Ausf�hrung erlauben
		// TODO: IgnoreErrors-Flag kann ab fl�chiger Nutzung von CASCADE 6.1 aus Pr�fwissen ausgebaut werden, nach fl�chigem Ausbau aus PW kann Parameter aus PP entfernt werden		
		boolean bIgnoreErrors = false; // Voreinstellung: Fehler bei der TAL Ausf�hrung werden nicht ignoriert
		int iMaxErrorRepeat = 1; // Voreinstellung: maximal 1 Wdhlg im Fehlerfall
		// Verwaltung der verarbeiteten SG [IO, NIO, OFFEN]
		Set<String> setEcuIO = new HashSet<String>(), setEcuNIO = new HashSet<String>(), setEcuOpen = new HashSet<String>();

		// Verwaltung der verarbeiteten SG [IO, NIO, NOTEXECUTABLE] in sortierten Listen mit Variable=ECU und Wert=TA, SWT Kategories als HashSet; Bsp.: Variable=0x29 ECU:DSC, Wert=swDeploy als HashSet
		TreeMap<String, HashSet<String>> mapEcuIO = new TreeMap<String, HashSet<String>>(), mapEcuNIO = new TreeMap<String, HashSet<String>>(), mapEcuNotExecutable = new TreeMap<String, HashSet<String>>();

		String testScreenID = this.getPr�flingLaufzeitUmgebung().getName(); // TestScreenID (aus Pr�fstandsinstanz)
		
		// Modusumschaltung
		boolean bModusSwitch = false; // per default keine Modusumschaltung
		String[] modeSwitchGatewayList = (String[]) Collections.emptyList().toArray( new String[Collections.emptyList().size()] ); //Liste der umzuschaltenden Gateways [default aus Template-Bedatung typischerweise leer, umzuschaltenden Gateways k�nnen hier�ber eingegrenzt werden]
		
		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
		boolean bDebugPerform = true; // gibt an, ob eine Performencemessung erfolgen sollen

		// Die maximale Anzahl der dynamischen Retries: Nach der Ausf�hrung werden die Fehlerhafte actions 
		// in eine neue TAL integriert und diese TAL nochmals ausgef�hrt
		int iDynamicRetries = 0;
		int iRuns = 0; //Anzahl der bereits durchgef�hrten l�ufe
		boolean bDoRepeat = false;
		
		try {

			// Argumente einlesen und checken
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
					else if( !getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) && !getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DEBUG_PERFORM
				if( getArg( "DEBUG_PERFORM" ) != null ) {
					if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "TRUE" ) )
						bDebugPerform = true;
					else if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "FALSE" ) )
						bDebugPerform = false;
					else if( !getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "FALSE" ) && !getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "TRUE" ) )
						throw new PPExecutionException( "DEBUG_PERFORM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// VEHICLE_STATE_INCLUDE_LIST
				if( getArg( "VEHICLE_STATE_INCLUDE_LIST" ) != null ) {
					vehicleState_include_exclude_List[0] = checkAndModifyEcuHexArray( extractValues( getArg( "VEHICLE_STATE_INCLUDE_LIST" ) ) );
				}

				// VEHICLE_STATE_EXCLUDE_LIST
				if( getArg( "VEHICLE_STATE_EXCLUDE_LIST" ) != null ) {
					vehicleState_include_exclude_List[1] = checkAndModifyEcuHexArray( extractValues( getArg( "VEHICLE_STATE_EXCLUDE_LIST" ) ) );

					// Darf nur zusammen mit der IncludeList definiert werden, wenn ein '*' Platzhalter in einer Liste ist
					if( getArg( "VEHICLE_STATE_INCLUDE_LIST" ) != null ) {
						// Kleiner Fehlercheck?
						if( (vehicleState_include_exclude_List[0].length > 1 && vehicleState_include_exclude_List[1].length > 1) || (vehicleState_include_exclude_List[0][0].equalsIgnoreCase( "*" ) && vehicleState_include_exclude_List[1][0].equalsIgnoreCase( "*" )) )
							throw new PPExecutionException( PB.getString( "psdz.parameter.ParameterfehlerVehicleStateList" ) );

						// includeList mit '*' ?
						if( vehicleState_include_exclude_List[0][0].equalsIgnoreCase( "*" ) )
							cPlatzhalter_vehicleState_inc_ex_Lists = 'i';

						// excludeList mit '*' ?
						if( vehicleState_include_exclude_List[1][0].equalsIgnoreCase( "*" ) )
							cPlatzhalter_vehicleState_inc_ex_Lists = 'e';
					}
				}

				// INCLUDE_LIST
				if( getArg( "INCLUDE_LIST" ) != null ) {
					include_exclude_List[0] = checkAndModifyEcuHexArray( extractValues( getArg( "INCLUDE_LIST" ) ) );
				}

				// EXCLUDE_LIST
				if( getArg( "EXCLUDE_LIST" ) != null ) {
					include_exclude_List[1] = checkAndModifyEcuHexArray( extractValues( getArg( "EXCLUDE_LIST" ) ) );

					// Darf nur zusammen mit der IncludeList definiert werden, wenn ein '*' Platzhalter in einer Liste ist
					if( getArg( "INCLUDE_LIST" ) != null ) {
						// Kleiner Fehlercheck?
						if( (include_exclude_List[0].length > 1 && include_exclude_List[1].length > 1) || (include_exclude_List[0][0].equalsIgnoreCase( "*" ) && include_exclude_List[1][0].equalsIgnoreCase( "*" )) )
							throw new PPExecutionException( PB.getString( "psdz.parameter.ParameterfehlerIncludeExcludeList" ) );

						// includeList mit '*' ?
						if( include_exclude_List[0][0].equalsIgnoreCase( "*" ) )
							cPlatzhalter_inc_ex_Lists = 'i';

						// excludeList mit '*' ?
						if( include_exclude_List[1][0].equalsIgnoreCase( "*" ) )
							cPlatzhalter_inc_ex_Lists = 'e';
					}
				}

				// BTL_FLASH
				if( getArg( "BTL_FLASH" ) != null ) {
					if( getArg( "BTL_FLASH" ).equalsIgnoreCase( "TRUE" ) )
						bBTL_Flash = true;
					else if( getArg( "BTL_FLASH" ).equalsIgnoreCase( "FALSE" ) )
						bBTL_Flash = false;
					else if( !getArg( "BTL_FLASH" ).equalsIgnoreCase( "FALSE" ) && !getArg( "BTL_FLASH" ).equalsIgnoreCase( "TRUE" ) )
						throw new PPExecutionException( "BTL_FLASH " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// FORCE_EXECUTION
				if( getArg( "FORCE_EXECUTION" ) != null ) {
					if( getArg( "FORCE_EXECUTION" ).equalsIgnoreCase( "FALSE" ) )
						bForceExecution = false;
					else if( getArg( "FORCE_EXECUTION" ).equalsIgnoreCase( "TRUE" ) )
						bForceExecution = true;
					else if( !getArg( "FORCE_EXECUTION" ).equalsIgnoreCase( "TRUE" ) && !getArg( "FORCE_EXECUTION" ).equalsIgnoreCase( "FALSE" ) )
						throw new PPExecutionException( "FORCE_EXECUTION " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// PARALLEL_EXECUTION
				if( getArg( "PARALLEL_EXECUTION" ) != null ) {
					if( getArg( "PARALLEL_EXECUTION" ).equalsIgnoreCase( "FALSE" ) )
						bParallelExecution = false;
					else if( getArg( "PARALLEL_EXECUTION" ).equalsIgnoreCase( "TRUE" ) )
						bParallelExecution = true;
					else if( !getArg( "PARALLEL_EXECUTION" ).equalsIgnoreCase( "TRUE" ) && !getArg( "PARALLEL_EXECUTION" ).equalsIgnoreCase( "FALSE" ) )
						throw new PPExecutionException( "PARALLEL_EXECUTION " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// MAX_ERROR_REPEAT
				if( getArg( "MAX_ERROR_REPEAT" ) != null ) {
					try {
						iMaxErrorRepeat = Integer.parseInt( extractValues( getArg( "MAX_ERROR_REPEAT" ) )[0] );
						if( iMaxErrorRepeat > 10 || iMaxErrorRepeat < 1 )
							throw new NumberFormatException( "Fehler: Wert muss 1 <= x <= 10 sein!" );
					} catch( NumberFormatException nfe ) {
						throw new PPExecutionException( PB.getString( "psdz.parameter.ParameterfehlerMaxErrorRepeat" ) );
					}
				}

				// IGNORE_ERRORS
				if( getArg( "IGNORE_ERRORS" ) != null ) {
					if( getArg( "IGNORE_ERRORS" ).equalsIgnoreCase( "TRUE" ) )
						bIgnoreErrors = true;
					else if( getArg( "IGNORE_ERRORS" ).equalsIgnoreCase( "FALSE" ) )
						bIgnoreErrors = false;
					else if( !getArg( "IGNORE_ERRORS" ).equalsIgnoreCase( "FALSE" ) && !getArg( "IGNORE_ERRORS" ).equalsIgnoreCase( "TRUE" ) )
						throw new PPExecutionException( "IGNORE_ERRORS " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// MODE_SWITCH_GATEWAY_LIST
				if( getArg( "MODE_SWITCH_GATEWAY_LIST" ) != null ) {
					modeSwitchGatewayList = extractValues( getArg( "MODE_SWITCH_GATEWAY_LIST" ) );
				}
				
				// MODE_SWITCH
				if( getArg( "MODE_SWITCH" ) != null ) {
					if( getArg( "MODE_SWITCH" ).equalsIgnoreCase( "TRUE" ) )
						bModusSwitch = true;
					else if( getArg( "MODE_SWITCH" ).equalsIgnoreCase( "FALSE" ) )
						bModusSwitch = false;
					else if( !getArg( "MODE_SWITCH" ).equalsIgnoreCase( "FALSE" ) && !getArg( "MODE_SWITCH" ).equalsIgnoreCase( "TRUE" ) )
						throw new PPExecutionException( "MODE_SWITCH " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DYNAMIC_RETRIES
				if( getArg( "DYNAMIC_RETRIES" ) != null ) {
					try {
						iDynamicRetries = new Integer( extractValues( getArg( "DYNAMIC_RETRIES" ) )[0] ).intValue();
					} catch( NumberFormatException e ) {
						throw new PPExecutionException( "DYNAMIC_RETRIES " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
					}
				}
			
			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			
			// setze tempor�re Variablen
			try {
				// setze tempor�re Variablen f�r die Modusumschaltung
				this.getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_MODE_SWITCH", new Boolean( bModusSwitch ) );
				this.getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_MODE_SWITCH_GATEWAY_LIST", new ArrayList<String>( Arrays.asList( modeSwitchGatewayList ) ) );
			} catch( Exception e ) {
				// Nothing
			}
			
			// Performancemessung Start
			if( bDebugPerform )
				System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming PP START" );

			// Devices holen
			try {
				psdz = getPr�flingLaufzeitUmgebung().getDeviceManager().getPSdZ();
				if( bDebug )
					CascadeLogging.getLogger().log( LogLevel.INFO, "PSdZSingleEcuProgramming: Get PSdZ Device finished." );
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			
			// Informiere den Benutzer
			userDialog = getUserDialog(ergListe); //wg. Problem mit zu gro�en Dialogen bei Multi-Instanz
			userDialog.setDisplayProgress( true );
			userDialog.getProgressbar().setIndeterminate( true );
			userDialog.displayMessage( PB.getString( "psdz.ud.SGEinzelprogrammierung" ), ";" + PB.getString( "psdz.ud.Vorbereitunglaeuft" ), -1 );
			releaseUserDialog(ergListe); //wg. Problem mit zu gro�en Dialogen bei Multi-Instanz
			if( bDebug )
				CascadeLogging.getLogger().log( LogLevel.INFO, "PSdZSingleEcuProgramming: Get UserDialog Device finished." );

			try {
				// Berechnung der neuen maskierten Listen, bei Verwendung eines Platzhalters '*' bei den vehicleState- und den normalen include exclude Listen
				vehicleState_include_exclude_List = calculatePlaceholderForLists( vehicleState_include_exclude_List, cPlatzhalter_vehicleState_inc_ex_Lists, "VEHICLE_STATE_INCLUDE_EXCLUDE_LIST", bDebug );
				include_exclude_List = calculatePlaceholderForLists( include_exclude_List, cPlatzhalter_inc_ex_Lists, "INCLUDE_EXCLUDE_LIST", bDebug );

				// *************************************************************************************
				// * 1. VORBEREITUNG: Wir berechnen eine TAL f�r Programmieren auf Basis der Parameter *
				// *************************************************************************************

				// TAL-Filter bef�llen, es m�ssen hier alle Kategorien in den Filter aufgenommen werden
				// Kategorien, die nicht aufgef�hrt sind, sollen verboten werden.
				CascadeTALFilter myTALFilter = new CascadeTALFilter();
				Object[] allCategories = CascadeTACategories.getAllCategories().toArray();

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.getSVTSoll' START" );
				}

				// Wir holen erst einmal die ECU Soll-Liste als String Array von Hex Werten
				String[] sollECUs = new String[psdz.getSVTSoll().getEcuList().size()];
				for( int j = 0; j < sollECUs.length; j++ )
					sollECUs[j] = Integer.toHexString( ((CascadeECU) psdz.getSVTSoll().getEcuList().get( j )).getDiagnosticAddress().intValue() );

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.getSVTSoll' ENDE" );
				}

				if( bDebug ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", CASCADE PSdZSingleEcuProgramming: Include List:" + Arrays.asList( include_exclude_List[0] ).toString() + ", Exclude List:" + Arrays.asList( include_exclude_List[1] ).toString() );
				}
				
				// gehe alle Kategorien durch ...
				for( int i = 0; i < allCategories.length; i++ ) {
					if( bDebug )
						System.out.println( "PSdZ ID:" + testScreenID + ", PSdZSingleEcuProgramming: Categories Loop Counter: " + i + ", Category: " + ((CascadeTACategories) allCategories[i]).toString() );
					CascadeFilteredTACategory myCategory = new CascadeFilteredTACategory( (CascadeTACategories) allCategories[i] );
					// Treffer in der Liste der Actions?
					boolean filteredActionFound = false;

					// Ist die Kategorie blFlash - BootloaderFlash [bei Bedarf] oder swDeploy - programmieren?
					if( ( allCategories[i].toString().equalsIgnoreCase( CascadeTACategories.BL_FLASH.toString() ) && bBTL_Flash ? true : false ) || allCategories[i].toString().equalsIgnoreCase( CascadeTACategories.SW_DEPLOY.toString() ) ) {
						filteredActionFound = true;
						myCategory.setAffectAllEcus( false );

						// kein Platzhalter keine include- und keine exclude Liste
						if( cPlatzhalter_inc_ex_Lists == 'n' && include_exclude_List[0].length == 0 && include_exclude_List[1].length == 0 ) {
							// die aus der Sollverbauung anzeigen
							myCategory.addEcusToList( sollECUs );
							myCategory.setBlacklist( false );
							if( bDebug ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", CASCADE SingleEcuProgramming: NO STAR, NO INC, NO EX, Filter List:" + Arrays.asList( myCategory.getEcuList() ).toString() );
							}
						}

						// kein Platzhalter und include- aber keine exclude Liste
						if( cPlatzhalter_inc_ex_Lists == 'n' && include_exclude_List[0].length > 0 && include_exclude_List[1].length == 0 ) {
							// die aus der IncludeListe anzeigen, aber nur wenn sie auch in der Sollverbauung sind
							List<String> includeAsList = Arrays.asList( include_exclude_List[0] );
							for( int j = 0; j < sollECUs.length; j++ ) {
								if( !includeAsList.contains( sollECUs[j] ) )
									myCategory.addEcuToList( sollECUs[j] );
							}
							myCategory.setBlacklist( true );
							if( bDebug ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", CASCADE SingleEcuProgramming: NO STAR, WITH INC:" + Arrays.asList( include_exclude_List[0] ).toString() + ", NO EX, Filter List:" + Arrays.asList( myCategory.getEcuList() ).toString() );
							}
						}

						// kein Platzhalter keine include- und aber exclude Liste
						if( cPlatzhalter_inc_ex_Lists == 'n' && include_exclude_List[0].length == 0 && include_exclude_List[1].length > 0 ) {
							// die aus der Sollverbauung anzeigen minus die aus der exclude Liste
							List<String> excludeAsList = Arrays.asList( include_exclude_List[1] );
							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.getSVTSoll' START" );
							}
							for( int j = 0; j < psdz.getSVTSoll().getEcuList().size(); j++ ) {
								if( excludeAsList.contains( Integer.toHexString( ((CascadeECU) psdz.getSVTSoll().getEcuList().get( j )).getDiagnosticAddress().intValue() ) ) )
									myCategory.addEcuToList( Integer.toHexString( ((CascadeECU) psdz.getSVTSoll().getEcuList().get( j )).getDiagnosticAddress().intValue() ) );
							}
							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.getSVTSoll' ENDE" );
							}

							myCategory.setBlacklist( true );
							if( bDebug ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", CASCADE SingleEcuProgramming: NO STAR, NO INC, WITH EX:" + Arrays.asList( include_exclude_List[1] ).toString() + ", Filter List:" + Arrays.asList( myCategory.getEcuList() ).toString() );
							}
						}

						// includeListe hat Eintr�ge bei Platzhalterverwendung
						if( cPlatzhalter_inc_ex_Lists == 'i' && include_exclude_List[0].length > 0 && include_exclude_List[1].length == 0 ) {
							// die aus der include (bei Platzhalter) verwenden, aber nur wenn sie auch in der Sollverbauung sind
							List<String> includeAsList = Arrays.asList( include_exclude_List[0] );
							for( int j = 0; j < sollECUs.length; j++ ) {
								if( !includeAsList.contains( sollECUs[j] ) )
									myCategory.addEcuToList( sollECUs[j] );
							}
							myCategory.setBlacklist( true );
							if( bDebug ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", CASCADE SingleEcuProgramming: WITH STAR, WITH INC, NO EX, Filter List:" + Arrays.asList( myCategory.getEcuList() ).toString() );
							}
						}

						// excludeListe hat Eintr�ge bei Platzhalterverwendung
						if( cPlatzhalter_inc_ex_Lists == 'e' && include_exclude_List[0].length == 0 && include_exclude_List[1].length > 0 ) {
							// die aus der exclude (bei Platzhalter) aber nur wenn sie auch in der Sollverbauung sind
							List<String> excludeAsList = Arrays.asList( include_exclude_List[1] );
							for( int j = 0; j < psdz.getSVTSoll().getEcuList().size(); j++ ) {
								if( excludeAsList.contains( Integer.toHexString( ((CascadeECU) psdz.getSVTSoll().getEcuList().get( j )).getDiagnosticAddress().intValue() ) ) )
									myCategory.addEcuToList( Integer.toHexString( ((CascadeECU) psdz.getSVTSoll().getEcuList().get( j )).getDiagnosticAddress().intValue() ) );
							}

							myCategory.setBlacklist( true );
							if( bDebug ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", CASCADE SingleEcuProgramming: WITH STAR, NO INC, WITH EX, Filter List:" + Arrays.asList( myCategory.getEcuList() ).toString() );
							}
						}

						// ForceExecution?
						myCategory.setForceExecution( bForceExecution );
					}

					if( !filteredActionFound ) {
						myCategory.setAffectAllEcus( true );
						myCategory.setBlacklist( true );
					}

					myTALFilter.addFilteredTACategory( myCategory );
				}

				// jetzt den Filter setzen
				if( vehicleState_include_exclude_List[0].length > 0 ) {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.generateAndStoreTAL' START" );
					}
					// Fall IncludeList
					// da TAL-Filterung ausschlie�lich auf BL_FLASH und SW_DEPLOY erfolgt, muss kein SWT-Status ausgelesen werden
					psdz.generateAndStoreTAL( vehicleState_include_exclude_List[0], false, myTALFilter, false );
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.generateAndStoreTAL' ENDE" );
					}

				} else if( vehicleState_include_exclude_List[1].length > 0 ) {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.generateAndStoreTAL' START" );
					}
					// Fall ExcludeList
					// da TAL-Filterung ausschlie�lich auf BL_FLASH und SW_DEPLOY erfolgt, muss kein SWT-Status ausgelesen werden
					psdz.generateAndStoreTAL( vehicleState_include_exclude_List[1], true, myTALFilter, false );
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.generateAndStoreTAL' ENDE" );
					}

				} else {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.generateAndStoreTAL' START" );
					}
					// �bergebe leere Liste
					// da TAL-Filterung ausschlie�lich auf BL_FLASH und SW_DEPLOY erfolgt, muss kein SWT-Status ausgelesen werden
					String[] emptyList = {};
					psdz.generateAndStoreTAL( emptyList, true, myTALFilter, false );
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.generateAndStoreTAL' ENDE" );
					}

				}

				// ****************************************************************************************
				// * 2. AUSWAHL: Der Bediener kann jetzt die Steuerger�te f�r das Programmieren ausw�hlen *
				// ****************************************************************************************

				Hashtable<String, String> htECUs = new Hashtable<String, String>();
				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.getTAL' START" );
				}

				// merken der nicht zu programmierenden ECUs f�r die TAL Filterung
				Hashtable<String, String> htECUsNotProgrammable = new Hashtable<String, String>();

				for( int i = 0; i < psdz.getTAL().getTalLines().length; i++ ) {
					StringBuffer sbNumber = new StringBuffer( "00" );
					StringBuffer sbBaseNameFormat = new StringBuffer( "        " );
					sbNumber.append( Integer.toHexString( psdz.getTAL().getTalLines()[i].getDiagnosticAddress().intValue() ).toUpperCase() );
					htECUsNotProgrammable.put( sbNumber.substring( sbNumber.length() - 2 ), "" );
					sbBaseNameFormat.append( psdz.getTAL().getTalLines()[i].getBaseVariantName() );
					// System.out.println( "TTT" + sbNumber.toString() );
					// System.out.println( "TB" + sbNumber.substring( sbNumber.length() - 2 ) );
					htECUs.put( "0x" + sbNumber.substring( sbNumber.length() - 2 ) + PB.getString( "psdz.text.SG" ) + psdz.getTAL().getTalLines()[i].getBaseVariantName(), sbNumber.substring( sbNumber.length() - 2 ) );
				}

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.getTAL' ENDE" );
				}

				// Eingabe abfragen
				userDialog = getUserDialog(ergListe); //wg. Problem mit zu gro�en Dialogen bei Multi-Instanz
				String[] strECUList = (String[]) htECUs.keySet().toArray( new String[htECUs.keySet().size()] );
				Arrays.sort( strECUList );
				int[] index = userDialog.requestUserInputMultipleChoice( PB.getString( "psdz.ud.SGEinzelprogrammierung" ), PB.getString( "psdz.ud.SGEinzelprogrammierungText" ), 0, strECUList );
				releaseUserDialog(ergListe); //wg. Problem mit zu gro�en Dialogen bei Multi-Instanz
				
				if( index != null && index.length > 0 ) {
					// rechne neue Include-Liste
					String[] strECUsProgrammingSelection = null;
					StringBuffer sbECUSelectionAnzeige = new StringBuffer( "" );
					strECUsProgrammingSelection = new String[index.length];
					try {
						for( int i = 0; i < index.length; i++ ) {
							sbECUSelectionAnzeige.append( strECUList[index[i]] + ";" );
							strECUsProgrammingSelection[i] = (String) htECUs.get( strECUList[index[i]] );
							htECUsNotProgrammable.remove( strECUsProgrammingSelection[i] );
						}
					} catch( ArrayIndexOutOfBoundsException ex ) {
						// Tue nichts Fall Leerzeichen
					}

					// nicht zu programmierenden SGs
					String[] strECUsNotProgrammable = (String[]) htECUsNotProgrammable.keySet().toArray( new String[htECUsNotProgrammable.keySet().size()] );

					// User informieren
					userDialog = getUserDialog(ergListe); //wg. Problem mit zu gro�en Dialogen bei Multi-Instanz
					userDialog.setLineWrap( false );
					userDialog.setDisplayProgress( true );
					userDialog.getProgressbar().setIndeterminate( true );
					userDialog.displayMessage( PB.getString( "psdz.ud.SGEinzelprogrammierung" ), ";" + PB.getString( "psdz.ud.SGwerdenvorbereitet" ) + ";" + sbECUSelectionAnzeige.toString(), -1 );
					releaseUserDialog(ergListe); //wg. Problem mit zu gro�en Dialogen bei Multi-Instanz

					// filtere die TAL mit dem neuen Filter, wobei BTL-Flash nur bei Bedarf erfolgt
					if( bBTL_Flash ) {
						myTALFilter.getFilteredTACategory( CascadeTACategories.BL_FLASH ).deleteAllEcuFromList();
						myTALFilter.getFilteredTACategory( CascadeTACategories.BL_FLASH ).setAffectAllEcus( false );
						myTALFilter.getFilteredTACategory( CascadeTACategories.BL_FLASH ).setBlacklist( true );
						myTALFilter.getFilteredTACategory( CascadeTACategories.BL_FLASH ).setForceExecution( false );
						myTALFilter.getFilteredTACategory( CascadeTACategories.BL_FLASH ).addEcusToList( strECUsNotProgrammable );
					}
					myTALFilter.getFilteredTACategory( CascadeTACategories.SW_DEPLOY ).deleteAllEcuFromList();
					myTALFilter.getFilteredTACategory( CascadeTACategories.SW_DEPLOY ).setAffectAllEcus( false );
					myTALFilter.getFilteredTACategory( CascadeTACategories.SW_DEPLOY ).setBlacklist( true );
					myTALFilter.getFilteredTACategory( CascadeTACategories.SW_DEPLOY ).setForceExecution( false );
					myTALFilter.getFilteredTACategory( CascadeTACategories.SW_DEPLOY ).addEcusToList( strECUsNotProgrammable );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.filterTAL' START" );
					}
					psdz.filterTAL( null, myTALFilter );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.filterTAL' ENDE" );
					}

					// ****************************************************************************************
					// * 3. TAL-Ausf�hrung: Auf Basis der Auswahl werden jetzt die Steuerger�te programmiert. *
					// ****************************************************************************************
					
					setEcuIO.clear();
					setEcuNIO.clear();
					setEcuOpen.clear();
					
					//Schleife f�r automatische Wiederholungen im Fehlerfall
					while( iRuns == 0 || bDoRepeat ) {
						iRuns++;
						bDoRepeat = false;
						
						try {
						
							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.startTALExecution' START" );
							}
							// asynchrone Ausf�hrung einer TAL starten
							psdz.startTALExecution( bParallelExecution, iMaxErrorRepeat, true );
	
							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.startTALExecution' ENDE" );
							}
	
							// Vorbereitung der Statusanzeige, w�hrend der Ausf�hrung
							userDialog = getUserDialog(ergListe); //wg. Problem mit zu gro�en Dialogen bei Multi-Instanz
							userDialog.setAllowCancel( true );
							userDialog.setLineWrap( false );
							userDialog.setDisplayProgress( true );
							userDialog.getProgressbar().setIndeterminate( false );
	
							TreeMap<String, HashMap<String, String>> mapAnzeigeECUs = new TreeMap<String, HashMap<String, String>>();
	
							// �berpr�fe pollend die Ausf�hrung der TAL
							while( true ) {
								// Performancemessung
								if( bDebugPerform ) {
									System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.getTALExecutionStatus' START" );
								}
								// lese den Status + hier wird f�r I_TAL_POLLTIME in ms gewartet
								CascadeTAL myTALResult = psdz.getTALExecutionStatus( I_TAL_POLLTIME, null );
	
								// Performancemessung
								if( bDebugPerform ) {
									System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.getTALExecutionStatus' ENDE" );
								}
	
								// Performancemessung
								if( bDebugPerform ) {
									System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.getProgressMap' START" );
								}
								// auslesen der Fortschrittstabelle
								HashMap<?, ?> myProgressMap = psdz.getProgressMap();
								// Performancemessung
								if( bDebugPerform ) {
									System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.getProgressMap' ENDE" );
								}
	
								// Ergebnisanalyse
								int iECU_IO = 0, iECU_NIO = 0, iECU_OPEN = 0;
								setEcuIO.clear();
								setEcuNIO.clear();
								setEcuOpen.clear();
								
								// z�hlen der Processed und ProcessedWithErrors SGBM ID Eintr�ge
								int iIDExecutable = 0, iIDProcessed = 0, iIDProcessedWithErrors = 0;
	
								// eigentliche Ergebnisanalyse
								for( int i = 0; i < myTALResult.getTalLines().length; i++ ) {
									// BasisName des SG ermitteln (0x'hexcode' + " ECU:'Basisname')
									String sgAdress = "0" + Integer.toHexString( myTALResult.getTalLines()[i].getDiagnosticAddress().intValue() ).toUpperCase();
									String sgIDName = "0x" + sgAdress.substring( sgAdress.length() - 2, sgAdress.length() ) + " ECU:" + myTALResult.getTalLines()[i].getBaseVariantName();
		
									if( !mapAnzeigeECUs.containsKey( sgIDName ) ) {
										HashMap<String, String> mapAnzECU = new HashMap<String, String>();
										mapAnzECU.put( "0", sgIDName + " : 0 %;" );
										mapAnzeigeECUs.put( sgIDName, mapAnzECU );
									}
									String res = null;
									res = getProgressECU( myProgressMap, sgAdress.length() == 2 ? sgAdress : sgAdress.substring( sgAdress.length() - 2, sgAdress.length() ) );
									if( res != null ) {
									HashMap<String, String> mapAnzECU = new HashMap<String, String>();
									mapAnzECU = mapAnzeigeECUs.get( sgIDName );
									String fort = mapAnzECU.get( "0" );
									fort = sgIDName + " : " + res + " %;";
									mapAnzECU.put( "0", fort );
									mapAnzeigeECUs.put( sgIDName, mapAnzECU );
									}
		
									// falls schon vorhanden, dieses ECU erst mal aus den anderen Listen entfernen // Executable ist dominant
									setEcuIO.remove( sgIDName );
									setEcuNIO.remove( sgIDName );
		
									// Anzahl ECU_OPEN's
									// Es wird der TAL-Line Status analysiert und nicht der Status der TALTACategories, damit Fehler, die nach der Transaktion auftreten (-> FINALIZATION) auch erfasst werden 
									if( !myTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.PROCESSED ) && !myTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.PROCESSEDWITHERROR ) && !myTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.INACTIVE ) && !myTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.NOTEXECUTABLE ) )
										setEcuOpen.add( sgIDName );
		
									// Anzahl ECU_NIO's holen
									if( myTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.PROCESSEDWITHERROR ) || myTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.NOTEXECUTABLE ) ) {
										if( !setEcuIO.contains( sgIDName ) && !setEcuOpen.contains( sgIDName ) )
											setEcuNIO.add( sgIDName );
									}
		
									// Anzahl ECU_IO's holen
									if( myTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.PROCESSED ) ) {
										if( !setEcuNIO.contains( sgIDName ) && !setEcuOpen.contains( sgIDName ) )
											setEcuIO.add( sgIDName );
									}
		
									// Status der einzelnen TAs (SWTs hier nicht relevant) holen und merken in Anzeige-Map
									// UND
									// z�hle die SGBM IDs (executable, processed, processedWithErrors)
									for( int j = 0; j < myTALResult.getTalLines()[i].getTALTACategories().length; j++ ) {
										// Status der einzelnen TAs und SWTs holen und merken in IO und NIO Listen
		
										HashMap<String, String> mapAnzECU = new HashMap<String, String>();
										mapAnzECU = mapAnzeigeECUs.get( sgIDName );
										if( !mapAnzECU.containsKey( myTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString() ) ) {
											mapAnzECU.put( myTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString(), myTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString() + " = ???" );
											mapAnzeigeECUs.put( sgIDName, mapAnzECU );
										}
		
										// IO TA-Kategorien analysieren
										if( myTALResult.getTalLines()[i].getTALTACategories()[j].getStatus().equals( CascadeExecutionStatus.PROCESSED ) ) {
											mapAnzECU = mapAnzeigeECUs.get( sgIDName );
											mapAnzECU.put( myTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString(), myTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString() + " = transactionIO" );
											mapAnzeigeECUs.put( sgIDName, mapAnzECU );
										}
		
										// NIO TA-Kategorien analysieren
										if( myTALResult.getTalLines()[i].getTALTACategories()[j].getStatus().equals( CascadeExecutionStatus.PROCESSEDWITHERROR ) ) {
											mapAnzECU = mapAnzeigeECUs.get( sgIDName );
											mapAnzECU.put( myTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString(), myTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString() + " = transactionNIO" );
											mapAnzeigeECUs.put( sgIDName, mapAnzECU );
										}
		
										// NotExcecutable (auch NIO !)
										if( myTALResult.getTalLines()[i].getTALTACategories()[j].getStatus().equals( CascadeExecutionStatus.NOTEXECUTABLE ) ) {
											mapAnzECU = mapAnzeigeECUs.get( sgIDName );
											mapAnzECU.put( myTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString(), myTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString() + " = transactionNOTExecutable" );
											mapAnzeigeECUs.put( sgIDName, mapAnzECU );
										}
		
										// z�hle die SGBM IDs (executable, processed, processedWithErrors)
										for( int k = 0; k < myTALResult.getTalLines()[i].getTALTACategories()[j].getTAs().length; k++ ) {
											// ID executable zaehlen
											if( !myTALResult.getTalLines()[i].getTALTACategories()[j].getTAs()[k].getStatus().equals( CascadeExecutionStatus.PROCESSED ) && !myTALResult.getTalLines()[i].getTALTACategories()[j].getTAs()[k].getStatus().equals( CascadeExecutionStatus.PROCESSEDWITHERROR ) )
												++iIDExecutable;
		
											// ID processed zaehlen
											if( myTALResult.getTalLines()[i].getTALTACategories()[j].getTAs()[k].getStatus().equals( CascadeExecutionStatus.PROCESSED ) )
												++iIDProcessed;
		
											// ID processedWithError zaehlen
											if( myTALResult.getTalLines()[i].getTALTACategories()[j].getTAs()[k].getStatus().equals( CascadeExecutionStatus.PROCESSEDWITHERROR ) )
												++iIDProcessedWithErrors;
										}
									}
		
									// Werte setzen
									iECU_OPEN = setEcuOpen.size();
									iECU_NIO = setEcuNIO.size();
									iECU_IO = setEcuIO.size();
								}
		
								// Anzeige des Ergebnisses
								// ECU Nachbereitung
								if( iECU_OPEN == 0 )
									userDialog.displayMessage( (iRuns > 1 ? iRuns + ". " : "")+ PB.getString( "psdz.ud.TALAusfuehrung" ), PB.getString( "psdz.ud.SGBehandlunglaeuft" ) + psdz.getIStufeSoll() + "\n" + PB.getString( "psdz.ud.VIN" ) + this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer() + ";" + iECU_IO + " " + PB.getString( "psdz.ud.ECUok" ) + iECU_NIO + " " + PB.getString( "psdz.ud.ECUnok" ) + iECU_OPEN + " " + PB.getString( "psdz.ud.ECUopen" ) + ";" + PB.getString( "psdz.ud.SGNachbereitung" ), -1 );
								// ECU Bearbeitung (programmieren...)
								else
									userDialog.displayMessage( (iRuns > 1 ? iRuns + ". " : "")+ PB.getString( "psdz.ud.TALAusfuehrung" ), PB.getString( "psdz.ud.SGBehandlunglaeuft" ) + psdz.getIStufeSoll() + "\n" + PB.getString( "psdz.ud.VIN" ) + this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer() + ";" + iECU_IO + " " + PB.getString( "psdz.ud.ECUok" ) + iECU_NIO + " " + PB.getString( "psdz.ud.ECUnok" ) + iECU_OPEN + " " + PB.getString( "psdz.ud.ECUopen" ) + ";" + PB.getString( "psdz.ud.Programmierfortschritt" ) + "(ID " + (iIDProcessed + iIDProcessedWithErrors) + "/" + (iIDExecutable + iIDProcessed + iIDProcessedWithErrors) + ") " + psdz.getProgress() + "%" + ";" + getAnzeige( mapAnzeigeECUs ), -1 );
		
								userDialog.getProgressbar().setMaximum( iECU_IO + iECU_NIO + iECU_OPEN );
								userDialog.getProgressbar().setValue( iECU_IO + iECU_NIO );
								userDialog.getProgressbar().setString( (iECU_IO + iECU_NIO) + " / " + (iECU_IO + iECU_NIO + iECU_OPEN) );
		
								// CHECK TAL EXECUTION STATUS
								// fertig IO?
								// CascadeTALExecutionStatus.FINISHED_WITH_WARNINGS, CascadeTALExecutionStatus.FINISHED_FOR_HW_TRANSACTIONS_WITH_WARNINGS und CascadeTALExecutionStatus.FINISHED_FOR_HW_TRANSACTIONS werden auf CascadeTALExecutionStatus.FINISHED durch CASCADE-Kern-SW gemappt
								if( myTALResult.getStatus().getName().equals( CascadeTALExecutionStatus.FINISHED.getName() ) ) {
									//Ergebnis TAL auf PLatte ablegen
									psdz.getTALExecutionStatus( 0, "STATUS");
									
									// Erzeugen der TreeMaps IO, NIO und NotExecutable
									buildMaps( myTALResult, mapEcuIO, mapEcuNIO, mapEcuNotExecutable );
									// IO - ECUs in Ergebnisliste (APDM) eintragen
									entryECUsDetails( ergListe, mapEcuIO, "psdz.tal.transactionIO", Ergebnis.FT_IO, null, null );
									
									// IO Ergebnis in (APDM) eintragen
									result = new Ergebnis( "PSdZSingleEcuProgramming", "Result TAL", "", "", "", "STATUS", myTALResult.getStatus().getName(), myTALResult.getStatus().getName(), "", "0", "", "", "", "", "", Ergebnis.FT_IO );
									ergListe.add( result );
									// wiederholschleife umgehen
									iRuns = iDynamicRetries + 1;
									bDoRepeat = false;
									break;
								}
		
								// fertig NIO? // neue Stati
								// CascadeTALExecutionStatus.FINISHED_WITH_ERROR, CascadeTALExecutionStatus.ABORTED_BY_ERROR, CascadeTALExecutionStatus.ABORTED_BY_USER und CascadeTALExecutionStatus.FINISHED_FOR_HW_TRANSACTIONS_WITH_ERROR werden auf CascadeTALExecutionStatus.FINISHED_WITH_ERROR_IN_TRANSACTION durch CASCADE-Kern-SW gemappt
								if( myTALResult.getStatus().getName().equals( CascadeTALExecutionStatus.FINISHED_WITH_ERROR_IN_TRANSACTION.getName() ) ) {
									// mba: Nur wenn die maximale Anzahl �berschritten ist wird abgebrochen.
									if( iRuns > iDynamicRetries ) {
										//Ergebnis TAL auf PLatte ablegen
										psdz.getTALExecutionStatus( 0, "STATUS");
										
										// Performancemessung
										if( bDebugPerform ) {
											System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.getEcuStatisticsHexString' START" );
										}
										// Erzeugen der TreeMaps IO, NIO und NotExecutable
										buildMaps( myTALResult, mapEcuIO, mapEcuNIO, mapEcuNotExecutable );
										// PSdZ hole die aufgelaufenen Events
										HashMap<String, List<Exception>> psdzErrors = new HashMap<String, List<Exception>>( buildExcHashMap( myTALResult ) );
										HashMap<String, String> psdzECUStatisticsHexStr = new HashMap<String, String>( psdz.getEcuStatisticsHexString() );
										// Performancemessung
										if( bDebugPerform ) {
											System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.getEcuStatisticsHexString' ENDE" );
										}
		
										// IO - ECUs in Ergebnisliste (APDM) eintragen
										entryECUsDetails( ergListe, mapEcuIO, "psdz.tal.transactionIO", Ergebnis.FT_IO, null, null );
										
										// Eintrag der NIO - / NotExecutable - ECUs ins APDM
										entryECUsFT_NIO( ergListe, mapEcuNIO, mapEcuNotExecutable, psdzErrors, psdzECUStatisticsHexStr );
										
										// NIO Ergebnis in APDM eintragen
										result = new Ergebnis( "PSdZSingleEcuProgramming", "Result TAL", "", "", "", "STATUS", myTALResult.getStatus().getName(), "", "", "0", "", "", "", PB.getString( "psdz.tal.transactionNIO" ), "", Ergebnis.FT_NIO );
										ergListe.add( result );
										throw new PPExecutionException();
									}
									//dokumentiere TAL auf PLatte
									psdz.getTALExecutionStatus( 0, "REPEAT_" + iRuns ); // �nderungen
		
									//wiederhol-flag setzen und endlosschleife unterbrechen damit es weiter gehen kann.
									bDoRepeat = true;
									if( bDebug )
										System.out.println( "Wiederhole TAL" );
									
									CascadeTALLine line = null;
									
									// �ber alle ausgef�hrten TAL-Lines gehen. Was vorher schon i.o. (processed) war wird auf
									// INACTIVE gesetzt. Das wird nicht mehr ausgef�hrt und sp�ter als I.o. interpretiert
									for( int i = 0; i < myTALResult.getTalLines().length; i++ ) {
										line = myTALResult.getTalLines()[i];
										if( bDebug )
											System.out.println( "TalLine: " + line.getBaseVariantName() + ", " + line.getTALTACategories()[0].toString() );
		
										if( line.getStatus().equals( CascadeExecutionStatus.PROCESSED ) ) {
		
											// manipuliere die TAL lines
											psdz.manipulateTALLinePSdZ( line );
										} else {//Eintrag mit Processed with Error. Dokumentation f�r Wiederholung
											result = new Ergebnis( "PSdZSingleEcuProgramming", "Retry", "", "", "", "TalLine", line.getBaseVariantName(), "", "", "0", "", "", "", "", "", Ergebnis.FT_RETRY );
											ergListe.add( result );
										}
									}
		
									//dokumentieren, dass wiederholt wird.
									result = new Ergebnis( "PSdZSingleEcuProgramming", "DynamicRetry", "", "", "", "Retry", "" + iRuns, "" + iDynamicRetries, "", "0", "", "", "", "", "", Ergebnis.FT_RETRY );
									ergListe.add( result );
		
									break;
								}
		
		
								// Abbruch durch Werker???
								if( userDialog.isCancelled() == true ) {
									userDialog.reset();
		
									// Performancemessung
									if( bDebugPerform ) {
										System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.cancelTALExecution' START" );
									}
									psdz.cancelTALExecution();
									// Performancemessung
									if( bDebugPerform ) {
										System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.cancelTALExecution' ENDE" );
									}
		
									// Performancemessung
									if( bDebugPerform ) {
										System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.getEcuStatisticsHexString' START" );
									}
									// Erzeugen der TreeMaps IO, NIO und NotExecutable
									buildMaps( myTALResult, mapEcuIO, mapEcuNIO, mapEcuNotExecutable );
									// PSdZ hole die aufgelaufenen Events
									HashMap<String, List<Exception>> psdzErrors = new HashMap<String, List<Exception>>( buildExcHashMap( myTALResult ) );
									HashMap<String, String> psdzECUStatisticsHexStr = new HashMap<String, String>( psdz.getEcuStatisticsHexString() );
									// Performancemessung
									if( bDebugPerform ) {
										System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming 'psdz.getEcuStatisticsHexString' ENDE" );
									}
		
									// IO - ECUs in Ergebnisliste (APDM) eintragen
									entryECUsDetails( ergListe, mapEcuIO, "psdz.tal.transactionIO", Ergebnis.FT_IO, null, null );
									
									// Eintrag der NIO - / NotExecutable - ECUs ins APDM
									entryECUsFT_NIO( ergListe, mapEcuNIO, mapEcuNotExecutable, psdzErrors, psdzECUStatisticsHexStr );
									// Werkerabbruch in Ergebnisliste (APDM) eintragen
									result = new Ergebnis( "PSdZSingleEcuProgramming", "Userdialog", "", "", "", "", "", "", "", "0", "", PB.getString( "werkerAbbruch" ), "", PB.getString( "pollingAbbruch" ), "", Ergebnis.FT_NIO );
									ergListe.add( result );
									throw new PPExecutionException();
								}
							}
							
						} catch( CascadePSdZConditionsNotCorrectException e ) {
							// Fehler abfangen und dokumentiern
							if( bDebug )
								e.printStackTrace();
							result = new Ergebnis( "PSdZ", "PSdZSingleEcuProgramming", "", "", "", "", "", "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Bedingungen" ), Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						} catch( CascadePSdZRuntimeException e ) {
							// Fehler abfangen und dokumentiern
							if( bDebug )
								e.printStackTrace();
							result = new Ergebnis( "PSdZ", "PSdZSingleEcuProgramming", "", "", "", "", "", "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Laufzeit" ), Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						} 
					} // Ende While-Schleife

				// *********************************************************************************
				// * 4. ENDE: JUHU, Es gibt nichts zu tun, oder wir haben die Auswahl programmiert *
				// *********************************************************************************

				}
			} catch( CascadePSdZArgumentException e ) {
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZ", "PSdZSingleEcuProgramming", "", "", "", "", "", "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Argument" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( CascadePSdZConditionsNotCorrectException e ) {
				// Fehler abfangen und dokumentiern
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZ", "PSdZSingleEcuProgramming", "", "", "", "", "", "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Bedingungen" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( CascadePSdZRuntimeException e ) {
				// Fehler abfangen und dokumentiern
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZ", "PSdZSingleEcuProgramming", "", "", "", "", "", "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Laufzeit" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// ******************************************************************************
		// * Check auf Fehler bei weicher KIS // z.B.: KIS Doppeltreffer -> PP dann NIO *
		// ******************************************************************************
		try {
			List<CascadeConfigError> svbConfigErrors = psdz.getSollverbauungConfigErrors();
			CascadeConfigError myCascadeConfigError = null;

			// jetzt die ConfigError Liste durchgehen und eintragen der Fehler
			for( int i = 0; i < svbConfigErrors.size(); i++ ) {
				myCascadeConfigError = (CascadeConfigError) svbConfigErrors.get( i );
				// Fehler eintragen und ausgeben
				result = new Ergebnis( "PSdZSingleEcuProgramming", "Result KIS", "", "", "", "KIS Error ID:" + myCascadeConfigError.getConfigErrorCode() + ", ECU: 0x" + Integer.toHexString( myCascadeConfigError.getDiagAddress().intValue() ), "KIS Error Text:" + myCascadeConfigError.getConfigErrorText(), "", "", "0", "", "", "", "Error-Tolerant KIS Error", "", Ergebnis.FT_NIO );
				ergListe.add( result );
			}

			// weiche KIS Fehler PP NIO 
			if( svbConfigErrors.size() > 0 )
				throw new PPExecutionException();
		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			status = STATUS_EXECUTION_ERROR;
		}

		// R�cksetzen der Variablen f�r die Modusumschaltung
		try {
			this.getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_MODE_SWITCH", new Boolean( false ) );
			this.getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "PSDZ__TEMP_MODE_SWITCH_GATEWAY_LIST", new ArrayList<Object>() );
		} catch( Exception e ) {
			// Nothing
		}
		
		// Freigabe der benutzten Devices
		if( psdz != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releasePSdZ();
				if( bDebug )
					CascadeLogging.getLogger().log( LogLevel.INFO, "PSdZSingleEcuProgramming: Release PSdZ Device finished." );
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
		try {
			releaseUserDialog(ergListe);
			if( bDebug )
				CascadeLogging.getLogger().log( LogLevel.INFO, "PSdZSingleEcuProgramming: Release UserDialog Device finished." );
		} catch( PPExecutionException e ) {
			if( bDebug )
				e.printStackTrace();
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "PSdZSingleEcuProgramming", "", "", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}
		setPPStatus( info, status, ergListe );
		// Performancemessung Ende
		if( bDebugPerform )
			System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZSingleEcuProgramming PP ENDE" );

	}
	
	/**
	 * Kapselt das Bezeihen des User-Dialoges.
	 * 
	 * @param ergListe Liste, wo im Fehlerfall das NIO-Ergebnis abgelegt wird.
	 */
	private UserDialog getUserDialog(Vector<Ergebnis> ergListe) throws PPExecutionException {
		Ergebnis result;
		try {
			return getPr�flingLaufzeitUmgebung().getUserDialog();
		} catch( Exception e ) {
			if( e instanceof DeviceLockedException )
				result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
			else if( e instanceof DeviceNotAvailableException )
				result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			throw new PPExecutionException();
		}		
	}

	
	/**
	 * Kapselt die Freigabe User-Dialoges. 
	 * 
	 * @param ergListe Liste, wo im Fehlerfall das NIO-Ergebnis abgelegt wird.
	 */	
	private void releaseUserDialog(Vector<Ergebnis> ergListe) throws PPExecutionException {
		Ergebnis result;
		try {
			getPr�flingLaufzeitUmgebung().releaseUserDialog();			
		} catch( Exception e ) {
			if( e instanceof DeviceLockedException )
				result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			throw new PPExecutionException();
		}		
		getPr�flingLaufzeitUmgebung().freeUserDialog(); // erzwingt, dass beim n�chsten getUserdialog eine neue Instanz angelegt wird 
	}

}

/**
 * Checksum_2_0_F_Pruefprozedur.java

 * 
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;



/**
 * Implementierung der Pr�fprozedur, die Barcodes pr�ft und Teilstrings
 * zur�ckgibt.
 * Die Ergebnisse werden in den Results CS_RES1, CS_RES2, CS_RES3 gespeichert 
 * 
 * 1. Parameter einlesen
 * 2. Checksumme �berpr�fen
 * 3. String in Teile zerlegen. Falls angegeben noch ins Ediabas 'Data'-Format bringen
 * 
 * @author Michael Baum
 * @version Implementierung
 * @version 1_0_F 26.03.07 MB Ersterstellung
 * @version 2_0_F 10.09.07 MB �nderung Status wenn CS nicht korrekt. 
 */
public class Checksum_2_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	static final String CHECKSUM_MODE = "CHECKSUM_MODE"; // Die Checksummenberechnung : MODULO_36
	static final String BARCODE = "BARCODE";             // Der String der bearbeitet werden soll
	static final String RES1_POS = "RES1_POS";           // Die absolute Position des ersten Teilstrings
	static final String RES1_LEN = "RES1_LEN";           // Die Anzahl der Zeichen vom ersten Teilstring
	static final String RES1_DAT = "RES1_DAT";           // bestimmt, ob der String im Ediabas "Data-Format" (mit Leerzeichen) formatiert werden soll
	static final String RES2_POS = "RES2_POS";
	static final String RES2_LEN = "RES2_LEN";
	static final String RES2_DAT = "RES2_DAT";
	static final String RES3_POS = "RES3_POS";
	static final String RES3_LEN = "RES3_LEN";
	static final String RES3_DAT = "RES3_DAT";
	static final String DEBUG = "DEBUG";




	//----
	// Erlaubte Verfahren
	static final String MODULO_36 = "MODULO_36";




	// statische Hilfsvariablen
	public static boolean s_debug = true;
	public static boolean s_testmode = false;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public Checksum_2_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling
	 *            Klasse des zugeh. Pr�flings
	 * @param pruefprozName
	 *            Name der Pr�fprozedur
	 * @param hasToBeExecuted
	 *            Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public Checksum_2_0_F_Pruefprozedur(Pruefling pruefling,
			String pruefprozName, Boolean hasToBeExecuted) {
		super(pruefling, pruefprozName, hasToBeExecuted);
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "RES2_POS", "RES2_LEN", "RES2_DAT", "RES3_POS", "RES3_LEN", "RES3_DAT", "DEBUG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "BARCODE", "CHECKSUM_MODE", "RES1_POS", "RES1_LEN", "RES1_DAT" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	public boolean checkArgs() {

		try{

			//if (!s_testmode){
			if (!super.checkArgs())
				return false;

			// Momentan wird nur Pr�fsumme nach MODULO_36 unterst�tzt.
			if (!getArg(CHECKSUM_MODE).equalsIgnoreCase(MODULO_36)) return false;

			if (getArg(RES1_LEN) != null){
				if (Integer.parseInt(getArg(RES1_LEN)) > 0 ){
					if (getArg(RES1_POS) == null) return false;
					if (getArg(RES1_DAT) == null) return false;
				}
			}

			if (getArg(RES2_LEN) != null){
				if (Integer.parseInt(getArg(RES2_LEN)) > 0 ){
					if (getArg(RES2_POS) == null) return false;
					if (getArg(RES2_DAT) == null) return false;
				}
			}

			if (getArg(RES3_LEN) != null){
				if (Integer.parseInt(getArg(RES3_LEN)) > 0 ){
					if (getArg(RES3_POS) == null) return false;
					if (getArg(RES3_DAT) == null) return false;
				}
			}

			return true;
		}
		catch(Exception ex){
			if (s_debug) ex.printStackTrace();
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus:
	 * 1. Parameter einlesen
	 * 2. Checksumme �berpr�fen
	 * 3. String in Teile zerlegen. Falls angegeben noch ins Ediabas 'Data'-Format bringen
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	public void execute(ExecutionInfo info) {

		Hashtable ht_number;
		Hashtable ht_char;
		String[] jobparameters = null;

		ht_number = new Hashtable();
		ht_char = new Hashtable();

		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		char checksum_calc;     // Die errechnete Sollpr�fziffer des Barcodes
		char checksum_input;    // Die eingelesene IST-Pr�fziffer des Barcodes

		// die results. momentan maximal 3. evtl �ndern auf dynamisch?
		int     res1_pos = 0; 
		int     res1_len = 0; 
		boolean res1_dat = false;
		int     res2_pos = 0; 
		int     res2_len = 0; 
		boolean res2_dat = false;
		int     res3_pos = 0; 
		int     res3_len = 0;  
		boolean res3_dat = false;
		String  barcode;          // Der eingelesende Barcode als String
		String  checksum_mode;    // Die Art der Pr�fziffernberechnung
		String  text;

		try {
			// Parameter holen
			try {
				if (getArg(DEBUG) != null){
					s_debug = Boolean.valueOf(getArg(DEBUG)).booleanValue();
				}
				//F�r Debug: Ausgabe der Parameter
				if (s_debug){

					Iterator it = this.getArgs().entrySet().iterator();
					Map.Entry entry = null;

					System.out.println("Args:");
					while (it.hasNext()){
						entry = (Map.Entry) it.next();
						System.out.println(entry.getKey().toString() + ": " + entry.getValue().toString());
					}
				}

				// Parameter pr�fen
				if (checkArgs() == false)
					throw new PPExecutionException(PB.getString("parameterexistenz"));

				barcode = getArg(BARCODE);
				if(barcode.indexOf('@') != -1) {
					jobparameters = extractValues(barcode);
					barcode = "";
					for (int k=0;k<jobparameters.length-1;k++)
						barcode+= jobparameters[k]+";";

					barcode += jobparameters[jobparameters.length-1];
				}

				checksum_mode = getArg(CHECKSUM_MODE);


				res1_pos = Integer.parseInt(getArg(RES1_POS));
				res1_len = Integer.parseInt(getArg(RES1_LEN));
				res1_dat = Boolean.valueOf(getArg(RES1_DAT)).booleanValue();

				if (getArg(RES2_LEN) != null){
					res2_pos = Integer.parseInt(getArg(RES2_POS));
					res2_len = Integer.parseInt(getArg(RES2_LEN));
					res2_dat = Boolean.valueOf(getArg(RES2_DAT)).booleanValue();
				}

				if (getArg(RES3_LEN) != null){
					res3_pos = Integer.parseInt(getArg(RES3_POS));
					res3_len = Integer.parseInt(getArg(RES3_LEN));
					res3_dat = Boolean.valueOf(getArg(RES3_DAT)).booleanValue();
				}


			} catch (Exception e) {
				if (e.getMessage() != null)
					result = new Ergebnis("ParaFehler", "", "", "", "", "", "",
							"", "", "0", "", "", "", PB
							.getString("parametrierfehler"), e
							.getMessage(), Ergebnis.FT_NIO_SYS);
				else
					result = new Ergebnis("ParaFehler", "", "", "", "", "", "",
							"", "", "0", "", "", "", PB
							.getString("parametrierfehler"), "",
							Ergebnis.FT_NIO_SYS);
				ergListe.add(result);
				status = STATUS_EXECUTION_ERROR;
				throw new PPExecutionException();
			}

			// Tabelle f�r Pr�fziffermapping aufbauen
			buildTable(checksum_mode, ht_number, ht_char);



			if (checksum_mode != "") {

				// Checksum berechnen
				checksum_calc  = calcCheckSum(barcode, checksum_mode, ht_number, ht_char);
				if (s_debug)System.out.println("Checksum_calc: " + checksum_calc);
				checksum_input = readCheckSum(barcode, checksum_mode);
				if (s_debug)System.out.println("Checksum_read: " + checksum_input);

				if ( checksum_calc != checksum_input ){
					// fehler ausgeben, die checksumme passt nicht!
					result = new Ergebnis( "INPUT",         // ID - ID Ergebnis ID
							"Userdialog",   // werkzeug Informationsquelle (z.B. Diagnose, Strommesszange, ...)
							"",             // parameter_1 Parameter 1 f�r Werkzeug
							"",             // parameter_2 Parameter 2 f�r Werkzeug
							"",             // parameter_3 Parameter 3 f�r Werkzeug
							"INPUT",        // ergebnis  Name des angeforderten Ergebniswertes
							"" + barcode,     // ergebnisWert Zur�ckgelieferter Ergebniswert
							""+checksum_calc,             // minWert Zul�ssiger Minimalwert
							""+checksum_calc,             // maxWert Zul�ssiger Maximalwert
							"0",            // wiederholungen Anzahl an Wiederholungen, bis sich der unter fehlertyp angegebene Wert ergab
							"",             // frageText Fragetext, der dem Werker gestellt wird
							"",             // antwortText Antwort, die der Werker gegeben hat
							"",             // anweisText Anweisung, die der Werker auszuf�hren hatgestellt wird
							"Barocode checksum incorrect",             // fehlerText Fehlertext, falls ein Fehler vorliegt
							"",             // hinweisText Hinweis f�r Werker zur Fehlerbeseitigung
							Ergebnis.FT_NIO ); // fehlerTyp
					ergListe.add(result);
					status = STATUS_EXECUTION_ERROR;
				}
				else{

					/*// gesamtresult nicht notwendig, das es bereits in einer anderen pr�fprozedur gespeichert wurde
					// Results speichern
					result = new Ergebnis( "INPUT",         // ID - ID Ergebnis ID
							"Userdialog",   // werkzeug Informationsquelle (z.B. Diagnose, Strommesszange, ...)
							"",             // parameter_1 Parameter 1 f�r Werkzeug
							"",             // parameter_2 Parameter 2 f�r Werkzeug
							"",             // parameter_3 Parameter 3 f�r Werkzeug
							"INPUT",        // ergebnis  Name des angeforderten Ergebniswertes
							"" + barcode,     // ergebnisWert Zur�ckgelieferter Ergebniswert
							""+checksum_calc,             // minWert Zul�ssiger Minimalwert
							""+checksum_calc,             // maxWert Zul�ssiger Maximalwert
							"0",            // wiederholungen Anzahl an Wiederholungen, bis sich der unter fehlertyp angegebene Wert ergab
							"",             // frageText Fragetext, der dem Werker gestellt wird
							"",             // antwortText Antwort, die der Werker gegeben hat
							"",             // anweisText Anweisung, die der Werker auszuf�hren hatgestellt wird
							"",             // fehlerText Fehlertext, falls ein Fehler vorliegt
							"",             // hinweisText Hinweis f�r Werker zur Fehlerbeseitigung
							Ergebnis.FT_IO ); // fehlerTyp
					ergListe.add(result);
					 */
					text = barcode.substring(res1_pos, res1_pos + res1_len);
					if(res1_dat) text = getDataString(text);
					if(s_debug) System.out.println("CS_RES1: " + text);
					result = new Ergebnis( "CS_RES1",         // ID - ID Ergebnis ID
							"Userdialog",   // werkzeug Informationsquelle (z.B. Diagnose, Strommesszange, ...)
							"",             // parameter_1 Parameter 1 f�r Werkzeug
							"",             // parameter_2 Parameter 2 f�r Werkzeug
							"",             // parameter_3 Parameter 3 f�r Werkzeug
							"CS_RES1",        // ergebnis  Name des angeforderten Ergebniswertes
							"" + text,     // ergebnisWert Zur�ckgelieferter Ergebniswert
							"",             // minWert Zul�ssiger Minimalwert
							"",             // maxWert Zul�ssiger Maximalwert
							"0",            // wiederholungen Anzahl an Wiederholungen, bis sich der unter fehlertyp angegebene Wert ergab
							"",             // frageText Fragetext, der dem Werker gestellt wird
							"",             // antwortText Antwort, die der Werker gegeben hat
							"",             // anweisText Anweisung, die der Werker auszuf�hren hatgestellt wird
							"",             // fehlerText Fehlertext, falls ein Fehler vorliegt
							"",             // hinweisText Hinweis f�r Werker zur Fehlerbeseitigung
							Ergebnis.FT_IO ); // fehlerTyp
					ergListe.add(result);

					setDynamicAttribute("CS_RES1", text, null);


					// res2
					if (res2_len > 0){
						text = barcode.substring(res2_pos, res2_pos + res2_len);
						if(res2_dat) text = getDataString(text);
						if(s_debug) System.out.println("CS_RES2: " + text);
						result = new Ergebnis( "CS_RES2",         // ID - ID Ergebnis ID
								"Userdialog",   // werkzeug Informationsquelle (z.B. Diagnose, Strommesszange, ...)
								"",             // parameter_1 Parameter 1 f�r Werkzeug
								"",             // parameter_2 Parameter 2 f�r Werkzeug
								"",             // parameter_3 Parameter 3 f�r Werkzeug
								"CS_RES2",        // ergebnis  Name des angeforderten Ergebniswertes
								"" + text,     // ergebnisWert Zur�ckgelieferter Ergebniswert
								"",             // minWert Zul�ssiger Minimalwert
								"",             // maxWert Zul�ssiger Maximalwert
								"0",            // wiederholungen Anzahl an Wiederholungen, bis sich der unter fehlertyp angegebene Wert ergab
								"",             // frageText Fragetext, der dem Werker gestellt wird
								"",             // antwortText Antwort, die der Werker gegeben hat
								"",             // anweisText Anweisung, die der Werker auszuf�hren hatgestellt wird
								"",             // fehlerText Fehlertext, falls ein Fehler vorliegt
								"",             // hinweisText Hinweis f�r Werker zur Fehlerbeseitigung
								Ergebnis.FT_IO ); // fehlerTyp
						ergListe.add(result);
						setDynamicAttribute("CS_RES2", text, null);
					}

					// res3
					if (res3_len > 0){
						text = barcode.substring(res3_pos, res3_pos + res3_len);
						if(res3_dat) text = getDataString(text);
						if(s_debug) System.out.println("CS_RES3: " + text);
						result = new Ergebnis( "CS_RES3",         // ID - ID Ergebnis ID
								"Userdialog",   // werkzeug Informationsquelle (z.B. Diagnose, Strommesszange, ...)
								"",             // parameter_1 Parameter 1 f�r Werkzeug
								"",             // parameter_2 Parameter 2 f�r Werkzeug
								"",             // parameter_3 Parameter 3 f�r Werkzeug
								"CS_RES3",        // ergebnis  Name des angeforderten Ergebniswertes
								"" + text,     // ergebnisWert Zur�ckgelieferter Ergebniswert
								"",             // minWert Zul�ssiger Minimalwert
								"",             // maxWert Zul�ssiger Maximalwert
								"0",            // wiederholungen Anzahl an Wiederholungen, bis sich der unter fehlertyp angegebene Wert ergab
								"",             // frageText Fragetext, der dem Werker gestellt wird
								"",             // antwortText Antwort, die der Werker gegeben hat
								"",             // anweisText Anweisung, die der Werker auszuf�hren hatgestellt wird
								"",             // fehlerText Fehlertext, falls ein Fehler vorliegt
								"",             // hinweisText Hinweis f�r Werker zur Fehlerbeseitigung
								Ergebnis.FT_IO ); // fehlerTyp
						ergListe.add(result);

						setDynamicAttribute("CS_RES3", text, null);

					}
				}





			}



		} catch (Exception e) {
			result = new Ergebnis("Init", "CHECKSUM", "", "", "", "", "", "",
					"", "0", "", "", "", "Exception", PB
					.getString("unerwarteterLaufzeitfehler"),
					Ergebnis.FT_NIO_SYS);
			e.printStackTrace();
			ergListe.add(result);
			status = STATUS_EXECUTION_ERROR;
		} catch (Throwable t) {
			result = new Ergebnis("Init", "CHECKSUM", "", "", "", "", "", "",
					"", "0", "", "", "", "Throwable", PB
					.getString("unerwarteterLaufzeitfehler"),
					Ergebnis.FT_NIO_SYS);
			ergListe.add(result);
			status = STATUS_EXECUTION_ERROR;
		}
		if (status == STATUS_EXECUTION_OK) {
			result = new Ergebnis("Init", "CHECKSUM", "", "", "", "", "", "",
					"", "0", "", "", "", "", "", Ergebnis.FT_IO);
			ergListe.add(result);
		}

		if(!s_testmode){
			setPPStatus(info, status, ergListe);
		}
	}
	
	/**
	 * Hilfsmethode: Konvertiert einen String in einen Ediabas-Datenstring.
	 * D.h. ein Byte-String ohne Leerzeichen wird in einzelne Bytes zerlegt und mit Leerzeichen getrennt
	 * 
	 * z.B. : text = "0A1234EF" --> "0A 12 34 EF"
	 * 
	 * @param text: Input String ohne Leerzeichen
	 * @return der Text im Ediabas 'Data' Format
	 */
	private String getDataString(String text){
		String result = "";
		
		for (int i = 0; i < text.length(); i+=2){
			if(i+2 > text.length()){
				result += "0"+ text.substring(i,i+1);
			}
			else{
				result += text.substring(i,i+2);
			}
			if (i < text.length()-2) result += " ";
		}
		
		return result;
	}

	/**
	 * Berechnet die Checksumme des barcodes. Das letzte Zeichen (=Barcode-Checksum) wird dabei ignoriert.
	 * @param barcode: Der Barcode dessen Pr�fziffer berechnet werden soll
	 * @param checksum_mode: Algorithmus f�r Pr�fzifferberechnung, z.B. MODULO_36
	 * @param ht_number: Umrechnungstabelle der Zahlen
	 * @param ht_char: Umrechnungstabelle der Buchstaben
	 * @return die berechnete Pr�fziffer als char
	 */
	private char calcCheckSum(String barcode, String checksum_mode, Hashtable ht_number, Hashtable ht_char) {
		int check = 0;
		char ret = ' ';
		if (checksum_mode =="") check = 0;

		if (checksum_mode.equalsIgnoreCase(MODULO_36)){
			int factor_even = 1;
			int factor_odd = 3;
			int sum = 0;
			int num = 0;
			int char_value;
			
			//int pos = 0;

			char[] chars = barcode.toCharArray();
			for (int i = 0; i < chars.length-1; i++) {

				char_value = ((Integer) ht_char.get(new Character(chars[i]))).intValue();

				if ((i+1) % 2 == 0)
					num = char_value * factor_even;
				else
					num = char_value * factor_odd;
				sum += num;
			}


			check = sum % 36;

			ret = ((Character) ht_number.get(new Integer(check))).charValue();
			return ret;
		}
		return '%';

	}

	/**
	 * Liest die Checksumme des Barcodes = Das letzte Zeichen des Barcodes.
	 * @param barcode: Der Barcode dessen Pr�fziffer berechnet werden soll
	 * @param checksum_mode: Algorithmus f�r Pr�fzifferberechnung, z.B. MODULO_36
	 * @param ht_number: Umrechnungstabelle der Zahlen
	 * @param ht_char: Umrechnungstabelle der Buchstaben
	 * @return die berechnete Pr�fziffer als char
	 * */
	
	private char readCheckSum(String barcode, String checksum_mode) {
		char c_check = 0;
		c_check = barcode.charAt(barcode.length()-1);
		return c_check;
	}

	/**
	 * Baut die Zuordnungstabelle f�r die Zuordnung von Pr�fziffer zur Checksumme auf.
	 * @param code
	 * @return die fertige Umrechnunstabelle
	 */
	private boolean buildTable(String code, Hashtable ht_number, Hashtable ht_char) {
		if (code.equalsIgnoreCase(MODULO_36)) {
			add2Table(0,  '0', ht_number, ht_char);
			add2Table(1,  '1', ht_number, ht_char);
			add2Table(2,  '2', ht_number, ht_char);
			add2Table(3,  '3', ht_number, ht_char);
			add2Table(4,  '4', ht_number, ht_char);
			add2Table(5,  '5', ht_number, ht_char);
			add2Table(6,  '6', ht_number, ht_char);
			add2Table(7,  '7', ht_number, ht_char);
			add2Table(8,  '8', ht_number, ht_char);
			add2Table(9,  '9', ht_number, ht_char);
			add2Table(10, 'A', ht_number, ht_char);
			add2Table(11, 'B', ht_number, ht_char);
			add2Table(12, 'C', ht_number, ht_char);
			add2Table(13, 'D', ht_number, ht_char);
			add2Table(14, 'E', ht_number, ht_char);
			add2Table(15, 'F', ht_number, ht_char);
			add2Table(16, 'G', ht_number, ht_char);
			add2Table(17, 'H', ht_number, ht_char);
			add2Table(18, 'I', ht_number, ht_char);
			add2Table(19, 'J', ht_number, ht_char);
			add2Table(20, 'K', ht_number, ht_char);
			add2Table(21, 'L', ht_number, ht_char);
			add2Table(22, 'M', ht_number, ht_char);
			add2Table(23, 'N', ht_number, ht_char);
			add2Table(24, 'O', ht_number, ht_char);
			add2Table(25, 'P', ht_number, ht_char);
			add2Table(26, 'Q', ht_number, ht_char);
			add2Table(27, 'R', ht_number, ht_char);
			add2Table(28, 'S', ht_number, ht_char);
			add2Table(29, 'T', ht_number, ht_char);
			add2Table(30, 'U', ht_number, ht_char);
			add2Table(31, 'V', ht_number, ht_char);
			add2Table(32, 'W', ht_number, ht_char);
			add2Table(33, 'X', ht_number, ht_char);
			add2Table(34, 'Y', ht_number, ht_char);
			add2Table(35, 'Z', ht_number, ht_char);
			add2Table(36, '-', ht_number, ht_char);
			add2Table(37, '.', ht_number, ht_char);
			add2Table(38, ' ', ht_number, ht_char);
			add2Table(39, '$', ht_number, ht_char);
			add2Table(40, '/', ht_number, ht_char);
			add2Table(41, '+', ht_number, ht_char);
			add2Table(42, '%', ht_number, ht_char);
			return true;
		}
		return false;
	}

	/**
	 * Hilfsmethode zum Aufbau der Umrechnunstabelle. Wird nur von buildTable() verwendet!
	 * @param number    Checksumme
	 * @param character Zugeordnete Ziffer/Buchstabe
	 * @param ht_number Zieltabelle
	 * @param ht_char   Zieltabelle
	 * @return
	 */
	private boolean add2Table(int number, char character, Hashtable ht_number, Hashtable ht_char) {
		ht_number.put(new Integer(number), new Character(character));
		ht_char.put(new Character(character), new Integer(number));
		return true;
	}

	/**
	 * Assigns a given value to a specified dynamic attribute
	 * @param label the name of the dynamic attribute
	 * @param value the value of the dynamic attribute
	 * @storage defines the PL that will be used for storing dynamic attributes
	 */
	public void setDynamicAttribute(String label, String value, String storage) throws PPExecutionException {
		if(!s_testmode){
			if (s_debug) System.out.println("CASPlusData: setDynamicAttribute("+label+","+value+","+storage+")");
			Pruefling pl;   //reference to the storing PL
			if (storage != null) {
				pl = getPr�fling().getAuftrag().getPr�fling(storage);   //get the reference to the storing PL
			} else {
				pl = getPr�fling();   //get the reference to the storing PL (use current PL)
			}
			if (pl == null) {
				//throw an exeception if storing PL is null
				if (s_debug) System.out.println("CASPlusData.setDynamicAttribute: Storage-PL not found");
				else throw new PPExecutionException("CASPlusData.setDynamicAttribute: Storage-PL not found");
			}
			//get the hashtable of the PL where the attributes are stored
			Map ht = pl.getAllAttributes();
			if (ht == null) {
				//thow an exception if ht is null
				if (s_debug) System.out.println("CASPlusData.setDynamicAttribute: Hashtable not found");
				else throw new PPExecutionException("CASPlusData.setDynamicAttribute: Hashtable not found");
			}
			ht.put(label, value);   //put the pair (label/value) to the hashtable
		}
	}

	/**
	 * Test-Methode f�r Standalone-Test �ber MAIN Methode. Nicht f�r Cascade geeignet 
	 */
	public void initTest(){
		arguments = new Hashtable();
		arguments.put("BARCODE", "9815ABE7-3-1-T");
		arguments.put("CHECKSUM_MODE", "MODULO_36");
		arguments.put("RES1_POS", "0");
		arguments.put("RES1_LEN", "8");
		arguments.put("RES1_DAT", "true");
		arguments.put("RES2_POS", "9");
		arguments.put("RES2_LEN", "1");
		arguments.put("RES2_DAT", "false");
		arguments.put("DEBUG", "true");
		//arguments.put("", "");


	}



	/**
	 * Standalone Testmethode zum Testen der Klasse!
	 * @param args
	 */
	public static void main(String[] args){

		Checksum_2_0_F_Pruefprozedur.s_testmode = true;

		Checksum_2_0_F_Pruefprozedur test = new Checksum_2_0_F_Pruefprozedur();
		ExecutionInfo info = new ExecutionInfo(1,1,1,false);

		//System.out.println(test.getRequiredArgs()[1]);
		//System.out.println(test.getOptionalArgs()[1]);

		test.initTest();

		test.execute(info);
		
	}




}

/*
 * PUInfo_3_0_F_Pruefprozedur.java
 *
 * Created on 12.01.06
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.Enumeration;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.*;


/** 
 *  Implementierung der Pr�fprozedure, die eine/mehrere Pr�fstandsvariablen bearbeiten kann  
 *  @autor Werner Braunstorfer
 *  @version Implementierung
 *  @version 8_0_T 26.07.2017 MKe Schreibfehler CHECKSATUS behoben  <BR>
 *  @version 9_0_F 26.07.2017 MKe F-Version
 *  
 */
public class PUInfo_9_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	   * DefaultKonstruktor, nur fuer die Deserialisierung
	   */
	  public PUInfo_9_0_F_Pruefprozedur() {}	
	  
	  // Debug flags
	  boolean i_bPUInfoDebug_Standard = false;
	  boolean i_bPUInfoDebug_CheckArgs = false;
	    	  
		/**
	     * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	     * @param pruefling Klasse des zugeh. Pr�flings
	     * @param pruefprozName Name der Pr�fprozedur
	     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	     */	  
	  public PUInfo_9_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
	  
	  
	
	public boolean checkArgs() {
	    
		// Debug output on begin of checkArgs()
        if( i_bPUInfoDebug_CheckArgs ) {
            Enumeration enu = getArgs().keys();
            
            System.out.println("");
            System.out.println("checkArgs in: "+this.getName());
            System.out.println("All Arguments from this PP:");
            while( enu.hasMoreElements() ) {
                // The actual argument
                String strGivenkey = (String) enu.nextElement();
                String strActualArg = getArg(strGivenkey);
                System.out.println(strGivenkey+" = "+strActualArg);
            }
        }
        
        try {
        	
        	// 1. Check: Sind alle requiredArgs gesetzt
        	String[] requiredArgs = getRequiredArgs();

        	for(int i = 0; i < requiredArgs.length; i++) {
        		if(getArg(requiredArgs[i]) == null) return false;
        		if(getArg(requiredArgs[i]).equals("") == true) return false;
        	}
        	
        	if(i_bPUInfoDebug_CheckArgs) System.out.println("Done 1. Check");
        	
        	
        	// 2. Check: Ist ein g�ltiger Jobname gesetzt
        	// M�glich Jobname sind "SETSTATUS" und "CHECKSTATUS"
        	String jobname = getArg(getRequiredArgs()[0]);
        	if((jobname.equalsIgnoreCase("SETSTATUS") == false) && (jobname.equalsIgnoreCase("CHECKSTATUS") == false)){
        		System.out.println("JobName nicht g�ltig; Jobname: " + jobname);
        		return false;
        	}
        	
         	
        } catch (Exception e) {
            return false;
        } catch (Throwable e) {
            return false;
        }
        return true;
	}
	  

	
	
	public void execute( ExecutionInfo info ) {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        String debugString = null;
        String defaultStatus = "NIO";
        String statusVariable[] = null;
        String puStatus = null;
        
        /***********************************************
         * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
         * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
         ***********************************************/
        try {
            UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
            myAnalyser.LogSetTestStepName(this.getName());
            getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
        } catch (Exception e) {
            try {
                getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
            }
            catch (Exception e1) {}
        } 

        if(getArg("DEBUG") != null) {
        	debugString = getArg("DEBUG");
        } 
        else if (getPr�fling().getAttribut("DEBUG") != null) {
        	debugString = getPr�fling().getAttribut("DEBUG");
        }
        	
        
        // Debuginfos auswerten
        if(debugString != null){
        	String[] debugFlags = splitArg(debugString);
        	int n = debugFlags.length;
        	if (n >= 1){
        		for(int i = 0; i < n; i++) {
        			if(debugFlags[i].equalsIgnoreCase(("PUINFO_CHECK_STANDARD"))){
        				i_bPUInfoDebug_Standard = true;
        			}
                    else if( debugFlags[i].equalsIgnoreCase("PUINFO_CHECK_ARGS") ) {
                        i_bPUInfoDebug_CheckArgs = true;
                    }
                    else if( debugFlags[i].equalsIgnoreCase("ALL") ) {
                        i_bPUInfoDebug_Standard = true;
                        i_bPUInfoDebug_CheckArgs = true;
                    }
        		}
        	}
        }
        
        // Initial debug lines
        if( i_bPUInfoDebug_Standard ) {
            System.out.println("---");
            System.out.println("Start execute() from  PUInfo.");
            System.out.println("Test step: "+this.getName());
            System.out.println("---");
        }
        if( debugString!=null) {
            System.out.println("The debug string is: "+debugString);
            System.out.println("As a i_oResult of debug arguments/attributes the debug flags are: ");
            System.out.println("i_bPUInfoDebug_Standard = "+i_bPUInfoDebug_Standard);
            System.out.println("i_bPUInfoDebug_CheckArgs = "+i_bPUInfoDebug_CheckArgs);
        }
        
        try {
        	try{
        	
        		if( checkArgs() == false ) 
        			throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
        	}
        	catch( PPExecutionException e ) {
                if (e.getMessage() != null)
                        result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                else
                        result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
        	
            // Parameter auslesen
        	if(i_bPUInfoDebug_Standard)
            	System.out.println("Parameter ermitteln:");
            	
            	
        	// PU-Variablen auslesen
        	String puvariablen = getArg(getOptionalArgs()[0]);
        	// Falls kein Parameter angegeben -> Standardvariable einf�gen
        	if(puvariablen == null) {
        		puvariablen = "PUVARIABLE1";        		
        	}
        	
        	// PU-Variablen separieren. Trennzeichen = ; 
        	statusVariable = splitArg(puvariablen);
            	
        	// PU_Status auslesen. Falls nicht in Pr�fschritt enthalten, auf Default-Status setzten
        	puStatus = getArg(getOptionalArgs()[1]);
 
        	// Parameter auslesen
        	if(i_bPUInfoDebug_Standard)
            	System.out.println("Ergebnis puStatus: " + puStatus);        	
        	
        	// Falls kein PU-Status vorhanden -> setzen auf Defaultstatus
        	if(puStatus == null) {
        		// PU-Status nicht angegeben
        		puStatus = defaultStatus;
            	if(i_bPUInfoDebug_Standard)
                	System.out.println("PU-Status nicht vorhanden! Defaultwert setzten: " + puStatus);
        	}
        	else if(puStatus.equalsIgnoreCase("IO") == true) {
        		puStatus = "IO";
            	if(i_bPUInfoDebug_Standard)
                	System.out.println("PU-Status: " + puStatus);
        	}
        	else if(puStatus.equalsIgnoreCase("NIO") == true){
        		puStatus = "NIO";        	
            	if(i_bPUInfoDebug_Standard)
                	System.out.println("PU-Status: " + puStatus);
            }
        	else {
        		// PU-Status nicht bekannt
            	if(i_bPUInfoDebug_Standard)
                	System.out.println("PU-Status nicht bekannt: " + puStatus);
        		throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
        	}
        	
        	// �berpr�fen, ob PU-Variablen existieren. Falls nein, wird diese angelegt
        	// und mit NIO vorbelegt.
        	for (int i = 0; i < statusVariable.length; i++){
            	String tempStatusVariable = null;
        		// PU-Variable abrufen
        		try {
        			tempStatusVariable = (String)this.getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.TEMPORARY, statusVariable[i]);
        			if(i_bPUInfoDebug_Standard){
        				System.out.println("PU-Variable " + statusVariable[i] + " mit Status : " + tempStatusVariable);
        			}
                } 
        		
                // Variable nicht vorhanden; Variable anlegen
        		catch (VariablesException f) {
                	if(i_bPUInfoDebug_Standard){
                		System.out.println("Statusvariabel [" + i + "]: " + statusVariable[i] + " nicht vorhanden");
                	}

                	try {
                    	// PU-Variable anlegen mit Default-Status
                		this.getPr�flingLaufzeitUmgebung().setPruefstandVariable(VariableModes.TEMPORARY, statusVariable[i], defaultStatus);
    	        		
                		result = new Ergebnis( "PUInfo", "PUInfo", "CREATEVARIABLE", "", statusVariable[i], "", "", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO);
    	        		ergListe.add(result);
                		
    	        		if(i_bPUInfoDebug_Standard){
                    		System.out.println("Statusvariabel [" + i + "]: " + statusVariable[i] + " anlegen mit Status: " + defaultStatus);
                    	}                    	
                    }
                    catch (VariablesException e) {
                    	if(i_bPUInfoDebug_Standard){
                    		System.out.println("Statusvariabel [" + i + "]: " + statusVariable[i] + " konnte nicht angelegt werden");
                    	}
        	            result = new Ergebnis( "ExecFehler", "PUInfo", "CREATEVARIABLE", "", statusVariable[i], "", "NIO", "", "", "0", "", "", "", "Exception: VariablesException", "", Ergebnis.FT_NIO);
        	            ergListe.add(result);
        	            throw new PPExecutionException();
                     }                              	
                }            
        	}
        	
 
        	// Alle PU-Variablen erfolgreich eingelesen und initialisiert!!!!!
        	
        	String job = getArg(getRequiredArgs()[0]);
        	if(job.equalsIgnoreCase("SETSTATUS")){
            	for (int i = 0; i < statusVariable.length; i++){
            		try {
            			this.getPr�flingLaufzeitUmgebung().setPruefstandVariable(VariableModes.TEMPORARY, statusVariable[i], puStatus);
            			if(i_bPUInfoDebug_Standard){
            				System.out.println("Statusvariabel [" + i + "]: " + statusVariable[i] + " neu setzten: " + puStatus);
            			}

            			result = new Ergebnis( "PUInfo", "PUInfo", "SETVARIABLE", "", statusVariable[i], "", "", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO);
    	        		ergListe.add(result);
            		}
            		// Variable konnte nicht gesetzt werden
            		catch (VariablesException f) {
            			if(i_bPUInfoDebug_Standard){
            				System.out.println("Statusvariabel [" + i + "]: " + statusVariable[i] + " konnte nicht gesetzt werden");
            			}
        	            result = new Ergebnis( "ExecFehler", "PUInfo", "SETSTATUS", "", statusVariable[i], "", "NIO", "", "", "0", "", "", "", "Exception: VariablesException", "", Ergebnis.FT_NIO);
        	            ergListe.add(result);
        	            throw new PPExecutionException();
            		}          		
            	}
        		
        	} else if(job.equalsIgnoreCase("CHECKSTATUS")){
        		boolean pruefergebnis = true; // Pr�fung war IO
        		for(int i = 0; i < statusVariable.length; i++){
            		String tempStatusVariable = null;
        			try {
            			// Status der aktuellen PU-Variable auslesen
        				tempStatusVariable = (String)this.getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.TEMPORARY, statusVariable[i]);
            			if(i_bPUInfoDebug_Standard){
            				System.out.println("PU-Variable " + statusVariable[i] + " mit intenen Status : " + tempStatusVariable);
            			}
                    } 
                    catch (VariablesException f) {
                    	if(i_bPUInfoDebug_Standard){
                    		System.out.println("Statusvariabel [" + i + "]: " + statusVariable[i] + " nicht vorhanden");
                    	}
        	            result = new Ergebnis( "ExecFehler", "PUInfo", "CHECKSTATUS", "", statusVariable[i], "", "NIO", "", "", "0", "", "", "", "Exception: VariablesException", "", Ergebnis.FT_NIO);
        	            ergListe.add(result);
        	            throw new PPExecutionException();
                    }
                    // Vergleich zwischen dem Status von PU-Varibale und Soll-Variable
                    if(tempStatusVariable.equalsIgnoreCase(puStatus) == false){
            			if(i_bPUInfoDebug_Standard){
            				System.out.println("Status PU-Variable (" + statusVariable[i] + "): " + tempStatusVariable + "\t Vergleichswert: " + puStatus);
            			}
            			// SOll <> IST
            			pruefergebnis = false;
            			break;
                    }
        		}
	            
        		// Pr�fergebniss protokollieren
        		if(pruefergebnis == true){
	        		// Vergleich zwischen allen PU-Variablen-Stati stimmen mit der Soll-Variable �berein
	        		result = new Ergebnis( "PUInfo", "PUInfo", "CHECKSTATUS", "", puvariablen, "", "IO", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO);
	        		ergListe.add(result);
	            } 
	            else {
	            	status = STATUS_EXECUTION_ERROR;
	            	// Vergleich zwischen allen PU-Variablen-Stati stimmen mit der Soll-Variable NICHT �berein
	        		result = new Ergebnis( "PUInfo", "PUInfo", "CHECKSTATUS", "", puvariablen, "", "NIO", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO);
	        		ergListe.add(result);	            	
	            }
        	}

       
        } catch (PPExecutionException e) {
	        status = STATUS_EXECUTION_ERROR;
	    } catch (Exception e) {
	        if (e instanceof DeviceLockedException)
	            result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
	        else if (e instanceof DeviceNotAvailableException)
	            result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "DeviceNotAvailableException", Ergebnis.FT_NIO_SYS );
	        else
	            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler" ), "Exception: "+e.getMessage(), Ergebnis.FT_NIO_SYS );
	        ergListe.add(result);
	        status = STATUS_EXECUTION_ERROR;
	    } catch (Throwable e) {
	        result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler" ), "Throwable: "+e.getMessage(), Ergebnis.FT_NIO_SYS );
	        ergListe.add(result);
	        status = STATUS_EXECUTION_ERROR;
	    }

        //Status setzen
        setPPStatus( info, status, ergListe );
	}

	
	
	public String[] getRequiredArgs() {
		// TODO Auto-generated method stub
        String[] args = {"JOB"};
		return args;
	}

	public String[] getOptionalArgs() {
		// TODO Auto-generated method stub
        String[] args = {"PUVARIABLE", "STATUS"};
		return args;
	}

}

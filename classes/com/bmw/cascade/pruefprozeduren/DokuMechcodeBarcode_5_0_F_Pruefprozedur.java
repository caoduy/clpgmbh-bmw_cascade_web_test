/* 
* DokuMechcodeBarcode_1_0_F_Pruefprozedur.java 
* 
* Created on 17.06.14 
*/ 


//Import- Abschnitt 
package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.framework.udnext.UDNHandle;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.UDNButtonConfiguration;
import com.bmw.cascade.pruefstand.framework.udnext.UserDialogNextRuntimeEnvironment;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.UDNState;
import com.bmw.cascade.pruefstand.framework.udnext.UDNButtonSection.UDNButton;
import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.util.dom.DomPruefstand;
import com.bmw.cascade.util.dom.DomException;


/**
 * Implementierung der Pr�fprozedur, die auf eine Eingabe eines Barcodes durch den Benutzer wartet,
 * diesen auswertet und dann den X Datensatz schreibt.
 * 
 * 
 * @author BMW TI-545 Peter Winklhofer / Stephan Schubert
 * @version 1_0_F initiale Version
 * @version 2_0_T neues Barcodeformat hinzugef�gt
 * @version 3_0_F F Version
 * @version 4_0_T Wenn der Benutzer abbricht hat das zugeh�rige Ergebnis jetzt den Status "F".
 * 				  Es wurde ein neues Barcodeformat f�r die Erfassung von Schlie�codes mit IPS-Q hinzugef�gt.
 * 				  Behebung eines Fehlers bei dem es zu einem Ergebnis ohne "F" Result kommen konnte.
 * @version 5_0_F F Version
 */
public class DokuMechcodeBarcode_5_0_F_Pruefprozedur extends AbstractBMWPruefprozedur
{

	static final long serialVersionUID = 1L;
	
	enum MODE { UNKNOWN, RECORD, RECORD_NEW, DISPLAY}
	
	//Pr�fprozedur Parameter
	private String m_awt;
	private MODE m_mode = MODE.UNKNOWN;
	private long m_lockset_partnr_1 = 0;
	private long m_lockset_partnr_2 = 0;
	

	/**
	 * Default-Konstruktor, nur f�r die Deserialisierung.
	 */
	public DokuMechcodeBarcode_5_0_F_Pruefprozedur()
	{
	}

	/**
	 * Erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh�rigen Pr�flings.
	 * @param pruefprozName Name der Pr�fprozedur.
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit.
	 */
	public DokuMechcodeBarcode_5_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted )
	{
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialisiert die Argumente.
	 */
	protected void attributeInit()
	{
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * Argument LOCKSET_PARTNR_1 / LOCKSET_PARTNR_2:
	 * 		Es kann eine oder zwei erwartete Sachnummer(n) angegeben werden.
	 * 		Falls beide erwarteten Sachnummern nicht mit der erkannten �bereinstimmen wird die Pr�fprozedur abgebrochen.
	 * 		Wenn beide Argumente nicht gesetzt sind werden alle Sachnummern akzeptiert.
	 * 
	 * Argument AWT:
	 * 		Es kann ein benutzerdefinierter Anweisungstext f�r den Dialog zum Barcode scannen angegeben werden.
	 * 
	 * @return String-Array mit den optionalen Argumenten.
	 */
	public String[] getOptionalArgs()
	{
		String[] args = { "LOCKSET_PARTNR_1", "LOCKSET_PARTNR_2", "AWT" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 *  Argument MODE kann folgende Werte besitzen:
	 *  MODE = RECORD 		--> Barcode einlesen und in X Datensatz dokumentieren.
	 *  
	 *  MODE = RECORD_NEW 	--> Barcode einlesen und in X Datensatz dokumentieren.
	 *                          Pr�ft ob ein Fahrzeug bereits erfasst ist
	 *  						und zeigt gegebenenfalls einen Warndialog an.
	 *  
	 *  MODE = DISPLAY 		--> Vorhandenen Mechcode-Datensatz anzeigen
	 * 
	 * @return String-Array mit den zwingend erforderlichen Argumenten.
	 */
	public String[] getRequiredArgs()
	{
		String[] args = { "MODE" };
		return args;
	}

	/**
	 * Pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>True</code> bei positiver Pr�fung, andernfalls <code>false</code>.
	 */
	public boolean checkArgs() 
	{
		String temp, temp2 = null;
		
		try 
		{
			if( super.checkArgs() )
			{
				//�berpr�fe ob der erforderliche Parameter MODE gesetzt ist.
				temp = getArg( getRequiredArgs()[0]);	//MODE
				if (temp == null) return false;
				
				//�berpr�fe MODE auf zul�ssige Werte.
				if 	(		!temp.equalsIgnoreCase("RECORD")
						&& 	!temp.equalsIgnoreCase("RECORD_NEW")
						&& 	!temp.equalsIgnoreCase("DISPLAY")
					) return false;
				
				
				//�berpr�fe den optionalen Parameter LOCKSET_PARTNR_1
				temp = getArg( getOptionalArgs()[0]);
				if( temp != null )
				{
					temp = temp.trim();
					if (temp.length() == 0) 
					{
						temp = null;
					} 
					else 
					{
						//Nur numerische Werte sind zul�ssig f�r LOCKSET_PARTNR_1.
						try 
						{
							Long.parseLong(temp);
						} 
						catch(NumberFormatException n)
						{
							return false;
						}
					}
				}
				
				//�berpr�fe den optionalen Parameter LOCKSET_PARTNR_2
				temp2 = getArg( getOptionalArgs()[1]);
				if( temp2 != null ) 
				{
					temp2 = temp2.trim();
					if (temp2.length() == 0)
					{
						temp2 = null;
					}
					else
					{
						//Nur numerische Werte sind zul�ssig f�r LOCKSET_PARTNR_2.
						try
						{
							Long.parseLong(temp2);
						}
						catch(NumberFormatException n)
						{
							return false;
						}
					}
				}

				return true;
			}
			else //if( super.checkArgs() )
				return false;
			
		}
		catch( Throwable e ) 
		{
			return false;
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info )
	{
		// Immer notwendig.
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// Spezifische Variablen.
		boolean debug = true;
		String temp;
		boolean bc_format_error;
		String bc_format_error_txt = "";
		String bc_mechcode = "?????????";
		char bc_mechcode_cs = '?';
		String bc_partnumber_str = "-1";
		long bc_partnumber = -1;
		String bc_body;
		String bc_cs;
		
		String msg_text;
		String header_text;
		String hwt_partnumbers = "";
		int num_retries = 0;
		UDNState style = UDNState.MESSAGE;
		boolean abort_already_exists = false;
		
		UserDialogNextRuntimeEnvironment dlu = null;
		UDNHandle handle = null;
		String input;

		try
		{
			// Parameter holen.
			try
			{
				
				if( !checkArgs() )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				//�berpr�fe ob der erforderliche Parameter MODE gesetzt ist.
				temp = getArg( getRequiredArgs()[0]);	//MODE
				if (temp == null) throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				if (temp.equalsIgnoreCase("RECORD")) m_mode = MODE.RECORD;
				else if (temp.equalsIgnoreCase("RECORD_NEW")) m_mode = MODE.RECORD_NEW;
				else if (temp.equalsIgnoreCase("DISPLAY")) m_mode = MODE.DISPLAY;
				else
				{
					//Unbekannter MODE
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				}
				
				//�berpr�fe den optionalen Parameter LOCKSET_PARTNR_1
				m_lockset_partnr_1 = 0;
				temp = getArg( getOptionalArgs()[0]);	//LOCKSET_PARTNR_1
				if( temp != null )
				{
					temp = temp.trim();
					if (temp.length() != 0)
					{
						try
						{
							m_lockset_partnr_1 = Long.parseLong(temp);
						} 
						catch(NumberFormatException n)
						{
							throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
						}
					}
				}
				
				//�berpr�fe den optionalen Parameter LOCKSET_PARTNR_2
				m_lockset_partnr_2 = 0;
				temp = getArg( getOptionalArgs()[1]);	//LOCKSET_PARTNR_2
				if( temp != null )
				{
					temp = temp.trim();
					if (temp.length() != 0)
					{
						try
						{
							m_lockset_partnr_2 = Long.parseLong(temp);
						}
						catch(NumberFormatException n)
						{
							throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
						}
					}
				}
				
				//Erzeuge String zum anzeigen der erwarteten Sachnummern.
				hwt_partnumbers = "";
				if(m_lockset_partnr_1 != 0)
					hwt_partnumbers += m_lockset_partnr_1;
				
				if((m_lockset_partnr_1 != 0) && (m_lockset_partnr_2 != 0))
					hwt_partnumbers += " / ";
				
				if (m_lockset_partnr_2 != 0) 
					hwt_partnumbers += m_lockset_partnr_2;
				
				// Meldungstext AWT
				m_awt = getArg( getOptionalArgs()[2] );

			}
			catch( PPExecutionException e )
			{
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				
				ergListe.add( result );
				throw new PPExecutionException();
			}
			
			// Hier die Record Modi behandeln
			if ((m_mode == MODE.RECORD) || (m_mode == MODE.RECORD_NEW))
			{

				// Dialoge anzeigen
				try 
				{
					dlu = getPr�flingLaufzeitUmgebung().getUserDialogNext();
					handle = dlu.allocateUserDialogNext();

					//Ggf. Default-Bezeichung setzen
					if ((m_awt == null) || (m_awt.trim().length() == 0))
					{
						if (isDE()) m_awt = "Gleichschlie�ung Mechcode & Sachnummer";
						else m_awt = "lockset cutting code & partnumber";
					}

					//Pr�fe beim ersten Durchlauf ob Mechcode-Datensatz bereits vorhanden und zeige ggf. WerkerFrage
					abort_already_exists = false;
					if (m_mode == MODE.RECORD_NEW)
					{
						StringBuffer buff = null;
						try
						{
							buff = readFromCascadeServerOrPrufstand(debug);
						}
						catch (Exception e)
						{
							buff = null;
							if (debug) System.out.println("DokuMechcodeBarcode: X-Dataset not available, Exception: " + e.getMessage());
							if (isDE()) result = new Ergebnis( "X-Datensatz", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "", "X-Datensatz existiert noch nicht", Ergebnis.FT_IO );
							else result = new Ergebnis( "X-dataset", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "", "X-Dataset does not exist yet", Ergebnis.FT_IO );
							ergListe.add( result );
						}
						
						if (buff != null)
						{
							//Dialog zum Nachfragen anzeigen
							String ft;
							if (isDE()) ft = "Gleichschlie�ung wurde f�r diese FGNR bereits erfasst!\n Neue Gleichschlie�ung trotzdem erfassen?";
							else ft = "lockset for this VIN already registered!\nregister new lockset anyway?";
							if( handle != null )
							{
								UDNButton answer;
								answer = dlu.displayMessage( handle, UDNState.MESSAGE, PB.getString( "frage" ), ft, 0, UDNButtonConfiguration.YES_NO_BUTTON );
								dlu.releaseUserDialogNext( handle ); //!!!
								// Antwort auswerten.
								if( answer == UDNButton.YES_BUTTON )
								{
									if (isDE()) result = new Ergebnis( "X-Datensatz", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", ft, "YES", "", "", ft, Ergebnis.FT_IO );
									else result = new Ergebnis( "X-dataset", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", ft, "YES", "", "", ft, Ergebnis.FT_IO );
								}
								else
								{
									if (isDE()) result = new Ergebnis( "X-Datensatz", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", ft, "NO", "", PB.getString( "werkerfrageAbbruch" ), ft, Ergebnis.FT_IO );
									else result = new Ergebnis( "X-dataset", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", ft, "NO", "", PB.getString( "werkerfrageAbbruch" ), ft, Ergebnis.FT_IO );
									abort_already_exists = true;
								}
								ergListe.add( result );
							} 
							else
								throw new DeviceNotAvailableException();
						}
					}
					
					//Meldung und Header f�r ersten Durchlauf setzen
					msg_text = m_awt;
					if (isDE()) header_text = "Barcode einscannen oder manuelle Eingabe";
					else header_text = "Scan the bar code or manual input";
										
					input = null;
					Vector<Ergebnis> ergListeError = new Vector<Ergebnis>();
					Vector<Ergebnis> ergListeIO = new Vector<Ergebnis>();
					
					while ((input == null) && (status == STATUS_EXECUTION_OK) && !abort_already_exists)
					{
						Ergebnis resultInput = null;
						Ergebnis resultMechCode = null;
						Ergebnis resultBarCode = null;
						Ergebnis resultSNR = null;
						
						bc_format_error = false;
						bc_format_error_txt = "";
						
						boolean userAbort = false;
						
						
						if( handle != null )
						{
							input = dlu.requestTextInput( handle, style, header_text, msg_text, 0, UDNButtonConfiguration.OK_CANCEL_BUTTON );
							dlu.releaseUserDialogNext( handle ); //!!!
						} 
						else
							throw new DeviceNotAvailableException();
						
						// Eingabe pr�fen, ob sie eventuell leer war.
						if (input == null)
						{
							//input NULL = ABBRUCH gedr�ckt
							if (debug) System.out.println("DokuMechcodeBarcode: input = null");
							if (isDE()) resultInput = new Ergebnis( "INPUT", "DokuMechcodeBarcode", "", "", "", "INPUT", "", "", "", ""+num_retries, m_awt, "Abbruch durch Nutzer", "", "", "", Ergebnis.FT_NIO );
							else resultInput = new Ergebnis( "INPUT", "DokuMechcodeBarcode", "", "", "", "INPUT", "", "", "", ""+num_retries, m_awt, "canceled by user", "", "", "", Ergebnis.FT_NIO );
							status = STATUS_EXECUTION_ERROR;
							bc_format_error = true;
							userAbort = true;
						} 
						else if (input.trim().length() == 0) 
						{
							//input leer
							if (debug) System.out.println("DokuMechcodeBarcode: input leer");
							if (isDE()) resultInput = new Ergebnis( "INPUT", "DokuMechcodeBarcode", "", "", "", "INPUT", "empty", "", "", ""+num_retries, m_awt, "", "", "", "", Ergebnis.FT_IGNORE );
							else resultInput = new Ergebnis( "INPUT", "DokuMechcodeBarcode", "", "", "", "INPUT", "empty", "", "", ""+num_retries, m_awt, "", "", "", "", Ergebnis.FT_IGNORE );
							input = null;
							if (isDE()) msg_text = m_awt + "\nLeere Eingabe! \nBitte korrekten Barcode einlesen oder eingeben!";
							else msg_text = m_awt + "\nempty input!\n read or type a correct barcode";
							style = UDNState.ALERT;
							bc_format_error = true;
						}
						else 
						{
							//input nicht leer
							if (isDE()) resultInput = new Ergebnis( "INPUT", "DokuMechcodeBarcode", "", "", "", "INPUT", input, "", "", ""+num_retries, m_awt, "", "", "", "", Ergebnis.FT_IO );
							else resultInput = new Ergebnis( "INPUT", "DokuMechcodeBarcode", "", "", "", "INPUT", input, "", "", ""+num_retries, m_awt, "", "", "", "", Ergebnis.FT_IO );
							//input zerlegen und Format pr�fen
							// Format 1: HF00004937-729646503QX
						    // Format 2: HF004937-7296465
							
							input = input.trim().toUpperCase();
							if (input.length() == 22) //Format 1
							{
								// Format 1: HF00004937-729646503QX								
								// Hinweis: Trennzeichen '-' wird nicht explizit gepr�ft, da f�lschlicherweise auch nicht EWSCASTool gepr�ft
								bc_mechcode = input.substring(0, 9);
								if (debug) System.out.println("DokuMechcodeBarcode: bc_mechcode = \"" + bc_mechcode + "\"");
								bc_mechcode_cs = input.charAt(9);
								if (debug) System.out.println("DokuMechcodeBarcode: bc_mechcode_cs = \"" + bc_mechcode_cs + "\"");
								bc_partnumber_str = input.substring(11, 11+7);
								if (debug) System.out.println("DokuMechcodeBarcode: bc_partnumber_str = \"" + bc_partnumber_str + "\"");
								bc_body = input.substring(0,20);
								if (debug) System.out.println("DokuMechcodeBarcode: bc_body = \"" + bc_body + "\"");
								bc_cs = input.substring(20,22);
								if (debug) System.out.println("DokuMechcodeBarcode: bc_cs = \"" + bc_cs + "\"");
								//2 Checksummen-Bytes �ber gesamten Barcode berechnen
								temp = calcBarcodeChecksum(bc_body);
								if (!temp.equals(bc_cs)) 
								{
									//Checksummen-Fehler
									bc_format_error = true;
									if (isDE()) bc_format_error_txt = "Checksummen-Fehler in " + input;
									else bc_format_error_txt = "checksum error in " + input;									
								}
										
								
							} 
							else if (input.length() == 16) //Format 2
							{
							    // Format 2: HF004937-7296465
								// Hinweis: Trennzeichen '-' wird nicht ausgewertet, da f�lschlicherweise auch nicht EWSCASTool gepr�ft
								bc_mechcode=input.substring(0, 7);
								bc_mechcode=bc_mechcode.substring(0, 3)+"00"+bc_mechcode.substring(3);	//Wegoptimierte 00 einf�gen
								if (debug) System.out.println("DokuMechcodeBarcode: bc_mechcode = \"" + bc_mechcode + "\"");
								bc_mechcode_cs = input.charAt(7);
								if (debug) System.out.println("DokuMechcodeBarcode: bc_mechcode_cs = \"" + bc_mechcode_cs + "\"");
								bc_partnumber_str = input.substring(9);
								if (debug) System.out.println("DokuMechcodeBarcode: bc_partnumber_str = \"" + bc_partnumber_str + "\"");
							}
							else if (input.length() == 18) //Format 3 (N�tig wegen Lieferung von falschen Barcodes. Kann entfernt werden wenn diese aufgebraucht sind.)
							{
								// Format 3: HF00001996-7300332
								
								//Den Header �berpr�fen.
								String header = input.substring(0, 6);
								if(!header.contentEquals("HF0000") || input.charAt(10)!='-')
								{
									bc_format_error = true;
									if (isDE()) bc_format_error_txt = "Ung�ltiger Barcode";
									else bc_format_error_txt = "invalid bar code";								
								}
								else
								{
									//Den MechCode ohne die zus�tzliche 0 �bernehmen
									bc_mechcode=input.substring(0, 5) + input.substring(6, 10);
									if (debug) System.out.println("DokuMechcodeBarcode: bc_mechcode = \"" + bc_mechcode + "\"");
								
									//Die Checksumme fehlt bei diesem Format und muss selbst berechnet werden!
									bc_mechcode_cs = calculateMechCodeChecksum(bc_mechcode, debug);
									if (debug) System.out.println("DokuMechcodeBarcode: bc_mechcode_cs = \"" + bc_mechcode_cs + "\"");
								
									//Jetzt noch die Sachnummer �bernehmen.
									bc_partnumber_str = input.substring(11);
									if (debug) System.out.println("DokuMechcodeBarcode: bc_partnumber_str = \"" + bc_partnumber_str + "\"");
								}
							}
							else if (input.length() == 23) //Format 4 (IPS-Q)
							{
								//Format 4: 0C729646603HF0002422400
															
								//Den Header �berpr�fen
								String header = input.substring(0, 2);
								String footer = input.substring(21, 23);
								
								//Der Barcode beginnt immer mit "0C" und endet mit "00".
								if( (!header.contentEquals("0C")) || (!footer.contentEquals("00")))
								{
									bc_format_error = true;
									if (isDE()) bc_format_error_txt = "Ung�ltiger Barcode";
									else bc_format_error_txt = "invalid bar code";								
								}
								else
								{
									bc_mechcode = input.substring(11,20);
									if (debug) System.out.println("DokuMechcodeBarcode: bc_mechcode = \"" + bc_mechcode + "\"");
									
									bc_mechcode_cs = input.charAt(20);
								    if (debug) System.out.println("DokuMechcodeBarcode: bc_mechcode_cs = \"" + bc_mechcode_cs + "\"");
									
									bc_partnumber_str = input.substring(2,9);
									if (debug) System.out.println("DokuMechcodeBarcode: bc_partnumber_str = \"" + bc_partnumber_str + "\"");
								}
							}
							else 
							{
								//falsche L�nge, weder Format 1, noch Format 2, noch Format 3, noch Format 4
								bc_format_error = true;
								if (isDE()) bc_format_error_txt = "Ung�ltige L�nge";
								else bc_format_error_txt = "invalid length";
							}
							
							
							//Pr�fziffer f�r Mechcode testen							
							boolean checksumNOK = true;
							
							if(!bc_format_error)
							{
							  try
							  {
							    checksumNOK = (bc_mechcode_cs != calculateMechCodeChecksum(bc_mechcode, debug));
							  }
							  catch(PPExecutionException p)
							  {
								bc_format_error = true;
								if (isDE()) bc_format_error_txt = "Ung�ltiger Barcode";
								else bc_format_error_txt = "invalid bar code";
							  }
							}
																			
							if (!bc_format_error)
							{	
								if (checksumNOK)
								{
									//Pr�fziffer falsch
									bc_format_error = true;
									if (isDE()) bc_format_error_txt = "Fehler Pr�fziffer in " + bc_mechcode + bc_mechcode_cs;
									else bc_format_error_txt = "check digit error in " + bc_mechcode + bc_mechcode_cs;									
								} 
								else
								{
									//Cascade-Ergebnis anlegen
									if (isDE()) resultMechCode = new Ergebnis( "MECHCODE", "DokuMechcodeBarcode", "", "", "", "MECHCODE", bc_mechcode + bc_mechcode_cs, "", "", ""+num_retries, "", "", "", "", "", Ergebnis.FT_IO );
									else resultMechCode = new Ergebnis( "MECHCODE", "DokuMechcodeBarcode", "", "", "", "MECHCODE", bc_mechcode + bc_mechcode_cs, "", "", ""+num_retries, "", "", "", "", "", Ergebnis.FT_IO );
								}
							}
							
							//Sachnummern-Format �berpr�fen
							if(!bc_format_error)
							{
							  try 
							  {
							  	bc_partnumber = Long.parseLong(bc_partnumber_str);
							  }
							  catch (NumberFormatException n) 
							  {
								//Sachnummer nicht numerisch
								bc_format_error = true;
								if (isDE()) bc_format_error_txt = "Format-Fehler SNR " + bc_partnumber;
								else bc_format_error_txt = "format error partnumber " + bc_partnumber;																	
							  }
							}
							
							//Bei Format oder Checksummen-Fehlern Meldung setzen und ein CASCADE-Ergebnis erzeugen
							if (bc_format_error)
							{
								if (debug) System.out.println("DokuMechcodeBarcode: Format Fehler \"" + input + "\"");
								if (isDE()) resultBarCode = new Ergebnis( "BARCODE", "DokuMechcodeBarcode", "", "", "", "BARCODE", input, "", "", ""+num_retries, "", "", "", "FORMAT ERROR " + bc_format_error_txt, "", Ergebnis.FT_IGNORE );
								else resultBarCode = new Ergebnis( "BARCODE", "DokuMechcodeBarcode", "", "", "", "BARCODE", input, "", "", ""+num_retries, "", "", "", "FORMAT ERROR " + bc_format_error_txt, "", Ergebnis.FT_IGNORE );
								input = null;
								if (isDE()) msg_text = m_awt + "\nFormat Fehler!\n" + bc_format_error_txt + "\nBitte korrekten Barcode einlesen oder eingeben!";
								else msg_text = m_awt + "\ninput format error!\n" + bc_format_error_txt + "\n read or type a correct barcode";
								style = UDNState.ALERT;
								//dlu.displayMessage(handle, UDNState.ALERT, error_text, awt, 0, UDNButtonConfiguration.OK_BUTTON);
								//dlu.releaseUserDialogNext( handle ); //!!!
							}
							
							//Sachnummer pr�fen
							if (!bc_format_error)
							{
								if ((m_lockset_partnr_1 != 0) && (bc_partnumber == m_lockset_partnr_1)) 
								{
									//Cascade-Ergebnis anlegen SNR = SNR1
									if (isDE()) resultSNR = new Ergebnis( "SNR", "DokuMechcodeBarcode", "", "", "", "SNR", "" + bc_partnumber, "", "", ""+num_retries, "", "", "", "", "SNR = Prim�rsachnummer", Ergebnis.FT_IO );
									else resultSNR = new Ergebnis( "SNR", "DokuMechcodeBarcode", "", "", "", "SNR", "" + bc_partnumber, "", "", ""+num_retries, "", "", "", "", "SNR = primary partnumber", Ergebnis.FT_IO );									
								} 
								else if ((m_lockset_partnr_2 != 0) && (bc_partnumber == m_lockset_partnr_2))
								{
									//Cascade-Ergebnis anlegen SNR = SNR2
									if (isDE()) resultSNR = new Ergebnis( "SNR", "DokuMechcodeBarcode", "", "", "", "SNR", "" + bc_partnumber, "", "", ""+num_retries, "", "", "", "", "SNR = Alternativsachnummer", Ergebnis.FT_IO );
									else resultSNR = new Ergebnis( "SNR", "DokuMechcodeBarcode", "", "", "", "SNR", "" + bc_partnumber, "", "", ""+num_retries, "", "", "", "", "SNR = secondary partnumber", Ergebnis.FT_IO );
								}
								else if ((m_lockset_partnr_1 == 0) && (m_lockset_partnr_2 == 0))
								{
									//Es sind alle Sachnummern zul�ssig, da es keine Vorgabe gibt.
									//Das Cascade-Ergebnis anlegen.
									if (isDE()) resultSNR = new Ergebnis( "SNR", "DokuMechcodeBarcode", "", "", "", "SNR", "" + bc_partnumber, "", "", ""+num_retries, "", "", "", "", "ohne Bewertung", Ergebnis.FT_IO );
									else resultSNR = new Ergebnis( "SNR", "DokuMechcodeBarcode", "", "", "", "SNR", "" + bc_partnumber, "", "", ""+num_retries, "", "", "", "", "no check", Ergebnis.FT_IO );
								}
								else
								{
									//Die Sachnummer stimmt nicht mit der Vorgabe �berein.
									if (debug) System.out.println("DokuMechcodeBarcode: Sachnummer " + bc_partnumber + " != " + m_lockset_partnr_1 + "/" + m_lockset_partnr_2);
									if (isDE()) resultSNR = new Ergebnis( "SNR", "DokuMechcodeBarcode", "", "", "", "SNR", ""+ bc_partnumber, "", "", ""+num_retries, "", "", "", "Falsche Sachnummer", "" + bc_partnumber + " != " + m_lockset_partnr_1 + " / " + m_lockset_partnr_2, Ergebnis.FT_IGNORE );
									else resultSNR = new Ergebnis( "SNR", "DokuMechcodeBarcode", "", "", "", "SNR", ""+ bc_partnumber, "", "", ""+num_retries, "", "", "", "wrong partnumber", "" + bc_partnumber + " != " + m_lockset_partnr_1 + " / " + m_lockset_partnr_2, Ergebnis.FT_IGNORE );
									input = null;
									if (isDE()) msg_text = m_awt + "\nUnerwartete Sachnummer gefunden!\n" + "Gefundene Sachnummer: " + bc_partnumber + "\nErwartete Sachnummer(n): " + hwt_partnumbers + "\nBitte korrekten Barcode einlesen oder eingeben!";
									else msg_text = m_awt + "\nUnexpected partnumber found!\n" + "Found partnumber: " + bc_partnumber + "\nExpected partnumber(s): " + hwt_partnumbers + "\n read or type a correct barcode";
									style = UDNState.ALERT;	
									
									bc_format_error = true;
								}

							}
						}
						
						//Falls ein Fehler aufgetreten ist dann den Z�hler an den Ergebnissnamen anh�ngen.
						//Results in der Error Liste merken.
						if(bc_format_error)
						{
							String ergebnisName;
							//String ergebnisID;
							
							if(resultInput != null)
							{
								ergebnisName = resultInput.getErgebnis();								
								resultInput.setErgebnis(ergebnisName + "_" + num_retries);
								
								//Wenn der Benutzer auf Abbruch gedr�ckt hat dann muss dieses Ergebenis auf fail bleiben!
								if(!userAbort)
								{
									//Auf ignore setzen da der Benutzer noch eine Chance bekommt!
									resultInput.setFehlerTyp(Ergebnis.FT_IGNORE);
								}
								
								ergListeError.add(resultInput);
							}
							if(resultMechCode != null)
							{
								ergebnisName = resultMechCode.getErgebnis();						
								resultMechCode.setErgebnis(ergebnisName + "_" + num_retries);																

								ergListeError.add(resultMechCode);
							}
							if(resultBarCode != null)
							{
								ergebnisName = resultBarCode.getErgebnis();
								resultBarCode.setErgebnis(ergebnisName + "_" + num_retries);
								
								ergListeError.add(resultBarCode);
							}
							if(resultSNR != null)
							{
								ergebnisName = resultSNR.getErgebnis();
								resultSNR.setErgebnis(ergebnisName + "_" + num_retries);
								
								ergListeError.add(resultSNR);
							}						
						}
						//Results in der IO Liste merken
						else
						{
							if(resultInput != null)
							{
								ergListeIO.add(resultInput);								
							}
							if(resultMechCode != null)
							{								
								ergListeIO.add(resultMechCode);
							}
							if(resultBarCode != null)
							{
								ergListeIO.add(resultBarCode);								
							}
							if(resultSNR != null)
							{
								ergListeIO.add(resultSNR);
							}
						}
						num_retries++;
					} //END while ((input == null) && (status == STATUS_EXECUTION_OK) && !abort_already_exists)
					
					//Ergebnisse aus der IO Liste und der Fehler Liste �bertragen in die gesamte Liste.
					
					//Zuerst die Ergebnisse die IO sind
					for(int i=0;i<ergListeIO.size();i++)
					{
						ergListe.add(ergListeIO.elementAt(i));						
					}
					//Dann die Ergebnisse die NIO sind
					for(int i=0;i<ergListeError.size();i++)
					{
						ergListe.add(ergListeError.elementAt(i));						
					}
					
					
					if ((status == STATUS_EXECUTION_OK) && !abort_already_exists)
					{
						//X Datensatz XML erzeugen
				        StringBuffer xml = new StringBuffer();
				        xml.append("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\r\n");
				        xml.append("<vehicleImmobilizer xmlns=\"http://bmw.com/immobiliser\""+
				                " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""+
				                " xsi:schemaLocation=\"http://bmw.com/immobiliser ../vehicleImmobilizerV01_01.xsd\""+
				                " version=\"01.01\" refSchema=\"vehicleImmobilizerV01_01.xsd\">\r\n");

				        xml.append("\t<vehicle vin=\"" + getPr�fling().getAuftrag().getFahrgestellnummer() +"\">\r\n");
				        xml.append("\t\t<coding>\r\n");
				        xml.append("\t\t\t<mechanicalLockingSystem" +
				                " version=\"01.00\"" +
				                " refSchema=\"mechanicalLockingSystemV0100.xsd\"" +
				                " creationDateTime=\"");
				        xml.append(getXMLCreationTime());
				        xml.append("\">\r\n");
				        xml.append("\t\t\t\t<lockingCode>"+bc_mechcode + bc_mechcode_cs +"</lockingCode>\r\n");
				        xml.append("\t\t\t</mechanicalLockingSystem>\r\n");
				        xml.append("\t\t</coding>\r\n");
				        xml.append("\t</vehicle>\r\n");
				        xml.append("</vehicleImmobilizer>\r\n");
				        
	                   //Send Data to CASCADE-Server
	                   try 
	                   {
	                       DomPruefstand.writeDomData(getPr�fling().getAuftrag().getFahrgestellnummer7(), "X", xml.toString().getBytes());
	                   } 
	                   catch(Exception e) 
	                   {
	                	   if (debug) System.out.println("DokuMechcodeBarcode: Senden an CASCADE-Server fehlgeschlagen: " + e.getMessage());
	                       throw e;
	                   }


					}

				}
				catch( Exception e )
				{
					if( e instanceof DeviceLockedException )
						result = new Ergebnis( "ExecFehler", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
					else if( e instanceof DeviceNotAvailableException )
						result = new Ergebnis( "ExecFehler", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
					else if( e instanceof DomException )
						result = new Ergebnis( "ExecFehler", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "DomException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "ExecFehler", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}

			}
			else if (m_mode == MODE.DISPLAY)
			{
				//Anzeige eines vorhandenen Datensatzes
				try
				{
					StringBuffer buff = null;
					String str;
					String mechcode = "??????????";
					int i = -1, j = -1;
					try
					{
						buff = readFromCascadeServerOrPrufstand(debug);
					}
					catch (Exception e)
					{
						buff = null;
						if (debug) System.out.println("DokuMechcodeBarcode: X-Dataset not available, Exception: " + e.getMessage());
					}
					
					if (buff == null)
					{
						//kein X-Datensatz verf�gbar
						if (debug) System.out.println("DokuMechcodeBarcode: X-Dataset not available: null" );
						if (isDE()) m_awt = "Kein MechcodeDatensatz verf�gbar!";
						else m_awt = "no mechcode dataset available!";
						style = UDNState.ALERT;
						if (isDE()) result = new Ergebnis( "X-Datensatz Anzeigen", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "", "X-Datensatz ist nicht verf�gbar", Ergebnis.FT_NIO_SYS );
						else result = new Ergebnis( "X-dataset display", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "", "X-Dataset not available", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
					else
					{
						//X-Datensatz verf�gbar, also mechcode extrahieren
						str = buff.toString().toUpperCase();
						if (debug) System.out.println("DokuMechcodeBarcode: X-Dataset:\n" + str + "\n" );
						i = str.indexOf("<LOCKINGCODE>");
						if (i == -1)
						{
							//Format-Fehler im Datensatz: kein <lockingCode>
							if (debug) System.out.println("DokuMechcodeBarcode: X-Dataset Format error: no <lockingCode> ");
							if (isDE()) result = new Ergebnis( "X-Datensatz Anzeigen", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "", "X-Datensatz Formatfehler", Ergebnis.FT_NIO_SYS );
							else result = new Ergebnis( "X-dataset display", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "", "X-Dataset format error", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						}
						i = i + 13;	//Skip <LOCKINGCODE>
						j = str.indexOf("<",i);
						if (j == -1)
						{
							//Format-Fehler im Datensatz: kein <lockingCode>
							if (debug) System.out.println("DokuMechcodeBarcode: X-Dataset Format error: no ende of <lockingCode> ");
							if (isDE()) result = new Ergebnis( "X-Datensatz Anzeigen", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "", "X-Datensatz Formatfehler", Ergebnis.FT_NIO_SYS );
							else result = new Ergebnis( "X-dataset display", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "", "X-Dataset format error", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						}
						//Mechcode inkl. PZ ausschneiden
						mechcode = str.substring(i,j);
						if (debug) System.out.println("DokuMechcodeBarcode: mechcode:" + mechcode);
						
						//Dialog Anzeigen
						dlu = getPr�flingLaufzeitUmgebung().getUserDialogNext();
						handle = dlu.allocateUserDialogNext();

						//Ggf. Default-Bezeichung setzen
						if ((m_awt == null) || (m_awt.trim().length() == 0))
						{
							if (isDE())
							{
								m_awt = "Gleichschlie�ung Mechcode";
							}
							else
							{
								m_awt = "lockset cutting code";
							}
						}
						m_awt = m_awt + ":\n" + mechcode;
						if (isDE()) result = new Ergebnis( "X-Datensatz Anzeigen", "DokuMechcodeBarcode", "", "", "", "MECHCODE", "" + mechcode, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						else result = new Ergebnis( "X-dataset display", "DokuMechcodeBarcode", "", "", "", "MECHCODE", "" + mechcode, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );

					}
					//Diaglog anzeigen
					if (isDE()) header_text = "Mechcode"; 
					else header_text = "cutting code";
					if( handle != null ) {
						dlu.displayMessage( handle, style, header_text, m_awt, 0, UDNButtonConfiguration.OK_BUTTON );
						dlu.releaseUserDialogNext( handle ); //!!!
					} else
						throw new DeviceNotAvailableException();

				}
				catch( Exception e )
				{
					if( e instanceof DeviceLockedException )
						result = new Ergebnis( "ExecFehler", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
					else if( e instanceof DeviceNotAvailableException )
						result = new Ergebnis( "ExecFehler", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
					else if( e instanceof DomException )
						result = new Ergebnis( "ExecFehler", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "DomException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "ExecFehler", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}
			}
			else
			{
				throw new PPExecutionException("MODE=UNKOWN not supported");
			}

		}
		catch( PPExecutionException e )
		{
			String resultText = "Execution Exception";	
			String executionExceptionText = e.getLocalizedMessage();
			if(executionExceptionText!=null && !executionExceptionText.isEmpty())
				resultText+=": " + executionExceptionText;
				
			result = new Ergebnis( "ExecFehler", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "Exception", resultText, Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}
		catch( Exception e )
		{
			result = new Ergebnis( "ExecFehler", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}
		catch( Throwable e )
		{
			result = new Ergebnis( "ExecFehler", "DokuMechcodeBarcode", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		//Status setzen
		setPPStatus( info, status, ergListe );
	}

    /**
     * Methode zur Berechnung der Pr�fziffer des mechanischen Schlie�codes.
     *
     * @param mechCode Der mechanische Schlie�code, f�r welchen die Pr�fziffer
     *        zu berechnen ist (L�nge 9). z.B. HA0001234
     * @param debug Schaltet Debug-Ausgaben ein
     * @return Die berechnete Pr�fziffer als char.
     * @throws CError Fehler, falls MechCode nicht die L�nge 9 hat
     */
    public static char calculateMechCodeChecksum(String mechCode, boolean debug) throws PPExecutionException
    {
        int sum=0;
        
        if (mechCode.length() != 9) 
        {
        	if (debug) System.out.println("DokuMechcodeBarcode: Invalid length calculateMechCodeChecksum: " + mechCode);
            throw new PPExecutionException("calculateMechCodeChecksum: mechcode \""+ mechCode + "\"has invalid length!");
        }
        
        try 
        {
            sum=sum+quersum(2, mechCode.charAt(2), mechCode, debug);
            sum=sum+quersum(1, mechCode.charAt(3), mechCode, debug);
            sum=sum+quersum(2, mechCode.charAt(4), mechCode, debug);
            sum=sum+quersum(1, mechCode.charAt(5), mechCode, debug);
            sum=sum+quersum(2, mechCode.charAt(6), mechCode, debug);
            sum=sum+quersum(1, mechCode.charAt(7), mechCode, debug);
            sum=sum+quersum(2, mechCode.charAt(8), mechCode, debug);
            sum=100-sum;
        } 
        catch (PPExecutionException p) 
        {
     	   if (debug) System.out.println("DokuMechcodeBarcode: Exception in calculateMechCodeChecksum: " + p.getMessage());
           throw p;
        }
        return (Integer.toString(sum % 10).charAt(0));
        
    }

    
    /**
     * Methode, welche bei der Berechnung der Pr�fziffer f�r den mechanischen
     * Schlie�code herangezogen wird.
     * 
     * @param factor Multiplikator f�r Zahl
     * @param zeichen Zeichen 0-9,A-Z, wird umgewandelt in Zahl zwischen 0-35
     * @param mechCode Fehlercode (nur f�r Exception)
     * @param debug Schaltet Debug-Ausgaben ein
     * @return Quersumme
     * @throws CError
     */
    public static int quersum(int factor, char zeichen, String mechCode, boolean debug) throws PPExecutionException
    {
        int zahl, zehner, einer;
        
        try 
        {
            zahl = Integer.parseInt((""+zeichen).toUpperCase(), 36); //$NON-NLS-1$
        } 
        catch (NumberFormatException x) 
        {
        	if (debug) System.out.println("DokuMechcodeBarcode: Exception in quersum: " + x.getMessage());
            throw new PPExecutionException("calculateMechCodeChecksum: mechcode has invalid character +\"" + zeichen + "\"");
        }
        
        zahl = zahl * factor;
        
        zehner = zahl / 10;
        einer = zahl % 10;
        
        return zehner + einer;
    }

    /** Berechnet die 2 Pr�fziffern zu einem Barcode (RFID Ersatz) im Format
     * 
     *  HF00012345-123456789AJ
     * 
     * @param barcode Barcode OHNE die 2 Pr�fziffern, also z.B.: HF00012345-123456789
     * @return Berechnete Pr�fziffern, hier AJ
     */
    static public String calcBarcodeChecksum(String barcode)
    {
        int i;
        byte[] array=new byte[barcode.length()];
        for (i=0; i<barcode.length(); i++)
        {
            array[i]=(byte)barcode.charAt(i);
        }
        int pz=crc16(array);
    
        int c1=(pz/256)%256;
        int c2=pz%256;
        c1=c1%26;
        c2=c2%26;
        String erg="";
        erg+=(char)('A'+c1);
        erg+=(char)('A'+c2);
        return erg;
    }




    /** CRC 16 Algorithm
     * 
     * @param array any byte arry
     * @return CRC16 Value
     */
    static public int crc16(byte[] array) 
    {

    int[] table = 
    {
        0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
        0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
        0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
        0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
        0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
        0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
        0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
        0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
        0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
        0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
        0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
        0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
        0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
        0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
        0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
        0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
        0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
        0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
        0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
        0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
        0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
        0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
        0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
        0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
        0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
        0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
        0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
        0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
        0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
        0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
        0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
        0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040,
    };

        int crc = 0x0000;
        for (byte b : array) 
        {
            crc = (crc >>> 8) ^ table[(crc ^ b) & 0xff];
        }

        return crc;

    }

	
   /** Ermittelt, ob die Sprache in Cascade deutsch oder englisch ist
    * 
    * @return true: Deutsch, false: nicht Deutsch (-> Englisch)
    */
   private static boolean isDE() 
   {
       try 
       {
           if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true ) return true;
           else return false;
       }
       catch (Exception e) 
       {
           return false;   //default is english
       }
   }
   
   
   /** Holt X-Datensatz vom CASCADE Server
    * 
    * @param debug Schaltet Debug-Ausgaben ein
    * @return Eingelesener X-Datensatz
    * @throws Exception Fehler beim Laden
    */
   private StringBuffer readFromCascadeServerOrPrufstand(boolean debug) throws Exception
   {
	   StringBuffer buff = null;
	   
	   //Zuerst Quelle lokal
	   buff = readFromCascadePruefstand(debug);
	   
	   if (buff == null)
	   {
		   //Quelle Server direkt
		   buff = readFromCascadeServer(debug);
	   }
	   return buff;
   }

	   /** Holt X-Datensatz vom CASCADE Server
    * 
    * @param debug Schaltet Debug-Ausgaben ein
    * @return Eingelesener X-Datensatz
    * @throws Exception Fehler beim Laden
    */
   private StringBuffer readFromCascadeServer(boolean debug) throws Exception
   {
       byte rawData[];

       try 
       {
           //call the rmi-method
           rawData= DomPruefstand.readDomData(getPr�fling().getAuftrag().getFahrgestellnummer7(), "X");
           if ((rawData != null) && (rawData.length > 1)) 
           {
               String strData = new String(rawData);
               if ((strData != null) && (strData.length() > 1)) 
               {
                   return new StringBuffer(strData.trim());
               }
           }
       } 
       catch(Exception e) 
       {
    	   if (debug) System.out.println("DokuMechcodeBarcode: Exception in readFromCascadeServer: " + e.getMessage());
           throw e;
       }
	   if (debug) System.out.println("DokuMechcodeBarcode: Exception in readFromCascadeServer: Dataset is empty");
       throw new Exception("Dataset is empty");
   }

   /** Liest X-Datensatz aus lokalem Ausgangsverzeichnis
    * 
    * @param debug Schaltet Debug-Ausgaben ein
    * @return eingelesener X-Datensatz
    * @throws Exception Fehler beim Laden
    */
   private StringBuffer readFromCascadePruefstand(boolean debug) throws Exception
   {  
	   File localDir;
	   File localFile;
	   FileReader in = null;
	   StringBuffer buffer=new StringBuffer();
       
       localDir = new File(DomPruefstand.PRUEFSTAND_DIRECTORY+File.separator);
       if (localDir.exists()) 
       {
           localFile = new File(localDir, getPr�fling().getAuftrag().getFahrgestellnummer7()+"X.xml");
           if (localFile.exists() && localFile.canRead()) 
           {
               //datasetfile exists so try to read it
               in=null;
               try
               {
                   in = new FileReader(localFile);
                   int size = (int)localFile.length();
                   if (size<=1)
                   {
                       throw new Exception("Dataset is empty");
                   }
                   char[] cbuff = new char[size];
                   in.read(cbuff);
                   buffer.append(cbuff);
                   return buffer;
               }
               catch (Exception err)
               {
            	   if (debug) System.out.println("DokuMechcodeBarcode: Exception in readFromCascadePruefstand: " + err.getMessage());
            	   throw err;
               }
               finally
               {
                   if (in!=null)
                   {
                       try
                       {
                            in.close();
                       }
                       catch (IOException e) { }
                   }
               }
           }
           else
           {
        	   //no file or not readable
               return null;
           }
       }
       else
       {
    	   //no local directory
           return null;
       }
   }
   
   /** Schreibt Datensatz auf den Cascade-Server (am Ende des PUs bei IO)
    * 
    * @param debug Schaltet Debug-Ausgaben ein
    * @param Suffix ndebuchstabe f�r Datensatz, z.B. "C" bei CAS-Datensatz, "E" bei EWS4-Sync-Datensatz
    * @param Data Datensatz
    * @throws Exception Fehler beim Schreiben
    */
   public void writeDatasetToCascade(StringBuffer Data, boolean debug) throws Exception
   {
       try 
       {
           DomPruefstand.writeDomData(getPr�fling().getAuftrag().getFahrgestellnummer7(), "X", Data.toString().getBytes());
       } 
       catch(Exception e) 
       {
    	   if (debug) System.out.println("DokuMechcodeBarcode: Exception in writeDatasetToCascade: " + e.getMessage());
           throw e;
       }
   }
   
   
   /** Generiert CreationTime f�r xml Datensatz
    * 
    * @return CreationTime
    */
   private StringBuffer getXMLCreationTime()
   {
	   Calendar now = Calendar.getInstance();
	   
	   int prog_Year = now.get(Calendar.YEAR);
       int prog_Month = now.get(Calendar.MONTH)+1;
       int prog_Day = now.get(Calendar.DAY_OF_MONTH);    
       
       String prog_Time = addZero(now.get(Calendar.HOUR_OF_DAY), 2);
       prog_Time+=":"; //$NON-NLS-1$
       prog_Time+= addZero(now.get(Calendar.MINUTE), 2);
       prog_Time+=":"; //$NON-NLS-1$
       prog_Time+= addZero(now.get(Calendar.SECOND), 2);      
       
       if (prog_Year<1000) prog_Year+=2000;     
       
       StringBuffer xml=new StringBuffer();
       xml.append(prog_Year);
       xml.append("-"); //$NON-NLS-1$
       xml.append(addZero(prog_Month, 2));
       xml.append("-"); //$NON-NLS-1$
       xml.append(addZero(prog_Day, 2));
       xml.append("T"); //$NON-NLS-1$
       xml.append(prog_Time);
       return xml;
   }
   
   /** Wandelt einen int in einen String und f�gt vorne so viele 0 hinzu, bis der String die L�nge 'Len' hat
    * 
    * @param Val Integer
    * @param Len L�nge des Strings
    * @return Val als Integer ggf. mit f�hrenden Nullen
    */
   private String addZero(int Val, int Len)
   {
       String erg=Integer.toString(Val);
       while (erg.length()<Len) erg="0"+erg; //$NON-NLS-1$
       return erg;
   }

}

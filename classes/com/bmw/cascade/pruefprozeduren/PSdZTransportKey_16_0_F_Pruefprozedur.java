package com.bmw.cascade.pruefprozeduren;

import java.util.Map;
import java.util.Vector;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.psdz.PSdZ;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeMSMResult;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeReturnCodeType;
import com.bmw.cascade.pruefstand.devices.psdz.data.msm.CascadeF25SignalKeyVerificationResult;
import com.bmw.cascade.pruefstand.devices.psdz.data.msm.CascadeMSMF310Result;
import com.bmw.cascade.pruefstand.devices.psdz.data.msm.CascadeMSMFunctionResult;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementierung einer Pr�fprozedur, die Transportschl�ssel im Fahrzeug einbringt und verteilt
 * 
 * @author Mueller, BMW AG; Buboltz, BMW AG; Busetti, GEFASOFT AG; Gampl, BMW AG <BR>
 *         16.05.2007 TB festes Wartezeiten von 100ms <BR>
 *         03.01.2008 DB Einbau "testScreenID" in Debug-Ausgaben <BR>
 *         14.04.2008 JS Version 5. Ereignisdefinition fuer TSL Update.<BR>
 *         24.06.2008 DB �nderung holen des "testScreenID" <BR>
 *         02.04.2013 TB Einbau 35up MSM Erweiterungen (F50, F25 Funktionen hinzu und allg. �berarbeitungen, optionale Parameter erweitert zwecks Durchg�ngigkeit)<BR>
 *         10.04.2013 TB Die F30 Funktion (DeployKey) f�hrt auch zus�tzlich noch die F40 mit aus + entsprechende Anpassungen und Vereinfachungen <BR>
 *         15.04.2013 TB UserDialog -1 beim Parameter und setDynamicFontSize auf false <BR>
 *         16.04.2013 TB Vorbelegung bMsmF50DeploySignalKey=false, bMsmF25SystemCheck=false ge�ndert. MSM Versionsabfrage noch eingebaut, bei der Funktion "MSM_F310_STORE_KEY". Ansonsten gibt es bei den CSM's Fehler, falls die MSM Version < 11 ist.<BR>
 *         20.04.2013 TB F-Version <BR>
 *         09.10.2014 MG T-Version (9_0) <BR>
 *         ---------- -- - Codebereinigung (Ausbau alter Methoden: getPSdZExceptions(), clearPSdZExceptions()); neue CASCADE Mindestversion 6.1.0 <BR>
 *         20.10.2014 MG F-Version (10_0), Mindestversionsanforderung CASCADE 6.1.0 / PSdZ 4.9.1 <BR>
 *         26.01.2015 MG T-Version (11_0) <BR>
 *         ---------- -- - Alte MSM-Parameter (STORE_KEY, CHECK_STORAGE, DEPLOY_KEY, CHECK_DEPLOYMENT und UPDATE_KEY) entfernt <BR>
 *         ---------- -- - LOP 1853: Ausgabe (in Log-Datei Pr�fstandScreen) der Pr�fprozedurversion sowie s�mtlicher optionaler und nicht optionaler Argumente, die an die Pr�fprozedur �bergeben werden zum Zweck der Nachvollziehbarkeit im Fehlerfall. <BR>
 *         ---------- -- - LOP 1888: Lokalisierte Ausgabe der Fehlerursachen bei TSL-Fehlern erfolgt nun f�r s�mtliche MSM-Methodenaufrufe <BR>
 *         04.02.2015 MG F-Version (12_0) <BR>
 *         10.02.2015 MG T-Version (13_0) <BR>
 *         ---------- -- - LOP 1899: Ausgabe s�mtlicher Fehlermeldungen �berarbeitet und auf einheitliche Darstellung in Fehlerprotokoll/APDM sowie durchg�ngig deutsch- und englischsprachige �bersetzung umgestellt. <BR>
 *         18.02.2015 MG F-Version (14_0) <BR>
 *         25.03.2015 MG T-Version (15_0) <BR>
 *         ---------- -- - LOP 1924: Fehlende FsCSMs (pre35up und 35up), die im Rahmen der F310-Pr�fung innerhalb der Trasportschl�sselliste erkannt wurden, sollen immer in Fehlerprotokoll ausgegeben werden, nicht nur bei einer MSM-Version = 11 <BR>
 *         ---------- -- - Bugfix: Funktion F50: FsCSM-Fehler werden auch dann ausgegeben, wenn der MSM-Returncode = OK ist <BR>
 *         ---------- -- - Bugfix: Funktion F25: MSM-Fehler werden nur dann ausgegeben, wenn der MSM-Returncode != OK ist <BR>
 *         27.08.2015 MG F-Version (16_0), Mindestversionsanforderung CASCADE 6.1.0 / PSdZ 4.9.1 <BR>
 * 
 */
public class PSdZTransportKey_16_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSdZTransportKey_16_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSdZTransportKey_16_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG", "DEBUG_PERFORM", "MSM_BATCH_MODE", "MSM_F25_SYSTEM_CHECK", "MSM_F30_AND_F40_DEPLOY_AND_CHECK", "MSM_F310_CHECK_STORE", "MSM_F310_STORE_KEY", "MSM_F50_DEPLOY_SIGNAL_KEY", "TIMEOUT" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = {};
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		if( !super.checkArgs() )
			return false;
		return true;
	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; // default english
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {

		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
		boolean bDebugPerform = true; // gibt an, ob eine Performencemessung erfolgen sollen
		boolean bMsmBatchMode = false; // MSM_BATCH_MODE 
		boolean bMsmF25SystemCheck = false; // MSM_F25_SYSTEM_CHECK
		boolean bMsmF30DeployKeyAlsoF40InitFingerprint = true; // MSM_F30_AND_F40_DEPLOY_AND_CHECK
		boolean bMsmF310CheckStore = true; // MSM_F310_CHECK_STORE
		boolean bMsmF310StoreKey = true; // MSM_F310_STORE_KEY
		boolean bMsmF50DeploySignalKey = false; // MSM_F50_DEPLOY_SIGNAL_KEY
		final boolean DE = checkDE(); // Systemsprache DE wenn true
		int timeout = 0;
		int status = STATUS_EXECUTION_OK;

		Ergebnis result;
		PSdZ psdz = null;
		String errorCode = DE ? "PSdZ Fehler Code: " : "PSdZ ERROR Code: ";
		String errorDescription = DE ? " Fehlerbeschreibung: " : " Error Description: ";
		String errorText = null;
		String optionalArgumentName = null;
		String optionalArgumentValue = null;
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";
		String testScreenID = this.getPr�flingLaufzeitUmgebung().getName(); // TestScreenID (aus Pr�fstandsinstanz)
		String[] optionalArgumentsNames = getOptionalArgs();
		String[] optionalArgumentsValues = null;
		UserDialog userDialog = null;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();

		try {
			// Argumente loggen, einlesen und checken
			try {

				// Ausgabe der Pr�fprozedurversion sowie s�mtlicher an die Pr�fprozedur �bergebener Argumente (required/optional) f�r sp�tere Analysezwecke
				System.out.println( "PSdZ ID:" + testScreenID + ", Initialize " + this.getClassName() );

				// aktuell keine zwingenden Argumente parametrierbar, deshalb nur Ausgabe optionaler Argumente
				for( int i = 0; i < optionalArgumentsNames.length; i++ ) {
					optionalArgumentName = optionalArgumentsNames[i];
					optionalArgumentValue = "";
					// gibt es optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( optionalArgumentName ) != null ) {
						optionalArgumentsValues = extractValues( getArg( optionalArgumentName ) );
						for( int k = 0; k < optionalArgumentsValues.length; k++ ) {
							// Listen von optionalen Argumenten werden durch ";" getrennt ausgegeben
							optionalArgumentValue = optionalArgumentValue + optionalArgumentsValues[k] + ((optionalArgumentsValues.length > 1 && k < (optionalArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						optionalArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZTransportKey with optional argument: " + optionalArgumentName + " = " + optionalArgumentValue );
				}

				// Obligatorische Argumentpr�fung auf Vorhandensein von nicht optionalen sowie �berz�hligen Argumenten
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
					else
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}
				// DEBUG_PERFORM
				if( getArg( "DEBUG_PERFORM" ) != null ) {
					if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "TRUE" ) )
						bDebugPerform = true;
					else if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "FALSE" ) )
						bDebugPerform = false;
					else
						throw new PPExecutionException( "DEBUG_PERFORM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// MSM_BATCH_MODE
				if( getArg( "MSM_BATCH_MODE" ) != null ) {
					if( getArg( "MSM_BATCH_MODE" ).equalsIgnoreCase( "TRUE" ) )
						bMsmBatchMode = true;
					else if( getArg( "MSM_BATCH_MODE" ).equalsIgnoreCase( "FALSE" ) )
						bMsmBatchMode = false;
					else
						throw new PPExecutionException( "MSM_BATCH_MODE " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// MSM_F25_SYSTEM_CHECK
				if( getArg( "MSM_F25_SYSTEM_CHECK" ) != null ) {
					if( getArg( "MSM_F25_SYSTEM_CHECK" ).equalsIgnoreCase( "TRUE" ) )
						bMsmF25SystemCheck = true;
					else if( getArg( "MSM_F25_SYSTEM_CHECK" ).equalsIgnoreCase( "FALSE" ) )
						bMsmF25SystemCheck = false;
					else
						throw new PPExecutionException( "MSM_F25_SYSTEM_CHECK " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// MSM_F30_AND_F40_DEPLOY_AND_CHECK
				if( getArg( "MSM_F30_AND_F40_DEPLOY_AND_CHECK" ) != null ) {
					if( getArg( "MSM_F30_AND_F40_DEPLOY_AND_CHECK" ).equalsIgnoreCase( "TRUE" ) )
						bMsmF30DeployKeyAlsoF40InitFingerprint = true;
					else if( getArg( "MSM_F30_AND_F40_DEPLOY_AND_CHECK" ).equalsIgnoreCase( "FALSE" ) )
						bMsmF30DeployKeyAlsoF40InitFingerprint = false;
					else
						throw new PPExecutionException( "MSM_F30_AND_F40_DEPLOY_AND_CHECK " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// MSM_F310_CHECK_STORE
				if( getArg( "MSM_F310_CHECK_STORE" ) != null ) {
					if( getArg( "MSM_F310_CHECK_STORE" ).equalsIgnoreCase( "TRUE" ) )
						bMsmF310CheckStore = true;
					else if( getArg( "MSM_F310_CHECK_STORE" ).equalsIgnoreCase( "FALSE" ) )
						bMsmF310CheckStore = false;
					else
						throw new PPExecutionException( "MSM_F310_CHECK_STORE " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// MSM_F310_STORE_KEY
				if( getArg( "MSM_F310_STORE_KEY" ) != null ) {
					if( getArg( "MSM_F310_STORE_KEY" ).equalsIgnoreCase( "TRUE" ) )
						bMsmF310StoreKey = true;
					else if( getArg( "MSM_F310_STORE_KEY" ).equalsIgnoreCase( "FALSE" ) )
						bMsmF310StoreKey = false;
					else
						throw new PPExecutionException( "MSM_F310_STORE_KEY " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// MSM_F50_DEPLOY_SIGNAL_KEY
				if( getArg( "MSM_F50_DEPLOY_SIGNAL_KEY" ) != null ) {
					if( getArg( "MSM_F50_DEPLOY_SIGNAL_KEY" ).equalsIgnoreCase( "TRUE" ) )
						bMsmF50DeploySignalKey = true;
					else if( getArg( "MSM_F50_DEPLOY_SIGNAL_KEY" ).equalsIgnoreCase( "FALSE" ) )
						bMsmF50DeploySignalKey = false;
					else
						throw new PPExecutionException( "MSM_F50_DEPLOY_SIGNAL_KEY " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// TIMEOUT
				if( getArg( "TIMEOUT" ) != null ) {
					try {
						timeout = Integer.parseInt( getArg( "TIMEOUT" ) );
					} catch( Exception e ) {
						throw new PPExecutionException( PB.getString( "psdz.parameter.ParameterfehlerTimeout" ) );
					}
				}

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			// Performancemessung Start
			if( bDebugPerform )
				System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZTransportKey PP START" );

			// Devices holen
			try {
				psdz = getPr�flingLaufzeitUmgebung().getDeviceManager().getPSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			try {
				userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Userdialog einblenden
			userDialog.setDisplayProgress( true );
			userDialog.setAllowCancel( true );
			userDialog.getProgressbar().setIndeterminate( true );
			userDialog.setRows( 8 );
			userDialog.setColumns( 28 );

			userDialog.displayMessage( "PSdZ Transport Key", "MSM_F310_STORE_KEY: " + bMsmF310StoreKey + ";MSM_F310_CHECK_STORE: " + bMsmF310CheckStore + ";MSM_F30_AND_F40_DEPLOY_AND_CHECK: " + bMsmF30DeployKeyAlsoF40InitFingerprint + "; MSM_F50_DEPLOY_SIGNAL_KEY: " + bMsmF50DeploySignalKey + "; MSM_F25_SYSTEM_CHECK: " + bMsmF25SystemCheck + "; MSM_BATCH_MODE: " + bMsmBatchMode + ";;" + PB.getString( "psdz.ud.TransportKeyActionLaeuft" ), -1 );

			// MSM F310 generiere und speichere die TSLs (Argument: MSM_F310_STORE_KEY) // TSL einbringen
			if( bMsmF310StoreKey ) {
				try {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PsdZTransportKey 'psdz.msm_F310_generateAndStoreTSL' START" );
					}

					// jetzt die eigentliche F310_generateAndStoreTSL ausf�hren
					CascadeMSMF310Result msmF310Result = psdz.msm_F310_generateAndStoreTSL();

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PsdZTransportKey 'psdz.msm_F310_generateAndStoreTSL' ENDE" );
					}

					// Zus�tzliche �berpr�fung (W. Mark Anforderung)
					// Pr�fe ob PSdZ eine TSL geschrieben hat, in der FsCSMs fehlen, die laut SVT-Soll eigentlich h�tten vorhanden sein sollen?
					// Schreiben einer TSL hat prinzipiell funktioniert (aus PSdZ Sicht), jedoch soll dies trotzdem als Fehler gewertet werden
					if( !msmF310Result.getFsCSMMissingFromTSL().isEmpty() ) {

						// SG Einzelfehler ausgeben
						errorText = DE ? "FsCSM Steuerger�t aus der SVT-Soll fehlt in der Transportschl�sselliste! Steuerger�t hat nicht geantwortet oder ODX-Daten fehlerhaft!" : "FsCSM ECU module from the SVT target list is not part of the TSL list! ECU did not respond or ODX data not correct!";
						for( Integer diagAddrs : msmF310Result.getFsCSMMissingFromTSL() ) {
							int ecu = diagAddrs.intValue();
							result = new Ergebnis( "PSdZTransportKey", "PSdZ", (DE ? "FsCSM Steuerger�t: " : "FsCSM ECU: ") + (ecu != -1 ? "0x" + Integer.toHexString( ecu ).toUpperCase() + " (dec: " + ecu + ")" : "- (dec: " + ecu + ")"), "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText, "", Ergebnis.FT_NIO );
							ergListe.add( result );
						}

						// allg. Fehler loggen
						errorText = DE ? "Nicht alle FsCSM Steuerger�te aus der SVT-Soll sind in der TSL enthalten!" : "Missing FsCSM ECU's inside the TSL list (Based on the SVT target list)!";
						result = new Ergebnis( "PSdZTransportKey", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.FehlerBeiEinbringenDerTransportschluesselliste" ) + " - MSM_F310_STORE_KEY! " + errorText, "", Ergebnis.FT_NIO );
						ergListe.add( result );

						// Fehler und fertig
						status = STATUS_EXECUTION_ERROR;
					}

				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					errorText = e.getLocalizedMessage();
					result = new Ergebnis( "PSdZTransportKey", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.FehlerBeiEinbringenDerTransportschluesselliste" ) + " - MSM_F310_STORE_KEY!" + (errorText != null ? errorDescription + errorText : ""), "", Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

			// MSM F310 Funktion pr�fe ob die TSLs gespeichert wurden (Argument: MSM_F310_CHECK_STORE) // Check, ob Schl�ssel korrekt im Fahrzeug eingebracht wurden // 
			if( bMsmF310CheckStore ) {
				try {
					long lStart = System.currentTimeMillis();
					long lEnd = lStart + (timeout * 1000);
					long lOpen = 0;

					do {
						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PsdZTransportKey 'psdz.msm_F310_checkStoreTSL()' START" );
						}

						CascadeMSMResult msmResult = psdz.msm_F310_checkStoreTSL();

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PsdZTransportKey 'psdz.msm_F310_checkStoreTSL()' ENDE" );
						}

						if( bDebug )
							System.out.println( "PSdZ ID:" + testScreenID + ", MSM-Result: " + msmResult.getMSMResultDescription() );

						// Fertig und IO?
						if( CascadeMSMResult.MSM_NO_ERROR.equalsIgnoreCase( msmResult.getMSMResultDescription() ) ) {
							break;
						}

						// Bei Fehlern melde dies NIO
						if( CascadeMSMResult.MSM_ERROR.equalsIgnoreCase( msmResult.getMSMResultDescription() ) ) {
							errorText = DE ? "MSM_F310_CHECK_STORE R�ckgabe-Code: " : "MSM_F310_CHECK_STORE Return Code: ";
							result = new Ergebnis( "PSdZTransportKey", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.FehlerBeiEinbringenDerTransportschluesselliste" ) + " - MSM_F310_CHECK_STORE! " + errorText + msmResult.getMSMResultDescription(), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
							break;
						}

						// Abbruch durch Werker?
						if( userDialog.isCancelled() == true ) {
							errorText = PB.getString( "werkerAbbruch" );
							result = new Ergebnis( "PSdZTransportKey", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.FehlerBeiEinbringenDerTransportschluesselliste" ) + " - MSM_F310_CHECK_STORE! " + errorText, "", Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
							break;
						}

						// Ende erreicht?
						lOpen = lEnd - System.currentTimeMillis();
						if( lOpen <= 0 ) {
							errorText = DE ? "Pr�fschritt Abbruch wegen Zeit�berschreitung (TIMEOUT). MSM_F310_CHECK_STORE R�ckgabe-Code: " : "Test aborted due to TIMEOUT. MSM_F310_CHECK_STORE Return Code: ";
							result = new Ergebnis( "PSdZTransportKey", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.FehlerBeiEinbringenDerTransportschluesselliste" ) + " - MSM_F310_CHECK_STORE! " + errorText + msmResult.getMSMResultDescription(), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
							break;
						}

						// wir warten...
						try {
							Thread.sleep( 100 );
						} catch( InterruptedException e ) {
						}
					} while( lOpen > 0 );

				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					errorText = e.getLocalizedMessage();
					result = new Ergebnis( "PSdZTransportKey", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "psdz.error.FehlerBeiEinbringenDerTransportschluesselliste" ) + " - MSM_F310_CHECK_STORE!" + (errorText != null ? errorDescription + errorText : ""), "", Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

			// MSM F30 + F40 Funktion, verteile die Keys und initialisiere Fingerprint (Argument: MSM_F30_AND_F40_DEPLOY_AND_CHECK) // Verteilung anstossen und Verteilung pr�fen
			if( bMsmF30DeployKeyAlsoF40InitFingerprint ) {
				try {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PsdZTransportKey 'psdz.msm_F30_distributeCommunicationKey_Also_F40_initializeFingerprint' START" );
					}

					psdz.msm_F30_distributeCommunicationKey_Also_F40_initializeFingerprint();

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PsdZTransportKey 'psdz.msm_F30_distributeCommunicationKey_Also_F40_initializeFingerprint' ENDE" );
					}
				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					errorText = e.getLocalizedMessage();
					result = new Ergebnis( "PSdZTransportKey", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler bei der Verteilung der Transportschl�ssel" : "Error during distribution of transport keys") + " - MSM_F30_AND_F40_DEPLOY_AND_CHECK!" + (errorText != null ? errorDescription + errorText : ""), "", Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

			// MSM F50 Initialisierung sichere Bordnetzsignale (Argument: MSM_F50_DEPLOY_SIGNAL_KEY)
			if( bMsmF50DeploySignalKey ) {
				try {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PsdZTransportKey 'psdz.msm_F50_distributeVehicleKey' START" );
					}

					CascadeMSMFunctionResult msmFunctionResult = psdz.msm_F50_distributeVehicleKey();

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PsdZTransportKey 'psdz.msm_F50_distributeVehicleKey' ENDE" );
					}

					// sind Fehler aufgetreten?

					// SG Einzelfehler (-> FsCSM) ausgeben
					if( !msmFunctionResult.getFsCsmErrorMessages().isEmpty() ) {
						for( Map.Entry<Integer, String> fsCsmErrorsEntry : msmFunctionResult.getFsCsmErrorMessages().entrySet() ) {
							int ecu = fsCsmErrorsEntry.getKey().intValue();
							result = new Ergebnis( "PSdZTransportKey", "PSdZ", (DE ? "FsCSM Steuerger�t: " : "FsCSM ECU: ") + (ecu != -1 ? "0x" + Integer.toHexString( ecu ).toUpperCase() + " (dec: " + ecu + ")" : "- (dec: " + ecu + ")"), "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", fsCsmErrorsEntry.getValue(), "", Ergebnis.FT_NIO );
							ergListe.add( result );
						}
						status = STATUS_EXECUTION_ERROR;
					}

					// allg. (MSM) Fehler loggen, jedoch nur wenn Fehler vorliegt
					if( !msmFunctionResult.getMSMReturnValue().getReturnCode().equals( CascadeReturnCodeType.OK ) ) {
						errorText = DE ? "MSM_F50_DEPLOY_SIGNAL_KEY R�ckgabe-Code: " : "MSM_F50_DEPLOY_SIGNAL_KEY Return Code: ";
						result = new Ergebnis( "PSdZTransportKey", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler bei der Verteilung des Signalschl�ssels" : "Error during signal key deployment") + " - MSM_F50_DEPLOY_SIGNAL_KEY! " + errorText + msmFunctionResult.getMSMReturnValue().getReturnCode().toString() + "," + errorDescription + msmFunctionResult.getMSMReturnValue().getLocalizedMessage() + ", (" + errorCode + msmFunctionResult.getMSMReturnValue().getMessageId() + ")", "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}

				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					errorText = e.getLocalizedMessage();
					result = new Ergebnis( "PSdZTransportKey", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler bei der Verteilung des Signalschl�ssels" : "Error during signal key deployment") + " - MSM_F50_DEPLOY_SIGNAL_KEY!" + (errorText != null ? errorDescription + errorText : ""), "", Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

			// MSM F25 F�hrt einen abschlie�enden Verbaucheck bzw. eine System�berpr�fung durch (Argument: MSM_F25_SYSTEM_CHECK)  
			if( bMsmF25SystemCheck ) {
				try {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PsdZTransportKey 'psdz.msm_F25_checkMsmSystemInitialisation' START" );
					}

					CascadeF25SignalKeyVerificationResult msmVerificationResult = psdz.msm_F25_checkMsmSystemInitialisation();

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PsdZTransportKey 'psdz.msm_F25_checkMsmSystemInitialisation' ENDE" );
					}

					// sind Fehler aufgetreten?

					// SG Einzelfehler (-> FsCSM) ausgeben
					if( !msmVerificationResult.getFsCsmErrorMessages().isEmpty() ) {
						for( Map.Entry<Integer, String> fsCsmErrorsEntry : msmVerificationResult.getFsCsmErrorMessages().entrySet() ) {
							int ecu = fsCsmErrorsEntry.getKey().intValue();
							result = new Ergebnis( "PSdZTransportKey", "PSdZ", (DE ? "FsCSM Steuerger�t: " : "FsCSM ECU: ") + (ecu != -1 ? "0x" + Integer.toHexString( ecu ).toUpperCase() + " (dec: " + ecu + ")" : "- (dec: " + ecu + ")"), "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", fsCsmErrorsEntry.getValue(), "", Ergebnis.FT_NIO );
							ergListe.add( result );
						}
						status = STATUS_EXECUTION_ERROR;
					}

					// allg. (MSM) Fehler loggen, jedoch nur wenn Fehler vorliegt
					if( !msmVerificationResult.getMSMReturnValue().getReturnCode().equals( CascadeReturnCodeType.OK ) ) {
						errorText = DE ? "MSM_F25_SYSTEM_CHECK R�ckgabe-Code: " : "MSM_F25_SYSTEM_CHECK Return Code: ";
						result = new Ergebnis( "PSdZTransportKey", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler bei der MSM Systeminitialisierung" : "Error during MSM system initialization") + " - MSM_F25_SYSTEM_CHECK! " + errorText + msmVerificationResult.getMSMReturnValue().getReturnCode().toString() + "," + errorDescription + msmVerificationResult.getMSMReturnValue().getLocalizedMessage() + ", (" + errorCode + msmVerificationResult.getMSMReturnValue().getMessageId() + ")", "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}

					// Zus�tzliche �berpr�fung (W. Mark Anforderung)
					// Pr�fe ob PSdZ eine TSL geschrieben hat, in der FsCSMs fehlen, die laut SVT-Soll eigentlich h�tten vorhanden sein sollen?
					// Schreiben einer TSL hat prinzipiell funktioniert (aus PSdZ Sicht), jedoch soll dies trotzdem als Fehler gewertet werden
					if( !msmVerificationResult.getFsCSMMissingFromTSL().isEmpty() ) {

						// SG Einzelfehler (-> FsCSM) ausgeben
						errorText = DE ? "FsCSM Steuerger�t aus der SVT-Soll fehlt in der Transportschl�sselliste!" : "FsCSM ECU module from the SVT target list is not part of the TSL list!";
						for( Integer diagAddrs : msmVerificationResult.getFsCSMMissingFromTSL() ) {
							int ecu = diagAddrs.intValue();
							result = new Ergebnis( "PSdZTransportKey", "PSdZ", (DE ? "FsCSM Steuerger�t: " : "FsCSM ECU: ") + (ecu != -1 ? "0x" + Integer.toHexString( ecu ).toUpperCase() + " (dec: " + ecu + ")" : "- (dec: " + ecu + ")"), "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText, "", Ergebnis.FT_NIO );
							ergListe.add( result );
						}

						// allg. Fehler loggen					
						errorText = DE ? "Nicht alle FsCSM Steuerger�te aus der SVT-Soll sind in der TSL enthalten!" : "Missing FsCSM ECU's inside the TSL list (based on the SVT target list)!";
						result = new Ergebnis( "PSdZTransportKey", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler bei der MSM Systeminitialisierung" : "Error during MSM system initialization") + " - MSM_F25_SYSTEM_CHECK! " + errorText, "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}

				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					errorText = e.getLocalizedMessage();
					result = new Ergebnis( "PSdZTransportKey", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler bei der MSM Systeminitialisierung" : "Error during MSM system initialization") + " - MSM_F25_SYSTEM_CHECK!" + (errorText != null ? errorDescription + errorText : ""), "", Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

			// MSM Batchablauf Store TSL and Distribute TSL Keys (Argument: MSM_BATCH_MODE) // TSLUpdate komplett
			if( bMsmBatchMode ) {
				try {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PsdZTransportKey 'psdz.msm_batchUpdateGenerateAndStoreTSLAndDistributeKeys' START" );
					}

					// TSL Update als kompletter Batchablauf
					psdz.msm_batchUpdateGenerateAndStoreTSLAndDistributeKeys( false );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PsdZTransportKey 'psdz.msm_batchUpdateGenerateAndStoreTSLAndDistributeKeys' ENDE" );
					}

				} catch( Exception e ) {
					if( bDebug ) {
						e.printStackTrace();
					}
					errorText = e.getLocalizedMessage();
					result = new Ergebnis( "PSdZTransportKey", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler beim Update der kompletten Transportschl�sselliste " : "Update of complete transport key list failed ") + " - MSM_BATCH_MODE!" + (errorText != null ? errorDescription + errorText : ""), "", Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Freigabe der benutzten Devices
		if( psdz != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releasePSdZ();
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
		try {
			getPr�flingLaufzeitUmgebung().releaseUserDialog();
		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			if( e instanceof DeviceLockedException )
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "PSdZTransportKey", "PSdZ", "MSM_F310_STORE_KEY:=" + bMsmF310StoreKey + ", MSM_F310_CHECK_STORE:=" + bMsmF310CheckStore + ", MSM_F30_AND_F40_DEPLOY_AND_CHECK:=" + bMsmF30DeployKeyAlsoF40InitFingerprint + ", MSM_F50_DEPLOY_SIGNAL_KEY:=" + bMsmF50DeploySignalKey + ", MSM_F25_SYSTEM_CHECK:=" + bMsmF25SystemCheck + ", MSM_BATCH_MODE:=" + bMsmBatchMode + ", DEBUG:=" + bDebug + ", TIMEOUT:=" + timeout, "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}

		// Status setzen
		setPPStatus( info, status, ergListe );

		// Performancemessung Ende
		if( bDebugPerform )
			System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZTransportKey PP ENDE" );
	}
}

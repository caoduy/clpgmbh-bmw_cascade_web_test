package com.bmw.cascade.pruefprozeduren;


import java.io.*;
import java.util.*;
import java.lang.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.uianalyse.*;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyser.*;
import com.bmw.cascade.pruefstand.visualisierung.*;


/** UIToleranzShowLWMAngle_3_0_F_Pruefprozedur.java<BR>
 * <BR>
 * Created on 20.05.2003<BR>
 * <BR>
 * bei Aenderung Hr. Kraski TI-430 Tel.: 20727 informieren!<BR>
 * <BR>
 * Implementierung <BR>
 * Lenkwinkelmesswaage auslesen - Grobablauf:<BR>
 * 1. Anzeige der Wineklstellung der Lenkradmesswaage<BR>
 * 2. Abbruch durch den Benutzer<BR>
 * <BR>
 * @author Klaus Kraski, BMW AG, ZSI<BR>
 * @author Ulf Plath, TI-432
 *
 * @version 0_0_1 20.05.2003 KK Grundfunktionalitaet und FA Freigabe<BR>
 * @version 0_0_2 22.05.2003 KK einige Displayanzeigen ausblenden<BR>
 * @version 0_0_3 10.10.2008 UP Unterst�tzung f�r mehrere Kan�le hinzugef�gt (faktoren.txt)
 */
public class UIToleranzShowLWMAngle_3_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    	static final long serialVersionUID = 1L;
    	
    	// Name der Datei, die die Kalibrierwerte enth�lt
    	private static final String fileName = "faktoren.txt";
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public UIToleranzShowLWMAngle_3_0_F_Pruefprozedur() {}

  /**
     * erzeugt eine neue Pruefprozedur mit obigem Verhalten
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public UIToleranzShowLWMAngle_3_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    /**
     * initialisiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /**
     * liefert die optionalen Argumente
     * @return Stringvektor der optionalen Argumente
     */
    public String[] getOptionalArgs() {
//  optionale parameter
        String[] args = {};
        return args;
    }
    
    /**
     * liefert die zwingend erforderlichen Argumente
     * @return Stringvektor der ben�tigten Argumente
     */
    public String[] getRequiredArgs() {
//
//  RESULT: Messgr��e (Spannung U oder Stromst�rke A)
//  KANAL: Me�kanal (normalerweise 1)
        
//  zw. erforderliche para  0         1
        String[] args = {"RESULT", "KANAL"};
        return args;
    }
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
     * @return true, wenn die Ueberpruefung der zwingenden Argumente ok ist
     */
    public boolean checkArgs() {
        String strtemp;
        float minValue, maxValue;
        int duration;
        int IOChannel;
        boolean ok;
        String result_str;
        try {
            ok= super.checkArgs();
            if (ok == true) {
                //
                // Result in V oder A
                //
                strtemp=getArg(getRequiredArgs()[0]);
                if (strtemp != null) {
                    if (strtemp.startsWith("I") || strtemp.startsWith("U")) {
                        result_str=strtemp;
                    }
                    else {
                        return false;
                    }
                }
                else return false;
                //
                // check IO-channels
                // Eingabe von 1 bis 8 zul�ssig
                //
                strtemp=getArg(getRequiredArgs()[1]);
                if (strtemp != null) {
                    try {
                        IOChannel=Integer.parseInt(strtemp);
                        if (IOChannel < 1 || IOChannel > 8) return false;
                    } catch (NumberFormatException e) {
                        return false;
                    }
                }
            }
            return ok;
        } catch (Exception e) {
            return false;
        } catch (Throwable e) {
            return false;
        }
    }   // end of checkArgs
    
    /**
     * f�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        // spezifische Variablen
        UserDialog ud = null;           // f�r Instanz auf UserDialog - nur eine Instanz holen
        UIAnalyser analyzerUI=null;     // f�r Instanz auf UIAnalyzer
        int channel=1;
        
        String temp=null;               // tempor�re Variable
        String result_str=null;         // Zwischenspeichern V oder A
        String awt;                     // Message an Werker
        float aValue=0.0f;             // Umrechnungsfaktor a
        float bValue=0.0f;             // Umrechnungsfaktor b
        float istValue=0.0f;            // Messwert in [mV]
        float istWinkel=0.0f;           // umgerechneter Messwert in [Grad]

        float voltage0=0.0f;            // Spannungswert f�r 0 Grad
        float voltage1=0.0f;            // Spannungswert f�r 5 Grad
        float min_Spannung=0.0f;        // minimaler Spannunswert in einem Messzyklus
        float max_Spannung=0.0f;        // maximaler Spannunswert in einem Messzyklus
        
        long dauer=30000;                   // Messdauer (kommt vom Pr�fling)
        long starttime=0;               // Startzeit einer Messung

        // interne Variablen
        boolean udInUse = false;
        boolean measure_loop = true;
//        boolean Winkel_OK = false;
        int answer;
        String iotext;
        String tmpstr;
       
        try {
            try {
                if( checkArgs() == false ) throw new PPExecutionException(PB.getString("Parameterexistenz"));
                //
                // Messgr��e und untere, obere Toleranz einlesen
                //
                result_str = getArg(getRequiredArgs()[0]);

                // Channel setzen

                temp=getArg(getRequiredArgs()[1]);
                if (temp != null) {
                    try {
                        analyzerUI=getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyser();
                        channel=Integer.parseInt(temp);
                        analyzerUI.setKanal(channel); // Kanal von 1-8, intern 0-7
                    } catch (DeviceLockedException e) {
                        e.printStackTrace();
                        result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "deviceLockedException" ), e.getMessage(), Ergebnis.FT_NIO_SYS);
                        ergListe.add(result);
                        throw new PPExecutionException();
                    } catch (DeviceNotAvailableException e) {
                        e.printStackTrace();
                        result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "deviceNotAvailableException" ), e.getMessage(), Ergebnis.FT_NIO_SYS);
                        ergListe.add(result);
                        throw new PPExecutionException();
                    } catch (DeviceParameterException e) {
                        e.printStackTrace();
                        result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "deviceParameterException" ), e.getMessage(), Ergebnis.FT_NIO_SYS);
                        ergListe.add(result);
                        throw new PPExecutionException();
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                        result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unzulaessigerADKanal" ), e.getMessage(), Ergebnis.FT_NIO_SYS);
                        ergListe.add(result);
                        throw new PPExecutionException();
                    } catch (DeviceIOException e) {
                        e.printStackTrace();
                        result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS);
                        ergListe.add(result);
                        throw new PPExecutionException();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (e.getMessage() != null) {
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                }
                else {
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
                }
                ergListe.add(result);
                throw new PPExecutionException();
            }

            
            // ****************************
            // * Werkeranweisung ausgeben *
            // ****************************
            
            // Wenn noch kein Infofenster, dann hole eine Instanz und poste das Infofeld
            if (udInUse != true) {
                udInUse = true;
                try {
                    ud = getPr�flingLaufzeitUmgebung().getUserDialog();
                } catch (Exception e) {
                    if (e instanceof DeviceLockedException)
                        result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
                    else if (e instanceof DeviceNotAvailableException)
                        result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
                    else
                        result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
                    ergListe.add(result);
                    throw new PPExecutionException();
                }
            }
            //
            // Faktor aus Datei auslesen
            //
            aValue = getCalibValue(channel, "A");
            bValue = getCalibValue(channel, "B");

            
            //-----------------------
            // Messung
            //-----------------------
            //
            // do-schleife bis Werker den <Abbruch>-Button anklickt
            //
            starttime=System.currentTimeMillis();
//            ud.displayMessage("Start:", "   Winkelanzeige", -1);
            do {
                istValue=getMeasuredValue(analyzerUI, result_str, min_Spannung, max_Spannung);
                //
                // Umrechnen in Spannung in Winkel mit Hilfe der Umrechnungsfaktoren
                // aus der Kalibrierung
                //
                istWinkel = aValue * istValue + bValue;
//                answer=ud.requestUserInputDigital( "Ausgabe" , "   IstWinkel: " + Float.toString(istWinkel) + "�", 30);
//                if (answer == getPr�flingLaufzeitUmgebung().getUserDialog().YES_KEY ) {
//                    measure_loop=false;
//                }
//                else {
//                    measure_loop=true;
//                }
                ud.displayMessage("Ausgabe", "   IstWinkel: " + Float.toString(istWinkel) + "�", -1);
            } while  (System.currentTimeMillis() < starttime + dauer);
//            ud.displayMessage("Stop:", "   Winkelanzeige", -1);
    
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
            e.printStackTrace();
        } catch (Throwable e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
            e.printStackTrace();
        }
        
//Freigabe der benutzten Devices
// 1. UIAnalyser
        if( analyzerUI != null ) {
            try {
                getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyser();
            } catch (Exception  e) {
                if (e instanceof DeviceLockedException) {
                    result = new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "" , Ergebnis.FT_NIO_SYS );
                }
                else {
                    result = new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "" , Ergebnis.FT_NIO_SYS );
                }
                ergListe.add(result);
                status= STATUS_EXECUTION_ERROR;
                analyzerUI=null;
            }
        }
//
// 2. UserDialog
        if( ud != null ) {
            try {
                getPr�flingLaufzeitUmgebung().releaseUserDialog();
            } catch (Exception e) {
                if (e instanceof DeviceLockedException) {
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
                }
                else {
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
                }
                ergListe.add(result);
                status = STATUS_EXECUTION_ERROR;
                ud = null;
            }
            udInUse = false;
        }        
// Ergebnisstatus setzen
        setPPStatus( info, status, ergListe );
        
    } // End of execute()
    
    /* Ab hier private Methoden */
    
//
// einlesen der Messwerte
// �bergeben wird:
// istValue: Mittelwert
// minVal:   unterer Grenzwert
// maxVal:   oberer Grenzwert

    private float getMeasuredValue(UIAnalyser anUI, String r_str, float minVal, float maxVal) { 
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;

        BoundInfo messung;         // Struktur mit Messinfos (Mittelwert, untere-, obere Grenze)
        float ist_Value=0.0f;
        
        try {
//
//          100 bedeutet 100 msec (Messzyklus)
//
            if (r_str.equals("U") == true) {
                messung=anUI.getVoltageBoundInfo(false, 100);
            }
            else {
                messung=anUI.getCurrentBoundInfo(false, 100);
            }
            ist_Value=messung.getAverageValue()/1000.0f;
            minVal=messung.getMinValue()/1000.0f;
            maxVal=messung.getMaxValue()/1000.0f;

        } catch (DeviceParameterException e) {
            e.printStackTrace();
            result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "deviceParameterException" ), e.getMessage(), Ergebnis.FT_NIO_SYS);
            ergListe.add(result);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unzulaessigerADKanal" ), e.getMessage(), Ergebnis.FT_NIO_SYS);
            ergListe.add(result);
        } catch (DeviceIOException e) {
            e.printStackTrace();
            result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS);
            ergListe.add(result);
        } catch (Exception e) {
            e.printStackTrace();
            result= new Ergebnis( "ExecFehler", "UIAnalyzer", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS);
            ergListe.add(result);
        }

        return ist_Value;
    }
    
    /**
     * Liest die Kalibrierfaktoren aus der entsprechenden Datei aus.
     * 
     * @param channel Der gew�nschte Kanal (1-8)
     * @param prefix Pr�fix des gew�nschten Werts (A oder B)
     * @return Den gelesenen Wert
     * @throws PPExecutionException
     */
    private float getCalibValue(int channel, String prefix) throws PPExecutionException {
    	float calibValue = -9999.9f;
    	StringBuffer temp = new StringBuffer();
    	String ioText ="";
    	int[] infoIndex = {-1, -1};
    	 
    	try {
    		BufferedReader fis = new BufferedReader( new FileReader(fileName));
			while ((ioText = fis.readLine()) != null) {
				temp.append(ioText);
			}
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PPExecutionException("Exception occurred during reading from file " + fileName + ".");
		}
		
		infoIndex[0] = temp.indexOf(prefix + Integer.toString(channel));
		
		if (infoIndex[0] >= 0) {
			infoIndex[1] = temp.indexOf(";", infoIndex[0]);
			calibValue = Float.parseFloat(temp.substring(infoIndex[0] + 5, infoIndex[1]));
		} else {
			throw new PPExecutionException("Calibration value for desired channel could not be found in " + fileName + ".");
		}
   	
    	return calibValue;
    }

} // End of Class

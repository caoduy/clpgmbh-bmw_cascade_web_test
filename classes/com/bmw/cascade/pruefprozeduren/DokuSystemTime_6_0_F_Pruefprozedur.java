/*
 * SystemTime_V0_0_3_FA_Pruefprozedur.java
 *
 * Created on 19.05.04
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import java.text.SimpleDateFormat;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;

/**
 * Implementierung der Pr�fprozedur, die eine Ergebniszeile mit der aktuellen 
 * Systemzeit erzeugt.
 * 
 * Zwingende Argumente:
 *
 * keine
 * 
 * Optionale Argumente:
 * 
 * FORMAT:		Formatter-String, gem�� Java-Doc zur Klasse 
 * 				{@link java.text.SimpleDateFormat}. Das hei�t:
 * 
 * 				S�mtliche Buchstaben besitzen eine Bedeutung. Wenn ein 
 * 				Buchstabe oder Text im Datum mit auftauchen soll, so ist dieser
 * 				in ' zu schreiben. Alle andere Zeichen wie . : - / oder das
 * 				Leerzeichen ben�tigen keine Maskierung.
 * 				
 * 				Wenn ein ' im Datum anzuzeigen ist, muss dieses als '' 
 * 				geschrieben werden.
 *  
 * 				Achtung: Das Semikolon und das @-Zeichen w�rden ebenfalls 
 * 				unver�ndert �bernommen, haben jedoch in Pr�fprozeduren eine
 * 				Sonderfunktion.   
 * 
 * 				Bedeutung der Buchstaben im Rahmen der Formatierung:
 * 				 				 
 *				yyyy  	Jahr, 4-stellig, z. B. 1996  
 *				yy  	Jahr, 2-stellig, z. B. 96
 *				MMMM 	Monat als Text ausgeschrieben, z. B. August  
 *				MMM 	Monat als Text abgek�rzt, z. B. Aug f�r August
 *				MM 		Monat 2-stellig, z. B. 08 f�r August				
 *				ww 		Kalenderwoche, mindestens 2-stellig, z. B. 27 oder 03  
 *				w  		Kalenderwoche, mindestens 1-stellig, z. B. 27 oder 3
 *				WW		Woche im Monat, mindestens 2-stellig, z. B. 01  
 *				W  		Woche im Monat, mindestens 1-stellig, z. B. 1
 *				DDD  	Tag im Jahr, mindestens 3-stellig, z.B. 089  
 *				DD  	Tag im Jahr, mindestens 2-stellig, z.B. 89
 *				D  		Tag im Jahr, mindestens 1-stellig, z.B. 9 oder 89
 *				dd  	Tag im Monat, mindestens 2-stellig, z. B. 01  
 *				d  		Tag im Monat, mindestens 1-stellig, z. B. 1 oder 10  
 *				EEEE  	Wochentag als Text ausgeschrieben, z. B. Dienstag  
 *				EEE  	Wochentag als Text abgek�rzt, z. B. Die
 *				a  		am/pm-Markierung, z. B. PM  
 *				HH  	Stunde (0-23), mindestens 2-stellig, z. B. 15 oder 07  
 *				H  		Stunde (0-23), mindestens 1-stellig, z. B. 15 oder 7
 *				kk  	Stunde (1-24), mindestens 2-stellig, z. B. 15 oder 07  
 *				k  		Stunde (1-24), mindestens 1-stellig, z. B. 15 oder 7
 * 				KK  	Stunde in am/pm (0-11), mindestens 2-stellig, z. B. 11 oder 07  
 *				K  		Stunde in am/pm (0-11), mindestens 1-stellig, z. B. 11 oder 7
 *				hh  	Stunde in am/pm (1-12), mindestens 2-stellig, z. B. 11 oder 07  
 *				h  		Stunde in am/pm (1-12), mindestens 1-stellig, z. B. 11 oder 7
 *				mm 		Minute, mindestens 2-stellig, z. B. 30 oder 05  
 *				m 		Minute, mindestens 1-stellig, z. B. 30 oder 5
 *				ss 		Sekunde, mindestens 2-stellig, z. B. 59 oder 02  
 *				s 		Sekunde, mindestens 1-stellig, z. B. 59 oder 2 
 *				SSS  	Millisekunde, mindestens 3-stellig, z. B. 978 oder 001  
 *				SS  	Millisekunde, mindestens 2-stellig, z. B. 978 oder 01
 *				S  		Millisekunde, mindestens 1-stellig, z. B. 978 oder 1
 *			
 *				Beispiele:
 *
 *				"HH 'Uhr'"  								12 Uhr  
 *				"H:mm"  									0:08  
 *				"EEEE 'der' dd. MMMM yyyy HH:mm:ss:SSS"   	Dienstag der 08. August 2008 09:01:17:586
 * 				"d. MMM yy H:m:s"    						8. Aug 08 9:1:17
 *				"yyyy-MM-dd' 'HH:mm:ss:SSS"					2008-07-31 13:15:42:012  
 *				"yyyy-MM-dd HH:mm:ss:SSS"					2008-07-31 13:15:42:012
 *
 *				Default ist das Format "yyyy-MM-dd' 'HH:mm:ss:SSS".
 *
 * @author St�binger
 * @version V0_0_1 19.05.2004 SS Implementierung<BR>
 *          V2_0 09.04.2006 TB Zeitformat erg�nzt<BR>
 *          V3_0 10.04.2006 TB Eigener Resultvektor f�r die formatierte Zeitausgabe<BR>
 *			V4_0 30.04.2006 FG Korrektur APDM Ergbenisformat (Zeile 101-103)<BR>
 *			V6_0 17.09.2008 PR Zeit- und Datumsformat parametrierbar gemacht<BR>
 */
public class DokuSystemTime_6_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
  
  	/**
   	* DefaultKonstruktor, nur fuer die Deserialisierung
   	*/
  	public DokuSystemTime_6_0_F_Pruefprozedur() {}

  	/**
     * erzeugt eine neue Pruefprozedur.
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
  	public DokuSystemTime_6_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }


    /**
     * initialsiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }

    /**
     * liefert die optionalen Argumente
     */
    public String[] getOptionalArgs() {
        String[] args = {"FORMAT"};
        return args;
    }

    /**
     * liefert die zwingend erforderlichen Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = new String[0];
        return args;
    }

    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
     */
    public boolean checkArgs() {
       return super.checkArgs();
    }

    /**
     * f�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        Date date = new Date();
        String formatString; //beschreibt das Wunschformat 
        SimpleDateFormat dateFormat;

        try {
            //Parameter holen und Argumente pr�fen.
            try {
            	// Ist der allgemeine Check fehlgeschlagen?
                if( !checkArgs() ) 
                	throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
                
             // Hole die Formatangabe.
                formatString = getArg( getOptionalArgs()[0] );
                if (formatString == null)
                	formatString = "yyyy-MM-dd' 'HH:mm:ss:SSS"; //Default
                else if ( formatString.indexOf('@') != -1 )
                	formatString = getPPResult( formatString );

            } catch (PPExecutionException e) {
                if (e.getMessage() != null)
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }                                    
            
            // Notwendige Wartezeit von 10ms, da sonst die gesamte Durchf�hrung < 1ms dauert. Dies verursacht Fehler bei der Unterscheidung mit anderen Pr�fschritten
            try{
            	Thread.sleep( 10 );
            }catch(Exception ex){
            	// nicht notwendig
            }
        
            //Date-Formatter initialisieren.
            dateFormat = new SimpleDateFormat( formatString );
            
            // Ergebniseintrag erzeugen
            long ms = System.currentTimeMillis();
            date.setTime(ms);
            result = new Ergebnis( "SysTime", "System", "", "", "", "SystemTime", Long.toString(ms), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
            ergListe.add(result);
            result = new Ergebnis( "SysDate", "System", "", "", "", "SystemDate", dateFormat.format(date), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
            ergListe.add(result);
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
        //Status setzen
        setPPStatus( info, status, ergListe );

    }

}


package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;

/**
 * <b>StringCat</b><br>
 * 
 * Implementierung einer Pr�fprozedur, die Strings verketten und mit Leerzeichen versehen kann
 *
 * @author Ulf Plath, TI-432
 * @version 1_0_F 10.07.2008  UP  Ersterstellung
 * @version 2_0_F 22.07.2008  UP  Logausgaben im Fehlerfall ge�ndert
 * @version 3_0_F 25.07.2008  UP  Schleifenverhalten in checkArgs() ge�ndert
 * @version 4_0_F 31.08.2009  UP  Methode zur �berpr�fung von Strings ge�ndert
 * @version 5_0_F 21.09.2009  UP  Fehler in checkArgs() behoben, der das BLANKS-Argument falsch bewertete
 * @version 6_0_F 09.07.2014  UP  Umgestaltung des Ablaufs, damit die Strings in der richtigen Reihenfolge bleiben,
 *                                der neue Parameter BLANKSTRING wurde hinzugef�gt
 */
public class StringCat_6_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur f�r die Deserialisierung
	 */
	public StringCat_6_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pr�fprozedur.
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public StringCat_6_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialisiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "STRING[2..N]", "BLANK[1..N]", "BLANKS", "BLANKSTRING", "RESULTNAME", "DEBUG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "STRING1" };
		return args;
	}

	/**
	 * pr�ft die in CASCADE angegebenen Argumente auf "rechtm��ige" Existenz.
	 */
	public boolean checkArgs() {
		int i = 0;
		int j = 0;
		int intIndex = 0;
		String currentArg = null;
		String strIndex = null;
		String currentKey = null;
		;
		boolean exist = false;
		boolean debug = (getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;

		// 1. Check: Sind alle requiredArgs gesetzt
		String[] requiredArgs = getRequiredArgs();
		for( i = 0; i < requiredArgs.length; i++ ) {
			if( getArg( requiredArgs[i].toUpperCase() ) == null )
				return false;
			if( getArg( requiredArgs[i].toUpperCase() ).equals( "" ) == true )
				return false;
		}
		log( debug, "OK", "Required arguments checked successfully." );

		// 2. Check: Welche optionalArgs sind gesetzt
		Enumeration enu = getArgs().keys();
		String[] optionalArgs = getOptionalArgs();

		while( enu.hasMoreElements() ) {
			currentKey = (String) enu.nextElement();
			exist = false;
			j = 0;

			// Argument unbenannt
			if( getArg( currentKey ).equals( "" ) == true )
				return false;

			// geh�rt Argument zu required Args
			while( (exist == false) && (j < requiredArgs.length) ) {
				if( currentKey.equalsIgnoreCase( requiredArgs[j] ) == true )
					exist = true;
				j++;
			}
			j = 0;

			// ist Argument STRING[2..N]-Argument
			if( exist == false && currentKey.toUpperCase().startsWith( "STRING" ) ) {
				strIndex = currentKey.substring( 6 );

				// versuche, Indizes von STRING[2..N] zu extrahieren
				try {
					intIndex = Integer.parseInt( strIndex );
					if( intIndex < 2 ) {
						log( debug, "Error", "Index of STRING argument to small (< 2)" );
						return false;
					}
					exist = true;
				} catch( NumberFormatException e ) {
					log( debug, "Error", "Could not extract index of STRING[2..N] argument" );
					return false;
				}
			}

			// ist Argument BLANKS- oder BLANK[1..N]-Argument
			if( exist == false && currentKey.toUpperCase().startsWith( "BLANK" ) ) {
				strIndex = currentKey.toString();
				if( strIndex.equalsIgnoreCase( "BLANKS" ) || strIndex.equalsIgnoreCase( "BLANKSTRING" ) )
					exist = true;
				else {
					// versuche, Index von BLANK[2..N] zu extrahieren
					try {
						strIndex = strIndex.substring( 5 );
						intIndex = Integer.parseInt( strIndex );
						if( intIndex < 1 ) {
							log( debug, "Error", "Index of BLANK argument to small (< 1)" );
							return false;
						}
						exist = true;
					} catch( NumberFormatException e ) {
						log( debug, "Error", "Could not extract index of BLANK[1..N] argument" );
						return false;
					}
				}
			}

			// ist Argument sonstiges opt. Argument
			while( (exist == false) && (j < optionalArgs.length) ) {
				if( currentKey.equalsIgnoreCase( optionalArgs[j] ) == true )
					exist = true;
				j++;
			}
			// Argument offenbar undefiniert
			if( exist == false )
				return exist;
		} // Ende WHILE-Schleife					

		// Tests bestanden, somit ok
		log( debug, "OK", "No undefined arguments detected." );
		return exist;
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// externe Argumente
		boolean debug = (getArg( "DEBUG" ) != null && getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
		String resultname = (getArg( "RESULTNAME" ) != null) ? getArg( "RESULTNAME" ) : "Concatenate";
		boolean boolBlankN = (getArg( "BLANKS" ) != null && getArg( "BLANKS" ).equalsIgnoreCase( "TRUE" )) ? true : false;
		String blankString = getArg( "BLANKSTRING" ) != null ? getArg( "BLANKSTRING" ) : " ";

		// interne Variablen
		Vector resList = new Vector();
		Vector strKeys = new Vector();
		int status = STATUS_EXECUTION_OK;
		int i = 0;
		String strCurrentKey = "";
		String strCurrentElement = "";
		String resString = "";
		String strBlankX = "";
		boolean boolBlankX = false;

		try {
			// Argumente pr�fen
			if( !checkArgs() ) {
				// NOK - Pr�fung fehlgeschlagen!
				status = STATUS_EXECUTION_ERROR;
				resList.add( new Ergebnis( "StatusCheckArgs", "", "", "", "", "CheckArgsNOk", "NOk", "Ok", "Ok", "0", "", "", "", PB.getString( "parameterexistenz" ), "", Ergebnis.FT_NIO ) );
				log( debug, "Error", "Check of arguments failed." );
				throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			}

			Enumeration enu = getArgs().keys();

			// Argumente nach STRING... durchsuchen
			while( enu.hasMoreElements() ) {
				strCurrentKey = (String) enu.nextElement();
				strCurrentElement = getArg( strCurrentKey );

				log( debug, "Execute", "Current key: " + strCurrentKey );
				log( debug, "Execute", "Current element: " + strCurrentElement );

				if( strCurrentKey.startsWith( "STRING" ) ) {
					// Ordnungsnummer speichern
					strKeys.add( strCurrentKey.substring( 6 ) );
				}
			}

			// Vektor (variable Gr��e) in Array (fixe Gr��e, sortierbar) exportieren
			int[] intKeys = new int[strKeys.size()];

			for( i = 0; i < strKeys.size(); i++ ) {
				// parse-F�higkeit wurde bei checkArgs() schon gepr�ft
				intKeys[i] = Integer.parseInt( strKeys.elementAt( i ).toString() );
			}

			Arrays.sort( intKeys );

			// Strings zusammenh�ngen
			for( i = 0; i < intKeys.length; i++ ) {
				try {
					strCurrentKey = "STRING" + intKeys[i];
					strCurrentElement = getArg( strCurrentKey );

					// @-Konstrukte aufl�sen
					if( strCurrentElement.indexOf( '@' ) != -1 ) {
						strCurrentElement = getPPResult( strCurrentElement );
					}

					// auf gew�nschtes Leerzeichen pr�fen
					strBlankX = getArg( "BLANK" + intKeys[i] );
					boolBlankX = strBlankX != null && strBlankX.equalsIgnoreCase( "TRUE" ) ? true : false;

					if( (boolBlankN || boolBlankX) && i < (intKeys.length - 1) )
						strCurrentElement += blankString;

				} catch( InformationNotAvailableException e ) {
					status = STATUS_EXECUTION_ERROR;
					log( debug, "Error", "Result " + strCurrentElement + " not available!" );
					e.printStackTrace();
				} catch( Exception e ) {
					status = STATUS_EXECUTION_ERROR;
					log( debug, "Error", "Error in @-construct evaluation! " + e.getMessage() );
					e.printStackTrace();
				}

				resString += strCurrentElement;
			}

			// Ergebnis ablegen
			resList.add( new Ergebnis( resultname, "", "", "", "", "String concatenation successful", resString, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ) );

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
			log( debug, "Error", "executing procedure, PPExecutionException: " + e.getMessage() );
			e.printStackTrace();
		} catch( Exception e ) {
			status = STATUS_EXECUTION_ERROR;
			resList.add( new Ergebnis( "ExecError", "StringCat", "", "", "", "", "", "", "", "0", "", "", "", "Exception: " + (e.getMessage() != null ? e.getMessage() : ""), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS ) );
			log( debug, "Error", "executing procedure, Exception: " + e.getMessage() );
			e.printStackTrace();
		} catch( Throwable e ) {
			status = STATUS_EXECUTION_ERROR;
			resList.add( new Ergebnis( "ExecError", "StringCat", "", "", "", "", "", "", "", "0", "", "", "", "Throwable: " + (e.getMessage() != null ? e.getMessage() : ""), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS ) );
			log( debug, "Error", "executing procedure, Throwable: " + e.getMessage() );
			e.printStackTrace();
		}

		// Ergebnisse in CASCADE-Log schreiben
		for( i = 0; i < resList.size(); i++ ) {
			log( debug, "APDMResult" + i, resList.get( i ).toString() );
		}

		setPPStatus( info, status, resList );
	}

	/**
	 * <b>log</b>
	 * <br>
	 * Schreibt Text in die CASCADE-Logdatei, wenn das Debug-Flag gesetzt ist
	 * <br>
	 * @param debug - the debug flag
	 * @param type - the type of message to display
	 * @param text - the message text
	 */
	private void log( boolean debug, String type, String text ) {
		if( type.equalsIgnoreCase( "Error" ) || debug ) {
			System.out.println( "[" + getLocalName() + "] [" + type + "] " + text );
		}
	}
}

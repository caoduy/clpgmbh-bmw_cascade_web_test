package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.util.dom.DomPruefstand;

/**
 * Dokumentation von E/E Daten (Seriennummer, Herstelldatum, SW-St�nde, ...) der elekronischen, dokumentationspflichtigen L�fter ab G11 in der 35up Produktfamilie.<BR>
 * <BR>
 * Funktion:<BR>
 * - Dokumentation aller E-L�fter Daten vgl. Hintergrund<BR>
 * - PP kapselt den Gesamtablauf und alle diesbez�glichen Diagnosejobs.<BR>
 * - Daten werden als separates DOM File mit dem Suffix 'G' �bertragen.<BR>
 * <BR>
 * Hintergrund:<BR>
 * - ab G11 hat jedes Fahrzeug einen dokumentationspflichtigen E-L�fter<BR>
 * - Sicherheitsfahrzeuge k�nnen 2 E-L�fter haben in diesem Fall wird optional einfach eine zweite Motronik angesprochen.<BR>
 * - Die Daten zur SGBD sollten i.d.R. einfach �ber die Existenz aus dem virtuellen Fzg. ermittelt werden, optional k�nnen diese per �bergabeparameter �berschrieben werden.<BR>
 * - Laut Entwicklung ist der Job und das Job Argument immer konstant pro Motor SGBD.<BR> 
 * - DOM erwartet das Jahr beim Herstelldatum 4-stellig, SG liefert jedoch nur die letzten 2 Stellen. H�nge die f�hrenden 2 vom aktuellen Datum noch an.<BR>
 * <BR>
 * Der SGBD Extra-Job wird �ber status_lesen mit dem Argument ELUE_LIN_META gestartet und beinhaltet folgende Gr��en: <BR>
 * Dabei sind alle Datenformate mit *_ub uint8 und mit *_uw uint16-Gr��en angegeben. <BR>
 * <BR>
 * Beschreibung SGBD Result	           SGBD Results	                                        SGBD Result                        DME-Daten	                  Beispiele                                <BR>
 * ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------<BR>
 * Ergebnis verwertbar     	           0: Fehler (Relaisfehler, LIN-Fehler)                 STAT_STATUS_ELUE_LIN               BMWfanlin1_st_ReadByIdTstr_ub                                           <BR>
 * (JOB-Auswertung wurde deaktiviert)  1: Metadaten i.O.                                                                                                                                               <BR>
 *                                     2: Metadaten n.i.O. (mind. Ein unplausibler Wert)		                                                                                                       <BR>
 * LIN Identifikationsnummern          Supplier ID	                                        STAT_ZULIEF_NUM_WERT               BMWfanlin1_st_SplId_uw	      z.B. 7FF7                                <BR>
 *                                     Function ID	                                        STAT_FUNK_NUM_WERT                 BMWfanlin1_st_FctId_uw	      z.B. 2F45                                <BR>
 *                                     Variante	                                            STAT_VAR_LIN_WERT                  BMWfanlin1_st_Var_ub	          z.B. 02                                  <BR>
 * Seriennummer	(tagesbezogen)         Seriennummer	                                        STAT_SERIEN_NR_WERT                BMWfanlin1_st_SerlNr_uw	      z.B. 6666 (theor. m�glich z.B. 12345678) <BR>
 * Produktionsdatum	                   Produktionsdatum: Tag	                            STAT_PROD_TAG_WERT                 BMWfanlin1_st_ProdDay_ub	      TT                                       <BR>
 *                                     Produktionsdatum: Monat	                            STAT_PROD_MON_WERT                 BMWfanlin1_st_ProdMth_ub	      MM                                       <BR>
 *                                     Produktionsdatum: Jahr	                            STAT_PROD_JHR_WERT                 BMWfanlin1_st_ProdYr_ub	      YY                                       <BR>
 * BMW Teilenummer mit                 Versorgungsspannung	                                STAT_SPNG_VERS_WERT                BMWfanlin1_u_Spl_ub	          z.B. 12                                  <BR>
 * cod. Zusatzinfos z.B. 1201020304    Leistungsklasse	                                    STAT_LEIST_KLASSE                  BMWfanlin1_pwr_Clas_ub	      z.B. 01 (entspricht 50W)                 <BR>
 *                                     Hersteller L�fterantrieb	                            STAT_HERST_LUEFTER_WERT            BMWfanlin1_qlf_DrvProd_ub	  z.B. 02 (z.B. Magna)                     <BR>
 *                                     Werk Herstellung L�fterantrieb                   	STAT_WERK_LUEFTER_WERT             BMWfanlin1_qlf_DrvProdFac_ub	  z.B. 03 (z.B. Werk Sailauf)              <BR>
 *                                     Produktfamilie L�fterantrieb                     	STAT_FAM_LUEFTER_WERT              BMWfanlin1_qlf_DrvProdLine_ub  z.B. 04 (z.B. GEN2)                      <BR>
 * Softwarenummer	                   Software Referenznummer	                            STAT_SW_REF_WERT                   BMWfanlin1_st_SwRefNr_uw	      z.B. SW43                                <BR>
 * Hardwarenummer	                   Hardware Referenznummer	                            STAT_HW_REF_WERT                   BMWfanlin1_st_HwRefNr_uw	      z.B. HW88                                <BR>
 * <BR>
 * @author BMW TI-538 Thomas Buboltz (TB)<BR>
 * @version 1_0_F 28.10.2013 TB Implementierung<BR>
 *          2_0_T 20.10.2014 TB BugFix: VARIANTE@SGBD_SIV noch ausrechnen, falls das Attribut: SGBD bei Motor SGBD'n dies so enth�lt <BR> 
 *          3_0_T 07.11.2014 TB Request seitens Entwicklung: STAT_STATUS_ELUE_LIN Job und entsprechende �berpr�fung komplett ausbauen, ab I-420 35up sollte es dann funktionieren <BR>
 *          4_0_F 27.11.2014 TB F-Version <BR>
 *          5_0_T 02.09.2015 TB HotFix, wenn SGBD null ist (nicht parametriert und kein Motor PL gefunden) <BR>
 *          6_0_F 02.09.2015 TB F-Version <BR>
 *          7_0_T 23.01.2017 TB LOP 2150, Eine zweite Motorstuerung hat aktuell doch keinen E-L�fter perspektivisch jedoch vielleicht schon. Wenn der Parameter "SGBD_SLAVE = null oder NULL oder 0" ist dann frage keinen zweiten L�fter ab.<BR>
 *          8_0_F 23.01.2017 TB F-Version <BR>
 */
public class DokuEFan_8_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DokuEFan_8_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DokuEFan_8_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "SGBD_MASTER", "SGBD_SLAVE", "JOB", "DEBUG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = {};
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert!!!
	 */
	@Override
	public boolean checkArgs() {
		// Obligatorisch.
		return super.checkArgs();
	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		StringBuffer sbXml = new StringBuffer();

		// Parameter
		String[] sgbd = { null, null };
		String job = null;
		final String IGNORE_TOKEN_FOR_SGBD_SLAVE_JOB_EXECUTION = "0NULL"; // "0 oder NULL [in Gro�- oder Kleinbuchstaben]"
		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen

		// EDIABAS
		EdiabasProxyThread ediabas = null;

		// JobParameter
		final String JOB = "STATUS_LESEN";
		final String JOB_PARAMETER = "ARG;ELUE_LIN_META";

		// JOBRESULTS
		//LIN Identifikationsnummern
		final String STAT_ZULIEF_NUM_WERT = "STAT_ZULIEF_NUM_WERT";
		final String STAT_FUNK_NUM_WERT = "STAT_FUNK_NUM_WERT";
		final String STAT_VAR_LIN_WERT = "STAT_VAR_LIN_WERT";
		//Seriennummer
		final String STAT_SERIEN_NR_WERT = "STAT_SERIEN_NR_WERT";
		//Herstelldatum
		final String STAT_PROD_TAG_WERT = "STAT_PROD_TAG_WERT";
		final String STAT_PROD_MON_WERT = "STAT_PROD_MON_WERT";
		final String STAT_PROD_JHR_WERT = "STAT_PROD_JHR_WERT";
		//Teilenummer codiert
		final String STAT_SPNG_VERS_WERT = "STAT_SPNG_VERS_WERT";
		final String STAT_LEIST_KLASSE_WERT = "STAT_LEIST_KLASSE";
		final String STAT_HERST_LUEFTER_WERT = "STAT_HERST_LUEFTER_WERT";
		final String STAT_WERK_LUEFTER_WERT = "STAT_WERK_LUEFTER_WERT";
		final String STAT_FAM_LUEFTER_WERT = "STAT_FAM_LUEFTER_WERT";
		//Softwarenummer
		final String STAT_SW_REF_WERT = "STAT_SW_REF_WERT";
		//Hardwarenummer
		final String STAT_HW_REF_WERT = "STAT_HW_REF_WERT";

		// intern 
		String[] linVersion = { null, null };
		String[] serialNumber = { null, null };
		String[] manufactureDate = { null, null };
		String[] partNumber = { null, null };
		String[] swVersion = { null, null };
		String[] hwVersion = { null, null };

		final boolean DE = checkDE(); // Systemsprache DE wenn true

		final String PL_NAME_ENGINE = "MOTOR"; // PL Preffix fuer Motor

		try {
			//Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG Parameter
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( !getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				} else {
					// alternativ DEBUG �ber Pr�fstandsvariable noch auswerten
					try {
						Object objDebug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
						if( objDebug != null ) {
							if( objDebug instanceof String && "TRUE".equalsIgnoreCase( (String) objDebug ) )
								bDebug = true;
							else if( objDebug instanceof Boolean && ((Boolean) objDebug).booleanValue() )
								bDebug = true;
						}
					} catch( VariablesException e ) {
						// Nothing, bDebug ist false vorbelegt
					}
				}
				if( bDebug ) {
					System.out.println( "PP DokuEFan: PARAMETERCHECK, Parameter: DEBUG=<" + bDebug + ">" );
				}

				// SGBD(s)
				if( getArg( "SGBD_MASTER" ) != null ) {
					sgbd[0] = extractValues( getArg( "SGBD_MASTER" ) )[0];
				}
				if( getArg( "SGBD_SLAVE" ) != null ) {
					sgbd[1] = extractValues( getArg( "SGBD_SLAVE" ) )[0];
				}

				// JOB(s)
				if( getArg( "JOB" ) != null ) {
					job = extractValues( getArg( "JOB" ) )[0];
				} else
					job = JOB;

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// EDIABAS holen
			try {
				ediabas = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();
			} catch( DeviceNotAvailableException e ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Prueflinge scannen
			Pruefling[] prueflinge = this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getPr�flinge();
			for( int i = 0; i < prueflinge.length; i++ ) {
				// SGBD Master E-L�fter
				if( prueflinge[i].getName().equalsIgnoreCase( PL_NAME_ENGINE ) && sgbd[0] == null ) {
					// Bei Referenzoperator errechne die Quellinfo
					if( ((String) prueflinge[i].getAllAttributes().get( "SGBD" )).indexOf( "@" ) != -1 ) {
						String sgbdParameter = (String) prueflinge[i].getAllAttributes().get( "SGBD" );

						// Bei SGBD=VARIANTE@SGBD_SIV l�se dies �ber den generierenden Pr�fschritt auf. Aufgrund anderer PL, muss noch auf das Format: <Resultname>@<Pr�flingname>.<Pr�fschritt> umgerechnet werden 
						sgbd[0] = extractValues( sgbdParameter.substring( 0, sgbdParameter.indexOf( "@" ) + 1 ) + PL_NAME_ENGINE + "." + sgbdParameter.substring( sgbdParameter.indexOf( "@" ) + 1 ) )[0];
					} else {
						sgbd[0] = (String) prueflinge[i].getAllAttributes().get( "SGBD" );
					}

					// Doku SGBD Job f�r den Motor
					if( bDebug ) {
						System.out.println( "PP DokuEFan: SGBD EDIABAS Job SGBD:=<" + sgbd[0] + ">" );
					}
				}

				// SGBD Slave E-L�fter
				if( prueflinge[i].getName().equalsIgnoreCase( PL_NAME_ENGINE + "2" ) && sgbd[1] == null ) {
					// Bei Referenzoperator errechne die Quellinfo
					if( ((String) prueflinge[i].getAllAttributes().get( "SGBD" )).indexOf( "@" ) != -1 ) {
						String sgbdParameter = (String) prueflinge[i].getAllAttributes().get( "SGBD" );

						// Bei SGBD=VARIANTE@SGBD_SIV l�se dies �ber den generierenden Pr�fschritt auf. Aufgrund anderer PL, muss noch auf das Format: <Resultname>@<Pr�flingname>.<Pr�fschritt> umgerechnet werden 
						sgbd[1] = extractValues( sgbdParameter.substring( 0, sgbdParameter.indexOf( "@" ) + 1 ) + PL_NAME_ENGINE + "2." + sgbdParameter.substring( sgbdParameter.indexOf( "@" ) + 1 ) )[0];
					} else {
						sgbd[1] = (String) prueflinge[i].getAllAttributes().get( "SGBD" );
					}

					// Doku SGBD Job f�r den Motor
					if( bDebug ) {
						System.out.println( "PP DokuEFan: SGBD EDIABAS Job SGBD:=<" + sgbd[1] + ">" );
					}
				}
			}

			// Utility: ersten 2 Stellen vom aktuellen Jahr ermitteln
			GregorianCalendar cal = new GregorianCalendar();
			String currentYear_Leading2Digits = Integer.toString( cal.get( GregorianCalendar.YEAR ) ).substring( 0, 2 );

			// L�nge f�r die Schleifen ermitteln / entweder nur 1 Motor + E-L�fter, od. 2 Motoren + 2 E-L�fter. 
			// Zus�tzlich gilt die Einschr�nkung, dass wenn kein zweiter E-L�fter ausgewertet werden soll, wenn der Parameter SGBD_SLAVE = null oder NULL oder 0 parametriert ist  
			int length = sgbd[1] != null && !IGNORE_TOKEN_FOR_SGBD_SLAVE_JOB_EXECUTION.contains( sgbd[1].toUpperCase() ) ? 2 : 1;

			// Job ausf�hren
			for( int i = 0; i < length; i++ ) {
				try {
					if( bDebug ) {
						System.out.println( "PP DokuEFan: Exceute EDIABAS Job SGBD:=<" + sgbd[i] + ">, JOB:=<" + job + ">, Parameter:=<" + JOB_PARAMETER + ">" );
					}

					// SGBD Null abfangen (falls kein Motor PL vorhanden und keine SGBD parametriert wurde)
					if( sgbd[i] == null ) {
						throw new EdiabasResultNotFoundException( DE ? "SGBD ist null!" : "SGBD is null!" );
					}

					// Job ausf�hren
					String temp = ediabas.executeDiagJob( sgbd[i], job, JOB_PARAMETER, "" );
					if( temp.equals( "OKAY" ) ) {
						// Results auslesen

						// LIN Identifikationsnummer
						linVersion[i] = ediabas.getDiagResultValue( STAT_ZULIEF_NUM_WERT ) + ediabas.getDiagResultValue( STAT_FUNK_NUM_WERT ) + ediabas.getDiagResultValue( STAT_VAR_LIN_WERT );
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd[i], job, JOB_PARAMETER, "LIN_VERSION", linVersion[i], "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
						if( bDebug )
							System.out.println( "PP DokuEFan: EDIABAS-RESULTS, Name=<" + "LIN_VERSION" + ">, Result=<" + linVersion[i] + ">" );

						// Seriennummer
						serialNumber[i] = ediabas.getDiagResultValue( STAT_SERIEN_NR_WERT );
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd[i], job, JOB_PARAMETER, STAT_SERIEN_NR_WERT, serialNumber[i], "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
						if( bDebug )
							System.out.println( "PP DokuEFan: EDIABAS-RESULTS, Name=<" + STAT_SERIEN_NR_WERT + ">, Result=<" + serialNumber[i] + ">" );

						// Produktionsdatum
						manufactureDate[i] = currentYear_Leading2Digits + ediabas.getDiagResultValue( STAT_PROD_JHR_WERT ) + "-" + ediabas.getDiagResultValue( STAT_PROD_MON_WERT ) + "-" + ediabas.getDiagResultValue( STAT_PROD_TAG_WERT );
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd[i], job, JOB_PARAMETER, "MANUFACTURE_DATE", manufactureDate[i], "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
						if( bDebug )
							System.out.println( "PP DokuEFan: EDIABAS-RESULTS, Name=<" + "MANUFACTURE_DATE" + ">, Result=<" + manufactureDate[i] + ">" );

						// Teilenummer codiert
						partNumber[i] = ediabas.getDiagResultValue( STAT_SPNG_VERS_WERT ) + ediabas.getDiagResultValue( STAT_LEIST_KLASSE_WERT ) + ediabas.getDiagResultValue( STAT_HERST_LUEFTER_WERT ) + ediabas.getDiagResultValue( STAT_WERK_LUEFTER_WERT ) + ediabas.getDiagResultValue( STAT_FAM_LUEFTER_WERT );
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd[i], job, JOB_PARAMETER, "PART_NUMBER", partNumber[i], "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
						if( bDebug )
							System.out.println( "PP DokuEFan: EDIABAS-RESULTS, Name=<" + "PART_NUMBER" + ">, Result=<" + partNumber[i] + ">" );

						// SW Version
						swVersion[i] = ediabas.getDiagResultValue( STAT_SW_REF_WERT );
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd[i], job, JOB_PARAMETER, STAT_SW_REF_WERT, swVersion[i], "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
						if( bDebug )
							System.out.println( "PP DokuEFan: EDIABAS-RESULTS, Name=<" + STAT_SW_REF_WERT + ">, Result=<" + swVersion[i] + ">" );

						// HW Version
						hwVersion[i] = ediabas.getDiagResultValue( STAT_HW_REF_WERT );
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd[i], job, JOB_PARAMETER, STAT_HW_REF_WERT, hwVersion[i], "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
						if( bDebug )
							System.out.println( "PP DokuEFan: EDIABAS-RESULTS, Name=<" + STAT_HW_REF_WERT + ">, Result=<" + hwVersion[i] + ">" );

					} else {
						// Jobstatus NOT OKAY
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd[i], job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					}
				} catch( ApiCallFailedException e ) {
					if( bDebug )
						e.printStackTrace();
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd[i], job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} catch( EdiabasResultNotFoundException e ) {
					if( bDebug )
						e.printStackTrace();
					if( e.getMessage() != null )
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd[i], job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd[i], job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}
			}

			// Dokumentation
			// XML-Output-Buffer zusammenbauen
			sbXml.append( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" );
			sbXml.append( "<vehicleElectronicComponentsG\r\n" );
			sbXml.append( "xmlns=\"http://bmw.com/vehicleElectronicComponents\"\r\n" );
			sbXml.append( "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\r\n" );
			sbXml.append( "xsi:schemaLocation=\"http://bmw.com/vehicleElectronicComponents vehicleElectronicComponentsG.xsd\"\r\n" );
			sbXml.append( "version=\"01.00\" refSchema=\"vehicleElectronicComponentsG.xsd\">\r\n" );
			sbXml.append( "<vehicle vinShort=\"" );
			sbXml.append( getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7() );
			sbXml.append( "\">\r\n" );
			sbXml.append( "<electronicComponents>\r\n" );

			for( int i = 0; i < length; i++ ) {
				sbXml.append( "\t<electronicComponent>\r\n" );
				sbXml.append( "\t\t<sensor sensorID=\"" + (i == 0 ? "3D80" : "3D88") + "\">\r\n" );
				sbXml.append( "\t\t\t<linVersion>" + linVersion[i] + "</linVersion>\r\n" );
				sbXml.append( "\t\t</sensor>\r\n" );
				sbXml.append( "\t\t<serialNo>" + serialNumber[i] + "</serialNo>\r\n" );
				sbXml.append( "\t\t<manufacturingDate>" + manufactureDate[i] + "</manufacturingDate>\r\n" );
				sbXml.append( "\t\t<assemblyPartNo>" + partNumber[i] + "</assemblyPartNo>\r\n" );
				sbXml.append( "\t\t<software>\r\n" );
				sbXml.append( "\t\t\t<softwareVersion type =\"func\">" + swVersion[i] + "</softwareVersion>\r\n" );
				sbXml.append( "\t\t</software>\r\n" );
				sbXml.append( "\t\t<hardwareVersion>" + hwVersion[i] + "</hardwareVersion>\r\n" );
				sbXml.append( "\t</electronicComponent>\r\n" );
			}

			sbXml.append( "</electronicComponents>\r\n" );
			sbXml.append( "</vehicle>\r\n" );
			sbXml.append( "</vehicleElectronicComponentsG>\r\n" );

			try {
				DomPruefstand.writeDomData( getPr�fling().getAuftrag().getFahrgestellnummer7(), "G", sbXml.toString().getBytes() );
			} catch( Exception e ) {
				result = new Ergebnis( "ExecFehler", "DomTransfer", "", "", "", "", "", "", "", "0", "", "", "", "FileWrite", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//IO-Ablauf als Status festhalten
			result = new Ergebnis( "DokuEFan", "DomTransfer", "", "", "", "STATUS", "OK", "OK", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );

		} catch( PPExecutionException e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// PP Status setzen
		setPPStatus( info, status, ergListe );
	}
}

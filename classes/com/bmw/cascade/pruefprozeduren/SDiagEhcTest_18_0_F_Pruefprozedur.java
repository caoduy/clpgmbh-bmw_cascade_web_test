package com.bmw.cascade.pruefprozeduren;

/*
 * SDiagEhcTest_14_0_F_Pruefprozedur.java
 *
 * --------------------------------------------------------------------------
 * 08.05.2001  SH  V0_0_1  Ersterstellung
 * 23.01.2002  SH  V0_0_2  Erweitert f�r E39, E6x
 * 24.01.2002  SH  V0_0_3  Korrektur f�r E65/66/67
 * 04.02.2002  RS  V0_0_4  Analyse Vertauscherpruefung auf Fehlererkennung erweitert!
 * 15.06.2004  SP  V0_0_5  Ergaenzung Analyse Vertauscherpruefung um rechte Seite
 * 15.06.2004  SH  V0_0_6  Reduzierung Wartezeit auf 5 sec und Kommentare
 * 21.06.2004  SH  V0_0_7  Ansteuerzeit auf 8 sec und Korrektur rechter Test
 * 12.10.2004  SH  V0_0_8  TA Pruefung ganz durchlaufen // Fehler beim ersten Test (links) soll auch NIO erzeugen
 * 14.10.2004  SH  V0_0_9  FA FA-Version der Pr�fprozedur mit Version 8
 * 05.12.2005  SH  V0_1_0  Ergaenzung der Vertauscherpruefung f�r E70 sgbd (2 job's)
 * 10.07.2007  PJ  V0_1_1  Ergaenzung der Vertauscherpruefung f�r F01 sgbd
 * 17.07.2007  PJ  V0_1_2  Zeitangaben ge�ndert f�r F01 sgbd
 * 25.10.2007  SH  14_0_F  F01 EHC intergiert
 * 06.03.2008  SH  16_0_F  F01 EHC Diagnosejobs ge�ndert/optimiert und optionaler Parameter Zeit hinzugef�gt
 * 11.03.2008  CS  17_0_F  Absicherung optionaler Parameter Zeit, falls Angabe vergessen; Parameteransteuerung links/rechts f�r F01 getauscht
 * 09.12.2013  CW  18_0_F  Text korrigiert
 * ------------------------------------------------------------------------------------------
 */
import java.util.Vector;


import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;

/**
 * Implementierung von EHC (S_Vertau) INPA-Sonderskript.
 * @author Crichton
 * @version Implementierung
 */
public class SDiagEhcTest_18_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

        static final long serialVersionUID = 1L;
        
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public SDiagEhcTest_18_0_F_Pruefprozedur() {}

  /**
     * erzeugt eine neue Pruefprozedur mit obigem Verhalten
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public SDiagEhcTest_18_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }


    /**
     * initialisiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }

    /**
     * liefert die optionalen Argumente
     */
    public String[] getOptionalArgs() {
        String[] args = {"ADJUSTMENT_TIME"};
        return args;
    }

    /**
     * liefert die zwingend erforderlichen Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = {"SGBD"};
        return args;
    }

    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
     */
    public boolean checkArgs() {
        String[] requiredArgs = getRequiredArgs();

        try {
            // SGBD muss existieren
            if ( getArg(requiredArgs[0]) == null ) return false;
            return true;
        } catch (Exception e) {
            return false;
        } catch (Throwable e) {
            return false;
        }
    }

    /**
     * f�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info )  {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;

        // spezifische Variablen
        String sgbd;
        String temp;
        String job1 ="";
        String param1_job1 ="";
        String erg1_job1 ="";
        String erg2_job1 ="";
        String job2 ="";
        String param1_job2 ="";
        String param2_job2 ="";
        String param3_job2 ="";
        String job2_adjustmentTime ="";

        //Parameter f�r UDS Jobname
        String job2_name;
        String job2_digitalwert;
        String job2_ventil_links;
        String job2_ventil_rechts;
        boolean compressorAvailable = true;
        //Ediabas
        EdiabasProxyThread ediabas = null;

        boolean ventil = true;

        int fafi_hinten_links_1 = 0;
        int fafi_hinten_links_2 = 0;
        int fafi_hinten_rechts_1 = 0;
        int fafi_hinten_rechts_2 = 0;
        int differenceL = 0;
        int differenceR = 0;

        try {

     /* Pr�fen ob Argumente stimmen
      */
            try {
                if( checkArgs() == false ) throw new PPExecutionException();
                sgbd = getArg(getRequiredArgs()[0]);
            } catch (Exception e) {
                e.printStackTrace();
                result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Package.properties: "+PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }

            try {
                ediabas = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();
            } catch (DeviceNotAvailableException e) {
                result = new Ergebnis( "ExecFehler", "EHCTEST", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }


      /* Namensbelegung der Jobs unterschiedlich je nach Fahrzeugtyp
       */
         

            if(sgbd.equals("EHC_01") == true) {                                 //erg�nzung f�r F01 - pj
                
            //�berpr�fung ob ADJUSTMENT_TIME als Parameter angegeben wurde
            job2_adjustmentTime = getArg(this.getOptionalArgs()[0]);
            if(job2_adjustmentTime==null){
            	job2_adjustmentTime = "10";
            }
                
                job1 = "STATUS_LESEN";
                param1_job1 = "ARG;STATUS_FILTERWERTE";
                erg1_job1 = "STAT_FASTFILTER_RL_WERT";
                erg2_job1 = "STAT_FASTFILTER_RR_WERT";
                job2 = "STEUERN";
                job2_name ="ARG;STEUERN_DIGITALSIGNALE";
                job2_digitalwert ="1";
                job2_ventil_links ="0xC2";
                job2_ventil_rechts="0xC1";

                
                param2_job2 =   job2_name + ";" + job2_digitalwert + ";" + job2_adjustmentTime + ";" + job2_ventil_links;               //ventil hinten links
                param3_job2 =   job2_name + ";" + job2_digitalwert + ";" + job2_adjustmentTime + ";" + job2_ventil_rechts;              //ventil hinten rechts

           //     param2_job2 = "ARG;STEUERN_DIGITALSIGNALE;1;100;17";          //ventil hinten links
           //     param3_job2 = "ARG;STEUERN_DIGITALSIGNALE;1;100;18";          //ventil hinten rechts

                compressorAvailable = false;
            }
            if(sgbd.equals("EHC_E65") == true) {
                job1 = "LESEN_FILTERWERTE";
                param1_job1 = "";
                erg1_job1 = "FASTFILTER_RL";
                erg2_job1 = "FASTFILTER_RR";
                job2 = "STEUERN_DIGITALSIGNALE";
                param1_job2 = "20;1;80";     // Kompressor 8 sec ein
                param2_job2 = "17;1;80";     // Balgventil links 8 sec ein
                param3_job2 = "18;1;80";     // Balgventil rechts 8 sec ein
                job2_adjustmentTime = "8";
                compressorAvailable = true;

            }
            if(sgbd.equals("EHC_70") == true) {                 //erg�nzung f�r e70sgbd - sj
                job1 = "STATUS_FILTERWERTE";                                    //erg�nzung f�r e70sgbd - sj
                param1_job1 = "";
                erg1_job1 = "STAT_FASTFILTER_RL";                               //erg�nzung f�r e70sgbd - sj
                erg2_job1 = "STAT_FASTFILTER_RR";                               //erg�nzung f�r e70sgbd - sj
                job2 = "STEUERN_DIGITALSIGNALE";                                //erg�nzung f�r e70sgbd - sj
                param1_job2 = "20;1;80";                                        // Kompressor 8 sec ein         //erg�nzung f�r e70sgbd - sj
                param2_job2 = "17;1;80";                        // Balgventil links 8 sec ein   //erg�nzung f�r e70sgbd - sj
                param3_job2 = "18;1;80";                        // Balgventil rechts 8 sec ein  //erg�nzung f�r e70sgbd - sj
                job2_adjustmentTime = "8";                     //erg�nzung f�r e70sgbd - sj
                compressorAvailable = true;
            }
            if(sgbd.equals("EHC") == true) {
                job1 = "STATUS_ONLINE_LESEN";
                param1_job1 = "";
                erg1_job1 = "STAT_FAST_FILTER_HL_WERT";
                erg2_job1 = "STAT_FAST_FILTER_HR_WERT";
                job2 = "STATUS_VORGEBEN";
                param1_job2 = "HL_H";
                param2_job2 = ""; //Existiert bei E39 nicht, HL_H steuert sowohl Kompressor als auch Ventil
                ventil = false;
                compressorAvailable = true;
            }
            if(sgbd.equals("EHC_2N2") == true) {
                //Parameter f�r E53 mit 2-Achs-EHC, kommen noch
            }

            // **********************************
            // *                                *
            // * Beginn mit dem LINKERN TEST!!! *
            // *                                *
            // **********************************


            // Funktions- und Vertauscherpr�fung Ventile und Sensoren --> LINKS

            /* Filterwerte lesen
             */
            try {
                temp = ediabas.executeDiagJob(sgbd,job1,param1_job1,"");
                if( temp.equals("OKAY") == false) {
                    result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job1, param1_job1, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
                    ergListe.add(result);
                    throw new PPExecutionException();
                } else {
                    // Linke-Seite messen
//                      fafi_hinten_links_1 = Integer.parseInt( Ediabas.getDiagResultValue(erg1_job1) );
                        fafi_hinten_links_1 = Math.round(Float.parseFloat( ediabas.getDiagResultValue(erg1_job1) ));
                    result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job1, param1_job1, erg1_job1, Integer.toString(fafi_hinten_links_1), "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
                    ergListe.add(result);
                    // Rechte-Seite messen
//                    fafi_hinten_rechts_1 = Integer.parseInt( Ediabas.getDiagResultValue(erg2_job1) );
                    fafi_hinten_rechts_1 = Math.round(Float.parseFloat( ediabas.getDiagResultValue(erg2_job1) ));
                    result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job1, param1_job1, erg2_job1, Integer.toString(fafi_hinten_rechts_1), "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
                    ergListe.add(result);
                }
            } catch( ApiCallFailedException e ) {
                result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job1, param1_job1, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
                ergListe.add(result);
                throw new PPExecutionException();
            }

     /* Pause (kopiert von INPA)
      */
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
            }

     /* Kompressoransteuerung
      * Letzte Parameter (100) ist Zeit (10 Sek)
      * bei E39 gleichzeitig auch Ventilsteuerung
      */
            if(compressorAvailable){
                    try {
                        temp = ediabas.executeDiagJob(sgbd,job2,param1_job2,"");
                        if( temp.equals("OKAY") == false) {
                            result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job2, param1_job2, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
                            ergListe.add(result);
                            throw new PPExecutionException();
                        } else {
                            result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job2, param1_job2, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                            ergListe.add(result);
                        }
                    } catch( ApiCallFailedException e ) {
                        result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job2, param1_job2, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
                        ergListe.add(result);
                        throw new PPExecutionException();
                    }
            }

       /* Ventil hinten links
        * Letzte Parameter (80) ist Zeit (8 Sek)
        */
            if (ventil) {
                try {
                    temp = ediabas.executeDiagJob(sgbd,job2,param2_job2,"");
                    if( temp.equals("OKAY") == false) {
                        result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job2, param2_job2, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
                        ergListe.add(result);
                        throw new PPExecutionException();
                    } else {
                        result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job2, param2_job2, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                        ergListe.add(result);
                    }
                } catch( ApiCallFailedException e ) {
                    result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job2, param2_job2, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
                    ergListe.add(result);
                    throw new PPExecutionException();
                }
            }

     /* Nachricht auf Bildschirm bringen
      */
            try {
                getPr�flingLaufzeitUmgebung().getUserDialog().displayMessage( PB.getString( "anweisung" ), PB.getString( "ehcWirdAngesteuert" ) +" (L)", -1);
            } catch (Exception e) {
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
                else if (e instanceof DeviceNotAvailableException)
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }

     /* Pause (kopiert von INPA)
      * Warte fuer 8 sekunden wahrend der Ansteuerung...
      */
            try {
                Thread.sleep(new Integer(job2_adjustmentTime).intValue()*1000);
            } catch (Exception e) {
            }

     /* Nachricht von Bildschirm loeschen
      */
            try {
                getPr�flingLaufzeitUmgebung().releaseUserDialog();
            } catch (Exception e) {
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
                else if (e instanceof DeviceNotAvailableException)
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }

     /* Filterwerte lesen
      */
            try {
                temp = ediabas.executeDiagJob(sgbd,job1,param1_job1,"");
                if( temp.equals("OKAY") == false) {
                    result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job1, param1_job1, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
                    ergListe.add(result);
                    throw new PPExecutionException();
                } else {
                    // Linke-Seite messen
//                    fafi_hinten_links_2 = Integer.parseInt( Ediabas.getDiagResultValue(erg1_job1) );
                    fafi_hinten_links_2 = Math.round(Float.parseFloat(ediabas.getDiagResultValue(erg1_job1) ));
                    result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job1, param1_job1, erg1_job1, Integer.toString(fafi_hinten_links_2), "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
                    ergListe.add(result);
                    // Rechte-Seite messen
//                    fafi_hinten_rechts_2 = Integer.parseInt( Ediabas.getDiagResultValue(erg2_job1) );
                    fafi_hinten_rechts_2 = Math.round(Float.parseFloat( ediabas.getDiagResultValue(erg2_job1) ));
                    result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job1, param1_job1, erg2_job1, Integer.toString(fafi_hinten_rechts_2), "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
                    ergListe.add(result);
                }
            } catch( ApiCallFailedException e ) {
                result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job1, param1_job1, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
                ergListe.add(result);
                throw new PPExecutionException();
            }


     /* Analyse von Ergebnissen:
      * Formel: Die Differenz der linken Werte minus der Differenz der rechten Werte, mu� > 0 sein!
      */

                differenceL = (fafi_hinten_links_2 - fafi_hinten_links_1) - (fafi_hinten_rechts_2 - fafi_hinten_rechts_1);
                if (differenceL > 0) {
                    result = new Ergebnis( "Analyse", "Result", "Linke_Seite", "", "", "Result", Integer.toString(differenceL), "0", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                    ergListe.add(result);
                }
                else {
                    result = new Ergebnis( "Analyse", "Result", "Linke_Seite", "", "", "Result", Integer.toString(differenceL), "0", "", "0", "", "", "", "Ausf�hrungsfehler - Test Luftfeder links (Messwerte = 0)", "", Ergebnis.FT_NIO );
                    ergListe.add(result);
                    throw new PPExecutionException();
                }



            // ********************************
            // *                              *
            // * ab hier hier RECHTER TEST!!! *
            // *                              *
            // ********************************

            // Funktions- und Vertauscherpr�fung Ventile und Sensoren -->RECHTS

            fafi_hinten_links_1 = 0;
            fafi_hinten_links_2 = 0;
            fafi_hinten_rechts_1 = 0;
            fafi_hinten_rechts_2 = 0;

       /* Filterwerte lesen
        */
            try {
                temp = ediabas.executeDiagJob(sgbd,job1,param1_job1,"");
                if( temp.equals("OKAY") == false) {
                    result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job1, param1_job1, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
                    ergListe.add(result);
                    throw new PPExecutionException();
                } else {
                    // Linke-Seite messen
//                    fafi_hinten_links_1 = Integer.parseInt( Ediabas.getDiagResultValue(erg1_job1) );
                    fafi_hinten_links_1 = Math.round(Float.parseFloat( ediabas.getDiagResultValue(erg1_job1) ));
                    result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job1, param1_job1, erg1_job1, Integer.toString(fafi_hinten_links_1), "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
                    ergListe.add(result);
                    // Rechte-Seite messen
//                    fafi_hinten_rechts_1 = Integer.parseInt( Ediabas.getDiagResultValue(erg2_job1) );
                    fafi_hinten_rechts_1 = Math.round(Float.parseFloat( ediabas.getDiagResultValue(erg2_job1) ));
                    result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job1, param1_job1, erg2_job1, Integer.toString(fafi_hinten_rechts_1), "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
                    ergListe.add(result);
                }
            } catch( ApiCallFailedException e ) {
                result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job1, param1_job1, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
                ergListe.add(result);
                throw new PPExecutionException();
            }

     /* Pause (kopiert von INPA)
      */
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
            }

     /* Kompressoransteuerung
      * Letzte Parameter (100) ist Zeit (10 Sek)
      * bei E39 gleichzeitig auch Ventilsteuerung
      */
            if(compressorAvailable){
                    try {
                        temp = ediabas.executeDiagJob(sgbd,job2,param1_job2,"");
                        if( temp.equals("OKAY") == false) {
                            result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job2, param1_job2, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
                            ergListe.add(result);
                            throw new PPExecutionException();
                        } else {
                            result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job2, param1_job2, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                            ergListe.add(result);
                        }
                    } catch( ApiCallFailedException e ) {
                        result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job2, param1_job2, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
                        ergListe.add(result);
                        throw new PPExecutionException();
                    }
            }
     /* Ventil hinten rechts
      * Letzte Parameter (80) ist Zeit (8 Sek)
      */
           if (ventil) {
               try {
                   temp = ediabas.executeDiagJob(sgbd,job2,param3_job2,"");
                   if( temp.equals("OKAY") == false) {
                       result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job2, param3_job2, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
                       ergListe.add(result);
                       throw new PPExecutionException();
                   } else {
                       result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job2, param3_job2, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                       ergListe.add(result);
                   }
               } catch( ApiCallFailedException e ) {
                   result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job2, param3_job2, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
                   ergListe.add(result);
                   throw new PPExecutionException();
               }

           }

     /* Nachricht auf Bildschirm bringen
      */
            try {
                getPr�flingLaufzeitUmgebung().getUserDialog().displayMessage( PB.getString( "anweisung" ), PB.getString( "ehcWirdAngesteuert" ) + " (R)", -1);
            } catch (Exception e) {
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
                else if (e instanceof DeviceNotAvailableException)
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }

     /* Pause (kopiert von INPA)
      * Warte fuer 8 sekunden wahrend der Ansteuerung...
      */
            try {
                Thread.sleep(new Integer(job2_adjustmentTime).intValue()*1000);
            } catch (Exception e) {
            }

     /* Nachricht von Bildschirm loeschen
      */
            try {
                getPr�flingLaufzeitUmgebung().releaseUserDialog();
            } catch (Exception e) {
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
                else if (e instanceof DeviceNotAvailableException)
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }

     /* Filterwerte lesen
      */
            try {
                temp = ediabas.executeDiagJob(sgbd,job1,param1_job1,"");
                if( temp.equals("OKAY") == false) {
                    result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job1, param1_job1, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
                    ergListe.add(result);
                    throw new PPExecutionException();
                } else {
                    // Linke-Seite messen
//                    fafi_hinten_links_2 = Integer.parseInt( edibas.getDiagResultValue(erg1_job1) );
                    fafi_hinten_links_2 = Math.round(Float.parseFloat( ediabas.getDiagResultValue(erg1_job1) ));
                    result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job1, param1_job1, erg1_job1, Integer.toString(fafi_hinten_links_2), "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
                    ergListe.add(result);
                    // Rechte-Seite messen
//                    fafi_hinten_rechts_2 = Integer.parseInt( edibas.getDiagResultValue(erg2_job1) );
                    fafi_hinten_rechts_2 = Math.round(Float.parseFloat( ediabas.getDiagResultValue(erg2_job1) ));
                    result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job1, param1_job1, erg2_job1, Integer.toString(fafi_hinten_rechts_2), "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
                    ergListe.add(result);
                }
            } catch( ApiCallFailedException e ) {
                result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job1, param1_job1, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode()+": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
                ergListe.add(result);
                throw new PPExecutionException();
            }


     /* Analyse von Ergebnissen:
      * Formel: Die Differenz der rechten Werte minus der Differenz der linken Werte, mu� > 0 sein!
      */

                differenceR = (fafi_hinten_rechts_2 - fafi_hinten_rechts_1) - (fafi_hinten_links_2 - fafi_hinten_links_1);
                if (differenceR > 0) {
                    result = new Ergebnis( "Analyse", "Result", "Rechte_Seite", "", "", "Result", Integer.toString(differenceR), "0", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                    ergListe.add(result);
                }
                else {
                    result = new Ergebnis( "Analyse", "Result", "Rechte_Seite", "", "", "Result", Integer.toString(differenceR), "0", "", "0", "", "", "", "Ausf�hrungsfehler - Test Luftfeder rechts (Messwerte = 0)", "", Ergebnis.FT_NIO );
                    ergListe.add(result);
                    throw new PPExecutionException();
                }


            /*
            // ist bereits oben gepr�ft
            if (differenceL <= 0 )
                throw new PPExecutionException();

            */
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
            e.printStackTrace();
        } catch (Throwable e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }

        setPPStatus( info, status, ergListe );
    } // End of execute()

} // End of Class


/*
 * Created on 17.06.2005
 * UiLogEnd
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.Vector;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * @author Dirk Gehring
 *
 * UiLogEnd
 * beendet den TransientenModus Continuous
 * @param keine
 */
public class UiLogEnd_5_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public UiLogEnd_5_0_F_Pruefprozedur() {
	}

	/**
	 * @param pruefling
	 * @param pruefprozName
	 * @param hasToBeExecuted
	 */
	public UiLogEnd_5_0_F_Pruefprozedur(Pruefling pruefling,
			String pruefprozName, Boolean hasToBeExecuted) {
		super(pruefling, pruefprozName, hasToBeExecuted);
		attributeInit();
	}
	
	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = {"AWT"};
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = new String[0];
		return args;
	}
	
	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 */
	public boolean checkArgs() {
		boolean ok;
		try {
			ok = super.checkArgs();
			return ok;
		} catch (Exception e) {
			return false;
		} catch (Throwable e) {
			return false;
		}
	}
	public void execute(ExecutionInfo info) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;
		
		//spezifische Variablen
		UIAnalyserEcos myUI = null; 	//Device UIAnalyser
		boolean ECOS_TRACE_CONTINUOUS=false;

		String awt = null;
		UserDialog myDialog = null;
		String temp=null;		//Tempor�re Variable zur Initialisation der optionalen Parameter		
		boolean udInUse = false; //Ob eine Meldung auf dem Handterminal dargestellt wird
		
		try {//Auswerten der Argumente
			try {
				if (checkArgs() == false)
					throw new PPExecutionException(PB
							.getString("parameterexistenz"));
				//AWT
				temp = getArg(getOptionalArgs()[0]);
				if(temp!=null){
					awt = temp;
				}
				else{
					//Default-Wert="";
					awt = "";
				}
			} catch (PPExecutionException e) {
				if (e.getMessage() != null)
					result = new Ergebnis("ParaFehler", "", "", "", "", "", "",
							"", "", "0", "", "", "", PB
									.getString("parametrierfehler"), e
									.getMessage(), Ergebnis.FT_NIO_SYS);
				else
					result = new Ergebnis("ParaFehler", "", "", "", "", "", "",
							"", "", "0", "", "", "", PB
									.getString("parametrierfehler"), "",
							Ergebnis.FT_NIO_SYS);
				ergListe.add(result);
				throw new PPExecutionException();
			}//Ende Auwerten der Argumente

			try {//Holen des Devices UIAnalyser
				myUI = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			} catch (Exception e) {
				//Systemfehler, muss man genau analysieren
				e.printStackTrace();
				if (e instanceof DeviceLockedException)
					result = new Ergebnis("ExecFehler", "UIAnalyser", "", "",
							"", "", "", "", "", "0", "", "", "",
							e.getMessage(), "DeviceLockedException",
							Ergebnis.FT_NIO_SYS);
				else if (e instanceof DeviceNotAvailableException)
					result = new Ergebnis("ExecFehler", "UIAnalyser", "", "",
							"", "", "", "", "", "0", "", "", "",
							e.getMessage(), "DeviceNotAvailableException",
							Ergebnis.FT_NIO_SYS);
				else
					result = new Ergebnis("ExecFehler", "UIAnalyser", "", "",
							"", "", "", "", "", "0", "", "", "",
							e.getMessage(), "Exception", Ergebnis.FT_NIO_SYS);
				ergListe.add(result);
				throw new PPExecutionException();
			}//Ende Holen des Devices UIAnalyser
			
			try {//Holen der Pr�fstandsvariablen TransientenModus
				ECOS_TRACE_CONTINUOUS =((Boolean)getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PS_CONFIG,"ECOS_TRACE_CONTINUOUS")).booleanValue();
			} catch (Exception e) {
				if (e instanceof VariablesException) {
					//Falls keine Pr�fstandsvariable angegeben ist, oder es tritt beim Abholen ein Fehler auf
					// TransientenModus inaktiv
					ECOS_TRACE_CONTINUOUS = false;
				} else {
					throw new PPExecutionException();
				}
			}//Ende Holen der Pr�fstandsvariablen
			
			
			//Einzige Aufgabe dieser PP
			if(ECOS_TRACE_CONTINUOUS){
				try {
					//AWT wird auf dem Bildschirm dargestellt
					myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
					myDialog.displayMessage(PB.getString("anweisung"), awt, -1);
					udInUse = true;
				} catch (Exception e) {
					//Systemfehler, muss man genau analysieren
					e.printStackTrace();
					result = new Ergebnis("ExecFehler", "Userdialog", "", "",
							"", "", "", "", "", "0", "", "", "",
							e.getMessage(), PB
									.getString("unerwarteterLaufzeitfehler"),
							Ergebnis.FT_NIO_SYS);
					ergListe.add(result);
					throw new PPExecutionException("Get UserDialog");
				}
				myUI.StopTransientRecording();
			}

		} catch (PPExecutionException e) {
			status = STATUS_EXECUTION_ERROR;
		} catch (Exception e) {
			//Systemfehler, muss man genau analysieren
			e.printStackTrace();
			result = new Ergebnis("ExecFehler", "", "", "", "", "", "", "", "",
					"0", "", "", "", e.getMessage(), PB
							.getString("unerwarteterLaufzeitfehler"),
					Ergebnis.FT_NIO_SYS);
			ergListe.add(result);
			status = STATUS_EXECUTION_ERROR;
		} catch (Throwable e) {
			//Systemfehler, muss man genau analysieren
			e.printStackTrace();
			result = new Ergebnis("ExecFehler", "", "", "", "", "", "", "", "",
					"0", "", "", "", e.getMessage(), PB
							.getString("unerwarteterLaufzeitfehler"),
					Ergebnis.FT_NIO_SYS);
			ergListe.add(result);
			status = STATUS_EXECUTION_ERROR;
		}

		if(myUI != null){
			try {//Device UIAnalyser wird freigegeben
				getPr�flingLaufzeitUmgebung().getDeviceManager()
						.releaseUIAnalyserEcos();
			} catch (Exception e) {
				//Systemfehler, muss man genau analysieren
				e.printStackTrace();
				if (e instanceof DeviceLockedException)
					result = new Ergebnis("ExecFehler", "UIAnalyser", "", "", "",
							"", "", "", "", "0", "", "", "", e.getMessage(),
							"DeviceLockedException", Ergebnis.FT_NIO_SYS);
				else
					result = new Ergebnis("ExecFehler", "UIAnalyser", "", "", "",
							"", "", "", "", "0", "", "", "", e.getMessage(),
							"release", Ergebnis.FT_NIO_SYS);
				ergListe.add(result);
				status = STATUS_EXECUTION_ERROR;
			}
		}
		myUI = null;
        //UserDialog wird frei gegeben
		if (udInUse == true) {
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch (Exception e) {
				//Systemfehler, muss man genau analysieren
				e.printStackTrace();
				if (e instanceof DeviceLockedException)
					result = new Ergebnis("ExecFehler", "Userdialog", "", "",
							"", "", "", "", "", "0", "", "", "",
							e.getMessage(), "DeviceLockedException",
							Ergebnis.FT_NIO_SYS);
				else
					result = new Ergebnis("ExecFehler", "Userdialog", "", "",
							"", "", "", "", "", "0", "", "", "",
							e.getMessage(), "release", Ergebnis.FT_NIO_SYS);
				ergListe.add(result);
				status = STATUS_EXECUTION_ERROR;
			}
		}
		//Status setzen
		setPPStatus(info, status, ergListe);

	}//Ende execute
}

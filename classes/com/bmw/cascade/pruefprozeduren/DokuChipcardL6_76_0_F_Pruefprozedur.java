/*
 * DokuChipcardL6_x_y_Pruefprozedur.java
 *
 * Created on 11.04.2007
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.util.dom.DomPruefstand;

/** Ermittelt �ber funktionale Jobs alle angeschlossenen Steuerger�te und Sensoren
 *
 * @author Daniel Frey, Claudia Wolf, Thomas Buboltz
 * @version V1_0_F  01.01.2007 FD Initialversion
 * @version V3_0_T  04.09.2007 FD Fehlermeldung bei AUTO, wenn 2 sensoren mit gleicher ID gemeldet werden.
 *                                BN2K: Erfassen weiterer Daten
 *                                Batteriesensor handling
 * @version V4_0_F  04.09.2007 FD F-Version                               
 * @version V5_0_T  21.09.2007 FD Sinnvolle Fehlermeldung, wenn PL Attribute falsch bedated
 *                                Pr�fung auf gleichen Sensor in 2 verschiedenen SGs
 *                                physikalisches Nachfragen, wenn SG bei funktionalem Aufruf nicht antwortet
 *                                Defaults: DOMDOKU_ECU=TRUE, DOMDOKU_SENSOR=IGNORE
 *                                IBS Software Version dokumentieren
 *                                L6 Lieferantennummer nicht mehr dokumentieren
 * @version V6_0_F  21.09.2007 FD F-Version
 * @version V7_0_T  14.11.2007 FD ECUs, die keinen Pr�fling haben, k�nnen mit dem Parameter DOMDOKU_IGNORE= ignoriert werden.
 * @version V8_0_F  14.11.2007 FD F-Version 
 * @version V9_0_T  16.11.2007 FD Fehler abfangen, falls IBS SGBD nicht ermittelt werden kann (bei ExtractValues)
 * @version V10_0_F 17.01.2008 FD F-Version       
 * @version V11_0_T 31.01.2008 FD Job/Result f�r IBS charging balance ermitteln �ber optionale Parameter setzbar                       
 * @version V12_0_F 31.01.2008 FD F-Version   
 * @version V13_0_T 18.02.2008 FD SGBD Namen als optionale Parameter SGBD_UDS, SGBD_BN2K (Defaults: F01, F01BN2K)
 *                                Bei BN2K kann die Seriennummer alternativ auch schon bei IDENT_FUNKTIONAL ermittelt werden.
 * @version V14_0_F 18.02.2008 FD F-Version
 * @version V15_0_T 22.04.2008 FD manufactoringDate und manufacturerCode im XML umgedreht
 *                                Wenn keine SoftwareVersionen, dann auch kein software tag
 * @version V16_0_F 22.04.2008 FD F-Version
 * @version V19_0_T 16.09.2008 FD Schema Version 1.02 -> 1.03
 *                                Neuer opt. Parameter IBS_CHARGING_BALANCE_PARAMETER
 * @version V20_0_F 16.09.2008 FD F-Version
 * @version V21_0_T 16.09.2008 FD Neuer opt. Parameter IBS_CHARGING_BALANCE_SGBD
 * @version V22_0_F 16.09.2008 FD F-Version
 * @version V23_0_T 24.11.2008 FD Mind. ein IO Ergebnis: Gefundene SGs
 *                                (Wird auch im NIO Fall generiert. In dem Fall gibt es aber auch NIO Ergebnisse)
 * @version V24_0_F 24.11.2008 FD F-Version
 * @version V25_0_T 26.01.2010 FD BN2K Aufrufe abschaltbar durch SGBD_BN2K=NONE                                
 * @version V26_0_F 26.01.2010 FD F-Version
 * @version V27_0_T 06.09.2010 FD Bugfix: For i_stufe_lesen auch konfigurierte SGBD nehmen, statt hartcodiert "F01"
 * @version V28_0_F 06.09.2010 FD F-Version
 * @version V29_0_T	01.02.2011 FD Workaround, sodass beim Airbag-SG (ACSM4) die Sensoren physikalisch ausgelesen werden
 * @version V30_0_F 01.02.2011 FD F-Version
 * @version V33_0_T 16.02.2011 FD Job for reading uds sensors configurable
 *                                New sensor doku flag: AUTO_PHYS
 * @version V34_0_F 16.02.2011 FD F-Version
 * @version V35_0_T 01.07.2011 LG SGBD index conversion to hexadecimal (IG_DOM field restriction)
 * @version V36_0_F 26.09.2011 LG Freigabe
 * @version V37_0_T 22.02.2012 FD T-Version Default f�r Herstelldatum Lesen auf true gesetzt
 * @version V38_0_T 07.03.2012 TB T-Version Herstelldatum Lesen wieder auf false gesetzt, weil damit Fehler auftraten.
 * @version V39_0_T 11.04.2012 FD T_Version Neuer optionaler Parameter: SENSOR_READ_FUNCTIONAL
 *                                          Wenn FALSE (default): Sensoren werden rein physikalisch ausgelesen.
 *                                          Das ist sinnvoll wenn Steuerger�te, deren Sensoren nicht interessieren, bei 
 *                                          Sensor_lesen_functional den Job und damit den gesamten Pr�fschritt ausbremsen.
 * @version V40_0_F 11.04.2012 FD F-Version
 * @version V41_0_T 14.06.2012 FD T-Version Herstelldatum Lesen �ber F01.prg (Braucht F01.prg Version >=1.16)
 * @version V42_0_F 14.06.2012 FD F-Version
 * @version V43_0_T 12.09.2012 FD T-Version Wenn Physical Job fehlschl�gt: Ecu Gruppe in Fehlermeldung ausgeben. 
 * @version V44_0_F 27.09.2012 FD F-Version
 * @version V45_0_T 05.03.2013 FD T-Version CALID CVN lesen und dokumentieren 
 * @version V46_0_T 19.03.2013 FD T-Version CALID CVN: auch Position dokumentieren
 * @version V47_0_T 07.05.2013 FD T-Version Hotfix aus 46_1: TCB nicht dokumentieren f�r alle SGs im Seriennummerbereich: 0000333600 bis 0000365823
 * @version V48_0_T 18.06.2013 FD T-Version Schemaanpassung carbValues, nur funktionaler Job wird ausgewertet
 * @version V49_0_F 18.06.2012 FD F-Version
 * @version V50_0_T 25.06.2013 FD T-Version Schemaanpassung carbValues: <carbValues> wird nur erzeugt, wenn es mind. einen Eintrag gibt
 * @version V51_0_F 25.06.2013 FD F-Version
 * @version V52_0_T 05.11.2013 FD T-Version Hotfix aus 46_1 entfernt 
 *                                          Kein "calid_cvn_lesen_funktional" bei Motorrad
 * @version V53_0_F 05.11.2013 FD F-Version
 * @version V54_0_T 28.05.2014 CW F-Version Parameter DOMDOKU_IBS_JOB und DOMDOKU_IBS_JOB_PAR hinzugef�gt, da der ident-job beim IBS2015 als Status_lesen Job mit Argument und
 * 											mit anderem Namen als IdentIBS aufzurufen ist. Die abzufragenden Ergebnisse beim IdentIBS wurden ebenfalls angepasst.
 * @version V55_0_F 28.05.2014 CW F-Version
 * @version V56_0_T 20.10.2014 TB T-Version DOMDOKU_IBS_JOB_PAR ausgebaut, weiterhin soll die IBS: Ladebilanz nicht dokumentiert werden, wenn die IBS_CHARGING_BALANCE_JOB fehlt, Jobs f�r IBS 2015 korrigiert
 * @version V57_0_T 24.10.2014 TB T-Version Mit DOMDOKU_IBS = FALSE werden keine Batteriesensor Themen gemacht
 * @version V58_0_T 01.12.2014 TB T-Version Korrektur IBS2015 Result f�r die Seriennummer: STAT_SERIENNUMMER_IBS2015_DATA -> STAT_SERIENNUMMER_IBS2015_WERT 
 * @version V59_0_T 01.12.2014 TB T-Version Korrektur IBS2015 Result f�r die Seriennummer: STAT_SERIENNUMMER_IBS2015_DATA -> STAT_SERIENNUMMER_IBS2015_WERT, Datentyp noch angepasst -> Seriennummer als String behandelen
 * @version V60_0_F 10.12.2014 TB F-Version
 * @version V61_0_T 09.02.2015 TB T-Version LOP 1897: Die PP "DokuChipcardL6" dahingehend erweitern, dass bei der Steuerung der Sensorbehandlung �ber den jeweiligen Pr�fling ein "DOMDOKU_SENSOR_XXXX = IGNORE" den Fehler bei einem leeren Ergebnissatz ("API-0014: RESULT NOT FOUND") nicht als NIO bewertet, Generics.  
 * @version V62_0_F 09.02.2015 TB F-Version
 * @version V63_0_T 11.02.2015 TB T-Version Erweiterung zu LOP 1897: fange sicherheitshalber alle "API-0014 RESULT NOT FOUND" Fehler beim Sensoren Ident Lesen ab.    
 * @version V64_0_F 11.02.2015 TB F-Version
 * @version V65_0_T 14.10.2015 TB T-Version LOP 1909: Erweiterung zur Erfassung des elektr. HW �nderungsindexes, Anpassung des Datenformates beim Batteriesensor ab SP 2015 auf apiResultText, DOM-Schema Version von V01.04 -> V01.07, Klartext des Sensors ermitteln und im Fehlerfall mit ausgeben <BR> 
 * @version V66_0_F 12.01.2016 TB F-Version
 * @version V67_0_T 19.03.2017 TB T-Version LOP 2119: Im Rahmen Einf�hrung des Zertifikatemgmt. SP 2018 sollen die dort erstmals genutzten eineindeutigen SG-ID's: ecuUID's �ber DokuChipcard und DOM erfasst und dokumentiert werden. ecuUID (16 HexByte: 0-9, A-F). <BR>
 *  				14.03.2017 TB T-Version LOP 2185: Parametrierfehler "Es wurden mehrere Sensoren mit Verbauort 0x..." gemeldet behoben <BR>
 *                  15.05.2017 TB T-Version LOP 2198: Vorbereitende Fehlertoleranz f�r die Einf�hung der F01.prg. Beim Flag Sensor dokumentieren = AUTO od. AUTO_PHYS wird ein Job Status: "ERROR_ECU_REQUEST_OUT_OF_RANGE" ignoriert und ausgeblendet. <BR>
 * @version V68_0_T 22.05.2017 TB T-Version LOP 2199: BMW Motorrad nutzt neben der fkt. SGBD (X_K001.prg), ebenfalls die fkt. SGBD (X_KS01). Diese ebenfalls ber�cksichtigen. <BR>
 * @version V69_0_T 27.06.2017 TB T-Version LOP 2211: Das DSC-SG bei M-Fahrzeugen beantwortet den neuen Service bzgl. ECUUID fehlerhaft, obwohl es diesen Service nicht unterst�tzt. Normalerweise ein EUC-Bug, trotzdem wurde die PP Robustheit dbzgl. nochmal verbessert. <BR>
 * @version V70_0_T 27.06.2017 TB T-Version LOP 2211: BugFix: Beim phys. Nachadressieren des neuen Jobs gab es noch einen Fehler, der die Pr�fung NIO machte, wenn der Job inkorrekt funktioniert.<BR>
 * @version V71_0_F 27.06.2017 TB F-Version<BR>
 * @version V72_0_T 13.09.2017 TB T-Version LOP 2248: Das RangeExtender SG (i3-Fahrzeuge) antwort fehlerhaft auf den Service: "hw_modification_index_lesen_funktional". Laut E-Bereich (C. Gebhart) ist der Job erst f�r neue auf das SP2021 basierende SG verpflichtend<BR>
 * @version V73_0_F 25.09.2017 TB F-Version<BR>
 * @version V74_0_T 19.11.2017 TB T-Version LOP 2275: AQ-Anforderung - Fuer das SRR SG HC2_G11.prg uebergangsweise die Sensoren mit Spezialjob auslesen und sofern vorhanden dokumentieren. TODO: Kann 2020 wieder entfallen. <BR>
 * @version V75_0_T 09.01.2018 TB T-Version LOP 2294: Erfassung und Dokumentation der CAL-ID, CVN Daten ebenfalls fuer Motorrad aktivieren. <BR>
 * @version V76_0_F 10.01.2018 TB F-Version<BR>
 */
public class DokuChipcardL6_76_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	/** */
	static final long serialVersionUID = 1L;

	/** Debug Level: 0 keine Ausgaben 
	 *               10-20 zu�stzliche Debug-Ausgaben in Pr�fstandScreen.log
	 */
	private int m_debug_level;

	/** Log-Prefix */
	private final String LOG_PREFIX = ("PP " + this.getClass().getSimpleName() + ": ").replaceFirst( "_Pruefprozedur", "" );
	/** Fehler-ID */
	private final String EDIABASERROR = "EdiabasError";
	/** Fehler-ID */
	private final String ECUMISSING = "EcuMissing";
	/** Fehler-ID */
	private final String SENSORMISSING = "SensorMissing";
	/** Fehler-ID */
	private final String ECUANSWERED = "WrongECUAnswered";
	/** Fehler-ID */
	private final String SENSORANSWERED = "WrongSensorAnswered";
	/** Fehler-ID */
	private final String GROUPADDRMISMATCH = "GroupAddressMismatch";
	/** Fehler-ID */
	private final String TRANSFERFAILED = "TransferFailed";
	/** Fehler-ID */
	private final String WRONGNET = "WrongBoardNet";
	/** Fehler-ID */
	private final String ADDRESSCONFLICT = "ADDRESSCONFLICT";
	/** Fehler-ID */
	private final String SGBDMISSING = "SGBDMISSING";
	/** Fehler-ID */
	private final String NOFUNCANSWER = "NOFUNCANSWER";

	/** Kennung, falls ExtractValues f�r IBS SGBD nicht funktioniert hat */
	private final String IBS_SGBD_ERROR = "IBS_SGBD_ERROR";

	/** PL-Attribut f�r Ecu vorhanden+Dokumentieren / nicht vorhanden*/
	private final String PL_DOMDOKU_ECU = "DOMDOKU_ECU";
	/** PL-Attribut f�r Ecu Seriennummer dokumentieren ja / nein */
	private final String PL_DOMDOKU_ECU_SER = "DOMDOKU_ECU_SER";
	/** PL-Attribut f�r Ecu UID-Seriennummer dokumentieren ja / nein */
	private final String PL_DOMDOKU_ECU_UID = "DOMDOKU_ECU_UID";
	/** PL-Attribut f�r Sensor vorhanden+Dokumentieren / nicht vorhanden*/
	private final String PL_DOMDOKU_SENSOR = "DOMDOKU_SENSOR";
	/** PL-Attribut f�r Ecu Herstelldatum dokumentieren */
	private final String PL_DOMDOKU_READHERSTELLDATUM = "DOMDOKU_ECU_MANUFACTURING_DATE";
	/** PL-Attribut f�r Ecu Lieferantennummer dokumentieren */
	//private final String PL_DOMDOKU_READLIEFERANTENNUMMER="DOMDOKU_ECU_SUPPLIER_ID";

	/** Ecu/Sensor nicht erwartet, nicht dokumentieren */
	private final int PL_FLAG_FALSE = 0;
	/** Ecu/Sensor erwartet, dokumentieren */
	private final int PL_FLAG_TRUE = 1;
	/** Ecu/Sensor nicht dokumentieren, egal ob vorhanden oder nicht */
	private final int PL_FLAG_IGNORE = 2;
	/** Ecu dokumentieren, eventuelle Fehler nur FT_IGNORE nicht FT_NIO */
	private final int PL_FLAG_IGNOREERR = 3;
	/** Es wird nicht festgelegt, welche Sensoren erwartet werden.
	 * Es gibt keine erwarteten oder unerwarteten Sensoren.
	 * Die gefundenen Sensoren dokumentieren. */
	private final int PL_FLAG_AUTO = 4;
	/** Es wird nicht festgelegt, welche Sensoren erwartet werden.
	 * Es gibt keine erwarteten oder unerwarteten Sensoren.
	 * Die gefundenen Sensoren dokumentieren. 
	 * Wenn sich das SG funktional keine Sensoren meldet, physikalisch nachfrage */
	private final int PL_FLAG_AUTO_PHYS = 7;
	/** Bei Sensor (neben VerbauortNr) nur Seriennummer dokumentieren */
	private final int PL_FLAG_SER = 5;
	/** Bei Sensor (neben VerbauortNr) nur Sachnummer dokumentieren */
	private final int PL_FLAG_SNR = 6;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DokuChipcardL6_76_0_F_Pruefprozedur() {
	}

	/** Erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DokuChipcardL6_76_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 * 
	 * @return zwingende Argumente als String Array
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = {};
		return args;
	}

	/**
	 * liefert die optionalen Argumente
	 * 
	 * @return opt. Argumente als String Array
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG", "DOMDOKU_IBS", "DOMDOKU_IBS_SGBD", "DOMDOKU_IGNORE", "IBS_CHARGING_BALANCE_JOB", "IBS_CHARGING_BALANCE_RESULT", "SGBD_UDS", "SGBD_BN2K", "IBS_CHARGING_BALANCE_PARAMETER", "IBS_CHARGING_BALANCE_SGBD", "SENSOR_JOB_UDS", "SENSOR_READ_FUNCTIONAL", "DOMDOKU_IBS_JOB" };
		return args;
	}

	/**
	 * �berpr�fung der Argumente
	 * 
	 * @return false: Argumente fehlerhaft, true: Argumente IO
	 */
	@Override
	public boolean checkArgs() {
		Vector<Ergebnis> errors = new Vector<Ergebnis>();
		return checkArgs( errors );
	}

	/** �berpr�ft die Argumente f�r diese PP
	 * 
	 * @param errors Fehlerliste
	 * @return false: Argumente fehlerhaft, true: Argumente IO
	 */
	public boolean checkArgs( Vector<Ergebnis> errors ) {
		Ergebnis result;

		try {
			if( !super.checkArgs() ) {
				if( isDE() )
					result = new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error", Ergebnis.FT_NIO_SYS );
				errors.add( result );
				return false;
			}

			return true;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/** Funktionale SGBD f�r UDS */
	private String m_SGBD_UDS;
	/** Funktionale SGBD f�r Bordnetz 2000 */
	private String m_SGBD_BN2K;

	/** FlagNummer 
	 * PL_FLAG_FALSE, PL_FLAG_TRUE usw...
	 * in String umwandeln
	 * 
	 * @param FlagInt Doku Flag
	 * @return Flag als String
	 */
	private String flagIntToString( int FlagInt ) {
		switch( FlagInt ) {
			case PL_FLAG_FALSE:
				return "FALSE";
			case PL_FLAG_TRUE:
				return "TRUE";
			case PL_FLAG_IGNORE:
				return "IGNORE";
			case PL_FLAG_IGNOREERR:
				return "IGNOREERR";
			case PL_FLAG_AUTO:
				return "AUTO";
			case PL_FLAG_AUTO_PHYS:
				return "AUTO_PHYS";
			case PL_FLAG_SER:
				return "SER";
			case PL_FLAG_SNR:
				return "SNR";
		}
		return Integer.toString( FlagInt );
	}

	/** f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		int status = STATUS_EXECUTION_OK;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		EdiabasProxyThread ept;
		ECU_Infos EcuInfos;
		ECU_Info EcuInfo;
		Vector<CError> tolerableErrors = new Vector<CError>(); // Fehler die nicht zu einem sofortigen Abbruch f�hren
		int i;
		Ergebnis entry;
		String debuglevel_str;
		String domdoku_ibs;
		String domdoku_ibs_sgbd;
		String domdoku_ibs_job;
		String domdoku_ignore_str;
		String[] domdoku_ignore_ecus_str;
		String sensor_job_uds;
		String sensor_read_functional;
		boolean sensor_read_functional_b;
		String ibs_charging_balance_sgbd;
		String ibs_charging_balance_job;
		String ibs_charging_balance_parameter;
		String ibs_charging_balance_result;
		int h;
		String str;

		try {
			debuglevel_str = getArg( "DEBUG" );
			if( debuglevel_str == null )
				m_debug_level = 0;
			else if( debuglevel_str.length() == 0 )
				m_debug_level = 0;
			else if( "TRUE".equalsIgnoreCase( debuglevel_str ) )
				m_debug_level = 20;
			else if( "FALSE".equalsIgnoreCase( debuglevel_str ) )
				m_debug_level = 0;
			else {
				try {
					m_debug_level = Integer.parseInt( debuglevel_str );
					if( m_debug_level < 0 )
						m_debug_level = 0;
				} catch( NumberFormatException nfe ) {
					debuglevel_str = "DEBUG (=" + debuglevel_str + ")";
					if( isDE() )
						System.out.println( LOG_PREFIX + "DokuChipcardL6.execute: " + debuglevel_str + " ung�ltig" );
					else
						System.out.println( LOG_PREFIX + "DokuChipcardL6.execute: " + debuglevel_str + " invalid" );
					if( isDE() )
						throw new CError( "ParameterFehler", "Parameter " + debuglevel_str + " ung�ltig", "Erlaubt: TRUE, FALSE, 0-20" );
					else
						throw new CError( "ParameterError", "parameter " + debuglevel_str + " invalid", "Allowed: TRUE, FALSE, 0-20" );
				}
			}

			if( m_debug_level >= 10 ) {
				System.out.println( LOG_PREFIX + "### DEBUG LEVEL: " + m_debug_level + " ###" );
			}

			m_SGBD_UDS = getArg( getOptionalArgs()[6] );
			if( m_SGBD_UDS == null )
				m_SGBD_UDS = "";
			m_SGBD_UDS = m_SGBD_UDS.trim();
			if( m_SGBD_UDS.length() == 0 )
				m_SGBD_UDS = "F01";

			m_SGBD_BN2K = getArg( getOptionalArgs()[7] );
			if( m_SGBD_BN2K == null )
				m_SGBD_BN2K = "";
			m_SGBD_BN2K = m_SGBD_BN2K.trim();
			if( m_SGBD_BN2K.length() == 0 )
				m_SGBD_BN2K = "F01BN2K";

			sensor_job_uds = getArg( getOptionalArgs()[10] );
			if( sensor_job_uds == null )
				sensor_job_uds = "";
			sensor_job_uds = sensor_job_uds.trim();
			if( sensor_job_uds.length() == 0 )
				sensor_job_uds = "sensoren_ident_lesen_funktional";

			sensor_read_functional = getArg( getOptionalArgs()[11] );
			if( sensor_read_functional == null )
				sensor_read_functional = "FALSE";
			sensor_read_functional = sensor_read_functional.trim();
			sensor_read_functional_b = sensor_read_functional.equalsIgnoreCase( "TRUE" );

			if( m_debug_level >= 11 ) {
				System.out.println( getOptionalArgs()[6] + "=" + m_SGBD_UDS );
				System.out.println( getOptionalArgs()[7] + "=" + m_SGBD_BN2K );
				System.out.println( getOptionalArgs()[10] + "=" + sensor_job_uds );
				System.out.println( getOptionalArgs()[11] + "=" + sensor_read_functional + " (" + sensor_read_functional_b + ")" );
			}

			domdoku_ibs = getArg( getOptionalArgs()[1] );
			domdoku_ibs_sgbd = getArg( getOptionalArgs()[2] );
			domdoku_ibs_job = getArg( getOptionalArgs()[12] );
			if( domdoku_ibs_sgbd != null ) {
				if( domdoku_ibs_sgbd.length() > 0 ) {
					try {
						//extract values n�tig, falls im Pr�fling das ergebnis einer andere PP verwendet werden soll.
						domdoku_ibs_sgbd = extractValues( domdoku_ibs_sgbd )[0];
					} catch( Exception ex ) {
						domdoku_ibs_sgbd = IBS_SGBD_ERROR + ": " + ex.getMessage();
					} catch( Throwable ex ) {
						domdoku_ibs_sgbd = IBS_SGBD_ERROR + ": " + ex.getMessage();
					}
				}
			}

			if( m_debug_level >= 11 ) {
				System.out.println( LOG_PREFIX + getOptionalArgs()[1] + "=" + domdoku_ibs );
				System.out.println( LOG_PREFIX + getOptionalArgs()[2] + "=" + domdoku_ibs_sgbd );
				System.out.println( LOG_PREFIX + getOptionalArgs()[12] + "=" + domdoku_ibs_job );
			}

			ibs_charging_balance_job = getArg( getOptionalArgs()[4] );
			if( ibs_charging_balance_job != null ) {
				ibs_charging_balance_job = ibs_charging_balance_job.trim();
				if( ibs_charging_balance_job.length() == 0 )
					ibs_charging_balance_job = null;
			}

			ibs_charging_balance_result = getArg( getOptionalArgs()[5] );
			if( ibs_charging_balance_result != null ) {
				ibs_charging_balance_result = ibs_charging_balance_result.trim();
				if( ibs_charging_balance_result.length() == 0 )
					ibs_charging_balance_result = null;
			}
			if( ibs_charging_balance_result == null )
				ibs_charging_balance_result = "STAT_SOC_FIT_WERT";

			ibs_charging_balance_parameter = getArg( getOptionalArgs()[8] );
			if( ibs_charging_balance_parameter != null ) {
				ibs_charging_balance_parameter = ibs_charging_balance_parameter.trim();
				if( ibs_charging_balance_parameter.length() == 0 )
					ibs_charging_balance_parameter = null;
			}
			if( ibs_charging_balance_parameter == null )
				ibs_charging_balance_parameter = "";

			ibs_charging_balance_sgbd = getArg( getOptionalArgs()[9] );
			if( ibs_charging_balance_sgbd != null ) {
				ibs_charging_balance_sgbd = ibs_charging_balance_sgbd.trim();
				if( ibs_charging_balance_sgbd.length() == 0 )
					ibs_charging_balance_sgbd = null;
			}
			if( ibs_charging_balance_sgbd == null )
				ibs_charging_balance_sgbd = domdoku_ibs_sgbd;
			else {
				if( ibs_charging_balance_sgbd.length() > 0 ) {
					try {
						//extract values n�tig, falls im Pr�fling das ergebnis einer andere PP verwendet werden soll.
						ibs_charging_balance_sgbd = extractValues( ibs_charging_balance_sgbd )[0];
					} catch( Exception ex ) {
						ibs_charging_balance_sgbd = IBS_SGBD_ERROR + ": " + ex.getMessage();
					} catch( Throwable ex ) {
						ibs_charging_balance_sgbd = IBS_SGBD_ERROR + ": " + ex.getMessage();
					}
				}
			}

			if( m_debug_level >= 11 ) {
				System.out.println( LOG_PREFIX + getOptionalArgs()[9] + "=" + ibs_charging_balance_sgbd );
				System.out.println( LOG_PREFIX + getOptionalArgs()[4] + "=" + ibs_charging_balance_job );
				System.out.println( LOG_PREFIX + getOptionalArgs()[8] + "=" + ibs_charging_balance_parameter );
				System.out.println( LOG_PREFIX + getOptionalArgs()[5] + "=" + ibs_charging_balance_result );
			}

			EcuInfos = buildECUInfos( domdoku_ibs, domdoku_ibs_sgbd, ibs_charging_balance_sgbd );

			if( m_debug_level >= 15 )
				EcuInfos.dumpEcuInfo();

			ept = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();

			EcuInfos.determineECUAddresses( ept, tolerableErrors );

			if( m_debug_level >= 15 ) {
				System.out.println( LOG_PREFIX + "" );
				System.out.println( LOG_PREFIX + "== With Adress ==" );
				System.out.println( LOG_PREFIX + "" );
			}
			if( m_debug_level >= 10 )
				EcuInfos.dumpEcuInfo();

			domdoku_ignore_str = getArg( getOptionalArgs()[3] );
			if( domdoku_ignore_str != null ) {
				if( domdoku_ignore_str.length() > 0 ) {
					String debug_str = getOptionalArgs()[3] + "=";

					domdoku_ignore_ecus_str = domdoku_ignore_str.split( "," );
					for( i = 0; i < domdoku_ignore_ecus_str.length; i++ ) {
						domdoku_ignore_ecus_str[i] = domdoku_ignore_ecus_str[i].trim().toUpperCase();
						try {
							if( domdoku_ignore_ecus_str[i].startsWith( "0X" ) ) {
								h = Integer.parseInt( domdoku_ignore_ecus_str[i].substring( 2 ), 16 );
							} else {
								h = Integer.parseInt( domdoku_ignore_ecus_str[i] );
							}

							EcuInfo = EcuInfos.findByEcuAdress( h );
							if( EcuInfo != null ) {
								if( EcuInfo.m_Doku_Flag != PL_FLAG_IGNORE ) {
									if( isDE() )
										str = "Pr�fling " + EcuInfo.m_Pruefling + " konfiguriert DokuChipcard f�r das SG " + EcuInfo.m_ECU_GRP + " mit Adresse 0x" + Integer.toHexString( h ) + ", aber dieses SG steht auch in der Liste DOMDOKU_IGNORE=";
									else
										str = "Pr�fling " + EcuInfo.m_Pruefling + " configures DokuChipcard for the ECU " + EcuInfo.m_ECU_GRP + " with address 0x" + Integer.toHexString( h ) + ", but this ECU is also listed in DOMDOKU_IGNORE=";
									str += domdoku_ignore_str;
									throw new CError( ADDRESSCONFLICT, str, "" );
								}

							}

							EcuInfos.m_EcusToIgnore.add( new Integer( h ) );
							debug_str += " 0x" + Integer.toHexString( h );
						} catch( NumberFormatException nfr ) {
						}
					}
					if( m_debug_level >= 11 ) {
						System.out.println( LOG_PREFIX + debug_str );
					}
				}
			}

			EcuInfos.m_IStufe = do_ISTUFE_LESEN( ept, tolerableErrors );
			do_IDENT( ept, EcuInfos, tolerableErrors );
			do_HW_MODIFICATION_INDEX_LESEN( ept, EcuInfos, tolerableErrors );
			do_SERIENNUMMER_LESEN( ept, EcuInfos, tolerableErrors );
			do_ECU_UID_LESEN( ept, EcuInfos, tolerableErrors );
			do_HERSTELLINFO_LESEN( ept, EcuInfos, tolerableErrors );
			do_SVK_LESEN( ept, EcuInfos, tolerableErrors );
			do_CALID_LESEN( ept, EcuInfos, tolerableErrors );
			do_AIF_LESEN( ept, EcuInfos, tolerableErrors );
			do_C_AEI_LESEN( ept, EcuInfos, tolerableErrors );
			do_SENSOREN_IDENT_LESEN( ept, EcuInfos, sensor_job_uds, sensor_read_functional_b, tolerableErrors );
			do_SENSOREN_HC2_IDENT_LESEN_INTERIM( ept, EcuInfos, tolerableErrors ); //TODO: Kann 2020 wieder entfallen
			if( !"FALSE".equalsIgnoreCase( domdoku_ibs ) ) {
				do_BATTERIESENSOR_LESEN( ept, EcuInfos, domdoku_ibs_job, ibs_charging_balance_job, ibs_charging_balance_parameter, ibs_charging_balance_result, tolerableErrors );
			}

			if( m_debug_level >= 15 ) {
				System.out.println( LOG_PREFIX + "" );
				System.out.println( LOG_PREFIX + "== After Communication ==" );
				System.out.println( LOG_PREFIX + "" );
				EcuInfos.dumpEcuInfo();
			}

			dokuEcuInfo( EcuInfos );

			str = "";
			for( i = 0; i < EcuInfos.nrOfECUs(); i++ ) {
				EcuInfo = EcuInfos.getECUInfo( i );
				if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNORE )
					continue;
				if( EcuInfo.m_Doku_Flag == PL_FLAG_FALSE )
					continue;
				if( (EcuInfo.m_Netz == 1 && EcuInfo.m_SGBD_Index != null) || (EcuInfo.m_Netz == 2 && EcuInfo.m_part_No != null) ) {
					if( str.length() > 0 )
						str += ", ";
					str += EcuInfo.m_ECU_GRP;
				}
			}

			if( EcuInfos.m_IBSSensor.m_Sensor_Doku_Flag == PL_FLAG_TRUE ) {
				if( EcuInfos.m_IBSSensor.m_assemblyNo != null ) {
					if( str.length() > 0 )
						str += ", ";
					str += EcuInfos.m_IBSSensor.m_SGBD;
				}
			}

			if( str.length() == 0 ) {
				if( isDE() )
					str = "Keine";
				if( isDE() )
					str = "None";
			}

			if( isDE() )
				ergListe.add( new Ergebnis( "FOUND_ECUS", "", "", "", "", "Gefundene Steuerger�te", str, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ) );
			else
				ergListe.add( new Ergebnis( "FOUND_ECUS", "", "", "", "", "Found ECUs", str, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO ) );

			for( i = 0; i < tolerableErrors.size(); i++ ) {
				entry = ((CError) tolerableErrors.get( i )).getEntry();
				ergListe.add( entry );
				if( entry.getFehlerTyp().equals( Ergebnis.FT_NIO ) || entry.getFehlerTyp().equals( Ergebnis.FT_NIO_SYS ) )
					status = STATUS_EXECUTION_ERROR;
			}

		} catch( CError err ) {
			err.printStackTrace();
			ergListe.add( err.getEntry() );
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			System.out.println( LOG_PREFIX + e.getMessage() );
			e.printStackTrace();
			ergListe.add( new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS ) );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			System.out.println( LOG_PREFIX + e.getMessage() );
			e.printStackTrace();
			ergListe.add( new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS ) );
			status = STATUS_EXECUTION_ERROR;
		}

		setPPStatus( info, status, ergListe );
	}

	/** Erzeugt die interne Datenstruktur f�r die erwarteten ECUs und Sensoren.
	 * Dazu werden in allen Pr�flinge abgeleitet von Abstract...Pruefling die Attribute
	 * DOMDOKU_ECU, DOMDOKU_ECU_SER, DOMDOKU_SENSOR usw. ausgewertet
	 * 
	 * @param DokuIBS String der im Parameter DOMDOKU_IBS �bergeben wurde. Sollte TRUE, IGNORE oder leer sein.
	 * @param DokuIBS_SGBD String der im Parameter DOMDOKU_IBS_SGBD �bergeben wurde. Sollte ein SGBDName oder leer sein.
	 * @param DokuChargingBalance_SGBD String der im Parameter IBS_CHARGING_BALANCE_SGBD �bergeben wurde. Sollte ein SGBDName oder leer sein.
	 * @return Liste der erwarteten ECUs (die Sensoren h�ngen in der internen Struktur an den ECUs)
	 * @throws CError Fehlerobjekt
	 */
	@SuppressWarnings("unchecked")
	private ECU_Infos buildECUInfos( String DokuIBS, String DokuIBS_SGBD, String DokuChargingBalance_SGBD ) throws CError {
		int i;
		String PL_type;
		String str;
		String Doku_Str;
		String Sensor_Str;
		ECU_Info info;
		Pruefling alle_prueflinge[];
		ECU_Infos erg;
		int Netz;
		boolean readSerNum, readEcuUIDNum, readHerstelldatum, readLieferantennummer;
		String readSerNum_str, readEcuUIDNum_str, readHerstelldatum_str;
		//String  readLieferantennummer_str;
		String SGBD, GRUPPE;
		int Ecu_Doku_Flag, Sensor_Doku_Flag, One_Sensor_Doku_Flag;
		Map<String, String> allAtt;
		Iterator<String> it;
		HashMap<Integer, Integer> SensorDokuMap;

		alle_prueflinge = getPr�fling().getAuftrag().getPr�flinge();
		erg = new ECU_Infos();

		if( "TRUE".equalsIgnoreCase( DokuIBS ) )
			erg.addIBSSensor( PL_FLAG_TRUE, DokuIBS_SGBD, DokuChargingBalance_SGBD );
		else
			erg.addIBSSensor( PL_FLAG_FALSE, DokuIBS_SGBD, DokuChargingBalance_SGBD );

		for( i = 0; i < alle_prueflinge.length; i++ ) {
			PL_type = alle_prueflinge[i].getClass().getSuperclass().getName();
			if( m_debug_level >= 10 )
				System.out.println( LOG_PREFIX + alle_prueflinge[i].getClass().getName() + " -> " + PL_type );

			Netz = 0;
			if( PL_type.indexOf( "AbstractEcuPruefling" ) < 0 && PL_type.indexOf( "AbstractEcuUDSPruefling" ) < 0 && PL_type.indexOf( "AbstractCodierPruefling" ) < 0 )
				continue;

			Ecu_Doku_Flag = PL_FLAG_TRUE;
			Doku_Str = alle_prueflinge[i].getAttribut( PL_DOMDOKU_ECU );
			if( Doku_Str == null )
				Ecu_Doku_Flag = PL_FLAG_TRUE;
			else if( Doku_Str.length() == 0 )
				Ecu_Doku_Flag = PL_FLAG_TRUE;
			else if( Doku_Str.equalsIgnoreCase( "IGNORE" ) )
				Ecu_Doku_Flag = PL_FLAG_IGNORE;
			else if( Doku_Str.equalsIgnoreCase( "TRUE" ) )
				Ecu_Doku_Flag = PL_FLAG_TRUE;
			else if( Doku_Str.equalsIgnoreCase( "FALSE" ) )
				Ecu_Doku_Flag = PL_FLAG_FALSE;
			else if( Doku_Str.equalsIgnoreCase( "IGNOREERR" ) )
				Ecu_Doku_Flag = PL_FLAG_IGNOREERR;
			else if( Doku_Str.equalsIgnoreCase( "SKIP" ) )
				continue;
			else {
				throwBadEntryError( PL_DOMDOKU_ECU, alle_prueflinge[i].getName(), Doku_Str );
			}

			SGBD = alle_prueflinge[i].getAttribut( "SGBD" );
			if( SGBD == null )
				SGBD = "";
			if( SGBD.length() == 0 ) {
				if( isDE() )
					str = "Keine SGBD definiert in ";
				else
					str = "No SGBD defined in ";
				throw new CError( "buildECUInfos", str + alle_prueflinge[i].getName(), "" );
			}

			GRUPPE = alle_prueflinge[i].getAttribut( "GRUPPE" );
			if( GRUPPE == null )
				GRUPPE = "";
			if( GRUPPE.length() == 0 ) {
				if( isDE() )
					str = "Keine GRUPPE definiert in ";
				else
					str = "No GRUPPE defined in ";
				throw new CError( "buildECUInfos", str + alle_prueflinge[i].getName(), "" );
			}

			if( GRUPPE.substring( 0, 2 ).equalsIgnoreCase( "G_" ) )
				Netz = 1;
			else if( GRUPPE.substring( 0, 2 ).equalsIgnoreCase( "D_" ) )
				Netz = 2;
			else {
				if( isDE() )
					str = "GRUPPE=\"" + GRUPPE + "\" beginnt nicht mit G_ oder D_ in ";
				else
					str = "GRUPPE=\"" + GRUPPE + "\" does not start with G_ or D_ in ";
				throw new CError( "buildECUInfos", str + alle_prueflinge[i].getName(), "" );
			}

			readSerNum = true;
			readSerNum_str = alle_prueflinge[i].getAttribut( PL_DOMDOKU_ECU_SER );
			if( readSerNum_str != null ) {
				if( readSerNum_str.equalsIgnoreCase( "FALSE" ) )
					readSerNum = false;
				else if( readSerNum_str.equalsIgnoreCase( "TRUE" ) )
					readSerNum = true;
				else {
					throwBadEntryError( PL_DOMDOKU_ECU_SER, alle_prueflinge[i].getName(), readSerNum_str );
				}
			}

			readEcuUIDNum = true;
			readEcuUIDNum_str = alle_prueflinge[i].getAttribut( PL_DOMDOKU_ECU_UID );
			if( readEcuUIDNum_str != null ) {
				if( readEcuUIDNum_str.equalsIgnoreCase( "FALSE" ) )
					readEcuUIDNum = false;
				else if( readEcuUIDNum_str.equalsIgnoreCase( "TRUE" ) )
					readEcuUIDNum = true;
				else {
					throwBadEntryError( PL_DOMDOKU_ECU_UID, alle_prueflinge[i].getName(), readEcuUIDNum_str );
				}
			}

			readHerstelldatum = true;
			readHerstelldatum_str = alle_prueflinge[i].getAttribut( PL_DOMDOKU_READHERSTELLDATUM );
			if( readHerstelldatum_str != null ) {
				if( readHerstelldatum_str.equalsIgnoreCase( "TRUE" ) )
					readHerstelldatum = true;
				else if( readHerstelldatum_str.equalsIgnoreCase( "FALSE" ) )
					readHerstelldatum = false;
				else {
					throwBadEntryError( PL_DOMDOKU_READHERSTELLDATUM, alle_prueflinge[i].getName(), readHerstelldatum_str );
				}
			}

			readLieferantennummer = false;
			/* Deaktiviert, weil dieser Wert in DOM derzeit nicht gespeichert wird
			readLieferantennummer_str=alle_prueflinge[i].getAttribut(PL_DOMDOKU_READLIEFERANTENNUMMER);
			if (readLieferantennummer_str!=null)
			{
			    if (readLieferantennummer_str.equalsIgnoreCase("TRUE")) readLieferantennummer=true;
			    else if (readLieferantennummer_str.equalsIgnoreCase("FALSE")) readLieferantennummer=false;
			    else
			    {
			        throwBadEntryError(PL_DOMDOKU_READLIEFERANTENNUMMER, alle_prueflinge[i].getName(), readLieferantennummer_str);
			    }
			}
			*/

			Sensor_Str = alle_prueflinge[i].getAttribut( PL_DOMDOKU_SENSOR );

			Sensor_Doku_Flag = PL_FLAG_IGNORE;
			if( Sensor_Str == null )
				Sensor_Doku_Flag = PL_FLAG_IGNORE;
			else if( Sensor_Str.length() == 0 )
				Sensor_Doku_Flag = PL_FLAG_IGNORE;
			else if( Sensor_Str.equalsIgnoreCase( "TRUE" ) )
				Sensor_Doku_Flag = PL_FLAG_TRUE;
			else if( Sensor_Str.equalsIgnoreCase( "FALSE" ) )
				Sensor_Doku_Flag = PL_FLAG_FALSE;
			else if( Sensor_Str.equalsIgnoreCase( "AUTO" ) )
				Sensor_Doku_Flag = PL_FLAG_AUTO;
			else if( Sensor_Str.equalsIgnoreCase( "AUTO_PHYS" ) )
				Sensor_Doku_Flag = PL_FLAG_AUTO_PHYS;
			else if( Sensor_Str.equalsIgnoreCase( "IGNORE" ) )
				Sensor_Doku_Flag = PL_FLAG_IGNORE;
			else {
				throwBadEntryError( PL_DOMDOKU_SENSOR, alle_prueflinge[i].getName(), Sensor_Str );
			}

			SensorDokuMap = new HashMap<Integer, Integer>();
			if( Sensor_Doku_Flag == PL_FLAG_TRUE ) {
				allAtt = alle_prueflinge[i].getAllAttributes();
				it = allAtt.keySet().iterator();
				while( it.hasNext() ) {
					Object key;
					String keystr;
					int sensor_id;
					String sensor_id_str;
					Object Sensor_obj;

					key = it.next();
					if( key instanceof String ) {
						keystr = (String) key;
						if( keystr.startsWith( PL_DOMDOKU_SENSOR ) ) {
							sensor_id_str = keystr.substring( PL_DOMDOKU_SENSOR.length() );
							if( sensor_id_str.startsWith( "_" ) ) {
								sensor_id_str = sensor_id_str.substring( 1 );
								Sensor_obj = allAtt.get( key );
								One_Sensor_Doku_Flag = -1;
								if( Sensor_obj == null ) {
									Sensor_Str = "null";
								} else if( Sensor_obj instanceof String ) {
									Sensor_Str = (String) Sensor_obj;
									if( Sensor_Str.equalsIgnoreCase( "TRUE" ) )
										One_Sensor_Doku_Flag = PL_FLAG_TRUE;
									else if( Sensor_Str.equalsIgnoreCase( "FALSE" ) )
										One_Sensor_Doku_Flag = PL_FLAG_FALSE;
									else if( Sensor_Str.equalsIgnoreCase( "IGNORE" ) )
										One_Sensor_Doku_Flag = PL_FLAG_IGNORE;
									else if( Sensor_Str.equalsIgnoreCase( "SER" ) )
										One_Sensor_Doku_Flag = PL_FLAG_SER;
									else if( Sensor_Str.equalsIgnoreCase( "SNR" ) )
										One_Sensor_Doku_Flag = PL_FLAG_SNR;

									try {
										sensor_id = Integer.parseInt( sensor_id_str, 16 );
										if( One_Sensor_Doku_Flag >= 0 )
											SensorDokuMap.put( new Integer( sensor_id ), new Integer( One_Sensor_Doku_Flag ) );
									} catch( NumberFormatException ex ) {
										if( isDE() )
											str = "Ung�ltige SensorID " + sensor_id_str + " in ";
										else
											str = "Bad sensor id " + sensor_id_str + " in ";
										throw new CError( "buildECUInfos", str + alle_prueflinge[i].getName(), "" );
									}
								} else {
									Sensor_Str = Sensor_obj.getClass().getName();
								}
								if( One_Sensor_Doku_Flag < 0 ) {
									throwBadEntryError( keystr, alle_prueflinge[i].getName(), Sensor_Str );
								}
							}
						}
					}
				}
			}

			info = new ECU_Info( alle_prueflinge[i].getName(), Ecu_Doku_Flag, SGBD, GRUPPE, Netz, readSerNum, readEcuUIDNum, readHerstelldatum, readLieferantennummer, Sensor_Doku_Flag, SensorDokuMap );
			erg.add( info );
		}

		return erg;
	}

	/** Erzeugt ein Fehlerobjekt f�r einen falschen Pr�flgingseintrag
	 * 
	 * @param AttName Name des Pr�flingsattributs
	 * @param PLName Name des Pr�flings
	 * @param Entry (fehlerhafter) Wert des Attributs
	 * @throws CError erzeugtes Fehlerobjekt
	 */
	private void throwBadEntryError( String AttName, String PLName, String Entry ) throws CError {
		String str;

		if( isDE() )
			str = "Ung�ltiger Eintrag " + AttName + "=" + Entry + " in ";
		else
			str = "Bad setting " + AttName + "=" + Entry + " in ";
		throw new CError( "buildECUInfos", str + PLName, "" );
	}

	/** Ermittelt die I-Stufe 
	 * 
	 * @param ept Ediabas
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @return Ermittelte I-Stufe
	 */
	private String do_ISTUFE_LESEN( EdiabasProxyThread ept, Vector<CError> TolerableErrors ) {
		final String JOBNAME = "i_stufe_lesen";
		final String ERRORTXT = isDE() ? "Fehler bei " + JOBNAME : "Error during " + JOBNAME;
		String action;

		try {
			action = "executeJob";

			executeJob( ept, m_SGBD_UDS, JOBNAME );
		} catch( CError e ) {
			TolerableErrors.add( e );
			return "unknown";
		}

		action = "";
		try {
			action = "apiResultText(I_STUFE_WERK)";
			return ept.apiResultText( "I_STUFE_WERK", 1, "" );
		} catch( ApiCallFailedException e ) {
			TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, ept.apiErrorText() ) );
			return "unknown";
		}
	}

	/** versucht ein ECU physikalisch anzusprechen (wenn es funktional nicht geantwortet hat)
	 * 
	 * @param ept Ediabas
	 * @param EcuInfo Steuerger�t
	 * @param JOBNAME Name des physikalisch auszuf�hrenden Jobs
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen) 
	 * @param ERRORTXT Fehlertext, wenn null: kein Eintrag in TolerableErrors, weil SG nur physikalisch geantwortet hat
	 * @return true: SG hat geantwortet, false: SG hat nicht geantwortet
	 */
	private boolean tryPhysical( EdiabasProxyThread ept, ECU_Info EcuInfo, String JOBNAME, Vector<CError> TolerableErrors, String ERRORTXT ) {
		String SGBD;
		String str;

		String Status = "null";

		SGBD = (EcuInfo.m_Netz == 1 ? m_SGBD_UDS : m_SGBD_BN2K);
		try {
			Status = executeJob( ept, SGBD, JOBNAME, Integer.toString( EcuInfo.m_ECU_Adr ), EcuInfo.m_ECU_GRP );
		} catch( CError e ) {
			if( e.getEntry().getHinweisText().indexOf( "IFH-0009" ) < 0 ) {
				final String JOB_SERIENNUMMER_LESEN = "SERIENNUMMER_LESEN_FUNKTIONAL";
				final String ERROR_REQUEST_OUT_OF_RANGE = "ERROR_ECU_REQUEST_OUT_OF_RANGE";
				final String JOB_ECU_UID_LESEN = "ECU_UID_LESEN_FUNKTIONAL";

				if( JOBNAME.equalsIgnoreCase( JOB_SERIENNUMMER_LESEN ) && Status.equalsIgnoreCase( ERROR_REQUEST_OUT_OF_RANGE ) && (EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_AUTO) || (EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_AUTO) ) {
					TolerableErrors.add( new CError( e.m_entry.getID(), e.m_entry.getFehlerText(), e.m_entry.getHinweisText(), Ergebnis.FT_IGNORE ) );
				} else if( JOBNAME.equalsIgnoreCase( JOB_ECU_UID_LESEN ) ) {
					TolerableErrors.add( new CError( e.m_entry.getID(), e.m_entry.getFehlerText(), e.m_entry.getHinweisText(), Ergebnis.FT_IGNORE ) );
				} else {
					TolerableErrors.add( e );
				}
			}

			return false;
		}

		if( ERRORTXT != null ) {
			if( isDE() )
				str = "SG in Gruppe \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( EcuInfo.m_ECU_Adr ) + ") hat nicht funktional geantwortet, nur physikalisch.";
			else
				str = "ECU in Group \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( EcuInfo.m_ECU_Adr ) + ") did not answer functional, only physical.";

			TolerableErrors.add( new CError( NOFUNCANSWER, ERRORTXT, str, Ergebnis.FT_IGNORE ) );
		}
		return true;
	}

	/** Ermittelt SGBD-Index der ECUs und tr�gt sie in die Interne Datenstruktur ein
	 * 
	 * @param ept Ediabas
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @throws CError Fehlerobjekt, das zu sofortigem Abbruch f�hrt
	 */
	private void do_IDENT( EdiabasProxyThread ept, ECU_Infos EcuInfos, Vector<CError> TolerableErrors ) throws CError {
		int Netz, Sets;
		String SGBD;
		Vector<ECU_Info> MissingECUs;
		int i;
		ECU_Info EcuInfo;

		final String JOBNAME = "ident_funktional";
		final String ERRORTXT = isDE() ? "Fehler bei " + JOBNAME : "Error during " + JOBNAME;

		EcuInfos.setAllNotVisited();

		for( Netz = 1; Netz <= 2; Netz++ ) {
			SGBD = (Netz == 1 ? m_SGBD_UDS : m_SGBD_BN2K);
			Sets = executeFunctionalJob( ept, SGBD, JOBNAME );

			do_IDENT_analyse( ept, Netz, Sets, EcuInfos, TolerableErrors, ERRORTXT );
		}

		MissingECUs = getMissingECUs( EcuInfos, -1 );
		if( !MissingECUs.isEmpty() ) {
			for( i = 0; i < MissingECUs.size(); i++ ) {
				EcuInfo = (ECU_Info) MissingECUs.get( i );

				if( m_debug_level >= 10 )
					System.out.println( LOG_PREFIX + "Try to read ident for SG " + Integer.toHexString( EcuInfo.m_ECU_Adr ) + " physical" );

				if( !tryPhysical( ept, EcuInfo, JOBNAME, TolerableErrors, ERRORTXT ) )
					continue;
				do_IDENT_analyse_one_set( ept, EcuInfo.m_Netz, 1, EcuInfos, TolerableErrors, ERRORTXT );
			}
			MissingECUs = getMissingECUs( EcuInfos, -1 );
		}
		checkForMisssingECUs( MissingECUs, ERRORTXT, TolerableErrors );
	}

	/** Analysiert alle Sets des funktionalen IDENT Aufrufs
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Sets Anzahl der Sets
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT Allgemeiner Fehlertext, enth�lt Name des funktionalen Jobs.
	 */
	private void do_IDENT_analyse( EdiabasProxyThread ept, int Netz, int Sets, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int i;
		for( i = 1; i < Sets; i++ ) {
			do_IDENT_analyse_one_set( ept, Netz, i, EcuInfos, TolerableErrors, ERRORTXT );
		}
	}

	/** Analysiert ein Set eines IDENT Aufrufs
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Set Nummer des Sets 
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT Allgemeiner Fehlertext, enth�lt Name des funktionalen Jobs.
	 */
	private void do_IDENT_analyse_one_set( EdiabasProxyThread ept, int Netz, int Set, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int ECUAddr;
		String errtyp;
		String ECUGruppe;
		String SGBD_Index;
		String action;
		ECU_Info EcuInfo;
		String str;
		int year, month, day;

		EcuInfo = null;
		action = "apiResultInt(ID_SG_ADR," + Set + ")";
		try {
			ECUAddr = ept.apiResultInt( "ID_SG_ADR", Set );
			EcuInfo = EcuInfos.findByEcuAdress( ECUAddr );

			errtyp = Ergebnis.FT_NIO;
			if( EcuInfo != null ) {
				if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNOREERR )
					errtyp = Ergebnis.FT_IGNORE;
			}

			if( EcuInfo == null ) {
				String errtyp2 = errtyp;
				if( EcuInfos.m_EcusToIgnore.contains( new Integer( ECUAddr ) ) )
					errtyp2 = Ergebnis.FT_IGNORE;

				action = "apiResultText(ECU_GRUPPE," + Set + ") ECU=0x" + Integer.toHexString( ECUAddr );
				ECUGruppe = ept.apiResultText( "ECU_GRUPPE", Set, "" );

				if( isDE() )
					str = "SG in Gruppe \"" + ECUGruppe + "\" (0x" + Integer.toHexString( ECUAddr ) + ") hat geantwortet, d�rfte aber nicht antworten.";
				else
					str = "ECU in Group \"" + ECUGruppe + "\" (0x" + Integer.toHexString( ECUAddr ) + ") did answer, but should not.";
				TolerableErrors.add( new CError( ECUANSWERED, ERRORTXT, str, errtyp2 ) );
			} else if( EcuInfo.m_Doku_Flag == PL_FLAG_FALSE ) {
				if( isDE() )
					str = "SG in Gruppe \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") hat geantwortet, d�rfte aber nicht antworten.";
				else
					str = "ECU in Group \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") did answer, but should not.";
				TolerableErrors.add( new CError( ECUANSWERED, ERRORTXT, str, errtyp ) );
			}

			if( EcuInfo != null ) {
				EcuInfo.m_visited = true;

				if( EcuInfo.m_Doku_Flag != PL_FLAG_IGNORE ) {
					action = "apiResultText(ECU_GRUPPE," + Set + ") ECU=0x" + Integer.toHexString( ECUAddr );
					ECUGruppe = ept.apiResultText( "ECU_GRUPPE", Set, "" );

					if( !EcuInfo.m_ECU_GRP.equals( ECUGruppe ) ) {
						if( isDE() )
							str = "SG an Adresse 0x" + Integer.toHexString( ECUAddr ) + ": Job sagt Gruppe \"" + ECUGruppe + "\", Pr�fling sagt Gruppe \"" + EcuInfo.m_ECU_GRP + "\"";
						else
							str = "ECU at address 0x" + Integer.toHexString( ECUAddr ) + ": Job says Group \"" + ECUGruppe + "\", Pr�fling says group \"" + EcuInfo.m_ECU_GRP + "\"";
						TolerableErrors.add( new CError( GROUPADDRMISMATCH, ERRORTXT, str, errtyp ) );
						EcuInfo = null;
					}
				}
			}

			if( EcuInfo != null ) {
				if( EcuInfo.m_Doku_Flag != PL_FLAG_IGNORE && EcuInfo.m_Doku_Flag != PL_FLAG_FALSE ) {
					if( EcuInfo.m_Netz != Netz ) {
						if( Netz == 1 ) {
							if( isDE() )
								str = "SG in Gruppe \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") hat im UDS Netz geantwortet, sollte aber im Bordnetz 2000 sein.";
							else
								str = "ECU in Group \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") answered in UDS net, but should be in boardnet 2000.";
						} else {
							if( isDE() )
								str = "SG in Gruppe \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") hat im Bordnetz 2000 geantwortet, sollte aber im UDS Netz sein.";
							else
								str = "ECU in Group \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") answered in boardnet 2000, but should be in UDS net.";
						}
						TolerableErrors.add( new CError( WRONGNET, ERRORTXT, str, errtyp ) );
					} else if( Netz == 1 ) {
						action = "apiResultText(ID_SGBD_INDEX," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						SGBD_Index = ept.apiResultText( "ID_SGBD_INDEX", Set, "" );
						try {
							EcuInfo.m_SGBD_Index = Integer.toHexString( Integer.parseInt( SGBD_Index.trim() ) ).toUpperCase();
						} catch( NumberFormatException e ) {
							EcuInfo.m_SGBD_Index = "0";
						}
					} else if( Netz == 2 ) {
						action = "apiResultInt(ID_VAR_INDEX," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						EcuInfo.m_var_Index = ept.apiResultInt( "ID_VAR_INDEX", Set );

						action = "apiResultInt(ID_DIAG_INDEX," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						EcuInfo.m_diag_Index = ept.apiResultInt( "ID_DIAG_INDEX", Set );

						action = "apiResultInt(ID_COD_INDEX," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						EcuInfo.m_coding_Index = ept.apiResultInt( "ID_COD_INDEX", Set );

						action = "apiResultText(ID_BMW_NR," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						EcuInfo.m_part_No = ept.apiResultText( "ID_BMW_NR", Set, "" );

						action = "apiResultText(ID_SW_NR_OSV," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						EcuInfo.m_software_oper = ept.apiResultText( "ID_SW_NR_OSV", Set, "" );

						action = "apiResultText(ID_SW_NR_FSV," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						EcuInfo.m_software_func = ept.apiResultText( "ID_SW_NR_FSV", Set, "" );

						action = "apiResultText(ID_SW_NR_MCV," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						EcuInfo.m_software_comm = ept.apiResultText( "ID_SW_NR_MCV", Set, "" );

						action = "apiResultText(ID_SW_NR_RES," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						EcuInfo.m_software_resv = ept.apiResultText( "ID_SW_NR_RES", Set, "" );

						action = "apiResultText(ID_HW_NR," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						EcuInfo.m_hardware_version = ept.apiResultText( "ID_HW_NR", Set, "" );

						action = "apiResultInt(ID_DATUM_JAHR," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						year = ept.apiResultInt( "ID_DATUM_JAHR", Set );
						if( year < 1000 )
							year += 2000;

						action = "apiResultInt(ID_DATUM_MONAT," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						month = ept.apiResultInt( "ID_DATUM_MONAT", Set );

						action = "apiResultInt(ID_DATUM_TAG," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						day = ept.apiResultInt( "ID_DATUM_TAG", Set );
						EcuInfo.m_Herstelldatum = Integer.toString( year );
						EcuInfo.m_Herstelldatum += "-";
						str = Integer.toString( month );
						while( str.length() < 2 )
							str = "0" + str;
						EcuInfo.m_Herstelldatum += str;
						EcuInfo.m_Herstelldatum += "-";
						str = Integer.toString( day );
						while( str.length() < 2 )
							str = "0" + str;
						EcuInfo.m_Herstelldatum += str;

						if( EcuInfo.m_readSeriennummer ) {
							EcuInfo.m_Seriennummer = ept.apiResultText( "SERIENNUMMER", Set, "" );
						}

						action = "apiResultInt(ID_LIEF_NR," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						EcuInfo.m_Lieferantennummer = Integer.toString( ept.apiResultInt( "ID_LIEF_NR", Set ) );
					}
				}
			}
		} catch( ApiCallFailedException e ) {
			errtyp = Ergebnis.FT_NIO;
			if( EcuInfo != null ) {
				if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNOREERR )
					errtyp = Ergebnis.FT_IGNORE;
			}
			TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, ept.apiErrorText(), errtyp ) );
		}

	}

	/** Ermittelt den hwAenderungs-Index und tr�gt sie in die Interne Datenstruktur ein
	 * 
	 * @param ept Ediabas
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @throws CError Fehlerobjekt, das zu sofortigem Abbruch f�hrt
	 */
	private void do_HW_MODIFICATION_INDEX_LESEN( EdiabasProxyThread ept, ECU_Infos EcuInfos, Vector<CError> TolerableErrors ) throws CError {
		int Sets;
		String SGBD;
		final String JOBNAME = "hw_modification_index_lesen_funktional";
		final String ERRORTXT = isDE() ? "Fehler bei " + JOBNAME : "Error during " + JOBNAME;

		EcuInfos.setAllNotVisited();

		SGBD = m_SGBD_UDS;

		// Motorrat hat keinen Job "hw_modification_index_lesen_funktional" -> gar nicht erst probieren, w�rde schiefgehen. 
		if( SGBD.equals( "X_K001" ) || SGBD.equals( "X_KS01" ) )
			return;

		Sets = executeFunctionalJob( ept, SGBD, JOBNAME );
		do_HW_MODIFICATION_INDEX_LESEN_analyse( ept, 1, Sets, EcuInfos, TolerableErrors, ERRORTXT );
	}

	/** Analysiert alle Sets des funktionalen hw_modification_index_lesen Aufrufs
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Sets Anzahl der Sets
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT Allgemeiner Fehlertext, enth�lt Name des funktionalen Jobs.
	 */
	private void do_HW_MODIFICATION_INDEX_LESEN_analyse( EdiabasProxyThread ept, int Netz, int Sets, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int i;
		for( i = 1; i < Sets; i++ ) {
			do_HW_MODIFICATION_INDEX_LESEN_analyse_one_set( ept, Netz, i, EcuInfos, TolerableErrors, ERRORTXT );
		}
	}

	/** Analysiert ein Set eines hw_modification_index_lesen Aufrufs
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Set Nummer des Sets
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT Allgemeiner Fehlertext, enth�lt Name des funktionalen Jobs.
	 */
	private void do_HW_MODIFICATION_INDEX_LESEN_analyse_one_set( EdiabasProxyThread ept, int Netz, int Set, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int ECUAddr;
		String action, str;
		ECU_Info EcuInfo;

		action = "apiResultInt(ID_SG_ADR," + Set + ")";
		EcuInfo = null;
		try {
			ECUAddr = ept.apiResultInt( "ID_SG_ADR", Set );

			EcuInfo = EcuInfos.findByEcuAdress( ECUAddr );
			if( EcuInfo == null ) {
				String errtyp2 = Ergebnis.FT_NIO;
				if( EcuInfos.m_EcusToIgnore.contains( new Integer( ECUAddr ) ) )
					errtyp2 = Ergebnis.FT_IGNORE;

				if( isDE() )
					str = "SG an Adresse 0x" + Integer.toHexString( ECUAddr ) + " hat geantwortet, d�rfte aber nicht antworten.";
				else
					str = "ECU at Address 0x" + Integer.toHexString( ECUAddr ) + " did answer, but should not.";
				TolerableErrors.add( new CError( ECUANSWERED, ERRORTXT, str, errtyp2 ) );
			} else if( EcuInfo.m_Doku_Flag == PL_FLAG_FALSE ) {
				if( isDE() )
					str = "SG in Gruppe \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") hat geantwortet, d�rfte aber nicht antworten.";
				else
					str = "ECU in Group \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") did answer, but should not.";
				TolerableErrors.add( new CError( ECUANSWERED, ERRORTXT, str ) );
			}

			if( EcuInfo != null ) {
				EcuInfo.m_visited = true;
				if( EcuInfo.m_Doku_Flag != PL_FLAG_FALSE && EcuInfo.m_Doku_Flag != PL_FLAG_IGNORE ) {

					action = "apiResultText(STAT_HW_MODIFICATION_INDEX_WERT, Set " + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
					EcuInfo.m_hw_modification_Index = ept.apiResultText( "STAT_HW_MODIFICATION_INDEX_WERT", Set, "" );
				}
			}
		} catch( ApiCallFailedException e ) {
			String errtyp;
			errtyp = Ergebnis.FT_NIO;
			if( EcuInfo != null ) {
				if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNOREERR || ept.apiErrorText().toUpperCase().indexOf( "RESULT NOT FOUND" ) > 0 )
					errtyp = Ergebnis.FT_IGNORE;
			}
			TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, ept.apiErrorText(), errtyp ) );
		}
	}

	/** Ermittelt die Seriennummer der ECUs und tr�gt sie in die Interne Datenstruktur ein
	 * 
	 * @param ept Ediabas
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @throws CError Fehlerobjekt, das zu sofortigem Abbruch f�hrt
	 */
	private void do_SERIENNUMMER_LESEN( EdiabasProxyThread ept, ECU_Infos EcuInfos, Vector<CError> TolerableErrors ) throws CError {
		int Netz, Sets;
		String SGBD;
		Vector<ECU_Info> MissingECUs;
		int i;
		ECU_Info EcuInfo;
		final String JOBNAME = "seriennummer_lesen_funktional";
		final String ERRORTXT = isDE() ? "Fehler bei " + JOBNAME : "Error during " + JOBNAME;

		EcuInfos.setAllNotVisited();

		for( Netz = 1; Netz <= 2; Netz++ ) {
			SGBD = (Netz == 1 ? m_SGBD_UDS : m_SGBD_BN2K);
			Sets = executeFunctionalJob( ept, SGBD, JOBNAME );
			do_SERIENNUMMER_LESEN_analyse( ept, Netz, Sets, EcuInfos, TolerableErrors, ERRORTXT );
		}

		MissingECUs = getMissingECUs( EcuInfos, -1 );
		if( !MissingECUs.isEmpty() ) {
			for( i = 0; i < MissingECUs.size(); i++ ) {
				EcuInfo = (ECU_Info) MissingECUs.get( i );

				if( m_debug_level >= 10 )
					System.out.println( LOG_PREFIX + "Try to read serial number for SG " + Integer.toHexString( EcuInfo.m_ECU_Adr ) + " physical" );

				if( !tryPhysical( ept, EcuInfo, JOBNAME, TolerableErrors, ERRORTXT ) )
					continue;
				do_SERIENNUMMER_LESEN_analyse_one_set( ept, EcuInfo.m_Netz, 1, EcuInfos, TolerableErrors, ERRORTXT );
			}
			MissingECUs = getMissingECUs( EcuInfos, -1 );
		}
		checkForMisssingECUs( MissingECUs, ERRORTXT, TolerableErrors );
	}

	/** Analysiert alle Sets des funktionalen seriennummer_lesen Aufrufs
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Sets Anzahl der Sets
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT
	 */
	private void do_SERIENNUMMER_LESEN_analyse( EdiabasProxyThread ept, int Netz, int Sets, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int i;
		for( i = 1; i < Sets; i++ ) {
			do_SERIENNUMMER_LESEN_analyse_one_set( ept, Netz, i, EcuInfos, TolerableErrors, ERRORTXT );
		}
	}

	/** Analysiert ein Set eines seriennummer_lesen Aufrufs
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Set Nummer des Sets
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT
	 */
	private void do_SERIENNUMMER_LESEN_analyse_one_set( EdiabasProxyThread ept, int Netz, int Set, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int ECUAddr;
		String SerialNumber;
		String action;
		String str;
		ECU_Info EcuInfo;

		EcuInfo = null;
		action = "apiResultInt(ID_SG_ADR," + Set + ")";
		try {
			ECUAddr = ept.apiResultInt( "ID_SG_ADR", Set );

			EcuInfo = EcuInfos.findByEcuAdress( ECUAddr );
			if( EcuInfo != null ) {
				EcuInfo.m_visited = true;

				if( EcuInfo.m_Doku_Flag != PL_FLAG_FALSE && EcuInfo.m_Doku_Flag != PL_FLAG_IGNORE && EcuInfo.m_readSeriennummer ) {
					action = "apiResultText(SERIENNUMMER, Set " + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
					try {
						SerialNumber = ept.apiResultText( "SERIENNUMMER", Set, "" );

						EcuInfo.m_Seriennummer = SerialNumber;
					} catch( ApiCallFailedException e ) {
						if( Netz == 1 )
							throw e;
						if( EcuInfo.m_Seriennummer == null )
							throw e;
						if( EcuInfo.m_Seriennummer.length() == 0 )
							throw e;

						// Bei BN2K kann die Seriennummer auch schon bei IDENT_FUNKTIONAL ermittelt worden sein (z.B. TOP_AMP)
					}
				}
			}

			if( EcuInfo == null ) {
				String errtyp2 = Ergebnis.FT_NIO;
				if( EcuInfos.m_EcusToIgnore.contains( new Integer( ECUAddr ) ) )
					errtyp2 = Ergebnis.FT_IGNORE;

				if( isDE() )
					str = "SG an Adresse 0x" + Integer.toHexString( ECUAddr ) + " hat geantwortet, d�rfte aber nicht antworten.";
				else
					str = "ECU at Address 0x" + Integer.toHexString( ECUAddr ) + " did answer, but should not.";
				TolerableErrors.add( new CError( ECUANSWERED, ERRORTXT, str, errtyp2 ) );
			} else if( EcuInfo.m_Doku_Flag == PL_FLAG_FALSE ) {
				if( isDE() )
					str = "SG in Gruppe \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") hat geantwortet, d�rfte aber nicht antworten.";
				else
					str = "ECU in Group \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") did answer, but should not.";
				TolerableErrors.add( new CError( ECUANSWERED, ERRORTXT, str ) );
			}
		} catch( ApiCallFailedException e ) {
			String errtyp;
			errtyp = Ergebnis.FT_NIO;
			if( EcuInfo != null ) {
				if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNOREERR )
					errtyp = Ergebnis.FT_IGNORE;
			}
			TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, ept.apiErrorText(), errtyp ) );
		}
	}

	/** Ermittelt die eindeutige Seriennummer (ecuUID) der ECUs und tr�gt sie in die Interne Datenstruktur ein
	 * 
	 * @param ept Ediabas
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @throws CError Fehlerobjekt, das zu sofortigem Abbruch f�hrt
	 */
	private void do_ECU_UID_LESEN( EdiabasProxyThread ept, ECU_Infos EcuInfos, Vector<CError> TolerableErrors ) throws CError {
		int Sets = 0;
		String SGBD;
		Vector<ECU_Info> VisitedECUs;
		//		Vector<ECU_Info> PhysicallyAskedECUs = new Vector<ECU_Info>(); // TODO ab SP 2021 dann k�nnen alle SG den Service
		int i;
		ECU_Info EcuInfo;
		final String JOBNAME = "ecu_uid_lesen_funktional";
		final String ERRORTXT = isDE() ? "Fehler bei " + JOBNAME : "Error during " + JOBNAME;

		EcuInfos.setAllNotVisited();

		SGBD = m_SGBD_UDS;

		// Motorrat hat keinen Job "ecu_uid_lesen_funktional" -> gar nicht erst probieren, w�rde schiefgehen. 
		if( SGBD.equals( "X_K001" ) || SGBD.equals( "X_KS01" ) )
			return;

		try {
			Sets = executeFunctionalJob( ept, SGBD, JOBNAME );
		} catch( CError cer ) {
			// Wenn Job fehlerhaft dann ignoriere dies, Job wird noch nicht durchg�ngig unterst�tzt
			TolerableErrors.add( new CError( cer.m_entry.getID(), cer.m_entry.getFehlerText(), cer.m_entry.getHinweisText(), Ergebnis.FT_IGNORE ) );
			return;
		}
		do_ECU_UID_LESEN_analyse( ept, 1, Sets, EcuInfos, TolerableErrors, ERRORTXT );

		VisitedECUs = getVisitedECUs( EcuInfos, 2 );
		if( !VisitedECUs.isEmpty() ) {
			for( i = 0; i < VisitedECUs.size(); i++ ) {
				EcuInfo = (ECU_Info) VisitedECUs.get( i );

				if( m_debug_level >= 10 )
					System.out.println( LOG_PREFIX + "Try to read ecu-uid number for SG " + Integer.toHexString( EcuInfo.m_ECU_Adr ) + " physical" );

				if( !tryPhysical( ept, EcuInfo, JOBNAME, TolerableErrors, ERRORTXT ) )
					continue;
				//				PhysicallyAskedECUs.add( EcuInfo ); // TODO ab SP 2021 dann k�nnen alle SG den Service
				do_ECU_UID_LESEN_analyse_one_set( ept, EcuInfo.m_Netz, 1, EcuInfos, TolerableErrors, ERRORTXT );
			}
			VisitedECUs = getVisitedECUs( EcuInfos, 2 );
		}
		//		checkForMisssingECUs( PhysicallyAskedECUs, ERRORTXT, TolerableErrors ); // TODO ab SP 2021 dann k�nnen alle SG den Service
	}

	/** Analysiert alle Sets des funktionalen ecu_uid_lesen Aufrufs (ecuUID = eindeutige ECU Seriennummer)
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Sets Anzahl der Sets
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT
	 */
	private void do_ECU_UID_LESEN_analyse( EdiabasProxyThread ept, int Netz, int Sets, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int i;
		for( i = 1; i < Sets; i++ ) {
			do_ECU_UID_LESEN_analyse_one_set( ept, Netz, i, EcuInfos, TolerableErrors, ERRORTXT );
		}
	}

	/** Analysiert ein Set eines ecuUID_lesen Aufrufs (ecuUID = eindeutige ECU Seriennummer)
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Set Nummer des Sets
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT
	 */
	private void do_ECU_UID_LESEN_analyse_one_set( EdiabasProxyThread ept, int Netz, int Set, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int ECUAddr;
		String action;
		String str;
		ECU_Info EcuInfo;

		EcuInfo = null;
		action = "apiResultInt(ID_SG_ADR," + Set + ")";
		try {
			ECUAddr = ept.apiResultInt( "ID_SG_ADR", Set );

			EcuInfo = EcuInfos.findByEcuAdress( ECUAddr );
			if( EcuInfo != null ) {
				//soll dieses SG �berhaupt gelesen werden?
				if( EcuInfo.m_readECUUIDnummer ) {
					EcuInfo.m_visited = true;
				}

				if( EcuInfo.m_Doku_Flag != PL_FLAG_FALSE && EcuInfo.m_Doku_Flag != PL_FLAG_IGNORE && EcuInfo.m_readECUUIDnummer ) {
					action = "apiResultText(ECU_UID, Set " + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
					try {
						EcuInfo.m_ecuUID = ept.apiResultText( "ECU_UID", Set, "" );
						EcuInfo.m_answered = true;
					} catch( ApiCallFailedException e ) {
						if( Netz == 1 )
							throw e;
						if( EcuInfo.m_ecuUID == null )
							throw e;
						if( EcuInfo.m_ecuUID.length() == 0 )
							throw e;
					}
				}
			}

			if( EcuInfo == null ) {
				String errtyp2 = Ergebnis.FT_NIO;
				if( EcuInfos.m_EcusToIgnore.contains( new Integer( ECUAddr ) ) )
					errtyp2 = Ergebnis.FT_IGNORE;

				if( isDE() )
					str = "SG an Adresse 0x" + Integer.toHexString( ECUAddr ) + " hat geantwortet, d�rfte aber nicht antworten.";
				else
					str = "ECU at Address 0x" + Integer.toHexString( ECUAddr ) + " did answer, but should not.";
				TolerableErrors.add( new CError( ECUANSWERED, ERRORTXT, str, errtyp2 ) );
			} else if( EcuInfo.m_Doku_Flag == PL_FLAG_FALSE ) {
				if( isDE() )
					str = "SG in Gruppe \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") hat geantwortet, d�rfte aber nicht antworten.";
				else
					str = "ECU in Group \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") did answer, but should not.";
				TolerableErrors.add( new CError( ECUANSWERED, ERRORTXT, str ) );
			}
		} catch( ApiCallFailedException e ) {
			String errtyp;
			errtyp = Ergebnis.FT_NIO;
			if( EcuInfo != null ) {
				if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNOREERR || !EcuInfo.m_answered || !EcuInfo.m_readECUUIDnummer || ept.apiErrorText().toUpperCase().indexOf( "RESULT NOT FOUND" ) > 0 )
					errtyp = Ergebnis.FT_IGNORE;
			}
			TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, ept.apiErrorText(), errtyp ) );
		}
	}

	/** Ermittelt (nicht funktional) optional Herstelldatum und Lieferantennummer der ECus.
	 * 
	 * @param ept Ediabas
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @throws CError Fehlerobjekt, das zu sofortigem Abbruch f�hrt
	 */
	private void do_HERSTELLINFO_LESEN( EdiabasProxyThread ept, ECU_Infos EcuInfos, Vector<CError> TolerableErrors ) throws CError {
		int i;
		ECU_Info EcuInfo;
		String action;
		String tmp;
		int pos1, pos2;
		String year, month, day;

		for( i = 0; i < EcuInfos.nrOfECUs(); i++ ) {
			EcuInfo = EcuInfos.getECUInfo( i );

			if( EcuInfo.m_Doku_Flag != PL_FLAG_FALSE && EcuInfo.m_Doku_Flag != PL_FLAG_IGNORE ) {
				if( EcuInfo.m_readHerstelldatum && EcuInfo.m_Netz == 1 ) {
					final String JOBNAME = "herstelldatum_lesen_funktional";
					final String ERRORTXT = isDE() ? "Fehler bei " + JOBNAME : "Error during " + JOBNAME;

					if( tryPhysical( ept, EcuInfo, JOBNAME, TolerableErrors, null ) ) {
						action = "apiResultText(HERSTELLDATUM," + i + ")";
						try {
							tmp = ept.apiResultText( "HERSTELLDATUM", 1, "" );
							EcuInfo.m_Herstelldatum = "2001-01-01";
							pos1 = tmp.indexOf( "." );
							if( pos1 >= 0 ) {
								pos2 = tmp.indexOf( ".", pos1 + 1 );
								if( pos2 >= 0 ) {
									day = tmp.substring( 0, pos1 );
									month = tmp.substring( pos1 + 1, pos2 );
									year = tmp.substring( pos2 + 1 );
									while( day.length() < 2 )
										day = "0" + day;
									while( month.length() < 2 )
										month = "0" + month;
									while( year.length() < 3 )
										year = "0" + year;
									if( year.length() == 3 )
										year = "2" + year;
									EcuInfo.m_Herstelldatum = year + "-" + month + "-" + day;
								}
							}
						} catch( ApiCallFailedException e ) {
							String errtyp;
							errtyp = Ergebnis.FT_NIO;
							if( EcuInfo != null ) {
								if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNOREERR )
									errtyp = Ergebnis.FT_IGNORE;
							}
							TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, ept.apiErrorText(), errtyp ) );
						}
					}
				}

				if( EcuInfo.m_readLieferantennummer ) {
					final String JOBNAME = "herstellinfo_lesen";
					final String ERRORTXT = isDE() ? "Fehler bei " + JOBNAME : "Error during " + JOBNAME;

					executeJob( ept, EcuInfo.m_ECU_SGBD, JOBNAME );
					action = "apiResultText(ID_LIEF_NR," + i + ")";
					try {
						EcuInfo.m_Lieferantennummer = ept.apiResultText( "ID_LIEF_NR", 1, "" );
					} catch( ApiCallFailedException e ) {
						String errtyp;
						errtyp = Ergebnis.FT_NIO;
						if( EcuInfo != null ) {
							if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNOREERR )
								errtyp = Ergebnis.FT_IGNORE;
						}
						TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, ept.apiErrorText(), errtyp ) );
					}
				}
			}
		}
	}

	/** Ermittelt SVK Daten der ECUs und tr�gt sie in die Interne Datenstruktur ein
	 * 
	 * @param ept Ediabas
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @throws CError Fehlerobjekt, das zu sofortigem Abbruch f�hrt
	 */
	private void do_SVK_LESEN( EdiabasProxyThread ept, ECU_Infos EcuInfos, Vector<CError> TolerableErrors ) throws CError {
		int Sets;
		String SGBD;
		Vector<ECU_Info> MissingECUs;
		int i;
		ECU_Info EcuInfo;
		final String JOBNAME = "svk_lesen_funktional";
		final String ERRORTXT = isDE() ? "Fehler bei " + JOBNAME : "Error during " + JOBNAME;

		EcuInfos.setAllNotVisited();

		SGBD = m_SGBD_UDS;
		Sets = executeFunctionalJob( ept, SGBD, JOBNAME );
		do_SVK_LESEN_analyse( ept, 1, Sets, EcuInfos, TolerableErrors, ERRORTXT );

		MissingECUs = getMissingECUs( EcuInfos, 2 );
		if( !MissingECUs.isEmpty() ) {
			for( i = 0; i < MissingECUs.size(); i++ ) {
				EcuInfo = (ECU_Info) MissingECUs.get( i );

				if( m_debug_level >= 10 )
					System.out.println( LOG_PREFIX + "Try to read svks for SG " + Integer.toHexString( EcuInfo.m_ECU_Adr ) + " physical" );

				if( !tryPhysical( ept, EcuInfo, JOBNAME, TolerableErrors, ERRORTXT ) )
					continue;
				do_SVK_LESEN_analyse_one_set( ept, EcuInfo.m_Netz, 1, EcuInfos, TolerableErrors, ERRORTXT );
			}
			MissingECUs = getMissingECUs( EcuInfos, 2 );
		}
		checkForMisssingECUs( MissingECUs, ERRORTXT, TolerableErrors );
	}

	/** Analysiert alle Sets des funktionalen svk_lesen Aufrufs
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Sets Anzahl der Sets
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT Allgemeiner Fehlertext, enth�lt Name des funktionalen Jobs.
	 */
	private void do_SVK_LESEN_analyse( EdiabasProxyThread ept, int Netz, int Sets, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int i;
		for( i = 1; i < Sets; i++ ) {
			do_SVK_LESEN_analyse_one_set( ept, Netz, i, EcuInfos, TolerableErrors, ERRORTXT );
		}
	}

	/** Analysiert ein Set eines svk_lesen Aufrufs
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Set Nummer des Sets
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT Allgemeiner Fehlertext, enth�lt Name des funktionalen Jobs.
	 */
	private void do_SVK_LESEN_analyse_one_set( EdiabasProxyThread ept, int Netz, int Set, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int j;
		int ECUAddr;
		String action, str;
		ECU_Info EcuInfo;
		SVK_Entry svk_entry;
		Vector<SVK_Entry> svk_entries;
		String resultname;

		action = "apiResultInt(ID_SG_ADR," + Set + ")";
		svk_entries = new Vector<SVK_Entry>();
		EcuInfo = null;
		try {
			ECUAddr = ept.apiResultInt( "ID_SG_ADR", Set );

			EcuInfo = EcuInfos.findByEcuAdress( ECUAddr );
			if( EcuInfo == null ) {
				String errtyp2 = Ergebnis.FT_NIO;
				if( EcuInfos.m_EcusToIgnore.contains( new Integer( ECUAddr ) ) )
					errtyp2 = Ergebnis.FT_IGNORE;

				if( isDE() )
					str = "SG an Adresse 0x" + Integer.toHexString( ECUAddr ) + " hat geantwortet, d�rfte aber nicht antworten.";
				else
					str = "ECU at Address 0x" + Integer.toHexString( ECUAddr ) + " did answer, but should not.";
				TolerableErrors.add( new CError( ECUANSWERED, ERRORTXT, str, errtyp2 ) );
			} else if( EcuInfo.m_Doku_Flag == PL_FLAG_FALSE ) {
				if( isDE() )
					str = "SG in Gruppe \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") hat geantwortet, d�rfte aber nicht antworten.";
				else
					str = "ECU in Group \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") did answer, but should not.";
				TolerableErrors.add( new CError( ECUANSWERED, ERRORTXT, str ) );
			}

			if( EcuInfo != null ) {
				EcuInfo.m_visited = true;
				if( EcuInfo.m_Doku_Flag != PL_FLAG_FALSE && EcuInfo.m_Doku_Flag != PL_FLAG_IGNORE ) {
					j = 0;
					while( true ) {
						svk_entry = new SVK_Entry();
						resultname = "XWE" + (j + 1) + "_VERSION";
						try {
							svk_entry.m_Version = ept.apiResultText( resultname, Set, "" );
						} catch( ApiCallFailedException e ) {
							break;
						}

						resultname = "XWE" + (j + 1) + "_SGBM_IDENTIFIER";
						action = "apiResultText(" + resultname + ", " + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						svk_entry.m_SGBM_ID = ept.apiResultText( resultname, Set, "" );
						resultname = "XWE" + (j + 1) + "_PROZESSKLASSE_KURZTEXT";
						action = "apiResultText(" + resultname + ", " + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						svk_entry.m_Prozessklasse = ept.apiResultText( resultname, Set, "" );
						svk_entries.add( svk_entry );
						j++;
					}

					EcuInfo.m_SVK_Entries = new SVK_Entry[svk_entries.size()];
					for( j = 0; j < svk_entries.size(); j++ )
						EcuInfo.m_SVK_Entries[j] = (SVK_Entry) (svk_entries.get( j ));
				}
			}
		} catch( ApiCallFailedException e ) {
			String errtyp;
			errtyp = Ergebnis.FT_NIO;
			if( EcuInfo != null ) {
				if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNOREERR )
					errtyp = Ergebnis.FT_IGNORE;
			}
			TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, ept.apiErrorText(), errtyp ) );
		}
	}

	/** Ermittelt CALID und CVN Daten der ECUs und tr�gt sie in die Interne Datenstruktur ein
	 * 
	 * @param ept Ediabas
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @throws CError Fehlerobjekt, das zu sofortigem Abbruch f�hrt
	 */
	private void do_CALID_LESEN( EdiabasProxyThread ept, ECU_Infos EcuInfos, Vector<CError> TolerableErrors ) throws CError {
		int Sets;
		String SGBD;
		final String JOBNAME = "calid_cvn_lesen_funktional";
		final String ERRORTXT = isDE() ? "Fehler bei " + JOBNAME : "Error during " + JOBNAME;

		EcuInfos.setAllNotVisited();

		SGBD = m_SGBD_UDS;

		Sets = executeFunctionalJob( ept, SGBD, JOBNAME );
		do_CALID_LESEN_analyse( ept, 1, Sets, EcuInfos, TolerableErrors, ERRORTXT );
	}

	/** Analysiert alle Sets des funktionalen calid_cvn_lesen Aufrufs
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Sets Anzahl der Sets
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT Allgemeiner Fehlertext, enth�lt Name des funktionalen Jobs.
	 */
	private void do_CALID_LESEN_analyse( EdiabasProxyThread ept, int Netz, int Sets, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int i;
		for( i = 1; i < Sets; i++ ) {
			do_CALID_LESEN_analyse_one_set( ept, Netz, i, EcuInfos, TolerableErrors, ERRORTXT );
		}
	}

	/** Analysiert ein Set eines calid_cvn_lesen Aufrufs
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Set Nummer des Sets
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT Allgemeiner Fehlertext, enth�lt Name des funktionalen Jobs.
	 */
	private void do_CALID_LESEN_analyse_one_set( EdiabasProxyThread ept, int Netz, int Set, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int j;
		int ECUAddr;
		String action, str;
		ECU_Info EcuInfo;
		CALIDCVN_Entry calidcvn_entry;
		Vector<CALIDCVN_Entry> calidcvn_entries;
		String resultname;

		action = "apiResultInt(ID_SG_ADR," + Set + ")";
		calidcvn_entries = new Vector<CALIDCVN_Entry>();
		EcuInfo = null;
		try {
			ECUAddr = ept.apiResultInt( "ID_SG_ADR", Set );

			EcuInfo = EcuInfos.findByEcuAdress( ECUAddr );
			if( EcuInfo == null ) {
				String errtyp2 = Ergebnis.FT_NIO;
				if( EcuInfos.m_EcusToIgnore.contains( new Integer( ECUAddr ) ) )
					errtyp2 = Ergebnis.FT_IGNORE;

				if( isDE() )
					str = "SG an Adresse 0x" + Integer.toHexString( ECUAddr ) + " hat geantwortet, d�rfte aber nicht antworten.";
				else
					str = "ECU at Address 0x" + Integer.toHexString( ECUAddr ) + " did answer, but should not.";
				TolerableErrors.add( new CError( ECUANSWERED, ERRORTXT, str, errtyp2 ) );
			} else if( EcuInfo.m_Doku_Flag == PL_FLAG_FALSE ) {
				if( isDE() )
					str = "SG in Gruppe \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") hat geantwortet, d�rfte aber nicht antworten.";
				else
					str = "ECU in Group \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") did answer, but should not.";
				TolerableErrors.add( new CError( ECUANSWERED, ERRORTXT, str ) );
			}

			if( EcuInfo != null ) {
				EcuInfo.m_visited = true;
				if( EcuInfo.m_Doku_Flag != PL_FLAG_FALSE && EcuInfo.m_Doku_Flag != PL_FLAG_IGNORE ) {
					j = 0;
					while( true ) {
						calidcvn_entry = new CALIDCVN_Entry();
						resultname = "CALID" + (j + 1);
						try {
							calidcvn_entry.m_calid = ept.apiResultText( resultname, Set, "" );
						} catch( ApiCallFailedException e ) {
							break;
						}

						resultname = "CVN" + (j + 1);
						action = "apiResultText(" + resultname + ", " + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						calidcvn_entry.m_cvn = ept.apiResultText( resultname, Set, "" );
						calidcvn_entry.m_position = (j + 1);
						calidcvn_entries.add( calidcvn_entry );
						j++;
					}

					EcuInfo.m_CALIDCVN_Entries = new CALIDCVN_Entry[calidcvn_entries.size()];
					for( j = 0; j < calidcvn_entries.size(); j++ )
						EcuInfo.m_CALIDCVN_Entries[j] = (CALIDCVN_Entry) (calidcvn_entries.get( j ));
				}
			}
		} catch( ApiCallFailedException e ) {
			String errtyp;
			errtyp = Ergebnis.FT_NIO;
			if( EcuInfo != null ) {
				if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNOREERR )
					errtyp = Ergebnis.FT_IGNORE;
			}
			TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, ept.apiErrorText(), errtyp ) );
		}
	}

	/** Ermittelt die assembly part no f�r alle BN2K SGs
	 * 
	 * @param ept Ediabas
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @throws CError Fehlerobjekt, das zu sofortigem Abbruch f�hrt
	 */
	private void do_AIF_LESEN( EdiabasProxyThread ept, ECU_Infos EcuInfos, Vector<CError> TolerableErrors ) throws CError {
		int Sets;
		String SGBD;
		Vector<ECU_Info> MissingECUs;
		int i;
		ECU_Info EcuInfo;
		final String JOBNAME = "aif_lesen_funktional";
		final String ERRORTXT = isDE() ? "Fehler bei " + JOBNAME : "Error during " + JOBNAME;

		EcuInfos.setAllNotVisited();

		SGBD = m_SGBD_BN2K;
		Sets = executeFunctionalJob( ept, SGBD, JOBNAME );
		do_AIF_LESEN_analyse( ept, 2, Sets, EcuInfos, TolerableErrors, ERRORTXT );

		MissingECUs = getMissingECUs( EcuInfos, 1 );
		if( !MissingECUs.isEmpty() ) {
			for( i = 0; i < MissingECUs.size(); i++ ) {
				EcuInfo = (ECU_Info) MissingECUs.get( i );

				if( m_debug_level >= 10 )
					System.out.println( LOG_PREFIX + "Try to read aif for SG " + Integer.toHexString( EcuInfo.m_ECU_Adr ) + " physical" );

				if( !tryPhysical( ept, EcuInfo, JOBNAME, TolerableErrors, ERRORTXT ) )
					continue;
				do_AIF_LESEN_analyse_one_set( ept, EcuInfo.m_Netz, 1, EcuInfos, TolerableErrors, ERRORTXT );
			}
			MissingECUs = getMissingECUs( EcuInfos, 1 );
		}
		checkForMisssingECUs( MissingECUs, ERRORTXT, TolerableErrors );
	}

	/** Analysiert alle Sets des funktionalen aif_lesen Aufrufs
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Sets Anzahl der Sets
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT Allgemeiner Fehlertext, enth�lt Name des funktionalen Jobs.
	 */
	private void do_AIF_LESEN_analyse( EdiabasProxyThread ept, int Netz, int Sets, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int i;
		for( i = 1; i < Sets; i++ ) {
			do_AIF_LESEN_analyse_one_set( ept, Netz, i, EcuInfos, TolerableErrors, ERRORTXT );
		}
	}

	/** Analysiert ein Set eines funktionalen aif_lesen Aufrufs
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Set Nummer des Sets
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT Allgemeiner Fehlertext, enth�lt Name des funktionalen Jobs.
	 */
	private void do_AIF_LESEN_analyse_one_set( EdiabasProxyThread ept, int Netz, int Set, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int ECUAddr;
		String action;
		ECU_Info EcuInfo;
		String errtyp;
		String str;

		EcuInfo = null;
		action = "apiResultInt(ID_SG_ADR," + Set + ")";
		try {
			ECUAddr = ept.apiResultInt( "ID_SG_ADR", Set );
			EcuInfo = EcuInfos.findByEcuAdress( ECUAddr );

			errtyp = Ergebnis.FT_NIO;
			if( EcuInfo != null ) {
				if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNOREERR )
					errtyp = Ergebnis.FT_IGNORE;
			}

			if( EcuInfo == null ) {
				String errtyp2 = errtyp;
				if( EcuInfos.m_EcusToIgnore.contains( new Integer( ECUAddr ) ) )
					errtyp2 = Ergebnis.FT_IGNORE;

				if( isDE() )
					str = "SG 0x" + Integer.toHexString( ECUAddr ) + " hat geantwortet, d�rfte aber nicht antworten.";
				else
					str = "ECU 0x" + Integer.toHexString( ECUAddr ) + " did answer, but should not.";
				TolerableErrors.add( new CError( ECUANSWERED, ERRORTXT, str, errtyp2 ) );
			} else if( EcuInfo.m_Doku_Flag == PL_FLAG_FALSE ) {
				if( isDE() )
					str = "SG in Gruppe \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") hat geantwortet, d�rfte aber nicht antworten.";
				else
					str = "ECU in Group \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") did answer, but should not.";
				TolerableErrors.add( new CError( ECUANSWERED, ERRORTXT, str, errtyp ) );
			}

			if( EcuInfo != null ) {
				EcuInfo.m_visited = true;
				if( EcuInfo.m_Doku_Flag != PL_FLAG_IGNORE && EcuInfo.m_Doku_Flag != PL_FLAG_FALSE ) {
					if( EcuInfo.m_Netz != Netz ) {
						if( Netz == 1 ) {
							if( isDE() )
								str = "SG in Gruppe \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") hat im UDS Netz geantwortet, sollte aber im Bordnetz 2000 sein.";
							else
								str = "ECU in Group \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") answered in UDS net, but should be in boardnet 2000.";
						} else {
							if( isDE() )
								str = "SG in Gruppe \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") hat im Bordnetz 2000 geantwortet, sollte aber im UDS Netz sein.";
							else
								str = "ECU in Group \"" + EcuInfo.m_ECU_GRP + "\" (0x" + Integer.toHexString( ECUAddr ) + ") answered in boardnet 2000, but should be in UDS net.";
						}
						TolerableErrors.add( new CError( WRONGNET, ERRORTXT, str, errtyp ) );
					} else if( Netz == 2 ) {
						action = "apiResultText(AIF_ZB_NR," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						EcuInfo.m_assembly_No = ept.apiResultText( "AIF_ZB_NR", Set, "" );
					}
				}
			}
		} catch( ApiCallFailedException e ) {
			errtyp = Ergebnis.FT_NIO;
			if( EcuInfo != null ) {
				if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNOREERR )
					errtyp = Ergebnis.FT_IGNORE;
			}
			TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, ept.apiErrorText(), errtyp ) );
		}
	}

	/** Ermittelt den �nderungscodierindex f�r alle BN2K SGs
	 * 
	 * @param ept Ediabas
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @throws CError Fehlerobjekt, das zu sofortigem Abbruch f�hrt
	 */
	private void do_C_AEI_LESEN( EdiabasProxyThread ept, ECU_Infos EcuInfos, Vector<CError> TolerableErrors ) throws CError {
		int Sets;
		String SGBD;
		Vector<ECU_Info> MissingECUs;
		int i;
		ECU_Info EcuInfo;
		final String JOBNAME = "c_aei_lesen_funktional";
		final String ERRORTXT = isDE() ? "Fehler bei " + JOBNAME : "Error during " + JOBNAME;

		EcuInfos.setAllNotVisited();

		SGBD = m_SGBD_BN2K;
		Sets = executeFunctionalJob( ept, SGBD, JOBNAME );
		do_C_AEI_LESEN_analyse( ept, 2, Sets, EcuInfos, TolerableErrors, ERRORTXT );

		MissingECUs = getMissingECUs( EcuInfos, 1 );
		if( !MissingECUs.isEmpty() ) {
			for( i = 0; i < MissingECUs.size(); i++ ) {
				EcuInfo = (ECU_Info) MissingECUs.get( i );

				if( m_debug_level >= 10 )
					System.out.println( LOG_PREFIX + "Try to read c_aei for SG " + Integer.toHexString( EcuInfo.m_ECU_Adr ) + " physical" );

				if( !tryPhysical( ept, EcuInfo, JOBNAME, TolerableErrors, ERRORTXT ) )
					continue;
				do_C_AEI_LESEN_analyse_one_set( ept, EcuInfo.m_Netz, 1, EcuInfos, TolerableErrors, ERRORTXT );
			}
			MissingECUs = getMissingECUs( EcuInfos, 1 );
		}
		checkForMisssingECUs( MissingECUs, ERRORTXT, TolerableErrors );
	}

	/** Analysiert alle Sets des funktionalen c_aei_lesen Aufrufs
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Sets Anzahl der Sets
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT Allgemeiner Fehlertext, enth�lt Name des funktionalen Jobs.
	 */
	private void do_C_AEI_LESEN_analyse( EdiabasProxyThread ept, int Netz, int Sets, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int i;
		for( i = 1; i < Sets; i++ ) {
			do_C_AEI_LESEN_analyse_one_set( ept, Netz, i, EcuInfos, TolerableErrors, ERRORTXT );
		}
	}

	/** Analysiert ein Set eines funktionalen c_aei_lesen Aufrufs
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Set Nummer des Sets
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT Allgemeiner Fehlertext, enth�lt Name des funktionalen Jobs.
	 */
	private void do_C_AEI_LESEN_analyse_one_set( EdiabasProxyThread ept, int Netz, int Set, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int ECUAddr;
		String action;
		ECU_Info EcuInfo;
		String errtyp;
		String str;

		EcuInfo = null;
		action = "apiResultInt(ID_SG_ADR," + Set + ")";
		try {
			ECUAddr = ept.apiResultInt( "ID_SG_ADR", Set );
			EcuInfo = EcuInfos.findByEcuAdress( ECUAddr );

			errtyp = Ergebnis.FT_NIO;
			if( EcuInfo != null ) {
				if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNOREERR )
					errtyp = Ergebnis.FT_IGNORE;
			}

			if( EcuInfo == null ) {
				String errtyp2 = errtyp;
				if( EcuInfos.m_EcusToIgnore.contains( new Integer( ECUAddr ) ) )
					errtyp2 = Ergebnis.FT_IGNORE;

				if( isDE() )
					str = "SG 0x" + Integer.toHexString( ECUAddr ) + " hat geantwortet, d�rfte aber nicht antworten.";
				else
					str = "ECU 0x" + Integer.toHexString( ECUAddr ) + " did answer, but should not.";
				TolerableErrors.add( new CError( ECUANSWERED, ERRORTXT, str, errtyp2 ) );
			} else if( EcuInfo.m_Doku_Flag == PL_FLAG_FALSE ) {
				if( isDE() )
					str = "SG in Gruppe " + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ") hat geantwortet, d�rfte aber nicht antworten.";
				else
					str = "ECU in Group " + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ") did answer, but should not.";
				TolerableErrors.add( new CError( ECUANSWERED, ERRORTXT, str, errtyp ) );
			}

			if( EcuInfo != null ) {
				EcuInfo.m_visited = true;
				if( EcuInfo.m_Doku_Flag != PL_FLAG_IGNORE && EcuInfo.m_Doku_Flag != PL_FLAG_FALSE ) {
					if( EcuInfo.m_Netz != Netz ) {
						if( Netz == 1 ) {
							if( isDE() )
								str = "SG in Gruppe " + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ") hat im UDS Netz geantwortet, sollte aber im Bordnetz 2000 sein.";
							else
								str = "ECU in Group " + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ") answered in UDS net, but should be in boardnet 2000.";
						} else {
							if( isDE() )
								str = "SG in Gruppe " + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ") hat im Bordnetz 2000 geantwortet, sollte aber im UDS Netz sein.";
							else
								str = "ECU in Group " + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ") answered in boardnet 2000, but should be in UDS net.";
						}
						TolerableErrors.add( new CError( WRONGNET, ERRORTXT, str, errtyp ) );
					} else if( Netz == 2 ) {
						action = "apiResultText(COD_AE_INDEX," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
						EcuInfo.m_c_aei_Index = ept.apiResultText( "COD_AE_INDEX", Set, "" );
					}
				}
			}
		} catch( ApiCallFailedException e ) {
			errtyp = Ergebnis.FT_NIO;
			if( EcuInfo != null ) {
				if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNOREERR )
					errtyp = Ergebnis.FT_IGNORE;
			}
			TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, ept.apiErrorText(), errtyp ) );
		}
	}

	/** Ermittelt Sensor-Daten und tr�gt sie in die interne Datenstruktur ein
	 * 
	 * @param ept Ediabas
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten (und die SGs enthalten die erwarteten Sensoren)
	 * @param sensor_job_uds (Optional konfigurierbarer Job um Sensoren auszulesen)
	 * @param readfunctional true: Sensoren funktional auslesen (mit Notstrategie physikalisch), false: Sensoren nur physikalisch auslesen 
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @throws CError Fehlerobjekt, das zu sofortigem Abbruch f�hrt
	 */
	private void do_SENSOREN_IDENT_LESEN( EdiabasProxyThread ept, ECU_Infos EcuInfos, String sensor_job_uds, boolean readfunctional, Vector<CError> TolerableErrors ) throws CError {
		int Netz, Sets, EcuNr;
		String SGBD;
		Vector<Object> MissingSensors;
		ECU_Info EcuInfo;
		int i;
		final String sensor_job_kwp2000 = "sensoren_ident_lesen_funktional";
		String JOBNAME;
		String ERRORTXT;
		HashMap<Integer, ECU_Info> EcusWithMissingSensors = new HashMap<Integer, ECU_Info>();
		Iterator<Integer> it;
		Integer EcuAdr;

		EcuInfos.setAllNotVisited();

		if( readfunctional ) {
			for( Netz = 1; Netz <= 2; Netz++ ) {
				SGBD = (Netz == 1 ? m_SGBD_UDS : m_SGBD_BN2K);
				JOBNAME = (Netz == 1 ? sensor_job_uds : sensor_job_kwp2000);
				ERRORTXT = isDE() ? "Fehler bei " + JOBNAME : "Error during " + JOBNAME;
				Sets = executeFunctionalJob( ept, SGBD, JOBNAME );
				do_SENSOREN_IDENT_LESEN_analyse( ept, Netz, Sets, EcuInfos, TolerableErrors, ERRORTXT );
			}
		}

		MissingSensors = getMissingSensors( EcuInfos );
		if( !MissingSensors.isEmpty() ) {
			for( i = 0; i < MissingSensors.size(); i += 2 ) {
				EcuInfo = (ECU_Info) MissingSensors.get( i + 1 );
				EcusWithMissingSensors.put( new Integer( EcuInfo.m_ECU_Adr ), EcuInfo );
			}

			it = EcusWithMissingSensors.keySet().iterator();
			while( it.hasNext() ) {
				EcuAdr = (Integer) it.next();
				EcuInfo = (ECU_Info) EcusWithMissingSensors.get( EcuAdr );

				JOBNAME = (EcuInfo.m_Netz == 1 ? sensor_job_uds : sensor_job_kwp2000);
				ERRORTXT = isDE() ? "Fehler bei " + JOBNAME : "Error during " + JOBNAME;

				if( m_debug_level >= 10 )
					System.out.println( LOG_PREFIX + "Try to read sensors for SG " + Integer.toHexString( EcuAdr.intValue() ) + " physical" );

				if( !tryPhysical( ept, EcuInfo, JOBNAME, TolerableErrors, readfunctional ? ERRORTXT : null ) )
					continue;

				try {
					Sets = ept.apiResultSets();
				} catch( ApiCallFailedException e ) {
					Sets = 0;
				}

				for( i = 1; i < Sets; i++ ) {
					do_SENSOREN_IDENT_LESEN_analyse_one_set( ept, EcuInfo.m_Netz, i, EcuInfos, TolerableErrors, ERRORTXT );
				}
			}

			MissingSensors = getMissingSensors( EcuInfos );
		}

		// Sonderbehandlung f�r Steuerger�te, die zu bl�d sind, Ihre Sensoren funktional zu melden:
		// Wenn <keine Sensoren gemeldet> UND (<Sensor_Doku_Flag==PL_FLAG_AUTO_PHYS> ODER (<Sensor_Doku_Flag==PL_FLAG_AUTO> UND !readfunctional))
		// 
		// ->dann <Physikalisch nachfragen>

		for( EcuNr = 0; EcuNr < EcuInfos.nrOfECUs(); EcuNr++ ) {
			EcuInfo = EcuInfos.getECUInfo( EcuNr );
			if( EcuInfo != null )
				if( EcuInfo.m_Subbus_Entries.getAnzahl() == 0 && (EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_AUTO_PHYS || (EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_AUTO && !readfunctional)) ) {
					JOBNAME = (EcuInfo.m_Netz == 1 ? sensor_job_uds : sensor_job_kwp2000);
					ERRORTXT = isDE() ? "Fehler bei " + JOBNAME : "Error during " + JOBNAME;

					if( m_debug_level >= 10 )
						System.out.println( LOG_PREFIX + "AUTO sensor doku: Try to read sensors for SG " + Integer.toHexString( EcuInfo.m_ECU_Adr ) + " physical" );

					if( tryPhysical( ept, EcuInfo, JOBNAME, TolerableErrors, readfunctional ? ERRORTXT : null ) ) {
						try {
							Sets = ept.apiResultSets();
						} catch( ApiCallFailedException e ) {
							Sets = 0;
						}

						for( i = 1; i < Sets; i++ ) {
							do_SENSOREN_IDENT_LESEN_analyse_one_set( ept, EcuInfo.m_Netz, i, EcuInfos, TolerableErrors, ERRORTXT );
						}
					}
				}
		}

		JOBNAME = sensor_job_uds;
		ERRORTXT = isDE() ? "Fehler bei " + JOBNAME : "Error during " + JOBNAME;

		checkForMissingSensors( MissingSensors, ERRORTXT, TolerableErrors );
		checkForDoubleSensors( EcuInfos, ERRORTXT, TolerableErrors );
	}

	/** Analysiert alle Sets des funktionalen sensoren_ident_lesen Aufrufs
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Sets Anzahl der Sets
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten und Sensoren
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT Allgemeiner Fehlertext, enth�lt Name des funktionalen Jobs.
	 */
	private void do_SENSOREN_IDENT_LESEN_analyse( EdiabasProxyThread ept, int Netz, int Sets, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int i;
		for( i = 1; i < Sets; i++ ) {
			do_SENSOREN_IDENT_LESEN_analyse_one_set( ept, Netz, i, EcuInfos, TolerableErrors, ERRORTXT );
		}
	}

	/** Analysiert ein Set eines  sensoren_ident_lesen Aufrufs
	 * 
	 * @param ept Ediabas
	 * @param Netz 1: UDS, 2: BN2K
	 * @param Set Nummer des Sets
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten und Sensoren
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @param ERRORTXT Allgemeiner Fehlertext, enth�lt Name des funktionalen Jobs.
	 */
	private void do_SENSOREN_IDENT_LESEN_analyse_one_set( EdiabasProxyThread ept, int Netz, int Set, ECU_Infos EcuInfos, Vector<CError> TolerableErrors, String ERRORTXT ) {
		int ECUAddr;
		String action, str, VerbauortBez = "?";
		ECU_Info EcuInfo;
		int VerbauortNr;
		Subbus_Entry subbus_entry;
		boolean EcuAnsweredButShouldNot;
		//		String jobResult = "";

		action = "apiResultInt(ID_SG_ADR," + Set + ")";
		EcuInfo = null;
		try {
			ECUAddr = ept.apiResultInt( "ID_SG_ADR", Set );
			EcuInfo = EcuInfos.findByEcuAdress( ECUAddr );

			if( EcuInfo != null ) {
				if( EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_IGNORE )
					return;
			}

			//			// JobStatus noch �berpr�fen, pro Satz
			//			action = "apiResultText(JOB_STATUS," + Set + ")";
			//			jobResult = ept.apiResultText( "JOB_STATUS", Set, "" );
			//			if( !jobResult.equalsIgnoreCase( "OKAY" ) ) {
			//				throw new Exception();
			//			}

			if( EcuInfo == null )
				action = "apiResultInt(SENSOR_VERBAUORT_NR," + Set + ") ECU=0x" + Integer.toHexString( ECUAddr );
			else
				action = "apiResultInt(SENSOR_VERBAUORT_NR," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
			VerbauortNr = ept.apiResultInt( "SENSOR_VERBAUORT_NR", Set );
			while( VerbauortNr < 0 ) {
				VerbauortNr += 65536;
			}

			// Klartextbezeichnung des Sensors (VerbauortBez) ermitteln und bei Fehlern mit ausgeben
			if( EcuInfo == null )
				action = "apiResultText(SENSOR_VERBAUORT," + Set + ") ECU=0x" + Integer.toHexString( ECUAddr );
			else
				action = "apiResultText(SENSOR_VERBAUORT," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
			VerbauortBez = ept.apiResultText( "SENSOR_VERBAUORT", Set, "" );

			if( m_debug_level >= 20 ) {
				System.out.println( LOG_PREFIX + "Sensor found for ECU 0x" + Integer.toHexString( ECUAddr ) + ", Verbauort=" + VerbauortNr + " (dec), Verbauortbez.=" + VerbauortBez );
			}

			if( EcuInfo != null ) {
				String errtyp;
				errtyp = Ergebnis.FT_NIO;
				if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNOREERR )
					errtyp = Ergebnis.FT_IGNORE;

				subbus_entry = EcuInfo.m_Subbus_Entries.findEntry( VerbauortNr );

				EcuAnsweredButShouldNot = false;

				if( EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_FALSE ) {
					EcuAnsweredButShouldNot = true;
				} else if( EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_TRUE ) {
					if( subbus_entry == null ) {
						EcuAnsweredButShouldNot = true;
					} else if( subbus_entry.m_Sensor_Doku_Flag == PL_FLAG_FALSE ) {
						EcuAnsweredButShouldNot = true;
					}
				}

				if( EcuAnsweredButShouldNot ) {
					if( isDE() )
						str = "Sensor 0x" + Integer.toHexString( VerbauortNr ) + " (" + VerbauortBez + ") " + " in SG " + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ") hat geantwortet, d�rfte aber nicht antworten.";
					else
						str = "Sensor 0x" + Integer.toHexString( VerbauortNr ) + " (" + VerbauortBez + ") " + " in ECU " + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ") did answer, but should not.";
					TolerableErrors.add( new CError( SENSORANSWERED, ERRORTXT, str, errtyp ) );
				}

				if( EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_AUTO || EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_AUTO_PHYS ) {
					subbus_entry = EcuInfo.m_Subbus_Entries.findEntry( VerbauortNr );
					if( subbus_entry == null ) {
						subbus_entry = new Subbus_Entry( VerbauortNr, PL_FLAG_TRUE );
						subbus_entry.m_visited = true;
						subbus_entry.m_BMW_Nr = ept.apiResultText( "SENSOR_BMW_NR", Set, "" );
						subbus_entry.m_Part_Nr = ept.apiResultText( "SENSOR_PART_NR", Set, "" );

						EcuInfo.m_Subbus_Entries.addSubbus( subbus_entry );
					} else {
						if( isDE() )
							str = "In SG " + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ") wurden mehrere Sensoren mit Verbauort 0x" + Integer.toHexString( VerbauortNr ) + " (" + VerbauortBez + ") " + " gemeldet";
						else
							str = "In ECU " + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ") were several sensor found with SENSOR_VERBAUORT_NR 0x" + Integer.toHexString( VerbauortNr ) + " (" + VerbauortBez + ") ";
						TolerableErrors.add( new CError( SENSORANSWERED, ERRORTXT, str, errtyp ) );
					}
				} else if( subbus_entry != null ) {
					if( subbus_entry.m_visited == true ) {
						if( EcuInfo.m_Sensor_Doku_Flag != PL_FLAG_IGNORE && subbus_entry.m_Sensor_Doku_Flag != PL_FLAG_IGNORE ) {
							if( isDE() )
								str = "In SG " + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ") wurden mehrere Sensoren mit Verbauort 0x" + Integer.toHexString( VerbauortNr ) + " (" + VerbauortBez + ") " + " gemeldet";
							else
								str = "In ECU " + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ") were several sensor found with SENSOR_VERBAUORT_NR 0x" + Integer.toHexString( VerbauortNr ) + " (" + VerbauortBez + ") ";
							TolerableErrors.add( new CError( SENSORANSWERED, ERRORTXT, str, errtyp ) );
						}
					}

					subbus_entry.m_visited = true;
					if( EcuInfo.m_Sensor_Doku_Flag != PL_FLAG_FALSE && EcuInfo.m_Sensor_Doku_Flag != PL_FLAG_IGNORE && subbus_entry.m_Sensor_Doku_Flag != PL_FLAG_FALSE && subbus_entry.m_Sensor_Doku_Flag != PL_FLAG_IGNORE ) {
						if( subbus_entry.m_Sensor_Doku_Flag == PL_FLAG_SNR || subbus_entry.m_Sensor_Doku_Flag == PL_FLAG_TRUE ) {
							action = "apiResultText(SENSOR_BMW_NR," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
							subbus_entry.m_BMW_Nr = ept.apiResultText( "SENSOR_BMW_NR", Set, "" );
						}

						if( subbus_entry.m_Sensor_Doku_Flag == PL_FLAG_SER || subbus_entry.m_Sensor_Doku_Flag == PL_FLAG_TRUE ) {
							action = "apiResultText(SENSOR_PART_NR," + Set + ") ECU=" + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( ECUAddr ) + ")";
							subbus_entry.m_Part_Nr = ept.apiResultText( "SENSOR_PART_NR", Set, "" );
						}
					}
				}
			} else {
				String errtyp2 = Ergebnis.FT_NIO;
				if( EcuInfos.m_EcusToIgnore.contains( new Integer( ECUAddr ) ) )
					errtyp2 = Ergebnis.FT_IGNORE;

				if( isDE() )
					str = "Sensor 0x" + Integer.toHexString( VerbauortNr ) + " (" + VerbauortBez + ") " + " in SG 0x" + Integer.toHexString( ECUAddr ) + " hat geantwortet, d�rfte aber nicht antworten.";
				else
					str = "Sensor 0x" + Integer.toHexString( VerbauortNr ) + " (" + VerbauortBez + ") " + " in ECU 0x" + Integer.toHexString( ECUAddr ) + " did answer, but should not.";
				TolerableErrors.add( new CError( SENSORANSWERED, ERRORTXT, str, errtyp2 ) );
			}
		} catch( ApiCallFailedException e ) {
			String errtyp;
			errtyp = Ergebnis.FT_NIO;
			if( EcuInfo != null ) {
				if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNOREERR || EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_IGNORE || ept.apiErrorText().toUpperCase().indexOf( "RESULT NOT FOUND" ) > 0 ) {
					errtyp = Ergebnis.FT_IGNORE;
				}
			}
			TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, ept.apiErrorText(), errtyp ) );
		}
		//		} catch( Exception f ) {
		//			String errtyp;
		//			errtyp = Ergebnis.FT_NIO;
		//			if( EcuInfo != null ) {
		//				if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNOREERR || EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_IGNORE ) {
		//					errtyp = Ergebnis.FT_IGNORE;
		//				}
		//			}
		//			TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, jobResult, errtyp ) );
		//		}
	}

	/** Ermittelt (nicht funktional) als Sonderablauf interimsm��ig zus�tzlich die Sensor Daten des HC2 Steuerger�tes.
	 * 
	 * @param ept Ediabas
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * TODO: Methode kann 2020 wieder entfallen
	 */
	private void do_SENSOREN_HC2_IDENT_LESEN_INTERIM( EdiabasProxyThread ept, ECU_Infos EcuInfos, Vector<CError> TolerableErrors ) {
		ECU_Info EcuInfo_HC2 = EcuInfos.m_HC2_ECU_Info;
		//		System.out.println( "XXX call methode: do_SENSOREN_HC2_IDENT_LESEN_INTERIM" );

		// wenn kein HC2 PL vorhanden ist, mache keine weiteren Aktivit�ten
		if( EcuInfo_HC2 == null ) {
			return;
		}

		EcuInfos.m_HC2_ECU_Info.m_Sensor_Doku_Flag = PL_FLAG_AUTO; // Dokumentation der Sensoren hier einschalten, obwohl dies �ber PL deaktiviert ist (vermeide PL �nderung) und dokumentiere die Sensoren

		final String sensor_job_hc2 = "sensoren_ident_lesen_hc2";
		String ERRORTXT = isDE() ? "Fehler bei " + sensor_job_hc2 : "Error during " + sensor_job_hc2;
		String action = "";
		//		System.out.println( "XXX call methode: Step 2" );

		// Versuche den Job auszuf�hren. Wenn dies fehlschl�gt Fehler mit IGNORE dokumentieren und nichts weiteres machen.
		try {
			action = "JOBSTATUS,1";
			executeJob( ept, EcuInfo_HC2.m_ECU_SGBD, sensor_job_hc2 );
		} catch( CError cex ) {
			//			cex.printStackTrace();
			TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, ept.apiErrorText(), Ergebnis.FT_IGNORE ) );
			return;
		}
		//		System.out.println( "XXX call methode: Step 3" );

		// Auslesen und dokumentieren der Sensoren. Aller 4 wenn m�glich. Wenn EDIABAS Fehler auftreten, wird dies mit IGNORE dokumentiert.
		try {
			action = "apiResultText(SENSOR_VR_VERBAUORT_NR, 1), apiResultText(SENSOR_VR_PART_NR, 1)";
			add_HC2_Subbus_Entry( ept, EcuInfo_HC2, "SENSOR_VR_VERBAUORT_NR", "SENSOR_VR_PART_NR" );
			action = "apiResultText(SENSOR_VL_VERBAUORT_NR, 1), apiResultText(SENSOR_VL_PART_NR, 1)";
			add_HC2_Subbus_Entry( ept, EcuInfo_HC2, "SENSOR_VL_VERBAUORT_NR", "SENSOR_VL_PART_NR" );
			action = "apiResultText(SENSOR_HR_VERBAUORT_NR, 1), apiResultText(SENSOR_HR_PART_NR, 1)";
			add_HC2_Subbus_Entry( ept, EcuInfo_HC2, "SENSOR_HR_VERBAUORT_NR", "SENSOR_HR_PART_NR" );
			action = "apiResultText(SENSOR_HL_VERBAUORT_NR, 1), apiResultText(SENSOR_HL_PART_NR, 1)";
			add_HC2_Subbus_Entry( ept, EcuInfo_HC2, "SENSOR_HL_VERBAUORT_NR", "SENSOR_HL_PART_NR" );
		} catch( ApiCallFailedException apiex ) {
			//			apiex.printStackTrace();
			TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, ept.apiErrorText(), Ergebnis.FT_IGNORE ) );
		}
		//		System.out.println( "XXX call methode: Step 4" );
	}

	/**
	 * Hilfsfunktion f�r den Sonderablauf zur interimsm��igen Ermittlung der Sensor Daten des HC2 Steuerger�tes
	 * 
	 * @param ept Ediabas
	 * @param EcuInfo_HC2 HC2 Ecu Info Objekt
	 * @param verbauort Verbauort Result Info
	 * @param part_nr Part Part Nr Result Info
	 * @throws ApiCallFailedException Ediabas ApiCall Exception
	 * TODO: Methode kann 2020 wieder entfallen
	 */
	private void add_HC2_Subbus_Entry( EdiabasProxyThread ept, ECU_Info EcuInfo_HC2, String verbauort, String part_nr ) throws ApiCallFailedException {
		Subbus_Entry subbus_entry = new Subbus_Entry( new Integer( ept.apiResultText( verbauort, 1, "" ) ), PL_FLAG_TRUE );
		subbus_entry.m_visited = true;
		subbus_entry.m_BMW_Nr = "--";
		subbus_entry.m_Part_Nr = ept.apiResultText( part_nr, 1, "" );

		// Im Fall des zwei kanaligen HC SG nur vorne (links, rechts), werden die folgenden Sensoren hinten mit "--" zur�ckgegeben, diese sollen dann gar nicht dokumentiert werden
		if( !subbus_entry.m_Part_Nr.startsWith( "--" ) ) {
			EcuInfo_HC2.m_Subbus_Entries.addSubbus( subbus_entry );
		}
	}

	/** Ermittelt (nicht funktional) die Daten des Batteriesensors um sie zu dokumentieren.
	 * 
	 * @param ept Ediabas
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten (und die SGs enthalten die erwarteten Sensoren)
	 * @param Ibs_Job Ediabas Job der anstatt des Standard Jobs f�r den "ident_ibs" ausgef�hrt werden soll  
	 * @param Ibs_Charging_Balance_Job Job zum Ermitteln der ChargingBalance (z.B. status_systemcheck_aep_info_2)
	 * @param Ibs_Charging_Balance_Parameter Requestparameter f�r Ibs_Charging_Balance_Job
	 * @param Ibs_Charging_Balance_Result Result des Jobs <Ibs_Charging_Balance_Job> in dem Charging Balance steht (z.B. STAT_SOC_FIT_WERT)
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 * @throws CError Fehlerobjekt, das zu sofortigem Abbruch f�hrt
	 */
	private void do_BATTERIESENSOR_LESEN( EdiabasProxyThread ept, ECU_Infos EcuInfos, String Ibs_Job, String Ibs_Charging_Balance_Job, String Ibs_Charging_Balance_Parameter, String Ibs_Charging_Balance_Result, Vector<CError> TolerableErrors ) throws CError {
		String action, v1Str, v2Str;
		int v1, v2, v3;
		final String DEFAULT_IBS_JOB = "ident_ibs";

		if( EcuInfos.m_IBSSensor == null )
			return;
		if( EcuInfos.m_IBSSensor.m_Sensor_Doku_Flag != PL_FLAG_TRUE )
			return;

		action = "";
		String JOBNAME = "";
		if( Ibs_Job == null || Ibs_Job.length() == 0 ) {
			JOBNAME = DEFAULT_IBS_JOB;
		} else {
			JOBNAME = Ibs_Job;
		}
		String ERRORTXT = isDE() ? "Fehler bei " + JOBNAME : "Error during " + JOBNAME;

		//wurde ein anderer IBS_Job parametriert? ab IBS 2015 (Bsp. SGBD: DME8FF_R)
		if( !JOBNAME.equalsIgnoreCase( DEFAULT_IBS_JOB ) ) {
			try {
				executeJob( ept, EcuInfos.m_IBSSensor.m_SGBD, JOBNAME );
				action = "apiResultInt(STAT_SERIENNUMMER_IBS2015_WERT)";
				EcuInfos.m_IBSSensor.m_SerialNo = ept.apiResultText( "STAT_SERIENNUMMER_IBS2015_WERT", 1, "" );
				action = "apiResultInt(STAT_ZUSBAUNUMMER_WERT)";
				EcuInfos.m_IBSSensor.m_assemblyNo = ept.apiResultText( "STAT_ZUSBAUNUMMER_WERT", 1, "" );
				action = "apiResultInt(STAT_HW_REF_NUMMER_IBS2015_WERT)";
				v1Str = ept.apiResultText( "STAT_HW_REF_NUMMER_IBS2015_WERT", 1, "" );
				action = "apiResultInt(STAT_SW_REF_NUMMER_IBS2015_WERT)";
				v2Str = ept.apiResultText( "STAT_SW_REF_NUMMER_IBS2015_WERT", 1, "" );
				EcuInfos.m_IBSSensor.m_SoftwareVersion = v1Str + "." + v2Str;
			} catch( ApiCallFailedException e ) {
				String errtyp;
				errtyp = Ergebnis.FT_NIO;
				TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, ept.apiErrorText(), errtyp ) );
			}
		} else {
			try {
				executeJob( ept, EcuInfos.m_IBSSensor.m_SGBD, JOBNAME );
				action = "apiResultDWord(SERIENNUMMER)";
				EcuInfos.m_IBSSensor.m_SerialNo = "" + ept.apiResultDWord( "SERIENNUMMER", 1 );
				action = "apiResultText(ID_BMW_NR)";
				EcuInfos.m_IBSSensor.m_assemblyNo = ept.apiResultText( "ID_BMW_NR", 1, "" );
				action = "apiResultInt(ZIF_PROGRAMMSTAND)";
				v1 = ept.apiResultInt( "ZIF_PROGRAMMSTAND", 1 );
				action = "apiResultInt(ZIF_STATUS)";
				v2 = ept.apiResultInt( "ZIF_STATUS", 1 );
				action = "apiResultInt(HW_REF)";
				v3 = ept.apiResultInt( "HW_REF", 1 );
				EcuInfos.m_IBSSensor.m_SoftwareVersion = Integer.toString( v1 ) + "." + Integer.toString( v2 ) + "." + Integer.toString( v3 );
			} catch( ApiCallFailedException e ) {
				String errtyp;
				errtyp = Ergebnis.FT_NIO;
				TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, ept.apiErrorText(), errtyp ) );
			}
		}
		try {
			JOBNAME = Ibs_Charging_Balance_Job;

			// Job f�r die Ladebilanz hat keine Aussagekraft mehr. (M. Riedel) Wenn dieser nicht vorhanden ist od. leer ist, dann dokumentiere nicht mehr die Ladebilanz
			if( JOBNAME == null || JOBNAME.equalsIgnoreCase( "" ) ) {
				return;
			}
			ERRORTXT = isDE() ? "Fehler bei " + JOBNAME : "Error during " + JOBNAME;
			executeJob( ept, EcuInfos.m_IBSSensor.m_Charging_Balance_SGBD, JOBNAME, Ibs_Charging_Balance_Parameter, null );
			action = "apiResultInt(" + Ibs_Charging_Balance_Result + ")";
			EcuInfos.m_IBSSensor.m_charging_balance = ept.apiResultInt( Ibs_Charging_Balance_Result, 1 );
		} catch( ApiCallFailedException e ) {
			String errtyp;
			errtyp = Ergebnis.FT_NIO;
			TolerableErrors.add( new CError( EDIABASERROR, ERRORTXT + "/" + action, ept.apiErrorText(), errtyp ) );
		}

	}

	/** Ermittelt die Menge aller SGs die nicht geantwortet haben
	 * 
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param IgnoreNetz 1: UDS Netz nicht nach fehlenden ECUs untersuchen, 2: BN2K Netz nicht nach fehlenden ECUs untersuchen, sonst: beide Netze untersuchen
	 * @return Menge der SGs die nicht geantwortet haben
	 */
	private Vector<ECU_Info> getMissingECUs( ECU_Infos EcuInfos, int IgnoreNetz ) {
		int i;
		ECU_Info EcuInfo;
		Vector<ECU_Info> erg = new Vector<ECU_Info>();

		for( i = 0; i < EcuInfos.nrOfECUs(); i++ ) {
			EcuInfo = EcuInfos.getECUInfo( i );
			if( EcuInfo.m_Netz != IgnoreNetz ) {
				if( !EcuInfo.m_visited && EcuInfo.m_Doku_Flag != PL_FLAG_FALSE && EcuInfo.m_Doku_Flag != PL_FLAG_IGNORE ) {
					erg.add( EcuInfo );
				}
			}
		}
		return erg;
	}

	/** Ermittelt die Menge aller SGs die beim funktionalen Job geantwortet haben
	 * 
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten
	 * @param IgnoreNetz 1: UDS Netz nicht nach fehlenden ECUs untersuchen, 2: BN2K Netz nicht nach fehlenden ECUs untersuchen, sonst: beide Netze untersuchen
	 * @return Menge der SGs die nicht geantwortet haben
	 */
	private Vector<ECU_Info> getVisitedECUs( ECU_Infos EcuInfos, int IgnoreNetz ) {
		int i;
		ECU_Info EcuInfo;
		Vector<ECU_Info> erg = new Vector<ECU_Info>();

		for( i = 0; i < EcuInfos.nrOfECUs(); i++ ) {
			EcuInfo = EcuInfos.getECUInfo( i );
			if( EcuInfo.m_Netz != IgnoreNetz ) {
				if( EcuInfo.m_visited && EcuInfo.m_Doku_Flag != PL_FLAG_FALSE && EcuInfo.m_Doku_Flag != PL_FLAG_IGNORE ) {
					erg.add( EcuInfo );
				}
			}
		}
		return erg;
	}

	/** Generiert Fehler f�r alle SGs die nach einem funktionalen Job keine Antwort geliefert haben 
	 * 
	 * @param MissingECUs Menge der SGs die nicht geantwortet haben
	 * @param ErrorTxt Fehlertext (Kontext des Aufrufers)
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 */
	private void checkForMisssingECUs( Vector<ECU_Info> MissingECUs, String ErrorTxt, Vector<CError> TolerableErrors ) {
		ECU_Info EcuInfo;
		String errtyp, str;
		int i;

		for( i = 0; i < MissingECUs.size(); i++ ) {
			EcuInfo = (ECU_Info) MissingECUs.get( i );
			if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNOREERR ) {
				errtyp = Ergebnis.FT_IGNORE;
			} else {
				errtyp = Ergebnis.FT_NIO;
			}
			if( isDE() )
				str = "SG in Gruppe " + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( EcuInfo.m_ECU_Adr ) + ") hat nicht geantwortet.";
			else
				str = "ECU in Group " + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( EcuInfo.m_ECU_Adr ) + ") did not answer.";
			TolerableErrors.add( new CError( ECUMISSING, ErrorTxt, str, errtyp ) );
		}
	}

	/** Ermittelt die Menge aller Sensoren die nicht geantwortet haben
	 * 
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten/Sensoren
	 * @return Menge der Sensoren die nicht geantwortet haben
	 */
	private Vector<Object> getMissingSensors( ECU_Infos EcuInfos ) {
		int i, j;
		ECU_Info EcuInfo;
		Subbus_Entry Entry;
		Vector<Object> erg = new Vector<Object>();

		for( i = 0; i < EcuInfos.nrOfECUs(); i++ ) {
			EcuInfo = EcuInfos.getECUInfo( i );
			for( j = 0; j < EcuInfo.m_Subbus_Entries.getAnzahl(); j++ ) {
				Entry = EcuInfo.m_Subbus_Entries.getEntry( j );
				//System.out.println( LOG_PREFIX + "### "+Integer.toHexString(Entry.m_Verbauort_Nr)+": "+Entry.m_visited+","+EcuInfo.m_Sensor_Doku_Flag+","+Entry.m_Sensor_Doku_Flag+"\r\n");
				if( !Entry.m_visited && EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_TRUE && Entry.m_Sensor_Doku_Flag != PL_FLAG_IGNORE && Entry.m_Sensor_Doku_Flag != PL_FLAG_FALSE ) {
					erg.add( Entry );
					erg.add( EcuInfo );
				}
			}
		}
		return erg;
	}

	/** Pr�ft, welche Sensoren nach einem funktionalen Job nicht gemeldet wurden. 
	 * 
	 * @param missingSensors Liste der Sensoren, die nicht gemeldet wurden
	 * @param ErrorTxt Fehlertext (Kontext des Aufrufers)
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 */
	private void checkForMissingSensors( Vector<Object> missingSensors, String ErrorTxt, Vector<CError> TolerableErrors ) {
		int i;
		ECU_Info EcuInfo;
		String str;
		Subbus_Entry Entry;

		for( i = 0; i < missingSensors.size(); i += 2 ) {
			Entry = (Subbus_Entry) missingSensors.get( i );
			EcuInfo = (ECU_Info) missingSensors.get( i + 1 );
			if( isDE() )
				str = "Sensor 0x" + Integer.toHexString( Entry.m_Verbauort_Nr ) + " in SG 0x" + Integer.toHexString( EcuInfo.m_ECU_Adr ) + " hat nicht geantwortet.";
			else
				str = "Sensor 0x" + Integer.toHexString( Entry.m_Verbauort_Nr ) + " in SG 0x" + Integer.toHexString( EcuInfo.m_ECU_Adr ) + " did not answer.";
			TolerableErrors.add( new CError( SENSORMISSING, ErrorTxt, str ) );
		}
	}

	/** Pr�ft, welche Sensoren nach einem funktionalen Job mehrfach gemeldet wurden (von verschiedenen ECUs.
	 * Doppelmeldungen von einem Ecu werden schon von der analysefunktion entdeckt und gemeldet).
	 * 
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten (und die SGs enthalten die erwarteten Sensoren)
	 * @param ErrorTxt Fehlertext (Kontext des Aufrufers)
	 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
	 */
	private void checkForDoubleSensors( ECU_Infos EcuInfos, String ErrorTxt, Vector<CError> TolerableErrors ) {
		Integer SensorID;
		HashMap<Integer, ECU_Info> SensorIDs;

		int i, j;
		ECU_Info EcuInfo;
		ECU_Info EcuInfo2;
		Subbus_Entry Entry;
		String str;

		SensorIDs = new HashMap<Integer, ECU_Info>();
		for( i = 0; i < EcuInfos.nrOfECUs(); i++ ) {
			EcuInfo = EcuInfos.getECUInfo( i );
			for( j = 0; j < EcuInfo.m_Subbus_Entries.getAnzahl(); j++ ) {
				Entry = EcuInfo.m_Subbus_Entries.getEntry( j );
				SensorID = new Integer( Entry.m_Verbauort_Nr );
				if( SensorIDs.containsKey( SensorID ) ) {
					EcuInfo2 = (ECU_Info) SensorIDs.get( SensorID );
					if( isDE() ) {
						str = "Es wurden mehrere Sensoren mit Verbauort 0x" + Integer.toHexString( Entry.m_Verbauort_Nr ) + " gemeldet:";
						str += "In SG " + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( EcuInfo.m_ECU_Adr ) + ") und ";
						str += EcuInfo2.m_ECU_GRP + " (0x" + Integer.toHexString( EcuInfo2.m_ECU_Adr ) + ")";
					} else {
						str = "Several sensors with ID 0x" + Integer.toHexString( Entry.m_Verbauort_Nr ) + " were reported:";
						str += "In ECU " + EcuInfo.m_ECU_GRP + " (0x" + Integer.toHexString( EcuInfo.m_ECU_Adr ) + ") and ";
						str += EcuInfo2.m_ECU_GRP + " (0x" + Integer.toHexString( EcuInfo2.m_ECU_Adr ) + ")";
					}
					TolerableErrors.add( new CError( SENSORANSWERED, ErrorTxt, str ) );
				}
				// LOP 2185
				if( Entry.m_visited ) {
					SensorIDs.put( SensorID, EcuInfo );
				}
			}
		}
	}

	/** Erzeugt den XML Datensatz und �bermittelt ihn an den Cascade-Server
	 * 
	 * @param EcuInfos Datenstruktur mit allen erwarteten Steuerger�ten (und die SGs enthalten die erwarteten Sensoren)
	 * @throws CError Fehlerobjekt
	 */
	private void dokuEcuInfo( ECU_Infos EcuInfos ) throws CError {
		int i, j;
		ECU_Info EcuInfo;
		SVK_Entry SVKEntry;
		CALIDCVN_Entry CALIDCVNEntry;
		Subbus_Entry SubbusEntry;
		StringBuffer xmlstr = new StringBuffer();
		String tmp;

		xmlstr.append( "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\r\n" );
		xmlstr.append( "<vehicleElectronicComponents version=\"01.08\" refSchema=\"vehicleElectronicComponentsV0108.xsd\">\r\n" );
		xmlstr.append( "\t<vehicle vinShort=\"" );
		xmlstr.append( getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7() );
		xmlstr.append( "\" integrationLevel=\"" );
		xmlstr.append( EcuInfos.m_IStufe );
		xmlstr.append( "\">\r\n" );
		xmlstr.append( "\t\t<electronicComponents>\r\n" );

		for( i = 0; i < EcuInfos.nrOfECUs(); i++ ) {
			EcuInfo = EcuInfos.getECUInfo( i );
			if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNORE )
				continue;
			if( EcuInfo.m_Doku_Flag == PL_FLAG_FALSE )
				continue;

			xmlstr.append( "\t\t\t<electronicComponent>\r\n" );
			xmlstr.append( "\t\t\t\t<controlUnit diagnosticAddress=\"" );
			xmlstr.append( Integer.toString( EcuInfo.m_ECU_Adr ) );
			xmlstr.append( "\">\r\n" );
			if( EcuInfo.m_Netz == 1 ) {
				if( EcuInfo.m_SGBD_Index != null ) {
					xmlstr.append( "\t\t\t\t\t<sgbdIndex>" );
					xmlstr.append( EcuInfo.m_SGBD_Index );
					xmlstr.append( "</sgbdIndex>\r\n" );
				}

				// Ermittelter hwModification (HardwareAenderungs Index), nur ab UDS Erweiterung ab 10/2015
				if( EcuInfo.m_hw_modification_Index != null ) {
					xmlstr.append( "\t\t\t\t\t<hwModificationIndex>" );
					xmlstr.append( EcuInfo.m_hw_modification_Index );
					xmlstr.append( "</hwModificationIndex>\r\n" );
				}

				xmlstr.append( "\t\t\t\t\t<technicalUnits>\r\n" );
				if( EcuInfo.m_SVK_Entries != null ) {
					for( j = 0; j < EcuInfo.m_SVK_Entries.length; j++ ) {
						SVKEntry = EcuInfo.m_SVK_Entries[j];
						xmlstr.append( "\t\t\t\t\t\t<technicalUnit processClass=\"" );
						xmlstr.append( SVKEntry.m_Prozessklasse );
						xmlstr.append( "\" ident=\"" );
						xmlstr.append( SVKEntry.m_SGBM_ID );
						xmlstr.append( "\" version=\"" );
						xmlstr.append( SVKEntry.m_Version );
						xmlstr.append( "\"/>\r\n" );
					}
				}
				xmlstr.append( "\t\t\t\t\t</technicalUnits>\r\n" );

				if( EcuInfo.m_CALIDCVN_Entries != null ) {
					if( EcuInfo.m_CALIDCVN_Entries.length > 0 ) {
						xmlstr.append( "\t\t\t\t\t<carbValues>\r\n" );
						for( j = 0; j < EcuInfo.m_CALIDCVN_Entries.length; j++ ) {
							CALIDCVNEntry = EcuInfo.m_CALIDCVN_Entries[j];
							if( CALIDCVNEntry.m_calid != null && CALIDCVNEntry.m_cvn != null ) {
								xmlstr.append( "\t\t\t\t\t\t<carbValue seqNo=\"" );
								xmlstr.append( Integer.toString( CALIDCVNEntry.m_position ) );
								xmlstr.append( "\" calid=\"" );
								xmlstr.append( CALIDCVNEntry.m_calid );
								xmlstr.append( "\" cvn=\"" );
								xmlstr.append( CALIDCVNEntry.m_cvn );
								xmlstr.append( "\"/>\r\n" );
							}
						}
						xmlstr.append( "\t\t\t\t\t</carbValues>\r\n" );
					}
				}
			}
			if( EcuInfo.m_Netz == 2 ) {
				xmlstr.append( "\t\t\t\t\t<varIndex>" );
				xmlstr.append( Integer.toString( EcuInfo.m_var_Index ) );
				xmlstr.append( "</varIndex>\r\n" );

				xmlstr.append( "\t\t\t\t\t<diagnosticIndex>" );
				xmlstr.append( Integer.toString( EcuInfo.m_diag_Index ) );
				xmlstr.append( "</diagnosticIndex>\r\n" );

				xmlstr.append( "\t\t\t\t\t<codingIndex>" );
				xmlstr.append( Integer.toString( EcuInfo.m_coding_Index ) );
				xmlstr.append( "</codingIndex>\r\n" );

				if( EcuInfo.m_c_aei_Index != null ) {
					xmlstr.append( "\t\t\t\t\t<changeCodingIndex>" );
					xmlstr.append( EcuInfo.m_c_aei_Index );
					xmlstr.append( "</changeCodingIndex>\r\n" );
				}
			}

			xmlstr.append( "\t\t\t\t</controlUnit>\r\n" );
			if( EcuInfo.m_Netz == 2 ) {
				if( EcuInfo.m_part_No != null ) {
					xmlstr.append( "\t\t\t\t<partNo>" );
					xmlstr.append( EcuInfo.m_part_No );
					xmlstr.append( "</partNo>\r\n" );
				}
			}
			xmlstr.append( "\t\t\t\t<serialNo>" );
			if( EcuInfo.m_Seriennummer != null ) {
				xmlstr.append( EcuInfo.m_Seriennummer );
			}
			xmlstr.append( "</serialNo>\r\n" );

			// Ermittelter ecuUID-Nummer (eindeutige SG-Seriennummer), erste SG liefern dies ab SP2018
			if( EcuInfo.m_ecuUID != null ) {
				xmlstr.append( "\t\t\t\t<ecuUID>" );
				xmlstr.append( EcuInfo.m_ecuUID );
				xmlstr.append( "</ecuUID>\r\n" );
			}

			if( EcuInfo.m_Lieferantennummer != null ) {
				xmlstr.append( "\t\t\t\t<manufacturerCode>" );
				xmlstr.append( EcuInfo.m_Lieferantennummer );
				xmlstr.append( "</manufacturerCode>\r\n" );
			}

			if( EcuInfo.m_Herstelldatum != null ) {
				xmlstr.append( "\t\t\t\t<manufacturingDate>" );
				xmlstr.append( EcuInfo.m_Herstelldatum );
				xmlstr.append( "</manufacturingDate>\r\n" );
			}

			if( EcuInfo.m_Netz == 2 ) {
				if( EcuInfo.m_assembly_No != null ) {
					xmlstr.append( "\t\t\t\t<assemblyPartNo>" );
					xmlstr.append( EcuInfo.m_assembly_No );
					xmlstr.append( "</assemblyPartNo>\r\n" );
				}

				if( EcuInfo.m_software_oper != null || EcuInfo.m_software_func != null || EcuInfo.m_software_resv != null || EcuInfo.m_software_comm != null ) {
					xmlstr.append( "\t\t\t\t<software>\r\n" );
					if( EcuInfo.m_software_oper != null ) {
						xmlstr.append( "\t\t\t\t\t<softwareVersion type=\"oper\">" );
						xmlstr.append( EcuInfo.m_software_oper );
						xmlstr.append( "</softwareVersion>\r\n" );
					}
					if( EcuInfo.m_software_func != null ) {
						xmlstr.append( "\t\t\t\t\t<softwareVersion type=\"func\">" );
						xmlstr.append( EcuInfo.m_software_func );
						xmlstr.append( "</softwareVersion>\r\n" );
					}
					if( EcuInfo.m_software_resv != null ) {
						xmlstr.append( "\t\t\t\t\t<softwareVersion type=\"resv\">" );
						xmlstr.append( EcuInfo.m_software_resv );
						xmlstr.append( "</softwareVersion>\r\n" );
					}
					if( EcuInfo.m_software_comm != null ) {
						xmlstr.append( "\t\t\t\t\t<softwareVersion type=\"comm\">" );
						xmlstr.append( EcuInfo.m_software_comm );
						xmlstr.append( "</softwareVersion>\r\n" );
					}
					xmlstr.append( "\t\t\t\t</software>\r\n" );
				}

				if( EcuInfo.m_hardware_version != null ) {
					xmlstr.append( "\t\t\t\t<hardwareVersion>" );
					xmlstr.append( EcuInfo.m_hardware_version );
					xmlstr.append( "</hardwareVersion>\r\n" );
				}
			}
			xmlstr.append( "\t\t\t</electronicComponent>\r\n" );
		}

		for( i = 0; i < EcuInfos.nrOfECUs(); i++ ) {
			EcuInfo = EcuInfos.getECUInfo( i );
			if( EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_IGNORE )
				continue;
			if( EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_FALSE )
				continue;

			if( EcuInfo.m_Subbus_Entries != null ) {
				for( j = 0; j < EcuInfo.m_Subbus_Entries.getAnzahl(); j++ ) {
					SubbusEntry = (Subbus_Entry) EcuInfo.m_Subbus_Entries.getEntry( j );
					if( SubbusEntry.m_Sensor_Doku_Flag == PL_FLAG_IGNORE )
						continue;
					if( SubbusEntry.m_Sensor_Doku_Flag == PL_FLAG_FALSE )
						continue;

					xmlstr.append( "\t\t\t<electronicComponent>\r\n" );
					xmlstr.append( "\t\t\t\t<sensor sensorID=\"" );
					tmp = Integer.toHexString( SubbusEntry.m_Verbauort_Nr ).toUpperCase();
					while( tmp.length() < 4 )
						tmp = "0" + tmp;
					xmlstr.append( tmp );
					xmlstr.append( "\"/>\r\n" );
					xmlstr.append( "\t\t\t\t<serialNo>" );
					if( SubbusEntry.m_Part_Nr != null ) {
						xmlstr.append( SubbusEntry.m_Part_Nr );
					}
					xmlstr.append( "</serialNo>\r\n" );
					if( SubbusEntry.m_BMW_Nr != null ) {
						xmlstr.append( "\t\t\t\t<assemblyPartNo>" );
						xmlstr.append( SubbusEntry.m_BMW_Nr );
						xmlstr.append( "</assemblyPartNo>\r\n" );
					}

					xmlstr.append( "\t\t\t</electronicComponent>\r\n" );
				}
			}
		}

		if( EcuInfos.m_IBSSensor != null ) {
			if( EcuInfos.m_IBSSensor.m_Sensor_Doku_Flag == PL_FLAG_TRUE ) {
				xmlstr.append( "\t\t\t<electronicComponent>\r\n" );
				xmlstr.append( "\t\t\t\t<sensor sensorID=\"" );
				tmp = Integer.toHexString( EcuInfos.m_IBSSensor.m_SensorID ).toUpperCase();
				while( tmp.length() < 4 )
					tmp = "0" + tmp;
				xmlstr.append( tmp );
				xmlstr.append( "\">\r\n" );
				if( EcuInfos.m_IBSSensor.m_Charging_Balance_SGBD != null ) {
					xmlstr.append( "\t\t\t\t\t<chargingBalance>" );
					xmlstr.append( Integer.toString( EcuInfos.m_IBSSensor.m_charging_balance ) + ".000" );
					xmlstr.append( "</chargingBalance>\r\n" );
				}
				xmlstr.append( "\t\t\t\t</sensor>\r\n" );
				xmlstr.append( "\t\t\t\t<serialNo>" );
				xmlstr.append( EcuInfos.m_IBSSensor.m_SerialNo );
				xmlstr.append( "</serialNo>\r\n" );
				xmlstr.append( "\t\t\t\t<assemblyPartNo>" );
				if( EcuInfos.m_IBSSensor.m_assemblyNo != null ) {
					xmlstr.append( EcuInfos.m_IBSSensor.m_assemblyNo );
				}
				xmlstr.append( "</assemblyPartNo>\r\n" );
				if( EcuInfos.m_IBSSensor.m_SoftwareVersion != null ) {
					xmlstr.append( "\t\t\t\t<software>\r\n" );
					xmlstr.append( "\t\t\t\t\t<softwareVersion type=\"func\">" );
					xmlstr.append( EcuInfos.m_IBSSensor.m_SoftwareVersion );
					xmlstr.append( "</softwareVersion>\r\n" );
					xmlstr.append( "\t\t\t\t</software>\r\n" );
				}
				xmlstr.append( "\t\t\t</electronicComponent>\r\n" );
			}
		}

		xmlstr.append( "\t\t</electronicComponents>\r\n" );
		xmlstr.append( "\t</vehicle>\r\n" );
		xmlstr.append( "</vehicleElectronicComponents>\r\n" );

		if( m_debug_level >= 10 ) {
			System.out.println( LOG_PREFIX + "XML file: \n" + xmlstr );
		}

		try {
			DomPruefstand.writeDomData( getPr�fling().getAuftrag().getFahrgestellnummer7(), "I", xmlstr.toString().getBytes() );
		} catch( Exception e ) {
			String str;
			if( isDE() )
				str = "Datentranfer zum Cascade server fehlgeschlagen";
			else
				str = "Transfer of dataset to Cascade server failed";
			throw new CError( TRANSFERFAILED, str, e.getMessage() );
		}
	}

	/** Ediabsjob ohne Requestparamer ausf�hren und Ergebnis STATUS=OKAY pr�fen
	 * 
	 * @param ept Ediabas
	 * @param SGBD Ediabas SGBD
	 * @param JobName Name des EdiabasJobs
	 * @throws CError Fehlerobjekt
	 */
	private void executeJob( EdiabasProxyThread ept, String SGBD, String JobName ) throws CError {
		executeJob( ept, SGBD, JobName, "", null );
	}

	/** Ediabasjob ausf�hren und Ergebnis STATUS=OKAY pr�fen
	 * 
	 * @param ept Ediabas
	 * @param SGBD Ediabas SGBD
	 * @param JobName Name des EdiabasJobs
	 * @param Args RequestParameter
	 * @param Gruppe Ecu Gruppe
	 * @return Status des Ediabas Jobs
	 * @throws CError Fehlerobjekt
	 */
	private String executeJob( EdiabasProxyThread ept, String SGBD, String JobName, String Args, String Gruppe ) throws CError {
		String Status = "null";
		String ERRORTXT = isDE() ? "Fehler bei " + SGBD + "/" + JobName + "/executeDiagJob (" + Args + ")" : "Error during " + JobName + "/executeJob (" + Args + ")";

		if( Gruppe != null ) {
			ERRORTXT += isDE() ? " (SG Gruppe: " + Gruppe + ")" : " (ECU Group: " + Gruppe + ")";
		}

		try {
			Status = ept.executeDiagJob( SGBD, JobName, Args, "" );
		} catch( ApiCallFailedException e ) {
			throw new CError( EDIABASERROR, ERRORTXT, ept.apiErrorText() );
		} catch( EdiabasResultNotFoundException e ) {
			throw new CError( EDIABASERROR, ERRORTXT, "EdiabasResultNotFoundException" );
		}

		if( !Status.equalsIgnoreCase( "OKAY" ) )
			throw new CError( EDIABASERROR, ERRORTXT, "STATUS=" + Status );

		return Status;
	}

	/** Funktionalen Ediabasjob ausf�hren
	 * 
	 * @param ept Ediabas
	 * @param SGBD SGBD f�r den funktionalen Aufruf (typischerweise F01 oder F01BN2K)
	 * @param JobName Name des Jobs
	 * @return Anzahl der Antwortsets
	 * @throws CError Fehlerobjekt
	 */
	private int executeFunctionalJob( EdiabasProxyThread ept, String SGBD, String JobName ) throws CError {
		final String ERRORTXT = isDE() ? "Fehler bei " + SGBD + "/" + JobName + "/executeDiagJob" : "Error during " + SGBD + "/" + JobName + "/executeDiagJob";
		int Sets;

		if( SGBD.equalsIgnoreCase( m_SGBD_BN2K ) && (SGBD.equalsIgnoreCase( "0" ) || SGBD.equalsIgnoreCase( "NONE" )) )
			return 1; // Wenn als BN2K 0 oder NONE angeben ist: Aufruf gar nicht erst probieren.

		try {
			executeJob( ept, SGBD, JobName );
		} catch( CError err ) {
			Ergebnis erg;
			erg = err.getEntry();
			if( erg != null ) {
				String txt = erg.getHinweisText();
				if( txt != null ) {
					if( txt.indexOf( "IFH-0009" ) >= 0 ) {
						if( m_debug_level >= 1 ) {
							System.out.println( LOG_PREFIX + "No Ecu answered for " + SGBD + "/" + JobName + "/executeDiagJob" );
						}

						// Bei funktionalen Jobs kein Fehler wenn gar kein Steuerger�t antwortet
						return 1;
					}
				}
			}
			throw err;
		}

		try {
			Sets = ept.apiResultSets();
		} catch( ApiCallFailedException e ) {
			throw new CError( EDIABASERROR, ERRORTXT, ept.apiErrorText() );
		}

		if( Sets == 0 )
			throw new CError( EDIABASERROR, ERRORTXT, "NrOfSets=" + Sets );
		return Sets;
	}

	/** Klasse, welche die Liste aller erwarteten Steuerger�te enth�lt,
	 * sowie steuerger�te�begreifende Informationen (I-Stufe)
	 * 
	 */
	private class ECU_Infos {
		/** Vom Fahrzeug ermittelte I-Stufe */
		public String m_IStufe;
		/** Liste der erwarteten Steuerger�te */
		private Vector<ECU_Info> m_InfoList = new Vector<ECU_Info>();
		/** Map von SG-Adresse auf Index in m_InfoList */
		private HashMap<Integer, Integer> m_InfoAdressMap = new HashMap<Integer, Integer>();
		/** Map von SG Gruppe (GRP) auf Index in m_InfoList */
		private HashMap<String, Integer> m_InfoGroupMap = new HashMap<String, Integer>();
		/** Batteriesensor */
		private IBS_Sensor m_IBSSensor = null;
		/** Liste der Ecus, die vorhanden sein k�nnen, aber keinen Pr�fling haben */
		public Set<Integer> m_EcusToIgnore = new HashSet<Integer>();
		/** ECU_Info Objekt des HC_2 SG, falls vorhanden od. null */
		//TODO: Kann 2020 wieder entfallen
		public ECU_Info m_HC2_ECU_Info;

		/** F�gt ein weiteres erwartetes Steuerger�t hinzu.
		 * Existiert das SG schon, so m�ssen die Doku-Flags identisch und FALSE oder IGNORE sein,
		 * oder einmal IGNORE und einmal TRUE sein.
		 * 
		 * @param EcuInfo Info �ber erwartetes Steuerger�t
		 * @throws CError Fehlerobjekt
		 */
		public void add( ECU_Info EcuInfo ) throws CError {
			Integer val;
			ECU_Info OtherEcu;
			String str;

			// Uebergangsmaessig die Existenz des HC2_G11 erkennen und das zugehoerige ECU_INFO Objekt sofern vorhanden merken
			// TODO: Kann 2020 wieder entfallen
			if( EcuInfo.m_ECU_SGBD.equalsIgnoreCase( "HC2_G11" ) ) {
				m_HC2_ECU_Info = EcuInfo;
			}

			val = (Integer) m_InfoGroupMap.get( EcuInfo.m_ECU_GRP );

			if( val == null ) {
				m_InfoList.add( EcuInfo );
				val = new Integer( m_InfoList.size() - 1 );
				m_InfoGroupMap.put( EcuInfo.m_ECU_GRP, val );
				return;
			}

			OtherEcu = (ECU_Info) m_InfoList.get( val.intValue() );

			// Ecu und Sensor Flag sind jeweils gleich und jeweils FALSE oder IGNORE
			if( EcuInfo.m_Doku_Flag == PL_FLAG_FALSE && OtherEcu.m_Doku_Flag == PL_FLAG_FALSE && EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_FALSE && OtherEcu.m_Sensor_Doku_Flag == PL_FLAG_FALSE )
				return;

			if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNORE && OtherEcu.m_Doku_Flag == PL_FLAG_IGNORE && EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_FALSE && OtherEcu.m_Sensor_Doku_Flag == PL_FLAG_FALSE )
				return;

			if( EcuInfo.m_Doku_Flag == PL_FLAG_FALSE && OtherEcu.m_Doku_Flag == PL_FLAG_FALSE && EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_IGNORE && OtherEcu.m_Sensor_Doku_Flag == PL_FLAG_IGNORE )
				return;

			if( EcuInfo.m_Doku_Flag == PL_FLAG_IGNORE && OtherEcu.m_Doku_Flag == PL_FLAG_IGNORE && EcuInfo.m_Sensor_Doku_Flag == PL_FLAG_IGNORE && OtherEcu.m_Sensor_Doku_Flag == PL_FLAG_IGNORE )
				return;

			if( isDE() ) {
				str = "Zwei Pr�flinge f�r DokuChipcard:\r\n";
			} else {
				str = "Two Pr�flings for DokuChipcard:\r\n";
			}
			str += " " + OtherEcu.m_Pruefling + ": ";
			str += PL_DOMDOKU_ECU + "=" + flagIntToString( OtherEcu.m_Doku_Flag );
			str += ", ";
			str += PL_DOMDOKU_SENSOR + "=" + flagIntToString( OtherEcu.m_Sensor_Doku_Flag );
			str += "\r\n " + EcuInfo.m_Pruefling + ": ";
			str += PL_DOMDOKU_ECU + "=" + flagIntToString( EcuInfo.m_Doku_Flag );
			str += ", ";
			str += PL_DOMDOKU_SENSOR + "=" + flagIntToString( EcuInfo.m_Sensor_Doku_Flag );

			throw new CError( "buildECUInfos", str, "" );
		}

		/** F�gt den erwarteten ECUs und Sensoren den Batteriesensor (IBS) hinzu
		 * 
		 * @param DokuFlag TRUE IGNORE oder FALSE
		 * @param SGBD SGBD f�r Kommunikation mit dem IBS Sensor
		 * @param ChargingBalanceSGBD zur Ermittlung des Ladezustands (normalerweise =SGBD)
		 * @throws CError Fehler, falls SGBD leer
		 */
		public void addIBSSensor( int DokuFlag, String SGBD, String ChargingBalanceSGBD ) throws CError {
			m_IBSSensor = new IBS_Sensor( DokuFlag, SGBD, ChargingBalanceSGBD );
		}

		/** Ermittelt �ber die F01.prg die SG-Adresse aus der SG Gruppe
		 * 
		 * @param ept Ediabas
		 * @param TolerableErrors Fehler, die nicht zu einem sofortigen Abbruch f�hren (aber durchaus den Gesamtablauf NIO werden lassen k�nnen)
		 * @throws CError Fehler falls es einen Adressenkonflikt gibt
		 */
		public void determineECUAddresses( EdiabasProxyThread ept, Vector<CError> TolerableErrors ) throws CError {
			int i;
			ECU_Info ecuinfo;
			ECU_Info other_ecuinfo;
			int ECU_Adress;
			String SGBD;
			Integer val, pos;

			for( i = 0; i < m_InfoList.size(); i++ ) {
				ecuinfo = (ECU_Info) (m_InfoList.get( i ));

				try {
					SGBD = (ecuinfo.m_Netz == 1 ? m_SGBD_UDS : m_SGBD_BN2K);
					if( SGBD.equalsIgnoreCase( m_SGBD_BN2K ) && (m_SGBD_BN2K.equalsIgnoreCase( "0" ) || m_SGBD_BN2K.equalsIgnoreCase( "NONE" )) )
						throw new CError( SGBDMISSING, "Found BN2K Pr�fling " + ecuinfo.m_Pruefling + ", but no BN2K SGBD is specified", "" );
					executeJob( ept, SGBD, "grp2sgadr", ecuinfo.m_ECU_GRP, null );
				} catch( CError e ) {
					TolerableErrors.add( e );
					continue;
				}

				try {
					ECU_Adress = ept.apiResultInt( "SG_ADR", 1 );
				} catch( ApiCallFailedException e ) {
					String str;
					if( isDE() )
						str = "SG_ADR konnte nicht aus Gruppe \"" + ecuinfo.m_ECU_GRP + "\" bestimmt werden.";
					else
						str = "Failed to determine SG_ADR from group \"" + ecuinfo.m_ECU_GRP + "\".";
					TolerableErrors.add( new CError( EDIABASERROR, str, ept.apiErrorText() ) );
					continue;
				}

				ecuinfo.m_ECU_Adr = ECU_Adress;
				val = new Integer( ECU_Adress );

				pos = (Integer) m_InfoAdressMap.get( val );
				if( pos != null ) {
					String str;
					other_ecuinfo = (ECU_Info) (m_InfoList.get( pos.intValue() ));

					if( isDE() )
						str = "2 Pr�flinge f�r ein SG mit Adresse 0x" + Integer.toHexString( ECU_Adress ) + " gefunden: ";
					else
						str = "Found 2 Pr�flings for an ECU with Adress 0x" + Integer.toHexString( ECU_Adress ) + ": ";
					str += other_ecuinfo.m_Pruefling + " (" + other_ecuinfo.m_ECU_GRP + "), ";
					str += ecuinfo.m_Pruefling + " (" + ecuinfo.m_ECU_GRP + "), ";

					throw new CError( ADDRESSCONFLICT, str, "" );
				} else {
					m_InfoAdressMap.put( val, new Integer( i ) );
				}
			}
		}

		/** Sucht den Eintrag f�r ein SG mit einer bestimmten Adresse
		 * 
		 * @param ECU_Adress SG Adresse 
		 * @return gefundener Eintrag oder null
		 */
		public ECU_Info findByEcuAdress( int ECU_Adress ) {
			Integer val;

			Integer key = new Integer( ECU_Adress );

			val = (Integer) m_InfoAdressMap.get( key );
			if( val == null )
				return null;

			return (ECU_Info) (m_InfoList.get( val.intValue() ));
		}

		//		/** Sucht einen Eintrag f�r ein SG mit einer bestimmten GRP
		//		 * 
		//		 * @param Group GRP-Name
		//		 * @return gefundener Eintrag oder null
		//		 */
		//		public ECU_Info findByEcuGroup( String Group ) {
		//			Integer val;
		//
		//			val = (Integer) m_InfoGroupMap.get( Group );
		//			if( val == null )
		//				return null;
		//
		//			return (ECU_Info) (m_InfoList.get( val.intValue() ));
		//		}

		/** Liefert Anzahl der durch die PLs konfigurierten erwarteten ECUs
		 * 
		 * @return Anzahl der erwarteten ECUs
		 */
		public int nrOfECUs() {
			return m_InfoList.size();
		}

		/** Liefert ein erwartetes ECU 
		 * 
		 * @param index 0<=Index<nrOfECUs()
		 * @return Eintrag
		 */
		public ECU_Info getECUInfo( int index ) {
			return (ECU_Info) (m_InfoList.get( index ));
		}

		/** Alle ECU und Sensor Eintr�ge als "nicht besucht" markieren.
		 * Notwending, um nach Abarbeitung eines funktionalen Jobs zu wissen,
		 * welche ECUs und Sensoren erwartet wurden, aber nicht geantwortet haben.
		 */
		public void setAllNotVisited() {
			int Anzahl, i;

			Anzahl = nrOfECUs();
			for( i = 0; i < Anzahl; i++ ) {
				getECUInfo( i ).setNonVisited();
			}
		}

		/** Alle Daten zu den Ecus und Sensoren auf die Konsole ausgeben*/
		public void dumpEcuInfo() {
			int i;
			ECU_Info EcuInfo;

			System.out.println( LOG_PREFIX + "IStufe: " + m_IStufe );
			for( i = 0; i < nrOfECUs(); i++ ) {
				EcuInfo = getECUInfo( i );
				EcuInfo.dump();
			}
			if( m_IBSSensor != null )
				m_IBSSensor.dump();
		}
	}

	/** Eintrag f�r ein erwartetes Steuerger�t
	 * 
	 */
	private class ECU_Info {
		/** Konstruktor
		 * 
		 * @param Pruefling Name des Pr�flings
		 * @param Doku_Flag TRUE: Steuerger�t mu� vorhanden sein
		 *                  FALSE: Steuerger�t darf nicht vorhanden sein
		 *                  IGNORE: Steuerger�t wird nicht dokumentiert, egal ob es sich meldet oder nicht
		 * @param SGBD      SGBD f�r das Steuerger�t
		 * @param GRUPPE    GRP f�r das Steuerger�t
		 * @param Netz      1: UDS,  2: BN2K
		 * @param readSernum true: Seriennummer ermitteln und dokumentieren, false: Seriennummer nicht ermitteln und nicht dokumentieren
		 * @param readecuUIDnum true: ecuUID-Nummer ermitteln und dokumentieren, false: ecuUID-Nummer nicht ermitteln und nicht dokumentieren
		 * @param readHerstelldatum true: Herstelldatum ermitteln und dokumentieren, false: Herstelldatum nicht ermitteln und nicht dokumentieren (nur bei UDS relevant)
		 * @param readLieferantennummer true: Lieferantennummer ermitteln und dokumentieren, false: Lieferantennummer nicht ermitteln und nicht dokumentieren (nur bei UDS relevant)
		 * @param Sensor_Doku_Flag Hauptflag f�r Sensoren
		 *                         TRUE: Sensoren dokumentieren, erwartete sensoren sind explizit angegeben
		 *                         FALSE: Es d�rfen sich keine Sensoren melden
		 *                         AUTO: Alle Sensoren die sich melden, werden dokumentiert. Erwartete Sensoren m�ssen nicht explizit angegeben werden
		 * @param Sensor_Doku_Map Map Sensor ID -> DOKU_FLAG f�r diesen Sensor
		 */
		public ECU_Info( String Pruefling, int Doku_Flag, String SGBD, String GRUPPE, int Netz, boolean readSernum, boolean readecuUIDnum, boolean readHerstelldatum, boolean readLieferantennummer, int Sensor_Doku_Flag, Map<Integer, Integer> Sensor_Doku_Map ) {
			m_Pruefling = Pruefling;
			m_Doku_Flag = Doku_Flag;
			m_ECU_SGBD = SGBD;
			m_ECU_GRP = GRUPPE;
			m_readSeriennummer = readSernum;
			m_readECUUIDnummer = readecuUIDnum;
			m_readHerstelldatum = readHerstelldatum;
			m_readLieferantennummer = readLieferantennummer;

			m_visited = false;
			m_answered = false;
			m_Netz = Netz;
			m_Seriennummer = null;
			m_ecuUID = null;
			m_Herstelldatum = null;
			m_Lieferantennummer = null;
			m_SGBD_Index = null;
			m_hw_modification_Index = null;
			m_var_Index = 0;
			m_diag_Index = 0;
			m_coding_Index = 0;
			m_part_No = null;
			m_software_oper = null;
			m_software_func = null;
			m_software_comm = null;
			m_hardware_version = null;
			m_software_resv = null;
			m_assembly_No = null;
			m_c_aei_Index = null;

			m_SVK_Entries = null;
			m_CALIDCVN_Entries = null;

			m_Sensor_Doku_Flag = Sensor_Doku_Flag;
			m_Subbus_Entries = new Subbus_Entries( Sensor_Doku_Map );
		}

		/** Dieses SG und alle seine Sensoren als "nicht besucht" markieren
		 * Notwending, um nach Abarbeitung eines funktionalen Jobs zu wissen,
		 * welche ECUs und Sensoren erwartet wurden, aber nicht geantwortet haben.
		 */
		public void setNonVisited() {
			m_visited = false;
			m_answered = false;
			if( m_Subbus_Entries != null ) {
				m_Subbus_Entries.setNonVisited();
			}
		}

		/** Inhalt des Eintrags auf Konsole ausgeben (f�r Debugzwecke) */
		public void dump() {
			int i;

			System.out.println( LOG_PREFIX + "Pruefling:         " + m_Pruefling );
			System.out.println( LOG_PREFIX + "DokuFlag:          " + flagIntToString( m_Doku_Flag ) );
			System.out.println( LOG_PREFIX + "SerFlag:           " + m_readSeriennummer );
			System.out.println( LOG_PREFIX + "Netz:              " + m_Netz );
			System.out.println( LOG_PREFIX + "SGBD:              " + m_ECU_SGBD );
			System.out.println( LOG_PREFIX + "GRP:               " + m_ECU_GRP );
			System.out.println( LOG_PREFIX + "Adresse:           0x" + Integer.toHexString( m_ECU_Adr ) );
			System.out.println( LOG_PREFIX + "Seriennummer:      " + m_Seriennummer );
			System.out.println( LOG_PREFIX + "ecuUID-Nummer:     " + m_ecuUID );
			System.out.println( LOG_PREFIX + "Herstelldatum:     " + m_Herstelldatum );
			System.out.println( LOG_PREFIX + "Lieferantennummer: " + m_Lieferantennummer );
			if( m_Netz == 1 ) {
				System.out.println( LOG_PREFIX + "SGBD Index:        " + m_SGBD_Index );
				System.out.println( LOG_PREFIX + "HW Aend. Index:    " + (m_hw_modification_Index != null ? m_hw_modification_Index : "null") );
			} else {
				System.out.println( LOG_PREFIX + "Var Index:         " + m_var_Index );
				System.out.println( LOG_PREFIX + "Diag Index:        " + m_diag_Index );
				System.out.println( LOG_PREFIX + "Cod Index:         " + m_coding_Index );
				System.out.println( LOG_PREFIX + "PartNo:            " + m_part_No );
				System.out.println( LOG_PREFIX + "Soft. oper:        " + m_software_oper );
				System.out.println( LOG_PREFIX + "Soft. func:        " + m_software_func );
				System.out.println( LOG_PREFIX + "Soft. comm:        " + m_software_comm );
				System.out.println( LOG_PREFIX + "Soft. resv:        " + m_software_resv );
				System.out.println( LOG_PREFIX + "Hardw.:            " + m_hardware_version );
				System.out.println( LOG_PREFIX + "Assembly No:       " + m_assembly_No );
				System.out.println( LOG_PREFIX + "AEI Index:         " + m_c_aei_Index );
			}

			System.out.println( LOG_PREFIX + "SVK Entries:       " );
			if( m_SVK_Entries == null ) {
				System.out.println( LOG_PREFIX + "null" );
			} else {
				System.out.println( LOG_PREFIX );
				for( i = 0; i < m_SVK_Entries.length; i++ ) {
					m_SVK_Entries[i].dump();
				}
			}
			System.out.println( LOG_PREFIX + "CALIDCVN Entries:       " );
			if( m_CALIDCVN_Entries == null ) {
				System.out.println( LOG_PREFIX + "null" );
			} else {
				System.out.println( LOG_PREFIX );
				for( i = 0; i < m_CALIDCVN_Entries.length; i++ ) {
					m_CALIDCVN_Entries[i].dump();
				}
			}

			System.out.println( LOG_PREFIX + "SensorDokuFlag:    " + flagIntToString( m_Sensor_Doku_Flag ) );

			System.out.println( LOG_PREFIX + "Subbus Entries:    " );
			if( m_Subbus_Entries == null ) {
				System.out.println( LOG_PREFIX + "null" );
			} else {
				System.out.println( LOG_PREFIX );
				m_Subbus_Entries.dump();
			}
		}

		/** Name des Pr�flings */
		public String m_Pruefling;
		// Wird durch Pr�flingsattribute bef�llt
		/** Dokuflag: TRUE, FALSE, IGNORE */
		public int m_Doku_Flag;
		/** Name des SGDB f�r das SG */
		public String m_ECU_SGBD;
		/** Name der GRP f�r das SG */
		public String m_ECU_GRP;
		/** Flag, ob Seriennummer ermittelt werden soll */
		public boolean m_readSeriennummer;
		/** Flag, ob ecuUID-Nummer ermittelt werden soll */
		public boolean m_readECUUIDnummer;
		/** Flag, ob Herstelldatum ermittelt werden soll (bei L6 nicht funktional) */
		public boolean m_readHerstelldatum;
		/** Flag, ob Lieferantennummer ermittelt werden soll (bei L6 nicht funktional) */
		public boolean m_readLieferantennummer;
		/** Flag, ob Sensoren ermittelt werden sollen: TRUE, FALSE, IGNORE, AUTO */
		public int m_Sensor_Doku_Flag;
		/** Sensoren */
		public Subbus_Entries m_Subbus_Entries;
		/** 1: UDS, 2: BN2K */
		public int m_Netz;

		/** Flag, ob ECU bei Durchgang bef�llt wurde. */
		public boolean m_visited;

		/** Flag, ob geantwortet wurde */
		public boolean m_answered;

		/** SG Adresse, wird aus GRP �ber F01.prg bestimmt */
		public int m_ECU_Adr;

		// Wird aus SG ausgelesen
		/** Ermittelte Seriennummer */
		public String m_Seriennummer;
		/** Ermittelte eineindeutige Steuerger�t-Seriennummer 'ecuUID */
		public String m_ecuUID;
		/** Ermitteltes Herstelldatum */
		public String m_Herstelldatum;
		/** Ermittelte Lieferantennummer */
		public String m_Lieferantennummer;
		/** Ermittelter SGBD Index, nur L6 */
		public String m_SGBD_Index;
		/** Ermittelter hwModification (HardwareAenderungs Index), nur ab UDS und ab Erweiterung 10/2015 */
		public String m_hw_modification_Index;

		/** SVK Eintr�ge, nur >=L6 */
		public SVK_Entry[] m_SVK_Entries;
		/** CALID CVN Eintr�ge, nur >=L6 */
		public CALIDCVN_Entry[] m_CALIDCVN_Entries;

		// Nur BN2K:
		/** Varianten Index */
		public int m_var_Index; // <varIndex>16696</varIndex> 
		/** Diagnose Index */
		public int m_diag_Index; // <diagnosticIndex>1200</diagnosticIndex>
		/** Codier Index */
		public int m_coding_Index; // <codingIndex>3</codingIndex>
		/** BMW_NR */
		public String m_part_No; // <partNo>6952615</partNo>
		/** Softwareversion */
		public String m_software_oper; // <softwareVersion type="oper">2.3.1</softwareVersion>
		/** Softwareversion */
		public String m_software_func; // <softwareVersion type="func">6.5.5</softwareVersion>
		/** Softwareversion */
		public String m_software_comm; //<softwareVersion type="comm">0.6.14</softwareVersion>
		/** Softwareversion */
		public String m_software_resv; //  <softwareVersion type="resv">5.8.0</softwareVersion> 
		/** Hardwareversion */
		public String m_hardware_version; //<hardwareVersion>C2</hardwareVersion>
		/** ZB Nummer */
		public String m_assembly_No; //<assemblyPartNo>6952615</assemblyPartNo>
		/** �nderungscodierindex */
		public String m_c_aei_Index;

	}

	/** Ein SVK Eintrag, nur ab L6 */
	private class SVK_Entry {
		/** Ermittelte Prozessklasse */
		public String m_Prozessklasse = null;
		/** Ermittelte SGBM ID */
		public String m_SGBM_ID = null;
		/** Ermittelte Version */
		public String m_Version = null;

		/** Ausgabe in Konsole, zu Debugzwecken */
		public void dump() {
			System.out.println( LOG_PREFIX + "  Prozessklasse: " + m_Prozessklasse );
			System.out.println( LOG_PREFIX + "  SGBM ID:       " + m_SGBM_ID );
			System.out.println( LOG_PREFIX + "  Version:       " + m_Version );
		}
	}

	/** Ein CALIDCVN (CarbUnit) Eintrag, nur ab L6 */
	private class CALIDCVN_Entry {
		/** Position */
		public int m_position;
		/** Ermittelte CalID*/
		public String m_calid = null;
		/** Ermittelte CVN */
		public String m_cvn = null;

		/** Ausgabe in Konsole, zu Debugzwecken */
		public void dump() {
			System.out.println( LOG_PREFIX + "  Position: " + m_position );
			System.out.println( LOG_PREFIX + "  CalID: " + m_calid );
			System.out.println( LOG_PREFIX + "  CVN:   " + m_cvn );
		}
	}

	/** Sensoreintr�ge */
	private class Subbus_Entries {
		/** Liste der Eintr�ge */
		private Vector<Subbus_Entry> m_Entries;
		/** Map Verbauort -> Index in m_Entries*/
		private Map<Integer, Integer> m_EntriesVerbauortMap;

		/** Standardkonstruktor
		 * 
		 * @param SensorMap Map Sensor ID -> DOKU_FLAG f�r diesen Sensor
		 */
		public Subbus_Entries( Map<Integer, Integer> SensorMap ) {
			Iterator<Integer> it;
			Integer key;
			Integer flag;

			m_Entries = new Vector<Subbus_Entry>();
			m_EntriesVerbauortMap = new HashMap<Integer, Integer>();

			it = SensorMap.keySet().iterator();
			while( it.hasNext() ) {
				key = (Integer) it.next();
				flag = (Integer) SensorMap.get( key );
				addSubbus( key.intValue(), flag.intValue() );
			}
		}

		/** Erwarteten Sensor hinzuf�gen
		 * 
		 * @param VerbauortID Verbauort
		 * @param DokuFlag TRUE, FALSE, IGNORE, SER, SNR
		 */
		public void addSubbus( int VerbauortID, int DokuFlag ) {
			Subbus_Entry entry;

			entry = new Subbus_Entry( VerbauortID, DokuFlag );
			addSubbus( entry );
		}

		/** Erwarteten Sensor hinzuf�gen
		 * 
		 * @param entry Sensordaten
		 */
		public void addSubbus( Subbus_Entry entry ) {
			Integer key;
			Integer index;

			key = new Integer( entry.m_Verbauort_Nr );
			index = (Integer) m_EntriesVerbauortMap.get( key );
			if( index == null ) {
				m_Entries.add( entry );
				m_EntriesVerbauortMap.put( key, new Integer( m_Entries.size() - 1 ) );
			} else {
				m_Entries.set( index.intValue(), entry );
			}
		}

		/** Sensoreintrag �ber Verbauort ermitteln
		 * 
		 * @param VerbauortNr Verbauort (=SensorID)
		 * @return SensorEintrag
		 */
		public Subbus_Entry findEntry( int VerbauortNr ) {
			Integer index;

			index = (Integer) m_EntriesVerbauortMap.get( new Integer( VerbauortNr ) );
			if( index == null )
				return null;
			return (Subbus_Entry) m_Entries.get( index.intValue() );
		}

		/** Ermittelt Anzahl der eingtragenen Sensoren
		 * 
		 * @return Anzahl der eingtragenen Sensoren
		 */
		public int getAnzahl() {
			return m_Entries.size();
		}

		/** Liefert einen Sensoreintrag �ber seinen Index
		 * 
		 * @param Index Index des Eintrags 0<Index<getAnzahl()-1
		 * @return Sensoreintrag
		 */
		public Subbus_Entry getEntry( int Index ) {
			return (Subbus_Entry) m_Entries.get( Index );
		}

		/** Alle Sensoreintr�ge als "nicht besucht" markieren
		 * 
		 * Wichtig um nach funktionalem aufruf zu wissen, welche Sensoren sich nicht gemeldet haben.
		 */
		public void setNonVisited() {
			int i;
			Subbus_Entry entry;

			for( i = 0; i < m_Entries.size(); i++ ) {
				entry = (Subbus_Entry) m_Entries.get( i );
				entry.m_visited = false;
			}
		}

		/** Ausgabe in Konsole, zu Debugzwecken */
		public void dump() {
			int i;
			Subbus_Entry entry;

			for( i = 0; i < m_Entries.size(); i++ ) {
				entry = (Subbus_Entry) m_Entries.get( i );
				entry.dump();
			}
		}
	}

	/** Ein Sensoreintag */
	private class Subbus_Entry {
		/** Konstruktor
		 * 
		 * @param VerbauortID Verbauort (=SensorID)
		 * @param DokuFlag DokuFlag: TRUE, FALSE, IGNORE, SER, SNR 
		 */
		public Subbus_Entry( int VerbauortID, int DokuFlag ) {
			m_Verbauort_Nr = VerbauortID;
			m_Sensor_Doku_Flag = DokuFlag;
			m_BMW_Nr = null;
			m_Part_Nr = null;
			m_visited = false;
		}

		/** Verbauort = Sensor ID */
		public int m_Verbauort_Nr;
		/** DokuFlag: TRUE, FALSE, IGNORE, SER, SNR */
		public int m_Sensor_Doku_Flag;

		/** BMWNr = assemblyPartNo*/
		public String m_BMW_Nr;
		/** PartNr = serialNo*/
		public String m_Part_Nr;
		/** Flag: Sensor wurde beim Aufruf gemeldet */
		public boolean m_visited;

		/** Ausgabe in Konsole, zu Debugzwecken */
		public void dump() {
			System.out.println( LOG_PREFIX + "  Verbauort Nr: " + Integer.toHexString( m_Verbauort_Nr ) );
			System.out.println( LOG_PREFIX + "  vorhanden:    " + m_visited );
			System.out.println( LOG_PREFIX + "  DokuFlag:     " + flagIntToString( m_Sensor_Doku_Flag ) );
			System.out.println( LOG_PREFIX + "  BWM Nr:       " + m_BMW_Nr );
			System.out.println( LOG_PREFIX + "  Part Nr:      " + m_Part_Nr );
		}
	}

	/** Spezieller Sensor: Batteriesensor IBS */
	private class IBS_Sensor {
		/** Verbauort = Sensor ID */
		public int m_SensorID;
		/** DokuFlag: TRUE, FALSE, IGNORE */
		public int m_Sensor_Doku_Flag;
		/** SGBD um Batteriesensor abzufragen (typischerweise eine MotorSGBD) */
		public String m_SGBD;
		/** SGBD um Ladezustand abzugfragen (normalerweise = m_SGBD) */
		public String m_Charging_Balance_SGBD;

		/** Seriennummer */
		public String m_SerialNo;
		/** BMWNr = assemblyPartNo */
		public String m_assemblyNo;
		/** chargingBalance */
		public int m_charging_balance;
		/** Software Version */
		public String m_SoftwareVersion;

		/** Konstruktor 
		 * 
		 * @param DokuFlag DokuFlag: TRUE, FALSE, IGNORE 
		 * @param SGBD SGBD um Batteriesensor abzufragen (typischerweise eine MotorSGBD)
		 * @param Charging_Balance_SGBD um Ladezustand abzugfragen (normalerweise = m_SGBD) 
		 * @throws CError Fehlerobjekt
		 */
		IBS_Sensor( int DokuFlag, String SGBD, String Charging_Balance_SGBD ) throws CError {
			m_SensorID = 0x3C00;
			m_Sensor_Doku_Flag = DokuFlag;

			if( SGBD == null ) {
				SGBD = "";
			}

			if( Charging_Balance_SGBD == null ) {
				Charging_Balance_SGBD = "";
			}

			if( DokuFlag == PL_FLAG_TRUE ) {
				if( SGBD.length() == 0 ) {
					String str;
					if( isDE() )
						str = "Keine SGBD angegeben f�r Batteriesensor";
					else
						str = "No SGBD specified for battery sensor";
					throw new CError( SGBDMISSING, str, "" );
				} else if( SGBD.startsWith( IBS_SGBD_ERROR ) ) {
					String str;
					if( isDE() )
						str = "SGBD f�r Batteriesensor konnte nicht ermittelt werden";
					else
						str = "SGBD for battery sensor could not be determined";
					throw new CError( IBS_SGBD_ERROR, str, SGBD );
				}

				if( Charging_Balance_SGBD.length() == 0 ) {
					String str;
					if( isDE() )
						str = "Keine SGBD angegeben f�r Charging Balance";
					else
						str = "No SGBD specified for charging balance";
					throw new CError( SGBDMISSING, str, "" );
				} else if( Charging_Balance_SGBD.startsWith( IBS_SGBD_ERROR ) ) {
					String str;
					if( isDE() )
						str = "SGBD f�r Charging Balance konnte nicht ermittelt werden";
					else
						str = "SGBD for charging balance could not be determined";
					throw new CError( IBS_SGBD_ERROR, str, SGBD );
				}
			} else {
				if( SGBD.startsWith( IBS_SGBD_ERROR ) ) {
					SGBD = "";
				}
				if( Charging_Balance_SGBD.startsWith( IBS_SGBD_ERROR ) ) {
					Charging_Balance_SGBD = "";
				}
			}

			m_SGBD = SGBD;
			m_Charging_Balance_SGBD = Charging_Balance_SGBD;

			m_SerialNo = "0";
			m_assemblyNo = null;
			m_charging_balance = 0;
			m_SoftwareVersion = "";
		}

		/** Ausgabe in Konsole, zu Debugzwecken */
		public void dump() {
			System.out.println( LOG_PREFIX + "Batteriesensor: " );
			System.out.println( LOG_PREFIX + "  DokuFlag:     " + flagIntToString( m_Sensor_Doku_Flag ) );
			System.out.println( LOG_PREFIX + "  SGBD:         " + m_SGBD );
			System.out.println( LOG_PREFIX + "  ChargeSGBD:   " + m_Charging_Balance_SGBD );
			System.out.println( LOG_PREFIX + "  Sensor ID:    " + Integer.toHexString( m_SensorID ) );
			System.out.println( LOG_PREFIX + "  Serial No:    " + m_SerialNo );
			System.out.println( LOG_PREFIX + "  Assembly No:  " + m_assemblyNo );
			System.out.println( LOG_PREFIX + "  Software ver: " + m_SoftwareVersion );
			System.out.println( LOG_PREFIX + "  Charging Bal: " + Integer.toString( m_charging_balance ) );
		}
	}

	/** Ermittelt Srache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean isDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default is english
		}
	}

	/** Fehlerobjekt  */
	private class CError extends Exception {
		/** */
		private static final long serialVersionUID = -5654634010303049207L;
		/** Cascadeergebnis */
		Ergebnis m_entry;

		/** Konstruktor (Fehlertyp ist FT_NIO)
		 * 
		 * @param ID FehlerID
		 * @param Fehlertext Fehlertext
		 * @param Hinweistext Hinweistext
		 */
		public CError( String ID, String Fehlertext, String Hinweistext ) {
			m_entry = new Ergebnis( ID, "", "", "", "", "", "", "", "", "0", "", "", "", Fehlertext, Hinweistext, Ergebnis.FT_NIO );
		}

		/** Konstruktor 
		 * 
		 * @param ID FehlerID
		 * @param Fehlertext Fehlertext
		 * @param Hinweistext Hinweistext
		 * @param ErrorType FehlerTyp (z.B. Ergebnis.FT_NIO)
		 */
		public CError( String ID, String Fehlertext, String Hinweistext, String ErrorType ) {
			m_entry = new Ergebnis( ID, "", "", "", "", "", "", "", "", "0", "", "", "", Fehlertext, Hinweistext, ErrorType );

		}

		/** Liefert das CascadeErgebnis 
		 * 
		 * @return Cascadeergebnis
		 */
		public Ergebnis getEntry() {
			return m_entry;
		}
	}
}

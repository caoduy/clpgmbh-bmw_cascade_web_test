/*
 * DiagLeseFSUDS_x_y_F_Pruefprozedur.java
 *
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.ediabas.*;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;

/**
 * Implementierung der Pr�fprozedur, die per Diagnose den Fehlerspeicher ausliest und
 * dabei Fehler auf Basis des Fehlerortes ignoriert bzw. weitere Analysen durch Ausf�hrung der
 * Analyse-PPs vornimmt. Sollten nur zu ignorierende Fehler vorliegen, wird mit der �bergebenen
 * Clear-PP das L�schen vorgenommen.
 * @author Winkler, Crichton, M�ller, Buboltz, Schoenert BMW AG, Rettig GEFASOFT GmbH
 * @version 1_0_F 14.07.06 TB Uebernahme von DiagLeseFS + neuer EDIABAS-Weg + kleine Korrekturen <br>
 *          2_0_F 07.09.06 TB BugFix ECOS <BR>
 *          3_0_F 08.09.2006 TB Verschoenerung ECOS <BR>
 *          4_0_T 17.02.2009 PR Zu ignorierende Fehler werden aus dem Argument FORT der Analyse-Pr�fprozedur dynamisch entfernt (danach wird das urspr�ngliche FORT-Argument wiederhergestellt) <BR>  
 *          5_0_F 30.04.2009 PR F-Version <BR>
 *          6_0_T 04.04.2012 RG Abfrage und Ausgabe der Results: F_VORHANDEN_NR und F_VORHANDEN_TEXT in den Antworttext bzw. Anweisungstext  <BR>
 *          7_0_F 04.04.2012 RG F-Version mit vorherigen �nderungen <BR>
 *          8_0_T 31.05.2012 CW BugFix wenn nur ein Fehlercode in der Analyse Pr�fprozedur angegeben wird
 *          9_0_F 01.08.2012 CW BugFix wenn nur ein Fehlercode in der Analyse Pr�fprozedur angegeben wird
 *         10_0_T 06.08.2012 FS Erweiterung um Optionale Argumente "EREIGNIS_IGNORE_TRUE" und "EREIGNIS_IGNORE_EXCLUDE"
 *         11_0_F 06.08.2012 FS Erweiterung um Optionale Argumente "EREIGNIS_IGNORE_TRUE" und "EREIGNIS_IGNORE_EXCLUDE"
 *         12_0_T 07.08.2012 FS BugFix bei "EREIGNIS_IGNORE_EXCLUDE": Ruecksetzen von DTCExcludeFlag
 *         13_0_F 07.08.2012 FS BugFix bei "EREIGNIS_IGNORE_EXCLUDE": Ruecksetzen von DTCExcludeFlag
 *		   14_0_T 22.08.2016 TB Erweiterungen f�r Paralleldiagnose analog zur DiagToleranzPP (LOP 2015), Generics eingebaut + Compiler Warnungen behoben<BR>
 *         15_0_F 03.02.2017 MKe F-Version 
 *         16_0_T 21.07.2017 MKe Es besteht nun die M�glichkeit dem Parameter ANALYSE_PP ein einzelnes Semikolon zu �bergeben. In diesem Fall ignoriert er die ANALYSE_PP Parametrierung.
 *         17_0_F 26.07.2017 MKe F-Version
 *		   18_0_T 09.08.2017 MK Erweiterung der Fehlermeldung wenn falsche PP als Analyse-PP angegeben wird. Siehe LOP 2240.
 *         19_0_F 09.08.2017 MK F-Version
 *         20_0_T 23.01.2018 MKe Bugfix bei der Freigabe der Paralleldiagnose
 *         21_0_F 23.01.2018 MKe F-Version
 **/
public class DiagLeseFSUDS_21_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagLeseFSUDS_21_0_F_Pruefprozedur() {
	}

	/**
	   * Erzeugt eine neue Pruefprozedur.
	   * @param pruefling Klasse des zugeh. Pr�flings
	   * @param pruefprozName Name der Pr�fprozedur
	   * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	   */
	public DiagLeseFSUDS_21_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		super.attributeInit();
	}

	/**
	 * Initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente
	 * @return eine Liste sie optionale Argurmenten
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "JOB", "IGNORE", "ANALYSE_PP", "CLEAR_PP", "IGNORE_MESSAGE", "PPLDB_TEXT", "EREIGNIS_IGNORE_TRUE", "EREIGNIS_IGNORE_EXCLUDE", "TAG" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente
	 * @return eine Liste sie zwingende Argurmenten
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "SGBD" };
		return args;
	}

	/**
	 * Pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 * @return true wenn Argumente iO sind sonst false
	 */
	@Override
	public boolean checkArgs() {
		Pruefprozedur p;
		String temp;
		boolean ok;
		int i;

		try {
			ok = super.checkArgs();
			if( ok == true ) {
				//SGBD
				temp = getArg( getRequiredArgs()[0] );
				if( temp.indexOf( ';' ) != -1 )
					return false;
				//Job
				temp = getArg( getOptionalArgs()[0] );
				if( temp != null ) {
					if( temp.indexOf( ';' ) != -1 )
						return false;
				}
				//Ignore
				temp = getArg( getOptionalArgs()[1] );
				try {
					if( temp != null )
						temp = extractErrorNumbers( temp );
				} catch( NumberFormatException e ) {
					return false;
				}
				//Analyse-PPs
				temp = getArg( getOptionalArgs()[2] );
				if( temp != null && !temp.equalsIgnoreCase( ";" ) ) {
					String[] tempArray = splitArg( temp );
					for( i = 0; i < tempArray.length; i++ ) {
						p = getPr�fling().getPr�fprozedur( tempArray[i] );
						if( p == null )
							return false;
						if( p.checkArgs() == false )
							return false;
					}
				}
				//Clear-PP
				temp = getArg( getOptionalArgs()[3] );
				if( temp != null ) {
					p = getPr�fling().getPr�fprozedur( temp );
					if( p == null )
						return false;
					if( p.checkArgs() == false )
						return false;
				}
				//EREIGNIS_IGNORE_TRUE
				if( getArg( getOptionalArgs()[6] ) != null ) {
					if( (getArg( getOptionalArgs()[6] ).toUpperCase().equalsIgnoreCase( "TRUE" ) == false) && (getArg( getOptionalArgs()[6] ).toUpperCase().equalsIgnoreCase( "FALSE" ) == false) ) {
						return false;
					}
				}
				//EREIGNIS_IGNORE_EXCLUDE
				temp = getArg( getOptionalArgs()[7] );
				try {
					if( temp != null )
						temp = extractErrorNumbers( temp );
				} catch( NumberFormatException e ) {
					return false;
				}
			}
			return ok;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( System.getProperty( "user.language" ).equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String sgbd = null;
		String job = null;
		String clear = null;
		String ignoreMessageText = null;
		String temp = null;
		boolean errorPresent = false;
		boolean ereignisIgnoreDTC = false;
		boolean DTCExcludeFlag = false;
		String[] analyseNames = new String[0];
		String[] ignoreNumbers = new String[0];
		String[] ignoreNumbersExclude = new String[0];
		Pruefprozedur[] analysePruefprozeduren = new Pruefprozedur[0];
		Pruefprozedur clearPruefprozedur = null;
		Hashtable<String, Ergebnis> temporaryResults = new Hashtable<String, Ergebnis>();
		boolean DEBUG = false;
		boolean alreadyReleased = false;
		EdiabasProxyThread ediabas = null;
		final boolean DE = checkDE(); // Systemsprache DE wenn true

		//Paralleldiagnose
		boolean parallel = false;
		String tag = null;
		DeviceManager devMan = null;

		/***********************************************
		 * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
		 * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		 ***********************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		try {
			// Parameter holen / initialisieren
			try {
				// Parameter existenz
				if( checkArgs() == false )
					throw new PPExecutionException( "Parameterexistenz" );
				// SGBD
				sgbd = extractValues( getArg( getRequiredArgs()[0] ) )[0];
				// Jobname
				job = getArg( getOptionalArgs()[0] );
				if( job == null )
					job = "FS_LESEN";
				// Fehler die auszublenden sind
				temp = getArg( getOptionalArgs()[1] );
				if( temp != null ) {
					temp = extractErrorNumbers( temp );
					ignoreNumbers = temp.split( ";" );
				}
				// ignoreDTC(Fehler ignorieren bei DTC = 1)
				if( getArg( getOptionalArgs()[6] ) != null ) {
					if( getArg( getOptionalArgs()[6] ).toUpperCase().equalsIgnoreCase( "TRUE" ) ) {
						ereignisIgnoreDTC = true;
					}
				}
				// IgnoreNumbersExclude (Fehler die nicht ignoriert werden, trotz DTC=1 bei ignoreDTC)
				temp = getArg( getOptionalArgs()[7] );
				if( temp != null ) {
					temp = extractErrorNumbers( temp );
					ignoreNumbersExclude = temp.split( ";" );
				}

				// Analyse Pr�fprozedur(en)
				temp = getArg( getOptionalArgs()[2] );
				if( temp != null && !temp.equalsIgnoreCase( ";" ) ) {
					analyseNames = splitArg( temp );
					analysePruefprozeduren = new Pruefprozedur[analyseNames.length];
					for( int i = 0; i < analysePruefprozeduren.length; i++ ) {
						analysePruefprozeduren[i] = getPr�fling().getPr�fprozedur( analyseNames[i] );
						if( analysePruefprozeduren[i].getArg( "FORT" ) == null )
							if( DE == true )
								throw new PPExecutionException( "Parametrisierte ANALYSE_PP muss Parameter FORT enthalten" );
							else
								throw new PPExecutionException( "Parameterized ANALYSE_PP has to have the parameter FORT" );
					}
				}

				// Job um Fehlerspeicher zu l�schen (falls vorhanden)
				clear = getArg( getOptionalArgs()[3] );
				// Sollen Fehler auf Fehlerprotokoll erscheinen, aber mit "Fehler kann ignoriert werden)
				temp = getArg( getOptionalArgs()[4] );
				if( temp != null ) {
					if( temp.equalsIgnoreCase( "TRUE" ) ) {
						ignoreMessageText = PB.getString( "ignoreMessage" );
						if( ignoreMessageText.startsWith( "ignore" ) ) {
							ignoreMessageText = "Fehler kann ignoriert werden";
						}
					}
				}
			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			// Ausf�hrung
			try {
				// Paralleldiagnose, Pr�fstandvariabel auslesen, ob Paralleldiagnose 
				try {
					Object pr_var;

					pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
					if( pr_var != null ) {
						if( pr_var instanceof String ) {
							if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
								parallel = true;
							}
						} else if( pr_var instanceof Boolean ) {
							if( ((Boolean) pr_var).booleanValue() ) {
								parallel = true;
							}
						}
					}
				} catch( VariablesException e ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				}

				// Devicemanager
				devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();

				if( getArg( "TAG" ) != null )
					tag = getArg( "TAG" );

				if( parallel ) // Ediabas holen
				{
					try {
						ediabas = devMan.getEdiabasParallel( tag, sgbd );
					} catch( Exception ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Wrong Cascade configuration for parallel diagnosys", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();
					} catch( Throwable ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();
					}
				} else {
					try {
						ediabas = devMan.getEdiabas( tag );
					} catch( Exception ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();

					}
				}
				// <<<<< Paralleldiagnose            

				// EDIABAS Job ausf�hren
				temp = ediabas.executeDiagJob( sgbd, job, "", "" );
				if( temp.equals( "OKAY" ) == false ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} else {
					result = new Ergebnis( "Status", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				}
				// Alle Fehlerspeicherinfos (tempor�r) speichern
				for( int i = 1; i < (ediabas.getDiagJobSaetze() - 1); i++ ) {
					String fOrtNr = ediabas.getDiagResultValue( i, "F_ORT_NR" );
					String fOrtText = ediabas.getDiagResultValue( i, "F_ORT_TEXT" );
					String fEreignisDTC = ediabas.getDiagResultValue( i, "F_EREIGNIS_DTC" ); //neu		
					String fVorhandenText = null;
					String fVorhandenNr = null;
					String fHexCode = null;
					try {
						fHexCode = ediabas.getDiagResultValue( i, "F_HEX_CODE" );

					} catch( Exception ernfe ) {
						// Nicht alle Steuerger�te unterst�tzen diese Zusatzinformation, also Fehler ignorieren
						fHexCode = null;
					}

					try {

						fVorhandenText = ediabas.getDiagResultValue( i, "F_VORHANDEN_TEXT" );
					} catch( Exception ernfe ) {
						// Nicht alle Steuerger�te unterst�tzen diese Zusatzinformation, also Fehler ignorieren
						fVorhandenText = "";
					}

					try {

						fVorhandenNr = ediabas.getDiagResultValue( i, "F_VORHANDEN_NR" );
					} catch( Exception ernfe ) {
						// Nicht alle Steuerger�te unterst�tzen diese Zusatzinformation, also Fehler ignorieren
						fVorhandenNr = "";
					}

					String hwt = "";
					String errorType = Ergebnis.FT_NIO;
					// Flag setzen, notwendig um entscheiden zu k�nnen ob FS gel�scht werden soll
					errorPresent = true;

					// Falls Fehler ausgeblendet werden soll, FT_IGNORE oder Hinweistext setzen
					for( int j = 0; j < ignoreNumbers.length; j++ ) {
						if( ignoreNumbers[j].equalsIgnoreCase( fOrtNr ) ) {
							if( ignoreMessageText != null ) {
								hwt = ignoreMessageText;
							} else {
								errorType = Ergebnis.FT_IGNORE;
							}
						}
					}

					//Falls Fehler ausgeblendet werden soll weil Ereignis_DTC = 1, FT_IGNORE oder Hinweistext setzen
					if( ereignisIgnoreDTC == true ) {
						if( fEreignisDTC.charAt( 0 ) == '1' ) {
							for( int j = 0; j < ignoreNumbersExclude.length; j++ ) {
								if( ignoreNumbersExclude[j].equalsIgnoreCase( fOrtNr ) ) {
									DTCExcludeFlag = true;
								}
							}

							if( DTCExcludeFlag == false ) {
								if( ignoreMessageText != null ) {
									hwt = ignoreMessageText;
								} else {
									errorType = Ergebnis.FT_IGNORE;
								}
							}
						}
						DTCExcludeFlag = false;
					}

					// Existiert Zusatzinformation �ber einen Hex-Fehlercode?
					if( fHexCode != null )
						result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", fOrtNr + " (HEX " + fHexCode + ")", "", "", "0", "", fVorhandenNr, fVorhandenText, fOrtText, hwt, errorType );
					else
						result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", fOrtNr, "", "", "0", "", fVorhandenNr, fVorhandenText, fOrtText, hwt, errorType );
					temporaryResults.put( fOrtNr, result );
					if( DEBUG )
						writeDebug( "error found " + fOrtNr + ", " + fOrtText + ", " + hwt + ", " + errorType );
				}
			} catch( ApiCallFailedException e ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Freigabe Paralleldiagnose
			if( parallel ) {
				try {
					devMan.releaseEdiabasParallel( ediabas, tag, sgbd );
					alreadyReleased = true;
				} catch( Exception ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				} catch( Throwable t ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
			}

			// Ggf. weitere Analyse f�r Fehler die (noch) nicht ausgeblendet werden k�nnen
			for( int i = 0; i < analysePruefprozeduren.length; i++ ) {
				boolean executeAnalyse = false;
				String[] tempErrors = extractErrorNumbers( analysePruefprozeduren[i].getArg( "FORT" ) ).split( ";" );

				// Pr�fen ob Analysepr�fprozedur durchgef�hrt werden muss
				for( int j = 0; j < tempErrors.length; j++ ) {
					if( DEBUG )
						writeDebug( "analyse PP " + analysePruefprozeduren[i].getName() + ", error " + tempErrors[j] );
					if( temporaryResults.containsKey( tempErrors[j] ) ) {
						result = (Ergebnis) temporaryResults.get( tempErrors[j] );
						if( result.getFehlerTyp() == Ergebnis.FT_NIO ) {
							if( DEBUG )
								writeDebug( "analyse PP " + analysePruefprozeduren[i].getName() + ", error " + tempErrors[j] + " match" );
							analysePruefprozeduren[i].setAttribut( "ERROR_SGBD", sgbd );
							analysePruefprozeduren[i].setAttribut( "ERROR_JOB", job );
							temporaryResults.remove( tempErrors[j] );
							executeAnalyse = true;
						}
					}
				}
				// Einzelne Analysepr�fprozedur(en) durchf�hren
				if( executeAnalyse ) {
					if( DEBUG )
						writeDebug( "analyse PP " + analysePruefprozeduren[i].getName() + ", executing" );
					// Aus FORT alle Codes entfernen, die zu ignorieren sind.
					// Vorher alte Parametrierung merken. [RETTIG]                    
					String oldFort = analysePruefprozeduren[i].getArg( "FORT" ); //alte FORT-Parametrierung merken                    
					List<String> fortList = new ArrayList<String>( Arrays.asList( tempErrors ) ); //FORT-Liste der Analyse-Pr�fprozedur (�ber ArrayList gehen, da asList das Array kapselt und somit keine �nderungen m�glich sind)                    
					List<String> ignoreList = Arrays.asList( ignoreNumbers ); //Liste mit den zu ignorierenden Fehlercodes
					fortList.removeAll( ignoreList ); //zu ignorierende Code aus der FORT-Liste l�schen
					analysePruefprozeduren[i].setArgs( "FORT=" + assembleErrorNumbers( (String[]) fortList.toArray( new String[0] ) ) ); //FORT-Parameter auf Basis der ge�nderten FORT-Liste neu setzen
					// Pr�fprozedur durchf�hren
					analysePruefprozeduren[i].setPr�flingLaufzeitUmgebung( getPr�flingLaufzeitUmgebung() );
					analysePruefprozeduren[i].execute( info );
					// Alte FORT-Parametrierung wiederherstellen.
					analysePruefprozeduren[i].setArgs( "FORT=" + oldFort ); //alte FORT-Parametrierung wiederherstellen [RETTIG]
					// Pr�fprozedur Ergebnis dokumentieren
					if( analysePruefprozeduren[i].getExecStatus() != STATUS_EXECUTION_OK ) {
						if( DEBUG )
							writeDebug( "analyse PP " + analysePruefprozeduren[i].getName() + ", niO" );
						status = STATUS_EXECUTION_ERROR;
						result = new Ergebnis( "FS", getPr�fling().getName() + "." + analysePruefprozeduren[i].getName(), "", "", "", "Status", PB.getString( "ausfuehrungsstatusNIO" ), "", "", "", "", "", "", "", "", Ergebnis.FT_NIO );
						ergListe.add( result );
					} else {
						if( DEBUG )
							writeDebug( "analyse PP " + analysePruefprozeduren[i].getName() + ", iO" );
						result = new Ergebnis( "FS", getPr�fling().getName() + "." + analysePruefprozeduren[i].getName(), "", "", "", "Status", "", "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
				}
			}
			// Nach der Analyse verbleibende Fehler zum Fehlerliste hinzuf�gen (und Status setzen)
			Enumeration<Ergebnis> myEnum = temporaryResults.elements();
			while( myEnum.hasMoreElements() ) {
				result = (Ergebnis) myEnum.nextElement();
				if( result.getFehlerTyp() == Ergebnis.FT_NIO ) {
					status = STATUS_EXECUTION_ERROR;
				}
				ergListe.add( result );
			}
			// Nach der Analyse ist ggf. der Fehlerspeicher zu l�schen
			if( errorPresent && (status == STATUS_EXECUTION_OK) && (clear != null) ) {
				clearPruefprozedur = getPr�fling().getPr�fprozedur( clear );
				clearPruefprozedur.setPr�flingLaufzeitUmgebung( getPr�flingLaufzeitUmgebung() );
				clearPruefprozedur.execute( info );
				status = clearPruefprozedur.getExecStatus();
			}
		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception (" + e.getMessage() + ")", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable (" + e.getMessage() + ")", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Freigabe Paralleldiagnose
		if( parallel && alreadyReleased == false ) {
			try {
				devMan.releaseEdiabasParallel( ediabas, tag, sgbd );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable t ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		setPPStatus( info, status, ergListe );

	}

	private void writeDebug( String info ) {
		System.out.println( "DiagLeseFS: " + getPr�fling().getName() + "." + getName() + " " + info );
	}

	/**
	 * Hilfsmethode f�r Argument IGNORE: L�st den �bergebenen String auf
	 * @param in der Argument IGNORE
	 * @return vollst�ndige String (zB "1-3" wird als "1;2;3" zur�ckgeliefert)
	 */
	private String extractErrorNumbers( String in ) {
		String sub, subSub;
		int[] posArray;
		int counter = 1;
		int min, max;
		int i, j;

		for( i = 0; i < in.length(); i++ ) {
			if( in.charAt( i ) == ';' )
				counter++;
		}

		posArray = new int[counter + 1];
		j = 0;
		posArray[j++] = -1;
		for( i = 0; i < in.length(); i++ ) {
			if( in.charAt( i ) == ';' )
				posArray[j++] = i;
		}
		posArray[j++] = in.length();

		StringBuffer temp = new StringBuffer();
		//temp.append( ";" );
		for( i = 0; i < counter; i++ ) {
			sub = (in.substring( posArray[i] + 1, posArray[i + 1] )).trim();
			if( sub.compareTo( "" ) != 0 ) {
				j = sub.indexOf( "-" );
				if( (j < 1) || (j == (sub.length() - 1)) ) {
					temp.append( sub );
					temp.append( ";" );
				} else {
					subSub = sub.substring( 0, j );
					min = Integer.parseInt( subSub );
					subSub = sub.substring( j + 1, sub.length() );
					max = Integer.parseInt( subSub );
					for( j = min; j <= max; j++ ) {
						temp.append( j );
						temp.append( ";" );
					}
				}
			}
		}
		return temp.toString();
	}

	/**
	 * Baut aus dem �bergebenen String-Array mit Fehlercodes einen String, wo 
	 * aufeinander folgende Code in der Schreibweise "n-m" 
	 * (z. B. 8779879-8779900)zusammengefasst werden. Nicht zusammenfassbare 
	 * Einzelcodes sowie zusammengefasste Codes werden mit Semikolon getrennt 
	 * (z. B. 8779879-8779900;23424423;2500000-2500650).
	 * 
	 * Hinweis: Folgen, welche nur aus zwei Code bestehen (z.B. 10,11) werden
	 * nicht in Von-Bis-Bereiche (10-11) �berf�hrt, sondern bleiben Einzelcodes
	 * (10;11). 
	 * 
	 * @param in Ein String-Array mit den zu verarbeitenden Codes.
	 * @return Der generierte String.
	 */
	private String assembleErrorNumbers( String[] in ) { //[RETTIG]
		StringBuffer buffer = new StringBuffer();
		double vOld, v, lastAppendedIndex;

		if( in == null || in.length == 0 )
			return "";

		//  Erstes Element einf�gen.
		buffer.append( in[0] );
		vOld = Double.parseDouble( in[0] );
		lastAppendedIndex = 0;

		//  Restlichen Elemente durchgehen,
		for( int i = 1; i < in.length; i++ ) {
			v = Double.parseDouble( in[i] ); //aktuelles Element				
			if( v - vOld != 1 ) { //Differenz zwischem letzten und aktuellen Element = 1? 
				if( i - lastAppendedIndex > 1 ) { //Liegen zwischem dem letzten �bernommenen Element und dem aktuellen Element ein oder mehr Elemente? Dann bilden wir einen Von-Bis-Bereich. 
					if( i - lastAppendedIndex > 2 ) //�berspannt der Von-Bis-Bereich mehr als zwei Codes? 
						buffer.append( "-" ); //Ja. Trennzeichen ist ein -.
					else
						buffer.append( ";" ); //Nein. Dann doch keinen Von-Bis-Bereich anfangen (Trennzeichen ist ein ;).
					buffer.append( in[i - 1] ); //Bis-Element anh�ngen
					buffer.append( ";" );
					buffer.append( in[i] ); //aktuelles Element anh�ngen
				} else { //kein Von-Bis-Bereich
					buffer.append( ";" );
					buffer.append( in[i] ); //aktuelles Element anh�ngen
				}
				lastAppendedIndex = i;
			}
			vOld = v;
		}

		//  Wenn das letzte Element Teil eines Von-Bis-Bereiches war, dann 
		//  diesen nun schlie�en.
		if( lastAppendedIndex != (in.length - 1) ) {
			if( (in.length - 1) - lastAppendedIndex > 1 )
				buffer.append( "-" );
			else
				buffer.append( ";" );
			buffer.append( in[in.length - 1] );
		}

		return buffer.toString();
	}

	/*
	    static public void main(String args[]) {
	    	System.out.println( assembleErrorNumbers(null) );
	    	System.out.println( assembleErrorNumbers(new String[] {}) );
	    	System.out.println( assembleErrorNumbers(new String[] {"10"}) );
	    	System.out.println( assembleErrorNumbers(new String[] {"10", "20"}) );
	    	System.out.println( assembleErrorNumbers(new String[] {"11", "10"}) );
	    	    	   
	    	System.out.println( assembleErrorNumbers(new String[] {"10", "11"}) );
	    	System.out.println( assembleErrorNumbers(new String[] {"10", "11", "99"}) );
	    	System.out.println( assembleErrorNumbers(new String[] {"99", "10", "11"}) );
	    	System.out.println( assembleErrorNumbers(new String[] {"99", "10", "11", "99"}) );
	    	
	    	System.out.println( assembleErrorNumbers(new String[] {"10", "11", "12"}) );
	    	System.out.println( assembleErrorNumbers(new String[] {"10", "11", "12", "99"}) );
	    	System.out.println( assembleErrorNumbers(new String[] {"99", "10", "11", "12"}) );
	    	System.out.println( assembleErrorNumbers(new String[] {"99", "10", "11", "12", "99"}) );
	    	
	    	System.out.println( assembleErrorNumbers(new String[] {"10", "11", "12", "5", "6", "7", "1", "1", }) );
	    	System.out.println( assembleErrorNumbers(new String[] {"10", "11", "12", "5", "6", "8", "1", "1", }) );
	    	System.out.println( assembleErrorNumbers(new String[] {"10", "11", "12", "5", "6", "1", "2", "3", }) );
	    	System.out.println( assembleErrorNumbers(new String[] {"10", "11", "12", "5", "6", "1", "1", "2", }) );
	    }
	*/
}

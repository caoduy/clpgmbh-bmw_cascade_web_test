package com.bmw.cascade.pruefprozeduren;

import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.DeviceIOException;
import com.bmw.cascade.pruefstand.devices.relaycontrol.RelayControl;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;

/**
 * Implementierung der Pr�fprozedur, die die Reifendrucksensoren - Ids ermittelt.
 * Ablauf
 * - Warten auf ein Fahrzeut (DigitalIO Input 1)
 * - Starten des Tests auf Tpm-Device
 * - Check auf Abbruch: User-Cancel, Timeout oder Ende des Fahrzeut (DigitalIO Input 2)
 * - zyclisches Abfragen des Tpm-Device-Status bis != "in process" (oder Abbruchbedingung => NIO)
 * - Auswerten des Ergebnisses des Tpm Device : a)  alle 4 Ids empfangen => fertig
 * -                                            b)  nur 2 Ids empfangen => erneut starten,
 *                                                  werden dann die �brigen zwei Ids empfangen,
 *                                                  so werden diese als Hinterreifen interpretiert
 *                                              c)  Keine Id empfangen => erneut starten
 * - Check ob alle Ids Ok (!="0000000000" && !="9999999999")
 *
 * @author Gefasoft Regensburg Moosbauer Christian, Staudinger, Weber
 * @version V0_0_1  06.04.2005  Implementierung
 * @version V0_0_4  14.05.2005  RelayControl anstatt DigitalIODevice
 * @version V0_0_5  18.05.2005  SensorIds auch im Fehlerfall in Ergebnissliste
 * @version V0_0_6  07.03.2006  Warte auf negative Flanke Endebit; Pr�fe: sensorIds!=Ok, wenn kein RDC
 * @version V0_0_7  08.06.2006  tpmDevice.close(); auskommentiert. Devicemanager k�mmert sich darum. Falls die PP das device instanziert wird der Port im destructor freigegeben
 * @version V0_0_8  08.06.2006  Optionaler Parameter LOG_Prefix hinzugef�gt. Zuordnung der Hinteren R�der angepasst.
 * @version V0_0_9  30.06.2006  RelayControl Ausg�nge 0:AmpelGr�n, 1:AmpelRot(systemFehler), test2 mu� mindesten 1x ausgef�hrt werden
 * @version V0_0_10 15.08.2006  Optional Parameter: ASSIGN_BACKTOFRONT (default=FALSE)
 * @version V0_0_11 21.08.2006  Check Sensor id plausibility: all 4 ids must be different.
 * @author BMW AG Ludwig Staudinger, ZA-T-425
 * @version V0_0_12 T 22.05.2007  Interference (9999999999) for non-RDC Cars ignored and replaced by (0000000000)if optional Parameter IGNORE_NORDC_INTERFERENCE=TRUE.
 * @version V0_0_13 F 22.05.2007  live version of 12_0_T
 * @version V0_0_14 F 21.08.2007  fw added memory debug
 * @version V0_0_15 T 26.11.2007  fw eliminated unnecessary log outputs, corrected result names according to Casade/APDM spec
 * @version V0_0_16 F 26.11.2007  fw live version of 15_0_T
 * @version V0_0_17 F 23.08.2008  fw added new parameters for sending serial commands like GEN filtering/level before test, corrected error outputs
 * @version V0_0_18 F 23.08.2008  fw live version of 17_0_T
 * @version V0_0_19 T 16.07.2009  fw debugging extended
 * @version V0_0_20 T 29.10.2009  fd Close bei SerialGeneric und TPM Device
 * @author Gefasoft Regensburg Moosbauer Christian
 * CASCADE VERSION : starting with 3_0_1f or higher
 * @version V0_1_20 T 16.02.2010  TPM Device returns generation of sensors, PP checks sensor generations if GEN_FILTER > 0, TPM Device has functions to send commands and wait for answer, set genFilter if param is 0 => disable if it was 2/3 previouse 
 * @version V0_1_22 F 17.02.2010  if genFilter > 0 => PP ignores sensorId
 * @version V0_1_23 F 26.02.2010  new parameter OVERWRITE_CROSSTALK, TRUE => 9999999999 -> 1234567890
 * @version V0_1_24 F 12.03.2010  logging if userdialog is null, only close tpmDevice if we created an instance here (not from device manager)
 * @version V0_1_25 F 16.03.2010  check subparams of parameter "COMPORT" (if it is like "COM1;10;DEBUG" and we add ";10;DEBUG" automatically, this will cause an error in the constructor, becuase [3] is the baudrate !!!)
 * @version V0_1_26 F 11.10.2010  checkForAbortTest for 100ms (was 1500ms) => check for test_2 complete all 100 ms, lMaxRetryStartTest changed from 5 to -1 => infinite
 * @version V0_1_27 T 15.10.2010  sensor generation 3.5, genfilter must be set to 4 and device returnes genlevel 35 => in pr�fling the parameter GEN_FILTER of this PP must be set to "35" 
 * @version V0_1_28 F 15.10.2010  only F_Version 
 * @version V0_1_29 F 06.07.2011  retry to send the parameters to EOL tester, log a warning if this fails, new optional arg IGNORE_SEND_PARAMS_FAIL (default: false)
 * @version V0_1_30 T 10.01.2012  sensor Conti RDCi, genfilter must be set to 5 and device returnes genlevel 80 => in pr�fling the parameter GEN_FILTER of this PP must be set to "80" 
 * @version V0_1_31 F 28.03.2012  sensor Conti RDCi, genfilter must be set to 5 and device returnes genlevel 80 => in pr�fling the parameter GEN_FILTER of this PP must be set to "80" 
 * @version V0_1_32 T 30.11.2012  new Tpm-Device-Parameters: Test2_matches, Test2_front_dur, Response_wait_dur, Trigger_cycles
 * @version V0_1_33 F 17.12.2012  F-version


 */
public class RDC_33_0_F_Pruefprozedur extends AbstractBMWPruefprozedur
{
	static final long serialVersionUID = 1L;
	static final int checkForAbortTestTime = 100; // [ms]
	/**
	 * RelayControl bits
	 */
	static final int INBIT_BEGIN_OF_CAR = 1;
	static final int INBIT_END_OF_CAR = 2;

	static final int OUTBIT_LIGHT_GREEN = 1;
	static final int OUTBIT_LIGHT_RED = 2;

	//language
	private final boolean isEnglish = System.getProperty( "user.language" ).equalsIgnoreCase( "EN" );

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public RDC_33_0_F_Pruefprozedur() {}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public RDC_33_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted)
	{
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	boolean bDEBUG = false;

	/** Debug Level: 0 keine Ausgaben 
	 *               20 maximale Ausgaben
	 */
	private int m_debug_level=0;

	private int TIMEOUT_SERIAL = 500;

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit()
	{
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs()
	{
		String[] args = {"RESULTDISPLAYTIME","DEBUG",
				"IGNORE_NO_DIGITAL_IO","COMPORT",
				"INIT_RELAY","NO_RDC","WAIT_FOR_END_OF_CAR",
				"LOG_PREFIX","ASSIGN_BACKTOFRONT","OVERWRITE_CROSSTALK",
				"IGNORE_NORDC_INTERFERENCE","MEMORY_DEBUG","---",
				"GEN_FILTER", "LEVEL", "TEST", "IGNORE_SEND_PARAMS_FAIL", 
				// dbu/20121130
				"TEST2_MATCHES", "TEST2_FRONT_DUR","RESPONSE_WAIT_DUR", "TRIGGER_CYCLES", };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs()
	{
		String[] args = {"TIMEOUT","START_SIGNAL_EDGE"};
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 */
	public boolean checkArgs()
	{
		boolean ok = true;

		try
		{
			ok = super.checkArgs();
			Long.parseLong(getArg("TIMEOUT"));
		}
		catch (Exception e)
		{
			ok = false;
		}
		catch (Throwable e)
		{
			ok = false;
		}
		return ok;
	}

	class RID
	{
		public String id = "";
		public int gen = -1;
	}
	class ResultIDs
	{
		public RID idvl=new RID();
		public RID idvr=new RID();
		public RID idhl=new RID();
		public RID idhr=new RID();
		public boolean bTest2OnceSuc=false;
	};

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info )
	{
		//20120621/dbb:  Wrong Log Entry
		System.out.println("RDC_32_0_T_Pruefprozedur::execute");
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		ResultIDs resultIDsRes = new ResultIDs();
		String szComPort = "COM1";
		long lTimeout = 90000;
		long lMilliesStart = 0;
		long lMaxRetryStartTest = -1; // was 5, now infinite
		int nShowResultDuration = 10;
		boolean bMemDEBUG = false;
		boolean bIgnoreNoDigitalIO = false;
		boolean bStartSignalEdge = true;
		boolean bInitRelay = true;
		boolean bHasRDC = true;
		boolean bWaitForEndOfCar = true;
		String szLogPrefix="";

		boolean bDevmanagerReleaseTpm = false;
		boolean bDevmanagerReleaseDigIO = false;
		boolean bAssignBackToFront = false;
		boolean bOverwriteCrossTalk = false;
		boolean bIgnoreNoRDCInterference = false;

		boolean bSetGenFilter = false;
		boolean bSetLevel = false;
		boolean bTest2Matches = false;
		boolean bTest2FrontDur = false;
		boolean bResponseWaitDur = false;
		boolean bTriggerCycles = false;
		boolean bIgnoreSendParamsFail = false;

		String sLevel = "-1";
		String sGenFilter = "-1";
		String sTest2Matches = "-1";
		String sTest2FrontDur = "-1";
		String sResponseWaitDur = "-1";
		String sTriggerCycles = "-1";
		int nGenFilter = -1;

		com.bmw.cascade.pruefstand.visualisierung.UserDialog myDialog = null;
		com.bmw.cascade.pruefstand.devices.tpm.TpmDevice tpmDevice = null;//TpmBeruF1 tpmDevice = null;
		RelayControl digitalIOCard = null;

		DeviceManager devManager = null;
		if(null != getPr�flingLaufzeitUmgebung())
		{
			devManager = getPr�flingLaufzeitUmgebung().getDeviceManager();
		}

		try
		{
			//Parameter holen
			try
			{
				if( checkArgs() == false )
				{
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				}

				//String sArg,sArg2;
				String sArg;

				// TIMEOUT
				lTimeout = Long.parseLong(getArg("TIMEOUT"));

				//START_SIGNAL_EDGE
				if( getArg( "START_SIGNAL_EDGE" ) != null )
					bStartSignalEdge = (0==getArg( "START_SIGNAL_EDGE" ).compareToIgnoreCase("TRUE"));
				//NO_RDC
				if( getArg( "NO_RDC" ) != null )
					bHasRDC = (0!=getArg( "NO_RDC" ).compareToIgnoreCase("TRUE"));
				//WAIT_FOR_END_OF_CAR
				if( getArg( "WAIT_FOR_END_OF_CAR" ) != null )
					bWaitForEndOfCar = (0==getArg( "WAIT_FOR_END_OF_CAR" ).compareToIgnoreCase("TRUE"));

				if( getArg( "LOG_PREFIX" ) != null )
					szLogPrefix = getArg( "LOG_PREFIX" );

				if( getArg( "INIT_RELAY" ) != null )
					bInitRelay = (0==getArg( "INIT_RELAY" ).compareToIgnoreCase("TRUE"));

				// DEBUG
				if( getArg( "DEBUG" ) != null )
					bDEBUG = (0==getArg( "DEBUG" ).compareToIgnoreCase("TRUE"));

				// MEMDEBUG
				if( getArg( "MEMORY_DEBUG" ) != null )
					bMemDEBUG = (0==getArg( "MEMORY_DEBUG" ).compareToIgnoreCase("TRUE"));
				//IGNORE_NO_DIGITAL_IO
				if( getArg( "IGNORE_NO_DIGITAL_IO" ) != null )
					bIgnoreNoDigitalIO = (0==getArg( "IGNORE_NO_DIGITAL_IO" ).compareToIgnoreCase("TRUE"));

				String sResDisplTime = getArg("RESULTDISPLAYTIME");
				if( null != sResDisplTime)
					nShowResultDuration = Integer.parseInt(sResDisplTime);

				// COMPORT
				if(null != getArg( "COMPORT" ))
					szComPort =  getArg( "COMPORT" );

				//ASSIGN_BACKTOFRONT
				if( getArg( "ASSIGN_BACKTOFRONT" ) != null )
					bAssignBackToFront = (0==getArg( "ASSIGN_BACKTOFRONT" ).compareToIgnoreCase("TRUE"));

				// OVERWRITE_CROSSTALK
				if( getArg( "OVERWRITE_CROSSTALK" ) != null )
					bOverwriteCrossTalk = (0==getArg( "OVERWRITE_CROSSTALK" ).compareToIgnoreCase("TRUE"));
				//IGNORE_NORDC_INTERFERENCE
				if( getArg( "IGNORE_NORDC_INTERFERENCE" ) != null )
					bIgnoreNoRDCInterference = (0==getArg( "IGNORE_NORDC_INTERFERENCE" ).compareToIgnoreCase("TRUE"));

				//---- new settings before test -----------

				//setLevel
				sArg = getArg( "LEVEL" );
				if( sArg != null && sArg.length() > 0 ) {
					int tempInt = Integer.parseInt(sArg);
					if (tempInt<1 || tempInt >5) {
						if (isEnglish) throw new PPExecutionException( "LEVEL out of range [1;5]");
						else throw new PPExecutionException( "LEVEL au�erhalb des g�ltigen Bereich [1;5]");
					}
					bSetLevel = true;
					sLevel = sArg;
				}

				//set filter
				sArg = getArg( "GEN_FILTER" );
				if( sArg != null && sArg.length() > 0 ) {
					nGenFilter = Integer.parseInt(sArg);
					if (nGenFilter!=0 && nGenFilter!=2 && nGenFilter!=3 && nGenFilter!=35 && nGenFilter!=80) {
						if (isEnglish) throw new PPExecutionException( "GEN_FILTER out of range {0,2,3,35,80}");
						else throw new PPExecutionException( "GEN_FILTER au�erhalb des g�ltigen Bereich {0,2,3,35,80}");
					}
					if (nGenFilter < 0) bSetGenFilter = false;
					else bSetGenFilter = true;
					sGenFilter = sArg;
					//IGNORE_SEND_PARAMS_FAIL
					if( getArg( "IGNORE_SEND_PARAMS_FAIL" ) != null )
						bIgnoreSendParamsFail = (0==getArg( "IGNORE_SEND_PARAMS_FAIL" ).compareToIgnoreCase("TRUE"));
				}
				// dbu/20121130: "TEST2_MATCHES", "TEST2_FRONT_DUR","RESPONSE_WAIT_DUR", "TRIGGER_CYCLES" 
				// dbu/20121204: Validation according to BMW EOL Specification V1.3.pdf, page 15
				sArg = getArg( "TEST2_MATCHES" );
				if( sArg != null && sArg.length() > 0 ) {
					int tempInt = Integer.parseInt(sArg);
					if (tempInt<1 || tempInt >10) {
						if (isEnglish) throw new PPExecutionException( "TEST2_MATCHES out of range [1;10]");
						else throw new PPExecutionException( "TEST2_MATCHES au�erhalb des g�ltigen Bereich [1;10]");
					}
					bTest2Matches = true;
					sTest2Matches = sArg;
				}
				sArg = getArg( "TEST2_FRONT_DUR" );
				if( sArg != null && sArg.length() > 0 ) {
					int tempInt = Integer.parseInt(sArg);
					if (tempInt<1 || tempInt >250) {
						if (isEnglish) throw new PPExecutionException( "TEST2_FRONT_DUR out of range [1;250], unit is sec/100");
						else throw new PPExecutionException( "TEST2_FRONT_DUR au�erhalb des g�ltigen Bereich [1;250], die Einheit ist sec/100");
					}
					bTest2FrontDur = true;
					sTest2FrontDur = sArg;
				}
				sArg = getArg( "RESPONSE_WAIT_DUR" );
				if( sArg != null && sArg.length() > 0 ) {
					int tempInt = Integer.parseInt(sArg);
					if (tempInt<1 || tempInt >250) {
						if (isEnglish) throw new PPExecutionException( "RESPONSE_WAIT_DUR out of range [1;250], unit is sec/100");
						else throw new PPExecutionException( "RESPONSE_WAIT_DUR au�erhalb des g�ltigen Bereich [1;250], die Einheit ist sec/100");
					}
					bResponseWaitDur = true;
					sResponseWaitDur = sArg;
				}
				sArg = getArg( "TRIGGER_CYCLES" );
				if( sArg != null && sArg.length() > 0 ) {
					int tempInt = Integer.parseInt(sArg);
					if (tempInt<1 || tempInt >10) {
						if (isEnglish) throw new PPExecutionException( "TRIGGER_CYCLES out of range [1;10]");
						else throw new PPExecutionException( "TRIGGER_CYCLES au�erhalb des g�ltigen Bereich [1;10]");
					}
					bTriggerCycles = true;
					sTriggerCycles = sArg;
				}

			}
			catch (PPExecutionException e)
			{
				if (e.getMessage() != null)
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add(result);
				throw new PPExecutionException();
			}

			if (bMemDEBUG) {
				System.out.println("RDC MemDebug: NumberThreads at beginning - "+Thread.activeCount());
				System.out.println("RDC MemDebug: Free Memory at beginning - "+Runtime.getRuntime().freeMemory());

				dumpThreads();      
			}



			// Digital IO device holen
			if (null != devManager)
			{
				if(bDEBUG)
					System.out.println("devManager.getRelayControl ...");
				try
				{
					digitalIOCard = devManager.getRelayControl();
					if(bDEBUG)
						System.out.println("devManager.getRelayControl suc");
					bDevmanagerReleaseDigIO = true;
				}
				catch(Throwable e)
				{
					if(bDEBUG)
						System.out.println("devManager.getRelayControl(): throwable: "+e);
					digitalIOCard = null;
					bDevmanagerReleaseDigIO = false;
				}
			}
			if(null == digitalIOCard)
			{
				try
				{
					digitalIOCard = new com.bmw.cascade.pruefstand.devices.relaycontrol.StandardRelayControl("ignore");
				}
				catch(Exception ex)
				{
					digitalIOCard = null;
					if(!bIgnoreNoDigitalIO)
					{
						throw(ex);
					}
				}
			}

			if((null == digitalIOCard)&&(false == bIgnoreNoDigitalIO))
			{
				// throw exception if no dig-IO found and not ignore
				if (bDEBUG) System.out.println("Error: no RelayControl found.");
				ergListe.add(new Ergebnis( 	"DeviceError", "", "", "", "", "", "", "", "", "0", "", "", "",
						"Failed to get Device RelayControl", "",Ergebnis.FT_NIO_SYS ));
				throw new PPExecutionException();
			}
			else if(null != digitalIOCard)
			{
				if (bDEBUG) System.out.println("RelayControl found.");
				if (bInitRelay) digitalIOCard.init();
			}

			// TPM device holen
			if (null != devManager)
			{
				if (bDEBUG) System.out.println("devManager.getTpmDevice ...");
				try
				{
					tpmDevice = devManager.getTpmDevice();
					if(null != tpmDevice)
					{
						if (bDEBUG) System.out.println("devManager.getTpmDevice suc");
						bDevmanagerReleaseTpm = true;
					}
				}
				catch(Throwable e)
				{
					if(bDEBUG)
						System.out.println("devManager.getTpmDevice(): throwable: "+e);
					tpmDevice = null;
					bDevmanagerReleaseTpm = false;
				}
			}

			if (null == tpmDevice)
			{
				try
				{
					if (bDEBUG) System.out.println("get TpmDevice directly ...");

					String szParam = szComPort;

					if (bDEBUG)
					{   // check param string for TpmBeruF1 constructor
						// default constructor param is "COM1;10;NODEBUG;9600"
						String[] subParams = szComPort.split( ";" );
						if(1 == subParams.length)
						{
							szParam += ";10;DEBUG";
						}
						else if(2 == subParams.length)
						{
							szParam += ";DEBUG";
						}
						else if(3 <= subParams.length)
						{
							if(!subParams[2].equalsIgnoreCase( "DEBUG" ))
							{
								//replace param[2] with "DEBUG"
								szParam = subParams[0] + ";" + subParams[1] + ";DEBUG";
								for(int i=3;i<subParams.length;i++)
								{
									szParam += (";" +  subParams[i]);
								}
							}
						}
					}

					tpmDevice = new com.bmw.cascade.pruefstand.devices.tpm.TpmBeruF1(szParam);
					if (bDEBUG) System.out.println("get TpmDevice directly suc");
				}
				catch(Exception e)
				{
					System.out.println("getTpbBeruF1 : exception");
					ergListe.add(new Ergebnis( 	"DeviceError", "", "", "", "", "", "", "", "", "0", "", "", "",
							"Failed to get TPM-Device: "+e.getMessage(), "",Ergebnis.FT_NIO_SYS ));
					throw new PPExecutionException();
				}
			}

			// ---------------------------------------------------------
			// -------- DO SETTINGS BEFORE TEST
			// ---------------------------------------------------------

			try
			{
				String genF = sGenFilter;
				if(35 == nGenFilter) genF = "4";
				else if(80 == nGenFilter) genF = "5";
				if (bSetGenFilter)
					setTpmDeviceParams(tpmDevice, genF, "Gen_filter ", bIgnoreSendParamsFail);
				if (bSetLevel)
					setTpmDeviceParams(tpmDevice, sLevel, "LF_power_level ", bIgnoreSendParamsFail);
				if (bTest2Matches)
					setTpmDeviceParams(tpmDevice, sTest2Matches, "Test2_matches ", bIgnoreSendParamsFail);
				if (bTest2FrontDur)
					setTpmDeviceParams(tpmDevice, sTest2FrontDur, "Test2_front_dur ", bIgnoreSendParamsFail);
				if (bResponseWaitDur)
					setTpmDeviceParams(tpmDevice, sResponseWaitDur, "Response_wait_dur ", bIgnoreSendParamsFail);
				if (bTriggerCycles)
					setTpmDeviceParams(tpmDevice, sTriggerCycles, "Trigger_cycles ", bIgnoreSendParamsFail);
			}
			catch (CError err)
			{
				ergListe.add(err.getEntry());
				throw new PPExecutionException();
			}
			// ---------------------------------------------------------
			// -------- NORMAL TEST
			// ---------------------------------------------------------

			// test ausf�hren
			if(bDEBUG) System.out.println("startingtest ...");
			if(null != getPr�flingLaufzeitUmgebung())
			{
				myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
				if(null == myDialog)
				{
					//if(bDEBUG)
					System.out.println("getPr�flingLaufzeitUmgebung().getUserDialog() returns null");    	      	  
				}
			}
			else
			{
				//if(bDEBUG)
				System.out.println("getPr�flingLaufzeitUmgebung() returns null => no userDialog");    	  
			}
			if(bDEBUG && (null != myDialog)) myDialog.setAllowCancel(true);
			if(null != myDialog)
			{
				myDialog.displayMessage( "test", PB.getString( "pruefprozedur.waitForVehicle" ), -1 );
			}

			// warte auf auto, Eingang 1
			if (null != digitalIOCard)
			{
				if (bDEBUG) System.out.println("waiting for inputBit 1 ...");
				waitForCar(digitalIOCard,myDialog,ergListe,bStartSignalEdge,bDEBUG);
			}

			// timeout form now
			lMilliesStart = System.currentTimeMillis();
			// test starten
			if(bDEBUG && (null != myDialog))
			{
				myDialog.setAllowCancel(true);
			}
			if(null != myDialog)
				myDialog.displayMessage( "test", PB.getString( "pruefprozedur.getSensorIds" ), -1 );

			getSensorIds(bHasRDC, lTimeout, lMilliesStart, lMaxRetryStartTest, bAssignBackToFront, bOverwriteCrossTalk, tpmDevice, digitalIOCard, myDialog, resultIDsRes, ergListe, bDEBUG, bIgnoreNoRDCInterference, nGenFilter);

			// ergebniss erzeugen
			if(!checkResultsOk(bHasRDC, resultIDsRes, nGenFilter, bDEBUG))
			{
				ergListe.add(new Ergebnis( 	"RESULT_IDS_NOK", "TPM", "", "", "", "", "", "", "", "0", "", "", "",
						PB.getString("pruefprozedur.resultIdsNOK"), "",Ergebnis.FT_NIO_SYS ));
				throw new PPExecutionException();
			}
			else if(!checkResultsPlausible(bHasRDC,resultIDsRes))
			{
				ergListe.add(new Ergebnis( 	"RESULT_IDS_NOT_PLAUSIBLE", "TPM", "", "", "", "", "", "", "", "0", "", "", "",
						PB.getString("pruefprozedur.resultIdsNotPlausible"), "",Ergebnis.FT_NIO_SYS ));
				throw new PPExecutionException();
			}
		}
		catch (PPExecutionException e)
		{
			status = STATUS_EXECUTION_ERROR;
		}
		catch (Exception e)
		{
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add(result);
			System.out.println("Exception: "+e);
			e.printStackTrace();
			status = STATUS_EXECUTION_ERROR;
		}
		catch (Throwable e)
		{
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add(result);
			System.out.println("Throwable: "+e);
			e.printStackTrace();
			status = STATUS_EXECUTION_ERROR;
		}
		finally
		{
			setSignalTower(digitalIOCard, !resultIDsRes.bTest2OnceSuc);
			// wait for end of car
			if(bWaitForEndOfCar && (null!=digitalIOCard))
			{
				if(0==lMilliesStart)
					lMilliesStart = System.currentTimeMillis();
				if(null != myDialog)
					myDialog.displayMessage( "test", PB.getString( "pruefprozedur.waitForEndOfVehicle" ), -1 );
				int status2 = waitForEndOfCar(lTimeout, lMilliesStart, digitalIOCard,myDialog,ergListe, bDEBUG);
				if(STATUS_EXECUTION_OK == status)
					status = status2; // waitForEndOfCar must not set status to ok
			}
			//--- ADD RESULTS TO ERGLIST ---

			ergListe.add(new Ergebnis( "SENSOR_ID_VL", "TPM", "", "", "", "SENSOR_ID_VL",resultIDsRes.idvl.id, "", "", "0", "", "", "",
					"", "", checkResultOk(bHasRDC,resultIDsRes.idvl.id)?Ergebnis.FT_IO:Ergebnis.FT_NIO ));
			ergListe.add(new Ergebnis( "SENSOR_ID_VR", "TPM", "", "", "", "SENSOR_ID_VR",resultIDsRes.idvr.id, "", "", "0", "", "", "",
					"", "", checkResultOk(bHasRDC,resultIDsRes.idvr.id)?Ergebnis.FT_IO:Ergebnis.FT_NIO ));
			ergListe.add(new Ergebnis( "SENSOR_ID_HL", "TPM", "", "", "", "SENSOR_ID_HL",resultIDsRes.idhl.id, "", "", "0", "", "", "",
					"", "", checkResultOk(bHasRDC,resultIDsRes.idhl.id)?Ergebnis.FT_IO:Ergebnis.FT_NIO ));
			ergListe.add(new Ergebnis( "SENSOR_ID_HR", "TPM", "", "", "", "SENSOR_ID_HR",resultIDsRes.idhr.id, "", "", "0", "", "", "",
					"", "", checkResultOk(bHasRDC,resultIDsRes.idhr.id)?Ergebnis.FT_IO:Ergebnis.FT_NIO ));

			String szResultIds =  buildResString(resultIDsRes);

			// log result <
			if(bDEBUG) System.out.println("RDC*******************************************************************");	      
			if(0<szLogPrefix.length())
				System.out.println(szLogPrefix);
			System.out.println((bHasRDC ? "HAS_RDC" : "NON_RDC") + " test completed: " + szResultIds);
			if(bDEBUG) System.out.println("RDC*******************************************************************");
			// log result />

			if((null != myDialog)&&(0<nShowResultDuration))
				myDialog.requestUserMessage(bHasRDC?"RDC":"no RDC"+": Sensor Ids", szResultIds, nShowResultDuration);

			try
			{
				// release objects
				if(null != myDialog)
				{
					myDialog.close();
					getPr�flingLaufzeitUmgebung().releaseUserDialog();
				}

				if (null != tpmDevice)
				{

					if(bDevmanagerReleaseTpm)
					{
						if (null != devManager)
						{
							devManager.releaseTpmDevice();
							if (bDEBUG) System.out.println("tpmDevice released");
						}
					}
					else
					{
						tpmDevice.close();
						if (bDEBUG) System.out.println("tpmDevice closed");        	  
					}
					tpmDevice = null;
				}

				if (null != digitalIOCard)
				{
					if ((null != devManager) && bDevmanagerReleaseDigIO )
						devManager.releaseRelayControl();
					//else
					//digitalIOCard.dispose();
					digitalIOCard = null;
					if(bDEBUG) System.out.println("digitalIO released");
				}
			}
			catch(Exception e)
			{
				System.out.println("Exception in finally: release objects: "+e.getMessage());
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception:"+e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add(result);
				status = STATUS_EXECUTION_ERROR;
			}
		}
		if (bMemDEBUG) {
			System.out.println("RDC MemDebug: NumberThreads at end - "+Thread.activeCount());
			System.out.println("RDC MemDebug: Free Memory at end - "+Runtime.getRuntime().freeMemory());

			dumpThreads();      
		}

		setPPStatus( info, status, ergListe );
		//System.out.println("RDC_12_0_F_Pruefprozedur complete");
	}

	/** Schickt die Kommandos direkt �ber das SerialGeneric device
	 * 
	 * @param tpmDevice TpmDevice von Device Manager
	 * @param sParam Wert f�r Kommando
	 * @param command Kommando zum Setzen des Wertes
	 * @throws CError Fehler (enth�lt Cascade result "Ergebnis")
	 */
	private void setTpmDeviceParams(com.bmw.cascade.pruefstand.devices.tpm.TpmDevice tpmDevice, String sParam, String command, boolean bIgnoreSendParamsFail) throws CError 
	{
		if (bDEBUG) System.out.println("sending Parameters ...");

		String sReturnString = "";
		CError err = null;
		try
		{
			boolean sendParamPass = false;
			CError errLast = null;
			//send commands
			for(int i=0;i<3;i++)
			{
				errLast = null;
				try
				{
					sReturnString = command_execute(tpmDevice, "cpedit", false, TIMEOUT_SERIAL);
					sReturnString = command_execute(tpmDevice, command+sParam, true, TIMEOUT_SERIAL);
				}
				catch(CError err_)
				{
					errLast = err_;
					sReturnString="";
				}
				if (sReturnString.indexOf("Change is active")==-1) 
				{
					System.out.println("WARNING: retry to send " + command + " ...");
					try
					{
						Thread.sleep(25);
					}
					catch (InterruptedException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{				  
					sendParamPass = true;
					break;
				}
			}
			if(sendParamPass)
			{
				if (bDEBUG) System.out.println("set "+ command +" suc");
			}
			else if(bIgnoreSendParamsFail)
			{
				System.out.println("WARNING: failed to set Gen_filter IGNORE_SEND_PARAMS_FAIL=TRUE !!!");
			}
			else
			{
				if(null != errLast)
					throw errLast;
				if (isEnglish) 
					throw new CError("SerialGenericError", "Error setting RDC " + command, "Answer from device is: "+sReturnString, Ergebnis.FT_NIO_SYS);
				else 
					throw new CError("SerialGenericError", "Fehler beim Setzen des RDC " + command, "Antwort vom Ger�t ist: "+sReturnString, Ergebnis.FT_NIO_SYS);
			}

		}
		catch (CError e)
		{
			err=e;
		}

		if (err!=null) throw err;

		if (bDEBUG) System.out.println("... Parameters sent");
	}

	/**
	 */
	String buildResString(ResultIDs resultIDsRes)
	{
		String szResultIds =  
				"idvl:"+resultIDsRes.idvl.id+" g="+resultIDsRes.idvl.gen+"; "
						+"idvr:"+resultIDsRes.idvr.id+" g="+resultIDsRes.idvr.gen+"; "
						+"idhl:"+resultIDsRes.idhl.id+" g="+resultIDsRes.idhl.gen+"; "
						+"idhr:"+resultIDsRes.idhr.id+" g="+resultIDsRes.idhr.gen+"; ";
		return szResultIds;
	}
	/**
	 * get sensor ids, return true if test2 was once executed
	 * retry test if any tire not found or any backTire is found as frontTire ("9999999999")
	 * @param lTimeout timeout
	 * @param lMaxRetryStartTest max number to start test on Tpm-Device
	 * @param tpmDevice tpm-Device
	 * @param digitalIOCard Digital IO card
	 * @param resultIDs output, Sensor-ids
	 * @param ergListe List of results
	 * @param bDEBUG
	 */
	private boolean getSensorIds( boolean bHasRDC,  long lTimeout,long lMilliesStart, long lMaxRetryStartTest,
			boolean bAssignBackToFront, boolean bOverwriteCrossTalk,
			com.bmw.cascade.pruefstand.devices.tpm.TpmDevice tpmDevice,
			RelayControl digitalIOCard,
			com.bmw.cascade.pruefstand.visualisierung.UserDialog myDialog,
			ResultIDs resultIDsRes,Vector ergListe,
			boolean bDEBUG, boolean bIgnoreNoRDCInterference, int nGenFilter) throws Exception
			{    
		boolean bComplete = false;
		boolean bTestRunning = false;
		long lCntStartTest = 0;

		resultIDsRes.bTest2OnceSuc = false;
		boolean bRet = false;

		try
		{
			while(!bComplete)
			{
				lCntStartTest = 0;
				bTestRunning = false;
				// start Test
				while(!bComplete && (false == (bTestRunning = tpmDevice.startTest())))
				{
					long lMilliesCheckForAbort = System.currentTimeMillis();
					lCntStartTest++;
					if((lCntStartTest > lMaxRetryStartTest) && (lMaxRetryStartTest > 0))
					{
						// max retry
						if (bDEBUG) System.out.println("PP_RDC: Stop startTestLoop due to max retry start");
						ergListe.add(new Ergebnis( 	"DeviceError", "TPM", "", "", "",
								"MAX_RETRY_STARTTEST", "", "", "",
								""+lCntStartTest, "", "", "",
								PB.getString("pruefprozedur.maxRetryStartTest"), "StartTest2",Ergebnis.FT_NIO_SYS ));
						throw new PPExecutionException();
					}
					else
					{
						while(!bComplete && ((System.currentTimeMillis()-lMilliesCheckForAbort)<checkForAbortTestTime))
						{
							bComplete = checkForAbortTest(bHasRDC,digitalIOCard,myDialog,lTimeout,lMilliesStart,ergListe);
							if(!bComplete)
							{
								Thread.sleep(1);
							}
						}
					}
				}

				// wait for test complete, get state
				while(bTestRunning && !bComplete)
				{
					long lMilliesCheckForAbort = System.currentTimeMillis();
					while(!bComplete && ((System.currentTimeMillis()-lMilliesCheckForAbort)<checkForAbortTestTime))
					{
						bComplete = checkForAbortTest(bHasRDC,digitalIOCard,myDialog,lTimeout,lMilliesStart,ergListe);
						Thread.sleep(1);
					}
					if(false == bComplete)
					{
						if(bDEBUG)
							System.out.println("PP_RDC: check test isInProgress");
						try
						{
							//-------------------------------------
							bTestRunning = tpmDevice.isInProgress();
							//-------------------------------------
						}
						catch(Exception e)
						{
							System.out.println("PP_RDC: Exception: isInProgress: "+e);
							ergListe.add(new Ergebnis( 	"DeviceError", "TPM", "", "", "",
									"FAILED_GET_INPROGRESS", "", "", "",
									""+lCntStartTest, "", "", "",
									PB.getString("pruefprozedur.failedGetInProgress"), "isInProgress",Ergebnis.FT_NIO_SYS ));
							throw new PPExecutionException();
						}
						if(false == bTestRunning)
						{
							ResultIDs idsLast = new ResultIDs();
							idsLast.idvl.id = tpmDevice.getId("idvl");
							idsLast.idvr.id = tpmDevice.getId("idvr");
							idsLast.idhl.id = tpmDevice.getId("idhl");
							idsLast.idhr.id = tpmDevice.getId("idhr");

							try
							{
								idsLast.idvl.gen = tpmDevice.getGen("idvl");
								idsLast.idvr.gen = tpmDevice.getGen("idvr");
								idsLast.idhl.gen = tpmDevice.getGen("idhl");
								idsLast.idhr.gen = tpmDevice.getGen("idhr");
							}
							catch(Throwable th) // Throwable: java.lang.NoSuchMethodError
							{
								// Christian Moosbauer: 
								// Throwable wird abgefangen um alte cascade version zu unterst�tzen,
								// in der tpmDevice.getGen noch nicht existiert
								if(bDEBUG)
								{
									System.out.println("PP_RDC: tpmDevice.getGen Throwable: java.lang.NoSuchMethodError !!! => set gen to -1");
								}
								idsLast.idvl.gen = -1;
								idsLast.idvr.gen = -1;
								idsLast.idhl.gen = -1;
								idsLast.idhr.gen = -1;
							}
							checkStateSetResult(bHasRDC, bAssignBackToFront, bOverwriteCrossTalk, idsLast, resultIDsRes, bDEBUG, bIgnoreNoRDCInterference, nGenFilter);

							resultIDsRes.bTest2OnceSuc = true; // tpm device hat test2 min 1x ausgef�hrt

							// wenn alle ids ok => fertig
							if(checkResultsComplete(bHasRDC,resultIDsRes))
							{
								if(bDEBUG)
									System.out.println("PP_RDC: checkResults ok => complete");
								bComplete = true;
								bRet = true;
							}
							else if(null != myDialog)
							{
								myDialog.displayMessage( "test - "+PB.getString( "pruefprozedur.getSensorIds" ), buildResString(resultIDsRes), -1 );
							}
							else
							{
								System.out.println("PP_RDC:" + buildResString(resultIDsRes));
							}
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			throw (e);
		}
		finally
		{
			if(bDEBUG)
				System.out.println("PP_RDC: checkResults bTest2OnceSuc="+resultIDsRes.bTest2OnceSuc);
			try
			{
				tpmDevice.cancel();
			}
			catch(Exception e)
			{
				ergListe.add(new Ergebnis( 	"DeviceError", "TPM", "", "", "",
						"FAILED_TO_STOP_TEST2", "", "", "",
						""+lCntStartTest, "", "", "",
						PB.getString("pruefprozedur.failedStopTest"), "StopTest2",Ergebnis.FT_NIO_SYS ));
				throw new PPExecutionException();
			}      
		}

		return bRet;
			}

	/**
	 *  set light tower signals
	 *  @param digitalIOCard Digital IO card
	 *  @param bError true => red, false => green
	 */
	private void setSignalTower(RelayControl digitalIOCard, boolean bError)
	{
		if(null != digitalIOCard)
		{
			int[] writeBitsOn = new int[1];
			int[] writeBitsOff = new int[1];
			if(bError)
			{
				writeBitsOn[0] = OUTBIT_LIGHT_RED;
				writeBitsOff[0] = OUTBIT_LIGHT_GREEN;
			}
			else
			{
				writeBitsOn[0] = OUTBIT_LIGHT_GREEN;
				writeBitsOff[0] = OUTBIT_LIGHT_RED;
			}
			if(null != digitalIOCard)
			{
				try
				{
					digitalIOCard.setOn(writeBitsOn);
					digitalIOCard.setOff(writeBitsOff);
				}
				catch(DeviceIOException e)
				{
					System.out.println("PP_RDC: Error set signaltower:"+e);
				}
			}
		}
	}
	/**
	 * Check if the test has to be aborted
	 * - end of car (Input 2)
	 * - timeout
	 * - cancelled by by user
	 *  @param digitalIOCard Digital IO card
	 *  @param myDialog Userdialog
	 *  @param lTimeout timeout
	 *  @param lMilliesStart miliseconds since start of test
	 *  @param ergListe List of results
	 */
	private boolean checkForAbortTest(boolean bHasRDC, RelayControl digitalIOCard,
			com.bmw.cascade.pruefstand.visualisierung.UserDialog myDialog,
			long lTimeout, long lMilliesStart,Vector ergListe) throws Exception
			{
		boolean bComplete = false;
		int[] readBits = new int[1];
		readBits[0] = INBIT_END_OF_CAR;
		boolean[] bBits = null;
		if(null != digitalIOCard)
		{
			bBits = digitalIOCard.read(readBits);
			if(bBits[0])
			{
				if(bHasRDC)
				{
					if (bDEBUG) System.out.println("PP_RDC: Stop loop: Bit 2 HIGH");
					ergListe.add(new Ergebnis( 	"END_OF_CAR", "DigitalIO", "", "", "",
							"END_OF_CAR", "true", "false", "",
							"0", "", "", "",
							PB.getString("pruefprozedur.endOfCar"), "2",Ergebnis.FT_NIO_SYS ));
					throw new PPExecutionException();
				}
				else
				{
					// only stop, no error
					bComplete = true;
				}
			}
		}
		else if((null != myDialog) && myDialog.isCancelled())
		{
			if (bDEBUG) System.out.println("PP_RDC: Stop loop: user cancelled");
			ergListe.add(new Ergebnis( 	"USER_CANCELLED", "", "", "", "",
					"USER_CANCELLED", "", "", "",
					"0", "", "", "",
					PB.getString("pruefprozedur.abortOperator"), "",Ergebnis.FT_NIO_SYS ));
			throw new PPExecutionException();
		}
		else if((0<lTimeout)&&(lTimeout < (System.currentTimeMillis()-lMilliesStart)))
		{
			if(bHasRDC)
			{
				if (bDEBUG) System.out.println("PP_RDC: Stop loop due to timeout");
				ergListe.add(new Ergebnis( 	"TIMEOUT", "", "", "", "",
						"TIMEOUT", ""+lTimeout, "", "",
						"0", "", "", "",
						PB.getString("pruefprozedur.timeout"), "",Ergebnis.FT_NIO_SYS ));
				throw new PPExecutionException();
			}
			else
			{
				bComplete = true;
			}        
		}
		return bComplete;
			}
	/**
	 * Check if sensor ids returned by Tpm device are Ok and set restult object
	 * "0000000000" : id not found
	 * "9999999999" : more than one id found for a tire
	 * @param ts2 sensor ids returned by Tpm-Device
	 * @param resultIDs Output, sensor ids assigned by this PP
	 */
	private void checkStateSetResult(	boolean bHasRDC,
			boolean bAssignBackToFront,
			boolean bOverwriteCrossTalk,
			ResultIDs ts2,
			ResultIDs resultIDs,
			boolean bDEBUG,
			boolean bIgnoreNoRDCInterference,
			int nGenFilter)
	{
		if(bHasRDC)
		{
			boolean checkGen = (nGenFilter > 0);
			// front tires: 
			// false==bAssignBackToFront: if back is found and front not => don�t set back as front.
			// front sensores may not exist !!!
			//LINKS
			if((0==resultIDs.idvl.id.length())||resultIDs.idvl.id.equals("0000000000")||(bOverwriteCrossTalk && resultIDs.idvl.id.equals("9999999999")))
			{
				RID szTmp = new RID();
				szTmp.id = "0000000000";

				if(((0 != ts2.idvl.id.length()) && !ts2.idvl.id.equals("0000000000")))
				{ // front to front
					szTmp = ts2.idvl;
				}
				//20120621/dbb: Copy&Paste error
				else if( bAssignBackToFront &&  (0 != ts2.idhl.id.length()) && !ts2.idhl.id.equals("0000000000"))
				{ // back to front
					szTmp = ts2.idhl;
				}
				else if(0 != ts2.idvl.id.length())
				{ // e.g.: "00000000"
					szTmp = ts2.idvl;
				}

				if( (0<szTmp.id.length()) && 
						( !resultIDs.idvl.id.equals("9999999999") 
								|| (resultIDs.idvl.id.equals("9999999999") && !szTmp.id.equals("0000000000"))) )
				{ // do not overwrite 9999999999 with 0000000000
					if((checkGen && (nGenFilter==szTmp.gen))||!checkGen)
					{
						if(bDEBUG)
							System.out.println("idvl="+resultIDs.idvl.id+"->"+szTmp.id);
						resultIDs.idvl = szTmp;
					}
				}        
			}
			//RECHTS      
			if((0==resultIDs.idvr.id.length())||resultIDs.idvr.id.equals("0000000000")||(bOverwriteCrossTalk && resultIDs.idvr.id.equals("9999999999")))
			{
				RID szTmp = new RID();
				szTmp.id = "0000000000";

				if(((0 != ts2.idvr.id.length()) && !ts2.idvr.id.equals("0000000000")))
				{ // front to front
					szTmp = ts2.idvr;
				}
				else if( bAssignBackToFront &&  (0 != ts2.idhr.id.length()) && !ts2.idhr.id.equals("0000000000"))
				{ // back to front
					szTmp = ts2.idhr;
				}
				else if(0 != ts2.idvr.id.length())
				{ // e.g.: "00000000"
					szTmp = ts2.idvr;
				}

				if( (0<szTmp.id.length()) && 
						( !resultIDs.idvr.id.equals("9999999999") 
								|| (resultIDs.idvr.id.equals("9999999999") && !szTmp.id.equals("0000000000"))) )
				{ // do not overwrite 9999999999 with 0000000000
					if((checkGen && (nGenFilter==szTmp.gen))||!checkGen)
					{
						if(bDEBUG)
							System.out.println("idvr="+resultIDs.idvr.id+"->"+szTmp.id);
						resultIDs.idvr = szTmp;
					}
				}        
			}
			// if second run, front tires may be back
			if(((0==resultIDs.idhl.id.length())||resultIDs.idhl.id.equals("0000000000")||(bOverwriteCrossTalk && resultIDs.idhl.id.equals("9999999999")))
					&& checkResultOk(true, resultIDs.idvl.id)
					)
			{
				RID szTmp = new RID();
				szTmp.id = "0000000000";

				if(checkResultOk(true,ts2.idhl.id) && !ts2.idhl.id.equals(resultIDs.idvl.id))
					szTmp = ts2.idhl;
				else if(checkResultOk(true,ts2.idvl.id) && !ts2.idvl.id.equals(resultIDs.idvl.id))// check if front is back
					szTmp = ts2.idvl;
				else if(ts2.idhl.id.equals("9999999999"))
					szTmp = ts2.idhl;

				if( (0<szTmp.id.length()) && 
						( !resultIDs.idhl.id.equals("9999999999") 
								|| (resultIDs.idhl.id.equals("9999999999") && !szTmp.id.equals("0000000000"))) )
				{ // do not overwrite 9999999999 with 0000000000
					if((checkGen && (nGenFilter==szTmp.gen))||!checkGen)
					{
						if(bDEBUG)
							System.out.println("idhl="+resultIDs.idhl.id+"->"+szTmp.id);
						resultIDs.idhl = szTmp;
					}
				}
			}
			if(((0==resultIDs.idhr.id.length())||resultIDs.idhr.id.equals("0000000000")||(bOverwriteCrossTalk && resultIDs.idhr.id.equals("9999999999")))
					&& checkResultOk(true, resultIDs.idvr.id)
					)
			{
				RID szTmp = new RID();
				szTmp.id = "0000000000";

				if(checkResultOk(true,ts2.idhr.id) && !ts2.idhr.id.equals(resultIDs.idvr.id))
					szTmp = ts2.idhr;
				else if(checkResultOk(true,ts2.idvr.id) && !ts2.idvr.id.equals(resultIDs.idvr.id))// check if front is back
					szTmp = ts2.idvr;
				else if(ts2.idhr.id.equals("9999999999"))
					szTmp = ts2.idhr;

				if( (0<szTmp.id.length()) && 
						( !resultIDs.idhr.id.equals("9999999999") 
								|| (resultIDs.idhr.id.equals("9999999999") && !szTmp.id.equals("0000000000"))) )
				{ // do not overwrite 9999999999 with 0000000000
					if((checkGen && (nGenFilter==szTmp.gen))||!checkGen)
					{
						if(bDEBUG)
							System.out.println("idhr="+resultIDs.idhr.id+"->"+szTmp.id);
						resultIDs.idhr = szTmp;
					}
				}
			}
		}
		else // NO RDC - if Interference "9999999999" --> "0000000000"
		{
			RID szTmp = new RID();
			szTmp.id = "0000000000";
			if((0==resultIDs.idvl.id.length()) || resultIDs.idvl.id.equals("0000000000"))
			{
				if(ts2.idvl.id.equals("9999999999") && bIgnoreNoRDCInterference)
				{
					ts2.idvl = szTmp;
					if(bDEBUG) System.out.println("idvl=Interference");
				}
				if(bDEBUG) System.out.println("idvl="+resultIDs.idvl.id+"->"+ts2.idvl.id);
				resultIDs.idvl = ts2.idvl;
			}
			if((0==resultIDs.idvr.id.length()) || resultIDs.idvr.id.equals("0000000000"))
			{
				if(ts2.idvr.id.equals("9999999999") && bIgnoreNoRDCInterference)
				{
					ts2.idvr = szTmp;
					if(bDEBUG) System.out.println("idvr=Interference");
				}		
				if(bDEBUG) System.out.println("idvr="+resultIDs.idvr.id+"->"+ts2.idvr.id);
				resultIDs.idvr = ts2.idvr;
			}

			if((0==resultIDs.idhl.id.length()) || resultIDs.idhl.id.equals("0000000000"))
			{
				if(ts2.idhl.id.equals("9999999999") && bIgnoreNoRDCInterference)
				{
					ts2.idhl = szTmp;
					if(bDEBUG) System.out.println("idhl=Interference");
				}
				if(bDEBUG) System.out.println("idhl="+resultIDs.idhl.id+"->"+ts2.idhl.id);
				resultIDs.idhl = ts2.idhl;
			}
			if((0==resultIDs.idhr.id.length()) || resultIDs.idhr.id.equals("0000000000"))
			{
				if(ts2.idhr.id.equals("9999999999") && bIgnoreNoRDCInterference)
				{
					ts2.idhr = szTmp;
					if(bDEBUG) System.out.println("idhr=Interference");
				}
				if(bDEBUG) System.out.println("idhr="+resultIDs.idhr.id+"->"+ts2.idhr.id);
				resultIDs.idhr = ts2.idhr;
			}
		}
	}
	/**
	 * check if all sensor ids are valid (or 999999999)
	 * @param resultIDs Output, sensor ids assigned by this PP
	 */
	private boolean checkResultsComplete(boolean bHasRDC, ResultIDs resultIDs)
	{
		boolean bRet = true;

		if( /*(0==resultIDs.idvl.length()) || resultIDs.idvl.id.equals("0000000000")
    ||  (0==resultIDs.idvr.length()) || resultIDs.idvr.id.equals("0000000000") end trigger if backSensores found
    ||*/  (0==resultIDs.idhl.id.length()) || resultIDs.idhl.id.equals("0000000000") || resultIDs.idhl.id.equals("9999999999")
    ||  (0==resultIDs.idhr.id.length()) || resultIDs.idhr.id.equals("0000000000") || resultIDs.idhr.id.equals("9999999999")
				)
		{
			bRet = false;
		}
		return bRet;
	}
	/* check if single sensor id is ok
	 **/
	private boolean checkResultOk(boolean bHasRDC, String result)
	{
		boolean bRet = true;
		if(bHasRDC)
		{
			if( (0==result.length())
					||  (0 <= result.indexOf("0000000000"))
					||  (0 <= result.indexOf("9999999999")) )
			{
				bRet = false;
			}
		}
		else
		{
			if( (0==result.length()) || (!result.equals("0000000000")) )
				bRet = false;
		}
		return bRet;
	}
	/* check if all sensor ids are ok
	 **/
	private boolean checkResultsOk(boolean bHasRDC, ResultIDs resultIDs, int nGenFilter, boolean bDebug)
	{
		boolean bRet = true;

		if(     !checkResultOk(bHasRDC, resultIDs.idvl.id)
				||  !checkResultOk(bHasRDC, resultIDs.idvr.id)
				||  !checkResultOk(bHasRDC, resultIDs.idhl.id)
				||  !checkResultOk(bHasRDC, resultIDs.idhr.id) )
		{
			if (bDebug) System.out.println("PP_RDC: checkResultsOk, NIO because of sensorID value");
			bRet = false;
		}
		else if(bHasRDC && (0 < nGenFilter))
		{
			if((nGenFilter != resultIDs.idvl.gen)
					|| (nGenFilter != resultIDs.idvr.gen)
					|| (nGenFilter != resultIDs.idhl.gen)
					|| (nGenFilter != resultIDs.idhr.gen))
			{
				if (bDebug) System.out.println("PP_RDC: checkResultsOk, NIO because of sensorGen value");
				bRet = false;
			}
		}

		if(bRet && bDebug) System.out.println("PP_RDC: checkResultsOk, IO");

		return bRet;
	}

	/* check if all sensor ids are different
	 **/
	private boolean checkResultsPlausible(boolean bHasRDC,ResultIDs resultIDs)
	{
		boolean bRet = true;
		if(bHasRDC)
		{
			if( (resultIDs.idvl.id.equals(resultIDs.idvr.id))
					||(resultIDs.idvl.id.equals(resultIDs.idhl.id))
					||(resultIDs.idvl.id.equals(resultIDs.idhr.id))

					||(resultIDs.idvr.id.equals(resultIDs.idhr.id))
					||(resultIDs.idvr.id.equals(resultIDs.idhl.id))

					||(resultIDs.idhl.id.equals(resultIDs.idhr.id))
					)
			{
				bRet = false;
			}
		}
		return bRet;
	}
	/**
	 * wait for car, DigitalIOCard input 1
	 */
	private void waitForCar(RelayControl digitalIOCard,
			com.bmw.cascade.pruefstand.visualisierung.UserDialog myDialog,
			Vector ergListe,
			boolean bStartSignalEdge,
			boolean bDebug) throws Exception
			{
		boolean bPrvBit1=false;
		int[] readBits = new int[2];
		readBits[0] = INBIT_BEGIN_OF_CAR;
		readBits[1] = INBIT_END_OF_CAR;
		boolean[] bBits = null;
		try
		{
			while(true)
			{
				bBits = digitalIOCard.read(readBits);
				if(bBits[1])
				{
					if (bDebug) System.out.println("PP_RDC: Stop waiting for car: Bit 2 HIGH (EndOfCar)");
					ergListe.add(new Ergebnis( 	"END_OF_CAR", "DigitalIO", "", "", "",
							"END_OF_CAR", "true", "false", "",
							"0", "", "", "",
							PB.getString("pruefprozedur.endOfCar"), "2",Ergebnis.FT_NIO_SYS ));
					throw new PPExecutionException();
				}
				else if( (bBits[0] && (!bPrvBit1) && bStartSignalEdge)// positive edge
						|| ( (!bBits[0]) && bPrvBit1 && (!bStartSignalEdge)) ) // negative edge
				{
					if(bDebug)
						System.out.println("waitForCar(InputBit 1) suc: StartSignalEdge="+bStartSignalEdge);
					break;
				}
				else
				{
					Thread.sleep(100);
					if(null != myDialog)
					{
						if(myDialog.isCancelled())
						{
							if (bDebug) System.out.println("waitForCar(InputBit 1) cancelled by user.");
							break;
						}
					}
				}
				bPrvBit1 = bBits[0];
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception: "+e);
			e.printStackTrace();

			throw(e);
		}
			}
	/**
	 * wait for end of car, DigitalIOCard input 1
	 */
	private int waitForEndOfCar(long lTimeout, long lMilliesStart,
			RelayControl digitalIOCard,
			com.bmw.cascade.pruefstand.visualisierung.UserDialog myDialog,
			Vector ergListe, 
			boolean bDebug)
	{
		boolean bPrvBit2 = false;
		int[] readBits = new int[2];
		readBits[0] = INBIT_BEGIN_OF_CAR; // start
		readBits[1] = INBIT_END_OF_CAR; // stop
		boolean[] bBits = null;
		try
		{
			while(true)
			{
				bBits = digitalIOCard.read(readBits);
				if(bPrvBit2 && bBits[0])
				{
					if (bDebug) System.out.println("PP_RDC: waitForEndOfCar: start high && stop high");
					ergListe.add(new Ergebnis( 	"SENSOR_ERROR", "", "", "", "",
							"SENSOR_ERROR", "start-stop", "", "",
							"0", "", "", "",
							PB.getString("pruefprozedur.sensorError"), "",Ergebnis.FT_NIO_SYS ));
					return STATUS_EXECUTION_ERROR;
				}

				if(!bBits[1] && bPrvBit2)
				{
					//System.out.println("PP_RDC: Stop waiting for end of car: neg edge Bit 2 (EndOfCar)");
					break;
				}
				else if((0<lTimeout)&&(lTimeout < (System.currentTimeMillis()-lMilliesStart)))
				{
					if (bDebug) System.out.println("PP_RDC: Stop loop due to timeout");
					ergListe.add(new Ergebnis( 	"TIMEOUT", "", "", "", "",
							"TIMEOUT", ""+lTimeout, "", "",
							"0", "", "", "",
							PB.getString("pruefprozedur.timeout"), "",Ergebnis.FT_NIO_SYS ));
					return STATUS_EXECUTION_ERROR;
				}
				else
				{
					Thread.sleep(10);
					if((null != myDialog) && (myDialog.isCancelled()))
					{
						System.out.println("waitForEndOfCar(InputBit 2) cancelled by user.");
						break;
					}
				}
				bPrvBit2 = bBits[1];
			}
		}
		catch (Exception e)
		{
			ergListe.add(new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS ));
			System.out.println("Exception: "+e);
			e.printStackTrace();

			return STATUS_EXECUTION_ERROR;
		}
		catch (Throwable e)
		{
			ergListe.add(new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS ));
			System.out.println("Throwable: "+e);
			e.printStackTrace();
			return STATUS_EXECUTION_ERROR;
		}
		return STATUS_EXECUTION_OK;
	}

	/** F�hrt ein Kommando des Handterminals aus
	 * 
	 * @param mySerialDevice serielle Schnittstelle
	 * @param command Kommandoname
	 * @param Timeout Timeout 
	 * @return Eingelesene Bytes
	 * @throws CError Fehlerobjekt
	 */
	private String command_execute(com.bmw.cascade.pruefstand.devices.tpm.TpmDevice tpmDevice, String command, boolean waitForAnswer, int Timeout) throws CError
	{
		String ret = "";      
		if (m_debug_level>5) System.out.println("COMMAND \""+command+"\"");

		try
		{
			tpmDevice.sendCommand(command+"\r");
		}
		catch (Exception ex)
		{
			if (!isEnglish) throw new CError( "ExecFehler", "SerialWrite fehlgeschlagen", ex.getMessage(), Ergebnis.FT_NIO);
			else            throw new CError( "ExecFehler", "SerialWrite failed", ex.getMessage(), Ergebnis.FT_NIO); 
		}

		if(waitForAnswer)
		{
			if (m_debug_level>7) System.out.println("COMMAND sent, now reading");

			try
			{
				StringBuffer sbAnswer = new StringBuffer();
				if(!tpmDevice.waitForAnswer(">", Timeout, sbAnswer))
				{
					if (!isEnglish) throw new CError("ExecFehler", "SerialRead fehlgeschlagen", "Timeout", Ergebnis.FT_NIO);
					else            throw new CError("ExecFehler", "SerialRead failed", "Timeout", Ergebnis.FT_NIO);
				}
				else
				{
					ret = sbAnswer.toString();
					if (m_debug_level>7) System.out.println("COMMAND read finished");
					if (m_debug_level>12)
					{
						System.out.println("Complete Buffer:");
						System.out.println(ret);
					}
				}
			}
			catch (Exception ex)
			{
				if (!isEnglish) throw new CError( "ExecFehler", "SerialRead fehlgeschlagen", ex.getMessage(), Ergebnis.FT_NIO);
				else            throw new CError( "ExecFehler", "SerialRead failed", ex.getMessage(), Ergebnis.FT_NIO); 
			}
		}	
		return ret;  
	}

	/** Enumerate all Threads and print them */
	private void dumpThreads()
	{
		Thread[] thrds;
		int nr;
		int i;

		thrds=new Thread[Thread.activeCount()+10];
		nr=Thread.enumerate(thrds);

		System.out.println("--------------------------");
		System.out.println("Current Threads ("+nr+"):");

		for (i=0; i<nr; i++)
		{
			System.out.println(""+i+": "+thrds[i].getName());
		}
	}

	/** Fehlerobjekt  */
	private class CError extends Exception
	{
		/** */
		private static final long serialVersionUID=-5654634010303049207L;
		/** Cascadeergebnis */
		Ergebnis m_entry;

		/** Konstruktor (Fehlertyp ist FT_NIO)
		 * 
		 * @param ID FehlerID
		 * @param Fehlertext Fehlertext
		 * @param Hinweistext Hinweistext
		 */
		/*
      public CError(String ID, String Fehlertext, String Hinweistext)
      {
          m_entry=new Ergebnis(ID, "", "", "", "", "", "", "", "", "0", "", "", "", Fehlertext, Hinweistext, Ergebnis.FT_NIO );                      
      }
		 */
		/** Konstruktor 
		 * 
		 * @param ID FehlerID
		 * @param Fehlertext Fehlertext
		 * @param Hinweistext Hinweistext
		 * @param ErrorType FehlerTyp (z.B. Ergebnis.FT_NIO)
		 */
		public CError(String ID, String Fehlertext, String Hinweistext, String ErrorType)
		{
			m_entry=new Ergebnis(ID, "", "", "", "", "", "", "", "", "0", "", "", "", Fehlertext, Hinweistext, ErrorType );
		}

		/** Liefert das CascadeErgebnis 
		 * 
		 * @return Cascadeergebnis
		 */
		public Ergebnis getEntry()
		{
			return m_entry;
		}
	}

	//-----
}

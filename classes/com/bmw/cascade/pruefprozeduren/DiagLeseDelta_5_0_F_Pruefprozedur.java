package com.bmw.cascade.pruefprozeduren;

import java.util.StringTokenizer;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;

/**
 * Implementierung der Pr�fprozedur, die zu einem Zeitpunkt x (Zustand=0) per Diagnose den Fehlerspeicher 
 * ausliest und diese bis dahin aufgetretenen Fehler in einer Pr�fstandsvariablen speichert. Zum Zeitpunkt y
 * (Zustand=1) wird der aktuell aufgetretene FS mit dem vom Zeitpunkt x verglichen. Alle FS-Eintr�ge, die 
 * neu hinzugekommen sind werden nur gewertet.
 * Zus�tzlich gibt es eine Ignore-Liste mit FS-Eintr�gen. Diese FS-Eintr�ge werden ignoriert und nicht nach
 * APDM gesendet, wenn sie beim Zustand=1 auftreten.
 * PP funktioniert f�r UDS-SGs und auch �ltere SG-Typen!
 * @author C. Schumann, C. Wolf BMW AG
 * @version 1_0_F 27.10.08 CS Implementierung der Anforderungen. <br>
 * @version 2_0_F 07.04.11 CW Namen der Pr�fstandsvariabele "FS_Eintraege" ge�ndert auf "FS_Eintraege_'SGBD'". <br>
 * @version 3_0_T 07.04.11 TB Zusaetzlicher Debugmode ueber PS Variable, zur Fehleranalyse. <br>
 * @version 4_0_T 24.10.14 CW Zus�tzlicher Parameter DTC(enth�lt FS), ist dieser Parameter gesetzt so wird nur auf die 
 * 							  eingetragenen FS bei der Bewertung der zum Zeitpunkt y hinzugekommenen FS R�cksicht genommen;
 * 							  Zus�tzlicher Parameter DELETE_DTC, ist dieser auf true und unter DTC mind. ein FS gesetzt so wird 
 * 							  der FS der unter DTC parametriert wurde und beim Delta-Lesen entdeckt wurde, gel�scht; ist nur
 * 							  DELETE_DTC=true so werden alle Delta-DTCs gel�scht
 * @version 5_0_F 24.10.14 CW Freigabe 4_0_T
 */
public class DiagLeseDelta_5_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	static final long serialVersionUID = 1L;

	boolean bDebug = false; // Debugausgaben

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagLeseDelta_5_0_F_Pruefprozedur() {
	}

	/**
	 * Erzeugt eine neue Pruefprozedur.
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DiagLeseDelta_5_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente
	 * @return eine Liste sie optionale Argurmenten
	 */
	public String[] getOptionalArgs() {
		String[] args = { "JOB", "IGNORE", "DTC", "DELETE_DTC" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente
	 * @return eine Liste sie zwingende Argurmenten
	 */
	public String[] getRequiredArgs() {
		String[] args = { "SGBD", "ZUSTAND" };
		return args;
	}

	/**
	 * Pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 * @return true wenn Argumente iO sind sonst false
	 */
	public boolean checkArgs() {
		String temp;
		boolean ok;

		try {
			ok = super.checkArgs();
			if( ok == true ) {
				//SGBD
				temp = getArg( getRequiredArgs()[0] );
				if( temp == null )
					return false;
				//Zustand
				temp = getArg( getRequiredArgs()[1] );
				if( temp.equalsIgnoreCase( "0" ) || temp.equalsIgnoreCase( "1" ) ) {

				} else {
					return false;
				}
			}
			return ok;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String sgbd = null;
		String job = null;
		String zustand = null;
		String ignore = null;
		String dtc = null;
		boolean delete_dtc = false;
		EdiabasProxyThread myEdiabas = null;

		/***********************************************
		 * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
		 * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		 ***********************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		try {
			// Parameter holen / initialisieren
			try {
				// Parameter existenz
				if( checkArgs() == false )
					throw new PPExecutionException( "Parameterexistenz" );

				// DEBUG, �ber PS-Variable
				try {
					Object pr_var_debug;

					pr_var_debug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
					if( pr_var_debug != null ) {
						if( pr_var_debug instanceof String ) {
							if( "TRUE".equalsIgnoreCase( (String) pr_var_debug ) ) {
								bDebug = true;
							} else {
								bDebug = false;
							}
						} else if( pr_var_debug instanceof Boolean ) {
							if( ((Boolean) pr_var_debug).booleanValue() ) {
								bDebug = true;
							} else {
								bDebug = false;
							}
						}
					} else {
						bDebug = false;
					}

				} catch( VariablesException e ) {
					System.out.println( "DiagLeseDelta: Cannot read VariableModes.PSCONFIG, DEBUG, set Debug false" );
					bDebug = false;
				}

				// SGBD
				sgbd = extractValues( getArg( getRequiredArgs()[0] ) )[0];
				// Jobname
				job = getArg( getOptionalArgs()[0] );
				if( job == null )
					job = "FS_LESEN";
				// Zustand
				zustand = getArg( getRequiredArgs()[1] );
				// Ignorelist
				ignore = getArg( getOptionalArgs()[1] );
				// DTC List
				dtc = getArg( getOptionalArgs()[2] );
				System.out.println("dtc Wert " + dtc);
				// sollen die aufgetretenen DTCs gel�scht werden
				// wenn DELETE_DTC nicht parametriert wurde (null), dann ist delete_dtc=false
				delete_dtc = new Boolean( getArg( getOptionalArgs()[3] ));
				System.out.println("Wert von delete_dtc " + delete_dtc);

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			// Ausf�hrung
			try {
				if( bDebug )
					System.out.println( "PP DiagLeseDelta: Start of execution" );

				// neuer Weg Ediabas zu holen
				myEdiabas = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();

				// EDIABAS Job ausf�hren
				String temp = myEdiabas.executeDiagJob( sgbd, job, "", "" );
				if( temp.equals( "OKAY" ) == false ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} else {
					result = new Ergebnis( "Status", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				}
				//Zustand=0: FS lesen und in PS-Variable zwischenspeichern
				if( zustand.equalsIgnoreCase( "0" ) ) {
					if( bDebug )
						System.out.println( "PP DiagLeseDelta: Store the error memory entries into a tempory variable <FS_Eintraege_" + sgbd + ">" );

					String[] fsEintraege = new String[myEdiabas.getDiagJobSaetze() - 2];
					// Alle Fehlerort-Nummern (tempor�r) speichern
					for( int i = 1; i < (myEdiabas.getDiagJobSaetze() - 1); i++ ) {
						String fOrtNr = myEdiabas.getDiagResultValue( i, "F_ORT_NR" );
						fsEintraege[i - 1] = fOrtNr;
					}
					getPr�flingLaufzeitUmgebung().setPruefstandVariable( 3, "FS_Eintraege" + "_" + sgbd, fsEintraege );

					if( bDebug ) {
						for( int i = 0; i < fsEintraege.length; i++ )
							System.out.println( "PP DiagLeseDelta: Store the FS entries, Loop <" + i + ">, FS <" + fsEintraege[i] + ">" );
					}

					//Zustand=1: aktuellen FS mit den FS-Eintr�gen in der PS-Variable vgl. und nur
					//neue FS-Eintr�ge werten	
				} else {
					if( bDebug )
						System.out.println( "PP DiagLeseDelta: Get the FS entries from the tempory variable <FS_Eintraege_" + sgbd + ">" );

					String[] fsEintraege = (String[]) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 3, "FS_Eintraege" + "_" + sgbd );
					System.out.println("fsEintraege.length " + fsEintraege.length);
					System.out.println("myEdiabas.getDiagJobSaetze() " + myEdiabas.getDiagJobSaetze());
					String dtcsToDelete = "";
					for( int i = 1; i < (myEdiabas.getDiagJobSaetze() - 1); i++ ) {
						String fOrtNr = myEdiabas.getDiagResultValue( i, "F_ORT_NR" );
						boolean fOrtExists = false;
						for( int j = 0; j < fsEintraege.length; j++ ) {
							//FOrtNr schon vorhanden --> Ignorieren
							if( fOrtNr.equalsIgnoreCase( fsEintraege[j] ) ) {
								System.out.println("FS " + fOrtNr + " existiert bereits.");
								fOrtExists = true;
								break;
							}
						}
						//FS-Eintrag nach APDM schreiben, falls Fehler noch nicht aufgetreten ist
						if( !fOrtExists ) {
							//�berpr�fen ob Fehler in der Ignore-List ist und ggf. ignorieren
							boolean ignored = false;
							boolean isInDTCList = false;
							boolean documentAllDTC = false;
							if( ignore != null ) {
								StringTokenizer tokenizer = new StringTokenizer( ignore, ";" );
								while( tokenizer.hasMoreTokens() ) {
									String singleIgnore = tokenizer.nextToken();
									if( fOrtNr.equalsIgnoreCase( singleIgnore ) ) {
										ignored = true;
										break;
									}
								}
							}
							//wenn die dtc-Liste bef�llt ist, so sollen nur aufgetretene Fehler aus dieser Liste
							//zu APDM geschickt werden
							if( dtc != null ) {
								StringTokenizer tokenizer = new StringTokenizer( dtc, ";" );
								while( tokenizer.hasMoreTokens() ) {
									String singleDTC = tokenizer.nextToken();
									if( fOrtNr.equalsIgnoreCase( singleDTC ) ) {
										System.out.println("FS " + fOrtNr + " ist in der DTC-Liste parametriert.");
										isInDTCList = true;
										break;
									}
								}
							}else{
								//wenn DTC-Liste nicht parametriert wird, so werden alle FS beachtet und zu APDM geschickt
								documentAllDTC = true;
								System.out.println("FS " + fOrtNr + " wird beachtet, da keine DTC-Liste parametriert wurde.");
							}
							if( !ignored && (isInDTCList||documentAllDTC) ) {
								status = STATUS_EXECUTION_ERROR;
								String fOrtText = myEdiabas.getDiagResultValue( i, "F_ORT_TEXT" );
								String fHexCode = null;
								try {
									fHexCode = myEdiabas.getDiagResultValue( i, "F_HEX_CODE" );
								} catch( Exception ernfe ) {
									// Nicht alle Steuerger�te unterst�tzen diese Zusatzinformation, also Fehler ignorieren
									fHexCode = null;
								}
								// Existiert Zusatzinformation �ber einen Hex-Fehlercode?
								if( fHexCode != null )
									result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", fOrtNr + " (HEX " + fHexCode + ")", "", "", "0", "", "", "", fOrtText, "", Ergebnis.FT_NIO );
								else
									result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", fOrtNr, "", "", "0", "", "", "", fOrtText, "", Ergebnis.FT_NIO );

								ergListe.add( result );
							}
							//wenn delete_dtc=true und in der FS in der DTC-Liste ist oder es keine Liste gibt und
							//alle aufgetretenen Fehler relevant sind, dann werden diese gel�scht
							System.out.println("FS_LOESCHEN delete_dtc " + delete_dtc);
							System.out.println("FS_LOESCHEN isInDTCList " + isInDTCList);
							System.out.println("FS_LOESCHEN documentAllDTC " + documentAllDTC);
							if(delete_dtc && (isInDTCList||documentAllDTC)){
								dtcsToDelete = dtcsToDelete + fOrtNr + ";";
							}
						}
					}
					if(dtcsToDelete.length()>0){
						// EDIABAS Job ausf�hren
						dtcsToDelete = dtcsToDelete.substring( 0, dtcsToDelete.length()-1 );
						System.out.println("dtcsToDelete " + dtcsToDelete);
						StringTokenizer tokenizer = new StringTokenizer( dtcsToDelete, ";" );
						while( tokenizer.hasMoreTokens() ) {
							String singleDTC = tokenizer.nextToken();
							temp = myEdiabas.executeDiagJob( sgbd, "FS_LOESCHEN", singleDTC, "" );
							if( temp.equals( "OKAY" ) == false ) {
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, "FS_LOESCHEN", singleDTC, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								System.out.println(singleDTC + " Problem beim L�schen.");
								throw new PPExecutionException();
							} else {
								result = new Ergebnis( "Status", "EDIABAS", sgbd, "FS_LOESCHEN", singleDTC, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
								System.out.println(singleDTC + " gel�scht.");
							}
						}
						
					
					}
				}
			} catch( ApiCallFailedException e ) {
				e.printStackTrace();
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), myEdiabas.apiErrorCode() + ": " + myEdiabas.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException e ) {
			e.printStackTrace();
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception (" + e.getMessage() + ")", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable (" + e.getMessage() + ")", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}
		System.out.println("vor settPPStatus");
		setPPStatus( info, status, ergListe );

	}
}

package com.bmw.cascade.pruefprozeduren;

import java.util.StringTokenizer;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;

/**
 * PP zum Zerlegen eines beliebiegen Eingangsstrings auf Basis eines Tokens bzw. Delimiters.<BR>
 * Die ermittelten Substrings werden im virtuellen Fahrzeug abgespeichert.<BR>
 * Sind Key=Value Paare enthalten, dann werden diese einzeln mit dem Key als Bezeichnung abgespeichert.<BR>
 * <BR>
 * @author Buboltz BMW AG <BR>
 *         09.03.2014 TB Ersterstellung <BR>
 *         30.04.2015 MS Bugfix falls DELIMITER=,
 */
public class StringTokenizer_3_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public StringTokenizer_3_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public StringTokenizer_3_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "DELIMITER", "DEBUG" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = { "INSTRING" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		return super.checkArgs();
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// �bergabeparameter
		String instring = null;
		String delimiter = ";";
		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen

		try {
			// Argumente einlesen und checken
			try {
				// DEBUG abfragen
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
				} else {
					// noch Pr�fstandvariable pr�fen, ob Debug
					try {
						Object pr_var_debug;

						pr_var_debug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
						if( pr_var_debug != null ) {
							if( pr_var_debug instanceof String ) {
								if( "TRUE".equalsIgnoreCase( (String) pr_var_debug ) ) {
									bDebug = true;
								} else {
									bDebug = false;
								}
							} else if( pr_var_debug instanceof Boolean ) {
								if( ((Boolean) pr_var_debug).booleanValue() ) {
									bDebug = true;
								} else {
									bDebug = false;
								}
							}
						} else {
							bDebug = false;
						}

					} catch( VariablesException e ) {
						bDebug = false;
					}
				}

				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// Eingangsstring, als zwingendes Argument (direkter String bzw. �ber Refernzoperator)
				instring = extractValues( getArg( "INSTRING" ) )[0];

				// Trennzeichen, als zwingendes Argument (direkter String bzw. �ber Referenzoperator)
				if( getArg( "DELIMITER" ) != null ) {
					delimiter = extractValues( getArg( "DELIMITER" ) )[0];
					if (delimiter.equalsIgnoreCase( "$,$" )){
						delimiter = ",";
					}
				}

				// Debug
				if( bDebug ) {
					System.out.println( "PP StringTokizer, Parameter: instring = <" + instring + ">" );
					System.out.println( "PP StringTokizer, Parameter: delimiter = <" + delimiter + ">" );
				}
			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// zerlege den Eingangsstring anhand des Tokens
			StringTokenizer delimStringTokenizer = new StringTokenizer( instring, delimiter );
			int i = 0;
			while( delimStringTokenizer.hasMoreTokens() ) {
				String token = delimStringTokenizer.nextToken();

				try {
					// Wenn das Token die Struktur "Key=Value" beinhaltet, dann dokumentiere das Value unter der ID des Keys
					String[] keyvalue = token.split( "=" );
					result = new Ergebnis( keyvalue[0], "StringTokenizer", "", "", "", "RESULT", keyvalue[1], keyvalue[1], "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				} catch( Exception e ) {
					// Quasi ein IO-Fall

					// Das Token enth�lt kein "Key=Value" dokumentiere das Token allgemein mit der ID "TOKEN '+ Laufindex'"
					result = new Ergebnis( "TOKEN" + i++, "StringTokenizer", "", "", "", "RESULT", token, token, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				}

			}

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "StringTokenizer", "StringTokenizer", "", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}
		setPPStatus( info, status, ergListe );
	}
}

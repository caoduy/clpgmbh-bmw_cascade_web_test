/*
 * DiagSetInterface_3_0_F_Pruefprozedur.java
 *
 * Created on 09.01.08
 */
package com.bmw.cascade.pruefprozeduren;


import java.util.*;
import java.util.logging.Level;

import com.bmw.appframework.logging.LogLevel;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.visualisierung.*;
import com.bmw.cascade.util.logging.CascadeLogging;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.devices.b2v.*;
//import com.bmw.cascade.pruefstand.ediabas.*;



/**
 * Pr�fprozedur, welche das von EDIABAS zu verwendete Interface �ndert.
 *
 * Bei s�mtlichen Parametern k�nnen Pr�fstandsvariablen �bergeben werden, 
 * welche dann den jeweiligen Wert spezifizieren. In dem Fall muss vor 
 * dem Namen der Variablen ein $ geschrieben werden (wenn die Variable z. B. 
 * VAR_INT hei�t, ist $VAR_INT zu schreiben. Falls der Wert selbst mit einem 
 * $ beginnt, muss diesem ebenfalls ein $ vorangestellt werden ($$), hierbei 
 * w�rde sp�ter das erste $ automatisch entfernt und der Wert auch als Wert 
 * behandelt werden. Hinweis: Dies impliziert, dass Pr�fstandsvariablen nicht 
 * mit einem $ beginnen d�rfen.
 *
 * Zwingende Argumente:
 *
 * INTERFACE: 	Name des zu verwendenden Interfaces (z. B. STD:FUNK oder ENET).  				
 *
 * Optionale Argumente:
 * 
 * UNIT: 		  Stellt zus�tzliche Information zum jeweiligen Interface bereit. 
 * 				  Dabei ist grunds�tzlich nur ein Zeichen zul�ssig. Welche Zeichen 
 * 				  hierbei zur Verf�gung stehen und was diese repr�sentieren, h�ngt 
 * 				  vom jeweiligen Interface ab (bei EDIC = A, B, C etc. und bei 
 * 				  STD:OMITEC bzw. STD:FUNK = 1, 2, 3...).  
 *  
 * APPLICATION:   Spezifiziert die Applikation, welche innerhalb des Interfaces
 * 				  angesprochen wird (momentan verf�gt nur EDIC �ber mehrere
 * 				  Applikationen, die Angabe ist dort zwingend, z. B. EIDBSS). 
 * 
 * CONFIGURATION: �ndert den letzten Parameter beim apiInitExt (die Configuration).
 * 				  Prinzipiell k�nnen damit beliebige Parameter von Ediabas ge�ndert werden.
 * 				  z.B. den Paramter "IfhRemoteHost=192.169.0.1"
 *
 * !!! ACHTUNG Es ist zwingend notwendig, dass bei Verwendung des Configuration Parameters oder in Kombination mit dem B2V Device
 * !!!         die neueste Cascade Ediabas Anbindungs DLL verwendet wird. apijav32mt.dll Version: 1.0.4 
 *
 * @author Peter Rettig, TI-430; Thomas Buboltz, TP-430<BR>
 * @version 1_0_F PR 20.11.2007 Initiale Implementierung<BR>
 * 			2_0_F PR 28.11.2007	M�glichkeit integriert, die Parametrierung mittels Pr�fstandsvariablen vorzunehmen<BR>
 * 			3_0_F PR 09.01.2008	Korrekte Initialisierung von STD:FUNK integriert 
 *          4_0_T TB 19.03.2009 Einbau B2V und Anpassung auf neues variables B2V (Entfall B2V)
 *          5_0_F TB 26.05.2009 Analog T-Version, Freigabe nach Abschluss Test Werk Dingolfing
 *          8_0_T Mke 07.02.2016 deprecated Code entfernt  
 *          9_0_F Mke 07.02.2016 F-Version 
 */
public class DiagSetInterface_9_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    static final long serialVersionUID = 1L;
    
    private EdiabasProxyThread Ediabas;
    
    Vector ergListe;
    
    
    /**
     * DefaultKonstruktor, nur fuer die Deserialisierung
     */
    public DiagSetInterface_9_0_F_Pruefprozedur() {}

    
    /**
     * Konstruktor.
     *
     * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public DiagSetInterface_9_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super(pruefling, pruefprozName, hasToBeExecuted);
        attributeInit();
    }
    
    
    /**
     * Initialsiert die Default-Argumente.
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    
    /**
     * Liefert die optionalen Argumente.
     *
     * @return String-Array mit den optionalen Parametern.
     */
    public String[] getOptionalArgs() {
        String[] args = {"UNIT", "APPLICATION", "CONFIGURATION", "DEBUG"};
        return args;
    }
    
    
    /**
     * Liefert die zwingend erforderlichen Argumente.
     *
     * @return String-Array mit den zwingenden Parametern.
     */
    public String[] getRequiredArgs() {
        String[] args = {"INTERFACE"};
        return args;
    }
    
    
    /**
     * Pr�ft die Argumente auf Existenz und Wert.
     *
     * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt
     *         hat, ansonsten <code>true</code>.
     */
    public boolean checkArgs() {
        // Obligatorisch.
        if (!super.checkArgs()) 
        	return false;
                      
        return true;
    }
    
    
    /**
     * F�hrt die Pr�fprozedur aus.
     *
     * @param info Information zur Ausf�hrung.
     */
    public void execute( ExecutionInfo info ) {
        Ergebnis result;
        ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        //eigene Variablen
        String ifh;
        String ifhUnit;
        String ifhApplication;
        String ifhConfiguration;
        
		/** Flag; Sollen Debug-Infos geschrieben werden				*/
		boolean Debug = false;
        
		// Device B2V, notwe. bei Fzg. TCP/IP-Anbindung zum Finden der dem Fahrzeug (i.d.R. dem ZGW) zugeordn. IP-Adr.
		B2V myB2VHandler = null;
		
		DeviceManager devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();
		EdiabasProxyThread Ediabas;
        
        try {            
            //Parameter holen und Argumente pruefen.
            try {
                // Ist der allgemeine Check fehlgeschlagen?
                if( !checkArgs() ) 
                	throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

                // hole das Interface
                if( getArg( getRequiredArgs()[0] ).indexOf('@') != -1 )
                	ifh = getPPResult( getArg( getRequiredArgs()[0] ) );
                else
                	ifh = getArg( getRequiredArgs()[0] );
                ifh = processPrefix(ifh); 
                if (ifh == null)
                	throw new PPExecutionException( "Pruefstand variable not found!" );
                
                // hole die Angabe, falls mehrere gleiche Interface existieren, welches Interface genau gemeint ist
                ifhUnit = getArg( getOptionalArgs()[0] );
                if (ifhUnit != null) {  
                	if( ifhUnit.indexOf('@') != -1 )
                		ifhUnit = getPPResult( ifhUnit );
                } else
                	ifhUnit = ""; //default
                ifhUnit = processPrefix(ifhUnit);
                if (ifhUnit == null)
                	throw new PPExecutionException( "Pruefstand variable not found!" );
                
                // hole die Angabe, welche Interface-interne Applikation zu verwenden ist
                ifhApplication = getArg( getOptionalArgs()[1] );
                if (ifhApplication != null) {  
                	if( ifhApplication.indexOf('@') != -1 )
                		ifhApplication = getPPResult( ifhApplication );
                } else
                	ifhApplication = ""; //default
                ifhApplication = processPrefix(ifhApplication);
                if (ifhApplication == null)
                	throw new PPExecutionException( "Pruefstand variable not found!" );
                
                // hole die Angabe, welche Configuration fuer das Interface zu verwenden ist
                ifhConfiguration = getArg( getOptionalArgs()[2] );
                if (ifhConfiguration != null) {  
                	if( ifhConfiguration.indexOf('@') != -1 )
                		ifhConfiguration = getPPResult( ifhConfiguration );
                } else
                	ifhConfiguration = ""; //default
                ifhConfiguration = processPrefix(ifhConfiguration);
                if (ifhConfiguration == null)
                	throw new PPExecutionException( "Pruefstand variable not found!" );
                
				// DEBUG ON ?
				if( getArg( getOptionalArgs()[3] ) != null && getArg( getOptionalArgs()[3]).equalsIgnoreCase( "TRUE" ))
					Debug = true;
                
            } catch (PPExecutionException e) {
                if (e.getMessage() != null)
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
          //Ediabas holen
        	Ediabas = devMan.getEdiabas();
            
            // �ndere das Interface.
            try {
				try {
					//MBa f�r Paralleldiagnose:
					//Pr�fstand auf Parallelbetrieb=AUS schalten
					getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL", Boolean.FALSE );

				} catch( Exception e ) {
					CascadeLogging.getLogger().log( LogLevel.WARNING, "Ediabas Parallel Diagnostics was not disabled", e );
				}
            	
                Ediabas.apiEnd();
                
				// Device B2V konfiguriert ?
				try{
					myB2VHandler = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getB2V();
					myB2VHandler.requestAvailableVehicles();
				} catch (Exception exp) {
					// Wenn nicht konfiguriert oder Probleme, dann k�nnen wir mit B2V halt nicht's tun..
				}

				// Haben wir keinen 4. Parameter (ifhConfiguration) und ist das B2V Device verf�gbar?
				if ( ifhConfiguration.equalsIgnoreCase( "" )) {
					if ( myB2VHandler != null && (ifh.equalsIgnoreCase( "ENET" ) || ifh.equalsIgnoreCase( "PROXY:ENET" )) ){
						// TODO Parameter wird auf "RemoteHost" mit dem n�chsten Ediabas ge�ndert 
						
						// Alternative 1, Umschaltung der IP-Adresse �ber apiInitExtExt(...)
						Ediabas.apiInitExtExt( ifh, "", "", "IfhRemoteHost=" + myB2VHandler.getMVinIdoIdVin().get( this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7()  ) );
						if(Debug) System.out.println("DiagSetInterface PP : (B2V) : apiInitExtExt( \"" + ifh+"\" , \"\" , \"\" , \"IfhRemoteHost=" + myB2VHandler.getMVinIdoIdVin().get( this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7() ) + "\" )");
	
						// IP Adresse des Fahrzeuges i.d.R. die IP des ZGWs im virtuellen Fzg. merken
						result = new Ergebnis( "IP_ADDRESS", "B2V", "", "", "", "IP_ADDRESS", "" + myB2VHandler.getMVinIdoIdVin().get( this.getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7() ), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
					else
						// alter Aufruf mit apiInitExt und 3 Parametern
						Ediabas.apiInitExt( ifh, ifhUnit, ifhApplication); 
				}
				else
					// Wir haben einen 4. Parameter, also neuer Aufruf mit 4 Parametern
					Ediabas.apiInitExtExt( ifh, ifhUnit, ifhApplication, ifhConfiguration);
                               
                result = new Ergebnis( "SET_INTERFACE", "Ediabas.apiInitExt", "IFH=<" + ifh + ">", "DeviceUnit=<" + ifhUnit + ">", "DeviceApplication=<" + ifhApplication + ">, Configuration =<" + ifhConfiguration + ">", "", "", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                ergListe.add(result);                
                
            } catch (ApiCallFailedException e) {
                result = new Ergebnis( "Ediabas-Fehler", "", "", "", "", "", "", "", "", "", "", "", "", e.toString(), "" , Ergebnis.FT_NIO );
                ergListe.add(result);
                throw new PPExecutionException();
            }
          
            // Wenn auf Funk geschaltet wurde, dann diesen noch ordnungsgem�� 
            // initialisieren.
            if (ifh.equalsIgnoreCase("STD:FUNK")) 
            	initFunk( false/*keine Debug-Ausgaben schreiben*/ );
            
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO );
            e.printStackTrace();
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
		// Freigabe der benutzten Devices
		if( myB2VHandler != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseB2V();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "B2V", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "B2V", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
        
        setPPStatus(info, status, ergListe);
    }
    
    
    /**
     * Verabreitet einen Wert weiter, wenn dieser mit einem $ begonnen hat. In
     * dem Fall muss es sich um den Namen einer Pr�fstandsvariablen handeln,
     * deren Wert zur�ckgegeben wird. Falls nach dem f�hrenden $ ein weiteres 
     * folgt, dann diente das erste nur zur Maskierung, so dass der um das erste 
     * $ bereinigte Wert �bergeben wird.
     * 
     * @param value Wert.
     * @return Weiterverarbeiteter R�ckgabewert. Bei <code>null</code> wurde 
     * 		   die spezifizierte Pr�fstandsvariable nicht gefunden.
     */
    private String processPrefix(String value) {
    	String VAR_INDICATOR = "$";
    	    	    	
    	// �berpr�fe die G�ltigkeit des �bergebenen Parameters.
    	if (value == null)
    		return null;
    	
    	// Beginnen wir mit einem $?
    	if (value.startsWith(VAR_INDICATOR)) {
    		value = value.substring(1); //erstes $ abschneiden
    		
    		// Handelt es sich um ein $ welches mit einem f�hrenden $ maskiert 
    		// wurde?
    		if (!value.startsWith(VAR_INDICATOR)) //Kein weiteres $ an Anfang enthalten?
    			try {
    				value = (String)getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PS_CONFIG, value); //beziehe Inhalt der Pr�fstandsvariablen		
    			} catch (Exception x) {
    				value = null;
    			} //nein, dann Wert aus der Pr�fstandsvariablen beziehen
    	}
    	
    	return value;
    }
    
    
    /**
     * Initialisiere das Interfaces STD:FUNK. Hinweis: Im Falle einer Punkt zu 
     * Punkt-Verbindung kann auf eine Ausf�hrung verzichtet werden.
     * 
     * Dieser Code wurde aus der Pr�fprozedur DiagOpen_38_0_F_Pruefprozedur 
     * entnommen. Danke an den Autor!
     * 
     * @param debug Angabe, ob Debug-Ausgaben erfolgenden sollen.
     * @throws PPExecutionException Falls es zu Problemen kommt.
     */
    private void initFunk(boolean debug) throws PPExecutionException {
    	
    	Ergebnis result;    	
    	boolean initOK = true;
    	boolean startOK = false;
    	String temp;
    	boolean autoInitTried = false; // Automatische Bestimmung ob P2P- oder Linienmodus
		boolean autoInitSuccessful = false;
		boolean udInUse = false;
		int key;
		String fzs = getPr�fling().getAuftrag().getSteuerschluessel();
		String mdaNummer = null;
		int automode = 1;
		int diagmode = 1;
		String mdaExt = null; 
		int messageTimeout = 0;

		do {
			if( initOK == true ) {
				// Zwingend notwendig, um den MDA vom Datenkanal in den 
				// Organisationskanal zu schalten.
				try {
					temp = Ediabas.executeDiagJob( "IFR", "ENDE_PRUEFUNG", "", "JOB_STATUS" );
					if(debug) System.out.println("DiagSetInterface PP if( initOK == true ) --> JOB ENDE_PRUEFUNG: " + temp + " FGNR: " + fzs);				
				} catch( ApiCallFailedException e ) {					
				} catch( EdiabasResultNotFoundException e ) {					
				}

				// Aufbau der Funkverbindung
				try {
					temp = Ediabas.executeDiagJob( "IFR", "START_PRUEFUNG", fzs, "JOB_STATUS" );
					if(debug) System.out.println("DiagSetInterface PP if( initOK == true ) --> JOB START_PRUEFUNG: " + temp + " FGNR: " + fzs);
					
					if( temp.equals( "OKAY" ) == true )
						startOK = true;
					
					if(debug) System.out.println("DiagSetInterface PP if( initOK == true ) --> SET startOK: " + startOK);
					
				} catch( ApiCallFailedException e ) {
					startOK = false;
				} catch( EdiabasResultNotFoundException e ) {
					startOK = false;
				}
			}
			if( startOK == false ) {
				initOK = false;
				// Versuche erstmal, die Verbindungsart automatisch zu bestimmen -
				// allerdings nur einmal!
				if( /*(marriage == false) && [RETTIG]*/ (autoInitTried == false) ) {
					autoInitTried = true;
					int repeatCounter = 0;
					try {
						do {
							temp = Ediabas.executeDiagJob( "IFR", "STATUS_SLAVE_ZIELNUMMER", "", "" );
							if(debug) System.out.println("DiagSetInterface PP if( startOK == false ) --> JOB STATUS_SLAVE_ZIELNUMMER: " + temp);
							repeatCounter++;
							if( temp.equals( "OKAY" ) == true ) {
								mdaNummer = Ediabas.getDiagResultValue( "STAT_SLAVE_ZIELNUMMER" );
								if(debug) System.out.println("DiagSetInterface PP if( startOK == false ) --> STAT_SLAVE_ZIELNUMMER: " + mdaNummer);
								result = new Ergebnis( "Diagnose", "EDIABAS", "IFR", "STATUS_SLAVE_ZIELNUMMER", "", "STAT_SLAVE_ZIELNUMMER", mdaNummer, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
								if( mdaNummer.equalsIgnoreCase( "0" ) == false ) {
									// Punkt-zu-Punkt verbindung, somit OK da IFR-Jobs
									// komplett �berfl�ssig, aber Funk reagiert sich nicht
									// zum ersten Diagnose Aufruf...
									try {
										temp = Ediabas.executeDiagJob( "UTILITY", "STATUS_ZUENDUNG", "", "JOB_STATUS" );
									} catch( Exception e ) {
										// Fehler ignorieren beim ersten Job!
									}
									startOK = true;
									autoInitSuccessful = true;
								} 
								if (mdaNummer.equalsIgnoreCase( "0" ) == true){
									if(debug) System.out.println("DiagSetInterface PP if( startOK == false ) --> STAT_SLAVE_ZIELNUMMER = 0 --> Abbruch Schleife");
									break;
									}
							} else {
								if( (repeatCounter < 2) && (startOK == false) )
									if(debug) System.out.println("DiagSetInterface PP if( startOK == false ) --> JOB STATUS_SLAVE_ZIELNUMMER trys: " + repeatCounter);
									Thread.sleep( 200 );
							}
						} while( (repeatCounter < 2) && (startOK == false));
					} catch( Exception e ) {
						result = new Ergebnis( "Exception", "EDIABAS", "IFR", "STATUS_SLAVE_ZIELNUMMER", "", "STAT_SLAVE_ZIELNUMMER", "", "", "", "0", "", "", "", e.getMessage(), "", Ergebnis.FT_IO );
						ergListe.add( result );
						e.printStackTrace();						
					}
				}
				if( autoInitSuccessful == false ) {
					// Manueller Ablauf mit Eingabe durch den Benutzer
					try {
						// wenn MDA-Nummer von extern kommt, dann nutze diese ansonsten nehmen wir den alten - manuellen Weg �ber die UserEingabe
						mdaNummer = (mdaExt != null) ? mdaExt : getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputNumber( PB.getString( "Diagnose" ), PB.getString( "eingabeDerMdaNummer" ), messageTimeout );
						if(debug) System.out.println("DiagSetInterface PP if( autoInitSuccessful == false ) --> Used MDA-Nummer for assignment: " + mdaNummer);
						result = new Ergebnis( "MDA_NR_INPUT", "EDIABAS", "", "", "", "MDA_NR_INPUT", mdaNummer, "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
						udInUse = true;
						if( mdaNummer == null ) {
							result = new Ergebnis( "MDA", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Autostart Timeout = " + messageTimeout + "s", PB.getString( "abbruchDurchWerker" ), Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						}
						if( mdaNummer.equals( "" ) == true ) {
							// Abbruch?
							key = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputDigital( PB.getString( "Diagnose" ), PB.getString( "abbruchDurchWerker" ), messageTimeout );
							if( key != UserDialog.NO_KEY ) {
								result = new Ergebnis( "DiagFehler", "EDIABAS", "IFR", "INIT_IFR", fzs + ";" + mdaNummer, "", "", "", "", "0", "", "", "", PB.getString( "funkKommunikation" ), PB.getString( "abbruchDurchWerker" ), Ergebnis.FT_NIO );
								ergListe.add( result );
								throw new PPExecutionException();
							}
						} else if( mdaNummer.equals( "0" ) == true ) {
							// Punkt-zu-Punkt verbindung, somit OK da IFR-Jobs komplett
							// �berfl�ssig, aber Funk reagiert sich nicht zum ersten
							// Diagnose Aufruf...
							try {
								temp = Ediabas.executeDiagJob( "UTILITY", "STATUS_ZUENDUNG", "", "JOB_STATUS" );
							} catch( Exception e ) {
								// Fehler ignorieren beim ersten Job!
							}
							startOK = true;

						} else {
							// Verheiratung MDA mit FZS?
							try {
								temp = Ediabas.executeDiagJob( "IFR", "INIT_IFR", fzs + ";" + mdaNummer + ";" + automode + ";" + diagmode, "JOB_STATUS" );
								if(debug) System.out.println("DiagSetInterface PP if( autoInitSuccessful == false ) --> JOB INIT_IFR: " + temp + " MDA: " + mdaNummer + " FGNR: " +  fzs);
								if( temp.equals( "OKAY" ) == true ) {
									initOK = true;
									if(debug) System.out.println("DiagSetInterface PP if( autoInitSuccessful == false ) --> SET initOK: " + initOK );
								} else if( mdaExt != null ) {
									if(debug) System.out.println("DiagSetInterface PP if( autoInitSuccessful == false ) --> Fehler bei externen �bergabe MDA-Nummer: " + mdaExt );
									// falls die externe MDA Nummer nicht funktioniert, dann dokumentieren wir das und werfen eine Exception
									result = new Ergebnis( "MDA", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "MDA Extern = " + mdaExt, PB.getString( "mda" ) + ", " + PB.getString( "parametrierfehler" ), Ergebnis.FT_NIO );
									ergListe.add( result );
									throw new PPExecutionException();
								}
							} catch( ApiCallFailedException e ) {
								// Ignore
							} catch( EdiabasResultNotFoundException e ) {
								// Ignore
							}
						}
					} catch( Exception e ) {
						if( e instanceof PPExecutionException )
							throw new PPExecutionException();
						else if( e instanceof DeviceLockedException )
							result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
						else if( e instanceof DeviceNotAvailableException )
							result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
						else
							result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						throw new PPExecutionException();
					}
				}
			}
		} while( startOK == false );
		
    }
    
}


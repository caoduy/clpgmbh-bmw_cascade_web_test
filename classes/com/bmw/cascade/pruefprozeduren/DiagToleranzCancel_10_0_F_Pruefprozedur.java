/*
 * DiagToleranzCancel_V0_0_9_FA_Pruefprozedur.java
 *
 * Created on 10.03.03
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.ediabas.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementierung der Pr�fprozedur, die per Diagnose diverse Infos bewertet (auf erf�llt bzw. nicht erf�llt). 
 * Diese Bewertung kann pollend erfolgen, d.h. �ber das optionale Argument TIMEOUT kann gesteuert werden, wie
 * lange (Maximalzeit) die Bewertung erfolgt. �ber das optionale Argument OK_TIME kann angegeben werden, wie lange
 * der Status OK gehalten werden muss, damit dieser Status g�ltig ist. Optional ist die Ausgabe einer Werkeranweisung
 * m�glich, die eine Abbruchtaste anbietet. Auftretende EDIABAS-Fehler wie "Wrong UBATT" oder "No Response from Control Unit" 
 * werden Ignoriert, wenn das optionale Argument IGNORE_ERRORS gesetzt ist.
 * @author Winkler
 * @author Winklhofer
 * @version 0_0_9 10.03.03  IGNORE_ERRORS hinzugef�gt.
 * @version 0_1_0 03.07.03  mehrere HWTs hinzugef�gt.
 */
public class DiagToleranzCancel_10_0_F_Pruefprozedur extends AbstractBMWPruefprozedur 
{

  	static final long serialVersionUID = 1L;
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public DiagToleranzCancel_10_0_F_Pruefprozedur() {}

  /**
   * erzeugt eine neue Pruefprozedur, die auf Einhaltung bzw. auf Ausschlu� der zu analysierenden Werte pr�ft.
   * @param pruefling Klasse des zugeh. Pr�flings
   * @param pruefprozName Name der Pr�fprozedur
   * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
   */
    public DiagToleranzCancel_10_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) 
  {
    super( pruefling, pruefprozName, hasToBeExecuted );
    attributeInit();
  }  
  

  /**
   * initialsiert die Argumente
   */
  protected void attributeInit() {
    super.attributeInit();
    setAttribut("INVERT","FALSE");
  }

  /**
   * liefert die optionalen Argumente
   */
  public String[] getOptionalArgs() {
    String[] args = {"JOBPAR", "RESULT[1..N]", "MIN[1..N]","MAX[1..N]", "TIMEOUT", "OK_TIME", "PAUSE", "AWT", "HWT", "INVERT","IGNORE_ERRORS", "HWT[1..N]"};
    return args;
  }

  /**
   * liefert die zwingend erforderlichen Argumente
   */
  public String[] getRequiredArgs() {
    String[] args = {"SGBD", "JOB"};
    return args;
  }

  /**
   * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die parent-Methode aufgrund
   * der offenen Anzahl an Results
   */
   public boolean checkArgs() {
    double d;
    int i, j;
    int maxIndex = 0;
    boolean exist;

    try{
      // 1. Check: Sind alle requiredArgs gesetzt
      String[] requiredArgs = getRequiredArgs();
      for( i=0; i<requiredArgs.length; i++ ) {
        if( getArg(requiredArgs[i]) == null ) return false;
        if( getArg(requiredArgs[i]).equals("") == true ) return false;
      }
      // 2. Check: Sind �berfl�ssige Argumente gesetzt, d.h. Argumente, die weder required noch optional sind
      // Gesetzte Argumente, die mit "RESULT", "MIN" oder "MAX" beginnen, werden hierbei nicht analysiert, es wird
      // jedoch der maximale Index festgehalten. Die detaillierte Analyse erfolgt im 3-ten Check. 
      Enumeration enu = getArgs().keys();
      String[] optionalArgs = getOptionalArgs();
      String givenkey;
      String temp;
      while( enu.hasMoreElements()) {
        givenkey = (String) enu.nextElement();
        if( (givenkey.startsWith("RESULT") == false) && 
            (givenkey.startsWith("MIN") == false) &&
            (givenkey.startsWith("MAX") == false) &&
            ((givenkey.startsWith("HWT") == false)||(givenkey.equals("HWT") == true))) {
          exist = false;
          j = 0;
          while( (exist == false) && (j < requiredArgs.length) ) {
            if( givenkey.equals(requiredArgs[j]) == true ) exist = true;
            j++;
          }
          j = 0;
          while( (exist == false) && (j < optionalArgs.length) ) {
            if( givenkey.equals(optionalArgs[j]) == true ) exist = true;
            j++;
          }
          if (exist == false) return false;
          if( getArg(givenkey).equals("") == true ) return false;
        } else {
          if( givenkey.startsWith("RESULT") == true ) {
            temp = givenkey.substring(6);
          } else {
            temp = givenkey.substring(3);
          }
          try {
            j = Integer.parseInt(temp);
            if (j < 1) return false;
            if (j > maxIndex) maxIndex = j;
          } catch (NumberFormatException e) {
            return false;
          }  
        }
      }
      // 3. Check: Sind die Result- und Min-Argumente plausibel, Max-Argumente sind optional und werden daher gefordert.
      // Existieren jedoch Max-Werte, wird die Anzahl sowie die Konvertierung von Minwerten und Maxwerten zu Zahlen gepr�ft,
      // sofern es sich um kein Result einer anderen PP handelt
      String[] mins, maxs;
      for( i=1; i<=maxIndex; i++ ) {
        temp = getArg("RESULT"+i);
        if( temp == null ) return false;
        if( temp.indexOf(';') != -1 ) return false;
        if( (i > 1) && (temp.equals(getArg("RESULT1")) == true) )return false; 
        temp = getArg("MIN"+i);
        if( temp == null ) return false;
        if( getArg("MAX"+i) != null ) {
          mins = splitArg( getArg("MIN"+i) );
          maxs = splitArg( getArg("MAX"+i) );
          if (mins.length != maxs.length) return false;
          for( j=0; j<mins.length; j++ ) {
            try {         
              if( mins[j].indexOf('@') == -1) d = Double.parseDouble(mins[j]);
              if( maxs[j].indexOf('@') == -1) d = Double.parseDouble(maxs[j]);
            } catch (NumberFormatException e) {
              return false;
            }  
          }
        }
      }
      //4. Check: Falls TIMEOUT, PAUSE und OK_TIME gesetzt, mu� Integer sein
      for( i=4; i<=6; i++ ) {
        if( getArg(optionalArgs[i]) != null ) {
          try {         
            if( (Long.parseLong( getArg(optionalArgs[i]) )) < 0) return false;
          } catch (NumberFormatException e) {
            return false;
          }  
        }
      }  
      // 5. Check: Argmente SGBD, JOB, RESULT[1..N] d�rfen keine Alternativen enthalten
      enu = getArgs().keys();
      while( enu.hasMoreElements()) {
        givenkey = (String) enu.nextElement();
        if( (givenkey.equals("SGBD") == true) || (givenkey.equals("JOB") == true) || (givenkey.startsWith("RESULT") == true) ) {
          if (getArg(givenkey).indexOf(';') != -1) return false;
        }
      }
      // 6. Check: Argumente INVERT und IGNORE_ERROR m�ssen TRUE oder FALSE sein, falls vorhanden
      temp = getArg(getOptionalArgs()[9]);
      if ((temp != null) &&
          (!temp.equalsIgnoreCase("TRUE")) &&
          (!temp.equalsIgnoreCase("FALSE"))) return false;
      temp = getArg(getOptionalArgs()[10]);
      if ((temp != null) &&
          (!temp.equalsIgnoreCase("TRUE")) &&
          (!temp.equalsIgnoreCase("FALSE"))) return false;
      // Tests bestanden, somit ok 
      return true;
    } catch (Exception e) {
      return false;
    } catch (Throwable e) {
      return false;
    }                     
  }


  /**
   * f�hrt die Pr�fprozedur aus
   * @param info Information zur Ausf�hrung
   */
  public void execute( ExecutionInfo info )
  {
    // immer notwendig
    Ergebnis result;
    Vector ergListe = new Vector();
    int status = STATUS_EXECUTION_OK;

    // spezifische Variablen
    String temp, temp1, temp2;
    String sgbd, job, jobpar, jobres;
    String awt, hwt;
    String[] results = null;
    String[] mins, maxs;
    String[] hwts = null;
    double d;
    long endeZeit, okTime, pause, deltaT, firstOkTime;
    int resAnzahl;
    int i, j;
    boolean invert=false;
    boolean ok;
    boolean udInUse = false;
    boolean invalid = false;    //true if no results could be retrieved due to errors, e.g. "Wrong UBATT"
    boolean ignore_errors = false;
    UserDialog myDialog = null;

    try {
      //Parameter holen 
      try {
        if( checkArgs() == false ) throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
        sgbd   = extractValues( getArg( getRequiredArgs()[0] ) )[0];
        job    = getArg( getRequiredArgs()[1] );
        jobpar = getArg( getOptionalArgs()[0] );
        if (jobpar == null ) jobpar = "";
        jobres = "";
        resAnzahl = 0;
        while( (temp = getArg("RESULT"+(resAnzahl+1))) != null) {
          jobres = jobres+";"+extractValues( temp )[0];
          resAnzahl++;
        }
        if (resAnzahl > 0) {
          jobres = jobres.substring(1);
          results = splitArg(jobres); //Links sind in jobres bereits aufgel�st
          if (results.length != resAnzahl) throw new PPExecutionException( PB.getString( "parameterexistenz" )+"1" );
          for( i=1; i<=resAnzahl; i++ ) {
            mins = extractValues( getArg("MIN"+i) );
            if( getArg("MAX"+i) != null ) {
              maxs = extractValues( getArg("MAX"+i) );
              for( j=0; j<mins.length; j++ ) {
                try {         
                  d = Double.parseDouble(mins[j]);
                  d = Double.parseDouble(maxs[j]);
                } catch (NumberFormatException e) {
                  throw new PPExecutionException( PB.getString( "parameterexistenz" )+"2" );
                }  
              }
            }
          }
        } else {
          jobres = "JOB_STATUS";
        }
        //Timeout
        temp = getArg( getOptionalArgs()[4] );
        if( temp == null ) endeZeit = System.currentTimeMillis();
        else               endeZeit = System.currentTimeMillis() + Long.parseLong(temp);
        //OK_Time
        temp = getArg( getOptionalArgs()[5] );
        if( temp == null ) okTime = -1;
        else               okTime = Long.parseLong(temp);
        //Pause
        temp = getArg( getOptionalArgs()[6] );
        if( temp == null ) pause = 0;
        else               pause = Long.parseLong(temp);
        //Anweisungstext
        awt = getArg( getOptionalArgs()[7] );
        if( awt == null ) awt="";
        else awt = PB.getString( awt );
        //Hinweistext
        hwt = getArg( getOptionalArgs()[8] );
        if( hwt == null ) hwt="";
        else hwt = PB.getString( hwt );
        // mehrere Hinweistexte einlesen, wenn vorhanden
        hwts=new String[resAnzahl];
        for ( i=0;i<resAnzahl;i++ ) 
            hwts[i] = getArg("HWT"+(i+1));
        //Invert
        temp=getAttribut("INVERT");
        if (temp!=null) {
           if (temp.equalsIgnoreCase("TRUE") == true) 
              invert=true;
           else
              invert=false;
        }
        // Der Parameter �berschreibt das Attribut, das Attribut f�llt sp�ter weg
        temp = getArg( getOptionalArgs()[9] );
        if ( (temp != null) && temp.equalsIgnoreCase("TRUE") ) invert = true; 
        if ( (temp != null) && temp.equalsIgnoreCase("FALSE") ) invert = false; 
        //IGNORE_ERRORS
        temp=getArg( getOptionalArgs()[10] );
        if ( (temp != null) && temp.equalsIgnoreCase("TRUE") ) ignore_errors = true;         
      } catch (PPExecutionException e) {
        if (e.getMessage() != null)
          result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
        else
          result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
        ergListe.add(result);
        throw new PPExecutionException();
      }

      //Die Ausf�hrungs- und Analyseschleife
      firstOkTime = Long.MAX_VALUE;
      do {

        ergListe = new Vector(); //Neu f�r jeden Durchlauf

        //Ausf�hrung
        try {
          temp = Ediabas.executeDiagJob(sgbd,job,jobpar,jobres);
          invalid = false;
          if( temp.equals("OKAY") == false) {
            result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
            ergListe.add(result);
            invalid = true;
            if (!ignore_errors) throw new PPExecutionException();
          } else if (resAnzahl == 0) {
            result = new Ergebnis("Status", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
            ergListe.add(result);
          }
        } catch( ApiCallFailedException e ) {
          result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), Ediabas.apiErrorCode()+": " + Ediabas.apiErrorText(), Ergebnis.FT_NIO );
          ergListe.add(result);
          invalid = true;
          if (!ignore_errors) throw new PPExecutionException();
        } catch( EdiabasResultNotFoundException e ) {
          if (e.getMessage() != null)
            result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
          else
            result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
          ergListe.add(result);
          throw new PPExecutionException();
        } catch( Exception e) {
          result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), Ediabas.apiErrorCode()+": " + Ediabas.apiErrorText(), Ergebnis.FT_NIO );
          ergListe.add(result);
          invalid = true;
          if (!ignore_errors) throw new PPExecutionException();
        }        //Analyse
        i = 0;
        try {
          status = STATUS_EXECUTION_OK;
          if (invalid == false) {
          for( i=1; i<=resAnzahl; i++ ) {
            temp = Ediabas.getDiagResultValue(results[i-1]);
            mins = extractValues( getArg("MIN"+i) );
            temp1 = mins[0];
            for( j=1; j<mins.length; j++) {temp1 = temp1 + ";" + mins[j]; } 
            if( getArg("MAX"+i) != null ) {
              maxs = extractValues( getArg("MAX"+i) );
              temp2 = maxs[0];
              for( j=1; j<maxs.length; j++) {temp2 = temp2 + ";" + maxs[j]; } 
            } else {
              maxs = null;
              temp2 = "";
            }
            if(maxs == null) {
              //Stringanalyse
              ok = false;
              for( j=0; j<mins.length; j++) {
                if( mins[j].equalsIgnoreCase(temp) == true ) ok = true;
              }
              if( (ok==true) && (invert == false) ) {
                //Toleranz, somit IO
                result = new Ergebnis( results[i-1], "EDIABAS", sgbd, job, jobpar, results[i-1], temp, temp1, "", "0", "", "", awt, "", "", Ergebnis.FT_IO );
              } else if( (ok==false) && (invert == true) ) {
                //Invertierte Toleranz, somit IO
                result = new Ergebnis( results[i-1], "EDIABAS", sgbd, job, jobpar, results[i-1], temp, "!("+temp1+")", "", "0", "", "", awt, "", "", Ergebnis.FT_IO );
              } else if( (ok==false) && (invert == false) ) {
                //Toleranz, somit NIO
                String hwtDummy;
                if ( hwts[i-1]!=null ) 
                    hwtDummy=hwts[i-1];
                else
                    hwtDummy=hwt;
                result = new Ergebnis( results[i-1], "EDIABAS", sgbd, job, jobpar, results[i-1], temp, temp1, "", "0", "", "", awt, PB.getString( "toleranzFehler1" ), hwtDummy, Ergebnis.FT_NIO );
                status = STATUS_EXECUTION_ERROR;
              } else {
                //Invertierte Toleranz mit ok == true, somit NIO
                String hwtDummy;
                if ( hwts[i-1]!=null ) 
                    hwtDummy=hwts[i-1];
                else
                    hwtDummy=hwt;
                result = new Ergebnis( results[i-1], "EDIABAS", sgbd, job, jobpar, results[i-1], temp, "!("+temp1+")", "", "0", "", "", awt, PB.getString( "toleranzFehler2" ), hwtDummy, Ergebnis.FT_NIO );
                status = STATUS_EXECUTION_ERROR;
              }  
            } else {
              //Zahlenanalyse
              d = Double.parseDouble(temp);
              ok = false;
              for( j=0; j<mins.length; j++) {
                if( (Double.parseDouble(mins[j]) <= d) && (Double.parseDouble(maxs[j]) >= d) ) ok = true;
              }
              if( (ok==true) && (invert == false) ) {
                //Toleranz, somit IO
                result = new Ergebnis( results[i-1], "EDIABAS", sgbd, job, jobpar, results[i-1], temp, temp1, temp2, "0", "", "", awt, "", "", Ergebnis.FT_IO );
              } else if( (ok==false) && (invert == true) ) {
                //Invertierte Toleranz, somit IO
                result = new Ergebnis( results[i-1], "EDIABAS", sgbd, job, jobpar, results[i-1], temp, "!("+temp1+")", "!("+temp2+")", "0", "", "", awt, "", "", Ergebnis.FT_IO );
              } else if( (ok==false) && (invert == false) ) {
                //Toleranz, somit NIO
                String hwtDummy;
                if ( hwts[i-1]!=null ) 
                    hwtDummy=hwts[i-1];
                else
                    hwtDummy=hwt;
                result = new Ergebnis( results[i-1], "EDIABAS", sgbd, job, jobpar, results[i-1], temp, temp1, temp2, "0", "", "", awt, PB.getString( "toleranzFehler3" ), hwtDummy, Ergebnis.FT_NIO );
                status = STATUS_EXECUTION_ERROR;
              } else {
                //Invertierte Toleranz mit ok == true, somit NIO
                String hwtDummy;
                if ( hwts[i-1]!=null ) 
                    hwtDummy=hwts[i-1];
                else
                    hwtDummy=hwt;
                result = new Ergebnis( results[i-1], "EDIABAS", sgbd, job, jobpar, results[i-1], temp, "!("+temp1+")", "!("+temp2+")", "0", "", "", awt, PB.getString( "toleranzFehler4" ), hwtDummy, Ergebnis.FT_NIO );
                status = STATUS_EXECUTION_ERROR;
              }
            } 

            ergListe.add(result);

            if( pause > 0 ) {
              try {
                Thread.sleep(pause);
              } catch( InterruptedException e ) {
              }
            }

          }
          } else {
              //handling when invalid == true
              status = STATUS_EXECUTION_ERROR;
          }
        } catch( ApiCallFailedException e ) {
          if( results[i-1] == null ) results[i-1] = "null";
          result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, results[i-1], "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), Ediabas.apiErrorCode()+": " + Ediabas.apiErrorText(), Ergebnis.FT_NIO );
          ergListe.add(result);
          throw new PPExecutionException();
        } catch( EdiabasResultNotFoundException e ) {
          if( results[i-1] == null ) results[i-1] = "null";
          if( e.getMessage() != null ) 
            result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, results[i-1], "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
          else
            result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, results[i-1], "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
          ergListe.add(result);
          throw new PPExecutionException();
        } catch( PPExecutionException e ) {
          if( results[i-1] == null ) results[i-1] = "null";
          if (e.getMessage() != null)
            result = new Ergebnis( "ParaFehler", "EDIABAS", sgbd, job, jobpar, results[i-1], "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" )+" Parameter", e.getMessage(), Ergebnis.FT_NIO_SYS );
          else
            result = new Ergebnis( "ParaFehler", "EDIABAS", sgbd, job, jobpar, results[i-1], "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" )+" Parameter", "", Ergebnis.FT_NIO_SYS );
          ergListe.add(result);
          throw new PPExecutionException();
        }

        if( okTime > 0 ) { //Unter Umst�nden Zeitdauer noch nicht erf�llt
          if( status == STATUS_EXECUTION_OK ) {
            if( firstOkTime > System.currentTimeMillis() ) {
              // Letzte Ausf�hrung war NIO
              firstOkTime = System.currentTimeMillis();
              result = new Ergebnis( "OK-Zeit", "", "", "", "", "Dauer", "0", ""+okTime, ""+Long.MAX_VALUE, "0", "", "", "", PB.getString( "okZeitUnterschritten" ), "", Ergebnis.FT_NIO );
              status = STATUS_EXECUTION_ERROR;
            } else {
              //Letzte bzw. die letzten Ausf�hrung waren IO
              deltaT = System.currentTimeMillis() - firstOkTime;
              if( deltaT < okTime ) {
                status = STATUS_EXECUTION_ERROR;
                result = new Ergebnis( "OK-Zeit", "", "", "", "", "Dauer", ""+deltaT, ""+okTime, ""+Long.MAX_VALUE, "0", "", "", "", PB.getString( "okZeitUnterschritten" ), "", Ergebnis.FT_NIO );
              } else {
                result = new Ergebnis( "OK-Zeit", "", "", "", "", "Dauer", ""+deltaT, ""+okTime, ""+Long.MAX_VALUE, "0", "", "", "", "", "", Ergebnis.FT_IO );
              }
            }  
            ergListe.add(result);
          } else {
            //Fehlerhafte Ausf�hrung, Zeitstempel-Reset
            firstOkTime = Long.MAX_VALUE;
          }
        }

        if( (status != STATUS_EXECUTION_OK) && (endeZeit > System.currentTimeMillis()) ) {
          //Nur wenn Loop nicht zuende
          if( awt.equals("") == false ) {
            if( udInUse == false ) {
              //nur wenn was auszugeben ist und dieses noch nicht ausgegeben wurde
              myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
              myDialog.setAllowCancel(true);
              myDialog.displayStatusMessage( awt+" "+PB.getString( "abbruchDurchWerker" ), -1 );
              udInUse = true;
            } else {
              if( myDialog.isCancelled() == true ) {
                result = new Ergebnis( "Frage", "Userdialog", "", "", "", "", "", "", "", "0", awt, "OK", "", PB.getString( "abbruchDurchWerker" ), "", Ergebnis.FT_NIO );
                ergListe.add(result);
                throw new PPExecutionException();
              }
            }  
          }
        }
      
      } while( (endeZeit > System.currentTimeMillis()) && (status != STATUS_EXECUTION_OK) );

    } catch (PPExecutionException e) {
      status = STATUS_EXECUTION_ERROR;
    } catch (Exception e) {
      if (e instanceof DeviceLockedException)
        result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
      else if (e instanceof DeviceNotAvailableException)
        result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
      else
        result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
      ergListe.add(result);
      status = STATUS_EXECUTION_ERROR;
    } catch (Throwable e) {
      result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
      ergListe.add(result);
      status = STATUS_EXECUTION_ERROR;
    }                     

    //Freigabe der benutzten Devices
    if( udInUse == true ) {
      try {
        getPr�flingLaufzeitUmgebung().releaseUserDialog();
      } catch (Exception e) {
        if (e instanceof DeviceLockedException)
          result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
        else
          result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
        ergListe.add(result);
        status = STATUS_EXECUTION_ERROR;
      }
    }
    
    //Status setzen
    setPPStatus( info, status, ergListe );

  }


}


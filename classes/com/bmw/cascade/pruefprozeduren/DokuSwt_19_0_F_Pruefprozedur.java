/*
 * DokuSwt_13_0_F_Pruefprozedur.java
 *
 * Created on 07.09.01
 */
package com.bmw.cascade.pruefprozeduren;


import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.ediabas.*;
import com.bmw.cascade.util.dom.DomPruefstand;
import com.bmw.cascade.auftrag.*;
import com.rssit.swt.fstool.*;

import sun.misc.BASE64Decoder;

/**
 * @author Mueller / Merz / Seitz
 * @version Implementierung
 * 10.02.2005 rm 0.0.9  Erweiterung um ecuData f�r Night Vision Kamera
 *                      Speicherung der Seriennummer Kamera
 * 10.02.2005 nc 0.0.10  DOM Transfer korrigiert
 * 05.08.2005 rm 0.0.11  ApplID HEX->DEZ Wandlung integriert
 * 24.11.2005 rm 0.0.12  Erweiterung um BASE32 Codierung FSC Short
 * 30.08.2006 rm 0.0.13  F�hrende Null wird in der NVC Seriennummer abgeschnitten
 * 25.01.2008 sz 0.0.14  Erweiterung Seriennummer f�r FSC 0005 f�r NIVI2 SA 6UK
 * 26.03.2009 sz 0.0.15  Testversion
 * 26.03.2009 sz 0.0.16  Testversion
 * 26.03.2009 sz 0.0.17  Testversion 
 * 26.03.2009 sz 0.0.18  Schreiben des S File nur wenn FSCs im Auftrag
 * 15.10.2009 sz 0.0.19  NIVI Sonderbehandlung f�r Auftr�ge mit NIVI FSCs ohne NIVI SA
 */


public class DokuSwt_19_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    	static final long serialVersionUID = 1L;
		
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public DokuSwt_19_0_F_Pruefprozedur() {}

  /**
     * erzeugt eine neue Pruefprozedur mit obigem Verhalten
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public DokuSwt_19_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    
    /**
     * initialsiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /**
     * liefert die optionalen Argumente
     */
    public String[] getOptionalArgs() {
        String[] args = new String[0];
        return args;
    }
    
    /**
     * liefert die zwingend erforderlichen Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = new String[0];
        return args;
    }
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert!!!
     */
    public boolean checkArgs() {
        return true;
    }
    
    
    /**
     * f�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) {
        // Always required
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        StringBuffer xml = null;
        Auftrag myAuftrag = null;
        
        // Collect information
        xml = new StringBuffer();
        String az = "\"";

        // execution diagnostic job
        String sgbd   ="";
        String jobpar ="";
        String job    ="";
        String temp   ="";
        String serNr  ="";
        
        boolean isNIVI2 = false;
        
        
        // Header ist immer gleich
        xml.append("<?xml version="+az+"1.0"+az+"?>"+"\r\n");
        xml.append("<!DOCTYPE fscPackage SYSTEM "+az+"fscPackageV0301.dtd"+az+">"+"\r\n");
        
        try {

        	myAuftrag = getPr�fling().getAuftrag();

        	
        	if ( myAuftrag.getFscGeneralInformation()==null ) {
        		// no FSCs available. IO and skip creation of S File
        		result = new Ergebnis("Status", "", "", "", "", "STATUS", "VEHICLE HAS NO FSCs", "0", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
        		ergListe.add(result);
        		
        	} else { // FSCs are available, create S File

        		xml.append("<fscPackage status=\"").append(myAuftrag.getFscGeneralInformation().get("STATUS"));
        		xml.append("\" version=\"").append(myAuftrag.getFscGeneralInformation().get("VERSION")).append("\">\r\n");

        		xml.append("\t<orderInfo entityType=\"").append(myAuftrag.getFscGeneralInformation().get("ENTITY_TYPE"));
        		xml.append("\" entityID=\"").append(myAuftrag.getFscGeneralInformation().get("ENTITY_ID"));
        		xml.append("\" vinShort=\"").append(getPr�fling().getAuftrag().getFahrgestellnummer7()).append("\">");
        		xml.append(myAuftrag.getFscGeneralInformation().get("ORDER_INFO")).append("</orderInfo>\r\n");            	

        		if ( myAuftrag.getFscData()!=null ) {
        			if ( myAuftrag.getFscData().length > 0 ) {
        				xml.append("\t<fscContainers creationTime=\"").append(myAuftrag.getFscGeneralInformation().get("CREATION_TIME"));
        				xml.append("\" fscEntity=\"").append(myAuftrag.getFscGeneralInformation().get("FSC_ENTITY")).append("\">\r\n");
        				for ( int i=0; i<myAuftrag.getFscData().length; i++ ) {
        					int applNo;
        					try {
        						applNo = Integer.parseInt(((String) (myAuftrag.getFscData()[i].get("APPLICATION_NO"))),16);
        					} catch (NumberFormatException nfe) {
        						nfe.printStackTrace();
        						result = new Ergebnis( "DatenFehler", "ApplNo", "", "", "", "", "", "", "", "0", "", "", "", "InvalidFscValueException", PB.getString("fscfehlerhaft"), Ergebnis.FT_NIO );
        						ergListe.add(result);
        						throw new PPExecutionException();
        					}

        					// skip when NIVI FSCs within XML Auftrag but neither SA 6UK nor SA 611
        					if (applNo==5 || applNo==4 )
        					{
        						if (!myAuftrag.containsSA("06UK"))
        							if (!myAuftrag.containsSA("0611"))
        								continue;						
        					}
        					// -
        					
        					xml.append("\t\t<fscContainer>\r\n");
        					xml.append("\t\t\t<assemblyPartNo>").append(myAuftrag.getFscData()[i].get("ASSEMBLY_PART_NO")).append("</assemblyPartNo>\r\n");
        					xml.append("\t\t\t<changeID>").append(myAuftrag.getFscData()[i].get("CHANGE_ID")).append("</changeID>\r\n");
        					// OrderNummer aus dem FSC parsen
        					String purchaseOrderNo=null;
        					try {
        						String sFSC = (((String)(myAuftrag.getFscData()[i].get("FSC"))).trim());
        						if ( sFSC.length() == 20 )
        						{
        							// BASE32 codiert, FSC Short direkt �bergeben
        							/*
	                            byte[] binaryFsc = (byte[])myAuftrag.getFscData()[i].get("FSC");
	                            Fsc myFsc=new Fsc();
	                            myFsc.parseFSC(binaryFsc);
	                            purchaseOrderNo=(new String(myFsc.getBestell_Nr())).trim();
        							 */
        							// BASE32 hat keine Order No
        							purchaseOrderNo="0";                       		
        						}
        						else
        						{
        							// Zun�chst muss die BASE64 Decodierung erfolgen
        							BASE64Decoder myDecoder = new BASE64Decoder();
        							byte[] binaryFsc = myDecoder.decodeBuffer( ((String)(myAuftrag.getFscData()[i].get("FSC"))) );
        							Fsc myFsc=new Fsc();
        							myFsc.parseFSC(binaryFsc);
        							purchaseOrderNo=(new String(myFsc.getBestell_Nr())).trim();
        						}
        					} catch (Exception ifve) {
        						ifve.printStackTrace();
        						result = new Ergebnis( "DatenFehler", "FSCparsing", "", "", "", "", "", "", "", "0", "", "", "", "InvalidFscValueException", PB.getString("fscfehlerhaft"), Ergebnis.FT_NIO );
        						ergListe.add(result);
        						throw new PPExecutionException();
        					}
        					xml.append("\t\t\t<purchaseOrderNo>").append(purchaseOrderNo).append("</purchaseOrderNo>\r\n");
        					xml.append("\t\t\t<fscID>").append(myAuftrag.getFscData()[i].get("FSC_ID")).append("</fscID>\r\n");
        					xml.append("\t\t\t<swID>\r\n");
        					xml.append("\t\t\t\t<upgradeIndex>").append(myAuftrag.getFscData()[i].get("UPGRADE_INDEX")).append("</upgradeIndex>\r\n");
        					xml.append("\t\t\t\t<applicationNo>").append(myAuftrag.getFscData()[i].get("APPLICATION_NO")).append("</applicationNo>\r\n");
        					xml.append("\t\t\t</swID>\r\n");                        
        					xml.append("\t\t\t<diagnoseAddr>").append(myAuftrag.getFscData()[i].get("DIAGNOSE_ADRESSE")).append("</diagnoseAddr>\r\n");

        					if ( applNo==5 ) {
        						// Freischaltcodes mit Applikationsnummer 5 (Night Vision Kamera) Seriennummer speichern

        						// Diagnosejob ausf�hren
        						sgbd   = "";
        						job    = "";
        						jobpar = "";
        						// NIVI2 ECU?
        						if (myAuftrag.containsSA("06UK"))  
        						{
        							isNIVI2 = true;
        							sgbd   = "NIVI2";
        							job    = "SENSOREN_IDENT_LESEN";
        							jobpar = "";                            	
        						}
        						// NIVI1 ECU?
        						else if (myAuftrag.containsSA("0611"))
        						{
        							isNIVI2 = false;
        							sgbd   = "NVC_65";
        							job    = "seriennummer_lesen";
        							jobpar = "";                            	
        						} else {
        							result = new Ergebnis ( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", "Sonderausstattung NIVI nicht gefunden", "Sonderausstattung NIVI nicht gefunden", Ergebnis.FT_NIO );
        							ergListe.add(result);
        							throw new PPExecutionException();                            	
        						}
        						try {
        							temp = Ediabas.executeDiagJob(sgbd,job,jobpar,""); 
        							if( temp.equals("OKAY") == false) {
        								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
        								ergListe.add(result);
        								throw new PPExecutionException();
        							}
        							else {
        								result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
        								if (isNIVI2) {
        									String tmpSerNr = Ediabas.getDiagResultValue("SENSOR_PART_NR");
        									String serNrShort = tmpSerNr.substring(16, 24);
        									String serByte1 = serNrShort.substring(6, 8);
        									String serByte2 = serNrShort.substring(4, 6);
        									String serByte3 = serNrShort.substring(2, 4);
        									String serByte4 = serNrShort.substring(0, 2);
        									serNr = serByte1 + serByte2 + serByte3 + serByte4;                                    	
        								} else {
        									serNr = Ediabas.getDiagResultValue("SERIENNUMMER");                                    	
        								}
        								ergListe.add(result);
        							}
        						} catch( ApiCallFailedException e ) {
        							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), Ediabas.apiErrorCode()+": " + Ediabas.apiErrorText(), Ergebnis.FT_NIO );
        							ergListe.add(result);
        							throw new PPExecutionException();
        						} catch( EdiabasResultNotFoundException e ) {
        							if (e.getMessage() != null)
        								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
        							else
        								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
        							ergListe.add(result);
        							throw new PPExecutionException();
        						}   
        						// ACHTUNG: Da die Seriennummer hexadezimal im SG steht, ist diese nur 8 stellig.
        						// Der Standard-Diagnosejob liefert aber eine 9 stellige Nummer mit einer f�hrenden 0.
        						// Da die nachfolgenden Systeme eine 8 stellige Nummer erwarten, wird hier diese Korrektur durchgef�hrt
        						if (( serNr.length() > 8) && (serNr.startsWith("0")))
        						{
        							// F�hrende Null abschneiden
        							serNr = serNr.substring(1);
        						}                            	
        						xml.append("\t\t\t<ecuData><![CDATA[<ecuSNR>").append(serNr).append("</ecuSNR>]]></ecuData>\r\n");                            
        					}

        					xml.append("\t\t\t<fsc code=\"").append(myAuftrag.getFscData()[i].get("CODE"));                       
        					if ( applNo==5 ) {
        						// Freischaltcodes mit Applikationsnummer 5 FSC nicht speichern (Night Vision Kamera)                         
        						xml.append("\">").append("EMPTY").append("</fsc>\r\n");
        					}
        					else {
        						xml.append("\">").append(((String)myAuftrag.getFscData()[i].get("FSC")).trim()).append("</fsc>\r\n");
        					}                        
        					xml.append("\t\t</fscContainer>\r\n");
        				}
        				xml.append("\t</fscContainers>\r\n");
        			}
        		}
        		xml.append("</fscPackage>");

        		try {
        			DomPruefstand.writeDomData( getPr�fling().getAuftrag().getFahrgestellnummer7(), "S", xml.toString().getBytes() );
        		} catch(Exception e) {
        			result = new Ergebnis( "ExecFehler", "DomTransfer", "", "", "", "", "", "", "", "0", "", "", "", "FileWrite", ""+e.getMessage(), Ergebnis.FT_NIO_SYS );
        			ergListe.add(result);
        			throw new PPExecutionException();
        		}
        	}
        	//IO-Ablauf als Status festhalten
        	result = new Ergebnis("Status", "", "", "", "", "STATUS", ""+status, ""+STATUS_EXECUTION_OK, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
        	ergListe.add(result);


        } catch (PPExecutionException e) {
        	result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
        	ergListe.add(result);
        	status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
        	result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
        	e.printStackTrace();
        	ergListe.add(result);
        	status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
        	result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
        	ergListe.add(result);
        	e.printStackTrace();
        	status = STATUS_EXECUTION_ERROR;
        }
        
        setPPStatus( info, status, ergListe );
    }
}


package com.bmw.cascade.pruefprozeduren;

import java.util.Vector;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.psdz.PSdZ;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementierung einer Pr�fprozedur, die das VCM bef�llt
 * 
 * @author Mueller, BMW AG; Buboltz, BMW AG; Dieter Busetti, GEFASOFT AG; Gampl, BMW AG <BR>
 *         13.06.2007 TB Parameter WRITE_SVT -> WRITE_SVT_SOLL geaendert Typ boolean <BR>
 *         12.10.2007 DB Fehlertexte korrigiert (Sprachauswahl) <BR>
 *         03.01.2008 DB Einbau "testScreenID" in Debug-Ausgaben <BR>
 *         24.06.2008 DB �nderung holen des "testScreenID" <BR>
 *         24.09.2013 MG T-Version (6_0) <BR>
 *         ---------- -- - LocalizedMessage im Fehlerfall bei allen Services zus�tzlich als Fehlertext mit ausgeben <BR>
 *         17.10.2013 MG F-Version (7_0) <BR>
 *         09.10.2014 MG T-Version (8_0) <BR>
 *         ---------- -- - Generics und zus�tzliche Debugausgaben hinzu <BR>
 *         ---------- -- - Codebereinigung (Ausbau alter Methode: clearPSdZExceptions()); Neue CASCADE Mindestversion 6.1.0 <BR>
 *         20.10.2014 MG F-Version (9_0) <BR>
 *         29.01.2015 MG T-Version (10_0) <BR>
 *         ---------- -- - I_STUFE_SOLL, die im Regelfall aus dem FA kommt, soll im Nutzerdialog nur angezeigt werden, wenn diese auch geschrieben wird, da bei I-Stufen-Update eines Fahrzeuges ohne Schreiben der I-Stufe die I_STUFE_SOLL (FA) <> I_STUFE_SOLL (Fahrzeug) ist. Unn�tige Benutzerirritationen werden so vermieden. <BR>
 *         ---------- -- - LOP 1853: Ausgabe (in Log-Datei Pr�fstandScreen) der Pr�fprozedurversion sowie s�mtlicher optionaler und nicht optionaler Argumente, die an die Pr�fprozedur �bergeben werden zum Zweck der Nachvollziehbarkeit im Fehlerfall. <BR>
 *         10.02.2015 MG T-Version (11_0) <BR>
 *         ---------- -- - LOP 1899: Einheitliche Darstellung der Fehlerausgaben. <BR>
 *         18.02.2015 MG F-Version (12_0) <BR>
 * 
 */
public class PSdZVCMWrite_12_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSdZVCMWrite_12_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSdZVCMWrite_12_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG", "DEBUG_PERFORM", "GENERATE_LIST_OF_SERIAL_NO", "UPDATE_VCM_BACKUP", "WRITE_FA", "WRITE_I_STEP", "WRITE_SVT_SOLL" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = { "I_STUFE" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		if( !super.checkArgs() )
			return false;
		return true;
	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; // default english
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {
		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
		boolean bDebugPerform = true; // gibt an, ob eine Performencemessung erfolgen sollen
		boolean bGenerateListOfSerialNo = false; // gibt an, ob eine Liste mit Serien-Nummern generiert werden soll
		boolean bUpdateVCMBackup = true;
		boolean bWriteFA = true;
		boolean bWriteIStep = true;
		boolean bWriteSVTSoll = true;
		final boolean DE = checkDE(); // Systemsprache DE wenn true
		int status = STATUS_EXECUTION_OK;

		Ergebnis result;
		PSdZ psdz = null;
		String errorDescription = DE ? " Fehlerbeschreibung: " : " Error Description: ";
		String errorText = null;
		String iStufe = null;
		String optionalArgumentName = null;
		String optionalArgumentValue = null;
		String requiredArgumentName = null;
		String requiredArgumentValue = null;
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";
		String testScreenID = this.getPr�flingLaufzeitUmgebung().getName(); // TestScreenID (aus Pr�fstandsinstanz)
		String userDialogMessage = null;
		String[] optionalArgumentsNames = getOptionalArgs();
		String[] optionalArgumentsValues = null;
		String[] requiredArgumentsNames = getRequiredArgs();
		String[] requiredArgumentsValues = null;
		UserDialog userDialog = null;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();

		try {
			// Argumente loggen, einlesen und checken
			try {

				// Ausgabe der Pr�fprozedurversion sowie s�mtlicher an die Pr�fprozedur �bergebener Argumente (required/optional) f�r sp�tere Analysezwecke

				System.out.println( "PSdZ ID:" + testScreenID + ", Initialize " + this.getClassName() );

				for( int i = 0; i < requiredArgumentsNames.length; i++ ) {
					requiredArgumentName = requiredArgumentsNames[i];
					requiredArgumentValue = "";
					// gibt es nicht optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( requiredArgumentName ) != null ) {
						requiredArgumentsValues = extractValues( getArg( requiredArgumentName ) );
						for( int k = 0; k < requiredArgumentsValues.length; k++ ) {
							// Listen von nicht optionalen Argumenten werden durch ";" getrennt ausgegeben
							requiredArgumentValue = requiredArgumentValue + requiredArgumentsValues[k] + ((requiredArgumentsValues.length > 1 && k < (requiredArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						requiredArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZVCMWrite with required argument: " + requiredArgumentName + " = " + requiredArgumentValue );
				}

				for( int i = 0; i < optionalArgumentsNames.length; i++ ) {
					optionalArgumentName = optionalArgumentsNames[i];
					optionalArgumentValue = "";
					// gibt es optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( optionalArgumentName ) != null ) {
						optionalArgumentsValues = extractValues( getArg( optionalArgumentName ) );
						for( int k = 0; k < optionalArgumentsValues.length; k++ ) {
							// Listen von optionalen Argumenten werden durch ";" getrennt ausgegeben
							optionalArgumentValue = optionalArgumentValue + optionalArgumentsValues[k] + ((optionalArgumentsValues.length > 1 && k < (optionalArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						optionalArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZVCMWrite with optional argument: " + optionalArgumentName + " = " + optionalArgumentValue );
				}

				// Obligatorische Argumentpr�fung auf Vorhandensein von nicht optionalen sowie �berz�hligen Argumenten
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
					else
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DEBUG_PERFORM
				if( getArg( "DEBUG_PERFORM" ) != null ) {
					if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "TRUE" ) )
						bDebugPerform = true;
					else if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "FALSE" ) )
						bDebugPerform = false;
					else
						throw new PPExecutionException( "DEBUG_PERFORM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// ISTUFE
				iStufe = getArg( "I_STUFE" );

				// GENERATE_LIST_OF_SERIAL_NO
				if( getArg( "GENERATE_LIST_OF_SERIAL_NO" ) != null ) {
					if( getArg( "GENERATE_LIST_OF_SERIAL_NO" ).equalsIgnoreCase( "TRUE" ) )
						bGenerateListOfSerialNo = true;
					else if( getArg( "GENERATE_LIST_OF_SERIAL_NO" ).equalsIgnoreCase( "FALSE" ) )
						bGenerateListOfSerialNo = false;
					else
						throw new PPExecutionException( "GENERATE_LIST_OF_SERIAL_NO " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// UPDATE_VCM_BACKUP
				if( getArg( "UPDATE_VCM_BACKUP" ) != null ) {
					if( getArg( "UPDATE_VCM_BACKUP" ).equalsIgnoreCase( "TRUE" ) )
						bUpdateVCMBackup = true;
					else if( getArg( "UPDATE_VCM_BACKUP" ).equalsIgnoreCase( "FALSE" ) )
						bUpdateVCMBackup = false;
					else
						throw new PPExecutionException( "UPDATE_VCM_BACKUP " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// WRITE_FA
				if( getArg( "WRITE_FA" ) != null ) {
					if( getArg( "WRITE_FA" ).equalsIgnoreCase( "TRUE" ) )
						bWriteFA = true;
					else if( getArg( "WRITE_FA" ).equalsIgnoreCase( "FALSE" ) )
						bWriteFA = false;
					else
						throw new PPExecutionException( "WRITE_FA " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// WRITE_I_STEP
				if( getArg( "WRITE_I_STEP" ) != null ) {
					if( getArg( "WRITE_I_STEP" ).equalsIgnoreCase( "TRUE" ) )
						bWriteIStep = true;
					else if( getArg( "WRITE_I_STEP" ).equalsIgnoreCase( "FALSE" ) )
						bWriteIStep = false;
					else
						throw new PPExecutionException( "WRITE_I_STEP " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// WRITE_SVT_SOLL
				if( getArg( "WRITE_SVT_SOLL" ) != null ) {
					if( getArg( "WRITE_SVT_SOLL" ).equalsIgnoreCase( "TRUE" ) )
						bWriteSVTSoll = true;
					else if( getArg( "WRITE_SVT_SOLL" ).equalsIgnoreCase( "FALSE" ) )
						bWriteSVTSoll = false;
					else
						throw new PPExecutionException( "WRITE_SVT_SOLL " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			// Performancemessung Start
			if( bDebugPerform )
				System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVCMWrite PP START" );

			// Devices holen
			try {
				psdz = getPr�flingLaufzeitUmgebung().getDeviceManager().getPSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			try {
				userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Erstelle die Dialogmeldung abh�ngig von dem Parameter WRITE_I_STEP 
			// Hintergrund: I_STUFE_SOLL soll nur angezeigt werden, wenn diese auch geschrieben wird. Wird diese nicht geschrieben 
			// (z.B. im Nacharbeitsfall mit I-Stufenhub wo I-Stufe des Fahrzeuges ungleich I-Stufe aus FA (->I_STUFE_SOLL)), so wird diese auch nicht angezeigt und f�hrt so zu keiner Verwirrung der Anwender mehr.
			userDialogMessage = "WRITE_FA: " + bWriteFA + ";WRITE_I_STEP: " + bWriteIStep + ";WRITE_SVT_SOLL: " + bWriteSVTSoll + ";UPDATE_VCM_BACKUP: " + bUpdateVCMBackup + ";GENERATE_LIST_OF_SERIAL_NO: " + bGenerateListOfSerialNo + ";  " + PB.getString( "psdz.ud.VcmWriteLaeuft" );
			if( bWriteIStep )
				userDialogMessage = "I_STUFE_SOLL: " + iStufe + ";" + userDialogMessage;

			// Informiere den Benutzer
			userDialog.setDisplayProgress( true );
			userDialog.getProgressbar().setIndeterminate( true );
			userDialog.displayMessage( "PSdZ VCM WRITE", userDialogMessage, -1 );

			// Write FA and FP to VCM or Backup
			if( bWriteFA ) {
				try {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVCMWrite 'psdz.updateFAandFPinVCMorBackup' START" );
					}

					psdz.updateFAandFPinVCMorBackup( bUpdateVCMBackup );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVCMWrite 'psdz.updateFAandFPinVCMorBackup' ENDE" );
					}

				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					if( bUpdateVCMBackup )
						errorText = (DE ? "Fehler beim Schreiben des FA und/oder FP in VCM und/oder VCM_BACKUP!" : "Error while writing of FA and/or FP into VCM and/or VCM_BACKUP!");
					else
						errorText = (DE ? "Fehler beim Schreiben des FA und/oder FP in VCM!" : "Error while writing of FA and/or FP into VCM!");
					// falls Exception noch eine weitere Nachricht enth�lt, wird diese als zus�ztliche Fehlerbeschreibung mit ausgegeben
					if( e.getLocalizedMessage() != null )
						errorText = errorText + errorDescription + e.getLocalizedMessage();
					result = new Ergebnis( "PSdZVCMWrite", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText, "", Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

			// Write I-Step to VCM or Backup
			if( bWriteIStep ) {
				try {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVCMWrite 'psdz.setIStufeSoll' START" );
					}

					psdz.setIStufeSoll( iStufe );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVCMWrite 'psdz.setIStufeSoll' ENDE" );
					}
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVCMWrite 'psdz.updateIStepinVCMorBackup' START" );
					}

					psdz.updateIStepinVCMorBackup( bUpdateVCMBackup );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVCMWrite 'psdz.updateIStepinVCMorBackup' ENDE" );
					}

				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					if( bUpdateVCMBackup )
						errorText = (DE ? "Fehler beim Schreiben der I-Stufe in VCM und/oder VCM_BACKUP!" : "Error while writing I-Step into VCM and/or VCM_BACKUP!");
					else
						errorText = (DE ? "Fehler beim Schreiben der I-Stufe in VCM!" : "Error while writing I-Step into VCM!");
					// falls Exception noch eine weitere Nachricht enth�lt, wird diese als zus�ztliche Fehlerbeschreibung mit ausgegeben
					if( e.getLocalizedMessage() != null )
						errorText = errorText + errorDescription + e.getLocalizedMessage();
					result = new Ergebnis( "PSdZVCMWrite", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText, "", Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

			// Write SVT-SOLL
			if( bWriteSVTSoll ) {
				try {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVCMWrite 'psdz.setSVTinVCM' START" );
					}

					// Schreibe Daten
					psdz.setSVTinVCM( bGenerateListOfSerialNo );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVCMWrite 'psdz.setSVTinVCM' ENDE" );
					}

				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					errorText = e.getLocalizedMessage();
					result = new Ergebnis( "PSdZVCMWrite", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Fehler beim Schreiben der SVT-Soll in VCM!" : "Error while writing target SVT into VCM!") + (errorText != null ? errorDescription + errorText : ""), "", Ergebnis.FT_NIO );
					ergListe.add( result );

					status = STATUS_EXECUTION_ERROR;
				}
			}

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Freigabe der benutzten Devices
		if( psdz != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releasePSdZ();
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
		try {
			getPr�flingLaufzeitUmgebung().releaseUserDialog();
		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			if( e instanceof DeviceLockedException )
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "PSdZVCMWrite", "PSdZ", "I-STUFE:=" + iStufe + ", WRITE_FA:=" + bWriteFA + ", WRITE_I_STEP:=" + bWriteIStep + ", WRITE_SVT_SOLL:=" + bWriteSVTSoll + ", UPDATE_VCM_BACKUP:=" + bUpdateVCMBackup + ", GENERATE_LIST_OF_SERIAL_NO:=" + bGenerateListOfSerialNo + ", DEBUG:=" + bDebug + ", DEBUG_PERFORM:=" + bDebugPerform, "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}

		// Status setzen
		setPPStatus( info, status, ergListe );

		// Performancemessung Ende
		if( bDebugPerform )
			System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZVCMWrite PP ENDE" );
	}
}

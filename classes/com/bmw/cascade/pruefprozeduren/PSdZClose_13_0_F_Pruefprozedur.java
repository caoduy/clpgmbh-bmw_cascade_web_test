package com.bmw.cascade.pruefprozeduren;

import java.util.Vector;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.psdz.PSdZ;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementierung einer Pr�fprozedur, die die Initialisierung des PSdZ vornimmt
 * 
 * @author Mueller, BMW AG; Buboltz, BMW AG; Busetti, GEFASOFT AG; Gampl, BMW AG <BR>
 * 
 *         02.04.2008 -- Ausgaben bei Import <BR>
 *         24.06.2008 DB �nderung holen des "testScreenID" <BR>
 *         07.10.2014 MG <BR>
 *         ---------- -- - Generics hinzu <BR>
 *         ---------- -- - Codebereinigung (Ausbau alter Methode: clearPSdZExceptions()); neue CASCADE Mindestversion 6.1.0 <BR>
 *         20.10.2014 MG F-Version (9_0), Mindestversionsanforderung CASCADE 6.1.0 / PSdZ 4.9.1 <BR>
 *         14.07.2015 MG T-Version (10_0), Mindestversionsanforderung CASCADE 6.1.0 / PSdZ 4.9.1 <BR>
 *         ---------- -- - LOP 1853: Ausgabe (in Log-Datei Pr�fstandScreen) der Pr�fprozedurversion sowie s�mtlicher optionaler Argumente, die an die Pr�fprozedur �bergeben werden zum Zweck der Nachvollziehbarkeit im Fehlerfall <BR>
 *         ---------- -- - LOP 1899: Einheitliche Darstellung der Fehlerausgaben in PSdZ-Pr�fprozeduren <BR>
 *         28.07.2015 MG F-Version (11_0), Mindestversionsanforderung CASCADE 6.1.0 / PSdZ 4.9.1 <BR>
 *         09.11.2016 MG T-Version (12_0), Mindestversionsanforderung CASCADE 8.0.0 / PSdZ 5.02.00 <BR>
 *         ---------- -- - LOP 2126: VIN spezifisches PSdZ-Logging <BR>
 *         ---------- -- Altcode zum Start eines PDX-Imports nach Schlie�en der Fahrzeugverbindung ausgebaut, da Kapselung der vollst�ndigen PDX-Import-Funktionalit�t in PDXImportBL erfolgt ist <BR>
 *         06.09.2017 MG F-Version (13_0), Mindestversionsanforderung CASCADE 8.0.0 / PSdZ 5.02.00 <BR>
 * 
 */
public class PSdZClose_13_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DE fuer Sprachermittlung
	 */
	private final boolean DE = checkDE(); // Systemsprache DE wenn true

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSdZClose_13_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSdZClose_13_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG", "DEBUG_PERFORM" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = {};
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		if( !super.checkArgs() )
			return false;
		return true;
	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {

		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
		boolean bDebugPerform = true; // gibt an, ob eine Performencemessung erfolgen sollen
		int status = STATUS_EXECUTION_OK;

		Ergebnis result;
		PSdZ psdz = null;
		String closeVehicleConnectionFailed = DE ? "Schlie�en der Fahrzeugverbindung fehlgeschlagen. " : "Close vehicle connection failed. ";
		String optionalArgumentName = null;
		String optionalArgumentValue = null;
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";
		String testScreenID = this.getPr�flingLaufzeitUmgebung().getName(); // TestScreenID (aus Pr�fstandsinstanz)
		String[] optionalArgumentsNames = getOptionalArgs();
		String[] optionalArgumentsValues = null;
		UserDialog userDialog = null;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();

		try {

			// Argumente loggen, einlesen und checken
			try {

				// Ausgabe der Pr�fprozedurversion sowie s�mtlicher an die Pr�fprozedur �bergebener Argumente (required/optional) f�r sp�tere Analysezwecke

				System.out.println( "PSdZ ID:" + testScreenID + ", Initialize " + this.getClassName() );

				for( int i = 0; i < optionalArgumentsNames.length; i++ ) {

					optionalArgumentName = optionalArgumentsNames[i];
					optionalArgumentValue = "";
					// gibt es optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( optionalArgumentName ) != null ) {
						optionalArgumentsValues = extractValues( getArg( optionalArgumentName ) );
						for( int k = 0; k < optionalArgumentsValues.length; k++ ) {
							// Listen von optionalen Argumenten werden durch ";" getrennt ausgegeben
							optionalArgumentValue = optionalArgumentValue + optionalArgumentsValues[k] + ((optionalArgumentsValues.length > 1 && k < (optionalArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						optionalArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZClose with optional argument: " + optionalArgumentName + " = " + optionalArgumentValue );
				}

				// Obligatorische Argumentpr�fung auf Vorhandensein von nicht optionalen sowie �berz�hligen Argumenten
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG
				if( getArg( "DEBUG" ) != null ) {
					if( extractValues( getArg( "DEBUG" ) )[0].equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( extractValues( getArg( "DEBUG" ) )[0].equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
					else
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DEBUG_PERFORM
				if( getArg( "DEBUG_PERFORM" ) != null ) {
					if( extractValues( getArg( "DEBUG_PERFORM" ) )[0].equalsIgnoreCase( "TRUE" ) )
						bDebugPerform = true;
					else if( extractValues( getArg( "DEBUG_PERFORM" ) )[0].equalsIgnoreCase( "FALSE" ) )
						bDebugPerform = false;
					else
						throw new PPExecutionException( "DEBUG_PERFORM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Devices holen
			try {
				psdz = getPr�flingLaufzeitUmgebung().getDeviceManager().getPSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			try {
				userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Performancemessung Start
			if( bDebugPerform )
				System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZClose PP START" );

			// Schliessen des Devices
			try {

				userDialog.displayMessage( "PSdZClose", PB.getString( "psdz.ud.PSdZEnde" ), -1 );
				userDialog.setDisplayProgress( true );
				userDialog.getProgressbar().setIndeterminate( true );

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZClose 'psdz.closeVehicleConnection' START" );
				}

				psdz.closeVehicleConnection();

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZClose 'psdz.closeVehicleConnection' Ende" );
				}

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZClose 'psdz.close' START" );
				}

				psdz.close( true );

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZClose 'psdz.close' Ende" );
				}

				userDialog.reset();
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZClose", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", closeVehicleConnectionFailed + e.getLocalizedMessage(), "Exception", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Freigabe der benutzten Devices
		if( psdz != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releasePSdZ();
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
		try {
			getPr�flingLaufzeitUmgebung().releaseUserDialog();
		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			if( e instanceof DeviceLockedException )
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "PSdZClose", "PSdZ", "", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}
		setPPStatus( info, status, ergListe );

		// Performancemessung Ende
		if( bDebugPerform )
			System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZClose PP ENDE" );
	}

}

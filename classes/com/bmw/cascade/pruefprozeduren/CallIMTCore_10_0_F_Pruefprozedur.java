/*
 * CallIMTCore_10_0_F.java
 *
 * Created on 18.3.2015
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.jar.Attributes;
import java.util.jar.Attributes.Name;
import java.util.jar.JarInputStream;
import java.util.jar.Manifest;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.PrueflingLaufzeitUmgebung;
import com.bmw.cascade.pruefstand.Pruefstand;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.util.dom.DomException;
import com.bmw.cascade.util.dom.DomPruefstand;
import com.bmw.cascade.util.dynamicdevices.Version;
import com.bmw.cascade.util.dynamicdevices.Version.VersionType;

/**
 * Search for a matching version of the core jar file (e.g. IMT_CASCADE_CORE_10_0_F.jar) and execute it.
 * 
 * @author Peter Winklhofer / Rolf Antony (Softing) / Andreas Bernhauser (Softing)
 * 
 * @version V1_0_F 16.06.2015 PW initial version
 * @version V2_0_F 12.04.2016 RA Redesign of the classloader mechanism and workaround for the keywriter JNI DLL blocking
 *          problem
 * @version V3_0_F 09.09.2016 Added resolving of config values, fix resolve method
 * @version V4_0_F 11.10.2016 disabled debug output, use getClass().getName() instead of hardcoded classname, removed
 *          debug output in constructor
 * @version V5_0_T 17.02.2017 Applied codeStyle, changed error messages to english, LANGUAGE now contains the Cascade
 *          language, support for crypted modules
 * @version V6_0_F 21.04.2017 F-Version
 * @version V7_0_T 21.08.2017 Added function to get Cascade Plantcode, Added support for getPackage to IMTClassLoader
 * @version V9_0_T 12.10.2017 Added function to get data directory
 * @version V10_0_F 15.11.2017 F-Version
 */
public class CallIMTCore_10_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    /**
     * Workaround for the KeyWriter "C"-DLL, if it can not be unloaded by the garbage collector. In this case, we store
     * the first sucessfully loaded KeyWriter JNI classes in this Map. The Map is public, because other instances of the
     * IMT PP copy this Map to there own Map of keywriter JNI classes.
     */
    public static Map<String, Class<?>> staticJniClasses = new HashMap<String, Class<?>>();
    
    /** The name must match the name of the HashMap storing the JNI classes (staticJniClasses) */
    private static final String DEF_NAME_OF_STATIC_JNI_CLASSSES_MAP = "staticJniClasses";
    
    /** All classes with this classname prefix are loaded into the global static HashMap */
    private static final String DEF_PREFIX_JNI_CLASSES = "IMT_JNI_DLL_";
    
    /** TODO: remove it */
    private static final String DEF_PREFIX_JNI_CLASSES_TEMP = "NativeCallDll";
    
    /** serialVersionUID */
    private static final long serialVersionUID = 1L;
    
    private static final String DEF_SEQUENCE = "SEQUENCE";
    
    private static final String DEF_PREFIX_PARAM_LABEL = "PARAM_LABEL";
    
    private static final String DEF_PREFIX_PARAM_VALUE = "PARAM_VALUE";
    
    private static final String DEF_PREFIX_CONFIG_LABEL = "CONFIG_LABEL";
    
    private static final String DEF_PREFIX_CONFIG_VALUE = "CONFIG_VALUE";
    
    private static final String DEF_CORE_PREFIX = "IMT_CASCADE_CORE";
    
    private static final String DEF_CORE_CLASS = "com.bmw.imt.cascade.core.main.ImtCascadeCore";
    
    private static final Class<?> DEF_CORE_EXECUTE_METHOD_SIG[] = { String.class, HashMap.class, Vector.class,
            Object.class };
    
    private static final String DEF_CORE_EXECUTE_METHOD = "loadAndExecute";
    
    private static final Class<?> DEF_CORE_VERSION_METHOD_SIG[] = {};
    
    private static final String DEF_CORE_VERSION_METHOD = "cascade_InterfaceVersionGet";
    
    private static final String DEF_MOD_SUBDIR = "IMT_MOD";
    
    private final byte[] pruefProzBytes = new byte[] { (byte) 0xEC, (byte) 0x5A, (byte) 0x98, (byte) 0x87, (byte) 0x04,
            (byte) 0x7D, (byte) 0xED, (byte) 0xA6, (byte) 0x18, (byte) 0xF1, (byte) 0xAA, (byte) 0x5B, (byte) 0x7A,
            (byte) 0x5C, (byte) 0xC4, (byte) 0xC3 };
    
    private String m_sequence = null;
    
    private Vector<String> m_paramLabels = null;
    
    private Vector<String> m_paramValues = null;
    
    // TODO entfernen oder deaktivieren
    private static final boolean debug_core_load = false;
    
    /**
     * Default constructor, only used for deserialization
     */
    public CallIMTCore_10_0_F_Pruefprozedur() {
    }
    
    /**
     * Create a new pruefprozedur.
     * 
     * @param pruefling
     *            the associated pruefling
     * @param pruefprozName
     *            name of the pruefprozedur
     * @param hasToBeExecuted
     *            Execution condition for freedom from defects
     */
    public CallIMTCore_10_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super(pruefling, pruefprozName, hasToBeExecuted);
        attributeInit();
    }
    
    /** initialized the arguments */
    @Override
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /** returns the required arguments */
    @Override
    public String[] getRequiredArgs() {
        if (debug_core_load) {
            System.out.println(this.getClass().getName() + ".getRequiredArgs");
        }
        String[] args = { "SEQUENCE" };
        return args;
    }
    
    /** returns the optional arguments */
    @Override
    public String[] getOptionalArgs() {
        if (debug_core_load) {
            System.out.println(this.getClass().getName() + ".getOptionalArgs");
        }
        String[] args = { "PARAM_LABEL[1..N]", "PARAM_VALUE[1..N]", "CONFIG_LABEL[1..N]", "CONFIG_VALUE[1..N]" };
        return args;
    }
    
    /**
     * Check the parameter for this PP
     * 
     * @return true if at least one parameter error has been detected
     */
    @Override
    public boolean checkArgs() {
        if (debug_core_load) {
            System.out.println(this.getClass().getName() + ".checkArgs");
        }
        clearVars();
        Vector<Ergebnis> errors = new Vector<Ergebnis>();
        return checkArgs(errors);
    }
    
    /**
     * Clear the internal private variables. NOTE: This prevents permanent references on objects only required at
     * execution runtime. Therefore no unneeded stuff is serialized.
     */
    private void clearVars() {
        if (debug_core_load) {
            System.out.println(this.getClass().getName() + ".clearVars");
        }
        m_sequence = null;
        m_paramLabels = null;
        m_paramValues = null;
    }
    
    /**
     * Check the parameter for this PP. Also extracts the parameters and saves them in internal variables.
     * 
     * @param errors
     *            Cascade results with detected errors.
     * @return true if at least one parameter error has been detected
     */
    public boolean checkArgs(Vector<Ergebnis> errors) {
        if (debug_core_load) {
            System.out.println(this.getClass().getName() + ".checkArgs-internal");
        }
        Ergebnis result;
        
        try {
            try {
                m_sequence = getArg(DEF_SEQUENCE);
                if ((m_sequence == null) || m_sequence.trim().length() == 0) {
                    result = new Ergebnis("ExecError", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "", "",
                            "parameter check: " + DEF_SEQUENCE, "parameter error", Ergebnis.FT_NIO_SYS);
                    errors.add(result);
                    throw new PPExecutionException();
                }
                
                @SuppressWarnings("rawtypes")
                Hashtable ht = getArgs();
                // the keyset contains all parameter labels, everytime a parameter has been checked the label is removed
                // from the keyset
                // At the end the keyset must be empty. If not there are unknown parameters or gaps in the numbering.
                @SuppressWarnings({ "unchecked", "rawtypes" })
                Set keyset = new HashSet(ht.keySet());
                keyset.remove(DEF_SEQUENCE);
                
                m_paramLabels = new Vector<String>(30);
                m_paramValues = new Vector<String>(30);
                
                // Find all parameters starting at 1 using their label. Gaps are not allowed (they will be detected
                // later)
                int i = 1;
                boolean goon = true;
                do {
                    String label = (String) ht.get(DEF_PREFIX_PARAM_LABEL + i);
                    if (label != null) {
                        label = label.toUpperCase().trim();
                        
                        // All parameter names start with PARA_. Add this prefix if it is not already present.
                        if (!label.startsWith("PARA_")) {
                            label = "PARA_" + label;
                        }
                    }
                    if (label == null) {
                        // parameter label not available, thus we are finished
                        goon = false;
                        
                    } else if (label.trim().length() == 0) {
                        
                        // empty parameter label
                        result = new Ergebnis("ExecError", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "", "",
                                "parameter check: " + DEF_PREFIX_PARAM_LABEL + i, "parameter error",
                                Ergebnis.FT_NIO_SYS);
                        errors.add(result);
                        throw new PPExecutionException();
                        
                    } else {
                        // label is okay, so remove it from keyset
                        keyset.remove(DEF_PREFIX_PARAM_LABEL + i);
                        
                        // fetch the value for the label
                        String value = (String) ht.get(DEF_PREFIX_PARAM_VALUE + i);
                        
                        if (value != null) {
                            value = value.trim();
                        }
                        
                        // parameter value not available, thus this is an error
                        if (value == null) {
                            result = new Ergebnis("ExecError", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "",
                                    "", "parameter check: " + DEF_PREFIX_PARAM_VALUE + i, "parameter error",
                                    Ergebnis.FT_NIO_SYS);
                            errors.add(result);
                            throw new PPExecutionException();
                        }
                        
                        // empty parameter value
                        else if (value.trim().length() == 0) {
                            result = new Ergebnis("ExecError", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "",
                                    "", "parameter check: " + DEF_PREFIX_PARAM_VALUE + i, "parameter error",
                                    Ergebnis.FT_NIO_SYS);
                            errors.add(result);
                            throw new PPExecutionException();
                        }
                        
                        // value is okay, so remove it from keyset
                        else {
                            keyset.remove(DEF_PREFIX_PARAM_VALUE + i);
                            
                            // check if Label is already defined
                            for (int v = 0; v < m_paramLabels.size(); v++) {
                                if (label.equals(m_paramLabels.elementAt(v))) {
                                    // error cause label is already defined!
                                    result = new Ergebnis("ExecError", "CallIMTCore", "", "", "", "", "", "", "", "0",
                                            "", "", "", "parameter check: double definition " + label,
                                            "parameter error", Ergebnis.FT_NIO_SYS);
                                    errors.add(result);
                                    throw new PPExecutionException();
                                }
                            }
                            
                            // try to resolve the value
                            if (value.startsWith("@") && value.endsWith("@")) {
                                try {
                                    value = resolve(value);
                                } catch (Exception e) {
                                    value = null;
                                }
                            }
                            if (value != null) {
                                value = value.trim();
                                // save all pairs of label and value into two Vectors
                                m_paramLabels.add(label);
                                m_paramValues.add(value);
                            } else {
                                // case value is a link but the referred test stand variable does not exist.
                                // in this case do not set the parameter
                            }
                        }
                    }
                    // Increment for index i
                    i++;
                } while (goon);
                
                // Fetch and check configuration parameters
                i = 1;
                goon = true;
                do {
                    String label = (String) ht.get(DEF_PREFIX_CONFIG_LABEL + i);
                    if (label != null) {
                        label = label.toUpperCase().trim();
                        if (!label.startsWith("CFG_")) {
                            label = "CFG_" + label;
                        }
                    }
                    
                    // parameter label not available, thus we are finished
                    if (label == null) {
                        goon = false;
                    }
                    // empty parameter label
                    else if (label.trim().length() == 0) {
                        result = new Ergebnis("ExecError", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "", "",
                                "parameter check: " + DEF_PREFIX_CONFIG_LABEL + i, "parameter error",
                                Ergebnis.FT_NIO_SYS);
                        errors.add(result);
                        throw new PPExecutionException();
                    }
                    
                    // label is okay, so remove it from keyset
                    else {
                        keyset.remove(DEF_PREFIX_CONFIG_LABEL + i);
                        // fetch the value for the label
                        String value = (String) ht.get(DEF_PREFIX_CONFIG_VALUE + i);
                        if (value != null) {
                            value = value.trim();
                        }
                        if (value == null) {
                            // parameter value not available, thus this is an error
                            result = new Ergebnis("ExecError", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "",
                                    "", "parameter check: " + DEF_PREFIX_CONFIG_VALUE + i, "parameter error",
                                    Ergebnis.FT_NIO_SYS);
                            errors.add(result);
                            throw new PPExecutionException();
                        }
                        
                        // empty parameter value
                        else if (value.trim().length() == 0) {
                            result = new Ergebnis("ExecError", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "",
                                    "", "parameter check: " + DEF_PREFIX_CONFIG_VALUE + i, "parameter error",
                                    Ergebnis.FT_NIO_SYS);
                            
                            errors.add(result);
                            throw new PPExecutionException();
                        }
                        
                        // value is okay, so remove it from keyset
                        else {
                            keyset.remove(DEF_PREFIX_CONFIG_VALUE + i);
                            // check if Label is already defined
                            for (int v = 0; v < m_paramLabels.size(); v++) {
                                if (label.equals(m_paramLabels.elementAt(v))) {
                                    // error cause label is already defined!
                                    result = new Ergebnis("ExecError", "CallIMTCore", "", "", "", "", "", "", "", "0",
                                            "", "", "", "parameter check: double definition " + label,
                                            "parameter error", Ergebnis.FT_NIO_SYS);
                                    
                                    errors.add(result);
                                    throw new PPExecutionException();
                                }
                            }
                            
                            // try to resolve the value
                            if (value.startsWith("@") && value.endsWith("@")) {
                                try {
                                    value = resolve(value);
                                } catch (Exception e) {
                                    value = null;
                                }
                            }
                            if (value != null) {
                                value = value.trim();
                                // save all pairs of label and value into two Vectors
                                m_paramLabels.add(label);
                                m_paramValues.add(value);
                            } else {
                                // case value is a link but the referred test stand variable does not exist.
                                // in this case do not set the parameter
                            }
                        }
                    }
                    // Increment for index i
                    i++;
                } while (goon);
                
                // Now the keyset should be empty. If not there are unknown parameters or gaps in the numbering.
                if (!keyset.isEmpty()) {
                    // unknown parameters or gaps in the numbering
                    @SuppressWarnings("rawtypes")
                    Iterator iter = keyset.iterator();
                    Object o = iter.next();
                    result = new Ergebnis("ExecError", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "", "",
                            "parameter check: unexpected parameter " + o, "parameter error", Ergebnis.FT_NIO_SYS);
                    errors.add(result);
                    throw new PPExecutionException();
                }
            }
            
            // Error-Result should already be in result Vector
            catch (PPExecutionException ppe) {
                if (debug_core_load) {
                    System.out.println(
                            this.getClass().getName() + ".checkArgs-internal: PPExecutionException "
                                    + ppe.getMessage());
                }
                return false;
            }
            
            // Unexpected error during check so Create a new result
            catch (Exception e) {
                result = new Ergebnis("ExecError", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "", "",
                        "parameter error", "parameter error", Ergebnis.FT_NIO_SYS);
                errors.add(result);
                if (debug_core_load) {
                    System.out.println(this.getClass().getName() + ".checkArgs-internal: Exception " + e.getMessage());
                }
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        } catch (Throwable e) {
            return false;
        }
    }
    
    /**
     * Execute the PP
     * 
     * @param info
     *            Information for the execution.
     */
    @Override
    public void execute(ExecutionInfo info) {
        clearVars();
        int status = STATUS_EXECUTION_OK;
        Ergebnis result;
        Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
        
        try {
            // fetch parameters
            if (checkArgs(ergListe) == false) {
                throw new PPExecutionException(PB.getString("parameterexistenz"));
            }
            
            // load and start the core
            try {
                status = loadAndStartCore(ergListe);
            } catch (Throwable e) {
                if (debug_core_load) {
                    e.printStackTrace();
                }
                
                result = new Ergebnis("ExecFehler", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "", "",
                        "External call failed", "Probably IMTCore.jar in lib/ext not found or not in classpath,\r\n"
                                + e.getClass().getName() + " \r\n" + e.getMessage(),
                        Ergebnis.FT_NIO_SYS);
                ergListe.add(result);
                throw new PPExecutionException();
            } finally {
            }
            
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            if (debug_core_load) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
            result = new Ergebnis("ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception",
                    PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
            if (debug_core_load) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
            result = new Ergebnis("ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable",
                    PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS);
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
        setPPStatus(info, status, ergListe);
        clearVars();
    }
    
    /**
     * Load the core jar and start the core.
     * 
     * @param ergListe
     *            Vector for storing the results.
     * @return Cascade execution status.
     * @throws PPExecutionException
     *             Thrown if an error occurs.
     */
    private int loadAndStartCore(Vector<Ergebnis> ergListe) throws PPExecutionException {
        int status = STATUS_EXECUTION_OK;
        
        try {
            // determine log directory
            String homeDir = CascadeProperties.getCascadeHome();
            if (debug_core_load) {
                System.out.println("HOME-DIRECTORY: " + homeDir);
            }
            
            // Cascade log directory
            File logDirFile = new File(homeDir + File.separator + "log");
            
            // Cascade lib/ext directory
            File libExtDirFile = new File(homeDir + File.separator + "lib" + File.separator + "ext");
            
            // Cascade directory where the DLLs are stored
            File dllDirFile = new File(homeDir + File.separator + "bin");
            
            // Cascade directory whre data is stored
            File dataDirFile = new File(
                    homeDir + File.separator + "database" + File.separator + "pruefstand" + File.separator + "imt");
            
            // Cascade plantCode
            String plantCode = CascadeProperties.getCascadeWerkCode();
            
            // Create a HashMap with all parameters
            HashMap<String, String> paramsAndConfig = new HashMap<String, String>();
            
            // internal error size mismatch, should never happen
            if (m_paramLabels.size() != m_paramValues.size()) {
                Ergebnis result;
                result = new Ergebnis("ExecFehler", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "", "",
                        "Internal error", "Unexpected internal error 42", Ergebnis.FT_NIO_SYS);
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            // automatically set the configuration parameter LANGUAGE
            paramsAndConfig.put("LANGUAGE", CascadeProperties.getLanguage());
            
            byte[] b = Arrays.copyOf(pruefProzBytes, pruefProzBytes.length);
            paramsAndConfig.put("PP_BYTES", byteArrayToHexString(b));
            
            // insert the values from the Vectors into the HashMap
            for (int i = 0; i < m_paramLabels.size(); i++) {
                paramsAndConfig.put(m_paramLabels.elementAt(i), m_paramValues.elementAt(i));
            }
            
            // test mode or normal mode
            boolean useCascadeTestMode = false;
            
            com.bmw.cascade.pruefstand.Pruefstand ps = com.bmw.cascade.pruefstand.Pruefstand.getInstance();
            Version.VersionType vt = ps.getDynamicDeviceManager().getMode();
            
            if (vt == VersionType.TEST) {
                useCascadeTestMode = true;
            }
            
            // ===================================================================================
            // Search and load core jar and start the core
            // ===================================================================================
            
            // directory for modules
            File libModDirFile = new File(libExtDirFile, DEF_MOD_SUBDIR);
            
            if (libModDirFile == null || !libModDirFile.exists() || !libModDirFile.isDirectory()
                    || !libModDirFile.canRead()) {
                // error: directory for modules not available
                Ergebnis result = new Ergebnis("ExecFehler", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "", "",
                        DEF_MOD_SUBDIR + " not available",
                        "Directory for modules " + DEF_MOD_SUBDIR + " in ...\\lib\\ext\\ not available",
                        Ergebnis.FT_NIO_SYS);
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            // get an array of URLs to the core jar
            URL[] modURLs = null;
            modURLs = filterLibModForCore(libModDirFile, useCascadeTestMode);
            
            if (modURLs == null || modURLs.length == 0) {
                // error: no matching jar detected
                Ergebnis result = new Ergebnis("ExecFehler", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "", "",
                        DEF_CORE_PREFIX + " not available", "External libraray " + DEF_CORE_PREFIX + " not available",
                        Ergebnis.FT_NIO_SYS);
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            // -----------------------
            // LOAD and start the core
            // -----------------------
            try {
                // Workaround: Check Cascade for other loaded versions of PP CallIMTCore_x_x_F/T and copy their loaded
                // keywriter JNI classes
                getAlreadyLoadedKeyWriterJniClassesFromOtherImtInstances();
                
                IMTClassLoader cl = new IMTClassLoader();
                
                // Create IMTCallBackFunctions (required as argument for the core)
                IMTCallBackFunctions cb = new IMTCallBackFunctions(logDirFile, libExtDirFile, libModDirFile, dllDirFile,
                        dataDirFile, useCascadeTestMode, cl, plantCode);
                
                // Clear all URls of the IMT-ClassLoader and add the URL of the Core
                cl.clearUrls();
                cl.addUrls(modURLs);
                
                Class<?> coreClass = cl.findClass(DEF_CORE_CLASS);
                
                // Ask for Core version
                Method versionMethod = coreClass.getMethod(DEF_CORE_VERSION_METHOD, DEF_CORE_VERSION_METHOD_SIG);
                Object versionArguments[] = {};
                Object versionReturnValue = versionMethod.invoke(null, versionArguments);
                
                if (debug_core_load) {
                    System.out.println(this.getClass().getName() + ": CORE version " + versionReturnValue);
                }
                
                // TODO: Add a check here for the version number?
                // int versionNumber = (Integer)versionReturnValue;
                
                // Core execute sequence
                Method executeMethod = coreClass.getMethod(DEF_CORE_EXECUTE_METHOD, DEF_CORE_EXECUTE_METHOD_SIG);
                Object executeArguments[] = { m_sequence, paramsAndConfig, ergListe, cb };
                Object executeReturnValue = executeMethod.invoke(null, executeArguments);
                
                if (debug_core_load) {
                    System.out.println(this.getClass().getName() + ": CORE returned " + executeReturnValue);
                }
                
                int executeReturnValueInt = (Integer) executeReturnValue;
                
                if (executeReturnValueInt != STATUS_EXECUTION_OK) {
                    status = executeReturnValueInt;
                }
                
                // TODO: Clean UP!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! funktioniert nicht??
                versionMethod = null;
                versionArguments = null;
                
                cb.theClassLoader = null;
                cb = null;
                ergListe = null;
                paramsAndConfig = null;
                m_sequence = null;
                executeArguments = null;
                executeMethod = null;
                
                coreClass = null;
                
                cl.clearUrls();
                cl = null;
                
                System.gc();
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                }
                // Clean UP!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            }
            
            catch (IllegalAccessException e) {
                if (debug_core_load) {
                    e.printStackTrace(System.out);
                }
                Ergebnis result = new Ergebnis("ExecFehler", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "", "",
                        "IllegalAccessException",
                        "Unexpected internal Error,\r\n" + e.getClass().getName() + " \r\n" + e.getMessage(),
                        Ergebnis.FT_NIO_SYS);
                ergListe.add(result);
                throw new PPExecutionException();
            } catch (IllegalArgumentException e) {
                if (debug_core_load) {
                    e.printStackTrace(System.out);
                }
                Ergebnis result = new Ergebnis("ExecFehler", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "", "",
                        "IllegalArgumentException",
                        "Unexpected internal Error,\r\n" + e.getClass().getName() + " \r\n" + e.getMessage(),
                        Ergebnis.FT_NIO_SYS);
                ergListe.add(result);
                throw new PPExecutionException();
            } catch (SecurityException e) {
                if (debug_core_load) {
                    e.printStackTrace(System.out);
                }
                Ergebnis result = new Ergebnis("ExecFehler", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "", "",
                        "SecurityException",
                        "Unexpected internal Error,\r\n" + e.getClass().getName() + " \r\n" + e.getMessage(),
                        Ergebnis.FT_NIO_SYS);
                ergListe.add(result);
                throw new PPExecutionException();
            }
        } catch (
        
        PPExecutionException ppe) {
            throw ppe;// pass through
        } catch (Exception e) {
            if (debug_core_load) {
                e.printStackTrace(System.out);
            }
            Ergebnis result = new Ergebnis("ExecFehler", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "", "",
                    "Exception", "Unexpected Error,\r\n" + e.getClass().getName() + " \r\n" + e.getMessage(),
                    Ergebnis.FT_NIO_SYS);
            ergListe.add(result);
            throw new PPExecutionException();
        } catch (Throwable t) {
            if (debug_core_load) {
                t.printStackTrace(System.out);
            }
            Ergebnis result = new Ergebnis("ExecFehler", "CallIMTCore", "", "", "", "", "", "", "", "0", "", "", "",
                    "Throwable", "Unexpected Error,\r\n" + t.getClass().getName() + " \r\n" + t.getMessage(),
                    Ergebnis.FT_NIO_SYS);
            ergListe.add(result);
            throw new PPExecutionException();
        } finally {
            // Cleanup and remove references to loaded Core components
            // TODO: Check if there are any?
        }
        
        return status;
    }
    
    /**
     * Class bundling all callback functions from the ImtCascadeCore to the pruefprozedur.
     * 
     */
    public class IMTCallBackFunctions {
        /** CASCADE directory that contains the DLLs (e.g. Cascade/bin) */
        private final File dllDirFile;
        
        /** Log directory of CASCADE */
        private final File logDirFile;
        
        /** Lib/Ext directory of CASCADE (the directory for additional jar files) */
        private final File libExtDirFile;
        
        /** Lib/Ext/IMT_MOD directory of CASCADE (the directory for IMT modules) */
        private final File libModDirFile;
        
        /** CASCADE directory where data is stored (e.g. Cascade/database/pruefstand/imt) */
        private final File dataDirFile;
        
        /** True if CASCADE is running in test mode */
        private final boolean useCascadeTestMode;
        
        /** The IMT-ClassLoader */
        private IMTClassLoader theClassLoader;
        
        /** The plantCode */
        private final String plantCode;
        
        /**
         * Create an instance. This instance is passed as argument to the ImtCascadeCore and thereby provides access to
         * the callback functions it defines.
         * 
         * @param logDirFile
         *            Log directory of CASCADE.
         * @param libExtDirFile
         *            Lib/Ext directory of CASCADE (the directory for additional jar files).
         * @param libModDirFile
         *            Lib/Ext/IMT_MOD directory of CASCADE (the directory for IMT modules).
         * @param dllDirFile
         *            CASCADE directory that contains the DLLs (e.g. Cascade/bin)
         * @param dataDirFile
         *            CASCADE directory where data is stored (e.g. Cascade/database/pruefstand/imt)
         * @param useCascadeTestMode
         *            True if CASCADE is running in test mode.
         * @param cl
         *            The IMT-ClassLoader
         * @param plantCode
         *            The Cascade Plant Code
         */
        public IMTCallBackFunctions(File logDirFile, File libExtDirFile, File libModDirFile, File dllDirFile,
                File dataDirFile, boolean useCascadeTestMode, IMTClassLoader cl, String plantCode) {
            this.logDirFile = logDirFile;
            this.libExtDirFile = libExtDirFile;
            this.libModDirFile = libModDirFile;
            this.dllDirFile = dllDirFile;
            this.dataDirFile = dataDirFile;
            this.useCascadeTestMode = useCascadeTestMode;
            this.theClassLoader = cl;
            this.plantCode = plantCode;
        };
        
        /**
         * Add URLs to the IMT ClassLoader
         * 
         * @param urlsToAdd
         */
        public void addUrlsToClassLoader(URL[] urlsToAdd) {
            
            theClassLoader.addUrls(urlsToAdd);
        }
        
        /**
         * Get the used ClassLoader
         * 
         * @return theClassLoader
         */
        public ClassLoader getClassLoader() {
            return this.theClassLoader;
        }
        
        /**
         * @return Log directory of CASCADE
         */
        public File getLogDirFile() {
            return this.logDirFile;
        }
        
        /**
         * @return Lib/Ext directory of CASCADE (the directory for additional jar files).
         */
        public File getLibExtDirFile() {
            return this.libExtDirFile;
        }
        
        /**
         * @return Lib/Ext/IMT_MOD directory of CASCADE (the directory for IMT modules).
         */
        public File getLibModDirFile() {
            return this.libModDirFile;
        }
        
        /**
         * @return Lib/Ext/IMT_MOD directory of CASCADE (the directory for IMT modules).
         */
        public File getDllDirFile() {
            return this.dllDirFile;
        }
        
        /**
         * @return CASCADE directory where data is stored (e.g. Cascade/database/pruefstand/imt)
         */
        public File getDataDirFile() {
            return this.dataDirFile;
        }
        
        /**
         * @return The Cascade Plant Code.
         */
        public String getCascadePlantCode() {
            return this.plantCode;
        }
        
        /**
         * @return True if CASCADE is running in test mode.
         */
        public Boolean getUseCascadeTestMode() {
            return new Boolean(this.useCascadeTestMode);
        }
        
        /**
         * @return Provides a reference to the PrueflingLaufzeitUmgebung.
         */
        public PrueflingLaufzeitUmgebung getPrueflingLaufzeitUmgebungCallBack() {
            return getPr�flingLaufzeitUmgebung();
        }
        
        /**
         * @return Provides a reference to the Pruefstand.
         */
        public Pruefstand getPruefstandCallBack() {
            return Pruefstand.getInstance();
        }
        
        /**
         * @return The Directory for buffering DOM files before they are sent to the server.
         */
        public String getDomPruefstandDirectoryCallBack() {
            return DomPruefstand.PRUEFSTAND_DIRECTORY;
        }
        
        /**
         * @return The general distribution directory.
         */
        public String getDistributionConstantsPruefstandDirectoryCallBack() {
            return com.bmw.cascade.distribution.DistributionConstants.PRUEFSTAND_DIRECTORY;
        }
        
        /**
         * @return The name of the sub-directory in the distribution directory for key datasets.
         */
        public String getDistributionConstantsXmlCasDirectoryCallBack() {
            return com.bmw.cascade.distribution.DistributionConstants.XML_CAS_FILE;
        }
        
        /**
         * Read data from the DOM temp directory on the server.
         * 
         * @param fgnr
         *            the VIN number (7 characters)
         * @param type
         *            the type of DOM data should be one character long
         * @throws DomException
         *             if a problem occurs
         * @return the data
         */
        public byte[] readDomDataCallBack(String fgnr, String type) throws DomException {
            return DomPruefstand.readDomData(fgnr, type);
        }
        
        /**
         * Write data to the dom temp directory on the server.
         * 
         * @param fgnr
         *            the VIN number (7 characters)
         * @param type
         *            the type of DOM data should be one character long
         * @param data
         *            the data
         * @throws DomException
         *             if a problem occurs
         */
        public void writeDomDataCallBack(String fgnr, String type, byte[] data) throws DomException {
            DomPruefstand.writeDomData(fgnr, type, data);
        }
        
        /**
         * Add JNI Class to JNI Class map if value not exists
         * 
         * @param name
         *            The binary name of the class
         * 
         * @param jniClass
         *            The JNI Classobject
         */
        public void addJniClass(String name, Class<?> jniClass) {
            if (!CallIMTCore_10_0_F_Pruefprozedur.staticJniClasses.containsKey(name)) {
                CallIMTCore_10_0_F_Pruefprozedur.staticJniClasses.put(name, jniClass);
            }
        }
        
        /**
         * Check if JNI Class exists in JNI Class map
         * 
         * @param name
         *            The binary name of the class
         * 
         * @return true: map contains key, false: map not contains key
         */
        public boolean checkJniClass(String name) {
            return CallIMTCore_10_0_F_Pruefprozedur.staticJniClasses.containsKey(name);
        }
    }
    
    /**
     * If a parameter is enclosed by @ (starts with @ and ends with @) this internal auxiliary function will try to
     * resolve the value by returning the value of a test stand variable with the given name. if the parameter is not
     * enclosed by @ it is return unmodified. If the test stand variable with the given name does not exist null is
     * returned.
     * 
     * @param parameter
     *            The parameter value to be resolved.
     * @return The resolved parameter value or null.
     * @throws PPExecutionException
     *             when an error occurs.
     */
    private String resolve(String parameter) throws PPExecutionException {
        String erg = null;
        
        // resolve parameters starting and ending with a @ to the references value of the test stand variable.
        if (parameter.startsWith("@") && parameter.endsWith("@")) {
            // case resolving necessary
            parameter = parameter.substring(1, parameter.length() - 1);
            
            try {
                Object pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PS_CONFIG, parameter);
                if (pr_var != null) {
                    if (pr_var instanceof String) {
                        erg = (String) pr_var;
                    }
                }
            } catch (Exception e) {
            }
        } else {
            // case NO resolving necessary
            erg = parameter;
        }
        return erg;
    }
    
    /**
     * Bytearray to spaced hex string. Example: {127, 0, 1, -1, 66} --> "7F 00 01 FF 42"
     * 
     * @param barray
     *            the input byte array
     * @return the string
     */
    public static String byteArrayToSpacedHexString(byte[] barray) {
        StringBuffer out = new StringBuffer();
        if (barray == null) {
            return null; // null als null
        }
        if (barray.length == 0) {
            return ""; // leeres Array als ""
        }
        for (int i = 0; i < barray.length; i++) {
            int current = barray[i];
            String hexRaw = Integer.toHexString(current).toUpperCase();
            if (hexRaw.length() < 2) {
                out.append('0');
                out.append(hexRaw);
            } else {
                out.append(hexRaw.substring(hexRaw.length() - 2));
            }
            out.append(' ');
        }
        out.deleteCharAt(out.length() - 1); // letztes Leerzeichen am Ende l�schen
        return out.toString();
    }
    
    /**
     * Generates an array containing the URL of the matching core jar.
     * 
     * @param libModDirFile
     *            Directory for the modules jar files
     * @param useCascadeTestMode
     *            When true also test-versions are taken into account..
     * @return Array containing the URL of the matching core jar. When an error occurs an empty array is returned.
     */
    static URL[] filterLibModForCore(File libModDirFile, boolean useCascadeTestMode) {
        URL[] urls = null;
        
        if (libModDirFile == null) {
            // no directory given, thus no URLs
            if (debug_core_load) {
                System.out.println("filterLibModForCore - no directory specified");
            }
            return new URL[0];
        }
        if (!libModDirFile.exists()) {
            // directory does not exist
            if (debug_core_load) {
                System.out.println("filterLibModForCore - directory does not exist");
            }
            return new URL[0];
        }
        if (!libModDirFile.canRead()) {
            // directory not readable
            if (debug_core_load) {
                System.out.println("filterLibModForCore - directory not readable");
            }
            return new URL[0];
        }
        if (!libModDirFile.isDirectory()) {
            // not a directory
            if (debug_core_load) {
                System.out.println("filterLibModForCore - no directory");
            }
            urls = new URL[1];
            
            try {
                urls[0] = libModDirFile.toURI().toURL();
            } catch (MalformedURLException mue) {
                if (debug_core_load) {
                    mue.printStackTrace(System.out);
                }
                return new URL[0];
            }
            return urls;
        }
        
        // Loop over directory
        // Use a filenameFilter for prefiltering
        FilenameFilter fFilter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                name = name.toUpperCase();
                if (debug_core_load) {
                    System.out.println("filterLibModForCore - FilenameFilter: INPUT=" + name);
                }
                
                if (!name.startsWith(DEF_CORE_PREFIX + "_")) {
                    // Name does not start with "IMT_CASCADE_CORE_"
                    if (debug_core_load) {
                        System.out.println("filterLibModForCore - FilenameFilter: IMT_CORE_ missing");
                    }
                    return false;
                }
                
                // Cut off leading "IMT_CASCADE_CORE_".
                // Remaining is for example "1_10_999_T.JAR"
                name = name.substring(DEF_CORE_PREFIX.length() + 1);
                if (debug_core_load) {
                    System.out.println("filterLibModForCore - FilenameFilter: remaining after prefix=" + name);
                }
                
                int majorVersion = -1;
                int minorVersion = -1;
                // check major version
                int delim = name.indexOf('_');
                if (delim < 1) {
                    // A _ is missing after the version number
                    if (debug_core_load) {
                        System.out.println("filterLibModForCore - FilenameFilter: _ missing after MajorVersion");
                    }
                    return false;
                }
                String partStr = name.substring(0, delim);
                try {
                    majorVersion = Integer.parseInt(partStr);
                } catch (Exception e) {
                    // Major version is not numeric
                    if (debug_core_load) {
                        System.out.println("filterLibModForCore - FilenameFilter: MajorVersion not numeric");
                    }
                    return false;
                }
                name = name.substring(delim + 1);
                if (debug_core_load) {
                    System.out.println("filterLibModForCore - FilenameFilter: remaining after MajorVersion=" + name);
                }
                
                // minor version
                delim = name.indexOf('_');
                if (delim < 1) {
                    // A _ is missing after the version number
                    if (debug_core_load) {
                        System.out.println("filterLibModForCore - FilenameFilter: _ missing after MinorVersion");
                    }
                    return false;
                }
                partStr = name.substring(0, delim);
                try {
                    minorVersion = Integer.parseInt(partStr);
                } catch (Exception e) {
                    // minor version is not numeric
                    if (debug_core_load) {
                        System.out.println("filterLibModForCore - FilenameFilter: MinorVersion not numeric");
                    }
                    return false;
                }
                name = name.substring(delim + 1);
                if (debug_core_load) {
                    System.out.println("filterLibModForCore - FilenameFilter: remaining after MinorVersion=" + name);
                }
                /*
                 * // check all version numbers > 0 if ((majorVersion < 0) || (minorVersion < 0)) { // at least one is
                 * negative if (debug_core_load)
                 * System.out.println("filterLibModForCore - FilenameFilter: One Version is negative"); return false; }
                 * 
                 * // check all version numbers < 1000 if ((majorVersion > 999) || (minorVersion > 999)) { // at least
                 * one is to big if (debug_core_load)
                 * System.out.println("filterLibModForCore - FilenameFilter: One Version is more than 999"); return
                 * false; }
                 */
                if (name.length() == 0 || (name.charAt(0) != 'T' && name.charAt(0) != 'F')) {
                    // missing T or F marker
                    if (debug_core_load) {
                        System.out.println("filterLibModForCore - FilenameFilter: missing T or F");
                    }
                    return false;
                }
                name = name.substring(1);
                if (debug_core_load) {
                    System.out.println("filterLibModForCore - FilenameFilter: remaining after T/F=" + name);
                }
                
                if (name.length() != 0 && !name.equals(".JAR")) {
                    // for files .jar is missing / for directories some rubbish is remaining
                    if (debug_core_load) {
                        System.out.println("filterLibModForCore - FilenameFilter: missing or wrong extension");
                    }
                    return false;
                }
                return true;
            }
        };
        
        File allFiles[] = libModDirFile.listFiles(fFilter);
        URL currentTestURL = null;
        long currentTestCombinedVersion = -1;
        URL currentProdURL = null;
        long currentProdCombinedVersion = -1;
        
        for (File oneFile : allFiles) {
            if (debug_core_load) {
                System.out.println("FILE: " + oneFile.getName());
            }
            
            String oneName = oneFile.getName().toUpperCase();
            if (debug_core_load) {
                System.out.println("filterLibModForCore: INPUT=" + oneName);
            }
            
            // Cut off leading "IMT_CASCADE_CORE_". Remaining is for example "1_10_999_T.JAR"
            oneName = oneName.substring(DEF_CORE_PREFIX.length() + 1);
            
            if (debug_core_load) {
                System.out.println("filterLibModForCore: remaining after prefix=" + oneName);
            }
            
            int majorVersion = -1;
            int minorVersion = -1;
            // check major version
            int delim = oneName.indexOf('_');
            String partStr = oneName.substring(0, delim);
            try {
                majorVersion = Integer.parseInt(partStr);
            } catch (Exception e) {
                // major version is not numeric
                if (debug_core_load) {
                    System.out.println("filterLibModForCore: MajorVersion not numeric");
                }
                continue;
            }
            oneName = oneName.substring(delim + 1);
            if (debug_core_load) {
                System.out.println("filterLibModForCore: remaining after MajorVersion=" + oneName);
            }
            
            // minor version
            delim = oneName.indexOf('_');
            partStr = oneName.substring(0, delim);
            try {
                minorVersion = Integer.parseInt(partStr);
            } catch (Exception e) {
                // minor version is not numeric
                if (debug_core_load) {
                    System.out.println("filterLibModForCore: MinorVersion not numeric");
                }
                continue;
            }
            oneName = oneName.substring(delim + 1);
            
            if (debug_core_load) {
                System.out.println("filterLibModForCore: remaining after MinorVersion=" + oneName);
            }
            
            char typeTF = oneName.charAt(0);
            oneName = oneName.substring(1);
            
            if (debug_core_load) {
                System.out.println("filterLibModForCore: remaining after T/F=" + oneName);
            }
            
            long combinedVersion = majorVersion;
            combinedVersion = combinedVersion * 1000;
            combinedVersion += minorVersion;
            
            if (typeTF == 'F') {
                // remember the productive version, if version is higher
                if (combinedVersion > currentProdCombinedVersion) {
                    URI oneURI = oneFile.toURI();
                    URL oneURL = null;
                    try {
                        oneURL = oneURI.toURL();
                        currentProdURL = oneURL;
                        currentProdCombinedVersion = combinedVersion;
                        if (debug_core_load) {
                            System.out.println("filterLibModForCore: Prod-URL: " + oneURL);
                        }
                    } catch (MalformedURLException mue) {
                        if (debug_core_load) {
                            System.out.println("filterLibModForCore: MalformedURLException");
                            mue.printStackTrace(System.out);
                        }
                    }
                }
            } else {
                // remember the test version, if version is higher
                if (combinedVersion > currentTestCombinedVersion) {
                    URI oneURI = oneFile.toURI();
                    URL oneURL = null;
                    try {
                        oneURL = oneURI.toURL();
                        currentTestURL = oneURL;
                        currentTestCombinedVersion = combinedVersion;
                        if (debug_core_load) {
                            System.out.println("filterLibModForCore: Test-URL: " + oneURL);
                        }
                    } catch (MalformedURLException mue) {
                        if (debug_core_load) {
                            System.out.println("filterLibModForCore: MalformedURLException");
                            mue.printStackTrace(System.out);
                        }
                    }
                }
            }
        }
        
        if (!useCascadeTestMode && currentProdURL != null) {
            // productive mode and a productive URL
            URL[] res = { currentProdURL };
            if (debug_core_load) {
                System.out.println("filterLibModForCore: Selected URL: " + currentProdURL);
            }
            return res;
        } else if (useCascadeTestMode) {
            // test mode
            // decision depends on version number
            if (currentTestCombinedVersion > currentProdCombinedVersion) {
                // use test version
                URL[] res = { currentTestURL };
                if (debug_core_load) {
                    System.out.println("filterLibModForCore: Selected URL: " + currentTestURL);
                }
                return res;
            } else if (currentTestCombinedVersion < currentProdCombinedVersion) {
                // use productive version
                URL[] res = { currentProdURL };
                if (debug_core_load) {
                    System.out.println("filterLibModForCore: Selected URL: " + currentProdURL);
                }
                return res;
            } else if (currentProdCombinedVersion != -1) {
                // a T and a F version with the same number:
                // use productive version
                if (debug_core_load) {
                    System.out.println("filterLibModForCore: ERROR T and F versions with same number!!!");
                }
                URL[] res = { currentProdURL };
                if (debug_core_load) {
                    System.out.println("filterLibModForCore: Selected URL: " + currentProdURL);
                }
                return res;
            } else {
                // both combined versions are -1, thus no matching jar
                // or directory available
                if (debug_core_load) {
                    System.out.println("filterLibModForCore: Selected URL: null");
                }
                return new URL[0];
            }
        } else {
            // productive mode, but no matching jar or directory
            if (debug_core_load) {
                System.out.println("filterLibModForCore: Selected URL: null");
            }
            return new URL[0];
        }
    }
    
    /**
     * The one and only IMT ClassLoader. All IMT Classes must be loaded with this ClassLoader. The IMT-Core also
     * accesses this ClassLoader by calling Class.forName(). The Core adds URLs to the ClassLoader with the help of the
     * IMTCallBackFunctions.addUrlsToClassLoader().
     * 
     * This special ClassLoader is necessary, to use native DLLs (KeyWriter), that can't be loaded and accessed by
     * different ClassLoaders.
     * 
     * @author Rolf Antony (Softing)
     *
     */
    private class IMTClassLoader extends ClassLoader {
        
        /** The HashMap where the classes will be cached */
        private final Map<String, Class<?>> classes = new HashMap<String, Class<?>>();
        
        /** List with ClassLoader URLs as Strings */
        private final LinkedList<String> theClassLoaderUrls = new LinkedList<String>();
        
        /**
         * Standard Constructor
         */
        public IMTClassLoader() {
            super();
        }
        
        /**
         * Add URLs to the IMT ClassLoader. The new URLs are appended at the end of the list of URLs where the
         * ClassLoader searches the required classes. If a module is updated !WHILE THE SEQUENCE IS RUNNING!, the
         * ClassLoader uses the old module which has been used at first. The new module is used at the next start of a
         * sequence.
         * 
         * @param urlsToAdd
         */
        public void addUrls(URL[] urlsToAdd) {
            if (urlsToAdd == null) {
                return;
            }
            for (URL u : urlsToAdd) {
                theClassLoaderUrls.add(u.toString());
            }
        }
        
        /**
         * Clear all Urls of the IMT ClassLoader Clear also the cached classes, to make it possible to load newer
         * versions of the classes.
         */
        public void clearUrls() {
            theClassLoaderUrls.clear();
            classes.clear();
        }
        
        @Override
        public String toString() {
            return IMTClassLoader.class.getName();
        }
        
        @Override
        public URL getResource(String name) {
            
            URL url = null;
            BufferedInputStream in = null;
            
            for (String repositoryURL : theClassLoaderUrls) {
                try {
                    // Class file in JAR
                    if (repositoryURL.toUpperCase().endsWith(".JAR")) {
                        url = new URL("jar:" + repositoryURL + "!/" + name);
                    } else {
                        url = new URL(repositoryURL + name);
                    }
                    
                    in = new BufferedInputStream(url.openStream());
                    break;
                } catch (Exception e) {
                }
            }
            
            if (in == null) {
                return null;
            }
            
            try {
                in.close();
                return url;
                
            } catch (Exception e1) {
                return null;
            }
        }
        
        @Override
        protected Class<?> findClass(String name) throws ClassNotFoundException {
            
            // Workaround for the KeyWriter-Dll and its JNI Java class can not be unloaded by the Garbage Collector.
            // The keywriter "C"-DLL is locked by the first ClassLoader that loaded it.
            // Because of that, we use the first loaded keywriter class of the first ClassLoader instance for all new
            // ClassLoader instances.
            if (staticJniClasses.containsKey(name)) {
                return staticJniClasses.get(name);
            }
            
            if (classes.containsKey(name)) {
                return classes.get(name);
            }
            
            // Get InputStream for Class
            InputStream is = getInputStreamForName(name);
            byte[] classData = loadClassData(name, is);
            Manifest man = getManifest(name);
            definePackageInternal(name, man);
            Class<?> c = defineClass(name, classData, 0, classData.length);
            resolveClass(c);
            classes.put(name, c);
            
            // Workaround for the KeyWriter-Dll and its JNI can not be unloaded by the Garbage Collector.
            // Store the sucessfully loaded keywriter class in the global static HashMap
            if ((name.indexOf(DEF_PREFIX_JNI_CLASSES) != -1) || (name.indexOf(DEF_PREFIX_JNI_CLASSES_TEMP) != -1)) {
                staticJniClasses.put(name, c);
            }
            
            return c;
        }
        
        // Also called by VM to define Package for classes loaded from the CDS
        // archive
        private void definePackageInternal(String name, Manifest man) {
            int i = name.lastIndexOf('.');
            if (i != -1) {
                String pkgname = name.substring(0, i);
                
                if (getPackage(pkgname) == null) {
                    try {
                        if (man != null) {
                            definePackage(pkgname, man);
                        } else {
                            definePackage(pkgname, null, null, null, null, null, null, null);
                        }
                    } catch (IllegalArgumentException iae) {
                        // parallel-capable class loaders: re-verify in case of a
                        // race condition
                        if (getPackage(pkgname) == null) {
                            // Should never happen
                            throw new AssertionError("Cannot find package " + pkgname);
                        }
                    }
                }
            }
        }
        
        /**
         * Defines a new package by name in this ClassLoader. The attributes contained in the specified Manifest will be
         * used to obtain package version and sealing information. For sealed packages, the additional URL specifies the
         * code source URL from which the package was loaded.
         *
         * @param name
         *            the package name
         * @param man
         *            the Manifest containing package version and sealing information
         * @exception IllegalArgumentException
         *                if the package name duplicates an existing package either in this class loader or one of its
         *                ancestors
         * @return the newly defined Package object
         */
        protected Package definePackage(String name, Manifest man) throws IllegalArgumentException {
            String path = name.replace('.', '/').concat("/");
            String specTitle = null, specVersion = null, specVendor = null;
            String implTitle = null, implVersion = null, implVendor = null;
            String sealed = null;
            URL sealBase = null;
            
            Attributes attr = man.getAttributes(path);
            if (attr != null) {
                specTitle = attr.getValue(Name.SPECIFICATION_TITLE);
                specVersion = attr.getValue(Name.SPECIFICATION_VERSION);
                specVendor = attr.getValue(Name.SPECIFICATION_VENDOR);
                implTitle = attr.getValue(Name.IMPLEMENTATION_TITLE);
                implVersion = attr.getValue(Name.IMPLEMENTATION_VERSION);
                implVendor = attr.getValue(Name.IMPLEMENTATION_VENDOR);
                sealed = attr.getValue(Name.SEALED);
            }
            attr = man.getMainAttributes();
            if (attr != null) {
                if (specTitle == null) {
                    specTitle = attr.getValue(Name.SPECIFICATION_TITLE);
                }
                if (specVersion == null) {
                    specVersion = attr.getValue(Name.SPECIFICATION_VERSION);
                }
                if (specVendor == null) {
                    specVendor = attr.getValue(Name.SPECIFICATION_VENDOR);
                }
                if (implTitle == null) {
                    implTitle = attr.getValue(Name.IMPLEMENTATION_TITLE);
                }
                if (implVersion == null) {
                    implVersion = attr.getValue(Name.IMPLEMENTATION_VERSION);
                }
                if (implVendor == null) {
                    implVendor = attr.getValue(Name.IMPLEMENTATION_VENDOR);
                }
                if (sealed == null) {
                    sealed = attr.getValue(Name.SEALED);
                }
            }
            return definePackage(
                    name, specTitle, specVersion, specVendor, implTitle, implVersion, implVendor, sealBase);
        }
        
        /**
         * Get the manifest for the given class
         * 
         * @param name
         *            full class name
         * @return Manifest
         */
        private Manifest getManifest(String name) {
            JarInputStream jarIS = null;
            Manifest manifest = null;
            
            String repo = getRepositoryUrl(name);
            // repo not found
            if (repo == null) {
                return manifest;
            }
            
            InputStream is = null;
            try {
                URL url = new URL(repo);
                is = new BufferedInputStream(url.openStream());
            } catch (Exception e) {
            }
            // open InputStream failed
            if (is == null) {
                return manifest;
            }
            
            try {
                jarIS = new JarInputStream(is);
                manifest = jarIS.getManifest();
            } catch (IOException e) {
            } finally {
                if (jarIS != null) {
                    try {
                        jarIS.close();
                    } catch (IOException e) {
                    }
                }
            }
            return manifest;
        }
        
        /**
         * Search the repository (i.e. JAR) in the URLs
         * 
         * @param name
         *            full class name
         * @return InputStream The InputStream for the repository
         */
        private String getRepositoryUrl(String name) {
            BufferedInputStream in = null;
            String repo = null;
            
            for (String repositoryURL : theClassLoaderUrls) {
                try {
                    URL url;
                    
                    // Class file in JAR
                    if (repositoryURL.toUpperCase().endsWith(".JAR")) {
                        url = new URL("jar:" + repositoryURL + "!/" + name.replace('.', '/') + ".class");
                    } else {
                        url = new URL(repositoryURL + name.replace('.', '/') + ".class");
                    }
                    
                    in = new BufferedInputStream(url.openStream());
                    repo = repositoryURL;
                    break;
                } catch (Exception e) {
                }
            }
            
            return repo;
        }
        
        /**
         * Search the class in the URLs
         * 
         * @param name
         *            full class name
         * @return InputStream The InputStream for the class
         */
        private InputStream getInputStreamForName(String name) {
            BufferedInputStream in = null;
            
            for (String repositoryURL : theClassLoaderUrls) {
                try {
                    URL url;
                    
                    // Class file in JAR
                    if (repositoryURL.toUpperCase().endsWith(".JAR")) {
                        url = new URL("jar:" + repositoryURL + "!/" + name.replace('.', '/') + ".class");
                    } else {
                        url = new URL(repositoryURL + name.replace('.', '/') + ".class");
                    }
                    
                    in = new BufferedInputStream(url.openStream());
                    break;
                } catch (Exception e) {
                }
            }
            
            return in;
        }
        
        /**
         * Load the data of the class in the given InputStream
         * 
         * @param name
         *            full class name
         * @param in
         *            The InputStream for the class
         * @return byte[] with the read in data of the class
         * @throws ClassNotFoundException
         */
        private byte[] loadClassData(String name, InputStream in) throws ClassNotFoundException {
            
            if (in == null) {
                throw new ClassNotFoundException("Class [" + name + "] could not be found");
            }
            
            byte[] classData;
            
            try {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                int i;
                while ((i = in.read()) != -1) {
                    out.write(i);
                }
                in.close();
                classData = out.toByteArray();
                out.close();
            } catch (Exception e) {
                throw new ClassNotFoundException("Class [" + name + "] could not be read", e);
            }
            
            return classData;
        }
        
        @Override
        protected void finalize() throws Throwable {
            super.finalize();
            if (debug_core_load) {
                System.out.println(this.toString() + " - CL Finalized.");
            }
        }
    }
    
    /**
     * This Workaraound is necessary for the KeyWriter JNI class, that can not be garbage collected and which can only
     * load the keywriter C-DLL once. Search for all other loaded versions of the IMT Pr�fprozedur and copy all loaded
     * versions of the keywriter JNI classes.
     */
    private void getAlreadyLoadedKeyWriterJniClassesFromOtherImtInstances() {
        
        try {
            
            ClassLoader CL = Thread.currentThread().getContextClassLoader();
            
            Class<?> CL_class = CL.getClass();
            
            // Search for the top level classloader
            while (CL_class != java.lang.ClassLoader.class) {
                CL_class = CL_class.getSuperclass();
            }
            
            java.lang.reflect.Field ClassLoader_classes_field = CL_class.getDeclaredField("classes");
            
            ClassLoader_classes_field.setAccessible(true);
            
            @SuppressWarnings("unchecked")
            Vector<Class<?>> classes = (Vector<Class<?>>) ClassLoader_classes_field.get(CL);
            
            String SearchStringImtPPClasses = this.getPPName().toUpperCase();
            
            // Search for all IMT pruefprozedur versions loaded by the Cascade classloader
            for (Class<?> cl : classes) {
                
                if (cl.toString().toUpperCase().indexOf(SearchStringImtPPClasses) == -1) {
                    continue;
                }
                
                try {
                    @SuppressWarnings("unchecked")
                    Map<String, Class<?>> keyWriterJniClasses = (Map<String, Class<?>>) cl
                            .getField(DEF_NAME_OF_STATIC_JNI_CLASSSES_MAP).get(cl);
                    
                    // Copy the static map with the keywriter JNI classes of the other pruefprozedur to our own map.
                    // Overwrite existing entries.
                    staticJniClasses.putAll(keyWriterJniClasses);
                    
                } catch (NoSuchFieldException ex) {
                }
            }
            
        } catch (Exception e) {
        }
    }
    
    /**
     * Byte array to hex string. Example: {3,127, 0, 1, -1, 66} --> "037F0001FF42"
     *
     * @param barray
     *            the input byte array
     * @return the string
     */
    private static String byteArrayToHexString(byte[] barray) {
        StringBuffer out = new StringBuffer();
        if (barray == null) {
            return null;        // null as null
        }
        if (barray.length == 0) {
            return "";  // empty array as ""
        }
        int j = 33;
        for (int i = 0; i < barray.length; i++) {
            int current = barray[i];
            if (i % 2 == 0) {
                current += j;
                j--;
            }
            String hexRaw = Integer.toHexString(current).toUpperCase();
            if (hexRaw.length() < 2) {
                out.append('0');
                out.append(hexRaw);
            } else {
                out.append(hexRaw.substring(hexRaw.length() - 2));
            }
        }
        return out.toString();
    }
}

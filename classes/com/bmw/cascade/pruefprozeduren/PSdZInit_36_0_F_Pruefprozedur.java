package com.bmw.cascade.pruefprozeduren;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Vector;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.distribution.DistributionConstants;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.psdz.PSdZ;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeKISWBVersion;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeVersionInfo;
import com.bmw.cascade.pruefstand.devices.psdz.data.DiagnosticInterfaces;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.*;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementierung einer Pr�fprozedur, die die Initialisierung des PSdZ vornimmt
 * 
 * @author Mueller, BMW AG; Buboltz, BMW AG; Busetti, GEFASOFT AG; Gampl, BMW AG <BR>
 * 
 *         07.05.2007 TB PSdZ 2.1.2.x Funkverheiratung <BR>
 *         06.07.2007 TB Robustheit bei den Parametern verbessert. Erlaube bei "EDICNET_IP_ADDRESS" die Benutzung von Variablen aus dem Frontend (Variable: EDIC_NET_IP, Parameter: _VARIABLE:EDIC_NET_IP). Diese ist dann dominant.<BR>
 *         26.07.2007 TB Versuch FZS f�r DGF noch mit einzubauen, neuer optionaler Parameter USE_FZS nur bei Interface Funk default false, d.h. FGNR wird als ID benutzt <BR>
 *         02.01.2008 DB Einbau "testScreenID" in Debug-Ausgaben <BR>
 *         20.01.2008 TB !!! inkompatibel mit Cascade Versionen < 2.2.5!! �ffne Fahrzeugverbindung nur �ber die I-Stufe, wenn MCD Projekt nicht vorhanden ist <BR>
 *         12.02.2008 TB im Fall useFZS keine Zeichen bei der VIN abschneiten <BR>
 *         20.05.2008 TB Exception 1615 sorgt jetzt auch fuer eine Verheiratung??? <BR>
 *         15.06.2008 TB Zur�ckspeichern der VIN auch beim Verheiraten eingebaut <BR>
 *         19.06.2008 DB Ausgabe des PSdZ-Fehlers bei Abbruch (Verheiratung) eingebaut <BR>
 *         24.06.2008 DB �nderung holen des "testScreenID" <BR>
 *         28.01.2009 TB Bugfix Parameter "PRODUCTIVE_I_STUFE_LIST" <BR>
 *         24.03.2010 TB Anwenderwunsch, erlaube neue Variable "INTERFACE_IP_ADDRESS" weil wir jetzt 2 WLAN Interfaces haben (ICOM und EDICNET) + Fehler und Warnmeldungen angepa�t <BR>
 *         ---------- -- Falls beide Parameter konfiguriert sind, gewinnt der neue Parameter (INTERFACE_IP_ADDRESS), der alte (EDICNET_IP_ADDRESS) ist deprecated <BR>
 *         30.11.2010 TB Versionen in APDM dokumentieren (PSdZ, I-Stufe, KIS AL, KIS WB) <BR>
 *         29.08.2011 AN Abfangen der "@"-Operator Exception bei xxx_IP_ADDRESS um ein Init bei ENET und ICOM-"@" Parametrierung vorzunehmen <BR>
 *         12.06.2012 TB F-Version <BR>
 *         21.03.2013 TB Kleine �nderungen <BR>
 *         ---------- -- - Alte OpenConnection raus -> Parameter ("MCD_PROJECT", "VEHICLE_INFO") haben keine Funktion mehr, Achtung Pr�fschritte im PL nutzen diese Paramter noch <BR>
 *         ---------- -- - CheckCommunication (LOP1489) eingebaut, wenn NIO dann Fehler und raus <BR>
 *         08.11.2013 MG F-Version<BR>
 *         03.02.2014 TB Bugfix f�r potentielle NPE beim getKisWBVersions(), Generics, deprecated Methoden <BR>
 *         05.02.2014 MG Bugfix f�r fehlenden Userdialog w�hrend Pr�fung der Fahrzeugverbindung <BR>
 *         05.02.2014 MG F-Version (20_0) <BR>
 *         17.04.2014 MG T-Version (21_0) <BR>
 *         ---------- -- Verbindungspr�fung wird um einen zweiten Verbindungsversuch erweitert, falls der erste fehlschl�gt. Fehlermeldung bei Verbindungsfehler verbessert. <BR>
 *         24.04.2014 MG F-Version (22_0) <BR>
 *         22.07.2014 TB T-Version (23_0) <BR>
 *         ---------- -- f�r die neue SMART Architektur wurde der Bezug des orderXML ge�ndert <BR>
 *         22.07.2014 TB F-Version (24_0) <BR>
 *         13.11.2014 MG Kleine �nderungen <BR>
 *         ---------- -- - Bugfix f�r potentielle ClassCastException bei �ffnen der Fahrzeugverbindung <BR>
 *         ---------- -- - Falls Auftragsdatensatz weder in lokalem Auftragsdatenverzeichnis noch im Auftragsdatenverzeichnis des Server zu finden ist, wird zus�tzlich noch in einem weiteren Verzeichnis gesucht (-> APDM-Notfallstrategie). <BR>
 *         ---------- -- Somit wird verhindert, dass CASCADE bei Nutzung der APDM-Notfallstrategie zwar den Auftrag erkennt und den Pr�fablauf startet, die PP jedoch diese Auftragsdaten nicht einlie�t. <BR>
 *         13.11.2014 MG F-Version (26_0) <BR>
 *         13.11.2014 MG T-Version (27_0), Mindestversionsanforderung CASCADE 7.0.1 / PSdZ 5.00.01 <BR>
 *         ---------- -- - LOP 1831: Erweiterung um neuen optionalen Parameter ICOM_CAN_BRIDGE_ACTIVE zur Nutzung ab CASCADE 7.0 / PSdZ 5.00 <BR>
 *         ---------- -- - LOP 1832: Erweiterung um neuen optionalen Parameter SET_ALTERNATIVE_PSDZ_ROOT_DIRECTORY zur Nutzung ab CASCADE 7.0 / PSdZ 5.00 <BR>
 *         ---------- -- - LOP 1853: Ausgabe (in Log-Datei Pr�fstandScreen) der Pr�fprozedurversion sowie s�mtlicher optionaler und nicht optionaler Argumente, die an die Pr�fprozedur �bergeben werden zum Zweck der Nachvollziehbarkeit im Fehlerfall <BR>
 *         ---------- -- - Parameter MCD_PROJECT, VEHICLE_INFO und EDICNET_IP_ADDRESS aus PP entfernt, da diese fl�chig nicht mehr genutzt werden <BR>
 *         ---------- -- - In Parameter enthaltene Links werden in allen Parametern der Pr�fprozedur ersetzt <BR>
 *         01.12.2014 MG F-Version (28_0), Mindestversionsanforderung CASCADE 7.0.1 / PSdZ 5.00.01 <BR>
 *         13.05.2015 MG T-Version (29_0), Mindestversionsanforderung CASCADE 7.0.1 / PSdZ 5.00.01 <BR>
 *         ---------- -- - NPE entfernt falls getCurrentlySetPSdZRootDirectory() bei vorherigem PDX-Import-Fehler null zur�ckliefert <BR>
 *         ---------- -- - LOP 1899: Einheitliche Darstellung der Fehlerausgaben in PSdZ-Pr�fprozeduren <BR>
 *         28.07.2015 MG F-Version (30_0), Mindestversionsanforderung CASCADE 7.0.1 / PSdZ 5.00.01 <BR>
 *         26.08.2015 MG T-Version (31_0), (31_1), (31_2), Mindestversionsanforderung CASCADE 7.0.1 / PSdZ 5.00.01 <BR>
 *         ---------- -- - Methode isReachable(...) (TCP Verbindungspr�fung auf TCP Port 7 (Echo) ) umgestellt auf isReachableViaIcmpPing(...) (ICMP Pings) zur Pr�fung der Erreichbarkeit des Host <BR>
 *         ---------- -- - Debugausgaben in Methode readOrderXML(...) erweitert zur verbesserten Fehlernachvollziehbarkeit <BR>
 *         ---------- -- - Falls Auftragsdaten nicht lesbar sind erfolgt nun eine direkte Ausgabe im Fehlerprotokoll <BR>
 *         28.08.2015 MG F-Version (32_0), Mindestversionsanforderung CASCADE 7.0.1 / PSdZ 5.00.01 <BR>
 *         25.01.2016 MG T-Version (33_0), Mindestversionsanforderung CASCADE 7.1.0 / PSdZ 5.01.00 <BR>
 *         ---------- -- - deprecated Methode setEDICnetIPAddress gegen setInterfaceIPAddress getauscht <BR>
 *         ---------- -- - LOP 2128: Automatisierung der I-Stufenbereinigung mittels externer Datei <BR>
 *         04.11.2016 MG F-Version (34_0), Mindestversionsanforderung CASCADE 7.1.0 / PSdZ 5.01.00 <BR>
 *         07.11.2016 MG T-Version (35_0), Mindestversionsanforderung CASCADE 8.0.0 / PSdZ 5.02.00 <BR>
 *         ---------- -- - LOP 2126: VIN spezifisches PSdZ-Logging <BR>
 *         06.09.2017 MG F-Version (36_0), Mindestversionsanforderung CASCADE 8.0.0 / PSdZ 5.02.00 <BR>
 * 
 */
public class PSdZInit_36_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DE fuer Sprachermittlung
	 */
	private final boolean DE = checkDE(); // Systemsprache DE wenn true

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSdZInit_36_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSdZInit_36_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "ACTIVATE_VIN_SPECIFIC_PSDZ_LOGGING", "DEBUG", "DEBUG_PERFORM", "ICOM_CAN_BRIDGE_ACTIVE", "INTERFACE_IP_ADDRESS", "MDAEXT", "NO_COMMUNICATION", "PRODUCTIVE_I_STUFE_LIST", "SET_ALTERNATIVE_PSDZ_ROOT_DIRECTORY", "TIMEOUT", "USE_FZS" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = { "I_STUFE" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		if( !super.checkArgs() )
			return false;
		return true;
	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * einfache �berprung ob der Host per ICMP Ping erreichbar ist
	 * 
	 * @param host gibt den Host an, der gepingt werden soll
	 * @param bDebug gibt an, ob Debugausgaben erfolgen sollen
	 * 
	 * @return true, wenn ICMP Ping OK
	 */
	public static boolean isReachableViaIcmpPing( String host, boolean bDebug ) {
		boolean isReachable = false;

		try {
			Process proc = new ProcessBuilder( "ping", host ).start();

			int exitValue = proc.waitFor();
			if( exitValue == 0 )
				isReachable = true;
		} catch( IOException e1 ) {
			if( bDebug )
				e1.printStackTrace();
		} catch( InterruptedException e ) {
			if( bDebug )
				e.printStackTrace();
		}
		return isReachable;
	}

	/**
	 * Einlesen des Auftragsdatensatzes
	 * 
	 * @param shortVIN 7 stellige VIN des Auftrages
	 * @param bDebug gibt an, ob Debugausgaben erfolgen sollen
	 * @param testScreenID gibt an von welchem Testscreen geloggt wird
	 * 
	 * @return orderXMLContent (Windows default encoding der Datei wird vorausgesetzt) oder null
	 * @throws CascadePSdZConditionsNotCorrectException falls Auftragsdaten Datei nicht einlesbar
	 */
	public static String readOrderXML( String shortVIN, boolean bDebug, String testScreenID ) throws CascadePSdZConditionsNotCorrectException {
		String localEmergencyOrderDataFile = DistributionConstants.PRUEFSTAND_DIRECTORY + File.separator + DistributionConstants.XML_ORDER_FILE + File.separator + "a" + shortVIN + ".xml";
		String serverOrderDataFile = com.bmw.cascade.extern.roh2xml.auftrag.PB.getString( "AD_SAV_DIR" ).replace( '/', File.separatorChar ) + File.separator + "a" + shortVIN + ".xml";
		String serverEmergencyOrderDataFile = CascadeProperties.getCascadeDaten2().replace( '/', File.separatorChar ) + File.separator + "a" + shortVIN + ".xml";
		String cascadeServerHostName = com.bmw.cascade.pruefstand.PB.getString( "cascade.server.hostname" );

		if( bDebug )
			System.out.println( "PSdZ ID:" + testScreenID + ", Searching for order data files... " );
		// pr�fe als erstes das lokale Notstrategie Auftragsdatenverzeichnis
		File file = new File( localEmergencyOrderDataFile );
		if( file.exists() ) {
			if( bDebug )
				System.out.println( "PSdZ ID:" + testScreenID + ", Found order data file " + localEmergencyOrderDataFile );
		} else {
			if( bDebug )
				System.out.println( "PSdZ ID:" + testScreenID + ", Could not find order data file " + localEmergencyOrderDataFile + ", trying alternative path..." );
			// versuche alternativ den Auftragsdatensatz am Server zu lesen
			if( isReachableViaIcmpPing( com.bmw.cascade.pruefstand.PB.getString( "cascade.server.hostname" ), bDebug ) ) {
				file = new File( serverOrderDataFile );
				if( file.exists() ) {
					if( bDebug )
						System.out.println( "PSdZ ID:" + testScreenID + ", Found order data file " + serverOrderDataFile );
				} else {
					if( bDebug )
						System.out.println( "PSdZ ID:" + testScreenID + ", Could not find order data file " + serverOrderDataFile + ", trying alternative path..." );
					// pr�fe noch den Notfallpfad (APDM_emergency falls vorhanden)
					file = new File( serverEmergencyOrderDataFile );
					if( file.exists() ) {
						if( bDebug )
							System.out.println( "PSdZ ID:" + testScreenID + ", Found order data file " + serverEmergencyOrderDataFile );
					} else {
						if( bDebug )
							System.out.println( "PSdZ ID:" + testScreenID + ", Could not find order data file " + serverEmergencyOrderDataFile + ", no order data file could be found in any of the searched directories!" );
						throw new CascadePSdZConditionsNotCorrectException( checkDE() ? "Auftragsdatendatei " + "a" + shortVIN + ".xml" + " konnte nicht eingelesen werden, da in keinem der gesuchten Verzeichnisse verf�gbar!" : "Order data file " + "a" + shortVIN + ".xml" + " could not be read as not available in any of the searched directories!" );
					}
				}
			} else {
				if( bDebug )
					System.out.println( "PSdZ ID:" + testScreenID + ", Could not reach host " + cascadeServerHostName + ", no order data file could be found in any of the searched directories!" );
				throw new CascadePSdZConditionsNotCorrectException( checkDE() ? "Auftragsdatendatei " + "a" + shortVIN + ".xml" + " konnte nicht eingelesen werden, da Host " + cascadeServerHostName + " nicht erreichbar!" : "Order data file " + "a" + shortVIN + ".xml" + "could not be read as host " + cascadeServerHostName + " is unavailable!" );
			}
		}

		FileInputStream fis = null;
		String orderXMLContent = null;

		try {
			fis = new FileInputStream( file );
			byte[] data = new byte[(int) file.length()];
			fis.read( data );
			orderXMLContent = new String( data );
		} catch( IOException e ) {
			// Nothing
		} finally {
			try {
				if( fis != null ) {
					fis.close();
				}
			} catch( IOException e ) {
				// Nothing
			}
		}

		return orderXMLContent;
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {

		boolean bActivateVinSpecificPsdzLogging = true;
		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
		boolean bDebugPerform = true; // gibt an, ob eine Performencemessung erfolgen sollen
		boolean bIcomCanBridgeActive = false;
		boolean bNoCommunication = false;
		boolean buseFZS = false;
		final String VARIABLE_INTERFACE_NET_IP = "INTERFACE_NET_IP"; // Variable f�r Interfcae Net IP Adresse aus Frontend (ICOM // EDICNET)
		int dynamicRetriesConnectionCheck = 1;
		int key;
		int messageTimeout = 0;
		int runs = 0;
		int status = STATUS_EXECUTION_OK;

		Ergebnis result = null;
		File newPsdzRootDirectory = null;
		PSdZ psdz = null;
		String connectionCheckFailed = DE ? "Verbindungspr�fung fehlgeschlagen. Keine Verbindung zum Fahrzeug vorhanden!" : "Connection check failed. No connection to the vehicle available!";
		String iLevelCleanupFailed = DE ? "Bereinigung �berz�hliger I-Stufen fehlgeschlagen. " : "Cleanup of surplus I-levels failed. ";
		String iStufe = null;
		String interface_IP_Address = null;
		String mdaExt = null; // initialisiere Hilfsvariable fuer den externen Parameter fuer MDA Nummer
		String mdaNummer = null; // Variable MDA
		String openVehicleConnectionFailed = DE ? "�ffnen der Fahrzeugverbindung fehlgeschlagen. " : "Open vehicle connection failed. ";
		String optionalArgumentName = null;
		String optionalArgumentValue = null;
		String requiredArgumentName = null;
		String requiredArgumentValue = null;
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";
		String strAlternativePsdzRootDirectory = null;
		String testScreenID = this.getPr�flingLaufzeitUmgebung().getName(); // TestScreenID (aus Pr�fstandsinstanz)
		String vehicleVin = ""; // Zur�ckspeichern der VIN bei FZS Benutzung Bsp. DGF
		String vinForVinSpecificLogging = null;
		String[] optionalArgumentsNames = getOptionalArgs();
		String[] optionalArgumentsValues = null;
		String[] productiveIStufeList = null;
		String[] requiredArgumentsNames = getRequiredArgs();
		String[] requiredArgumentsValues = null;
		UserDialog userDialog = null;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();

		try {

			// Argumente loggen, einlesen und checken
			try {

				// Ausgabe der Pr�fprozedurversion sowie s�mtlicher an die Pr�fprozedur �bergebener Argumente (required/optional) f�r sp�tere Analysezwecke

				System.out.println( "PSdZ ID:" + testScreenID + ", Initialize " + this.getClassName() );

				for( int i = 0; i < requiredArgumentsNames.length; i++ ) {
					requiredArgumentName = requiredArgumentsNames[i];
					requiredArgumentValue = "";
					// gibt es nicht optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( requiredArgumentName ) != null ) {
						requiredArgumentsValues = extractValues( getArg( requiredArgumentName ) );
						for( int k = 0; k < requiredArgumentsValues.length; k++ ) {
							// Listen von nicht optionalen Argumenten werden durch ";" getrennt ausgegeben
							requiredArgumentValue = requiredArgumentValue + requiredArgumentsValues[k] + ((requiredArgumentsValues.length > 1 && k < (requiredArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						requiredArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZInit with required argument: " + requiredArgumentName + " = " + requiredArgumentValue );
				}

				for( int i = 0; i < optionalArgumentsNames.length; i++ ) {
					optionalArgumentName = optionalArgumentsNames[i];
					optionalArgumentValue = "";
					// gibt es optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( optionalArgumentName ) != null ) {
						optionalArgumentsValues = extractValues( getArg( optionalArgumentName ) );
						for( int k = 0; k < optionalArgumentsValues.length; k++ ) {
							// Listen von optionalen Argumenten werden durch ";" getrennt ausgegeben
							optionalArgumentValue = optionalArgumentValue + optionalArgumentsValues[k] + ((optionalArgumentsValues.length > 1 && k < (optionalArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						optionalArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZInit with optional argument: " + optionalArgumentName + " = " + optionalArgumentValue );
				}

				// Obligatorische Argumentpr�fung auf Vorhandensein von nicht optionalen sowie �berz�hligen Argumenten
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// ACTIVATE_VIN_SPECIFIC_PSDZ_LOGGING
				if( getArg( "ACTIVATE_VIN_SPECIFIC_PSDZ_LOGGING" ) != null ) {
					if( extractValues( getArg( "ACTIVATE_VIN_SPECIFIC_PSDZ_LOGGING" ) )[0].equalsIgnoreCase( "TRUE" ) )
						bActivateVinSpecificPsdzLogging = true;
					else if( extractValues( getArg( "ACTIVATE_VIN_SPECIFIC_PSDZ_LOGGING" ) )[0].equalsIgnoreCase( "FALSE" ) )
						bActivateVinSpecificPsdzLogging = false;
					else
						throw new PPExecutionException( "ACTIVATE_VIN_SPECIFIC_PSDZ_LOGGING " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DEBUG
				if( getArg( "DEBUG" ) != null ) {
					if( extractValues( getArg( "DEBUG" ) )[0].equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( extractValues( getArg( "DEBUG" ) )[0].equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
					else
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DEBUG_PERFORM
				if( getArg( "DEBUG_PERFORM" ) != null ) {
					if( extractValues( getArg( "DEBUG_PERFORM" ) )[0].equalsIgnoreCase( "TRUE" ) )
						bDebugPerform = true;
					else if( extractValues( getArg( "DEBUG_PERFORM" ) )[0].equalsIgnoreCase( "FALSE" ) )
						bDebugPerform = false;
					else
						throw new PPExecutionException( "DEBUG_PERFORM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// ICOM_CAN_BRIDGE_ACTIVE
				if( getArg( "ICOM_CAN_BRIDGE_ACTIVE" ) != null ) {
					if( extractValues( getArg( "ICOM_CAN_BRIDGE_ACTIVE" ) )[0].equalsIgnoreCase( "TRUE" ) )
						bIcomCanBridgeActive = true;
					else if( extractValues( getArg( "ICOM_CAN_BRIDGE_ACTIVE" ) )[0].equalsIgnoreCase( "FALSE" ) )
						bIcomCanBridgeActive = false;
					else
						throw new PPExecutionException( "ICOM_CAN_BRIDGE_ACTIVE " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// INTERFACE_IP_ADDRESS
				if( getArg( "INTERFACE_IP_ADDRESS" ) != null ) {

					try {
						interface_IP_Address = extractValues( getArg( "INTERFACE_IP_ADDRESS" ) )[0];
						if( interface_IP_Address.startsWith( "_VARIABLE:" + VARIABLE_INTERFACE_NET_IP ) )
							interface_IP_Address = (String) getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, VARIABLE_INTERFACE_NET_IP );
						// Dokumentiere Adresse
						result = new Ergebnis( "PSdZInit", "PSdZ", "", "", "", "INTERFACE_IP_ADDRESS", interface_IP_Address, "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					} catch( PPExecutionException e ) {
						interface_IP_Address = null;
					}
				}

				// I_STUFE
				iStufe = extractValues( getArg( "I_STUFE" ) )[0];

				// MDAEXT
				if( getArg( "MDAEXT" ) != null ) {
					mdaExt = extractValues( getArg( "MDAEXT" ) )[0];
				}

				// NO_COMMUNICATION Parameter
				if( getArg( "NO_COMMUNICATION" ) != null ) {
					if( extractValues( getArg( "NO_COMMUNICATION" ) )[0].equalsIgnoreCase( "TRUE" ) )
						bNoCommunication = true;
					else if( extractValues( getArg( "NO_COMMUNICATION" ) )[0].equalsIgnoreCase( "FALSE" ) )
						bNoCommunication = false;
					else
						throw new PPExecutionException( "NO_COMMUNICATION " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// PRODUCTIVE_I_STUFE_LIST
				if( getArg( "PRODUCTIVE_I_STUFE_LIST" ) != null ) {
					String arg = getArg( "PRODUCTIVE_I_STUFE_LIST" );
					// falls im �bergebenen Argument ein @-Konstrukt verwendet wird, soll der Inhalt zun�chst aufgel�st werden und danach die ";"-separierten Einzelwerte extrahiert werden
					// Die extractValues-Methode allein reicht hier nicht aus, denn hier erfolgt zuerst das "Tokenizing" und danach die @-Konstruktaufl�sung (sh. auch LOP 2128)
					if( arg.indexOf( '@' ) != -1 ) {
						try {
							productiveIStufeList = extractValues( getPPResult( arg ) );
						} catch( InformationNotAvailableException e ) {
							throw new PPExecutionException( e.getMessage() );
						}
					} else
						productiveIStufeList = extractValues( getArg( "PRODUCTIVE_I_STUFE_LIST" ) );
				}

				// SET_ALTERNATIVE_PSDZ_ROOT_DIRECTORY
				if( getArg( "SET_ALTERNATIVE_PSDZ_ROOT_DIRECTORY" ) != null ) {
					strAlternativePsdzRootDirectory = extractValues( getArg( "SET_ALTERNATIVE_PSDZ_ROOT_DIRECTORY" ) )[0];
				}

				// TIMEOUT
				if( getArg( "TIMEOUT" ) != null ) {
					messageTimeout = Integer.parseInt( extractValues( getArg( "TIMEOUT" ) )[0] );
				}

				// USE_FZS
				if( getArg( "USE_FZS" ) != null ) {
					if( extractValues( getArg( "USE_FZS" ) )[0].equalsIgnoreCase( "TRUE" ) )
						buseFZS = true;
					else if( extractValues( getArg( "USE_FZS" ) )[0].equalsIgnoreCase( "FALSE" ) )
						buseFZS = false;
					else
						throw new PPExecutionException( "USE_FZS " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Performancemessung Start
			if( bDebugPerform )
				System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZInit PP START" );

			// Devices holen
			try {
				psdz = getPr�flingLaufzeitUmgebung().getDeviceManager().getPSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			try {
				userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Initialisierung aufrufen
			try {
				userDialog.setDisplayProgress( true );
				userDialog.getProgressbar().setIndeterminate( true );
				userDialog.displayMessage( PB.getString( "psdz.ud.Initialisierung" ), ";" + PB.getString( "psdz.ud.Initialisierunglaeuft" ), -1 );
				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZInit 'psdz.init' START" );
				}

				// LOP 2126: VIN spezifisches PSdZ-Logging 

				// VIN spezifisches PSdZ-Logging erfolgt nur bei Aktivierung sowie nur im Single-Instanzbetrieb
				if( bActivateVinSpecificPsdzLogging && (psdz.getNumberOfTestscreens() < 2) )
					vinForVinSpecificLogging = getPr�fling().getAuftrag().getFahrgestellnummer7();
				else
					vinForVinSpecificLogging = null;

				// LOP 1832: Erweiterung um neuen optionalen Parameter SET_ALTERNATIVE_PSDZ_ROOT_DIRECTORY

				// Wechsel auf neues PSdZ-Root-Verzeichnis ist genau dann notwendig wenn aktuelles gesetztes PSdZ-Root-Verzeichnis sich von dem neu zu setzenden PSdZ-Root-Verzeichnis unterscheidet
				// falls jedoch
				// - SET_ALTERNATIVE_PSDZ_ROOT_DIRECTORY ung�ltige Werte enth�lt (psdzdata_new, psdzdata, psdzdata_old) ODER 
				// - angegebenes Verzeichnis aus SET_ALTERNATIVE_PSDZ_ROOT_DIRECTORY nicht existiert/nicht beschreibbar ist ODER
				// - ein Multi-Instanzbetrieb aktiv ist
				// -> Fehler und Abbruch

				// Das Ziel PSdZ-Root-Verzeichnis ist entweder der Inhalt des PP Parameters (falls bef�llt) oder, falls nicht angegeben "PSdZDaten" !
				newPsdzRootDirectory = strAlternativePsdzRootDirectory != null ? new File( PSdZ.getMainPsdzDirectory().toString(), strAlternativePsdzRootDirectory ) : PSdZ.getDefaultPsdzRootDirectory();

				// Folgende F�lle k�nnen auftreten (IST - aktuelles PSdZ-Root-Verzeichnis / SOLL - neu zu setzendes PSdZ-Root-Verzeichnis):
				//                                                                             S O L L 
				//                                               PSdZDaten                     PSdZDaten_alternativ_1        PSdZDaten_alternativ_2        ...
				//                  null                         setRoot()  (1)                setRoot()  (2)                setRoot()   (3)
				//                  PSdZDaten                    do nothing (4)                setRoot()  (5)                setRoot()   (6)
				//   I S T          PSdZDaten_alternativ_1       setRoot()  (7)                do nothing (8)                setRoot()   (9)
				//                  PSdZDaten_alternativ_2       setRoot() (10)                setRoot() (11)                do nothing (12)
				//                  ...

				// falls PSdZ root directory nach Fehlerfall w�hrend Import (z.B. wegen korruptem IC) nicht gesetzt wurde, wird dieser Fehler im Log dokumentiert
				if( bDebug && PSdZ.getCurrentlySetPSdZRootDirectory() == null )
					System.out.println( "PSdZ ID:" + testScreenID + ", The PSdZ root directory is currently not set! Potentially the previous PDX import could not be completed successfully." );

				// aktuell gesetztes PSdZ-Root-Verzeichnis entspricht Zielzustand ? Z.B. "PSdZDaten" -> "PSdZDaten" (4) oder "PSdZDaten_alternativ_1" -> "PSdZDaten_alternativ_1" (8) oder ... (12) oder ...
				if( PSdZ.getCurrentlySetPSdZRootDirectory() != null && PSdZ.getCurrentlySetPSdZRootDirectory().toString().equalsIgnoreCase( newPsdzRootDirectory.toString() ) ) {

					// Initialisierung ohne(!) Wechsel auf neues PSdZ-Root-Verzeichnis
					psdz.init( readOrderXML( getPr�fling().getAuftrag().getFahrgestellnummer7(), bDebug, testScreenID ), iStufe, null, vinForVinSpecificLogging );

				} else {

					// aktuell gesetztes PSdZ-Root-Verzeichnis entspricht nicht dem Zielzustand !
					// falls SET_ALTERNATIVE_PSDZ_ROOT_DIRECTORY gesetzt, m�ssen einige Bedingungen erf�llt sein, um auf ein alternatives PSdZ-Root-Verzeichnis zu wechseln (z.B. darf dieses nicht das default PSdZ-Root-Verzeichnis sein - dieses 
					// wird ohne Angabe von SET_ALTERNATIVE_PSDZ_ROOT_DIRECTORY in PP immer automatisch gesetzt
					if( strAlternativePsdzRootDirectory != null ) {
						if( !(strAlternativePsdzRootDirectory.equalsIgnoreCase( PSdZ.getDefaultPsdzRootDirectory().getName() )) && newPsdzRootDirectory.exists() && newPsdzRootDirectory.canRead() ) {
							// Set-Root auf alternatives PSdZ-Root-Verzeichnis z.B. "PSdZDaten_alternativ_1" wird nur im Single-Instanzbetrieb unterst�tzt
							// Hintergrund: SetRoot-Operationen auf einer Instanz w�hrend auf einer anderen Instanz beliebige PSdZ-Operationen laufen k�nnen, f�hren zu zahlreichen Fallunterscheidungen und damit unn�tiger Komplexit�t der SW
							if( psdz.getNumberOfTestscreens() < 2 ) {
								if( bDebug )
									System.out.println( "PSdZ ID:" + testScreenID + ", Switch over to alternative PSdZ root directory: " + newPsdzRootDirectory );
								// Initialisierung mit(!) Wechsel auf neues PSdZ-Root-Verzeichnis; F�lle: (2), (3), (5), (6), (9), (11)								
								psdz.init( readOrderXML( getPr�fling().getAuftrag().getFahrgestellnummer7(), bDebug, testScreenID ), iStufe, newPsdzRootDirectory, vinForVinSpecificLogging );

							} else {
								throw new CascadePSdZConfigurationException( PB.getString( "psdz.error.NoAlternativeRootWithinMultiInstanceEnvironment" ) );
							}
						} else {
							throw new CascadePSdZConfigurationException( PB.getString( "psdz.error.DirectoryNameForNewPSdZRootDirectoryNotSupportedOrNotExistent" ) + " (" + newPsdzRootDirectory + ")" );
						}
					} else {
						if( bDebug )
							System.out.println( "PSdZ ID:" + testScreenID + ", Return to default PSdZ root directory: " + newPsdzRootDirectory );
						// falls SET_ALTERNATIVE_PSDZ_ROOT_DIRECTORY nicht gesetzt, ist das Ziel (PSdZ-Root-Verzeichnis) immer "PSdZDaten"! Ein R�cksetzen des PSdZ-Root-Verzeichnisses erfolgt damit ohne weitere Pr�fung.
						// In diesem Fall erfolgt die Initialisierung mit(!) Wechsel auf das neue PSdZ-Root-Verzeichnis f�r die beiden verbleibenden F�lle: 
						// "PSdZDaten_alternativ_..." -> "PSdZDaten" (7)/(10) oder "null" -> "PSdZDaten" (1); "null" bedeutet hier -> Fehler bei setRoot nach PDX-Import d.h. PSdZ-Root-Verzeichnis konnte nicht gesetzt werden
						psdz.init( readOrderXML( getPr�fling().getAuftrag().getFahrgestellnummer7(), bDebug, testScreenID ), iStufe, newPsdzRootDirectory, vinForVinSpecificLogging );

					}
				}

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZInit 'psdz.init' ENDE" );
				}

			} catch( CascadePSdZArgumentException e ) {
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZInit", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Argument" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZInit", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), "Exception", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Bereinigung des Datenstandes, L�schen alter I-Stufen
			try {

				if( productiveIStufeList != null ) {
					// I-Stufenbereinigung soll nur dann erm�glicht werden, falls zuvor kein alternatives PSdZ-Root-Verzeichnis gesetzt wurde
					if( strAlternativePsdzRootDirectory == null ) {
						userDialog.reset();
						userDialog.displayMessage( PB.getString( "psdz.ud.Initialisierung" ), ";" + PB.getString( "psdz.ud.LoescheAlteDaten" ), -1 );
						userDialog.setDisplayProgress( true );
						userDialog.getProgressbar().setIndeterminate( true );
						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZInit 'psdz.deleteOldILevel' START" );
						}

						psdz.deleteOldILevel( productiveIStufeList );

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZInit 'psdz.deleteOldILevel' ENDE" );
						}
					} else {
						throw new CascadePSdZConfigurationException( PB.getString( "psdz.error.NoIStepCleanupUponAlternativePsdzRootDirectory" ) );
					}
				}
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZInit", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", iLevelCleanupFailed + e.getLocalizedMessage(), "Exception", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Verbindung zum Fahrzeug herstellen
			try {
				if( !bNoCommunication ) {

					// informiere den Benutzer
					userDialog.reset();
					userDialog.setDisplayProgress( true );
					userDialog.getProgressbar().setIndeterminate( true );
					userDialog.displayMessage( PB.getString( "psdz.ud.Initialisierung" ), ";" + PB.getString( "psdz.ud.AufbauKommunikation" ), -1 );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZInit 'psdz.setInterfaceIPAddress' START" );
					}

					// if available set INTERFACE-IP-Address, else null
					psdz.setInterfaceIPAddress( interface_IP_Address );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZInit 'psdz.setInterfaceIPAddress' ENDE" );
					}
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZInit 'psdz.openVehicleConnectionByIStep' START" );
					}

					// Nur bei FZS + Funk, jetzt wieder die VIN nehmen, weil sonst in PSdZ mehr Sachen nicht mehr richtig gehen
					if( buseFZS && psdz.getDeviceParams()[0].equalsIgnoreCase( DiagnosticInterfaces.STD_FUNK.toString() ) ) {
						// FGNR merken und hier (DGF arbeitet noch mit FZS) den Steuerschl�ssel setzen (numerisch)
						vehicleVin = getPr�fling().getAuftrag().getFahrgestellnummer();
						psdz.setVehicleVin( "BLABLABLAB" + getPr�fling().getAuftrag().getSteuerschluessel() + "X" );
					}

					// PSdZ mitteilen, dass CAN-Bridge im ICOM aktiv -> korrekte Topologiebestimmung in PSdZ f�r optimale Bandbreitennutzung des Fahrzeugzugangs
					if( bIcomCanBridgeActive ) {
						psdz.setICOMLinkPropertiesToDCan();
					} else {
						psdz.setICOMLinkPropertiesToEthernet();
					}

					// Verbindung zum Fahrzeug �ffnen
					psdz.openVehicleConnectionByIStep();

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZInit 'psdz.openVehicleConnectionByIStep' ENDE" );
					}

				}
			} catch( Exception e ) {
				// haben wir einen Fehler bei Funk und m�ssen wir verheiraten?
				if( e instanceof CascadePSdZRuntimeException && (((CascadePSdZRuntimeException) e).getMessageId() == 296 || ((CascadePSdZRuntimeException) e).getMessageId() == 297 || ((CascadePSdZRuntimeException) e).getMessageId() == 1615) ) {
					// Endlosschleife (Eingabe MDA Nummer muss erfolgen, oder Abbruch durch Werker)
					for( ;; ) {
						// wenn MDA-Nummer von extern kommt, dann nutze diese, ansonsten nehmen wir den alten - manuellen Weg �ber die UserEingabe
						if( mdaExt != null )
							mdaNummer = mdaExt;
						else {
							getPr�flingLaufzeitUmgebung().getUserDialog().setDisplayProgress( false );
							mdaNummer = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputNumber( PB.getString( "PSdZ" ), PB.getString( "eingabeDerMdaNummer" ), messageTimeout );
						}

						// Abbruch ?
						if( mdaNummer.equals( "" ) == true ) {
							// Abbruch?
							key = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputDigital( PB.getString( "PSdZ" ), PB.getString( "abbruchDurchWerker" ), messageTimeout );
							if( key != UserDialog.NO_KEY ) {
								result = new Ergebnis( "PSdZInit", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", openVehicleConnectionFailed + "MDA: " + mdaNummer + ". " + PB.getString( "funkKommunikation" ) + " [" + e.getLocalizedMessage() + "]", PB.getString( "abbruchDurchWerker" ), Ergebnis.FT_NIO );
								ergListe.add( result );
								throw new PPExecutionException();
							}
						} else {

						}

						// Aufruf Verheiratung
						try {
							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZInit 'psdz.openVehicleConnectionByIStepWithMdaMarriage' START" );
							}

							// Verbindung zum Fahrzeug �ffnen, mit �bergabe der MDA Nummer
							psdz.openVehicleConnectionByIStepWithMdaMarriage( mdaNummer );

							// Performancemessung
							if( bDebugPerform ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZInit 'psdz.openVehicleConnectionByIStepWithMdaMarriage' ENDE" );
							}

							// Dokumentation
							result = new Ergebnis( "PSdZInit", "PSdZ", "", "", "", "MDA_NR_INPUT", mdaNummer, "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );

							break;
						} catch( Exception f ) {
							if( bDebug )
								f.printStackTrace();
						}

						// Warte etwas...
						try {
							Thread.sleep( 333 );
						} catch( InterruptedException f ) {
						}

					}

				} else {
					if( bDebug )
						e.printStackTrace();
					result = new Ergebnis( "PSdZInit", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", openVehicleConnectionFailed + e.getLocalizedMessage(), "Exception", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}
			}

			// dokumentiere die Versionen
			CascadeVersionInfo myVersionInfo = PSdZ.getVersionInfo();
			result = new Ergebnis( "PSdZInit", "PSdZ", "", "", "", "PSdZ_VERSION", myVersionInfo.getPsdzVersion(), "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
			result = new Ergebnis( "PSdZInit", "PSdZ", "", "", "", "I-STEP", iStufe, "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
			result = new Ergebnis( "PSdZInit", "PSdZ", "", "", "", "KIS_AWL_VERSION", myVersionInfo.getKisAwlVersion(), "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );

			// etwas kniffliger lese die ganzen KIS WBs und vergleiche auf Zugeh�rigkeit zum Baureihenverbund -> Wenn ja ist das der Treffer
			CascadeKISWBVersion myKISWBVersion = null;
			if( myVersionInfo.getKisWBVersions() != null ) {
				for( int i = 0; i < myVersionInfo.getKisWBVersions().size(); i++ ) {
					myKISWBVersion = (CascadeKISWBVersion) myVersionInfo.getKisWBVersions().get( i );
					// Treffer
					if( myKISWBVersion.getBaureihenverbund().equalsIgnoreCase( iStufe.substring( 0, 4 ) ) ) {
						result = new Ergebnis( "PSdZInit", "PSdZ", "", "", "", "KIS_WB_VERSION", myKISWBVersion.getHauptversion() + "." + myKISWBVersion.getUnterversion() + "." + myKISWBVersion.getPatchversion(), "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
						break;
					}
					// kein Treffer
					if( i == myVersionInfo.getKisWBVersions().size() - 1 ) {
						// kein Treffer
						result = new Ergebnis( "PSdZInit", "PSdZ", "", "", "", "KIS_WB_VERSION", "0.0.0", "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
				}
			}

			// pr�fe Kommunikation mit dem Fahrzeug, NIO wenn Fehler
			if( !bNoCommunication ) {
				while( runs <= dynamicRetriesConnectionCheck ) {
					if( psdz.checkCommunication() ) {
						// Wiederholschleife umgehen, da Verbindung IO
						runs = dynamicRetriesConnectionCheck + 1;
					} else {
						if( runs == dynamicRetriesConnectionCheck ) {
							// wenn letzte Pr�fung noch immer fehlschl�gt erfolgt ADPM-Eintrag sowie NIO-Status
							result = new Ergebnis( "PSdZInit", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", connectionCheckFailed, "", Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
						runs++;
					}
				}
			}

			// reset UserDialog
			userDialog.reset();

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Nur bei FZS + Funk, jetzt wieder die VIN nehmen, weil sonst in PSdZ mehr Sachen nicht mehr richtig gehen
		if( buseFZS && !vehicleVin.equalsIgnoreCase( "" ) && psdz.getDeviceParams()[0].equalsIgnoreCase( DiagnosticInterfaces.STD_FUNK.toString() ) )
			psdz.setVehicleVin( vehicleVin );

		// Freigabe der benutzten Devices
		if( psdz != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releasePSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
		try {
			getPr�flingLaufzeitUmgebung().releaseUserDialog();
		} catch( Exception e ) {
			if( e instanceof DeviceLockedException )
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "PSdZInit", "PSdZ", "", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}
		setPPStatus( info, status, ergListe );
		// Performancemessung Ende
		if( bDebugPerform )
			System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZInit PP ENDE" );

	}
}

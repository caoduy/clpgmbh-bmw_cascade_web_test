/*
 * DiagSetInterface_5_0_F_Pruefprozedur.java
 *
 * Created on 26.11.14
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.io.*;
import java.text.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;

/**
 * Pr�fprozedur, welche den Inhalt eines Verzeichnisses (inkl. 
 * Unterverzeichnisse) oder ausgew�hlte Dateien kopiert. 
 *
 * F�r die beiden zwingenden Argumente SRC und DEST gilt:
 *   
 * Wenn das optionale Argument PARSE auf "SRC" (f�r Quellverzeichnis), "DEST" 
 * (f�r Zielverzeichnis) oder "BOTH" (f�r Quell- und Zielverzeichnis) gesetzt  
 * ist, dann wird das jeweilige Verzeichnis vor seiner Verwendung geparst. Die 
 * nachfolgend aufgef�hrten Codes werden hierbei automatisch 
 * wie folgt ersetzt: 
 * 
 * 	[YEAR4] = Jahr 4-stellig (z. B. 2008)
 * 	[YEAR2] = Jahr 2-stellig (z. B. 08)
 * 	[MONTH2] = Monat 2-stellig (z. B. 03 f�r M�rz)
 * 	[DAY2] = Tag des Monats (z. B. 14)
 * 	[HOUR] = Stunde
 * 	[MIN] = Minute
 * 	[SEC] = Sekunde
 * 	[VIN7] = 7-stellige Fahrgestellnummer
 * 
 * Da [ und ] Anfang und Ende der Codes kennzeichnen, sind diese,
 * wenn sie unver�ndert �bernommen werden sollen, wie folgt zu 
 * schreiben (nur wenn PARSE = TRUE ist):
 * 
 * 	[ als [[
 * 	] als ]]
 *
 *  
 * Zwingende Argumente:
 *
 * SRC:			Quellverzeichnis dessen Inhalt kopiert werden soll. Alternativ 
 * 				k�nnen auch ausgew�hlte Dateien spezifiziert werden (eine 
 * 				einzelne Datei durch ihren Namen oder mehrere Dateien mittels
 * 				Wildcards). Als Wildcard (Platzhalter) ist nur der * zul�ssig. 
 * 				Falls eine Wildcard-Angabe auch f�r ein Unterverzeichnis 
 * 				zutrifft, dann wird dieses ebenfalls kopiert. 
 * 				Gro�-/Kleinschriebung wird nicht unterschieden.  
 * 				
 * 				Beispiele:
 * 
 * 				C:/EC-Apps/Cascade/log/pruefstand*.log (alle Dateien/Verzeichnisse mit "pruefstand" am Anfang und ".log" am Ende)
 * 				C:/EC-Apps/Cascade/log/pruefstand* (alle Dateien/Verzeichnisse mit "pruefstand" am Anfang) 
 * 				C:/EC-Apps/Cascade/daten_test/dom/temp/ZL25031/*C.xml (alle Dateien/Verzeichnisse mit "C.xml" am Ende)
 * 				C:/EC-Apps/Cascade/log/pruefstandScreen.log (nur die Datei/das Verzeichnis pruefstandScreen.log)
 * 				C:/EC-Apps/Cascade/log/*Screen* (alle Dateien/Verzeichnisse, welche im Text "Screen" enthalten)			 
 * 				C:/EC-Apps/Cascade/log/pruefstand*.* (alle Dateien/Verzeichnisse, welche im Text "." enthalten und mit "pruefstand" beginnen)
 *
 * 				ACHTUNG: Falls man weder eine Datei noch eine Wildcard-Angabe
 * 				machen m�chte - also ein Verzeichnis kopiert werden soll - dann 
 * 				gilt:
 * 
 * 				Endet der Pfad mit einem Dateitrennzeichen ("\" oder "/"), dann 
 * 				wird nur der Inhalt (!) des Verzeichnisses kopiert, anderfalls 
 * 				das Verzeichnis direkt. Hinweis: Falls am Ende ein 
 * 				Dateitrennzeichen spezifiziert wird, werden *  nicht aufgel�st, 
 * 				da Wildcards nur dazu dienen, dass zu kopierende Element direkt 
 * 				zu beschreiben.
 * 
 *  			Beispiele:
 *  			
 *  			C:/EC-Apps/Cascade/log ("log" wird als zu kopierende Datei [!] angesehen, falls ein Verzeichnis mit diesem Namen existieren sollte, wird es unter dem Namen "log" in das Zielverzeichnis kopiert)
 *  			C:/EC-Apps/Cascade/log/ ("log" wird als Verzeichnis angesehen, dessen Inhalt zu kopieren ist; im Zielverzeichnis befindet sich dann nur der Inhalt von "log", nicht jedoch "log" selbst)
 * 
 * DEST:		Zielverzeichnis, in welches der Inhalt des Quellverzeichnisses
 * 				kopiert wird bzw. die spezifizierten Quelldateien kopiert 
 * 				werden. Falls das Zielverzeichnis nicht existiert, wird es
 * 				angelegt.  
 *
 * Optionale Argumente:
 * 
 * MOVE:		Wenn TRUE, dann werden die Quelldaten (Dateien und 
 * 				Verzeichnisse) verschoben statt kopiert.
 * 
 * PARSE: 		Wenn TRUE (Kompatibilit�t zu alter PP-Version) oder DEST, dann 
 * 				wird die Zielverzeichnisangabe wie oben beschrieben geparst. Bei 
 * 				SRC wird das Quellverzeichnis geparst, und bei BOTH beide.
 * 
 * NIO_WHEN_CONTENT_MISSING: Wenn TRUE, dann endet die PP mit dem Status NIO, 
 * 				falls kein Inhalt zum Kopieren/Verschieben gefunden wurde. 
 * 				Hinweis: Unterverzeichnisse (selbst wenn sie leer sind) gelten
 * 			    jedoch als Inhalt (falls also lediglich ein leeres 
 * 				Unterverzeichnis kopiert/verschoben wurde, erfolgt kein 
 * 				NIO-Setzen). Default: FALSE.
 *
 * @author Peter Rettig, TI-545<BR>
 * @version 1_0_F PR 10.01.2008 Initiale Implementierung<BR>
 * 			2_0_F PR 27.02.2008 Dokumentiere "scr" und "des" Verzeichnis im virt. Fahrzeug.<BR>
 * 			3_0_F PR 18.02.2008 Parsing auch in SRC implementiert.<BR>
 * 			4_0_T PR 21.11.2014 Es wurde ein Parameter hinzugef�gt, welcher steuert, ob die Pr�fprozedur NIO sein soll, wenn substanziell nichts kopiert/verschoben wurde (z. B. weil der Quellpfad nicht exitiert). Siehe LOP-Punkt 1849.<BR>
 * 			5_0_F PR 26.11.2014 �nderungen der 4_0_T produktiv geschaltet.<BR>
 *   		6_0_T FS 12.05.2016 Optionaler ACCESS_TIMEOUT Parameter f�r Dateizugriff eingef�hrt.Default: 10 Sekunden.<BR>
 *   		7_0_T MK 07.06.2016 Default Timeout auf 5 Sekunden angepasst.<BR>
 *     		8_0_F MK 07.06.2016 Produktive Freigabe Version V_7_0_T.<BR>
 *      	9_0_T FS 13.06.2016 Bugfix bei Zielverzeichniserstellung.<BR>
 *         10_0_T FS 13.06.2016 Produktive Freigabe Version V_9_0_T.<BR>
 */
public class DateiCopy_10_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	static final long DEFAULT_TIMEOUT = 5;

	public boolean nioWhenContentMissing = false; //gibt an, ob die Pr�fprozedur NIO sein soll, falls nichts zum Kopieren/Verschieben gefunden wurde
	public boolean contentMissing = true; //Angabe, dass kein Inhalt (inkl. Unterverzeichnisse) zum Kopieren oder Verschieben vorgefunden wurde
	public long timeout = DEFAULT_TIMEOUT;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DateiCopy_10_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 *
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DateiCopy_10_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 *
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = { "SRC", "DEST" };
		return args;
	}

	/**
	 * Liefert die optionalen Argumente.
	 *
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "MOVE", "PARSE", "NIO_WHEN_CONTENT_MISSING", "ACCESS_TIMEOUT" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 *
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt
	 *         hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		if( !super.checkArgs() )
			return false;

		return true;
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 *
	 * @param info Information zur Ausf�hrung.
	 */

	public void execute( ExecutionInfo info ) {
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		//eigene Variablen
		String src;
		String dest;
		boolean move = false; //verschieben?
		boolean parse_src = false; //Quellverzeichnis parsen?
		boolean parse_dest = false; //Zielverzeichnis parsen?         
		String buffer;

		try {

			//Parameter holen und Argumente pruefen.
			try {
				// Ist der allgemeine Check fehlgeschlagen?
				if( !checkArgs() )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// Hole die Quellangabe.
				if( getArg( getRequiredArgs()[0] ).indexOf( '@' ) != -1 )
					src = getPPResult( getArg( getRequiredArgs()[0] ) );
				else
					src = getArg( getRequiredArgs()[0] );

				// Hole die Zielangabe.
				if( getArg( getRequiredArgs()[1] ).indexOf( '@' ) != -1 )
					dest = getPPResult( getArg( getRequiredArgs()[1] ) );
				else
					dest = getArg( getRequiredArgs()[1] );

				// Hole die Angabe, welche festlegt, ob wir verschieben sollen.
				buffer = getArg( getOptionalArgs()[0] );
				if( buffer != null && buffer.equalsIgnoreCase( "TRUE" ) )
					move = true;

				// Hole die Angabe, welche festlegt, ob wir die Zielverzeichnis- 
				// angabe parsen sollen.
				buffer = getArg( getOptionalArgs()[1] );
				if( buffer != null ) {
					if( buffer.equalsIgnoreCase( "TRUE" ) )
						parse_dest = true;
					else if( buffer.equalsIgnoreCase( "DEST" ) )
						parse_dest = true;
					else if( buffer.equalsIgnoreCase( "SRC" ) )
						parse_src = true;
					else if( buffer.equalsIgnoreCase( "BOTH" ) ) {
						parse_src = true;
						parse_dest = true;
					}
				}

				// Hole die Angabe, welche festlegt, ob die Pr�fprozedur NIO 
				// sein soll, falls nichts zum Kopieren/Verschieben gefunden 
				// wurde.
				buffer = getArg( getOptionalArgs()[2] );
				if( buffer != null && buffer.equalsIgnoreCase( "TRUE" ) )
					nioWhenContentMissing = true;

				//Optionalen Parameter TIMEOUT holen
				if( getArg( getOptionalArgs()[3] ) != null ) {
					timeout = Long.parseLong( getArg( getOptionalArgs()[3] ).trim() );
				}

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Kopiere.
			try {
				copy( src, dest, move, parse_src, parse_dest, ergListe );
			} catch( IOException e ) {
				result = new Ergebnis( "Status", "DateiCopy", src, dest, (parse_src ? "parse source" : "") + ((parse_src && parse_dest) ? "/" : "") + (parse_dest ? "parse destination" : ""), move ? "move" : "copy", "ERROR", "", "", "", "", "", "", e.toString(), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			result = new Ergebnis( "Status", "DateiCopy", src, dest, (parse_src ? "parse source" : "") + ((parse_src && parse_dest) ? "/" : "") + (parse_dest ? "parse destination" : ""), move ? "move" : "copy", "OK", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO );
			e.printStackTrace();
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		setPPStatus( info, status, ergListe );
	}

	/**
	 * Kopiert/Verschiebt Dateien und Verzeichnisse.
	 * 
	 * @param src Quellverzeichnis. Ein * als Wildcard ist zul�ssig (mehrere
	 * 		  erlaubt, z. B. "*.*"). Falls man weder eine Datei noch eine 
	 * 		  Wildcard-Angabe machen m�chte - also nur ein komplettes 
	 * 		  Verzeichnis kopiert werden soll - dann muss die Pfadangabe mit 
	 * 		  einem Dateitrennzeichen enden ("\" oder "/").     
	 * @param dest Zielverzeichnis.
	 * @param move Bei <code>true</code> werden die kopierten Daten nach dem 
	 * 		  Kopieren gel�scht. 
	 * @param parse_src Wenn <code>true</code>, dann wird die 
	 * 		  Quellverzeichnisangabe geparst. 
	 * @param parse_dest Wenn <code>true</code>, dann wird die 
	 * 		  Zielverzeichnisangabe geparst.
	 * @param Ergebnisliste fuer die Dokumentation			
	 * 
	 * @throws IOException Falls es bei einer Dateioperationen zu einem Fehler 
	 *         kommt.
	 * @throws PPExecutionException 
	 */
	private void copy( String src, String dest, boolean move, boolean parse_src, boolean parse_dest, Vector ergListe ) throws IOException, PPExecutionException {
		//final long timeout = 5000; //maximale Zeit, die ein exists auf dem Zielverzeichnis dauern darf (wird f�r den internen Timeout-Check ben�tigt)
		final String srcDir; //Quellverzeichnis (bereinigt um Datei- oder Wildcard-Angaben)
		final String filterPattern; //Wildcard-Angabe (z. B. "*.log") oder einzelner Dateiname (z. B. "pruefstandScreen.log")
		final boolean wildcardAsPrefix; //Angabe, ob ganz am Anfang der Wildcard-Angabe ein Wildcard steht  
		final boolean wildcardAsSuffix; //Angabe, ob ganz am Ende der Wildcard-Angabe ein Wildcard steht    	
		final ArrayList tokens = new ArrayList(); //Speichert die Suchbegriffe der Wildcard-Angabe
		final FilenameFilter filter; //FilenameFilter, der sp�ter u. a. gem�� der Wildcard filtert
		final File destDirFile; //File-Objekt auf das Zielverzeichnis
		Ergebnis result; // Result fuer die Dokumentation

		//  Initialisierung.
		contentMissing = true; //Variable bei jedem execute() initialisieren, da bei F6 (Wiederholen) noch mit ihrem alten Wert vorbelegt ist

		//  Parse die Verzeichnisse, falls gew�nscht.
		if( parse_src )
			src = parse( src );
		if( parse_dest )
			dest = parse( dest );

		//  Quellverzeichnis dokumentieren
		result = new Ergebnis( "SRC", "DateiCopy", "", "", "", "Source", src, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
		ergListe.add( result );

		// 	Zielverzeichnis dokumentieren
		result = new Ergebnis( "DEST", "DateiCopy", "", "", "", "Destination", dest, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
		ergListe.add( result );

		//  Ermittle das Quellverzeichnis sowie das Filtermuster, das angibt,
		//  welche Dateien im Quellverzeichnis zu ber�cksichtigen sind. Hinweis: 
		//  Das Filtermuster ist �berlicherweise eine Wildcard-Angabe (z. B. 
		//  "*.log" oder *.*), aber auch reine Dateinamen sind m�glich (z. B. 
		//  "pruefstandScreen.log". Im letzteren Fall, wird dann sp�ter eben nur 
		//  nach dem Dateinamen gefiltert. 
		srcDir = getDir( src );
		filterPattern = getFile( src );

		//	Pr�fe, ob Wildcards (im Filtermuster) verwendet werden und zerlege 
		//  das Muster in die Suchbegriffe (bei "pruefstand*.log" dann 
		//  "pruefstand" und ".log").    		   		
		//  Finde den Text zwischen den Wildcards.
		if( filterPattern != null ) {
			StringTokenizer t = new StringTokenizer( filterPattern, "*" );
			while( t.hasMoreTokens() )
				tokens.add( t.nextToken() );
			wildcardAsPrefix = filterPattern.startsWith( "*" );
			wildcardAsSuffix = filterPattern.endsWith( "*" );
		} else {
			wildcardAsPrefix = false; //kann nicht als Default mit false vorbelegen, da final
			wildcardAsSuffix = false; //kann nicht als Default mit false vorbelegen, da final
		}

		//  Erzeuge den Filter.
		filter = new FilenameFilter() {
			public boolean accept( File dir, String name ) {
				int searchIndex = 0;
				String token;
				name = name.toUpperCase();

				if( tokens.size() == 1 && !wildcardAsPrefix && !wildcardAsSuffix ) { //Keine Wildcards benutzt?
					return name.equalsIgnoreCase( (String) tokens.get( 0 ) );
				} else { //Vorgehen, wenn Wildcards gesetzt sind        	
					for( int i = 0; i < tokens.size(); i++ ) {
						token = ((String) tokens.get( i )).toUpperCase();

						if( i == 0 && !wildcardAsPrefix ) { //Erstes Token und kein (!) Stern am Anfang?
							if( name.startsWith( token ) )
								searchIndex = token.length();
							else
								return false;
						} else if( i == tokens.size() - 1 && !wildcardAsSuffix ) { //Letztes Token und kein (!) Stern am Ende??
							if( !name.endsWith( token ) || (searchIndex + token.length() > name.length()) ) //Letztes Token nicht gefunden bzw. es beginnt vor dem Suchindex?
								return false;
						} else {
							searchIndex = name.indexOf( token, searchIndex );
							if( searchIndex == -1 )
								return false;
							searchIndex = searchIndex + token.length(); //neuer Suchstart soll nach der aktuellen Fundstelle liegen     					
						}
					}
					return true;
				}
			}
		};

		// Erzeuge Fileobjekte f�r Source und Destination
		destDirFile = new File( dest );
		File srcDirFile = new File( srcDir );
		try {
			// Pr�fe angegebenes Verzeichnis in Executorthread um einen Timeout zu erm�glichen
			ExecutorService exSrv = Executors.newSingleThreadExecutor();
			ExecutorCompletionService completionService = new ExecutorCompletionService( exSrv );
			completionService.submit( new CheckDirectories( srcDirFile, destDirFile ) );
			Future<Boolean> checkDirectoriesComplete = completionService.poll( timeout, TimeUnit.SECONDS );
			//Pr�fe, ob der Quellpfad existiert und versuche Destinationpfad zu erzeugen (bei Fehler -> ExecutionException)
			if( checkDirectoriesComplete != null && checkDirectoriesComplete.get() == true ) {
				exSrv.shutdown();
				//Kopiere
				copyDir( srcDirFile, destDirFile, filter, move );
				if( contentMissing && nioWhenContentMissing ) //Wurde nichts zum Kopieren/Verschieben gefunden (inkl. Unterverzeichnisse) und ist dies ein Problem?
					throw new IOException( "No (matching) content to copy/move found!" );
			} else {
				exSrv.shutdown();
				if( nioWhenContentMissing ) //Exception nur ausl�sen, wenn fehlende Quelldaten ein Problem sind
					throw new IOException( "Source path " + srcDirFile.getCanonicalPath() + " does not exist!" );
			}
		} catch( ExecutionException e ) {
			// Bei nicht erzeugbarem DestinationDir kommt es zu einer IOException im ExecutorThread
			// Leite diese weiter nach au�en
			throw new IOException( e );
		} catch( Exception e ) {
			throw new PPExecutionException();
		}
	}

	/**
	 * Ermittle die Position des letzten Dateitrennzeichens. F�r den Fall, 
	 * dass der Benutzer gemischte Dateitrennzeichen verwendet, werden 
	 * verschiedene Typen von Dateitrennzeichen (/, \ etc) ausprobiert und am  
	 * Ende die Position von demjenigen genommen, welches am weitesten hinten 	 
	 * steht.
	 * 
	 * @param file Der auszuwertende String.
	 * 
	 * @return Die Position des letzten Dateitrennzeichens oder -1, falls kein
	 * 		   solches gefunden werden konnte.
	 */
	private int getLastSeparator( String file ) {
		String[] separators = { "/", "\\", File.separator, ":" }; //verf�gbare Dateitrennzeichen
		int dummy;
		int lastSeparator = -1;

		//  �berpr�fe die G�ltigkeit der �bergebenen Parameter.
		if( file == null )
			return -1;

		//  Gehe alle m�glichen Dateitrennzeichen durch.
		for( int i = 0; i < separators.length; i++ ) {
			dummy = file.lastIndexOf( separators[i] ); //finde letztes Auftreten des aktuellen Dateitrennzeichens
			if( dummy > lastSeparator ) //Ist die Position hinter der des des zuletzt �berpr�ften Dateitrennzeichens?
				lastSeparator = dummy;
		}

		return lastSeparator;
	}

	/**
	 * Ermittelt aus einer Pfadangabe den Verzeichnisteil. Eine eventuelle
	 * Dateiangabe oder Wildcard-Angabe wird abgeschnitten.
	 * 
	 * @param file Der auszuwertende String.
	 * @return Der Verzeichnisteil oder <code>null</code>, falls kein solcher 
	 * 		   existiert.
	 */
	private String getDir( String file ) {
		int lastSeparator;

		//  �berpr�fe die G�ltigkeit der �bergebenen Parameter.
		if( file == null )
			return null;

		//  Ermittle die Position des letzten Dateitrennzeichens.
		lastSeparator = getLastSeparator( file );

		//  �bergebe nun den Teil, der sich vor dem letzten Dateitrennzeichen 
		//  befindet (inkl. dem Dateitrennzeichen selbst).
		if( lastSeparator == -1 ) //Kein Dateitrennzeichen gefunden?
			return file.substring( 0, file.length() ); //wenn nicht gefunden, dann gilt alles als Pfad (-> keine Dateiangabe)
		else
			return file.substring( 0, lastSeparator + 1 );
	}

	/**
	 * Ermittelt aus einer Pfadangabe den Dateinamen bzw. eine eventuelle
	 * Wildcard-Angabe.
	 * 
	 * @param file Der auszuwertende String.
	 * @return Der Dateiname/die Wildcard-Angabe oder <code>null</code>, 
	 * 		   falls keine derartige Information existiert (z. B. weil nur 
	 * 		   Verzeichnisse angegeben sind).
	 */
	private String getFile( String file ) {
		String result;
		int lastSeparator;

		//  �berpr�fe die G�ltigkeit der �bergebenen Parameter.    	
		if( file == null )
			return null;

		//  Ermittle die Position des letzten Dateitrennzeichens.
		lastSeparator = getLastSeparator( file );
		if( lastSeparator == -1 ) //Kein Dateitrennzeichen gefunden?
			return null;

		//  �bergebe nun den Teil, der sich nach dem letzten Dateitrennzeichen 
		//  befindet (ohne das Dateitrennzeichen selbst).
		if( lastSeparator + 1 < file.length() ) //War das Dateitrennzeichen nicht das allerletzte Zeichen?
			result = file.substring( lastSeparator + 1, file.length() );
		else
			return null; //Dateitrennzeichen steht ganz am Ende

		return result;
	}

	/**
	 * Kopiert eine Datei.
	 * 
	 * @param src Quelle.
	 * @param dest Ziel.
	 * @throws IOException Bei einem Fehler.
	 */
	private void copyFile( File src, File dest ) throws IOException {
		//  Erzeuge den Quell- und Ziel-Channel.
		java.nio.channels.FileChannel srcChannel = new FileInputStream( src ).getChannel();
		java.nio.channels.FileChannel dstChannel = new FileOutputStream( dest ).getChannel();

		//  Kopiere.
		dstChannel.transferFrom( srcChannel, 0, srcChannel.size() );

		//  Schlie�e die Channels.
		srcChannel.close();
		dstChannel.close();
	}

	/**
	 * Kopiert ein Verzeichnis.
	 * 
	 * @param src Quelle.
	 * @param dest Ziel.
	 * @param filter Ein Filter, der festlegt, welche Dateien/Verzeichnisse 
	 * 		  zu ber�cksichtigen sind.
	 * @param move Wenn <code>true</code>, dann werden die Daten verschoben und 
	 * 		  nicht kopiert.
	 * @throws IOException
	 */
	private void copyDir( File src, File dest, FilenameFilter filter, boolean move ) throws IOException {
		File[] content;
		File newEntry;

		if( filter != null )
			content = src.listFiles( filter ); //ermittle den Verzeichnisinhalt mit Filterung
		else
			content = src.listFiles(); //ermittle den Verzeichnisinhalt ohne Filterung

		if( content != null && content.length != 0 ) {
			contentMissing = false; //halte fest, dass die Pr�fprozedur etwas zum Kopieren/Verschieben gefunden hat
			for( int i = 0; i < content.length; i++ ) { //gehe ermittelten Inhalt durch
				if( Thread.interrupted() ) //Interrupt gew�scht?
					throw new IOException( "Copy operation interrupted!" );

				newEntry = new File( dest, content[i].getName() ); //File-Objekt f�r die neue Datei/das neue Verzeichnis anlegen

				if( content[i].isDirectory() ) { //Unterverzeichnis?
					if( !newEntry.exists() )
						if( !newEntry.mkdirs() ) //Unterverzeichnis im Zielverzeichnis anlegen    				
							throw new IOException( "Unable to create directory " + newEntry.getCanonicalPath() + "!" );
					copyDir( content[i], newEntry, null, move ); //Unterverzeichnis bef�llen (Filter soll innerhalb von Unterverzeichnissen NICHT angewandt werden)
					if( move ) //Verschieben?
						if( !content[i].delete() ) //Verzeichnis wurde samt Inhalt kopiert und kann nun (im Rahmen des Verschiebens) gel�scht werden (Hinweis: rename funktioniert bei Netzwerklaufwerken nur bei Dateien)
							throw new IOException( "Unable to delete directory " + content[i].getCanonicalPath() + "!" );
				} else {
					if( move ) { //Verschieben?
						if( newEntry.exists() ) //Falls die Datei im Ziel bereits existiert, muss sie vor dem Verschieben gel�scht werden!
							if( !newEntry.delete() )
								throw new IOException( "Unable to delete file " + newEntry.getCanonicalPath() + "!" );
						if( !content[i].renameTo( newEntry ) ) //verschiebe Datei/Verzeichnis und pr�fe Erfolg
							throw new IOException( "Unable to move " + content[i].getCanonicalPath() + " to " + newEntry.getCanonicalPath() + "!" );
					} else
						copyFile( content[i], newEntry ); //Datei kopieren
				}
			}
		}
	}

	/**
	 * Parst die Angabe zum Zielverzeichnis und nimmt automatisch Ersetzungen 
	 * vor. Hierbei gilt:
	 * 
	 * [YEAR4] = Jahr 4-stellig (z. B. 2008)
	 * [YEAR2] = Jahr 2-stellig (z. B. 08)
	 * [MONTH2] = Monat 2-stellig (z. B. 03 f�r M�rz)
	 * [DAY2] = Tag des Monats (z. B. 14)
	 * [HOUR] = Stunde
	 * [MIN] = Minute
	 * [SEC] = Sekunde
	 * [VIN7] = 7-stellige Fahrgestellnummer
	 * [[ = [
	 * ]] = ]
	 *  
	 * @param pattern Zu parsende Angabe.
	 * @return Die �bergebene Angabe mit den vorgenommenen Ersetzungen.
	 */
	private String parse( String pattern ) {
		StringBuffer stringBuffer = new StringBuffer();
		Date date = new Date();

		int i = 0;
		while( i < pattern.length() )
			if( pattern.charAt( i ) == '[' ) { //Beginn eines Pattern letter?
				if( pattern.startsWith( "[YEAR4]", i ) ) { //Jahr 4-stellig?    				
					stringBuffer.append( new SimpleDateFormat( "yyyy" ).format( date ) );
					i += 7;
				} else if( pattern.startsWith( "[YEAR2]", i ) ) { //Jahr 2-stellig?
					stringBuffer.append( new SimpleDateFormat( "yy" ).format( date ) );
					i += 7;
				} else if( pattern.startsWith( "[MONTH2]", i ) ) { //Monat 2-stellig?
					stringBuffer.append( new SimpleDateFormat( "MM" ).format( date ) );
					i += 8;
				} else if( pattern.startsWith( "[DAY2]", i ) ) { //Tag 2-stellig?
					stringBuffer.append( new SimpleDateFormat( "dd" ).format( date ) );
					i += 6;
				} else if( pattern.startsWith( "[HOUR]", i ) ) { //Stunde?
					stringBuffer.append( new SimpleDateFormat( "HH" ).format( date ) );
					i += 6;
				} else if( pattern.startsWith( "[MIN]", i ) ) { //Minute?
					stringBuffer.append( new SimpleDateFormat( "mm" ).format( date ) );
					i += 5;
				} else if( pattern.startsWith( "[SEC]", i ) ) { //Sekunde?
					stringBuffer.append( new SimpleDateFormat( "ss" ).format( date ) );
					i += 5;
				} else if( pattern.startsWith( "[VIN7]", i ) ) { //kurze Fahrgestellnummer?
					stringBuffer.append( getPr�fling().getAuftrag().getFahrgestellnummer7() );
					i += 6;
				} else if( pattern.startsWith( "[[", i ) ) { //[-Zeichen?
					stringBuffer.append( "[" );
					i += 2;
				} else { //Code nicht gefunden
					stringBuffer.append( pattern.charAt( i ) );
					i++; //Zeichen ([) anh�ngen
				}
			} else { //Nein, dann normales Zeichen oder ].
				if( pattern.startsWith( "]]", i ) ) { //]-Zeichen?
					stringBuffer.append( "]" );
					i += 2;
				} else {
					stringBuffer.append( pattern.charAt( i ) );
					i++;
				}
			}

		return stringBuffer.toString();
	}

	/**
	 * NUR ZUM TEST. Methode kapselt die komplette Wildcard-Funktionalit�t.
	 * 
	 * @param input String, der unter Wildcard-Sicht zu �berpr�fen ist.
	 * @param filterPattern Die Wildcard-Angabe
	 * @return <code>True</code>, falls der �bergebene String von der 
	 * 		  Wildcard-Angabe akzeptiert wurde.
	 */
	private boolean test( String input, String filterPattern ) {
		boolean wildcardAsPrefix;
		boolean wildcardAsSuffix;

		System.out.println( input + " vs. " + filterPattern );

		//  Finde den Text zwischen den Wildcards.
		ArrayList tokens = new ArrayList();
		StringTokenizer t = new StringTokenizer( filterPattern, "*" );
		while( t.hasMoreTokens() )
			tokens.add( t.nextToken() );
		wildcardAsPrefix = filterPattern.startsWith( "*" );
		wildcardAsSuffix = filterPattern.endsWith( "*" );
		System.out.println( "   " + tokens + " (" + wildcardAsPrefix + ", " + wildcardAsSuffix + ")" );

		//  Wende das Suchmuster an.
		int searchIndex = 0;
		String token;
		input = input.toUpperCase();

		for( int i = 0; i < tokens.size(); i++ ) {
			token = ((String) tokens.get( i )).toUpperCase();

			if( i == 0 && !wildcardAsPrefix ) //Erstes Token und kein (!) Stern am Anfang?
				if( input.startsWith( token ) ) {
					searchIndex = token.length();
				} else {
					System.out.println( "  ...ersten Suchbegriff " + token + " nicht gefunden" );
					return false;
				}
			else if( i == tokens.size() - 1 && !wildcardAsSuffix ) { //Letztes Token und kein (!) Stern am Ende??
				if( !input.endsWith( token ) || (searchIndex + token.length() > input.length()) ) { //Letztes Token nicht gefunden bzw. es beginnt vor dem Suchindex?
					System.out.println( "  ...letzten Suchbegriff " + token + " nicht gefunden" );
					return false;
				}
			} else {
				searchIndex = input.indexOf( token, searchIndex );
				if( searchIndex == -1 ) {
					System.out.println( "  ...Suchbegriff " + token + " nicht gefunden" );
					return false;
				}
				searchIndex = searchIndex + token.length(); //neuer Suchstart soll nach der aktuellen Fundstelle liegen     					
			}
		}

		System.out.println( "  ...IO" );
		return true;
	}

	/**
	 * NUR ZUM TEST.
	 * 
	 * @param args Kommandozeilenparameter.
	 */
	public static void main( String args[] ) {
		try {
			DateiCopy_10_0_F_Pruefprozedur p = new DateiCopy_10_0_F_Pruefprozedur();
			/*
			String dest;    		   		
			dest = "C:/EC-Apps/Cascade[[0]]\\log/[YEAR2]_[MONTH2]_[DAY2]-[HOUR]_[MIN]_[SEC]_Backup_";
			System.out.println(dest + " -> " + p.parseDest(dest));    		
			dest = "C:/EC-Apps/Cascade[[0]]\\log/[DAY2]_[MONTH2]_[YEAR4]_[HOUR]_[MIN]_[SEC]_Backup_";
			System.out.println(dest + " -> " + p.parseDest(dest));    		
			dest = "C:/EC-Apps/Cascade[[0]]\\log/[YEAR2]_[MONTH2]_[DAY2]-[HOUR]_[MIN]_[SEC]_Backup\\test";
			System.out.println(dest + " -> " + p.parseDest(dest));
			dest = "[YEAR2][MONTH2][DAY2][HOUR][MIN][SEC]";    		
			System.out.println(dest + " -> " + p.parseDest(dest));
			dest = "C:/EC-Apps/Cascade[[0]]\\log/[YEAR2_[MONTH2x]_[DAY2]-[HOUR]_[MIN]_[SEC]_Backup\\test";
			System.out.println(dest + " -> " + p.parseDest(dest));
			dest = "";
			System.out.println(dest + " -> " + p.parseDest(dest));
			dest = "nix";
			System.out.println(dest + " -> " + p.parseDest(dest));
			*/
			Vector ergListe = new Vector();
			String s;

			s = "C:\\_QUELLE\\operator.xml";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

			s = "C:\\_QUELLE\\*operator.xml";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

			s = "C:\\_QUELLE\\operator.xml*";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

			s = "C:\\_QUELLE\\operator*.xml";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

			s = "C:\\_QUELLE\\operator*xml";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

			s = "C:\\_QUELLE\\*operator.xml*";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

			s = "C:\\_QUELLE\\*operator*.xml*";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

			s = "C:\\_QUELLE\\*operator*xml*";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

			s = "C:\\_QUELLE\\op*ml";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

			s = "C:\\_QUELLE\\oper*tor*xml";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

			s = "C:\\_QUELLE\\*oper*tor*xml";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

			s = "C:\\_QUELLE\\oper*tor*xml*";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

			s = "C:\\_QUELLE\\*oper*tor*xml*";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

			s = "C:\\_QUELLE\\tor.xml*opera";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

			s = "C:\\_QUELLE\\oper";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

			s = "C:\\_QUELLE\\.xml";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

			s = "C:\\_QUELLE\\oper*";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

			s = "C:\\_QUELLE\\*tor.xml";
			p.copy( s, "C:\\_ZIEL", false/*move*/, true/*parse src*/, true/*parse dest*/, ergListe );
			System.out.println( s + ": " + p.contentMissing );

		} catch( IOException e ) {
			e.printStackTrace();
		} catch( PPExecutionException e ) {
			e.printStackTrace();
		}
	}

	/**
	 *  Die Klasse CheckSourceCreateDestination erm�glicht ein threadbasiertes �berpr�fen/Erzeugen des Zielordners und des Sourceordners.
	 * 
	 * @author Sch�nert Fabian, TI-545 BMW AG
	 *
	 */
	class CheckDirectories implements Callable {

		private File srcDirFile;
		private File destDirFile;

		public CheckDirectories( File srcDirFile, File destDirFile ) {
			this.srcDirFile = srcDirFile;
			this.destDirFile = destDirFile;
		}

		@Override
		public Boolean call() throws IOException {
			// Pr�fe und ggfs erstelle destination Dir
			if( !destDirFile.exists() )
				if( !destDirFile.mkdirs() )
					throw new IOException( "Unable to create directory " + destDirFile.getCanonicalPath() + "!" );

			//  Pr�fe Source Directory
			if( !srcDirFile.exists()) {    
				return false;
			}
			
			// Verzeichniszugriffe alle erfolgreich
			return true;
		}
	}

}

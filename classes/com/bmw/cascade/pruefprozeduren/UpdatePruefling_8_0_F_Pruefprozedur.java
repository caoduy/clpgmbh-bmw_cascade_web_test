/*
 * UpdatePruefling_VX_X_X_ZZ_Pruefprozedur.java
 *
 * Created on 18.03.02
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.auftrag.prueflinge.PrueflingProperties;
import com.bmw.cascade.auftrag.prueflinge.PruefprozedurNotAvailableException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;

/**
 * Implementierung der Pr�fprozedur, die eine Initialisierung der Pr�fprozeduren eines Pr�flings ausl�st
 * @author M�ller, Crichton
 * @version   MM   18.03.02  V0_0_1_FA  Ersterstellung
 * @version   NC   04.04.02  V0_0_2_FA  Bug: do while Schleife �ber PPs korrigiert
 * @version   NC   05.04.02  V0_0_3_FA  Kommentar hinzu, PP Ausf�hrungsschleife umstruktuiert, DEBUG hinzu
 * @version   MM   05.04.02  V0_0_6_FA  �bersetzung der PL-Namen hinzu, checkArgs Aufruf hinzu
 * @version   CS   16.06.08  V7_0_T     APDM-Ergebnis angepasst
 */
public class UpdatePruefling_8_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    	static final long serialVersionUID = 1L;
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public UpdatePruefling_8_0_F_Pruefprozedur() {}

  /**
     * erzeugt eine neue Pruefprozedur.
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public UpdatePruefling_8_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    /**
     * initialsiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /**
     * liefert die optionalen Argumente
     */
    public String[] getOptionalArgs() {
        String[] args = {"PPNAME[1..N]"};
        return args;
    }
    
    /**
     * liefert die zwingend erforderlichen Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = {"PLNAME"};
        return args;
    }
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
     */
    public boolean checkArgs() {
        if (getArg(getRequiredArgs()[0]) == null) return false;
        if (getArg(getRequiredArgs()[0]).equals("") == true) return false;
        if (getArg("PPNAME1") == null) return false;
        if (getArg("PPNAME1").equals("") == true) return false;
        return true;
    }   
    
    /**
     * f�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        // spezifische Variablen
        String plname=null;
        Pruefling pruefling=null;
        Vector ppNames=new Vector();
        boolean secUpdate = false;
        boolean DEBUG = true;
        
        try {
            
            /**
             * Parameter holen und pr�fen
             */
            try{
                if (checkArgs()==false) throw new PPExecutionException();
                plname = PrueflingProperties.getPrueflingNameFromTranslation(getArg(getRequiredArgs()[0]));
                int ppAnzahl=0;
                String temp=null;
                if (DEBUG) System.out.println("UpdatePruefling... ");
                while( (temp = getArg("PPNAME"+(ppAnzahl+1))) != null) {
                    ppNames.add(temp);
                    ppAnzahl++;
                    if (DEBUG) System.out.println("UpdatePruefling "+temp);
                }
                
            } catch (Exception exc) {
                if (exc.getMessage() != null)
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), exc.getMessage(), Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            /**
             * Pr�fling holen
             */
            pruefling=this.getPr�fling().getAuftrag().getPr�fling(plname);
            if (DEBUG) System.out.println("UpdatePruefling "+pruefling);
            if (pruefling==null) {
                result = new Ergebnis( "Update", plname, "", "", "", "", "", "", "", "0", "", "", "", "", PB.getString( "PrueflingNotFound" ),  Ergebnis.FT_NIO );
                ergListe.add(result);
                status = STATUS_EXECUTION_ERROR;
            }
            
            /**
             * Pr�fen ob Pr�fling schon aktualisert ist
             */
            if (status!=STATUS_EXECUTION_ERROR) {
                if (pruefling.getAttribut("UPDATE").equalsIgnoreCase("TRUE")) {
                    result = new Ergebnis( "Update", plname, "", "", "", "", "", "", "", "0", "", "", "", "", PB.getString( "UpdateNotAvailable" ),  Ergebnis.FT_IO );
                    ergListe.add(result);
                    secUpdate = true;
                }
            }
            
            if (DEBUG) System.out.println( "PL UPDATE Before: "+pruefling.getAttribut("UPDATE") );
            
            if ((secUpdate == false)&&(status==STATUS_EXECUTION_OK)) {
                
                /**
                 * Pr�fprozeduren holen
                 */
                for (int i=0;i<ppNames.size();i++) {
                    Pruefprozedur pp=null;
                    pp=pruefling.getPr�fprozedur((String)(ppNames.elementAt(i)));
                    if (DEBUG) System.out.println("UpdatePruefling execute "+pp);
                    if (pp != null) {
                        pp.setPr�flingLaufzeitUmgebung(getPr�flingLaufzeitUmgebung());
                        pp.execute(info);
                        status = pp.getExecStatus();
                        if (DEBUG) System.out.println("UpdatePruefling executed"+pp);
                        if( status != STATUS_EXECUTION_OK ) {
                            result = new Ergebnis( "Update", plname, "", "", (String)(ppNames.elementAt(i)), "", "", "", "", "0", "", "", "", "", PB.getString( "executionFailed" ),  Ergebnis.FT_NIO );
                            ergListe.add(result);
                        }
                    }
                    else {
                        result = new Ergebnis( "Update", plname, "", "", (String)(ppNames.elementAt(i)), "", "", "", "", "0", "", "", "", "", PB.getString( "notFound" ),  Ergebnis.FT_NIO );
                        ergListe.add(result);
                        status=STATUS_EXECUTION_ERROR;
                    }
                }
                
                /**
                 * Pr�fprozeduren ausf�hren
                 */
                if (status==STATUS_EXECUTION_OK) {
                    try {
                        // calling pruefprozedurenUpdate
                        pruefling.pr�fprozedurenUpdate();
                    } catch (PruefprozedurNotAvailableException ppnae) {
                        result = new Ergebnis( "Update", plname, "", "", "", "", "", "", "", "0", "", "", "", ppnae.getMessage(), PB.getString( "PruefprozedurNotAvailable" ),  Ergebnis.FT_NIO );
                        ergListe.add(result);
                        status = STATUS_EXECUTION_ERROR;
                    }
                }
                
                /**
                 * Pr�fling UPDATE Attribut auf TRUE setzen
                 */
                if (status==STATUS_EXECUTION_OK) {
                    // Update successful
                    pruefling.updateAttribut("UPDATE","TRUE");
                    result = new Ergebnis( "Update", plname, "", "", "", "", "", "", "", "0", "", "", "", "", "",  Ergebnis.FT_IO );
                    ergListe.add(result);
                }
                
            }
            
            if (DEBUG) System.out.println( "PL UPDATE After: "+pruefling.getAttribut("UPDATE") );
            
        } catch (Exception e) {
            result = new Ergebnis( "Update", plname, "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ),  Ergebnis.FT_NIO_SYS );
            e.printStackTrace();
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable t) {
            result = new Ergebnis( "Update", plname, "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ),  Ergebnis.FT_NIO_SYS );
            t.printStackTrace();
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        setPPStatus( info, status, ergListe );
    }
    
    
}


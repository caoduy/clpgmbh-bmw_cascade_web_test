package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;
import java.lang.Math;
import java.awt.*;
import java.awt.image.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.text.Caret;
import javax.imageio.ImageIO;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.ediabas.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.visualisierung.*;

/** SDiagAFSinit Pruefprozedur<BR>
 * <BR>
 * Created on 29.01.2007<BR>
 * <BR>
 * AFS initizialization process implementation.<BR>
 * The current angle is shown in a graphic interface for better understanding.<BR>
 * Evaluation is made after the 2nd, 3rd, 4th, ... turn while it is not valid and not timeout.<BR>
 * <BR>
 * @author PJ Pedro Jorge (IndustrieHansa GmbH), Ti-432, BMW AG<BR>
 *
 * @version 1_0_F 09.02.2007 PJ AFS initialization process with graphical user interface.<BR>
 * @version 2_0_F 14.02.2007 PJ Small improvements.<BR>
 * @version 3_0_F 04.05.2007 PJ Corrected APDM results.<BR>
 * @version 4_0_T 30.08.2007 PJ Added all the missing EDIABAS parameters as optional arguments (for backward compatibility).
 * 								Added the optional parameter ANGLE_RANGE for more flexibility.
 * @version 5_0_T 11.09.2007 PJ Final test version. 
 * @version 6_0_F 14.09.2007 PJ Changed to an F version.  
 * 								
 */
public class SDiagAFSinit_6_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    	static final long serialVersionUID = 1L;
    	// application states
		private static final int MIDDLE2LEFT = 0;
		private static final int LEFT2RIGHT = 1;
		private static final int RIGHT2MIDDLE = 2;
		private static final int END = 3;
		// area positions
		private static final int OTHER = -1;
		private static final int MIDDLE = 0;
		private static final int LEFT = 1;
		private static final int RIGHT = 2;
		// angle grafic consts
		private static final int AXIS_WIDTH = 1;					//px (graph axis width)
		private static final int BAR_WIDTH = 40;					//px (graph bar width)
		private static final Color COLOR_BORDER = Color.ORANGE; 			// borders
		private static final Color COLOR_BACKGROUND = Color.BLACK; 			// background
		private static final Color COLOR_AREA = Color.BLUE;					// area
		private static final Color COLOR_BAR_DEFAULT = Color.ORANGE; 		// angle bar default
		private static final Color COLOR_BAR_ACTIVE = Color.GREEN; 			// angle bar inside area

		
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public SDiagAFSinit_6_0_F_Pruefprozedur() {}

  /**
     * erzeugt eine neue Pruefprozedur mit obigem Verhalten
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public SDiagAFSinit_6_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    /**
     * initialisiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /**
     * liefert die zwingend erforderlichen Argumente
     * @return Stringvektor der ben�tigten Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = {
        		"SGBD", 
        		"BORDER_ANGLE_MIDDLE", 
				"BORDER_ANGLE_SIDE", 
				"TIMEOUT"
		};
        return args;
    }

    /**
     * liefert die optionalen Argumente
     * @return Stringvektor der optionalen Argumente
     */
    public String[] getOptionalArgs() {
        String[] args = {
        		"SGBD_JOB1",			// DEFAULT = "STATUS_FAHRERLENKWINKEL" 
        		"SGBD_ARGUMENTS1",		// DEFAULT = "" 
        		"SGBD_RESULT1",			// DEFAULT = "STAT_FAHRER_LENKWINKEL_WERT" 
        		"SGBD_JOB2",			// DEFAULT = "STATUS_LWM_INIT" 
        		"SGBD_ARGUMENTS2",		// DEFAULT = "" 
        		"SGBD_RESULT2",			// DEFAULT = "STAT_GUELTIGKEIT_LWM_WERT" 
        		"ANGLE_RANGE",			// DEFAULT = 2*(BORDER_ANGLE_SIDE + 50) 
        		"TEXT_MESSAGE",			// DEFAULT = ... 
        		"TEXT_SIZE",			// DEFAULT = 70 
        		"POLLING_TIME",			// DEFAULT = 100 ms
        		"END_TIME",				// DEFAULT = 3000 ms
				"DEBUG",				// DEFAULT = FALSE 
				"SIMULATION"			// DEFAULT = FALSE
		};
        return args;
    }
        
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
     * @return true, wenn die Ueberpruefung der zwingenden Argumente ok ist
     */
    public boolean checkArgs() {
        try {
        	if (!super.checkArgs())
    			throw new Exception("super.check args failed!");
    		
        	//---------------------- req: SGBD, BORDER_ANGLE_MIDDLE, BORDER_ANGLE_SIDE, TIMEOUT
        	// BORDER_ANGLE_MIDDLE > 0
    		if (new Float(extractArg("BORDER_ANGLE_MIDDLE")).floatValue() <= 0f) 
    			throw new Exception("BORDER_ANGLE_MIDDLE out of range! (>0): " + extractArg("BORDER_ANGLE_MIDDLE"));
        	// BORDER_ANGLE_SIDE > 0
    		if (new Float(extractArg("BORDER_ANGLE_SIDE")).floatValue() <= 0f) 
    			throw new Exception("BORDER_ANGLE_SIDE out of range! (>0): " + extractArg("BORDER_ANGLE_SIDE"));
        	// TIMEOUT > 0
    		if (new Long(extractArg("TIMEOUT")).longValue() <= 0L) 
    			throw new Exception("TIMEOUT out of range! (>0): " + extractArg("TIMEOUT"));
    		
    		//---------------------- opt: ANGLE_RANGE, TEXT_SIZE, POLLING_TIME, END_TIME
    		// ANGLE_RANGE > 0
    		if (extractArg("ANGLE_RANGE") != null)
    			if (new Float(extractArg("ANGLE_RANGE")).floatValue() < 2*new Float(extractArg("BORDER_ANGLE_SIDE")).floatValue()) 
    				throw new Exception("ANGLE_RANGE out of range! (< 2*BORDER_ANGLE_SIDE): " + extractArg("ANGLE_RANGE"));
    		// TEXT_SIZE > 0
    		if (extractArg("TEXT_SIZE") != null && new Integer(extractArg("TEXT_SIZE")).intValue() <= 0) 
    			throw new Exception("TEXT_SIZE out of range! (>0): " + extractArg("TEXT_SIZE"));
    		// POLLING_TIME > 0
    		if (extractArg("POLLING_TIME") != null && new Long(extractArg("POLLING_TIME")).longValue() <= 0L) 
    			throw new Exception("POLLING_TIME out of range! (>0): " + extractArg("POLLING_TIME"));
    		// END_TIME > 0
    		if (extractArg("END_TIME") != null && new Long(extractArg("END_TIME")).longValue() <= 0L) 
    			throw new Exception("END_TIME out of range! (>0): " + extractArg("END_TIME"));
    		return true;
            
        } catch (Exception e) {
        	log(true, "Error", "checking args, Exception: "+ e.getMessage());
            return false;
        } catch (Throwable e) {
        	log(true, "Error", "checking args, Throwable: "+ e.getMessage());
            return false;
        }
    }
    
    /**
     * f�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) 
    {
		boolean debug = ((getArg("DEBUG") != null) && (getArg("DEBUG").equalsIgnoreCase("TRUE"))) ? true : false;
		boolean simulation = ((getArg("SIMULATION") != null) && (getArg("SIMULATION").equalsIgnoreCase("TRUE"))) ? true : false;
		// swings
		JFrame frame = null;
		Font stdFont = null;
		JPanel topPanel = null; 			// TOP panel
		JTextArea msgArea = null;
		JPanel middlePanel = null; 			// MIDDLE panel
		JLabel imgLabel = null;
		JPanel bottomPanel = null; 			// BOTTOM panel
		JPanel anglePanel = null;
		JLabel angleLabel = null; 
		JPanel testPanel = null;
		JLabel codeLabel = null; 			
		JSlider testSlider = null; 			
		JCheckBox testCheck = null; 		
		JPanel countPanel = null;
		JLabel countLabel = null; 			
		// aux
		int status = STATUS_EXECUTION_OK;
        Vector resList = new Vector();
		String job = "", jobpar = "", jobres = "", diagRes = "";
		BufferedImage img = null;
		long actualTime = System.currentTimeMillis(), startTime = actualTime;
		long borderCrossTime = actualTime;
		boolean firstBorderCross = true;
		int appState = MIDDLE2LEFT, appStateOld = RIGHT2MIDDLE;
		float angle = 0, angleOld = 0;
		float minAngle = 0, maxAngle = 0;
		int count = 0;
		int afsInit = 0;
		boolean timeout = false;
		int lang = System.getProperty("user.language").equalsIgnoreCase("de") ? 1 : 0;
		String[][] msgs = {
				/*0*/{"Please turn full left and right\n until AFS initialization\n is complete", "Bitte wiederholt links und rechts\n Anschlaglenken bis AFS initialisiert\n ist"},
				/*1*/{"AFS initialization succeeded!\nPlease align the steering wheel", "AFS-Initialisierung erfolgt!\nBitte lenkrad Gerade"},
				/*2*/{"AFS initialization time\n expired!", "AFS-Initialisierungszeit ist\n abgelaufen!"},
				/*3*/{"Back to the middle and\n leave the wheel", "Zur�ck zur Mitte\n und Lenkrad loslassen"}
		};
		
		try {
			// --------------------------------------------------------------------------------------------------
			// check/extract args
			// --------------------------------------------------------------------------------------------------
			log(debug, "Ok", "-> "+ getClassName() +" (test_v4_06ago07_14h40)");
			if (!checkArgs()) {
            	// NIO
				status = STATUS_EXECUTION_ERROR;
	        	resList.add( new Ergebnis("StatusAfsInit", "SDiagAFSinit", "", "", "", "AfsInitNOk", "NOk", "Ok", "Ok", "0", "", "", "", PB.getString("parameterexistenz"), "", Ergebnis.FT_NIO) );
				log(debug, "Error", "check args failed.");
				throw new PPExecutionException(PB.getString("parameterexistenz"));
			}
			log(debug, "Ok", "check args ok.");
			
			// CASCADE parameters
			String sgbd = extractArg("SGBD");
			long timeoutTime = new Long(extractArg("TIMEOUT")).longValue(); //ms
			float borderMiddle = new Float(extractArg("BORDER_ANGLE_MIDDLE")).floatValue(); //�
			float borderSide = new Float(extractArg("BORDER_ANGLE_SIDE")).floatValue(); //�
			float angleRange = (extractArg("ANGLE_RANGE") != null) ? new Float(extractArg("ANGLE_RANGE")).floatValue() : 2*(borderSide + 50); //�
			String sgbdJob1 = (extractArg("SGBD_JOB1") != null) ? extractArg("SGBD_JOB1") : "STATUS_FAHRERLENKWINKEL";
			String sgbdArgs1 = (extractArg("SGBD_ARGUMENTS1") != null) ? extractArg("SGBD_ARGUMENTS1") : "";
			String sgbdRes1 = (extractArg("SGBD_RESULT1") != null) ? extractArg("SGBD_RESULT1") : "STAT_FAHRER_LENKWINKEL_WERT";
			String sgbdJob2 = (extractArg("SGBD_JOB2") != null) ? extractArg("SGBD_JOB2") : "STATUS_LWM_INIT";
			String sgbdArgs2 = (extractArg("SGBD_ARGUMENTS2") != null) ? extractArg("SGBD_ARGUMENTS2") : "";
			String sgbdRes2 = (extractArg("SGBD_RESULT2") != null) ? extractArg("SGBD_RESULT2") : "STAT_GUELTIGKEIT_LWM_WERT";
			long pollingTime = (extractArg("POLLING_TIME") != null) ? new Long(extractArg("POLLING_TIME")).longValue() : 100; //ms
			String textMessage = (extractArg("TEXT_MESSAGE") != null) ? extractArg("TEXT_MESSAGE") : msgs[0][lang];   
			int textSize = (extractArg("TEXT_SIZE") != null) ? new Integer(extractArg("TEXT_SIZE")).intValue() : 70;  
			long endTime = (extractArg("END_TIME") != null) ? new Long(extractArg("END_TIME")).longValue() : 3000; //ms
			
			// --------------------------------------------------------------------------------------------------
			// swings init
			// --------------------------------------------------------------------------------------------------
			Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
			stdFont = new Font(null, Font.BOLD, textSize);
			frame = new JFrame(lang == 1 ? "AFS-Initialisierung" : "AFS Initialization");
			frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
			frame.setSize(screenDim);
			frame.setLocation(screenDim.width/2 - frame.getWidth()/2, screenDim.height/2 - frame.getHeight()/2);
			frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
			frame.setResizable(false);
			// ----------------------------------------------------------- TOP panel
			msgArea = new JTextArea(2, 1);
			msgArea.setFont(stdFont);
			msgArea.setText(textMessage/*centerText(msgArea, stdFont, textMessage)*/);
			msgArea.setEditable(false);
			msgArea.setAlignmentX(Container.CENTER_ALIGNMENT);
			msgArea.setAlignmentY(Container.CENTER_ALIGNMENT);
			msgArea.setLineWrap(true);
			msgArea.setOpaque(false);
			topPanel = new JPanel(new BorderLayout());
			topPanel.setPreferredSize(new Dimension(screenDim.width, screenDim.height/4));
			topPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
			topPanel.setBackground(Color.LIGHT_GRAY);
			topPanel.add(msgArea);
			frame.getContentPane().add(topPanel, BorderLayout.PAGE_START);
			// ----------------------------------------------------------- MIDDLE panel
			img = new BufferedImage(screenDim.width, screenDim.height/2, BufferedImage.TYPE_INT_RGB);
			img = initImage(img, convDeg2Pix(img, angleRange, borderMiddle), convDeg2Pix(img, angleRange, borderSide));
			imgLabel = new JLabel(new ImageIcon(img), SwingConstants.CENTER);
		    imgLabel.setBorder(BorderFactory.createLineBorder(COLOR_BORDER));
			middlePanel = new JPanel(new BorderLayout());
			middlePanel.setPreferredSize(new Dimension(screenDim.width, screenDim.height/2));
			middlePanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
		    middlePanel.add(imgLabel);
			frame.getContentPane().add(middlePanel, BorderLayout.CENTER);
			// ----------------------------------------------------------- BOTTOM panel			
			angleLabel = new JLabel("0�", SwingConstants.CENTER);
			angleLabel.setFont(stdFont);
			angleLabel.setOpaque(false);
			anglePanel = new JPanel(new BorderLayout());
			anglePanel.setPreferredSize(new Dimension(screenDim.width/3, screenDim.height/4));
			anglePanel.setBorder(BorderFactory.createEtchedBorder());
			anglePanel.setOpaque(false);
		    anglePanel.add(angleLabel);
			// 
			codeLabel = new JLabel(getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7(), SwingConstants.CENTER);
			codeLabel.setFont(stdFont);
		    codeLabel.setPreferredSize(new Dimension(screenDim.width/3, screenDim.height/8));
			codeLabel.setOpaque(false);		    
			testSlider = new JSlider(SwingConstants.HORIZONTAL, -1*Math.round(angleRange/2), Math.round(angleRange/2), 0);
			testSlider.setPreferredSize(new Dimension(screenDim.width/6, screenDim.height/32));
			testSlider.setPaintTicks(true);
			testSlider.setMajorTickSpacing(100);
			testSlider.setVisible(simulation);
			testSlider.setEnabled(simulation);
			testSlider.setOpaque(false);
			testCheck = new JCheckBox("Valid", false);
			testCheck.setPreferredSize(new Dimension(screenDim.width/6, screenDim.height/32));
			testCheck.setVisible(simulation);
			testCheck.setEnabled(simulation);
			testCheck.setOpaque(false);
	    	testPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
			testPanel.setPreferredSize(new Dimension(screenDim.width/3, screenDim.height/4));
			testPanel.setBorder(BorderFactory.createEtchedBorder());
			testPanel.setOpaque(false);
			testPanel.add(codeLabel);
			testPanel.add(testSlider);
			testPanel.add(testCheck);
			// 
			countLabel = new JLabel("0x", SwingConstants.CENTER);
			countLabel.setFont(stdFont);
			countLabel.setOpaque(false);
			countPanel = new JPanel(new BorderLayout());
			countPanel.setPreferredSize(new Dimension(screenDim.width/3, screenDim.height/4));
			countPanel.setBorder(BorderFactory.createEtchedBorder());
			countPanel.setOpaque(false);
		    countPanel.add(countLabel);
		    //
			bottomPanel = new JPanel();
			bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));
			bottomPanel.setPreferredSize(new Dimension(screenDim.width, screenDim.height/4));
			bottomPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
			bottomPanel.setOpaque(true);
			bottomPanel.setBackground(Color.LIGHT_GRAY);			
			bottomPanel.add(anglePanel);
			bottomPanel.add(testPanel);
			bottomPanel.add(countPanel);
			frame.getContentPane().add(bottomPanel, BorderLayout.PAGE_END);
			// -----------------------------------------------------------
		 	frame.show();		
			log(debug, "Ok", "GUI created and shown.");

			// --------------------------------------------------------------------------------------------------
			// AFS initialization process cycle begin
			// --------------------------------------------------------------------------------------------------
			while (true) {
	
				// ---------------------
				// read angle and validity
				// ---------------------
				angleOld = angle;
				if (simulation) {
					// read from simulation components
					angle = (float)testSlider.getValue();
					afsInit = testCheck.isSelected() ? 1 : 0;					
				} else {
					// execute diagnose jobs
					try {
						// get angle (inverted)
						// --------------------
	                    job = sgbdJob1;
	                    jobpar = sgbdArgs1;
	                    jobres = sgbdRes1;
						diagRes = Ediabas.executeDiagJob(sgbd, job, jobpar, "");
	                    if (diagRes.equals("OKAY") == false) {
	                    	// NIO
							status = STATUS_EXECUTION_ERROR;
	                    	resList.add( new Ergebnis("DiagError", "EDIABAS", sgbd, job, jobpar, "EdiabasJobNOk", diagRes, "OKAY", "OKAY", "0", "", "", "", PB.getString("diagnosefehler"), "", Ergebnis.FT_NIO) );
							log(debug, "Error", "executing EDIABAS, sgbd: "+ sgbd +", sgbdJob: "+ job +", sgbdArgs: "+ jobpar +", status: "+ diagRes);
	                        throw new PPExecutionException(PB.getString("diagnosefehler"));
	                    }
						angle = Math.round(-1*Float.parseFloat(Ediabas.getDiagResultValue(jobres)));

						// get validity
						// --------------------
	                    job = sgbdJob2;
	                    jobpar = sgbdArgs2;
	                    jobres = sgbdRes2;
	                    diagRes = Ediabas.executeDiagJob(sgbd, job, jobpar, "");
	                    if (diagRes.equals("OKAY") == false) {
	                    	// NIO
							status = STATUS_EXECUTION_ERROR;
	                    	resList.add( new Ergebnis("DiagError", "EDIABAS", sgbd, job, jobpar, "EdiabasJobNOk", diagRes, "OKAY", "OKAY", "0", "", "", "", PB.getString("diagnosefehler"), "", Ergebnis.FT_NIO) );
							log(debug, "Error", "executing EDIABAS, sgbd: "+ sgbd +", sgbdJob: "+ job +", sgbdArgs: "+ jobpar +", status: "+ diagRes);
	                        throw new PPExecutionException(PB.getString("diagnosefehler"));
	                    }
						afsInit = Integer.parseInt(Ediabas.getDiagResultValue(jobres));
						
			        } catch (ApiCallFailedException e) {
                    	// NIO
						status = STATUS_EXECUTION_ERROR;
			        	resList.add( new Ergebnis("DiagError", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString("diagnosefehler"), Ediabas.apiErrorCode()+": " + Ediabas.apiErrorText(), Ergebnis.FT_NIO) ); 
						log(debug, "Error", "executing EDIABAS, sgbd: "+ sgbd +", sgbdJob: "+ job +", sgbdArgs: "+ jobpar +", sgbdRes: "+ jobres +", ApiCallFailedException: "+ e.getMessage());
			            throw new PPExecutionException(PB.getString("diagnosefehler"));
			        } catch (EdiabasResultNotFoundException e) {
                    	// NIO
						status = STATUS_EXECUTION_ERROR;
			        	resList.add( new Ergebnis("DiagError", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString("diagnosefehler"), (e.getMessage() != null ? e.getMessage() : ""), Ergebnis.FT_NIO_SYS) );
						log(debug, "Error", "executing EDIABAS, sgbd: "+ sgbd +", sgbdJob: "+ job +", sgbdArgs: "+ jobpar +", sgbdRes: "+ jobres +", EdiabasResultNotFoundException: "+ e.getMessage());
			            throw new PPExecutionException(PB.getString("diagnosefehler"));
			        }
				}
				// update min/max angles
				if (angle < minAngle)
					minAngle = angle;
				if (angle > maxAngle)
					maxAngle = angle;

				
				// ---------------------
				// state related tasks 
				// ---------------------
				actualTime = System.currentTimeMillis();
				
				// END
				// ---------
				if (appState == END) {
					if ((actualTime - startTime) > endTime) {
						break;	
					}
					
				} else {
					// Timeout check
					if (actualTime - startTime > timeoutTime) {
						// NIO
						status = STATUS_EXECUTION_ERROR;
			        	resList.add( new Ergebnis("StatusAfsInit", "SDiagAFSinit", sgbd, "", "", "AfsInitNOk", String.valueOf(count)+"x -> "+ String.valueOf(actualTime - startTime) +" ms", "0 ms", String.valueOf(timeoutTime) +" ms", "0", "", "", "", "AfsInitTimeout", "", Ergebnis.FT_NIO) );
						log(debug, "Error", "AFS initialization invalid. -> timeout ("+ String.valueOf(actualTime - startTime) +")");
						// update state
						appStateOld = appState;
						appState = END;
						// -> exit NIO...
						msgArea.setText(msgs[2][lang]);
						topPanel.setBackground(Color.RED);
						bottomPanel.setBackground(Color.RED);
						startTime = actualTime;

					// MIDDLE2LEFT
					// ---------
					} else if (appState == MIDDLE2LEFT) {
						// check reach to LEFT...
						if (angle < -1*borderSide) {
							// update state
							appStateOld = appState;
							appState = LEFT2RIGHT;
						}
						
					// LEFT2RIGHT	
					// ---------
					} else if (appState == LEFT2RIGHT) {
						// check reach to RIGHT...
						if (angle > borderSide) {
							// update state
							appStateOld = appState;
							appState = RIGHT2MIDDLE;
						}

					// RIGHT2MIDDLE	
					// ---------
					} else if (appState == RIGHT2MIDDLE) {
						// check reach to the MIDDLE...
						if (angle <= borderMiddle) {
							// update state
							appStateOld = appState;
							appState = MIDDLE2LEFT;
							//
							count++;								
							countLabel.setText(String.valueOf(count) +"x");
							// evaluate only from the 2nd time on...
							if (count >= 2) {
							    // evaluate sim/diagnose result
								if (afsInit == 1) {
									// IO
									status = STATUS_EXECUTION_OK;
						        	resList.add( new Ergebnis("StatusAfsInit", "SDiagAFSinit", sgbd, "", "", "AfsInitOk", count+"x -> L:"+ String.valueOf(maxAngle) +" R:"+ String.valueOf(minAngle), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO) );
									log(debug, "Ok", "AFS initialization valid. angle(L: "+ maxAngle +"�, R: "+ minAngle +"�) -> exit");
									// update state
									appStateOld = appState;
									appState = END;
									// -> exit IO...
									msgArea.setText(msgs[1][lang]);
									topPanel.setBackground(Color.GREEN);
									bottomPanel.setBackground(Color.GREEN);
									startTime = actualTime;
								} else {
									// RETRY
									status = STATUS_EXECUTION_ERROR;
						        	resList.add( new Ergebnis("StatusAfsInit", "SDiagAFSinit", sgbd, "", "", "AfsInitInc", count+"x -> L:"+ String.valueOf(maxAngle) +" R:"+ String.valueOf(minAngle), "", "", "0", "", "", "", "AfsInitIncomplete", "", Ergebnis.FT_RETRY) );
									log(debug, "Ok", "AFS initialization incomplete. angle(L: "+ maxAngle +"�, R: "+ minAngle +"�) -> repeat");
								}
							}	
						}
					}
				}
				
				// ---------------------
				// update GUI
				// ---------------------
				angleLabel.setText(String.valueOf(rnd(-1*angle, 2)) +"�");
				img = drawImage(img, convDeg2Pix(img, angleRange, borderMiddle), convDeg2Pix(img, angleRange, borderSide), convDeg2Pix(img, angleRange, angleOld), convDeg2Pix(img, angleRange, angle));
				imgLabel.setIcon(new ImageIcon(img));

				appSleep(pollingTime);					
			}
			// --------------------------------------------------------------------------------------------------
			log(debug, "Ok", "AFS initialization cycle ended ("+ String.valueOf(count) +"x)");
			
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
			log(debug, "Error", "executing procedure, PPExecutionException: "+ e.getMessage());
            // show error...
			try {
				msgArea.setText(e.getMessage());
				topPanel.setBackground(Color.RED);
				bottomPanel.setBackground(Color.RED);
			} catch (Throwable t) {
				log(debug, "Error", "showing error msg, Throwable: "+ t.getMessage());
			}
			appSleep(3000); //3s
        } catch (Exception e) {
            status = STATUS_EXECUTION_ERROR;
            resList.add( new Ergebnis("ExecError", "SDiagAFSinit", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS) );
			log(true, "Error", "executing procedure, Exception: "+ e.getMessage());
        } catch (Throwable e) {
            status = STATUS_EXECUTION_ERROR;
            resList.add( new Ergebnis("ExecError", "SDiagAFSinit", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString("unerwarteterLaufzeitfehler"), Ergebnis.FT_NIO_SYS) );
			log(true, "Error", "executing procedure, Throwable: "+ e.getMessage());
        }

		// --------------------------------------------------------------------------------------------------
		// close GUI
		// --------------------------------------------------------------------------------------------------
		if (frame != null) {
			frame.dispose();
			frame = null;
			log(debug, "Ok", "GUI disposed.");
		}

		// -------------------------------------------------------------------------------------------
    	// set and log results
		// -------------------------------------------------------------------------------------------
		setPPStatus(info, status, resList);
		log(debug, "Var", "Execution status: "+ (status == STATUS_EXECUTION_OK ? "Ok" : "Error"));
		for (int i=0; i<resList.size(); i++) {
			log(debug, "Var", "APDM result "+ i +":\n"+ ((Ergebnis)resList.get(i)).toString());
		}
    }
    
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//											UTILS
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * <b>centerText </b>
	 * <br>
	 * @param textArea
	 * @param textFont
	 * @param text
	 * @return 
	 */	    
    private String centerText(JTextArea textArea, Font textFont, String text) 
	{
		FontMetrics fontMetrics = textArea.getFontMetrics(textFont);
		
		// ------------
		// build lines
		// ------------
		String resText = "";
		String testLine = "";
		String[] words = text.split(" ");
		for (int i = 0; i < words.length; i++) {
			testLine = resText.substring(resText.indexOf("\n") != -1 ? resText.indexOf("\n")+1 : 0) +" "+ words[i];
			log(true, "TESTS", "testLine: "+ testLine +", size: "+ testLine.length() +", metricsSize: "+ fontMetrics.stringWidth(testLine));
			if (fontMetrics.stringWidth(testLine) > 1200) {
				resText += "\n"+ words[i];
			} else {
				resText += (i == 0 ? "" : " ")+ words[i];
			}
//			log(true, "TESTS", "resText: "+ resText);
		}
		log(true, "TESTS", "lines buit. resText: "+ resText);
		
		// ------------
		// center lines
		// ------------
		String[] lines = resText.split("\n");
		int halfDif = 0;
		resText = "";
		String spaces = "";
		for (int i = 0; i < lines.length; i++) {
			halfDif = new Double(Math.floor(1200 - fontMetrics.stringWidth(lines[i]))).intValue();
			log(true, "TESTS", lines[i] +", halfDif = "+ halfDif +" ("+ 1200 +" - "+ fontMetrics.stringWidth(lines[i]) +")");
			// insert blankspaces prefix
			for (int s = 0; s < halfDif; s++) {
				spaces += " ";
			}
			lines[i] = spaces.concat(lines[i]);
			resText += lines[i];
		}
		log(true, "TESTS", "center lines. resText: "+ resText);
		return resText;
	}
    
	/**
	 * <b>fillImage </b>
	 * <br>
	 * @param image - the BufferedImage object
	 * @param color - the Color object
	 */	    
	private static BufferedImage initImage(BufferedImage img, int pxAbsBorder1, int pxAbsBorder2) 
	{
		Color color = COLOR_BACKGROUND;
		int pxOffset = Math.round(img.getWidth()/2);
		
		for (int y = 0; y < img.getHeight(); y ++) { 
			// draw the background
			for (int x = 0; x < img.getWidth(); x ++) {
				if ( getArea(pxAbsBorder1, pxAbsBorder2, x - pxOffset) == LEFT ||
					 getArea(pxAbsBorder1, pxAbsBorder2, x - pxOffset) == RIGHT ||
					 getArea(pxAbsBorder1, pxAbsBorder2, x - pxOffset) == MIDDLE ) {
					// special areas
					color = COLOR_AREA;
				} else if (isBorder(pxAbsBorder1, pxAbsBorder2, x - pxOffset)) {	
					// border
					color = COLOR_BORDER;
				} else {
					// background 
					color = COLOR_BACKGROUND;
				}
				img.setRGB(x, y, color.getRGB());
			}
    	}
		return img;
	}

	/**
	 * <b>drawImage </b>
	 * <br>
	 * @param img
	 * @param pxAbsBorder1 - within [0, img.getWidth()/2] px
	 * @param pxAbsBorder2 - within [0, img.getWidth()/2] px
	 * @param pxAngleOld - within [-img.getWidth()/2, img.getWidth()/2] px
	 * @param pxAngle - within [-img.getWidth()/2, img.getWidth()/2] px
	 * @return img
	 */
	private BufferedImage drawImage(BufferedImage img, int pxAbsBorder1, int pxAbsBorder2, int pxAngleOld, int pxAngle) 
	{
		Color color = COLOR_BACKGROUND;
		int pxOffset = Math.round(img.getWidth()/2);
		int pxBarHalfWidth = Math.round(BAR_WIDTH/2); 
		int pxAxisHalfWidth = Math.round(AXIS_WIDTH); 
		int currArea = 0;
		
		for (int y = 0; y < img.getHeight(); y++) {			
			//
			// Re-draw the background behind the bar
			//
			for (int x = pxAngleOld + pxOffset - pxBarHalfWidth; x < pxAngleOld + pxOffset + pxBarHalfWidth; x++) {
				// only within GUI x range
				if (x >= 0 && x < img.getWidth()) { 
					currArea = getArea(pxAbsBorder1, pxAbsBorder2, x - pxOffset);
					
					// border
					if (isBorder(pxAbsBorder1, pxAbsBorder2, x - pxOffset)) {	
						color = COLOR_BORDER;
					
					// special areas
					} else if (currArea == LEFT || currArea == MIDDLE || currArea == RIGHT) {
						color = COLOR_AREA;
					
					// background
					} else {
						color = COLOR_BACKGROUND;
					}
					img.setRGB(x, y, color.getRGB());
				}
			}
			//
			// draw the angle bar
			//
			currArea = getArea(pxAbsBorder1, pxAbsBorder2, pxAngle);
			for (int x = pxAngle + pxOffset - pxBarHalfWidth; x < pxAngle + pxOffset + pxBarHalfWidth; x++) {
				// only within GUI x range
				if (x > 0 && x < img.getWidth()) { 
					
					// angle bar axis
					if (x >= pxAngle + pxOffset - pxAxisHalfWidth && x < pxAngle + pxOffset + pxAxisHalfWidth) {
						color = COLOR_BACKGROUND;
					
					// angle bar inside area
					} else if (currArea == LEFT || currArea == MIDDLE || currArea == RIGHT) {
						color = COLOR_BAR_ACTIVE;
					
					// angle bar
					} else {
						color = COLOR_BAR_DEFAULT;
					}
					img.setRGB(x, y, color.getRGB());
				}	
			}
		}
		return img;
	}
	
	/**
	 * <b>drawImage </b>
	 * <br>
	 * @param img
	 * @param pxAbsBorder1 - within [0, img.getWidth()/2] px
	 * @param pxAbsBorder2 - within [0, img.getWidth()/2] px
	 * @param pxAngleOld - within [-img.getWidth()/2, img.getWidth()/2] px
	 * @param pxAngle - within [-img.getWidth()/2, img.getWidth()/2] px
	 * @return img
	 *
	private BufferedImage drawImage(BufferedImage img, int pxAbsBorder1, int pxAbsBorder2, int pxAngleOld, int pxAngle) 
	{
		Color color = COLOR_BACKGROUND;
		int pxOffset = Math.round(img.getWidth()/2);
		
		for (int y = 0; y < img.getHeight(); y++) {			
			// re-draw the background behind the bar
			for (int x = pxAngleOld + pxOffset - BAR_WIDTH; x < pxAngleOld + pxOffset + BAR_WIDTH; x++) {
				if (x >= 0 && x < img.getWidth()) { // only within GUI x range
					if ( getArea(pxAbsBorder1, pxAbsBorder2, x - pxOffset) == LEFT ||
						 getArea(pxAbsBorder1, pxAbsBorder2, x - pxOffset) == RIGHT ||
						 getArea(pxAbsBorder1, pxAbsBorder2, x - pxOffset) == MIDDLE ) {
						// special areas
						color = COLOR_AREA;
					} else if (isBorder(pxAbsBorder1, pxAbsBorder2, x - pxOffset)) {	
						// border
						color = COLOR_BORDER;
					} else {
						// background
						color = COLOR_BACKGROUND;
					}
					img.setRGB(x, y, color.getRGB());
				}
			}
			// draw the angle bar
			for (int x = pxAngle + pxOffset - BAR_WIDTH; x < pxAngle + pxOffset + BAR_WIDTH; x ++) {
				if (x > 0 && x < img.getWidth()) { // only within GUI x range
					if (x >= pxAngle + pxOffset - BAR_AXIS_WIDTH && x < pxAngle + pxOffset + BAR_AXIS_WIDTH ) {
						// angle bar axis
						color = COLOR_BACKGROUND;
					} else if ( getArea(pxAbsBorder1, pxAbsBorder2, x - pxOffset) == LEFT ||
								getArea(pxAbsBorder1, pxAbsBorder2, x - pxOffset) == RIGHT ||
								getArea(pxAbsBorder1, pxAbsBorder2, x - pxOffset) == MIDDLE ) {
						// angle bar inside area
						color = COLOR_BAR_ACTIVE;
					} else {
						// angle bar
						color = COLOR_BAR_DEFAULT;
					}
					img.setRGB(x, y, color.getRGB());
				}	
			}
		}
		return img;
	}
*/
	/**
	 * <b>convDeg2Pix</b>
	 * <br>
	 * Convert degrees [-ANGLE_RANGE/2, ANGLE_RANGE/2] to pixels [-img.getWidth()/2, img.getWidth()/2]<br>
	 * <br>
	 * @param img
	 * @param angleRange
	 * @param angleValue
	 * @return
	 */
	private static int convDeg2Pix(BufferedImage img, float angleRange, float angleValue) 
	{
		return Math.round(img.getWidth()*angleValue/angleRange);
	}

	
	/**
	 * <b>getArea </b>
	 * <br>
	 * Gets the area for the given angle.
	 * <br>
	 * @param absBorderMiddle - middle area limit (abs value) [0, ANGLE_RANGE/2] or [0, img.getWidth()/2]
	 * @param absBorderSide - side areas limits (abs value) [0, ANGLE_RANGE/2] or [0, img.getWidth()/2]
	 * @param value - value within [-ANGLE_RANGE/2, ANGLE_RANGE/2] or [-img.getWidth()/2, img.getWidth()/2] 
	 * @return
	 * . -1 (OTHER area)
	 * .  0 (MIDDLE area)
	 * .  1 (LEFT area)
	 * .  2 (RIGHT area)
	 */
	private static int getArea(int absBorderMiddle, int absBorderSide, int value) 
	{
		// MIDDLE area
		if (value > - absBorderMiddle && value < absBorderMiddle)
			return MIDDLE;
		// LEFT SIDE area
		else if (value < - absBorderSide)
			return LEFT;
		// RIGHT SIDE area
		else if (value > absBorderSide)
			return RIGHT;
		// OTHER area
		else
			return OTHER;	
	}
	
	/**
	 * <b>isBorder </b>
	 * <br>
	 * Checks if the given x position is a border line.
	 * <br>
	 * @param absBorderMiddle - middle area limit (abs value) [0, ANGLE_RANGE/2] or [0, img.getWidth()/2]
	 * @param absBorderSide - side areas limits (abs value) [0, ANGLE_RANGE/2] or [0, img.getWidth()/2]
	 * @param value - value within [-ANGLE_RANGE/2, ANGLE_RANGE/2] or [-img.getWidth()/2, img.getWidth()/2] 
	 * @return
	 */
	private static boolean isBorder(int absBorderMiddle, int absBorderSide, int value) 
	{
		return (value == 0 || 
				value == - absBorderMiddle || value == absBorderMiddle ||
				value == - absBorderSide || value == absBorderSide);
	}
	
	/**
	 * <b>rnd </b>
	 * <br>
	 * Rounds a float value to n decimal places.
	 * <br>
	 * @param value - the float value to round
	 * @param decPlaces - the number of decimal places to use while rounding
	 * @return the rounded float value
	 */
	private static double rnd(double value, int decPlaces) 
	{
		float tmp = 1;
		for (int i = 1; i <= decPlaces; i++)
			tmp *= 10;
		return (Math.rint(value * tmp)) / tmp;
	}

	/**
	 * <b>extractArg</b>
	 * <br>
	 * Extract one or several(split with ;) 'CASCADEconstruct' values if present in the given arguments.
	 * <br>
	 * @param debug is the debug flag
	 * @return the arg extracted value string 
	 * @throws InformationNotAvailableException if a '@contruct' value is no available
	 */
	private String extractArg(String argName) throws InformationNotAvailableException 
	{
		String argValue = getArg(argName);
		
		if (argValue == null) {
			// NULL value
			return null;
		}
		if (argValue.indexOf("@") != -1) {	
			// @construct value
			String[] argSubValues = argValue.split(";");
			argValue = "";
			for (int i=0; i<argSubValues.length; i++) {
				if (argSubValues[i].indexOf("@") != -1) {
					// extract @construct var value
					argSubValues[i] = getPPResult(argSubValues[i].toUpperCase().trim());
				}
				argValue += (i == 0 ? "" : " ") + argSubValues[i];
			}
		}
		return argValue;
	}
	
	/**
	 * <b>appSleep</b>
	 * <br>
	 * Sleeps for the specified time in milliseconds<br>
	 * <br>
	 * @param ms - sleeping time in milliseconds
	 */
	private void appSleep(long ms)
	{
		try {
			Thread.sleep(ms);
		} catch (Exception e) {
			//..
		}
	}

	/**
	 * <b>log </b> 
	 * <br>
	 * Logs a message if the given debug flag is active or if it is an 'Error'
	 * message. 
	 * <br>
	 * @param debug - the debug flag
	 * @param type - the type of message to display
	 * @param text - the message text
	 */
	private void log(boolean debug, String type, String text) 
	{
		if (type.equalsIgnoreCase("Error") || debug) {
			System.out.println("["+ getLocalName() +"] ["+ type +"] "+ text);
		}
	}
	
}

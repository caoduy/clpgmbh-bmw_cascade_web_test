package com.bmw.cascade.pruefprozeduren;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Vector;

import sun.misc.BASE64Encoder;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementation of a Testprocedure, which prompts the Operator to identify himself by scanning his
 * IPS-Q Card. The Operator authorizations are defined by a XML File. Only a registered Operator is
 * allowed to execute certain Pruefumfaenge.
 *
 * @author ZWE Z. Wen, ESG Elektroniksystem- und Logistik-GmbH
 * @author LST Ludwig Staudinger, ZA-T-425
 * @author PRE Peter Rettig, Gefasoft Engineering GmbH
 * @author RBE Ren� Bergmann, TI-547
 * @author FSC Fabian Sch�nert, TI-545
 * @version <br>
 *          V_1_0_F 08.10.2008 LST Ersterstellung mit Uebernahmen aus WerkerIdent PP - Produktivtest OK.<br>
 *          V_2_0_F 08.10.2008 LST Behandlung von Exclude PU's (!).<br>
 *          V_3_0_T 19.05.2014 PRE Bugfix wg. Problem mit Pr�fungsabbruch (TestScreen.stopConfirmed und TestScreen.stopConfirmed m�ssen auf true gesetzt werden, sonst steht der Pr�fstand).
 *          Als Notl�sung (da Bugfix ohne CASCADE-�nderung n�tig) erfolgt der Abbruch nun �ber einen HTTP-Request. Achtung: CASCADE-Version 6.1.0 ist erforderlich, da erst hier das HTTP getriggerte Cancel zur Verf�gung steht.<br>
 *          V_4_0_T 05.06.2014 PRE Bei einer Fehleingabe soll im Ergebnis unter Par1 nicht mehr der Pfad zur XML-Datei stehen, da sich diese Information dem Mitarbeiter bei aktiven ScreenPrinter nicht erschlie�en soll.<br>
 *          V_5_0_F 01.07.2014 PRE Produktive Aktivierung.<br>
 *          V_6_0_F 30.01.2015 UPL Ueber den opt. Parameter INPUT kann ein Ergebnis eines anderen Pruefschritts als Eingabe verwendet werden (z.B. WerkerEingabe)<br>
 *          V_7_0_T 25.02.2015 RBE Ueber den opt.Parameter ENCODE kann gesteuert werden, ob die Eingabe BASE64-verschl�sselt behandelt wird; Anforderung aus FT FuN: die Werker-IPSQ-Kennungen in der .xml-Datei sollen verschl�sselt gepflegt sein; Achtung: eingegeben wird weiterhin der unverschl�sselte Wert<br>
 *          V_8_0_F 25.02.2015 RBE Prod. Freigabe der 7_0_T<br>
 *          V_9_0_T 31.08.2015 FSC Optionaler Parameter CONTINUE_ON_NIO erm�glicht das Fortsetzen der Pr�fung auch bei NIO Ergebnis<br>
 *          V_10_0_F 02.09.2015 FSC Produktive Aktivierung. <br>
 *          V_11_0_T ??.??.2016 MK Die L�nge des eingegebenen Strings soll auf 12 bzw. 13 Stellen begrenzt werden<br>
 *          V_12_0_F ??.??.2016 MK Produktive Aktivierung. <br>
 *          V_13_0_T 02.09.2016 MK Bugfix, da der Check der Eingabestringl�nge bei einer Verschl�sselung Probleme macht<br>  
 *          V_14_0_F 05.09.2016 MK Produktive Aktivierung. <br>  
 *   		V_15_0_T 05.09.2016 MKe Der Name des Werkers soll nicht mehr an APDM �bertragen werden. <br>  
 *       	V_16_0_F 20.12.2016 MKe F-Version. <br>  
 *          V_17_0_T 04.05.2017 MKe Parameter DOKU_ID und DOKU_NAME hinzugef�gt. <br>  
 *          V_18_0_T 08.05.2017 MKe Parameter ID_LENGHT_MIN und ID_LENGHT_MAX Parameter hinzugef�gt. <br>  
 *          V_19_0_T 09.05.2017 MKe Bugfix beim Auslesen des ID_LENGHT_MAX Parameters. <br>  
 *          V_20_0_T 09.05.2017 MKe Ergebnisgenerierung angepasst. <br>  
 *          V_21_0_F 10.05.2017 MKe F-Version <br>  

 */
public class WerkerAccess_21_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	boolean bDEBUG = false;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public WerkerAccess_21_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 *
	 * @param pruefling
	 *            Klasse des zugeh. Prueflings
	 * @param pruefprozName
	 *            Name der Pruefprozedur
	 * @param hasToBeExecuted
	 *            Ausfuehrbedingung f�r Fehlerfreiheit
	 */
	public WerkerAccess_21_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG", "INPUT", "ENCODE", "CONTINUE_ON_NIO", "DOKU_ID", "DOKU_NAME", "ID_LENGHT_MIN", "ID_LENGHT_MAX" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "DIR", "FILENAME", "EXTENSION" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die
	 * parent-Methode aufgrund der offenen Anzahl an Results
	 */
	@Override
	public boolean checkArgs() {
		int i, j;
		boolean exist = true;

		try {
			// 1. Check: Sind alle requiredArgs gesetzt
			String[] requiredArgs = getRequiredArgs();
			for( i = 0; i < requiredArgs.length; i++ ) {
				if( getArg( requiredArgs[i] ) == null )
					return false;
				if( getArg( requiredArgs[i] ).equals( "" ) == true )
					return false;
			}
			// 2. Check: Sind �berfl�ssige Argumente gesetzt, d.h. Argumente, die weder required noch optional sind
			// Ein gesetztes Argument, das mit LINE wird nicht analysiert, es wird aber der maximale Index festgehalten.
			// Die detaillierte Analyse erfolgt im 3-ten Check.
			Enumeration enu = getArgs().keys();
			String[] optionalArgs = getOptionalArgs();
			String givenkey;
			while( enu.hasMoreElements() ) {
				givenkey = (String) enu.nextElement();
				if( (givenkey.startsWith( "DIR" ) == false) && (givenkey.startsWith( "FILENAME" ) == false) && (givenkey.startsWith( "EXTENSION" ) == false) ) {
					exist = false;
					j = 0;
					while( (exist == false) && (j < requiredArgs.length) ) {
						if( givenkey.equals( requiredArgs[j] ) == true )
							exist = true;
						j++;
					}
					j = 0;
					while( (exist == false) && (j < optionalArgs.length) ) {
						if( givenkey.equals( optionalArgs[j] ) == true )
							exist = true;
						j++;
					}
					if( exist == false )
						return false;
					if( getArg( givenkey ).equals( "" ) == true )
						return false;
				}
			}

			// Tests bestanden, somit ok
			return true;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * fuehrt die Pruefprozedur aus
	 *
	 * @param info
	 *            Information zur Ausfuehrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;
		String strErrorMessage = ""; // Fehlermeldung die bei n.i.O. ausgegeben werden soll

		// spezifische Variablen
		String filename, extension, dir, dir_temp;
		String strFilecontent = null;
		String strOperatorINPUT = "NoInput";
		String strPhysicalPuName = null;
		String strPhysicalPuNameWild = null;
		boolean bENCODE = false; // RBE ab v_7_0_T
		boolean continueNIO = false; // ab v_9_0_T
		boolean dokuName = false;
		boolean dokuID = true;
		int ID_LENGHT_MIN = 12;
		int ID_LENGHT_MAX = 13;

		int strOperatorINPUTLength = 0; // L�nge der Werkereingabe
		//int iAnswer; // Antwort von Dialogbox
		UserDialog myDialog = null; // Variable f�r Dialogbox

		// For PP Result
		String PPErgebnisFehlertyp = "";
		String PPErgebnisID = "";
		String PPErgebnisWerkzeug = "";
		String PPErgebnisMinimalwert = "";
		String PPErgebnisMaximalwert = "";
		String PPErgebniswert = "";
		String PPErgebnisFehlertext = "";
		String PPErgebnisHinweistext = "";
		String PPErgebnisParameter1 = "";
		String PPErgebnisErgebnisname = "";
		String PPErgebnisWiederholungen = "";
		String PPServerComPPGesamtErgebnis = Ergebnis.FT_IO;

		try {
			// Parameter holen------------------------------------------------------------------------------------------
			try {
				// Parameterpruefung
				if( !checkArgs() )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// Zwingenden singulaeren Parameter DIR holen
				if( getArg( getRequiredArgs()[0] ).indexOf( '@' ) != -1 )
					dir_temp = getPPResult( getArg( getRequiredArgs()[0] ) ).trim();
				else
					dir_temp = getArg( getRequiredArgs()[0] ).trim();

				// falls ";" im DateiPfad enthalten (z.B. Aus �bergabe aus Dim DirectoryReader f�r Delete File=false, z.B. c:/dim;fals
				StringTokenizer myTokenizer = new StringTokenizer( dir_temp, ";" );
				try {
					dir = myTokenizer.nextToken();
				} catch( NoSuchElementException e ) {
					dir = dir_temp;
				}

				// Zwingenden singulaeren Parameter NAME holen
				if( getArg( getRequiredArgs()[1] ).indexOf( '@' ) != -1 )
					filename = getPPResult( getArg( getRequiredArgs()[1] ) ).trim();
				else
					filename = getArg( getRequiredArgs()[1] ).trim();

				// Zwingenden singulaeren Parameter EXTENSION holen
				if( getArg( getRequiredArgs()[2] ).indexOf( '@' ) != -1 )
					extension = getPPResult( getArg( getRequiredArgs()[2] ) ).trim();
				else
					extension = getArg( getRequiredArgs()[2] ).trim();

				// optionale Parameter pr�fen
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDEBUG = false;
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDEBUG = true;
				}

				// Parameter INPUT darf nur einen Wert haben
				if( getArg( "INPUT" ) != null && extractValues( getArg( "INPUT" ) ).length > 1 )
					throw new PPExecutionException( "Parameter INPUT accepts only one value!" );

				// Parameter ENCODE pr�fen
				if( getArg( "ENCODE" ) != null ) {
					if( getArg( "ENCODE" ).equalsIgnoreCase( "FALSE" ) )
						bENCODE = false;
					if( getArg( "ENCODE" ).equalsIgnoreCase( "TRUE" ) )
						bENCODE = true;
				}

				// Parameter CONTINUE_ON_NIO pr�fen
				if( getArg( "CONTINUE_ON_NIO" ) != null ) {
					if( getArg( "CONTINUE_ON_NIO" ).equalsIgnoreCase( "FALSE" ) )
						continueNIO = false;
					if( getArg( "CONTINUE_ON_NIO" ).equalsIgnoreCase( "TRUE" ) )
						continueNIO = true;
				}

				// Parameter DOKU_ID pr�fen
				if( getArg( "DOKU_ID" ) != null ) {
					if( getArg( "DOKU_ID" ).equalsIgnoreCase( "FALSE" ) )
						dokuID = false;
					if( getArg( "DOKU_ID" ).equalsIgnoreCase( "TRUE" ) )
						dokuID = true;
				}
				// Parameter DOKU_NAME pr�fen
				if( getArg( "DOKU_NAME" ) != null ) {
					if( getArg( "DOKU_NAME" ).equalsIgnoreCase( "FALSE" ) )
						dokuName = false;
					if( getArg( "DOKU_NAME" ).equalsIgnoreCase( "TRUE" ) )
						dokuName = true;
				}
				// Parameter ID_LENGHT_MIN pr�fen
				if( getArg( "ID_LENGHT_MIN" ) != null ) {
					ID_LENGHT_MIN = Integer.parseInt( getArg( "ID_LENGHT_MIN" ) );
				} // Parameter ID_LENGHT_MAX pr�fen
				if( getArg( "ID_LENGHT_MAX" ) != null ) {
					ID_LENGHT_MAX = Integer.parseInt( getArg( "ID_LENGHT_MAX" ) );
				}
			} catch( NumberFormatException e ) {
				e.printStackTrace();
				result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( PPExecutionException e ) {
				e.printStackTrace();
				result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Holen des Devices Dialogbox---------------------------------------------------------------------
			try {
				myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
			} catch( Exception e ) {
				// Systemfehler, muss man genau analysieren
				e.printStackTrace();
				result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException( "Get UserDialog" );
			}

			try { // Main----------------------------------------------------------------------------------------
					// Get Name of Pruefumfang
				strPhysicalPuName = getPr�flingLaufzeitUmgebung().getPr�fumfangName();
				if( bDEBUG )
					System.out.println( "Main strPhysicalPuName= " + strPhysicalPuName );

				// Holen des Pr�fstandsnamens
				String pruefstandnr = getPr�flingLaufzeitUmgebung().getName();
				StringTokenizer toker = new StringTokenizer( pruefstandnr, "@" );
				while( toker.hasMoreTokens() ) {
					pruefstandnr = toker.nextToken();
				}

				// Holen der Werker-Nummer bzw. der Eingabe
				if( getArg( "INPUT" ) == null ) {
					if( bENCODE ) {
						// wenn ENCODE gesetzt ist, soll die Eingabe maskiert erfolgen..
						strOperatorINPUT = myDialog.requestUserInputPassword( PB.getString( "pruefernummer" ), PB.getString( "pruefernummerEingabe" ), 0 );
						strOperatorINPUTLength = strOperatorINPUT.length();
						if( bDEBUG )
							System.out.println( "Input = " + strOperatorINPUT );
						// ..und nachfolgend verschl�sselt bewertet werden
						BASE64Encoder b64Enc = new BASE64Encoder();
						strOperatorINPUT = (new String( b64Enc.encodeBuffer( strOperatorINPUT.getBytes() ) )).trim();
						if( bDEBUG )
							System.out.println( "Encoded Input = " + strOperatorINPUT );
					} else {
						// .. ansonsten nicht maskiert und nicht verschl�sselt
						strOperatorINPUT = myDialog.requestUserInput( PB.getString( "pruefernummer" ), PB.getString( "pruefernummerEingabe" ), 0 );
						strOperatorINPUTLength = strOperatorINPUT.length();
					}
					getPr�flingLaufzeitUmgebung().releaseUserDialog();
				} else {
					strOperatorINPUT = extractValues( getArg( "INPUT" ) )[0];
				}
				strOperatorINPUT = strOperatorINPUT.trim();
				if( strOperatorINPUT.equalsIgnoreCase( "" ) ) {
					strOperatorINPUT = "NoInput";
					//}else if( strOperatorINPUTLength != 12 && strOperatorINPUTLength != 13 ){
				} else if( strOperatorINPUTLength < ID_LENGHT_MIN || strOperatorINPUTLength > ID_LENGHT_MAX ) {
					strOperatorINPUT = "WrongInputLength";
				}
				// Separator am Ende kuerzen
				if( dir.endsWith( "\\" ) || (dir.endsWith( "/" )) )
					dir = dir.substring( 0, dir.length() - 1 );
				if( dir.endsWith( "\\\\" ) )
					dir = dir.substring( 0, dir.length() - 2 );

				// Fileobjekt generieren
				File f = new File( dir + System.getProperty( "file.separator" ) + filename + "." + extension );

				// Data read and compare
				// -----------------------------------------------------------------------------------
				if( (f.exists() && f.canRead()) ) {
					BufferedReader in = new BufferedReader( new FileReader( f ) );
					strFilecontent = "";
					try {
						do {
							strFilecontent = strFilecontent + in.readLine() + "\r\n";
						} while( in.ready() );

						if( getTagAmount( strFilecontent, "<testGroup Groupname=", "</testGroup>" ) != -1 ) {
							for( int a = 1; a <= getTagAmount( strFilecontent, "<testGroup Groupname=", "</testGroup>" ); a++ ) {
								if( bDEBUG )
									System.out.println( "Count a= " + a );
								String strGroupContent = getTagContent( strFilecontent, "<testGroup ", "</testGroup>", a ).trim();
								if( bDEBUG )
									System.out.println( "Main strGroupContent=\r\n" + strGroupContent );

								String strGroupname = getTagContent( strGroupContent, "Groupname=", ">", 1 ).trim();
								strGroupname = strGroupname.substring( 1, strGroupname.length() - 1 );
								if( bDEBUG )
									System.out.println( "Main strGroupname= " + strGroupname );

								if( getTagAmount( strGroupContent, "<operator IPS_QID=", "/>" ) != -1 ) {
									for( int b = 1; b <= getTagAmount( strGroupContent, "<operator IPS_QID=", " OperatorName=" ); b++ ) {
										if( bDEBUG )
											System.out.println( "Count b= " + b );
										String strIPS_QID = getTagContent( strGroupContent, "<operator IPS_QID=", " OperatorName=", b ).trim();
										if( strIPS_QID != "NotFound" )
											strIPS_QID = strIPS_QID.substring( 1, strIPS_QID.length() - 1 );
										if( bDEBUG )
											System.out.println( "Main strIPS_QID= " + strIPS_QID );
										if( bDEBUG )
											System.out.println( "Main strOperatorINPUT= " + strOperatorINPUT );

										if( strIPS_QID.equalsIgnoreCase( strOperatorINPUT ) ) {
											String strOperatorName = getTagContent( strGroupContent, "OperatorName=", "/>", b ).trim();
											strOperatorName = strOperatorName.substring( 1, strOperatorName.length() - 1 );
											if( bDEBUG )
												System.out.println( "Main strOperatorName= " + strOperatorName );

											a = getTagAmount( strFilecontent, "<testGroup Groupname=", "</testGroup>" );
											b = getTagAmount( strGroupContent, "<operator IPS_QID=", " OperatorName=" );

											if( getTagAmount( strGroupContent, "<puName>", "</puName>" ) != -1 ) {
												int ipuExclusion = 0;

												for( int c = 1; c <= getTagAmount( strGroupContent, "<puName>", "</puName>" ); c++ ) {
													if( bDEBUG )
														System.out.println( "Count c= " + c );
													String strpuName = getTagContent( strGroupContent, "<puName>", "</puName>", c ).trim();
													if( bDEBUG )
														System.out.println( "Main strpuName= " + strpuName );

													// Check for Exclusion !
													if( strpuName.indexOf( "!" ) != -1 ) {
														// Remove Exclusion
														// character ! from
														// StartOfString
														ipuExclusion = 1;
														strpuName = strpuName.substring( 1, strpuName.length() );
														if( bDEBUG )
															System.out.println( "Main Exclusion strpuName= " + strpuName );
													}

													// Check for Wildcard *
													if( strpuName.indexOf( "*" ) != -1 ) {
														// Remove Wildcard
														// character * from
														// EndofString
														strpuName = strpuName.substring( 0, strpuName.length() - 1 );
														if( bDEBUG )
															System.out.println( "Main Wildcard strpuName= " + strpuName );
														// Shorten
														// PhysicalPuName to
														// length of strpuName
														strPhysicalPuNameWild = strPhysicalPuName.substring( 0, strpuName.length() );
														if( bDEBUG )
															System.out.println( "Main Wildcard strPhysicalPuNameWild= " + strPhysicalPuNameWild );
													}

													if( (strpuName.equalsIgnoreCase( "ALL" ) || strpuName.equalsIgnoreCase( strPhysicalPuName ) || strpuName.equalsIgnoreCase( strPhysicalPuNameWild )) && !((ipuExclusion == 1) && (strpuName.equalsIgnoreCase( strPhysicalPuName ) || strpuName.equalsIgnoreCase( strPhysicalPuNameWild ))) ) {

														c = getTagAmount( strGroupContent, "<puName>", "</puName>" ); // Stop
														// Searching
														// for
														// PUs

														if( bDEBUG )
															System.out.println( "PU execution granted" );
														PPServerComPPGesamtErgebnis = Ergebnis.FT_IO;
														PPErgebnisParameter1 = strOperatorINPUT;
														PPErgebnisErgebnisname = strPhysicalPuName + " execution granted";
														PPErgebniswert = strOperatorName;
														PPErgebnisFehlertext = "";
														PPErgebnisHinweistext = "";

													} else {
														if( bDEBUG )
															System.out.println( "Operator not authorized for this PU" );
														PPServerComPPGesamtErgebnis = Ergebnis.FT_NIO;
														PPErgebnisParameter1 = strOperatorINPUT;
														PPErgebnisErgebnisname = "IPS_QID";
														PPErgebniswert = strOperatorName;
														PPErgebnisFehlertext = "PU (" + strPhysicalPuName + ") not listed in Group: " + strGroupname;
														PPErgebnisHinweistext = "Operator not authorized for this PU";

														if( ((ipuExclusion == 1) && (strpuName.equalsIgnoreCase( strPhysicalPuName ) || strpuName.equalsIgnoreCase( strPhysicalPuNameWild ))) ) {

															c = getTagAmount( strGroupContent, "<puName>", "</puName>" ); // Stop
															// Searching
															// for
															// other
															// PUs
															PPErgebnisFehlertext = "PU (" + strPhysicalPuName + ") excluded in Group: " + strGroupname;
															if( bDEBUG )
																System.out.println( "Operator excluded for this PU" );
														}
													}
												}
											} else {
												if( bDEBUG )
													System.out.println( "Invalid File Format - Expected Tag not found: <puName>" );
												PPServerComPPGesamtErgebnis = Ergebnis.FT_NIO;
												PPErgebnisParameter1 = strOperatorINPUT;
												PPErgebnisErgebnisname = "XML Tag";
												PPErgebniswert = "NotFound";
												PPErgebnisFehlertext = "Expected Tag not found: <puName>";
												PPErgebnisHinweistext = "Invalid File Format";

											}
										} else {
											if( bDEBUG )
												System.out.println( "Operator not listed" );
											PPServerComPPGesamtErgebnis = Ergebnis.FT_NIO;
											PPErgebnisParameter1 = strOperatorINPUT;
											PPErgebnisErgebnisname = "IPS_QID";
											PPErgebniswert = "NotFound";
											PPErgebnisFehlertext = "Operator (IPS_QID: " + strOperatorINPUT + ") not listed";
											PPErgebnisHinweistext = "";
										}
									}
								} else {
									if( bDEBUG )
										System.out.println( "Invalid File Format - Expected Tag not found: <operator IPS_QID=" );
									PPServerComPPGesamtErgebnis = Ergebnis.FT_NIO;
									PPErgebnisParameter1 = strOperatorINPUT;
									PPErgebnisErgebnisname = "XML Tag";
									PPErgebniswert = "NotFound";
									PPErgebnisFehlertext = "Expected Tag not found: <operator IPS_QID=";
									PPErgebnisHinweistext = "Invalid File Format";
								}
							}
						} else {
							if( bDEBUG )
								System.out.println( "Invalid File Format - Expected Tag not found: <testGroup Groupname=" );
							PPServerComPPGesamtErgebnis = Ergebnis.FT_NIO;
							PPErgebnisParameter1 = strOperatorINPUT;
							PPErgebnisErgebnisname = "XML Tag";
							PPErgebniswert = "NotFound";
							PPErgebnisFehlertext = "Expected Tag not found: <testGroup Groupname=";
							PPErgebnisHinweistext = "Invalid File Format";
						}

					} catch( NullPointerException e ) {
						if( bDEBUG )
							System.out.println( "Catched NullPointerExeption= " + strFilecontent );
					}
					in.close();
					result = new Ergebnis( "Status", "System", /*
																* dir + System.getProperty (
																* "file.separator" ) + filename + "."
																* + extension
																*/"", "", "", "FILE ReadZeile1", "TRUE", "TRUE", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				} else {
					// Ergebnis Doc
					result = new Ergebnis( "Status", "System", /*
																* dir + System.getProperty (
																* "file.separator" ) + filename + "."
																* + extension
																*/"", "", "", "FILE ReadZeile1 EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				}
				// Result Documentation
				result = new Ergebnis( "pu_name", PPErgebnisWerkzeug, "", "", "", "pu_name", PPErgebnisErgebnisname, PPErgebnisMinimalwert, PPErgebnisMaximalwert, "0", "", "", "", PPErgebnisFehlertext, PPErgebnisHinweistext, PPServerComPPGesamtErgebnis );
				ergListe.add( result );
				
				if( dokuID && dokuName ) {
					result = new Ergebnis( "operator_id", PPErgebnisWerkzeug, "", "", "", "operator_id", PPErgebnisParameter1, PPErgebnisMinimalwert, PPErgebnisMaximalwert, "0", "", "", "", PPErgebnisFehlertext, PPErgebnisHinweistext, PPServerComPPGesamtErgebnis );
					ergListe.add( result );
					result = new Ergebnis( "operator_name", PPErgebnisWerkzeug, "", "", "", "operator_name", PPErgebniswert, PPErgebnisMinimalwert, PPErgebnisMaximalwert, "0", "", "", "", PPErgebnisFehlertext, PPErgebnisHinweistext, PPServerComPPGesamtErgebnis );
					ergListe.add( result );
				} else if( !dokuID && dokuName ) {
					result = new Ergebnis( "operator_name", PPErgebnisWerkzeug, "", "", "", "operator_name", PPErgebniswert, PPErgebnisMinimalwert, PPErgebnisMaximalwert, "0", "", "", "", PPErgebnisFehlertext, PPErgebnisHinweistext, PPServerComPPGesamtErgebnis );
					ergListe.add( result );
				} else if( dokuID && !dokuName ) {
					result = new Ergebnis( "operator_id", PPErgebnisWerkzeug, "", "", "", "operator_id", PPErgebnisParameter1, PPErgebnisMinimalwert, PPErgebnisMaximalwert, "0", "", "", "", PPErgebnisFehlertext, PPErgebnisHinweistext, PPServerComPPGesamtErgebnis );
					ergListe.add( result );
				} 
				if( PPServerComPPGesamtErgebnis.equalsIgnoreCase( Ergebnis.FT_NIO ) ) {
					status = STATUS_EXECUTION_ERROR;
					if( !continueNIO )
						stopViaHttpRequest();
				}

			} catch( IOException e ) {
				e.printStackTrace();
				result = new Ergebnis( "Status", "System", /*
															* dir + System.getProperty(
															* "file.separator" ) + filename + "." +
															* extension
															*/"", "", "", "FILE IO EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( SecurityException e ) {
				e.printStackTrace();
				result = new Ergebnis( "Status", "System", /*
															* dir + System.getProperty(
															* "file.separator" ) + filename + "." +
															* extension
															*/"", "", "", "FILE ReadZeile1 SECURITY EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", strErrorMessage, PB.getString( "unerwarteterLaufzeitfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", strErrorMessage, PB.getString( "unerwarteterLaufzeitfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", strErrorMessage, PB.getString( "unerwarteterLaufzeitfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Device Dialgobox wird
		// freigegeben-------------------------------------------------------------------
		try {
			getPr�flingLaufzeitUmgebung().releaseUserDialog();
		} catch( Exception e ) {
			// Systemfehler, muss man genau analysieren
			e.printStackTrace();
			if( e instanceof DeviceLockedException )
				result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", e.getMessage(), "release", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		setPPStatus( info, status, ergListe );
	}

	// -------------------------Subfunctions
	// START-------------------------------------------------------------
	/**
	 * Returns Number of Groups defined by Start- and EndTag from strFilecontent.
	 */
	private int getTagAmount( String strFileContent, String strStartToken, String strEndToken ) {
		int iTestGroups = -1;
		int iTemp = 0;
		// Get amount of Tag Groups from filecontent
		if( strFileContent != null && strFileContent.indexOf( strStartToken ) != -1 ) {
			iTestGroups = 0;
			do {
				iTemp = strFileContent.indexOf( strEndToken, iTemp + strEndToken.length() );
				// System.out.println("getTagAmount iTemp= "+iTemp);
				iTestGroups++;
				// System.out.println("getTagAmount iTestGroups= "+iTestGroups);
			} while( strFileContent.indexOf( strEndToken, iTemp + strEndToken.length() ) != -1 );
		}
		return iTestGroups;
	}

	/**
	 * Returns the content of a Group defined by Start- and EndTag from strFilecontent.
	 */
	private String getTagContent( String strFileContent, String strStartToken, String strEndToken, int iGroup ) {
		String strTagContent = "NotFound";
		int iTestGroup = 0;
		int iOffset = 0;
		// Get content of Tag from filecontent
		if( strFileContent != null && strFileContent.indexOf( strStartToken ) != -1 ) {
			do {
				if( strFileContent.indexOf( strStartToken, iOffset ) != -1 ) {
					// System.out.println("getTagContent indexOfStartToken= "+strFileContent.indexOf(strStartToken,
					// iOffset));
					// System.out.println("getTagContent iOffset1= "+iOffset);
					strTagContent = strFileContent.substring( strFileContent.indexOf( strStartToken, iOffset ) + strStartToken.length(), strFileContent.indexOf( strEndToken, iOffset ) );
					// System.out.println("getTagContent strTagContent=\r\n"+strTagContent);
					iOffset = strFileContent.indexOf( strEndToken, iOffset ) + strEndToken.length();
					iTestGroup++;
				}
			} while( iTestGroup != iGroup );
		}
		return strTagContent;
	}

	private void stopViaHttpRequest() throws IOException {
		String url = "http://localhost:8086/testscreen-" + (getPr�flingLaufzeitUmgebung().getNumber() + 1) + "?canceltest=true"; // ist
		// Multi-Instanz-f�hig
		if( bDEBUG )
			System.out.println( "Trigger test stop via HTTP request (GET) to URL: " + url );

		URL obj = new URL( url );
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setConnectTimeout( 15000 ); // in ms
		con.setRequestMethod( "GET" ); // ist eigentlich Default

		if( bDEBUG )
			System.out.println( "Response code : " + con.getResponseCode() );

		BufferedReader in = new BufferedReader( new InputStreamReader( con.getInputStream() ) );
		String inputLine;
		StringBuffer response = new StringBuffer();
		while( (inputLine = in.readLine()) != null )
			response.append( inputLine );
		in.close();

		if( bDEBUG )
			System.out.println( "Response content: " + response.toString() );

		con.disconnect();
	}

	// -------------------------Subfunctions
	// END---------------------------------------------------------------
}

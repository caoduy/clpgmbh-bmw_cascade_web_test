package com.bmw.cascade.pruefprozeduren;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceIOException;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.DeviceParameterException;
import com.bmw.cascade.pruefstand.devices.audioanalyse.AudioAnalyser;
import com.bmw.cascade.pruefstand.devices.audioanalyse.AudioFileDescriptor;
import com.bmw.cascade.pruefstand.devices.audioanalyse.DN6000Constants;
import com.bmw.cascade.pruefstand.devices.audioanalyse.ReferenceDataDescriptor;
import com.bmw.cascade.pruefstand.devices.audioanalyse.ToleranzSpek;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.util.CascadeDateFormat;

/**
 * Does audio measurements by the use of an audio analyzer. It uses the analyzer for creating a audio spektrum and evaluates it.
 *
 * @author Michael Baum(mb), Rainer Merz (rm), Frank Weber (fw), Eleonor Fotsing Tchoutang (ef)
 * @version 72_0_F fw 04.03.2009 live version of 71_0_T
 * @version 71_0_T fw 04.03.2009 allowed max values till 63 instead of 60
 * @version 70_0_F fw 26.01.2009 live version of 69_0_T
 * @version 69_0_T fw 26.01.2009 bugfix sync noise control with profiles
 * @version 68_0_F fw 25.01.2009 live version of 67_0_T
 * @version 67_0_T fw 25.01.2009 bugfix name sync noise and bugfix recorduration conflict with profiles
 * @version 66_0_F fw 15.12.2008 live version of 65_0_T
 * @version 65_0_T fw 15.12.2008 rework to support profiles
 * @version 64_0_F fw 18.02.2008 added feature to deactivate zero line measurement and confirm via parameters
 * @version 63_0_F fw 26.11.2007 live version of 62_0_T
 * @version 62_0_T fw 26.11.2007 changed color of user dialogue, activated results for noise in case of noise, corrected bug when no init frequency is set
 * @version 61_0_F fw 26.11.2007 live version of 60_0_T
 * @version 60_0_T fw 26.11.2007 corrected results according to Cascade/APDM specification
 * @version 59_0_F fw 21.10.2007 live version of 58_0_T
 * @version 58_0_T fw 21.10.2007 added possibility to have separate volumes and several init-frequencies
 * @version 57_0_F fw 16.10.2007 reduced results for sync measurment to only zero line if zero lin eis detected
 * @version 56_0_F fw 23.09.2007 live version of 55_0_T
 * @version 55_0_T fw 23.09.2007 added feature to activate min max tracing tracing, removed unneceassry loop for noise spectrum
 * @version 54_0_F fw 21.08.2007 changed local tracing output for channel
 * @version 53_0_F fw 04.07.2007 changed check of analyser init
 * @version 52_0_F fw 01.07.2007 changed check for parameter record duration
 * @version 51_0_F fw 19.06.2007 corrected bugs for generic job parameters (L6) 
 * @version 50_0_F fw 25.05.2007 corrected bug for english sync measuremnt error texts (did also create german text)
 * @version 49_0_F fw 17.05.2007 live version of 48_0_T
 * @version 48_0_T fw 17.05.2007 added parameters to set recorduration individually for sync ranges, added parameter separators, needs Cascade 2.1.7
 * @version 47_0_F fw 26.04.2007 live version of 46_0_T
 * @version 46_0_T fw 26.04.2007 corrected Benglish error texts, needs Cascade 2.1.7
 * @version 45_0_F fw 23.04.2007 live version of 44_0_T
 * @version 44_0_T fw 23.04.2007 added parameter to set initial pause after each ferquency output of sync measurement, changed limit for init pause, needs Cascade 2.1.7
 * @version 43_0_F fw 20.04.2007 live version of 42_0_T
 * @version 42_0_T fw 20.04.2007 reworked implementation (APDM report) to use max sweep mesurement with limited frequencies, needs Cascade 2.1.7
 * @version 41_0_F fw 17.04.2007 live version of 40_0_T
 * @version 40_0_T fw 17.04.2007 reworked implementation to use max sweep mesurement with limited frequencies, needs Cascade 2.1.7
 * @version 39_0_F fw 09.04.2007 live version of 38_0_T
 * @version 38_0_T fw 09.04.2007 add parameters to also use max sweep mesurement with limited frequencies, needs Cascade 2.1.7
 * @version 37_0_F fw 31.03.2007 live version of 36_0_T
 * @version 36_0_T fw 18.03.2007 add parameters to also use sweep mesurement with limited frequencies, needs Cascade 2.1.7
 * @version 35_0_F ef/fw 02.12.2006 live version of 34_0_T, needs Cascade 2.1.7!
 * @version 34_0_T ef/fw 02.12.2006 added dropouts for sync measurement and noise detection, added repetition of single frequencies, needs Cascade 2.1.7
 * @version 33_0_F fw 02.12.2006 live version of 32_0_T, needs Cascade 2.1.7!
 * @version 32_0_T fw 02.12.2006 corrected APDM writing for synchro, added @ functionality for SNYC-SGBD,VOUME,JOB,JOBPAR,
 *  							 special solution AMP_60/TOPDSP2 added, changed some attributes to optional, changed synchro zero measurement
 *  							 added feature for local tracing, writing single tracing results, noise recognition features added
 *  							 needs Cascade 2.1.7!
 * @version 31_0_F fw 10.11.2006 live version of 30_0_T
 * @version 30_0_T fw 10.11.2006 added feature to measure synchronous without putting out frequencies
 * @version 29_0_F fw 04.11.2006 live version of 28_0_T
 * @version 28_0_T fw 04.11.2006 added feature to measure synchronous
 * @version 27_0_F fw 23.08.2006 live version of 26_0_T
 * @version 26_0_T fw 23.08.2006 parameter CONFIRM_ZERO_MEASUREMENT_USE_SECOND_MEASUREMENT added, english texts corrected
 * @version 25_0_F fw 07.08.2006 live version of 24_0_T
 * @version 24_0_T fw 07.08.2006 parameter CONFIRM_ZERO_MEASUREMENT_TIMEOUT and CONFIRM_ZERO_MEASUREMENT_TEXT added to use confirmation window
 * @version 23_0_F fw 07.05.2006 live version of 22_0_T
 * @version 22_0_T fw 07.05.2006 parameter ERRORTEXT_ZERO_MEASUREMENT added, measurement done even without tolerance
 * @version 21_0_F fw 16.03.2006 productive version of 20_0_T
 * @version 20_0_T fw 16.03.2006 bugfix for dropouts
 * @version 19_0_F fw 03.03.2006 productive version of 18_0_T
 * @version 18_0_T fw 03.03.2006 bugfix result columns, error count
 * @version 17_0_T fw 14.02.2006 removed error info, error count in results added, DO NOT USE BEFORE CASCADE 2.1.3
 * @version 16_0_F fw 01.11.2005 productive version of 15_0_T
 * @version 15_0_T fw 01.11.2005 added -1 as values to deactivate local tracing
 * @version 14_0_F fw 18.07.2005 productive version of 13_0_T
 * @version 13_0_T fw 18.07.2005 smaller variable fixes, parameters Toleranzindex and Ergebisindex added
 * @version 12 fw 17.06.2005 HWT parmeter check reworked
 * @version 11 fw 14.06.2005 HWT added
 * @version 10 fw 12.05.2005 setRecordDuration activated for Cascade 2.0
 * @version 0.0.9 fw 07.04.2005 bugfix
 * @version 0.0.8 fw 04.04.2005 bugfix
 * @version 0.0.7 fw 07.03.2005 Umarbeitung Aufruf Klirrspekrum, Umstellung auf eigenstaendige
 *          Pruefprozedur, setRecordDuration prepared
 * @version 0.0.6 fw 15.11.2004 version bugfix
 * @version 0.0.5 rm 22.09.2004 Das Toleranz und Ergebnisverzeichnis kann jetzt als optionaler
 *          Parameter gesetzt werden
 * @version 0.0.4 rm writeResultBuffer fuer die Uebergabe der Ergebnisse nach APDM ergaenzt
 * @version 0.0.3 fw 01.03.2004 optional parameter Baureihe added <br>
 *          Parameter: RUNMODE-Parameter: <br>
 *          <br>
 *          MESSTYP: legt die Art der �berpr�fungen fest <br>
 *          Audio_Analyse Maximum- und Klirrpr�fung <br>
 *          Micro_Exist Maximum-Pr�fung <br>
 *          Mute_Check Minimum-Pr�fung <br>
 *          Micro_Calibration Maximum-Minimum-Pr�fung <br>
 *          All Maximum-Minimum-Klirr-Pr�fung (zum Test) <br>
 * @version 73_0_T te/cw 10.8.2011 Block-Result ausgebaut
 * @version 74_0_F te/cw 29.8.2011 F-Version
 */
public class AudioBasic_74_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public AudioBasic_74_0_F_Pruefprozedur() {
	}

	//f�r Auswertung des Messung und �berpr�fung der Parameter notwendig
	private String sBaureihe = ""; // to contain optional Parameter Baureihe
	private String sKarosserie = ""; // to contain optional Parameter Karosserie

	private String myErrorTxt = null;


	//	language
	private final boolean isEnglish = System.getProperty( "user.language" ).equalsIgnoreCase( "EN" );

	private String[] ToleranceDirectories = { "c:\\audio\\toleranz", "c:\\audio\\toleranz_apdm", "c:\\audio\\toleranz_sk" };
	private String[] ResultDirectories = { "c:\\audio\\ergebnis", "c:\\audio\\ergebnis_apdm", "c:\\audio\\ergebnis_sk" };
	
	/**
	 * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	 *
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public AudioBasic_74_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName,
			Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = new String[] { 
			"--- Profiles to avoid many parameters ---",
			"SYNC_PROFILE",
			"--- PARAMETERS for APDM/local Traces ---",
			"KAROSSERIE", "LSTYP", "RADIOTYP","BAUREIHE",
			// general parameters for normal and sync measurment
			"--- PARAMETERS for all measurement types ---",
			"RECORDDURATION", "HWT",
			// zero measurement
			"--- PARAMETERS for detecting zero lines ---",
			"ERRORTEXT_ZERO_MEASUREMENT",
			"CONFIRM_ZERO_MEASUREMENT_TIMEOUT", "CONFIRM_ZERO_MEASUREMENT_TEXT",
			"CONFIRM_ZERO_MEASUREMENT_USE_SECOND_MEASUREMENT",
			// normal min/max/rattle measurement
			"--- PARAMETERS for measuring with local tolerances ---",
			"FBNAME[1..N]", "FSTART[1..N]", "FSTOP[1..N]", "FBIGNORE[1..N]",
			"MBNAME[1..N]", "MSTART[1..N]", "MSTOP[1..N]", "MBIGNORE[1..N]",
			"KBNAME[2..N]", "KSTART[1..N]", "KSTOP[1..N]", "KBIGNORE[1..N]",
			"TOLERANZINDEX","ERGEBNISINDEX",
			// for using sweep with limited frequencies
			"--- PARAMETERS for measuring with limited tolerances ---",
			"SWEEP_TOL_FREQUENCIES[1..N]","SWEEP_MIN_VALUES[1..N]","SWEEP_MAX_VALUES[1..N]",
			// sync measurement
			"--- PARAMETERS for measuring synchronized - general ---",
			"SYNCHRO_FREQUENCIES[1..N]","SYNCHRO_TOL_FREQUENCIES[1..N]",
			"SYNCHRO_NAME[1..N]","SYNCHRO_MIN_VALUES[1..N]","SYNCHRO_MAX_VALUES[1..N]",
			"SYNCHRO_DROPOUT[1..N]","SYNCHRO_RECORDDURATION[1..N]","SYNCHRO_VOLUME[1..N]",
			"REPEAT_SINGLE_FREQ_ONCE_ON_ERROR",
			//  sync ediabas
			"--- PARAMETERS for measuring synchronized - ediabas ---",
			"SYNCHRO_SGBD","SYNCHRO_JOBNAME","SYNCHRO_JOBPAR",
			"SYNCHRO_INITFREQUENCY","SYNCHRO_VOLUME","SYNCHRO_CHANNEL",
			"SYNCHRO_INITPAUSE","SYNCHRO_INIT_SINGLE_FREQUENCY_PAUSE",
			// sync tracing
			"--- PARAMETERS for measuring synchronized - tracing ---",
			"SYNCHRO_SINGLE_TRACE","SYNCHRO_LOCAL_TRACE",
			// sync noise
			"--- PARAMETERS for measuring synchronized - noise ---",
			"SYNCHRO_NOISE_NAME[1..N]_[1..N]","SYNCHRO_NOISE_FREQUENCIES[1..N]_[1..N]",
			"SYNCHRO_NOISE_MAX_VALUES[1..N]_[1..N]", //max_default = 63
			"SYNCHRO_NOISE_MIN_VALUES[1..N]_[1..N]",
			"SYNCHRO_NOISE_DROPOUT[1..N]_[1..N]",// dropout abh�ngig vom St�rger�usch und vom Sync-Bereich
			"SYNCHRO_IGNORE_NOISE","SYNCHRO_DEACTIVATE_NOISE_REC",// whether to ignore noise or not
			// debug
			"--- PARAMETERS for debugging ---",
			"DEBUG"
		};		
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = new String[] { "KANAL" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	public boolean checkArgs() {
		int i; //Hilfs-int f�r FOR schleifen
		try {
			//Sind alle requiredArgs gesetzt
			String sArg = "";
			String[] requiredArgs = getRequiredArgs();
			for( i = 0; i < requiredArgs.length; i++ ) {
				sArg = getArg( requiredArgs[i] );
				if( sArg == null || sArg.equals( "" ) ) {
					return false;
				}
			}
		} catch( Exception e ) {
			return false;
		}
		return true;
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 *
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {

		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		UserDialog ud = null;
		AudioAnalyser myAudio = null;
		EdiabasProxyThread ed = null;
		ReferenceDataDescriptor refData;
		StringBuffer resultBuffer = new StringBuffer( "" );
		ToleranzSpek spek;

		// index for tolerance files
		int ToleranceDirectoryIndex = 0;

		// index for result files
		int ResultDirectoryIndex = 0;

		//duration of Record
		int recordDuration = 5000;

		//for putting out a special error message
		String hwt = "";
		String zeroError = "";
		boolean useZeroAlert = true;

		//define if local trace is used
		boolean useTrace = true;
		boolean toleranceError = false;
		
		//for synchronous measurement
		boolean useSyncMeasurement=false;
		String[] syncName;
		float[][] syncFreqs;
		float sumsyncFreqs[];
		float[][] syncTolFreqs;
		int[] syncMaxSpecValues;
		int[][] indexTolFreqs;
		int[][] syncMins;
		int[][] syncMaxs;
		int[][] syncFreqsMin;
		int[][] syncFreqsMax;
		boolean useMax[];
		String syncSgbd="";
		String syncJob="";
		String syncJobPar="";
		//float syncInitFreq=0;
		float[] syncInitFrequencies = null;
		int numInitFreqs = 0;
		int syncVolume = 0;
		int[] syncVolumes = null;
		int syncChannel = 0;
		int[] numSyncFreqs = null;
		int[] syncRecordDurations = null;
		int syncInitPause = 50;
		int syncSingleFrequencyPause = 25;
		int syncJobParIndexVolume=-1;
		int syncJobParIndexFreq=-1;
		int syncJobParIndexChannel=-1;
		boolean syncJobParUseSpecialCommand = false;
		boolean syncSingleTrace = false;
		boolean useMinMaxTrace = false;
		boolean syncLocalTrace = false;
		int numOkFreqs = 0; 
		int [] syncDropout = null;
		boolean repeatOnceOnError = true; // default true
		int repeat = 0;
		
		// sync noise
		String[][] syncNoiseName= null ;
		int[][][] syncIndexNoiseFreqs= null;
		int[][] syncnumNoiseFreqs= null;
		int[][][] syncNoiseMaxs= null;
		int[][][] syncNoiseMins= null ;
		int[][]syncNoiseMinOccurr= null;
		boolean noiseDetected=false;
		boolean useSyncNoise = false;
		boolean ignore_noise = true;
		boolean deactivate_noise = false;
		int[] syncMaxSpecValues2 = null;
		int noise_num = 0;
		
		
		StringTokenizer myTokenizer = null;

		// define zero line measurement
		boolean isZero=false;
		boolean useZeroError = false;
		boolean useZeroConfirmWindow=false;
		boolean zeroConfirmSecondTest=false;
		int zeroConfirmWindowTimeOut=15;
		String zeroConfirmWindowText = "";
		if (isEnglish) zeroConfirmWindowText="Please insert microphone correctly";
		else zeroConfirmWindowText="Bitte Mikrofon korrekt einh�ngen";

		//float f1, f2;
		//int fSamplesIgnore;
		int i,j;

		//max index
		int maxIndex_max = 0; // Z�hler f�r die Maximum - Pr�fungen
		int maxIndex_min = 0; // Z�hler f�r die Minimum - Pr�fungen
		int maxIndex_klirr = 0; // Z�hler f�r die Klirr - Pr�fungen
		int maxIndex_sync = 0; // Z�hler f�r die Sync - Pr�fungen
		int maxIndex_sweep = 0; // Z�hler f�r die Sweep - Pr�fungen
		int maxIndex_noise_sync[] = null; // Z�hler f�r die Sync - St�rger�usch - Pr�fungen
		//int numNoiseFreq = 0;
		String[] name_max;
		String[] name_min;
		String[] name_klirr;
		float[] start_max;
		float[] start_min;
		float[] start_klirr;
		float[] stop_max;
		float[] stop_min;
		float[] stop_klirr;
		int[] drop_max;
		int[] drop_min;
		int[] drop_klirr;
		
		// limited frequencies for sweep measurement
		boolean useLimitedSweep = false;
		float[][] sweepTolFreqs;
		int[][] indexSweepTolFreqs;
		int[][] sweepMins;
		int[][] sweepMaxs;
		boolean useSweepMax[];
		int[] numSweepFreqs = null;
		int[] sweepMaxSpecValues = null;
		

		// used for execution
		String jobpar = "";
		String job = "";
		String temp = "";



		//other arguments
		String channel = "";
		String lstype = "";
		String radio = "";
		boolean debug = false;
		
		boolean useProfiles = false;
		boolean useProfileUDS = false;
		String[] profile_speakers = {"NONE","NONE","NONE"};
		String[] profile_tols = {"NONE","NONE","NONE"};
		boolean profile_first_zero_line = true;
		String profile_channel = "CHANNEL_FRONT_RIGHT";
		int profile_number_speakers = 1;
		
		final String PROFILE_SGBD = "SGBD"; 
		final String PROFILE_RECORDURATION = "RECORDDURATION_SYNC";  
		final String PROFILE_ZERO_FIRST = "ERRORTEXT_ZERO_FIRST_MEASUREMENT"; 
		final String PROFILE_ZERO_NEXT = "ERRORTEXT_ZERO_NEXT_MEASUREMENTS"; 
		final String PROFILE_ZERO_TIMEOUT = "ZERO_MEASUREMENT_TIMEOUT";
		final String PROFILE_INITFREQUENCY = "SYNCFREQUENCY_INIT";
		final String PROFILE_VOLUME = "SINEVOLUME";
		final String PROFILE_DEBUG = "DEBUG";
		final String PROFILE_REPEAT= "REPEAT_SINGLE_FREQ_ONCE_ON_ERROR";
		final String PROFILE_SINGLE_TRACE= "SYNC_SINGLE_TRACE";
		final String PROFILE_LOCAL_TRACE= "SYNC_LOCAL_TRACE";
		final String PROFILE_RADIOTYP= "SGBD";
		final String PROFILE_LSTYP= "LS_TYP";
		// uds
		final String PROFILE_UDS_JOB = "STEUERN_ROUTINE";
		final String PROFILE_UDS_JOBPAR = "ARG;SINUSGENERATOR;STR;%FREQUENCY%;%VOLUME%;%CHANNEL%";
		
		final String PROFILE_TWEETER_NAME= "TWEETER_NAME";
		final String PROFILE_TWEETER_FREQUENCIES= "SYNCFREQUENCIES_HIGH";
		final String PROFILE_TWEETER_DROPOUT= "SYNC_DROPOUT_HIGH";
		final String PROFILE_MIDRANGE_NAME= "MIDRANGE_NAME";
		final String PROFILE_MIDRANGE_FREQUENCIES= "SYNCFREQUENCIES_MID";
		final String PROFILE_MIDRANGE_DROPOUT= "SYNC_DROPOUT_MID";
		final String PROFILE_WOOFER_NAME= "WOOFER_NAME";
		final String PROFILE_WOOFER_FREQUENCIES= "SYNCFREQUENCIES_LOW";
		final String PROFILE_WOOFER_DROPOUT= "SYNC_DROPOUT_LOW";
			
		//NOISE
		final String PROFILE_NOISE_IGNORE= "IGNORE_NOISE";
		
		final String PROFILE_TWEETER_NOISE1_NAME= "SYNC_NOISE_TWEETER_NAME1";
		final String PROFILE_TWEETER_NOISE1_NAME_ALT= "SNYC_NOISE_TWEETER_NAME1";
		final String PROFILE_TWEETER_NOISE1_FREQ= "SYNC_NOISE_TWEETER_FREQ1";
		final String PROFILE_TWEETER_NOISE1_FREQ_ALT= "SNYC_NOISE_TWEETER_FREQ1";
		final String PROFILE_TWEETER_NOISE1_MIN= "SYNC_NOISE_TWEETER_MIN1";
		final String PROFILE_TWEETER_NOISE1_MIN_ALT= "SNYC_NOISE_TWEETER_MIN1";
		final String PROFILE_TWEETER_NOISE1_DROP= "SYNC_NOISE_TWEETER_DROPOUT1";
		
		final String PROFILE_TWEETER_NOISE2_NAME= "SYNC_NOISE_TWEETER_NAME2";
		final String PROFILE_TWEETER_NOISE2_NAME_ALT= "SNYC_NOISE_TWEETER_NAME2";
		final String PROFILE_TWEETER_NOISE2_FREQ= "SYNC_NOISE_TWEETER_FREQ2";
		final String PROFILE_TWEETER_NOISE2_FREQ_ALT= "SNYC_NOISE_TWEETER_FREQ2";
		final String PROFILE_TWEETER_NOISE2_MIN= "SYNC_NOISE_TWEETER_MIN2";
		final String PROFILE_TWEETER_NOISE2_MIN_ALT= "SNYC_NOISE_TWEETER_MIN2";
		final String PROFILE_TWEETER_NOISE2_DROP= "SYNC_NOISE_TWEETER_DROPOUT2";
		
		final String PROFILE_MIDRANGE_NOISE_NAME= "SYNC_NOISE_MIDRANGE_NAME";
		final String PROFILE_MIDRANGE_NOISE_NAME_ALT= "SNYC_NOISE_MIDRANGE_NAME";
		final String PROFILE_MIDRANGE_NOISE_FREQ= "SYNC_NOISE_MIDRANGE_FREQ";
		final String PROFILE_MIDRANGE_NOISE_FREQ_ALT= "SNYC_NOISE_MIDRANGE_FREQ";
		final String PROFILE_MIDRANGE_NOISE_MIN= "SYNC_NOISE_MIDRANGE_MIN";
		final String PROFILE_MIDRANGE_NOISE_MIN_ALT= "SNYC_NOISE_MIDRANGE_MIN";
		final String PROFILE_MIDRANGE_NOISE_DROP= "SYNC_NOISE_MIDRANGE_DROPOUT";
		
		final String PROFILE_WOOFER_NOISE_NAME= "SYNC_NOISE_WOOFER_NAME";
		final String PROFILE_WOOFER_NOISE_NAME_ALT= "SNYC_NOISE_WOOFER_NAME";
		final String PROFILE_WOOFER_NOISE_FREQ= "SYNC_NOISE_WOOFER_FREQ";
		final String PROFILE_WOOFER_NOISE_FREQ_ALT= "SNYC_NOISE_WOOFER_FREQ";
		final String PROFILE_WOOFER_NOISE_MIN= "SYNC_NOISE_WOOFER_MIN";
		final String PROFILE_WOOFER_NOISE_MIN_ALT= "SNYC_NOISE_WOOFER_MIN";
		final String PROFILE_WOOFER_NOISE_DROP= "SYNC_NOISE_WOOFER_DROPOUT";
		
		//TOLERANCES
		final String PROFILE_TOL= "SYNC_TOL_VALUES_";
		

		try {
// -------------------------------------------------------------- check Parameters ------------------------------------------

			try {
				//Parametercheck required args
				if( checkArgs() == false )
					if( isEnglish )
						throw new PPExecutionException( "Error checking mandatory arguments");
					else
						throw new PPExecutionException( "Fehler bei der �berpr�fung der Pflichtparameter" );

				String sArg,sArg2; //Argument
				
				// check if noise is deactivated even if other arguments are present
				sArg = getArg("SYNCHRO_DEACTIVATE_NOISE_REC");
				if (sArg!= null && sArg.equalsIgnoreCase( "" )== false && sArg.equalsIgnoreCase( "null" )== false) {
					deactivate_noise = sArg.equalsIgnoreCase("true");
					if(debug) System.out.println("IGNORE NOISE : " + ignore_noise);
				}
						
				sArg = getArg("SYNC_PROFILE");			
				if (sArg!= null && sArg.equalsIgnoreCase( "" )== false && sArg.equalsIgnoreCase( "null" )== false) {
					useProfiles = true;
					useSyncMeasurement = true;
					if (deactivate_noise) useSyncNoise = false;
					else useSyncNoise = true;
					myTokenizer = new StringTokenizer( sArg, ";" );
					for( j = 0; myTokenizer.hasMoreTokens(); j++ ) {
						String token = myTokenizer.nextToken();
						// zero line
						if (j==0) {
							//not UDS
							useProfileUDS = false;
							if (token.equalsIgnoreCase("FIRST_MEASUREMENT")) profile_first_zero_line = true;
							else if (token.equalsIgnoreCase("NEXT_MEASUREMENT")) profile_first_zero_line = false;
							// UDS
							else if (token.equalsIgnoreCase("UDS_FIRST_MEASUREMENT")) {
								profile_first_zero_line = true;
								useProfileUDS = true;
							}
							else if (token.equalsIgnoreCase("UDS_NEXT_MEASUREMENT")) {
								profile_first_zero_line = false;
								useProfileUDS = true;
							}
							else {
								if (isEnglish) throw new PPExecutionException( "Problem at SYNC PROFILE - ZERO LINE");
								else throw new PPExecutionException( "Problem beim SYNC PROFIL - ZERO LINE");
							}
						}
						// CHANNEL
						if (j==1) {
							if (token.length()>0) profile_channel = token;
							else {
								if (isEnglish) throw new PPExecutionException( "Problem at SYNC PROFILE - CHANNEL");
								else throw new PPExecutionException( "Problem beim SYNC PROFIL - KANAL");
							}
						}
						// SPEAKER
						if (j==2) {
							if (token.equalsIgnoreCase("TWEETER")) {
								profile_number_speakers = 1;
								profile_speakers[0]="TWEETER";
								maxIndex_noise_sync= new int[profile_number_speakers];
								maxIndex_noise_sync[0]=2;
								noise_num = 2;
							}
							else if (token.equalsIgnoreCase("MIDRANGE"))  {
								profile_number_speakers = 1;
								profile_speakers[0]="MIDRANGE";
								maxIndex_noise_sync= new int[profile_number_speakers];
								maxIndex_noise_sync[0]=1;
								noise_num = 1;
							}
							else if (token.equalsIgnoreCase("WOOFER"))  {
								profile_number_speakers = 1;
								profile_speakers[0]="WOOFER";
								maxIndex_noise_sync= new int[profile_number_speakers];
								maxIndex_noise_sync[0]=1;
								noise_num = 1;
							}
							else if (token.equalsIgnoreCase("TWEETER+MIDRANGE"))  {
								profile_number_speakers = 2;
								profile_speakers[0]="TWEETER";
								profile_speakers[1]="MIDRANGE";
								maxIndex_noise_sync= new int[profile_number_speakers];
								maxIndex_noise_sync[0]=2;
								maxIndex_noise_sync[1]=1;
								noise_num = 2;
							}
							else if (token.equalsIgnoreCase("TWEETER+WOOFER")) {
								profile_number_speakers = 2;
								profile_speakers[0]="TWEETER";
								profile_speakers[1]="WOOFER";
								maxIndex_noise_sync= new int[profile_number_speakers];
								maxIndex_noise_sync[0]=2;
								maxIndex_noise_sync[1]=1;
								noise_num = 2;
							}
							else if (token.equalsIgnoreCase("MIDRANGE+WOOFER")) {
								profile_number_speakers = 2;
								profile_speakers[0]="MIDRANGE";
								profile_speakers[1]="WOOFER";
								maxIndex_noise_sync= new int[profile_number_speakers];
								maxIndex_noise_sync[0]=1;
								maxIndex_noise_sync[1]=1;
								noise_num = 1;
							}
							else if (token.equalsIgnoreCase("TWEETER+MIDRANGE+WOOFER")) {
								profile_number_speakers = 3;
								profile_speakers[0]="TWEETER";
								profile_speakers[1]="MIDRANGE";
								profile_speakers[2]="WOOFER";
								maxIndex_noise_sync= new int[profile_number_speakers];
								maxIndex_noise_sync[0]=2;
								maxIndex_noise_sync[1]=1;
								maxIndex_noise_sync[2]=1;
								noise_num = 2;
							}
							else {
								if (isEnglish) throw new PPExecutionException( "Problem at SYNC PROFILE - SPEAKER");
								else throw new PPExecutionException( "Problem beim SYNC PROFIL - LAUTSPRECHER");
							}
						}
						// tol names
						if (j==3) {
							if (token.length()>0) profile_tols[0] = token;
						}
						if (j==4) {
							if (token.length()>0) profile_tols[1] = token;
						}
						if (j==5) {
							if (token.length()>0) profile_tols[2] = token;
						}
					}
					if (j<2+profile_number_speakers) {
						if (isEnglish) throw new PPExecutionException( "Problem at SYNC PROFILE - TOLERANCE NAMES");
						else throw new PPExecutionException( "Problem beim SYNC PROFIL - TOLERANZNAMEN");
					}
				}
				
				sArg = getArg("DEBUG");	
				if (sArg == null && useProfiles) sArg = this.getPr�fling().getAttribut(PROFILE_DEBUG);
				if (sArg!= null && sArg.equalsIgnoreCase( "" )== false && sArg.equalsIgnoreCase( "null" )== false) {
					debug = sArg.equalsIgnoreCase("true");
					if(debug)System.out.println("debug: " + debug);
				}
				
				// -------------- complete parameters------------------------------------------------------------------------
				Enumeration enu = getArgs().keys();
				String givenkey;
				// calculate max index
				if (useProfiles) {
					maxIndex_sync = profile_number_speakers;
				}
				else {
					while( enu.hasMoreElements() ) {
						givenkey = (String) enu.nextElement();
						//max
						if( givenkey.startsWith( "FBNAME" )) {
							sArg = getArg(givenkey);
							if (sArg!= null && sArg.equalsIgnoreCase( "" )== false && sArg.equalsIgnoreCase( "null" )== false) {
								i = Integer.parseInt( givenkey.substring( 6 ) );
								if (i>maxIndex_max) maxIndex_max=i;
							}
						}
						// min
						if( givenkey.startsWith( "MBNAME" )) {
							sArg = getArg(givenkey);
							if (sArg!= null && sArg.equalsIgnoreCase( "" )== false && sArg.equalsIgnoreCase( "null" )== false) {
								i = Integer.parseInt( givenkey.substring( 6 ) );
								if (i>maxIndex_min) maxIndex_min=i;
							}
						}
						//  rattle
						if( givenkey.startsWith( "KBNAME" )) {
							sArg = getArg(givenkey);
							if (sArg!= null && sArg.equalsIgnoreCase( "" )== false && sArg.equalsIgnoreCase( "null" )== false) {
								i = Integer.parseInt( givenkey.substring( 6 ) );
								if (i>maxIndex_klirr) maxIndex_klirr=i;
							}
						}
						// synchro
						if( givenkey.startsWith( "SYNCHRO_NAME" )) {
							//if(debug)System.out.println(" snychro name found! "); 
							sArg = getArg(givenkey);
							if (sArg!= null && sArg.equalsIgnoreCase( "" )== false && sArg.equalsIgnoreCase( "null" )== false) {
								i = Integer.parseInt( givenkey.substring( 12 ) );
								if (i>maxIndex_sync) maxIndex_sync=i;
								useSyncMeasurement = true;
								if(debug) System.out.println("Number sync Block is : " + maxIndex_sync); 
							}
						}
						// sweep limited 
						if( !useLimitedSweep && givenkey.startsWith( "SWEEP_TOL_FREQUENCIES" )) {
							//if(debug)System.out.println(" snychro name found! "); 
							sArg = getArg(givenkey);
							if (sArg!= null && sArg.equalsIgnoreCase( "" )== false && sArg.equalsIgnoreCase( "null" )== false) {
								i = Integer.parseInt( givenkey.substring( 21 ) );
								if (i>maxIndex_sweep) maxIndex_sweep=i;
								useLimitedSweep = true;
								//if(debug)System.out.println("Number sweep Block is : " + maxIndex_sweep); 
							}
						}
					}
				}
				
				// check max for synchro noise -> subarrays individual for each sync name
				if (!useProfiles) {
					if (useSyncMeasurement && !deactivate_noise) {
						maxIndex_noise_sync = new int[maxIndex_sync];
						// initialize noise blocks
						for (i = 0; i<maxIndex_sync; i++ ) {
							maxIndex_noise_sync[i]=0;
						}
						enu = getArgs().keys();
						while(enu.hasMoreElements()){
							givenkey = (String) enu.nextElement();
							// synchro noise 
							if( givenkey.startsWith( "SYNCHRO_NOISE_NAME" )) {
								sArg = getArg(givenkey);
								if (sArg!= null && sArg.equalsIgnoreCase( "" )== false && sArg.equalsIgnoreCase( "null" )== false) {
									String tempString = givenkey.substring( 18 );
									int position = tempString.indexOf( '_' );
									int index1 = Integer.parseInt(tempString.substring( 0, position ));
									int index2 = Integer.parseInt(tempString.substring( position+1 ));
									if (index2 >maxIndex_noise_sync[index1-1]) maxIndex_noise_sync[index1-1]=index2;
									useSyncNoise=true;
									if(noise_num <  maxIndex_noise_sync[index1 -1] )noise_num = maxIndex_noise_sync[index1 -1];
									
								}
							}
						}
					}
				}

				// check if any measurement type is specified
				if (maxIndex_max==0 && maxIndex_min==0 && maxIndex_klirr==0 && maxIndex_sync==0) {
					if (isEnglish) throw new PPExecutionException( "No measurement specified with FBNAME, KBNAME, MBNAME or SYNCHRO_NAME");
					else throw new PPExecutionException( "Keine Messung spezifiziert mit FBNAME, KBNAME, MBNAME or SYNCHRO_NAME");
				}

				// check if all enumerated values are present
				name_max = new String[maxIndex_max];
				start_max = new float[maxIndex_max];
				stop_max = new float[maxIndex_max];
				drop_max = new int[maxIndex_max];
				sweepTolFreqs = new float[maxIndex_max][];
				indexSweepTolFreqs = new int[maxIndex_max][];
				sweepMins = new int[maxIndex_max][];
				sweepMaxs = new int[maxIndex_max][];
				useSweepMax = new boolean[maxIndex_max];
				numSweepFreqs = new int[maxIndex_max];
				for( i = 1; i <= maxIndex_max; i++ ) { //Maximum - Parameter
					sArg = getArg("FBNAME" + i);
					if( (sArg == null) || (sArg == "") ) {
						if (isEnglish) throw new PPExecutionException( "Error Parameter FBNAME" + i + ": not present");
						else throw new PPExecutionException( "Fehler Parameter FBNAME" + i + ": nicht vorhanden");
					}
					else name_max[i-1] = sArg;
					// getting parameters for limited sweep measurment
					if (useLimitedSweep) {
						// parsing tol freqs
						sArg = getArg( "SWEEP_TOL_FREQUENCIES" + i );
						if( (sArg == null) || (sArg == "") ) {
							if (isEnglish) throw new PPExecutionException( "Error Parameter SWEEP_TOL_FREQUENCIES" + i + ": not present");
							else throw new PPExecutionException( "Fehler Parameter SWEEP_TOL_FREQUENCIES" + i + ": nicht vorhanden");
						}
						else {
						    myTokenizer = new StringTokenizer( sArg, ";" );
						    numSweepFreqs[i-1] = myTokenizer.countTokens();
							sweepTolFreqs[i-1] = new float[numSweepFreqs[i-1]];
							indexSweepTolFreqs[i-1] = new int[numSweepFreqs[i-1]];
							for( j = 0; myTokenizer.hasMoreTokens(); j++ ) {
								String token = myTokenizer.nextToken();
								try {
									if (debug) System.out.println("Parsing Sweep Tol_Freq ["+(i-1)+"]["+(j)+"]: "+token);
									float newfloat = Float.parseFloat( token );
									indexSweepTolFreqs[i-1][j] = Arrays.binarySearch( DN6000Constants.FREQUENCY_FRAME, newfloat);
									if (debug) System.out.println("Found Sweep frequency at index:"+indexSweepTolFreqs[i-1][j]);
									if( indexSweepTolFreqs[i-1][j] < 0 ) {
										if( isEnglish ) 
											throw new PPExecutionException( "Frequency at SWEEP_TOL_FREQUENCIES" + i + " do not match with anaylser frequency list" );
										else
											throw new PPExecutionException( "Frequenz bei SWEEP_TOL_FREQUENCIES" + i + " nicht gefunden in Analyser Frequenzliste" );
									}
									else sweepTolFreqs[i-1][j] = newfloat;
								} catch( Exception e ) {
									if( isEnglish ){
										result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Error in sweep tol frequencies ("+i+"): " + e.getMessage(), Ergebnis.FT_NIO_SYS );
									}else{
										result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Fehler bei sweep Tol Frequenzen ("+i+"): " + e.getMessage(), Ergebnis.FT_NIO_SYS );
									}ergListe.add( result );
									throw new PPExecutionException();
								}
							}
						}
						
						// parsing sync min values
						sArg = getArg( "SWEEP_MIN_VALUES" + i );
						if( (sArg == null) || (sArg == "")) {
							if (isEnglish) throw new PPExecutionException( "Error Parameter SWEEP_MIN_VALUES" + i + ": not present");
							else throw new PPExecutionException( "Fehler Parameter SWEEP_MIN_VALUES" + i + ": nicht vorhanden");
						}
						else {
							myTokenizer = new StringTokenizer( sArg, ";" );
							sweepMins[i-1] = new int[numSweepFreqs[i-1]];
							for( j = 0; myTokenizer.hasMoreTokens(); j++ ) {
								String token = myTokenizer.nextToken();
								try {
									int newInt = Integer.parseInt( token );
									if( newInt < 0 || newInt > 63 ) {
										if( isEnglish )
											throw new PPExecutionException( "Values at SWEEP_MIN_VALUES" + i + " out of range [0;63]" );
										else
											throw new PPExecutionException( "Werte bei SWEEP_MIN_VALUES" + i + "ausserhalb des Bereichs [0;63]" );
									}
									else sweepMins[i-1][j] = newInt;
								} catch( Exception e ) {
									if( isEnglish )
										result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Error in sweep min values: " + e.getMessage(), Ergebnis.FT_NIO_SYS );
									else
										result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Fehler bei sweep Min Werten: " + e.getMessage(), Ergebnis.FT_NIO_SYS );
									ergListe.add( result );
									throw new PPExecutionException();
								}
							}
						}
						// parsing sync max values
						sArg = getArg( "SWEEP_MAX_VALUES" + i );
						if( sArg != null) {
							useSweepMax[i-1]=true;
							myTokenizer = new StringTokenizer( sArg, ";" );
							sweepMaxs[i-1] = new int[numSweepFreqs[i-1]];
							for( j = 0; myTokenizer.hasMoreTokens(); j++ ) {
								String token = myTokenizer.nextToken();
								try {
									int newInt = Integer.parseInt( token );
									if( newInt < 0 || newInt > 63 ) {
										if( isEnglish )
											throw new PPExecutionException( "Values at SWEEP_MAX_VALUES" + i + " out of range [0;63]" );
										else
											throw new PPExecutionException( "Werte bei SWEEP_MAX_VALUES" + i + "ausserhalb des Bereichs [0;63]" );
									}
									else sweepMaxs[i-1][j] = newInt;
									// check max > min
									if (sweepMaxs[i-1][j]<sweepMins[i-1][j]) {
										if (isEnglish) throw new PPExecutionException( "Error Parameter SWEEP_MIN_VALUES" + i + " Number "+j+" greater than SWEEP_MAX_VALUES" + i);
										else throw new PPExecutionException( "Fehler Parameter SWEEP_MIN_VALUES" + i + " Nummer "+j+" gr��er als SWEEP_MAX_VALUES" + i);
									}
								} catch( Exception e ) {
									if( isEnglish )
										result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Error in synchro max values: " + e.getMessage(), Ergebnis.FT_NIO_SYS );
									else
										result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Fehler bei Synchro Max Werten: " + e.getMessage(), Ergebnis.FT_NIO_SYS );
									ergListe.add( result );
									throw new PPExecutionException();
								}
							}
						}
						else useSweepMax[i-1]=false;
						
					}
					// getting parameters for normal max measurment
					else {
						sArg = getArg( "FSTART" + i );
						if( (sArg == null) || (sArg == "")) {
							if (isEnglish) throw new PPExecutionException( "Error Parameter FSTART" + i + ": not present");
							else throw new PPExecutionException( "Fehler Parameter FSTART" + i + ": nicht vorhanden");
						}
						sArg2 = getArg( "FSTOP" + i );
						if( (sArg2 == null) || (sArg2 == "") ) {
							if (isEnglish) throw new PPExecutionException( "Error Parameter FSTOP" + i + ": not present");
							else throw new PPExecutionException( "Fehler Parameter FSTOP" + i + ": nicht vorhanden");
						}
						if( ((sArg != null) && (sArg2 != null)) &&
							(Float.parseFloat( sArg ) >= Float.parseFloat( sArg2 ))) {
							if (isEnglish) throw new PPExecutionException( "Error Parameter FSTART" + i + " greater than FSTOP" + i);
							else throw new PPExecutionException( "Fehler Parameter FSTART" + i + " gr��er als FSTOP" + i);
						}
						else {
							start_max[i-1] = Float.parseFloat(sArg);
							stop_max[i-1] = Float.parseFloat(sArg2);
						}
					}
					// dropouts needed for both methods
					if( (getArg( "FBIGNORE" + i ) == null) || (getArg( "FBIGNORE" + i ) == "") ) {
						if (isEnglish) throw new PPExecutionException( "Error Parameter FBIGNORE" + i + ": not present");
						else throw new PPExecutionException( "Fehler Parameter FBIGNORE" + i + ": nicht vorhanden");
					}
					else drop_max[i-1] = Integer.parseInt( getArg( "FBIGNORE" + i ) );
				}
				name_min = new String[maxIndex_max];
				start_min = new float[maxIndex_max];
				stop_min = new float[maxIndex_max];
				drop_min = new int[maxIndex_max];
				for( i = 1; i <= maxIndex_min; i++ ) { //Minimum - Parameter
					sArg = getArg( "MBNAME" + i );
					if( (sArg == null) || (sArg == "") ) {
						if (isEnglish) throw new PPExecutionException( "Error Parameter MBNAME" + i + ": not present");
						else throw new PPExecutionException( "Fehler Parameter MBNAME" + i + ": nicht vorhanden");
					}
					else name_min[i-1] = sArg;
					sArg = getArg( "MBIGNORE" + i );
					if( (sArg == null) || (sArg == "") ) {
						if (isEnglish) throw new PPExecutionException( "Error Parameter MBIGNORE" + i + ": not present");
						else throw new PPExecutionException( "Fehler Parameter MBIGNORE" + i + ": nicht vorhanden");
					}
					else drop_min[i-1] = Integer.parseInt( sArg );
					sArg = getArg( "MSTART" + i );
					if( (sArg == null) || (sArg == "")) {
						if (isEnglish) throw new PPExecutionException( "Error Parameter MSTART" + i + ": not present");
						else throw new PPExecutionException( "Fehler Parameter MSTART" + i + ": nicht vorhanden");
					}
					sArg2 = getArg( "MSTOP" + i );
					if( (getArg( "MSTOP" + i ) == null) || (getArg( "MSTOP" + i ) == "") ) {
						if (isEnglish) throw new PPExecutionException( "Error Parameter MSTOP" + i + ": not present");
						else throw new PPExecutionException( "Fehler Parameter MSTOP" + i + ": nicht vorhanden");
					}
					if( ((sArg != null) && (sArg2 != null)) &&
						(Float.parseFloat( sArg ) >= Float.parseFloat( sArg2 ))) {
						if (isEnglish) throw new PPExecutionException( "Error Parameter MSTART" + i + " greater than MSTOP" + i);
						else throw new PPExecutionException( "Fehler Parameter MSTART" + i + " gr��er als MSTOP" + i);

					}
					else {
						start_min[i-1] = Float.parseFloat(sArg);
						stop_min[i-1] = Float.parseFloat(sArg2);
					}
				}
				name_klirr = new String[maxIndex_max];
				start_klirr = new float[maxIndex_max];
				stop_klirr = new float[maxIndex_max];
				drop_klirr = new int[maxIndex_max];
				for( i = 1; i < maxIndex_klirr; i++ ) { //Klirr - Parameter
					sArg = getArg( "KBNAME" + i );
					if( (sArg == null) || (sArg == "") ) {
						if (isEnglish) throw new PPExecutionException( "Error Parameter KBNAME" + i + ": not present");
						else throw new PPExecutionException( "Fehler Parameter KBNAME" + i + ": nicht vorhanden");
					}
					else name_klirr[i-1] = sArg;
					sArg = getArg( "KBIGNORE" + i );
					if( (sArg == null) || (sArg == "") ) {
						if (isEnglish) throw new PPExecutionException( "Error Parameter KBIGNORE" + i + ": not present");
						else throw new PPExecutionException( "Fehler Parameter KBIGNORE" + i + ": nicht vorhanden");
					}
					else drop_klirr[i-1] = Integer.parseInt( sArg );
					sArg = getArg( "KSTART" + i );
					if( (sArg == null) || (sArg == "")) {
						if (isEnglish) throw new PPExecutionException( "Error Parameter KSTART" + i + ": not present");
						else throw new PPExecutionException( "Fehler Parameter KSTART" + i + ": nicht vorhanden");
					}
					sArg2 = getArg( "KSTOP" + i );
					if( (sArg2 == null) || (sArg2 == "") ) {
						if (isEnglish) throw new PPExecutionException( "Error Parameter KSTOP" + i + ": not present");
						else throw new PPExecutionException( "Fehler Parameter KSTOP" + i + ": nicht vorhanden");
					}
					if( ((sArg != null) && (sArg2 != null)) &&
						(Float.parseFloat( sArg ) >= Float.parseFloat( sArg2 ))) {
						if (isEnglish) throw new PPExecutionException( "Error Parameter KSTART" + i + " greater than KSTOP" + i);
						else throw new PPExecutionException( "Fehler Parameter KSTART" + i + " gr��er als KSTOP" + i);

					}
					else {
						start_klirr[i-1] = Float.parseFloat(sArg);
						stop_klirr[i-1] = Float.parseFloat(sArg2);
					}
				}
				
				// recordduration now read before sync to be able to use it for individual record durations
				sArg = getArg( "RECORDDURATION" );
				if (sArg == null && useProfiles) sArg = this.getPr�fling().getAttribut(PROFILE_RECORDURATION);
				if( sArg != null ) {
					int tempInt = Integer.parseInt( sArg );
					if( tempInt >= 25 && tempInt <= 15000 )
						recordDuration = tempInt;
					else if( isEnglish )
						throw new PPExecutionException( "Checkargs Error: Duration out of bounds" );
					else
						throw new PPExecutionException( "ParameterFehler: Aufnahmezeit ausserhalb des zul�ssigen Bereichs" );
				}
				// V58 initial frequency and volume now read before sync to be able to use it for individual settings
				sArg = getArg( "SYNCHRO_INITFREQUENCY" );
				if (sArg == null && useProfiles) sArg = this.getPr�fling().getAttribut(PROFILE_INITFREQUENCY);
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null" )))) {
					myTokenizer = new StringTokenizer( sArg, ";" );
					numInitFreqs = myTokenizer.countTokens();
					syncInitFrequencies = new float[numInitFreqs];
					for( j = 0; myTokenizer.hasMoreTokens(); j++ ) {
						String token = myTokenizer.nextToken();
						try {
							if (debug) System.out.println("Parsing_InitFreq["+(j)+"]: "+token);
							float newfloat = Float.parseFloat( token );
							if( newfloat < 1 || newfloat > 22000 ) {
								if( isEnglish )
									throw new PPExecutionException( "Frequency (No."+(j+1)+") at SYNCHRO_INITFREQUENCY" + i + " out of range [1;22000]" );
								else
									throw new PPExecutionException( "Frequenz (Nr."+(j+1)+") bei SYNCHRO_INITFREQUENCY" + i + "ausserhalb des Bereichs [1;22000]" );
							}
							else {
								syncInitFrequencies[j] = newfloat;
							}
						} catch( Exception e ) {
							if( isEnglish ){
								result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Error in synchro frequencies("+i+"): " + e.getMessage(), Ergebnis.FT_NIO_SYS );
							}else{
								result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Fehler bei Synchro Frequenzen("+i+"): " + e.getMessage(), Ergebnis.FT_NIO_SYS );
							}ergListe.add( result );
							throw new PPExecutionException();
						}
					}
				}
				sArg = getArg( "SYNCHRO_VOLUME" );
				if (sArg == null && useProfiles) sArg = this.getPr�fling().getAttribut(PROFILE_VOLUME);
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null" )))) {
					if (sArg.indexOf( '@' ) != -1) {
						int index = sArg.indexOf( '@' );
						try {
							sArg = getPr�fling().getAuftrag().getPr�fling( sArg.substring( index+1 ) ).getAttribut( sArg.substring( 0, index ));
						} catch (Exception e) {
							if( isEnglish )
								throw new PPExecutionException( "ERROR getting SYNCHRO_VOLUME with @-command!" );
							else
								throw new PPExecutionException( "Fehler beim Holen von SYNCHRO_VOLUME mit dem @-Kommando" );
						}
					}
					//int tempInt = Integer.parseInt( sArg );
					
					try{
						int tempInt = (Integer.decode( sArg)).intValue();
						if( tempInt < -96 || tempInt > 127 ) {
							if( isEnglish )
								throw new PPExecutionException( "Volume at SYNCHRO_VOLUME: out of range [-96;127]" );
							else
								throw new PPExecutionException( "Lautst�rke bei SYNCHRO_VOLUME: ausserhalb des Bereichs [-96;127]" );
						}
						else syncVolume = tempInt;
					}
					catch(Exception e){ e.printStackTrace();};					
				}
				
				// initializing sync values
				syncFreqs = new float[maxIndex_sync][];
				syncTolFreqs = new float[maxIndex_sync][];
				indexTolFreqs = new int[maxIndex_sync][];
				syncMins = new int[maxIndex_sync][];
				syncMaxs = new int[maxIndex_sync][];
				numSyncFreqs = new int[maxIndex_sync];
				useMax = new boolean[maxIndex_sync];
				syncName = new String[maxIndex_sync];
				syncDropout = new int[maxIndex_sync];
				sumsyncFreqs = new float[maxIndex_sync];
				syncNoiseName=new String[maxIndex_sync][];
				syncNoiseMinOccurr = new int[maxIndex_sync][];
				syncnumNoiseFreqs=new int[maxIndex_sync][];
				syncIndexNoiseFreqs=new int[maxIndex_sync][][];
				syncNoiseMaxs=new int[maxIndex_sync][][];
				syncNoiseMins=new int[maxIndex_sync][][];
				syncRecordDurations = new int[maxIndex_sync];
				syncVolumes = new int[maxIndex_sync];
				// new minMax
				syncFreqsMin = new int[maxIndex_sync][];
				syncFreqsMax = new int[maxIndex_sync][];
				
				// getting Synchro - Parameter
				for( i = 1; i <= maxIndex_sync; i++ ) {  
					sArg = getArg( "SYNCHRO_NAME" + i );
					if (sArg == null && useProfiles) {
						if (profile_speakers[i-1].equalsIgnoreCase("TWEETER")) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_NAME);
						if (profile_speakers[i-1].equalsIgnoreCase("MIDRANGE")) sArg = this.getPr�fling().getAttribut(PROFILE_MIDRANGE_NAME);
						if (profile_speakers[i-1].equalsIgnoreCase("WOOFER")) sArg = this.getPr�fling().getAttribut(PROFILE_WOOFER_NAME);
					}
					if( (sArg == null) || (sArg == "") ) {
						if (isEnglish) throw new PPExecutionException( "Error Parameter SYNCHRO_NAME" + i + ": not present");
						else throw new PPExecutionException( "Fehler Parameter SYNCHRO_NAME" + i + ": nicht vorhanden");
					}
					else {
						syncName[i-1] = sArg;
					}
					// parsing individual record durations
					sArg = getArg( "SYNCHRO_RECORDDURATION" + i );
					if( sArg != null && sArg.length()>0) {
						int tempInt = Integer.parseInt( sArg );
						if( tempInt >= 100 && tempInt <= 15000 )
							syncRecordDurations[i-1] = tempInt;
						else if( isEnglish )
							throw new PPExecutionException( "Recordduration at SYNCHRO_RECORDDURATION" + i + " out of range [100;15000]" );
						else
							throw new PPExecutionException( "Aufnahmezeit bei SYNCHRO_RECORDDURATION" + i + "ausserhalb des Bereichs [100;15000]" );
					}
					else syncRecordDurations[i-1] = recordDuration;
					// parsing individual volumes
					sArg = getArg( "SYNCHRO_VOLUME" + i );
					if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null" )))) {
						if (sArg.indexOf( '@' ) != -1) {
							int index = sArg.indexOf( '@' );
							try {
								sArg = getPr�fling().getAuftrag().getPr�fling( sArg.substring( index+1 ) ).getAttribut( sArg.substring( 0, index ));
							} catch (Exception e) {
								if( isEnglish )
									throw new PPExecutionException( "ERROR getting SYNCHRO_VOLUME"+i+" with @-command!" );
								else
									throw new PPExecutionException( "Fehler beim Holen von SYNCHRO_VOLUME"+i+" mit dem @-Kommando" );
							}
						}
						//int tempInt = Integer.parseInt( sArg );
						
						try{
							int tempInt = (Integer.decode( sArg)).intValue();
							if( tempInt < -96 || tempInt > 127 ) {
								if( isEnglish )
									throw new PPExecutionException( "Volume at SYNCHRO_VOLUME"+i+": out of range [-96;127]" );
								else
									throw new PPExecutionException( "Lautst�rke bei SYNCHRO_VOLUME"+i+": ausserhalb des Bereichs [-96;127]" );
							}
							else syncVolumes[i-1] = tempInt;
						}
						catch(Exception e){ e.printStackTrace();};					
					}
					else syncVolumes[i-1] = syncVolume;
					
					// parsing output frequencies
					sArg = getArg( "SYNCHRO_FREQUENCIES" + i );
					if (sArg == null && useProfiles) {
						if (profile_speakers[i-1].equalsIgnoreCase("TWEETER")) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_FREQUENCIES);
						if (profile_speakers[i-1].equalsIgnoreCase("MIDRANGE")) sArg = this.getPr�fling().getAttribut(PROFILE_MIDRANGE_FREQUENCIES);
						if (profile_speakers[i-1].equalsIgnoreCase("WOOFER")) sArg = this.getPr�fling().getAttribut(PROFILE_WOOFER_FREQUENCIES);
					}
					if( (sArg == null) || (sArg == "") ) {
						if (isEnglish) throw new PPExecutionException( "Error Parameter SYNCHRO_FREQUENCIES" + i + ": not present");
						else throw new PPExecutionException( "Fehler Parameter SYNCHRO_FREQUENCIES" + i + ": nicht vorhanden");
					}
					else {
						sumsyncFreqs[i-1] = 0;
						myTokenizer = new StringTokenizer( sArg, ";" );
						numSyncFreqs[i-1] = myTokenizer.countTokens();
						syncFreqs[i-1] = new float[numSyncFreqs[i-1]];
						for( j = 0; myTokenizer.hasMoreTokens(); j++ ) {
							String token = myTokenizer.nextToken();
							try {
								if (debug) System.out.println("Parsing_Freq ["+(i-1)+"]["+(j)+"]: "+token);
								float newfloat = Float.parseFloat( token );
								if( newfloat < -1 || newfloat > 22000 ) {
									if( isEnglish )
										throw new PPExecutionException( "Frequency (No."+(j+1)+") at SYNCHRO_FREQUENCIES" + i + " out of range [-1;22000]" );
									else
										throw new PPExecutionException( "Frequenz (Nr"+(j+1)+") bei SYNCHRO_FREQUENCIES" + i + "ausserhalb des Bereichs [-1;22000]" );
								}
								else {
									syncFreqs[i-1][j] = newfloat;
									sumsyncFreqs[i-1] += newfloat;
								}
							} catch( Exception e ) {
								if( isEnglish ){
									result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Error in synchro frequencies("+i+"): " + e.getMessage(), Ergebnis.FT_NIO_SYS );
								}else{
									result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Fehler bei Synchro Frequenzen("+i+"): " + e.getMessage(), Ergebnis.FT_NIO_SYS );
								}ergListe.add( result );
								throw new PPExecutionException();
							}
						}
					}
					// parsing tol freqs
					sArg = getArg( "SYNCHRO_TOL_FREQUENCIES" + i );
					if (sArg == null && useProfiles) {
						if (profile_speakers[i-1].equalsIgnoreCase("TWEETER")) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_FREQUENCIES);
						if (profile_speakers[i-1].equalsIgnoreCase("MIDRANGE")) sArg = this.getPr�fling().getAttribut(PROFILE_MIDRANGE_FREQUENCIES);
						if (profile_speakers[i-1].equalsIgnoreCase("WOOFER")) sArg = this.getPr�fling().getAttribut(PROFILE_WOOFER_FREQUENCIES);
					}
					if( (sArg == null) || (sArg == "") ) {
						if (isEnglish) throw new PPExecutionException( "Error Parameter SYNCHRO_TOL_FREQUENCIES" + i + ": not present");
						else throw new PPExecutionException( "Fehler Parameter SYNCHRO_TOL_FREQUENCIES" + i + ": nicht vorhanden");
					}
					else {
					    myTokenizer = new StringTokenizer( sArg, ";" );
						if (numSyncFreqs[i-1] != myTokenizer.countTokens()) {
							if( isEnglish )
								throw new PPExecutionException( "Number frequencies at SYNCHRO_TOL_FREQUENCIES does not match with number at SYNCHRO_FREQUENCIES" );
							else
								throw new PPExecutionException( "Anzahl Frequenz bei SYNCHRO_TOL_FREQUENCIES stimmt nicht mit der Anzahl bei SYNCHRO_FREQUENCIES �berein" );
						}
						syncTolFreqs[i-1] = new float[numSyncFreqs[i-1]];
						indexTolFreqs[i-1] = new int[numSyncFreqs[i-1]];
						// also fill min max
						syncFreqsMin[i-1] = new int[numSyncFreqs[i-1]]; 
						syncFreqsMax[i-1] = new int[numSyncFreqs[i-1]];
						for( j = 0; myTokenizer.hasMoreTokens(); j++ ) {
							String token = myTokenizer.nextToken();
							try {
								if (debug) System.out.println("Parsing Tol_Freq ["+(i-1)+"]["+(j)+"]: "+token);
								float newfloat = Float.parseFloat( token );
								indexTolFreqs[i-1][j] = Arrays.binarySearch( DN6000Constants.FREQUENCY_FRAME, newfloat);
								if (debug) System.out.println("Found frequency at index:"+indexTolFreqs[i-1][j]);
								if( indexTolFreqs[i-1][j] < 0 ) {
									if( isEnglish ) 
										throw new PPExecutionException( "Frequency (No."+(j+1)+") at SYNCHRO_TOL_FREQUENCIES" + i + " do not match with anaylser frequency list" );
									else
										throw new PPExecutionException( "Frequenz (No."+(j+1)+") bei SYNCHRO_TOL_FREQUENCIES" + i + " nicht gefunden in Analyser Frequenzliste" );
								}
								else {
									syncTolFreqs[i-1][j] = newfloat;
								}
							} catch( Exception e ) {
								if( isEnglish ){
									result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Error in synchro tol frequencies ("+i+"): " + e.getMessage(), Ergebnis.FT_NIO_SYS );
								}else{
									result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Fehler bei Synchro Tol Frequenzen ("+i+"): " + e.getMessage(), Ergebnis.FT_NIO_SYS );
								}ergListe.add( result );
								throw new PPExecutionException();
							}
						}
					}
					// parsing sync dropout	
					sArg = getArg( "SYNCHRO_DROPOUT" + i );
					if (sArg == null && useProfiles) {
						if (profile_speakers[i-1].equalsIgnoreCase("TWEETER")) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_DROPOUT);
						if (profile_speakers[i-1].equalsIgnoreCase("MIDRANGE")) sArg = this.getPr�fling().getAttribut(PROFILE_MIDRANGE_DROPOUT);
						if (profile_speakers[i-1].equalsIgnoreCase("WOOFER")) sArg = this.getPr�fling().getAttribut(PROFILE_WOOFER_DROPOUT);
					}
					if (sArg!= null && sArg.equalsIgnoreCase( "" )== false && sArg.equalsIgnoreCase( "null" )== false) {
						syncDropout[i-1] = Integer.parseInt(sArg);
						//if(debug) System.out.println("syncDropout[" + (i-1)+ " ]:  " + syncDropout[i-1]);
						
					}
					sArg = getArg( "SYNCHRO_MIN_VALUES" + i );
					if (sArg == null && useProfiles) sArg = this.getPr�fling().getAttribut(PROFILE_TOL+profile_tols[i-1]);
					// parsing sync min values
					if( (sArg == null) || (sArg == "")) {
						if (isEnglish) throw new PPExecutionException( "Error Parameter SYNCHRO_MIN_VALUES" + i + ": not present");
						else throw new PPExecutionException( "Fehler Parameter SYNCHRO_MIN_VALUES" + i + ": nicht vorhanden");
					}
					else {
						myTokenizer = new StringTokenizer( sArg, ";" );
						syncMins[i-1] = new int[numSyncFreqs[i-1]];
						for( j = 0; myTokenizer.hasMoreTokens(); j++ ) {
							String token = myTokenizer.nextToken();
							try {
								int newInt = Integer.parseInt( token );
								if( newInt < 0 || newInt > 63 ) {
									if( isEnglish )
										throw new PPExecutionException( "Values at SYNCHRO_MIN_VALUES" + i + " out of range [0;63]" );
									else
										throw new PPExecutionException( "Werte bei SYNCHRO_MIN_VALUES" + i + "ausserhalb des Bereichs [0;63]" );
								}
								else syncMins[i-1][j] = newInt;
							} catch( Exception e ) {
								if( isEnglish )
									result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Error in synchro min values: " + e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Fehler bei Synchro Min Werten: " + e.getMessage(), Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								throw new PPExecutionException();
							}
						}
					}
					// parsing sync max values
					sArg = getArg( "SYNCHRO_MAX_VALUES" + i );
					if( sArg != null) {
						useMax[i-1]=true;
						myTokenizer = new StringTokenizer( sArg, ";" );
						syncMaxs[i-1] = new int[numSyncFreqs[i-1]];
						for( j = 0; myTokenizer.hasMoreTokens(); j++ ) {
							String token = myTokenizer.nextToken();
							try {
								int newInt = Integer.parseInt( token );
								if( newInt < 0 || newInt > 63 ) {
									if( isEnglish )
										throw new PPExecutionException( "Values at SYNCHRO_MAX_VALUES" + i + " out of range [0;63]" );
									else
										throw new PPExecutionException( "Werte bei SYNCHRO_MAX_VALUES" + i + "ausserhalb des Bereichs [0;63]" );
								}
								else syncMaxs[i-1][j] = newInt;
								// check max > min
								if (syncMaxs[i-1][j]<syncMins[i-1][j]) {
									if (isEnglish) throw new PPExecutionException( "Error Parameter SYNCHRO_MIN_VALUES" + i + " Number "+j+" greater than SYNCHRO_MAX_VALUES" + i);
									else throw new PPExecutionException( "Fehler Parameter SYNCHRO_MIN_VALUES" + i + " Nummer "+j+" gr��er als SYNCHRO_MAX_VALUES" + i);
								}
							} catch( Exception e ) {
								if( isEnglish )
									result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Error in synchro max values: " + e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Fehler bei Synchro Max Werten: " + e.getMessage(), Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								throw new PPExecutionException();
							}
						}
					}
					else useMax[i-1]=false;
					
//########################################## Parsing noise arguments#################################################################################			
					
					if(debug) System.out.println("Start parsing noise arguments");
					
					
					if (useSyncNoise) {

				        if (maxIndex_noise_sync[i-1]>0) {
				        	// initializing subarrays
				        	syncNoiseName[i-1]=new String[maxIndex_noise_sync[i-1]];			
							syncnumNoiseFreqs[i-1]=new int[maxIndex_noise_sync[i-1]];							
							syncIndexNoiseFreqs[i-1]=new int[maxIndex_noise_sync[i-1]][];						
							syncNoiseMaxs[i-1]=new int[maxIndex_noise_sync[i-1]][];				
							syncNoiseMins[i-1]=new int [maxIndex_noise_sync[i-1]][];					
							syncNoiseMinOccurr[i-1] = new int[maxIndex_noise_sync[i-1]];

							for (j=1; j<= maxIndex_noise_sync[i-1] ;j++) {
								//noise names
								sArg = getArg("SYNCHRO_NOISE_NAME" + i + "_" + j);
								if (sArg == null && useProfiles) {
									if (profile_speakers[i-1].equalsIgnoreCase("TWEETER")) {
										if (j==1) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_NOISE1_NAME);
										if (sArg==null) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_NOISE1_NAME_ALT);
										if (j==2) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_NOISE2_NAME);
										if (sArg==null) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_NOISE2_NAME_ALT);
									}
									if (profile_speakers[i-1].equalsIgnoreCase("MIDRANGE")) sArg = this.getPr�fling().getAttribut(PROFILE_MIDRANGE_NOISE_NAME);
									if (sArg==null) sArg = this.getPr�fling().getAttribut(PROFILE_MIDRANGE_NOISE_NAME_ALT);
									if (profile_speakers[i-1].equalsIgnoreCase("WOOFER")) sArg = this.getPr�fling().getAttribut(PROFILE_WOOFER_NOISE_NAME);
									if (sArg==null) sArg = this.getPr�fling().getAttribut(PROFILE_WOOFER_NOISE_NAME_ALT);
								}
								if(debug)System.out.println("get synchro noise name  " +  sArg);
								try{
								    syncNoiseName[i-1][j-1] = sArg;
								}
								catch(Exception e){ e.printStackTrace();}
								sArg = getArg("SYNCHRO_NOISE_FREQUENCIES" + i + "_" + j);
								if (sArg == null && useProfiles) {
									if (profile_speakers[i-1].equalsIgnoreCase("TWEETER")) {
										if (j==1) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_NOISE1_FREQ);
										if (sArg==null) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_NOISE1_FREQ_ALT);
										if (j==2) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_NOISE2_FREQ);
										if (sArg==null) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_NOISE2_FREQ_ALT);
									}
									if (profile_speakers[i-1].equalsIgnoreCase("MIDRANGE")) sArg = this.getPr�fling().getAttribut(PROFILE_MIDRANGE_NOISE_FREQ);
									if (sArg==null) sArg = this.getPr�fling().getAttribut(PROFILE_MIDRANGE_NOISE_FREQ_ALT);
									if (profile_speakers[i-1].equalsIgnoreCase("WOOFER")) sArg = this.getPr�fling().getAttribut(PROFILE_WOOFER_NOISE_FREQ);
									if (sArg==null) sArg = this.getPr�fling().getAttribut(PROFILE_WOOFER_NOISE_FREQ_ALT);
								}
								myTokenizer = new StringTokenizer( sArg, ";" );
								syncnumNoiseFreqs[i-1][j-1] = myTokenizer.countTokens();
								syncIndexNoiseFreqs[i-1][j-1] = new int[syncnumNoiseFreqs[i-1][j-1]];
								for( int j2 = 0; myTokenizer.hasMoreTokens(); j2++ ) {
									
									String token = myTokenizer.nextToken();
									try {
										if (debug) System.out.println("Parsing Noise_Freq ["+(i-1)+"]["+(j)+"]["+(j2)+"]: "+token);
										
										
										float newfloat = Float.parseFloat( token );
										
										
										syncIndexNoiseFreqs[i-1][j-1][j2] = Arrays.binarySearch( DN6000Constants.FREQUENCY_FRAME, newfloat);
										//if (debug) System.out.println("Found frequency at index:" + syncIndexNoiseFreqs[i-1][j-1][j2]);
										//if(debug)System.out.println("token " + token + " newFloat " + newfloat + " index at syncIndexNoiseFreqs[" + (i-1) + "][" + (j-1)+ "][" + j2 + " : " +  syncIndexNoiseFreqs[i-1][j-1][j2]);
										
										if( syncIndexNoiseFreqs[i-1][j-1][j2] < 0 ) {
										
											if( isEnglish )
												throw new PPExecutionException( "Frequency at SYNCHRO_NOISE_FREQUENCIES" +i+"_"+j+"_"+j2 + " do not match with anaylser frequency list" );
											else
												throw new PPExecutionException( "Frequenz bei SYNCHRO_NOISE_FREQUENCIES" +i+"_"+j+"_"+j2 + " nicht gefunden in Analyser Frequenzliste" );
										}
									} catch( Exception e ) { 
										if( isEnglish )
											result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Error in synchro Noise frequencies ("+i+"_"+j+"): " + e.getMessage(), Ergebnis.FT_NIO_SYS );
										else
											result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Fehler bei Synchro St�rger�usch Frequenzen ("+i+"_"+j+"): " + e.getMessage(), Ergebnis.FT_NIO_SYS );
										ergListe.add( result );
										throw new PPExecutionException();
									}
								}
								// min noise values
								if(debug)System.out.println("get min noise values");
								sArg = getArg( "SYNCHRO_NOISE_MIN_VALUES" + i+"_"+j );
								if (sArg == null && useProfiles) {
									if (profile_speakers[i-1].equalsIgnoreCase("TWEETER")) {
										if (j==1) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_NOISE1_MIN);
										if (sArg==null) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_NOISE1_MIN_ALT);
										if (j==2) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_NOISE2_MIN);
										if (sArg==null) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_NOISE2_MIN_ALT);
									}
									if (profile_speakers[i-1].equalsIgnoreCase("MIDRANGE")) sArg = this.getPr�fling().getAttribut(PROFILE_MIDRANGE_NOISE_MIN);
									if (sArg==null) sArg = this.getPr�fling().getAttribut(PROFILE_MIDRANGE_NOISE_MIN_ALT);
									if (profile_speakers[i-1].equalsIgnoreCase("WOOFER")) sArg = this.getPr�fling().getAttribut(PROFILE_WOOFER_NOISE_MIN);
									if (sArg==null) sArg = this.getPr�fling().getAttribut(PROFILE_WOOFER_NOISE_MIN_ALT);
								}
								myTokenizer = new StringTokenizer( sArg, ";" );
								syncNoiseMins[i-1][j-1] = new int[syncnumNoiseFreqs[i-1][j-1]];
								for( int j2 = 0; myTokenizer.hasMoreTokens(); j2++ ) {
									String token = myTokenizer.nextToken();
						
									try {
										
										int newInt = Integer.parseInt( token );
										if( newInt < 0 || newInt > 63 ) {
											
											if( isEnglish )
												throw new PPExecutionException( "Values at SYNCHRO_NOISE_MIN_VALUES" + i+"_"+j + " out of range [0;63]" );
											else
												throw new PPExecutionException( "Werte bei SYNCHRO_NOISE_MIN_VALUES" + i+"_"+j + "ausserhalb des Bereichs [0;63]" );
										}
										else{ 
											
											try{syncNoiseMins[i-1][j-1][j2] = newInt;} 
											catch(Exception e){e.printStackTrace();}
										}
										//if(debug) System.out.println("synchro noise min values : " + syncNoiseMins[j-1][j2]);}
										
										//if(debug) System.out.println("noise mins: " +  (syncNoiseMins[i-1][j-1][j2]));
									} catch( Exception e ) {
										if( isEnglish ){
											
											
											result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Error in synchro noise min values: " + e.getMessage(), Ergebnis.FT_NIO_SYS );
										}
										else{
					 						
											result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Fehler bei Synchro St�rger�usch Min Werten: " + e.getMessage(), Ergebnis.FT_NIO_SYS );
										}ergListe.add( result );
										throw new PPExecutionException();
									}
									
								}
							
								// compare noise min and noise max
								sArg = getArg( "SYNCHRO_NOISE_MAX_VALUES" + i+"_"+j );
								// max noise values from attribute
								if(debug)System.out.println("get max values  ");
								if (sArg != null) {
									myTokenizer = new StringTokenizer( sArg, ";" );
									//if (debug) {System.out.println("SYNCHRO_NOISE_MAX_VALUES" + i+"_"+j  + sArg );}
									syncNoiseMaxs[i-1][j-1] = new int[syncnumNoiseFreqs[i-1][j-1]];
									//syncNoiseMaxs = new int[sync_number][maxIndex_noise_sync[i-1]][ syncnumNoiseFreqs[i-1][j-1]];
									for( int j2 = 0; myTokenizer.hasMoreTokens(); j2++ ) {
										String token = myTokenizer.nextToken();
										try {
											int newInt = Integer.parseInt( token );
											if(debug)System.out.println("int: "+ newInt);
											if( newInt < 0 || newInt > 63 ) {
												if(debug)System.out.println("parameter im Frontend �berpr�fen");
												if( isEnglish )
													throw new PPExecutionException( "Values at SYNCHRO_NOISE_MAX_VALUES" + i+"_"+j + " out of range [0;63]" );
												else
													throw new PPExecutionException( "Werte bei SYNCHRO_NOISE_MAX_VALUES" + i+"_"+j + "ausserhalb des Bereichs [0;63]" );
											}
											else{
												syncNoiseMaxs[i-1][j-1][j2] = newInt;
												if(debug) System.out.println("SYNCHRO_NOISE_MAX[" + i + "]["+ j + "]-[" + j2+ "] ist :" + syncNoiseMaxs[i-1][j-1][j2]);
												if (syncNoiseMaxs[i-1][j-1][j2]<syncNoiseMins[i-1][j-1][j2]) {
													if (isEnglish) throw new PPExecutionException( "Error Parameter SYNCHRO_NOISE_MIN" + i + "_"+ j + " Number "+j2+" greater than SYNCHRO_NOISE_MAX" + i + "_"+ j);
													else throw new PPExecutionException( "Fehler Parameter SYNCHRO_NOISE_MIN" + i + "_"+ j + " Nummer "+j2+" gr��er als SYNCHRO_NOISE_MAX" + i + "_"+ j);
												}
											}
											
										} catch( Exception e ) {
											
											if( isEnglish )
												result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Error in synchro noise max values: " + e.getMessage(), Ergebnis.FT_NIO_SYS );
											else
												result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "Fehler bei Synchro St�rger�usch Max Werten: " + e.getMessage(), Ergebnis.FT_NIO_SYS );
											ergListe.add( result );
											throw new PPExecutionException();
										}
									}
								}
								else {
									// max noise values max value set to default value 63
									syncNoiseMaxs[i-1][j-1] = new int[syncnumNoiseFreqs[i-1][j-1]];
									for( int j2 = 0; j2<syncnumNoiseFreqs[i-1][j-1]; j2++ ) {
										int newInt = 63;	
										try{
											syncNoiseMaxs[i-1][j-1][j2] = newInt;
											//if(debug) System.out.println("SYNCHRO_NOISE_MAX[] ist :" + syncNoiseMaxs[i-1][j-1][j2]);
										}
										catch(Exception e){e.printStackTrace();}
									}
								}
								
								//if(debug)System.out.println("get min occurr values  ");
								sArg = getArg( "SYNCHRO_NOISE_DROPOUT" + i+"_"+j );
								if (sArg == null && useProfiles) {
									if (profile_speakers[i-1].equalsIgnoreCase("TWEETER")) {
										if (j==1) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_NOISE1_DROP);
										if (j==2) sArg = this.getPr�fling().getAttribut(PROFILE_TWEETER_NOISE2_DROP);
									}
									if (profile_speakers[i-1].equalsIgnoreCase("MIDRANGE")) sArg = this.getPr�fling().getAttribut(PROFILE_MIDRANGE_NOISE_DROP);
									if (profile_speakers[i-1].equalsIgnoreCase("WOOFER")) sArg = this.getPr�fling().getAttribut(PROFILE_WOOFER_NOISE_DROP);
								}
								//if (debug) {System.out.println("SYNCHRO_NOISE_DROPOUT" + i+"_"+j  + getArg( "SYNCHRO_NOISE_DROPOUT" + i+"_"+j) );}
								if (sArg!= null && sArg.equalsIgnoreCase( "" )== false && sArg.equalsIgnoreCase( "null" )== false) {
									syncNoiseMinOccurr[i-1][j-1] = syncnumNoiseFreqs[i-1][j-1] - Integer.parseInt(sArg);
									//if(debug) System.out.println("syncNoiseMinOccurr[" + (i-1)+ "]["+ (j-1)+ "]: " + syncNoiseMinOccurr[i-1][j-1] );
									
								}
							
							}
							
							
						} else {
							//if(debug)System.out.println("maxIndexNoise not big enough ");
							syncNoiseName[i-1]=new String[1];
							syncNoiseName[i-1][0] = "null";
						}
					}
					
				}
				
				// getting the rest of the non array values
				
				sArg = getArg( "TOLERANZINDEX" );
				if( sArg != null ) {
					int tempInt = Integer.parseInt( sArg );
					if( tempInt >= 1 && tempInt <= ToleranceDirectories.length )
						ToleranceDirectoryIndex = tempInt;
				}
				sArg = getArg( "ERGEBNISINDEX" );
				if( sArg != null ) {
					int tempInt = Integer.parseInt( sArg );
					if( tempInt >= 1 && tempInt <= ResultDirectories.length )
						ResultDirectoryIndex = tempInt;
					if( tempInt == -1 )
						useTrace = false;
				}

				sArg = getArg( "HWT" );
				if( sArg != null ) {
					hwt = sArg.toString();
				}

				sBaureihe = getArg( "BAUREIHE" );
				if( sBaureihe == null || sBaureihe.equalsIgnoreCase( "" ) || sBaureihe.equalsIgnoreCase( "null" ) ) {
					sBaureihe = getPr�fling().getAuftrag().getBaureihe(); //Baureihe
				}

				sKarosserie = getArg( "KAROSSERIE" );
				if( sKarosserie == null || sKarosserie.equalsIgnoreCase( "" ) || sKarosserie.equalsIgnoreCase( "null" ) ) {
					sKarosserie = getPr�fling().getAuftrag().getKarosserieForm();
				}

				sArg = getArg( "ERRORTEXT_ZERO_MEASUREMENT" );
				if (sArg == null && useProfiles) {
					if (profile_first_zero_line) sArg = this.getPr�fling().getAttribut(PROFILE_ZERO_FIRST);
					else sArg = this.getPr�fling().getAttribut(PROFILE_ZERO_NEXT);
				}
				if( sArg != null && !sArg.equalsIgnoreCase("null")&& !sArg.equalsIgnoreCase("ignore")) {
					useZeroError = true;
					zeroError = sArg.toString();
					if (debug) System.out.println("Zero Measurement: activated with text: "+zeroError);
				}
				
				if (useZeroError) {
					sArg = getArg( "CONFIRM_ZERO_MEASUREMENT_TIMEOUT" );
					if (sArg == null && useProfiles) sArg = this.getPr�fling().getAttribut(PROFILE_ZERO_TIMEOUT);
					if( sArg != null && !sArg.equalsIgnoreCase("null")&& !sArg.equalsIgnoreCase("ignore")) {
						int tempInt = Integer.parseInt( sArg );
						if( tempInt == 0  || (tempInt >= 10 && tempInt <= 30000)) {
							useZeroConfirmWindow = true;
							zeroConfirmWindowTimeOut = tempInt;
							if (debug) System.out.println("Zero Measurement: confirm window activated with timeout: "+zeroConfirmWindowTimeOut);
						}
						else if( isEnglish )
							throw new PPExecutionException( "Checkargs Error: CONFIRM_ZERO_MEASUREMENT_TIMEOUT out of bounds" );
						else
							throw new PPExecutionException( "ParameterFehler: CONFIRM_ZERO_MEASUREMENT_TIMEOUT ausserhalb des zul�ssigen Bereichs" );
					}
					if (useZeroConfirmWindow) {
						sArg = getArg( "CONFIRM_ZERO_MEASUREMENT_TEXT" );
						if (sArg == null && useProfiles) {
							if (profile_first_zero_line) sArg = this.getPr�fling().getAttribut(PROFILE_ZERO_FIRST);
							else sArg = this.getPr�fling().getAttribut(PROFILE_ZERO_NEXT);
						}
						if( sArg != null ) {
							zeroConfirmWindowText = sArg.toString();
							if (debug) System.out.println("Zero Measurement: confirm window activated with text: "+zeroConfirmWindowText);
						}
						sArg = getArg( "CONFIRM_ZERO_MEASUREMENT_USE_SECOND_MEASUREMENT" );
						if( sArg != null ) {
							zeroConfirmSecondTest = sArg.equalsIgnoreCase( "TRUE" );
						}
					}
				}
				sArg = getArg( "SYNCHRO_SGBD" );
				if (sArg == null && useProfiles) sArg = this.getPr�fling().getAttribut(PROFILE_SGBD);
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null" )))) {
					if (sArg.indexOf( '@' ) == -1) {
						syncSgbd = sArg;
					}
					else {
						int index = sArg.indexOf( '@' );
						try {
							syncSgbd = getPr�fling().getAuftrag().getPr�fling( sArg.substring( index+1 ) ).getAttribut( sArg.substring( 0, index ));
						} catch (Exception e) {
							if( isEnglish )
								throw new PPExecutionException( "ERROR getting SYNCHRO_SGBD with @-command!" );
							else
								throw new PPExecutionException( "Fehler beim Holen von SYNCHRO_SGBD mit dem @-Kommando" );
						}
					}
				} else syncSgbd = getPr�fling().getAttribut( "SGBD" );

				sArg = getArg( "SYNCHRO_JOBNAME" );
				if (sArg == null && useProfiles && useProfileUDS) sArg = PROFILE_UDS_JOB;
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null" )))) {
					if (sArg.indexOf( '@' ) == -1) {
						syncJob = sArg;
					}
					else {
						int index = sArg.indexOf( '@' );
						try {
							syncJob = getPr�fling().getAuftrag().getPr�fling( sArg.substring( index+1 ) ).getAttribut( sArg.substring( 0, index ));
						} catch (Exception e) {
							if( isEnglish )
								throw new PPExecutionException( "ERROR getting SYNCHRO_JOBNAME with @-command!" );
							else
								throw new PPExecutionException( "Fehler beim Holen von SYNCHRO_JOBNAME mit dem @-Kommando" );
						}
					}
				} else syncJob="STEUERN_SINUSGENERATOR_EIN";
				sArg = getArg( "SYNCHRO_JOBPAR" );
				if (sArg == null && useProfiles && useProfileUDS) sArg = PROFILE_UDS_JOBPAR;
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null" )))) {
					if (sArg.indexOf( '@' ) == -1) {
						syncJobPar = sArg;
						if (syncJobPar.indexOf( '%' ) != -1) {
							syncJobParUseSpecialCommand = true;
							syncJobParIndexVolume=syncJobPar.indexOf( "%VOLUME%" );
							syncJobParIndexFreq=syncJobPar.indexOf( "%FREQUENCY%" );
							syncJobParIndexChannel=syncJobPar.indexOf( "%CHANNEL%" );
						}
					}
					else {
						int index = sArg.indexOf( '@' );
						try {
							syncJobPar = getPr�fling().getAuftrag().getPr�fling( sArg.substring( index+1 ) ).getAttribut( sArg.substring( 0, index ));
						} catch (Exception e) {
							if( isEnglish )
								throw new PPExecutionException( "ERROR getting SYNCHRO_JOBPAR with @-command!" );
							else
								throw new PPExecutionException( "Fehler beim Holen von SYNCHRO_JOBPAR mit dem @-Kommando" );
						}
					}
				} else syncJobPar="";
				sArg = getArg( "SYNCHRO_CHANNEL" );
				if (sArg == null && useProfiles) sArg = profile_channel;
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null" )))) {
					int tempInt = Integer.parseInt( sArg );
					if( tempInt < 0 || tempInt > 4096 ) {
						if( isEnglish )
							throw new PPExecutionException( "Volume at SYNCHRO_CHANNEL: out of range [0;4096]" );
						else
							throw new PPExecutionException( "Lautst�rke bei SYNCHRO_CHANNEL: ausserhalb des Bereichs [0;4096]" );
					}
					else syncChannel = tempInt;
				}
				sArg = getArg( "SYNCHRO_INITPAUSE" );
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null" )))) {
					int tempInt = Integer.parseInt( sArg );
					if( tempInt < 0 || tempInt > 1000 ) {
						if( isEnglish )
							throw new PPExecutionException( "SYNCHRO_INITPAUSE: out of range [0;1000]" );
						else
							throw new PPExecutionException( "SYNCHRO_INITPAUSE: ausserhalb des Bereichs [0;1000]" );
					}
					else syncInitPause = tempInt;
				}
				sArg = getArg( "SYNCHRO_INIT_SINGLE_FREQUENCY_PAUSE" );
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null" )))) {
					int tempInt = Integer.parseInt( sArg );
					if( tempInt < 0 || tempInt > 300 ) {
						if( isEnglish )
							throw new PPExecutionException( "SYNCHRO_INIT_SINGLE_FREQUENCY_PAUSE: out of range [0;300]" );
						else
							throw new PPExecutionException( "SYNCHRO_INIT_SINGLE_FREQUENCY_PAUSE: ausserhalb des Bereichs [0;300]" );
					}
					else syncSingleFrequencyPause = tempInt;
				}
				sArg = getArg( "SYNCHRO_SINGLE_TRACE" );
				if (sArg == null && useProfiles) sArg = this.getPr�fling().getAttribut(PROFILE_SINGLE_TRACE);
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null" )))) {
					syncSingleTrace = sArg.equalsIgnoreCase( "TRUE" );
					if (sArg.equalsIgnoreCase( "MINMAX" )) {
						useMinMaxTrace = true;
					} 
				}
				sArg = getArg( "SYNCHRO_LOCAL_TRACE" );
				if (sArg == null && useProfiles) sArg = this.getPr�fling().getAttribut(PROFILE_LOCAL_TRACE);
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null" )))) {
					syncLocalTrace = sArg.equalsIgnoreCase( "TRUE" );
				}
				// if sync freq is repeated once
				sArg = getArg("REPEAT_SINGLE_FREQ_ONCE_ON_ERROR");
				if (sArg == null && useProfiles) sArg = this.getPr�fling().getAttribut(PROFILE_REPEAT);
				if (sArg!= null && sArg.equalsIgnoreCase( "" )== false && sArg.equalsIgnoreCase( "null" )== false) {
					repeatOnceOnError = sArg.equalsIgnoreCase("true");
				}
				// check if noise is ignored
				sArg = getArg("SYNCHRO_IGNORE_NOISE");
				if (sArg == null && useProfiles) sArg = this.getPr�fling().getAttribut(PROFILE_NOISE_IGNORE);
				if (sArg!= null && sArg.equalsIgnoreCase( "" )== false && sArg.equalsIgnoreCase( "null" )== false) {
					ignore_noise = sArg.equalsIgnoreCase("true");
					if(debug) System.out.println("IGNORE NOISE : " + ignore_noise);
				}
				
				sArg = getArg( "KANAL" );
				if( sArg != null) {
					channel = sArg;
				}
				sArg = getArg( "LSTYP" );
				if (sArg == null && useProfiles) sArg = this.getPr�fling().getAttribut(PROFILE_LSTYP);
				if( sArg != null) {
					lstype = sArg;
				} else lstype="LSTYP NOT SPECIFIED";

				sArg = getArg( "RADIOTYP" );
				if (sArg == null && useProfiles) sArg = this.getPr�fling().getAttribut(PROFILE_RADIOTYP);
				if( sArg != null) {
					radio = sArg;
				} else radio="RADIOTYP NOT SPECIFIED";
			}
			catch( Exception e ) {
				if( e.getMessage() != null ){
					
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				}else{
					
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				}ergListe.add( result );
				throw new PPExecutionException();
			}

// -------------------------------------------------------------- get devices ------------------------------------------

			if (useSyncMeasurement) {
				try {
					ed = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();// new
				} catch( Exception e1 ) {
					if( isEnglish ) {
						throw new DeviceIOException( "error getting EDIABAS device: " + e1.getMessage() );
					} else {
						throw new DeviceIOException( "Fehler beim Holen des Device EDIABAS: " + e1.getMessage() );
					}
				}
			}

			//AudioAnalyser holen
			try {
				if( isEnglish )
					myErrorTxt = "Error getting device AudioAnalyser";
				else
					myErrorTxt = "Fehler beim Initialisieren des Devices AudioAnalyser";
				myAudio = getPr�flingLaufzeitUmgebung().getDeviceManager().getAudioAnalyser();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", myErrorTxt, "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			
			//AudioAnalyser parametrieren
			try {
				if( isEnglish )
					myErrorTxt = "Error reseting device AudioAnalyser";
				else
					myErrorTxt = "Fehler beim Zur�cksetzen des Devices AudioAnalyser";
				myAudio.reset();

				if( isEnglish )
					myErrorTxt = "Error setting recording time";
				else
					myErrorTxt = "Fehler beim Festlegen der Aufnahmezeit";
				myAudio.setRecordDuration( recordDuration );

				// setting tolerances for non-synchro-measurement
				if (!useSyncMeasurement && !useLimitedSweep) {
					if( isEnglish )
						myErrorTxt = "Error getting reference data";
					else
						myErrorTxt = "Fehler beim Initalisieren der Referenzfdaten";
					refData = new ReferenceDataDescriptor( getPr�fling().getAuftrag().getFahrgestellnummer7(), //7-stellige
																											   // FGST
							this.sBaureihe, //Baureihe
							this.sKarosserie, //Karosserie
							lstype, //Lautsprechertyp
							radio, //Radiotyp
							channel, //Kanal
							new AudioFileDescriptor( "sollwert.tol", this.ResultDirectories[ResultDirectoryIndex], this.ToleranceDirectories[ToleranceDirectoryIndex] ) );

					if( isEnglish )
						myErrorTxt = "Error setting reference data";
					else
						myErrorTxt = "Fehler beim Festlegen der Referenzdaten";
					myAudio.setReferenceData( refData );
				}
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", myErrorTxt, "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				// errors ignored to execute measurement
				//throw new PPExecutionException();
				status = STATUS_EXECUTION_ERROR;
				toleranceError=true;
			}

// -------------------------------------------------------------- perform normal non-sync measurement ------------------------------------------

			if (!useSyncMeasurement) {
				//if no tolerance is present write APDM buffer nonetheless
				if (!useLimitedSweep && toleranceError) {
					if (debug) System.out.println("Tolerance nok entering normal measurement");
					try {
						myAudio.calculateLMaxSpek();
						myAudio.writeResultBuffer( resultBuffer, channel );
					} catch( Exception e ) {
						if( e instanceof DeviceIOException )
							result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", "DeviceIOException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
						else if( e instanceof DeviceParameterException )
							result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", "DeviceParameterException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
						else
							result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", myErrorTxt, "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						throw new PPExecutionException();
					}

				/*	//Ergebnisdatenblock in Ergebnis speichern
					if (isEnglish) result = new Ergebnis( channel, "AudioAnalyser", "", "", "", "DATA_BLOCK", resultBuffer.toString(), "", "", "0", "", "", "", "", "BodyType: "+sKarosserie+", Radio: "+radio+", Loudspeaker-system: "+lstype, Ergebnis.FT_IO );
					else result = new Ergebnis( channel, "AudioAnalyser", "", "", "", "DATA_BLOCK", resultBuffer.toString(), "", "", "0", "", "", "", "", "Karosserie: "+sKarosserie+", Radio: "+radio+", Lautsprechersystem: "+lstype, Ergebnis.FT_IO );
					ergListe.add( result );*/
				}
				//normal measurement
				else {

					// Messung und Berechnung
					if (debug) System.out.println("Tolerance ok entering normal measurement");
					try {
						// sampleSepk for limited sweep
						if (useLimitedSweep) {
							if (debug) System.out.println("Entering record spec for : ");
							myAudio.sampleSpek();
						}
						//MaxSpek
						if( isEnglish )
							myErrorTxt = "Error calculating maximum spectrum";
						else
							myErrorTxt = "Fehler beim Berechnen des Maximumspektrums";
						myAudio.calculateLMaxSpek();

						//rattle spek
						if( isEnglish )
							myErrorTxt = "Error calculating rattle spectrum";
						else
							myErrorTxt = "Fehler beim Berechnen des Klirrspektrums";
						if (!useLimitedSweep) myAudio.calculateLRattleSpek();

						if( isEnglish )
							myErrorTxt = "Error writing APDM result buffer";
						else
							myErrorTxt = "Fehler beim Schreiben des APDM Ergebnis Puffers";
						if (!useLimitedSweep) myAudio.writeResultBuffer( resultBuffer, channel );
					} catch( Exception e ) {
						if( e instanceof DeviceIOException )
							result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", "DeviceIOException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
						else if( e instanceof DeviceParameterException )
							result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", "DeviceParameterException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
						else
							result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", myErrorTxt, "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						throw new PPExecutionException();
					}
					

					//Ergebnisdatenblock in Ergebnis speichern
					if (!useLimitedSweep) {
					/*	if (isEnglish) result = new Ergebnis( channel, "AudioAnalyser", "", "", "", "DATA_BLOCK", resultBuffer.toString(), "", "", "0", "", "", "", "", "BodyType: "+sKarosserie+", Radio: "+radio+", Loudspeaker-system: "+lstype, Ergebnis.FT_IO );
						else result = new Ergebnis( channel, "AudioAnalyser", "", "", "", "DATA_BLOCK", resultBuffer.toString(), "", "", "0", "", "", "", "", "Karosserie: "+sKarosserie+", Radio: "+radio+", Lautsprechersystem: "+lstype, Ergebnis.FT_IO );
						//result = new Ergebnis( channel, "AudioAnalyser", sKarosserie, radio, lstype, "DATA_BLOCK", resultBuffer.toString(), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );*/
						if (debug) System.out.println("got spectrum and wrote spectrum results");
					}
					// with limited sweep
					if (useLimitedSweep) {
						sweepMaxSpecValues=new int[DN6000Constants.FREQUENCY_FRAME.length];
						sweepMaxSpecValues = myAudio.getMaxSpek();
						//create and write APDM result
						createAPDMResultBuffer( resultBuffer, sweepMaxSpecValues, channel );
					/*	if (isEnglish) result = new Ergebnis( channel, "AudioAnalyser", "", "", "", "DATA_BLOCK", resultBuffer.toString(), "", "", "0", "", "", "", "", "BodyType: "+sKarosserie+", Radio: "+radio+", Loudspeaker-system: "+lstype, Ergebnis.FT_IO );
						else result = new Ergebnis( channel, "AudioAnalyser", "", "", "", "DATA_BLOCK", resultBuffer.toString(), "", "", "0", "", "", "", "", "Karosserie: "+sKarosserie+", Radio: "+radio+", Lautsprechersystem: "+lstype, Ergebnis.FT_IO );
						//result = new Ergebnis( channel, "AudioAnalyser", sKarosserie, radio, lstype, "DATA_BLOCK", resultBuffer.toString(), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result ); */
						if (debug) System.out.println("got spectrum and wrote spectrum results");

						// check for zero measurement
						isZero = true;
						for( int numFreq = 0; numFreq < DN6000Constants.FREQUENCY_FRAME.length; numFreq++ ) {
							if(useZeroError && isZero && (sweepMaxSpecValues[numFreq] > 0)) {
								isZero = false;
								numFreq = DN6000Constants.FREQUENCY_FRAME.length; // end if loop
							}
						}
						// check for zero measurement
						if (isZero && useZeroError) {
							if (debug) System.out.println("Zero Measurement: zero line detected");
							result = new Ergebnis( "ZERO LINE FIRST MEASUREMENT", "AudioAnalyser", "", "", "", "", "", "0", "", "", "", "", "", ""+zeroError, "", Ergebnis.FT_NIO );
							ergListe.add( result );
							if (useZeroConfirmWindow) {
								// write zeroline result
								if (debug) System.out.println("Zero Measurement: zero line detected and using confirm window");
								// display window
								try {
									ud = getPr�flingLaufzeitUmgebung().getUserDialog();
									// initially set cancel button
									// ud.setsetAllowCancel( true );
								} catch( Exception e ) {
									if( isEnglish )
										myErrorTxt = "Error getting User Dialogue for Confirm Window";
									else
										myErrorTxt = "Fehler beim Holen Userdialogs des Best�tigungsfensters";
									result = new Ergebnis( "UserDialog", "UserDialog", "", "", "", "", "", "0", "", "", "", "", "", ""+myErrorTxt, "", Ergebnis.FT_NIO );
									ergListe.add( result );
									status = STATUS_EXECUTION_ERROR;
								}
								
								if (isEnglish) {
									if (useZeroAlert) ud.requestAlertMessage("Alert",zeroConfirmWindowText,zeroConfirmWindowTimeOut);
									else ud.requestUserMessage("Alert",zeroConfirmWindowText,zeroConfirmWindowTimeOut);
								}
								else {
									if (useZeroAlert) ud.requestAlertMessage("Achtung",zeroConfirmWindowText,zeroConfirmWindowTimeOut);
									else ud.requestUserMessage("Achtung",zeroConfirmWindowText,zeroConfirmWindowTimeOut);
								}
								if (debug) System.out.println("Zero Measurement: zero line detected,using confirm window and record second time");
							}
							status = STATUS_EXECUTION_ERROR;
						}
						// perform evaluation when no zero line detected
						else {
							if (debug) System.out.println("Evaluating limited sweep");
							// evaluate min and create result
							for( i = 0; i < maxIndex_max; i++ ) {
								numOkFreqs = 0;
								for (j=0;j<numSweepFreqs[i];j++) {
									if (sweepMaxSpecValues[indexSweepTolFreqs[i][j]]<sweepMins[i][j]){
										if (debug) System.out.println("lim sweep too low lev");
										if (isEnglish) result = new Ergebnis( name_max[i]+(j+1), "AudioAnalyser", "", "", "", "Measurement value [dB]", ""+sweepMaxSpecValues[indexSweepTolFreqs[i][j]], ""+sweepMins[i][j], "", "", "", "", "", PB.getString( "toleranzFehler3" ), "Audio measurement ("+name_max[i]+") at frequency: " + sweepTolFreqs[i][j], Ergebnis.FT_IGNORE);
										result = new Ergebnis( name_max[i]+(j+1), "AudioAnalyser", "", "", "", "Messwert [dB]", ""+sweepMaxSpecValues[indexSweepTolFreqs[i][j]], ""+sweepMins[i][j], "", "", "", "", "", PB.getString( "toleranzFehler3" ), "Audio Messung ("+name_max[i]+") bei Frequenz: " + sweepTolFreqs[i][j], Ergebnis.FT_IGNORE);
									}
									else {
										if (useSweepMax[i]) {
											if (sweepMaxSpecValues[indexSweepTolFreqs[i][j]]>sweepMaxs[i][j]){
												if (debug) System.out.println("lim sweep too high lev");
												if (isEnglish) result = new Ergebnis( name_max[i]+(j+1), "AudioAnalyser", "", "", "", "Measurement value [dB]", ""+sweepMaxSpecValues[indexSweepTolFreqs[i][j]], ""+sweepMins[i][j], ""+sweepMaxs[i][j], "", "", "", "", PB.getString( "toleranzFehler3" ), "Audio measurement ("+name_max[i]+") at frequency: " + sweepTolFreqs[i][j], Ergebnis.FT_IGNORE);
												result = new Ergebnis( name_max[i]+(j+1), "AudioAnalyser", "", "", "", "Messwert [dB]", ""+sweepMaxSpecValues[indexSweepTolFreqs[i][j]], ""+sweepMins[i][j], ""+sweepMaxs[i][j], "", "", "", "", PB.getString( "toleranzFehler3" ), "Audio Messung ("+name_max[i]+") bei Frequenz: " + sweepTolFreqs[i][j], Ergebnis.FT_IGNORE);
											}
											else {
												if (debug) System.out.println("lim sweep ok lev with max");
												if (isEnglish) result = new Ergebnis( name_max[i]+(j+1), "AudioAnalyser", "", "", "", "Measurement value [dB]", ""+sweepMaxSpecValues[indexSweepTolFreqs[i][j]], ""+sweepMins[i][j], ""+sweepMaxs[i][j], "", "", "", "", "", "Audio measurement ("+name_max[i]+") at frequency: " + sweepTolFreqs[i][j], Ergebnis.FT_IO );
												result = new Ergebnis( name_max[i]+(j+1), "AudioAnalyser", "", "", "", "Messwert [dB]", ""+sweepMaxSpecValues[indexSweepTolFreqs[i][j]], ""+sweepMins[i][j], ""+sweepMaxs[i][j], "", "", "", "", "", "Audio Messung ("+name_max[i]+") bei Frequenz: " + sweepTolFreqs[i][j], Ergebnis.FT_IO );
												numOkFreqs++;
												if(debug) System.out.println("currently " + numOkFreqs + " exact ");
											}
										}
										else {
											if (debug) System.out.println("lim sweep ok lev");
											if (isEnglish) result = new Ergebnis( name_max[i]+(j+1), "AudioAnalyser", "", "", "", "Measurement value [dB]", ""+sweepMaxSpecValues[indexSweepTolFreqs[i][j]], ""+sweepMins[i][j], "", "", "", "", "", "", "Audio measurement ("+name_max[i]+") at frequency: " + sweepTolFreqs[i][j], Ergebnis.FT_IO );
											result = new Ergebnis( name_max[i]+(j+1), "AudioAnalyser", "", "", "", "Messwert [dB]", ""+sweepMaxSpecValues[indexSweepTolFreqs[i][j]], ""+sweepMins[i][j], "", "", "", "", "", "", "Audio Messung ("+name_max[i]+") bei Frequenz: " + sweepTolFreqs[i][j], Ergebnis.FT_IO );
											numOkFreqs++;
											if(debug) System.out.println("currently " + numOkFreqs + " exact ");
										}
									}
									ergListe.add( result );	
								}
								if (debug) System.out.println("Creating limited sweepsum result");
								// create result summing up sweep block testing incl. dropouts
								if(numOkFreqs ==0 || numOkFreqs < (numSweepFreqs[i]-drop_max[i])){
									status = STATUS_EXECUTION_ERROR;
									if (isEnglish) result = new Ergebnis( name_max[i], "AudioAnalyser", "", "", "", "", "", "", ""+drop_max[i], "", "", "", ""+ (numSweepFreqs[i]- numOkFreqs) + " faulty Frequencies available", PB.getString( "toleranzFehler3" ), "Audio measurement ("+name_max[i]+")", Ergebnis.FT_NIO);
									else result = new Ergebnis( name_max[i], "AudioAnalyser", "", "" , "", "", "", "", ""+drop_max[i], "", "", "", ""+ (numSweepFreqs[i]- numOkFreqs) + " fehlerhafte Frequenzen vorhanden", PB.getString( "toleranzFehler3" ), "Audio Messung ("+name_max[i]+")", Ergebnis.FT_NIO);
									ergListe.add(result);
								}
								if (debug) System.out.println("Created limited sweepsum result");
							}
							if (debug) System.out.println("values sweep compared");
						}
					}
					// normal, without limited sweep
					else {
						if (debug) System.out.println("Entering non limited sweep");
						
						//Bewertung Maximum-Spektrum
						
						for( i = 0; i < maxIndex_max; i++ ) {
							try {
								if( isEnglish )
									myErrorTxt = "Error evaluating maximum spectrum: ";
								else
									myErrorTxt = "Fehler beim Auswerten des Maximumspektrums: ";
								boolean testPassed = true;
								spek = myAudio.getLMaxToleranzSpek();
								int[] maxValues=null;
								if(useZeroError) maxValues = spek.getRecordedSpektrum();
								float[] freq = spek.getFrequency();
								int numErrors = 0;
								int numFreqs = 0;
								if (i==0) isZero=true;
								for( int numFreq = 0; numFreq < freq.length; numFreq++ ) {
									if(useZeroError && isZero && (i==0) && (maxValues[numFreq] > 0)) isZero = false;
									if( (freq[numFreq] >= start_max[i]) && (freq[numFreq] <= stop_max[i])) {
										if (myAudio.evaluateLMaxSpek( freq[numFreq], freq[numFreq] ) == false)  {
											numErrors++;
											if (numErrors > drop_max[i]) testPassed=false;
										}
										numFreqs++;
									}
								}
								if (debug) System.out.println("evaluated max, zero detection:"+isZero+", zerouse:"+useZeroError);
								// zero line detection
								if (isZero && useZeroError && (i==0)) {
									if (debug) System.out.println("Zero Measurement: zero line detected");
									result = new Ergebnis( "ZERO LINE FIRST MEASUREMENT", "AudioAnalyser", "", "", "", "", "", "0", "", "", "", "", "", ""+zeroError, "Found only values of zero", Ergebnis.FT_NIO );
									ergListe.add( result );
									if (useZeroConfirmWindow) {
										// write zeroline result
										if (debug) System.out.println("Zero Measurement: zero line detected and using confirm window");
										// display window
										try {
											ud = getPr�flingLaufzeitUmgebung().getUserDialog();
											// initially set cancel button
											// ud.setsetAllowCancel( true );
										} catch( Exception e ) {
											if( isEnglish )
												myErrorTxt = "Error getting User Dialogue for Confirm Window";
											else
												myErrorTxt = "Fehler beim Holen Userdialogs des Best�tigungsfensters";
											result = new Ergebnis( "UserDialog", "UserDialog", "", "", "", "", "", "0", "", "", "", "", "", ""+myErrorTxt, "", Ergebnis.FT_NIO );
											ergListe.add( result );
											status = STATUS_EXECUTION_ERROR;
										}								
										if (isEnglish) {
											if (useZeroAlert) ud.requestAlertMessage("Alert",zeroConfirmWindowText,zeroConfirmWindowTimeOut);
											else ud.requestUserMessage("Alert",zeroConfirmWindowText,zeroConfirmWindowTimeOut);
										}
										else {
											if (useZeroAlert) ud.requestAlertMessage("Achtung",zeroConfirmWindowText,zeroConfirmWindowTimeOut);
											else ud.requestUserMessage("Achtung",zeroConfirmWindowText,zeroConfirmWindowTimeOut);
										}
										if (debug) System.out.println("Zero Measurement: zero line detected,using confirm window and record second time");
										// record and evaluate again
										if (zeroConfirmSecondTest) {
											myAudio.sampleSpek();
											if( isEnglish )
												myErrorTxt = "Error calculating maximum spectrum (after confirm window)";
											else
												myErrorTxt = "Fehler beim Berechnen des Maximumspektrums (nach Best�tigungsfenster)";
											myAudio.calculateLMaxSpek();
											if( isEnglish )
												myErrorTxt = "Error writing APDM result buffer (after confirm window)";
											else
												myErrorTxt = "Fehler beim Schreiben des APDM Ergebnis Puffers (nach Best�tigungsfenster)";
											myAudio.writeResultBuffer( resultBuffer, channel );
											// Ergebnisdatenblock in Ergebnis speichern
										/*	if (isEnglish) result = new Ergebnis( channel, "AudioAnalyser", "", "", "", "DATA_BLOCK", resultBuffer.toString(), "", "", "0", "", "", "", "", "BodyType: "+sKarosserie+", Radio: "+radio+", Loudspeaker-system: "+lstype, Ergebnis.FT_IO );
											else result = new Ergebnis( channel, "AudioAnalyser", "", "", "", "DATA_BLOCK", resultBuffer.toString(), "", "", "0", "", "", "", "", "Karosserie: "+sKarosserie+", Radio: "+radio+", Lautsprechersystem: "+lstype, Ergebnis.FT_IO );
											ergListe.add( result ); */
											//result = new Ergebnis( channel, "AudioAnalyser", sKarosserie, radio, lstype, "DATA_BLOCK", resultBuffer.toString(), "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
											if( isEnglish )
												myErrorTxt = "Error evaluating maximum spectrum: ";
											else
												myErrorTxt = "Fehler beim Auswerten des Maximumspektrums: ";
											testPassed = true;
											spek = myAudio.getLMaxToleranzSpek();
											maxValues = spek.getRecordedSpektrum();
											freq = spek.getFrequency();
											numErrors = 0;
											numFreqs = 0;
											isZero=true;
											for( int numFreq = 0; numFreq < freq.length; numFreq++ ) {
												if(useZeroError && isZero && (maxValues[numFreq] > 0)) isZero = false;
												if( (freq[numFreq] >= start_max[i]) && (freq[numFreq] <= stop_max[i])) {
													if (myAudio.evaluateLMaxSpek( freq[numFreq], freq[numFreq] ) == false)  {
														numErrors++;
														if (numErrors > drop_max[i]) testPassed=false;
													}
													numFreqs++;
												}
											}
											if (isZero) {
												result = new Ergebnis( "ZERO LINE SECOND MEASUREMENT", "AudioAnalyser", "", "", "", "", "", "0", "", "", "", "", "", ""+zeroError, "Found only values of zero", Ergebnis.FT_NIO );
												ergListe.add( result );
												status = STATUS_EXECUTION_ERROR;
											}
										}
										else status = STATUS_EXECUTION_ERROR;
									}
									else status = STATUS_EXECUTION_ERROR;
								}
								//myAudio.evaluateLMaxSpek( start_max[i], stop_max[i], drop_max[i] );
								String numberFreqs="";
								String numberErrors="";
								try {
									numberErrors=numberErrors+numErrors;
									numberFreqs=numberFreqs+numFreqs;
								} catch (Exception e){};
								if( testPassed ) {
									if( isEnglish )
										result = new Ergebnis( name_max[i], "AudioAnalyser", "", "", "", name_max[i]+" (faulty frequencies)", ""+numberErrors, "0", ""+drop_max[i], "", "", "", "", "", "Evaluated MAX Spectrum ("+numberFreqs+" frequencies): From frequency: " + start_max[i] + " to frequency: " + stop_max[i], Ergebnis.FT_IO );
									else
										result = new Ergebnis( name_max[i], "AudioAnalyser", "", "", "", name_max[i]+" (fehlerhafte Frequenzen)", ""+numberErrors, "0", ""+drop_max[i], "", "", "", "", "", "Bewertung MAX Spektrum ("+numberFreqs+" Frequenzen): Zwischen Frequenz: " + start_max[i] + " und Frequenz: " + stop_max[i], Ergebnis.FT_IO );
									ergListe.add( result );
								} else if (!(useZeroError && isZero)){
									if( isEnglish )
										result = new Ergebnis( name_max[i], "AudioAnalyser", "", "", "", name_max[i]+" (faulty frequencies)", ""+numberErrors, "0", ""+drop_max[i], "", "", "", "", PB.getString( "toleranzFehler3" ), "Evaluated MAX Spectrum ("+numberFreqs+" frequencies): From frequency: " + start_max[i] + " to frequency: " + stop_max[i], Ergebnis.FT_NIO );
									else
										result = new Ergebnis( name_max[i], "AudioAnalyser", "", "", "", name_max[i]+" (fehlerhafte Frequenzen)", ""+numberErrors, "0", ""+drop_max[i], "", "", "", "", PB.getString( "toleranzFehler3" ), "Bewertung MAX Spektrum ("+numberFreqs+" Frequenzen): Zwischen Frequenz: " + start_max[i] + " und Frequenz: " + stop_max[i], Ergebnis.FT_NIO );
									status = STATUS_EXECUTION_ERROR;
									ergListe.add( result );
								}								
							} catch( Exception e ) {
								if( e instanceof DeviceIOException ) {
									result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", ""+name_max[i], "", "", "", "0", "", "", "", "DeviceIOException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
								} else if( e instanceof DeviceParameterException ) {
									result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", ""+name_max[i], "", "", "", "0", "", "", "", "DeviceParameterException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
								} else {
									result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", ""+name_max[i], "", "", "", "0", "", "", "", myErrorTxt, "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
								}
								ergListe.add( result );
								throw new PPExecutionException();
							}
						}
					}

					//Bewertung Minimum-Spektrum
					for( i = 0; i < maxIndex_min; i++ ) {
						try {
							if( isEnglish )
								myErrorTxt = "Error evaluating minimum spectrum: ";
							else
								myErrorTxt = "Fehler beim Auswerten des Minimumspektrums: ";
							boolean testPassed = true;
							spek = myAudio.getLMinToleranzSpek();
							float[] freq = spek.getFrequency();
							int numErrors = 0;
							int numFreqs = 0;
							for( int numFreq = 0; numFreq < freq.length; numFreq++ ) {
								if( (freq[numFreq] >= start_min[i]) && (freq[numFreq] <= stop_min[i])) {
									if (myAudio.evaluateLMinSpek( freq[numFreq], freq[numFreq] ) == false)  {
										numErrors++;
										if (numErrors > drop_min[i]) testPassed=false;
									}
									numFreqs++;
								}
							}
							//myAudio.evaluateLMinSpek( start_min[i], stop_min[i], fSamplesIgnore );
							String numberFreqs="";
							String numberErrors="";
							try {
								numberErrors=numberErrors+numErrors;
								numberFreqs=numberFreqs+numFreqs;
							} catch (Exception e){};
							if( testPassed ) {
								if( isEnglish )
									result = new Ergebnis( name_min[i], "AudioAnalyser", "", "", "", name_min[i]+" (faulty frequencies)", ""+numberErrors, "0", ""+drop_min[i], "", "", "", "", "", "Evaluated MIN Spectrum ("+numberFreqs+" frequencies): From frequency: " + start_min[i] + " to frequency: " + stop_min[i], Ergebnis.FT_IO );
								else
									result = new Ergebnis( name_min[i], "AudioAnalyser", "", "", "", name_min[i]+" (fehlerhafte Frequenzen)", ""+numberErrors, "0", ""+drop_min[i], "", "", "", "", "", "Bewertung Min Spektrum ("+numberFreqs+" Frequenzen): Zwischen Frequenz: " + start_min[i] + " und Frequenz: " + stop_min[i], Ergebnis.FT_IO );
							} else {
								if( isEnglish )
									result = new Ergebnis( name_min[i], "AudioAnalyser", "", "", "", name_min[i]+" (faulty frequencies)", ""+numberErrors, "0", ""+drop_min[i], "", "", "", "", PB.getString( "toleranzFehler3" ), "Evaluated MIN Spectrum ("+numberFreqs+" frequencies): From frequency: " + start_min[i] + " to frequency: " + stop_min[i], Ergebnis.FT_NIO  );
								else
									result = new Ergebnis( name_min[i], "AudioAnalyser", "", "", "", name_min[i]+" (fehlerhafte Frequenzen)", ""+numberErrors, "0", ""+drop_min[i], "", "", "", "", PB.getString( "toleranzFehler3" ), "Bewertung Min Spektrum ("+numberFreqs+" Frequenzen): Zwischen Frequenz: " + start_min[i] + " und Frequenz: " + stop_min[i], Ergebnis.FT_NIO  );
								status = STATUS_EXECUTION_ERROR;
							}
							ergListe.add( result );
						} catch( Exception e ) {
							if( e instanceof DeviceIOException ) {
								if( isEnglish )
									result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", ""+ name_min[i], "", "", "", "0", "", "", "", "DeviceIOException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
								else
									result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "Zwischen Frequenz: " + start_min[i] + " und Frequenz: " + stop_min[i], "Max. erlaubte Fehler: " + drop_min[i] + " f�r " + name_min[i], "", "", "", "", "0", "", "", "", "DeviceIOException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS  );
							}
							else if( e instanceof DeviceParameterException ) {
								result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", ""+ name_min[i], "", "", "", "0", "", "", "", "DeviceParameterException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
							}
							else {
								result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", ""+ name_min[i], "", "", "", "0", "", "", "", myErrorTxt, "" + e.getMessage(), Ergebnis.FT_NIO_SYS );							}
							ergListe.add( result );
							throw new PPExecutionException();
						}
					}

					//Bewertung Klirr-Spektrum
					for( i = 0; i < maxIndex_klirr; i++ ) {
						try {
							if( isEnglish )
								myErrorTxt = "Error evaluating rattle spectrum: ";
							else
								myErrorTxt = "Fehler beim Auswerten des Klirrspektrums: ";
							boolean testPassed = true;
							spek = myAudio.getLRattleToleranzSpek();
							float[] freq = spek.getFrequency();
							int numErrors = 0;
							int numFreqs = 0;
							for( int numFreq = 0; numFreq < freq.length; numFreq++ ) {
								if( (freq[numFreq] >= start_klirr[i]) && (freq[numFreq] <= stop_klirr[i])) {
									if (myAudio.evaluateLRattleSpek( freq[numFreq], freq[numFreq] ) == false)  {
										numErrors++;
										if (numErrors > drop_klirr[i]) testPassed=false;
									}
									numFreqs++;
								}
							}
							//myAudio.evaluateLRattleSpek( start_klirr[i], stop_klirr[i], drop_klirr[i] );
							String numberFreqs="";
							String numberErrors="";
							try {
								numberErrors=numberErrors+numErrors;
								numberFreqs=numberFreqs+numFreqs;
							} catch (Exception e){};
							if(  testPassed ) {
								if( isEnglish )
									result = new Ergebnis( name_klirr[i], "AudioAnalyser", "", "", "", name_klirr[i]+" (faulty frequencies)", ""+numberErrors, "0", ""+drop_klirr[i], "", "", "", "", "", "Evaluated RATTLE Spectrum ("+numberFreqs+" frequencies): From frequency: " + start_klirr[i] + " to frequency: " + stop_klirr[i], Ergebnis.FT_IO );
								else
									result = new Ergebnis( name_klirr[i], "AudioAnalyser", "", "", "", name_klirr[i]+" (fehlerhafte Frequenzen)", ""+numberErrors, "0", ""+drop_klirr[i], "", "", "", "", "", "Bewertung Klirr Spektrum ("+numberFreqs+" Frequenzen): Zwischen Frequenz: " + start_klirr[i] + " und Frequenz: " + stop_klirr[i], Ergebnis.FT_IO );
							} else {
								myAudio.traceLangZeitSpektrum();
								if( isEnglish )
									result = new Ergebnis( name_klirr[i], "AudioAnalyser", "", "", "", name_klirr[i]+" (faulty frequencies)", ""+numberErrors, "0", ""+drop_klirr[i], "", "", "", "", PB.getString( "toleranzFehler3" ), "Evaluated RATTLE Spectrum ("+numberFreqs+" frequencies): From frequency: " + start_klirr[i] + " to frequency: " + stop_klirr[i], Ergebnis.FT_NIO );
								else
									result = new Ergebnis( name_klirr[i], "AudioAnalyser", "", "", "", name_klirr[i]+" (fehlerhafte Frequenzen)", ""+numberErrors, "0", ""+drop_klirr[i], "", "", "", "", PB.getString( "toleranzFehler3" ), "Bewertung Klirr Spektrum ("+numberFreqs+" Frequenzen): Zwischen Frequenz: " + start_klirr[i] + " und Frequenz: " + stop_klirr[i], Ergebnis.FT_NIO );
								status = STATUS_EXECUTION_ERROR;
							}

							ergListe.add( result );
						} catch( Exception e ) {
							if( e instanceof DeviceIOException ) {
								result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", ""+name_klirr[i], "", "", "", "0", "", "", "", "DeviceIOException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
							}
							else if( e instanceof DeviceParameterException ) {
								result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", ""+name_klirr[i], "", "", "", "0", "", "", "", "DeviceParameterException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
							}
							else {
								result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", ""+name_klirr[i], "", "", "", "0", "", "", "", myErrorTxt, "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
							}
							ergListe.add( result );
							throw new PPExecutionException();
						}
					}
					if( (status == STATUS_EXECUTION_ERROR) && (hwt.length() > 0) && (!(useZeroError && isZero)) ) {
						result = new Ergebnis( "HWT", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", "" + hwt, "", Ergebnis.FT_NIO );
						ergListe.add( result );
					}

					// Aufzeichnung der Ergebnisse
					try {
						if( isEnglish )
							myErrorTxt = "Error during local tracing";
						else
							myErrorTxt = "Fehler beim Schreiben des lokalen Trace";
						if (useTrace) {
							if (useLimitedSweep) {
								// local trace erzeugen
								if (debug) System.out.println("lim sweep trace creating");
									createLocalTrace(radio,lstype,channel,indexSweepTolFreqs,sweepMins,useSweepMax,sweepMaxs,sweepMaxSpecValues,sweepMaxSpecValues);
							}
							else  myAudio.traceResults();
						}
					} catch( Exception e ) {
						if( e instanceof DeviceIOException )
							result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", "DeviceIOException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
						else if( e instanceof DeviceParameterException )
							result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", "DeviceParameterException", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
						else
							result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", myErrorTxt, "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				}
			}
//	-------------------------------------------------------------- perform SYNC measurement ------------------------------------------
			else {
				if(debug)System.out.println("perform normal sync measurement ");
				// put out init sine tone -- only when init frequency is  used
				if (syncInitFrequencies != null) {
					for (i=0; i< syncInitFrequencies.length;i++){
						if (debug) System.out.println("Entering sync init freq["+(i+1)+"]: "+syncInitFrequencies[i]);
						job = syncJob;
						if (syncJobParUseSpecialCommand) {
							jobpar = syncJobPar;
							if (syncJobParIndexVolume>=0) jobpar = jobpar.replaceAll( "%VOLUME%", ""+syncVolumes[0] );
							if (syncJobParIndexFreq>=0) jobpar = jobpar.replaceAll( "%FREQUENCY%", ""+ (int) syncInitFrequencies[i] );
							if (syncJobParIndexChannel>=0) jobpar = jobpar.replaceAll( "%CHANNEL%", ""+syncChannel );
						} else jobpar = "" + (int) syncInitFrequencies[i] + ";" + syncVolumes[0] + ";" + syncChannel;
						try {
							if (debug) System.out.println("Using syncsgbd: #"+syncSgbd+"#");
							//special execution AMP_60
							if (syncSgbd.equalsIgnoreCase("AMP_60")) {
								if (debug) System.out.println("Entering AMP_60");
								job="stellglied_test_ausschalten";
								jobpar = "";
								temp = ed.executeDiagJob( syncSgbd, job, jobpar, "" );
								job="stellglied_test_einschalten";
								jobpar = (int) syncInitFrequencies[i] + ";" + syncVolumes[0] + ";" + syncChannel;
							}
							// special execution TOPDSP2
							if (syncSgbd.equalsIgnoreCase("TOPDSP2")) {
								job="steuern_sinusgenerator";
								jobpar = syncChannel + ";" + (int) syncInitFrequencies[i] + ";" + syncVolumes[0];
							}
							temp = ed.executeDiagJob( syncSgbd, job, jobpar, "" );
							if( temp.equals( "OKAY" ) == false ) {
								result = new Ergebnis( "DiagFehler", "EDIABAS", syncSgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								throw new PPExecutionException();
							} else {
								result = new Ergebnis( "Diagnose", "EDIABAS", syncSgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
							}
						} catch( ApiCallFailedException e ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", syncSgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						} catch( EdiabasResultNotFoundException e ) {
							if( e.getMessage() != null )
								result = new Ergebnis( "DiagFehler", "EDIABAS", syncSgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "DiagFehler", "EDIABAS", syncSgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						}
						// init pause
						try {
							Thread.sleep( syncInitPause );
						} catch( InterruptedException e ) {
						}
					}
				}
				int[] tempValues = new int[DN6000Constants.FREQUENCY_FRAME.length];
				
				syncMaxSpecValues = new int[DN6000Constants.FREQUENCY_FRAME.length];
				
				//noise detection
				 int noise_count = 0;
				 int noise_occur = 0;
				 String[] noise_type = new String[noise_num];

				isZero=true;
				
				for (i=0;i<maxIndex_sync;i++) {
					
					// set RecordDuration individually
					myAudio.setRecordDuration( syncRecordDurations[i] );

					//spectrum for noise recognition
					syncMaxSpecValues2 = new int[DN6000Constants.FREQUENCY_FRAME.length];
					noise_count = 0;
					numOkFreqs = 0;
					for(int t= 0; t<noise_type.length; t++) noise_type[t] = "";
					
					for (j=0;j<numSyncFreqs[i];j++) {
						repeat = 1;
						//repeat freq on error!
						for(int k = 0; k < repeat;k++){
						//put out sine tone when syncFreqs are > 0
							//if(debug)System.out.println("TEEST FOR FREQ i: " + i + " j " + j  + " k " + k + " repeat " + repeat);
							if (syncFreqs[i][j]>0) { 
								if (debug) System.out.println("Entering sync freq: "+syncFreqs[i][j]);
								job = syncJob;
								if (syncJobParUseSpecialCommand) {
									jobpar = syncJobPar;
									if (syncJobParIndexVolume>=0) jobpar = jobpar.replaceAll( "%VOLUME%", ""+syncVolumes[i] );
									if (syncJobParIndexFreq>=0) jobpar = jobpar.replaceAll( "%FREQUENCY%", ""+(int) syncFreqs[i][j] );
									if (syncJobParIndexChannel>=0) jobpar = jobpar.replaceAll( "%CHANNEL%", ""+syncChannel );
								} else jobpar = (int) syncFreqs[i][j] + ";" + syncVolumes[i] + ";" + syncChannel;
								try {
									// special execution AMP_60
									if (syncSgbd.equalsIgnoreCase("AMP_60")) {
										job="stellglied_test_ausschalten";
										jobpar = "";
										temp = ed.executeDiagJob( syncSgbd, job, jobpar, "" );
										job="stellglied_test_einschalten";
										jobpar = (int) syncFreqs[i][j]+ ";" + syncVolumes[i] + ";" + syncChannel;
									}
									// special execution TOPDSP2
									if (syncSgbd.equalsIgnoreCase("TOPDSP2")) {
										job="steuern_sinusgenerator";
										jobpar = syncChannel + ";" + (int) syncFreqs[i][j] + ";" + syncVolumes[i];
									}
									temp = ed.executeDiagJob( syncSgbd, job, jobpar, "" );
									if( temp.equals( "OKAY" ) == false ) {
										result = new Ergebnis( "DiagFehler", "EDIABAS", syncSgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
										ergListe.add( result );
										throw new PPExecutionException();
									} else {
										result = new Ergebnis( "Diagnose", "EDIABAS", syncSgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
										ergListe.add( result );
									}
								} catch( ApiCallFailedException e ) {
									result = new Ergebnis( "DiagFehler", "EDIABAS", syncSgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
									ergListe.add( result );
									throw new PPExecutionException();
								} catch( EdiabasResultNotFoundException e ) {
									if( e.getMessage() != null )
										result = new Ergebnis( "DiagFehler", "EDIABAS", syncSgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
									else
										result = new Ergebnis( "DiagFehler", "EDIABAS", syncSgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
									ergListe.add( result );
									throw new PPExecutionException();
								}
	
								//single frequency pause
								try {
									Thread.sleep( syncSingleFrequencyPause );
									//Thread.sleep( syncInitPause );
								} catch( InterruptedException e ) {}
							}
							// record spectrum when needed
							if (sumsyncFreqs[i] > 0 || j==0) {
								if (debug) System.out.println("Entering record spec for : "+syncFreqs[i][j]);
								myAudio.sampleSpek();
								if (debug) System.out.println("Sample spek done");
								myAudio.calculateLMaxSpek();
								if (debug) System.out.println("calculate max");
								tempValues = myAudio.getMaxSpek();
								if (debug) System.out.println("get spek");
								// calc hull curves
								for( int numFreq = 0; numFreq < DN6000Constants.FREQUENCY_FRAME.length; numFreq++ ) {
									if(useZeroError && isZero && (tempValues[numFreq] > 0)) isZero = false;
									if((i==0 && j == 0 && k == 0) || tempValues[numFreq] > syncMaxSpecValues[numFreq]) {
										syncMaxSpecValues[numFreq] = tempValues[numFreq];
									}
									//record second local spectrum. Needed for noise detection
									if((j == 0 && k == 0) || tempValues[numFreq] > syncMaxSpecValues[numFreq]) {
										syncMaxSpecValues2[numFreq] = tempValues[numFreq];
									}
								}
								if (debug) System.out.println("calculated long spek");
							}
							
							// fill minmax
							if (useMinMaxTrace) {
								if ((k == 0) || tempValues[indexTolFreqs[i][j]] < syncFreqsMin[i][j]) {
									syncFreqsMin[i][j] = tempValues[indexTolFreqs[i][j]];
								}
								if ((k == 0) || tempValues[indexTolFreqs[i][j]] > syncFreqsMax[i][j]) {
									syncFreqsMax[i][j] = tempValues[indexTolFreqs[i][j]];
								}
							}
	
	
							// evaluate min and create result
							
							if (tempValues[indexTolFreqs[i][j]]<syncMins[i][j]){
								if(repeatOnceOnError){
									if(debug)System.out.println("Repeating frequency " + syncTolFreqs[i][j] + ": reapeat no " + (k+1));
									if (k == 0 ) {
										repeat = 2;//repeat once
									}
									if (isEnglish) result = new Ergebnis( syncName[i]+(j+1), "AudioAnalyser", "", "", "", "Measurement value [dB]", ""+tempValues[indexTolFreqs[i][j]], ""+syncMins[i][j], "", "", "", "", "", PB.getString( "toleranzFehler3" ), "Audio measurement ("+syncName[i]+") at frequency: " + syncTolFreqs[i][j], Ergebnis.FT_IO);
									else result = new Ergebnis( syncName[i]+(j+1), "AudioAnalyser", "", "", "", "Messwert [dB]", ""+tempValues[indexTolFreqs[i][j]], ""+syncMins[i][j], "", "", "", "", "", PB.getString( "toleranzFehler3" ), "Audio Messung ("+syncName[i]+") bei Frequenz: " + syncTolFreqs[i][j], Ergebnis.FT_IO );
								}
								else{
									if (isEnglish) result = new Ergebnis( syncName[i]+(j+1), "AudioAnalyser", "", "", "", "Measurement value [dB]", ""+tempValues[indexTolFreqs[i][j]], ""+syncMins[i][j], "", "", "", "", "", PB.getString( "toleranzFehler3" ), "Audio measurement ("+syncName[i]+") at frequency: " + syncTolFreqs[i][j], Ergebnis.FT_IGNORE);
									else result = new Ergebnis( syncName[i]+(j+1), "AudioAnalyser", "", "", "", "Messwert [dB]", ""+tempValues[indexTolFreqs[i][j]], ""+syncMins[i][j], "", "", "", "", "", PB.getString( "toleranzFehler3" ), "Audio Messung ("+syncName[i]+") bei Frequenz: " + syncTolFreqs[i][j], Ergebnis.FT_IGNORE);
								}
							}
							else {
								// evaluate min and create result
								if (useMax[i]) {
									if (tempValues[indexTolFreqs[i][j]]>syncMaxs[i][j]) {
										if(repeatOnceOnError){
											if(debug)System.out.println("Repeating frequency " + syncTolFreqs[i][j] + ": reapeat no " + (k+1));
											if (k == 0 ) {
												repeat = 2;//repeat once
											}
											if (isEnglish) result = new Ergebnis( syncName[i]+(j+1), "AudioAnalyser", "", "", "", "Measurement value [dB]", ""+tempValues[indexTolFreqs[i][j]], ""+syncMins[i][j], ""+syncMaxs[i][j], "", "", "", "", PB.getString( "toleranzFehler3" ), "Audio measurement ("+syncName[i]+") at frequency: " + syncTolFreqs[i][j], Ergebnis.FT_IO ); 
											else result = new Ergebnis( syncName[i]+(j+1), "AudioAnalyser", "", "", "", "Messwert [dB]", ""+tempValues[indexTolFreqs[i][j]], ""+syncMins[i][j], ""+syncMaxs[i][j], "", "", "", "", PB.getString( "toleranzFehler3" ), "Audio Messung ("+syncName[i]+") bei Frequenz: " + syncTolFreqs[i][j], Ergebnis.FT_IO );
										}
										else{
											if (isEnglish) result = new Ergebnis( syncName[i]+(j+1), "AudioAnalyser", "", "", "", "Measurement value [dB]", ""+tempValues[indexTolFreqs[i][j]], ""+syncMins[i][j], ""+syncMaxs[i][j], "", "", "", "", PB.getString( "toleranzFehler3" ), "Audio measurement ("+syncName[i]+") at frequency: " + syncTolFreqs[i][j], Ergebnis.FT_IGNORE );
											else result = new Ergebnis( syncName[i]+(j+1), "AudioAnalyser", "", "", "", "Messwert [dB]", ""+tempValues[indexTolFreqs[i][j]], ""+syncMins[i][j], ""+syncMaxs[i][j], "", "", "", "", PB.getString( "toleranzFehler3" ), "Audio Messung ("+syncName[i]+") bei Frequenz: " + syncTolFreqs[i][j], Ergebnis.FT_IGNORE );
										}
									}
									else {
										if (isEnglish) result = new Ergebnis( syncName[i]+(j+1), "AudioAnalyser", "", "", "", "Measurement value [dB]", ""+tempValues[indexTolFreqs[i][j]], ""+syncMins[i][j], ""+syncMaxs[i][j], "", "", "", "", "", "Audio measurement ("+syncName[i]+") at frequency: " + syncTolFreqs[i][j], Ergebnis.FT_IO );
										else result = new Ergebnis( syncName[i]+(j+1), "AudioAnalyser", "", "", "", "Messwert [dB]", ""+tempValues[indexTolFreqs[i][j]], ""+syncMins[i][j], ""+syncMaxs[i][j], "", "", "", "", "", "Audio Messung ("+syncName[i]+") bei Frequenz: " + syncTolFreqs[i][j], Ergebnis.FT_IO );
										numOkFreqs++;
										if(debug)System.out.println("currently " + numOkFreqs + " exact ");
									}
								}
								else {
									if (isEnglish) result = new Ergebnis( syncName[i]+(j+1), "AudioAnalyser", "", "", "", "Measurement value [dB]", ""+tempValues[indexTolFreqs[i][j]], ""+syncMins[i][j], "", "", "", "", "", "", "Audio measurement ("+syncName[i]+") at frequency: " + syncTolFreqs[i][j], Ergebnis.FT_IO );
									else result = new Ergebnis( syncName[i]+(j+1), "AudioAnalyser", "", "", "", "Messwert [dB]", ""+tempValues[indexTolFreqs[i][j]], ""+syncMins[i][j], "", "", "", "", "", "", "Audio Messung ("+syncName[i]+") bei Frequenz: " + syncTolFreqs[i][j], Ergebnis.FT_IO );
									numOkFreqs++;
									//if(debug)System.out.println("currently " + numOkFreqs + " exact ");
								}
							}
							
							ergListe.add( result );
							if (debug) System.out.println("values compared");
							// create traces
							// local trace erzeugen
							if (syncSingleTrace) {
								if (debug) System.out.println("using single trace");
								if (syncLocalTrace) {
									int[][] tempIndex = new int[1][1];
									tempIndex[0][0] = indexTolFreqs[i][j];
									int[][] tempMin = new int[1][1];
									tempMin[0][0] = syncMins[i][j];
									int[][] tempMax = new int[1][1];
									boolean[] tempUseMax = new boolean[1];
									if (useMax[i]) {
										tempUseMax[0]=true;
										tempMax[0][0] = syncMaxs[i][j];
									}
									else tempUseMax[0]=false;
									createLocalTrace(radio,lstype,channel+"_"+syncName[i]+"_Frequency_No_" +(j+1),tempIndex,tempMin,tempUseMax,tempMax,tempValues,tempValues);
								}
								if (debug) System.out.println("local trace done");
								// APDM Result buffer erzeugen
							/*	createAPDMResultBuffer( resultBuffer, tempValues, channel+"_"+syncName[i]+"_Frequency_No_" +(j+1));
								// Ergebnisdatenblock in Ergebnis speichern
								result = new Ergebnis( channel+"_"+syncName[i]+"_Frequency_No_" +(j+1), "AudioAnalyser", "", "", "", "DATA_BLOCK", resultBuffer.toString(), "", "", "0", "", "", "", "", "Radio: "+radio+", System: "+lstype, Ergebnis.FT_IO );
								ergListe.add( result ); */
								if (debug) System.out.println("APDM trace done");
							}
						}
					}
					// create result summing up sync block testing including drop-outs
					if(numOkFreqs ==0 || numOkFreqs < (numSyncFreqs[i]-syncDropout[i])){
						status = STATUS_EXECUTION_ERROR;
						if (isEnglish) result = new Ergebnis( syncName[i], "AudioAnalyser", "", "", "", "Audio measurement ("+syncName[i]+")", "", "", "", "", "", "", ""+ (numSyncFreqs[i]- numOkFreqs) + " of " + numSyncFreqs[i] + " frequencies faulty, allowed: "+syncDropout[i], syncName[i]+" not detected", "", Ergebnis.FT_NIO);
						else result = new Ergebnis( syncName[i], "AudioAnalyser", "", "" , "", "Audio Messung ("+syncName[i]+")", "", "", "", "", "", "", ""+ (numSyncFreqs[i]- numOkFreqs) + " von " + numSyncFreqs[i] + " Frequenzen fehlerhaft, erlaubt: "+syncDropout[i], syncName[i]+" nicht erkannt", "", Ergebnis.FT_NIO);
						ergListe.add(result);
					}



//##########################################################NOISE DETECTION NEW#############################################################
					//if(debug)System.out.println("#################################################################################");
					//if(debug)System.out.println("Synchro Bereich number: " + i);
					if (useSyncNoise) {
						if (debug) System.out.println("Entering noise");
						int count = 1;
						for (int j2 = 0; j2 < maxIndex_noise_sync[i];j2++){
							noise_occur = 0;
							// check if number of noise frequency occurs is too high
							for (int j3 = 0; j3 < syncnumNoiseFreqs[i][j2];j3++) {
								if(debug)System.out.println("syncMaxSpecValues2 at position: " + syncIndexNoiseFreqs[i][j2][j3] +  "has value " +  syncMaxSpecValues2[syncIndexNoiseFreqs[i][j2][j3]]);
								if(debug)System.out.println("syncNoiseMaxs[ " + i + "][ " + j2+ "][" + j3 + "]"  + syncNoiseMaxs[i][j2][j3]);
								if(debug)System.out.println("syncNoiseMins[ " + i + "][ " + j2+ "][" + j3 + "]"  + syncNoiseMins[i][j2][j3]);
									if((syncMaxSpecValues2[syncIndexNoiseFreqs[i][j2][j3]] <= syncNoiseMaxs[i][j2][j3]) && (syncNoiseMins[i][j2][j3]<=syncMaxSpecValues2[syncIndexNoiseFreqs[i][j2][j3]])){
										noise_occur ++;
										// abort further measures if limit is reached
										if(noise_occur == syncNoiseMinOccurr[i][j2]){
											j3 = syncnumNoiseFreqs[i][j2]; 
										}
									}
							}							
							if(debug)System.out.println("NOISE_OCCUR = " + noise_occur);
							// create noise type, etc. in case of error 
							if((syncNoiseMinOccurr[i][j2]>0) &&( noise_occur >= syncNoiseMinOccurr[i][j2])){
								noise_count++;
								noise_type[count-1] = syncNoiseName[i][j2];
								count++;
								// create single frequency results
								for (int j3 = 0; j3 < syncnumNoiseFreqs[i][j2];j3++) {
									if (isEnglish) result = new Ergebnis( syncName[i]+"_"+syncNoiseName[i][j2]+(j2+1)+"_"+(j3+1), "AudioAnalyser", "", "", "", "Measurement value [dB]", ""+syncMaxSpecValues2[syncIndexNoiseFreqs[i][j2][j3]], ""+syncNoiseMins[i][j2][j3], ""+syncNoiseMaxs[i][j2][j3], "", "", "", "","Noise detection ("+syncNoiseName[i][j2]+")", "Audio measurement ("+syncName[i]+") at frequency: " + DN6000Constants.FREQUENCY_FRAME[syncIndexNoiseFreqs[i][j2][j3]], Ergebnis.FT_IGNORE);
									else result = new Ergebnis( syncName[i]+"_"+syncNoiseName[i][j2]+(j2+1)+"_"+(j3+1), "AudioAnalyser", "", "", "", "Messwert [dB]", ""+syncMaxSpecValues2[syncIndexNoiseFreqs[i][j2][j3]], ""+syncNoiseMins[i][j2][j3], ""+syncNoiseMaxs[i][j2][j3], "", "", "", "", "St�rger�uscherkennung ("+syncNoiseName[i][j2]+")", "Audio Messung ("+syncName[i]+") bei Frequenz: " + DN6000Constants.FREQUENCY_FRAME[syncIndexNoiseFreqs[i][j2][j3]], Ergebnis.FT_IGNORE);
									ergListe.add( result );
								}								
							}	
						}
					}    
						
						if(debug)System.out.println("Noise count " + noise_count);
						
					    if(noise_count>0){
					    	noiseDetected=true;
							if(noise_count <3){
								for(int k=1 ; k <= noise_type.length; k++){
									//st�rger�usch ausgeben
									if( noise_type[k-1] !=null && noise_type[k-1] != ""){
										if(debug)System.out.println("noise " + "NOISE_TYPE[" + (k-1) + "] is: " + noise_type[k-1]  + " in block" + syncName[i] + " detected.");
										if(!ignore_noise ){// dont ignore noise
											 status = STATUS_EXECUTION_ERROR;
	              							 if (isEnglish) result = new Ergebnis( "NOISE"+(i+1)+"_"+k, "AudioAnalyser", "", "" , "", "", "", "", "", "", "", "", "", "Noise:"+noise_type[k-1]+" at Audio measurement "+syncName[i], "Please repeat measurement", Ergebnis.FT_NIO); 
	              							 else result = new Ergebnis( "NOISE"+(i+1)+"_"+k, "AudioAnalyser", "", "" , "", "", " ", "", "", "", "", "", "", "St�rger�usch:"+noise_type[k-1]+" bei Audio Messung "+syncName[i], "Messung muss wiederholt werden", Ergebnis.FT_NIO);
	              							 ergListe.add( result );
										}
										else{
											if (isEnglish) result = new Ergebnis( "NOISE"+(i+1)+"_"+k, "AudioAnalyser", "", "" , "", "", "", "", "", "", "", "", "", "Noise:"+noise_type[k-1]+" at Audio measurement "+syncName[i], "Please repeat measurement", Ergebnis.FT_IO);	 	 
	              							else result = new Ergebnis( "NOISE"+(i+1)+"_"+k, "AudioAnalyser", "", "" , "", "", "", "", "", "", "", "", "", "St�rger�usch:"+noise_type[k-1]+" bei Audio Messung "+syncName[i], "Messung muss wiederholt werden", Ergebnis.FT_IO);
											ergListe.add( result );
										}
									}	
								}
							}
							else{
								//if(debug)System.out.println("Noise available but not explicit identifiable ");
								if(!ignore_noise){ // if not ignore noise set Ergebnis.FT_IO
									//if(debug)System.out.println("dont ignore noise");
									status = STATUS_EXECUTION_ERROR;
									if (isEnglish) result = new Ergebnis("NOISE"+(i+1), "AudioAnalyser", "", "" , "", "", "", "", "", "", "", "", "", "Noise available but not explicit identifiable at Audio measurement "+syncName[i], "Repeat measurement", Ergebnis.FT_NIO);
									else result = new Ergebnis( "NOISE"+(i+1), "AudioAnalyser", "", "" , "", "", "", "", "", "", "", "", "", "St�rger�usch vorhanden aber nicht eindeutig identifizierbar bei Audio Messung "+syncName[i], "Messung muss wiederholt werden", Ergebnis.FT_NIO);
									ergListe.add( result );
								}
								else {
									if (isEnglish)result = new Ergebnis("NOISE"+(i+1), "AudioAnalyser", "", "" , "", "", "", "", "", "", "", "", "", "Noise available but not explicit identifiable at Audio measurement "+syncName[i], "Repeat measurement", Ergebnis.FT_IO);
	      							else result = new Ergebnis( "NOISE"+(i+1), "AudioAnalyser", "", "" , "", "", "", "", "", "", "", "", "", "St�rger�usch vorhanden aber nicht eindeutig identifizierbar bei Audio Messung "+syncName[i], "Messung muss wiederholt werden", Ergebnis.FT_IO);
									ergListe.add( result );
								}
							}
						}
					    else{ 
					    	if(debug)System.out.println("no noise detected"); 
					    
					    }
				}				
				// check for zero measurement
				if (isZero && useZeroError) {
					if (debug) System.out.println("Zero Measurement: zero line detected");
					// clear old results
					ergListe.removeAllElements();				
					result = new Ergebnis( "ZERO LINE FIRST MEASUREMENT", "AudioAnalyser", "", "", "", "", "", "0", "", "", "", "", "", ""+zeroError, "", Ergebnis.FT_NIO );
					ergListe.add( result );
					if (useZeroConfirmWindow) {
						// write zeroline result
						if (debug) System.out.println("Zero Measurement: zero line detected and using confirm window");
						// display window
						try {
							ud = getPr�flingLaufzeitUmgebung().getUserDialog();
							// initially set cancel button
							// ud.setsetAllowCancel( true );
						} catch( Exception e ) {
							if( isEnglish )
								myErrorTxt = "Error getting User Dialogue for Confirm Window";
							else
								myErrorTxt = "Fehler beim Holen Userdialogs des Best�tigungsfensters";
							result = new Ergebnis( "UserDialog", "UserDialog", "", "", "", "", "", "0", "", "", "", "", "", ""+myErrorTxt, "", Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}						
						if (isEnglish) {
							if (useZeroAlert) ud.requestAlertMessage("Alert",zeroConfirmWindowText,zeroConfirmWindowTimeOut);
							else ud.requestUserMessage("Alert",zeroConfirmWindowText,zeroConfirmWindowTimeOut);
						}
						else {
							if (useZeroAlert) ud.requestAlertMessage("Achtung",zeroConfirmWindowText,zeroConfirmWindowTimeOut);
							else ud.requestUserMessage("Achtung",zeroConfirmWindowText,zeroConfirmWindowTimeOut);
						}
						if (debug) System.out.println("Zero Measurement: zero line detected,using confirm window and record second time");
					}
					status = STATUS_EXECUTION_ERROR;
				}

				//create hwt result
				if( (status == STATUS_EXECUTION_ERROR) && (hwt.length() > 0) && (!(useZeroError && isZero)) && (!(noiseDetected && !ignore_noise))) {
					result = new Ergebnis( "HWT", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", "" + hwt, "", Ergebnis.FT_NIO );
					ergListe.add( result );
				}
				
				// minmax tracing
				if (useMinMaxTrace) {
					for (int i3 = 0; i3<2;i3++) {												
						for (int i1 = 0; i1 < indexTolFreqs.length;i1++) {
							for (int i2 = 0; i2 < indexTolFreqs[i1].length;i2++) {
								// min trace
								if (i3==0) syncMaxSpecValues[indexTolFreqs[i1][i2]] = syncFreqsMin[i1][i2];
								// max trace
								if (i3==1) syncMaxSpecValues[indexTolFreqs[i1][i2]] = syncFreqsMax[i1][i2];
							}
						}
						// local trace erzeugen
						if (syncLocalTrace ) {
							createLocalTrace(radio,lstype,channel,indexTolFreqs,syncMins,useMax,syncMaxs,syncMaxSpecValues,syncMaxSpecValues);
						}
					/*	// APDM Result buffer erzeugen
						createAPDMResultBuffer( resultBuffer, syncMaxSpecValues, channel );
						// Ergebnisdatenblock in Ergebnis speichern
						result = new Ergebnis( channel, "AudioAnalyser", "", "", "", "DATA_BLOCK", resultBuffer.toString(), "", "", "0", "", "", "", "", "Radio: "+radio+", System: "+lstype, Ergebnis.FT_IO );
						ergListe.add( result );*/
					}
				}
				// normal single tracing
				else {
					// local trace erzeugen
					if (syncLocalTrace ) {
						createLocalTrace(radio,lstype,channel,indexTolFreqs,syncMins,useMax,syncMaxs,syncMaxSpecValues,syncMaxSpecValues);
					}
					// APDM Result buffer erzeugen
					createAPDMResultBuffer( resultBuffer, syncMaxSpecValues, channel );
					// Ergebnisdatenblock in Ergebnis speichern
				/*	result = new Ergebnis( channel, "AudioAnalyser", "", "", "", "DATA_BLOCK", resultBuffer.toString(), "", "", "0", "", "", "", "", "Radio: "+radio+", System: "+lstype, Ergebnis.FT_IO );
					ergListe.add( result ); */
				}
			}
		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" )+ ": " + e.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" )+ ": " + e.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}


		//Freigabe der benutzten Devices
		if( myAudio != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseAudioAnalyser();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "AudioAnalyser", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
			myAudio = null;
		}
		if( ud != null ) {
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "UserDialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "UserDialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
			ud = null;
		}
		//Status setzen
		setPPStatus( info, status, ergListe );

	}

// private methods ------------------------------------------------------------------------------------------------

	//private methode createAPDMResultBuffer
	/**
	 * Fills a StringBuffer with the results. The Buffer is transfered to Hex, for the APDM-Database
	 *
	 * @version 33
	 * @author Frank Weber
	 * @param resultBuffer StringBuffer that has to be written
	 * @param maxSpektrum the values to be written
	 * @param channelName String that describes the measured audio channel
	 * @exception DeviceParameterException thrown if result file is not writeable
	 */
	private void createAPDMResultBuffer( StringBuffer resultBuffer, int[] maxSpektrum, String channelName ) throws DeviceIOException {
		try {
			String tempResult = "";
			String tempVorkomma = "";
			String tempNachkomma = "";
			String temp = "";
			String nullString = "0000";

			resultBuffer.delete( 0, resultBuffer.length() );

			float[] frequencyList = DN6000Constants.FREQUENCY_FRAME;

			int[] RattleSpektrum = new int[DN6000Constants.FREQUENCY_FRAME.length];
			for( int i = 0; i < RattleSpektrum.length; i++ ) {
				RattleSpektrum[i] = 0;
			}

			if( frequencyList.length != maxSpektrum.length ) {
				throw new DeviceParameterException("Length of spectrum does not match");
			}

			try {
				temp = ("[" + channelName + "]");

				// ### convert channel name into HEX Code
				{
					byte[] tempByte = temp.getBytes();

					for( int j = 0; j < tempByte.length; j++ ) {
						tempResult = "";
						tempResult = Integer.toHexString( tempByte[j] );
						if( tempResult.length() < 2 ) {
							resultBuffer.append( "0" );
							resultBuffer.append( tempResult );
						} else {
							resultBuffer.append( tempResult );
						}
					}
				}
				tempResult = "";

				// ### write the calculated tables to StringBuffer
				for( int i = 0; i < DN6000Constants.FREQUENCY_FRAME.length; i++ ) {

					int k = 0;

					// Frequenzwert im Format 2 Bytes Vorkommastelle und 1 Byte Nachkommastelle
					// speichern
					temp = (String.valueOf( frequencyList[i] ));

					k = temp.indexOf( "." );

					tempVorkomma = temp.substring( 0, k );
					tempNachkomma = temp.substring( k + 1, k + 2 );

					// 2 Bytes fuer Vorkomma zusammenstellen
					temp = Integer.toHexString( Integer.parseInt( tempVorkomma ) );
					if( temp.length() > 4 )
						tempResult += temp.substring( temp.length() - 4, temp.length() );
					else {
						tempResult += nullString.substring( 0, 4 - temp.length() );
						tempResult += temp;
					}

					// 1 Byte fuer Nachkomma zusammenstellen
					temp = Integer.toHexString( Byte.parseByte( tempNachkomma ) );
					if( temp.length() > 2 )
						tempResult += temp.substring( temp.length() - 2, temp.length() );
					else {
						tempResult += nullString.substring( 0, 2 - temp.length() );
						tempResult += temp;
					}

					// Maximumwert in Hex schreiben ( 1 Byte )
					temp = Integer.toHexString( maxSpektrum[i] );
					if( temp.length() > 2 )
						tempResult += temp.substring( temp.length() - 2, temp.length() );
					else {
						tempResult += nullString.substring( 0, 2 - temp.length() );
						tempResult += temp;
					}

					// Klirrwert in Hex schreiben ( 2 Byte )
					temp = Integer.toHexString( RattleSpektrum[i] );
					if( temp.length() > 4 )
						tempResult += temp.substring( temp.length() - 4, temp.length() );
					else {
						tempResult += nullString.substring( 0, 4 - temp.length() );
						tempResult += temp;
					}
				}
				resultBuffer.append( tempResult );
			} catch( Exception e ) {
				if( isEnglish )
					throw new DeviceIOException( "Problem with writing APDM result Buffer: " + e.getMessage() );
				else
					throw new DeviceIOException( "Fehler beim Schreiben des APDM Ergebnis Puffers: " + e.getMessage() );
			}
		} catch( Exception e ) {
			e.printStackTrace();
			if( isEnglish )
				throw new DeviceIOException( "Error in writeResultBuffer: " + e.getMessage() );
			else
				throw new DeviceIOException( "Fehler beim Schreiben des APDM Ergebnis Puffers: " + e.getMessage() );
		}
	}

	/**
	 * Writes the local result file.
	 *
	 * @version 33
	 * @author Frank Weber
	 * @param series defines the series
	 * @param radio defines the radio
	 * @param lstype defines the loudspeakers type
	 * @param channel defines the measurement channel
	 * @param index defines the index in the frequency frame and measurement values
	 * @param min defines the minimum value
	 * @param max defines the maximum value
	 * @param maxspec defines the max spectrum
	 * @param rattlespec defines the rattle spectrum
	 * @exception DeviceIOException thrown if a problem with the Output Stream happened
	 * @exception DeviceParameterException thrown, if no output path available
	 */
	public void createLocalTrace(String radio, String lstype, String channel, int[][] index, int[][] min, boolean[] usemax, int[][] max, int[] maxspec, int[] rattlespec) throws DeviceIOException, DeviceParameterException {
		try {
			// prepare
			Date d = new Date();
			int endindex = 0;
			String newPath = "c:"+File.separator+"audio"+File.separator+"ergebnis"+File.separator+sBaureihe+File.separator+sKarosserie+File.separator+radio+File.separator+lstype;
			File resultPath = new File( newPath );
			resultPath.mkdirs();
			String zeit = CascadeDateFormat.formatTime( d ).substring( 0, 2 ) + CascadeDateFormat.formatTime( d ).substring( 3, 5 ) + CascadeDateFormat.formatTime( d ).substring( 6, 8 );
			String datum = CascadeDateFormat.formatDate( d ).substring( 8, 10 ) + CascadeDateFormat.formatDate( d ).substring( 3, 5 ) + CascadeDateFormat.formatDate( d ).substring( 0, 2 );
			String filename = getPr�fling().getAuftrag().getFahrgestellnummer7() + "_" + channel.toUpperCase().replace( ' ', '_' ) + "_" + datum + "_" + zeit + ".DAT";
			FileOutputStream resultWriter = new FileOutputStream( resultPath + File.separator + filename, false );
			//write header
			resultWriter.write( ("                        Result Audio measurment\r\n").getBytes() );
			resultWriter.write( ("=========================================================================\r\n").getBytes() );
			resultWriter.write( ("Testing time            : " + CascadeDateFormat.formatDate( d ) + ", " + CascadeDateFormat.formatTime( d ) + "\r\n").getBytes() );
			resultWriter.write( ("Vehicle information\r\n").getBytes() );
			resultWriter.write( ("  VIN                   : " + getPr�fling().getAuftrag().getFahrgestellnummer7() + "\r\n").getBytes() );
			resultWriter.write( ("  Series                : " + getPr�fling().getAuftrag().getBaureihe() + "\r\n").getBytes() );
			resultWriter.write( ("  Body Type             : " + sKarosserie + "\r\n").getBytes() );
			resultWriter.write( ("  Radio                 : " + radio + "\r\n").getBytes() );
			resultWriter.write( ("  Loudspeaker system    : " + lstype + "\r\n").getBytes() );
			resultWriter.write( ("  Messkanal             : " + channel.toUpperCase() + "\r\n").getBytes() );
			resultWriter.write( ("Result \r\n").getBytes() );
			// values
			resultWriter.write( ("  Value                 : ").getBytes() );
			for (int i=0;i<index.length;i++) {
				endindex = index[i].length;
				if (i==index.length-1) endindex = index[i].length-1;
				for (int j=0;j<endindex;j++) {
					resultWriter.write( (maxspec[index[i][j]] + ", ").getBytes() );
				}
			}
			resultWriter.write( (maxspec[index[index.length-1][endindex]] + "\r\n").getBytes() );
			// parameters
			resultWriter.write( ("Parameters \r\n").getBytes() );
			resultWriter.write( ("  Evaluated Frequency   : ").getBytes() );
			for (int i=0;i<index.length;i++) {
				endindex = index[i].length;
				if (i==index.length-1) endindex = index[i].length-1;
				for (int j=0;j<endindex;j++) {
					resultWriter.write( (DN6000Constants.FREQUENCY_FRAME[index[i][j]] + ", ").getBytes() );
				}
			}
			resultWriter.write( (DN6000Constants.FREQUENCY_FRAME[index[index.length-1][endindex]] + "\r\n").getBytes() );
			resultWriter.write( ("  Min Value             : ").getBytes() );
			for (int i=0;i<index.length;i++) {
				endindex = index[i].length;
				if (i==index.length-1) endindex = index[i].length-1;
				for (int j=0;j<endindex;j++) {
					resultWriter.write( (min[i][j] + ", ").getBytes() );
				}
			}
			resultWriter.write( (min[index.length-1][endindex] + "\r\n").getBytes() );

			resultWriter.write( ("  Max Value             : ").getBytes() );
			for (int i=0;i<index.length;i++) {
				endindex = index[i].length;
				if (i==index.length-1) endindex = index[i].length-1;
				if (usemax[i]) {
					for (int j=0;j<endindex;j++) {
						resultWriter.write( (max[i][j] + ", ").getBytes() );
					}
				}
			}
			if (usemax[index.length-1]) resultWriter.write( (max[index.length-1][endindex] + "\r\n").getBytes() );
			else resultWriter.write("\r\n".getBytes());


			resultWriter.write( ("=========================================================================\r\n").getBytes() );
			// write data
			resultWriter.write( "Data: Freq. MAX Rattle \r\n".getBytes() );
			for( int i = 0; i < DN6000Constants.FREQUENCY_FRAME.length; i++ ) {
				resultWriter.write( "Freq: ".getBytes() );
				resultWriter.write( (String.valueOf( DN6000Constants.FREQUENCY_FRAME[i] )).getBytes() );
				resultWriter.write( " ".getBytes() );
				// write max
				resultWriter.write( (maxspec[i] + " ").getBytes() );
				// write rattle
				resultWriter.write( (rattlespec[i] + " ").getBytes() );
				// change to next line
				resultWriter.write( "\r\n".getBytes() );
			}
			resultWriter.write( "\r\n".getBytes() );
			resultWriter.close();
		}
		catch( Exception e ) {
			e.printStackTrace();
			if( e instanceof DeviceIOException )
				throw new DeviceIOException( "Error in local tracing: " + e.getMessage() );
			else if( e instanceof DeviceParameterException )
				throw new DeviceParameterException( "Error in traceResults: " + e.getMessage() );
			else
				throw new DeviceIOException( "Error in traceResults: " + e.getMessage() );
		}
	}

//end of pruefprocedure
}

package com.bmw.cascade.pruefprozeduren;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.psdz.*;
import com.bmw.cascade.pruefstand.devices.psdz.data.*;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.*;
import com.bmw.cascade.pruefstand.devices.psdz.util.CascadeTALReader;
import com.bmw.cascade.pruefstand.visualisierung.*;

import java.util.*;

/**
 * Implementierung einer Pr�fprozedur, die eine TAL generiert
 * 
 * @author Mueller, BMW AG; Buboltz, BMW AG; Seitz, BMW AG; Rettig, GEFASOFT GmbH; Busetti, GEFASOFT AG; Gampl, BMW AG <BR>
 * @version 22.02.2007 PR Erweiterung Parameter-Check hinsichtlich sich gegenseitig ausschlie�ender Argumente.<BR>
 *          22.03.2007 TB Korrektur Parameter Check fuer SHOW_TAL <BR>
 *          25.03.2007 TB sauberer PSdZ Exception catch und Option Platzhalter '*' eingebaut <BR>
 *          12.04.2007 TB SVTist Ermittlung hier entfernt -> PP PSdZGetSVTs, DEU / ENG Texte durchg�ngig <BR>
 *          21.04.2007 TB Fehler Cancel bei UserDialog Abbruch korrigiert + Texte korrigiert <BR>
 *          09.08.2007 TB Korrektur TAL Filter, Robustheit beim Parametercheck verbessert <BR>
 *          24.08.2007 JS Korrektur SWT upgradeIndex per default auf 1 �ber Schleife <BR>
 *          03.01.2008 DB Einbau "testScreenID" in Debug-Ausgaben <BR>
 *          30.01.2008 TB BugFix Gro�- / Kleinschreibung Hexzahlen <BR>
 *          02.06.2008 DB Ersetzen deprecated-Methode 'psdz.generateAndStoreSollverbauungTAL(...)' durch 'psdz.generateAndStoreTAL(...)' <BR>
 *          24.06.2008 DB �nderung holen des "testScreenID" <BR>
 *          01.12.2010 AN �berpr�fung auf Status NotExecutable + Parameter eingef�gt<BR>
 *          30.05.2011 TB opt. Argument "DUPLICATE_ECU_SOLL_LIST" hinzugef�gt (default: false, bei true wird die ECU Ist List mit der ECU Soll List �berschrieben <BR>
 *          21.03.2013 TB T-Version (16_0) <BR>
 *          ---------- -- - LOP 1610 Anforderung: requestSWT Status nur dann ausf�hren, wenn FSC Aktivit�ten �ber den TAL-Filter drin sind od. der Filter leer ist, ACHTUNG: CASCADE Version ab 6.0.0 wird ben�tigt <BR>
 *          20.04.2013 TB T-Version (16_0) <BR>
 *          ---------- -- - BEGU Sub-Kategorien beim swDeploy unterstuetzen, Generics <BR>
 *          08.11.2013 MG F-Version (17_0) <BR>
 *          11.06.2014 MG T-Version (18_0), Mindestversionsanforderung CASCADE 6.1.0 / PSdZ 4.9.1 <BR>
 *          ---------- -- - LOP Punkt 1789: Als Kurzfristma�nahme wird eine Mehrfachangabe einer Diagnoseadresse in einer Include-/Exclude-Liste bereits in der PP entfernt <BR>
 *          ---------- -- - ForceExecution-Flag, sowie Include-/Exclude-Listen werden in s�mtlichen F�llen (incl. BEGU) in PruefstandScreen_0.log als Debug-Ausgaben mit ausgegeben, da die Ausgaben zum TAL-Filter aus CASCADE 6.1.0 ausgebaut wurden <BR>
 *          ---------- -- - Codeoptimierungen (Berechung von Negativliste bei Include-Liste und aktivem ForceExecution-Flag entfernt). <BR>
 *          ---------- --   CASCADE Workaround-Code in PSdZ-Device kann somit ab fl�chiger Nutzung der PP wieder ausgebaut werden (-> getBEGUSpecialFilterCases und defineFilterForSingleSWDeploysBEGU) <BR>
 *          ---------- -- - Ausbau der deprecated Methoden <BR>
 *          17.06.2014 MG F-Version (19_0), Mindestversionsanforderung CASCADE 6.1.0 / PSdZ 4.9.1 <BR>
 *          30.07.2015 MG T-Version (20_0), Mindestversionsanforderung CASCADE 7.0.1 / PSdZ 5.00.01 <BR>
 *          ---------- -- - �berfl�ssige Debugausgaben zu clearPSdZExceptions entfernt <BR>
 *          ---------- -- - Kurzfristma�nhame zu LOP Punkt 1789 wieder entfernt, da Fehlerbehebung in CASCADE-Kern verf�gbar. Mindestversion f�r PP ist CASCADE 7.0 <BR>
 *          ---------- -- - LOP 1853: Ausgabe (in Log-Datei Pr�fstandScreen) der Pr�fprozedurversion sowie s�mtlicher optionaler und nicht optionaler Argumente, die an die Pr�fprozedur �bergeben werden zum Zweck der Nachvollziehbarkeit im Fehlerfall. <BR>
 *          ---------- -- - LOP 1899: Einheitliche Darstellung der Fehlerausgaben in PSdZ-Pr�fprozeduren <BR>
 *          ---------- -- - Fehlerbehebung: Bei Nutzung des Parameters CHECK_TALLINE_STATUS wurden TAL-Lines mit Status "notExecutable" f�lschlicherweise nicht erkannt <BR>
 *          27.08.2015 MG F-Version (21_0), Mindestversionsanforderung CASCADE 7.0.1 / PSdZ 5.00.01 <BR>
 *          25.01.2016 MG T-Version (22_0), Mindestversionsanforderung CASCADE 7.1.0 / PSdZ 5.01.00 <BR>
 *          ---------- -- - LOP 1926: Nutzung der neuen Funktionalit�t zu CR2439 (Sicherstellung der SW-Konsistenz im Steuerger�t nach Programmierung auch bei Verwendung von BegU-TAL-Filtern) ab PSdZ 5.01.00. Mindestversion f�r PP ist CASCADE 7.1.0 / PSdZ 5.01.00 <BR>
 *          04.02.2016 MG F-Version (23_0), Mindestversionsanforderung CASCADE 7.1.0 / PSdZ 5.01.00 <BR>
 * 
 */
public class PSdZGenerateTAL_23_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	// Debug flags
	boolean bTracesOn = false;
	boolean bCheckArgsOn = false;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSdZGenerateTAL_23_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSdZGenerateTAL_23_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "ACTION[1..N]", "CHECK_TALLINE_STATUS", "DEBUG", "DEBUG_PERFORM", "DUPLICATE_ECU_SOLL_LIST", "EXCLUDE_LIST[1..N]", "FORCE_EXECUTION[1..N]", "INCLUDE_LIST[1..N]", "SHOW_DURATION", "SHOW_TAL", "SWT_ACTION[1..N]", "SWT_EXCLUDE_LIST[1..N]", "SWT_INCLUDE_LIST[1..N]", "VEHICLE_STATE_EXCLUDE_LIST", "VEHICLE_STATE_INCLUDE_LIST" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = {};
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	@SuppressWarnings("unchecked")
	public boolean checkArgs() {
		int i = 0, j = 0;
		int maxIndex = 0;
		boolean bOptionalArg = false;

		// Debug output on begin of checkArgs()
		if( bCheckArgsOn ) {
			Enumeration<?> enu = getArgs().keys();

			System.out.println( "PSdZGenerateTAL: checkArgs" );
			System.out.println( "PSdZGenerateTAL: checkArgs in: " + this.getName() );
			System.out.println( "PSdZGenerateTAL: All Arguments from this PP:" );
			while( enu.hasMoreElements() ) {
				// The actual argument
				String strGivenkey = (String) enu.nextElement();
				String strActualArg = getArg( strGivenkey );
				System.out.println( "PSdZGenerateTAL: " + strGivenkey + " = " + strActualArg );
			}
		}

		try {
			// 1. Check: Sind alle requiredArgs gesetzt
			String[] requiredArgs = getRequiredArgs();
			for( i = 0; i < requiredArgs.length; i++ ) {
				if( getArg( requiredArgs[i] ) == null )
					return false;
				if( getArg( requiredArgs[i] ).equals( "" ) == true )
					return false;
			}
			if( bCheckArgsOn ) {
				System.out.println( "PSdZGenerateTAL: Done 1. Check" );
			}
			// 2. Check: Sind �berfl�ssige Argumente gesetzt, d.h. Argumente, die weder required
			// noch optional sind
			// Gesetzte Argumente, die mit ..[1..N] beginnen, werden hierbei nicht analysiert, es
			// wird
			// jedoch der maximale Index festgehalten.
			Enumeration<?> enu = getArgs().keys();
			String[] optionalArgs = getOptionalArgs();
			String givenkey = null;
			String temp = null;
			while( enu.hasMoreElements() ) {
				givenkey = (String) enu.nextElement();
				// Wenn Argument ohne Index
				if( (givenkey.startsWith( "ACTION" ) == false) || (givenkey.startsWith( "INCLUDE_LIST" ) == false) || (givenkey.startsWith( "EXCLUDE_LIST" ) == false) || (givenkey.startsWith( "FORCE_EXECUTION" ) == false) || (givenkey.startsWith( "SWT_ACTION" ) == false) || (givenkey.startsWith( "SWT_INCLUDE_LIST" ) == false) || (givenkey.equals( "SWT_EXCLUDE_LIST" ) == false) ) {

					j = 0;
					bOptionalArg = false;
					while( (bOptionalArg == false) && (j < optionalArgs.length) ) {
						if( givenkey.equals( optionalArgs[j] ) == true )
							bOptionalArg = true;
						j++;
					}
					if( getArg( givenkey ).equals( "" ) == true )
						return false;
				}
				// Wenn Argument mit Index
				else {
					if( givenkey.startsWith( "ACTION" ) == true ) {
						temp = givenkey.substring( 6 );
					} else if( givenkey.startsWith( "INCLUDE_LIST" ) == true || givenkey.startsWith( "EXCLUDE_LIST" ) == true ) {
						temp = givenkey.substring( 12 );
					} else if( givenkey.startsWith( "FORCE_EXECUTION" ) == true ) {
						temp = givenkey.substring( 15 );
					} else if( givenkey.startsWith( "SWT_ACTION" ) == true ) {
						temp = givenkey.substring( 10 );
					} else if( givenkey.startsWith( "SWT_INCLUDE_LIST" ) == true || givenkey.startsWith( "SWT_EXCLUDE_LIST" ) == true ) {
						temp = givenkey.substring( 16 );
					}
					try {
						j = Integer.parseInt( temp );
						if( j < 1 )
							return false;
						if( j > maxIndex )
							maxIndex = j;
					} catch( NumberFormatException e ) {
						e.printStackTrace();
						return false;
					}
				}
			}
			if( bCheckArgsOn ) {
				System.out.println( "PSdZGenerateTAL: Done 2. Check" );
			}

			// Check f�r sich gegenseitig ausschlie�ende Parameter.
			String[][] blacklist = { { "INCLUDE_LIST", "EXCLUDE_LIST" }, { "SWT_INCLUDE_LIST", "SWT_EXCLUDE_LIST" } }; // Paare von sich gegenseitig ausschlie�enden Argumenten (ohne [1..N]-Angabe!)

			if( !checkMutualExclusiveArgs( blacklist, (String[]) getArgs().keySet().toArray( new String[0] ) ) ) // pr�fe, ob sich gegenseitig ausschlie�ende Argumente verwendet werden (indizierte Argumente werden pro Index separat betrachtet)
				return false;

			// Tests bestanden, somit ok
			if( bCheckArgsOn ) {
				System.out.println( "PSdZGenerateTAL: ok = true" );
				System.out.println( "PSdZGenerateTAL: End checkArgs() from PSdZGenerateTAL." );
			}
			return true;
		} catch( Exception e ) {
			if( bCheckArgsOn )
				e.printStackTrace();
			return false;
		} catch( Throwable e ) {
			if( bCheckArgsOn )
				e.printStackTrace();
			return false;
		}
	}

	/**
	 * Pr�ft, ob sich gegenseitig ausschlie�ende Argumente auch tats�chlich nicht gemeinsam verwendet werden. Indizierte Argumente werden pro Index separat betrachtet und sind als solche im Parameter <code>names</code> am Ende NICHT durch die Angabe [1..N] o. �. zu kennzeichnen. Achtung: Gro�-/Kleinschreibung wird unterschieden!
	 * 
	 * @param blacklist Zweidimensionales String-Array mit Zeilen und Spalten ([Zeile][Spalte]). Wobei in den Zeilen jene Argumentnamen stehen m�ssen, die sich gegenseitig ausschlie�en (Negativ-Liste). Hinweis: Die Anzahl der Spalten darf von Zeile zu Zeile variieren.
	 * @param args Die zu pr�fenden Pr�fprozedur-Argumente.
	 */
	private boolean checkMutualExclusiveArgs( String[][] blacklist, String[] args ) {
		HashMap<String, List<String>> map = new HashMap<String, List<String>>();

		// �berpr�fe die G�ltigkeit der �bergebenen Parameter.
		if( blacklist == null || blacklist.length == 0 || args == null || args.length == 0 )
			return true; // wenn keine Negativ-Liste oder zu kontrollierenden Argumente spezifiziert wurden, dann ist alles OK

		// Sortiere alle Pr�fprozedur-Argumente nach ihren Indizes in eine
		// HashMap. Die Schl�ssel sind die Indizes und die Werte jeweils eine
		// Liste mit jenen Pr�fprozedur-Argumenten, welche den im Schl�ssel
		// spezifizierten Index tragen (die Ablage in den Werten erfolgt jedoch
		// ohne Index). Hinweis: Nicht indizierte Argumente werden dem Index -1
		// zu geordnet. Dar�ber hinaus gilt, dass Pr�fprozedur-Argumente,
		// welche nur aus Ziffern bestehen, ignoriert werden.
		for( int i = 0; i < args.length; i++ )
			// gehe alle zupr�fenden Pr�fprozedur-Argumente durch
			for( int charIndex = args[i].length() - 1; charIndex >= 0; charIndex-- )
				// gehe das aktuelle Argument zeichenweise von hinten nach vorne durch und suche nach dem ersten Zeichen, welches keine Ziffer ist
				if( args[i].charAt( charIndex ) < '0' || args[i].charAt( charIndex ) > '9' ) { // Keine Ziffer?
					String number = "-1"; // Nummernbestandteil des Pr�fprozedur-Arguments (Default, wenn keine Nummer da ist, = -1)

					if( charIndex != args[i].length() - 1 ) // Wurde �berhaupt eine Ziffer gefunden?
						number = args[i].substring( charIndex + 1 );

					List<String> list = (List<String>) map.get( number ); // hole Liste zur Nummer
					if( list == null ) { // Liste noch nicht exitent?
						list = new ArrayList<String>();
						map.put( number, list );
					}

					list.add( args[i].substring( 0, charIndex + 1 ) ); // Wert (ohne Nummernbestandteil) in Liste ablegen

					break; // dieses Argument nicht weiter durchsuchen
				}

		// Pr�fe, ob pro Liste in der Negativ-Liste stehende Kombinationen
		// vorkommen. Gehe hierbei die Zeilen des zweidimensionalen Arrays mit
		// den sich gegenseitig ausschlie�enden Argumenten (Negativ-Liste)
		// durch und pr�fe, ob zwei oder mehr Elemente einer jeden Zeile
		// innerhalb einer einzelnen Liste vorkommen.
		boolean found = false;
		Iterator<?> iter = map.values().iterator(); // hole alle Werte der HashMap (die Schl�ssel waren nur f�r das Einf�gen von Bedeutung)
		while( iter.hasNext() ) { // gehe alle oben angelegten Listen durch
			List<?> list = (List<?>) iter.next(); // beziehe n�chste Liste
			for( int zeile = 0; zeile < blacklist.length; zeile++ ) { // gehe alle Zeilen der Negativ-Liste durch
				found = false; // r�cksetzen, damit wir aktuelle Zeile pr�fen k�nnen
				for( int spalte = 0; spalte < blacklist[zeile].length; spalte++ )
					// gehe alle Spalten der aktuellen Zeile durch
					if( list.contains( blacklist[zeile][spalte] ) ) // Ist der Wert aus der Negativ-Liste in Liste enthalten?
						if( found ) // Wurde schon zuvor ein Treffer erzielt (ist dies also der zweite Wert der aktuellen Negativ-Listen-Zeile, der sich in der aktuellen Liste wiederfindet)?
							return false; // NIO-Ende
						else
							found = true; // vermerke den Treffer

			}
		}

		return true; // IO-Ende
	}

	/**
	 * Lese die indizierten Argumente
	 * 
	 * @param parameterName
	 * @return StringArray mit den Argumenten
	 */
	private String[] readIndexedParameter( String parameterName ) {
		int resAnzahl = 0;
		while( (getArg( parameterName + (resAnzahl + 1) )) != null ) {
			resAnzahl++;
		}
		String[] resList = new String[resAnzahl];
		for( int i = 0; i < resAnzahl; i++ )
			resList[i] = getArg( parameterName + (i + 1) );
		return resList;
	}

	/**
	 * Pr�fe und modifiziere ein Eingangssarray aus ECU Hex Adressen. Ignoriere den '*' als m�glichen Platzhalter, der ebenfalls in der Liste auftauchen kann
	 * 
	 * @param strInputArray mit ECU SG Adressen in hexadezimaler Form
	 * @return modifiziertes Eingangsarray aus ECU Hex Adressen mit Kleinbuchstaben
	 */
	private String[] checkAndModifyEcuHexArray( String[] strInputArray ) {
		for( int i = 0; i < strInputArray.length; i++ ) {
			if( !strInputArray[i].equalsIgnoreCase( "*" ) )
				strInputArray[i] = Integer.toHexString( new Integer( Integer.parseInt( strInputArray[i], 16 ) ).intValue() ).toLowerCase();
		}

		return strInputArray;
	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; // default english
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {
		boolean bCheckSWTStatus = false; // gibt an, ob ein requestSWTStatus durchgef�hrt werden soll, oder nicht
		boolean bCheckTalLineStatus = false; // gibt an, ob der TAL-Line Status auf "NotExecutable" gep�rft wird
		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
		boolean bDebugPerform = true; // gibt an, ob eine Performencemessung erfolgen sollen
		boolean bDuplicateECUSollList = false; // bei TRUE wird die Liste aus der "installedECUList_Soll" nach "installedECUList_Ist" dupliziert
		boolean bOptionalIndexedArgument = false;
		boolean showTAL = false;
		char cPlatzhalter = 'n'; // Verwaltung des '*' Platzhalters: 'n'-nichts oder ohne Platzhalter, 'i'-include Liste hat Platzhalter, 'e'-exclude Liste hat Platzhalter
		final boolean DE = checkDE(); // Systemsprache DE wenn true
		final String NAME_ECU_LIST_IST = "installedECUList_Ist"; // Token f�r Element "installedECUList_Ist" in der TAL. Achtung bei PSdZ seitigen �nderungen!
		final String NAME_ECU_LIST_SOLL = "installedECUList_Soll"; // Token f�r Element "installedECUList_Soll" in der TAL. Achtung bei PSdZ seitigen �nderungen!
		// df- funktionale Adresse neues Bordnetz; ef-funktionale Adresse altes Bordnetz; f1, f4, f5 Testeradressen
		final String[] STAR_IGNORE_LIST = { "df", "ef", "f1", "f4", "f5" }; // IGNORE Liste f�r die Platzhalter-Verwendung, hier k�nnen z.B. die funktionalen ECU Adressen stehen, ACHTUNG: hier z.B. nur "ef" und nicht "EF" eintragen
		final List<String> STAR_IGNORE_LIST_AS_LIST = Arrays.asList( STAR_IGNORE_LIST );
		int iShowDuration = 3; // Anzeige per default 3 Sekunden
		int status = STATUS_EXECUTION_OK;

		ArrayList<String> maskList = new ArrayList<String>(); // Mittels eines '*' beim ersten Element in der vehicleStateIncludeList oder vehicleStateExcludeList werden in der jeweils anderen Liste alle ECUs eingetragen ohne die ECUs in der List wo kein Platzhalter steht und ohne die ECUs aus der IGNORE_LIST.
		Ergebnis result;
		Hashtable<String, Boolean> forceExecutionList = new Hashtable<String, Boolean>();
		Hashtable<String, String[]> actionExcludeList = new Hashtable<String, String[]>();
		Hashtable<String, String[]> actionIncludeList = new Hashtable<String, String[]>();
		Hashtable<String, String[]> swtActionExcludeList = new Hashtable<String, String[]>();
		Hashtable<String, String[]> swtActionIncludeList = new Hashtable<String, String[]>();
		List<String> actions = new ArrayList<String>(); // Liste fuer die Actions "swDeploy" etc.
		List<String> subActions = new ArrayList<String>(); // Liste der SubActions "swDeploy | swDeleteTA" etc.
		PSdZ psdz = null;
		String givenKey = null;
		String optionalArgumentName = null;
		String optionalArgumentNameForIndexedArgument = null;
		String optionalArgumentValue = null;
		String optionalIndexedArgumentValue = null;
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";
		String testScreenID = this.getPr�flingLaufzeitUmgebung().getName(); // TestScreenID (aus Pr�fstandsinstanz)
		String[] actionAndSubActionList = null;
		String[] optionalArgumentsNames = getOptionalArgs();
		String[] optionalArgumentsValues = null;
		String[] swtActionList = null;
		String[] vehicleStateExcludeList = null;
		String[] vehicleStateIncludeList = null;
		UserDialog userDialog = null;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();

		try {

			// Argumente loggen, einlesen und checken
			try {

				// Ausgabe der Pr�fprozedurversion sowie s�mtlicher an die Pr�fprozedur �bergebener Argumente (required/optional) f�r sp�tere Analysezwecke
				System.out.println( "PSdZ ID:" + testScreenID + ", Initialize " + this.getClassName() );

				// aktuell sind in PP keine zwingenden Argumente parametrierbar, deshalb nur Ausgabe optionaler Argumente
				for( int i = 0; i < optionalArgumentsNames.length; i++ ) {

					optionalArgumentName = optionalArgumentsNames[i];
					// falls es sich bei aktuell untersuchtem Argumentnamen um indizierten Namen handelt, extrahiere "Klarnamen" ohne Index
					// Indizierte Argumentnamen: ACTION[1..N], EXCLUDE_LIST[1..N], FORCE_EXECUTION[1..N], INCLUDE_LIST[1..N], SWT_ACTION[1..N], SWT_EXCLUDE_LIST[1..N] oder SWT_INCLUDE_LIST[1..N]
					optionalArgumentNameForIndexedArgument = optionalArgumentName.replace( "[1..N]", "" );

					bOptionalIndexedArgument = optionalArgumentName.contains( "[1..N]" );
					if( bOptionalIndexedArgument ) {
						// SortedMap muss f�r jeden untersuchten indizierten Argumentnamen erneut leer erzeugt werden
						SortedMap<String, String[]> optionalIndexedArgumentsSortedMap = new TreeMap<String, String[]>();
						Enumeration<?> enumKeys = getArgs().keys();
						// gehe �ber alle an PP �bergebene Argumente ...
						while( enumKeys.hasMoreElements() ) {
							givenKey = (String) enumKeys.nextElement();
							// ... und pr�fe, ob der an PP �bergebene Argumentname mit dem aktuell gesuchten Argumentnamen beginnt z.B. "INCLUDE_LIST1" beginnt mit "INCLUDE_LIST"
							if( givenKey.startsWith( optionalArgumentNameForIndexedArgument ) ) {
								// falls ja, nehme den an PP �bergebenen Argumentnamen und Argumentwert in sortierte Liste auf
								optionalIndexedArgumentsSortedMap.put( givenKey, extractValues( getArg( givenKey ) ) );
							}
						}
						// gebe sortierte Liste der indizierten Argumentnamen und Argumentwerte aus, jedoch nur wenn es etwas auszugeben gibt
						if( !optionalIndexedArgumentsSortedMap.isEmpty() ) {
							Iterator<?> iteratorSortedMap = optionalIndexedArgumentsSortedMap.keySet().iterator();
							while( iteratorSortedMap.hasNext() ) {
								String optionalIndexedArgumentKey = (String) iteratorSortedMap.next(); // z.B. "INCLUDE_LIST1"
								String[] optionalIndexedArgumentValues = optionalIndexedArgumentsSortedMap.get( optionalIndexedArgumentKey ); // z.B. "12;13;30"
								optionalIndexedArgumentValue = "";
								for( int l = 0; l < optionalIndexedArgumentValues.length; l++ ) {
									// Values in Form von Array-Eintraegen der optionalen indizierten Argumente werden durch ";" getrennt ausgegeben
									optionalIndexedArgumentValue = optionalIndexedArgumentValue + optionalIndexedArgumentValues[l] + ((optionalIndexedArgumentValues.length > 1 && l < (optionalIndexedArgumentValues.length - 1)) ? ";" : "");
								}
								System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZGenerateTAL with optional argument: " + optionalIndexedArgumentKey + " = " + optionalIndexedArgumentValue );
							}
						} else {
							System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZGenerateTAL with optional argument: " + optionalArgumentName + " => no indexed argument was specified (using default value)" );
						}

					} else {
						optionalArgumentValue = "";
						// gibt es optionale (nicht indizierte) Argumente, die an PP �bergeben wurden ?
						if( getArg( optionalArgumentName ) != null ) {
							optionalArgumentsValues = extractValues( getArg( optionalArgumentName ) );
							for( int k = 0; k < optionalArgumentsValues.length; k++ ) {
								// Values in Form von Array-Eintraegen der optionalen (nicht indizierten) Argumente werden durch ";" getrennt ausgegeben
								optionalArgumentValue = optionalArgumentValue + optionalArgumentsValues[k] + ((optionalArgumentsValues.length > 1 && k < (optionalArgumentsValues.length - 1)) ? ";" : "");
							}
						} else {
							optionalArgumentValue = "null (using default value)";
						}

						System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZGenerateTAL with optional argument: " + optionalArgumentName + " = " + optionalArgumentValue );
					}

				}

				// Obligatorische Argumentpr�fung auf Vorhandensein von nicht optionalen sowie �berz�hligen Argumenten
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// Extract ACTION_LIST and SUB_ACTION_LIST
				// Bei SubActions ist "|" das Trennzeichen, z.B. "swDeploy | swDeleteTA" 
				actionAndSubActionList = readIndexedParameter( "ACTION" );

				for( String str : Arrays.asList( actionAndSubActionList ) ) {
					int indexOfPipe = str.indexOf( "|" );

					if( indexOfPipe == -1 ) {
						// Die ohne "|" sind die Actions
						actions.add( str.trim() );
					} else {
						// Die mit "|" sind die SubActions im Format "Actions | SubActions"
						subActions.add( str.trim() );
					}
				}

				// Include, Exclude und ForceExecution der Actions
				for( int i = 0; i < actionAndSubActionList.length; i++ ) {
					if( getArg( "INCLUDE_LIST" + (i + 1) ) != null )
						actionIncludeList.put( actionAndSubActionList[i], checkAndModifyEcuHexArray( extractValues( getArg( "INCLUDE_LIST" + (i + 1) ) ) ) );
					if( getArg( "EXCLUDE_LIST" + (i + 1) ) != null )
						if( getArg( "INCLUDE_LIST" + (i + 1) ) != null )
							throw new PPExecutionException( "EXCLUDE_LIST" + (i + 1) + " + INCLUDE_LIST" + (i + 1) + PB.getString( "psdz.parameter.ParameterfehlerGegenseitigerAusschluss" ) );
						else
							actionExcludeList.put( actionAndSubActionList[i], checkAndModifyEcuHexArray( extractValues( getArg( "EXCLUDE_LIST" + (i + 1) ) ) ) );
					if( getArg( "FORCE_EXECUTION" + (i + 1) ) != null ) {
						if( !(getArg( "FORCE_EXECUTION" + (i + 1) ).equalsIgnoreCase( "TRUE" ) || getArg( "FORCE_EXECUTION" + (i + 1) ).equalsIgnoreCase( "FALSE" )) )
							throw new PPExecutionException( PB.getString( "psdz.parameter.ParameterfehlerForceExecution" ) );
						forceExecutionList.put( actionAndSubActionList[i], getArg( "FORCE_EXECUTION" + (i + 1) ).equalsIgnoreCase( "TRUE" ) ? new Boolean( true ) : new Boolean( false ) );
					}
				}

				// CHECK_TALLINE_STATUS
				if( getArg( "CHECK_TALLINE_STATUS" ) != null ) {
					if( getArg( "CHECK_TALLINE_STATUS" ).equalsIgnoreCase( "TRUE" ) )
						bCheckTalLineStatus = true;
					else if( getArg( "CHECK_TALLINE_STATUS" ).equalsIgnoreCase( "FALSE" ) )
						bCheckTalLineStatus = false;
					else
						throw new PPExecutionException( "CHECK_TALLINE_STATUS " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DEBUG
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
					else
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DEBUG_PERFORM
				if( getArg( "DEBUG_PERFORM" ) != null ) {
					if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "TRUE" ) )
						bDebugPerform = true;
					else if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "FALSE" ) )
						bDebugPerform = false;
					else
						throw new PPExecutionException( "DEBUG_PERFORM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DUPLICATE_ECU_SOLL_LIST
				if( getArg( "DUPLICATE_ECU_SOLL_LIST" ) != null ) {
					if( getArg( "DUPLICATE_ECU_SOLL_LIST" ).equalsIgnoreCase( "TRUE" ) )
						bDuplicateECUSollList = true;
					else if( getArg( "DUPLICATE_ECU_SOLL_LIST" ).equalsIgnoreCase( "FALSE" ) )
						bDuplicateECUSollList = false;
					else
						throw new PPExecutionException( "DUPLICATE_ECU_SOLL_LIST " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// SHOW_DURATION
				if( getArg( "SHOW_DURATION" ) != null ) {
					try {
						iShowDuration = new Integer( extractValues( getArg( "SHOW_DURATION" ) )[0] ).intValue();
					} catch( NumberFormatException e ) {
						throw new PPExecutionException( "SHOW_DURATION " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
					}
				}

				// SHOW_TAL
				if( getArg( "SHOW_TAL" ) != null ) {
					if( getArg( "SHOW_TAL" ).equalsIgnoreCase( "TRUE" ) )
						showTAL = true;
					else if( getArg( "SHOW_TAL" ).equalsIgnoreCase( "FALSE" ) )
						showTAL = false;
					else
						throw new PPExecutionException( "SHOW_TAL " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// SWT_ACTION_LIST
				swtActionList = readIndexedParameter( "SWT_ACTION" );

				// Include und Exclude der SWT Actions
				for( int i = 0; i < swtActionList.length; i++ ) {
					try {
						CascadeSWTCategories.parse( swtActionList[i] );
					} catch( Exception e ) {
						throw new PPExecutionException( PB.getString( "psdz.parameter.ParameterfehlerSWTAction" ) );
					}
					if( getArg( "SWT_INCLUDE_LIST" + (i + 1) ) != null )
						swtActionIncludeList.put( swtActionList[i], checkAndModifyEcuHexArray( extractValues( getArg( "SWT_INCLUDE_LIST" + (i + 1) ) ) ) );
					if( getArg( "SWT_EXCLUDE_LIST" + (i + 1) ) != null )
						if( getArg( "SWT_INCLUDE_LIST" + (i + 1) ) != null )
							throw new PPExecutionException( "SWT_EXCLUDE_LIST" + (i + 1) + " + SWT_INCLUDE_LIST" + (i + 1) + PB.getString( "psdz.parameter.ParameterfehlerGegenseitigerAusschluss" ) );
						else
							swtActionExcludeList.put( swtActionList[i], checkAndModifyEcuHexArray( extractValues( getArg( "SWT_EXCLUDE_LIST" + (i + 1) ) ) ) );
				}

				// VEHICLE_STATE_INCLUDE_LIST
				if( getArg( "VEHICLE_STATE_INCLUDE_LIST" ) != null ) {
					vehicleStateIncludeList = checkAndModifyEcuHexArray( extractValues( getArg( "VEHICLE_STATE_INCLUDE_LIST" ) ) );
				}

				// VEHICLE_STATE_EXCLUDE_LIST
				if( getArg( "VEHICLE_STATE_EXCLUDE_LIST" ) != null ) {
					vehicleStateExcludeList = checkAndModifyEcuHexArray( extractValues( getArg( "VEHICLE_STATE_EXCLUDE_LIST" ) ) );

					// Darf nur zusammen mit der IncludeList definiert werden, wenn ein '*' Platzhalter in einer Liste ist
					if( getArg( "VEHICLE_STATE_INCLUDE_LIST" ) != null ) {
						// Kleiner Fehlercheck?
						if( (vehicleStateIncludeList.length > 1 && vehicleStateExcludeList.length > 1) || (vehicleStateIncludeList[0].equalsIgnoreCase( "*" ) && vehicleStateExcludeList[0].equalsIgnoreCase( "*" )) )
							throw new PPExecutionException( PB.getString( "psdz.parameter.ParameterfehlerVehicleStateList" ) );

						// includeList mit '*' ?
						if( vehicleStateIncludeList[0].equalsIgnoreCase( "*" ) )
							cPlatzhalter = 'i';

						// excludeList mit '*' ?
						if( vehicleStateExcludeList[0].equalsIgnoreCase( "*" ) )
							cPlatzhalter = 'e';
					}
				}

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			// Performancemessung Start
			if( bDebugPerform )
				System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGenerateTAL PP START" );

			// Devices holen
			try {
				psdz = getPr�flingLaufzeitUmgebung().getDeviceManager().getPSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			try {
				userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// requestSWTStatus soll dann belegt werden, wenn FSC Aktivit�ten bef�llt sind oder wenn keine Actions gesetzt sind
			if( actionAndSubActionList != null && actionAndSubActionList.length != 0 ) {
				for( String strAction : actionAndSubActionList ) {
					if( strAction.equalsIgnoreCase( CascadeTACategories.FSC_BACKUP.toString() ) || strAction.equalsIgnoreCase( CascadeTACategories.FSC_DEPLOY.toString() ) ) {
						bCheckSWTStatus = true;
						break;
					}
				}
			} else {
				bCheckSWTStatus = true;
			}

			// Informiere den Benutzer
			userDialog.setDisplayProgress( true );
			userDialog.getProgressbar().setIndeterminate( true );

			// Berechnung der ECU Maske, bei Verwendung eines Platzhalters '*' bei der vehicleStateIncludeList oder der vehicleStateExcludeList
			switch( cPlatzhalter ) {
			// include Liste mit '*'
				case 'i':
					List<String> vehicleStateExcludeListAsList = Arrays.asList( vehicleStateExcludeList );

					for( int i = 0; i < 256; i++ ) {
						if( !vehicleStateExcludeListAsList.contains( Integer.toHexString( i ) ) && !vehicleStateExcludeListAsList.contains( Integer.toHexString( i ).toUpperCase() ) && !STAR_IGNORE_LIST_AS_LIST.contains( Integer.toHexString( i ) ) )
							maskList.add( Integer.toHexString( i ) );
					}
					vehicleStateIncludeList = (String[]) maskList.toArray( new String[maskList.size()] );
					vehicleStateExcludeList = null;

					if( bDebug )
						System.out.println( "PSdZ ID:" + testScreenID + ", MaskList bei IncludeList: " + maskList.toString() );

					break;

				// exclude Liste mit '*'
				case 'e':
					List<String> vehicleStateIncludeListAsList = Arrays.asList( vehicleStateIncludeList );

					for( int i = 0; i < 256; i++ ) {
						if( !vehicleStateIncludeListAsList.contains( Integer.toHexString( i ) ) && !vehicleStateIncludeListAsList.contains( Integer.toHexString( i ).toUpperCase() ) && !STAR_IGNORE_LIST_AS_LIST.contains( Integer.toHexString( i ) ) )
							maskList.add( Integer.toHexString( i ) );
					}
					vehicleStateExcludeList = (String[]) maskList.toArray( new String[maskList.size()] );
					vehicleStateIncludeList = null;

					if( bDebug )
						System.out.println( "PSdZ ID:" + testScreenID + ", MaskList bei ExcludeList: " + maskList.toString() );

					break;

				// tue nichts...
				default:
			}

			try {

				// TAL Generierung
				userDialog.displayMessage( PB.getString( "psdz.ud.TALGenerierung" ), ";    " + PB.getString( "psdz.ud.TALGenerierunglaeuft" ), -1 );

				// TAL-Filter bef�llen, es m�ssen hier alle Kategorien in den Filter aufgenommen werden
				// Kategorien, die nicht im Parameter ACTION[1..N] aufgef�hrt sind, sollen verboten
				// werden.
				CascadeTALFilter myTALFilter = new CascadeTALFilter();
				Object[] allCategories = CascadeTACategories.getAllCategories().toArray();

				// nur wenn ACTIONS und/oder SUBACTIONS �bergeben werden...
				// Hintergrund: Wird nur eine SUBACTION �ber die PP �bergeben, m�ssen s�mtliche Kategorien dennoch durchlaufen werden, damit der TAL-Filter korrekt bef�llt wird. Andernfalls w�rden f�r den Fall, dass nur SUBACTIONS �bergeben werden
				// f�r die restlichen ACTIONS die TA-Kategorien im TAL-Filter nicht bef�llt werden (-> empty) und so diese ACTIONS erlaubt werden, obwohl diese ausgefiltert werden m�ssen. 
				if( actions.size() > 0 || subActions.size() > 0 ) {
					for( int i = 0; i < allCategories.length; i++ ) {

						if( bDebug )
							System.out.println( "PSdZ ID:" + testScreenID + ", PSdZGenerateTAL: Categories Loop Counter: " + i + ", Category: " + ((CascadeTACategories) allCategories[i]).toString() );
						CascadeFilteredTACategory myCategory = new CascadeFilteredTACategory( (CascadeTACategories) allCategories[i] );
						// Treffer in der Liste der Actions?
						boolean filteredActionFound = false;

						for( int j = 0; j < actionAndSubActionList.length; j++ ) {
							// Beinhaltet die Action eine Subaction z.B. "swDeploy | swDeleteTA", dann �bergehe diese Action
							if( actionAndSubActionList[j].indexOf( "|" ) != -1 ) {
								continue;
							}

							// analysiere die Actions
							if( allCategories[i].toString().equalsIgnoreCase( actionAndSubActionList[j] ) ) {
								filteredActionFound = true;

								// ForceExecutionList?
								if( forceExecutionList.get( actionAndSubActionList[j] ) != null ) {
									myCategory.setForceExecution( ((Boolean) forceExecutionList.get( actionAndSubActionList[j] )).booleanValue() );
								}

								// Gibt es zu der Category eine Include-Liste?
								if( actionIncludeList.get( actionAndSubActionList[j] ) != null ) {
									// Manual Parameter hier entfernt, weil es gibt die zus�tzlichen PP f�r codieren / programmieren
									if( bDebug ) {
										StringBuffer sbECUAdresses = new StringBuffer();
										for( int iCount = 0; iCount < ((String[]) actionIncludeList.get( actionAndSubActionList[j] )).length; iCount++ ) {
											sbECUAdresses.append( ((String[]) actionIncludeList.get( actionAndSubActionList[j] ))[iCount] + " " );
										}
										System.out.println( "PSdZ ID:" + testScreenID + ", PSdZGenerateTAL: Action: " + actionAndSubActionList[j] + ", ECU count: " + ((String[]) actionIncludeList.get( actionAndSubActionList[j] )).length + ", ECU HEX addresses included ( 0x " + sbECUAdresses.toString().toUpperCase() + "), ForceExecution-Flag: " + myCategory.isForceExecution() );
									}
									myCategory.addEcusToList( (String[]) actionIncludeList.get( actionAndSubActionList[j] ) );
									myCategory.setBlacklist( false );
								} else if( actionExcludeList.get( actionAndSubActionList[j] ) != null ) {
									if( bDebug ) {
										StringBuffer sbECUAdresses = new StringBuffer();
										for( int iCount = 0; iCount < ((String[]) actionExcludeList.get( actionAndSubActionList[j] )).length; iCount++ ) {
											sbECUAdresses.append( ((String[]) actionExcludeList.get( actionAndSubActionList[j] ))[iCount] + " " );
										}
										System.out.println( "PSdZ ID:" + testScreenID + ", PSdZGenerateTAL: Action: " + actionAndSubActionList[j] + ", ECU count: " + ((String[]) actionExcludeList.get( actionAndSubActionList[j] )).length + ", ECU HEX addresses excluded ( 0x " + sbECUAdresses.toString().toUpperCase() + "), ForceExecution-Flag: " + myCategory.isForceExecution() );
									}
									myCategory.addEcusToList( (String[]) actionExcludeList.get( actionAndSubActionList[j] ) );
									myCategory.setBlacklist( true );
								} else {
									if( bDebug ) {
										System.out.println( "PSdZ ID:" + testScreenID + ", PSdZGenerateTAL: Action: " + actionAndSubActionList[j] + ", ECU count: 256" + ", all possible ECU HEX addresses are included, ForceExecution-Flag: " + myCategory.isForceExecution() );
									}
									myCategory.setAffectAllEcus( true );
									myCategory.setBlacklist( false );
								}
							}
						}
						if( !filteredActionFound ) {
							myCategory.setAffectAllEcus( true );
							myCategory.setBlacklist( true );
						}

						myTALFilter.addFilteredTACategory( myCategory );

					}
				}

				// SUB-ACTION "swDeploy" Behandlung. Aktuell nur relevant bzgl. BEGU, d.h. "swDeploy" kennt Subkategorien z.B. in der Form "swDeploy | swDeleteTA"
				for( int i = 0; i < subActions.size(); i++ ) {
					if( bDebug )
						System.out.println( "PSdZ ID:" + testScreenID + ", PSdZGenerateTAL: Sub-Categories Loop Counter: " + i + ", Sub-Category: " + subActions.get( i ) );

					// Die Sub-Action hat z.B. das Format: "swDeploy | swDeleteTA", ermittle parentName und subName und erzeuge eine neue CascadeFilteredTASubCategory
					int indexOfPipe = subActions.get( i ).indexOf( "|" );
					//String subActionParentName = subActions.get( i ).substring( 0, indexOfPipe ).trim();
					String subActionSubName = subActions.get( i ).substring( indexOfPipe + 1 ).trim();

					CascadeFilteredTASubCategory mySubCategory = new CascadeFilteredTASubCategory( CascadeTACategories.SW_DEPLOY, subActionSubName );

					// Treffer in der Liste der SubActions?
					boolean filteredSubActionFound = false;

					// die Liste der gesamten Actions durchgehen und die Teildaten des CascadeFilteredTASubCategory Objektes bei Treffern bef�llen
					for( int j = 0; j < actionAndSubActionList.length; j++ ) {
						// Die Action beinhaltet keine Subaction z.B. "swDeploy | swDeleteTA", dann mache mit der n�chsten Action weiter
						if( actionAndSubActionList[j].indexOf( "|" ) == -1 ) {
							continue;
						}

						// analysiere die Actions
						if( subActions.get( i ).equalsIgnoreCase( actionAndSubActionList[j] ) ) {
							filteredSubActionFound = true;

							// ForceExecutionList?
							if( forceExecutionList.get( actionAndSubActionList[j] ) != null ) {
								mySubCategory.setForceExecution( ((Boolean) forceExecutionList.get( actionAndSubActionList[j] )).booleanValue() );
							}

							// Gibt es zu der Sub-Category eine Include-Liste?
							if( actionIncludeList.get( actionAndSubActionList[j] ) != null ) {
								if( bDebug ) {
									StringBuffer sbECUAdresses = new StringBuffer();
									for( int iCount = 0; iCount < ((String[]) actionIncludeList.get( actionAndSubActionList[j] )).length; iCount++ ) {
										sbECUAdresses.append( ((String[]) actionIncludeList.get( actionAndSubActionList[j] ))[iCount] + " " );
									}
									System.out.println( "PSdZ ID:" + testScreenID + ", PSdZGenerateTAL: Sub-Action: " + actionAndSubActionList[j] + ", ECU count: " + ((String[]) actionIncludeList.get( actionAndSubActionList[j] )).length + ", ECU HEX addresses included ( 0x " + sbECUAdresses.toString().toUpperCase() + "), ForceExecution-Flag: " + mySubCategory.isForceExecution() );
								}
								mySubCategory.addEcusToList( (String[]) actionIncludeList.get( actionAndSubActionList[j] ) );
								mySubCategory.setBlacklist( false );
							} else if( actionExcludeList.get( actionAndSubActionList[j] ) != null ) {
								if( bDebug ) {
									StringBuffer sbECUAdresses = new StringBuffer();
									for( int iCount = 0; iCount < ((String[]) actionExcludeList.get( actionAndSubActionList[j] )).length; iCount++ ) {
										sbECUAdresses.append( ((String[]) actionExcludeList.get( actionAndSubActionList[j] ))[iCount] + " " );
									}
									System.out.println( "PSdZ ID:" + testScreenID + ", PSdZGenerateTAL: Sub-Action: " + actionAndSubActionList[j] + ", ECU count: " + ((String[]) actionExcludeList.get( actionAndSubActionList[j] )).length + ", ECU HEX addresses excluded ( 0x " + sbECUAdresses.toString().toUpperCase() + "), ForceExecution-Flag: " + mySubCategory.isForceExecution() );
								}
								mySubCategory.addEcusToList( (String[]) actionExcludeList.get( actionAndSubActionList[j] ) );
								mySubCategory.setBlacklist( true );
							} else {
								if( bDebug ) {
									System.out.println( "PSdZ ID:" + testScreenID + ", PSdZGenerateTAL: Sub-Action: " + actionAndSubActionList[j] + ", ECU count: 256" + ", all possible ECU HEX addresses are included, ForceExecution-Flag: " + mySubCategory.isForceExecution() );
								}
								mySubCategory.setAffectAllEcus( true );
								mySubCategory.setBlacklist( false );
							}
						}
					}
					if( !filteredSubActionFound ) {
						mySubCategory.setAffectAllEcus( true );
						mySubCategory.setBlacklist( true );
					}

					myTALFilter.addFilteredTASwDeploySubCategory( mySubCategory );
				}

				// SWT Action Behandlung
				if( swtActionList.length > 0 ) {
					for( int j = 0; j < swtActionList.length; j++ ) {
						CascadeFilteredSWTCategory mySWTCategory = new CascadeFilteredSWTCategory( CascadeSWTCategories.parse( swtActionList[j] ) );
						// Gibt es zu der Category eine Include-Liste?
						if( swtActionIncludeList.get( swtActionList[j] ) != null ) {

							String[] s = (String[]) swtActionIncludeList.get( swtActionList[j] );
							String[] l = new String[s.length];
							for( int i = 0; i < s.length; i++ ) {
								l[i] = "1";
							}

							mySWTCategory.addAppIDsToList( (String[]) swtActionIncludeList.get( swtActionList[j] ) );
							mySWTCategory.addUpgrdIDXsToList( (String[]) l );

							mySWTCategory.setBlacklist( false );
						} else if( swtActionExcludeList.get( swtActionList[j] ) != null ) {

							String[] s = (String[]) swtActionExcludeList.get( swtActionList[j] );
							String[] l = new String[s.length];
							for( int i = 0; i < s.length; i++ ) {
								l[i] = "1";
							}

							mySWTCategory.addAppIDsToList( (String[]) swtActionExcludeList.get( swtActionList[j] ) );
							mySWTCategory.addUpgrdIDXsToList( (String[]) l );
							mySWTCategory.setBlacklist( true );
						}
						myTALFilter.addFilteredSWTCategory( mySWTCategory );
					}
				}

				// jetzt den Filter setzen
				if( vehicleStateIncludeList != null ) {
					// Fall IncludeList
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGenerateTAL 'psdz.generateAndStoreTAL' START" );
					}
					psdz.generateAndStoreTAL( vehicleStateIncludeList, false, myTALFilter, bCheckSWTStatus, true );
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGenerateTAL 'psdz.generateAndStoreTAL' ENDE" );
					}

				} else if( vehicleStateExcludeList != null ) {
					// Fall ExcludeList
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGenerateTAL 'psdz.generateAndStoreTAL' START" );
					}
					psdz.generateAndStoreTAL( vehicleStateExcludeList, true, myTALFilter, bCheckSWTStatus, true );
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGenerateTAL 'psdz.generateAndStoreTAL' ENDE" );
					}

				} else {
					// �bergebe leere Liste
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGenerateTAL 'psdz.generateAndStoreTAL' START" );
					}

					String[] emptyList = {};
					psdz.generateAndStoreTAL( emptyList, true, myTALFilter, bCheckSWTStatus, true );
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGenerateTAL 'psdz.generateAndStoreTAL' ENDE" );
					}

				}

				// Abfrage ob TALLine auf Not Executable
				if( bCheckTalLineStatus ) {

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGenerateTAL 'psdz.getTAL' START" );
					}

					CascadeTAL casTAL = psdz.getTAL();

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGenerateTAL 'psdz.getTAL' ENDE" );
					}
					CascadeTALLine[] myLines = casTAL.getTalLines();
					for( int i = 0; i < myLines.length; i++ ) {

						if( myLines[i].getStatus().equals( CascadeExecutionStatus.NOTEXECUTABLE ) ) {
							result = new Ergebnis( "PSdZGenerateTAL", "PSdZ", myLines[i].getBaseVariantName().toString() + ", Adr.: 0x" + Integer.toHexString( myLines[i].getDiagnosticAddress().intValue() ).toUpperCase(), "CHECK TALLINE STATUS", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", DE ? "Status der TAL-Line ist \"nicht ausf�hrbar\"" : "Status of TAL line is \"not executable\"", "", Ergebnis.FT_NIO );

							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}
				}

				// Dupliziere wenn parametriert und vorhanden die "installedECUList_Soll" nach "installedECUList_Ist"
				// Hintegrund: In der L7 / LK ist das SG: HU-CIC Gateway und es wird dann die TAL abgebrochen, wenn das HU-CIC in der installedECUList_Soll steht, jedoch nicht in der installedECUList_Ist
				// Bugfix: Dupliziere wenn parametriert und vorhanden die "installedECUList_Soll" nach "installedECUList_Ist"
				if( bDuplicateECUSollList ) {

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGenerateTAL 'psdz.getTALReader' START" );
					}

					CascadeTALReader casTALReader = psdz.getTALReader();
					StringBuffer sbPSdZTAL = new StringBuffer( casTALReader.getPsdzTALasXML() );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGenerateTAL 'psdz.getTALReader' ENDE" );
					}

					// Manipuliere.., wenn beide Listen vorhanden sind
					int indexECUListIst = sbPSdZTAL.indexOf( NAME_ECU_LIST_IST );
					int indexECUListSoll = sbPSdZTAL.indexOf( NAME_ECU_LIST_SOLL );

					if( indexECUListIst != -1 && indexECUListSoll != -1 ) {
						String sollListe = sbPSdZTAL.substring( indexECUListSoll - 1, sbPSdZTAL.indexOf( "/" + NAME_ECU_LIST_SOLL ) + 2 + NAME_ECU_LIST_SOLL.length() ); // ermittle SollListe als String
						String newIstListe = sollListe.replaceAll( NAME_ECU_LIST_SOLL, NAME_ECU_LIST_IST );
						String[] psdzDeviceParams = psdz.getDeviceParams();
						boolean bEnableSVTTraces = (psdzDeviceParams != null && psdzDeviceParams.length > 3) ? Boolean.valueOf( psdzDeviceParams[3] ).booleanValue() : false;

						psdz.setPSdZTAL( sbPSdZTAL.replace( indexECUListIst - 1, sbPSdZTAL.indexOf( "/" + NAME_ECU_LIST_IST ) + 2 + NAME_ECU_LIST_IST.length(), newIstListe ).toString() ); // ersetze Istliste mit SollListe und setze dies als neue TAL 
						if( bEnableSVTTraces ) {
							psdz.logPSdZContent( PSdZ.TAL + "_WITH_DUPLICATED_ECU_SOLL_LIST", psdz.getPSdZTALasXML() ); // XML dokumentieren
						}
						// Doku APDM, dass manipuliert wurde
						result = new Ergebnis( "PSdZGenerateTAL", "PSdZ", "DUPLICATE_ECU_SOLL_LIST:=TRUE", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
				}

				// TAL anzeigen und best�tigen?
				if( showTAL ) {
					// UserDialog zur�cksetzen
					userDialog.reset();

					StringBuffer sbTAL = new StringBuffer();
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGenerateTAL 'psdz.getTAL' START" );
					}

					CascadeTAL casTAL = psdz.getTAL();

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGenerateTAL 'psdz.getTAL' ENDE" );
					}

					CascadeTALLine[] myLines = casTAL.getTalLines();
					for( int i = 0; i < myLines.length; i++ ) {
						sbTAL.append( PB.getString( "psdz.text.Steuergeraet" ) + myLines[i].getBaseVariantName() + ";" );
						sbTAL.append( PB.getString( "psdz.text.DiagAdresse" ) + Integer.toHexString( myLines[i].getDiagnosticAddress().intValue() ).toUpperCase() + PB.getString( "psdz.text.Dez" ) + myLines[i].getDiagnosticAddress() + ");" );

						for( int j = 0; j < myLines[i].getTALTACategories().length; j++ ) {
							CascadeTALTACategory myTALTACategory = myLines[i].getTALTACategories()[j];
							sbTAL.append( PB.getString( "psdz.text.Aktion" ) + myTALTACategory.getCategory().toString() + " Status: " + myTALTACategory.getStatus() + ";" );

							for( int k = 0; k < myTALTACategory.getTAs().length; k++ ) {
								CascadeTA myTA = myTALTACategory.getTAs()[k];
								sbTAL.append( PB.getString( "psdz.text.Prozessklasse" ) + myTA.getProcessClass() + " Version: (" + myTA.getMainVersion() + "." + myTA.getSubVersion() + "." + myTA.getPatchVersion() + ");" );
							}
						}
						sbTAL.append( ";" );
					}

					// Drucken und Abbruch eingebaut
					userDialog.setLineWrap( false );
					userDialog.setNoButtonText( PB.getString( "psdz.ud.ButtonCancel" ) );
					userDialog.setAuxiliaryButtonVisibilityMask( UserDialog.PRINT_BUTTON_MASK );
					int iSelection = userDialog.requestUserInputDigital( PB.getString( "psdz.ud.TALOK" ), sbTAL.toString(), iShowDuration );

					// Wurde die TAL-Anzeige durch den Benutzter abgebrochen? (Nein Button)
					if( iSelection == 2 ) {
						result = new Ergebnis( "PSdZGenerateTAL", "Userdialog", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", PB.getString( "werkerAbbruch" ), "", PB.getString( "pollingAbbruch" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					}
				}
			} catch( CascadePSdZArgumentException e ) {
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZGenerateTAL", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Argument" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( CascadePSdZConditionsNotCorrectException e ) {
				// Fehler abfangen und dokumentiern
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZGenerateTAL", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Bedingungen" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( CascadePSdZRuntimeException e ) {
				// Fehler abfangen und dokumentiern
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZGenerateTAL", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Laufzeit" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Freigabe der benutzten Devices
		if( psdz != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releasePSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
		try {
			getPr�flingLaufzeitUmgebung().releaseUserDialog();
		} catch( Exception e ) {
			if( e instanceof DeviceLockedException )
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "PSdZGenerateTAL", "PSdZ", "", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}
		setPPStatus( info, status, ergListe );
		// Performancemessung Ende
		if( bDebugPerform )
			System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZGenerateTAL PP ENDE" );

	}
}

/*
 * DiagMultiJobSingleResult Created on 16.06.04
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.Enumeration;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceIOException;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Implementierung der Pr�fprozedur, die per Diagnose das Ergebnis eines Jobs bewertet und anhand
 * dessen andere Jobs ausf�hren kann. Inzwischen kann auch nach betimmten Results gesucht werden.
 * 
 * @author Frank Weber, Thomas Eckardt
 * @version 8_0_F MK 25.04.2017	F-Version auf Basis von 6_0_F
 * @version 7_0_T MK 14.07.2016	Log Ausgaben erweitert wegen unerwartetem Laufzeitfehler 
 * @version 6_0_F te 17.02.2011	allowed ";" for HWT
 * @version 5_0_T te 17.02.2011	allowed ";" for HWT
 * @version 4_0_F fw 26.10.2007 added feature to deactivate cancel dialogue, corrected minmax check
 * @version 3_0_F fw 11.10.2006 added feature to search base job, hwt added, inverting added
 * @version 2_0_F fw 26.07.2004 checkargs reworked, minmax now are allow <0, jobresult added
 * @version 1_0_F fw 30.06.2004 inital creation
 */
public class DiagMultiJobSingleResult_8_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	
	//language
	private final boolean isEnglish = System.getProperty( "user.language" ).equalsIgnoreCase( "EN" );
	  

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagMultiJobSingleResult_8_0_F_Pruefprozedur() {
	}

	// legt die benutzbaren Verzweigungen abh�ngig vom Basisjob fest
	private String[] sJobArtMenge = { 	"MIN", // wird bei Unterschreitung der MIN-Grenze angezogen
										"MAX", // Wird bei �berschreitung des MAX-Wertes angezogen
										"CASE:", // legt equalsIgnoreCase mit nachfolgendem String zugrunde
										"DEFAULT" // standrad, falls kein CASE: zutrifft
	};

	private int iNumberJobs = 0;
	private int iNumberMaxJobs = 0;
	private int iNumberMinJobs = 0;
	private int iNumberCaseJobs = 0;
	private int iNumberDefaultJobs = 0;
	boolean bTrace = false;
	
	// private klasse, die einen Job bewertet/einordnet
	public class JobObject {
		public boolean isCase = false;
		public boolean isMin = false;
		public boolean isMax = false;
		public boolean isDefault = false;
		public boolean isBaseJob = false;
		public boolean isJobRes = false;
		public boolean isInvert=false;
		public String jobArt = null;
		private String caseString = null;
		public String jobName = null;
		public String jobPar = null;
		private double jobMin = 0;
		private double jobMax = 0;
		public String jobSGBD = null;
		public String jobResult = null;
		public long jobPause = 0;
		public String jobMaxString = "";
		public String jobMinString = "";
		public String jobAWT = "";

		// Konstruktor, bewertet die �bergebenen Parameter und teilt Job in die entsprechenden
		// KLassen ein
		public JobObject( String standardSGBD, String sgbd, String jobname, String par,
				String jobresult, String min, String max, String pause, String jobart, String isinvert, String awt ) throws PPExecutionException {
			if( bTrace )
				System.out.println( "constructor JobObject reached" );
			// sgbd
			if( sgbd == null || sgbd.equalsIgnoreCase( "" ) || sgbd.equalsIgnoreCase( "null" ) )
				jobSGBD = standardSGBD;
			else
				jobSGBD = sgbd;
			jobName = jobname;
			if( par == null || par.equalsIgnoreCase( "null" ) )
				jobPar = "";
			else
				jobPar = par;
			// jobres
			if( jobresult == null || jobresult.equalsIgnoreCase( "null" ) ) {
				jobResult = "";
				isJobRes = false;
			} else {
				jobResult = jobresult;
				isJobRes = true;
			}
			// Pause
			if( pause == null || pause.equalsIgnoreCase( "null" ) )
				jobPause = 0;
			else {
				try {
					jobPause = Long.parseLong( pause );
				} catch( NumberFormatException e ) {
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				}
			}
			// min
			if( min != null && min.equalsIgnoreCase( "null" ) == false ) {
				try {
					jobMin = Double.parseDouble( min );
					jobMinString = min;
				} catch( NumberFormatException e ) {
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				}
				if( jobResult == null )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			}
			// max
			if( max != null && max.equalsIgnoreCase( "null" ) == false ) {
				try {
					jobMax = Double.parseDouble( max );
					jobMaxString = max;
				} catch( NumberFormatException e ) {
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				}
				if( jobResult == null )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			}
			// jobart
			if( jobart.startsWith( "MIN" ) ) {
				isMin = true;
				jobArt = "MIN";

			}
			if( jobart.startsWith( "MAX" ) ) {
				isMax = true;
				jobArt = "MAX";
			}
			if( jobart.startsWith( "CASE:" ) ) {
				caseString = jobart.substring( 5 );
				if( caseString.equalsIgnoreCase( "" ) )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				isCase = true;
				jobArt = "CASE";
			}
			if( jobart.startsWith( "BASE" ) ) {
				isBaseJob = true;
				jobArt = "BASE";
				if( jobResult == null )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			}
			if( jobart.startsWith( "DEFAULT" ) ) {
				isDefault = true;
				jobArt = "DEFAULT";
			}
			//invert
			if( isinvert != null ) {
				isInvert=isinvert.equalsIgnoreCase( "TRUE" );
			}
			// awt
			if( awt != null ) {
				jobAWT=awt;
			}
			//end of constructor
			if( bTrace )
			System.out.println( "Parameter Konstruktor: " + jobSGBD + "-" + jobSGBD + "-" + jobname + "-" + jobPar 
					+ "-" + jobResult + "-" + jobMin + "-" + jobMax + "-" + jobPause + "-" + jobArt + "-" + isInvert
					+ "-" + jobAWT);
		}

		// �berpr�fen ob �bergebender String mit case ueberinstimmt
		public boolean checkCase( String checkString ) {
			if( isCase == false )
				return false;
			if (jobName.equalsIgnoreCase( "FALSE" )) return false;
			if( checkString.equalsIgnoreCase( caseString ) )
				return (!isInvert);
			else
				return (isInvert);
		}

		// �berpr�fen ob �bergenes Result nicht gr��er als max
		public boolean checkMax( String jobres ) {
			if( jobResult.equalsIgnoreCase( "" ) )
				return false;
			double jobDouble;
			try {
				jobDouble = Double.parseDouble( jobres );
			} catch( NumberFormatException e ) {
				return (!isInvert);
			}
			if( jobDouble > jobMax )
				return (isInvert);
			else
				return (!isInvert);
		}

		// �berpr�fen ob �bergenes Result nicht kleiner als min
		public boolean checkMin( String jobres ) {
			if( jobResult.equalsIgnoreCase( "" ) )
				return false;
			double jobDouble;
			try {
				jobDouble = Double.parseDouble( jobres );
			} catch( NumberFormatException e ) {
				return (!isInvert);
			}
			if( jobDouble < jobMin )
				return (isInvert);
			else
				return (!isInvert);
		}
		
		// -2 general error, -1 %N% not found, 0 SearchString not found, job has to be executed before
		public int searchResults( EdiabasProxyThread ed1, int start, int stop ) {
			int position = 0;
			if( jobResult.equalsIgnoreCase( "" ) )
				return -2;
			if (jobResult.indexOf( "%N%" )<0) return -1;
			String tempRes="";
			String returnValue="";
			for (int i=start;i<=stop;i++) {
				tempRes=jobResult.replaceAll("%N%", ""+i);
				try {
					returnValue=ed1.getDiagResultValue( tempRes.trim() );
				} catch (Exception e) {
					System.out.println(tempRes.trim()+": "+e.getMessage());
				}
				if (returnValue.equalsIgnoreCase( this.jobMinString )) return i;
			}
			return position;
		}
		
		// pause Job
		public void pause() {
			try {
				Thread.sleep( jobPause );
			} catch( InterruptedException e ) {
			}
		}
	}

	/**
	 * erzeugt eine neue Pruefprozedur, die die oben beschriebene Funktion implementiert.
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DiagMultiJobSingleResult_8_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName,
			Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "BASISJOB_MIN", "BASISJOB_MAX", "JOBMIN[1..N]", "JOBMAX[1..N]",
						  "JOBRESULT[1..N]", "BASISJOB_PAR", "JOBART[2..N]", "JOB[2..N]",
						  "JOBPAR[1..N]", "JOBPAUSE[1..N]", "JOBSGBD[1..N]", "BASISJOB_PAUSE",
						  "TIMEOUT", "BASISJOB_AWT", "JOBAWT[1..N]", "HWT",
						  "BASISJOB_INVERT", "JOBINVERT[1..N]",
						  "BASISJOB_SEARCHRESULTS_STARTNUMBER","BASISJOB_SEARCHRESULTS_STOPNUMBER",
						  "BASISJOB_EVALUATE","TRACE","CANCEL"};
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "BASISSGBD", "BASISJOB", "BASISJOB_RESULT", "JOBART1", "JOB1" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die
	 * parent-Methode aufgrund der offenen Anzahl an Results
	 */
	public boolean checkArgs() {
		int i, j;
		int maxJobPar = 0;
		int maxJobArt = 1;
		int maxJob = 1;
		int maxMin = 0;
		int maxMax = 0;
		int maxAwt = 0;
		int maxResult = 0;
		int maxInvert = 0;

		iNumberJobs = 0;
		iNumberMaxJobs = 0;
		iNumberMinJobs = 0;
		iNumberCaseJobs = 0;
		iNumberDefaultJobs = 0;

		try {
			// 1. Check: Sind alle requiredArgs gesetzt
			String[] requiredArgs = getRequiredArgs();
			for( i = 0; i < requiredArgs.length; i++ ) {
				if( getArg( requiredArgs[i] ) == null )
					return false;
				if( getArg( requiredArgs[i] ).equals( "" ) == true )
					return false;
			}
			// 2. Check: sonstige checks
			Enumeration enu = getArgs().keys();
			String givenkey;
			String temp;
			while( enu.hasMoreElements() ) {
				givenkey = (String) enu.nextElement();
				// falls nicht spezifizierter Parameter integriert
				if( (givenkey.startsWith( "JOBMIN" ) == false) && (givenkey.startsWith( "JOBMAX" ) == false) 
						&& (givenkey.equalsIgnoreCase( "BASISJOB_PAR" ) == false) && (givenkey.equalsIgnoreCase( "BASISJOB_PAUSE" ) == false) 
						&& (givenkey.equalsIgnoreCase( "BASISJOB_MIN" ) == false) && (givenkey.equalsIgnoreCase( "BASISJOB_MAX" ) == false) 
						&& (givenkey.equalsIgnoreCase( "TIMEOUT" ) == false) && (givenkey.equalsIgnoreCase( "BASISJOB_AWT" ) == false) 
						&& (givenkey.equalsIgnoreCase( "BASISJOB" ) == false) && (givenkey.equalsIgnoreCase( "BASISJOB_RESULT" ) == false) 
						&& (givenkey.equalsIgnoreCase( "BASISSGBD" ) == false) && (givenkey.startsWith( "JOB" ) == false)
						&& (givenkey.equalsIgnoreCase( "HWT" ) == false) && (givenkey.equalsIgnoreCase( "BASISJOB_INVERT" ) == false)
						&& (givenkey.equalsIgnoreCase( "BASISJOB_SEARCHRESULTS_STARTNUMBER" ) == false)
						&& (givenkey.equalsIgnoreCase( "BASISJOB_SEARCHRESULTS_STOPNUMBER" ) == false)
						&& (givenkey.equalsIgnoreCase( "BASISJOB_EVALUATE" ) == false
						&& (givenkey.equalsIgnoreCase( "TRACE" ) == false))&& (givenkey.equalsIgnoreCase( "CANCEL" ) == false)) {
					return false;

				} else {
					// nur BASISJOB_PAR,JOBPAR[1..N] d�rfen ";" enthalten
					if( (givenkey.equalsIgnoreCase( "BASISJOB_PAR" ) == false) && (givenkey.startsWith( "JOBPAR" ) == false) && (givenkey.equalsIgnoreCase( "HWT" ) == false) && (givenkey.equalsIgnoreCase( "AWT" ) == false))
						if( getArg( givenkey ).indexOf( ';' ) != -1 )
							return false;
					// TIMEOUT,PAUSEN m�ssen integer und >= 0 sein
					if( (givenkey.equalsIgnoreCase( "TIMEOUT" ) == true) || (givenkey.equalsIgnoreCase( "BASISJOB_PAUSE" ) == true) || (givenkey.startsWith( "JOBPAUSE" ) == true) ) {
						try {
							if( (Long.parseLong( getArg( givenkey ) )) < 0 )
								return false;
						} catch( NumberFormatException e ) {
							return false;
						}
					}
					// min und max m�ssen gr��er null sein
					/*
					 * if ( (givenkey.equalsIgnoreCase("BASISJOB_MIN") == true) ||
					 * (givenkey.equalsIgnoreCase("BASISJOB_MAX") == true) ||
					 * (givenkey.startsWith("MIN") == true) || (givenkey.startsWith("MAX") == true)) {
					 * try { if( (Double.parseDouble( getArg(givenkey) )) < 0) return false; } catch
					 * (NumberFormatException e) { return false; } }
					 */
					// Basisjob MAX muss gr��er gleich MIN sein
					if( givenkey.equalsIgnoreCase( "BASISJOB_MAX" ) )
						if( getArg( "BASISJOB_MIN" ) != null )
							if( Double.parseDouble( getArg( givenkey ) ) < Double.parseDouble( getArg( "BASISJOB_MIN" ) ) )
								return false;
					// MAX muss gr��er gleich MIN sein
					if( givenkey.startsWith( "JOBMAX" ) )
						if( getArg( "JOBMIN" + givenkey.substring( 6 ) ) != null )
							if( Double.parseDouble( getArg( givenkey ) ) < Double.parseDouble( getArg( "JOBMIN" + givenkey.substring( 6 ) ) ) )
								return false;
					// BASISJOB_SEARCHRESULTS_STOPNUMBER muss gr��er gleich BASISJOB_SEARCHRESULTS_STARTNUMBER sein
					if( givenkey.startsWith( "BASISJOB_SEARCHRESULTS_STOPNUMBER" ) ) {
						if( getArg( "BASISJOB_SEARCHRESULTS_STARTNUMBER" ) != null ) {
							if( Double.parseDouble( getArg( givenkey ) ) < Double.parseDouble( getArg( "BASISJOB_SEARCHRESULTS_STARTNUMBER" ) ) )
								return false;
						}
						else return false;
					}

					// JOBART muss mit bestimmten Prefixen beginnen
					if( givenkey.startsWith( "JOBART" ) == true ) {
						boolean keywordFound = false;
						for( int i1 = 0; i1 < sJobArtMenge.length; i1++ ) {
							if( getArg( givenkey ).startsWith( sJobArtMenge[i1] ) ) {
								keywordFound = true;
								this.iNumberJobs++;
								if( getArg( givenkey ).startsWith( "MIN" ) )
									this.iNumberMinJobs++;
								if( getArg( givenkey ).startsWith( "MAX" ) )
									this.iNumberMaxJobs++;
								if( getArg( givenkey ).startsWith( "CASE" ) )
									this.iNumberCaseJobs++;
								if( getArg( givenkey ).startsWith( "DEFAULT" ) )
									this.iNumberDefaultJobs++;
							}
						}
						if( keywordFound == false )
							return false;
					}
					// get max indizes
					if( givenkey.startsWith( "JOBPAR" ) == true ) {
						temp = givenkey.substring( 6 );
						try {
							j = Integer.parseInt( temp );
							if( j < 1 )
								return false;
							if( j > maxJobPar )
								maxJobPar = j;
						} catch( NumberFormatException e ) {
							return false;
						}
					} else if( givenkey.startsWith( "JOBART" ) == true ) {
						temp = givenkey.substring( 6 );
						try {
							j = Integer.parseInt( temp );
							if( j < 1 )
								return false;
							if( j > maxJobArt )
								maxJobArt = j;
						} catch( NumberFormatException e ) {
							return false;
						}
					} else if( givenkey.startsWith( "JOBPAUSE" ) == true ) {
						temp = givenkey.substring( 8 );
						try {
							j = Integer.parseInt( temp );
							if( j < 1 )
								return false;
							if( j > maxJob )
								maxJob = j;
						} catch( NumberFormatException e ) {
							return false;
						}
					} else if( givenkey.startsWith( "JOBSGBD" ) == true ) {
						temp = givenkey.substring( 7 );
						try {
							j = Integer.parseInt( temp );
							if( j < 1 )
								return false;
							if( j > maxJob )
								maxJob = j;
						} catch( NumberFormatException e ) {
							return false;
						}
					} else if( givenkey.startsWith( "JOBMIN" ) == true ) {
						temp = givenkey.substring( 6 );
						try {
							j = Integer.parseInt( temp );
							if( j < 1 )
								return false;
							if( j > maxMin )
								maxMin = j;
						} catch( NumberFormatException e ) {
							return false;
						}
					} else if( givenkey.startsWith( "JOBMAX" ) == true ) {
						temp = givenkey.substring( 6 );
						try {
							j = Integer.parseInt( temp );
							if( j < 1 )
								return false;
							if( j > maxMax )
								maxMax = j;
						} catch( NumberFormatException e ) {
							return false;
						}
					} else if( givenkey.startsWith( "JOBAWT" ) == true ) {
						temp = givenkey.substring( 6 );
						try {
							j = Integer.parseInt( temp );
							if( j < 1 )
								return false;
							if( j > maxAwt )
								maxAwt = j;
						} catch( NumberFormatException e ) {
							return false;
						}
					} else if( givenkey.startsWith( "JOBRESULT" ) == true ) {
						temp = givenkey.substring( 9 );
						try {
							j = Integer.parseInt( temp );
							if( j < 1 )
								return false;
							if( j > maxResult )
								maxResult = j;
						} catch( NumberFormatException e ) {
							return false;
						}
					} else if( givenkey.startsWith( "JOBINVERT" ) == true ) {
						temp = givenkey.substring( 9 );
						try {
							j = Integer.parseInt( temp );
							if( j < 1 )
								return false;
							if( j > maxInvert )
								maxInvert= j;
						} catch( NumberFormatException e ) {
							return false;
						}
					} else if( givenkey.startsWith( "JOB" ) == true ) {
						temp = givenkey.substring( 3 );
						try {
							j = Integer.parseInt( temp );
							if( j < 1 )
								return false;
							if( j > maxJob )
								maxJob = j;
						} catch( NumberFormatException e ) {
							return false;
						}
					}

				}
			}
			// jobart und job m�ssen gleiche anzahl haben
			if( maxJob != maxJobArt )
				return false;
			// alle anderen jobparameter d�rfen mit maximal gleicher Anzahl vorhanden sein
			if( maxJobPar > maxJob || maxMin > maxJob || maxMax > maxJob || maxAwt > maxJob || maxResult > maxJob )
				return false;
			// Tests bestanden, somit ok
			return true;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// Devices
		UserDialog ud = null;
		EdiabasProxyThread ed = null;

		// spezifische Variablen
		String temp = "";
		String temp1 = "";
		String sgbd = "";
		String job = "";
		String jobpar = "";
		String jobres = "";
		String hwt;
		
		// Kategorien f�r die Jobs
		JobObject baseJob = null;
		JobObject[] defaultJobs = null;
		JobObject[] maxJobs = null;
		JobObject[] minJobs = null;
		JobObject[] caseJobs = null;
		JobObject tempJob = null;
		// indizes f�r die einzelnen Kategorien
		int indexMax = 0;
		int indexMin = 0;
		int indexCase = 0;
		int indexDefault = 0;
		int searchResultsStart = 0;
		int searchResultsEnd = 0;
		int indexSearch = -1;

		// double d;
		long endeZeit, currentTime;
		int i;
		// sind jobs korrekt ausgef�hrt worden
		boolean executionMaxJobs = true;
		boolean executionMinJobs = true;
		boolean executionCaseJobs = true;
		boolean executionDefaultJobs = true;
		boolean caseJobMatched = false;
		boolean toBeRepeated = false;
		boolean useHWT = false;
		boolean useBaseSearch=false;
		boolean baseJobEvaluate=false;
		boolean useCancel= true;

		try {
			// Parameter holen
			try {
//				 Evaluate Base Job
				temp = getArg( "TRACE" );
				if( temp != null ) {
					bTrace=temp.equalsIgnoreCase( "TRUE" );
				}
				if( bTrace )
					System.out.println( "Checkargs reached" );
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				if( bTrace )
					System.out.println( "Checkargs passed" );
				if( bTrace )
					System.out.println( "Basisjob Parameter: " + getArg( "BASISSGBD" ) + "-" + getArg( "BASISSGBD" ) + "-" + getArg( "BASISJOB" ) + "-" + getArg( "BASISJOB_PAR" ) 
							+ "-" + getArg( "BASISJOB_RESULT" ) + "-" + getArg( "BASISJOB_MIN" ) + "-" + getArg( "BASISJOB_MAX" ) + "-" + getArg( "BASISJOB_PAUSE" ) + "-" + "BASE" 
							+ "-" + getArg( "BASISJOB_INVERT" ) + "-" + getArg( "BASISJOB_AWT" ));
				baseJob = new JobObject( getArg( "BASISSGBD" ), getArg( "BASISSGBD" ), getArg( "BASISJOB" ),
						getArg( "BASISJOB_PAR" ), getArg( "BASISJOB_RESULT" ), getArg( "BASISJOB_MIN" ), getArg( "BASISJOB_MAX" ),
						getArg( "BASISJOB_PAUSE" ), "BASE" , getArg( "BASISJOB_INVERT" ),getArg( "BASISJOB_AWT" ));
				maxJobs = new JobObject[this.iNumberMaxJobs];
				minJobs = new JobObject[this.iNumberMinJobs];
				caseJobs = new JobObject[this.iNumberCaseJobs];
				defaultJobs = new JobObject[this.iNumberDefaultJobs];
				if( bTrace )
					System.out.println( "BaseJob done, initiating other jobs" );
				for( i = 1; i <= this.iNumberJobs; i++ ) {
					tempJob = new JobObject( getArg( "BASISSGBD" ), getArg( "JOBSGBD" + i ), getArg( "JOB" + i ),
							getArg( "JOBPAR" + i ), getArg( "JOBRESULT" + i ), getArg( "JOBMIN" + i ), getArg( "JOBMAX" + i ),
							getArg( "JOBPAUSE" + i ), getArg( "JOBART" + i ),getArg( "JOBINVERT" + i ),getArg( "JOBAWT" + i ) );
					if( tempJob.isMin ) {
						minJobs[indexMin] = tempJob;
						indexMin++;
					}
					if( tempJob.isMax ) {
						maxJobs[indexMax] = tempJob;
						indexMax++;
					}
					if( tempJob.isCase ) {
						caseJobs[indexCase] = tempJob;
						indexCase++;
					}
					if( tempJob.isDefault ) {
						defaultJobs[indexDefault] = tempJob;
						indexDefault++;
					}
				}
				if( bTrace )
					System.out.println( "other jobs init done" );
				// Timeout
				temp = getArg( "TIMEOUT" );
				if( temp == null ) {
					endeZeit = System.currentTimeMillis();
					toBeRepeated = false;
				} else {
					endeZeit = System.currentTimeMillis() + Long.parseLong( temp );
					toBeRepeated = true;
				}

				// Hinweistext
				hwt = getArg( "HWT" );
				if( hwt == null ) {
					hwt = "";
				}
				else {
					useHWT  = true;
					hwt = PB.getString( hwt );
				}
					
				// ResultSearching
				temp = getArg( "BASISJOB_SEARCHRESULTS_STARTNUMBER" );
				temp1 = getArg( "BASISJOB_SEARCHRESULTS_STOPNUMBER" );
				if( temp != null && temp1 != null ) {
					searchResultsStart=Integer.parseInt( temp );
					if (temp1.startsWith( "@" )) searchResultsEnd=searchResultsStart-1;
					searchResultsEnd=Integer.parseInt( temp1 );
					useBaseSearch=true;
				}
				
				// Evaluate Base Job
				temp = getArg( "BASISJOB_EVALUATE" );
				if( temp != null ) {
					baseJobEvaluate=temp.equalsIgnoreCase( "TRUE" );
				}
				
				// use cancel
				temp = getArg( "CANCEL" );
				if( temp != null ) {
					useCancel=(!temp.equalsIgnoreCase( "FALSE" ));
				}

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			try {
				ed = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();// new
			} catch( Exception e1 ) {
				if( isEnglish ) {
					throw new DeviceIOException( "error getting EDIABAS device: " + e1.getMessage() );
				} else {
					throw new DeviceIOException( "Fehler beim Holen des Device EDIABAS: " + e1.getMessage() );
				}
			}

			// Die Ausf�hrungs- und Analyseschleife
			currentTime = System.currentTimeMillis();
			do {
				ergListe = new Vector(); // Neu f�r jeden Durchlauf
				executionMaxJobs = true;
				executionMinJobs = true;
				executionCaseJobs = true;
				executionDefaultJobs = true;
				// Ausf�hrung Basisjob
				try {
					sgbd = baseJob.jobSGBD;
					job = baseJob.jobName;
					jobpar = baseJob.jobPar;
					jobres = baseJob.jobResult;
					if( bTrace )
						System.out.println( "Executing base job" );
					temp = ed.executeDiagJob( sgbd, job, jobpar, "" );
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					} else {
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
				} catch( ApiCallFailedException e ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} catch( EdiabasResultNotFoundException e ) {
					if( e.getMessage() != null )
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}
				baseJob.pause(); 
				if( bTrace )
					System.out.println( "Starting analysis base job" );
				//perform baseSearch if needed
				if (useBaseSearch) {
					// replace end index in case of @ command with result value
					if (searchResultsEnd<searchResultsStart) {
						try {
							String tempString = ed.getDiagResultValue( getArg( "BASISJOB_SEARCHRESULTS_STOPNUMBER" ).substring( 1 ) );
							searchResultsEnd = Integer.parseInt( tempString );
						} catch (Exception e1) {
							if (isEnglish) result = new Ergebnis( "@Fehler", "Parameter BASISJOB_SEARCHRESULTS_STOPNUMBER", "", "", "", "", "", "", "", "0", "", "", "", "Fehler beim Parsen von @Result f�r Suche", "", Ergebnis.FT_NIO_SYS );
							else result = new Ergebnis( "@Fehler", "Parameter BASISJOB_SEARCHRESULTS_STOPNUMBER", "", "", "", "", "", "", "", "0", "", "", "", "Error parsing @Result for search", "", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						}
						
					}
					int tempInt = baseJob.searchResults( ed, searchResultsStart, searchResultsEnd);
					switch(tempInt) {
						case -2: {
							result = new Ergebnis( "SEARCHFAILURE", "EDIABAS", baseJob.jobSGBD, baseJob.jobName, baseJob.jobPar, "Failure in Search", "No such JobResult", baseJob.jobMinString, "", "0", "", "", "", "No such JobResult", "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						}	
						case -1: {
							result = new Ergebnis( "SEARCHFAILURE", "EDIABAS", baseJob.jobSGBD, baseJob.jobName, baseJob.jobPar, "Failure in Search", "No %N% in JobResult", baseJob.jobMinString, "", "0", "", "", "", "No %N% in JobResult", "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						}
						case 0:{
							result = new Ergebnis( "SEARCHFAILURE", "EDIABAS", baseJob.jobSGBD, baseJob.jobName, baseJob.jobPar, "Failure in Search", "Pattern not found", baseJob.jobMinString, "", "0", "", "", "", "Pattern not found", "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						}
						default:
							result = new Ergebnis( "SEARCH", "EDIABAS", baseJob.jobSGBD, baseJob.jobName, baseJob.jobPar, "Search Result", ""+tempInt, baseJob.jobMinString, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
							indexSearch=tempInt;
					}
				}
				// Analyse
				if (useBaseSearch) jobres = jobres.replaceAll( "%N%", ""+indexSearch );
				temp = ed.getDiagResultValue( jobres );
				if (useHWT) {
					try {
						if (ud == null) {
							ud = getPr�flingLaufzeitUmgebung().getUserDialog();
							ud.setAllowCancel( useCancel );
							ud.displayMessage( "HWT", hwt,-1 );
						}
						else {
							if (!(ud.isCancelled())) {
								ud.setAllowCancel( useCancel );
								ud.displayMessage( "HWT", hwt,-1 );
							}
							else endeZeit = System.currentTimeMillis();
						}
					} catch( Exception e ) {
						if( isEnglish ) {
							result = new Ergebnis( "USERDIAGLOGERROR", "USERDIALOG", "", "", "", "", "", "", "", "0", "", "", "", "Error getting HWT Dialogue: "+e.getMessage(), "", Ergebnis.FT_NIO );
						}
						else {
							result = new Ergebnis( "USERDIAGLOGERROR", "USERDIALOG", "", "", "", "", "", "", "", "0", "", "", "", "Fehler beim Holen HWT Userdialogs: "+e.getMessage(), "", Ergebnis.FT_NIO );
						}
						ergListe.add( result );
						throw new PPExecutionException();

					}
				}
				
				
				// maxjobs
				if( !baseJob.checkMax( temp ) ) {
					if( bTrace )
						System.out.println( "executing max jobs" );
					executionMaxJobs = true;
					for( i = 0; i < indexMax && executionMaxJobs; i++ ) {
						// execute maxjob
						try {
							sgbd = maxJobs[i].jobSGBD;
							job = maxJobs[i].jobName;
							jobpar = maxJobs[i].jobPar;
							if (useBaseSearch && (jobpar.indexOf( "%N%" )>=0)) {
								jobpar = jobpar.replaceAll( "%N%", ""+indexSearch );
							}
							jobres = maxJobs[i].jobResult;
							temp1 = ed.executeDiagJob( sgbd, job, jobpar, jobres );
							if( temp1.equals( "OKAY" ) == false ) {
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp1, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								throw new PPExecutionException();
							} else {
								result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp1, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
							}
						} catch( ApiCallFailedException e ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						} catch( EdiabasResultNotFoundException e ) {
							if( e.getMessage() != null )
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						}
						if( maxJobs[i].isJobRes ) {
							temp1 = ed.getDiagResultValue( jobres );
							// Bewertung der Ausf�hrung der Jobs
							if( !maxJobs[i].checkMax( temp1 ) || !maxJobs[i].checkMin( temp1 ) ) {
								executionMaxJobs = false;
								result = new Ergebnis( "MAXFAILURE"+i, "EDIABAS", maxJobs[i].jobSGBD, maxJobs[i].jobName, maxJobs[i].jobPar, "MAXFAILURE", temp1, maxJobs[i].jobMinString, maxJobs[i].jobMaxString, "0", "", "", maxJobs[i].jobAWT, PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
								ergListe.add( result );
							}
						}
						maxJobs[i].pause(); 
					}
				}
				// minjobs
				if( !baseJob.checkMin( temp ) ) {
					if( bTrace )
						System.out.println( "executing min jobs" );
					executionMinJobs = true;
					for( i = 0; i < indexMin && executionMinJobs; i++ ) {
						// execute minjob
						try {
							sgbd = minJobs[i].jobSGBD;
							job = minJobs[i].jobName;
							jobpar = minJobs[i].jobPar;
							if (useBaseSearch && (jobpar.indexOf( "%N%" )>=0)) {
								jobpar = jobpar.replaceAll( "%N%", ""+indexSearch );
							}
							jobres = minJobs[i].jobResult;
							temp1 = ed.executeDiagJob( sgbd, job, jobpar, jobres );
							if( temp1.equals( "OKAY" ) == false ) {
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp1, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								throw new PPExecutionException();
							} else {
								result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp1, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
							}
						} catch( ApiCallFailedException e ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						} catch( EdiabasResultNotFoundException e ) {
							if( e.getMessage() != null )
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						}
						if( minJobs[i].isJobRes ) {
							temp1 = ed.getDiagResultValue( jobres );
							// Bewertung der Ausf�hrung der Jobs
							if( !minJobs[i].checkMax( temp1 ) || !minJobs[i].checkMin( temp1 ) ) {
								executionMinJobs = false;
								result = new Ergebnis( "MINFAILURE"+i, "EDIABAS", minJobs[i].jobSGBD, minJobs[i].jobName, minJobs[i].jobPar, "MINFAILURE", temp1, minJobs[i].jobMinString, minJobs[i].jobMaxString, "0", "", "", minJobs[i].jobAWT, PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
								ergListe.add( result );
							}
						}
						minJobs[i].pause(); 
					}
				}
				// caseJobs
				executionCaseJobs = true;
				caseJobMatched = false;
				for( i = 0; i < indexCase && executionCaseJobs; i++ ) {
					// execute casejob
					if( caseJobs[i].checkCase( temp ) ) {
						if( bTrace )
							System.out.println( "executing case jobs" );
						caseJobMatched = true;
						try {
							sgbd = caseJobs[i].jobSGBD;
							job = caseJobs[i].jobName;
							jobpar = caseJobs[i].jobPar;
							if (useBaseSearch && (jobpar.indexOf( "%N%" )>=0)) {
								jobpar = jobpar.replaceAll( "%N%", ""+indexSearch );
							}
							jobres = caseJobs[i].jobResult;
							temp1 = ed.executeDiagJob( sgbd, job, jobpar, jobres );
							if( temp1.equals( "OKAY" ) == false ) {
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp1, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								throw new PPExecutionException();
							} else {
								result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp1, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
							}
						} catch( ApiCallFailedException e ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						} catch( EdiabasResultNotFoundException e ) {
							if( e.getMessage() != null )
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						}
						if( caseJobs[i].isJobRes ) {
							temp1 = ed.getDiagResultValue( jobres );
							// Bewertung der Ausf�hrung der Jobs
							if( !caseJobs[i].checkMax( temp1 ) || !caseJobs[i].checkMin( temp1 ) ) {
								executionCaseJobs = false;
								result = new Ergebnis( "CASEFAILURE"+i, "EDIABAS", caseJobs[i].jobSGBD, caseJobs[i].jobName, caseJobs[i].jobPar, "CASEFAILURE", temp1, caseJobs[i].jobMinString, caseJobs[i].jobMaxString, "0", "", "", caseJobs[i].jobAWT, PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
								ergListe.add( result );
							}
						}
						caseJobs[i].pause(); 
					}
				}
				
				// defaultjobs
				if( caseJobMatched == false ) {
					for( i = 0; i < indexDefault && executionDefaultJobs; i++ ) {
						// execute minjob
						if( bTrace )
							System.out.println( "executing default jobs" );
						executionDefaultJobs = true;
						try {
							sgbd = defaultJobs[i].jobSGBD;
							job = defaultJobs[i].jobName;
							jobpar = defaultJobs[i].jobPar;
							if (useBaseSearch && (jobpar.indexOf( "%N%" )>=0)) {
								jobpar = jobpar.replaceAll( "%N%", ""+indexSearch );
							}
							jobres = defaultJobs[i].jobResult;
							temp1 = ed.executeDiagJob( sgbd, job, jobpar, jobres );
							if( temp1.equals( "OKAY" ) == false ) {
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp1, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								throw new PPExecutionException();
							} else {
								result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp1, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
							}
						} catch( ApiCallFailedException e ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						} catch( EdiabasResultNotFoundException e ) {
							if( e.getMessage() != null )
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						}
						if( defaultJobs[i].isJobRes ) {
							temp1 = ed.getDiagResultValue( jobres );
							// Bewertung der Ausf�hrung der Jobs
							if( (!defaultJobs[i].checkMax( temp1 ) || !defaultJobs[i].checkMin( temp1 ))) {
								executionDefaultJobs = false;
								result = new Ergebnis( "DEFAULTFAILURE"+i, "EDIABAS", defaultJobs[i].jobSGBD, defaultJobs[i].jobName, defaultJobs[i].jobPar, "DEFAULTFAILURE", temp1, defaultJobs[i].jobMinString, defaultJobs[i].jobMaxString, "0", "", "", defaultJobs[i].jobAWT, PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
								ergListe.add( result );
							}
						}
						defaultJobs[i].pause(); 
					}
				}
				currentTime = System.currentTimeMillis();
			} while( // timeout
			(currentTime < endeZeit) && toBeRepeated &&
			// execution errors
			(!(executionDefaultJobs && executionCaseJobs && executionMinJobs && executionMaxJobs) || 
					(!baseJob.checkMax( temp ) || !baseJob.checkMin( temp ))) );

			// Fehler bei der Ausf�hrung der Jobkategorien
			if( executionMaxJobs == false ) {
				// max error
				//result = new Ergebnis( "MAXFAILURE", "EDIABAS", maxJobs[i].jobSGBD, maxJobs[i].jobName, maxJobs[i].jobPar, "MAXFAILURE", temp, maxJobs[i].jobMinString, maxJobs[i].jobMaxString, "0", "", "", awt, PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
				//ergListe.add( result );
				throw new PPExecutionException();
			} else if( executionMinJobs == false ) {
				//result = new Ergebnis( "MINFAILURE", "EDIABAS", minJobs[i].jobSGBD, minJobs[i].jobName, minJobs[i].jobPar, "MINFAILURE", temp, minJobs[i].jobMinString, minJobs[i].jobMaxString, "0", "", "", awt, PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
				//ergListe.add( result );
				throw new PPExecutionException();
			} else if( executionCaseJobs == false ) {
				//result = new Ergebnis( "CASEFAILURE", "EDIABAS", caseJobs[i].jobSGBD, caseJobs[i].jobName, caseJobs[i].jobPar, "CASEFAILURE", temp, caseJobs[i].jobMinString, caseJobs[i].jobMaxString, "0", "", "", awt, PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
				//ergListe.add( result );
				throw new PPExecutionException();
			} else if( executionDefaultJobs == false ) {
				//result = new Ergebnis( "DEFAULTFAILURE", "EDIABAS", defaultJobs[i].jobSGBD, defaultJobs[i].jobName, defaultJobs[i].jobPar, "DEFAULTFAILURE", temp, defaultJobs[i].jobMinString, defaultJobs[i].jobMaxString, "0", "", "", awt, PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
				//ergListe.add( result );
				throw new PPExecutionException();
			}
			// fehler beim basisjob (timeout vorausgesetzt)
			else if( baseJobEvaluate &&
					(!baseJob.checkMax( temp ) || !baseJob.checkMin( temp )) ) {
				result = new Ergebnis( "BASEJOBFAILURE", "EDIABAS", baseJob.jobSGBD, baseJob.jobName+" (BASEJOB)", baseJob.jobPar, baseJob.jobResult, temp, baseJob.jobMinString, baseJob.jobMaxString, "0", "", "", baseJob.jobAWT, PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				if (ud.isCancelled()) result = new Ergebnis( "Frage", "Userdialog", "", "", "", "", "", "", "", "0", hwt, PB.getString( "werkerAbbruch" ), "", PB.getString("pollingAbbruch"), "", Ergebnis.FT_NIO ); 
				ergListe.add( result );
				throw new PPExecutionException();
			}
			// IO Fall
			else
				result = new Ergebnis( "MULTIJOB", "EDIABAS", baseJob.jobSGBD, baseJob.jobName, baseJob.jobPar, "MULTIJOB", temp, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Freigabe der benutzten Devices
		if( ud != null) {
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		// Status setzen
		setPPStatus( info, status, ergListe );

	}

}

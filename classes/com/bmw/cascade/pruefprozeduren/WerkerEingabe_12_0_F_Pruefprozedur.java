/*
 * WerkerEingabe_x_y_F_Pruefprozedur.java Created on 06.03.03
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;

/**
 * Implementierung der Pr�fprozedur, die auf eine Eingabe durch den Benutzer wartet.
 * 
 * @author M�ller
 * @version 0_0_1  06.03.2003 MM Implementierung
 * @version 0_0_2  17.05.2005 RG Optionale Parameter MIN und MAX dazu. Wenn nur MIN angegeben ist
 *          wird ein Stringvergleich durchgefuehrt.
 * @version 3_0_T  01.03.2006 RG Angabe mehrerer Mins m�glich f�r Stringvergleich, �berpr�fung der
 *          Argumente jetzt in checkArgs() <br>
 * @version 4_0_T  29.05.2006 TB Erlaube bei allen Parametern den @-Referenzparameter <BR> 
 * @version 5_0_F  10.07.2007 RG, TB Merge beider T-Versionen und F-Release <BR>
 * @version 6_0_T  29.01.2015 UP, Erweiterung um startsWith() und endsWith() <BR> 
 * @version 7_0_T  20.05.2015 MS, Erweiterung optinalen Parameter CHECKIPSQ und Eingaber der Werkerpr�fung zu checken <BR>
 * @version 8_0_F  xx.xx.2015 MS F-Version <BR>
 * @version 9_0_T  12.07.2015 TB LOP 2098, opt. UD_TIMEOUT Parameter bei den Benutzereingaben. <BR>       
 * @version 10_0_F 14.07.2015 TB F-Version. <BR>    
 * @version 11_0_T 27.07.2015 MK Bugfix: Bei einem Timout kam es zu einem unerwartetem Laufzeitfehler<BR>  
 * @version 12_0_F 28.07.2015 MK F-Version. <BR> 
 */
public class WerkerEingabe_12_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public WerkerEingabe_12_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public WerkerEingabe_12_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "NUMBERS_ONLY", "MIN", "MAX", "PREFIX", "SUFFIX", "CHECKIPSQ", "UD_TIMEOUT" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "AWT" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 */
	@Override
	public boolean checkArgs() {
		boolean ok;
		String[] temp;

		try {
			//NUMBERS_ONLY muss TRUE oder FALSE sein
			if( getArg( getOptionalArgs()[0] ) != null ) {
				if( (getArg( getOptionalArgs()[0] ).equalsIgnoreCase( "TRUE" ) == false) && (getArg( getOptionalArgs()[0] ).equalsIgnoreCase( "FALSE" ) == false) ) {
					throw new Exception( "Check of arguments failed. NUMBERS_ONLY accepts only values 'true' or 'false'!" );
				}
			}

			//wenn ein MAX vorhanden ist muss auch ein MIN da sein, dann jedoch nur eins
			if( getArg( "MAX" ) != null ) {
				if( getArg( "MIN" ) == null ) {
					throw new Exception( "Check of arguments failed. A specified MAX value requires a MIN value!" );
				}
				temp = extractValues( getArg( "MIN" ) );
				if( temp.length != 1 ) {
					throw new Exception( "Check of arguments failed. A specified MAX value requires only one MIN value!" );
				}
			}

			//wenn ein PREFIX gesetzt ist, m�ssen MIN, MAX und SUFFIX leer sein
			if( getArg( "PREFIX" ) != null ) {
				if( getArg( "MIN" ) != null || getArg( "MAX" ) != null || getArg( "SUFFIX" ) != null ) {
					throw new Exception( "Check of arguments failed. If PREFIX is specified, MIN, MAX and SUFFIX must remain empty!" );
				}
			}

			//wenn ein SUFFIX gesetzt ist, m�ssen MIN, MAX und PREFIX leer sein
			if( getArg( "SUFFIX" ) != null ) {
				if( getArg( "MIN" ) != null || getArg( "MAX" ) != null || getArg( "PREFIX" ) != null ) {
					throw new Exception( "Check of arguments failed. If SUFFIX is specified, MIN, MAX and PREFIX must remain empty!" );
				}
			}

			ok = super.checkArgs();
			return ok;
		} catch( Exception e ) {
			e.printStackTrace();
			return false;
		} catch( Throwable e ) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String text;
		String input;
		int iInput = 0;
		boolean numbers = false;
		boolean udInUse = false;

		String[] sMins = null;
		String minString = "";
		String sMax = "";
		String param1 = "";
		String[] prefixes = null;
		String[] suffixes = null;
		int j;
		int iMin = 0;
		int iMax = 0;
		int ud_timeout = 0;
		boolean bewerten = false;
		boolean minOnly = false;
		boolean minAndMax = false;
		boolean evalPrfxs = false;
		boolean evalSufxs = false;
		boolean equals = false;
		boolean matchFound = false;
		boolean checkIPSQ = false;
		int retry = 3;
		boolean check;

		try {
			//Parameter holen
			try {
				if( checkArgs() == false ) {
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				}
				text = PB.getString( extractValues( getArg( getRequiredArgs()[0] ) )[0] );
				if( getArg( getOptionalArgs()[0] ) != null ) {
					if( getArg( getOptionalArgs()[0] ).equalsIgnoreCase( "TRUE" ) ) {
						numbers = true;
					}
				}

				//MIN
				if( getArg( "MIN" ) != null ) {
					sMins = extractValues( getArg( "MIN" ) );
					minString = sMins[0];
					for( j = 1; j < sMins.length; j++ ) {
						minString = minString + ";" + sMins[j];
					}

				}
				//MAX
				if( getArg( "MAX" ) != null ) {
					sMax = extractValues( getArg( "MAX" ) )[0];
					try {
						iMax = Integer.parseInt( sMax );
					} catch( NumberFormatException nfe ) {
						throw new PPExecutionException( "NumberFormatException (MAX)" );
					}
					try {
						iMin = Integer.parseInt( sMins[0] );
					} catch( NumberFormatException nfe ) {
						throw new PPExecutionException( "NumberFormatException (MIN)" );
					}
				}

				//bewerten oder nicht
				if( getArg( "MAX" ) != null ) {
					if( getArg( "MIN" ) != null ) {
						bewerten = true;
						minAndMax = true;
					}
				} else {
					if( getArg( "MIN" ) != null ) {
						bewerten = true;
						minOnly = true;
					}
				}

				//PREFIX
				if( getArg( "PREFIX" ) != null ) {
					prefixes = extractValues( getArg( "PREFIX" ) );
					bewerten = true;
					evalPrfxs = true;
				}

				//SUFFIX
				if( getArg( "SUFFIX" ) != null ) {
					suffixes = extractValues( getArg( "SUFFIX" ) );
					bewerten = true;
					evalSufxs = true;
				}

				// Parameter CHECKIPSQ pr�fen
				if( getArg( "CHECKIPSQ" ) != null ) {
					if( getArg( "CHECKIPSQ" ).equalsIgnoreCase( "FALSE" ) )
						checkIPSQ = false;
					if( getArg( "CHECKIPSQ" ).equalsIgnoreCase( "TRUE" ) )
						checkIPSQ = true;
				}

				// UD_TIMEOUT
				if( getArg( "UD_TIMEOUT" ) != null ) {
					try {
						ud_timeout = Integer.parseInt( extractValues( getArg( "UD_TIMEOUT" ) )[0] );
					} catch( NumberFormatException e ) {
						// Nothing
					}
				}

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null ) {
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				} else {
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				}
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//Devices holen
			try {
				if( numbers ) {
					input = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputNumber( PB.getString( "Eingabe" ), text, ud_timeout > 0 ? ud_timeout / 1000 : 0 );
				} else {
					if( checkIPSQ ) {
						do {
							input = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInput( PB.getString( "Eingabe" ), text, ud_timeout > 0 ? ud_timeout / 1000 : 0 );
							check = checkPrueferName( input );
							if( !check ) {
								getPr�flingLaufzeitUmgebung().getUserDialog().requestStatusMessage( "Pruefernr", "Pruefernummer ung�ltig!", ud_timeout > 0 ? ud_timeout / 1000 : 0 );
								retry--;
							}
						} while( !check && (retry != 0) );
						if( !check ) {
							result = new Ergebnis( "Status", "checkPrueferName", input, "", "", "Check PrueferName WRONG", "" + check, "", "", "", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
							stopViaHttpRequest();
						}

						// Pruefername per Variable verf�gbar machen
						getPr�flingLaufzeitUmgebung().setPruefstandVariable( VariableModes.PERSISTENT, "CURRENT_PRUEFER_NAME", input );

					} else {
						input = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInput( PB.getString( "Eingabe" ), text, ud_timeout > 0 ? ud_timeout / 1000 : 0 );
					}
				}
				udInUse = true;
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//Ergebnisparameter 1 setzen
			param1 = numbers ? "numbers" : "";

			if( input==null||input.equalsIgnoreCase( "" ) ) {
				result = new Ergebnis( "INPUT", "Userdialog", param1, "", "", "", "", "", "", "0", text, "", "", "", "", Ergebnis.FT_NIO );
				status = STATUS_EXECUTION_ERROR;
				ergListe.add( result );
			}

			if( bewerten == true ) {
				//es soll generell bewertet werden
				if( minOnly == true ) {
					//Stringvergleich
					for( j = 0; j < sMins.length; j++ ) {
						if( sMins[j].equalsIgnoreCase( input ) == true ) {
							equals = true;
							break;
						}
					}

					if( equals == true ) {
						//Strings stimmen ueberein
						result = new Ergebnis( "INPUT", "Userdialog", param1, "", "", "INPUT", "" + input, minString, "", "0", text, "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );

					} else {
						//Strings stimmen nicht ueberein
						result = new Ergebnis( "INPUT", "Userdialog", param1, "", "", "INPUT", "" + input, minString, "", "0", text, "", "", "", PB.getString( "toleranzFehler1" ), Ergebnis.FT_NIO );

						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				}
				if( minAndMax == true ) {
					//Vergleich auf MIN MAX
					try {
						iInput = Integer.parseInt( input );
					} catch( NumberFormatException nfe ) {
						result = new Ergebnis( "INPUT", "Userdialog", param1, "", "", "INPUT", input, "", "", "0", text, "", "", "", PB.getString( "numberFormat" ), Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					}
					if( (iInput < iMin) || (iInput > iMax) ) {
						result = new Ergebnis( "INPUT", "Userdialog", param1, "", "", "INPUT", "" + iInput, "" + iMin, "" + iMax, "0", text, "", "", "", PB.getString( "toleranzFehler3" ), Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					} else {
						result = new Ergebnis( "INPUT", "Userdialog", param1, "", "", "INPUT", "" + iInput, "" + iMin, "" + iMax, "0", text, "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
				}
				if( evalPrfxs == true ) {
					//Vergleich des Pr�fixes
					for( j = 0; j < prefixes.length; j++ ) {
						minString += prefixes[j] + "[...], ";
						if( input.startsWith( prefixes[j] ) ) {
							matchFound = true;
							break;
						}
					}
					if( !matchFound ) {
						result = new Ergebnis( "INPUT", "Userdialog", param1, "", "", "INPUT", "" + input, minString.substring( 0, minString.length() - 2 ), "", "0", text, "", "", "", PB.getString( "toleranzFehler3" ), Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					} else {
						input = input.substring( prefixes[j].length() );
						result = new Ergebnis( "INPUT", "Userdialog", param1, "", "", "INPUT", "" + input, "", "", "0", text, "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
				}
				if( evalSufxs == true ) {
					//Vergleich des Suffixes
					matchFound = false;
					for( j = 0; j < suffixes.length; j++ ) {
						minString += "[...]" + suffixes[j] + ", ";
						if( input.endsWith( suffixes[j] ) ) {
							matchFound = true;
							break;
						}
					}
					if( !matchFound ) {
						result = new Ergebnis( "INPUT", "Userdialog", param1, "", "", "INPUT", "" + input, minString.substring( 0, minString.length() - 2 ), "", "0", text, "", "", "", PB.getString( "toleranzFehler3" ), Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					} else {
						input = input.substring( 0, (input.length() - suffixes[j].length()) );
						result = new Ergebnis( "INPUT", "Userdialog", param1, "", "", "INPUT", "" + input, "", "", "0", text, "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
				}
			} else {
				//es soll auch ohne Bewertung in APDM dokumentiert werden
				result = new Ergebnis( "INPUT", "Userdialog", param1, "", "", "INPUT", "" + input, "", "", "0", text, "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		//Devices freigeben
		if( udInUse == true ) {
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		//Status setzen
		setPPStatus( info, status, ergListe );

	}

	private void stopViaHttpRequest() throws IOException {
		String url = "http://localhost:8086/testscreen-" + (getPr�flingLaufzeitUmgebung().getNumber() + 1) + "?canceltest=true"; // ist
		// Multi-Instanz-f�hig

		URL obj = new URL( url );
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setConnectTimeout( 15000 ); // in ms
		con.setRequestMethod( "GET" ); // ist eigentlich Default

		BufferedReader in = new BufferedReader( new InputStreamReader( con.getInputStream() ) );
		String inputLine;
		StringBuffer response = new StringBuffer();
		while( (inputLine = in.readLine()) != null )
			response.append( inputLine );
		in.close();

		con.disconnect();
	}

	/**
	 * �berpr�fung der Pruefernamens aktuell in Form einer simplen
	 * L�ngen�berpr�fung
	 * 
	 * @param prueferName
	 *            , als Eingangsparameter
	 * @return true, wenn die L�nge des prueferNamen 12 oder 13 stellig ist
	 */
	private boolean checkPrueferName( final String prueferName ) {
		// aktuell ein simpler Check, L�nge des PrueferNamen muss entweder 12
		// od. 13 Zeichen sein
		return prueferName.length() == 12 || prueferName.length() == 13;
	}

}

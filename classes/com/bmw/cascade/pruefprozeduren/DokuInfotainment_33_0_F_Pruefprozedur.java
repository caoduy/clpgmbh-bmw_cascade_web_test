/*
 * DokuInfotainment_X_X_X_Pruefprozedur.java
 *
 * Created on 17.04.2008
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.File;
import java.io.PrintWriter;
import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.util.dom.DomPruefstand;

/**
 * Erzeugt den Domdatensatz f�r Telefons, SDARS, usw. 
 *
 * @author Daniel Frey
 * @version V1_0_F  23.04.2009 FD Initialversion
 * @version V2_0_F  08.06.2009 FD Phone und TPA immer in eigenem File
 * @version V3_0_T  13.07.2009 FD Backup File INFOTAINMENT.DAT (incl. DOKU_PARTITION/HistorySplit)
 * @version V4_0_F  13.07.2009 FD F-Version
 * @version V5_0_T  08.09.2009 FD Buchstabe f�r TPA Files O -> V
 * @version V6_0_F  13.07.2009 FD F-Version 
 * @version V7_0_T  08.10.2009 FD Optional keinen DS erstellen
 *                                in Results # vorangestellt  
 * @version V8_0_F  08.10.2009 FD F-Version
 * @version V9_0_T  02.11.2009 FD TPA: �nderung des XML Schemas
 *                                     Seriennummer hat jetzt 18 Zeichen und mu� nicht mehr von Hex nach Int umgerechnet werden  
 * @version V10_0_F 02.11.2009 FD F-Version
 * @version V11_0_T 13.11.2009 FD TPA Schemaversion 0501, Rest 0400
 * @version V12_0_F 13.11.2009 FD F-Version
 * @version V13_0_T 08.12.2009 FD partNumber -> partNo
 * @version V14_0_F 08.12.2009 FD F-Version
 * @version V15_0_T 25.09.2014 TB SDARS Erweiterung "packageId" und "pellet" abw�rtskompatibel mitdokumentieren, DOM Schema ist mit der Erweiterung: 5.02, ohne Check der neuen Felder - L�ngen sind variabel, adapt. Parametercheck, Generics <BR>
 * @version V16_0_F 25.09.2014 TB F-Version<BR>
 * @version V17_0_T 03.05.2015 TB Erweiterung f�r die Erfassung der reprogrammierbaren SIM Karte "euiccId", DOM Schema ist mit der Erweiterung: 5.03 <BR>
 * @version V18_0_F 17.09.2014 MK F-Version<BR>
 * @version V19_0_T 03.08.2016 KW Erweiterung f�r die Erfassung der SIM Karte "glonassiccId", DOM Schema ist mit der Erweiterung: 5.04 <BR>
 * @version V20_0_T 29.09.2016 KW F-Version<BR>
 * @version V21_0_T 12.10.2016 TB BugFix Parametercheck korrigiert, wenn die Glonass Icc Id's fehlen<BR>
 * @version V22_0_F 12.10.2016 TB F-Version<BR>
 * @version V23_0_T 10.01.2017 TB LOP 2147 Algorithmus beim Datumscheck im SG ist fehlerhaft ab 2017 (Hintergrund: Bsp.: HEADUNIT_SN=MM01DFH2772127 enth�lt an der 7 Stelle codiert das Jahr H entspricht hier 2017)<BR>
 * @version V24_0_F 10.01.2017 TB F-Version<BR>
 * @version V25_0_T 18.01.2017 KW Wenn 19 Nullen und ein F beim Einganswert f�r die EuICCId �bergeben werden, der Key f�r die EuICCId nicht an DOM geschrieben werden. LOP 2114<BR>
 * @version V26_0_F 16.03.2017 KW F-Version<BR>
 * @version V26_1_F 20.03.2017 KW Reaktivieren v24
 * @version V27_0_T 22.03.2017 KW Wenn 19 Nullen und ein F beim Einganswert f�r die EuICCId �bergeben werden, der Key f�r die EuICCId nicht an DOM geschrieben werden. LOP 2114<BR>
 * @version V28_0_F 05.04.2017 KW F-Version, LOP 2182 Nach R�ckmeldung von der QMT via T. Teepe kann der Datumscheck in der PP DokuInfotainment komplett deaktiviert werden. 
                                  Hinweise: Parameter muss aus Abw�rtskompatibilit�tsgr�nden erhalten bleiben. Weiterhin muss die online Doku aktualisiert werden. vgl. auch LOP 2146<BR>
 * @version V29_0_T 10.07.2017 KW Verbesserung des xml Schema Formats. <BR>  
 * @version V30_0_T 21.07.2017 CW Zugriff mit Pr�fstands�bergreifenden @-Operator funktioniert nicht beim SMART-Server. <BR>
 * @version V31_0_F 21.07.2017 CW Freigabe 30_0_T. <BR>    
 * @version V32_0_T 02.11.2017 CW Zugriff mit Pr�fstands�bergreifenden @-Operator hatte einen Fehler. Es wurde versucht auf Ergebnisname@Pr�fschrittname.Prueflingname zuzugreifen.
 * 								  Jetzt wird korrekt auf Ergebnisname@Prueflingname.Pr�fschrittname zugegriffen. <BR>  
 * @version V33_0_F 03.11.2017 CW Freigabe 32_0_T. <BR>                                                              
 * */
public class DokuInfotainment_33_0_F_Pruefprozedur extends AbstractBMWPruefprozedur 
{
    /** */
    static final long serialVersionUID = 1L;    
    
    /**
     * Default path used for local documentation (history) of datasets
     */
    private static final String DEFAULT_DOKU_LOCAL_PATH = "C:/";

    /**
     * Property defining the path used for local documentation (history) of datasets.
     * PB.getString() is used.
     */
    private static final String DOKU_LOCAL_PATH_PROPERTY = "casTracePath";

    /**
     * Prefix for the local filename (history) in partition mode
     */
    private static final String DOKU_LOCAL_PREFIX = "INFOTAINMENT";

    /**
     * extension for the local filename (history) in partition mode
     */
    private static final String DOKU_LOCAL_EXTENSION = "DAT";
   
   
    /**
     * DefaultKonstruktor, nur fuer die Deserialisierung
     */
    public DokuInfotainment_33_0_F_Pruefprozedur() {}

    /**
    * erzeugt eine neue Pruefprozedur.
    * @param pruefling Klasse des zugeh. Pr�flings
    * @param pruefprozName Name der Pr�fprozedur
    * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
    */
    public DokuInfotainment_33_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) 
    {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
      
        fillCheckTypes();
    }
   
    /** Bef�llt m_checkTypes
     * Map Inputparameter String -> Integer Konstante
     */
    private void fillCheckTypes()
    {
        if (m_checkTypes!=null) return;

        m_checkTypes=new HashMap<String, Integer>();
        m_checkTypes.put("FALSE", new Integer(NO_CHECK));
        m_checkTypes.put("NOYEAR", new Integer(CHECK_WITHOUT_YEAR));
        m_checkTypes.put("TRUE", new Integer(FULL_CHECK));
    }

    /** Keinen Check der Nummer durchf�hren */
    static final int NO_CHECK=0;
    /** Check durchf�hren, aber Jahreszahl nicht pr�fen */
    static final int CHECK_WITHOUT_YEAR=1;
    /** Check durchf�hren, und Jahreszahl pr�fen (max +-2 Jahre zu heute) */
    static final int FULL_CHECK=2;

    /** M�gliche Arten, die Seriennummer zu pr�fen
     * Map Inputparameter String -> Integer Konstante */
    private HashMap<String, Integer> m_checkTypes=null;

    /** initialsiert die Argumente */
    @Override
	protected void attributeInit() 
    {
        super.attributeInit();
    }

    /** liefert die zwingend erforderlichen Argumente 
     * 
     * Reihenfolge
     * _RESULTNAME
     * _TESTSTEPNAME
     * mu� eingehalten werden. 
     * 
     * */
    @Override
	public String[] getRequiredArgs() 
    {
        String[] args = { "SDARS_ESN_RESULTNAME",
                "SDARS_ESN_TESTSTEPNAME",
                "SDARS_SN_RESULTNAME",
                "SDARS_SN_TESTSTEPNAME",
                "SDARS_PACKAGE_ID_RESULTNAME",
                "SDARS_PACKAGE_ID_TESTSTEPNAME",
                "SDARS_PELLET_RESULTNAME",
                "SDARS_PELLET_TESTSTEPNAME",
                "SDARS_ENCRYPTION_TYPE",
                "SDARS_USE_CHECK",

                "HEADUNIT_SN_RESULTNAME",
                "HEADUNIT_SN_TESTSTEPNAME",
                "HEADUNIT_USE_CHECK",

                "CDC_SN_RESULTNAME",
                "CDC_SN_TESTSTEPNAME",
                "CDC_USE_CHECK",

                "MMC_SN_RESULTNAME",
                "MMC_SN_TESTSTEPNAME",
                "MMC_USE_CHECK",

                "PHONE_DEVICE_ID_RESULTNAME",
                "PHONE_DEVICE_ID_TESTSTEPNAME",
                "PHONE_SUBSCRIBER_ID_RESULTNAME",
                "PHONE_SUBSCRIBER_ID_TESTSTEPNAME",
                "PHONE_EUICC_ID_RESULTNAME",
                "PHONE_EUICC_ID_TESTSTEPNAME",
                "PHONE_GLONASSICC_ID_RESULTNAME",   
                "PHONE_GLONASSICC_ID_TESTSTEPNAME", 
                "PHONE_STANDARD",
                "PHONE_USE_CHECK",


                "TPA_DEVICE_ID_RESULTNAME",
                "TPA_DEVICE_ID_TESTSTEPNAME",
                "TPA_SUBSCRIBER_ID_RESULTNAME",
                "TPA_SUBSCRIBER_ID_TESTSTEPNAME",
                "TPA_SN_RESULTNAME",
                "TPA_SN_TESTSTEPNAME",
                "TPA_PARTNR_RESULTNAME",
                "TPA_PARTNR_TESTSTEPNAME",
                "TPA_STANDARD",
                "TPA_USE_CHECK"
        };
        return args;
    }


    /** liefert die optionalen Argumente */
    @Override
	public String[] getOptionalArgs() 
    {
        String[] args = { "SPLIT_FILES", "DOKU_PARTITION", "CREATE_DATASET" };
        return args;
    }
   
    /** �berpr�fung der Parameter f�r diese PP 
     * @return false: mindestens ein Parameterfehler gefunden
     */
    @Override
	public boolean checkArgs() 
    {
        try
        {
            checkArgs_();
            return true;
        }
        catch (CError e)
        {
            return false;
        }
    }

	/**
	 * Re-Implemented super.checkArgs()
	 *             
	 * TODO 
     * 25.09.2014 Mit der SDARS Erweiterung soll f�r die �bergangszeit Abw�rtskompatibilit�t sichergestellt werden, d.h. die neuen Parameter k�nnen fehlen
     * Deshalb wird hier ein adaptierter CheckArgs gemacht. Perspektivisch kann wieder auf die Auswertung von super.checkArgs() umgestellt werden.
     * Ignoriere die neuen zwingenden Parameter: SDARS_PACKAGE... und SDARS_PELLET... beim Check
     * 03.05.2015 Analoge Behandlung f�r die reprogrammierbare SIM Karte Parameter: PHONE_EUICC_ID... beim Check
	 * 
	 * Pr�ft soweit als m�glich, ob die gesetzten Argumente die Ausf�hrkriterien der Pruefprozedur
	 * erf�llen. An dieser Stelle nur �berpr�fung, ob alle zwingenden Argumente vorhanden sind und
	 * keine nicht zur Verwendung kommenden Argumente gesetzt wurden. Es findet keine Bewertung der
	 * Argumentwerte mit Ausnahme Leerstring statt.
	 * 
	 * @return true, falls die gesetzten Argumente die Ausf�hrkriterien der Pruefprozedur erf�llen,
	 *         ansonsten false
	 */
	public boolean checkArgs_ReImplemented() {
		int i, j;
		boolean exist;
		// 1. Check: Sind alle requiredArgs gesetzt
		String[] requiredArgs = getRequiredArgs();
		for( i = 0; i < requiredArgs.length; i++ ) {
			//neuer Parameter ignorieren und Sicherstellung der Abw�rtskompatibilit�t
			if (requiredArgs[i].toUpperCase().startsWith( "SDARS_PACKAGE" ) || (requiredArgs[i].toUpperCase().startsWith( "SDARS_PELLET" )) || (requiredArgs[i].toUpperCase().startsWith( "PHONE_EUICC" )) || (requiredArgs[i].toUpperCase().startsWith( "PHONE_GLONASSICC" ))) {
				continue;
			}
			
			if( getArg( requiredArgs[i].toUpperCase() ) == null )
				return false;
			if( getArg( requiredArgs[i].toUpperCase() ).equals( "" ) == true )
				return false;
		}
		// 2. Check: Sind �berfl�ssige Argumente gesetzt, d.h. Argumente, die weder required noch
		// optional sind
		Enumeration<?> enu = getArgs().keys();
		String[] optionalArgs = getOptionalArgs();
		String givenkey;
		while( enu.hasMoreElements() ) {
			givenkey = (String) enu.nextElement();
			exist = false;
			j = 0;
			while( (exist == false) && (j < requiredArgs.length) ) {
				if( givenkey.equalsIgnoreCase( requiredArgs[j] ) == true )
					exist = true;
				j++;
			}
			j = 0;
			while( (exist == false) && (j < optionalArgs.length) ) {
				if( givenkey.equalsIgnoreCase( optionalArgs[j] ) == true )
					exist = true;
				j++;
			}
			if( exist == false )
				return false;
			if( getArg( givenkey ).equals( "" ) == true )
				return false;
		}
		// Tests bestanden, somit ok
		return true;
	}
    
    /** �berpr�fung der Parameter f�r diese PP 
     * 
     * @throws CError Fehler in Parametern
     */
    public void checkArgs_() throws CError 
    {
        CError err;
        int i;
        String ArgName;
        String ArgValue;
        String AllowedCheckTypes;
        Iterator<String> it;

        try 
        {
            //TODO 
        	// 25.09.2014 Mit der SDARS Erweiterung soll f�r die �bergangszeit Abw�rtskompatibilit�t sichergestellt werden, d.h. die neuen Parameter k�nnen fehlen
        	// 03.05.2015 Analoge Behandlung f�r die reprogrammierbare SIM Karte Parameter: PHONE_EUICC_ID... beim Check
        	// Deshalb wird hier ein adaptierter CheckArgs gemacht. Perspektivisch kann wieder auf die Auswertung von super.checkArgs() umgestellt werden 
        	if (!checkArgs_ReImplemented()) 
            {
                if (isDE()) err=new CError( new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameterexistenz", "Parametrierfehler", Ergebnis.FT_NIO_SYS ));
                else        err=new CError( new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "parameter error", "parameter error", Ergebnis.FT_NIO_SYS ));
                throw err;
            }

            fillCheckTypes();
            it=m_checkTypes.keySet().iterator();
            AllowedCheckTypes="";
            while (it.hasNext())
            {
                ArgValue=(String)(it.next());
                if (AllowedCheckTypes.length()!=0)
                {
                    if (it.hasNext())
                        AllowedCheckTypes+=",";
                    else
                        if (isDE())
                            AllowedCheckTypes+=" oder ";
                        else
                            AllowedCheckTypes+=" or ";
                }
                AllowedCheckTypes+=ArgValue;
            }

            for (i=0; i<getRequiredArgs().length; i++)
            {
                ArgName=getRequiredArgs()[i];
                if (!ArgName.endsWith("_USE_CHECK")) continue;
                ArgValue=getArg(ArgName);
                if (!m_checkTypes.containsKey(ArgValue))
                {
                    if (isDE()) err=new CError( new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameter "+ArgName+"="+ArgValue+" ist nicht "+AllowedCheckTypes, "Parametrierfehler", Ergebnis.FT_NIO_SYS ));
                    else        err=new CError( new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameter "+ArgName+"="+ArgValue+" is not "+AllowedCheckTypes, "parameter error", Ergebnis.FT_NIO_SYS ));
                    throw err;
                }
            }

            ArgName=getOptionalArgs()[0];
            ArgValue=getArg(ArgName);
            if (ArgValue!=null)
            {
                ArgValue=ArgValue.trim();
                if (ArgValue.length()!=0)
                {
                    if (!ArgValue.equalsIgnoreCase("TRUE") && !ArgValue.equalsIgnoreCase("FALSE"))
                    {
                        if (isDE()) err=new CError( new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameter "+ArgName+"="+ArgValue+" ist nicht TRUE oder FALSE", "Parametrierfehler", Ergebnis.FT_NIO_SYS ));
                        else        err=new CError( new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameter "+ArgName+"="+ArgValue+" is not TRUE or FALSE", "parameter error", Ergebnis.FT_NIO_SYS ));
                        throw err;
                    }
                }
            }
            
            ArgName=getOptionalArgs()[2];
            ArgValue=getArg(ArgName);
            if (ArgValue!=null)
            {
                ArgValue=ArgValue.trim();
                if (ArgValue.length()!=0)
                {
                    if (!ArgValue.equalsIgnoreCase("TRUE") && !ArgValue.equalsIgnoreCase("FALSE"))
                    {
                        if (isDE()) err=new CError( new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameter "+ArgName+"="+ArgValue+" ist nicht TRUE oder FALSE", "Parametrierfehler", Ergebnis.FT_NIO_SYS ));
                        else        err=new CError( new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", "Parameter "+ArgName+"="+ArgValue+" is not TRUE or FALSE", "parameter error", Ergebnis.FT_NIO_SYS ));
                        throw err;
                    }
                }
            }

        } 
        catch (CError e)
        {
            throw e;
        }
        catch (Exception e) 
        {
            e.printStackTrace();
            throw new CError(new Ergebnis( "ExecFehler", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", e.getClass().getName()+": "+e.getMessage(), "Parametrierfehler, siehe PruefstandScreen.log", Ergebnis.FT_NIO_SYS ));
        } 
        catch (Throwable e) 
        {
            e.printStackTrace();
            throw new CError(new Ergebnis( "ExecError", "CHECKARGS", "", "", "", "", "", "", "", "0", "", "", "", e.getClass().getName()+": "+e.getMessage(), "parameter error, see PruefstandScreen.log", Ergebnis.FT_NIO_SYS ));
        }
    }
   
    /** Cascade Results */
    private Vector<Ergebnis> m_ergListe; 
    
    /** f�hrt die Pr�fprozedur aus
     * 
     * @param info Information zur Ausf�hrung
     */
    @Override
	public void execute( ExecutionInfo info ) 
    {
    int status = STATUS_EXECUTION_OK;
    Ergebnis result;
    Map<String, String>    ValuesToDocument;
    boolean SplitFiles, CreateDataset;
    String ArgValue;
    int HistorySplitSize;
    // invalid EU ICC ID 19 Nullen und ein f
    final String invalidEUICC = "0000000000000000000f";

        m_ergListe=new Vector<Ergebnis>();
    
        try 
        {
            //Parameter pr�fen
            checkArgs_();

            SplitFiles=false;
            ArgValue=getArg(getOptionalArgs()[0]);
            if (ArgValue!=null)
            {
                ArgValue=ArgValue.trim();
                if (ArgValue.length()!=0)
                {
                    if (ArgValue.equalsIgnoreCase("TRUE")) SplitFiles=true;
                }
            }
            
            CreateDataset=true;
            ArgValue=getArg(getOptionalArgs()[2]);
            if (ArgValue!=null)
            {
                ArgValue=ArgValue.trim();
                if (ArgValue.length()!=0)
                {
                    if (ArgValue.equalsIgnoreCase("FALSE")) CreateDataset=false;
                }
            }

            HistorySplitSize=0;
            ArgValue=getArg(getOptionalArgs()[1]);
            if (ArgValue!=null)
            {
                ArgValue=ArgValue.trim();
                try
                {
                    HistorySplitSize=Integer.parseInt(ArgValue);
                }
                catch (NumberFormatException ex)
                {
                    HistorySplitSize=0;
                }
            }

            
            ValuesToDocument=getTestStepResults();

            String SDARS_ESN;
            String SDARS_SN;
            String SDARS_PACKAGE_ID;
            String SDARS_PELLET;
            String SDARS_ENCRYPTION_TYPE;
            int    SDARS_USE_CHECK;

            String HEADUNIT_SN;
            int    HEADUNIT_USE_CHECK;

            String CDC_SN;
            int    CDC_USE_CHECK;

            String MMC_SN;
            int    MMC_USE_CHECK;

            String PHONE_DEVICE_ID;
            String PHONE_SUBSCRIBER_ID;
            String PHONE_EUICC_ID;
            String PHONE_GLONASSICC_ID; 
            String PHONE_STANDARD;
            int    PHONE_USE_CHECK;

            String TPA_DEVICE_ID;
            String TPA_SUBSCRIBER_ID;
            String TPA_SN;
            String TPA_PARTNR;
            String TPA_STANDARD;
            int    TPA_USE_CHECK;         

            SDARS_ESN=(String)ValuesToDocument.get("SDARS_ESN");
            SDARS_SN=(String)ValuesToDocument.get("SDARS_SN");
            SDARS_PACKAGE_ID=(String)ValuesToDocument.get("SDARS_PACKAGE_ID");
            SDARS_PELLET=(String)ValuesToDocument.get("SDARS_PELLET");
            SDARS_ENCRYPTION_TYPE=getArg("SDARS_ENCRYPTION_TYPE");
            SDARS_USE_CHECK=getCheck("SDARS_USE_CHECK");

            HEADUNIT_SN=(String)ValuesToDocument.get("HEADUNIT_SN");
            HEADUNIT_USE_CHECK=getCheck("HEADUNIT_USE_CHECK");

            CDC_SN=(String)ValuesToDocument.get("CDC_SN");
            CDC_USE_CHECK=getCheck("CDC_USE_CHECK");

            MMC_SN=(String)ValuesToDocument.get("MMC_SN");
            MMC_USE_CHECK=getCheck("MMC_USE_CHECK");

            PHONE_DEVICE_ID=(String)ValuesToDocument.get("PHONE_DEVICE_ID");
            PHONE_SUBSCRIBER_ID=(String)ValuesToDocument.get("PHONE_SUBSCRIBER_ID");
            PHONE_EUICC_ID=(String)ValuesToDocument.get("PHONE_EUICC_ID");
            // Wenn 19 Nullen + f beim Einganswert f�r die EuICCId �bergeben werden, der Key f�r die EuICCId nicht an DOM geschrieben werden. LOP 2114
            if (PHONE_EUICC_ID!=null && PHONE_EUICC_ID.equalsIgnoreCase(invalidEUICC)) {
            	PHONE_EUICC_ID = null;
            }
            	
            PHONE_GLONASSICC_ID=(String)ValuesToDocument.get("PHONE_GLONASSICC_ID"); 
   
            PHONE_STANDARD=getArg("PHONE_STANDARD");
            PHONE_USE_CHECK=getCheck("PHONE_USE_CHECK");

            TPA_DEVICE_ID=(String)ValuesToDocument.get("TPA_DEVICE_ID");
            TPA_SUBSCRIBER_ID=(String)ValuesToDocument.get("TPA_SUBSCRIBER_ID");
            TPA_SN=(String)ValuesToDocument.get("TPA_SN");
            TPA_PARTNR=(String)ValuesToDocument.get("TPA_PARTNR");
            TPA_STANDARD=getArg("TPA_STANDARD");
            TPA_USE_CHECK=getCheck("TPA_USE_CHECK");

            String Plant = CascadeProperties.getCascadeWerkCode();

            dokumentInfotainment(SplitFiles, CreateDataset,
                    Plant,
                    HistorySplitSize,

                    SDARS_ESN,
                    SDARS_SN,
                    SDARS_PACKAGE_ID,
                    SDARS_PELLET,
                    SDARS_ENCRYPTION_TYPE,
                    SDARS_USE_CHECK,

                    HEADUNIT_SN,
                    HEADUNIT_USE_CHECK,

                    CDC_SN,
                    CDC_USE_CHECK,

                    MMC_SN,
                    MMC_USE_CHECK,

                    PHONE_DEVICE_ID,
                    PHONE_SUBSCRIBER_ID,
                    PHONE_EUICC_ID,
                    PHONE_GLONASSICC_ID,
                    PHONE_STANDARD,
                    PHONE_USE_CHECK,

                    TPA_DEVICE_ID,
                    TPA_SUBSCRIBER_ID,
                    TPA_SN,
                    TPA_PARTNR,
                    TPA_STANDARD,
                    TPA_USE_CHECK);
        } 
        catch (CError e) 
        {
            m_ergListe.addAll(e.getCascadeResults());
            status = STATUS_EXECUTION_ERROR;
        } 
        catch (Exception e) 
        {
            System.out.println(e.getMessage());
            e.printStackTrace(); 
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getClass().getName(), PB.getString( "unerwarteterLaufzeitfehler" )+", see PruefstandScreenLog", Ergebnis.FT_NIO_SYS );
            m_ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } 
        catch (Throwable e) 
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", e.getClass().getName(), PB.getString( "unerwarteterLaufzeitfehler" )+", see PruefstandScreenLog", Ergebnis.FT_NIO_SYS );
            m_ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }

        setPPStatus( info, status, m_ergListe );
    }

    /** Holt die Pr�fschrittergebnisse ab, die die Nummern aus den verschiedenen Steuerger�ten ausgelesen haben.
     * Wenn ein Pr�fschritt im Pr�fling vorhanden ist, dann mu� er auch ausgef�hrt worden sein.
     * 
     * @return Map <Parametername, z.B. SDARS_ESN>=<Wert>
     * @throws CError
     */
    private Map<String, String> getTestStepResults() throws CError
    {
        int i;
        String[] ReqArgs;
        String ParName;
        String ParNameBase;
        String ResultName;
        String TestStepName;
        String[] TestStepParts;
        String   PruefschrittName;
        String   PrueflingName;
        Pruefling PL;
        Pruefprozedur PP;
        String value;
        HashMap<String, String>  erg=new HashMap<String, String>();

        ReqArgs=getRequiredArgs();

        for (i=0; i<ReqArgs.length; i++)
        {
            ParName=ReqArgs[i];

            if (!ParName.endsWith("_TESTSTEPNAME")) continue;
            ParNameBase=ParName.substring(0, ParName.length()-13);

            TestStepName=getArg(ParName);
            if (TestStepName==null) continue;
            TestStepName=TestStepName.trim();
            if (TestStepName.length()==0) continue;

            ResultName=getArg(ReqArgs[i-1]);

            if (TestStepName.equalsIgnoreCase("IGNORE"))
            {
                erg.put(ParNameBase, ResultName);
                continue;
            }

            TestStepParts=TestStepName.split("\\.");
            if (TestStepParts.length==1)
            {
                PruefschrittName=TestStepParts[0];
                PrueflingName=null;
            }
            else
            {
                PruefschrittName=TestStepParts[0];
                PrueflingName=TestStepParts[1];
            }

            if (PrueflingName==null)
            {
                PL=getPr�fling();
            }
            else
            {
                PL=getPr�fling().getAuftrag().getPr�fling(PrueflingName);
            }

            if (PL==null)
            {
                continue;
            }

            try
            {
                PP=PL.getPr�fprozedur(PruefschrittName);
            }
            catch (Exception e)
            {
                PP=null;
            }

            if (PP!=null)
            {

                try
                {
                    //value = PP.getHistoryListe().getLatestErgebnis( ResultName ).getErgebnisWert();
                    value = ((AbstractBMWPruefprozedur)PP).getPPResult( ResultName + "@" + PL.getName() + "." + PruefschrittName );
                    if (value==null)
                    {
                        if (isDE()) throw new CError(new Ergebnis("MissingResult", "", "", "", "", "", "", "", "", "0", "", "", "", "Pr�fschritt "+PruefschrittName+" in Pr�fling "+PL.getName()+" gefunden, aber Ergebnis "+ResultName+" ist null", "", Ergebnis.FT_NIO_SYS ));
                        else        throw new CError(new Ergebnis("MissingResult", "", "", "", "", "", "", "", "", "0", "", "", "", "Teststep "+PruefschrittName+" in Pr�fling "+PL.getName()+" found, but result "+ResultName+" is null", "", Ergebnis.FT_NIO_SYS ));
                    }
                    erg.put(ParNameBase, value);
                }
                catch (Exception e)
                {
                    if (isDE()) throw new CError(new Ergebnis("MissingResult", "", "", "", "", "", "", "", "", "0", "", "", "", "Pr�fschritt "+PruefschrittName+" in Pr�fling "+PL.getName()+" gefunden, aber Ergebnis "+ResultName+" fehlt", "Vermutlich wurde der Pr�schritt nicht ausgef�hrt", Ergebnis.FT_NIO_SYS ));
                    else        throw new CError(new Ergebnis("MissingResult", "", "", "", "", "", "", "", "", "0", "", "", "", "Teststep "+PruefschrittName+" in Pr�fling "+PL.getName()+" found, but result "+ResultName+" is missing", "Probably, the teststep was not executed", Ergebnis.FT_NIO_SYS ));
                }
            }
            else
            {
                value=null;
            }
        }
        return erg;
    }
   
    /** Holt den Inhalt des Parameters "ArgName"
     * und wandelt in den String in eine Konstante um:
     * "FALSE" -> NO_CHECK
     * "NOYEAR" -> CHECK_WITHOUT_YEAR
     * "TRUE" -> FULL_CHECK
     * 
     *  @param ArgName Inputparameter
     *  @return Check Art 
     * */
    public int getCheck(String ArgName)
    {
        String str;

        str=getArg(ArgName);

        if (m_checkTypes.containsKey(str))
        {
            return ((Integer)m_checkTypes.get(str)).intValue();
        }
        return 2; // D�rfte nie auftreten, wenn checkArgs_ korrekt
    }

   /** Erzeugt DOM Dateien f�r alle Infotainmentger�te
    * 
    * @param SplitFiles Ein (K) oder mehrere (K, T, N) DOM Datens�tze erzeugen
    * @param CreateDataset true: DOM-Datens�tze erzeugen und auf Cascade Server stellen, 
    *                      false: Nur Cascade Results erzeugen
    * @param Plant Werkskennung
    * @param HistorySplitSize Filegr��e ab der INFOTAINMENT.DAT gesplitted wird (0: kein Splitten)
    * @param SDARS_ESN 
    * @param SDARS_SN
    * @param SDARS_PACKAGE_ID 
    * @param SDARS_PELLET
    * @param SDARS_ENCRYPTION_TYPE
    * @param SDARS_USE_CHECK
    * @param HEADUNIT_SN
    * @param HEADUNIT_USE_CHECK
    * @param CDC_SN
    * @param CDC_USE_CHECK
    * @param MMC_SN
    * @param MMC_USE_CHECK
    * @param PHONE_DEVICE_ID
    * @param PHONE_SUBSCRIBER_ID
    * @param PHONE_EUICC_ID
    * @param PHONE_GLONASSICC_ID 
    * @param PHONE_STANDARD
    * @param PHONE_USE_CHECK
    * @param TPA_DEVICE_ID
    * @param TPA_SUBSCRIBER_ID
    * @param TPA_SN
    * @param TPA_PARTNR
    * @param TPA_STANDARD
    * @param TPA_USE_CHECK
    * @throws CError Fehler beim Dokumentieren
    */
    private void dokumentInfotainment(boolean SplitFiles,
                                      boolean CreateDataset,
                                      String Plant,
                                      int HistorySplitSize,
           
                                      String SDARS_ESN,
                                      String SDARS_SN,
                                      String SDARS_PACKAGE_ID,
                                      String SDARS_PELLET,
                                      String SDARS_ENCRYPTION_TYPE,
                                      int    SDARS_USE_CHECK,
                                     
                                      String HEADUNIT_SN,
                                      int    HEADUNIT_USE_CHECK,
   
                                      String CDC_SN,
                                      int    CDC_USE_CHECK,
           
                                      String MMC_SN,
                                      int    MMC_USE_CHECK,
                                     
                                      String PHONE_DEVICE_ID,
                                      String PHONE_SUBSCRIBER_ID,
                                      String PHONE_EUICC_ID,
                                      String PHONE_GLONASSICC_ID,
                                      String PHONE_STANDARD,
                                      int    PHONE_USE_CHECK,

                                      String TPA_DEVICE_ID,
                                      String TPA_SUBSCRIBER_ID,
                                      String TPA_SN,
                                      String TPA_PARTNR,
                                      String TPA_STANDARD,
                                      int    TPA_USE_CHECK) throws CError
    {
    final String XML_FILE_COM = "K";
    final String XML_FILE_PHONE = "T";
    final String XML_FILE_SDARS = "N";
    final String XML_FILE_TPA = "V";
    final String TPA_Schema="0501";
    final String SDARS_Schema="0502";
    final String EUICCID_Schema="0504";
    final String Rest_Schema="0400";
    boolean SDARS_Present, HEADUNIT_Present, CDC_Present, MMC_Present, PHONE_Present, TPA_Present;
    StringBuffer output_com, output_phone, output_sdars, output_tpa;
    String MissingValues;
    Vector<CError> errors;
    StringBuffer[] dokuStrs;
    Vector<StringBuffer> backupStrs=new Vector<StringBuffer>();
   
        if (isDE()) MissingValues="Fehlende Werte:";
        else        MissingValues="Missing Values:";

        if (SDARS_ESN==null && SDARS_SN==null)
            SDARS_Present=false;
        else if (SDARS_ESN!=null && SDARS_SN!=null)
            SDARS_Present=true;
        else
        {
            if (SDARS_ESN==null) MissingValues+=" SDARS_ESN";
            if (SDARS_SN==null) MissingValues+=" SDARS_SN";
            if (isDE()) throw new CError("Es sind nicht alle Werte von SDARS vorhanden", MissingValues);
            else        throw new CError("Not all values for SDARS are available", MissingValues);
        }

        HEADUNIT_Present=HEADUNIT_SN!=null;
        CDC_Present=CDC_SN!=null;
        MMC_Present=MMC_SN!=null;

        if (PHONE_DEVICE_ID==null && PHONE_SUBSCRIBER_ID==null)
            PHONE_Present=false;
        else if (PHONE_DEVICE_ID!=null && PHONE_SUBSCRIBER_ID!=null)
            PHONE_Present=true;
        else
        {
            if (PHONE_DEVICE_ID==null) MissingValues+=" PHONE_DEVICE_ID";
            if (PHONE_SUBSCRIBER_ID==null) MissingValues+=" PHONE_SUBSCRIBER_ID";
            if (isDE()) throw new CError("Es sind nicht alle Werte von PHONE vorhanden", MissingValues);
            else        throw new CError("Not all values for PHONE are available", MissingValues);
        }

        if (TPA_DEVICE_ID==null && TPA_SUBSCRIBER_ID==null && TPA_SN==null && TPA_PARTNR==null)
            TPA_Present=false;
        else if (TPA_DEVICE_ID!=null && TPA_SUBSCRIBER_ID!=null && TPA_SN!=null && TPA_PARTNR!=null)
            TPA_Present=true;
        else
        {
            if (TPA_DEVICE_ID==null) MissingValues+=" TPA_DEVICE_ID";
            if (TPA_SUBSCRIBER_ID==null) MissingValues+=" TPA_SUBSCRIBER_ID";
            if (TPA_SN==null) MissingValues+=" TPA_SN";
            if (TPA_PARTNR==null) MissingValues+=" TPA_PARTNR";
            if (isDE()) throw new CError("Es sind nicht alle Werte von TPA vorhanden", MissingValues);
            else        throw new CError("Not all values for TPA are available", MissingValues);

        }
       
   
        if (!SDARS_Present && 
            !HEADUNIT_Present && 
            !CDC_Present && 
            !MMC_Present && 
            !PHONE_Present && 
            !TPA_Present) return; //Nichts zu dokumentieren
   
        errors=new Vector<CError>();

        if (SDARS_SN!=null)
        {
            if (SDARS_SN.length()>12) SDARS_SN=SDARS_SN.substring(SDARS_SN.length()-12, SDARS_SN.length());
        }
       
        if (SDARS_Present)    checkSDARS(SDARS_ESN, SDARS_SN, SDARS_USE_CHECK, errors);
        if (HEADUNIT_Present) checkHeadunit(HEADUNIT_SN, HEADUNIT_USE_CHECK, errors);
        if (CDC_Present)      checkCDC(CDC_SN, CDC_USE_CHECK, errors);
        if (MMC_Present)      checkMMC(MMC_SN, MMC_USE_CHECK, errors);
        if (PHONE_Present)    checkPHONE(PHONE_DEVICE_ID, PHONE_SUBSCRIBER_ID, PHONE_STANDARD, PHONE_USE_CHECK, errors);
        if (TPA_Present)      checkTPA(TPA_DEVICE_ID, TPA_SUBSCRIBER_ID, TPA_SN, TPA_PARTNR, TPA_USE_CHECK, errors);
       
        if (errors.size()>0) throw new CError(errors);

        output_com=null;
        output_phone=null;
        output_sdars=null;
        output_tpa=null;

        if (SplitFiles)
        {
            if (CDC_Present || MMC_Present || HEADUNIT_Present)
            {
                output_com=create_XML_header(Rest_Schema);
                if (HEADUNIT_Present)
                {
                    dokuStrs=dokumentHeadunit(HEADUNIT_SN);
                    output_com.append(dokuStrs[0]);
                    backupStrs.add(dokuStrs[1]);
                }
                if (CDC_Present)
                {
                    dokuStrs=dokumentCDC(CDC_SN);
                    output_com.append(dokuStrs[0]);
                    backupStrs.add(dokuStrs[1]);
                }
                if (MMC_Present)
                {
                    dokuStrs=dokumentMMC(MMC_SN);
                    output_com.append(dokuStrs[0]);
                    backupStrs.add(dokuStrs[1]);
                }
                output_com.append(create_XML_footer());
            }

            if (PHONE_Present)
            {
                output_phone=create_XML_header(PHONE_EUICC_ID == null ? Rest_Schema : EUICCID_Schema);
                dokuStrs=dokumentPhone(PHONE_DEVICE_ID, PHONE_SUBSCRIBER_ID, PHONE_STANDARD, PHONE_EUICC_ID, PHONE_GLONASSICC_ID, Plant);
                output_phone.append(dokuStrs[0]);
                backupStrs.add(dokuStrs[1]);
                output_phone.append(create_XML_footer());
            }

            if (SDARS_Present)
            {
                output_sdars=create_XML_header(SDARS_PELLET == null ? Rest_Schema : SDARS_Schema);
                dokuStrs=dokumentSDARS(SDARS_ESN, SDARS_SN, SDARS_ENCRYPTION_TYPE, SDARS_PACKAGE_ID, SDARS_PELLET);
                output_sdars.append(dokuStrs[0]);
                backupStrs.add(dokuStrs[1]);
                output_sdars.append(create_XML_footer());
            }

            if (TPA_Present)
            {
                output_tpa=create_XML_header(TPA_Schema);
                dokuStrs=dokumentTPA(TPA_DEVICE_ID, TPA_SUBSCRIBER_ID, TPA_SN, TPA_PARTNR, TPA_STANDARD, Plant);
                output_tpa.append(dokuStrs[0]);
                backupStrs.add(dokuStrs[1]);
                output_tpa.append(create_XML_footer());
            }

        }
        else
        {
            if (HEADUNIT_Present || CDC_Present || MMC_Present || SDARS_Present)
            {
                output_com=create_XML_header(SDARS_PELLET == null ? Rest_Schema : SDARS_Schema);
                if (HEADUNIT_Present)
                {
                    dokuStrs=dokumentHeadunit(HEADUNIT_SN);
                    output_com.append(dokuStrs[0]);
                    backupStrs.add(dokuStrs[1]);
                }
                if (CDC_Present)
                {
                    dokuStrs=dokumentCDC(CDC_SN);
                    output_com.append(dokuStrs[0]);
                    backupStrs.add(dokuStrs[1]);
                }
                if (MMC_Present)
                {
                    dokuStrs=dokumentMMC(MMC_SN);
                    output_com.append(dokuStrs[0]);
                    backupStrs.add(dokuStrs[1]);
                }
                if (SDARS_Present)
                {
                    dokuStrs=dokumentSDARS(SDARS_ESN, SDARS_SN, SDARS_ENCRYPTION_TYPE, SDARS_PACKAGE_ID, SDARS_PELLET);
                    output_com.append(dokuStrs[0]);
                    backupStrs.add(dokuStrs[1]);
                }
                output_com.append(create_XML_footer());
            }

            if (PHONE_Present)
            {
                output_phone=create_XML_header(PHONE_EUICC_ID == null ? Rest_Schema : EUICCID_Schema);
                dokuStrs=dokumentPhone(PHONE_DEVICE_ID, PHONE_SUBSCRIBER_ID, PHONE_STANDARD, PHONE_EUICC_ID, PHONE_GLONASSICC_ID, Plant);
                output_phone.append(dokuStrs[0]);
                backupStrs.add(dokuStrs[1]);
                output_phone.append(create_XML_footer());
            }

            if (TPA_Present)
            {
                output_tpa=create_XML_header(TPA_Schema);
                dokuStrs=dokumentTPA(TPA_DEVICE_ID, TPA_SUBSCRIBER_ID, TPA_SN, TPA_PARTNR, TPA_STANDARD, Plant);
                output_tpa.append(dokuStrs[0]);
                backupStrs.add(dokuStrs[1]);
                output_tpa.append(create_XML_footer());
            }
        }
       
        save2History(backupStrs, HistorySplitSize, Plant);
        
        if (CreateDataset)
        {
            try
            {
                if (output_com!=null)   DomPruefstand.writeDomData( getPr�fling().getAuftrag().getFahrgestellnummer7(), XML_FILE_COM, output_com.toString().getBytes() );
                if (output_phone!=null) DomPruefstand.writeDomData( getPr�fling().getAuftrag().getFahrgestellnummer7(), XML_FILE_PHONE, output_phone.toString().getBytes() );
                if (output_sdars!=null) DomPruefstand.writeDomData( getPr�fling().getAuftrag().getFahrgestellnummer7(), XML_FILE_SDARS, output_sdars.toString().getBytes() );
                if (output_tpa!=null)   DomPruefstand.writeDomData( getPr�fling().getAuftrag().getFahrgestellnummer7(), XML_FILE_TPA, output_tpa.toString().getBytes() );
            }
            catch (Exception e)
            {
                if (isDE()) throw new CError("DomTransfer fehlgeschlagen", e.getClass().getName()+":"+e.getMessage());
                else        throw new CError("DomTransfer failed", e.getClass().getName()+":"+e.getMessage());
            }
        }
    }
        

    /** Pr�ft die SDARS Parameter
     * 
     * @param SDARS_ESN 12 Zeichen, alphanumerisch
     * @param SDARS_SN  12 Zeichen, alphanumerisch
     * @param useCheck Flag, ob Check durchgef�hrt werden soll
     * @param errors Fehlereintrage bei fehlgeschlagenem Check
     */
    private void checkSDARS(String SDARS_ESN, String SDARS_SN, int useCheck, Vector<CError> errors)
    {
        if (useCheck==NO_CHECK) return;

        try
        {
            isSerialNumberCorrect("SDARS_SN", SDARS_SN, 12, false );
        }
        catch (CError err)
        {
            errors.add(err);
        }

        try
        {
            isSerialNumberCorrect("SDARS_ESN", SDARS_ESN, 12, false );
        }
        catch (CError err)
        {
            errors.add(err);
        }
    }
   
    /** Pr�ft die Radio Parameter
     * 
     * @param HEADUNIT_SN 14 Zeichen, alphanumerisch ohne "O", Pr�fung der Jahreszahl 
     * @param useCheck Flag, ob Check durchgef�hrt werden soll mit oder ohne Pr�fung der Jahreszahl
     * @param errors Fehlereintrage bei fehlgeschlagenem Check
     */
    private void checkHeadunit(String HEADUNIT_SN, int useCheck, Vector<CError> errors)
    {
        if (useCheck==NO_CHECK) return;
        try
        {
            isSerialNumberCorrect("HEADUNIT_SN", HEADUNIT_SN, 14, useCheck==FULL_CHECK );
        }
        catch (CError err)
        {
            errors.add(err);
        }
    }

    /** Pr�ft die CDC Parameter
     * 
     * @param CDC_SN 14 Zeichen, alphanumerisch ohne "O", Pr�fung der Jahreszahl
     * @param useCheck Flag, ob Check durchgef�hrt werden soll mit oder ohne Pr�fung der Jahreszahl
     * @param errors Fehlereintrage bei fehlgeschlagenem Check
     */
    private void checkCDC(String CDC_SN, int useCheck, Vector<CError> errors)
    {
        if (useCheck==NO_CHECK) return;

        try
        {
            isSerialNumberCorrect("CDC_SN", CDC_SN, 14, useCheck==FULL_CHECK );
        }
        catch (CError err)
        {
            errors.add(err);
        }

    }
   
    /** Pr�ft die MMC Parameter
     * 
     * @param MMC_SN 14 Zeichen, alphanumerisch ohne "O", Pr�fung der Jahreszahl
     * @param useCheck Flag, ob Check durchgef�hrt werden soll mit oder ohne Pr�fung der Jahreszahl
     * @param errors Fehlereintrage bei fehlgeschlagenem Check
     */
    private void checkMMC(String MMC_SN, int useCheck, Vector<CError> errors)
    {
        if (useCheck==NO_CHECK) return;

        try
        {
            isSerialNumberCorrect("MMC_SN", MMC_SN, 14, useCheck==FULL_CHECK );
        }
        catch (CError err)
        {
            errors.add(err);
        }
    }


    /** Pr�ft die Telefon Parameter
     * 
     * @param PHONE_DEVICE_ID alphanumerisch, beliebige Anzahl
     * @param PHONE_SUBSCRIBER_ID alphanumerisch, beliebige Anzahl 
     * @param PHONE_STANDARD Telefonstandard: ECE oder US
     * @param useCheck Flag, ob Check durchgef�hrt werden soll
     * @param errors Fehlereintrage bei fehlgeschlagenem Check
     */
    private void checkPHONE(String PHONE_DEVICE_ID, String PHONE_SUBSCRIBER_ID, String PHONE_STANDARD, int useCheck, Vector<CError> errors) 
    {
        if (useCheck==NO_CHECK) return;

        if ( PHONE_STANDARD.equalsIgnoreCase("US") ) 
        {
            try
            {
                isSerialNumberCorrect("PHONE_DEVICE_ID", PHONE_DEVICE_ID, 1, false );
            }
            catch (CError err)
            {
                errors.add(err);
            }

            try
            {
                isSerialNumberCorrect("PHONE_SUBSCRIBER_ID", PHONE_SUBSCRIBER_ID, 1, false );
            }
            catch (CError err)
            {
                errors.add(err);
            }
        } 
        else 
        {
            try
            {
                isSerialNumberCorrect("PHONE_DEVICE_ID", PHONE_DEVICE_ID, 1, false );
            }
            catch (CError err)
            {
                errors.add(err);
            }

            try
            {
                isSerialNumberCorrect("PHONE_SUBSCRIBER_ID", PHONE_SUBSCRIBER_ID, 1, false );
            }
            catch (CError err)
            {
                errors.add(err);
            }
        }
    }
   
    /** Pr�ft die TPA Parameter
     * 
     * @param TPA_DEVICE_ID
     * @param TPA_SUBSCRIBER_ID
     * @param TPA_SN
     * @param TPA_PARTNR
     * @param useCheck Flag, ob Check durchgef�hrt werden soll
     * @param errors Fehlereintrage bei fehlgeschlagenem Check
     */
    private void checkTPA(String TPA_DEVICE_ID, String TPA_SUBSCRIBER_ID, String TPA_SN, String TPA_PARTNR, int useCheck, Vector<CError> errors)
    {
        if (useCheck==NO_CHECK) return;

        try
        {
            isSerialNumberCorrect("TPA_DEVICE_ID", TPA_DEVICE_ID, 1, false );
        }
        catch (CError err)
        {
            errors.add(err);
        }

        try
        {
            isSerialNumberCorrect("TPA_SUBSCRIBER_ID", TPA_SUBSCRIBER_ID, 1, false );
        }
        catch (CError err)
        {
            errors.add(err);
        }

        try
        {
            isSerialNumberCorrect("TPA_SN", TPA_SN, 1, false );
        }
        catch (CError err)
        {
            errors.add(err);
        }

        try
        {
            isSerialNumberCorrect("TPA_PARTNR", TPA_PARTNR, 1, false );
        }
        catch (CError err)
        {
            errors.add(err);
        }

    }
   
   
    /**
     * pr�ft ob die Seriennummer alphanumerisch ist und die korrekte Laenge aufweist
     * 
     * @param SnName Name der der Seriennummer (f�r Fehlertext)
     * @param serialNumber die zu pruefende Seriennummer
     * @param serialLength zu pr�fende L�nge der Seriennummer (falls >1 angegeben)
     * @param checkRadio gibt an, ob auch Jahreszahl und eingschraenkte Buchstaben �berpr�ft werden
     *            sollen (fuer Radios)
     * @throws CError Falls check fehlgeschlagen ist
     */
    public void isSerialNumberCorrect(String SnName, String serialNumber, int serialLength, boolean checkRadio ) throws CError 
    {
        //maximal differennce of years
        final int MAX_DIFF_YEARS = 2;
        // invalid serials
        final String invalidSerials = "00000000000000000000";

        final String SerErrTxt=SnName+"="+serialNumber;

        //check if length is as expected, if no length is expected check if it is not zero
        if( (serialLength > 1 && (serialNumber.length() != serialLength)) || serialNumber.length() < 1 ) 
        {
            if (isDE()) throw new CError( "Seriennummer "+SerErrTxt+": Anzahl Stellen falsch ("+serialNumber.length()+" anstatt "+serialLength+")", "" ); 
            else        throw new CError( "Serial number "+SerErrTxt+": length incorrect ("+serialNumber.length()+" instead of "+serialLength+")", "" );
        }

        // check if invalid serials are present
        if (invalidSerials.length()>0) 
        {
            StringTokenizer myTokenizer = new StringTokenizer( invalidSerials, ";" );
            String token = "";
            while( myTokenizer.hasMoreTokens() ) 
            {
                token = myTokenizer.nextToken();
                if (serialNumber.equalsIgnoreCase(token)) 
                {
                    if (isDE()) 
                    {
                        throw new CError( "Seriennummer "+SerErrTxt+" in Liste ung�ltiger Seriennummern gefunden", "" );
                    }
                    else 
                    {
                        throw new CError( "Serial number "+SerErrTxt+" found in list of invalid serial number", "" );
                    }
                }
            }
        }

        // check for faulty characters
        String allowedCharacters = "";
        String allowedCharactersReg = "";
        if( checkRadio ) 
        {
            allowedCharactersReg = "[abcdefghijklmnprstuvwxyzABCDEFGHIJKLMNPRSTUVWXYZ0-9]+";
            allowedCharacters = "abcdefghijklmnprstuvwxyzABCDEFGHIJKLMNPRSTUVWXYZ0-9";
        }
        else 
        {
            allowedCharactersReg = "[a-zA-Z0-9\\-]+";
            allowedCharacters = "a-zA-Z0-9\\-";
        }

        if( serialNumber.matches( allowedCharactersReg ) == false ) 
        {
            if (isDE()) 
            {
                throw new CError( "Seriennummer "+SerErrTxt+": ung�ltige Zeichen in Seriennummer gefunden ( erlaubt: \""+allowedCharacters+"\")", "");
            }
            else 
            {
                throw new CError( "Serial number "+SerErrTxt+": found invalid characters in serial( allowed are: \""+allowedCharacters+"\")", "");
            }
        }   

        /* LOP 2182 Nach R�ckmeldung von der QMT via T. Teepe kann der Datumscheck in der PP DokuInfotainment komplett deaktiviert werden. 
         * Hinweise: Parameter muss aus Abw�rtskompatibilit�tsgr�nden erhalten bleiben. Weiterhin muss die online Doku aktualisiert werden. vgl. auch LOP 2146
 
        //special check for Car Radio Identification number (for radios/cdc)
        if( checkRadio && serialNumber.length()>6) 
        {
            Calendar calendar = new GregorianCalendar();
            int year = calendar.get( Calendar.YEAR );
            String years = "Y123456789ABCDEFGHIJKLMNPRSTVWX";
            int calc_year =((year - 2000) % years.length());;
            int index = years.indexOf( serialNumber.substring( 6, 7 ) );
            if (index == -1) 
            {
                if (isDE()) 
                {
                    throw new CError( "Seriennummer "+SerErrTxt+": ung�ltiges Zeichen bei Produktionsjahr gefunden (7. Stelle, gefunden:"+serialNumber.substring( 6, 7 )+", erlaubt: "+years+")", "");
                }
                else 
                {
                    throw new CError( "Serial number "+SerErrTxt+": found invalid character at production year (7th letter, found:"+serialNumber.substring( 6, 7 )+", allowed: "+years+")", "");
                }
            }
            else 
            {      
                int diff = Math.abs( calc_year - index );
                int calc_year_found = index - calc_year + year;
                if( diff > MAX_DIFF_YEARS || diff < 0 ) 
                {
                    if (isDE()) 
                    {
                        throw new CError( "Seriennummer "+SerErrTxt+": Produktionsjahr (7. Stelle) ung�ltig (gefunden: "+calc_year_found+", aktuelles Jahr: "+year+", Max. erlaubte Differenz: "+MAX_DIFF_YEARS+" Jahre)", "");
                    }
                    else 
                    {
                        throw new CError( "Serial number "+SerErrTxt+": production year (7th digit) not valid (found year: "+calc_year_found+", actual year: "+year+", max difference allowed: "+MAX_DIFF_YEARS+" years)", "");
                    }
                }
            }
        }
        */
    }
   
   
    /** Erzeugt XML f�r Phone
     * 
     * @param PHONE_DEVICE_ID
     * @param PHONE_SUBSCRIBER_ID
     * @param PHONE_STANDARD
     * @param PHONE_EUICC_ID
     * @param PHONE_GLONASSICC_ID
     * @param Plant
     * @return XML-Block
     */
    private StringBuffer[] dokumentPhone(String PHONE_DEVICE_ID,
            String PHONE_SUBSCRIBER_ID,
            String PHONE_STANDARD,
            String PHONE_EUICC_ID,
            String PHONE_GLONASSICC_ID, 
            String Plant)
    {
        makeResult("PHONE_DEVICE_ID", PHONE_DEVICE_ID);
        makeResult("PHONE_SUBSCRIBER_ID", PHONE_SUBSCRIBER_ID); 
        makeResult("PHONE_STANDARD", PHONE_SUBSCRIBER_ID);
        makeResult("PHONE_EUICC_ID", PHONE_EUICC_ID);
        makeResult("PHONE_GLONASSICC_ID", PHONE_GLONASSICC_ID);
        
        StringBuffer[] tempBuffer=new StringBuffer[2];
        tempBuffer[0]=new StringBuffer();
        tempBuffer[1]=new StringBuffer();

        tempBuffer[0].append( "\t\t\t" + "<communicationUnit communicationUnitClass=\"M\">" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<deviceId>" + PHONE_DEVICE_ID + "</deviceId>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<subscriberId>" + PHONE_SUBSCRIBER_ID + "</subscriberId>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<standard>" + PHONE_STANDARD + "</standard>" + "\r\n" );
        // Abw�rtskompatibilit�t, wenn der neue Wert nicht da ist, dann dokumentiere ihn nicht
        if (PHONE_EUICC_ID != null) {
        	tempBuffer[0].append( "\t\t\t\t" + "<euiccId>" + PHONE_EUICC_ID + "</euiccId>" + "\r\n" );
        }
        
        // Abw�rtskompatibilit�t, wenn der neue Wert nicht da ist, dann dokumentiere ihn nicht 
        if (PHONE_GLONASSICC_ID != null) {
        	tempBuffer[0].append( "\t\t\t\t" + "<glonassiccId>" + PHONE_GLONASSICC_ID + "</glonassiccId>" + "\r\n" );
        } 
        
        tempBuffer[0].append( "\t\t\t\t" + "<controlUnit>" + "\r\n");
        tempBuffer[0].append( "\t\t\t\t\t" + "<assemblyArea>" + Plant + "</assemblyArea>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "</controlUnit>" + "\r\n");
        tempBuffer[0].append( "\t\t\t" + "</communicationUnit>" + "\r\n" );

        tempBuffer[1].append("PHONE:"+getPr�fling().getAuftrag().getFahrgestellnummer()+";"+PHONE_DEVICE_ID+";"+PHONE_SUBSCRIBER_ID+";"+PHONE_STANDARD+";"+PHONE_EUICC_ID+";"+PHONE_GLONASSICC_ID); 
        
        return tempBuffer;
    }

    /** Erzeugt XML f�r Headunit
     * 
     * @param HEADUNIT_SN
     * @return XML-Block
     */
    private StringBuffer[] dokumentHeadunit(String HEADUNIT_SN)
    {
        makeResult("HEADUNIT_SN", HEADUNIT_SN);

        
        StringBuffer[] tempBuffer=new StringBuffer[2];
        tempBuffer[0]=new StringBuffer();
        tempBuffer[1]=new StringBuffer();

        tempBuffer[0].append( "\t\t\t" + "<communicationUnit communicationUnitClass=\"B\">" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<manufacturerCode>" + HEADUNIT_SN.substring( 0, 2 ) + "</manufacturerCode>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<manufacturerDeviceType>" + HEADUNIT_SN.substring( 2, 6 ) + "</manufacturerDeviceType>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<productionYearCode>" + HEADUNIT_SN.substring( 6, 7 ) + "</productionYearCode>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<manufacturerSerialNo>" + HEADUNIT_SN.substring( 7 ) + "</manufacturerSerialNo>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t" + "</communicationUnit>" + "\r\n" );

        tempBuffer[1].append("HEADUNIT:"+getPr�fling().getAuftrag().getFahrgestellnummer()+";"+HEADUNIT_SN);
        
        return tempBuffer;
    }

    /** Erzeugt XML f�r CDC
     * 
     * @param CDC_SN
     * @return XML-Block
     */
    private StringBuffer[] dokumentCDC(String CDC_SN)
    {
        makeResult("CDC_SN", CDC_SN);

        StringBuffer[] tempBuffer=new StringBuffer[2];
        tempBuffer[0]=new StringBuffer();
        tempBuffer[1]=new StringBuffer();

        tempBuffer[0].append( "\t\t\t" + "<communicationUnit communicationUnitClass=\"C\">" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<manufacturerCode>" + CDC_SN.substring( 0, 2 ) + "</manufacturerCode>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<manufacturerDeviceType>" + CDC_SN.substring( 2, 6 ) + "</manufacturerDeviceType>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<productionYearCode>" + CDC_SN.substring( 6, 7 ) + "</productionYearCode>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<manufacturerSerialNo>" + CDC_SN.substring( 7 ) + "</manufacturerSerialNo>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t" + "</communicationUnit>" + "\r\n" );

        tempBuffer[1].append("CDC:"+getPr�fling().getAuftrag().getFahrgestellnummer()+";"+CDC_SN);
        
        return tempBuffer;
    }

    /**  Erzeugt XML f�r MMC
     * 
     * @param MMC_SN
     * @return XML-Block
     */
    private StringBuffer[] dokumentMMC(String MMC_SN)
    {
        makeResult("MMC_SN", MMC_SN);

        StringBuffer[] tempBuffer=new StringBuffer[2];
        tempBuffer[0]=new StringBuffer();
        tempBuffer[1]=new StringBuffer();

        tempBuffer[0].append( "\t\t\t" + "<communicationUnit communicationUnitClass=\"D\">" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<manufacturerCode>" +MMC_SN.substring( 0, 2 ) + "</manufacturerCode>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<manufacturerDeviceType>" + MMC_SN.substring( 2, 6 ) + "</manufacturerDeviceType>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<productionYearCode>" + MMC_SN.substring( 6, 7 ) + "</productionYearCode>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<manufacturerSerialNo>" + MMC_SN.substring( 7 ) + "</manufacturerSerialNo>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t" + "</communicationUnit>" + "\r\n" );

        tempBuffer[1].append("MMC:"+getPr�fling().getAuftrag().getFahrgestellnummer()+";"+MMC_SN);
        
        return tempBuffer;
    }

    /** Erzeugt XML f�r SDARS
     * 
     * @param SDARS_ESN
     * @param SDARS_SN
     * @param SDARS_ENCRYPTION_TYPE
     * @param SDARS_PACKAGE_ID
     * @param SDARS_PELLET
     * @return XML-Block
     */
    private StringBuffer[] dokumentSDARS(String SDARS_ESN,
            String SDARS_SN,
            String SDARS_ENCRYPTION_TYPE,
            String SDARS_PACKAGE_ID,
            String SDARS_PELLET )
    {
        makeResult("SDARS_ESN", SDARS_ESN);
        makeResult("SDARS_SN", SDARS_SN);
        makeResult("SDARS_ENCRYPTION_TYPE", SDARS_ENCRYPTION_TYPE);
        makeResult("SDARS_PACKAGE_ID", SDARS_PACKAGE_ID);
        makeResult("SDARS_PELLET", SDARS_PELLET);

        StringBuffer[] tempBuffer=new StringBuffer[2];
        tempBuffer[0]=new StringBuffer();
        tempBuffer[1]=new StringBuffer();

        tempBuffer[0].append( "\t\t\t" + "<communicationUnit communicationUnitClass=\"S\">" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<deviceId>" + SDARS_SN + "</deviceId>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<subscriberId>" + SDARS_ESN + "</subscriberId>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<dataEncryption>" + SDARS_ENCRYPTION_TYPE + "</dataEncryption>" + "\r\n" );
        // Abw�rtskompatibilit�t, wenn die neuen Werte nicht da sind, dokumentieren wir sie nicht
        if (SDARS_PACKAGE_ID != null && SDARS_PELLET != null) {
        	tempBuffer[0].append( "\t\t\t\t" + "<packageId>" + SDARS_PACKAGE_ID + "</packageId>" + "\r\n" );
        	tempBuffer[0].append( "\t\t\t\t" + "<pellet>" + SDARS_PELLET + "</pellet>" + "\r\n" );
        }
        tempBuffer[0].append( "\t\t\t" + "</communicationUnit>" + "\r\n" );

        tempBuffer[1].append("SDARS:"+getPr�fling().getAuftrag().getFahrgestellnummer()+";"+SDARS_ESN+";"+SDARS_SN+";"+SDARS_ENCRYPTION_TYPE+";"+SDARS_PACKAGE_ID+";"+SDARS_PELLET);
        
        return tempBuffer;
    }    

    /** Erzeugt XML F�r TPA
     * 
     * @param TPA_DEVICE_ID
     * @param TPA_SUBSCRIBER_ID
     * @param TPA_SN
     * @param TPA_PARTNUMBER
     * @param TPA_STANDARD
     * @param Plant
     * @return XML-Block
     */
    private StringBuffer[] dokumentTPA(String TPA_DEVICE_ID,
            String TPA_SUBSCRIBER_ID,
            String TPA_SN,
            String TPA_PARTNUMBER,
            String TPA_STANDARD,
            String Plant)
    {
        makeResult("TPA_DEVICE_ID", TPA_DEVICE_ID);
        makeResult("TPA_SUBSCRIBER_ID", TPA_SUBSCRIBER_ID);
        makeResult("TPA_SN", TPA_SN);
        makeResult("TPA_PARTNUMBER", TPA_PARTNUMBER);
        makeResult("TPA_STANDARD", TPA_STANDARD);

        StringBuffer[] tempBuffer=new StringBuffer[2];
        tempBuffer[0]=new StringBuffer();
        tempBuffer[1]=new StringBuffer();

        tempBuffer[0].append( "\t\t\t" + "<communicationUnit communicationUnitClass=\"T\">" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<deviceId>" + TPA_DEVICE_ID + "</deviceId>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<subscriberId>" + TPA_SUBSCRIBER_ID + "</subscriberId>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "<controlUnit>" + "\r\n");
        tempBuffer[0].append( "\t\t\t\t\t" + "<assemblyArea>" + Plant + "</assemblyArea>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t\t" + "<countryVersion>" + TPA_STANDARD + "</countryVersion>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t\t" + "<partNo>" + TPA_PARTNUMBER + "</partNo>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t\t" + "<serialNo>" + TPA_SN + "</serialNo>" + "\r\n" );
        tempBuffer[0].append( "\t\t\t\t" + "</controlUnit>" + "\r\n");
        tempBuffer[0].append( "\t\t\t" + "</communicationUnit>" + "\r\n" );

        tempBuffer[1].append("TPA:"+getPr�fling().getAuftrag().getFahrgestellnummer()+";"+TPA_DEVICE_ID+";"+TPA_SUBSCRIBER_ID+";"+TPA_SN+";"+TPA_PARTNUMBER+";"+TPA_STANDARD);
        
        return tempBuffer;
    }

    /** Erzeugt XML Header
     * 
     * @param schemaversion XML Schemaversion, z.B. "0400" oder "0501"
     * @return XML-Block
     */
    private StringBuffer create_XML_header(String schemaversion) 
    {
        StringBuffer sb=new StringBuffer();

        String az = "\"";

        sb.append( "<?xml version=" + az + "1.0" + az + " encoding=" + az + "iso-8859-1" + az + "?>" + "\r\n" );
        sb.append( "<vehicleCommunicationUnits xmlns=\"http://bmw.com/vehicleCommunicationUnits\"\n" );
        sb.append( "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" );
        sb.append( "xsi:schemaLocation=\"http://bmw.com/vehicleCommunicationUnits vehicleCommunicationUnitsV"+schemaversion+".xsd\"\n" );
        sb.append( "version=\""+schemaversion.substring(0,2)+"."+schemaversion.substring(2)+"\" refSchema=\"vehicleCommunicationUnitsV"+schemaversion+".xsd\">\n" );
        sb.append( "\t" + "<vehicle vinShort=" + az + getPr�fling().getAuftrag().getFahrgestellnummer7() + az + ">" + "\r\n" );
        sb.append( "\t\t" + "<communicationUnits>" + "\r\n" );

        return sb;
    }

    /** Erzeugt XML Footer
     * 
     * @return XML-Block
     */
    private StringBuffer create_XML_footer() 
    {
        StringBuffer sb=new StringBuffer();

        sb.append( "\t\t" + "</communicationUnits>" + "\r\n" );
        sb.append( "\t" + "</vehicle>" + "\r\n" );
        sb.append( "</vehicleCommunicationUnits>" + "\r\n" );
        return sb;
    }
    
    /** Schreibt Zeile in INFOTAINMENT.DAT
     * 
     * @param lines Eintr�ge
     * @param HistorySplitSize Splitsize (0 = kein Splitting)
     * @param Plant Werksbezeichnung
     * @throws CError Fehler beim Schreiben
     */
    protected void save2History(Vector<StringBuffer> lines, int HistorySplitSize, String Plant) throws CError   
    {
    int i;
    StringBuffer line;
    String localFilePath = PB.getString(DOKU_LOCAL_PATH_PROPERTY); 

        // is the property present
        if ( (localFilePath == null) ||
             (DOKU_LOCAL_PATH_PROPERTY.equals(localFilePath)) ) 
        { 
            localFilePath = DEFAULT_DOKU_LOCAL_PATH;
        }
        File localPath = new File(localFilePath);
        
        localPath.mkdirs();
        
        Date rightNow = new Date();
        
        //*************************
        //start document locally
        //**************************
        

        //save dataset
        String localFileName = DOKU_LOCAL_PREFIX+"."+DOKU_LOCAL_EXTENSION;
        File localFile = new File(localPath, localFileName);
        try 
        {
            java.io.FileOutputStream fos=new java.io.FileOutputStream(localFile, true);
            
            //Write the dataset

            PrintWriter pw = new PrintWriter(fos, true);

            for (i=0; i<lines.size(); i++)
            {
                line=(StringBuffer)lines.get(i);
                line.append(";" + rightNow + "\r\n");                                    //append date and time //$NON-NLS-1$ //$NON-NLS-2$
                pw.print(line);
            }
            pw.flush();
            pw.close();
            fos.close();
            
            
            /*Check if the file has reached the maximum limit and partitionmode is enabled*/
            long fileSize = localFile.length();                                 //current filesize
            if ((HistorySplitSize > 0) && (fileSize > HistorySplitSize)) 
            {            //is partitionmode on and the file too big
                StringBuffer destname = new StringBuffer(DOKU_LOCAL_PREFIX);    //construct the new filename
                Calendar currDate = Calendar.getInstance();                     //current day and year
                StringBuffer expand = null;                                     //temp buffer
                expand = new StringBuffer("" + currDate.get(Calendar.YEAR));    //fetch year //$NON-NLS-1$
                while (expand.length() < 4) expand.insert(0,'0');               //expand to length 4
                destname.append(expand);                                        //append year
                expand = new StringBuffer("" + (currDate.get(Calendar.MONTH) - Calendar.JANUARY + 1));    //fetch month //$NON-NLS-1$
                while (expand.length() < 2) expand.insert(0,'0');               //expand to length 2
                destname.append(expand);                                        //append month
                expand = new StringBuffer("" + currDate.get(Calendar.DAY_OF_MONTH));    //fetch day //$NON-NLS-1$
                while (expand.length() < 2) expand.insert(0,'0');               //expand to length 2
                destname.append(expand);                                        //append day
                expand = new StringBuffer("" + currDate.get(Calendar.HOUR_OF_DAY));    //fetch hour //$NON-NLS-1$
                while (expand.length() < 2) expand.insert(0,'0');               //expand to length 2
                destname.append('_');                                           //append '_'
                destname.append(expand);                                        //append hour
                expand = new StringBuffer("" + currDate.get(Calendar.MINUTE));  //fetch minute //$NON-NLS-1$
                while (expand.length() < 2) expand.insert(0,'0');               //expand to length 2
                destname.append('_');                                           //append '_'
                destname.append(expand);                                        //append minute
                destname.append('_');
                destname.append(Plant);
                destname.append('.');                                           //append '.'
                destname.append(DOKU_LOCAL_EXTENSION);                          //append extension
                File dest = new File(localFile.getParentFile(), destname.toString()); //destination file
                if (!dest.exists()) 
                {                                            //ONLY move if file does not already exist
                    //rename orig CAS.dat to CAS_20020805_13_30.dat
                    localFile.renameTo(dest);                                       //rename CAS.dat
                }
            }
            
        } 
        catch(Exception e) 
        {
            if (isDE()) throw new CError("Fehler beim Schreiben der Backupdatei "+DOKU_LOCAL_PREFIX+"."+DOKU_LOCAL_EXTENSION, e.getMessage());  
            else        throw new CError("Failed to write backup file "+DOKU_LOCAL_PREFIX+"."+DOKU_LOCAL_EXTENSION, e.getMessage());  
        }
    }
    
    /** Tr�gt (IO) Result in m_ergListe ein 
     * 
     * @param Name Name des Results
     * @param Value Wert des Results
     */
    private void makeResult(String Name, String Value)
    {
    Ergebnis erg;
    
        erg=new Ergebnis( "DATASETENTRY", "", "", "", "", Name, "#"+Value, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );

        m_ergListe.add(erg);
    }
    
    
    /** Fehlerklasse, die Cascade-Results nach "oben" transportiert */
    private class CError extends Exception
    {
       /** */
       private static final long serialVersionUID=1L;
    
       /** Cascade Fehler Result */
       private Vector<Ergebnis> m_results; // Vector of Ergebnis
       
       /** Standardkonstruktor 
        * 
        * @param result Cascade Fehler Result
        */
       public CError(Ergebnis result)
       {
           m_results=new Vector<Ergebnis>();
           m_results.add(result);
       }
       
       /** Standardkonstruktor, erzeugt Cascade Result aus Strings
        *  
        * @param ErrorText Fehlertext
        * @param HinweisText Hinweistext
        */
       public CError(String ErrorText, String HinweisText)
       {
           m_results=new Vector<Ergebnis>();
           m_results.add(new Ergebnis("dokumentInfotainment", "", "", "", "", "", "", "", "", "0", "", "", "", ErrorText, HinweisText, Ergebnis.FT_NIO));
       }
       
       /** Standardkonstruktor, erzeugt Cascade Results aus einem Vector von CError objekten 
        * (Sammelt mehrere Fehler in einem Fehlerobjekt)
        * 
        * @param errors Vector vom Typ CError
        */
       public CError(Vector<CError> errors)
       {
       int i;
       CError err;
       Vector<Ergebnis> res2;
       
           m_results=new Vector<Ergebnis>();
           for (i=0; i<errors.size(); i++)
           {
               err=(CError)errors.get(i);
               res2=err.getCascadeResults();
               m_results.addAll(res2);
           }
       }
       
       /** Liefert Cascade Fehler Result
        * 
        * @return Cascade Fehler Result
        */
       public Vector<Ergebnis> getCascadeResults() { return m_results; }
   }
   
   /** Ermittelt, ob die Sprache in Cascade deutsch oder englisch ist
    * 
    * @return true: Deutsch, false: nicht Deutsch (-> Englisch)
    */
   private static boolean isDE() 
   {
       try 
       {
           if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true ) return true;
           else return false;
       }
       catch (Exception e) 
       {
           return false;   //default is english
       }
   }
}



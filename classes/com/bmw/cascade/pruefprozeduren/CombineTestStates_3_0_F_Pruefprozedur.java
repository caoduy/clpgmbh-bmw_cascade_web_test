/*
 * CombineTestStates_1_0_F_Pruefprozedur.java
 *
 */

package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.appframework.logging.LogLevel;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefprozeduren.Ergebnis;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.util.*;
import com.bmw.cascade.util.logging.CascadeLogging;

/**
 * Testprocedure in the Cascade-System that permits to combine the status
 * from previous test-steps in the "Pr�fumfang" (test-sequence).
 * @author Andreas Mair, BMW TI-432
 * @version 0_0_1_FA 04.08.2004 AM  Erstellung.<BR>
 * @version 0_0_2_T 07.02.2017 MKe  deprecated Code entfernt.<BR>
 * @version 0_0_3_F 07.02.2017 MKe  F-Version.<BR>
 */
public class CombineTestStates_3_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public CombineTestStates_3_0_F_Pruefprozedur() {
	}

	// Needed for every test-procedure:
	//---------------------------------
	private Vector i_oErgListe = new Vector(); // result vector
	private int i_iStatus = STATUS_EXECUTION_OK; // execution status

	// Actual Cascade-result
	private Ergebnis i_oResult;
	// Strings for the result objects in execute
	private String i_sID = ""; // ID
	private String i_sWerkzeug = ""; // werkzeug
	private String i_sParameter_1 = ""; // parameter_1
	private String i_sParameter_2 = ""; // parameter_2
	private String i_sParameter_3 = ""; // parameter_3
	private String i_sErgebnis = ""; // ergebnis
	private String i_sErgebniswert = ""; // ergebniswert
	private String i_sMinWert = ""; // minWert
	private String i_sMaxWert = ""; // maxWert
	private String i_sWiederholungen = ""; // wiederholungen
	private String i_sFrageText = ""; // frageText
	private String i_sAntwortText = ""; // antwortText
	private String i_sAnweisText = ""; // anweisText
	private String i_sFehlerText = ""; // fehlertext
	private String i_sHinweisText = ""; // hinweistext
	private String i_sFehlertyp = ""; // fehlertyp

	// Specific for this procedure
	//----------------------------

	// Number of test steps
	private int i_iTestSteps = 0; // Valid number of arguments beginning with STEP..
	private int i_iMaxStepIndex = 0; // Maximum STEP.. index found (must not be equal to the number of STEP's)
	Vector i_vStepIndex = new Vector(); // Integer
	Vector i_vExecStates = new Vector(); // Integer
	Vector i_vTestSteps = new Vector(); // String
	private String i_sCombination = null;
	private String i_sCombinationParam = null;

	// Debug flags
	boolean i_bCombineTestStatesDebug_Standard = false;
	boolean i_bCombineTestStatesDebug_CheckArgs = false;
	boolean i_bCombineTestStatesDebug_DebugMode = false;

	/**
	 * Creates a new instance of TestStepSequence... .
	 * @param pruefling "Test-item"-class containing this concrete test-procedure
	 * @param pruefprozName name of the test-procedure
	 * @param hasToBeExecuted logical condition if the test-procedure schould be executed
	 */
	public CombineTestStates_3_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initializes the arguments.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Gets the mandatory arguments
	 */
	public String[] getRequiredArgs() {
		String[] args = { "STEP1", "STEP2", "COMBINATION" };
		return args;
	}

	/**
	 * Gets the optional arguments.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "STEP3..N", "DEBUG" };
		return args;
	}

	/**************************************************************************/
	/**************************************************************************/

	/**
	 * Checks if
	 * - all mandatory arguments are present
	 * - the arguments are valid
	 * @return true if the check was successful.
	 */
	public boolean checkArgs() {
		boolean ok = false; // return value

		// Debug print of all arguments
		if( i_bCombineTestStatesDebug_CheckArgs ) {
			CascadeLogging.getLogger().log( LogLevel.DEBUG, "Start checkArgs() from CombineTestStates." );
			CascadeLogging.getLogger().log( LogLevel.DEBUG, "" );
			CascadeLogging.getLogger().log( LogLevel.DEBUG, "All Arguments from this PP: " + this.getName() );
			String sGivenkey, sActualArg;
			Enumeration enu = getArgs().keys();
			while( enu.hasMoreElements() ) {
				// The actual argument
				sGivenkey = (String) enu.nextElement();
				sActualArg = getArg( sGivenkey );
				CascadeLogging.getLogger().log( LogLevel.DEBUG, sGivenkey + " = " + sActualArg );
			}
			CascadeLogging.getLogger().log( LogLevel.DEBUG, "---" );
		}

		int i, j;
		String sGivenkey, sActualArg;
		Enumeration enu = getArgs().keys();
		String[] requiredArgs = getRequiredArgs();
		String[] optionalArgs = getOptionalArgs();
		int requiredN = requiredArgs.length;
		int optionalN = optionalArgs.length;
		boolean required = false, optional = false;

		// For all arguments
		while( enu.hasMoreElements() ) {
			sGivenkey = (String) enu.nextElement();
			sActualArg = getArg( sGivenkey );
			required = false;
			optional = false;
			j = 0;
			// Tests, if required argument
			while( (required == false) && (j < requiredN) ) {
				if( sGivenkey.equals( requiredArgs[j] ) == true )
					required = true;
				j++;
			}

			// Tests, if valid argument
			if( sGivenkey.startsWith( "STEP" ) ) {
				// Tests if the index is valid
				String sStepIndex = sGivenkey.substring( 4 );
				int iStepIndex;
				try {
					iStepIndex = Integer.parseInt( sStepIndex );
					if( iStepIndex > i_iMaxStepIndex )
						i_iMaxStepIndex = iStepIndex;
				} catch( NumberFormatException e ) {
					if( i_bCombineTestStatesDebug_CheckArgs ) {
						CascadeLogging.getLogger().log( LogLevel.DEBUG, "Argument error in checkArgs()!" );
						CascadeLogging.getLogger().log( LogLevel.DEBUG, "Invalid index of test-step in" + sGivenkey );
					}
					return false;
				}
				if( iStepIndex > 2 )
					optional = true;

				/* Tests, if there are present 1,2, or 3 parameter details to define
				   the relevant test-step. */
				String[] sStepParams;
				try {
					sStepParams = extractValues( sActualArg );
				} catch( PPExecutionException e ) {
					if( i_bCombineTestStatesDebug_CheckArgs ) {
						CascadeLogging.getLogger().log( LogLevel.DEBUG, "Argument error in checkArgs()!" );
						CascadeLogging.getLogger().log( LogLevel.DEBUG, "Step parameter not available!" );
					}
					return false;
				}
				int iStepParams = sStepParams.length;
				if( iStepParams < 1 || iStepParams > 3 ) {
					if( i_bCombineTestStatesDebug_CheckArgs ) {
						CascadeLogging.getLogger().log( LogLevel.DEBUG, "Argument error in checkArgs()!" );
						CascadeLogging.getLogger().log( LogLevel.DEBUG, "Invalid number of parameter details in" + sActualArg );
					}
					return false;
				}
				i_iTestSteps++;
			} // End: if starts with "STEP"
		} // End: for all arguments

		ok = true;
		// Final debug print
		if( i_bCombineTestStatesDebug_CheckArgs ) {
			CascadeLogging.getLogger().log( LogLevel.DEBUG, "ok = " + ok );
			CascadeLogging.getLogger().log( LogLevel.DEBUG, "End checkArgs() from CombineTestStates." );
		}
		return ok;
	} // End checkArgs()

	/**************************************************************************/
	/**************************************************************************/

	/**
	 * Executes the test-procedure
	 * @param info Information about the execution.
	 */
	public void execute( ExecutionInfo info ) {
		// Initialisation
		initBeforeExecute();

		/* Sets debug flags as a result from the optional debug argument in the PP or as a result from
		 * the debug attribute in the P. */
		String debugString = null;
		if( getArg( "DEBUG" ) != null ) {
			debugString = getArg( "DEBUG" ); // debug argument in PP (preference)
		} else if( getPr�fling().getAttribut( "DEBUG" ) != null ) {
			debugString = getPr�fling().getAttribut( "DEBUG" ); // debug attribute in P
		}
		if( debugString != null ) {
			CascadeLogging.getLogger().log( LogLevel.INFO, "" );
			CascadeLogging.getLogger().log( LogLevel.INFO, "The debug string is: " + debugString );
			String[] debugFlags = splitArg( debugString );
			int n = debugFlags.length;
			if( n >= 1 ) {
				for( int i = 0; i < n; i++ ) {
					if( debugFlags[i].equalsIgnoreCase( "COMBINE_TEST_STATES" ) ) {
						i_bCombineTestStatesDebug_Standard = true;
					} else if( debugFlags[i].equalsIgnoreCase( "COMBINE_TEST_STATES_ARGS" ) ) {
						i_bCombineTestStatesDebug_CheckArgs = true;
					} else if( debugFlags[i].equalsIgnoreCase( "COMBINE_TEST_STATES_DEBUG_MODE" ) ) {
						i_bCombineTestStatesDebug_DebugMode = true;
					} else if( debugFlags[i].equalsIgnoreCase( "ALL" ) ) {
						i_bCombineTestStatesDebug_Standard = true;
						i_bCombineTestStatesDebug_CheckArgs = true;
						i_bCombineTestStatesDebug_DebugMode = true;
					}
				}
			}
		}

		// Initial debug lines
		if( i_bCombineTestStatesDebug_Standard ) {
			CascadeLogging.getLogger().log( LogLevel.DEBUG, "---" );
			CascadeLogging.getLogger().log( LogLevel.DEBUG, "Start execute() from TestStepSequence." );
			CascadeLogging.getLogger().log( LogLevel.DEBUG, "Test step: " + this.getName() );
			CascadeLogging.getLogger().log( LogLevel.DEBUG, "---" );
		}
		if( debugString != null && i_bCombineTestStatesDebug_DebugMode ) {
			CascadeLogging.getLogger().log( LogLevel.DEBUG, "The debug string is: " + debugString );
			CascadeLogging.getLogger().log( LogLevel.DEBUG, "As a result of debug arguments/attributes the debug flags are: " );
			CascadeLogging.getLogger().log( LogLevel.DEBUG, "i_bCombineTestStatesDebug_Standard = " + i_bCombineTestStatesDebug_Standard );
			CascadeLogging.getLogger().log( LogLevel.DEBUG, "i_bCombineTestStatesDebug_CheckArgs = " + i_bCombineTestStatesDebug_CheckArgs );
			CascadeLogging.getLogger().log( LogLevel.DEBUG, "i_bCombineTestStatesDebug_DebugMode = " + i_bCombineTestStatesDebug_DebugMode );
		}

		try {
			// Parameter check
			//----------------
			if( !checkArgs() ) {
				if( i_bCombineTestStatesDebug_CheckArgs ) {
					CascadeLogging.getLogger().log( LogLevel.DEBUG, "Argument error on using checkArgs() in execute()" );
				}

				resetResultStrings();
				i_sID = "ARGUMENT_ERROR";
				i_sWerkzeug = "CHECK PARAMETERS IN EXECUTE";
				i_sFehlerText = PB.getString( "parametrierfehler" );
				i_sHinweisText = "CHECK ARGS NOT SUCCESSFUL!";
				i_sFehlertyp = Ergebnis.FT_NIO_SYS;
				// Pass a new exception upwards (PPExecutionException -> error in parameters!)
				throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			} else {
				if( i_bCombineTestStatesDebug_CheckArgs ) {
					CascadeLogging.getLogger().log( LogLevel.DEBUG, "Arguments ok on using checkArgs() in execute()" );
				}
			}

			// Teststep indexes (order of execution)
			int i;
			String sActualArg;
			i_vStepIndex.setSize( i_iTestSteps );
			i_vExecStates.setSize( i_iTestSteps );
			i_vTestSteps.setSize( i_iTestSteps );
			int iFoundIndex = 0;
			for( i = 0; iFoundIndex < i_iTestSteps || i < i_iMaxStepIndex; i++ ) {
				sActualArg = getArg( "STEP" + i );
				if( sActualArg != null ) {
					i_vStepIndex.setElementAt( new Integer( i ), iFoundIndex );
					iFoundIndex++;
				}
			}

			// Retrieves all execution states
			int iStepId = 0;
			i_sParameter_1 = getArg( "STEP1" );
			i_sParameter_2 = getArg( "STEP2" );
			for( i = 0; i < i_iTestSteps; i++ ) {
				iStepId = ((Integer) i_vStepIndex.elementAt( i )).intValue();
				sActualArg = getArg( "STEP" + iStepId );
				i_vTestSteps.setElementAt( sActualArg, i );
				i_vExecStates.setElementAt( new Integer( getPPExecStatus( sActualArg ) ), i );
			} // End: For all test steps

			// Combination of execution states
			i_sParameter_3 = getArg( "COMBINATION" );
			String[] saCombination = extractValues( getArg( "COMBINATION" ) );
			i_sCombination = saCombination[0];
			i_sCombinationParam = saCombination[1];

			// If 'k' of 'n' results are IO -> combination IO
			if( i_sCombination.equals( "MIN_IO_NUMBER" ) ) {
				int n = i_iTestSteps;
				int k = 0;
				int iIoNumber;
				try {
					k = Integer.parseInt( i_sCombinationParam );
				} catch( NumberFormatException e ) {
					resetResultStrings();
					i_sID = "ARGUMENT_ERROR";
					i_sWerkzeug = "EXECUTE IN COMBINE_TEST_STATES";
					i_sFehlerText = PB.getString( "parametrierfehler" );
					i_sHinweisText = "INVALID COMBINATION PARAMETER: " + i_sCombinationParam;
					i_sFehlertyp = Ergebnis.FT_NIO_SYS;
					// Pass a new exception upwards (PPExecutionException -> error in parameters!)
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				}
				iIoNumber = getIOResultNumber();
				i_sErgebnis = "IO-result number";
				i_sErgebniswert = String.valueOf( iIoNumber );
				i_sMinWert = String.valueOf( k );
				i_sMaxWert = String.valueOf( n );
				i_sWiederholungen = "1";

				// IO-Result
				if( iIoNumber >= k ) {
					i_iStatus = STATUS_EXECUTION_OK;
				}
				// NIO-Result
				else {
					i_iStatus = STATUS_EXECUTION_ERROR;
					i_sFehlerText = "Not enough IO-results"; // fehlertext
					i_sHinweisText = "NIO: " + getNIOTestSteps(); // hinweistext
					i_sFehlertyp = Ergebnis.FT_NIO;
				}
			}
			// Invalid combination rule
			else {
				resetResultStrings();
				i_sID = "ARGUMENT_ERROR";
				i_sWerkzeug = "EXECUTE IN COMBINE_TEST_STATES";
				i_sFehlerText = PB.getString( "parametrierfehler" );
				i_sHinweisText = "INVALID COMBINATION RULE: " + i_sCombination;
				i_sFehlertyp = Ergebnis.FT_NIO_SYS;
				// Pass a new exception upwards (PPExecutionException -> error in parameters!)
				throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			}

			if( i_bCombineTestStatesDebug_Standard ) {
				CascadeLogging.getLogger().log( LogLevel.DEBUG, "States of all test-steps:" );
				for( int s = 0; s < i_iTestSteps; s++ ) {
					String arg = "STEP" + ((Integer) i_vStepIndex.elementAt( s )).intValue();
					String ts = (String) i_vTestSteps.elementAt( s );
					int iExecState = ((Integer) i_vExecStates.elementAt( s )).intValue();
					String sExecState = getPPExecStatusAsString( iExecState );
					CascadeLogging.getLogger().log( LogLevel.DEBUG, arg + "=" + ts + " with exec-state: " + sExecState );
				}
			}

			// Result for IO, NIO
			i_oResult = new Ergebnis( i_sID, i_sWerkzeug, i_sParameter_1, i_sParameter_2, i_sParameter_3, i_sErgebnis, i_sErgebniswert, i_sMinWert, i_sMaxWert, i_sWiederholungen, i_sFrageText, i_sAntwortText, i_sAnweisText, i_sFehlerText, i_sHinweisText, i_sFehlertyp );
			i_oErgListe.add( i_oResult );
		} // End: Try of execute
		catch( PPExecutionException ppee ) {
			i_iStatus = STATUS_EXECUTION_ERROR;
			ppee.printStackTrace();
			i_oResult = new Ergebnis( i_sID, i_sWerkzeug, i_sParameter_1, i_sParameter_2, i_sParameter_3, i_sErgebnis, i_sErgebniswert, i_sMinWert, i_sMaxWert, i_sWiederholungen, i_sFrageText, i_sAntwortText, i_sAnweisText, i_sFehlerText, i_sHinweisText, i_sFehlertyp );
			i_oErgListe.add( i_oResult );
		} catch( Exception ex ) {
			i_iStatus = STATUS_EXECUTION_ERROR;
			ex.printStackTrace();
			resetResultStrings();
			i_sID = "EXCEPTION";
			i_sFehlerText = "EXCEPTION";
			i_sHinweisText = PB.getString( "unerwarteterLaufzeitfehler" );
			i_sFehlertyp = Ergebnis.FT_NIO_SYS;

			i_oResult = new Ergebnis( i_sID, i_sWerkzeug, i_sParameter_1, i_sParameter_2, i_sParameter_3, i_sErgebnis, i_sErgebniswert, i_sMinWert, i_sMaxWert, i_sWiederholungen, i_sFrageText, i_sAntwortText, i_sAnweisText, i_sFehlerText, i_sHinweisText, i_sFehlertyp );
			i_oErgListe.add( i_oResult );
		} catch( Throwable t ) {
			i_iStatus = STATUS_EXECUTION_ERROR;
			t.printStackTrace();
			resetResultStrings();
			i_sID = "THROWABLE";
			i_sFehlerText = "THROWABLE";
			i_sHinweisText = PB.getString( "unerwarteterLaufzeitfehler" );
			i_sFehlertyp = Ergebnis.FT_NIO_SYS;

			i_oResult = new Ergebnis( i_sID, i_sWerkzeug, i_sParameter_1, i_sParameter_2, i_sParameter_3, i_sErgebnis, i_sErgebniswert, i_sMinWert, i_sMaxWert, i_sWiederholungen, i_sFrageText, i_sAntwortText, i_sAnweisText, i_sFehlerText, i_sHinweisText, i_sFehlertyp );
			i_oErgListe.add( i_oResult );
		}

		//i_iStatus setzen
		setPPStatus( info, i_iStatus, i_oErgListe );
	} // End execute

	//**************************************************************************
	// Private Methodes
	//**************************************************************************

	/**
	 * Reset the result strings
	 */
	private void resetResultStrings() {
		i_sID = ""; // ID
		i_sWerkzeug = ""; // werkzeug
		i_sParameter_1 = ""; // parameter_1
		i_sParameter_2 = ""; // parameter_2
		i_sParameter_3 = ""; // parameter_3
		i_sErgebnis = ""; // ergebnis
		i_sErgebniswert = ""; // ergebniswert
		i_sMinWert = ""; // minWert
		i_sMaxWert = ""; // maxWert
		i_sWiederholungen = ""; // wiederholungen
		i_sFrageText = ""; // frageText
		i_sAntwortText = ""; // antwortText
		i_sAnweisText = ""; // anweisText
		i_sFehlerText = ""; // fehlertext
		i_sHinweisText = ""; // hinweistext
		i_sFehlertyp = ""; // fehlertyp
	}

	/**
	 * Initializes the necessary instance-variables before a new execution
	 */
	private void initBeforeExecute() {
		i_oErgListe = new Vector();
		i_iStatus = STATUS_EXECUTION_OK;
		resetResultStrings();

		i_vStepIndex.clear();
		i_vExecStates.clear();
		i_vTestSteps.clear();

		i_sID = getName();
		i_sWerkzeug = "COMBINE TEST STATES";
		i_sFehlertyp = Ergebnis.FT_IO;

		i_iTestSteps = 0;
		i_iMaxStepIndex = 0;
	}

	/**
	 * Gets the number of IO-results from the vector i_vExecStates
	 */
	private int getIOResultNumber() {
		int iIoNum = 0;
		for( int s = 0; s < i_iTestSteps; s++ ) {
			if( ((Integer) i_vExecStates.elementAt( s )).intValue() == STATUS_EXECUTION_OK ) {
				iIoNum++;
			}
		}
		return iIoNum;
	}

	/**
	 * Gets the NIO-test-steps as list in a string-object where the test steps have the
	 * form '[Pr�flingsname.]Pr�fschrittname' and are devided by ';'.
	 */
	private String getNIOTestSteps() {
		String sNioSteps = null;
		for( int s = 0; s < i_iTestSteps; s++ ) {
			if( ((Integer) i_vExecStates.elementAt( s )).intValue() != STATUS_EXECUTION_OK ) {
				if( sNioSteps == null ) {
					sNioSteps = (String) i_vTestSteps.elementAt( s );
				} else {
					sNioSteps = sNioSteps + ";" + (String) i_vTestSteps.elementAt( s );
				}
			}
		}
		return sNioSteps;
	}

	/**
	 * Gets the execution status for any test-step in the virtual vehicle
	 * relevant for this test-step.
	 * @param sPP test-step in a string with the form '[Pr�flingsname.]Pr�fschrittname'
	 * @return the execution status as int value.
	 */
	private int getPPExecStatus( String sPP ) throws InformationNotAvailableException {
		Pruefprozedur oTs = null; // Test-step
		oTs = getPP( sPP );
		return oTs.getExecStatus();
	}

	/**
	 * Gets any other test-step in the virtual vehicle relevant for this test-step.
	 * @param sPP test-step in a string with the form '[Pr�flingsname.]Pr�fschrittname'
	 * @return the test-step-object
	 */
	private Pruefprozedur getPP( String sPP ) throws InformationNotAvailableException {
		Pruefprozedur oTs = null; // Test-step, return value
		String sPrueflingName;
		String sTestStepName;

		int iFirstDotPos;
		iFirstDotPos = sPP.indexOf( '.' );
		// Test-step in the current Pruefling
		if( iFirstDotPos == -1 ) {
			oTs = this.getPr�fling().getPr�fprozedur( sPP );
		}
		// Test-step in another Pruefling
		else if( iFirstDotPos == sPP.lastIndexOf( "." ) ) {
			sPrueflingName = sPP.substring( 0, iFirstDotPos );
			sTestStepName = sPP.substring( iFirstDotPos + 1 );
			oTs = this.getPr�fling().getAuftrag().getPr�fling( sPrueflingName ).getPr�fprozedur( sTestStepName );
		}
		// 2 or more dots -> invalid test-step-name
		else {
			throw new InformationNotAvailableException( "Two or more dots in " + sPP + "! should be:  '[Pr�flingsname.]Pr�fschrittname'" );
		}

		if( oTs == null ) {
			throw new InformationNotAvailableException( "Test step " + sPP + " not avialable!" );
		}

		return oTs;
	}

	/**
	 * Gets the status as string value for the given status as int-value.
	 * @param intStatus test-step in a string with the form '[Pr�flingsname.]Pr�fschrittname'
	 * @return the execution status as string value.
	 */
	private String getPPExecStatusAsString( int intStatus ) {
		String sStatus = null;
		switch( intStatus ) {
			case STATUS_UNDEFINED:
				sStatus = "STATUS_UNDEFINED";
				break;
			case STATUS_NOT_EXECUTED:
				sStatus = "STATUS_NOT_EXECUTED";
				break;
			case STATUS_RUNNING:
				sStatus = "STATUS_RUNNING";
				break;
			case STATUS_EXECUTION_OK:
				sStatus = "STATUS_EXECUTION_OK";
				break;
			case STATUS_EXECUTION_ERROR:
				sStatus = "STATUS_EXECUTION_ERROR";
				break;
			case STATUS_EXECUTION_ABORTED_BY_USER:
				sStatus = "STATUS_EXECUTION_ABORTED_BY_USER";
				break;
			case STATUS_EXECUTION_RETRY:
				sStatus = "STATUS_EXECUTION_RETRY";
				break;
			case STATUS_EXECUTION_ERROR_IGNORE:
				sStatus = "STATUS_EXECUTION_ERROR_IGNORE";
				break;
			case STATUS_DISABLED:
				sStatus = "STATUS_DISABLED";
				break;
			default:
				sStatus = "STATUS_UNDEFINED";
		}
		return sStatus;
	}
}

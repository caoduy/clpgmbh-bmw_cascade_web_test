/*
 * DiagLeseFSEinzelUDS_VX_X_X_YY_Pruefprozedur.java
 *
 * Created on 24.04.2008
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import java.util.Map.Entry;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;

//import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;

/**
 * Implementierung der Pr�fprozedur, die per Diagnose den Fehlerspeicher ausliest und
 * dabei Fehler auf Basis des Fehlerortes ignoriert bzw. weitere Analysen durch Ausf�hrung der
 * Analyse-PPs vornimmt. Sollten nur zu ignorierende Fehler vorliegen, wird mit der �bergebenen
 * Clear-PP das L�schen vorgenommen.
 * @author Borutta, Schumann, Gebauer, K�ser, Buboltz, Wolf
 * @version 0_0_1_FA  24.04.08  ib  Uebernahme der PP DiagLeseFSUDS_3_0_F_Pruefprozedur 
 * @version 2_0_T		2.10.09 CS Bugfix: Hexcode des Vorg�nger FS-Eintrags wurde immer angezeigt
 * @version 3_0_F		2.10.09 FG F_VERSION
 * @version 5_0_F		20.02.13 MK Erweiterte Ausgaben f�r Debug eingebaut
 * @version 6_0_T		23.09.14 CW Parametrierung der fortlaufenden FCODES und zugeh�rigen HWTs toleranter gemacht
 * @version 7_0_F		20.10.14 CW Freigabe 6_0_T
 * @version	10_0_T		03.08.16 TB Erweiterungen f�r Paralleldiagnose analog zur DiagToleranzPP (LOP 2015), �berfl�ssiges Import entfernt + Generics eingebaut + Compiler Warnungen behoben<BR>
 * @version	11_0_F		25.01.17 MKe F-Version  
 * @version	12_0_T		23.01.18 MKe Bugfix bei der Freigabe der Paralleldiagnose  
 * @version	13_0_F		23.01.18 MKe F-Version 
 */
public class DiagLeseFSEinzelUDS_13_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagLeseFSEinzelUDS_13_0_F_Pruefprozedur() {
	}

	/**
	  * erzeugt eine neue Pruefprozedur.
	  * @param pruefling Klasse des zugeh. Pr�flings
	  * @param pruefprozName Name der Pr�fprozedur
	  * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	  */
	public DiagLeseFSEinzelUDS_13_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		//* letztes Argument angeh�ngt analog zu DiagLeseFSUDS
		String[] args = { "JOB", "IGNORE", "ANALYSE_PP", "CLEAR_PP", "IGNORE_MESSAGE", "FCODE[1..N]", "HWT[1..N]", "PPLDB_TEXT", "DEBUG", "TAG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "SGBD" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	@Override
	public boolean checkArgs() {
		Pruefprozedur p;
		String temp;
		boolean debug = (getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
		boolean ok;
		int i;

		try {

			//  super.checkArgs() auskommentiert, da ansonsten bei FCODE1 und HWT der Aufruf fehlschl�gt
			ok = true; //super.checkArgs();
			//System.out.print("Check Args " + String.valueOf(ok));
			if( ok == true ) {
				//SGBD
				temp = getArg( getRequiredArgs()[0] );
				if( temp.indexOf( ';' ) != -1 )
					return false;
				//Job
				temp = getArg( getOptionalArgs()[0] );
				if( temp != null ) {
					if( temp.indexOf( ';' ) != -1 )
						return false;
				}
				//Ignore
				temp = getArg( getOptionalArgs()[1] );
				try {
					if( temp != null )
						temp = extractErrorNumbers( temp );
				} catch( NumberFormatException e ) {
					return false;
				}
				//Analyse-PPs
				temp = getArg( getOptionalArgs()[2] );
				if( temp != null ) {
					String[] tempArray = splitArg( temp );
					for( i = 0; i < tempArray.length; i++ ) {
						p = getPr�fling().getPr�fprozedur( tempArray[i] );
						if( p == null )
							return false;
						if( p.checkArgs() == false )
							return false;
					}
				}
				//Clear-PP
				temp = getArg( getOptionalArgs()[3] );
				if( temp != null ) {
					p = getPr�fling().getPr�fprozedur( temp );
					if( p == null )
						return false;
					if( p.checkArgs() == false )
						return false;
				}
			}
			return ok;
		} catch( Exception e ) {
			if( debug )
				System.out.println( e.getMessage() );
			return false;
		} catch( Throwable e ) {
			if( debug )
				System.out.println( e.getMessage() );
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		Pruefprozedur p;
		String[] tempArray;
		String temp = null;
		String sgbd = null;
		String job;
		String jobResults;
		String ignore;
		String analyse;
		String clear;
		int ergS�tze = 0;
		int i, j, k, l;
		boolean hexCode = false;
		boolean ignoreMessage = false;
		String ignoreMessageText = null;

		//* Variablen eingef�gt analog zu DiagLeseFSUDS
		boolean errorPresent = false;
		String[] analyseNames = new String[0];
		Pruefprozedur[] analysePruefprozeduren = new Pruefprozedur[0];
		Pruefprozedur clearPruefprozedur = null;
		Hashtable<String, Ergebnis> temporaryResults = new Hashtable<String, Ergebnis>();
		boolean debug = (getArg( "DEBUG" ) != null) && (getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" )) ? true : false;
		boolean DEBUG = false;
		EdiabasProxyThread ediabas = null;

		//Paralleldiagnose
		boolean parallel = false;
		boolean alreadyReleased = false;
		String tag = null;
		DeviceManager devMan = null;

		int FCodeAnzahl; //  Variable fuer die Anzahl angegeben FCodes
		String hwt;
		Hashtable<String, String> allArgs = getArgs();
		List<List<String>> fCodeList = new ArrayList<List<String>>();

		/***********************************************
		  * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
		  * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		 ***********************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		try {
			//Parameter holen  
			try {
				// Parameter Existenz
				if( checkArgs() == false )
					throw new PPExecutionException( "Parameterexistenz" );
				// SGBD
				sgbd = extractValues( getArg( getRequiredArgs()[0] ) )[0];
				if( debug )
					System.out.println( "Aktuelle SGBD " + String.valueOf( sgbd ) );

				// Jobname
				job = getArg( getOptionalArgs()[0] );
				if( debug )
					System.out.println( "Aktueller Job " + String.valueOf( job ) );
				if( job == null ) {
					job = "FS_LESEN";
					if( debug )
						System.out.println( "job wurde FS_LESEN zugewiesen" );
				}

				// Fehler die auszublenden sind
				ignore = getArg( getOptionalArgs()[1] ); //IGNORE
				if( ignore != null ) {
					ignore = extractErrorNumbers( ignore );
					if( debug )
						System.out.println( "IGNORE " + String.valueOf( ignore ) );
				}

				// Analyse Pr�fprozedur(en)
				analyse = getArg( getOptionalArgs()[2] );
				if( analyse != null ) {
					analyseNames = splitArg( analyse );
					analysePruefprozeduren = new Pruefprozedur[analyseNames.length];
					for( i = 0; i < analysePruefprozeduren.length; i++ ) {
						analysePruefprozeduren[i] = getPr�fling().getPr�fprozedur( analyseNames[i] );
					}
				}

				temp = getPr�fling().getAuftrag().getBaureihe();
				if( temp.startsWith( "E6" ) == true ) {
					hexCode = true;
					jobResults = ";F_ORT_NR;F_ORT_TEXT;F_HEX_CODE;";
					if( debug )
						System.out.println( "Aktuelle Jobresults " + String.valueOf( jobResults ) );
				} else {
					jobResults = ";F_ORT_NR;F_ORT_TEXT;";
					if( debug )
						System.out.println( "Aktuelle Jobresults " + String.valueOf( jobResults ) );
				}

				//alternatives Einlesen der Fehlercodes
				Set<Entry<String, String>> argSet = allArgs.entrySet();
				for( Iterator<Map.Entry<String, String>> it = argSet.iterator(); it.hasNext(); ) {
					Entry<String, String> singleItem = it.next();
					if( singleItem.getKey().startsWith( "FCODE" ) ) {
						fCodeList.add( new ArrayList<String>( Arrays.asList( singleItem.getKey(), singleItem.getValue() ) ) );
						if( debug ) {
							System.out.println( "Aktueller FCode " + singleItem.getKey() );
							System.out.println( "Aktueller FCode Wert " + singleItem.getValue() );
						}
					}
				}
				FCodeAnzahl = fCodeList.size();
				// Einlesen der Anzahl der angegeben Fehler-Codes und Ablage aller Fehler-Codes
				if( debug )
					System.out.println( "Anzahl FCODES " + String.valueOf( FCodeAnzahl ) );

				//HWTs m�ssen nicht mehr bestimmt werden, da diese mit Hilfe der zugeh�rigen FCodes gefunden werden k�nnen

				if( analyse != null ) {
					analyseNames = splitArg( analyse );
					analysePruefprozeduren = new Pruefprozedur[analyseNames.length];
					for( i = 0; i < analysePruefprozeduren.length; i++ ) {
						analysePruefprozeduren[i] = getPr�fling().getPr�fprozedur( analyseNames[i] );
					}
					for( i = 0; i < analyseNames.length; i++ ) {
						p = getPr�fling().getPr�fprozedur( analyseNames[i] );
						temp = extractErrorNumbers( p.getArg( "FORT" ) );
						if( (p.getArg( "SGBD" ) == null) && (p.getArg( "JOB" ) == null) ) {
							//Holen der relevanten Fehlerspeicherinformationen
							j = 1;
							while( p.getArg( "RESULT" + j ) != null ) {
								temp = p.getArg( "RESULT" + j );
								j++;
								if( jobResults.indexOf( ";" + temp + ";" ) < 0 )
									jobResults = jobResults + temp + ";";
							}
						}
					}
				}
				jobResults = jobResults.substring( 1, jobResults.length() - 1 );
				// Job um Fehlerspeicher zu l�schen (falls vorhanden)
				clear = getArg( getOptionalArgs()[3] );
				//System.out.println("clear variable " + String.valueOf(clear));
				// Sollen Fehler auf Fehlerprotokoll erscheinen, aber mit "Fehler kann ignoriert werden)
				temp = getArg( getOptionalArgs()[4] );
				if( temp != null ) {
					if( temp.equalsIgnoreCase( "TRUE" ) ) {
						ignoreMessage = true;
						ignoreMessageText = PB.getString( "ignoreMessage" );
						if( ignoreMessageText.startsWith( "ignore" ) ) {
							ignoreMessageText = "Fehler kann ignoriert werden";
						}
					}
				}
			} catch( PPExecutionException e ) {
				//e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				if( debug )
					System.out.println( e.getMessage() );
				throw new PPExecutionException();
			}

			if( ignoreMessage ) {
				// Anzeigetext holen
				ignoreMessageText = PB.getString( "ignoreMessage" );
				if( ignoreMessageText.startsWith( "ignore" ) ) {
					ignoreMessageText = "Fehler kann ignoriert werden";
				}
			}

			try {
				if( DEBUG )
					System.out.println( "Ausf�hrung" );

				// Paralleldiagnose, Pr�fstandvariabel auslesen, ob Paralleldiagnose 
				try {
					Object pr_var;

					pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
					if( pr_var != null ) {
						if( pr_var instanceof String ) {
							if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
								parallel = true;
							}
						} else if( pr_var instanceof Boolean ) {
							if( ((Boolean) pr_var).booleanValue() ) {
								parallel = true;
							}
						}
					}
				} catch( VariablesException e ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				}

				// Devicemanager
				devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();

				if( getArg( "TAG" ) != null )
					tag = getArg( "TAG" );

				if( parallel ) // Ediabas holen
				{
					try {
						ediabas = devMan.getEdiabasParallel( tag, sgbd );
					} catch( Exception ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Wrong Cascade configuration for parallel diagnosys", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();
					} catch( Throwable ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();
					}
				} else {
					try {
						ediabas = devMan.getEdiabas( tag );
					} catch( Exception ex ) {
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();

					}
				}
				// <<<<< Paralleldiagnose            

				// EDIABAS Job ausf�hren
				temp = ediabas.executeDiagJob( sgbd, job, "", "" );
				if( temp.equals( "OKAY" ) == false ) { // Es sind Fehler bei der Ausf�hrung aufgetreten
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} else {
					result = new Ergebnis( "Status", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				}

				//Alle Fehlerspeicherinfos speichern mit Status Fehler (F)
				//System.out.println("Ediabas-Results: Hole alle FS-Eintr�ge");
				ergS�tze = ediabas.getDiagJobSaetze(); // Anzahl der gefunden Fehler
				tempArray = splitArg( jobResults );
				String[][] errors = new String[ergS�tze - 2][tempArray.length + 1];

				// Aufsplittung der Ergebnisse  in ein Array
				for( i = 0; i < ergS�tze - 2; i++ ) {
					for( j = 0; j < tempArray.length; j++ ) {
						errors[i][j] = ediabas.getDiagResultValue( i + 1, tempArray[j] );
					}
					errors[i][tempArray.length] = Ergebnis.FT_NIO;
				}

				if( ergS�tze > 2 ) {
					// Grunds�tzlich auszublendende Fehler ausblenden
					//System.out.println("Ediabas-Results: Analysiere IGNORE");

					if( ignore != null ) {
						for( i = 0; i < ergS�tze - 2; i++ ) {
							if( ignore.indexOf( ";" + errors[i][0] + ";" ) >= 0 ) {
								errors[i][tempArray.length] = Ergebnis.FT_IGNORE;
							}
						}
					}

					boolean weitereAnalyse = false;
					// Sind noch Datens�tze vorhanden, die nicht ausgblendet werden, wird die weitere Analyse enabled
					for( i = 0; i < ergS�tze - 2; i++ ) {
						if( errors[i][tempArray.length].equals( Ergebnis.FT_NIO ) == true )
							weitereAnalyse = true;
					}

					// Auswertung der Erg-S�tze nach FCodes

					//result = new Ergebnis( results[i-1], "EDIABAS", sgbd, job, jobpar, results[i-1], temp, temp1, temp2, "0", "", "", awt, PB.getString( "toleranzFehler3" ), hwtDummy, Ergebnis.FT_NIO );

					// Alle Ergebniss�tze nach 
					for( i = 0; i < ergS�tze - 2; i++ ) {

						String fHexCode = null;
						try {
							fHexCode = ediabas.getDiagResultValue( i + 1, "F_HEX_CODE" );
						} catch( Exception ernfe ) {
							// Nicht alle Steuerger�te unterst�tzen diese Zusatzinformation, also Fehler ignorieren
							fHexCode = null;
						}
						//System.out.println("Aktueller ErgSatz " + String.valueOf(i));
						// Mit der errors[i][0] muss das Array der Fehlernummern durchsucht werden
						for( j = 0; j < FCodeAnzahl; j++ ) {
							//System.out.println("Aktueller Fehler " + String.valueOf(errors[i][0]));   
							//System.out.println("Aktueller Fehlerort " + String.valueOf(errors[i][1]));   
							//System.out.println("Wat dat " + String.valueOf(errors[i][tempArray.length]));   
							//System.out.println("Aktueller FCODE " + fCodeList.get( j ).get( 1 ).toString());
							//System.out.println("Aktueller HWT " + htArgs.get( "HWT" +  fCodeList.get( j ).get( 0 ).toString().substring( 5 )).toString()); 
							if( errors[i][0].equals( fCodeList.get( j ).get( 1 ).toString() ) ) {
								// falls der Code beinhaltet ist, wird einer neuer Eintrag generiert

								//ist ein zugeh�riger HWT zum FCODE vorhanden?
								String singleHWT = "";
								if( allArgs.get( "HWT" + fCodeList.get( j ).get( 0 ).toString().substring( 5 ) ) != null ) {
									singleHWT = allArgs.get( "HWT" + fCodeList.get( j ).get( 0 ).toString().substring( 5 ) ).toString();
								}

								// Existiert Zusatzinformation �ber einen Hex-Fehlercode?
								if( fHexCode != null )
									result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", errors[i][0] + " (HEX " + fHexCode + ")", "", "", "0", "", "", "", errors[i][1], singleHWT, errors[i][tempArray.length] );
								else
									result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", errors[i][0], "", "", "0", "", "", "", errors[i][1], singleHWT, errors[i][tempArray.length] );

								ergListe.add( result );
								status = STATUS_EXECUTION_ERROR; // Job ist fehlgeschlagen
							}
						}

					}

					// Paralleldiagnose
					if( parallel ) {
						try {
							devMan.releaseEdiabasParallel( ediabas, tag, sgbd );
							alreadyReleased = true;
						} catch( Exception ex ) {
							result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						} catch( Throwable t ) {
							result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					}

					// Wenn Fcodes eingetragen sind, dann wird der normale FS_Lesen_Meachanismus deaktiviert
					if( FCodeAnzahl > 0 ) {
						weitereAnalyse = false;
						ergS�tze = 0;
					}

					//Andere PPs, hier nur die ohne Diagnose
					if( (analyse != null) && (weitereAnalyse == true) ) {
						i = 0;
						while( (weitereAnalyse == true) && (i < splitArg( analyse ).length) ) {
							//System.out.println("Checke Pruefprozedur (FS) "+splitArg(analyse)[i]);
							p = getPr�fling().getPr�fprozedur( splitArg( analyse )[i] );
							//System.out.println("Pruefprozedur-Attrib FORT="+p.getArg("FORT"));
							//Nur die, wo keine neue Diagnose notwendig ist
							//System.out.println("Pruefprozedur "+p);
							if( (p.getArg( "SGBD" ) == null) && (p.getArg( "JOB" ) == null) ) {
								//System.out.println("Pruefprozedur-Attrib FORT="+p.getArg("FORT"));
								ignore = extractErrorNumbers( p.getArg( "FORT" ) );
								//System.out.println("Pruefprozedur "+splitArg(analyse)[i]+" geladen: F-Orte="+ignore);
								for( j = 0; j < ergS�tze - 2; j++ ) {
									//Ueber alle noch fehlerhaften FS-Eintr�ge
									//System.out.println("Analyse des "+j+"-ten Fehlers noetig?");
									if( (ignore.indexOf( ";" + errors[j][0] + ";" ) >= 0) && (errors[j][tempArray.length].equals( Ergebnis.FT_NIO ) == true) ) {
										//Gefunden, Args umbiegen (Resultnamen durch Werte ersetzen) und executen
										//System.out.println("Analyse des "+j+"-ten Fehlers ist noetig");
										//
										//Hier sollte eigentlich geclont werden, geht zur Zeit jedoch nicht.
										//Somit alle Results holen, zwischenspeichern und sp�ter zur�ckschreiben;
										//pc = (Pruefprozedur) p.clone();
										String argStringOrig = "";
										k = 1;
										while( p.getArg( "RESULT" + k ) != null ) {
											temp = p.getArg( "RESULT" + k );
											//System.out.print("RESULT"+k+" von "+temp);
											argStringOrig = argStringOrig + ", RESULT" + k + "=" + temp;
											l = 0;
											while( l < tempArray.length ) {
												if( tempArray[l].equalsIgnoreCase( temp ) == true ) {
													//System.out.print(" auf "+errors[j][l]+" geaendert:");
													p.setArgs( "RESULT" + k + "=" + errors[j][l] );
													//System.out.println(" DONE");
													l = tempArray.length;
												}
												l++;
											}
											k++;
										}
										p.setPr�flingLaufzeitUmgebung( getPr�flingLaufzeitUmgebung() ); // Crichton
										p.execute( info );
										if( p.getExecStatus() == STATUS_EXECUTION_OK )
											errors[j][tempArray.length] = Ergebnis.FT_IGNORE;
										p.setArgs( argStringOrig.substring( 1 ) );

									}
								} //Ende FOR
								weitereAnalyse = false;
								for( j = 0; j < ergS�tze - 2; j++ ) {
									if( errors[j][tempArray.length].equals( Ergebnis.FT_NIO ) == true )
										weitereAnalyse = true;
								}
							}
							i++;
						} //Ende while
					}

					//Andere PPs, hier nur die mit Diagnose
					if( (analyse != null) && (weitereAnalyse == true) ) {
						i = 0;
						while( (weitereAnalyse == true) && (i < splitArg( analyse ).length) ) {
							//System.out.println("Checke Pruefprozedur (DIAG) "+splitArg(analyse)[i]);
							p = getPr�fling().getPr�fprozedur( splitArg( analyse )[i] );
							//Nur die, wo Diagnose gemacht wird
							if( (p.getArg( "SGBD" ) != null) && (p.getArg( "JOB" ) != null) ) {
								ignore = extractErrorNumbers( p.getArg( "FORT" ) );
								boolean codeExistiert = false;
								for( j = 0; j < ergS�tze - 2; j++ ) {
									if( (ignore.indexOf( ";" + errors[j][0] + ";" ) >= 0) && (errors[j][tempArray.length].equals( Ergebnis.FT_NIO ) == true) )
										codeExistiert = true;
								}
								if( codeExistiert == true ) {
									//System.out.println("Pruefprozedur "+splitArg(analyse)[i]+" wird ausgef�hrt: F-Orte="+ignore);
									p.setPr�flingLaufzeitUmgebung( getPr�flingLaufzeitUmgebung() ); // Crichton
									p.execute( info );
									if( p.getExecStatus() == STATUS_EXECUTION_OK ) {
										for( j = 0; j < ergS�tze - 2; j++ ) {
											if( ignore.indexOf( ";" + errors[j][0] + ";" ) >= 0 )
												errors[j][tempArray.length] = Ergebnis.FT_IGNORE;
										}
										weitereAnalyse = false;
										for( j = 0; j < ergS�tze - 2; j++ ) {
											if( errors[j][tempArray.length].equals( Ergebnis.FT_NIO ) == true )
												weitereAnalyse = true;
										}
									}
								}
							}
							i++;
						} //Ende while
					}

					//Doku und Status
					for( i = 0; i < ergS�tze - 2; i++ ) {
						hwt = "";

						//Fehlerort und Text als Ergebnis speichern (kann aber sein dass es FT_IGNORE ist)
						result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", errors[i][0], "", "", "0", "", "", "", errors[i][1], hwt, errors[i][tempArray.length] );
						ergListe.add( result );
						//beim FT_NIO  Pruefprozedur status auf NIO setzen und ggf. Hex Code als Ergebnis speichern
						if( errors[i][tempArray.length].equals( Ergebnis.FT_NIO ) == true ) {
							status = STATUS_EXECUTION_ERROR;
							if( hexCode == true ) {
								result = new Ergebnis( "FORT " + errors[i][0], "EDIABAS", sgbd, job, "", "F_HEX_CODE", errors[i][2], "", "", "0", "", "", "", "", "", errors[i][tempArray.length] );
								ergListe.add( result );
							}
						}
					}

					if( (status == STATUS_EXECUTION_OK) && (clear != null) ) {
						p = getPr�fling().getPr�fprozedur( clear );
						p.setPr�flingLaufzeitUmgebung( getPr�flingLaufzeitUmgebung() ); // Crichton
						p.execute( info );
						status = p.getExecStatus(); // Crichton
					}

				} // Ende if (ergS�tze > 2)

			} catch( ApiCallFailedException e ) {
				e.printStackTrace();
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//* eingef�gt analog zu DiagLesenFSUDS
			// Ggf. weitere Analyse f�r Fehler die (noch) nicht ausgeblendet werden k�nnen
			if( DEBUG )
				System.out.println( "Ggf. weitere Analyse f�r Fehler die (noch) nicht ausgeblendet werden k�nnen" );
			for( i = 0; i < analysePruefprozeduren.length; i++ ) {
				boolean executeAnalyse = false;
				String[] tempErrors = extractErrorNumbers( analysePruefprozeduren[i].getArg( "FORT" ) ).split( ";" );

				// Pr�fen ob Analysepr�fprozedur durchgef�hrt werden muss
				for( j = 0; j < tempErrors.length; j++ ) {
					if( DEBUG )
						writeDebug( "analyse PP " + analysePruefprozeduren[i].getName() + ", error " + tempErrors[j] );
					if( temporaryResults.containsKey( tempErrors[j] ) ) {
						result = (Ergebnis) temporaryResults.get( tempErrors[j] );
						if( result.getFehlerTyp() == Ergebnis.FT_NIO ) {
							if( DEBUG )
								writeDebug( "analyse PP " + analysePruefprozeduren[i].getName() + ", error " + tempErrors[j] + " match" );
							analysePruefprozeduren[i].setAttribut( "ERROR_SGBD", sgbd );
							analysePruefprozeduren[i].setAttribut( "ERROR_JOB", job );
							temporaryResults.remove( tempErrors[j] );
							executeAnalyse = true;
						}
					}
				}
				//Einzelne Analysepr�fprozedur(en) durchf�hren");
				if( executeAnalyse ) {
					if( DEBUG )
						writeDebug( "analyse PP " + analysePruefprozeduren[i].getName() + ", executing" );
					// Pr�fprozedur durchf�hren
					analysePruefprozeduren[i].setPr�flingLaufzeitUmgebung( getPr�flingLaufzeitUmgebung() );
					analysePruefprozeduren[i].execute( info );
					// Pr�fprozedur Ergebnis dokumentieren
					if( analysePruefprozeduren[i].getExecStatus() != STATUS_EXECUTION_OK ) {
						if( DEBUG )
							writeDebug( "analyse PP " + analysePruefprozeduren[i].getName() + ", niO" );
						status = STATUS_EXECUTION_ERROR;
						result = new Ergebnis( "FS", getPr�fling().getName() + "." + analysePruefprozeduren[i].getName(), "", "", "", "Status", PB.getString( "ausfuehrungsstatusNIO" ), "", "", "", "", "", "", "", "", Ergebnis.FT_NIO );
						ergListe.add( result );
					} else {
						if( DEBUG )
							writeDebug( "analyse PP " + analysePruefprozeduren[i].getName() + ", iO" );
						result = new Ergebnis( "FS", getPr�fling().getName() + "." + analysePruefprozeduren[i].getName(), "", "", "", "Status", "", "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
				}
			}
			// Nach der Analyse verbleibende Fehler zum Fehlerliste hinzuf�gen (und Status setzen)
			Enumeration<Ergebnis> myEnum = temporaryResults.elements();
			while( myEnum.hasMoreElements() ) {
				result = (Ergebnis) myEnum.nextElement();
				if( result.getFehlerTyp() == Ergebnis.FT_NIO ) {
					status = STATUS_EXECUTION_ERROR;
				}
				ergListe.add( result );
			}
			// Nach der Analyse ist ggf. der Fehlerspeicher zu l�schen
			if( errorPresent && (status == STATUS_EXECUTION_OK) && (clear != null) ) {
				clearPruefprozedur = getPr�fling().getPr�fprozedur( clear );
				clearPruefprozedur.setPr�flingLaufzeitUmgebung( getPr�flingLaufzeitUmgebung() );
				clearPruefprozedur.execute( info );
				status = clearPruefprozedur.getExecStatus();
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Paralleldiagnose
		if( parallel && alreadyReleased == false ) {
			try {
				devMan.releaseEdiabasParallel( ediabas, tag, sgbd );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable t ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		setPPStatus( info, status, ergListe );

	}

	//* eingef�gt analog zu DiagLesenFSUDS
	private void writeDebug( String info ) {
		System.out.println( "DiagLeseFS: " + getPr�fling().getName() + "." + getName() + " " + info );
	}

	/**
	 * Hilfsmethode f�r Argument IGNORE: L�st den �bergebenen String auf
	 * @param in der Argument IGNORE
	 * @return vollst�ndige String (zB "1-3" wird als "1;2;3" zur�ckgeliefert)
	 */
	private String extractErrorNumbers( String in ) {
		String sub, subSub;
		String out;
		int[] posArray;
		int counter = 1;
		int min, max;
		int i, j;

		for( i = 0; i < in.length(); i++ ) {
			if( in.charAt( i ) == ';' )
				counter++;
		}

		posArray = new int[counter + 1];
		j = 0;
		posArray[j++] = -1;
		for( i = 0; i < in.length(); i++ ) {
			if( in.charAt( i ) == ';' )
				posArray[j++] = i;
		}
		posArray[j++] = in.length();

		out = ";";
		for( i = 0; i < counter; i++ ) {
			sub = (in.substring( posArray[i] + 1, posArray[i + 1] )).trim();
			if( sub.compareTo( "" ) != 0 ) {
				j = sub.indexOf( "-" );
				if( (j < 1) || (j == (sub.length() - 1)) ) {
					out = out + sub + ";";
				} else {
					subSub = sub.substring( 0, j );
					min = Integer.parseInt( subSub );
					subSub = sub.substring( j + 1, sub.length() );
					max = Integer.parseInt( subSub );
					for( j = min; j <= max; j++ ) {
						out = out + j + ";";
					}
				}
			}
		}
		return out;
	}
}

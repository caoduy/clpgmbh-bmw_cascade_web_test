/*
 * DiagSetConfig_3_0_F_Pruefprozedur.java
 *
 * Created on 16.09.03
 */
package com.bmw.cascade.pruefprozeduren;


import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.visualisierung.*;
import com.bmw.cascade.CascadeProperties;

import com.bmw.cascade.pruefstand.ediabas.*;


/**
 * Pr�fprozedur, welche die Ediabas-Konfiguration �ndert.
 *
 * Zwingende Argumente:
 *
 * NAME: Name der zu �ndernden Konfigurations-Einstellung.
 * VALUE: Wert der zu �ndernden Konfigurations-Einstellung.
 *
 * Optionale Argumente: keine
 *
 * @author Peter Rettig, TI-431<BR>
 * @version 0_0_1_FA PR 24.10.2003 Initiale Implementierung<BR>
 * @version 0_0_2_TA SB 04.08.2004 Fuer Uebergabeparameter auch den @-Operator erlauben + Konfigurationselement im virt. Fzg. speichern - TA-Version<BR>
 * @version 0_0_3_FA SB 08.11.2004 FA-Freigabe nach Test<BR>
 */
public class DiagSetConfig_3_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    	static final long serialVersionUID = 1L;
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public DiagSetConfig_3_0_F_Pruefprozedur() {}

  /**
     * Konstruktor.
     *
     * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public DiagSetConfig_3_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super(pruefling, pruefprozName, hasToBeExecuted);
        attributeInit();
    }
    
    
    /**
     * Initialsiert die Default-Argumente.
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    
    /**
     * Liefert die optionalen Argumente.
     *
     * @return String-Array mit den optionalen Parametern.
     */
    public String[] getOptionalArgs() {
        String[] args = {};
        return args;
    }
    
    
    /**
     * Liefert die zwingend erforderlichen Argumente.
     *
     * @return String-Array mit den zwingenden Parametern.
     */
    public String[] getRequiredArgs() {
        String[] args = {"NAME", "VALUE"};
        return args;
    }
    
    
    /**
     * Pr�ft die Argumente auf Existenz und Wert.
     *
     * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt
     *         hat, ansonsten <code>true</code>.
     */
    public boolean checkArgs() {
        // Obligatorisch.
        if (!super.checkArgs()) return false;
        
        // �berpr�fe die Argumente. Hinweis: VALUE wird unber�cksichtigt gelassen.
        String name=getArg("NAME");
        if (name==null || name.length()==0)
            return false;
        
        return true;
    }
    
    
    /**
     * F�hrt die Pr�fprozedur aus.
     *
     * @param info Information zur Ausf�hrung.
     */
    public void execute( ExecutionInfo info ) {
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        //eigene Variablen
        String name = null;
        String value = null;
        
        try {
            
            //Parameter holen und Argumente pruefen
            try {
                // hat der allgemeine Check fehlgeschlagen???
                if( checkArgs() == false ) throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
                // hole den Namen
                if( getArg( getRequiredArgs()[0] ).indexOf('@') != -1 )
                    name = getPPResult( getArg( getRequiredArgs()[0] ) );
                else
                    name = getArg( getRequiredArgs()[0] );
                // hole den Wert
                if( getArg( getRequiredArgs()[1] ).indexOf('@') != -1 )
                    value = getPPResult( getArg( getRequiredArgs()[1] ) );
                else
                    value = getArg( getRequiredArgs()[1] );
                
            } catch (PPExecutionException e) {
                if (e.getMessage() != null)
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            // �ndere die Ediabas-Konfiguration.
            try {
                Ediabas.apiSetConfig(name, value);
                result = new Ergebnis( "ELEMENT", "SET_CONFIG", "ELEMENT", name, "", "VALUE", value, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                ergListe.add(result);
            } catch (ApiCallFailedException e) {
                result = new Ergebnis( "Ediabas-Fehler", "", "", "", "", "", "", "", "", "", "", "", "", e.toString(), "" , Ergebnis.FT_NIO );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO );
            e.printStackTrace();
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
        setPPStatus(info, status, ergListe);
    }
    
}


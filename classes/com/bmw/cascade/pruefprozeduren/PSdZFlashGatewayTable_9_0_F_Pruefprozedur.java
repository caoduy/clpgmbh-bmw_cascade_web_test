package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import java.util.Map.Entry;

import com.bmw.appframework.logging.LogLevel;
import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.psdz.PSdZ;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeConfigError;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeExecutionStatus;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeFilteredTACategory;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTA;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTACategories;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTAL;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTALExecutionStatus;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTALFilter;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTALLine;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTALTACategory;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.*;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.util.logging.CascadeLogging;

/**
 * Implementierung einer Pr�fprozedur, die das Flashen der Gatewaytabelle durchf�hrt (Hintergrund: Variantenreduktion BDC SG)
 * 
 * @author Thomas Buboltz, BMW AG; Martin Gampl, BMW AG <BR>
 *         24.05.2011 TB Ersterstellung <BR>
 *         04.09.2013 MG Fehler, die bei der TAL-Abarbeitung entstehen werden nun aus TAL extrahiert und weiterverarbeitet (Fehlerprotokoll/APDM) <BR>
 *         04.09.2013 MG Fehler in der Sollverbauung, die aufgrund von KIS-Fehlern entstehen und bei weicher KIS-Pr�fung nicht zum Abbruch des Ablaufes f�hren, z.B. KIS Doppeltreffer f�hren zum PP-Status NIO 11.09.2013 MG F-Version <BR>
 *         01.02.2014 TB LOP 1693: Titel: Einbringen der Gateway-Tabelle in Nacharbeitsfall parametrierbar machen, deprecated Methoden [TALExecutionStati m�ssen noch drin bleiben, weil die �nderung im Kern erst ausgerollt wird] und Generics. T-Version <BR>
 *         03.02.2014 TB F-Version (5_0) <BR>
 *         25.01.2016 MG T-Version (6_0), Mindestversionsanforderung CASCADE 7.1.0 / PSdZ 5.01.00 <BR>
 *         ---------- -- - Ausbau der deprecated Methoden/deprecated TAL-Status; Mindestversionsanforderung CASCADE 6.1.0 / PSdZ 4.9.1<BR>
 *         ---------- -- - Debugausgaben im Fehlerfall verbessert <BR>
 *         ---------- -- - Nutzung der neuen Methode generateAndStoreTAL(String[], boolean, CascadeTALFilter, boolean, boolean); Mindestversionsanforderung CASCADE 7.1.0 / PSdZ 5.01.00 <BR>
 *         ---------- -- - LOP 1853: Ausgabe (in Log-Datei Pr�fstandScreen) der Pr�fprozedurversion sowie s�mtlicher optionaler und nicht optionaler Argumente, die an die Pr�fprozedur �bergeben werden zum Zweck der Nachvollziehbarkeit im Fehlerfall. <BR>
 *         ---------- -- - LOP 1899: Einheitliche Darstellung der Fehlerausgaben in PSdZ-Pr�fprozeduren <BR>
 *         04.02.2016 MG F-Version (7_0), Mindestversionsanforderung CASCADE 7.1.0 / PSdZ 5.01.00 <BR>
 *         09.08.2016 MG T-Version (8_T) <BR>
 *         ---------- -- - LOP 2107: Im Fall einer fehlerhaften TAL wird allg. NIO-TAL-Gesamtstatus in Ergebnisliste nur noch dann ausgegeben, wenn keine weiteren allg. oder SG spezifischen TAL-Fehler erfasst wurden <BR>
 *         19.09.2016 MG F-Version (9_F), Mindestversionsanforderung CASCADE 7.1.0 / PSdZ 5.01.00 <BR>
 */
public class PSdZFlashGatewayTable_9_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * PSdZ als globale Variable
	 */
	private PSdZ psdz = null;

	/**
	 * DE fuer Sprachermittlung
	 */
	private final boolean DE = checkDE(); // Systemsprache DE wenn true

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSdZFlashGatewayTable_9_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSdZFlashGatewayTable_9_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG", "DEBUG_PERFORM", "FORCE_EXECUTION" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = { "ECU_ADDRESS_HEX" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		return super.checkArgs();
	}

	/**
	 * Tr�gt ECUs in die Ergebnisliste (APDM) ein
	 * 
	 * @param mErgListe Ergebnisliste
	 * @param mMap Map mit einzutragenden ECU's
	 * @param mTransact einzutragende Transaktion
	 * @param mResult einzutragendes Ergebnis
	 * @param mPsdzErrors zugeh�rige einzutragende Fehler
	 * @param mECUStatistics Map mit SG Antwortverhalten
	 * @return failureLoggedToErgList gibt an, ob ein oder mehrere Fehler in Ergebnisliste eingetragen wurden
	 * @throws CascadePSdZRuntimeException
	 */
	private boolean entryECUsDetails( Vector<Ergebnis> mErgListe, TreeMap<String, HashSet<String>> mMap, String mTransact, String mSResult, HashMap<String, List<Exception>> mPsdzErrors, HashMap<String, String> mECUStatistics ) throws CascadePSdZRuntimeException, CascadePSdZConditionsNotCorrectException {
		boolean failureLoggedToErgList = false;
		Ergebnis mResult;
		String key, fehlertext, antwortVerhalten;
		CascadePSdZRuntimeException cscRE;
		Exception ex;
		String action = DE ? "Aktion: " : "Action: ";
		String errorDescription = DE ? "Fehlerbeschreibung: " : "Error description: ";
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";
		String titleBestellTeilTAIS = DE ? "BestellNr: " : "OrderPartNo: "; // TAIS Nr. Bestellteil

		// Schleife �ber alle Eintr�ge der �bergebenen Map
		for( Iterator<Entry<String, HashSet<String>>> e = mMap.entrySet().iterator(); e.hasNext(); ) {
			Map.Entry<String, HashSet<String>> entry = e.next(); // z.B. SG:JBBF 0x00=[cdDeploy]
			cscRE = null;
			ex = null;

			String tempKey = entry.getKey();

			// etwas aufw�ndigerer Code (anstatt einfacher Suche nach ":"), stellt aber sicher, dass Basisvariante sicher ermittelt wird, auch dann wenn "psdz.SG" zuk�nftig ":" enthalten sollte(!)
			String sgBaseVariantName = tempKey.substring( tempKey.indexOf( PB.getString( "psdz.SG" ) ) + PB.getString( "psdz.SG" ).length() + 1, tempKey.indexOf( "0x" ) - 1 ); // z.B. "BDC_GW2"
			String sgDiagAddress = tempKey.substring( tempKey.indexOf( "0x" ) ); // z.B. "0x10"

			HashSet<String> myContent = new HashSet<String>();
			myContent = ((HashSet<String>) entry.getValue()); // -> [gwtbDeploy]

			Iterator<String> i = myContent.iterator();
			StringBuffer text = new StringBuffer();
			boolean hasNext = i.hasNext();
			Object o = null;

			while( hasNext ) {
				o = i.next();
				text.append( PB.getString( "psdz.tal." + String.valueOf( o ).toLowerCase().trim() ) );
				hasNext = i.hasNext();
				if( hasNext )
					text.append( ", " );
			}

			if( !mSResult.equalsIgnoreCase( Ergebnis.FT_IO ) ) {
				for( Iterator<String> j = myContent.iterator(); j.hasNext(); ) {
					String sgCat = j.next().toLowerCase().trim(); // -> "gwtbDeploy"
					List<Exception> psdZErrorsExceptionList = mPsdzErrors.get( sgDiagAddress + "_" + sgCat );

					// extrahiere alle Exceptions (falls vorhanden) aus dem aktuell selektierten Key von mPsdzErrors (z.B. "0x10_gwtbdeploy")
					if( psdZErrorsExceptionList != null ) {
						for( int excep = 0; excep < psdZErrorsExceptionList.size(); excep++ ) {
							cscRE = null;
							ex = psdZErrorsExceptionList.get( excep );
							if( ex instanceof CascadePSdZRuntimeException ) {
								cscRE = (CascadePSdZRuntimeException) ex;
							}

							// den anderen Key (-> Diagnose-Adresse ohne 0x und ohne f�hrende Nullen) f�r die ECUStatistics holen (etwas aufwendig...)
							key = sgDiagAddress.charAt( 2 ) == '0' ? sgDiagAddress.substring( 3, 4 ) : sgDiagAddress.substring( 2, 4 );

							// der Fehlertext
							if( cscRE != null ) {
								fehlertext = action + cscRE.getStrErrorContextTranslated() + ", " + errorDescription + cscRE.getMessage() + ", (PSdZ ID: " + cscRE.getMessageId() + ")";
							} else {
								fehlertext = PB.getString( mTransact );
							}

							// SG Antwortverhalten nur eintragen, wenn SG nie geantwortet hat oder sporadisch
							if( mECUStatistics.get( key ).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.0" ) ) || mECUStatistics.get( key ).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.1" ) ) )
								antwortVerhalten = " (" + PB.getString( "psdz.SG" ) + " " + PB.getString( "psdz.antwortet" ) + " " + mECUStatistics.get( key ) + ")";
							else
								antwortVerhalten = "";

							// LOP 1436: Ausgabe um TAIS Teilenummer anreichern und zus�tzlich, wenn m�glich noch die TAIS Nummer des bestellbaren Teils mit ausgeben. Unterdr�cke die Bestellnummer, wenn diese gleich der Nummer des logTeils ist

							String logTeil = "";
							String bestellTeilTAIS = "";
							StringBuffer strBufNummernTAIS = new StringBuffer();

							if( psdz.getLogistischesTeil( key ) != null ) {
								logTeil = psdz.getLogistischesTeil( key ).getSachNrTAIS();
								strBufNummernTAIS.append( ", " + PB.getString( "psdz.text.TaisNummer" ) + logTeil );

								bestellTeilTAIS = psdz.getLogistischesTeil( key ).getSachNrBestellbaresTeilTAIS();
								strBufNummernTAIS.append( logTeil.equals( bestellTeilTAIS ) ? "" : " (" + titleBestellTeilTAIS + bestellTeilTAIS + ")" );
							}

							mResult = new Ergebnis( "PSdZFlashGatewayTable", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress + strBufNummernTAIS.toString(), text.toString(), "", "STATUS", statusNotOkay, "", "", "0", "", "", "", fehlertext + antwortVerhalten, "", mSResult );
							mErgListe.add( mResult );
							failureLoggedToErgList = true;
						}
					}
				}
			} else {
				mResult = new Ergebnis( "PSdZFlashGatewayTable", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, text.toString(), "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", mSResult );
				mErgListe.add( mResult );
			}
		}
		return failureLoggedToErgList;
	}

	/**
	 * Eintrag der allgemeinen Fehler, des NIO-ECU oder des NotExecutable-ECU ins APDM
	 * 
	 * @param gErgListe Ergebnisliste
	 * @param gMapNIO Map mit NIO-ECU's
	 * @param gMapNotExec Map mit NotExecutable-ECU's
	 * @param gPsdzErrors zugeh�rige einzutragende Fehler
	 * @param gECUStatistics Map mit SG Antwortverhalten
	 * @return failureLoggedToErgList gibt an, ob ein oder mehrere Fehler in Ergebnisliste eingetragen wurden
	 * @throws CascadePSdZRuntimeException
	 */
	private boolean entryECUsFT_NIO( Vector<Ergebnis> gErgListe, TreeMap<String, HashSet<String>> gMapNIO, TreeMap<String, HashSet<String>> gMapNotExec, HashMap<String, List<Exception>> gPsdzErrors, HashMap<String, String> gECUStatistics ) throws CascadePSdZRuntimeException, CascadePSdZConditionsNotCorrectException {
		List<Boolean> failureLoggedToErgList = new ArrayList<Boolean>();
		CascadePSdZRuntimeException cscRE;
		Ergebnis gResult;
		String action = DE ? "Aktion: " : "Action: ";
		String errorDescription = DE ? "Fehlerbeschreibung: " : "Error description: ";
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";

		// Haben wir irgendeinen allgemeinen Fehler ohne SG Adresse?
		if( gPsdzErrors.containsKey( "-1" ) ) {

			// Liste der Fehler durchgehen
			for( Exception ex : gPsdzErrors.get( "-1" ) ) {
				cscRE = null;
				if( ex instanceof CascadePSdZRuntimeException ) {
					cscRE = (CascadePSdZRuntimeException) ex;
				}

				gResult = new Ergebnis( "PSdZFlashGatewayTable", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", cscRE != null ? action + cscRE.getStrErrorContextTranslated() + ", " + errorDescription + cscRE.getMessage() + ", (PSdZ ID: " + cscRE.getMessageId() + ")" : ex.getMessage(), "", Ergebnis.FT_NIO );
				gErgListe.add( gResult );
				failureLoggedToErgList.add( Boolean.TRUE );
			}
		}

		// NIO ECUs in Ergebnisliste (APDM) eintragen
		failureLoggedToErgList.add( entryECUsDetails( gErgListe, gMapNIO, "psdz.tal.transactionNIO", Ergebnis.FT_NIO, gPsdzErrors, gECUStatistics ) );

		// NIO ECUs NotExecutable in Ergebnisliste (APDM) eintragen
		failureLoggedToErgList.add( entryECUsDetails( gErgListe, gMapNotExec, "psdz.tal.transactionNOTExecutable", Ergebnis.FT_NIO, gPsdzErrors, gECUStatistics ) );

		return failureLoggedToErgList.contains( Boolean.TRUE );
	}

	/**
	 * Aufbau einer HashMap aus den Exceptions der TAL (Aufbau: key = Steuerger�teadresse in hex + _ + TACategorie z.B. '0x10_gwtbdeploy', wert = 'Exception'
	 * 
	 * @param mTALResult zu analysierende CascadeTAL
	 * @return HashMap aus input-CascadeTAL erzeugte HashMap
	 */
	private HashMap<String, List<Exception>> buildExcHashMap( CascadeTAL mTALResult ) {
		HashMap<String, List<Exception>> ret_val = new HashMap<String, List<Exception>>();

		// Exceptions in der TAL?
		if( mTALResult.getExceptions().size() > 0 ) {
			ret_val.put( "-1", mTALResult.getExceptions() );
		}

		CascadeTALLine[] myLines = mTALResult.getTalLines();
		for( int i = 0; i < myLines.length; i++ ) {
			String sgAddressTemp = "0" + Integer.toHexString( myLines[i].getDiagnosticAddress().intValue() ).toUpperCase(); // z.B. "010"
			String sgDiagAddress = "0x" + sgAddressTemp.substring( sgAddressTemp.length() - 2, sgAddressTemp.length() ); // z.B. "0x10"
			String sgCat = "_" + myLines[i].getTALTACategories()[0].getCategory().toString().toLowerCase().trim(); // z.B. "_gwtbdeploy"
			boolean foundException = false;

			// Exceptions in der TALLine?
			if( myLines[i].getExceptions().size() > 0 ) {
				ret_val.put( sgDiagAddress + sgCat, myLines[i].getExceptions() );
				foundException = true;
			}

			if( !foundException ) {
				for( int j = 0; j < myLines[i].getTALTACategories().length; j++ ) {
					CascadeTALTACategory myTALTACategory = myLines[i].getTALTACategories()[j];
					sgCat = "_" + myTALTACategory.getCategory().toString().toLowerCase().trim();
					// Pr�fung der TAs
					for( int k = 0; k < myTALTACategory.getTAs().length; k++ ) {
						CascadeTA myTA = myTALTACategory.getTAs()[k];
						// Exceptions in der Transaktion?
						if( !myTA.getExceptions().isEmpty() ) {
							ret_val.put( sgDiagAddress + sgCat, myTA.getExceptions() );
						}
					}
					// Pr�fung der FscTAs nicht notwendig, da bei Gatewaytabellen-Programmierung per TAL-Filter ausgefiltert					
				}
			}
		}
		return ret_val;
	}

	/**
	 * Aufbau von drei TreeMaps aus den Ergebnissen der TAL (Aufbau: key = 'SG:' + Basisvariantenname + ' ' + Steuerger�teadresse in hex, wert = TALCategorie z.B. 'SG:BDC_GW2 0x10', wert = 'gwtbDeploy'
	 * 
	 * @param mTALResult zu analysierende CascadeTAL
	 * @param mEcuIO aus input-CascadeTAL erzeugte TreeMap mit CascadeExcecutionStatus = PROCESSED
	 * @param mEcuNIO aus input-CascadeTAL erzeugte TreeMap mit CascadeExcecutionStatus = PROCESSEDWITHERROR
	 * @param mEcuNotExecutable aus input-CascadeTAL erzeugte TreeMap mit CascadeExcecutionStatus = NOTEXECUTABLE
	 */
	private void buildMaps( CascadeTAL mTALResult, TreeMap<String, HashSet<String>> mEcuIO, TreeMap<String, HashSet<String>> mEcuNIO, TreeMap<String, HashSet<String>> mEcuNotExecutable ) {
		// Leeren der Maps
		mEcuIO.clear();
		mEcuNIO.clear();
		mEcuNotExecutable.clear();

		// Ergebnisanalyse

		for( int i = 0; i < mTALResult.getTalLines().length; i++ ) {
			// BasisName des SG ermitteln ("ECU:'Basisname'" + " 0x'hexcode'")
			String sgAddressTemp = "0" + Integer.toHexString( mTALResult.getTalLines()[i].getDiagnosticAddress().intValue() ).toUpperCase(); // z.B. "010"
			String sgIDName = PB.getString( "psdz.SG" ) + ":" + mTALResult.getTalLines()[i].getBaseVariantName() + " " + "0x" + sgAddressTemp.substring( sgAddressTemp.length() - 2, sgAddressTemp.length() ); // z.B. "SG:BDC_GW2 0x10"

			// Status der einzelnen TAs holen und merken in IO-, NIO- und NotExecutable-Listen
			for( int j = 0; j < mTALResult.getTalLines()[i].getTALTACategories().length; j++ ) {
				// IO TAL-Lines analysieren und TalLine-Kategorien merken
				// Es werden der TAL-Line Status analysiert und nicht der Status der TALTACategories, damit Fehler, die nach der Transaktion auftreten (-> FINALIZATION) auch erfasst werden 
				if( mTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.PROCESSED ) || mTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.INACTIVE ) ) {
					HashSet<String> setTalTaCategoriesIO = new HashSet<String>();
					// pr�fe ob schon eingetragen
					if( mEcuIO.containsKey( sgIDName ) )
						setTalTaCategoriesIO = mEcuIO.get( sgIDName );

					setTalTaCategoriesIO.add( mTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString() );
					mEcuIO.put( sgIDName, setTalTaCategoriesIO );
				}

				// NIO TAL-Lines analysieren und TalLine-Kategorien merken (-> gwtbDeploy)
				if( mTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.PROCESSEDWITHERROR ) ) {
					HashSet<String> setTalTaCategoriesNIO = new HashSet<String>();
					// pr�fe ob schon eingetragen
					if( mEcuNIO.containsKey( sgIDName ) )
						setTalTaCategoriesNIO = mEcuNIO.get( sgIDName );

					setTalTaCategoriesNIO.add( mTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString() );
					mEcuNIO.put( sgIDName, setTalTaCategoriesNIO );
				}

				// NotExcecutable (auch NIO !)
				if( mTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.NOTEXECUTABLE ) ) {
					HashSet<String> setTalTaCategoriesNotExecutable = new HashSet<String>();
					// pr�fe ob schon eingetragen
					if( mEcuNotExecutable.containsKey( sgIDName ) )
						setTalTaCategoriesNotExecutable = mEcuNotExecutable.get( sgIDName );

					setTalTaCategoriesNotExecutable.add( mTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString() );
					// Key von mEcuNotExecutable z.B. "SG:BDC_GW2 0x10", value z.B. "gwtbDeploy" 
					mEcuNotExecutable.put( sgIDName, setTalTaCategoriesNotExecutable );
				}
			}
		}
	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {
		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
		boolean bDebugPerform = true; // gibt an, ob eine Performencemessung erfolgen sollen
		boolean bFailureLoggedToErgList = false; // gib an, ob ein Fehler in Ergebnisliste eingetragen wurde
		boolean bForceExecution = true; // erzwinge die Ausf�hrung - deshalb hier per default auf true, d.h. bei True wird das Schreiben der Gateway-Tabelle erzwungen, bei False (IST- gleich SOLL-Stand, z.B. in der Nacharbeit) wird die Gateway Tabelle nicht ins Fahrzeug eingebracht
		boolean bParallelExecution = true; // Voreinstellung: parallele Ausf�hrung erlauben
		final boolean DE = checkDE(); // Systemsprache DE wenn true
		final int I_TAL_POLLTIME = 1000; // alle I_TAL_POLLTIME in ms TAL Status �berpr�fen
		int iMaxErrorRepeat = 1; // Voreinstellung: maximal 1 Wdhlg im Fehlerfall
		int status = STATUS_EXECUTION_OK;

		Ergebnis result;
		String ecuAddressHex = null;
		String optionalArgumentName = null;
		String optionalArgumentValue = null;
		String requiredArgumentName = null;
		String requiredArgumentValue = null;
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";
		String testScreenID = this.getPr�flingLaufzeitUmgebung().getName(); // TestScreenID (aus Pr�fstandsinstanz)
		String[] optionalArgumentsNames = getOptionalArgs();
		String[] optionalArgumentsValues = null;
		String[] requiredArgumentsNames = getRequiredArgs();
		String[] requiredArgumentsValues = null;
		// Verwaltung der verarbeiteten SG [IO, NIO, NOTEXECUTABLE] in sortierten Listen mit Variable=ECU und Wert=TA, SWT Kategories als HashSet; Bsp.: Variable=0x29 ECU:DSC, Wert=cdDeploy, fscDeploy als HashSet
		TreeMap<String, HashSet<String>> mapEcuIO = new TreeMap<String, HashSet<String>>(), mapEcuNIO = new TreeMap<String, HashSet<String>>(), mapEcuNotExecutable = new TreeMap<String, HashSet<String>>();
		UserDialog userDialog = null;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();

		try {
			// Argumente loggen, einlesen und checken
			try {

				// Ausgabe der Pr�fprozedurversion sowie s�mtlicher an die Pr�fprozedur �bergebener Argumente (required/optional) f�r sp�tere Analysezwecke

				System.out.println( "PSdZ ID:" + testScreenID + ", Initialize " + this.getClassName() );

				for( int i = 0; i < requiredArgumentsNames.length; i++ ) {
					requiredArgumentName = requiredArgumentsNames[i];
					requiredArgumentValue = "";
					// gibt es nicht optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( requiredArgumentName ) != null ) {
						requiredArgumentsValues = extractValues( getArg( requiredArgumentName ) );
						for( int k = 0; k < requiredArgumentsValues.length; k++ ) {
							// Listen von nicht optionalen Argumenten werden durch ";" getrennt ausgegeben
							requiredArgumentValue = requiredArgumentValue + requiredArgumentsValues[k] + ((requiredArgumentsValues.length > 1 && k < (requiredArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						requiredArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZFlashGatewayTable with required argument: " + requiredArgumentName + " = " + requiredArgumentValue );
				}

				for( int i = 0; i < optionalArgumentsNames.length; i++ ) {
					optionalArgumentName = optionalArgumentsNames[i];
					optionalArgumentValue = "";
					// gibt es optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( optionalArgumentName ) != null ) {
						optionalArgumentsValues = extractValues( getArg( optionalArgumentName ) );
						for( int k = 0; k < optionalArgumentsValues.length; k++ ) {
							// Listen von optionalen Argumenten werden durch ";" getrennt ausgegeben
							optionalArgumentValue = optionalArgumentValue + optionalArgumentsValues[k] + ((optionalArgumentsValues.length > 1 && k < (optionalArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						optionalArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZFlashGatewayTable with optional argument: " + optionalArgumentName + " = " + optionalArgumentValue );
				}

				// Obligatorische Argumentpr�fung auf Vorhandensein von nicht optionalen sowie �berz�hligen Argumenten
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
					else
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DEBUG_PERFORM
				if( getArg( "DEBUG_PERFORM" ) != null ) {
					if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "TRUE" ) )
						bDebugPerform = true;
					else if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "FALSE" ) )
						bDebugPerform = false;
					else
						throw new PPExecutionException( "DEBUG_PERFORM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// ECU_ADDRESS_HEX
				ecuAddressHex = extractValues( getArg( "ECU_ADDRESS_HEX" ) )[0];

				// FORCE_EXECUTION
				if( getArg( "FORCE_EXECUTION" ) != null ) {
					if( getArg( "FORCE_EXECUTION" ).equalsIgnoreCase( "TRUE" ) )
						bForceExecution = true;
					else if( getArg( "FORCE_EXECUTION" ).equalsIgnoreCase( "FALSE" ) )
						bForceExecution = false;
					else
						throw new PPExecutionException( "FORCE_EXECUTION " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			// Performancemessung Start
			if( bDebugPerform )
				System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable PP START" );

			// Devices holen
			try {
				psdz = getPr�flingLaufzeitUmgebung().getDeviceManager().getPSdZ();
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			try {
				userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
				// Informiere den Benutzer
				userDialog.setDisplayProgress( true );
				userDialog.getProgressbar().setIndeterminate( true );
				String title = DE ? "PSdZ Flash der Gatewaytabelle" : "PSdZ flash gateway table";
				userDialog.displayMessage( title, ";" + PB.getString( "psdz.ud.Vorbereitunglaeuft" ), -1 );
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Flashe die Gateway Tabelle
			try {
				// ********************************************************************************
				// * 1. SVT IST holen                                                             *
				// ********************************************************************************
				String[] ecuSollList = { ecuAddressHex };

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.generateAndStoreSVTIst' START" );
				}

				// SVT ist berechnen
				psdz.generateAndStoreSVTIst( "_GWTB", ecuSollList, false );

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.generateAndStoreSVTIst' ENDE" );
				}

				// ********************************************************************************
				// * 2. SOLLVERBAUUNG berechnen                                                   *
				// ********************************************************************************
				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.generateAndStoreSollverbauungForDaWerk' START" );
				}

				// SOLLVERBAUUNG berechnen
				psdz.generateAndStoreSollverbauungForDaWerk( "_GWTB", ecuAddressHex );

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.generateAndStoreSollverbauungForDaWerk' ENDE" );
				}

				// ********************************************************************************
				// * 3. TAL Filter f�r Flash der Gateway Tabelle berechnen                        *
				// ********************************************************************************

				// TAL-Filter bef�llen, es m�ssen hier alle Kategorien in den Filter aufgenommen werden
				// Kategorien, die nicht aufgef�hrt sind, sollen verboten werden.
				CascadeTALFilter myTALFilter = new CascadeTALFilter();
				Object[] allCategories = CascadeTACategories.getAllCategories().toArray();

				// gehe alle Kategorien durch ...
				for( int i = 0; i < allCategories.length; i++ ) {
					if( bDebug )
						System.out.println( "PSdZ ID:" + testScreenID + ", PSdZFlashGatewayTable: Categories Loop Counter: " + i + ", Category: " + ((CascadeTACategories) allCategories[i]).toString() );

					CascadeFilteredTACategory myCategory = new CascadeFilteredTACategory( (CascadeTACategories) allCategories[i] );
					// Treffer in der Liste der Actions?
					boolean filteredActionFound = false;

					// Ist die Kategorie gwDeploy - Einbringen der GatewayTabelle?
					if( allCategories[i].toString().equalsIgnoreCase( CascadeTACategories.GWTB_DEPLOY.toString() ) ) {
						filteredActionFound = true;
						myCategory.setAffectAllEcus( false );

						myCategory.addEcusToList( ecuSollList );
						myCategory.setBlacklist( false );

						// ForceExecution?
						myCategory.setForceExecution( bForceExecution );
					}

					if( !filteredActionFound ) {
						myCategory.setAffectAllEcus( true );
						myCategory.setBlacklist( true );
					}

					myTALFilter.addFilteredTACategory( myCategory );
				}

				// ********************************************************************************
				// * 4. TAL aus Basis Filter f�r Flash der Gateway Tabelle berechnen              *
				// ********************************************************************************
				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.generateAndStoreTAL' START" );
				}

				// TAL berechnen
				psdz.generateAndStoreTAL( ecuSollList, false, myTALFilter, false, false );

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.generateAndStoreTAL' ENDE" );
				}

				// ********************************************************************************
				// * 5. TAL-Ausf�hrung: Flash der Gateway Tabelle                                 *
				// ********************************************************************************

				CascadeTAL myTALResult = null;

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.getTAL' START" );
				}

				// Jetzt die erzeugte, gefilterte TAL weiterverwenden
				myTALResult = psdz.getTAL();

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.getTAL' ENDE" );
				}

				// falls TAL nicht leer ist, wird neuer Nutzerdialog zum Flash der Gatewaytabelle angezeigt
				if( myTALResult.getTalLines().length > 0 ) {
					// UserDialog Reset
					userDialog.reset();

					// Info via UserDialog
					userDialog.setAllowCancel( true );
					userDialog.getProgressbar().setIndeterminate( true );
					String title = DE ? "PSdZ Flash der Gatewaytabelle" : "PSdZ flash gateway table";
					String message = DE ? "Flashe Gateway Tabelle in SG:" + ecuAddressHex : "Flash gateway table to ECU:" + ecuAddressHex;
					userDialog.displayMessage( title, ";" + message, -1 );
				}

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.startTALExecution' START" );
				}
				// asynchrone Ausf�hrung einer TAL starten
				psdz.startTALExecution( bParallelExecution, iMaxErrorRepeat, false );

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.startTALExecution' ENDE" );
				}

				// �berpr�fe pollend die Ausf�hrung der TAL
				while( true ) {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.getTALExecutionStatus' START" );
					}
					// lese den Status + hier wird f�r I_TAL_POLLTIME in ms gewartet
					myTALResult = psdz.getTALExecutionStatus( I_TAL_POLLTIME, null );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.getTALExecutionStatus' ENDE" );
					}

					// CHECK TAL EXECUTION STATUS
					// fertig IO?

					// CascadeTALExecutionStatus.FINISHED_WITH_WARNINGS, CascadeTALExecutionStatus.FINISHED_FOR_HW_TRANSACTIONS_WITH_WARNINGS und CascadeTALExecutionStatus.FINISHED_FOR_HW_TRANSACTIONS werden auf CascadeTALExecutionStatus.FINISHED durch CASCADE-Kern-SW gemappt
					if( myTALResult.getStatus().getName().equals( CascadeTALExecutionStatus.FINISHED.getName() ) ) {
						//Ergebnis TAL auf PLatte ablegen
						psdz.getTALExecutionStatus( 0, "_STATUS_GWTB_FLASH" );

						// Erzeugen der TreeMaps IO, NIO und NotExecutable
						buildMaps( myTALResult, mapEcuIO, mapEcuNIO, mapEcuNotExecutable );

						// IO - ECU in Ergebnisliste (APDM) eintragen
						entryECUsDetails( ergListe, mapEcuIO, "psdz.tal.transactionIO", Ergebnis.FT_IO, null, null );

						// IO Ergebnis in (APDM) eintragen
						result = new Ergebnis( "PSdZFlashGatewayTable", "PSdZ", "", "", "", DE ? "Ausf�hrungsstatus der TAL" : "TAL execution status", myTALResult.getStatus().getName(), myTALResult.getStatus().getName(), "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );

						break;
					}

					// fertig NIO?
					// CascadeTALExecutionStatus.FINISHED_WITH_ERROR, CascadeTALExecutionStatus.ABORTED_BY_ERROR, CascadeTALExecutionStatus.ABORTED_BY_USER und CascadeTALExecutionStatus.FINISHED_FOR_HW_TRANSACTIONS_WITH_ERROR werden auf CascadeTALExecutionStatus.FINISHED_WITH_ERROR_IN_TRANSACTION durch CASCADE-Kern-SW gemappt
					if( myTALResult.getStatus().getName().equals( CascadeTALExecutionStatus.FINISHED_WITH_ERROR_IN_TRANSACTION.getName() ) ) {

						//Ergebnis TAL auf PLatte ablegen
						psdz.getTALExecutionStatus( 0, "_STATUS_GWTB_FLASH" );

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.getEcuStatisticsHexString' START" );
						}
						// Erzeugen der TreeMaps IO, NIO und NotExecutable
						buildMaps( myTALResult, mapEcuIO, mapEcuNIO, mapEcuNotExecutable );
						// PSdZ hole die aufgelaufenen Fehler
						HashMap<String, List<Exception>> psdzErrors = new HashMap<String, List<Exception>>( buildExcHashMap( myTALResult ) );
						HashMap<String, String> psdzECUStatisticsHexStr = new HashMap<String, String>( psdz.getEcuStatisticsHexString() );
						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.getEcuStatisticsHexString' ENDE" );
						}

						// IO - ECU in Ergebnisliste (APDM) eintragen
						entryECUsDetails( ergListe, mapEcuIO, "psdz.tal.transactionIO", Ergebnis.FT_IO, null, null );

						// NIO - / NotExecutable - ECU in Ergebnisliste (APDM) eintragen
						bFailureLoggedToErgList = entryECUsFT_NIO( ergListe, mapEcuNIO, mapEcuNotExecutable, psdzErrors, psdzECUStatisticsHexStr );

						// NIO Ergebnis (allg.) in APDM eintragen, jedoch nur, wenn bisher noch kein anderer allgemeiner oder SG spezifischer Fehler in Ergebnisliste erfasst wurde, jedoch der TAL-Status dennoch fehlerhaft war (sh. LOP 2107)
						if( !bFailureLoggedToErgList ) {
							result = new Ergebnis( "PSdZFlashGatewayTable", "PSdZ", "", "", "", DE ? "Ausf�hrungsstatus der TAL" : "TAL execution status", myTALResult.getStatus().getName(), CascadeTALExecutionStatus.FINISHED.toString(), "", "0", "", "", "", DE ? "Ausgef�hrte TAL enth�lt Fehler" : "Executed TAL contains errors", "", Ergebnis.FT_NIO );
							ergListe.add( result );
						}

						throw new PPExecutionException();
					}

					// Abbruch durch Werker???
					if( userDialog.isCancelled() == true ) {
						userDialog.reset();

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.cancelTALExecution' START" );
						}
						psdz.cancelTALExecution();

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.cancelTALExecution' ENDE" );
						}

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.getEcuStatisticsHexString' START" );
						}
						// Erzeugen der TreeMaps IO, NIO und NotExecutable
						buildMaps( myTALResult, mapEcuIO, mapEcuNIO, mapEcuNotExecutable );
						// PSdZ hole die aufgelaufenen Events 
						HashMap<String, List<Exception>> psdzErrors = new HashMap<String, List<Exception>>( buildExcHashMap( myTALResult ) );
						HashMap<String, String> psdzECUStatisticsHexStr = new HashMap<String, String>( psdz.getEcuStatisticsHexString() );
						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable 'psdz.getEcuStatisticsHexString' ENDE" );
						}

						// IO - ECU in Ergebnisliste (APDM) eintragen
						entryECUsDetails( ergListe, mapEcuIO, "psdz.tal.transactionIO", Ergebnis.FT_IO, null, null );

						// NIO - / NotExecutable - ECU in Ergebnisliste (APDM) eintragen
						entryECUsFT_NIO( ergListe, mapEcuNIO, mapEcuNotExecutable, psdzErrors, psdzECUStatisticsHexStr );

						// Werkerabbruch in Ergebnisliste (APDM) eintragen
						result = new Ergebnis( "PSdZFlashGatewayTable", "Userdialog", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", PB.getString( "werkerAbbruch" ), "", PB.getString( "pollingAbbruch" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );

						throw new PPExecutionException();
					}
				}
			} catch( CascadePSdZArgumentException e ) {
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZFlashGatewayTable", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Argument" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( CascadePSdZConditionsNotCorrectException e ) {
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZFlashGatewayTable", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Bedingungen" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( CascadePSdZRuntimeException e ) {
				if( bDebug )
					e.printStackTrace();
				// Dokumentiere den Fehler in APDM 
				result = new Ergebnis( "PSdZFlashGatewayTable", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Laufzeit" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}

			// reset UserDialog
			userDialog.reset();

			// ****************************************************************************
			// * 6. ENDE: FERTIG                                                          *
			// ****************************************************************************

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// *************************************************************************************************
		// * Check auf Fehler bei fehlertoleranter (weicher) KIS // z.B.: KIS Doppeltreffer -> PP dann NIO *
		// *************************************************************************************************
		try {
			List<CascadeConfigError> svbConfigErrors = psdz.getSollverbauungConfigErrors();

			CascadeConfigError myCascadeConfigError = null;

			// jetzt die ConfigError Liste durchgehen und Fehler eintragen
			for( int i = 0; i < svbConfigErrors.size(); i++ ) {
				myCascadeConfigError = svbConfigErrors.get( i );
				// Fehler eintragen und ausgeben
				result = new Ergebnis( "PSdZFlashGatewayTable", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", (DE ? "Pr�fung der SVT-Soll bei Nutzung von fehlertoleranter KIS fehlgeschlagen! " : "SVT-Soll check failed when using error tolerant KIS! ") + (DE ? "KIS Fehler-ID: " : "KIS error ID: ") + myCascadeConfigError.getConfigErrorCode() + (DE ? ", SG: 0x" : ", ECU: 0x") + Integer.toHexString( myCascadeConfigError.getDiagAddress().intValue() ) + (DE ? ", KIS Fehlerbeschreibung: " : ", KIS error text: ") + myCascadeConfigError.getConfigErrorText(), "", Ergebnis.FT_NIO );
				ergListe.add( result );
			}

			// weiche KIS Fehler -> PP NIO 
			if( svbConfigErrors.size() > 0 )
				throw new PPExecutionException();
		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			status = STATUS_EXECUTION_ERROR;
		}

		// Freigabe der benutzten Devices
		if( psdz != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releasePSdZ();
				if( bDebug )
					CascadeLogging.getLogger().log( LogLevel.INFO, "PSdZFlashGatewayTable: Release PSdZ Device finished." );
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
		try {
			getPr�flingLaufzeitUmgebung().releaseUserDialog();
			if( bDebug )
				CascadeLogging.getLogger().log( LogLevel.INFO, "PSdZFlashGatewayTable: Release UserDialog Device finished." );
		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			if( e instanceof DeviceLockedException )
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "PSdZFlashGatewayTable", "", "", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}
		setPPStatus( info, status, ergListe );

		// Performancemessung Ende
		if( bDebugPerform )
			System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZFlashGatewayTable PP ENDE" );
	}
}

package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.framework.udnext.UDNButtonSection.UDNButton;
import com.bmw.cascade.pruefstand.framework.udnext.UDNHandle;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.ImageLocation;
import com.bmw.cascade.pruefstand.framework.udnext.UserDialogNextRuntimeEnvironment;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.UDNState;

/**
 * Implementierung der Pr�fprozedur, die f�r eine vorgegeben Zeitdauer eine Message an den Werker ausgibt
 * @author Winkler
 * @version Implementierung
 * @version 8_0_T	16.07.2007  Bauermann, optionaler Parameter FENSTERNAME hinzu um Anweisungsfenster zu benennen
 * @version 9_0_F	17.07.2007  Gebauer F-Version
 * @version 10_0_T  02.08.2012  F. Schoenert: @ Referenz Operator bei den zwing. Argumenten "DAUER" und "AWT" eingebaut
 */
public class WerkerAnweisungUDN_1_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * Default-Konstruktor, nur fuer die Deserialisierung.
	 */
	public WerkerAnweisungUDN_1_0_F_Pruefprozedur() {
	}

	/**
	 * Erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh�rigen Pr�flings.
	 * @param pruefprozName Name der Pr�fprozedur.
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit.
	 */
	public WerkerAnweisungUDN_1_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den Arguementen.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "STYLE", "FENSTERNAME", "IMAGE", "IMAGE_LOCATION" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den Arguementen.
	 */
	public String[] getRequiredArgs() {
		String[] args = { "DAUER", "AWT" };
		return args;
	}

	/**
	 * Pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>True</code> bei positiver Pr�fung, andernfalls <code>false</code>.
	 */
	public boolean checkArgs() {
		try {
			if( super.checkArgs() ) {
				if( getArg( getOptionalArgs()[0] ) != null )
					Integer.parseInt( getArg( getOptionalArgs()[0] ) ); //Parsen m�glich?			
				return !((Long.parseLong( extractValues( getArg( getRequiredArgs()[0] ) )[0] ) ) <= 0); //Werte kleiner gleich 0 nicht zul�ssig
			} else
				return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {
		// Immer notwendig.
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// Spezifische Variablen.
		String title;
		String awt;
		String image;
		String imageLocation;
		int timeout; //in Sekunden
		int style;
		UserDialogNextRuntimeEnvironment dlu = null;
		UDNHandle handle = null;

		/***********************************************
		 * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
		 * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		 ***********************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		try {
			// Parameter holen.
			try {
				if( !checkArgs() )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// Timeout aus Kompatibilit�t in Millisekunden, welche intern in Sekunden konvertiert werden.
				try {
					timeout = Math.max( Integer.parseInt( extractValues( getArg( getRequiredArgs()[0] ) )[0] ) / 1000, 1 ); //immer daf�r sorgen, dass der Quotient nie kleiner 1 ist, was bei Datentyp int am Ende 0 ergibt...
				} catch( NumberFormatException e ) {
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				}

				// Meldungstext.
				awt = getArg( getRequiredArgs()[1] );
				if( awt.indexOf( '@' ) != -1 ) {
					String[] awts = extractValues( awt );
					awt = "";
					for( int i = 0; i < awts.length; i++ )
						awt = awt + awts[i] + " ";
				} else
					awt = PB.getString( awt );

				// Style.
				if( getArg( getOptionalArgs()[0] ) != null )
					style = Integer.parseInt( getArg( getOptionalArgs()[0] ) );
				else
					style = -1;

				// Titel.
				title = getArg( getOptionalArgs()[1] );
				if( title == null )
					title = PB.getString( "anweisung" );

				// Image. Dies ist das Bild, welches der Dialog anzeigen soll (falls gew�nscht).
				image = getArg( getOptionalArgs()[2] ); //kann auch null sein, dann kein Bild

				// Position des Bildes in Bezug zum Meldungstext.
				imageLocation = getArg( getOptionalArgs()[3] ); //kann auch null sein, dann existiert die Angabe nicht und der Default wird angenommen

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Dialog anzeigen.
			try {
				dlu = getPr�flingLaufzeitUmgebung().getUserDialogNext();
				handle = dlu.allocateUserDialogNext();

				if( handle != null ) {
					if( image != null )
						dlu.displayMessage( handle, toUDNState( style ), image, toImageLocation( imageLocation ), title, awt, timeout, new UDNButton[] {} );
					else
						dlu.displayMessage( handle, toUDNState( style ), title, awt, timeout, new UDNButton[] {} );

					dlu.releaseUserDialogNext( handle ); //!!!																			
				} else
					throw new DeviceNotAvailableException();

			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			result = new Ergebnis( title, "Userdialog", "", "", "", "", "", "", "", "0", "", "", awt, "", "", Ergebnis.FT_IO );
			ergListe.add( result );

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		//Status setzen
		setPPStatus( info, status, ergListe );
	}

	/**
	 * Konvertiert den STYLE-Parameter der Pr�fprozedur in den 
	 * korrespondierenden UDN-State.
	 * 
	 * @param style STYLE-Angabe der Pr�fprozedur.
	 * @return Korrespondierender UDN-State.
	 */
	private UDNState toUDNState( int style ) {
		UDNState state;
		switch( style ) {
			case 1:
				state = UDNState.STATUS;
				break;
			case 2:
				state = UDNState.ALERT;
				break;
			case 3:
				state = UDNState.WARNING;
				break;
			case 4:
				state = UDNState.NONE;
				break;
			default:
				state = UDNState.MESSAGE;
		}
		return state;
	}

	/**
	 * Konvertiert den IMAGE_LOCATION-Parameter der Pr�fprozedur in die 
	 * korrespondierende UDN-ImageLocation Angabe. Bei �bergabe von 
	 * <code>null</code> wird ImageLocation.IMAGE_LEFT als Default zur�ck 
	 * geliefert.
	 *  
	 * @param code IMAGE_LOCATION-Parameter der Pr�fprozedur.  
	 * @return Korrespondierendes UDN-ImageLocation.
	 */
	private ImageLocation toImageLocation( String locationCode ) {
		ImageLocation location;

		if( "R".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_RIGHT;
		else if( "L".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_LEFT;
		else if( "T".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_TOP;
		else if( "B".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_BOTTOM;
		else
			location = ImageLocation.IMAGE_LEFT;

		return location;
	}

}

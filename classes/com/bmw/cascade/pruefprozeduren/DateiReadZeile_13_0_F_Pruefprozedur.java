package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;

/** Implementierung der Pruefprozedur, die n-te Zeile aus einer Datei liest<BR>
 * <BR>
 * Created on 04.08.04<BR>
 * <BR>
 * bei Aenderung Silke Bla� TI-430 Tel. 28158 informieren!<BR>
 * <BR>
 * @author BMW TI-430 Bla�<BR>
 * @version V0_0_1  04.08.2004  SB  Implementierung<BR>
 * @version V0_0_2  16.11.2004  SB  Stefan B�gl Implementierung COMPARE<BR>
 * @version V0_0_3  25.11.2004  SB  Stefan B�gl Ausgabe ERRORMESSAGE bei allen Exceptions<BR>
 * @version V4_0_F  03.06.2008  CS  APDM Ergebnis Format angepasst.<BR>
 * @version V6_0_F  17.02.2015  MS  Erweiterung um Tokensuche.<BR>
 * @version V8_0_F  03.03.2015  MS  Bugfix Tokensuche.<BR>
 * @version V9_0_T  12.05.2016  FS  Optionaler ACCESS_TIMEOUT Parameter f�r Dateizugriff. Default: 10 Sekunden.<BR>
 * @version V10_0_T 07.06.2016	MK  Default Timeout auf 5 Sekunden angepasst.<BR> 
 * @version V11_0_F	07.06.2016	MK	Produktive Freigabe Version V_10_0_T.<BR>
 * @version V12_0_T 13.09.2016  KW  Optionaler CHECK_LINENUMBER Parameter f�r �berpr�fung der Zeilnummer. <BR>
 * @version V13_0_F 01.03.2017  KW  F-Version <BR>  
 * 
 */
public class DateiReadZeile_13_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	static final long DEFAULT_TIMEOUT = 5;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DateiReadZeile_13_0_F_Pruefprozedur() {
	}

	/**
	   * erzeugt eine neue Pruefprozedur.
	   * @param pruefling Klasse des zugeh. Prueflings
	   * @param pruefprozName Name der Pruefprozedur
	   * @param hasToBeExecuted Ausfuehrbedingung f�r Fehlerfreiheit
	   */
	public DateiReadZeile_13_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "SOLLDATEN", "ERRORMESSAGE", "TOKENS", "ACCESS_TIMEOUT", "CHECK_LINENUMBER" }; //wk
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "DIR", "FILENAME", "EXTENSION", "LINENUMBER" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die parent-Methode aufgrund
	 * der offenen Anzahl an Results
	 */
	public boolean checkArgs() {
		int i, j;
		boolean exist;
		int maxIndex = 0;

		try {
			// 1. Check: Sind alle requiredArgs gesetzt
			String[] requiredArgs = getRequiredArgs();
			for( i = 0; i < requiredArgs.length; i++ ) {
				if( getArg( requiredArgs[i] ) == null )
					return false;
				if( getArg( requiredArgs[i] ).equals( "" ) == true )
					return false;
			}
			// 2. Check: Sind �berfl�ssige Argumente gesetzt, d.h. Argumente, die weder required noch optional sind
			// Ein gesetztes Argument, das mit LINE wird nicht analysiert, es wird aber der maximale Index festgehalten.
			// Die detaillierte Analyse erfolgt im 3-ten Check.
			Enumeration enu = getArgs().keys();
			String[] optionalArgs = getOptionalArgs();
			String givenkey;
			String temp;
			while( enu.hasMoreElements() ) {
				givenkey = (String) enu.nextElement();
				if( (givenkey.startsWith( "DIR" ) == false) && (givenkey.startsWith( "FILENAME" ) == false) && (givenkey.startsWith( "EXTENSION" ) == false) ) {
					exist = false;
					j = 0;
					while( (exist == false) && (j < requiredArgs.length) ) {
						if( givenkey.equals( requiredArgs[j] ) == true )
							exist = true;
						j++;
					}
					j = 0;
					while( (exist == false) && (j < optionalArgs.length) ) {
						if( givenkey.equals( optionalArgs[j] ) == true )
							exist = true;
						j++;
					}
					if( exist == false )
						return false;
					if( getArg( givenkey ).equals( "" ) == true )
						return false;
				}
			}

			// Tests bestanden, somit ok
			return true;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * fuehrt die Pruefprozedur aus
	 * @param info Information zur Ausfuehrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;
		boolean bCompare = false;
		long timeout = DEFAULT_TIMEOUT;
		String strErrorMessage = "";//Fehlermeldung die bei n.i.O. ausgegeben werden soll
		String[] astrData = null;//DatenArray
		String[] tokenArray = null;
		boolean checkLinenumber = false;  //wk

		// spezifische Variablen
		String filename, extension, dir, dir_temp, linenumber, tokens = null;
		String zeile = null;

		try {
			//Parameter holen
			try {
				//Parameterpruefung
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				//Zwingenden singulaeren Parameter DIR holen
				if( getArg( getRequiredArgs()[0] ).indexOf( '@' ) != -1 )
					dir_temp = getPPResult( getArg( getRequiredArgs()[0] ) ).trim();
				else
					dir_temp = getArg( getRequiredArgs()[0] ).trim();

				// falls ";" im DateiPfad enthalten (z.B. Aus �bergabe aus Dim DirectoryReader f�r Delete File=false, z.B. c:/dim;fals
				StringTokenizer myTokenizer = new StringTokenizer( dir_temp, ";" );
				try {
					dir = myTokenizer.nextToken();
				} catch( NoSuchElementException e ) {
					dir = dir_temp;
				}

				//Zwingenden singulaeren Parameter NAME holen
				if( getArg( getRequiredArgs()[1] ).indexOf( '@' ) != -1 )
					filename = getPPResult( getArg( getRequiredArgs()[1] ) ).trim();
				else
					filename = getArg( getRequiredArgs()[1] ).trim();
				//Zwingenden singulaeren Parameter EXTENSION holen
				if( getArg( getRequiredArgs()[2] ).indexOf( '@' ) != -1 )
					extension = getPPResult( getArg( getRequiredArgs()[2] ) ).trim();
				else
					extension = getArg( getRequiredArgs()[2] ).trim();
				//Zwingenden singulaeren Parameter LINENUMBER holen
				if( getArg( getRequiredArgs()[3] ).indexOf( '@' ) != -1 )
					linenumber = getPPResult( getArg( getRequiredArgs()[3] ) ).trim();
				else
					linenumber = getArg( getRequiredArgs()[3] ).trim();
				//optionale Parameter pr�fen
				if( getArg( "SOLLDATEN" ) != null ) {
					bCompare = true;
					if( getArg( "ERRORMESSAGE" ) != null )
						strErrorMessage = getArg( "ERRORMESSAGE" );
					String[] astrDataTemp = null;
					astrDataTemp = getArg( "SOLLDATEN" ).split( ";" );
					astrData = new String[astrDataTemp.length];
					int i = 0;
					try {
						for( i = 0; i < astrDataTemp.length; i++ ) {
							if( astrDataTemp[i].indexOf( '@' ) != -1 )
								astrData[i] = getPPResult( astrDataTemp[i] );
							else
								astrData[i] = astrDataTemp[i];
						}
					} catch( InformationNotAvailableException e ) {
						throw new PPExecutionException( strErrorMessage + ": " + e.getMessage() );
					}
				}
				if( getArg( "TOKENS" ) != null ) {
					tokens = getArg( "TOKENS" );
					StringTokenizer tokenizer = new StringTokenizer( tokens, ";" );
					tokenArray = new String[tokenizer.countTokens()];
					int k = 0;
					while( tokenizer.hasMoreTokens() ) {
						tokenArray[k] = tokenizer.nextToken();
						k++;
					}
				}
				if( getArg( "ACCESS_TIMEOUT" ) != null ) {
					timeout = Long.parseLong( getArg( getOptionalArgs()[3] ).trim() );
				}
				//wk
				if( getArg( "CHECK_LINENUMBER" ) != null ) {
					if( getArg( "CHECK_LINENUMBER" ).equals( "TRUE" ) == true )
					checkLinenumber = true; 
				}
			} catch( PPExecutionException e ) {
				e.printStackTrace();
				result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			try {
				//Seperator am Ende kuerzen
				if( dir.endsWith( "\\" ) || (dir.endsWith( "/" )) )
					dir = dir.substring( 0, dir.length() - 1 );
				if( dir.endsWith( "\\\\" ) )
					dir = dir.substring( 0, dir.length() - 2 );

				//Fileobjekt generieren
				File f = new File( dir + System.getProperty( "file.separator" ) + filename + "." + extension );

				// Pr�fe Erreichbarkeit von File mit ExecutorThread, um Timeout zu erm�glichen
				ExecutorService exSrv = Executors.newSingleThreadExecutor();
				ExecutorCompletionService<Boolean> completionService = new ExecutorCompletionService<Boolean>( exSrv );
				completionService.submit( new CheckFileExists( f ) );
				Future<Boolean> fileExists = completionService.poll( timeout, TimeUnit.SECONDS );
				if( fileExists == null || fileExists.get() != true ) {
					exSrv.shutdown();
					throw new PPExecutionException( PB.getString( "Datei " + dir + System.getProperty( "file.separator" ) + filename + "." + extension + " nicht gefunden." ) );
				}
				exSrv.shutdown();

				if( bCompare ) {
					//lesen der Daten aus Datei und Vergleich
					FileInputStream fiStream = new FileInputStream( f );
					InputStreamReader isReader = new InputStreamReader( fiStream );
					BufferedReader brDatei = new BufferedReader( isReader );
					int iCounter = 0;
					String strTemp = "";
					while( (strTemp = brDatei.readLine()) != null ) {
						if( astrData.length - 1 < iCounter ) {
							result = new Ergebnis( "Fehler Vergleich", "", "", "", "", "Eintrag" + iCounter, strTemp, "", "", "0", "", "", strErrorMessage, "Vergleich n.i.O.", "Mehr Daten in Datei als Parameter.", Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
							break;//while-Schleife verlassen
						} else if( strTemp.equals( astrData[iCounter] ) == false ) {
							//Fehlermeldung generieren und den Pr�fschritt als n.i.O. ausgeben
							result = new Ergebnis( "Fehler Vergleich", "", "", "", "", "Eintrag" + iCounter, strTemp, astrData[iCounter], "", "0", "", "", strErrorMessage, "Vergleich n.i.O.", "", Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
							break;//while-Schleife verlassen
						}
						iCounter++;
					}
					brDatei.close();
					if( status != STATUS_EXECUTION_ERROR && astrData.length > iCounter ) {//check ob alle Daten auch in der Datei enthalten sind
						result = new Ergebnis( "Fehler Vergleich", "", "", "", "", "", "", "", "", "0", "", "", strErrorMessage, "Vergleich n.i.O.", "Mehr Parameter als Daten in Datei.", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				} else {
					if( !linenumber.equalsIgnoreCase( "X" ) ) {
						//Daten rausschreiben
						if( f.canRead() ) {
							BufferedReader in = new BufferedReader( new FileReader( f ) );
							try {
								int i = 0;
								do {
									zeile = in.readLine();
									i++;
								} while( in.ready() && (i < (Integer.parseInt( linenumber ))) );
								
								//wk wenn Zeilnummer gr��er als letzte Zeilnummer, gib leer String zur�ck
								if (checkLinenumber) {
									if (i < Integer.parseInt( linenumber )) {
										zeile = "";
									}			
								}
							} catch( NullPointerException e ) {
								zeile = "";
							}
							// �ndere die Ediabas-Konfiguration implements virtuellen FZG
							result = new Ergebnis( "zeile", "FUER_SET_CONFIG", "ELEMENT", "InterfaceSOLL", "", "zeile", zeile, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
							in.close();
							//Ergebnis Doc
							result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE ReadZeile1", "TRUE", "TRUE", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						} else {
							//Ergebnis Doc
							result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE ReadZeile1 EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						}
					}
				}
				if( tokenArray != null ) {
					if( f.canRead() ) {
						BufferedReader in = new BufferedReader( new FileReader( f ) );
						String tempToken = null;
						for( int i = 0; i < tokenArray.length; i++ ) {
							String tokenString = tokenArray[i];
							do {
								zeile = in.readLine();
								if( zeile.contains( tokenString ) ) {
									tempToken = zeile;
								}
							} while( in.ready() && zeile != null );
							if( tempToken != null ) {
								result = new Ergebnis( "token", tokenString, "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "zeile", tempToken, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
							} else {
								//Ergebnis Doc
								result = new Ergebnis( "token", tokenString, "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "zeile", "token " + tokenString + " nicht gefunden!", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
							}
						}
						in.close();
					} else {
						//Ergebnis Doc
						result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE ReadZeile1 EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					}
				}
			} catch( CancellationException e ) {
				result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE IO EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( InterruptedException e ) {
				result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE IO EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( ExecutionException e ) {
				result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE IO EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( IOException e ) {
				e.printStackTrace();
				result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE IO EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( SecurityException e ) {
				e.printStackTrace();
				result = new Ergebnis( "Status", "System", "", "", dir + System.getProperty( "file.separator" ) + filename + "." + extension, "FILE ReadZeile1 SECURITY EXCEPTION", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", strErrorMessage, PB.getString( "unerwarteterLaufzeitfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", strErrorMessage, PB.getString( "unerwarteterLaufzeitfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", strErrorMessage, PB.getString( "unerwarteterLaufzeitfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}
		setPPStatus( info, status, ergListe );
	}

	/**
	 *  Die Klasse CheckFileExists erm�glicht ein threadbasiertes �berpr�fen auf Dateiexistenz.
	 * 
	 * @author Sch�nert Fabian, TI-545 BMW AG
	 *
	 */
	class CheckFileExists implements Callable {

		private File file;

		public CheckFileExists( File file ) {
			this.file = file;
		}

		@Override
		public Boolean call() throws Exception {
			return file.exists();
		}
	}

}

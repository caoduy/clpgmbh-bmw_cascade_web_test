/*
 * WerkerQuittung_V0_0_1_FU_Pruefprozedur.java Created on 14.09.00
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;

/**
 * Implementierung der Pr�fprozedur, die eine Meldung an den Werker ausgibt, die dieser zu
 * quittieren hat. �ber das Argument STYLE kann gesteuert werden, ob eine Statusmeldung (STYLE = 1)
 * oder eine Alarmmeldung (STYLE=2) ausgegeben wird, ansonsten wird die Standardmeldung verwendet.
 * 
 * @author Winkler
 * @version Implementierung
 * @version 3_0_T Unterst�tzung @-Parameter implementiert f�r AWT
 */
public class WerkerQuittung_4_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public WerkerQuittung_4_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public WerkerQuittung_4_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "TITEL" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "AWT", "STYLE" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 */
	public boolean checkArgs() {
		boolean ok;

		try {
			ok = super.checkArgs();
			if( ok == true ) {
				try {
					if( (Integer.parseInt( getArg( getRequiredArgs()[1] ) )) < 0 )
						return true;
					else
						return true;
				} catch( NumberFormatException e ) {
					return false;
				}
			}
			return ok;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String titel;
		String awt;
		String[] awts;
		int style;
		boolean udInUse = false;

		try {
			// Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				awt = getArg( getRequiredArgs()[0] );

				// Unterst�tzung @-Parameter
				if( awt.indexOf( '@' ) != -1 ) {
					awts = extractValues( awt );
					awt = "";
					for( int k = 0; k < awts.length; k++ ) {
						awt = awt + awts[k] + " ";
					}

				} else {

					awt = PB.getString( awt );
				}

				style = Integer.parseInt( getArg( getRequiredArgs()[1] ) );
				titel = getArg( getOptionalArgs()[0] );
				if( titel == null )
					titel = "meldung";
			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			try {
				if( style == 1 )
					getPr�flingLaufzeitUmgebung().getUserDialog().requestStatusMessage( PB.getString( titel ), awt, 0 );
				else if( style == 2 )
					getPr�flingLaufzeitUmgebung().getUserDialog().requestAlertMessage( PB.getString( titel ), awt, 0 );
				else
					getPr�flingLaufzeitUmgebung().getUserDialog().requestUserMessage( PB.getString( titel ), awt, 0 );
				udInUse = true;
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			result = new Ergebnis( PB.getString( titel ), "Userdialog", "", "", "", "", "", "", "", "0", "", "", awt, "", "", Ergebnis.FT_IO );
			ergListe.add( result );

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Devices freigeben
		if( udInUse == true ) {
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		// Status setzen
		setPPStatus( info, status, ergListe );

	}

}

package com.bmw.cascade.pruefprozeduren;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariablesException;

/**
 * Bes. <br>
 * Diese Pr�fprozedur ermittelt die aktuelle Zeit und erzeugt ein Ergebnis f�r das virtuelle Fzg.
 * 
 * @author Gelfert, TL-423 <br>
 * @version 0_0_1_FA 17.04.2008 RG Ersterstellung <BR>
 * @version 2_0_F 26.09.2008    MB Erweiterung: Zeit in Millisekunden, Delta-Berechnung <BR>
 * @version 3_0_F 19.05.2008    JB �nderung: Zeit in Millisekunden, Delta-Berechnung STRING-Ergebnis ohne Anhang " ms" <BR>
 * @version 4_0_T 29.07.2014    CW Parametergewinnnung angepasst, sodass Parameter nur �ber CASCADE Standard-Methoden geholt werden
 * @version 5_0_F 29.07.2014    CW Freigabe 4_0_T
 */
public class TimeStamp_5_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	
	//static final String TIME_START = "TIME_START";
	//static final String TIME_NAME = "TIME_NAME";

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public TimeStamp_5_0_F_Pruefprozedur() {
	}

	boolean myDEBUG = false;

	/**
	 * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public TimeStamp_5_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 * 
	 * @return Stringarray der optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = new String[]{"TIME_START", "TIME_NAME"};
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 * 
	 * @return
	 */
	public String[] getRequiredArgs() {
		String[] args = new String[0];
		return args;
	}

	/**
	 * prueft - soweit als m�glich - die Argumente auf Existenz und Wert
	 * 
	 * @return true, wenn die Ueberpruefung der zwingenden Argumente ok ist
	 */
	public boolean checkArgs() {
		try {
			boolean DEBUG = false;
			try {
				DEBUG = ((Boolean) getPr�flingLaufzeitUmgebung().getPruefstandVariable( 2, "DEBUG" )).booleanValue();
				if( DEBUG == true ) {
					myDEBUG = true;
					System.out.println( "--------------------------" );
					System.out.println( "Start PP TimeStamp" );
				}
			} catch( VariablesException ve ) {
				myDEBUG = false;
				// ve.printStackTrace();
			}
			return super.checkArgs();
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {

		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		Date date_now;
		Date date_last;
		String sTimeName = "Delta";

		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen


		// hier gehts los
		try {
			// Parameter-Check
			if( checkArgs() == false )
				throw new PPExecutionException( PB.getString( "parametrierfehler" ) );

			SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss.SSS"  );
			date_now = new Date();
			String uhrzeit = sdf.format( date_now );
			if( myDEBUG )
				System.out.println( "TimeStamp: " + uhrzeit );

			result = new Ergebnis( "TIMESTAMP", "CASCADE", "", "", "", "TIMESTAMP", uhrzeit, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
			
			if(getArg("TIME_NAME")!=null){ 
				sTimeName = getArg("TIME_NAME");
			}
			
			if(getArg("TIME_START")!=null){
				String sTimeLast = getArg("TIME_START");
				
				if(sTimeLast.indexOf('@')>-1) sTimeLast = this.getPPResult(sTimeLast);
				date_last = sdf.parse(sTimeLast);
				long delta = date_now.getTime() - date_last.getTime();				
				String sDelta = String.valueOf( delta );
				//result = new Ergebnis( "TIMESTAMP", "CASCADE", "", "", "", "TIMESTAMP", uhrzeit, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				
				
				
				result = new Ergebnis( sTimeName,         // ID - ID Ergebnis ID
						"CASCADE",   // werkzeug Informationsquelle (z.B. Diagnose, Strommesszange, ...)
						"",             // parameter_1 Parameter 1 f�r Werkzeug
						"",             // parameter_2 Parameter 2 f�r Werkzeug
						"delta",             // parameter_3 Parameter 3 f�r Werkzeug
						sTimeName,        // ergebnis  Name des angeforderten Ergebniswertes
						sDelta,     // ergebnisWert Zur�ckgelieferter Ergebniswert
						"",             // minWert Zul�ssiger Minimalwert
						"",             // maxWert Zul�ssiger Maximalwert
						"0",            // wiederholungen Anzahl an Wiederholungen, bis sich der unter fehlertyp angegebene Wert ergab
						"",             // frageText Fragetext, der dem Werker gestellt wird
						"",             // antwortText Antwort, die der Werker gegeben hat
						"",             // anweisText Anweisung, die der Werker auszuf�hren hatgestellt wird
						"",             // fehlerText Fehlertext, falls ein Fehler vorliegt
						"",             // hinweisText Hinweis f�r Werker zur Fehlerbeseitigung
						Ergebnis.FT_IO ); // fehlerTyp
				ergListe.add( result );
			}

		} catch( PPExecutionException ppe ) {
			ppe.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "PPExecutionException", ppe.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ) + e.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ) + t.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		setPPStatus( info, status, ergListe );
	}
	

}

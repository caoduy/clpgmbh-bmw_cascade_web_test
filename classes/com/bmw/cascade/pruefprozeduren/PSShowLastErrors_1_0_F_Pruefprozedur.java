/*
 * MPS3Init_Cards
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import java.awt.*;

import com.bmw.appframework.logging.*;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.auftrag.report.RFilter;
import com.bmw.cascade.auftrag.report.ReportFilter;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.util.logging.*;

/**
 * Diese Pr�fprozedur schreibt die Fehlermeldungen aus dem Screenprinter ins Overviewpanel (bei Multiinstanzbetrieb). 
 * 
 * @author BMW TI-431 Burger
 * @version V_1_0 02.10.2009 AB Implementierung <br>
 */
public class PSShowLastErrors_1_0_F_Pruefprozedur extends AbstractBMWPruefprozedur 
{	
	static final long serialVersionUID = 1L;
	
	int i_Debug = 0;		

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSShowLastErrors_1_0_F_Pruefprozedur() 
	{
	}

	private Logger pruefstandLogger = CascadeLogging.getLogger( "PruefstandLogger" );
	
	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSShowLastErrors_1_0_F_Pruefprozedur ( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) 
	{
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() 
	{		
		super.attributeInit();	
	}

	/**
	 * liefert die optionalen Argumente
	 * 
	 */
	public String[] getOptionalArgs() 
	{
		String[] args = 
		{		
	    };
		
		return args;
	}

	/**
	 *@param TIME	Die Zeit, die der Fehlerdialog maximal angezeigt werden soll.
	 */
	public String[] getRequiredArgs() 
	{
		String[] args = 
		{
			"TIME"
	    };
		
		return args;
	}

	/**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die parent-Methode aufgrund
     * der offenen Anzahl an Results
	 */	
	public boolean checkArgs() 
	{		
		return true;
    }
	
	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) 
	{	
		//Result vars
		Vector v_ErgListe = new Vector();	
		int i_Status = STATUS_EXECUTION_OK;
		
    	//System vars
		String s_Val = null;
		int i_ShowTime = 0;
		UserDialog c_Dialog;
		
		s_Val = getArg("TIME");
		
		try
		{
			i_ShowTime = Integer.parseInt(s_Val);
			i_ShowTime /= 1000;
			if (i_ShowTime < 1)
				i_ShowTime = 1;
			
			String xmlErrorReport = null;
			RFilter filter = new ReportFilter();
			//filter.setFilter (RFilter.F_A_LAST_PRUEFUNG);
			//filter.setFilter (RFilter.F_PP_STATUS, new Integer(ExecutionConstants.STATUS_EXECUTION_ERROR));
			filter.setFilter (RFilter.F_PP_LETZES_ERGEBNIS);			
			filter.setFilter(RFilter.F_PP_STATUS, new Integer(ExecutionConstants.STATUS_EXECUTION_ERROR));
			
			xmlErrorReport = getPr�fling().getAuftrag().createReport (filter);
			
			String s_ErrText = com.bmw.cascade.extern.xml2roh.fehlerprot.XMLFehlerprotParser.parseXML(xmlErrorReport, getPr�fling().getAuftrag());
			
			if ((getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getOuterBlock().getErrorCount() > 0) && (s_ErrText.length() > 0))
			{
				c_Dialog = getPr�flingLaufzeitUmgebung().getUserDialog();
				c_Dialog.setDisplayMode(UserDialog.ALERT_MESSAGE);
				c_Dialog.setAllowCancel(true);
				c_Dialog.displayMessage("Screenprinter", s_ErrText, i_ShowTime);
			}			
		}
		catch (Exception e)
		{
			;
		}     	    
    	
		//Set Result
		setPPStatus (info, i_Status, v_ErgListe);
	}
}

/*
 * DokuNcs_V0_1_1_TA_Pruefprozedur.java Created on 17.04.01
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import java.text.*;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.ediabas.*;
import com.bmw.cascade.util.dom.DomPruefstand;
import com.bmw.cascade.*;

/**
 * Erzeugt NCS FA Datei. Dennert TO DO: 2. Format von Datum / Zeit stimmt nicht History
 * 
 * @author Crichton
 * @version Implementierung 24.04.02 LD Zusbaunummern komplett auskommentiert
 * 0_1_1_FA 22.04.04 MN Mini erg�nzt Bugfix: F�r den Fall, da� keine SAs im Auftrag sind
 *          jetzt keine Exception mehr <br>
 * 12_0_F   11.05.05 NC DOM Transfer korrigiert
 */
public class DokuNcs_12_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DokuNcs_12_0_F_Pruefprozedur() {
	}

	/**
	 * Formate f�r Datum- und Zeitausgaben.
	 */
	private static DateFormat dfDatum = DateFormat.getDateInstance( DateFormat.MEDIUM, Locale.GERMANY );
	private static DateFormat dfZeit = DateFormat.getTimeInstance( DateFormat.MEDIUM, Locale.GERMANY );

	/**
	 * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DokuNcs_12_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = new String[0];
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = new String[0];
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert!!!
	 */
	public boolean checkArgs() {
		return true;
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// Always required
		String sgbd = "";
		String job = "";
		String jobres = "";
		String jobargs = "";
		String temp = "";
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;
		int tempStatus = STATUS_EXECUTION_OK;

		// Variables for data set
		String fileName;
		StringBuffer xml = null;
		Pruefprozedur pp;

		// Check Parameters
		fileName = "";
		String separator = "\"";

		// Collect information
		xml = new StringBuffer();
		String az = "\"";

		// Header ist immer gleich
		xml.append( "<?xml version=" + az + "1.0" + az + " encoding=" + az + "iso-8859-1" + az + "?>" + "\r\n" );
		xml.append( "<!DOCTYPE vehicleOrders SYSTEM " + az + "vehicleOrders.dtd" + az + ">" + "\r\n" );

		// existieren immer
		xml.append( "<vehicleOrders>" + "\r\n" );
		xml.append( "\t" + "<order>" + "\r\n" );

		String vinLong = getPr�fling().getAuftrag().getFahrgestellnummer();

		try {
			if( (getPr�fling().getAuftrag().getBaureihe().equals( "E39" )) || (getPr�fling().getAuftrag().getBaureihe().equals( "E52" )) || (getPr�fling().getAuftrag().getBaureihe().equals( "E53" )) || (getPr�fling().getAuftrag().getBaureihe().equals( "R50" )) || (getPr�fling().getAuftrag().getBaureihe().equals( "R52" )) || (getPr�fling().getAuftrag().getBaureihe().equals( "R53" )) )
			// ZCS
			{
				String gm = "";
				String sa = "";
				String vn = "";

				String vinShort = getPr�fling().getAuftrag().getFahrgestellnummer7();

				// ZCS aus SG lesen
				try {
					// Zuerst SGBD_SIV ausfuehren, um den SGBD-Namen zu initialisieren
					pp = null;
					pp = getPr�fling().getAuftrag().findPr�fprozedur( "KOMBI", "SGBD_SIV" );
					if( pp != null ) {
						pp.setPr�flingLaufzeitUmgebung( getPr�flingLaufzeitUmgebung() );
						pp.execute( info );
						sgbd = getPr�fling().getAuftrag().getPr�fling( "KOMBI" ).getAttribut( "SGBD" );
						job = "C_ZCS_LESEN";
						jobargs = "";
						temp = Ediabas.executeDiagJob( sgbd, job, jobargs, "" );
						if( temp.equals( "OKAY" ) == false ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobargs, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						} else // if ( temp.equals("OKAY") == false )
						{
							gm = Ediabas.getDiagResultValue( "GM" );
							sa = Ediabas.getDiagResultValue( "SA" );
							vn = Ediabas.getDiagResultValue( "VN" );
						}
					}
				} catch( ApiCallFailedException acfe ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobargs, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), Ediabas.apiErrorCode() + ": " + Ediabas.apiErrorText(), Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				}
				// Einf�gen in den XML-Stream
				xml.append( "\t\t" + "<centralCodingKey vin=" + az + vinShort + az + " vinLong=" + az + vinLong + az + " c1=" + az + gm + az + " c2=" + az + sa + az + " c3=" + az + vn + az + " />\r\n" );
			} else
			// Fahrzeugauftrag
			{
				String checkDigit = calculateChecksum( vinLong );
				String location = CascadeProperties.getCascadeWerkCode();
				Date d = new Date();
				String datum = dfDatum.format( d );
				String zeit = dfZeit.format( d );
				xml.append( "\t" + "\t" + "<coding vinLong=" + az + vinLong + az + " checkDigit=" + az + checkDigit + az + " location=" + az + location + az + " time=" + az + datum + " " + zeit + az + ">" + "\r\n" );

				// existiert immer
				xml.append( "\t" + "\t" + "\t" + "<checksum>FP</checksum>" + "\r\n" );

				// existiert immer
				String type = getPr�fling().getAuftrag().getTypschl�ssel();

				// Build Version, leider muss 4-Stellig sein!!
				// String buildVersion =
				// getPr�fling().getAuftrag().getZeitkriteriumMonat()+getPr�fling().getAuftrag().getZeitkriteriumJahr();
				StringBuffer buildVersion = new StringBuffer();
				temp = getPr�fling().getAuftrag().getZeitkriteriumMonat();
				if( temp.length() == 1 )
					buildVersion.append( "0" );
				buildVersion.append( temp );
				temp = getPr�fling().getAuftrag().getZeitkriteriumJahr();
				if( temp.length() == 1 )
					buildVersion.append( "0" );
				buildVersion.append( temp );

				String model = getPr�fling().getAuftrag().getBaureihe();
				xml.append( "\t" + "\t" + "\t" + "<model type=" + az + type + az + " buildVersion=" + az + buildVersion + az + ">" + model + "</model>" + "\r\n" );

				// existiert immer
				String paintCode = getPr�fling().getAuftrag().getFarbe();
				xml.append( "\t" + "\t" + "\t" + "<paintCode>" + paintCode + "</paintCode>" + "\r\n" );

				// existiert immer
				String upholstery = getPr�fling().getAuftrag().getPolster();
				xml.append( "\t" + "\t" + "\t" + "<upholstery>" + upholstery + "</upholstery>" + "\r\n" );

				// existiert nur wenn SAs vorhanden sind
				String[] saCodes = getPr�fling().getAuftrag().getSAs();
				if( saCodes != null ) {
					xml.append( "\t" + "\t" + "\t" + "<options>" + "\r\n" );
					for( int i = 0; i < saCodes.length; i++ ) {
						xml.append( "\t" + "\t" + "\t" + "\t" + "<option>" + saCodes[i].substring( 1, 4 ) + "</option>" + "\r\n" );
					}
					xml.append( "\t" + "\t" + "\t" + "</options>" + "\r\n" );
				}

				// existiert nur wenn E-Worte vorhanden sind
				String[] ewCodes = getPr�fling().getAuftrag().getEWs();
				if( ewCodes != null ) {
					xml.append( "\t" + "\t" + "\t" + "<eWords>" + "\r\n" );
					for( int i = 0; i < ewCodes.length; i++ ) {
						xml.append( "\t" + "\t" + "\t" + "\t" + "<eWord>" + ewCodes[i] + "</eWord>" + "\r\n" );
					}
					xml.append( "\t" + "\t" + "\t" + "</eWords>" + "\r\n" );
				}

				/*
				 * LH Codierung 6 905 863.4 Teil 2 "Fahrzeugauftrag" entf�llt seit 22.03.02 //
				 * Zusbau Nummern String[] unName = new String[3]; String[] unNummer = new
				 * String[3]; int j=0; // Sonderablauf f�r den E65 ohne ZB-NR check - loeschen,
				 * sobald Serienablauf eingehalten werden kann if (
				 * (getPr�fling().getAuftrag().getBaureihe().equals("E65")) ||
				 * (getPr�fling().getAuftrag().getBaureihe().equals("E66")) ||
				 * (getPr�fling().getAuftrag().getBaureihe().equals("E67")) ) { try {
				 * unName[j]="DME1"; // Zuerst SGBD_SIV ausfuehren, um den SGBD-Namen zu
				 * initialisieren pp = null; pp =
				 * getPr�fling().getAuftrag().findPr�fprozedur("MOTOR","SGBD_SIV"); if (pp != null) {
				 * pp.setPr�flingLaufzeitUmgebung(getPr�flingLaufzeitUmgebung()); pp.execute(info);
				 * sgbd=getPr�fling().getAuftrag().getPr�fling("MOTOR").getAttribut("SGBD");
				 * job="AIF_LESEN"; jobargs=""; temp = Ediabas.executeDiagJob(sgbd,job,jobargs,"");
				 * if ( temp.equals("OKAY") == false ) { result = new Ergebnis( "DiagFehler",
				 * "EDIABAS", sgbd, job, jobargs, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "",
				 * PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO); ergListe.add(result);
				 * unNummer[j]="Error"; } else // if ( temp.equals("OKAY") == false ) {
				 * unNummer[j]=Ediabas.getDiagResultValue("AIF_ZB_NR"); j++; } } } catch
				 * (ApiCallFailedException acfe) { result = new Ergebnis( "DiagFehler", "EDIABAS",
				 * sgbd, job, jobargs, "", "", "", "", "0", "", "", "", PB.getString(
				 * "diagnosefehler" ),Ediabas.apiErrorCode()+": " + Ediabas.apiErrorText(),
				 * Ergebnis.FT_NIO); ergListe.add(result); unNummer[j]="Error"; } try {
				 * unName[j]="DME2"; // Zuerst SGBD_SIV ausfuehren, um den SGBD-Namen zu
				 * initialisieren pp = null; pp =
				 * getPr�fling().getAuftrag().findPr�fprozedur("MOTOR2","SGBD_SIV"); if (pp != null) {
				 * pp.setPr�flingLaufzeitUmgebung(getPr�flingLaufzeitUmgebung()); pp.execute(info);
				 * sgbd=getPr�fling().getAuftrag().getPr�fling("MOTOR2").getAttribut("SGBD");
				 * job="AIF_LESEN"; jobargs=""; temp = Ediabas.executeDiagJob(sgbd,job,jobargs,"");
				 * if ( temp.equals("OKAY") == false ) { result = new Ergebnis( "DiagFehler",
				 * "EDIABAS", sgbd, job, jobargs, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "",
				 * PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO); ergListe.add(result);
				 * unNummer[j]="Error"; } else // if ( temp.equals("OKAY") == false ) {
				 * unNummer[j]=Ediabas.getDiagResultValue("AIF_ZB_NR"); j++; } } } catch
				 * (ApiCallFailedException acfe) { result = new Ergebnis( "DiagFehler", "EDIABAS",
				 * sgbd, job, jobargs, "", "", "", "", "0", "", "", "", PB.getString(
				 * "diagnosefehler" ), Ediabas.apiErrorCode()+": " + Ediabas.apiErrorText(),
				 * Ergebnis.FT_NIO); ergListe.add(result); unNummer[j]="Error"; } try {
				 * unName[j]="EGS"; // Zuerst SGBD_SIV ausfuehren, um den SGBD-Namen zu
				 * initialisieren pp = null; pp =
				 * getPr�fling().getAuftrag().findPr�fprozedur("GETRIEBE","SGBD_SIV"); if (pp !=
				 * null) { pp.setPr�flingLaufzeitUmgebung(getPr�flingLaufzeitUmgebung());
				 * pp.execute(info);
				 * sgbd=getPr�fling().getAuftrag().getPr�fling("GETRIEBE").getAttribut("SGBD");
				 * job="AIF_LESEN"; jobargs=""; temp = Ediabas.executeDiagJob(sgbd,job,jobargs,"");
				 * if ( temp.equals("OKAY") == false ) { result = new Ergebnis( "DiagFehler",
				 * "EDIABAS", sgbd, job, jobargs, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "",
				 * PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO); ergListe.add(result);
				 * unNummer[j]="Error"; } else // if ( temp.equals("OKAY") == false ) {
				 * unNummer[j]=Ediabas.getDiagResultValue("AIF_ZB_NR"); j++; } } } catch
				 * (ApiCallFailedException acfe) { result = new Ergebnis( "DiagFehler", "EDIABAS",
				 * sgbd, job, jobargs, "", "", "", "", "0", "", "", "", PB.getString(
				 * "diagnosefehler" ), Ediabas.apiErrorCode()+": " + Ediabas.apiErrorText(),
				 * Ergebnis.FT_NIO); ergListe.add(result); unNummer[j]="Error"; } } else // Alle
				 * Baureihen ausser E65 { pp = null; pp =
				 * getPr�fling().getAuftrag().findPr�fprozedur("MOTOR","ZB_NR"); if (pp != null) {
				 * pp.setPr�flingLaufzeitUmgebung(getPr�flingLaufzeitUmgebung()); pp.execute(info);
				 * tempStatus = pp.getExecStatus(); if( tempStatus == STATUS_EXECUTION_OK ) {
				 * unName[j] = "DME1"; unNummer[j] = getPPResult("AIF_ZB_NR@MOTOR.ZB_NR"); j++; }
				 * else { // Beim fehler PP liefert NIO zurueck aber erzeugt sowieso eine Datei mit
				 * "ERROR" eintrag status = STATUS_EXECUTION_ERROR; unName[j] = "DME1"; unNummer[j] =
				 * "ERROR"; j++; } } pp = null; pp =
				 * getPr�fling().getAuftrag().findPr�fprozedur("MOTOR_2","ZB_NR"); if (pp != null) {
				 * pp.setPr�flingLaufzeitUmgebung(getPr�flingLaufzeitUmgebung()); pp.execute(info);
				 * tempStatus = pp.getExecStatus(); if( tempStatus == STATUS_EXECUTION_OK ) {
				 * unName[j] = "DME2"; unNummer[j] = getPPResult("AIF_ZB_NR@MOTOR_2.ZB_NR"); j++; }
				 * else { // Beim fehler PP liefert NIO zurueck aber erzeugt sowieso eine Datei mit
				 * "ERROR" eintrag status = STATUS_EXECUTION_ERROR; unName[j] = "DME2"; unNummer[j] =
				 * "ERROR"; j++; } } pp = null; pp =
				 * getPr�fling().getAuftrag().findPr�fprozedur("GETRIEBE","ZB_NR"); if (pp != null) {
				 * pp.setPr�flingLaufzeitUmgebung(getPr�flingLaufzeitUmgebung()); pp.execute(info);
				 * tempStatus = pp.getExecStatus(); if( tempStatus == STATUS_EXECUTION_OK ) {
				 * unName[j] = "EGS"; unNummer[j] = getPPResult("AIF_ZB_NR@GETRIEBE.ZB_NR"); j++; }
				 * else { // Beim fehler PP liefert NIO zurueck aber erzeugt sowieso eine Datei mit
				 * "ERROR" eintrag status = STATUS_EXECUTION_ERROR; unName[j] = "EGS"; unNummer[j] =
				 * "ERROR"; j++; } } } if (unName[0] != null) { xml.append("\t"+"\t"+"\t"+"
				 * <units>"+"\r\n"); for (int i=0; i < unName.length; i++) { if ( unName[i] != null ) {
				 * xml.append("\t"+"\t"+"\t"+"\t"+" <unit unName="+az+unName[i]+az+">"+unNummer[i]+"
				 * </unit>"+"\r\n"); } } xml.append("\t"+"\t"+"\t"+" </units>"+"\r\n"); }
				 */

				// existieren immer
				xml.append( "\t" + "\t" + "</coding>" + "\r\n" );

			}

			xml.append( "\t" + "</order>" + "\r\n" );
			xml.append( "</vehicleOrders>" + "\r\n" );

			//und jetzt der Datei lokal schreiben, es wird am ende von FZ Pr�fung nach dem Server
			// verschoben.
			try {
				DomPruefstand.writeDomData( getPr�fling().getAuftrag().getFahrgestellnummer7(), "F", xml.toString().getBytes() );
			} catch( Exception e ) {
				result = new Ergebnis( "ExecFehler", "DomTransfer", "", "", "", "", "", "", "", "0", "", "", "", "FileWrite", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Ergebnis speichern
			if( status == STATUS_EXECUTION_OK ) {
				result = new Ergebnis( "DomDokumentation", "", "", "", "", "STATUS", "" + status, "" + STATUS_EXECUTION_OK, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );
			} else {
				result = new Ergebnis( "DomDokumentation", "", "", "", "", "STATUS", "" + status, "" + STATUS_EXECUTION_OK, "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
				ergListe.add( result );
			}
		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			e.printStackTrace();
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		setPPStatus( info, status, ergListe );
	}

	private String calculateChecksum( String fgnr ) {
		boolean toggle = true;
		int coffset = 55;
		int doffset = 48;
		int sum = 0;
		int modsum = 0;
		int chint = 0;
		char chkSum = 0;

		char[] charr = fgnr.toCharArray();
		for( int i = 0; i < charr.length; i++ ) {
			if( Character.isDigit( charr[i] ) ) {
				chint = ((int) charr[i]) - doffset;
			} else {
				chint = ((int) charr[i]) - coffset;
			}
			if( toggle ) {
				toggle = false;
				sum += (chint * 3);
			} else {
				toggle = true;
				sum += chint;
			}
		}

		sum += 70; // von f�hrendem String "FP"

		int result = sum % 36;

		if( result < 10 ) {
			result = result + doffset;
		} else {
			result = result + coffset;
		}

		byte[] myBytes = { Byte.parseByte( Integer.toString( result ) ) };
		return (new String( myBytes ));
	}

}


/*
 * Pause_VX_X_X_XX_Pruefprozedur
 *
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;


import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.pruefumfang.*;


/**
 * Implementierung der Pr�fprozedur, die eine vorgegeben Zeitdauer wartet
 *
 * @author BMW TI-430 Winkler, Schaller, M�ller, Crichton, Schumann, Saller
 * @version V0_0_1  14.09.2000  HW  Implementierung <br>
 *          V0_0_2  13.08.2002  RS  Erweiterung um Counterbox mit Hinweistext <br>
 *          V0_0_3  16.10.2002  RS  Bug-fix Token wird jetzt beim hwt auch ersetzt <br>
 *          V0_0_4  27.06.2003  MM  Berechnung der Wartezeit korrigiert, da sie abh�ngig vom Userdialogaufbau war <br>
 *          V0_0_5  14.07.2003  NC  Semikolon sind im HWT erlaubt (Zeilenumbruche) <br>
 *          V10_0_F 16.05.2008  CS  Variable "dauer" auf Ganzzahligkeit �berpr�ft, falls nicht ganzzahlig, angepasst <br> 
 *          V14_0_F	04.02.2011	MS	�nderung Systemzeit in System.nanoTime(); Umstellung Pause auf LockSupport.parkNanos() <br>
 *          V16_0_F	13.01.2014	TB	Erlaube @-Operator beim Dauer-Parameter <br>
 *          V17_0_F	23.01.2014	TB	F-Version <br>
 */
public class Pause_17_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
	
	static final long serialVersionUID = 1L;
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */	
  public Pause_17_0_F_Pruefprozedur() {}

  /**
     * erzeugt eine neue Pruefprozedur.
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public Pause_17_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
        
    }
    
    
    /**
     * initialsiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /**
     * liefert die optionalen Argumente
     */
    public String[] getOptionalArgs() {
        // String[] args = new String[0];
        String[] args = {"HWT"};
        return args;
    }
    
    /**
     * liefert die zwingend erforderlichen Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = {"DAUER"};
        return args;
    }
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
     */
    public boolean checkArgs() {
		// Obligatorisch.
		return super.checkArgs();
    }
    
    /**
     * f�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
   public void execute(ExecutionInfo info ) {
    	 // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        // spezifische Variablen
        long dauer = 0;
        long temp = 0;

        String hwt;
        
        /***********************************************
         * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
         * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
         ***********************************************/
        try {
            UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
            myAnalyser.LogSetTestStepName(this.getName());
            getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
        } catch (Exception e) {
            try {
                getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
            }
            catch (Exception e1) {}
        } 

        try {
            //Parameter holen
            try {
                if( checkArgs() == false ) throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
                try {
                    dauer = Long.parseLong( extractValues(getArg( getRequiredArgs()[0] ) ) [0] ) ;
                } catch (NumberFormatException e) {
                    throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
                }
                if( getArg( getOptionalArgs()[0] ) != null ) {
                    hwt = PB.getString( getArg( getOptionalArgs()[0] ) );
                }
                else {
                    hwt = "";
                }
            } catch (PPExecutionException e) {
            	e.printStackTrace();
                if (e.getMessage() != null)
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            //Warten
    		long startTime = System.nanoTime()/1000000;
            long endTime = startTime+dauer;
            
            if( (dauer > 2999) && ( getArg( getOptionalArgs()[0] ) != null ) ) {
                com.bmw.cascade.pruefstand.visualisierung.UserDialog myDialog = null;
                try {
                    myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
                } catch (Exception e) {
                }
                do {
                	// Achtung, hier sollen nur ganzzahlige Ergebnisse weiterverwendet werden!
                	// falls Ergebnis nicht ganzzahlig -> dauer anpassen
                	if(dauer%1000!=0){
                		int modValue = (int) (dauer%1000);
                		dauer = dauer + (1000-modValue);
                		temp = (dauer/1000);
                	}else{
                		temp = (dauer/1000); 
                	}
                    try {
                        myDialog.displayMessage( PB.getString( "hinweis" ), temp+" sec. "+hwt, -1 );
                    } catch (Exception e) {

                    }
                    if (dauer>1300) {
                        
                        	LockSupport.parkNanos(1000*1000000);                        
                       
                    } else {
                        // Restzeit ist zu kurz f�r einen erneuten Durchlauf - also einfach abwarten
                        
                    		LockSupport.parkNanos(dauer*1000000);
                    }
                    // Die Restdauer muss an dieser Stelle neu berechnet werden
                    dauer = endTime-(System.nanoTime()/1000000);
                    
                } while(dauer > 0);
                try {
                    getPr�flingLaufzeitUmgebung().releaseUserDialog();
                } catch (Exception e) {
                }
            }
            else {
            	
            	//Unterteilung der Pause in 100ms Bl�cke
                do {                	
                	if (dauer <= 100){
                		
                		LockSupport.parkNanos(dauer*1000000);
                    	dauer = endTime-(System.nanoTime()/1000000);
                	}
                	else{
                		do{
                			
                		dauer = 100;                		
                    	LockSupport.parkNanos(dauer*1000000);                    	
                    	dauer = endTime-(System.nanoTime()/1000000); 
                    	
                		}while (dauer > 100);
                	}
                } while( dauer > 0 );
                
            }
    		          
            
            result = new Ergebnis( "Status", "System", "", "", "", "", "", "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
            ergListe.add(result);
            
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
        setPPStatus( info, status, ergListe );
    	
    }
    
}

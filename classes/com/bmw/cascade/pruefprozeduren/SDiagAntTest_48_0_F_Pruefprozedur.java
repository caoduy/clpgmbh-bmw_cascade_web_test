package com.bmw.cascade.pruefprozeduren;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;

import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceIOException;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceParameterException;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;


/**
 * Implementierung von Antennentest zur �berpr�fung des Verbaues der Antennen Es werden folgende
 * Jobs ben�tigt: <br>
 * STEUERN_ANT_EIGEN_DIAG <br>
 * STEUERN_FREQUENZ <br>
 * STATUS_ANT_QFS <br>
 * STEUERN_ANT_SCAN <br>
 * <br>
 * Pflichtparameter: "SGBD" = SGBD-Name <br>
 * "FREQFM" = Frequenz des FM-Signals (Tuner wird auf diese Frequenz bei FM-Test eingestellt) <br>
 * "FREQAM" = Frequenz des AM-Signals (Tuner wird auf diese Frequenz bei AM-Test eingestellt) <br>
 * "COUNTFM" = Anzahl der zu testenden FM-Antennen <br>
 * "COUNTAM" = Anzahl der zu testenden AM-Antennen (bis jetzt nur eine m�glich!) <br>
 * "TOLERANCE_VAL[1..N]" = Toleranzwerte MAX f�r die Antennen (FM1, FM2, ... , AM) f�r jede Antenne
 * muss ein Toleranzwert angegeben werden <br>
 * <br>
 * Optionale Parameter: <br>
 * "KAROSSERIE" = Karosseriename <br>
 * "RADIOTYP" = Radiotypname <br>
 * "LSTYP" = Lautsprechertyp (Hifi..) <br>
 * "ANTENNENTYP[1..N]" = Name der Antennen (FM1,FM2,...AM) <br>
 * "CHECK_STATUS_ANT" = deaktivieren des Jobs STATUS_ANT_EIGENDIAGNOSE mit FALSE
 * "PAUSE_NACH_STEUERN_FREQ", "PAUSE_NACH_STEUERN_ANT",
 * "PAUSE_ANDERE_JOBS","PAUSE_ZWISCHEN_MESSUNGEN" Einstellen der Pausen <br>
 * "MIN_DIFF_AM_AMP_IN_DB" = die notwendige Differenz in der Feldst�rke zwischen AM-Verst�rker
 * aus/an <br>
 * "USE_1DB_PRECISION" = pr�ft auf 1db genau, vorerst nur CCC <br>
 * "NOTRACE" = Abschalten der Tracefunktion (durch APDM nicht mehr ben�tigt) <br>
 * <br>
 * Sind die optionalen Parameter "KAROSSERIE", "RADIOTYP" und "LSTYP" angegeben, wird ein
 * Ergebnisfile lokal gespeichert. <br>
 * <br>
 * 
 * @author Rainer Merz (rm), Frank Weber(fw)
 * @version 48_0_F FW 22.10.2009 live version of 45_0_F
 * @version 47_0_T FW 22.10.2009 changed diversity self diagnostic repetition to perform full diversity diagnostic cycle
 * @version 46_0_F FW 16.02.2009 live version of 45_0_F
 * @version 45_0_T FW 16.02.2009 bugfix lessorequal parameter
 * @version 44_0_F FW 12.01.2009 live version of 43_0_F
 * @version 43_0_T FW 12.01.2009 added function to be able to test max value, added new block parameters
 * @version 42_0_F FW 28.04.2008 live version of 41_0_T
 * @version 41_0_T IB 25.04.2008 added antenna test running information (awt)
 * @version 40_0_F FW 05.11.2007 enhanced result parsing to be able to compute L6 result type double
 * @version 39_0_F FW 24.10.2007 live version of 38_0_T
 * @version 38_0_T FW 24.10.2007 added Parameters to change fieldstrength and 
 * @version 37_0_F FW 31.07.2007 live version of 36_0_T
 * @version 36_0_T FW 31.07.2007 added new parameters to be able to send jobs before field strength test
 * @version 35_0_F FW 18.06.2007 live version of 34_0_T
 * @version 34_0_T FW 18.06.2007 JOBPAR_SWITCH_DIVERSITY_DIAG and JOBPARSET_FREQUENCY -> add end character ; as Cascade cannot do
 * @version 34_0_T FW 18.06.2007 continue on error possibility added
 * @version 33_0_F FW 06.12.2006 live version of 32_0_T
 * @version 32_0_T FW 06.12.2006 NUM_MEASURES_PER_ANTENNA added, added feature to skip setting fm frequency (set to 0)
 * @version 31_0_F FW 03.11.2006 live version of 30_0_T 
 * @version 30_0_T FW 03.11.2006 added parameters to define job names and parameters
 * @version 29_0_F FW 15.10.2006 live version of 28_0_T 
 * @version 28_0_T FW 15.10.2006 added parameter IGNORE_CHECK_STATUS_ANT to set ignore diversity self check
 * @version 27_0_F FW 05.10.2006 live version of 26_0_T 
 * @version 26_0_T FW 05.10.2006 added parameter to set repetitions for diversity self check
 * @version 25_0_F FW 19.03.2006 productive version of 22_0_T
 * @version 24_0_T FW 19.03.2006 check_status_ant reworked so that single FM measurement is possible
 * @version 23_0_F FW 17.02.2006 productive version of 22_0_T
 * @version 22_0_T FW 17.02.2006 checks for parameters implemented,
 *                               now -1 is possible to disable field strength measurement for some FM antennas
 * @version 21_0_T FW 17.02.2006 new Ediabas calls implemented,
 * @version 20 FW F version of 19
 * @version 19 Test FW deactivation self diagnosis in case of error added
 * @version 0_1_8 FW FA version for pruefprocedure
 * @version 0_1_7Test FW new parameter ERRORTEXT_STATUS_ANT to define errortext for diversity self
 *          diagnosis, TOLERANCE_MAXIMUM added for a maximum value
 * @version 0_1_6 FW new parameter SGBDFREQ for having a different sgdb for setting freuqencies, am
 *          amplifier test reworked,error message for diversity-antenna check reworked
 * @version 0_1_4 FW Parameter "USE_1DB_PRECISION" �berarbietet: kein check auf sgbdant = ant_60
 * @version 0_1_3 FW Zus�tzliche optionale Parameter hinzugef�gt: "MIN_DIFF_AM_AMP_IN_DB",
 *          "USE_1DB_PRECISION", "NOTRACE"
 * @version 0_1_2 RM Zus�tzliche optionale Parameter hinzugef�gt: "CHECK_STATUS_ANT",
 *          "PAUSE_NACH_STEUERN_FREQ", "PAUSE_NACH_STEUERN_ANT",
 *          "PAUSE_ANDERE_JOBS","PAUSE_ZWISCHEN_MESSUNGEN"
 * @version 0_1_1 RM Ergebnisbewertung von STATUS_ANT_EIGEN_DIAG f�r RAD1, RAD2, CCC, MASK
 *          gleichgezogen
 * @version 0_1_0 RM Ergebnisbewertung von STATUS_ANT_EIGEN_DIAG f�r CCC tempor�r deaktiviert
 * @version 0_0_9 RM STATUS_ANT_EIGEN_DIAG getrennt nach RAD1,RAD2 und CCC,MASK, da Ergebniswert
 *          unterschiedlich
 * @version 0_0_8 RM Wiederholung eingebaut bei fehlerhafter STATUS_ANT_EIGEN_DIAG, Exception bei
 *          STATUS_ANT_EIGEN_DIAG aktiviert
 * @version 0_0_7 RM Fehlerkorrektur Ergebnisfile schreiben f�r AM - Messung
 * @version 0_0_6 RM STEUERN_RDS entfernt
 * @version 0_0_5 RM STEUERN_FREQUENZ vor STEUERN_ANT_EINGEN_DIAG gesetzt
 * @version 0_0_3 RM Exception bei STEUERN_RDS deaktiviert
 * @version 0_0_2 RM Exception bei STATUS_ANT_EIGEN_DIAG deaktiviert
 */
public class SDiagAntTest_48_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public SDiagAntTest_48_0_F_Pruefprozedur() {
	}

	//F�r Traceinfo
	private static DateFormat dfDatum = DateFormat.getDateInstance( DateFormat.MEDIUM, Locale.GERMANY );
	private static DateFormat dfZeit = DateFormat.getTimeInstance( DateFormat.MEDIUM, Locale.GERMANY );

	private String sKarosserie;
	private String sRadioTyp;
	private String sLsTyp;
	private String sFgNr;
	private String sBaureihe;
	private String sAntennentyp[];
	private String sToleranzValues[];
	private String sMessfrequenz[];
	private String sAktMessfrequenz;

	// language
	private final boolean isEnglish = System.getProperty( "user.language" ).equalsIgnoreCase( "EN" );

	/**
	 * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public SDiagAntTest_48_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName,
			Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialisiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "TOLERANCE_VAL[1..N]","TOLERANCE_VALUES","ANTENNA_NAMES",
						  "KAROSSERIE", "RADIOTYP", "LSTYP", "ANTENNENTYP[1..N]", "CHECK_STATUS_ANT",
						  "PAUSE_NACH_STEUERN_FREQ", "PAUSE_NACH_STEUERN_ANT", "PAUSE_ANDERE_JOBS",
						  "PAUSE_ZWISCHEN_MESSUNGEN", "MIN_DIFF_AM_AMP_IN_DB", "USE_1DB_PRECISION",
						  "NOTRACE", "SGBDFREQ", "ERRORTEXT_STATUS_ANT", "TOLERANCE_MAXIMUM",
						  "NUM_REPETITIONS_CHECK_STATUS_ANT","IGNORE_CHECK_STATUS_ANT",
						  "NUM_MEASURES_PER_ANTENNA",
						  "JOBNAME_SETFREQUENCY","JOBNAME_START_DIVERSITY_DIAG",
						  "JOBNAME_STOP_DIVERSITY_DIAG","JOBNAME_SWITCH_DIVERSITY_DIAG",
						  "JOBNAME_MEASURE_FIELDSTRENGTH",
						  "JOBPAR_SETFREQUENCY","JOBPAR_START_DIVERSITY_DIAG",
						  "JOBPAR_STOP_DIVERSITY_DIAG","JOBPAR_SWITCH_DIVERSITY_DIAG",
						  "JOBPAR_MEASURE_FIELDSTRENGTH","CONTINUE_FM_DIV_DIAG_ON_ERROR",
						  "JOB_BEFORE_FM_NAME","JOB_BEFORE_FM_PAR","JOB_BEFORE_FM_PAUSE",
						  "JOB_BEFORE_AM_NAME","JOB_BEFORE_AM_PAR","JOB_BEFORE_AM_PAUSE",
						  "JOBRESULT_MEASURE_FIELDSTRENGTH", "JOBRESULT_CHECK_STATUS_ANT",
						  "AWT","UDS"};
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "SGBD", "FREQFM", "FREQAM", "COUNTFM", "COUNTAM" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	public boolean checkArgs() {
		int i;
		boolean ok;

		try {

			ok = super.checkArgs();
			if( ok == true ) {
				// 1. Check: Sind alle requiredArgs gesetzt
				String[] requiredArgs = getRequiredArgs();
				for( i = 0; i < requiredArgs.length; i++ ) {
					if( getArg( requiredArgs[i] ) == null )
						return false;
					if( getArg( requiredArgs[i] ).equals( "" ) == true )
						return false;
				}

				// bis jetzt nur eine AM - Antenne !
				if( Integer.parseInt( getArg( "COUNTAM" ) ) > 1 )
					return false;

				// f�r jede Antenne muss ein Toleranzwert angegeben sein
				if( ((Integer.parseInt( getArg( "COUNTFM" ) ) + Integer.parseInt( getArg( "COUNTAM" ) )) + 4) < requiredArgs.length )
					return false;

			}

			return true;
			// alle Argumente i.O. (vorhanden u. Format)

		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}

	}

	/**
	 * Writes the result file.
	 * 
	 * @version 1.0 2002-13-12
	 * @author Rainer Merz
	 * @exception DeviceIOException thrown if a problem with the Output Stream happened
	 * @exception DeviceParameterException thrown, if no output path available
	 */
	private void traceResults( int anzahlAntennen, int anzahlMesswerte, int[] status, int[] messwerte ) throws DeviceIOException, DeviceParameterException {
		try {
			Date d = new Date();

			File resultPath = new File( "C:\\Antenne\\Ergebnis" + File.separator + sBaureihe + File.separator + sKarosserie + File.separator + sRadioTyp + File.separator + sLsTyp );
			resultPath.mkdirs();
			String zeit = dfZeit.format( d ).substring( 0, 2 ) + dfZeit.format( d ).substring( 3, 5 ) + dfZeit.format( d ).substring( 6, 8 );
			String datum = dfDatum.format( d ).substring( 8, 10 ) + dfDatum.format( d ).substring( 3, 5 ) + dfDatum.format( d ).substring( 0, 2 );
			String filename = sFgNr + "_ANT_" + datum + "_" + zeit + ".DAT";
			FileOutputStream resultWriter = new FileOutputStream( resultPath + File.separator + filename, false );

			this.writeResultFileHeader( resultWriter, d );

			this.writeResultFile( resultWriter, anzahlAntennen, anzahlMesswerte, status, messwerte );

			resultWriter.close();

		} catch( Throwable e ) {
			if( isEnglish )
				throw new DeviceIOException( "Problem tracing result file: " + e.getMessage() );
			else
				throw new DeviceIOException( "Problem beim Tracen der Ergebnisse: " + e.getMessage() );
		}
	}

	/**
	 * Write the data header part to the result file.
	 * 
	 * @version 1.0 2002-13-12
	 * @author Rainer Merz
	 * @param fileWriter FileOutputStream that has to be written
	 * @exception DeviceIOException thrown if a problem with the Output Stream happened
	 * @exception DeviceParameterException thrown if result file is not writeable
	 */
	private void writeResultFileHeader( FileOutputStream fileWriter, Date d ) throws DeviceIOException {

		try {
			fileWriter.write( ("                        Ergebnis Antennenmessung\r\n").getBytes() );
			fileWriter.write( ("=========================================================================\r\n").getBytes() );
			fileWriter.write( ("Pr�fzeitpunkt           : " + dfDatum.format( d ) + ", " + dfZeit.format( d ) + "\r\n").getBytes() );
			fileWriter.write( ("Fahrzeuginformationen\r\n").getBytes() );
			fileWriter.write( ("  Fahrzeugidentifikation: " + sFgNr + "\r\n").getBytes() );
			fileWriter.write( ("  Baureihe              : " + sBaureihe + "\r\n").getBytes() );
			fileWriter.write( ("  Karosserieform        : " + sKarosserie + "\r\n").getBytes() );
			fileWriter.write( ("  Radiotyp              : " + sRadioTyp + "\r\n").getBytes() );
			fileWriter.write( ("  Lautsprechertyp       : " + sLsTyp + "\r\n").getBytes() );
			fileWriter.write( ("=========================================================================\r\n").getBytes() );
		} catch( Exception e ) {
			if( isEnglish )
				throw new DeviceIOException( "Problem with writing result file header: " + e.getMessage() );
			else
				throw new DeviceIOException( "Problem beim Schreiben des Kopfes der Ergebnisdatei: " + e.getMessage() );
		}
	}

	/**
	 * Write the data part to the result file.
	 * 
	 * @version 1.0 2002-13-12
	 * @author Rainer Merz
	 * @param fileWriter FileOutputStream that has to be written
	 * @exception DeviceIOException thrown if a problem with the Output Stream happened
	 * @exception DeviceParameterException thrown if result file is not writeable
	 */
	private void writeResultFile( FileOutputStream fileWriter, int anzahlAntennen, int anzahlMesswerte, int status[], int[] messwerte ) throws DeviceParameterException, DeviceIOException {
		int indAntenne;
		int i;
		int offset = 0;

		try {
			for( indAntenne = 0; indAntenne < anzahlAntennen; indAntenne++ ) {
				fileWriter.write( ("-------------------------------------------------------------------------\r\n").getBytes() );
				fileWriter.write( ("Antennentyp           : " + sAntennentyp[indAntenne] + "\r\n").getBytes() );
				fileWriter.write( ("Messfrequenz          : " + sMessfrequenz[indAntenne] + " kHz\r\n").getBytes() );
				fileWriter.write( ("Sollwert Max          : " + sToleranzValues[indAntenne] + "\r\n").getBytes() );

				// ### write the calculated tables to file
				fileWriter.write( "Messwerte             : ".getBytes() );
				for( i = offset; i < (offset + anzahlMesswerte); i++ ) {
					fileWriter.write( (String.valueOf( messwerte[i] )).getBytes() );
					if( (i % anzahlMesswerte) != (anzahlMesswerte - 1) )
						fileWriter.write( ", ".getBytes() );
				}
				offset += anzahlMesswerte;

				// ### change to next line
				fileWriter.write( "\r\n".getBytes() );
				fileWriter.write( "Ergebnis              : ".getBytes() );
				if( status[indAntenne] == STATUS_EXECUTION_OK )
					fileWriter.write( "IO".getBytes() );
				else
					fileWriter.write( "NIO".getBytes() );
				fileWriter.write( "\r\n".getBytes() );
				fileWriter.write( ("-------------------------------------------------------------------------\r\n").getBytes() );

				fileWriter.write( "\r\n".getBytes() );
				fileWriter.write( "\r\n".getBytes() );
			}
		} catch( Exception e ) {
			if( isEnglish )
				throw new DeviceIOException( "Problem with writing result file body: " + e.getMessage() );
			else
				throw new DeviceIOException( "Problem beim Schreiben der Daten der Ergebnisdatei: " + e.getMessage() );
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info Information zur Ausf�hrung modified 13.01.03
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;
		
		EdiabasProxyThread ed = null;

		// spezifische Variablen
		String sgbd = "";
		String sgbdFreq;
		String job;
		String jobpar;
		String temp;
		String AntRes;
		String resultName;
		String freqFM, freqAM;
		int ifreqFm = 0, ifreqAm = 0;
		int countFM = 0, countAM = 0, anzAntennen = 0;
		int i = 0;
		int indexAntenne = 0;
		int istMax = 0;
		int anzahlMessungen = 4;
		int[] tempResults;
		int[] results;
		int[] tolerances;
		int[] max_values;
		int[] sumStatus;
		int count = 0;
		int Error = 0;
		String errorText = "";
		int pauseNachSteuernFreq = 1000;
		int pauseNachSteuernAnt = 200;
		int pauseNachJobs = 100;
		int pauseZwischenMessungen = 100;
		int maximumValue = 9999;
		int numberRepetitionsStatusAnt = 1;
		
		boolean udInUse = false;

		String errorTextStatusAnt = null;
		
		//parameters to control diversity diagnosis
		boolean checkStatusEin = true;
		boolean ignoreStatusAnt = false;
		boolean continueFmDivDiagOnError = false;
		
		//jobnames
		String jobSetFrequency = "STEUERN_FREQUENZ";
		String jobStartDiversityDiagnosis = "STEUERN_ANT_EIGEN_DIAG";
		String jobStopDiversityDiagnosis = "STATUS_ANT_EIGEN_DIAG";
		String jobSwitchDiversityDiagnosis = "STEUERN_ANT_SCAN";
		String jobMeasureFieldStrength = "STATUS_ANT_QFS";
		//jobPars
		String jobParSetFrequency = "";
		String jobParStartDiversityDiagnosis = "";
		String jobParStopDiversityDiagnosis = "";
		String jobParSwitchDiversityDiagnosis = "";
		String jobParMeasureFieldStrength = "";

		// used fieldstrength
		String resultNameFieldstrength = "STAT_FIELDSTRENGTH";
		
		String status_ant_resultName = "STAT_ANT_EIGEN_DIAG";

		// if tracing is enabled
		boolean noTrace = false;

		int am_amp_difference_needed = 0;

		int resultNoAmAmplifier = 0;
		
		//to activate workarounds jobs
		boolean useJobBeforeFM=false;
		boolean useJobBeforeAM=false;
		String JobBeforeFM_Name="";
		String JobBeforeAM_Name="";
		String JobBeforeFM_Par="";
		String JobBeforeAM_Par="";
		int JobBeforeFM_Pause=0;
		int JobBeforeAM_Pause=0;
		
		UserDialog myDialog = null;
		StringTokenizer myTokenizer1=null,myTokenizer2 = null;
		
		boolean useUDS = false;
		
		boolean useAWT = true;
		String myAWT = "";
		if (isEnglish) myAWT = "Field Strength Antenna test running";
		else myAWT = "Feldst�rketest Antennen l�uft";

		//---------------------------------------------------------------check arguments and get
		// optional
		// ----------------------------------------------------------------------------------------------

		try {

			if( checkArgs() == false ) {
				result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			try {
				String sArg = "";
				sgbd = getArg( "SGBD" );
				freqFM = getArg( "FREQFM" );
				ifreqFm = Integer.parseInt( freqFM );
				if (ifreqFm<0 || ifreqFm > 110000) {
					if( isEnglish )
						throw new PPExecutionException( "Checkargs Error: FREQFM out of range [0;110000]" );
					else
						throw new PPExecutionException( "ParameterFehler: FREQFM ausserhalb [0;110000]" );
				}
				freqAM = getArg( "FREQAM" );
				ifreqAm = Integer.parseInt( freqAM );
				if (ifreqAm<150 || ifreqAm > 110000) {
					if( isEnglish )
						throw new PPExecutionException( "Checkargs Error: FREQAM out of range [150;110000]" );
					else
						throw new PPExecutionException( "ParameterFehler: FREQAM ausserhalb [150;110000]" );
				}
				countFM = Integer.parseInt( getArg( "COUNTFM" ) );
				countAM = Integer.parseInt( getArg( "COUNTAM" ) );
				anzAntennen = countFM + countAM;

				// check if diversity seldf diagnosis is used
				sArg = getArg( "CHECK_STATUS_ANT" );
				if( sArg != null ) {
					if( sArg.equalsIgnoreCase( "FALSE" ) )
						checkStatusEin = false;
				}

				// set pause after setting frequency
				sArg = getArg( "PAUSE_NACH_STEUERN_FREQ" );
				if( sArg != null ) {
					pauseNachSteuernFreq = Integer.parseInt( sArg );
					if (pauseNachSteuernFreq<0) {
						if( isEnglish )
							throw new PPExecutionException( "Checkargs Error: PAUSE_NACH_STEUERN_FREQ smaller than 0" );
						else
							throw new PPExecutionException( "ParameterFehler: PAUSE_NACH_STEUERN_FREQ kleiner als 0" );
					}
				}
				//  set pause after starting diversity self diagnosis
				sArg = getArg( "PAUSE_NACH_STEUERN_ANT" );
				if( sArg != null ) {
					pauseNachSteuernAnt = Integer.parseInt( sArg );
					if (pauseNachSteuernAnt<0) {
						if( isEnglish )
							throw new PPExecutionException( "Checkargs Error: PAUSE_NACH_STEUERN_ANT smaller than 0" );
						else
							throw new PPExecutionException( "ParameterFehler: PAUSE_NACH_STEUERN_ANT kleiner als 0" );
					}
				}

				// set pause after other diagnosis jobs
				sArg = getArg( "PAUSE_ANDERE_JOBS" );
				if( sArg != null ) {
					pauseNachJobs = Integer.parseInt( sArg );
					if (pauseNachJobs<0) {
						if( isEnglish )
							throw new PPExecutionException( "Checkargs Error: PAUSE_ANDERE_JOBS smaller than 0" );
						else
							throw new PPExecutionException( "ParameterFehler: PAUSE_ANDERE_JOBS kleiner als 0" );
					}
				}

				// set pauses between measurements
				sArg = getArg( "PAUSE_ZWISCHEN_MESSUNGEN" );
				if( sArg != null ) {
					pauseZwischenMessungen = Integer.parseInt( sArg );
					if (pauseZwischenMessungen<0) {
						if( isEnglish )
							throw new PPExecutionException( "Checkargs Error: PAUSE_ZWISCHEN_MESSUNGEN smaller than 0" );
						else
							throw new PPExecutionException( "ParameterFehler: PAUSE_ZWISCHEN_MESSUNGEN kleiner als 0" );
					}
				}

				// set minimum difference for am amplifier check, this activates the check
				sArg = getArg( "MIN_DIFF_AM_AMP_IN_DB" );
				if( sArg != null && countAM > 0 && (countFM > 0 || checkStatusEin) ) {
					am_amp_difference_needed = Integer.parseInt( sArg );
					if (am_amp_difference_needed<0) {
						if( isEnglish )
							throw new PPExecutionException( "Checkargs Error: MIN_DIFF_AM_AMP_IN_DB smaller than 0" );
						else
							throw new PPExecutionException( "ParameterFehler: MIN_DIFF_AM_AMP_IN_DB kleiner als 0" );
					}
				}

				// activates to use the exact result
				sArg = getArg( "USE_1DB_PRECISION" );
				if( sArg != null && sArg.equalsIgnoreCase( "TRUE" ) ) {
					resultNameFieldstrength = "STAT_FIELDSTRENGTH_EXACT";
				} else {
					am_amp_difference_needed = (int) Math.abs( Math.ceil( (am_amp_difference_needed / 4) ) );
				}

				//  used to deactivate the local tracing
				sArg = getArg( "NOTRACE" );
				if( sArg != null )
					noTrace = sArg.equalsIgnoreCase( "TRUE" );

				//  UDS profile
				sArg = getArg( "UDS" );
				if( sArg != null )
					useUDS = sArg.equalsIgnoreCase( "TRUE" );
				
				// other sgbd for changing frequencies
				sArg = getArg( "SGDBFREQ" );
				if( sArg != null )
					sgbdFreq = sArg;
				else
					sgbdFreq = sgbd;

				// get maximum value for antenna test
				sArg = getArg( "TOLERANCE_MAXIMUM" );
				if( sArg != null ) {
					maximumValue = Integer.parseInt( sArg );
					if (maximumValue<0) {
						if( isEnglish )
							throw new PPExecutionException( "Checkargs Error: TOLERANCE_MAXIMUM smaller than 0" );
						else
							throw new PPExecutionException( "ParameterFehler: TOLERANCE_MAXIMUM kleiner als 0" );
					}
				}

				tolerances = new int[anzAntennen];
				max_values = new int[anzAntennen];

				sAntennentyp = new String[anzAntennen];
				sToleranzValues = new String[anzAntennen];
				sMessfrequenz = new String[anzAntennen];
				sumStatus = new int[anzAntennen];
				
				int antennaTokens = 0;
				sArg = getArg( "ANTENNA_NAMES" );
				if (sArg != null) {
					myTokenizer1 = new StringTokenizer( sArg, ";" );
					antennaTokens = myTokenizer1.countTokens();
					if( antennaTokens > 0 && antennaTokens < anzAntennen) {
						if( isEnglish )
							throw new PPExecutionException( "Checkargs Error: ANTENNA_NAMES incorrect number tokens" );
						else
							throw new PPExecutionException( "ParameterFehler: ANTENNA_NAMES falsche Anzahl Werte" );
					}
				}
				
				int toleranceTokens = 0;
				sArg = getArg( "TOLERANCE_VALUES" );
				if (sArg != null) {
					myTokenizer2 = new StringTokenizer( sArg, ";" );
					toleranceTokens = myTokenizer2.countTokens();
					if( toleranceTokens > 0 && toleranceTokens < anzAntennen) {
						if( isEnglish )
							throw new PPExecutionException( "Checkargs Error: TOLERANCE_VALUES incorrect number tokens" );
						else
							throw new PPExecutionException( "ParameterFehler: TOLERANCE_VALUES falsche Anzahl Werte" );
					}
				}
				
				String token1=null, token2=null;
				for( i = 0; i < anzAntennen; i++ ) {
					if (antennaTokens>0) token1 = myTokenizer1.nextToken();
					if (toleranceTokens>0) token2 = myTokenizer2.nextToken();
					// antenna names
					sArg = getArg( "ANTENNENTYP" + (i + 1) );
					if( sArg != null ) {
						sAntennentyp[i] = sArg;
					} else {
						if (antennaTokens>0) sAntennentyp[i] = token1;
						else sAntennentyp[i] = "Antenne" + String.valueOf( i + 1 );
					}
					// tolerance values (mandatory)
					sArg = getArg( "TOLERANCE_VAL" + (i + 1) );
					if (sArg != null){
						if (sArg.startsWith("LESSOREQUAL(")) {
							sToleranzValues[i] = "0";
							tolerances[i] = 0;
							int endbracket = sArg.indexOf(")");
							max_values[i] = Integer.parseInt( sArg.substring(12,endbracket) );
						}
						else {
							sToleranzValues[i] = sArg;
							tolerances[i] = Integer.parseInt( sArg );
							max_values[i] = maximumValue;
						}
					}
					else {
						if (toleranceTokens>0) {
							if (token2.startsWith("LESSOREQUAL(")) {
								sToleranzValues[i] = "0";
								tolerances[i] = 0;
								int endbracket = token2.indexOf(")");
								max_values[i] = Integer.parseInt( token2.substring(12,endbracket) );
							}
							else {
								sToleranzValues[i] = token2;
								tolerances[i] = Integer.parseInt( token2 );
								max_values[i] = maximumValue;
							}
						}
						else { 
							tolerances[i] = -2;
						}
					}
					if (tolerances[i] < -1) {
						if( isEnglish )
							throw new PPExecutionException( "Checkargs Error: TOLERANCE_VALUE " + (i + 1)+" invalid" );
						else
							throw new PPExecutionException( "ParameterFehler: TOLERANCE_VALUE " + (i + 1)+" ung�ltig" );
					}
					
				}

				// get diversity self diagnosis error text
				sArg = getArg( "ERRORTEXT_STATUS_ANT" );
				if( sArg != null ) {
					errorTextStatusAnt = sArg;
				}
				// get number reptitions for diversity check
				sArg = getArg( "NUM_REPETITIONS_CHECK_STATUS_ANT" );
				if( sArg != null ) {
					int tempInt = Integer.parseInt( sArg );
					if (tempInt>=0 && tempInt <=10) {
						numberRepetitionsStatusAnt = tempInt;
					}
				}
				// ignore diversity self diagnosis
				sArg = getArg( "IGNORE_CHECK_STATUS_ANT" );
				if( sArg != null ) {
					ignoreStatusAnt = sArg.equalsIgnoreCase( "TRUE" );
				}
				
				// get number reptitions for diversity check
				sArg = getArg( "NUM_MEASURES_PER_ANTENNA" );
				if( sArg != null ) {
					int tempInt = Integer.parseInt( sArg );
					if (tempInt>0 && tempInt <=10) {
						anzahlMessungen = tempInt;
					}
				}
				// now define number of results
				results = new int[anzahlMessungen * anzAntennen];
				tempResults = new int[anzahlMessungen];
				
				// define jobnames
				sArg = getArg( "JOBNAME_SETFREQUENCY" );
				if (sArg == null && useUDS) sArg = "STEUERN";
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					jobSetFrequency = sArg;
				}
				sArg = getArg( "JOBNAME_START_DIVERSITY_DIAG" );
				if (sArg == null && useUDS) sArg = "STEUERN_ROUTINE";
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					jobStartDiversityDiagnosis = sArg;
				}
				sArg = getArg( "JOBNAME_STOP_DIVERSITY_DIAG" );
				if (sArg == null && useUDS) sArg = "STEUERN_ROUTINE";
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					jobStopDiversityDiagnosis = sArg;
				}
				sArg = getArg( "JOBNAME_SWITCH_DIVERSITY_DIAG" );
				if (sArg == null && useUDS) sArg = "STEUERN_ROUTINE";
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					jobSwitchDiversityDiagnosis = sArg;
				}
				sArg = getArg( "JOBNAME_MEASURE_FIELDSTRENGTH" );
				if (sArg == null && useUDS) sArg = "STATUS_LESEN";
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					jobMeasureFieldStrength = sArg;
				}
				
				// define jobpars
				sArg = getArg( "JOBPAR_SETFREQUENCY" );
				if (sArg == null && useUDS) sArg = "ARG;FREQUENZ";
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					jobParSetFrequency = sArg;
					//workaround Cascade
					if (!(jobParSetFrequency.endsWith(";"))) jobParSetFrequency += ";";
				}
				sArg = getArg( "JOBPAR_START_DIVERSITY_DIAG" );
				if (sArg == null && useUDS) sArg = "ARG;ANT_EIGEN_DIAG;STR";
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					jobParStartDiversityDiagnosis = sArg;
				}
				sArg = getArg( "JOBPAR_STOP_DIVERSITY_DIAG" );
				if (sArg == null && useUDS) sArg = "ARG;ANT_EIGEN_DIAG;RRR";
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					jobParStopDiversityDiagnosis = sArg;
				}
				sArg = getArg( "JOBPAR_SWITCH_DIVERSITY_DIAG" );
				if (sArg == null && useUDS) sArg = "ARG;STEUERN_ANT_SCAN;STR";
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					jobParSwitchDiversityDiagnosis = sArg;
					//workaround Cascade
					if (!(jobParSwitchDiversityDiagnosis.endsWith(";"))) jobParSwitchDiversityDiagnosis += ";";
				}
				sArg = getArg( "JOBPAR_MEASURE_FIELDSTRENGTH" );
				if (sArg == null && useUDS) sArg = "ARG;STATUS_ANT_QFS";
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					jobParMeasureFieldStrength = sArg;
				}
				
				sArg = getArg( "CONTINUE_FM_DIV_DIAG_ON_ERROR" );
				if( sArg != null  && sArg.length()>0 && !sArg.equalsIgnoreCase("null"))
					continueFmDivDiagOnError = sArg.equalsIgnoreCase( "TRUE" );
				
				// define workaround jobs
				sArg = getArg( "JOB_BEFORE_FM_NAME" );
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					useJobBeforeFM=true;
					JobBeforeFM_Name=sArg;
				}
				sArg = getArg( "JOB_BEFORE_AM_NAME" );
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					useJobBeforeAM=true;
					JobBeforeAM_Name=sArg;
				}
				sArg = getArg( "JOB_BEFORE_FM_PAR" );
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					JobBeforeFM_Par=sArg;
				}
				sArg = getArg( "JOB_BEFORE_AM_PAR" );
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					JobBeforeAM_Par=sArg;
				}
				sArg = getArg( "JOB_BEFORE_FM_PAUSE" );
				if( sArg != null ) {
					JobBeforeFM_Pause = Integer.parseInt( sArg );
					if (JobBeforeFM_Pause<0) {
						if( isEnglish )
							throw new PPExecutionException( "Checkargs Error: JOB_BEFORE_FM_PAUSE smaller than 0" );
						else
							throw new PPExecutionException( "ParameterFehler: JOB_BEFORE_FM_PAUSE kleiner als 0" );
					}
				}
				sArg = getArg( "JOB_BEFORE_AM_PAUSE" );
				if( sArg != null ) {
					JobBeforeAM_Pause = Integer.parseInt( sArg );
					if (JobBeforeAM_Pause<0) {
						if( isEnglish )
							throw new PPExecutionException( "Checkargs Error: JOB_BEFORE_AM_PAUSE smaller than 0" );
						else
							throw new PPExecutionException( "ParameterFehler: JOB_BEFORE_AM_PAUSE kleiner als 0" );
					}
				}
				sArg = getArg( "JOBRESULT_MEASURE_FIELDSTRENGTH" );
				if (sArg == null && useUDS) sArg = "STAT_FIELDSTRENGTH_WERT";
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					resultNameFieldstrength=sArg;
				}
				sArg = getArg( "JOBRESULT_CHECK_STATUS_ANT" );
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					status_ant_resultName=sArg;
				}
				
				sArg = getArg( "AWT" );
				if( sArg != null && sArg.length()>0 && (!(sArg.equalsIgnoreCase( "null")))) {
					myAWT=sArg;
					if (myAWT.equalsIgnoreCase("IGNORE")) useAWT=false;
				}
				
				
		} catch( Exception e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			if (useAWT) {
			    myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
				myDialog.displayMessage( PB.getString( "anweisung" ),myAWT, -1 );
				udInUse = true;
			}
			
			try {
				ed = getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();// new
			} catch( Exception e1 ) {
				if( isEnglish ) {
					throw new DeviceIOException( "error getting EDIABAS device: " + e1.getMessage() );
				} else {
					throw new DeviceIOException( "Fehler beim Holen des Device EDIABAS: " + e1.getMessage() );
				}
			}
			
			// ---------------------------------------------------------------------------------------------------------------------
			// Executing job before FM Diagnose (normally Workaround)
			if( useJobBeforeFM ) {
				job = JobBeforeFM_Name;
				jobpar = JobBeforeFM_Par;
				try {
					temp = ed.executeDiagJob( sgbdFreq, job, jobpar, "" );
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					} else {
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
				} catch( ApiCallFailedException e ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} catch( EdiabasResultNotFoundException e ) {
					if( e.getMessage() != null )
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );

					ergListe.add( result );
					throw new PPExecutionException();
				}

				// pause after execution
				try {
					Thread.sleep( JobBeforeFM_Pause );
				} catch( Exception e ) {
				}
			}
			

			// ---------------------------------------------------------------------------------------------------------------------
			// Setting FM frequency, also necessary for diversity diagnosis
			if( countFM > 0 || checkStatusEin ) {

				// Einstellen der Frequenz
				sAktMessfrequenz = freqFM; // FM-Messungen

				if (ifreqFm > 0) {
					job = jobSetFrequency;
					jobpar = jobParSetFrequency+sAktMessfrequenz;
	
					try {
						temp = ed.executeDiagJob( sgbdFreq, job, jobpar, "" );
						if( temp.equals( "OKAY" ) == false ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						} else {
							result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						}
					} catch( ApiCallFailedException e ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					} catch( EdiabasResultNotFoundException e ) {
						if( e.getMessage() != null )
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
						else
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
	
						ergListe.add( result );
						throw new PPExecutionException();
					}
	
					// "Kleine Pause nach STEUERN_FREQUENZ notwendig"
					try {
						Thread.sleep( pauseNachSteuernFreq );
					} catch( Exception e ) {
					}
				}
				// ---------------------------------------------------------------------------------------------------------------------
				// Antenneneigendiagnose starten
				if( checkStatusEin ) {
					do {
						job = jobStartDiversityDiagnosis;
						jobpar = jobParStartDiversityDiagnosis;
						try {
							temp = ed.executeDiagJob( sgbd, job, jobpar, "" );
							if( temp.equals( "OKAY" ) == false ) {
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								throw new PPExecutionException();
							} else {
								result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
							}
						} catch( ApiCallFailedException e ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						} catch( EdiabasResultNotFoundException e ) {
							if( e.getMessage() != null )
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							throw new PPExecutionException();
						}
	
						// Pause
						try {
							Thread.sleep( pauseNachSteuernAnt );
						} catch( Exception e ) {
						}
	
						// ----------------------------------------------------------------------------------------------------
						
						// Antenneneigendiagnose pr�fen
						job = jobStopDiversityDiagnosis;
						resultName = status_ant_resultName;
						jobpar = jobParStopDiversityDiagnosis;
						try {
							temp = ed.executeDiagJob( sgbd, job, jobpar, "" );
							if( temp.equals( "OKAY" ) == false ) {
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
								ergListe.add( result );
								if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
								else throw new PPExecutionException();
							} else {
								AntRes = ed.getDiagResultValue( resultName );
								result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, resultName, AntRes, "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
								ergListe.add( result );
								if( AntRes.equals( "0" ) == false ) {
									Error = 1;
									count++;
									
									// evaluate result
									if( count == (numberRepetitionsStatusAnt+1) ) {
										if( errorTextStatusAnt != null ) {
											errorText = errorTextStatusAnt;
										} else {
											if( isEnglish )
												errorText = "Failure in connection from diversity to antenna.";
											else
												errorText = "Fehlerhafte Verbindung vom Diversity zur Antenne.";
										}
										if (ignoreStatusAnt) {
											result = new Ergebnis( "AntFehler", "EDIABAS", sgbd, job, jobpar, resultName, AntRes, "0", "", "0", "", "", "", errorText, "", Ergebnis.FT_NIO );
											ergListe.add( result );
										}
										else {
											result = new Ergebnis( "AntFehler", "EDIABAS", sgbd, job, jobpar, resultName, AntRes, "0", "", "0", "", "", "", errorText, "", Ergebnis.FT_NIO );
											ergListe.add( result );
											if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
											else throw new PPExecutionException( errorText );
										}
									}
									else {
										// end antenna diagnosis in case of error  and repetition (not evaluated)
										job = jobSwitchDiversityDiagnosis;
										jobpar = jobParSwitchDiversityDiagnosis+"0";
										try {
											if (ed!= null && sgbd != null) {
												temp = ed.executeDiagJob( sgbd, job, jobpar, "" );
												if( temp.equals( "OKAY" ) == false ) {
													result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_IO );
													ergListe.add( result );
												}
											}
										} catch( ApiCallFailedException e1 ) {
											result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_IO );
											ergListe.add( result );
										} catch( EdiabasResultNotFoundException e1 ) {
											if( e1.getMessage() != null )
												result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e1.getMessage(), Ergebnis.FT_IO );
											else
												result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_IO );
											ergListe.add( result );
										}
										
										// kleine Pause
										try {
											Thread.sleep( pauseNachJobs );
										} catch( Exception e ) {
										}
										
										job = jobSetFrequency;
										jobpar = jobParSetFrequency+sAktMessfrequenz;
						
										try {
											temp = ed.executeDiagJob( sgbdFreq, job, jobpar, "" );
											if( temp.equals( "OKAY" ) == false ) {
												result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
												ergListe.add( result );
												throw new PPExecutionException();
											} else {
												result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
												ergListe.add( result );
											}
										} catch( ApiCallFailedException e ) {
											result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
											ergListe.add( result );
											throw new PPExecutionException();
										} catch( EdiabasResultNotFoundException e ) {
											if( e.getMessage() != null )
												result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
											else
												result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
						
											ergListe.add( result );
											throw new PPExecutionException();
										}
						
										// "Kleine Pause nach STEUERN_FREQUENZ notwendig"
										try {
											Thread.sleep( pauseNachSteuernFreq );
										} catch( Exception e ) {
										}
									}
								} else {
									Error = 0;
								}
							}
						} catch( ApiCallFailedException e ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
							ergListe.add( result );
							if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
							else throw new PPExecutionException();
						} catch( EdiabasResultNotFoundException e ) {
							if( e.getMessage() != null )
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
							else
								result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
							else throw new PPExecutionException();
						}
					} while( (count <= numberRepetitionsStatusAnt) && (Error == 1) );
					// Pause
					try {
						Thread.sleep( pauseNachJobs );
					} catch( Exception e ) {
					}
				}
			}
			// ---------------------------------------------------------------------------------------------------------
			// fm check
			for( indexAntenne = 0; indexAntenne < countFM; indexAntenne++ ) {
				sumStatus[indexAntenne] = STATUS_EXECUTION_OK;

				sMessfrequenz[indexAntenne] = sAktMessfrequenz;

				{
					//--------------------------------------------------------------------------------------------------------------------------------------
					// measurement
					job = jobMeasureFieldStrength;
					resultName = resultNameFieldstrength;
					jobpar = jobParMeasureFieldStrength;
					try {
						for( i = 0; i < anzahlMessungen; i++ ) {
							//only execute ediabas jobs when value is 0 or bigger
							if (tolerances[indexAntenne] >= 0) {
								temp = ed.executeDiagJob( sgbd, job, jobpar, "" );
								if( temp.equals( "OKAY" ) == false ) {
									result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
									ergListe.add( result );
									if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
									else throw new PPExecutionException();
								} else {
									tempResults[i] = (int) Double.parseDouble( ed.getDiagResultValue( resultName ) ); // Ergebnis
									// intern
									results[i + (indexAntenne * anzahlMessungen)] = tempResults[i]; // Ergebnis
									// intern
									result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, resultName, Integer.toString( tempResults[i] ), "", "", "", "", "", "", "", "", Ergebnis.FT_IO ); // Ergebnis
									// fuer
									// virtuelles
									// Fzg.
									ergListe.add( result );
								}
	
								// kleine Pause
								try {
									Thread.sleep( pauseZwischenMessungen );
								} catch( Exception e ) {
								}
							}
							else {
								tempResults[i] = 0;
								results[i + (indexAntenne * anzahlMessungen)] = 0;
							}
						}
					} catch( ApiCallFailedException e ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
						ergListe.add( result );
						if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
						else throw new PPExecutionException();
					} catch( EdiabasResultNotFoundException e ) {
						if( e.getMessage() != null )
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
						else
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
						else throw new PPExecutionException();
					}

					// -------------------------------------------------------------------------------------------
					/*
					 * FM Analyse von Ergebnissen: Sort array into ascending numerical order. take
					 * highest and check against Tolerance
					 */
					Arrays.sort( tempResults );
					istMax = tempResults[anzahlMessungen - 1];

					if( istMax > max_values[indexAntenne] ) {
						result = new Ergebnis( "Analyse", "MindestMax", "", "", "", sAntennentyp[indexAntenne], Integer.toString( istMax ), Integer.toString( tolerances[indexAntenne] ), Integer.toString( max_values[indexAntenne] ), "0", "", "", "", PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
						status = STATUS_EXECUTION_ERROR;
						sumStatus[indexAntenne] = STATUS_EXECUTION_ERROR;
					} else {

						if( istMax >= tolerances[indexAntenne] )
							result = new Ergebnis( "Analyse", "MindestMax", "", "", "", sAntennentyp[indexAntenne], Integer.toString( istMax ), Integer.toString( tolerances[indexAntenne] ), Integer.toString( max_values[indexAntenne] ), "0", "", "", "", "", "", Ergebnis.FT_IO );
						else {
							result = new Ergebnis( "Analyse", "MindestMax", "", "", "", sAntennentyp[indexAntenne], Integer.toString( istMax ), Integer.toString( tolerances[indexAntenne] ), Integer.toString( max_values[indexAntenne] ), "0", "", "", "", PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
							status = STATUS_EXECUTION_ERROR;
							sumStatus[indexAntenne] = STATUS_EXECUTION_ERROR;
						}
					}
					ergListe.add( result );
				}

				//-----------------------------------------------------------------------------------------------------------------------------------------
				// auf n�chste FM-Antenne schalten
				if( indexAntenne < (countFM - 1) ) {
					job = jobSwitchDiversityDiagnosis;
					jobpar = jobParSwitchDiversityDiagnosis+"1";
					try {
						temp = ed.executeDiagJob( sgbd, job, jobpar, "" );
						if( temp.equals( "OKAY" ) == false ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
							else throw new PPExecutionException();
						} else {
							result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						}
					} catch( ApiCallFailedException e ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
						ergListe.add( result );
						if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
						else throw new PPExecutionException();
					} catch( EdiabasResultNotFoundException e ) {
						if( e.getMessage() != null )
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
						else
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
						else throw new PPExecutionException();
					}
				}

				// kleine Pause
				try {
					Thread.sleep( pauseNachJobs );
				} catch( Exception e ) {
				}
				//-----------------------------------------------------------------------------------------------------------------------------------------
			}

			// -------------------------------------------------------------------------------------------------------------------------
			// Testing AM for amp difference
			if( am_amp_difference_needed > 0 ) {
				// ---------------------------------------------------------------------------------------------------------------------
				// Executing job before AM Diagnose (normally Workaround)
				if( useJobBeforeAM ) {
					job = JobBeforeAM_Name;
					jobpar = JobBeforeAM_Par;
					try {
						temp = ed.executeDiagJob( sgbdFreq, job, jobpar, "" );
						if( temp.equals( "OKAY" ) == false ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						} else {
							result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						}
					} catch( ApiCallFailedException e ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					} catch( EdiabasResultNotFoundException e ) {
						if( e.getMessage() != null )
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
						else
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );

						ergListe.add( result );
						throw new PPExecutionException();
					}

					// pause after execution
					try {
						Thread.sleep( JobBeforeAM_Pause );
					} catch( Exception e ) {
					}
				}
				//-----------------------------------------------------------------------------------------------------------------------------------------
				// auf AM-Antenne schalten
				sAktMessfrequenz = freqAM;
				job = jobSetFrequency;
				jobpar = jobParSetFrequency+sAktMessfrequenz;
				sMessfrequenz[indexAntenne] = sAktMessfrequenz;
				sumStatus[indexAntenne] = STATUS_EXECUTION_OK;
				try {
					temp = ed.executeDiagJob( sgbdFreq, job, jobpar, "" );
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
						else throw new PPExecutionException();
					} else {
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
				} catch( ApiCallFailedException e ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
					ergListe.add( result );
					if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
					else throw new PPExecutionException();
				} catch( EdiabasResultNotFoundException e ) {
					if( e.getMessage() != null )
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
					else throw new PPExecutionException();
				}

				// "Kleine Pause nach STEUERN_FREQUENZ notwendig"
				try {
					Thread.sleep( pauseNachSteuernFreq );
				} catch( Exception e ) {
				}

				// ----------------------------------------------------------------------------------------------------------------------------------------
				// AM measurement

				job = jobMeasureFieldStrength;
				resultName = resultNameFieldstrength;
				jobpar = jobParMeasureFieldStrength;
				try {
					for( i = 0; i < anzahlMessungen; i++ ) {
						temp = ed.executeDiagJob( sgbd, job, jobpar, "" );
						if( temp.equals( "OKAY" ) == false ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
							else throw new PPExecutionException();
						} else {
							tempResults[i] = (int) Double.parseDouble( ed.getDiagResultValue( resultName ) ); // Ergebnis
							// intern
							result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, resultName, Integer.toString( tempResults[i] ), "", "", "", "", "", "", "", "", Ergebnis.FT_IO ); // Ergebnis
							// fuer
							// virtuelles
							// Fzg.
							ergListe.add( result );
						}

						// kleine Pause
						try {
							Thread.sleep( pauseZwischenMessungen );
						} catch( Exception e ) {
						}
					}
				} catch( ApiCallFailedException e ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
					ergListe.add( result );
					if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
					else throw new PPExecutionException();
				} catch( EdiabasResultNotFoundException e ) {
					if( e.getMessage() != null )
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
					else throw new PPExecutionException();
				}

				// ---------------------------------------------------------------------------------------------------------
				//Auswertung
				try {
					Arrays.sort( tempResults );
					resultNoAmAmplifier = tempResults[anzahlMessungen - 1];
				} catch( Exception e ) {
					result = new Ergebnis( "AnalyseFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
					else throw new PPExecutionException();
				}
			}

			// ----------------------------------------------------------------------------------------------------------
			// ALWAYS NEEDED IF IN DIVERSITY DIAGNOSIS MODE
			if( countFM > 0 && checkStatusEin ) {
				job = jobSwitchDiversityDiagnosis;
				jobpar = jobParSwitchDiversityDiagnosis+"0";
				try {
					temp = ed.executeDiagJob( sgbd, job, jobpar, "" );
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
						else throw new PPExecutionException();
					} else {
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
				} catch( ApiCallFailedException e ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
					ergListe.add( result );
					if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
					else throw new PPExecutionException();
				} catch( EdiabasResultNotFoundException e ) {
					if( e.getMessage() != null )
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					if (continueFmDivDiagOnError) status = STATUS_EXECUTION_ERROR;
					else throw new PPExecutionException();
				}

				// kleine Pause
				try {
					Thread.sleep( pauseNachJobs );
				} catch( Exception e ) {
				}

			}

			// ----------------------------------------------------------------------------------------------------------
			// set AM frequency if not already set
			if( am_amp_difference_needed == 0 && countAM > 0 ) {
				// ---------------------------------------------------------------------------------------------------------------------
				// Executing job before AM Diagnose (normally Workaround)
				if( useJobBeforeAM ) {
					job = JobBeforeAM_Name;
					jobpar = JobBeforeAM_Par;
					try {
						temp = ed.executeDiagJob( sgbdFreq, job, jobpar, "" );
						if( temp.equals( "OKAY" ) == false ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						} else {
							result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						}
					} catch( ApiCallFailedException e ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					} catch( EdiabasResultNotFoundException e ) {
						if( e.getMessage() != null )
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
						else
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );

						ergListe.add( result );
						throw new PPExecutionException();
					}

					// pause after execution
					try {
						Thread.sleep( JobBeforeAM_Pause );
					} catch( Exception e ) {
					}
				}
				// auf AM-Antenne schalten
				sAktMessfrequenz = freqAM; // AM-Messung
				job = jobSetFrequency;
				jobpar = jobParSetFrequency+sAktMessfrequenz;
				sMessfrequenz[indexAntenne] = sAktMessfrequenz;
				sumStatus[indexAntenne] = STATUS_EXECUTION_OK;

				try {
					temp = ed.executeDiagJob( sgbdFreq, job, jobpar, "" );
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					} else {
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
				} catch( ApiCallFailedException e ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} catch( EdiabasResultNotFoundException e ) {
					if( e.getMessage() != null )
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}

				// "Kleine Pause nach STEUERN_FREQUENZ notwendig"
				try {
					Thread.sleep( pauseNachSteuernFreq );
				} catch( Exception e ) {
				}

			}

			//--------------------------------------------------------------------------------------------------------------------------------------
			// standard AM - Measurement
			if( countAM > 0 ) {
				job = jobMeasureFieldStrength;
				resultName = resultNameFieldstrength;
				jobpar = jobParMeasureFieldStrength;
				try {
					for( i = 0; i < anzahlMessungen; i++ ) {
						temp = ed.executeDiagJob( sgbd, job, jobpar, "" );
						if( temp.equals( "OKAY" ) == false ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							throw new PPExecutionException();
						} else {
							tempResults[i] = (int) Double.parseDouble( ed.getDiagResultValue( resultName ) ); // Ergebnis
							// intern
							results[i + (indexAntenne * anzahlMessungen)] = tempResults[i]; // Ergebnis
							// intern
							result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobpar, resultName, Integer.toString( tempResults[i] ), "", "", "", "", "", "", "", "", Ergebnis.FT_IO ); // Ergebnis
							// fuer
							// virtuelles
							// Fzg.
							ergListe.add( result );
						}

						// kleine Pause
						try {
							Thread.sleep( pauseZwischenMessungen );
						} catch( Exception e ) {
						}
					}
				} catch( ApiCallFailedException e ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} catch( EdiabasResultNotFoundException e ) {
					if( e.getMessage() != null )
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}
			}

			// ------------------------------------------------------------------------------------------------------
			/*
			 * AM Analyse von Ergebnissen: Sort array into ascending numerical order. take highest
			 * and check against Tolerance
			 */

			Arrays.sort( tempResults );
			istMax = tempResults[anzahlMessungen - 1];

			// analyse am difference
			if( am_amp_difference_needed > 0 ) {
				if( istMax >= (resultNoAmAmplifier + am_amp_difference_needed) )
					if( isEnglish )
						result = new Ergebnis( "Analyse", "AM-Amplifier", "", "", "", "AM Fieldstrength difference", Integer.toString( istMax - resultNoAmAmplifier ), Integer.toString( am_amp_difference_needed ), "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					else
						result = new Ergebnis( "Analyse", "AM-Verst�rker", "", "", "", "AM Feldst�rken Unterschied", Integer.toString( istMax - resultNoAmAmplifier ), Integer.toString( am_amp_difference_needed ), "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				else {
					if( isEnglish )
						result = new Ergebnis( "Analyse", "AM-Amplifier", "", "", "", "AM Fieldstrength difference", Integer.toString( istMax - resultNoAmAmplifier ), Integer.toString( am_amp_difference_needed ), "", "0", "", "", "", PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
					else
						result = new Ergebnis( "Analyse", "AM-Verst�rker", "", "", "", "AM Feldst�rken Unterschied", Integer.toString( istMax - resultNoAmAmplifier ), Integer.toString( am_amp_difference_needed ), "", "0", "", "", "", PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
					status = STATUS_EXECUTION_ERROR;
					ergListe.add( result );
				}
			}
			// am result creation
			if( countAM > 0 ) {
				if( istMax > max_values[indexAntenne] ) {
					result = new Ergebnis( "Analyse", "MindestMax", "", "", "", sAntennentyp[indexAntenne], Integer.toString( istMax ), Integer.toString( tolerances[indexAntenne] ), Integer.toString( max_values[indexAntenne] ), "0", "", "", "", PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
					status = STATUS_EXECUTION_ERROR;
					sumStatus[indexAntenne] = STATUS_EXECUTION_ERROR;
				} else {
					if( istMax >= tolerances[indexAntenne] )
						result = new Ergebnis( "Analyse", "MindestMax", "", "", "", sAntennentyp[indexAntenne], Integer.toString( istMax ), Integer.toString( tolerances[indexAntenne] ), Integer.toString( max_values[indexAntenne] ), "0", "", "", "", "", "", Ergebnis.FT_IO );
					else {
						result = new Ergebnis( "Analyse", "MindestMax", "", "", "", sAntennentyp[indexAntenne], Integer.toString( istMax ), Integer.toString( tolerances[indexAntenne] ), Integer.toString( max_values[indexAntenne] ), "0", "", "", "", PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
						status = STATUS_EXECUTION_ERROR;
						sumStatus[indexAntenne] = STATUS_EXECUTION_ERROR;
					}
				}
				ergListe.add( result );
			}

			sKarosserie = getArg( "KAROSSERIE" );
			if( sKarosserie == null )
				sKarosserie = getPr�fling().getAuftrag().getBaureihe();
			sRadioTyp = getArg( "RADIOTYP" );
			if( sRadioTyp == null )
				sRadioTyp = getPr�fling().getAttribut("SGBD");
			sLsTyp = getArg( "LSTYP" );
			if( sLsTyp == null )
				sLsTyp = getPr�fling().getAttribut("LS_TYP");
			if( (sKarosserie != null) && (sRadioTyp != null) && (sLsTyp != null) && !(noTrace) ) {
				// alle Parameter zum Schreiben des Trace Files vorhanden

				sFgNr = getPr�fling().getAuftrag().getFahrgestellnummer7();
				sBaureihe = getPr�fling().getAuftrag().getBaureihe();

				//Ergebnis in Datei schreiben
				try {
					traceResults( anzAntennen, anzahlMessungen, sumStatus, results );
				} catch( Exception e ) {
					if( e instanceof DeviceIOException ) {
						errorText = "DeviceIOException";
						result = new Ergebnis( "ExecFehler", "Antennentest", "", "", "", "", "", "", "", "0", "", "", "", errorText, "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
					} else if( e instanceof DeviceParameterException ) {
						errorText = "DeviceParameterException";
						result = new Ergebnis( "ExecFehler", "Antennentest", "", "", "", "", "", "", "", "0", "", "", "", errorText, "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
					} else {
						if( isEnglish )
							errorText = "Error locally tracing results";
						else
							errorText = "Fehler beim lokalen Tracen der Ergebnisse";
						result = new Ergebnis( "ExecFehler", "Antennentest", "", "", "", "", "", "", "", "0", "", "", "", "Exception (diagradio)", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
					}
					ergListe.add( result );
					throw new PPExecutionException( errorText );
				}

			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
			e.printStackTrace();
			// end self diagnosis in case of error
			if( checkStatusEin && sgbd != null && sgbd.equalsIgnoreCase( "" ) == false) {
				job = jobSwitchDiversityDiagnosis;
				jobpar = jobParSwitchDiversityDiagnosis+"0";
				try {
					if (ed!= null && sgbd != null) {
						temp = ed.executeDiagJob( sgbd, job, jobpar, "" );
						if( temp.equals( "OKAY" ) == false ) {
							result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
						}
					}
				} catch( ApiCallFailedException e1 ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ed.apiErrorCode() + ": " + ed.apiErrorText(), Ergebnis.FT_NIO );
					ergListe.add( result );
				} catch( EdiabasResultNotFoundException e1 ) {
					if( e.getMessage() != null )
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
					else
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobpar, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
				}
			}
			e.printStackTrace();
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ) + ": " + e.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			e.printStackTrace();
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ) + ": " + e.getMessage(), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			e.printStackTrace();
		}
		
		//Freigabe der benutzten Devices
        if( udInUse == true ) {
            try {
                getPr�flingLaufzeitUmgebung().releaseUserDialog();
            } catch (Exception e) {
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "DeviceLockedException", Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "ausfuehrungsfehler" ), "Exception: "+e.getMessage(), Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                status = STATUS_EXECUTION_ERROR;
            }
        }

		setPPStatus( info, status, ergListe );
	} // End of execute()
} // End of Class


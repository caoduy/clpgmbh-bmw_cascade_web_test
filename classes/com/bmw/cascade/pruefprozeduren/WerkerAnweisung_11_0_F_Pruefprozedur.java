/*
 * WerkerAnweisung_8_0_F_Pruefprozedur.java
 *
 * Created on 14.09.00
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;

/**
 * Implementierung der Pr�fprozedur, die f�r eine vorgegeben Zeitdauer eine Message an den Werker ausgibt
 * @author Winkler
 * @version Implementierung
 * @version 8_0_T	16.07.2007  Bauermann, optionaler Parameter FENSTERNAME hinzu um Anweisungsfenster zu benennen
 * @version 9_0_F	17.07.2007  Gebauer F-Version
 * @version 10_0_T  02.08.2012  F. Schoenert: @ Referenz Operator bei den zwing. Argumenten "DAUER" und "SWT" eingebaut
 * @version 11_0_F  22.11.2012  F-Version
 */
public class WerkerAnweisung_11_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public WerkerAnweisung_11_0_F_Pruefprozedur() {
	}

	// Variablen Definition
	String strFensterName = "";

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public WerkerAnweisung_11_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "STYLE", "FENSTERNAME" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "DAUER", "AWT" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 */
	public boolean checkArgs() {
		boolean ok;

		//Neu: Fenster-Name als optionales Argument
		this.strFensterName = getArg( getOptionalArgs()[1] );

		try {
			ok = super.checkArgs();
			if( ok == true ) {
				try {
					if( (Long.parseLong( (extractValues( getArg( getRequiredArgs()[0] ) ))[0] )) < 0 )
						return false;
				} catch( NumberFormatException e ) {
					return false;
				}
			}
			return ok;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String awt;
		long timestamp, deltatime;
		int style;
		boolean udInUse = false;

		/***********************************************
		 * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
		 * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		 ***********************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		try {
			//Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				try {
					deltatime = Long.parseLong( extractValues( getArg( getRequiredArgs()[0] ) )[0] );

				} catch( NumberFormatException e ) {
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				}
				awt = PB.getString( extractValues( getArg( getRequiredArgs()[1] ) )[0] );
				if( getArg( getOptionalArgs()[0] ) == null )
					style = 99;
				else
					style = Integer.parseInt( getArg( getOptionalArgs()[0] ) );

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			try {
				if( style == 99 )
					if( strFensterName == null ) {
						getPr�flingLaufzeitUmgebung().getUserDialog().displayMessage( PB.getString( "anweisung" ), awt, -1 );
					} else {
						getPr�flingLaufzeitUmgebung().getUserDialog().displayMessage( this.strFensterName, awt, -1 );
					}
				else if( style == 1 )
					if( strFensterName == null ) {
						getPr�flingLaufzeitUmgebung().getUserDialog().displayStatusMessage( PB.getString( "anweisung" ), awt, -1 );
					} else {
						getPr�flingLaufzeitUmgebung().getUserDialog().displayStatusMessage( this.strFensterName, awt, -1 );
					}
				else if( style == 2 )
					if( strFensterName == null ) {
						getPr�flingLaufzeitUmgebung().getUserDialog().displayAlertMessage( PB.getString( "anweisung" ), awt, -1 );
					} else {
						getPr�flingLaufzeitUmgebung().getUserDialog().displayAlertMessage( this.strFensterName, awt, -1 );
					}
				else if( strFensterName == null ) {
					getPr�flingLaufzeitUmgebung().getUserDialog().displayUserMessage( PB.getString( "anweisung" ), awt, -1 );
				} else {
					getPr�flingLaufzeitUmgebung().getUserDialog().displayUserMessage( this.strFensterName, awt, -1 );
				}

				udInUse = true;
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			timestamp = System.currentTimeMillis() + deltatime;
			while( deltatime > 0 ) {
				try {
					Thread.sleep( deltatime );
				} catch( InterruptedException e ) {
				}
				deltatime = timestamp - System.currentTimeMillis();
			}
			result = new Ergebnis( "Status", "UserDialog", "", "", "", "", "", "", "", "0", "", "", awt, "", "", Ergebnis.FT_IO );
			ergListe.add( result );

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		if( udInUse == true ) {
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		//Status setzen
		setPPStatus( info, status, ergListe );

	}
}

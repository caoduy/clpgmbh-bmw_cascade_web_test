package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.psdz.PSdZ;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.pruefstand.devices.psdz.data.*;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.CascadePSdZArgumentException;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.CascadePSdZConditionsNotCorrectException;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.CascadePSdZRuntimeException;

/**
 * Implementierung einer Pr�fprozedur, welche die Bestellsachnummer 
 * eines Steuerger�ts ermittelt und den Werker dann auffordert, dieses 
 * einzulegen. Ferner kann die Pr�fprozedur pr�fen, ob das korrekte Steuerger�t 
 * eingelegt wurde. Beide Funktionen sind optional, es muss jedoch mindestens 
 * eine von beiden (oder beide zusammen) verwendet werden. 
 *
 * Es wird ein zwingend erforderliches Argument erwartet:
 *
 * ECU_ADDRESS: Beinhaltet die Steuerger�teadresse desjenigen Steuerger�tes
 * (in hex ohne "h" o. �.), dessen Bestellsachnummer ermittelt werden soll bzw.  
 * dessen korrektes Einlegen zu �berpr�fen ist. Hinweis: Es k�nnen auch die 
 * Nummern von mehreren Steuerger�ten ermittelt werden, in diesem Fall sind 
 * Steuerger�teadressen Semikolon getrennt zu spezifizieren.
 *
 * Folgende optionale Argumente sind verf�gbar:
 *
 * SHOW_PART_NUMBER: Aktiviert (bei TRUE) die Ermittlung und Anzeige der 
 * Bestellsachnummer. Falls dieses Argument nicht verwendet wird, dann muss 
 * wenigstens das Argument SGBM_LIMIT verwendet werden. 
 *
 * SGBM_LIMIT: Falls dieses Argument angeben ist, wird eine TAL generiert
 * und gepr�ft, ob die Anzahl der SGBMs darin kleiner gleich dem hier
 * spezifizierten Wert ist. Einschr�nkung: Es wird stets nur das erste im
 * Argument ECU_ADDRESS spezifizierte Steuerger�t betrachtet. Hinweis: Es
 * ist eine Fahrzeugverbindung erforderlich und es muss zuvor (�ber einen 
 * separaten Pr�fschritt) eine SVT-Ist erzeugt werden! Falls dieses Argument 
 * nicht verwendet wird, dann muss wenigstens das Argument SHOW_PART_NUMBER 
 * verwendet werden.
 *
 * SGBM_LIMIT_CATEGORIES: Liste mit Kategorien (getrennt durch Semikolon), 
 * welche im Rahmen des Arguments SGBM_LIMIT zu ber�cksichtigen sind (z. B.
 * blFlash, cdDeploy, hwDeinstall, hwInstall etc.). Die Gro�-/Kleinschreibung 
 * spielt keine Rolle. Wenn SGBM_LIMIT_CATEGORIES nicht gesetzt ist, bezieht 
 * SGBM_LIMIT alle Kategorien ein.    
 *
 * USE_SVT_SOLL: Wenn TRUE, dann wird das Erzeugen einer SVT-Soll au�erhalb 
 * der Pr�fprozedur durchgef�hrt, andernfalls �bernimmt dies die Pr�fprozedur
 * selbst. Default ist FALSE (= Erzeugung durch Pr�fprozedur).
 *
 * DEBUG: Aktiviert (bei TRUE) das Schreiben von Debug-Ausgaben.
 *
 * @author Peter Rettig, GEFASOFT GmbH, Dieter Busetti, GEFASOFT AG, Thomas Buboltz BMW AG <BR> 
 *         27.04.2007 TB Fehlertexte korrigiert <BR>
 *         14.05.2007 PR Ermittlung und Anzeige der Bestellsachnummer sind nun optional (SHOW_PART_NUMBER) <BR>
 *         03.01.2008 DB Einbau "testScreenID" in Debug-Ausgaben <BR>
 *         22.02.2008 PR Einbau der Argumente SGBM_LIMIT_CATEGORIES und USE_SVT_SOLL <BR>
 *         23.02.2008 TB kleiner BugFix TAL Filter bei Verwendung des Argumentes SGBM_LIMIT_CATEGORIES <BR>
 *         24.06.2008 DB �nderung holen des "testScreenID", Ersetzen deprecated-Methode 'psdz.generateAndStoreSollverbauungTAL(...)' durch 'psdz.generateAndStoreTAL(...)' <BR>
 *         05.02.2012 TB LOP Punkt 1436: Neben der Elektroniksachnummer zus�tzlich wenn m�glich noch die bestellbare Sachnummer anzeigen. LOP Punkt 1437: Fehlermeldung �ndern "Bitte richtiges SG einlegen" -> "Bitte SG �berpr�fen"  <BR>
 *         20.04.2013 TB Neue Methode beim generateTAL keinen SWT Status ermitteln und generics, T-Version <BR>
 *         20.04.2013 TB F-Version <BR>
 */
public class PSdZCheckEcu_11_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * Default-Konstruktor, nur fuer die Deserialisierung
	 */
	public PSdZCheckEcu_11_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSdZCheckEcu_11_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG", "SHOW_PART_NUMBER", "SGBM_LIMIT", "SGBM_LIMIT_CATEGORIES", "USE_SVT_SOLL", "DEBUG_PERFORM" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = { "ECU_ADDRESS" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		if( !super.checkArgs() )
			return false;

		// Stelle sicher, dass von den Arguementen SHOW_PART_NUMBER und  
		// SGBM_LIMIT mindestens eines verwendet wird (beide gemeinsam w�re 
		// auch zul�ssig).
		if( getArg( "SHOW_PART_NUMBER" ) == null && getArg( "SGBM_LIMIT" ) == null )
			return false;

		return true;
	}

	/** Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		PSdZ psdz = null;
		UserDialog userDialog = null;
		String testScreenID = this.getPr�flingLaufzeitUmgebung().getName(); // TestScreenID (aus Pr�fstandsinstanz)
		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
		boolean bDebugPerform = true; // gibt an, ob eine Performencemessung erfolgen sollen

		String[] ecuAddresses; // enth�lt die �bergebenen Steuerger�teadressen (in hex. ohne "h" o. �.)
		HashMap<String, String> sachNummern = new HashMap<String, String>(); // ordnet den Steuerger�teadressen (=Schl�ssel) die im Laufe der Ausf�hrung ermittelten Bestellsachnummern (=Werte) zu
		boolean showPartNumber = false; //Angabe, ob die Bestellsachnummer ermittelt und angezeigt werden soll
		int sgbmLimit = -1; // maximal erlaubte SGBMs, falls man eine Transaktion ansto�en w�rde (-1 = nicht �berpr�fen)
		String[] sgmbLimitCategories = null; // Kategorieren, die im Rahmen der SGBM-Limit-Pr�fung ber�cksichtigt werden sollen
		boolean usePreviousGeneratedSVTSoll = false; // Angabe, ob eine zuvor erzeugte SVT-Soll zu verwenden ist (true), andernfalls (false) generieren wir eine eigene 

		final boolean DE = checkDE(); // Systemsprache DE wenn true

		try {
			// Argumente einlesen und checken.
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG.
				if( getArg( "DEBUG" ) != null )
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( !getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );

				// DEBUG_PERFORM
				if( getArg( "DEBUG_PERFORM" ) != null ) {
					if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "TRUE" ) )
						bDebugPerform = true;
					else if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "FALSE" ) )
						bDebugPerform = false;
					else if( !getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "FALSE" ) )
						throw new PPExecutionException( "DEBUG_PERFORM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// ECU_ADDRESS.
				ecuAddresses = splitArg( getArg( "ECU_ADDRESS" ) );

				// SHOW_PART_NUMBER.
				if( getArg( "SHOW_PART_NUMBER" ) != null )
					if( getArg( "SHOW_PART_NUMBER" ).equalsIgnoreCase( "TRUE" ) )
						showPartNumber = true;
					else if( !getArg( "SHOW_PART_NUMBER" ).equalsIgnoreCase( "FALSE" ) )
						throw new PPExecutionException( "SHOW_PART_NUMBER " + PB.getString( "psdz.parameter.ungueldigerWert" ) );

				// SGBM_LIMIT.
				if( getArg( "SGBM_LIMIT" ) != null )
					try {
						sgbmLimit = Integer.parseInt( getArg( "SGBM_LIMIT" ) );
					} catch( NumberFormatException e ) {
						throw new PPExecutionException( "SGBM_LIMIT " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
					}

				// SGBM_LIMIT_CATEGORIES.
				if( getArg( "SGBM_LIMIT_CATEGORIES" ) != null )
					sgmbLimitCategories = splitArg( getArg( "SGBM_LIMIT_CATEGORIES" ) );

				// USE_SVT_SOLL.
				if( getArg( "USE_SVT_SOLL" ) != null )
					if( getArg( "USE_SVT_SOLL" ).equalsIgnoreCase( "TRUE" ) )
						usePreviousGeneratedSVTSoll = true;
					else if( !getArg( "USE_SVT_SOLL" ).equalsIgnoreCase( "FALSE" ) )
						throw new PPExecutionException( "USE_SVT_SOLL " + PB.getString( "psdz.parameter.ungueldigerWert" ) );

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			// Performancemessung Start
			if( bDebugPerform )
				System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZCheckEcu PP START" );

			// PSdZ-Device holen.
			try {
				psdz = getPr�flingLaufzeitUmgebung().getDeviceManager().getPSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Dialog holen.
			try {
				userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			try {
				// Beziehe, falls gew�nscht, zu den �bergebenen Steuerger�te-
				// adressen nun schrittweise alle Bestellsachnummern und zeige
				// diese an.
				if( showPartNumber ) {
					// Beziehe Bestellsachnummern.
					CascadeLogistischesTeil teil;

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZCheckEcu 'psdz.generateAndStoreSollverbauungRoh' START" );
					}

					if( !usePreviousGeneratedSVTSoll ) //Keine eigene SVT-Soll erzeugen?  
						psdz.generateAndStoreSollverbauungRoh();

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZCheckEcu 'psdz.generateAndStoreSollverbauungRoh' ENDE" );
					}

					for( int i = 0; i < ecuAddresses.length; i++ ) { // gehe �bergebene SG-Liste durch
						teil = psdz.getLogistischesTeil( ecuAddresses[i] );
						if( teil != null )
							sachNummern.put( ecuAddresses[i], teil.getSachNrTAIS().equals( teil.getSachNrBestellbaresTeilTAIS() ) ? teil.getSachNrTAIS() : teil.getSachNrTAIS() + " [" + teil.getSachNrBestellbaresTeilTAIS() + "]" ); //Nur wenn die Nummern unterschiedlich sind, sollen beide gemerkt und ausgegeben werden
					}

					if( bDebug )
						System.out.println( "PSdZ ID:" + testScreenID + ", PSdZCheckEcu, Ausgabe der gefundenen Bestellsachnummern: " + sachNummern );

					// Fordere den Werker auf, ein Steuerger�t mit der er-
					// mittelten Bestellsachnummer einzulegen.
					String[] values = (String[]) sachNummern.values().toArray( new String[0] ); // beziehe die Sachnummern zur sp�teren Anzeige im Dialog
					Arrays.sort( values ); // bereite die Sachnummern zur Anzeige im Dialog auf
					StringBuffer buffer = new StringBuffer();
					for( int i = 0; i < values.length; i++ ) { // speichere die sortierten Sachnummern Komma-separiert in einem String-Buffer
						if( i != 0 )
							buffer.append( ", " );
						buffer.append( values[i] );
					}

					if( buffer.length() == 0 ) // Wurden �berhaupt Sachnummer gefunden?
						buffer.append( PB.getString( "psdz.text.NichtErmittelbar" ) );

					userDialog.requestStatusMessage( PB.getString( "anweisung" ), PB.getString( "psdz.text.SgEinlegen" ) + " " + buffer.toString(), 0 ); // zeige den Dialog an
				}

				// Pr�fe, falls gew�nscht, ob das korrekte Steuerger�t eingelegt
				// wurde. Achtung, hierf�r ist eine Fahrzeugverbindung erforder-
				// lich und es muss zuvor (�ber einen separaten Pr�fschritt)
				// eine SVT-Ist erzeugt worden sein!
				if( sgbmLimit != -1 ) { // SGBM-Limit festgelegt?
					String sgAdrInHex = ecuAddresses[0]; // wir ber�cksichtigen stets nur das erste SG in der SG-Liste

					// Erzeuge den TAL-Filter.
					CascadeTALFilter myTALFilter = new CascadeTALFilter(); // erzeuge TAL-Filter
					Object[] allCategories = CascadeTACategories.getAllCategories().toArray(); // erlaube f�r unser SG alle Kategorien
					for( int i = 0; i < allCategories.length; i++ ) {
						CascadeFilteredTACategory myCategory = new CascadeFilteredTACategory( (CascadeTACategories) allCategories[i] );
						myCategory.addEcusToList( new String[] { sgAdrInHex } );
						myCategory.setBlacklist( false );
						myCategory.setAffectAllEcus( false );

						//  Sind Kategorien angeben worden, auf die wir uns 
						//  beschr�nken sollen?
						if( sgmbLimitCategories != null && sgmbLimitCategories.length != 0 ) {
							myCategory.setBlacklist( true ); //erst einmal ausklammern
							for( int j = 0; j < sgmbLimitCategories.length; j++ )
								//dann pr�fen, ob gew�nscht ist  
								if( allCategories[i].toString().equalsIgnoreCase( sgmbLimitCategories[j] ) ) {
									myCategory.setBlacklist( false ); //und wenn gefunden, dann wieder aufnehmen
									break; //suche beenden
								}
						}

						myTALFilter.addFilteredTACategory( myCategory );
					}

					// Erzeuge Sollverbauung und TAL.
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZCheckEcu 'psdz.generateAndStoreTAL' START" );
					}

					psdz.generateAndStoreTAL( new String[] { sgAdrInHex }, // SG-Adresse in dez.
							false, // das obige ist keine Blacklist
							myTALFilter, // TAL-Filter
							false // den SWT Status nicht checken
					);

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZCheckEcu 'psdz.generateAndStoreTAL' ENDE" );
					}

					// Beziehe die TAL.
					CascadeTAL myTAL = psdz.getTAL();

					// Werte die TAL aus. Ermittle dabei die Anzahl aller SGBMs.
					int sgbmCounter = 0;
					CascadeTALLine[] myTALLines = myTAL.getTalLines(); // jede Zeile steht f�r alle Transaktionen einer CascadeTA-Kategorie eines ECU
					if( myTALLines != null )
						for( int i = 0; i < myTALLines.length; i++ ) {
							CascadeTALTACategory[] myTALTACategories = myTALLines[i].getTALTACategories();
							if( myTALTACategories != null )
								for( int j = 0; j < myTALTACategories.length; j++ ) {
									CascadeTA[] sgbms = myTALTACategories[j].getTAs(); // beziehe SGBMs
									if( sgbms != null && Integer.toHexString( myTALLines[i].getDiagnosticAddress().intValue() ).equalsIgnoreCase( sgAdrInHex ) )
										sgbmCounter += sgbms.length; // erh�he den SGBM-Z�hler
								}
						}

					// Pr�fe, ob das festgelegt SGBM-Limit �berschritten wurde.
					if( sgbmCounter > sgbmLimit ) {
						result = new Ergebnis( "PSdZCheckEcu", "ECU check", "SGBM limit: " + sgbmLimit, "", "", "SGBMs", sgbmCounter + "", "0", sgbmLimit + "", "0", "", "", "", PB.getString( "psdz.error.SgbmLimitUeberschritten" ), DE ? "Bitte SG �berpr�fen!" : "Please check the ECU!", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException();
					} else {
						result = new Ergebnis( "PSdZCheckEcu", "ECU check", "SGBM limit: " + sgbmLimit, "", "", "SGBMs", sgbmCounter + "", "0", sgbmLimit + "", "0", "", "", "", "", "", Ergebnis.FT_IO ); // IO
						ergListe.add( result );
					}

					// Performancemessung
					if( bDebugPerform )
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZCheckEcu GetTAL ENDE" );

				}

			} catch( CascadePSdZArgumentException e ) {
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZ", "PSdZCheckEcu", "", "", "", "", "", "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Argument" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( CascadePSdZConditionsNotCorrectException e ) {
				// Fehler abfangen und dokumentiern
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZ", "PSdZCheckEcu", "", "", "", "", "", "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Bedingungen" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( CascadePSdZRuntimeException e ) {
				// Fehler abfangen und dokumentiern
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZ", "PSdZCheckEcu", "", "", "", "", "", "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Laufzeit" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// PSdZ Device freigeben.
		if( psdz != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releasePSdZ();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		// Dialog freigeben.
		try {
			getPr�flingLaufzeitUmgebung().releaseUserDialog();
		} catch( Exception e ) {
			if( e instanceof DeviceLockedException )
				result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "Userdialog", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "PSdZCheckEcu", "", "", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}

		setPPStatus( info, status, ergListe );

		// Performancemessung Ende
		if( bDebugPerform )
			System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZCheckEcu PP ENDE" );
	}

	/**
	 * Extrahiert die in einer SVT gespeicherten SVKs und speichert diese zusammen mit der jeweiligen SG-Adresse in einer HashMap ab (Schl�ssel = SG-Adresse, Wert = SVK zum SG).
	 * 
	 * @param svt Die SVT, deren SVKs extrahiert werden sollen.
	 * @return HashMap mit den ausgelesenen SVKs als Wert und den SG-Adressen als Schl�ssel.
	 */
	/*
	 * private HashMap extractSGBMs(CascadeSVT svt) { HashMap svk = new HashMap(); //speichet pro Steuerger�t die SVK (Schl�ssel = SG-Adresse, Wert = SGBM-ID-Liste) List ecuList = svt.getEcuList(); //beziehe SG-spezifische Informationen (darunter auch die SVKs) if (ecuList != null) for (int i = 0; i < ecuList.size(); i++) { //gehe alle SGs durch // Hole Objekt, welches in der SVT die SG-Information speichert. CascadeECU ecu = (CascadeECU)ecuList.get(i); if (ecu == null) continue; System.out.println("RETTIG, base variant name: " + ecu.getBaseVariantName()); //tttttttttttttttt System.out.println("RETTIG, ecu address: " + ecu.getDiagnosticAddress()); //tttttttttttttttt // Beziehe die SVK zum SG. CascadeSGBMID[] sgbms = ecu.getSvk(); if (sgbms == null) continue; for (int j = 0; j < sgbms.length; j++) { //ttttttttttttttttt System.out.println("RETTIG, SGBM name: " + sgbms[j].getProcessClass().getName()); //tttttttttttttttt System.out.println("RETTIG, SGBM main version: " + sgbms[j].getMainVersion()); //tttttttttttttttt System.out.println("RETTIG, SGBM sub version: " + sgbms[j].getSubVersion()); //tttttttttttttttt System.out.println("RETTIG, SGBM patch version: " + sgbms[j].getPatchVersion()); //tttttttttttttttt } // Speichere SVK unter SG-Adresse in HashMap. svk.put(ecu.getDiagnosticAddress().toString(), sgbms); } return svk; }
	 */

}

package com.bmw.cascade.pruefprozeduren;

import java.io.File;
import java.io.FilenameFilter;
import java.util.*;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.bmw.appframework.logging.LogLevel;
import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.Pruefling;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.psdz.PSdZ;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeExecutionStatus;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeFscTA;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeSwtActionType;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeSwtApplication;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTAL;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTALExecutionStatus;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTALLine;
import com.bmw.cascade.pruefstand.devices.psdz.data.CascadeTALTACategory;
import com.bmw.cascade.pruefstand.devices.psdz.exceptions.*;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.util.logging.CascadeLogging;

/**
 * Implementierung einer Pr�fprozedur, die das Einspielen eines von extern zugesteuerten (Refurbish-)FSCs durchf�hrt. <BR>
 * Hintergrund: Im Werk nach der F2_DIAGNOSE gekoppelte Komponenten, die z.B. aufgrund Kreuztausch in neue Fahrzeuge verbaut werden, k�nnen �ber Refurbish-FSC entkoppelt und somit wiederverwendet werden. <BR>
 * 
 * @author Martin Gampl, BMW AG <BR>
 *         22.03.2016 MG Ersterstellung <BR>
 *         ---------- -- F-Version (1_0), Mindestversionsanforderung CASCADE 7.1.4 / PSdZ 5.01.xx <BR>
 *         29.11.2016 MG T-Version (2_0) <BR>
 *         ---------- -- LOP 2139: Erweiterung um Argument bzgl. Geber-VIN (Refurbish-VIN) sowie Optimierung L�schvorgang des FSCs <BR>
 *         02.12.2016 MG F-Version (3_0), Mindestversionsanforderung CASCADE 7.1.4 / PSdZ 5.01.xx <BR>
 */
public class PSdZDeployFsc_3_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * PSdZ als globale Variable
	 */
	private PSdZ psdz = null;

	/**
	 * DE fuer Sprachermittlung
	 */
	private final boolean DE = checkDE(); // Systemsprache DE wenn true

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public PSdZDeployFsc_3_0_F_Pruefprozedur() {
	}

	/**
	 * Konstruktor.
	 * 
	 * @param pruefling Pr�fling, in dem die Pr�fprozedur instantiiert ist.
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public PSdZDeployFsc_3_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Default-Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den optionalen Parametern.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG", "DEBUG_PERFORM", "DELETE_USED_FSC_FILE_FROM_XML_PATH", "REFURBISH_VIN" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den zwingenden Parametern.
	 */
	public String[] getRequiredArgs() {
		String[] args = { "FSC_XML_PATH" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		return super.checkArgs();
	}

	/**
	 * Tr�gt ECUs in die Ergebnisliste (APDM) ein
	 * 
	 * @param mErgListe Ergebnisliste
	 * @param mMap Map mit einzutragenden ECU's
	 * @param mTransact einzutragende Transaktion
	 * @param mResult einzutragendes Ergebnis
	 * @param mPsdzErrors zugeh�rige einzutragende Fehler
	 * @param mECUStatistics Map mit SG Antwortverhalten
	 * @throws CascadePSdZRuntimeException
	 */
	private void entryECUsDetails( Vector<Ergebnis> mErgListe, TreeMap<String, HashSet<String>> mMap, String mTransact, String mSResult, HashMap<String, List<Exception>> mPsdzErrors, HashMap<String, String> mECUStatistics ) throws CascadePSdZRuntimeException, CascadePSdZConditionsNotCorrectException {
		Ergebnis mResult;
		String key, fehlertext, antwortVerhalten;
		CascadePSdZRuntimeException cscRE;
		Exception ex;
		String action = DE ? "Aktion: " : "Action: ";
		String errorDescription = DE ? "Fehlerbeschreibung: " : "Error description: ";
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";

		// Schleife �ber alle Eintr�ge der �bergebenen Map
		for( Iterator<Entry<String, HashSet<String>>> e = mMap.entrySet().iterator(); e.hasNext(); ) {
			Map.Entry<String, HashSet<String>> entry = e.next(); // z.B. SG:JBBF 0x00=[cdDeploy]
			cscRE = null;
			ex = null;

			String tempKey = entry.getKey();

			// etwas aufw�ndigerer Code (anstatt einfacher Suche nach ":"), stellt aber sicher, dass Basisvariante sicher ermittelt wird, auch dann wenn "psdz.SG" zuk�nftig ":" enthalten sollte(!)
			String sgBaseVariantName = tempKey.substring( tempKey.indexOf( PB.getString( "psdz.SG" ) ) + PB.getString( "psdz.SG" ).length() + 1, tempKey.indexOf( "0x" ) - 1 ); // z.B. "HU_NBT2"
			String sgDiagAddress = tempKey.substring( tempKey.indexOf( "0x" ) ); // z.B. "0x63"

			HashSet<String> myContent = new HashSet<String>();
			myContent = ((HashSet<String>) entry.getValue()); // -> [fscDeploy]

			Iterator<String> i = myContent.iterator();
			StringBuffer text = new StringBuffer();
			boolean hasNext = i.hasNext();
			Object o = null;

			while( hasNext ) {
				o = i.next();
				text.append( PB.getString( "psdz.tal." + String.valueOf( o ).toLowerCase().trim() ) );
				hasNext = i.hasNext();
				if( hasNext )
					text.append( ", " );
			}

			if( !mSResult.equalsIgnoreCase( Ergebnis.FT_IO ) ) {
				for( Iterator<String> j = myContent.iterator(); j.hasNext(); ) {
					String sgCat = j.next().toLowerCase().trim(); // -> "fscDeploy"

					for( int excep = 0; excep < (mPsdzErrors.get( sgDiagAddress + "_" + sgCat )).size(); excep++ ) {
						ex = (mPsdzErrors.get( sgDiagAddress + "_" + sgCat )).get( excep );
						if( ex instanceof CascadePSdZRuntimeException ) {
							cscRE = (CascadePSdZRuntimeException) ex;
						}

						// den anderen Key (-> Diagnose-Adresse ohne 0x und ohne f�hrende Nullen) f�r die ECUStatistics holen (etwas aufwendig...)
						key = sgDiagAddress.charAt( 2 ) == '0' ? sgDiagAddress.substring( 3, 4 ) : sgDiagAddress.substring( 2, 4 );

						// der Fehlertext
						if( cscRE != null ) {
							fehlertext = action + cscRE.getStrErrorContextTranslated() + ", " + errorDescription + cscRE.getMessage() + ", (PSdZ ID: " + cscRE.getMessageId() + ")";
						} else {
							fehlertext = PB.getString( mTransact );
						}

						// SG Antwortverhalten nur eintragen, wenn SG nie geantwortet hat oder sporadisch
						if( mECUStatistics.get( key ).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.0" ) ) || mECUStatistics.get( key ).equalsIgnoreCase( com.bmw.cascade.pruefstand.devices.psdz.PB.getString( "psdz.ECUStatistics.1" ) ) )
							antwortVerhalten = " (" + PB.getString( "psdz.SG" ) + " " + PB.getString( "psdz.antwortet" ) + " " + mECUStatistics.get( key ) + ")";
						else
							antwortVerhalten = "";

						mResult = new Ergebnis( "PSdZDeployFsc", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, text.toString(), "", "STATUS", statusNotOkay, "", "", "0", "", "", "", fehlertext + antwortVerhalten, "", mSResult );
						mErgListe.add( mResult );
					}
				}
			} else {
				mResult = new Ergebnis( "PSdZDeployFsc", "PSdZ", sgBaseVariantName + (DE ? ", Adr.: " : ", Addr.: ") + sgDiagAddress, text.toString(), "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", mSResult );
				mErgListe.add( mResult );
			}
		}
	}

	/**
	 * Eintrag des ersten allgemeinen Fehlers, des NIO-ECU oder des NotExecutable-ECU ins APDM
	 * 
	 * @param gErgListe Ergebnisliste
	 * @param gMapNIO Map mit NIO-ECU's
	 * @param gMapNotExec Map mit NotExecutable-ECU's
	 * @param gPsdzErrors zugeh�rige einzutragende Fehler
	 * @param gECUStatistics Map mit SG Antwortverhalten
	 * @throws CascadePSdZRuntimeException
	 */
	private void entryECUsFT_NIO( Vector<Ergebnis> gErgListe, TreeMap<String, HashSet<String>> gMapNIO, TreeMap<String, HashSet<String>> gMapNotExec, HashMap<String, List<Exception>> gPsdzErrors, HashMap<String, String> gECUStatistics ) throws CascadePSdZRuntimeException, CascadePSdZConditionsNotCorrectException {
		Ergebnis gResult;
		String action = DE ? "Aktion: " : "Action: ";
		String errorDescription = DE ? "Fehlerbeschreibung: " : "Error description: ";
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";

		// Haben wir irgendeinen allgemeinen Fehler ohne SG Adresse?
		if( gPsdzErrors.containsKey( "-1" ) ) {
			CascadePSdZRuntimeException cscRE = null;

			// Liste der Fehler durchgehen
			for( int e = 0; e < (gPsdzErrors.get( "-1" )).size(); e++ ) {
				Exception ex = (gPsdzErrors.get( "-1" )).get( e );

				if( ex instanceof CascadePSdZRuntimeException ) {
					cscRE = (CascadePSdZRuntimeException) ex;
				}

				gResult = new Ergebnis( "PSdZDeployFsc", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", cscRE != null ? action + cscRE.getStrErrorContextTranslated() + ", " + errorDescription + cscRE.getMessage() + ", (PSdZ ID: " + cscRE.getMessageId() + ")" : ex.getMessage(), "", Ergebnis.FT_NIO );
				gErgListe.add( gResult );
			}
		}

		// NIO ECUs in Ergebnisliste (APDM) eintragen
		entryECUsDetails( gErgListe, gMapNIO, "psdz.tal.transactionNIO", Ergebnis.FT_NIO, gPsdzErrors, gECUStatistics );

		// NIO ECUs NotExecutable in Ergebnisliste (APDM) eintragen
		entryECUsDetails( gErgListe, gMapNotExec, "psdz.tal.transactionNOTExecutable", Ergebnis.FT_NIO, gPsdzErrors, gECUStatistics );
	}

	/**
	 * Aufbau einer HashMap aus den Exceptions der TAL (Aufbau: key = Steuerger�teadresse in hex + _ + TACategorie z.B. '0x63_fscdeploy', wert = 'Exception'
	 * 
	 * @param mTALResult zu analysierende CascadeTAL
	 * @return HashMap aus input-CascadeTAL erzeugte HashMap
	 */
	private HashMap<String, List<Exception>> buildExcHashMap( CascadeTAL mTALResult ) {
		HashMap<String, List<Exception>> ret_val = new HashMap<String, List<Exception>>();

		// Exceptions in der TAL?
		if( mTALResult.getExceptions().size() > 0 ) {
			ret_val.put( "-1", mTALResult.getExceptions() );
		}

		CascadeTALLine[] myLines = mTALResult.getTalLines();
		for( int i = 0; i < myLines.length; i++ ) {
			String sgAddressTemp = "0" + Integer.toHexString( myLines[i].getDiagnosticAddress().intValue() ).toUpperCase(); // z.B. "063"
			String sgDiagAddress = "0x" + sgAddressTemp.substring( sgAddressTemp.length() - 2, sgAddressTemp.length() ); // z.B. "0x63"
			String sgCat = "_" + myLines[i].getTALTACategories()[0].getCategory().toString().toLowerCase().trim(); // z.B. "_fscdeploy"
			boolean foundException = false;

			// Exceptions in der TALLine?
			if( myLines[i].getExceptions().size() > 0 ) {
				ret_val.put( sgDiagAddress + sgCat, myLines[i].getExceptions() );
				foundException = true;
			}

			if( !foundException ) {
				for( int j = 0; j < myLines[i].getTALTACategories().length; j++ ) {
					CascadeTALTACategory myTALTACategory = myLines[i].getTALTACategories()[j];
					sgCat = "_" + myTALTACategory.getCategory().toString().toLowerCase().trim();
					// Pr�fung der TAs in diesem Fall nicht notwendig, da in dieser PP nicht existent

					// Pr�fung der FscTAs					
					for( int k = 0; k < myTALTACategory.getFscTAs().length; k++ ) {
						CascadeFscTA myFscTA = myTALTACategory.getFscTAs()[k];
						// Exceptions in der FSC Transaktion?
						if( !myFscTA.getExceptions().isEmpty() ) {
							// erste gefundene Exception wird gespeichert und weitere Suche abgebrochen, da PSdZ in diesem Fall auch keine Folge-Transaktionen innerhalb der TA-Kategorie mehr anst��t 
							// und bei einem erneuten put-Aufruf auf das ret_val-Objekt f�r den gleichen key der zuerst gespeicherte value (-> erste Exception) �berschrieben w�rde
							ret_val.put( sgDiagAddress + sgCat, myFscTA.getExceptions() );
							break;
						}
					}
				}
			}
		}
		return ret_val;
	}

	/**
	 * Aufbau von drei TreeMaps aus den Ergebnissen der TAL (Aufbau: key = 'SG:' + Basisvariantenname + ' ' + Steuerger�teadresse in hex, wert = TALCategorie z.B. 'SG:BDC_GW2 0x10', wert = 'fscDeploy'
	 * 
	 * @param mTALResult zu analysierende CascadeTAL
	 * @param mEcuIO aus input-CascadeTAL erzeugte TreeMap mit CascadeExcecutionStatus = PROCESSED
	 * @param mEcuNIO aus input-CascadeTAL erzeugte TreeMap mit CascadeExcecutionStatus = PROCESSEDWITHERROR
	 * @param mEcuNotExecutable aus input-CascadeTAL erzeugte TreeMap mit CascadeExcecutionStatus = NOTEXECUTABLE
	 */
	private void buildMaps( CascadeTAL mTALResult, TreeMap<String, HashSet<String>> mEcuIO, TreeMap<String, HashSet<String>> mEcuNIO, TreeMap<String, HashSet<String>> mEcuNotExecutable ) {
		// Leeren der Maps
		mEcuIO.clear();
		mEcuNIO.clear();
		mEcuNotExecutable.clear();

		// Ergebnisanalyse

		for( int i = 0; i < mTALResult.getTalLines().length; i++ ) {
			// BasisName des SG ermitteln ("ECU:'Basisname'" + " 0x'hexcode'")
			String sgAddressTemp = "0" + Integer.toHexString( mTALResult.getTalLines()[i].getDiagnosticAddress().intValue() ).toUpperCase(); // z.B. "063"
			String sgIDName = PB.getString( "psdz.SG" ) + ":" + mTALResult.getTalLines()[i].getBaseVariantName() + " " + "0x" + sgAddressTemp.substring( sgAddressTemp.length() - 2, sgAddressTemp.length() ); // z.B. "SG:HU_NBT2 0x63"

			// Status der einzelnen TAs holen und merken in IO-, NIO- und NotExecutable-Listen
			for( int j = 0; j < mTALResult.getTalLines()[i].getTALTACategories().length; j++ ) {
				// IO TAL-Lines analysieren und TalLine-Kategorien merken
				// Es werden der TAL-Line Status analysiert und nicht der Status der TALTACategories, damit Fehler, die nach der Transaktion auftreten (-> FINALIZATION) auch erfasst werden 
				if( mTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.PROCESSED ) || mTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.INACTIVE ) ) {
					HashSet<String> setTalTaCategoriesIO = new HashSet<String>();
					// pr�fe ob schon eingetragen
					if( mEcuIO.containsKey( sgIDName ) )
						setTalTaCategoriesIO = mEcuIO.get( sgIDName );

					setTalTaCategoriesIO.add( mTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString() );
					mEcuIO.put( sgIDName, setTalTaCategoriesIO );
				}

				// NIO TAL-Lines analysieren und TalLine-Kategorien merken (-> fscDeploy)
				if( mTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.PROCESSEDWITHERROR ) ) {
					HashSet<String> setTalTaCategoriesNIO = new HashSet<String>();
					// pr�fe ob schon eingetragen
					if( mEcuNIO.containsKey( sgIDName ) )
						setTalTaCategoriesNIO = mEcuNIO.get( sgIDName );

					setTalTaCategoriesNIO.add( mTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString() );
					mEcuNIO.put( sgIDName, setTalTaCategoriesNIO );
				}

				// NotExcecutable (auch NIO !)
				if( mTALResult.getTalLines()[i].getStatus().equals( CascadeExecutionStatus.NOTEXECUTABLE ) ) {
					HashSet<String> setTalTaCategoriesNotExecutable = new HashSet<String>();
					// pr�fe ob schon eingetragen
					if( mEcuNotExecutable.containsKey( sgIDName ) )
						setTalTaCategoriesNotExecutable = mEcuNotExecutable.get( sgIDName );

					setTalTaCategoriesNotExecutable.add( mTALResult.getTalLines()[i].getTALTACategories()[j].getCategory().toString() );
					// Key von mEcuNotExecutable z.B. "SG:HU_NBT2 0x63", value z.B. "fscDeploy" 
					mEcuNotExecutable.put( sgIDName, setTalTaCategoriesNotExecutable );
				}
			}
		}
	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {
		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen
		boolean bDebugPerform = true; // gibt an, ob eine Performencemessung erfolgen sollen
		boolean bDeleteUsedFscFileFromXmlPath = true; // gibt an, nach erfolgreicher Einbringung des FSC, dieser vom parametrierten Dateipfad wieder gel�scht werden soll 
		boolean bParallelExecution = true; // Voreinstellung: parallele Ausf�hrung erlauben
		final boolean DE = checkDE(); // Systemsprache DE wenn true
		final int I_TAL_POLLTIME = 1000; // alle I_TAL_POLLTIME in ms TAL Status �berpr�fen
		int iMaxErrorRepeat = 1; // Voreinstellung: maximal 1 Wdhlg im Fehlerfall
		int status = STATUS_EXECUTION_OK;

		Ergebnis result;
		String optionalArgumentName = null;
		String optionalArgumentValue = null;
		String refurbishShortVin = null; // Refurbish-VIN / Geber-VIN
		String requiredArgumentName = null;
		String requiredArgumentValue = null;
		String statusNotOkay = DE ? "NICHT OKAY" : "NOT OKAY";
		String sFscXmlPath = null; // FSC XML File mit Pfad von extern
		String testScreenID = this.getPr�flingLaufzeitUmgebung().getName(); // TestScreenID (aus Pr�fstandsinstanz)
		String[] optionalArgumentsNames = getOptionalArgs();
		String[] optionalArgumentsValues = null;
		String[] requiredArgumentsNames = getRequiredArgs();
		String[] requiredArgumentsValues = null;
		// Verwaltung der verarbeiteten SG [IO, NIO, NOTEXECUTABLE] in sortierten Listen mit Variable=ECU und Wert=TA, SWT Kategories als HashSet; Bsp.: Variable=0x63 ECU:HU_NBT2, Wert=cdDeploy, fscDeploy als HashSet
		TreeMap<String, HashSet<String>> mapEcuIO = new TreeMap<String, HashSet<String>>(), mapEcuNIO = new TreeMap<String, HashSet<String>>(), mapEcuNotExecutable = new TreeMap<String, HashSet<String>>();
		UserDialog userDialog = null;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();

		try {
			// Argumente loggen, einlesen und checken
			try {

				// Ausgabe der Pr�fprozedurversion sowie s�mtlicher an die Pr�fprozedur �bergebener Argumente (required/optional) f�r sp�tere Analysezwecke

				System.out.println( "PSdZ ID:" + testScreenID + ", Initialize " + this.getClassName() );

				for( int i = 0; i < requiredArgumentsNames.length; i++ ) {
					requiredArgumentName = requiredArgumentsNames[i];
					requiredArgumentValue = "";
					// gibt es nicht optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( requiredArgumentName ) != null ) {
						requiredArgumentsValues = extractValues( getArg( requiredArgumentName ) );
						for( int k = 0; k < requiredArgumentsValues.length; k++ ) {
							// Listen von nicht optionalen Argumenten werden durch ";" getrennt ausgegeben
							requiredArgumentValue = requiredArgumentValue + requiredArgumentsValues[k] + ((requiredArgumentsValues.length > 1 && k < (requiredArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						requiredArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZDeployFsc with required argument: " + requiredArgumentName + " = " + requiredArgumentValue );
				}

				for( int i = 0; i < optionalArgumentsNames.length; i++ ) {
					optionalArgumentName = optionalArgumentsNames[i];
					optionalArgumentValue = "";
					// gibt es optionale Argumente, die an PP �bergeben wurden ?
					if( getArg( optionalArgumentName ) != null ) {
						optionalArgumentsValues = extractValues( getArg( optionalArgumentName ) );
						for( int k = 0; k < optionalArgumentsValues.length; k++ ) {
							// Listen von optionalen Argumenten werden durch ";" getrennt ausgegeben
							optionalArgumentValue = optionalArgumentValue + optionalArgumentsValues[k] + ((optionalArgumentsValues.length > 1 && k < (optionalArgumentsValues.length - 1)) ? ";" : "");
						}
					} else {
						optionalArgumentValue = "null (using default value)";
					}

					System.out.println( "PSdZ ID:" + testScreenID + ", Initialize PSdZDeployFsc with optional argument: " + optionalArgumentName + " = " + optionalArgumentValue );
				}

				// Obligatorische Argumentpr�fung auf Vorhandensein von nicht optionalen sowie �berz�hligen Argumenten
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
					else
						throw new PPExecutionException( "DEBUG " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DEBUG_PERFORM
				if( getArg( "DEBUG_PERFORM" ) != null ) {
					if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "TRUE" ) )
						bDebugPerform = true;
					else if( getArg( "DEBUG_PERFORM" ).equalsIgnoreCase( "FALSE" ) )
						bDebugPerform = false;
					else
						throw new PPExecutionException( "DEBUG_PERFORM " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// DELETE_USED_FSC_FILE_FROM_XML_PATH
				if( getArg( "DELETE_USED_FSC_FILE_FROM_XML_PATH" ) != null ) {
					if( getArg( "DELETE_USED_FSC_FILE_FROM_XML_PATH" ).equalsIgnoreCase( "TRUE" ) )
						bDeleteUsedFscFileFromXmlPath = true;
					else if( getArg( "DELETE_USED_FSC_FILE_FROM_XML_PATH" ).equalsIgnoreCase( "FALSE" ) )
						bDeleteUsedFscFileFromXmlPath = false;
					else
						throw new PPExecutionException( "DELETE_USED_FSC " + PB.getString( "psdz.parameter.ungueldigerWert" ) );
				}

				// FSC_XML_PATH
				if( getArg( "FSC_XML_PATH" ) != null ) {
					sFscXmlPath = extractValues( getArg( "FSC_XML_PATH" ) )[0];
				}

				// REFURBISH_VIN
				if( getArg( "REFURBISH_VIN" ) != null ) {
					refurbishShortVin = extractValues( getArg( "REFURBISH_VIN" ) )[0];
				}

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			// Performancemessung Start
			if( bDebugPerform )
				System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc PP START" );

			// Devices holen
			try {
				psdz = getPr�flingLaufzeitUmgebung().getDeviceManager().getPSdZ();
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			try {
				userDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
				// Informiere den Benutzer
				userDialog.setDisplayProgress( true );
				userDialog.getProgressbar().setIndeterminate( true );
				String title = DE ? "PSdZ Einbringen des Freischaltcodes" : "PSdZ Deploy FSC";
				userDialog.displayMessage( title, ";" + PB.getString( "psdz.ud.Vorbereitunglaeuft" ), -1 );
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Einbringen des (Refurbish-)FSCs
			try {

				// ********************************************************************************
				// * 1. FSC einlesen                                                              *
				// ********************************************************************************

				// Dateipfad erstellen. Die aufzufindende Datei hat folgendes Namens-Format: FSC_<Short-VIN>_<App-Nr><Upgrade-Index>.xml
				// Gesucht werden soll die Datei zur aktuellen Fahrzeug-VIN mit dem niedrigsten <App-Nr><Upgrade-Index>. 
				// Hintergrund: Der Refurbish-Prozess kann auch mehrfach durchgef�hrt werden, jedoch wird bei einem verbrauchten FSC ein neuer FSC mit jeweils um 1 erh�htem Upgrade-Index (UI) ben�tigt. Der verbrauchte FSC wird nach i.O. Freischaltung gel�scht.
				// Hierf�r wird durch die automatische Suche der jeweils n�chsth�here FSC angezogen.
				File fFscXmlPath = new File( sFscXmlPath );

				// Pr�fen ob Pfad existiert, falls nein Fehler und Ende
				if( !fFscXmlPath.exists() ) {
					String errorText = DE ? "Dateipfad " + sFscXmlPath + " aus Parameter FSC_XML_PATH existiert nicht!" : "File " + sFscXmlPath + " from parameter FSC_XML_PATH does not exist!";
					result = new Ergebnis( "PSdZDeployFsc", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText, "", Ergebnis.FT_NIO );
					ergListe.add( result );

					throw new PPExecutionException();
				}

				// Nur nach Dateien suchen, die die Fahrzeug-VIN oder die Refurbish-(Geber-)VIN enthalten. Erwartetes Format: FSC_<ShortVIN>_*.XML
				final String vehicleShortVin = psdz.getVehicleVin().substring( 17 - 7 ).toUpperCase();
				final String fileNameSearchPattern = refurbishShortVin != null ? "FSC_" + refurbishShortVin + "_" : "FSC_" + vehicleShortVin + "_";
				FilenameFilter fnf = new FilenameFilter() {
					@Override
					public boolean accept( File dir, String name ) {
						if( name.toUpperCase().indexOf( fileNameSearchPattern ) != -1 && name.toUpperCase().endsWith( ".XML" ) )
							return true;
						else
							return false;
					}
				};
				File[] fArrayFscXmlFiles = fFscXmlPath.listFiles( fnf );

				// Passende FSC-Dateien (Strings) aus bereits zur VIN passenden FSC-Dateien ermittlen
				String[] sArrayFscFileNames = new String[fArrayFscXmlFiles.length];
				for( int i = 0; i < fArrayFscXmlFiles.length; i++ ) {
					sArrayFscFileNames[i] = fArrayFscXmlFiles[i].getName();
				}

				// Pr�fen ob passende Datei gefunden wurde, falls nein Fehler und Ende
				if( sArrayFscFileNames.length == 0 ) {
					String errorText = DE ? "Keine passende Datei " + sFscXmlPath + File.separator + "FSC_" + vehicleShortVin + "_*.XML gefunden!" : "No matching file " + sFscXmlPath + File.separator + "FSC_" + vehicleShortVin + "_*.XML found!";
					result = new Ergebnis( "PSdZDeployFsc", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText, "", Ergebnis.FT_NIO );
					ergListe.add( result );

					throw new PPExecutionException();
				}

				// Falls Datei gefunden, dann Array sortieren
				Arrays.sort( sArrayFscFileNames );

				// Nehme erstes Element aus sortierter Liste von Dateienamen und erzeuge File-Objekt der zu verwendenden FSC-Datei
				File fFscXmlFile = new File( sFscXmlPath, sArrayFscFileNames[0] );

				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document document = null;
				Element root = null;
				try {
					document = builder.parse( fFscXmlFile );
					root = document.getDocumentElement();

				} catch( Exception e ) {
					String errorText = DE ? "Datei " + sFscXmlPath + File.separator + sArrayFscFileNames[0] + " nicht einlesbar. Dateiformat pr�fen!" : "File " + sFscXmlPath + File.separator + sArrayFscFileNames[0] + " could not be read. Check file format!";
					result = new Ergebnis( "PSdZDeployFsc", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText, "", Ergebnis.FT_NIO );
					ergListe.add( result );

					throw new PPExecutionException();
				}

				// Einlesen der XML-Elemente (FSC, Diagnoseadresse, ApplikationsNummer, Upgradeindex) aus FSC-Dateiformat
				String sFscHEX = null;
				for( int i = 0; i < root.getElementsByTagName( "fsc" ).getLength(); i++ ) {
					if( root.getElementsByTagName( "fsc" ).item( i ).getAttributes().item( 0 ).getNodeValue().equalsIgnoreCase( "HEX" ) ) {
						sFscHEX = root.getElementsByTagName( "fsc" ).item( i ).getFirstChild().getNodeValue();
					}
				}
				String ecuAddressHex = root.getElementsByTagName( "diagnoseAddr" ).getLength() != 0 ? root.getElementsByTagName( "diagnoseAddr" ).item( 0 ).getFirstChild().getNodeValue() : null;
				Integer applicationNumber = root.getElementsByTagName( "ApplicationNumber" ).getLength() != 0 ? Integer.valueOf( root.getElementsByTagName( "ApplicationNumber" ).item( 0 ).getFirstChild().getNodeValue(), 16 ) : null;
				Integer upgradeIndex = root.getElementsByTagName( "UpgradeIndex" ).getLength() != 0 ? Integer.valueOf( root.getElementsByTagName( "UpgradeIndex" ).item( 0 ).getFirstChild().getNodeValue(), 16 ) : null;

				if( sFscHEX == null || ecuAddressHex == null || applicationNumber == null || upgradeIndex == null ) {
					String errorText = DE ? "Datei " + sFscXmlPath + File.separator + sArrayFscFileNames[0] + " enth�lt nicht alle notwendigen Informationen (FSC, Diagnoseadresse, Applikationsnummer, Upgrade-Index)! Dateiinhalt pr�fen!" : "File " + sFscXmlPath + File.separator + sArrayFscFileNames[0] + " does not contain all required information (FSC, diag address, application number, upgrade index)! Check file content!";
					result = new Ergebnis( "PSdZDeployFsc", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText, "", Ergebnis.FT_NIO );
					ergListe.add( result );

					throw new PPExecutionException();
				}

				// ********************************************************************************
				// * 2. CascadeSWTApplication-Objekt bauen                                        *
				// ********************************************************************************

				// FSC-String in Byte-Array konvertieren				
				byte[] bArrayFscHEX = new byte[sFscHEX.length() / 2];
				for( int i = 0; i < sFscHEX.length() / 2; i++ ) {
					bArrayFscHEX[i] = (byte) ((Character.digit( sFscHEX.charAt( 2 * i ), 16 ) << 4) + Character.digit( sFscHEX.charAt( 2 * i + 1 ), 16 ));
				}

				CascadeSwtActionType swtActionType = CascadeSwtActionType.TYPE_ACTIVATE_UPGRADE;
				CascadeSwtApplication mySwtApplication = new CascadeSwtApplication( (int) applicationNumber, (int) upgradeIndex );
				mySwtApplication.setFSC( bArrayFscHEX );
				mySwtApplication.setCascadeSwtActionType( swtActionType );

				// ********************************************************************************
				// * 3. SVT IST ermitteln                                                         *
				// ********************************************************************************

				String[] ecuSollList = { ecuAddressHex };

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc 'psdz.generateAndStoreSVTIst' START" );
				}

				// SVT ist berechnen
				psdz.generateAndStoreSVTIst( "_FCFN", ecuSollList, false );

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc 'psdz.generateAndStoreSVTIst' ENDE" );
				}

				// ********************************************************************************
				// * 4. TAL generieren (FCFN)                                                     *
				// ********************************************************************************

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc 'psdz.generateAndStoreFcfnTAL' START" );
				}

				// TAL berechnen
				psdz.generateAndStoreFcfnTAL( mySwtApplication );

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc 'psdz.generateAndStoreFcfnTAL' ENDE" );
				}

				// ********************************************************************************
				// * 5. TAL ausf�hren: FSC einbringen                                             *
				// ********************************************************************************

				CascadeTAL myTALResult = null;

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc 'psdz.getTAL' START" );
				}

				// Jetzt die erzeugte, gefilterte TAL weiterverwenden
				myTALResult = psdz.getTAL();

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc 'psdz.getTAL' ENDE" );
				}

				// falls TAL nicht leer ist, wird neuer Nutzerdialog zum Einbringen des FSCs angezeigt
				if( myTALResult.getTalLines().length > 0 ) {
					// UserDialog Reset
					userDialog.reset();

					// Info via UserDialog
					userDialog.setAllowCancel( true );
					userDialog.getProgressbar().setIndeterminate( true );
					String title = DE ? "PSdZ Einbringen des Freischaltcodes" : "PSdZ Deploy FSC";
					String message = DE ? "Einbringen des FSCs in SG: 0x" + ecuAddressHex : "Deploy FSC to ECU: 0x" + ecuAddressHex;
					userDialog.displayMessage( title, ";" + message, -1 );
				}

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc 'psdz.executeTALAsync' START" );
				}
				// asynchrone Ausf�hrung einer TAL starten, ohne FSCs aus Auftragsdaten zu bef�llen 
				psdz.executeTALAsync( bParallelExecution, iMaxErrorRepeat );

				// Performancemessung
				if( bDebugPerform ) {
					System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc 'psdz.executeTALAsync' ENDE" );
				}

				// �berpr�fe pollend die Ausf�hrung der TAL
				while( true ) {
					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc 'psdz.getTALExecutionStatus' START" );
					}
					// lese den Status + hier wird f�r I_TAL_POLLTIME in ms gewartet
					myTALResult = psdz.getTALExecutionStatus( I_TAL_POLLTIME, null );

					// Performancemessung
					if( bDebugPerform ) {
						System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc 'psdz.getTALExecutionStatus' ENDE" );
					}

					// CHECK TAL EXECUTION STATUS
					// fertig IO?

					// CascadeTALExecutionStatus.FINISHED_WITH_WARNINGS, CascadeTALExecutionStatus.FINISHED_FOR_HW_TRANSACTIONS_WITH_WARNINGS und CascadeTALExecutionStatus.FINISHED_FOR_HW_TRANSACTIONS werden auf CascadeTALExecutionStatus.FINISHED durch CASCADE-Kern-SW gemappt
					if( myTALResult.getStatus().getName().equals( CascadeTALExecutionStatus.FINISHED.getName() ) ) {
						//Ergebnis TAL auf PLatte ablegen
						psdz.getTALExecutionStatus( 0, "_STATUS_FSC_DEPLOY" );

						// Erzeugen der TreeMaps IO, NIO und NotExecutable
						buildMaps( myTALResult, mapEcuIO, mapEcuNIO, mapEcuNotExecutable );

						// IO - ECU in Ergebnisliste (APDM) eintragen
						entryECUsDetails( ergListe, mapEcuIO, "psdz.tal.transactionIO", Ergebnis.FT_IO, null, null );

						// IO Ergebnis in (APDM) eintragen
						result = new Ergebnis( "PSdZDeployFsc", "PSdZ", "", "", "", DE ? "Ausf�hrungsstatus der TAL" : "TAL execution status", myTALResult.getStatus().getName(), myTALResult.getStatus().getName(), "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );

						break;
					}

					// fertig NIO?
					// CascadeTALExecutionStatus.FINISHED_WITH_ERROR, CascadeTALExecutionStatus.ABORTED_BY_ERROR, CascadeTALExecutionStatus.ABORTED_BY_USER und CascadeTALExecutionStatus.FINISHED_FOR_HW_TRANSACTIONS_WITH_ERROR werden auf CascadeTALExecutionStatus.FINISHED_WITH_ERROR_IN_TRANSACTION durch CASCADE-Kern-SW gemappt
					if( myTALResult.getStatus().getName().equals( CascadeTALExecutionStatus.FINISHED_WITH_ERROR_IN_TRANSACTION.getName() ) ) {

						//Ergebnis TAL auf PLatte ablegen
						psdz.getTALExecutionStatus( 0, "_STATUS_FSC_DEPLOY" );

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc 'psdz.getEcuStatisticsHexString' START" );
						}
						// Erzeugen der TreeMaps IO, NIO und NotExecutable
						buildMaps( myTALResult, mapEcuIO, mapEcuNIO, mapEcuNotExecutable );
						// PSdZ hole die aufgelaufenen Fehler
						HashMap<String, List<Exception>> psdzErrors = new HashMap<String, List<Exception>>( buildExcHashMap( myTALResult ) );
						HashMap<String, String> psdzECUStatisticsHexStr = new HashMap<String, String>( psdz.getEcuStatisticsHexString() );
						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc 'psdz.getEcuStatisticsHexString' ENDE" );
						}

						// IO - ECU in Ergebnisliste (APDM) eintragen
						entryECUsDetails( ergListe, mapEcuIO, "psdz.tal.transactionIO", Ergebnis.FT_IO, null, null );

						// NIO - / NotExecutable - ECU in Ergebnisliste (APDM) eintragen
						entryECUsFT_NIO( ergListe, mapEcuNIO, mapEcuNotExecutable, psdzErrors, psdzECUStatisticsHexStr );

						// NIO Ergebnis in APDM eintragen						
						result = new Ergebnis( "PSdZDeployFsc", "PSdZ", "", "", "", DE ? "Ausf�hrungsstatus der TAL" : "TAL execution status", myTALResult.getStatus().getName(), CascadeTALExecutionStatus.FINISHED.toString(), "", "0", "", "", "", DE ? "Ausgef�hrte TAL enth�lt Fehler" : "Executed TAL contains errors", "", Ergebnis.FT_NIO );
						ergListe.add( result );

						throw new PPExecutionException();
					}

					// Abbruch durch Werker???
					if( userDialog.isCancelled() == true ) {
						userDialog.reset();

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc 'psdz.cancelTALExecution' START" );
						}
						psdz.cancelTALExecution();

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc 'psdz.cancelTALExecution' ENDE" );
						}

						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc 'psdz.getEcuStatisticsHexString' START" );
						}
						// Erzeugen der TreeMaps IO, NIO und NotExecutable
						buildMaps( myTALResult, mapEcuIO, mapEcuNIO, mapEcuNotExecutable );
						// PSdZ hole die aufgelaufenen Events 
						HashMap<String, List<Exception>> psdzErrors = new HashMap<String, List<Exception>>( buildExcHashMap( myTALResult ) );
						HashMap<String, String> psdzECUStatisticsHexStr = new HashMap<String, String>( psdz.getEcuStatisticsHexString() );
						// Performancemessung
						if( bDebugPerform ) {
							System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc 'psdz.getEcuStatisticsHexString' ENDE" );
						}

						// IO - ECU in Ergebnisliste (APDM) eintragen
						entryECUsDetails( ergListe, mapEcuIO, "psdz.tal.transactionIO", Ergebnis.FT_IO, null, null );

						// NIO - / NotExecutable - ECU in Ergebnisliste (APDM) eintragen
						entryECUsFT_NIO( ergListe, mapEcuNIO, mapEcuNotExecutable, psdzErrors, psdzECUStatisticsHexStr );

						// Werkerabbruch in Ergebnisliste (APDM) eintragen
						result = new Ergebnis( "PSdZDeployFsc", "Userdialog", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", PB.getString( "werkerAbbruch" ), "", PB.getString( "pollingAbbruch" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );

						throw new PPExecutionException();
					}
				}

				// ********************************************************************************
				// * 6. FSC-Datei l�schen, falls parametriert (FSC wurde erfolgreich eingebracht) *
				// ********************************************************************************

				if( bDeleteUsedFscFileFromXmlPath ) {
					int retryloop = 0;
					while( true ) {
						retryloop++;
						boolean success = fFscXmlFile.delete();
						if( !success ) {
							if( retryloop == 3 ) {
								System.out.println( "PSdZ ID:" + testScreenID + ", DELETION OF FILE FINALLY FAILED! No more attempts are scheduled." );
								String errorText = DE ? "Datei " + fFscXmlFile.getPath() + " konnte nicht gel�scht werden!" : "File " + fFscXmlFile.getPath() + " could not be deleted!";
								result = new Ergebnis( "PSdZDeployFsc", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", errorText, "", Ergebnis.FT_NIO );
								ergListe.add( result );
								throw new PPExecutionException();
							}
							System.out.println( "PSdZ ID:" + testScreenID + ", Deletion of FSC file " + fFscXmlFile.getPath() + " failed! Next try to delete file in 5 secs, LOOP " + (retryloop) + " of 3..." );
							Thread.sleep( 5000 );
						} else {
							System.out.println( "PSdZ ID:" + testScreenID + ", FSC file " + fFscXmlFile.getPath() + " SUCCESSFULLY DELETED!" );
							break;
						}
					}
				}

			} catch( CascadePSdZArgumentException e ) {
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZDeployFsc", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Argument" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( CascadePSdZConditionsNotCorrectException e ) {
				if( bDebug )
					e.printStackTrace();
				result = new Ergebnis( "PSdZDeployFsc", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Bedingungen" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( CascadePSdZRuntimeException e ) {
				if( bDebug )
					e.printStackTrace();
				// Dokumentiere den Fehler in APDM 
				result = new Ergebnis( "PSdZDeployFsc", "PSdZ", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", e.getLocalizedMessage(), PB.getString( "psdz.error.Laufzeit" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}

			// reset UserDialog
			userDialog.reset();

			// ****************************************************************************
			// * 7. ENDE: FERTIG                                                          *
			// ****************************************************************************

		} catch( PPExecutionException ppe ) {
			if( bDebug )
				ppe.printStackTrace();
			status = STATUS_EXECUTION_ERROR;

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			if( bDebug )
				t.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// Freigabe der benutzten Devices
		if( psdz != null ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releasePSdZ();
				if( bDebug )
					CascadeLogging.getLogger().log( LogLevel.INFO, "PSdZDeployFsc: Release PSdZ Device finished." );
			} catch( Exception e ) {
				if( bDebug )
					e.printStackTrace();
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "PSdZ", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}
		try {
			getPr�flingLaufzeitUmgebung().releaseUserDialog();
			if( bDebug )
				CascadeLogging.getLogger().log( LogLevel.INFO, "PSdZDeployFsc: Release UserDialog Device finished." );
		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			if( e instanceof DeviceLockedException )
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
			else
				result = new Ergebnis( "Userdialog", "", "", "", "", "STATUS", statusNotOkay, "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "PSdZDeployFsc", "", "", "", "", "STATUS", "OKAY", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}
		setPPStatus( info, status, ergListe );

		// Performancemessung Ende
		if( bDebugPerform )
			System.out.println( "PSdZ ID:" + testScreenID + ", PERFORM PSdZDeployFsc PP ENDE" );
	}
}

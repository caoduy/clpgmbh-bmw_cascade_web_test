/*
 * SDiagEthernetBus Pruefprozedur.java
 *
 * Created on 03.07.14
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.DeviceLockedException;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;

/**
 * F�r die Ethernet SG mit integrierten Switch (z.B. ZGW/BDC, NBT/HeadUnit, SAS, ..) muss das Anlernen / Aktualisieren von deren MAC Adressen <BR>
 * in der ARL (ARP) Tabelle des Gateway SG (ZGW/BDC) getriggert werden. Nachfolgend muss �ber das Gateway SG gepr�ft werden, dass die MAC Adresse<BR>
 * auch in ARL (ARP) Tabelle enthalten ist. Ist dies so, soll das Pr�fergebnis als IO dokumentiert werden, andernfalls als NIO.
 * <BR>
 * Hinweis: Die MAC Adressen sind im SG in der Notation LSB dann MSB abgelegt<BR>
 * <BR>
 * Anforder: Michael Sarkadi, 23.06.2014, LOP 1795
 * <BR>
 * @author Buboltz <BR>
 * @version 1_0_F	03.07.2014  TB Erstimplementierung <BR>
 *          2_0_T   10.07.2014  TB PP Name in den Logausgaben aktualisiert, Fehlerbehandlung erweitert <BR> 
 *          3_0_F   10.07.2014  TB F-Version <BR> 
 *          4_0_T   03.11.2014  TB Weitere Ausgaben in APDM zur zus�tzlichen Dokumentation eingebaut <BR> 
 *          5_0_F   08.07.2015  TB Texte geringfuegig angepasst <BR>  
 *          6_0_T   24.03.2017  MKe Erweiterung Parallel Diagnose<BR>    
 *          7_0_F   24.03.2017  MKe F-Version<BR>    
 *          8_0_T   20.04.2017  MKe Parallel Diagnose Freigabe Proxy Thread hinzugef�gt.<BR>                    
 *          9_0_F   20.04.2017  MKe F-Version<BR>                
 */
public class SDiagEthernetBus_9_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public SDiagEthernetBus_9_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public SDiagEthernetBus_9_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "HWT", "DEBUG", "TAG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "SGBD", "SGBD_GATEWAY" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 *
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt
	 *         hat, ansonsten <code>true</code>.
	 */
	@Override
	public boolean checkArgs() {
		return super.checkArgs();
	}

	/** Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_ERROR;

		// spezifische Variablen
		String sgbd = null, sgbdGateway = null, job = null, hwt = null;
		String tag = null;

		DeviceManager devMan = null;
		boolean parallelDiagnose = false;

		// EDIABAS
		EdiabasProxyThread myEdiabas = null;
		// Client ECU
		final String JOB_CLIENT__STATUS_ETH_IP_CONFIGURATION = "STATUS_ETH_IP_CONFIGURATION";
		final String JOB_CLIENT__STATUS_ETH_IP_CONFIGURATION__ARGUMENTS = "0;1"; //0= request internal IP address, 1= send gratuitous ARL for the requested network interface (cp. EDIABAS docu)
		final String JOB_CLIENT__RESULT = "STAT_IP_CONFIGURATION";
		// Gateway ECU
		final String JOB_GATEWAY__STATUS_ETH_ARL_TABLE = "STATUS_ETH_ARL_TABLE";
		final String JOB_GATEWAY__STATUS_ETH_ARL_TABLE__ARGUMENTS = "0xff"; //0xff = ARL table of all ports (cp. EDIABAS docu)
		final String JOB_GATEWAY__RESULT_VLAN_COUNTS = "STAT_NUM_ARL_VLAN_ID_ENTRIES_WERT";
		final String JOB_GATEWAY__RESULT_VLAN_ID = "STAT_ARL_VLAN_ID_ENTRIES";

		final boolean DE = checkDE(); // Systemsprache DE wenn true

		/***********************************************************************
		 * Label in Transientdatei schreiben, die folgende Zeile ist auch n�tig
		 * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		 **********************************************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		try {
			//Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG abfragen
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
				} else {
					// noch Pr�fstandvariable pr�fen, ob Debug
					try {
						Object pr_var_debug;

						pr_var_debug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
						if( pr_var_debug != null ) {
							if( pr_var_debug instanceof String ) {
								if( "TRUE".equalsIgnoreCase( (String) pr_var_debug ) ) {
									bDebug = true;
								} else {
									bDebug = false;
								}
							} else if( pr_var_debug instanceof Boolean ) {
								if( ((Boolean) pr_var_debug).booleanValue() ) {
									bDebug = true;
								} else {
									bDebug = false;
								}
							}
						} else {
							bDebug = false;
						}

					} catch( VariablesException e ) {
						bDebug = false;
					}
				}

				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// SGBD
				sgbd = extractValues( getArg( "SGBD" ) )[0];

				// SGBD_GATEWAY
				sgbdGateway = extractValues( getArg( "SGBD_GATEWAY" ) )[0];

				//Hinweistext
				hwt = getArg( "HWT" );
				if( hwt == null )
					hwt = "";
				else
					hwt = PB.getString( hwt );

				// debug
				if( bDebug ) {
					System.out.println( "SDiagEthernetBus PP: Parameter processing Check is done. (SGBD: <" + sgbd + ">, SGBD_GATEWAY: <" + sgbdGateway + "> )" );
				}

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );

				if( bDebug ) {
					e.printStackTrace();
				}

				throw new PPExecutionException();
			}

			// Der eigentliche Check
			// Ausf�hrung EDIABAS Job

			// EDIABAS besorgen
			// MBa: Paralleldiagnose >>>>>
			try // Pr�fstandvariabel auslesen, ob Paralleldiagnose
			{
				Object pr_var;
				pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
				if( pr_var != null ) {
					if( pr_var instanceof String ) {
						if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
							parallelDiagnose = true;
						}
					} else if( pr_var instanceof Boolean ) {
						if( ((Boolean) pr_var).booleanValue() ) {
							parallelDiagnose = true;
						}
					}
				}
			} catch( VariablesException ex ) {
				if( bDebug ) {
					System.out.println( "Variable DIAG_PARALLEL nicht vorhanden: " + ex.getMessage() );
				}
			}

			// Devicemanager
			devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();

			if( getArg( "TAG" ) != null )
				tag = getArg( "TAG" );

			if( parallelDiagnose ) // Ediabas holen
			{
				try {
					myEdiabas = devMan.getEdiabasParallel( tag, sgbd );
				} catch( Exception ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Wrong Cascade configuration for parallel diagnosys", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				} catch( Throwable ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				}
			} else {
				try {
					myEdiabas = devMan.getEdiabas( tag );
				} catch( Exception ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();

				}
			}
			// <<<<< Paralleldiagnose

			try {
				// EDIABAS Job ausf�hren: Client MAC Adresse ermitteln und Update in der ARL Tabelle des Gateway instantan ansto�en
				String temp = myEdiabas.executeDiagJob( sgbd, job = JOB_CLIENT__STATUS_ETH_IP_CONFIGURATION, JOB_CLIENT__STATUS_ETH_IP_CONFIGURATION__ARGUMENTS, "" );
				if( temp.equals( "OKAY" ) == false ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, JOB_CLIENT__STATUS_ETH_IP_CONFIGURATION, JOB_CLIENT__STATUS_ETH_IP_CONFIGURATION__ARGUMENTS, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} else {
					result = new Ergebnis( "Status", "EDIABAS", sgbd, JOB_CLIENT__STATUS_ETH_IP_CONFIGURATION, JOB_CLIENT__STATUS_ETH_IP_CONFIGURATION__ARGUMENTS, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				}

				// MAC Adresse des Client Ethernet Partners merken
				String resultClientIPConfig = myEdiabas.getDiagResultValue( JOB_CLIENT__RESULT );
				String clientMACaddress = resultClientIPConfig.substring( resultClientIPConfig.length() - 17, resultClientIPConfig.length() );
				result = new Ergebnis( "CLIENT_MAC_ADDRESS", "EDIABAS", sgbd, "", "", "CLIENT_MAC_ADDRESS", clientMACaddress, clientMACaddress, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );
				if( bDebug ) {
					System.out.println( "SDiagEthernetBus PP: clientMACaddress is <" + clientMACaddress + ">" );
				}

				// EDIABAS Job ausf�hren: ARL Tabelle des Gateway einlesen
				temp = myEdiabas.executeDiagJob( sgbd = sgbdGateway, job = JOB_GATEWAY__STATUS_ETH_ARL_TABLE, JOB_GATEWAY__STATUS_ETH_ARL_TABLE__ARGUMENTS, "" );
				if( temp.equals( "OKAY" ) == false ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbdGateway, JOB_GATEWAY__STATUS_ETH_ARL_TABLE, JOB_GATEWAY__STATUS_ETH_ARL_TABLE__ARGUMENTS, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException();
				} else {
					result = new Ergebnis( "Status", "EDIABAS", sgbdGateway, JOB_GATEWAY__STATUS_ETH_ARL_TABLE, JOB_GATEWAY__STATUS_ETH_ARL_TABLE__ARGUMENTS, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				}

				// Anzahl der MAC Adressen in der ARL Tabelle des Gateway SG
				int macEntriesInARL = Integer.parseInt( myEdiabas.getDiagResultValue( 1, JOB_GATEWAY__RESULT_VLAN_COUNTS ) );
				if( bDebug ) {
					System.out.println( "SDiagEthernetBus PP: MAC addresses <" + macEntriesInARL + ">" );
				}

				// Die MAC Eintr�ge in der ARL Tabelle des ZGW analysieren
				for( int i = 1; i < macEntriesInARL + 1; i++ ) {
					String arlVlanID = myEdiabas.getDiagResultValue( i, JOB_GATEWAY__RESULT_VLAN_ID );

					// MAC Adressen der ARL Table einfach indiziert in APDM mitloggen
					result = new Ergebnis( "ARL_MAC_ADDRESS_" + i, "EDIABAS", sgbd, "", "", "ARL_MAC_ADDRESS_" + i, arlVlanID, arlVlanID, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );

					// Pr�fe ob die MAC Adresse des Clients in der ARL Tabelle des Gateway enthalten ist
					if( arlVlanID.indexOf( clientMACaddress ) != -1 ) {
						status = STATUS_EXECUTION_OK;

						result = new Ergebnis( "MAC_ADDRESS_FOUND", "EDIABAS", sgbd, "", "", "MAC_ADDRESS_FOUND", clientMACaddress, clientMACaddress, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}

					if( bDebug ) {
						System.out.println( "SDiagEthernetBus PP: loop <" + i + ">, result <" + arlVlanID + ">" );
					}
				}
			} catch( ApiCallFailedException exapicall ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "API_CALL", DE ? "falscher oder fehlerhafter API CALL" : "wrong or incorrect API CALL", "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), hwt, Ergebnis.FT_NIO );
				ergListe.add( result );

				throw new PPExecutionException();
			} catch( EdiabasResultNotFoundException exresult ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "RESULT", DE ? "Result nicht vorhanden" : "Result not found", "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), hwt, Ergebnis.FT_NIO );
				ergListe.add( result );

				throw new PPExecutionException();
			}

			// NIO Doku
			if( status != STATUS_EXECUTION_OK ) {
				result = new Ergebnis( "MAC_ADDRESS", "EDIABAS", sgbd, "", "", "MAC_ADDRESS", DE ? "MAC Adresse ungueltig oder nicht in ARL Tabelle enthalten" : "MAC address invalid or not in ARL table", DE ? "MAC Adresse in ARL Tabelle enthalten" : "ARL table contains MAC address", "", "0", "", "", "", PB.getString( "toleranzFehler1" ), hwt, Ergebnis.FT_NIO );
				ergListe.add( result );
			}
		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;

			// Debug Infos
			if( bDebug ) {
				e.printStackTrace();
			}
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;

			// Debug Infos
			if( bDebug ) {
				e.printStackTrace();
			}
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;

			// Debug Infos
			if( bDebug ) {
				e.printStackTrace();
			}
		}

		// MBa: Paralleldiagnose
		if( parallelDiagnose ) //&& ept!=null)
		{
			try {
				devMan.releaseEdiabasParallel( myEdiabas, tag, sgbd );
			} catch( DeviceLockedException ex ) {
				result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", ex.getMessage(), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Unexpected runtime error in releasing the parallel diagnosis", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable ex ) {
				result = new Ergebnis( "ExecFehler", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Unexpected runtime error in releasing the parallel diagnosis", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		//Status setzen
		setPPStatus( info, status, ergListe );
	}

}

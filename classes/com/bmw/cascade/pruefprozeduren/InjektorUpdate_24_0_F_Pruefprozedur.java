/*
 * InjektorUpdate Pruefprozedur.java Created on 06.12.06
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;

/**
 * Pr�fprozedure zur automatischen Erzeugung einer Motorcodierdatei aus einer bestehenden Motorcodierdatei und neuen Injektordaten (werden mit Handscanner eingelesen oder manuell eingegeben) Diese Pr�fprozedur unterst�tzt Injektordaten ab DDE7 (N47, N57, M57 US und Folgende) sowie Injektordaten f�r Benzinmotoren (N43, N53, N54, N63)
 * 
 * @autor Falk Gebauer, Claudia Wolf, Thomas Buboltz
 * @version 5_0_F FG 19.02.2007 Implementierung <br>
 *          6_0_F AU 12.02.2008 Erweiterung f�r M57 US Diesel (Kriegt ebenfalls ein neues Injektordatenformat, da DDE7) <br>
 *          7_0_F AU 12.02.2008 Version zum Injektortausch korrigiert f�r N47; Version unterst�tzt nun: N43, N53, N54, N63, N47, N57, M57 US Diesel <br>
 *          8_0_F FG 25.10.2010 Erweiterungen f�r Motoren N20, N26, N55, N56, S55, N63T�, S63T�, B38 und B48<br>
 *          8_0_T FG 08.02.2011 Anpassung f�r neues Injektorformat und 12 Zylindermotoren; G�ltige Motoren nun �ber Frontend parametrierbar <br>
 *          9_0_T CW 01.04.2011 Bugfix 8_0_T 11_0_T CW 15.02.2012 Bugfix f�r DieseInjektordaten <br>
 *          12_0_F CW 15.02.2012 Bugfix f�r DieseInjektordaten <br>
 *          13_0_T TB 28.03.2013 Anpassungen <BR>
 *          - APDM ben�tigt die Motornummer unbedingt im File selbst. Da dies kein "cod" File mehr ist, wird die Extension auf "rework" ge�ndert. <BR>
 *          - Das Verarbeiten der ge�nderten Motorinjektordaten ben�tigt etwas Zeit (Senden zum Server -> APDM schickt �nderung �ber MQS Queue an IPS-L -> IPS-L schickt den ge�nderten orderXML an APDM / CASCADE und CASCADE verteilt an die Clients). Deshalb nutze, falls vorhanden in erster Instanz die Daten aus der vorher angelegte lokale Backupdatei und erst falls keine vorhanden sind dann die aus dem Auftrag <BR>
 *          - Lesen der Motordaten aus den Serververzeichnissen wurde ausgebaut, entweder wir haben die Motordaten lokal in der Backupdatei oder im virtuellen Fahrzeug <BR>
 *          - Lokale Daten die �lter als DAYS (30) Tage sind, werden gel�scht. <BR>
 *          - rein deutsche Texte beim Parametercheck ersetzt durch die Nationalisierung aus der package.properties Datei. <BR>
 *          14_0_T MK 29.04.2013 Minimale �nderung im .rework File (AEND_INJ). <BR>
 *          15_0_T MK 10.06.2013 Minimale �nderung auch im lokalen Fall: .rework File (AEND_INJ). <BR>
 *          16_0_F MK <BR>
 *          17_0_T TB 09.07.2013 BugFix Motornummer wurde mehrfach eingef�gt. (Rausschneiden des zus�tzlichen FixStrings hatte gefehlt.) <BR> 
 *          18_0_F TB 09.07.2013 F-Version <BR>
 *          19_0_T MK 19.05.2014 Injektordaten werden beim Umbau ignoriert, da es sich um Dummy-Daten handelt. <BR>
 *			20_0_F MK 09.07.2014 F-Version
 *          21_0_T TB 13.06.2016 Bei der Eingabe die vom Scanner ermittelten Daten auf die L�nge des opt. Paramters: DATA_LENGTH k�rzen. <BR>
 *          22_0_F TB 14.06.2016 F-Version <BR>
 *          23_0_T MK 24.01.2017 Bei einem Injektortausch eines Dieselmotors kam es zu Problemen weil sich das Format der Injektordaten ge�ndert hat.<BR>
 *          24_0_F MK 14.06.2016 F-Version <BR>
 **/
public class InjektorUpdate_24_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	private boolean bDebug = true; // Debuginformationen freischalten

	LoadThread loadThread = null;

	LoadThread emergencyThread = null;

	final static int VARIANTE_DIESEL = 1;

	final static int VARIANTE_BENZIN_I = 2;

	final static int VARIANTE_BENZIN_II = 3;

	// entsprechende Injektortauschvariante
	int variante = 0;

	// Zylinderanzahl
	int zylinderAnzahl;

	// Neue Extension, weil die Datei jetzt noch den Namen enth�lt
	final static String EXTENSION_REWORK = "rework";

	// Zus�tzlich f�hrender Fixtext fuer APDM
	final static String AEND_INJ = "AEND_INJ";

	/**
	 * DE fuer Sprachermittlung
	 */
	private final boolean DE = checkDE(); // Systemsprache DE wenn true

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public InjektorUpdate_24_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	 * 
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public InjektorUpdate_24_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Setzt die aktuellen Parameter
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/** Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * Execute Funktion (non-Javadoc)
	 * 
	 * @see com.bmw.cascade.pruefprozeduren.Pruefprozedur#execute(com.bmw.cascade.pruefstand.pruefumfang.ExecutionInfo)
	 */
	public void execute( ExecutionInfo info ) {

		final boolean DE = checkDE(); // Systemsprache DE wenn true

		Vector ergListe = new Vector();
		Ergebnis result;
		int status = STATUS_EXECUTION_OK;

		// Krafstoffart konvertiert in Klartext
		String kraftstoff = null;

		// L�ndervariante aus Auftrag
		String laenderVariante = null;

		// MotorBaureihe aus Auftrag
		String motorBaureihe = null;

		// Motornummer aus Auftrag
		String motorNr = null;

		// zu tauschende Zylinder
		int[] cylinders = null;

		// Motordaten aus Auftrag
		String motorAbgleichDaten = null;

		// neu erstellte Motordaten
		String motorDatenNeu = null;

		// Fahrgestellnummer
		String fgNr = null;

		// Verzeichnis der zu schreibenden Motordaten
		File motorFileDir = null;

		// Informationsmeldungen anzeigen
		boolean infoMeldung = true;

		// Dauer der Anzeige der Informationsmeldungen in sek.
		int dauer = 5;

		// gelesenen Injektordaten; Feldgr��e entspricht Anzahl zu tauschender Injektoren
		String[] injData = null;

		// extrahierte IMA vom Injektor; Position im Feld bestimmt den konkreten Injektor
		String[] extIMA = new String[12];

		// Alle Diesel-Motoren welche den Injektortausch unterst�tzen
		String[] diesel_type = null;

		// Alle Benzin-Motoren welche den Injektortausch nach Variante 1 unterst�tzen
		String[] benzin_type_1 = null;

		// Alle Benzin-Motoren welche den Injektortausch nach Variante 2 unterst�tzen
		String[] benzin_type_2 = null;

		// Message wenn Dummy-Injektordaten
		String dummyInjector = DE ? "Injektoren k�nnen ohne Erfassung der Injektorwerte eingebaut werden." : "Injectors can be built in without injector data acquisition.";

		// Injektor Daten L�nge
		int length = 0;

		// DEBUG
		// Pr�fstandvariabel auslesen, ob Debug
		try {
			Object pr_var_debug;

			pr_var_debug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
			if( pr_var_debug != null ) {
				if( pr_var_debug instanceof String ) {
					if( "TRUE".equalsIgnoreCase( (String) pr_var_debug ) ) {
						bDebug = true;
					} else {
						bDebug = false;
					}
				} else if( pr_var_debug instanceof Boolean ) {
					if( ((Boolean) pr_var_debug).booleanValue() ) {
						bDebug = true;
					} else {
						bDebug = false;
					}
				}
			} else {
				bDebug = false;
			}

		} catch( VariablesException e ) {
			System.out.println( "FS-ANA: cannot read VariableModes.PSCONFIG, DEBUG, set Debug false" );
			bDebug = false;
		}

		// Parametercheck
		try {
			if( checkArgs() == false ) {
				result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), PB.getString( "parameterexistenz" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
			} else {

				// Verzeichnis initialisieren und ggf. vorher versuchen, dies zu erzeugen
				if( getArg( "PATH" ) != null ) {
					motorFileDir = new File( getArg( "PATH" ) );

					if( bDebug ) {
						System.out.println( "Parameter PATH = <" + motorFileDir.getCanonicalPath() + ">" );
					}

					// versuche noch zu erzeugen...
					if( !motorFileDir.exists() ) {
						motorFileDir.mkdirs();
					}

					// Verzeichnis vorhanden?
					if( motorFileDir.exists() == false ) {
						result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), PB.getString( "dir_exist" ), Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						motorFileDir = null;
						status = STATUS_EXECUTION_ERROR;
					} else {
						// Leserechte f�r Verzeichnis?
						if( motorFileDir.canRead() == false ) {
							result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), PB.getString( "dir_read" ), Ergebnis.FT_NIO_SYS );
							ergListe.add( result );
							motorFileDir = null;
							status = STATUS_EXECUTION_ERROR;
						} else {
							// Schreibrechte f�r Verzeichnis?
							if( motorFileDir.canWrite() == false ) {
								result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), PB.getString( "dir_write" ), Ergebnis.FT_NIO_SYS );
								ergListe.add( result );
								motorFileDir = null;
								status = STATUS_EXECUTION_ERROR;
							}
						}
					}
				}

				// es werden die m�glichen zu w�hlenden Zylinder aus dem Pr�flingseinstellungen
				// geholt
				try {
					zylinderAnzahl = new Integer( getArg( "ZYLINDER" ) ).intValue();
					if( bDebug )
						System.out.println( "cyl = " + zylinderAnzahl );
				} catch( NumberFormatException e ) {
					if( bDebug )
						e.printStackTrace();
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ) + " Zylinder", e.getMessage(), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException( PB.getString( "parametrierfehler" ) );
				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ) + " Zylinder", e.getMessage(), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException( PB.getString( "parametrierfehler" ) );
				}
				if( zylinderAnzahl > 12 )
					zylinderAnzahl = 12; // mehr als 12 Zylinder k�nnen nicht verarbeitet werden

				// Varianten auslesen
				if( getArg( "DIESEL" ) != null )
					diesel_type = extractValues( getArg( "DIESEL" ) );
				if( getArg( "BENZIN_1" ) != null )
					benzin_type_1 = extractValues( getArg( "BENZIN_1" ) );
				if( getArg( "BENZIN_2" ) != null )
					benzin_type_2 = extractValues( getArg( "BENZIN_2" ) );

				// optionale Argumente abholen
				try {
					if( (getArg( "INFO" ) != null) ) {
						if( (getArg( "INFO" )).equalsIgnoreCase( "true" ) )
							infoMeldung = true;
					}
					if( bDebug )
						System.out.println( "Infomeldung = " + infoMeldung );
				} catch( NumberFormatException e ) {
					if( bDebug )
						e.printStackTrace();
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ) + " INFO", e.getMessage(), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );

					throw new PPExecutionException( PB.getString( "parametrierfehler" ) );
				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ) + " INFO", e.getMessage(), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException( PB.getString( "parametrierfehler" ) );
				}

				try {
					if( getArg( "DAUER" ) != null )
						dauer = new Integer( getArg( "DAUER" ) ).intValue();
					if( bDebug )
						System.out.println( "Dauer = " + dauer );
				} catch( NumberFormatException e ) {
					if( bDebug )
						e.printStackTrace();
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ) + " DAUER", e.getMessage(), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException( PB.getString( "parametrierfehler" ) );
				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ) + " DAUER", e.getMessage(), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );

					throw new PPExecutionException( PB.getString( "parametrierfehler" ) );
				}

				try {
					if( getArg( "DATA_LENGTH" ) != null )
						length = new Integer( getArg( "DATA_LENGTH" ) ).intValue();
					if( bDebug )
						System.out.println( "Erlaubte Injektordaten Laenge = " + length );
				} catch( NumberFormatException e ) {
					if( bDebug )
						e.printStackTrace();
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ) + " DAUER", e.getMessage(), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException( PB.getString( "parametrierfehler" ) );
				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ) + " DAUER", e.getMessage(), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );

					throw new PPExecutionException( PB.getString( "parametrierfehler" ) );
				}

			}

			// Laufzeitumgebung des Userdialogs holen
			UserDialog myUserDialog = getPr�flingLaufzeitUmgebung().getUserDialog();

			// Kraftstoffart, Motornummer und motorBaureihe ermitteln
			motorBaureihe = this.getPr�fling().getAuftrag().getMotorBaureihe();
			laenderVariante = this.getPr�fling().getAuftrag().getL�nderVariante();
			fgNr = this.getPr�fling().getAuftrag().getFahrgestellnummer7();
			motorNr = this.getPr�fling().getAuftrag().getMotorNummer();
			String lokaleMotordaten = readMotorDatenLokal( motorNr );
			motorAbgleichDaten = lokaleMotordaten != null ? lokaleMotordaten : this.getPr�fling().getAuftrag().getMotorAbgleichDaten();

			if( bDebug )
				System.out.println( "gelesene Motorcodierdaten: " + motorAbgleichDaten );

			// Diesel
			if( this.getPr�fling().getAuftrag().getAntriebsKonzeption().equals( "D" ) ) {
				kraftstoff = "Diesel";

				// Pr�fe auf unterst�tzte Motorbaureihen
				if( diesel_type != null ) {
					for( int i = 0; i < diesel_type.length; i++ ) {
						if( diesel_type[i].equals( motorBaureihe ) ) {
							variante = VARIANTE_DIESEL;
							break;

						}
					}

					if( variante == 0 ) {
						myUserDialog.requestAlertMessage( PB.getString( "injector.title" ), MessageFormat.format( PB.getString( "Datenfehler.motor" ), new Object[] { fgNr, motorNr } ), 0 );
						result = new Ergebnis( "ParaFehler", "", "", "", motorBaureihe, "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), PB.getString( "Datenfehler.motor2" ), Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						throw new PPExecutionException( PB.getString( "Datenfehler.motor2" ) );

					}
				}
				// Benzin
			} else if( this.getPr�fling().getAuftrag().getAntriebsKonzeption().equals( "O" ) ) {

				kraftstoff = "Benzin";
				if( benzin_type_1 != null ) {
					// Pr�fe auf unterst�tzte Motorbaureihen
					for( int i = 0; i < benzin_type_1.length; i++ ) {
						if( benzin_type_1[i].equals( motorBaureihe ) ) {
							variante = VARIANTE_BENZIN_I;
							break;

						}
					}
				}
				if( benzin_type_2 != null && variante == 0 ) {
					for( int i = 0; i < benzin_type_2.length; i++ ) {
						if( benzin_type_2[i].equals( motorBaureihe ) ) {
							variante = VARIANTE_BENZIN_II;
							break;

						}
					}
				}

				if( variante == 0 ) {
					myUserDialog.requestAlertMessage( PB.getString( "injector.title" ), MessageFormat.format( PB.getString( "Datenfehler.motor" ), new Object[] { fgNr, motorNr } ), 0 );
					result = new Ergebnis( "ParaFehler", "", "", "", motorBaureihe, "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), PB.getString( "Datenfehler.motor2" ), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException( PB.getString( "Datenfehler.motor2" ) );
				}

			}

			// (kein Injektortausch f�r M57-US)
			if( (motorBaureihe.startsWith( "M57" ) && !(laenderVariante.startsWith( "USA" ))) ) {
				myUserDialog.requestAlertMessage( PB.getString( "injector.title" ), MessageFormat.format( PB.getString( "Datenfehler.motor" ), new Object[] { fgNr, motorNr } ), 0 );
				result = new Ergebnis( "ParaFehler", "", "", "", motorBaureihe, "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), PB.getString( "Datenfehler.motor2" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException( PB.getString( "Datenfehler.motor2" ) );
			}

			//			// Motordaten vom Server lesen (Datei) wenn Daten nicht im virtuellen Fahrzeug
			//			// verf�gbar
			//			if( motorAbgleichDaten == null ) {
			//				try {
			//					motorAbgleichDaten = getMotordaten( motorNr );
			//				} catch( Exception e ) {
			//					if( loadThread.getErrorMessage() != null ) {
			//						ergListe.add( loadThread.getErrorMessage() );
			//					} else if( emergencyThread.getErrorMessage() != null ) {
			//						ergListe.add( emergencyThread.getErrorMessage() );
			//					} else {
			//						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
			//						ergListe.add( result );
			//					}
			//					throw new PPExecutionException();
			//				}
			//			}
			//
			//			if( bDebug )
			//				System.out.println( "gelesene Motorcodierdaten: " + motorAbgleichDaten );

			// Ermittelte Fahrzeugdaten anzeigen

			if( bDebug )
				System.out.println( "Start Anzeige Userdialog" );

			if( infoMeldung ) {
				myUserDialog.requestStatusMessage( PB.getString( "injector.title" ), MessageFormat.format( PB.getString( "Daten.ud1" ), new Object[] { fgNr, motorBaureihe, kraftstoff, motorNr } ), dauer );
			}

			//LOP1775: Wenn Dummyinjektordaten im Auftrag stehen sollen die Injektoren ohne Erfassung gewechselt werden
			if( motorDatenInjektorenDummys( motorAbgleichDaten ) ) {
				myUserDialog.requestStatusMessage( PB.getString( "injector.title" ), dummyInjector, dauer );
				motorDatenNeu = "";
				//Zus�tzliches Result f�r Steuerung des weiteren Pr�fumfangablauf
				result = new Ergebnis( "DUMMYINJECTOR", "", "", "", motorNr, "Injector Dummy", "Dummy", "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );
			} else {
				// Bei welchem Zylinder soll der Injektor getauscht werden

				if( bDebug )
					myUserDialog.setAllowCancel( true ); // nur im Debug Abruch m�glich

				cylinders = myUserDialog.requestUserInputMultipleChoice( PB.getString( "injector.title" ), PB.getString( "Frage.zylinder" ), 0, zylinderWahl( zylinderAnzahl ) );

				// Kontrolle ob eingegebener Wert g�ltig ist (Integer zwischen 1 und max 12)
				if( cylinders == null || cylinders.length == 0 ) {
					result = new Ergebnis( "Benutzerabbruch", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "Benutzerabbruch" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					throw new PPExecutionException( PB.getString( "Benutzerabbruch" ) );
				}

				// Gr��e des injData[] anhand der zu wechselnden Injektoren bestimmen
				injData = new String[cylinders.length];

				// Daten des zu tauschenden Zylinders auslesen; Zylinder bestimmmt das Byte zum
				// Schreiben
				int i = 0;
				while( i < cylinders.length ) {
					myUserDialog.setAllowCancel( true );

					injData[i] = myUserDialog.requestUserInput( PB.getString( "injector.title" ), MessageFormat.format( PB.getString( "Frage.injector" ), new Object[] { new Integer( cylinders[i] + 1 ) } ), 0 );
					if( bDebug )
						System.out.println( "Ausgelesene Injektordaten f�r Zylinder " + (cylinders[i] + 1) + ": " + injData[i] );

					// Dateneingabe kontrollieren

					// Benutzerabbruch
					if( injData[i] == null ) {
						result = new Ergebnis( "Benutzerabbruch", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "Benutzerabbruch" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						throw new PPExecutionException( PB.getString( "Benutzerabbruch" ) );
					}

					// Laenge pr�fen und ggf. kuerzen
					if( length != 0 && injData[i].length() > length ) {
						injData[i] = injData[i].substring( 0, length );

						if( bDebug ) {
							System.out.println( "Injektordatum fuer Zylinder " + (cylinders[i] + 1) + " gekuerzt auf: " + injData[i] );
						}
					}

					try {

						// Dieselinjektoren
						if( variante == VARIANTE_DIESEL ) {

							if( bDebug )
								System.out.println( "Variante Diesel" );

							// L�nge des 3D-Barcodes beim Scannen des Injektors
							if( (injData[i].length() == 31) ) {

								// Diesel Daten konvertieren
								extIMA[cylinders[i]] = extracIMADiesel( injData[i] );
								if( bDebug )
									System.out.println( Integer.toString( i + 1 ) + " extrahierte IMA-Daten: " + extIMA[cylinders[i]] );
								i++;
								// verk�rzteHandeingabe des IMA Wertes
							} else if( injData[i].length() == 7 ) {

								// Wert direkt �bernehmen
								extIMA[cylinders[i]] = injData[i];
								if( bDebug )
									System.out.println( Integer.toString( i + 1 ) + " extrahierte IMA-Daten: " + extIMA[cylinders[i]] );
								i++;
								// Datenl�nge passt nicht zu Variante
							} else {
								// Fehlermeldungen anzeigen
								// keine Behandlung da bei Fehler falsche Dateneingabe vorliegt z.B
								myUserDialog.requestAlertMessage( PB.getString( "injector.title2" ), PB.getString( "Datenfehler.injektor" ), 0 );
								continue;
							}

							// Benzininjektoren Variante I
						} else if( variante == VARIANTE_BENZIN_I ) {

							if( bDebug )
								System.out.println( "Variante Benzin 1" );

							// L�nge des 3D-Barcodes beim Scannen des Injektors
							if( injData[i].length() == 28 ) {

								// Benzin Daten konvertieren und umformatieren
								extIMA[cylinders[i]] = formatCOD( extracIMABenzin_I( injData[i] ) );
								if( bDebug )
									System.out.println( Integer.toString( i + 1 ) + " extrahierte IMA-Daten: " + extIMA[cylinders[i]] );
								i++;
								// verk�rzte Handeingabe des IMA Wertes
							} else if( injData[i].length() == 6 ) {

								// Wert direkt �bernehmen und umformatieren
								extIMA[cylinders[i]] = formatCOD( injData[i] );
								if( bDebug )
									System.out.println( Integer.toString( i + 1 ) + " extrahierte IMA-Daten: " + extIMA[cylinders[i]] );
								i++;
								// Datenl�nge passt nicht zu Variante
							} else {
								// Fehlermeldungen anzeigen
								// keine Behandlung da bei Fehler falsche Dateneingabe vorliegt z.B
								myUserDialog.requestAlertMessage( PB.getString( "injector.title2" ), PB.getString( "Datenfehler.injektor" ), 0 );
								continue;
							}

							// Benzininjektoren Variante II
						} else if( variante == VARIANTE_BENZIN_II ) {

							if( bDebug )
								System.out.println( "Variante Benzin 2" );

							// L�nge des 3D-Barcodes beim Scannen des Injektors
							if( injData[i].length() == 36 ) {

								// Benzin Daten konvertieren und umformatieren
								extIMA[cylinders[i]] = formatCOD( extracIMABenzin_II( injData[i] ) );
								if( bDebug )
									System.out.println( Integer.toString( i + 1 ) + " extrahierte IMA-Daten: " + extIMA[cylinders[i]] );
								i++;
								// verk�rzte Handeingabe des IMA Wertes
							} else if( injData[i].length() == 3 ) {

								// Wert direkt �bernehmen und umformatieren
								extIMA[cylinders[i]] = formatCOD( injData[i] );
								if( bDebug )
									System.out.println( Integer.toString( i + 1 ) + " extrahierte IMA-Daten: " + extIMA[cylinders[i]] );
								i++;
								// Datenl�nge passt nicht zu Variante
							} else {
								// Fehlermeldungen anzeigen
								// keine Behandlung da bei Fehler falsche Dateneingabe vorliegt z.B
								myUserDialog.requestAlertMessage( PB.getString( "injector.title2" ), PB.getString( "Datenfehler.injektor" ), 0 );
								continue;
							}

						}

					} catch( Exception e ) {
						// Laufzeitfehler
						if( bDebug )
							e.printStackTrace();
						result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
						ergListe.add( result );
						throw new PPExecutionException( PB.getString( "unerwarteterLaufzeitfehler" ) );
					}
				}
				motorDatenNeu = changeMotorDatenInjektoren( motorAbgleichDaten, extIMA );

				if( bDebug )
					System.out.println( "ge�nderte Motordaten:" + motorDatenNeu );

				// l�sche alte lokale Motordaten, die �lter als DAYS [i.d.R. 30 Tage sind]
				deleteMotorDatenLokal();

				try {
					saveMotorDatenFile( motorDatenNeu, motorFileDir, motorNr );
				} catch( Exception e ) {
					if( bDebug )
						e.printStackTrace();
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "unerwarteterLaufzeitfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException( PB.getString( "ausfuehrungsfehler" ) );
				}

				// Dialog �ber erfolgreiches Auslesen anzeigen
				if( infoMeldung ) {
					if( status == STATUS_EXECUTION_OK )
						myUserDialog.requestStatusMessage( PB.getString( "injector.title" ), PB.getString( "injector.status" ), dauer );
					if( status == STATUS_EXECUTION_ERROR )
						myUserDialog.requestAlertMessage( PB.getString( "injector.title" ), PB.getString( "injector.status2" ), dauer );
				}
			}

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
			if( bDebug )
				e.printStackTrace();
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
			if( bDebug )
				e.printStackTrace();
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// IO Ergebnis erzeugen
		if( status == STATUS_EXECUTION_OK ) {
			result = new Ergebnis( "INJECTOR", "", "", "", motorNr, "Injector update OK", motorDatenNeu, motorAbgleichDaten, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			ergListe.add( result );
		}
		// Status setzen
		setPPStatus( info, status, ergListe );
	}

	/**
	 * Vergleicht die gelesenen Injektordaten. Wenn diese gleich sind handelt es sich um Dummy Werte. Siehe LOP 1775
	 */
	private boolean motorDatenInjektorenDummys( String motorDaten ) throws PPExecutionException {

		//unter welchen Bedingungen haben wir dummy daten?
		//3 Formate
		//Diesel --> hat immer Injektordaten --> MotorDatenInjektorenDummys=false;
		//Benzin_1 --> hat immer Injektordaten --> MotorDatenInjektorenDummys=false;
		//Benzin_2 --> Dummy-Daten, d.h. alle Injektoren haben den gleichen Wert

		//Standardbelegung false
		boolean motorDatenInjektorenDummys = false;

		if( variante == VARIANTE_BENZIN_II ) {
			//je Zylinder 1 Injektor
			int start = 165;
			int end = 165 + 20;
			String ersterInjektor = motorDaten.substring( start, end );
			for( int i = 1; i < zylinderAnzahl; i++ ) {
				//alle Zylinder nacheinander miteinander vergleichen
				//an Stelle 165 fangen die Injektoren an:
				start = end + 1;
				end = start + 20;
				String nachfolgenderInjektor = motorDaten.substring( start, end );
				if( nachfolgenderInjektor.equalsIgnoreCase( ersterInjektor ) ) {
					motorDatenInjektorenDummys = true;
					ersterInjektor = nachfolgenderInjektor;
				} else {
					motorDatenInjektorenDummys = false;
					break; //sind die Injektorwerte nicht gleich, so handelt es sich auch nicht um DummyInjektorenwerte
				}
			}
		}
		return motorDatenInjektorenDummys;
	}

	/**
	 * wandelt einen String in ein Hex-String
	 */
	private String hexFormat( String data ) {

		String injHex = new String();

		for( int i = 0; i < data.length(); i++ ) {
			int ch = (int) (data.charAt( i ));
			injHex = injHex + Integer.toHexString( ch );
		}

		return injHex;
	}

	/**
	 * Extrahiert aus den gelesen Daten die relevanten Werte f�r die Codierung der Dieselinjektoren f�r neues Injektorenformat ab DDE7 N47/M57 US, N57, Datenl�nge: 31 Zeichen char 13-19 relevant
	 */
	private String extracIMADiesel( String hex ) {

		String ima = hex.substring( 12, 19 );
		return ima;
	}

	/**
	 * Extrahiert aus den gelesen Daten die relevanten Werte f�r die Codierung der Benzininjektoren Datenl�nge: 28 Zeichen char 21-26 relevant
	 */
	private String extracIMABenzin_I( String hex ) {

		String ima = hex.substring( 20, 26 ); // Daten Injektor
		return ima;
	}

	/**
	 * Extrahiert aus den gelesen Daten die relevanten Werte f�r die Codierung der Benzininjektoren Datenl�nge: 36 Zeichen char 32-34 relevant
	 */
	private String extracIMABenzin_II( String hex ) {

		String ima = hex.substring( 31, 34 ); // Daten Injektor
		return ima;
	}

	/**
	 * formatiert die Injektordaten f�r Benzinmotoren der Variante I in das richtige Format f�r die Codierungsdatei 7 Byte HexFormat mit F�llwert "FF" im 7. Byte
	 */
	private String formatCOD( String ima ) {

		ima = hexFormat( ima ) + "FF";
		return ima;
	}

	/**
	 * konstruiert je nach Zylinderanzahl die Auswahl f�r den Benutzer
	 */
	private String[] zylinderWahl( int anzahl ) {

		String[] auswahl = new String[anzahl];
		for( int i = 0; i < anzahl; i++ ) {
			auswahl[i] = "Zylinder " + (i + 1);
		}
		return auswahl;
	}

	/**
	 * Konstruiert aus den Motorcodierdaten und den gelesen Injektordaten je nach gew�hlten Zylinder die neuen motorcodierdaten Unterscheidung zwischen Benzin und Diesel
	 */
	private String changeMotorDatenInjektoren( String motorDaten, String[] extIMA ) throws PPExecutionException {

		if( variante == VARIANTE_DIESEL )
			return changeMotorDatenInjektorenDiesel( motorDaten, extIMA );
		if( variante == VARIANTE_BENZIN_I && zylinderAnzahl != 12 )
			return changeMotorDatenInjektorenBenzin_I( motorDaten, extIMA );
		if( variante == VARIANTE_BENZIN_I && zylinderAnzahl == 12 )
			return changeMotorDatenInjektorenBenzin_12Zyl( motorDaten, extIMA );
		if( variante == VARIANTE_BENZIN_II )
			return changeMotorDatenInjektorenBenzin_II( motorDaten, extIMA );

		return null;
	}

	/**
	 * Konstruiert aus den Motorcodierdaten und den gelesen Injektordaten je nach gew�hlten Zylinder die neuen motorcodierdaten
	 */
	private String changeMotorDatenInjektorenDiesel( String motorDaten, String[] extIMA ) throws PPExecutionException {

		// neue Motordaten
		String neuMotorDaten = new String();

		// TeilString vor den Injektorwerten
		String dataBeforeIMA = new String();

		// originale IMA Werte aus den Motordaten
		String[] injIMAOriginal = new String[8];

		// TeilString nach den Injektorwerten
		String dataAfterIMA = new String();

		// Checksumme
		String chkSum = new String();

		try {

			if( bDebug )
				System.out.println( "Gesamt: " + motorDaten );
			// aufsplitten der Original Motordaten

			// nach dem 3. Leerzeichen im String motorDaten beginnen bei Dieselmotoren die Injektordaten
			int index = 0;
			for( int i = 0; i <= 2; i++ ) {
				index = motorDaten.indexOf( " ", index + 1 ) + 1;
			}

			// Daten vor den IMA Werten
			dataBeforeIMA = motorDaten.substring( 0, index );
			if( bDebug )
				System.out.println( "Teil 1: " + dataBeforeIMA );

			// IMA - Werte starten mit Byte 56 und sind jeweils 7 Byte lang
			// IMAs aus bisherigen MotorDaten �bernehmen

			for( int i = 0; i < 8; i++ ) {
				int pos = index + 10 * i;
				injIMAOriginal[i] = motorDaten.substring( pos, pos + 7 );
				if( bDebug )
					System.out.println( Integer.toString( i + 1 ) + " IMA alt: " + motorDaten.substring( pos, pos + 7 ) );
			}

			// Teil 3: Daten nach Injektordaten
			dataAfterIMA = motorDaten.substring( index + 10 * 7 + 7 );
			if( bDebug )
				System.out.println( "Teil 3: " + dataAfterIMA );

			// neue Motordaten zusammensetzen
			neuMotorDaten = dataBeforeIMA;

			for( int k = 0; k < 8; k++ ) {
				if( extIMA[k] != null ) {
					neuMotorDaten = neuMotorDaten + extIMA[k];
					if( bDebug )
						System.out.println( Integer.toString( k + 1 ) + " IMA neu: " + extIMA[k] );
				} else {
					neuMotorDaten = neuMotorDaten + injIMAOriginal[k];
					if( bDebug )
						System.out.println( Integer.toString( k + 1 ) + " IMA alt: " + injIMAOriginal[k] );
				}
				// alle Injektorwerte durch Komma trennen
				if( k != 7 )
					neuMotorDaten = neuMotorDaten + " , ";
			}

			neuMotorDaten = neuMotorDaten + dataAfterIMA;

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			throw new PPExecutionException( PB.getString( "Ausfuehrungsfehler_Format_Diesel" ) );
		}

		if( bDebug )
			System.out.println( "Neu: " + neuMotorDaten );

		// Checksumme entfernen
		neuMotorDaten = neuMotorDaten.substring( 0, neuMotorDaten.length() - 1 );

		// Checksumme f�r Datei bilden
		chkSum = buildChecksumASCII( neuMotorDaten );
		neuMotorDaten = neuMotorDaten + chkSum;

		return neuMotorDaten;
	}

	/**
	 * Konstruiert aus den Motorcodierdaten und den gelesen Injektordaten je nach gew�hlten Zylinder die neuen motorcodierdaten Variante Benzin I
	 */
	private String changeMotorDatenInjektorenBenzin_I( String motorDaten, String[] extIMA ) throws PPExecutionException {

		// neue Motordaten
		String neuMotorDaten = new String();

		// TeilString vor den Injektorwerten
		String dataBeforeIMA = new String();

		// TeilString nach den Injektorwerten
		String dataAfterIMA = new String();

		// originale IMA Werte aus den Motordaten
		String[] injIMAOriginal = new String[8];

		// Checksumme
		String chkSum = new String();

		try {
			// Leerzeichen entfernen
			motorDaten = motorDaten.replaceAll( " ", "" );
			if( bDebug )
				System.out.println( "Gesamt: " + motorDaten );

			// IMA - Werte starten mit Byte 56 und sind jeweils 7 Byte lang
			// 1. Teil (1-110) + 1-8 IMA (ermittelt aus Zylinder + 3.Teil (224-234)

			// Teil 1: Daten vor Injektordaten
			dataBeforeIMA = motorDaten.substring( 0, 110 );

			if( bDebug )
				System.out.println( "Teil 1: " + dataBeforeIMA );

			// Teil 2: Injektordaten
			for( int i = 0; i < 8; i++ ) {
				int pos = 110 + 14 * i;
				injIMAOriginal[i] = motorDaten.substring( pos, pos + 14 );
				if( bDebug )
					System.out.println( Integer.toString( i + 1 ) + " IMA alt: " + motorDaten.substring( pos, pos + 14 ) );
			}

			// Teil 3: Daten nach Injektordaten
			dataAfterIMA = motorDaten.substring( 222 );
			if( bDebug )
				System.out.println( "Teil 3: " + dataAfterIMA );

			// neue Motordaten zusammensetzen
			neuMotorDaten = dataBeforeIMA;

			for( int k = 0; k < 8; k++ ) {
				if( extIMA[k] != null ) {
					neuMotorDaten = neuMotorDaten + extIMA[k];
					if( bDebug )
						System.out.println( Integer.toString( k + 1 ) + " IMA neu: " + extIMA[k] );
				} else {
					neuMotorDaten = neuMotorDaten + injIMAOriginal[k];
					if( bDebug )
						System.out.println( Integer.toString( k + 1 ) + " IMA alt: " + injIMAOriginal[k] );
				}
			}

			neuMotorDaten = neuMotorDaten + dataAfterIMA;

			if( bDebug )
				System.out.println( "Neu: " + neuMotorDaten );

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			throw new PPExecutionException( PB.getString( "Ausfuehrungsfehler_Format_Benzin" ) );
		}

		// Checksumme entfernen
		neuMotorDaten = neuMotorDaten.substring( 0, neuMotorDaten.length() - 1 );

		// Checksumme neu berechnen f�r Motordaten ohne Leerzeichen
		chkSum = buildChecksumHex( neuMotorDaten.replaceAll( " ", "" ) );

		// Format wiederherstellen
		neuMotorDaten = this.reorganizeFormatBenzin( neuMotorDaten );

		if( bDebug )
			System.out.println( "Format formatiert: " + neuMotorDaten );

		// Checksumme anf�gen
		neuMotorDaten = neuMotorDaten + chkSum;

		return neuMotorDaten;

	}

	/**
	 * Konstruiert aus den Motorcodierdaten und den gelesen Injektordaten je nach gew�hlten Zylinder die neuen motorcodierdaten nach Variante BENZIN II gegen�ber den anderen Varianten werden nicht die gesamten neuen Injektordaten �bergeben (6 Zeichen) sondern nur noch der Durchflusswert (3 Zeichen / 2. Teil)
	 */
	private String changeMotorDatenInjektorenBenzin_II( String motorDaten, String[] extIMA ) throws PPExecutionException {

		// neue Motordaten
		String neuMotorDaten = new String();

		// TeilString vor den Injektorwerten
		String dataBeforeIMA = new String();

		// TeilString nach den Injektorwerten
		String dataAfterIMA = new String();

		// originale IMA Werte aus den Motordaten
		String[] injIMAOriginal = new String[8];

		// Checksumme
		String chkSum = new String();

		try {
			// Leerzeichen entfernen
			motorDaten = motorDaten.replaceAll( " ", "" );
			if( bDebug )
				System.out.println( "Gesamt: " + motorDaten );

			// IMA - Werte starten mit Byte 56 und sind jeweils 7 Byte lang
			// 1. Teil (1-110) + 1-8 IMA (ermittelt aus Zylinder + 3.Teil (224-234)

			// Teil 1: Daten vor Injektordaten
			dataBeforeIMA = motorDaten.substring( 0, 110 );

			if( bDebug )
				System.out.println( "Teil 1: " + dataBeforeIMA );

			// Teil 2: Injektordaten
			for( int i = 0; i < 8; i++ ) {
				int pos = 110 + 14 * i;
				injIMAOriginal[i] = motorDaten.substring( pos, pos + 14 );
				if( bDebug )
					System.out.println( Integer.toString( i + 1 ) + " IMA alt: " + motorDaten.substring( pos, pos + 14 ) );
			}

			// Teil 3: Daten nach Injektordaten
			dataAfterIMA = motorDaten.substring( 222 );
			if( bDebug )
				System.out.println( "Teil 3: " + dataAfterIMA );

			// neue Motordaten zusammensetzen
			neuMotorDaten = dataBeforeIMA;

			for( int k = 0; k < 8; k++ ) {
				if( extIMA[k] != null ) {
					neuMotorDaten = neuMotorDaten + injIMAOriginal[k].substring( 0, 6 ) + extIMA[k];
					if( bDebug )
						System.out.println( Integer.toString( k + 1 ) + " IMA neu: " + injIMAOriginal[k].substring( 0, 6 ) + extIMA[k] );
				} else {
					neuMotorDaten = neuMotorDaten + injIMAOriginal[k];
					if( bDebug )
						System.out.println( Integer.toString( k + 1 ) + " IMA alt: " + injIMAOriginal[k] );
				}
			}

			neuMotorDaten = neuMotorDaten + dataAfterIMA;

			if( bDebug )
				System.out.println( "Neu Gesamt: " + neuMotorDaten );

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			throw new PPExecutionException( PB.getString( "Ausfuehrungsfehler_Format_Benzin" ) );
		}

		// Checksumme entfernen
		neuMotorDaten = neuMotorDaten.substring( 0, neuMotorDaten.length() - 1 );

		// Checksumme neu berechnen f�r Motordaten ohne Leerzeichen
		chkSum = buildChecksumHex( neuMotorDaten.replaceAll( " ", "" ) );

		// Format wiederherstellen
		neuMotorDaten = this.reorganizeFormatBenzin( neuMotorDaten );

		if( bDebug )
			System.out.println( "Format formatiert: " + neuMotorDaten );

		// Checksumme anf�gen
		neuMotorDaten = neuMotorDaten + chkSum;

		return neuMotorDaten;

	}

	/**
	 * Konstruiert aus den Motorcodierdaten und den gelesen Injektordaten je nach gew�hlten Zylinder die neuen motorcodierdaten nach Variante BENZIN II gegen�ber den anderen Varianten werden nicht die gesamten neuen Injektordaten �bergeben (6 Zeichen) sondern nur noch der Durchflusswert (3 Zeichen / 2. Teil)
	 */
	private String changeMotorDatenInjektorenBenzin_12Zyl( String motorDaten, String[] extIMA ) throws PPExecutionException {

		// neue Motordaten
		String neuMotorDaten = new String();

		// TeilString nach den Injektorwerten
		String dataAfterIMA = new String();

		// originale IMA Werte aus den Motordaten
		String[] injIMAOriginal = new String[12];

		// Checksumme
		String chkSum = new String();

		try {
			// Leerzeichen entfernen
			motorDaten = motorDaten.replaceAll( " ", "" );
			if( bDebug )
				System.out.println( "Gesamt: " + motorDaten );

			// Teil 1: Injektordaten
			for( int i = 0; i < 12; i++ ) {
				int pos = 14 * i;
				injIMAOriginal[i] = motorDaten.substring( pos, pos + 14 );
				if( bDebug )
					System.out.println( Integer.toString( i + 1 ) + " IMA alt: " + motorDaten.substring( pos, pos + 14 ) );
			}

			// Teil 2: Daten nach Injektordaten
			dataAfterIMA = motorDaten.substring( 168 );
			if( bDebug )
				System.out.println( "Teil 3: " + dataAfterIMA );

			for( int k = 0; k < 12; k++ ) {
				if( extIMA[k] != null ) {
					neuMotorDaten = neuMotorDaten + extIMA[k];
					if( bDebug )
						System.out.println( Integer.toString( k + 1 ) + " IMA neu: " + extIMA[k] );
				} else {
					neuMotorDaten = neuMotorDaten + injIMAOriginal[k];
					if( bDebug )
						System.out.println( Integer.toString( k + 1 ) + " IMA alt: " + injIMAOriginal[k] );
				}
			}

			neuMotorDaten = neuMotorDaten + dataAfterIMA;

			if( bDebug )
				System.out.println( "Neu: " + neuMotorDaten );

		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			throw new PPExecutionException( PB.getString( "Ausfuehrungsfehler_Format_Benzin" ) );
		}

		// Checksumme entfernen
		neuMotorDaten = neuMotorDaten.substring( 0, neuMotorDaten.length() - 1 );

		// Checksumme neu berechnen f�r Motordaten ohne Leerzeichen
		chkSum = buildChecksumHex( neuMotorDaten.replaceAll( " ", "" ) );

		// Format wiederherstellen
		neuMotorDaten = this.reorganizeFormatBenzin( neuMotorDaten );

		if( bDebug )
			System.out.println( "Format formatiert: " + neuMotorDaten );

		// Checksumme anf�gen
		neuMotorDaten = neuMotorDaten + chkSum;

		return neuMotorDaten;

	}

	/**
	 * Versucht die Motordaten vom Server zu beziehen
	 * 
	 * @param motorNummer
	 * @return gelesene Motordaten
	 * @throws PPExecutionException
	 */
	private String getMotordaten( String motorNummer ) throws PPExecutionException {

		boolean tryEmergency = false;
		String motorFileName = "";
		String motorFileNameEmergency = "";
		String motorFileEmergencyPath = "";

		Monitor myMonitor = null;
		Monitor emergencyMonitor = null;
		String motorNr = motorNummer;
		String motorDaten = null;

		// Thread initialisieren, um Motordaten vom Server zu holen. Sollte das Netzlaufwerk nicht
		// verf�gbar sein,
		// so l�uft der Dateiladethread zu Ende. Der Hauptthread der Pr�fprozedur beendet sich nach
		// einem
		// bestimmten timeout.
		loadThread = new LoadThread();

		// Monitor, zur Kommunikation zwischen Hauptthread und Ladethread. Ist notwendig, da der
		// Ladethread bei
		// Fehlendem Netzwerk erst nach erhaltenem Netztimeout(ca.30 sec.) reagiert.
		myMonitor = new Monitor();

		// Bestimmung des Suchpfades f�r die Motordatendatei
		motorFileName = com.bmw.cascade.extern.roh2xml.auftrag.PB.getString( "MCD_IN_DIR" ) + File.separator + motorNr + "." + com.bmw.cascade.extern.roh2xml.auftrag.PB.getString( "MCD_EXTENSION" );

		// Setze die Parameter f�r den Thread
		loadThread.setPar( motorFileName, motorNr, myMonitor );

		// Lade-Thread starten, im Monitor den Threadstatus auf aktiv setzen
		myMonitor.setThreadState( true );
		loadThread.start();

		// holt sich den Threadstatus und wartet im Monitor so lange, bis der Thread fertig ist,
		// jedoch maximal bis zum Timeout. L�uft der Thread nach Ablauf des Timeouts immer noch,
		// so ist die Verbindung zum Netzlaufwerk nicht m�glich und es wird abgebrochen. Der Thread
		// l�uft im Hintergrund aus.
		if( myMonitor.getThreadState() ) {

			// Notfallpfad versuchen, da Server �ber Netz nicht erreichbar
			tryEmergency = true;
		} else {
			if( !loadThread.loadOk() ) {

				// Server erreichbar, aber Motordaten nicht vorhanden, Notfallpfad checken.
				tryEmergency = true;
			}
		}

		// Es wird versucht, die Motordaten �ber den Notfallpfad zu erreichen,
		// falls diese Propertie gesetzt wurde.
		if( tryEmergency ) {
			try {
				motorFileEmergencyPath = com.bmw.cascade.extern.roh2xml.auftrag.PB.getString( "MCD_EMERGENCY_DIR" );
			} catch( Exception e ) {
				throw new PPExecutionException( PB.getString( "Ausfuehrungsfehler_hole_MCD" ) );
			}
			if( !motorFileEmergencyPath.trim().equalsIgnoreCase( "" ) ) {

				// Notfallthread initialisieren
				emergencyThread = new LoadThread();

				// Monitor f�r die Kommunikation mit dem Notfallthread
				emergencyMonitor = new Monitor();

				// festlegen des Notfalldatensuchpfads
				motorFileNameEmergency = motorFileEmergencyPath + File.separator + motorNr + "." + com.bmw.cascade.extern.roh2xml.auftrag.PB.getString( "MCD_EXTENSION" );

				// Setze Parameter f�r Notfallthread
				emergencyThread.setPar( motorFileNameEmergency, motorNr, emergencyMonitor );

				// Starte Notfallthread
				emergencyMonitor.setThreadState( true );
				emergencyThread.start();

				// Der Thread l�uft weiter bis der Netzwerktimeout kommt und beendet sich dann, das
				// ist
				// zu lange. Die Pr�fprozedur beendet sich nach dem Warten im Monitor
				if( emergencyMonitor.getThreadState() ) {
					throw new PPExecutionException( PB.getString( "Ausfuehrungsfehler_hole_MCD_Notlauf" ) );
				}
			} else {
				throw new PPExecutionException( PB.getString( "ausfuehrungsfehler" ) );
			}
		}

		// Netzlaufwerk vorhanden und entsprechend des Threadergebnisses reagieren
		// Wenn Notfallthread erforderlich, dann Notfallthread auswerten.
		//
		if( emergencyThread != null ) {
			if( emergencyThread.loadOk() ) {
				motorDaten = emergencyThread.getMotorDaten();
			} else {
				throw new PPExecutionException( PB.getString( "Ausfuehrungsfehler_hole_MCD_Notlauf" ) );
			}
		} else {
			if( loadThread.loadOk() ) {
				motorDaten = loadThread.getMotorDaten();
			} else {
				throw new PPExecutionException( PB.getString( "ausfuehrungsfehler" ) );
			}
		}

		return motorDaten;
	}

	/**
	 * Alte lokale Motordaten l�schen, damit nichts voll�uft (�lter als DAYS [i.d.R. 30 Tage])
	 */
	private void deleteMotorDatenLokal() {
		File motorDir = new File( PB.getString( "injector.motorFileDir" ) );

		//l�sche vorher alte lokale Daten (�lter als DAYS), damit nichts voll l�uft
		try {
			for( File oldFile : motorDir.listFiles( new ExtensionFileNameFilter() ) ) {
				oldFile.delete();
			}
		} catch( Throwable thr ) {
			if( bDebug ) {
				thr.printStackTrace();
			}
		}
	}

	/**
	 * Lokale MotordatenDatei lesen
	 * 
	 * @param motorNummer
	 * @return Motordaten oder null
	 */
	private String readMotorDatenLokal( String motorNummer ) {
		File lokalMotorFile = new File( PB.getString( "injector.motorFileDir" ) + motorNummer + "." + EXTENSION_REWORK );
		String motordaten = null;

		if( lokalMotorFile.exists() ) {
			FileInputStream fis = null;
			try {
				fis = new FileInputStream( lokalMotorFile );
				int len = (int) lokalMotorFile.length();
				byte[] buffer = new byte[len];
				fis.read( buffer, 0, len );
				fis.close();

				motordaten = (new String( buffer )).trim().substring( AEND_INJ.length() + motorNummer.length() ); //Die f�hrenden FixStrings "AEND_INJ" + Motornummer noch rausk�rzen
			} catch( IOException e ) {
				if( bDebug )
					e.printStackTrace();
			} finally {
				if( fis != null ) {
					try {
						fis.close();
					} catch( IOException e ) {
						// Nothing
					}
				}
			}
			return motordaten;
		} else {
			return null;
		}
	}

	/**
	 * die Motorcodierdatei in das angegebene Verzeichnis schreiben
	 */
	private void saveMotorDatenFile( String daten, File directory, String nummer ) throws PPExecutionException {

		String motorDaten = daten;
		String motorNr = nummer;
		File motorDatei = null;
		File motorDir = null;

		FileOutputStream fosLocal = null;
		FileOutputStream fosServer = null;

		try {
			// Schreibe zuerst mal zus�tzlich das lokale Backup
			String s_backupPath = PB.getString( "injector.motorFileDir" );
			// Schl�ssel muss aufgel�st wurden sein
			if( !s_backupPath.equals( "injector.motorFileDir" ) ) {

				motorDir = new File( s_backupPath );
				if( !motorDir.exists() )
					motorDir.mkdirs();

				motorDatei = new File( motorDir + File.separator + motorNr + "." + EXTENSION_REWORK );
				fosLocal = new FileOutputStream( motorDatei );
				String content = AEND_INJ + nummer + motorDaten;
				byte[] bytes = content.getBytes();
				fosLocal.write( bytes );
				fosLocal.close();
			}

			// Versuche Datei auf dem konfigurierten Verzeichnispfad zu schreiben
			if( directory != null ) {
				motorDatei = new File( directory.getAbsolutePath() + File.separator + motorNr + "." + EXTENSION_REWORK );
				fosServer = new FileOutputStream( motorDatei );
				String content = AEND_INJ + nummer + motorDaten;
				byte[] bytes = content.getBytes();
				fosServer.write( bytes );
				fosServer.close();
			}
		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			throw new PPExecutionException( PB.getString( "Ausfuehrungsfehler_Speichern" ) );
		} catch( Throwable thr ) {
			if( bDebug )
				thr.printStackTrace();
			throw new PPExecutionException( PB.getString( "Ausfuehrungsfehler_Speichern" ) );
		} finally {
			if( fosLocal != null ) {
				try {
					fosLocal.close();
				} catch( IOException e ) {
				}
			}

			if( fosServer != null ) {
				try {
					fosServer.close();
				} catch( IOException e ) {
				}
			}
		}
	}

	/**
	 * Ermittelt die Checksumme (Modulo 36) f�r die Motordatendatei im HEX Zeichensatz
	 */
	private String buildChecksumHex( String data ) {

		boolean toggle = true;
		int coffset = 55; // Buschstabenoffste damit A = 10 bis Z = 36
		int doffset = 48; // Ziffernoffset damit "0" in hex auch "0" in int
		int sum = 0;
		int chint = 0;
		int result;

		char[] charr = data.toCharArray();
		for( int i = 0; i < charr.length; i++ ) {
			if( Character.isDigit( charr[i] ) ) {
				chint = charr[i] - doffset;
			} else {
				chint = charr[i] - coffset;
			}
			if( toggle ) {
				toggle = false;
				sum += (chint * 3);
			} else {
				toggle = true;
				sum += chint;
			}
		}

		result = sum % 36;
		if( result <= 9 ) {
			result += doffset;
		} else {
			result += coffset;
		}

		return (new Character( (char) result ).toString());
	}

	/**
	 * erzeugt die Checksumme (Modulo36) f�r ASCII Zeichesatz
	 */
	private String buildChecksumASCII( String data ) {

		int i = 0;
		char web;
		int pruefziffer_calc;
		int wert1 = 0;

		data = data.trim();
		data = data.replaceAll( " ", "" );

		// ungerade Stellen:
		do {
			web = data.charAt( i );
			if( (web > 0x20) && (web < 0x5B) ) {
				if( web < 'A' ) {
					wert1 = wert1 + web - '0';
				} else {
					wert1 = wert1 + web - 'A' + 10;
				}
			}
			i = i + 2;
		} while( i < data.length() );
		wert1 = wert1 * 3; // zus. Berechnungsvorschrift f�r ungerade Stellen

		// Gerade Stellen:
		i = 1;
		int wert2 = 0;
		do {
			web = data.charAt( i );
			if( (web > 0x20) && (web < 0x5B) ) {
				if( web < 'A' ) {
					wert2 = wert2 + web - '0';
				} else {
					wert2 = wert2 + web - 'A' + 10;
				}
			}
			i = i + 2;
		} while( i < data.length() );

		pruefziffer_calc = (wert1 + wert2);
		pruefziffer_calc = pruefziffer_calc % 36;
		if( pruefziffer_calc < 10 ) {
			pruefziffer_calc = pruefziffer_calc + '0';
		} else {
			pruefziffer_calc = pruefziffer_calc + 'A' - 10;
		}

		return new Character( (char) pruefziffer_calc ).toString();
	}

	/**
	 * Ausgangsformat der Orginaldatei wiederherstellen
	 */
	private String reorganizeFormatBenzin( String data ) {

		String newFormat = new String();

		for( int i = 0; i < data.length(); i = i + 2 ) {
			newFormat = newFormat + data.substring( i, i + 2 ) + " ";
		}
		return newFormat;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see com.bmw.cascade.pruefprozeduren.Pruefprozedur#getRequiredArgs()
	 */
	public String[] getRequiredArgs() {
		String[] args = { "PATH", "ZYLINDER" };
		return args;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see com.bmw.cascade.pruefprozeduren.Pruefprozedur#getOptionalArgs()
	 */
	public String[] getOptionalArgs() {
		String[] args = { "INFO", "DAUER", "DIESEL", "BENZIN_1", "BENZIN_2", "DATA_LENGTH" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert. �berschreibt die parent-Methode aufgrund der offenen Anzahl an Results
	 */
	public boolean checkArgs() {

		try {
			// 1. Check: Sind alle requiredArgs gesetzt
			String[] requiredArgs = getRequiredArgs();
			for( int i = 0; i < requiredArgs.length; i++ ) {
				if( getArg( requiredArgs[i] ) == null )
					return false;
				if( getArg( requiredArgs[i] ).equals( "" ) == true )
					return false;
			}
			return true;
		} catch( Exception ex ) {
			if( bDebug )
				ex.printStackTrace();
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * Lokale Dateien, die �lter als DAYS sind l�schen
	 */
	private class ExtensionFileNameFilter implements FilenameFilter {
		final int DAYS = 30; // 30 Tage, nur das nichts voll�uft

		private long getDate() {
			Calendar c = Calendar.getInstance();
			c.add( Calendar.DATE, -DAYS );

			return c.getTimeInMillis();
		}

		public boolean accept( File f, String s ) {
			return s.toLowerCase().endsWith( "." + EXTENSION_REWORK ) && f.lastModified() < getDate();
		}
	}

	/**
	 * Interne Klasse f�r den Monitor, um den Threadstatus zwischen Hauptthread und LadeThread auszutauschen
	 */
	private class Monitor {
		private boolean threadrunning;

		public Monitor() {
			super();
			threadrunning = false;
		}

		synchronized public void setThreadState( boolean state ) {
			threadrunning = state;
			notify();
		}

		synchronized public boolean getThreadState() {
			if( threadrunning ) {
				try {
					// timeout 4 sec., falls Netzwerk nicht da ist
					wait( 4000 );

				} catch( Exception e ) {
				}
			}
			return threadrunning;
		}
	}

	/**
	 * Thread zum laden der Motordaten, um Timeouts bei nicht vorhandenem Server zu verk�rzen
	 */
	private class LoadThread extends Thread {

		private String motorFileName = null;
		private boolean loadOK = false;
		private Ergebnis errorMessage;
		private String motorDaten = null;
		private String motorNr = null;
		private Monitor myMonitor = null;

		public void run() {
			try {

				// Versuche Datei auf dem Datenlaufwerk zu finden
				File motorDatei = new File( motorFileName );

				if( motorDatei.exists() == false )
					throw new IOException( "File doesn't exist" );

				if( motorDatei.canRead() == false )
					throw new IOException( "Can�t read file" );

				if( motorDatei.length() <= 0 )
					throw new IOException( "File empty" );

				FileInputStream fis = new FileInputStream( motorDatei );
				byte[] bytes = new byte[(int) (motorDatei.length())];
				if( fis.read( bytes, 0, ((int) motorDatei.length()) ) != ((int) motorDatei.length()) )
					throw new IOException( "File doesn't exist" );
				fis.close();

				// Ladevorgang war in Ordnung
				loadOK = true;
				motorDaten = (new String( bytes )).trim();

				// Thread ist fertig und beendet sich
				myMonitor.setThreadState( false );

			} catch( IOException e ) {
				// Datei nicht vorhanden
				errorMessage = new Ergebnis( "UpdateFehler", "IOFehler", motorNr, "", "", "", "", "", "", "0", "", "", "", PB.getString( "motorDatenUpdateFile" ) + " " + motorFileName, e.getMessage(), Ergebnis.FT_NIO );
				loadOK = false;
				myMonitor.setThreadState( false );
			} catch( Exception e ) {
				errorMessage = new Ergebnis( "UpdateFehler", "Exception", motorNr, "", "", "", "", "", "", "0", "", "", "", PB.getString( "motorDatenUpdateInit" ), e.getMessage(), Ergebnis.FT_NIO );
				loadOK = false;
				System.out.println( "Ladethread mit Fehler abgebrochen: " + e.getMessage() );
				myMonitor.setThreadState( false );
			}
		}

		public void setPar( String motorFileName, String motornummer, Monitor myMonitor ) {
			this.motorFileName = motorFileName;
			this.motorNr = motornummer;
			this.myMonitor = myMonitor;
		}

		public boolean loadOk() {
			return loadOK;
		}

		public Ergebnis getErrorMessage() {
			return errorMessage;
		}

		public String getMotorDaten() {
			return motorDaten;
		}

	}// end inner class

}// end Class

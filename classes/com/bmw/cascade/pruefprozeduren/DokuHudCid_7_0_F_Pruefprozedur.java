package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.util.dom.DomPruefstand;

/**
 * Dokumentation von E/E Seriennummerdaten der mit der LK neuen intelligenten Sensor SG HUD und CID.<BR>
 * <BR>
 * Funktion:<BR>
 *    - Von diesen SG werden die Sachnummer partNo und die Serien- oder Herstellernummer dokumentiert.<BR>
 *    - Alle Daten werden �ber @ Operator aus dem virtuellen Fzg. �bergeben.(Zur Fehlervermeidung [Netzwerk] m�ssen die SG Daten am gleichen Pr�fstand ermittelt werden)<BR>
 *    - Folgende Formatgrenzen gelten partNo: max Char(7) numerisch, serialNo: max Char(10) numerisch oder alphanumerisch<BR>
 *    - Es k�nnen je nach Fahrzeugkonfiguration entweder von beiden SG Daten kommen oder nur von einem bzw. gar nicht.<BR>
 *    - Die intelligenten SG HUD und CID h�ngen an diagnosef�higen Eltern SG (HUD am Kombi und das CID am HU-CIC, Champ oder NBT).<BR>     
 * <BR>
 * Hintergrund:<BR>
 *    - seitens Entwicklung unterst�tzen die neuen "Sensor" SG HUD / CID aktuell nicht den generischen Job Sensordaten lesen<BR>
 *    - aufgrund dessen k�nnen sie nicht �ber den generischen Weg DokuChipcard dokumentiert werden, weiterhin wird bei Problemen das gesamte Chipcard DokuFile nicht erzeugt<BR>
 * @author BMW TI-538 Thomas Buboltz (TB)<BR>
 * @version 1_0_F 31.05.2011 TB Implementierung<BR>
 *          2_0_F 29.06.2011 TB kleinere Verbesserungen, NIO wenn keine Datei geschrieben wird<BR>
 *          3_0_F 29.06.2011 TB Rohwerte kommen als z.B. 3.1232442E+008, verarbeite dies als Eingangswert // Achtung! alphanumerisch kommt damit kein Wert!<BR>
 *          4_0_F 06.07.2011 TB BMW Teilenummer f�r das CID kommt als dezimaler Bytestring "00 00 09 25 99 03"<BR>
 *          6_0_T 21.07.2017 CW Zugriff mit Pr�fstands�bergreifenden @-Operator funktioniert nicht beim SMART-Server<BR>
 *          7_0_F 21.07.2017 CW Freigabe 6_0_T<BR>
 */
public class DokuHudCid_7_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DokuHudCid_7_0_F_Pruefprozedur() {
	}

	/**
	   * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	   * @param pruefling Klasse des zugeh. Pr�flings
	   * @param pruefprozName Name der Pr�fprozedur
	   * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	   */
	public DokuHudCid_7_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "DEBUG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "HUD_ASSEMBLYPARTNUMBER", "HUD_SERIALNUMBER", "CID_ASSEMBLYPARTNUMBER", "CID_SERIALNUMBER" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert!!!
	 */
	public boolean checkArgs() {
		// Obligatorisch.
		return super.checkArgs();
	}

	/** Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; //default english
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		StringBuffer sbXml = new StringBuffer();

		String hudAssemblyPartNumber = null;
		String hudSerialNumber = null;
		String cidAssemblyPartNumber = null;
		String cidSerialNumber = null;

		final int MAX_LENGTH_PART_NO = 7;
		final int MAX_LENGTH_SERIAL_NO = 10;

		final boolean DE = checkDE(); // Systemsprache DE wenn true
		boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen

		try {
			//Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG Parameter
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
				}

				// HUD_ASSEMBLYPARTNUMBER
				if( getArg( "HUD_ASSEMBLYPARTNUMBER" ).indexOf( '@' ) != -1 ) {
					hudAssemblyPartNumber = getResultRedesigned( getArg( "HUD_ASSEMBLYPARTNUMBER" ) );
					if( hudAssemblyPartNumber != null ) {
						try {
							hudAssemblyPartNumber = "" + new Double( hudAssemblyPartNumber ).longValue();
							hudAssemblyPartNumber = hudAssemblyPartNumber.length() > MAX_LENGTH_PART_NO ? hudAssemblyPartNumber.substring( 0, MAX_LENGTH_PART_NO ) : hudAssemblyPartNumber;
						} catch( NumberFormatException e ) {
							hudAssemblyPartNumber = null;
						}
					}
				}
				if( bDebug )
					System.out.println( "PP DokuHudCid: PARAMETERCHECK, Parameter: HUD_ASSEMBLYPARTNUMBER=<" + hudAssemblyPartNumber + ">" );

				// HUD_SERIALNUMBER
				if( getArg( "HUD_SERIALNUMBER" ).indexOf( '@' ) != -1 ) {
					hudSerialNumber = getResultRedesigned( getArg( "HUD_SERIALNUMBER" ) );
					if( hudSerialNumber != null ) {
						try {
							hudSerialNumber = "" + new Double( hudSerialNumber ).longValue();
							hudSerialNumber = hudSerialNumber.length() > MAX_LENGTH_SERIAL_NO ? hudSerialNumber.substring( 0, MAX_LENGTH_SERIAL_NO ) : hudSerialNumber;
						} catch( NumberFormatException e ) {
							hudSerialNumber = null;
						}
					}
				}
				if( bDebug )
					System.out.println( "PP DokuHudCid: PARAMETERCHECK, Parameter: HUD_SERIALNUMBER=<" + hudSerialNumber + ">" );

				// CID_ASSEMBLYPARTNUMBER
				if( getArg( "CID_ASSEMBLYPARTNUMBER" ).indexOf( '@' ) != -1 ) {
					cidAssemblyPartNumber = getResultRedesigned( getArg( "CID_ASSEMBLYPARTNUMBER" ) );
					if( cidAssemblyPartNumber != null ) {
						try {
							cidAssemblyPartNumber = "" + Long.parseLong( cidAssemblyPartNumber.replace( " ", "" ), 10 );
							cidAssemblyPartNumber = cidAssemblyPartNumber.length() > MAX_LENGTH_PART_NO ? cidAssemblyPartNumber.substring( 0, MAX_LENGTH_PART_NO ) : cidAssemblyPartNumber;
						} catch( NumberFormatException e ) {
							cidAssemblyPartNumber = null;
						}
					}
				}
				if( bDebug )
					System.out.println( "PP DokuHudCid: PARAMETERCHECK, Parameter: CID_ASSEMBLYPARTNUMBER=<" + cidAssemblyPartNumber + ">" );

				// CID_SERIALNUMBER
				if( getArg( "CID_SERIALNUMBER" ).indexOf( '@' ) != -1 ) {
					cidSerialNumber = getResultRedesigned( getArg( "CID_SERIALNUMBER" ) );
					if( cidSerialNumber != null ) {
						try {
							cidSerialNumber = "" + new Double( cidSerialNumber ).longValue();
							cidSerialNumber = cidSerialNumber.length() > MAX_LENGTH_SERIAL_NO ? cidSerialNumber.substring( 0, MAX_LENGTH_SERIAL_NO ) : cidSerialNumber;
						} catch( NumberFormatException e ) {
							cidSerialNumber = null;
						}
					}
				}
				if( bDebug )
					System.out.println( "PP DokuHudCid: PARAMETERCHECK, Parameter: CID_SERIALNUMBER=<" + cidSerialNumber + ">" );

			} catch( PPExecutionException e ) {
				if( bDebug )
					e.printStackTrace();
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Dokumentiere, wenn Daten vorhanden sind...
			if( hudAssemblyPartNumber != null && hudSerialNumber != null || cidAssemblyPartNumber != null && cidSerialNumber != null ) {
				// XML-Output-Buffer zusammenbauen
				sbXml.append( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" );
				sbXml.append( "<vehicleElectronicComponentsJ\r\n" );
				sbXml.append( "xmlns=\"http://bmw.com/vehicleElectronicComponents\"\r\n" );
				sbXml.append( "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\r\n" );
				sbXml.append( "xsi:schemaLocation=\"http://bmw.com/vehicleElectronicComponents vehicleElectronicComponentsJ.xsd\"\r\n" );
				sbXml.append( "version=\"01.00\" refSchema=\"vehicleElectronicComponentsJ.xsd\">\r\n" );
				sbXml.append( "<vehicle vinShort=\"" );
				sbXml.append( getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getCurrentAuftrag().getFahrgestellnummer7() );
				sbXml.append( "\">\r\n" );
				sbXml.append( "<electronicComponents>\r\n" );

				// Haben wir CID Daten? entspricht Sensor ID:=5600
				if( cidAssemblyPartNumber != null && cidSerialNumber != null ) {
					//sbXml.append( "\t<!--ECU CID-->\r\n" );
					sbXml.append( "\t<electronicComponent>\r\n" );
					sbXml.append( "\t\t<sensor sensorID=\"5600\" />\r\n" );
					sbXml.append( "\t\t<serialNo>" + cidSerialNumber + "</serialNo>\r\n" );
					sbXml.append( "\t\t<assemblyPartNo>" + cidAssemblyPartNumber + "</assemblyPartNo>\r\n" );
					sbXml.append( "\t</electronicComponent>\r\n" );
				}

				// Haben wir HUD Daten? entspricht Sensor ID:=5800
				if( hudAssemblyPartNumber != null && hudSerialNumber != null ) {
					//sbXml.append( "\t<!--ECU HUD-->\r\n" );
					sbXml.append( "\t<electronicComponent>\r\n" );
					sbXml.append( "\t\t<sensor sensorID=\"5800\" />\r\n" );
					sbXml.append( "\t\t<serialNo>" + hudSerialNumber + "</serialNo>\r\n" );
					sbXml.append( "\t\t<assemblyPartNo>" + hudAssemblyPartNumber + "</assemblyPartNo>\r\n" );
					sbXml.append( "\t</electronicComponent>\r\n" );
				}

				sbXml.append( "</electronicComponents>\r\n" );
				sbXml.append( "</vehicle>\r\n" );
				sbXml.append( "</vehicleElectronicComponentsJ>\r\n" );

				try {
					DomPruefstand.writeDomData( getPr�fling().getAuftrag().getFahrgestellnummer7(), "J", sbXml.toString().getBytes() );
				} catch( Exception e ) {
					result = new Ergebnis( "ExecFehler", "DomTransfer", "", "", "", "", "", "", "", "0", "", "", "", "FileWrite", "" + e.getMessage(), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					throw new PPExecutionException();
				}

				//IO-Ablauf als Status festhalten
				result = new Ergebnis( "DokuHudCid", "DomTransfer", "", "", "", "STATUS", "OK", "OK", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );
			} else {
				String hinweisText = DE ? "Es wurde keine DOM Datei: " + getPr�fling().getAuftrag().getFahrgestellnummer7() + "J.xml, erzeugt!" : "No DOM file: " + getPr�fling().getAuftrag().getFahrgestellnummer7() + "J.xml, created!";

				//Es wurde keine DOM Datei geschrieben! NIO-Ablauf als Status festhalten
				result = new Ergebnis( "DokuHudCid", "DomTransfer", "", "", "", "STATUS", "OK", "NOK", "", "0", "", "", "", "ERROR", hinweisText, Ergebnis.FT_NIO );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}

		} catch( PPExecutionException e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			if( bDebug )
				e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		setPPStatus( info, status, ergListe );
	}

	/**
	 * Liefert den Ergebniswert einer Pr�fprozedur eines Pr�flings Originalmethode in der
	 * AbstractPruefprozedur funktioniert nicht, wenn der Ergebnisstatus nicht STATUS_EXECUTION_OK
	 * war
	 * 
	 * @return der Wert des Ergebnisses
	 * @param resultName Name des Ergebnisses entsprechend der Form
	 *            'ergname@pruefprozedur(.pruefling)'.
	 */
	private String getResultRedesigned( String resultName ) {

		Pruefprozedur pp;
		String temp;
		String resultValue = null;
		int idx;

		resultName = resultName.toUpperCase();

		// Formatcheck
		if( (idx = resultName.indexOf( '@' )) < 0 ) {
			return resultValue;
		}
		temp = resultName.substring( idx + 1 );
		if( temp.indexOf( "." ) < 0 ) {
			// Kein Punkt somit Suche in eigenen Pr�fling ...
			pp = getPr�fling().getPr�fprozedur( temp );
		} else if( temp.indexOf( "." ) == temp.lastIndexOf( "." ) ) {
			// Ein Punkt enthalten, Suche in einem anderen Pr�fling �ber den Auftrag...
			pp = getPr�fling().getAuftrag().getPr�fprozedur( temp );
		} else {
			// 2 oder mehr Punkte, somit Fehler
			return resultValue;
		}
		if( pp == null ) {
			return resultValue;
		}
		if( pp.isValid() == false )
			return resultValue;

		// Jetzt das Ergebnis holen
		idx = resultName.indexOf( '@' );
		temp = resultName.substring( 0, idx ).trim();

		try {
			//resultValue = pp.getHistoryListe().getLatestErgebnis( temp ).getErgebnisWert();
			resultValue = ((AbstractBMWPruefprozedur)pp).getPPResult( resultName );
		} catch( NullPointerException e ) {
			return resultValue;
		} catch( InformationNotAvailableException e ) {
			e.printStackTrace();
			return resultValue;
		} catch (Exception e){
			e.printStackTrace( );
			return resultValue;
		}

		return resultValue;
	}

}

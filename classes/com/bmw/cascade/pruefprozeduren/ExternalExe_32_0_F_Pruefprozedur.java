/*
 * ExternalExe_VX_X_X_ZZ_Pruefprozedur.java
 *
 * Created on 01.06.01
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.extern.xml.erg.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;


/**
 * Ruft ein externes Programm auf und wartet auf dessen Beendung. Anhand des Exit-Codes des externen Programms<BR>
 * wird der Status festgelegt.<BR>
 * <BR>
 * @author Hans-Juergen Winkler, Siegfried St�binger, Marcus M�ller, Rainer Merz, Thomas Buboltz, Neil Crichton, Andreas Mair, Werner Braunstorfer, Pedro Jorge, Claudia Schumann, Dieter B�cherl<BR>
 * @wersion 0_0_5_TA 04.06.2002  SS  XMLDOKU als optionaler Parameter<BR>
 *          0_0_6_FA 28.06.2002  SS  Freigabe der Testversion<BR>
 *          0_0_7_FA 22.07.2002  MM  FZS als optionaler Parameter<BR>
 *          0_0_8_FA 07.08.2002  TB  wenn "ARGS = -FZS" uebergebe FGNR & FZS<BR>
 *          0_0_9_FA 05.02.2003  RM  wenn "ARGS = RELEASE_HT" Handterminal freigeben<BR>
 *          0_1_0_FA 29.04.2003  TB  "ARGS = API, IFH" zum ausfuehren eines Testfiles (*.tst) mittels TOOLSET + Tracelevels<BR>
 *          0_1_1_TA 25.07.2003  NC  Bugfix: sollte w�hrend INPA nicht mehr fr�hseitig weiterlaufen<BR>
 *          0_1_2_TA 25.07.2003  NC  Keine �nderung zum 0_1_1_TA, notwendig aufgrund W6...<BR>
 *          0_1_3_TA 25.07.2003  NC  Debuginformationen hinzu aufgrund INPA Fehlersuche<BR>
 *          0_1_4_TA 01.08.2003  NC  Auf Basis 0_1_0_FA: Optionale Argumente DIALOG hinzu, schaltet UserDialog aus, falls nicht ben�tigt<BR>
 *          0_1_5_TA 01.08.2003  NC  Auf Basis 0_1_4_TA: Bugfix f�r DIALOG Argument<BR>
 *          0_1_6_TA 01.08.2003  NC  Auf Basis 0_1_5_TA: Freigabe als FA<BR>
 *          0_1_7_FA 12.12.2003  AM  Auf Basis von 0_1_6_FA: a) Bisher war "-FZS" nur als einzelner String im optionealen Argument "ARGS" m�glich.<BR>
 *                               AM  Nun k�nnen in ARGS,getrennt jeweils durch Semikolon, mehrere Argumente �bergeben werden.<BR>
 *                               AM  b) Der String "-FGNR" im "ARGS"-Argument wird durch -FGNR[...] (7 stellige FGNR) ersetzt.<BR>
 *          0_1_8_FA 12.12.2003  NC  Aufgrund der Windows XP Einf�hrung kann eine externe Applikationen in unterschiedlichen Verzeichnisse <BR>
 *                                   auf unterschiedlichen Pr�fst�nde liegen. Zwei neue optionale Parameter sind deswegen integriert worden <BR>
 *                                   (ALTERNATIVE_EXECALL und ALTERNATIVE_WDIR). <BR>
 *          0_1_9_FA 16.02.2004  NC  Bugfix: Alternative Arbeitsverzeichnis wird korrekt behandelt<BR>
 *          0_2_0_FA 21.02.2005  AM  Der String "-BR" im "ARGS"-Argument wird durch -BR[Baureihe] ersetzt.<BR>
 *                               AM  Der String "-WAC" im "ARGS"-Argument wird durch -WAC[Wheel alignment code] (Spurcode) ersetzt.<BR>
 *          0_2_1_FA 25.10.1005  WB  �ndern der Parameterreihenfolge f�r APDM und des virtuellen Fahrzeuges Param2 (Exe-Argument) und Param3 (Exe-Pfad).<BR>                      
 *			22_T 	 04.08.2006  PJ  Neues optionales argument "ARGSEXT[1..N]" als Vektor fuer zus�tzliche Argumente beim Aufruf der Externen Applikation NightVision.<BR>
 *									 Debugschalter eingebaut.<BR> 
 *			23_F 	 25.08.2006  PJ  Neues optionales argument "ARGSEXT[1..N]" als Vektor fuer zus�tzliche Argumente beim Aufruf der Externen Applikation NightVision.<BR>
 *									 Debugschalter eingebaut als parameter.<BR> 
 *			24_F 	 03.06.2008  CS  Anpassung an APDM Ergebnis Format.<BR>
 *			25_F	 21.07.2008	 UP  Als Argumente kann jetzt auch nur EIN @-Konstrukt �bergeben werden
 *          28_F     05.11.2009  TB  @-Operator bei Arguments und external Arguments, Komma wird als Token 'KOMMA' interpretiert <BR>
 *          29_T     28.01.2010  DB  OPTIONAL ARGS erweitert um READ_SO_NIO und READ_SO_IO: Auslesen von System.out und System.err des externen Programms<BR>
 *          30_F     ??
 *          31_T     19.04.2011  MB  Auswertung von Variablen ueber !PV(t,NAME)!,!PU!,!PSTATUS! 
 *          32_F     29.04.2011  MB  Freigabe
**/
public class ExternalExe_32_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    	static final long serialVersionUID = 1L;   	    
      	
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public ExternalExe_32_0_F_Pruefprozedur() {}

  /**
     * erzeugt eine neue Pruefprozedur.
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public ExternalExe_32_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    /**
     * initialsiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
        setAttribut("XMLDOKU", "FALSE");
    }
    
    /**
     * liefert die optionalen Argumente
     */
    public String[] getOptionalArgs() {
        String[] args = {"ARGS",				// 0 
        				 "WDIR", 				// 1
						 "XMLDOKU",				// 2 
						 "FZS", 				// 3
						 "RELEASE_HT", 			// 4
						 "API",					// 5 
						 "IFH", 				// 6
						 "DIALOG", 				// 7
						 "ALTERNATIVE_EXECALL", // 8
						 "ALTERNATIVE_WDIR", 	// 9
        				 "ARGSEXT[1..N]",		// 10
        				 "DEBUG", 				// 11
        				 "READ_SO_NIO",			// 12=Read System.out and System.err of external program only if exit value != 0
        				 "READ_SO_IO",};        // 13=Read System.out and System.err of external program only if exit value == 0
        return args;
    }
    
    /**
     * liefert die zwingend erforderlichen Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = {"EXECALL", "EXENAME"};
        return args;
    }
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
     */
    public boolean checkArgs() {       
        int i, j;
        int maxIndex = 0;
        boolean bRequiredArg, bOptionalArg;
        String[] saParamDetails;
        String sParamLine;        
        boolean debug = false;
    	
        try {
        	//
        	// 0. Check: Execute in DEBUG mode?
        	// 
        	if (getArg("DEBUG") != null) {
                debug = (getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;           
        	}
        	
        	if (debug) {
                Enumeration enu = getArgs().keys();   
        		System.out.println("*** DEBUG MODE (1000) ***");
                System.out.println("checkArgs() in: " + this.getName());
                System.out.println("All Arguments from this PP:");
                while (enu.hasMoreElements()) {
                    // The actual argument
                    String strGivenkey = (String) enu.nextElement();
                    String strActualArg = getArg(strGivenkey);
                    System.out.println(strGivenkey + " = " + strActualArg);
                }
            }
        	
        	//
        	// 1. Check: Required arguments
            //
        	String[] requiredArgs = getRequiredArgs();
            for (i=0; i<requiredArgs.length; i++) {
                if (getArg(requiredArgs[i]) == null) 
                	return false;
                if (getArg(requiredArgs[i]).equals("")) 
                	return false;
            }
            if (debug)
                System.out.println("Done 1. Check");
            //
            // 2. Check: Arguments extension indexes  
            //
            Enumeration enu = getArgs().keys();
            String[] optionalArgs = getOptionalArgs();
            String givenkey;
            String temp = "";
            while (enu.hasMoreElements()) {
                givenkey = (String) enu.nextElement();
                // Wenn Argument ohne Index
				if (!givenkey.startsWith("ARGSEXT")) {                    
                    j = 0;
                    bRequiredArg = false;
                    while ((bRequiredArg == false) && (j < requiredArgs.length)) {
                        if (givenkey.equals(requiredArgs[j]) == true) 
                        	bRequiredArg = true;
                        j++;
                    }
                    j = 0;
                    bOptionalArg = false;
                    while ((!bOptionalArg) && (j < optionalArgs.length)) {
                        if (givenkey.equals(optionalArgs[j])) 
                        	bOptionalArg = true;
                        j++;
                    }
                    if (!(bRequiredArg || bOptionalArg)) {
                    	return false;
                    }
                    if (getArg(givenkey).equals("")) { 
                    	return false;              
                    }
                    
                // Wenn Argument mit Index
                } else {                    
                    if (givenkey.startsWith("ARGSEXT"))
                    	temp = givenkey.substring(7);                 	
                	try {
                        j = Integer.parseInt(temp);
                        if (j < 1) { 
                        	return false;
                		}
                        if (j > maxIndex) { 
                        	maxIndex = j;
                        }
                        
                    } catch (NumberFormatException e) {
                        if (debug)
                            System.out.println("checkArgs() exception " + e.getMessage());
                        return false;
                    }
                }
            }
            if (debug) 
                System.out.println("Done 2. Check");
            //
            // 3. Check: check ARGSEXT contents (name & value). At least the name must be present.
            //
            for (i=1; i<=maxIndex; i++) {
            	sParamLine = getArg("ARGSEXT" + i);
                // parameter must have a name (1st detail) but may not have a value (2nd detail)
                if ((sParamLine == null) || (sParamLine.equals(""))) {
                	return false;
                }
                if (sParamLine.indexOf(';') != -1) {
                    saParamDetails = sParamLine.split(";");
                    if ( (saParamDetails.length < 1) || (saParamDetails.length > 2) ) {
                    	return false;
                    }
                    if (saParamDetails[0].trim().equals("")) {
                    	return false;
                    }	
                    if ((saParamDetails.length == 2) &&
                    	(!saParamDetails[1].trim().equals("")) && 
						(saParamDetails[1].indexOf('@') != -1)) {
	                    try {
	                        Double.parseDouble(getPPResult(saParamDetails[1]));                         
	                    } catch (NumberFormatException e) {
	                        if (debug) {
	                            System.out.println(e.getMessage());
	                        }
	                        return false;
	                    } catch (InformationNotAvailableException e) {
	                        if (debug) {
	                            System.out.println(e.getMessage());
	                        }
	                    	return false;
	                    }
                    }
                }
            }
            if (debug) 
                System.out.println("Done 3. Check");

            // Tests bestanden, somit ok
            if (debug)
            	System.out.println("checkArgs() finished ok.");
            return true;

        } catch (Exception e) {
            if (debug) {
            	e.printStackTrace();
                System.out.println("checkArgs() exception " + e.getMessage());
            }
            return false;
        } catch (Throwable e) {
            if (debug) {
            	e.printStackTrace();
                System.out.println("checkArgs() exception " + e.getMessage());
            }
            return false;
        }
	}        
                
    /**
     * f�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        // spezifische Variablen
        File workingDir = null;
        File alternativeWorkingDir = null;
        String exeName = null;
        String exeCall = null;
        String alternativeExeCall = null;
        String exeArgs = null;
        String execString, temp;
        int exeStatus;
        boolean udInUse = false;;
        String xmlErgebnisDatei = "";
        String xmlDtdDatei      = "";
        boolean xmldoku = false;
        boolean fzs = false;
        boolean release_ht = false;
        boolean showDialog = true;
        boolean debug = false;
        boolean readNIOResultFromSystem = false;
        boolean readIOResultFromSystem = false;
        
        //f�r das korrekte Schreiben des APDM-Ergebnisformates
        String param3="";
        
        try {
        	//Parameter holen
            try {
            	// execute in DEBUG mode?
                debug = "TRUE".equalsIgnoreCase(getArg("DEBUG"));           
            	// check args
            	if (checkArgs() == false) 
                	throw new PPExecutionException(PB.getString("parameterexistenz" ));
                exeCall = extractValues( getArg( getRequiredArgs()[0] ) ) [0];
                exeName = extractValues( getArg( getRequiredArgs()[1] ) ) [0];
                exeArgs = getArg( getOptionalArgs()[0] );           	
                if (getArg(getOptionalArgs()[1]) != null) 
                	workingDir = new File(getArg(getOptionalArgs()[1]));
            	
            } catch (PPExecutionException e) {
            	if (e.getMessage() != null)
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            // XMLDOKU �bernehmen, wenn als Parameter vorhanden wird Attribut �berschrieben
            xmldoku = "TRUE".equalsIgnoreCase(getAttribut("XMLDOKU"));
            if(getArg(getOptionalArgs()[2])!=null){
                if(getArg(getOptionalArgs()[2]).equalsIgnoreCase("TRUE")) xmldoku = true;
                if(getArg(getOptionalArgs()[2]).equalsIgnoreCase("FALSE")) xmldoku = false;
            }
            
            // Soll im XMLDOKU-Mode (Ergebnisobjekt vom aufgerufenen Programm) anstelle der FGNR (lang) eine FZS �bergeben werden?
            if(getArg(getOptionalArgs()[3])!=null){
                if(getArg(getOptionalArgs()[3]).equalsIgnoreCase("TRUE")) fzs = true;
                if(getArg(getOptionalArgs()[3]).equalsIgnoreCase("FALSE")) fzs = false;
            }
            
            // Soll das Handterminal freigegeben werden ?
            if(getArg(getOptionalArgs()[4])!=null){
                if(getArg(getOptionalArgs()[4]).equalsIgnoreCase("TRUE")) release_ht = true;
                if(getArg(getOptionalArgs()[4]).equalsIgnoreCase("FALSE")) release_ht = false;
            }
            
            // Wenn das optionale Argument ARGS mehrere Strings (durch ; getrennt -> mehere Parameter Details im frontend) enth�lt,
            // k�nnen die Einzelargumente hier manipuliert werden.
            if( exeArgs!=null ) {
            	String[] allArgs = splitArg( exeArgs );
                int iNumArgs = allArgs.length;
                String strArg=null;
                int n=0;
                // Eventuell ersetzen von Elementen der Argumentenliste
                for( n=0; n<iNumArgs; n++ ) {
                    strArg = allArgs[n];
                    
                    // werte "@" Operator aus und ersetzte falls m�glich
                    try{
                    	if( strArg.indexOf( "@" ) != -1 ) {
                    		StringTokenizer stLeft = new StringTokenizer( strArg.substring( 0, strArg.indexOf( "@" ) ), " :;,.-=" );
                    		StringTokenizer stRight = new StringTokenizer( strArg.substring( strArg.indexOf( "@" ) + 1,  strArg.length()), " :;,-=" );
                    		
                    		String strLeft = null;
                    		String strRight = null;
                    		
                    		// Suche auf der linken Seite
                    		if ( stLeft.hasMoreTokens() ) {
                    			while( stLeft.hasMoreTokens() ) 
                    				strLeft = stLeft.nextToken();
                    			
                    			strLeft = strLeft + "@";
                    		}
                    		else
                    			strLeft = strArg.substring( 0, strArg.indexOf( "@" ) ) + "@"; 
                    		
                    		if (debug)
                                System.out.println("exeArgs: strLeft = " + strLeft);
                    		
                    		// Suche auf der rechten Seite
                    		if ( stRight.hasMoreTokens() )
                    			strRight = stRight.nextToken();
                    		else
                    			strRight = strArg.substring( strArg.indexOf( "@" ) + 1, strArg.length() ); 

                    		if (debug)
                                System.out.println("exeArgs: strRight = " + strRight);
                    		
                    		// ersetzte die Werte sofern m�glich
                    		exeArgs = exeArgs.replaceFirst( strLeft + strRight, extractValues (strLeft + strRight) [0] );	
                    	}	
                    } catch (Exception e) {
                    }
                    
                    // Wenn exeArgs "-FZS" enth�lt, dann soll dieses ersetzt werden durch "-FZS[Fahrzeugsteuerschluessel]"
                    if( strArg.equalsIgnoreCase("-FZS") ) {
                        exeArgs = exeArgs.replaceFirst(strArg, "-FZS"+getPr�fling().getAuftrag().getSteuerschl�ssel() );
                    }
                    // Wenn exeArgs "-FGNR" enth�lt, dann soll dieses ersetzt werden durch "-FGNR[Fahrgestellnummer, 7-stellig]"
                    else if( strArg.equalsIgnoreCase("-FGNR") ) {
                        exeArgs = exeArgs.replaceFirst(strArg, "-FGNR"+getPr�fling().getAuftrag().getFahrgestellnummer7() );
                    }
                    // Wenn exeArgs "-BR" enth�lt, dann soll dieses ersetzt werden durch "-BR[Baureihe]"
                    else if( strArg.equalsIgnoreCase("-BR") ) {
                        exeArgs = exeArgs.replaceFirst(strArg, "-BR"+getPr�fling().getAuftrag().getBaureihe() );
                    }
                    // Wenn exeArgs "-WAC" enth�lt, dann soll dieses ersetzt werden durch "-WAC[Wheel alignment code]"
                    else if( strArg.equalsIgnoreCase("-WAC") ) {
                        exeArgs = exeArgs.replaceFirst(strArg, "-WAC"+getPr�fling().getAuftrag().getSpur() );
                    }
                    // Wurde eine Testdatei (*.tst) fuer Toolset als arg uebergeben -> setze Tracelevels
                    else if( strArg.trim().toUpperCase().endsWith(".TST") ) {
                        try {
                            int iAPILevel = Integer.parseInt(getArg( getOptionalArgs()[5] ));
                            int iIFHLevel = Integer.parseInt(getArg( getOptionalArgs()[6] ));
                            iAPILevel = (iAPILevel > 4) ? 1 : (iAPILevel < 0) ? 1 : iAPILevel;
                            iIFHLevel = (iIFHLevel > 4) ? 1 : (iIFHLevel < 0) ? 1 : iIFHLevel;
                            exeArgs = exeArgs.replaceFirst(strArg, strArg.toUpperCase()+" "+iAPILevel+" "+iIFHLevel);
                        } catch (NumberFormatException e) {
                            exeArgs = exeArgs.replaceFirst(strArg, strArg.toUpperCase()+ " 1 1");
                        }
                    }
                    
                  //Wenn exeArgs "-PU" enth�lt, dann soll dieses ersetzt werden durch "Pruefumfangname"
                    if( strArg.contains("!PU!") ) {
                        exeArgs = exeArgs.replaceFirst("!PU!", getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getLogicalName());//.getPr�fumfangName());
                    }
                    
                    //Aktueller Fehlerstand Stand des Pruefumfangs
                    if( strArg.contains("!PSTATUS!") ) {
                        //exeArgs = exeArgs.replaceFirst("!PSTATUS!", ""+getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getLastExecStatus());
                    	exeArgs = exeArgs.replaceFirst("!PSTATUS!", (getPr�flingLaufzeitUmgebung().getCurrentPr�fumfang().getOuterBlock().getErrorCount()>0?"NOK":"OK"));  // .equals(obj).getLastExecStatus());
                    }
                    
                    //Pruefstandsvariable aufloesen
                    if( (strArg.contains("!PV(")) && (strArg.contains( ")!" ))) {
                    	StringBuffer sbBefore = new StringBuffer();
                    	StringBuffer sbAfter = new StringBuffer();

                    	disolvePsVariable(strArg, sbBefore, sbAfter);
                    	
                    	exeArgs = exeArgs.replaceAll( Pattern.quote(sbBefore.toString()), sbAfter.toString());
                    	//exeArgs = exeArgs.replace(sbBefore.toString(), sbAfter.toString());
                    	if(debug)System.out.println("Result ExternalExe.disolvePsVariable() - exeArgs : " +  exeArgs);
                    }
                }
                // Argumentenliste mit Leerzeichen versehen und in eine Zeile bringen (keine Semikolons mehr!)
                if( iNumArgs >= 1 ) {
                    String argsAsOneLine="";
                    String[] allNewArgs = extractValues( exeArgs );
                    for( n=0; n<iNumArgs; n++ ) {
                        strArg = allNewArgs[n];
                        if(n==0) argsAsOneLine = allNewArgs[n];
                        else argsAsOneLine = argsAsOneLine+" "+allNewArgs[n];
                    }
                    exeArgs = argsAsOneLine;
                    
                    // Komma Tokens entfernen
                    exeArgs = exeArgs.replaceAll("'KOMMA'", ",");
                }
            }

            // Wenn das optionale Argument DIALOG vorhanden ist
            if (getArg( getOptionalArgs()[7] )!=null) {
                if(getArg( getOptionalArgs()[7] ).equalsIgnoreCase("FALSE")) showDialog = false;
            }
            
            // �berpr�fen ob zweiten Pfad definiert ist
            alternativeExeCall = getArg( getOptionalArgs()[8] );
            if( getArg(getOptionalArgs()[9]) != null ) 
            	alternativeWorkingDir = new File( getArg(getOptionalArgs()[9]) );
            
            
            // XML Parameter definieren
            if( xmldoku == true ) {
                xmlErgebnisDatei = "-XML"+PB.getString( "externeXMLPfad" )+getPr�fling().getName()+".xml";
                xmlDtdDatei      = "-DTDfile:///"+PB.getString( "externeXMLDtd" );
                String fgnr = null;
                if (fzs==false)
                    fgnr=getPr�fling().getAuftrag().getFahrgestellnummer();
                else
                    fgnr=getPr�fling().getAuftrag().getSteuerschl�ssel();
                if( exeArgs == null )
                    exeArgs = xmlErgebnisDatei + " " + xmlDtdDatei+ " -FGNR"+fgnr;
                else
                    exeArgs = exeArgs + " " + xmlErgebnisDatei + " " + xmlDtdDatei + " -FGNR"+fgnr;
            }
            
            //Message ausgeben
            if ( showDialog ) {
	            try {
	                getPr�flingLaufzeitUmgebung().getUserDialog().displayMessage( exeName, PB.getString( "externesProgrammWirdAusgef�hrt" ), -1 );
	                udInUse = true;
	            } catch (Exception e) {
	                if (debug)
	                    System.out.println("execute(..) exception " + e.getMessage());
	            	if (e instanceof DeviceLockedException)
	                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
	                else
	                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
	                ergListe.add(result);
	                throw new PPExecutionException();
	            }
            }
            
            // Handterminal freigeben
            if ( release_ht ) {
                getPr�flingLaufzeitUmgebung().getDeviceManager().releaseHandterminalAndComms();
                if (debug)
                    System.out.println("Hand terminal released.");
            }
            
            // Entscheidung ob alternativ Pfad n�tig ist
            if ( (new File(exeCall).exists() == false) && (alternativeExeCall != null) ) {
                execString = alternativeExeCall;
                workingDir = alternativeWorkingDir;
                if (debug)
                    System.out.println("Exe call inexistent, will use alternative exe call.");
            } else {
                execString = exeCall;
            }
            
            //Check if System.out and System.err Result Text should be read in from external.exe system.out
            readNIOResultFromSystem = "TRUE".equalsIgnoreCase(getArg(getOptionalArgs()[12]));
            readIOResultFromSystem = "TRUE".equalsIgnoreCase(getArg(getOptionalArgs()[13]));
            
            // Extract and append ARGSEXT[1..N] to the command line
			// arg name is appended to the arg value i.e. -CD434 -CH23
            String[] saDetails;
            String exeArgsExt = "";
            int ea = 1;
            while (getArg("ARGSEXT" + ea) != null) {
            	saDetails = splitArg(getArg("ARGSEXT" + ea));
				for (int d=0; d<saDetails.length; d++) {
					
                    // werte "@" Operator aus und ersetzte falls m�glich
                    try{
                    	if( saDetails[d].indexOf( "@" ) != -1 ) {
                    		StringTokenizer stLeft = new StringTokenizer( saDetails[d].substring( 0, saDetails[d].indexOf( "@" ) ), " :;,.-=" );
                    		StringTokenizer stRight = new StringTokenizer( saDetails[d].substring( saDetails[d].indexOf( "@" ) + 1, saDetails[d].length()), " :;,-=" );
                    		
                    		String strLeft= null;
                    		String strRight = null;
                    		
                    		// Suche auf der linken Seite
                    		if ( stLeft.hasMoreTokens() ) {
                    			while( stLeft.hasMoreTokens() ) 
                    				strLeft = stLeft.nextToken();

                    			strLeft = strLeft + "@";
                    		}
                    		else
                    			strLeft = saDetails[d].substring( 0, saDetails[d].indexOf( "@" ) ) + "@"; 
                    		
                    		if (debug)
                                System.out.println("ARGSEXT: strLeft = " + strLeft);
                    		
                    		// Suche auf der rechten Seite
                    		if ( stRight.hasMoreTokens() )
                    			strRight = stRight.nextToken();
                    		else
                    			strRight = saDetails[d].substring( saDetails[d].indexOf( "@" ) + 1, saDetails[d].length() ); 

                    		if (debug)
                                System.out.println("ARGSEXT" + ea + ": strRight = " + strRight);
                    		
                    		// ersetzte die Werte sofern m�glich
                    		saDetails[d] = saDetails[d].replaceFirst( strLeft + strRight, extractValues (strLeft + strRight) [0] );	
                    	}	
                    } catch (Exception e) {
                    }
					
					exeArgsExt = (exeArgsExt.equals("") ? "" : exeArgsExt) + (d == 0 ? " " : "") + saDetails[d];
				}
				ea++;            	
            }
            if (debug) {
               	System.out.println("exeArgs: " + (exeArgs != null ? exeArgs : "NULL")); 
            	System.out.println("exeArgsExt: " + (exeArgsExt != null ? exeArgsExt : "NULL")); 
            }
    		// Concatenate the extended arguments "exeArgsExt" to "exeArgs"
            exeArgs = ((exeArgs != null) ? exeArgs + " " : "") + exeArgsExt.trim();          

            // Ausf�hrung
            if( exeArgs != null ) 
            	execString = execString + " " + exeArgs.trim();

            if (debug)
            	System.out.println("execString: " + (execString != null ? execString : "NULL")); 

            // Execute external application
            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec( execString, null, workingDir );
            if (readNIOResultFromSystem || readIOResultFromSystem )
            {
            	StringBuffer returnBuf = new StringBuffer();
            	exeStatus = doWaitForAndReturnText(process, returnBuf);
                if ((0 == exeStatus && readIOResultFromSystem) || (0 != exeStatus && readNIOResultFromSystem))
                {
                	param3 = returnBuf.toString();
                }
            }
            else
            {
            	exeStatus = doWaitFor( process );
            }

            if( exeArgs == null ) 
            	exeArgs = "";
            if( workingDir == null ) 
            	temp = "";
            else 
            	temp = workingDir.getAbsolutePath();
            if (debug)
            	System.out.println("exeStatus: " + Integer.toString(exeStatus)); 
            
            //nur Param3 darf bef�llt werden, Param1 sollte SGBD beinhalten,
            //Param2 sollte APIJOB beinhalten
            if (param3.equals(""))
            	param3 = exeCall + "; " + temp + "; " + exeArgs;
            if( exeStatus == 0 ) {
                result = new Ergebnis( "Status", "System", "", "", param3, "EXESTATUS", ""+exeStatus, "0", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                ergListe.add(result);
            } else {
                result = new Ergebnis( "Status", "System", "", "", param3, "EXESTATUS", ""+exeStatus, "0", "", "0", "", "", "", PB.getString( "ausf�hrungsfehlerExternesProgramm" ), "", Ergebnis.FT_NIO );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            //Parsen von Ergebnissen
            if( xmldoku == true ) {
                String str = null;
                Vector myInfo = null;
                
                try {
                    File myFile = new File( PB.getString( "externeXMLPfad" )+getPr�fling().getName()+".xml" );
                    if( myFile.exists() == false ) throw new IOException("File doesn't exist");
                    if( myFile.canRead() == false ) throw new IOException("Can't read file");
                    if( myFile.length() <= 0 ) throw new IOException("File empty");
                    
                    FileInputStream fis = new FileInputStream( myFile ) ;
                    byte[] bytes = new byte[(int)(myFile.length())];
                    if( fis.read(bytes,0,((int)myFile.length())) != ((int)myFile.length()) ) throw new IOException("File content error");
                    fis.close();
                    str = new String(bytes).trim();
	                if (debug) 
	                    System.out.println("XML results file read.");
                    
                } catch( Exception e ) {
	                if (debug) 
	                    System.out.println("execute(..) exception " + e.getMessage());
	                //nur Param3 darf bef�llt werden, Param1 sollte SGBD beinhalten,
	                //Param2 sollte APIJOB beinhalten
	                param3 = exeCall + "; " + temp + "; " + exeArgs;
	                result = new Ergebnis( "Status", "System", "", "", param3, "DOKU", ""+exeStatus, "0", "", "0", "", "", "", PB.getString( "ausf�hrungsfehlerExternesProgramm" ), "", Ergebnis.FT_NIO );
                    ergListe.add(result);
                    throw new PPExecutionException();
                }
                
                try {
                    myInfo = XMLErgebnisParser.parseXML( str, getPr�fling().getAuftrag().getFahrgestellnummer() );
                    for (int i=0; i < myInfo.size(); i++) {
                        result =  (Ergebnis) myInfo.get(i);
                        ergListe.add(result);
                        if ( result.getFehlerTyp() != Ergebnis.FT_IO ) status = STATUS_EXECUTION_ERROR;
                    }
	                if (debug) 
	                    System.out.println("XML results parsed ok.");
                    
                } catch( Exception e ) {
	                if (debug) 
	                    System.out.println("execute(..) exception " + e.getMessage());
	                param3 = exeCall + "; " + temp + "; " + exeArgs;
	                result = new Ergebnis( "Status", "System", "", "", param3, "DOKU PARSING", ""+e, "0", "", "0", "", "", "", ""+e, "", Ergebnis.FT_NIO );
                    ergListe.add(result);
                    throw new PPExecutionException();
                }
            }
            
        } catch (PPExecutionException e) {
            if (debug) {
                System.out.println("execute(..) exception " + e.getMessage());
                e.printStackTrace();
            }
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            if (debug) {
                System.out.println("execute(..) exception " + e.getMessage());
                e.printStackTrace();
            }
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
            if (debug) {
                System.out.println("execute(..) exception " + e.getMessage());
                e.printStackTrace();
            }
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
        //Freigabe der benutzten Devices
        if( (showDialog) && (udInUse == true) ) {
            try {
                getPr�flingLaufzeitUmgebung().releaseUserDialog();
                if (debug)
                    System.out.println("User dialog released.");
            } catch (Exception e) {
                if (debug)
                    System.out.println("execute(..) exception " + e.getMessage());
                if (e instanceof DeviceLockedException)
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                status = STATUS_EXECUTION_ERROR;
            }
        }
        
        setPPStatus( info, status, ergListe );
    }
    
    //Wartet auf das Ende des externen Programms
    private int doWaitFor(Process p) {
        StringBuffer inBuffer = new StringBuffer();
        StringBuffer errBuffer = new StringBuffer();
        int exitValue = -1;
        try {
            InputStream in  = p.getInputStream();
            InputStream err = p.getErrorStream();
            boolean finished = false;
            while( !finished) {
                try {
                    while( in.available() > 0) {
                        Character c = new Character( (char) in.read());
                        inBuffer.append( c);
                    }
                    while( err.available() > 0) {
                        Character c = new Character( (char) err.read());
                        errBuffer.append( c);
                    }
                    exitValue = p.exitValue();
                    finished  = true;
                    try {
                        Thread.sleep(1);
                    } catch (Exception ex) {
                    }
                
                } catch (IllegalThreadStateException e) {
                    try {
                        Thread.sleep(500);
                    } catch (Exception ex) {
                    }
                }
            }
            if( errBuffer.length() > 0 ) {
                System.out.println("ERROR-BUFFER:");
                System.out.println(errBuffer);
            }
            if( inBuffer.length() > 0 ) {
                System.out.println("OUTPUT-BUFFER:");
                System.out.println(inBuffer);
            }
        }
        catch (Exception e) {
        }
        return exitValue;
    }
    
    //Wartet auf das Ende des externen Programms
    //und gibt die Konsolenausgabe der externen EXE zur�ck
    private int doWaitForAndReturnText(Process p, final StringBuffer returnBuf) {
        StringBuffer inBuffer = new StringBuffer();
        StringBuffer errBuffer = new StringBuffer();
        int exitValue = -1;
        try {
            InputStream in  = p.getInputStream();
            InputStream err = p.getErrorStream();
            boolean finished = false;
            while( !finished) {
                try {
                    while( in.available() > 0) {
                        Character c = new Character( (char) in.read());
                        inBuffer.append( c);
                    }
                    while( err.available() > 0) {
                        Character c = new Character( (char) err.read());
                        errBuffer.append( c);
                    }
                    exitValue = p.exitValue();
                    finished  = true;
                    try {
                        Thread.sleep(1);
                    } catch (Exception ex) {
                    }
                
                } catch (IllegalThreadStateException e) {
                    try {
                        Thread.sleep(500);
                    } catch (Exception ex) {
                    }
                }
            }
            if( errBuffer.length() > 0 ) {
            	returnBuf.append(errBuffer);
            }
            if( inBuffer.length() > 0 ) {
            	returnBuf.append(inBuffer);
            }
        }
        catch (Exception e) {
        }
        return exitValue;
    }
    
    /***
     * replaces string "pv(x,y)" with the equivalent pruefstandvariable
     * @param arg Text with variables: supported formats: *!PV(<varType>,<varName>)!*
     * <varType>: C=Config, T=Temporary, P=Persistent
     * <varName>: the name of the variable
     * @return sbBefore: the original substring before disolving the variable e.g. "!PV(C,ORT)!";
     * @return sbAfter: "BIP"
     */
    private String disolvePsVariable(String arg, StringBuffer sbBefore, StringBuffer sbAfter){
    	String ret = "", var = "", val = "", varType = "";

    	
    	int pos1, pos2; 
    	Object o;
    
    	ret = arg;
    	varType = "C";
    	
    	boolean debug = false;
    	
    	try{
    	if (getArg("DEBUG") != null) {
            debug = (getArg("DEBUG").equalsIgnoreCase("TRUE")) ? true : false;        
        }
    	}catch(Exception ex1){}
    	
    	if(debug)System.out.println("ExternalExe.disolvePsVariable()-arg : " + arg);
    	
    	pos1 = arg.indexOf("!PV(");
    	if(debug)System.out.println("ExternalExe.disolvePsVariable()- indexOf('!PV(') : " + pos1);
    	if (pos1 >= 0) {
    		pos2 = arg.indexOf(")!",pos1);
    		if(debug)System.out.println("ExternalExe.disolvePsVariable()- indexOf')!' : " + pos2);
    		
    		if (pos2 > pos1){
    			var = arg.substring(pos1+4, pos2);
    			
    			sbBefore.append(arg.substring(pos1, pos2+2));
    			if(debug)System.out.println("ExternalExe.disolvePsVariable() - Before : " + sbBefore.toString()); 
    			
    			//check if VarType is specified
    			if (((var.charAt(1)==',') && 
    				((var.toUpperCase().charAt(0)=='C' )
    			   ||(var.toUpperCase().charAt(0)=='T' )
    			   ||(var.toUpperCase().charAt(0)=='P' ))))
    			{
    				varType = ""+var.toUpperCase().charAt(0);
    				var = var.substring(2);
    			}
    			
    			
    			
    			if(debug)System.out.println("ExternalExe.disolvePsVariable()- variable name : " + var);
    			try{
    				if(varType.equalsIgnoreCase("P")) o = this.getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PERSISTENT, var);
    				else if(varType.equalsIgnoreCase("T")) o = this.getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.TEMPORARY, var);
    				else o = this.getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.PS_CONFIG, var);
    				
   				
	    			if (o!=null){
	    				val = o.toString();
	    				if(debug)System.out.println("ExternalExe.disolvePsVariable()- variable value : " + val);
	    				
	    				ret = arg.substring(0, pos1)+ val + arg.substring(pos2+2);
	    				sbAfter.append(val);
	    				if(debug)System.out.println("ExternalExe.disolvePsVariable() - After : " + sbAfter.toString()); 
	    				if(debug)System.out.println("ExternalExe.disolvePsVariable()- result : " + arg.substring(0, pos1)+ val + arg.substring(pos2+2));
	    			}
    			}catch(Exception x){
    				System.out.println("ExternalExe.disolvePsVariable()- Could not get Variable, using original text! " + var + " -  exception : " + x.getMessage() );
    				sbAfter.append(sbBefore);
    				
    			}
    		}
    	}
    	//ret =  val;
    	
    	return ret;
   	
    }
    

    
	public static void main(String[] args) {
		StringBuffer a = new StringBuffer();
		ExternalExe_32_0_F_Pruefprozedur ee = new ExternalExe_32_0_F_Pruefprozedur();
		ee.setAttribut("EXENAME", "CheckCompleteCmd.exe");
		ee.setAttribut("EXECALL", "CheckCompleteCmd.exe");
		ee.setAttribut("DEBUG", "tRuE");
		ee.setAttribut("FZS", "A123456");
		ee.setAttribut("WDIR", "D:/tmp/dds/CompleteCheck/CMD");
		ee.setAttribut("ARGS", "-ShortVin=\"A123456\"");
		ee.setAttribut("READ_SO_NIO", "TrUe");
		ee.attributeInit();
		System.out.println(a);
		ExecutionInfo info = new ExecutionInfo(1, 1, 1, false);
		ee.execute(info);
	}
	
	
	
    
    
}


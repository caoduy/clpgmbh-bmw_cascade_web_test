/*
 * DiagFSLesenFunkt_x_y_Pruefprozedur.java
 *
 * Created on 10.02.04
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;
import java.lang.System;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.visualisierung.*;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;

/**
 * Implementierung einer Pr�fporozedur, die eine funktionalen Job ausf�hrt
 * @author Gramer, M�ller, Rettig, Wang, Sch�nert, Buboltz
 * @version 0_0_1_FA  10.02.04  GR  Ersterstellung Basis-Implementierung ohne phs. Fehlespeicherlesen NICHT freigegeben
 * @version 0_0_2_FA  03.09.04  GR  Erweiterung auf INCLUDE- und EXCLUDE-List; Physikalische Nachfrage implementiert; Fehlerspeicher l�schen hinzu!
 * @version 0_0_3_FA  06.09.04  GR  Ausblendung per Attribute hinzu
 * @version 0_0_5_FA  18.01.05  GR  Fehlerkorrektur bei nicht gesetztem optionalen Parameter FS-LOESCHEN
 * @version 0_0_6_FA  13.06.06  PR  Hinzuf�gen der optionalen Parameter IGNORE_SCHEINFEHLER und IGNORE_FLEXIBEL
 * @version 7_0_T     13.06.06  MM  Optionaler Paramter NO_RETRY, um Ediabas-Wiederholungen auszuschalten
 * @version 8_0_F     28.10.06  MM  Optionaler Paramter IGNORE_FELEXIBEL in IGNORE_PRUEFORT umbenannt
 * @version 9_0_T     24.01.08  PR  Fehlercodes werden nun grunds�tzlich in dezimaler und mit abgehangener Hex-Schreibweise gespeichert
 * @version 10_0_F    04.08.08  PR  Version von T zu F-Version gemacht. 
 * @version 11_0_T    05.02.16  MKe Parameter TIMEOUT als globalen PP Timeout in Sekunden hinzugef�gt.
 * @version 12_0_F    10.05.16  FS  Produktive Freigabe Version 11_0_T.
 * @version	13_0_T	  22.08.16  TB  Erweiterungen f�r Paralleldiagnose analog zur DiagToleranzPP (LOP 2015), Imports + Generics eingebaut + Compiler Warnungen behoben, AWT-Parameter hinzu<BR>
 * @version	14_0_F	  25.01.17  MKe F-Version 
 */
public class DiagFSLesenFunkt_14_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DiagFSLesenFunkt_14_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur mit obigem Verhalten
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public DiagFSLesenFunkt_14_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 *
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		// Wenn ein anderer Job als FS_LESEN_FUNKTIONAL verwendet werden soll, muss es angegeben werden
		String[] args = { "JOB", "EXCLUDE_LIST", "INCLUDE_LIST", "CLEAR_PP", "IGNORE_SCHEINFEHLER", "IGNORE_PRUEFORT", "NO_RETRY", "TIMEOUT", "AWT", "TAG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		// Zwingend ist nur der Parameter SGBD
		String[] args = { "SGBD" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
	 */
	@Override
	public boolean checkArgs() {
		return super.checkArgs();
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		Vector<Ergebnis> ergListeZwischen = new Vector<Ergebnis>();

		int status = STATUS_EXECUTION_OK;

		Pruefling prueflinge[] = null; // alle Prueflinge im virtuellen Fahrzeug

		String temp = "";
		String sgbd = "";
		String job = "";
		String jobResults = "";

		String sgbdPruefling = ""; // Angegebene SGBD im Pr�fling

		String SGAdresseDez = ""; // Dezimale Adresse des Steuerger�ts
		String sSGBDAktuell; // Aktueller Name der SGBD
		String sGruppeAktuell; // Aktuelle Gruppe
		String AktCodes = ""; // String in dem die zum SG geh�renden FehlerCodes abgelegt werden

		String FSAnzahl;

		int i = 0;
		int resultAnz = 0; // Anzahl der
		int Timeout = 0;

		boolean bFSLoeschen = false;

		// Retry-Comm Settings
		boolean noRetry = false;
		String currentRetryValue = null;

		String jobargs = "";
		String awt = null;
		String[] excludeList = null;
		String[] includeList = null;
		String[] usedSGBDen = null; // Alle verwendeten SGBDen werden hier abgelegt
		Vector<String> vErrorCodes = new Vector<String>();

		Hashtable<String, String> group2address = new Hashtable<String, String>();
		Hashtable<String, String> address2group = new Hashtable<String, String>();
		Hashtable<String, String> address2sgbd = new Hashtable<String, String>();
		Hashtable<String, String> ErrorCodesProAddress = new Hashtable<String, String>(); // Pro SG werden die Fehlercodes abgelegt
		HashSet<String> ignoreAttribs = new HashSet<String>(); //nimmt die Namen der Attribute auf, welche in den Pr�flingen die auszublendenden Fehlercodes spezifizieren
		Hashtable<String, HashSet<String>> codesPerSgbdToIgnore = new Hashtable<String, HashSet<String>>(); //enth�lt pro SGBD eine Menge von Fehlercodes, die zu ignorieren sind (Schl�ssel ist SGBD, Wert eine HashSet mit den Codes) 

		Vector<String> requestedGroups = new Vector<String>();
		Vector<String> requestedAddresses = new Vector<String>();
		Vector<String> responseAddresses = new Vector<String>();
		Vector<String> noResponseAddresses = new Vector<String>();

		Vector<String> requestedSGBDen = new Vector<String>();

		DialogThread dialogThread = null;

		EdiabasProxyThread ediabas = null;

		//Paralleldiagnose
		boolean parallel = false;
		String tag = null;
		DeviceManager devMan = null;

		try {
			//Parameter holen und pr�fen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				else {
					// zwingende Parameter
					sgbd = getArg( getRequiredArgs()[0] );

					// OPTIONALE PARAMETER

					// Wenn das optinale Argument nicht belegt ist, wird standardmaessig der Job FS_LESEN_FUNKTIONAL verwendet
					job = getArg( getOptionalArgs()[0] );
					if( job == null )
						job = "FS_LESEN_FUNKTIONAL";

					if( job.toUpperCase().compareTo( "FS_LESEN_FUNKTIONAL" ) != 0 && job.toUpperCase().compareTo( "IS_LESEN_FUNKTIONAL" ) != 0 )
						throw new PPExecutionException( PB.getString( "Parametrierfehler JOB" ) );

					// Auswertung einer INCLUDE- und EXCLUDE-List
					if( getArg( getOptionalArgs()[1] ) != null )
						excludeList = splitArg( getArg( getOptionalArgs()[1] ) );
					if( getArg( getOptionalArgs()[2] ) != null )
						includeList = splitArg( getArg( getOptionalArgs()[2] ) );

					// Soll im Anschluss der Fehlerspeicher gel�scht werden?
					if( getArg( getOptionalArgs()[3] ) != null )
						bFSLoeschen = true;

					// Beziehe die Namen der Attribute, welche die auszublenden-
					// den Fehlercodes spezifizieren und �berf�hre diese in eine 
					// gemeinsame HashSet (zur Vermeidung doppelter Eintr�ge). 
					String[] strArray; //Puffer
					if( getArg( getOptionalArgs()[4] ) != null ) {
						strArray = splitArg( getArg( getOptionalArgs()[4] ) ); //beziehe die Attribut-Namen aus Parameter IGNORE_SCHEINFEHLER
						if( strArray != null )
							for( i = 0; i < strArray.length; i++ )
								ignoreAttribs.add( strArray[i] );
					}

					if( getArg( getOptionalArgs()[5] ) != null ) {
						strArray = splitArg( getArg( getOptionalArgs()[5] ) ); //beziehe die Attribut-Namen aus Parameter IGNORE_FLEXIBEL                    
						if( strArray != null )
							for( i = 0; i < strArray.length; i++ )
								ignoreAttribs.add( strArray[i] );
					}

					// Sollen die Ediabas-Retry-Einstellungen ge�ndert werden?
					if( getArg( getOptionalArgs()[6] ) != null ) {
						if( getArg( getOptionalArgs()[6] ).equalsIgnoreCase( "TRUE" ) )
							noRetry = true;
					}

					//Timeout for single request of all not replied ECU�s
					if( getArg( getOptionalArgs()[7] ) != null ) {
						Timeout = Integer.parseInt( getArg( getOptionalArgs()[7] ) ) * 1000; //for milliseconds
					}

					//AWT
					if( getArg( "AWT" ) != null ) {
						awt = extractValues( getArg( "AWT" ) )[0];
					}

				}
			} catch( Exception e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}
			// ENDE Parameter holen und pr�fen

			// Nachricht auf Bildschirm sofern ein AWT gefordert!
			if( awt != null ) {
				try {
					dialogThread = new DialogThread( awt );
					dialogThread.run();
				} catch( Exception e ) {
				}
			}

			// Alle Pr�flinge aus dem virtuellen Fahrzeug holen
			try {
				prueflinge = getPr�fling().getAuftrag().getPr�flinge(); // alle verf�gbaren Pr�flinge holen
				for( i = 0; i < prueflinge.length; i++ ) {
					temp = prueflinge[i].getAttribut( "GRUPPE" );
					sgbdPruefling = prueflinge[i].getAttribut( "SGBD" );

					// Pr�fe, welche der Attribute mit den auszublendenden 
					// Fehlercodes in diesem Pr�fling vorhanden sind.
					Iterator<String> iter = ignoreAttribs.iterator();
					String attribName;
					String attribValue;
					while( iter.hasNext() ) { //gehe die Attribut-Namen durch
						//  Ermittle den aktuellen Attribut-Namen.
						attribName = (String) iter.next();

						//  Frage das Attribut im aktuellen Pr�fling ab.
						attribValue = prueflinge[i].getAttribut( attribName );

						//  Beziehe die Fehlercodes aus dem ermittelten 
						//	Attribut.
						if( attribValue != null ) { //Attribut im Pr�fling vorhanden?
							//  Wenn f�r die SGBD schon zu ignorierende 
							//  Codes hinterlegt sind, dann die neuen Codes 
							//  mit anh�ngen. Andernfalls f�r die SGBD einen 
							//  neuen Eintrag in codesPerSgbdToIgnore 
							//  erzeugen.
							if( codesPerSgbdToIgnore.containsKey( sgbdPruefling ) ) { //F�r SGBD schon zu ignorierende Codes hinterlegt?                            			
								HashSet<String> toIgnore = (HashSet<String>) codesPerSgbdToIgnore.get( sgbdPruefling );

								//  Erweitert toIgnore um die neuen 
								//  Fehlercodes.
								String[] codeArray; //Puffer            
								codeArray = splitArg( attribValue );
								if( codeArray != null )
									for( int j = 0; j < codeArray.length; j++ )
										toIgnore.add( codeArray[j] );

								//Hinweis: Kein codesPerSgbdToIgnore.put notwendig, da Eintrag in der Hashtable codesPerSgbdToIgnore bereits vorhanden war und lediglich modifiziert wurde.
							} else { //Nein.
								HashSet<String> toIgnore = new HashSet<String>();

								//  Erweitert toIgnore um die neuen 
								//  Fehlercodes.
								String[] codeArray; //Puffer            
								codeArray = splitArg( attribValue );
								if( codeArray != null )
									for( int j = 0; j < codeArray.length; j++ )
										toIgnore.add( codeArray[j] );

								codesPerSgbdToIgnore.put( sgbdPruefling, toIgnore ); //initial in Hashtable ablegen (da f�r die SGBD dort noch nichts existierte)                    					
							}
						}
					}

					// handelt es sich um einen Diagnosepr�fling?
					if( (temp != null) && (temp.equals( "" ) == false) ) {
						if( requestedGroups.contains( temp ) == false ) {
							// Die CARB-Sgbd ist vom OBD-Pr�fling und wird generell ausgeblendet
							if( temp.equals( "CARB" ) == false ) {
								if( (includeList == null) && (excludeList == null) ) {
									requestedGroups.add( temp );
									requestedSGBDen.add( sgbdPruefling );
								} else if( (includeList != null) && (excludeList == null) ) {
									for( int j = 0; j < includeList.length; j++ )
										if( includeList[j].equals( temp ) ) {
											requestedGroups.add( temp );
											requestedSGBDen.add( sgbdPruefling );
											break;
										}
								} else if( (includeList == null) && (excludeList != null) ) {
									boolean found = false;
									for( int j = 0; j < excludeList.length; j++ )
										if( excludeList[j].equals( temp ) )
											found = true;
									if( !found ) {
										requestedGroups.add( temp );
										requestedSGBDen.add( sgbdPruefling );
									}
								} else if( (includeList != null) && (excludeList != null) ) {
									boolean found = false;
									for( int j = 0; j < excludeList.length; j++ )
										if( excludeList[j].equals( temp ) )
											found = true;
									for( int j = 0; j < includeList.length; j++ )
										if( (!found) && (includeList[j].equals( temp )) ) {
											requestedGroups.add( temp );
											requestedSGBDen.add( sgbdPruefling );
										}
								}
							} // if (temp.equals("CARB")==false)
								// Gruppendateiname in Vector speichern, um zu verhindern, dass ein Pr�fling doppelt auftaucht,
								// nur weil auf dieselbe Gruppendatei zugegriffen wird
						} // if ( requestedGroups.contains(temp)==false )
					} // if( (temp != null) && (temp.equals("")==false) )
				} // for
			} catch( Exception e ) {
				result = new Ergebnis( "ExecFehler", "GetPrueflinge", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Paralleldiagnose, Pr�fstandvariabel auslesen, ob Paralleldiagnose 
			try {
				Object pr_var;

				pr_var = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.TEMPORARY, "DIAG_PARALLEL" );
				if( pr_var != null ) {
					if( pr_var instanceof String ) {
						if( "TRUE".equalsIgnoreCase( (String) pr_var ) ) {
							parallel = true;
						}
					} else if( pr_var instanceof Boolean ) {
						if( ((Boolean) pr_var).booleanValue() ) {
							parallel = true;
						}
					}
				}
			} catch( VariablesException e ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				throw new PPExecutionException();
			}

			// Devicemanager
			devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();

			if( getArg( "TAG" ) != null )
				tag = getArg( "TAG" );

			if( parallel ) // Ediabas holen
			{
				try {
					ediabas = devMan.getEdiabasParallel( tag, sgbd );
				} catch( Exception ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Wrong Cascade configuration for parallel diagnosys", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				} catch( Throwable ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				}
			} else {
				try {
					ediabas = devMan.getEdiabas( tag );
				} catch( Exception ex ) {
					result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "No Ediabas Device available", Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();

				}
			}
			// <<<<< Paralleldiagnose            

			// Auslesen/Setzen der Retry-Comm Parameter, falls gew�nscht 
			if( noRetry ) {
				try {
					currentRetryValue = ediabas.apiGetConfig( "RetryComm" );
					if( currentRetryValue == null )
						currentRetryValue = "1";
					if( currentRetryValue.equalsIgnoreCase( "1" ) ) {
						ediabas.apiSetConfig( "RetryComm", "0" );
					}
				} catch( Exception e ) {
					e.printStackTrace();
					result = new Ergebnis( "DiagFehler", "EDIABAS", "apiSetConfig", "RetryComm", "0", "", "", "", "", "0", "", "", "", "Exception", e.getMessage(), Ergebnis.FT_IGNORE );
					ergListe.add( result );
				}
			}

			// Auslesen der Steuerger�teadressen
			for( i = 0; i < requestedGroups.size(); i++ ) {

				try {
					temp = ediabas.executeDiagJob( sgbd, "GRP2SGADR", requestedGroups.elementAt( i ).toString(), "" );
					if( temp.equalsIgnoreCase( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, "GRP2SGADR", requestedGroups.elementAt( i ).toString(), "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();
					} else {
						temp = ediabas.getDiagResultValue( "SG_ADR" );
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, "GRP2SGADR", requestedGroups.elementAt( i ).toString(), "SG_ADR", temp, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
						requestedAddresses.add( temp );
						group2address.put( requestedGroups.elementAt( i ), temp );
						address2group.put( temp, requestedGroups.elementAt( i ) );
						address2sgbd.put( temp, requestedSGBDen.elementAt( i ) );
					}

				} catch( ApiCallFailedException acfe ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, "GRP2SGADR", requestedGroups.elementAt( i ).toString(), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );

					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				} catch( Exception e ) {
					result = new Ergebnis( "Exception", "EDIABAS", sgbd, "GRP2SGADR", requestedGroups.elementAt( i ).toString(), "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO );
					ergListe.add( result );
					e.printStackTrace();
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				}
			}

			usedSGBDen = new String[requestedSGBDen.size()]; // Mehr als die SGBDen, die im virtuellen Fahrzeug vorhanden sind, werden nicht abgelegt

			// Ausf�hren des FS_LESEN_FUNKTIONAL
			try {
				temp = ediabas.executeDiagJob( sgbd, job, "", jobResults );
				//System.out.println("Zeige JobStatus " + temp);
				if( temp.equals( "OKAY" ) == false ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				} else {
					// Anzahl der Resultsets lesen
					resultAnz = ediabas.getDiagJobSaetze() - 1;
					result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, "", "JOB_STATUS", "OKAY (" + resultAnz + " Resultsets)", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
					status = STATUS_EXECUTION_OK;
				}
			}
			// Fehler beim Ausf�hren des EDIABAS-Jobs
			catch( ApiCallFailedException acfe ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				throw new PPExecutionException();
			}
			// Allgemeiner Fehler
			catch( Exception e ) {
				result = new Ergebnis( "Exception", "Ediabas", sgbd, job, "", "", "", "", "", "0", "", "", "", "", e.getMessage(), Ergebnis.FT_NIO );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				throw new PPExecutionException();
			}

			// Abarbeitung der einzelnen Fehler
			try {
				int IndexUsedSGBDen = 0;
				for( i = 1; i < (resultAnz); i++ ) {
					try {
						status = STATUS_EXECUTION_ERROR;
						// ist das Resultset gueltig?
						if( ediabas.getDiagResultValue( i, "JOB_STATUS" ).equals( "OKAY" ) == true ) {
							SGAdresseDez = ediabas.getDiagResultValue( i, "ID_SG_ADR" );
							if( requestedAddresses.contains( SGAdresseDez ) == true ) {
								FSAnzahl = ediabas.getDiagResultValue( i, "F_ANZ" );
								responseAddresses.add( SGAdresseDez ); // Legt die dezimale Adresse ab

								AktCodes = getErrorCodesString( ediabas, i, Integer.parseInt( FSAnzahl ) ); // Holt die FehlerCodes ab
								if( AktCodes.length() > 0 ) {
									IndexUsedSGBDen++;
									usedSGBDen[IndexUsedSGBDen] = SGAdresseDez;
									ErrorCodesProAddress.put( SGAdresseDez, AktCodes );
								}
							}
						}
					} catch( ApiCallFailedException acfe ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobargs, "Resultset " + i, "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ) + " " + ediabas.apiErrorCode(), ediabas.apiErrorText(), Ergebnis.FT_NIO );
						ergListe.add( result );
					} catch( Exception e ) {
						e.printStackTrace();
						result = new Ergebnis( "Exception", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
					}
				}

				// System.out.println("Fertig mit dem Einlesen der Orte");
				// Erzeugen der Ergebniss�tze
				// ben�tigt werden die Namen der SGBDen und die dazugeh�rigen Fehlercodes
				try {
					sSGBDAktuell = "";
					sGruppeAktuell = "";
					// Solange zu pr�fende SGBDen vorhanden sind
					for( int iSGBDZaehler = 1; iSGBDZaehler <= IndexUsedSGBDen; iSGBDZaehler++ ) {

						if( usedSGBDen[iSGBDZaehler].length() > 0 ) // Adresse ist nicht leer
						{
							sSGBDAktuell = String.valueOf( address2sgbd.get( usedSGBDen[iSGBDZaehler] ) );
							sGruppeAktuell = String.valueOf( address2group.get( usedSGBDen[iSGBDZaehler] ) );
							AktCodes = String.valueOf( ErrorCodesProAddress.get( usedSGBDen[iSGBDZaehler] ) ); // Fehler zu der SGBD auslesen
							vErrorCodes = getStringToVector( AktCodes, ";" ); // Fehler in Array aufteilen

							if( vErrorCodes.size() > 0 ) { // Werden f�r dieses SG Fehlereintr�ge ausgeblendet?
								ergListeZwischen = getErgebnisse( ediabas, sGruppeAktuell, sSGBDAktuell, vErrorCodes, codesPerSgbdToIgnore );
								// F�r jedes Element wird ein eigener ResultSet angelegt.
								for( i = 0; i < ergListeZwischen.size(); i++ )
									ergListe.add( ergListeZwischen.elementAt( i ) );

							} // ENDE: if (ErrorCodes.length > 0)
						} // ENDE: if (usedSGBDen[t].length() > 0)
					}
				} catch( Exception e ) {
					e.printStackTrace();
					result = new Ergebnis( "Exception", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
				}
			} catch( Exception e ) {
				result = new Ergebnis( "ExecFehler", "DiagFSLesenFunkt", "Ergebnis holen", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				e.printStackTrace();
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}

			// ===================================================
			// ===================================================
			// Nachfordern der Jobs mit physikalischer Adressierung

			// Welche SG' aus dem virtuellen Fahrzeug haben denn alles nicht geantwortet?
			for( i = 0; i < requestedAddresses.size(); i++ ) {
				if( responseAddresses.contains( requestedAddresses.elementAt( i ) ) == false )
					noResponseAddresses.add( requestedAddresses.elementAt( i ) );
			}

			long t_start = System.currentTimeMillis();
			long t_temp;

			for( i = 0; i < noResponseAddresses.size(); i++ ) {
				ergListeZwischen = null;
				String physJobArgs = null;

				t_temp = System.currentTimeMillis();

				if( ((t_temp - t_start) > Timeout) && (Timeout != 0) ) {
					result = new Ergebnis( "Exception", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "Timeout" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				}

				try {
					sSGBDAktuell = "";
					sGruppeAktuell = "";
					physJobArgs = (String) (noResponseAddresses.elementAt( i ));
					temp = ediabas.executeDiagJob( sgbd, "FS_LESEN_FUNKTIONAL", physJobArgs, "" );
					if( temp.equals( "OKAY" ) == true ) {
						sSGBDAktuell = (String) address2sgbd.get( noResponseAddresses.elementAt( i ) );
						sGruppeAktuell = (String) address2group.get( noResponseAddresses.elementAt( i ) );
						int iAnzahl = Integer.parseInt( ediabas.getDiagResultValue( 1, "F_ANZ" ) );
						vErrorCodes = getErrorCodesVector( ediabas, 1, iAnzahl );
						ergListeZwischen = getErgebnisse( ediabas, sGruppeAktuell, sSGBDAktuell, vErrorCodes, codesPerSgbdToIgnore );
						// Ausgabe der Ergebnis-S�tze
						for( int iErg = 0; iErg < ergListeZwischen.size(); iErg++ )
							ergListe.add( ergListeZwischen.elementAt( iErg ) );
					} else {
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, physJobArgs + "(" + (String) (address2group.get( noResponseAddresses.elementAt( i ) )) + ")", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				} catch( ApiCallFailedException acfe ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, physJobArgs + "(" + (String) (address2group.get( noResponseAddresses.elementAt( i ) )) + ")", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				} catch( Exception e ) {
					e.printStackTrace();
					result = new Ergebnis( "Exception", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
			}

			//===========================================
			// Fehlerspeicher l�schen
			if( bFSLoeschen == true ) { // Es soll gel�scht werden
				//System.out.println("Es soll gel�scht werden");
				try {
					job = job.substring( 0, 2 ) + "_LOESCHEN_FUNKTIONAL"; // Egal, ob FS oder IS, der hintere Teil des Jobs ist gleich
					jobargs = "";
					temp = ediabas.executeDiagJob( sgbd, job, jobargs, "" );
					if( temp.equals( "OKAY" ) == false ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobargs, "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
						throw new PPExecutionException();
					} else {
						// Anzahl der Resultsets lesen
						resultAnz = ediabas.getDiagJobSaetze() - 1;
						result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, jobargs, "JOB_STATUS", "OKAY (" + resultAnz + " Resultsets)", "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
						status = STATUS_EXECUTION_OK;

					}
				} catch( ApiCallFailedException acfe ) {
					result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobargs, "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				} catch( Exception e ) {
					result = new Ergebnis( "Exception", "Ediabas", sgbd, job, "", "", "", "", "", "0", "", "", "", "", e.getMessage(), Ergebnis.FT_NIO );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
					throw new PPExecutionException();
				}

				// Alle Resultsets lesen
				for( i = 1; i < (resultAnz); i++ ) {
					try {
						// ist das Resultset gueltig?
						if( ediabas.getDiagResultValue( i, "JOB_STATUS" ).equals( "OKAY" ) == true ) {
							// Adresse auslesen und in Vector schreiben
							temp = ediabas.getDiagResultValue( i, "ID_SG_ADR" );
							responseAddresses.add( temp );
						}
					} catch( ApiCallFailedException acfe ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, jobargs, "Resultset " + i, "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ) + " " + ediabas.apiErrorCode(), ediabas.apiErrorText(), Ergebnis.FT_NIO );
						ergListe.add( result );
					} catch( Exception e ) {
						e.printStackTrace();
						result = new Ergebnis( "Exception", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
					}
				}

				// Vergleich der Listen und Bef�llen der noResponseAddresses-Liste
				for( i = 0; i < requestedAddresses.size(); i++ ) {
					if( responseAddresses.contains( requestedAddresses.elementAt( i ) ) == false ) {
						noResponseAddresses.add( requestedAddresses.elementAt( i ) );
					}
				}

				// Alle nicht antwortenden SG's physikalisch anfragen

				for( i = 0; i < noResponseAddresses.size(); i++ ) {
					String physJobArgs = null;
					try {

						//
						physJobArgs = (String) (noResponseAddresses.elementAt( i )) + jobargs;
						//System.out.println("Args " + physJobArgs);
						temp = ediabas.executeDiagJob( sgbd, job, physJobArgs, "" );
						if( temp.equals( "OKAY" ) == true ) {
							result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, physJobArgs + "(" + (String) (address2group.get( noResponseAddresses.elementAt( i ) )) + ")", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						} else {
							result = new Ergebnis( "Diagnose", "EDIABAS", sgbd, job, physJobArgs + "(" + (String) (address2group.get( noResponseAddresses.elementAt( i ) )) + ")", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
							ergListe.add( result );
							status = STATUS_EXECUTION_ERROR;
						}
					} catch( ApiCallFailedException acfe ) {
						result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, physJobArgs + "(" + (String) (address2group.get( noResponseAddresses.elementAt( i ) )) + ")", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ediabas.apiErrorCode() + ": " + ediabas.apiErrorText(), Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					} catch( Exception e ) {
						e.printStackTrace();
						result = new Ergebnis( "Exception", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				}
			}
			// Ende Fehlerspeicher l�schen
			//===========================================

			// R�cksetzen der Retry-Comm Parameter, falls n�tig
			if( noRetry && (currentRetryValue.equalsIgnoreCase( "1" )) ) {
				try {
					ediabas.apiSetConfig( "RetryComm", "1" );
				} catch( Exception e ) {
					e.printStackTrace();
					result = new Ergebnis( "DiagFehler", "EDIABAS", "apiSetConfig", "RetryComm", "1", "", "", "", "", "0", "", "", "", "Exception", e.getMessage(), Ergebnis.FT_IGNORE );
					ergListe.add( result );
				}
			}
		} catch( PPExecutionException ppee ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "DiagFSLesenFunkt", "Basis", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			e.printStackTrace();
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable t ) {
			result = new Ergebnis( "ExecFehler", "DiagFSLesenFunkt", "basis", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			t.printStackTrace();
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		// R�cksetzen der Retry-Comm Parameter, falls n�tig
		if( currentRetryValue != null ) {
			if( noRetry && (currentRetryValue.equalsIgnoreCase( "1" )) ) {
				try {
					ediabas.apiSetConfig( "RetryComm", "1" );
				} catch( Exception e ) {
					e.printStackTrace();
					result = new Ergebnis( "DiagFehler", "EDIABAS", "apiSetConfig", "RetryComm", "1", "", "", "", "", "0", "", "", "", "Exception", e.getMessage(), Ergebnis.FT_IGNORE );
					ergListe.add( result );
				}
			}
		}

		//Freigabe/Beenden des Dialog-Threads
		if( dialogThread != null ) {
			dialogThread.interrupt();
		}

		// Paralleldiagnose
		if( parallel ) {
			try {
				devMan.releaseEdiabasParallel( ediabas, tag, sgbd );
			} catch( Exception ex ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			} catch( Throwable t ) {
				result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		setPPStatus( info, status, ergListe );
	}

	/** Dieser Thread macht der DialogBox.
	 */
	private class DialogThread implements Runnable {
		private boolean dialogRunning = false;
		private UserDialog myDialog = null;
		private String message;

		public DialogThread( String awt ) throws Exception {
			dialogRunning = true;
			message = awt;
			myDialog = getPr�flingLaufzeitUmgebung().getUserDialog();
		}

		@Override
		public void run() {
			try {
				myDialog.displayMessage( "Functional", message, -1 );
			} catch( Exception e ) {
			}
			dialogRunning = false;
		}

		public void interrupt() {
			while( this.dialogRunning() ) {
				try {
					Thread.sleep( 10 );
				} catch( Exception e ) {
				}
			}
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( Exception e ) {
			}
		}

		public boolean dialogRunning() {
			return dialogRunning;
		}

	}

	// berechnet den dezimal-Wert einer hex-Zeichenkette
	private int hexToDez( String sValue ) {
		double erg = 0;
		int retValue = 0;
		int count = 0;

		sValue = sValue.toUpperCase();

		while( sValue.length() > 0 ) // Solange noch Werte zum Umrechnen in der Zeichenkette vorhanden sind
		{
			int laenge = sValue.length();
			erg = erg + (Math.pow( 16, count++ ) * convertHex( sValue.charAt( laenge - 1 ) ));
			if( laenge > 1 ) {
				sValue = sValue.substring( 0, laenge - 1 );
			} else
				sValue = "";
		}
		retValue = (int) erg;
		return retValue;
	} // Ende hexToDez

	// Umrechnungstabelle fuer HexInDezimal
	private double convertHex( char cValue )
	// gibt dem einem Hex-Zeichen zugeh�rigen int-Wert zur�ck
	{
		switch( cValue ) {
			case '0':
				return 0;
			case '1':
				return 1;
			case '2':
				return 2;
			case '3':
				return 3;
			case '4':
				return 4;
			case '5':
				return 5;
			case '6':
				return 6;
			case '7':
				return 7;
			case '8':
				return 8;
			case '9':
				return 9;
			case 'A':
				return 10;
			case 'B':
				return 11;
			case 'C':
				return 12;
			case 'D':
				return 13;
			case 'E':
				return 14;
			case 'F':
				return 15;
			default:
				return -1; // Ungueltiger Wert
		}
	} // Ende convertHex

	/**
	 * Methode um die Fehlerorte und Fehlertexte pro SG zu erhalten
	 * @param ediabas
	 * @param sSGBD Welche SGBD wird zum auslesen der Fehlercodes ben�tigt
	 * @param Codes Welche Fehlerorte beinhaltet das SG
	 * @param codesPerSgbdToIgnore Enth�lt f�r jede SGBD die auszublendenden
	 * 		  Fehlercodes.
	 * @return Vector der die Ergebnis-S�tze zu diesem SG enth�lt 
	 */
	private Vector<Ergebnis> getErgebnisse( EdiabasProxyThread ediabas, String sGruppe, String sSGBD, Vector<String> Codes, Hashtable<String, HashSet<String>> codesPerSgbdToIgnore ) {
		Vector<Ergebnis> vErgebnisse = new Vector<Ergebnis>();
		Ergebnis result;
		int anzahlSaetze;
		String sUnbekannterFehlerort = "";
		int FehlerCode;
		boolean allToIgnore = true; //gibt an, ob alle spezifizierten Fehlercodes zu ignorieren sind
		HashSet<String> codesToIgnore; //nimmt die auszublendenden Fehlercodes auf

		try {
			//  Wenn der SGBD-Name erst beim SGBD-SIV ermittelt wird, mu� �ber 
			//  die Gruppe der Namen der SGBD ermittelt werden.
			if( sSGBD.toUpperCase().indexOf( "_SIV" ) != -1 )
				sSGBD = getSGBDVariante( ediabas, sGruppe );

			//  Ermittle die Fehler, welche f�r die vorliegende SGBD auszu- 
			//	blenden sind.
			codesToIgnore = (HashSet<String>) codesPerSgbdToIgnore.get( sSGBD );

			//  Pr�fe, ob alle Fehler der SGBD ausgeblendet werden sollen.
			for( int i = 0; i < Codes.size(); i++ ) //alle Fehler durchgehen
				if( codesToIgnore == null || !codesToIgnore.contains( (String) Codes.get( i ) ) ) { //Soll ein Fehler NICHT ausgeblendet werden?
					allToIgnore = false; //dann merken ... 
					break; //und Schleife verlassen
				}

			//  Wenn alle Fehler der SGBD auszublenden sind, dann bei den zu 
			//  erzeugenden Ergebnissen (sind FT_IGNORE) nur den Fehlerort an-
			//  stelle des Fehlerorttextes angeben. Andernfalls alle Fehler 
			//  durchgehen und f�r jeden Fehlerort den Fehlerorttext beziehen 
			//  und anschlie�end entscheiden, welche Fehler auszublenden 
			//  (FT_IGNORE) sind und welche nicht (FT_NIO).
			if( allToIgnore ) { //Sind alle Fehler auszublenden?  
				for( int i = 0; i < Codes.size(); i++ ) { //alle Fehler durchgehen und f�r jeden Fehler ein IGNORE-Ergebnis erzeugen        	
					result = new Ergebnis( "FS", "EDIABAS", sSGBD, "FS_LESEN_FUNKTIONAL", "", "F_ORT_NR", (String) Codes.get( i ) + " (HEX " + toHex( (String) Codes.get( i ), true ) + ")", "", "", "0", "", "", "", (String) Codes.get( i ), "", Ergebnis.FT_IGNORE );
					vErgebnisse.add( result );
				}
				Codes.clear(); //alle Fehler abgearbeitet

			} else { //Nein. Dann Fehlerorttexte beziehen und Ergebnisse generieren.
				ediabas.apiJob( sSGBD, "_table", "FORTTEXTE", "" );
				anzahlSaetze = Integer.parseInt( String.valueOf( ediabas.getDiagResultValue( 0, "SAETZE" ) ) );
				sUnbekannterFehlerort = ediabas.getDiagResultValue( anzahlSaetze, "COLUMN1" );
				// F�r jeden Satz wird ein Vergleich mit den Fehlercodes gemacht
				for( int iSGBDFOrte = 1; iSGBDFOrte <= anzahlSaetze; iSGBDFOrte++ ) {
					FehlerCode = hexToDez( (ediabas.getDiagResultValue( iSGBDFOrte, "COLUMN0" )).substring( 2 ) );
					//for (int iSGFehler=0; iSGFehler < Codes.size(); iSGFehler++) {
					for( int iSGFehler = Codes.size() - 1; iSGFehler >= 0; iSGFehler-- ) { //Codes von hinten durchgehen, da Elemente entfernt werden
						if( Codes.elementAt( iSGFehler ).toString().length() > 0 ) {
							if( FehlerCode == Integer.parseInt( Codes.elementAt( iSGFehler ).toString() ) ) {
								if( codesToIgnore != null && codesToIgnore.contains( "" + FehlerCode ) ) { // Fehler kann ignoriert werden
									result = new Ergebnis( "FS", "EDIABAS", sSGBD, "FS_LESEN_FUNKTIONAL", "", "F_ORT_NR", Codes.elementAt( iSGFehler ).toString() + " (HEX " + toHex( Codes.elementAt( iSGFehler ).toString(), true ) + ")", "", "", "0", "", "", "", ediabas.getDiagResultValue( iSGBDFOrte, "COLUMN1" ), "", Ergebnis.FT_IGNORE );
								} else {
									result = new Ergebnis( "FS", "EDIABAS", sSGBD, "FS_LESEN_FUNKTIONAL", "", "F_ORT_NR", Codes.elementAt( iSGFehler ).toString() + " (HEX " + toHex( Codes.elementAt( iSGFehler ).toString(), true ) + ")", "", "", "0", "", "", "", ediabas.getDiagResultValue( iSGBDFOrte, "COLUMN1" ), "", Ergebnis.FT_NIO );
								}
								vErgebnisse.add( result );
								Codes.removeElementAt( iSGFehler ); //Fehler rausnehmen -> die weiteren Durchl�ufe werden schneller
								break; // Fehler gefunden -> raus!
							}
						}
					}
				}
				//for (int iSGFehlerUnbekannt=0;iSGFehlerUnbekannt<Codes.size();iSGFehlerUnbekannt++) {
				for( int iSGFehlerUnbekannt = Codes.size() - 1; iSGFehlerUnbekannt >= 0; iSGFehlerUnbekannt-- ) { //Codes von hinten durchgehen, da Elemente entfernt werden		            	
					if( Codes.elementAt( iSGFehlerUnbekannt ).toString().length() > 0 ) {
						if( codesToIgnore != null && codesToIgnore.contains( Codes.elementAt( iSGFehlerUnbekannt ).toString() ) ) { // Fehler kann ignoriert werden 
							result = new Ergebnis( "FS", "EDIABAS", sSGBD, "FS_LESEN_FUNKTIONAL", "", "F_ORT_NR", Codes.elementAt( iSGFehlerUnbekannt ).toString() + " (HEX " + toHex( Codes.elementAt( iSGFehlerUnbekannt ).toString(), true ) + ")", "", "", "0", "", "", "", sUnbekannterFehlerort, "", Ergebnis.FT_IGNORE );
						} else {
							result = new Ergebnis( "FS", "EDIABAS", sSGBD, "FS_LESEN_FUNKTIONAL", "", "F_ORT_NR", Codes.elementAt( iSGFehlerUnbekannt ).toString() + " (HEX " + toHex( Codes.elementAt( iSGFehlerUnbekannt ).toString(), true ) + ")", "", "", "0", "", "", "", sUnbekannterFehlerort, "", Ergebnis.FT_NIO );
						}
						vErgebnisse.add( result );
						Codes.removeElementAt( iSGFehlerUnbekannt );
					}
				}
			}
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "Exception", "EDIABAS", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
			vErgebnisse.add( result );
		}

		return vErgebnisse;
	}

	/**
	 * Hilfsmethode
	 * @param ediabas
	 * @param iResultAktuell
	 * @param iFSAnzahl
	 * @return
	 */
	private String getErrorCodesString( EdiabasProxyThread ediabas, int iResultAktuell, int iFSAnzahl ) {
		String sFehlerCodes = "";
		Vector<String> vCodes = new Vector<String>();

		vCodes = getErrorCodesVector( ediabas, iResultAktuell, iFSAnzahl );
		if( vCodes.size() > 0 ) {
			for( int i = 0; i < vCodes.size(); i++ ) {
				if( sFehlerCodes.length() == 0 ) // Der erste Fehlerort
					sFehlerCodes = (String) vCodes.elementAt( i );
				else
					sFehlerCodes = sFehlerCodes + ";" + (String) vCodes.elementAt( i );
			}
		}
		return sFehlerCodes;
	}

	/**
	 * Methode f�r's Auslesen der Fehler eines Steuerger�tes
	 * @param ediabas
	 * @param iResultAktuell Aktueller Satz, der ausgelesen werden mu�
	 * @param iFSAnzahl Anzahl der Fehler f�r dieses SG
	 * @return Vector der alle FehlerOrte beihnaltet
	 */

	private Vector<String> getErrorCodesVector( EdiabasProxyThread ediabas, int iResultAktuell, int iFSAnzahl ) {
		Vector<String> vFehlerCodes = new Vector<String>();
		String sFSOrt = "";

		try {
			if( iFSAnzahl > 0 )
				for( int t = 1; t <= iFSAnzahl; t++ ) {
					sFSOrt = ediabas.getDiagResultValue( iResultAktuell, "F_ORT" + String.valueOf( t ) + "_NR" );
					vFehlerCodes.add( sFSOrt );
				}
		} catch( Exception e ) {
			e.printStackTrace();
		}

		return vFehlerCodes;
	}

	/**
	 * Methode f�r das Ermitteln einer SGBD-Variante sofern im Pr�fling auf das Ergebnis des SGBD-SIV's verwiesen wird
	 * @param ediabas
	 * @param sGruppe Gruppen-SGBD, die das SG enth�lt
	 * @return String Namen der SGBD
	 */
	private String getSGBDVariante( EdiabasProxyThread ediabas, String sGruppe ) throws PPExecutionException {

		String sVariante = "";

		try {
			// Devicemanager
			ediabas.apiJob( sGruppe, "INITIALISIERUNG", "", "VARIANTE" ); //Achtung Jobstatus existiert nicht!!!
			sVariante = ediabas.getDiagResultValue( 0, "VARIANTE" ).toUpperCase();
		} catch( Exception e ) {
			throw new PPExecutionException();
		}

		return sVariante;
	}

	/**
	 * Hilfsmethode f�r die Umwandlung eines Strings in einen Vector
	 * @param sBaseString String der in einen Vector aufgeteilt werden soll
	 * @param sTrenner Trennzeichen, nachdem der String aufgesplittet werden soll
	 * @return Vector der die einzelnen Elemente beinhaltet
	 */
	private Vector<String> getStringToVector( String sBaseString, String sTrenner ) {
		String[] sElemente;
		Vector<String> vReturn = new Vector<String>();
		sElemente = sBaseString.split( sTrenner );

		//System.out.println("Basis " + sBaseString);
		//   System.out.println("Trenner " + sTrenner);

		for( int i = 0; i < sElemente.length; i++ )
			vReturn.addElement( sElemente[i] );

		return vReturn;
	}

	/**
	 * Hilfsmethode zum Ermitteln ob ein IS zu ignorieren ist
	 * @param sIgnoreCodes
	 * @param iFehlerCode
	 * @return
	 */
	private boolean getIsCodeToIgnore( String sIgnoreCodes, int iFehlerCode ) {
		boolean bReturn = false;
		String[] sCodes = sIgnoreCodes.split( ";" );

		for( int i = 0; i < sCodes.length; i++ ) {
			if( sCodes[i].compareTo( String.valueOf( iFehlerCode ) ) == 0 ) {
				bReturn = true;
				break;
			}
		}

		return bReturn;
	}

	/**
	 * Konvertiert eine dezimalen Wert in seine hexadezmiale Darstellung (alle 
	 * Zeichen gro�geschrieben). 
	 * @param dezValue Der zu konvertierende Wert als long.
	 * @param formatAsPairs Angabe, ob die hexadezimalen Zeichen paarweise 
	 * 		  stehen sollen (z. B. 7B 52 19 statt 7B5219).
	 * @return Der hexadeziamle Wert.
	 */
	private String toHex( long dezValue, boolean formatAsPairs ) {
		String hexValue = Long.toHexString( dezValue ).toUpperCase();

		if( formatAsPairs ) { //Paarweise Anzeige (7B 52 19 statt 7B5219) gew�nscht? 
			if( hexValue.length() % 2 != 0 ) //paarweise Anzeige macht nur Sinn, wenn die Anzahl der Zeichen gerade ist
				hexValue = "0" + hexValue;

			StringBuffer buffer = new StringBuffer( hexValue );
			for( int i = buffer.length() - 1; i >= 0; i-- )
				if( i != 0 && i != buffer.length() - 1 && i % 2 == 0 )
					buffer.insert( i, ' ' );

			return buffer.toString();
		} else
			return hexValue;
	}

	/**
	 * Konvertiert eine dezimalen Wert in seine hexadezmiale Darstellung (alle 
	 * Zeichen gro�geschrieben). 
	 * @param dezValue Der zu konvertierende Wert als String.
	 * @param formatAsPairs Angabe, ob die hexadezimalen Zeichen paarweise 
	 * 		  stehen sollen (z. B. 7B 52 19 statt 7B5219).
	 * @return Der hexadeziamle Wert oder "", falls die Konvertierung nicht
	 * 		   m�glich war.
	 */
	private String toHex( String dezValue, boolean formatAsPairs ) {
		long dezValueLong;

		// Wandle eingegeben String in einen long um.
		try {
			dezValueLong = Long.parseLong( dezValue );
		} catch( NumberFormatException x ) {
			return "";
		}

		// Konvertiere.
		return toHex( dezValueLong, formatAsPairs );
	}

} // Ende der Pruefprozedur DiagFSLesenFunkt

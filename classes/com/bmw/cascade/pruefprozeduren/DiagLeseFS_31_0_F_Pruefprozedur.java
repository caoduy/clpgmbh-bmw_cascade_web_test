/*
 * DiagLeseFS_VX_X_X_YY_Pruefprozedur.java
 *
 */
package com.bmw.cascade.pruefprozeduren;

//import java.io.*;
import java.util.*;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.DeviceManager;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.ediabas.EdiabasResultNotFoundException;
import com.bmw.cascade.pruefstand.devices.ediabas.ApiCallFailedException;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;

/**
 * Implementierung der Pr�fprozedur, die per Diagnose den Fehlerspeicher ausliest und
 * dabei Fehler auf Basis des Fehlerortes ignoriert bzw. weitere Analysen durch Ausf�hrung der
 * Analyse-PPs vornimmt. Sollten nur zu ignorierende Fehler vorliegen, wird mit der �bergebenen
 * Clear-PP das L�schen vorgenommen.
 * @author Winkler, Crichton, M�ller BMW AG
 * @version 0_0_5_FA  ??.??.??  NC  Erste freigegebene Version? <br>
 *          0_0_6_TA  10.10.02  NC  Fehler HEX Code f�r alle E6x Bauriehen aktiviert <br>
 *          0_0_7_FA  07.11.02  NC  Freigabe: Fehler HEX Code f�r alle E6x Bauriehen aktiviert <br>
 *          0_0_8_TA  12.11.02  MM  Optionaler Parameter IGNORE_MESSAGE hinzu <br>
 *          0_0_9_FA  12.11.02  MM  Werkerhinweis beim Hexcode raus, Werksfreigabe <br>
 *          0_1_0_TA  18.06.04  NC  Pr�fprozedur umstruktuiert <br>
 *          0_1_1_FA  06.07.04  NC  Freigabe 0_1_0_TA <br>
 *          0_1_2_FA  06.07.04  NC  Version kurzzeitig zur�ckgesetzt nach Fehler<br>
 *          0_1_3_FA  07.07.04  NC  Bugfix auf Basis 0_1_1_FA <br>
 *          0_1_4_FA  13.08.04  NC  Fehlerspeicher l�schen wieder integriert<br>
 *          0_1_5_FA  13.08.04  MM  Bugfix: Auslesen des Hex-Codes gilt nicht f�r alle SGs<br>
 *          0_1_6_FA  18.10.04  NC  Pr�fprozedur schneller bei Ausblendung sehr viel Fehlereintr�ge<br>
 *          0_1_7_FA  25.10.04  NC  Debugausgaben ausgeschaltet<br>
 * 		    18_0_F    09.03.06  PR  Optionalen Parameter (PPLDB_TEXT) f�r Pr�fplandatenbank hinzugef�gt<br>
 *          23_0_T    01.09.08  TB  Traces Ausgaben + APDM Ausgaben im "Clear" Bereich der PP, Debug tempor�r ein <BR>
 *          25_0_T    12.10.08  MB  Neu: Parallel-Diagnose <BR>
 *          26_0_T    17.11.08  MB  Erweiterung der Fehlerbehandlung Parallel-Diagnose <BR>
 *          27_0_F    ??.??.??  MB  Erweiterung der Fehlerbehandlung Parallel-Diagnose <BR>
 *          28_0_T    04.04.04  RG  Abfrage und Ausgabe der Results: F_VORHANDEN_NR und F_VORHANDEN_TEXT in den Antworttext bzw. Anweisungstext <BR>
 *          29_0_F    04.04.04  RG  F-Version mit vorherigen �nderungen <BR>
 *          30_0_T	  28.01.16	MK  ErrorHandling angepasst, veraltete EDIABAS-Methode ersetzt.	
 */
public class DiagLeseFS_31_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    	static final long serialVersionUID = 1L;
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public DiagLeseFS_31_0_F_Pruefprozedur() {}

  /**
     * Erzeugt eine neue Pruefprozedur.
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public DiagLeseFS_31_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    /**
     * Initialsiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /**
     * Liefert die optionalen Argumente
     * @return eine Liste sie optionale Argurmenten
     */
    public String[] getOptionalArgs() {
        String[] args = {"JOB", "IGNORE", "ANALYSE_PP", "CLEAR_PP", "IGNORE_MESSAGE", "PPLDB_TEXT", "TAG"};
        return args;
    }
    
    /**
     * Liefert die zwingend erforderlichen Argumente
     * @return eine Liste sie zwingende Argurmenten
     */
    public String[] getRequiredArgs() {
        String[] args = {"SGBD"};
        return args;
    }
    
    /**
     * Pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
     * @return true wenn Argumente iO sind sonst false
     */
    public boolean checkArgs() {
        Pruefprozedur p;
        String temp;
        boolean ok;
        int i;
        
        try {
            ok = super.checkArgs();
            if( ok == true ) {
                //SGBD
                temp = getArg( getRequiredArgs()[0] );
                if( temp.indexOf(';') != -1 ) return false;
                //Job
                temp = getArg( getOptionalArgs()[0] );
                if( temp != null) {
                    if( temp.indexOf(';') != -1 ) return false;
                }
                //Ignore
                temp = getArg( getOptionalArgs()[1] );
                try {
                    if( temp != null ) temp = extractErrorNumbers(temp);
                } catch (NumberFormatException e) {
                    return false;
                }
                //Analyse-PPs
                temp = getArg( getOptionalArgs()[2] );
                if( temp != null ) {
                    String[] tempArray = splitArg(temp);
                    for( i=0; i<tempArray.length; i++ ) {
                        p = getPr�fling().getPr�fprozedur(tempArray[i]);
                        if( p == null ) return false;
                        if( p.checkArgs() == false ) return false;
                    }
                }
                //Clear-PP
                temp = getArg( getOptionalArgs()[3] );
                if( temp != null ) {
                    p = getPr�fling().getPr�fprozedur(temp);
                    if( p == null ) return false;
                    if( p.checkArgs() == false ) return false;
                }
            }
            return ok;
        } catch (Exception e) {
            return false;
        } catch (Throwable e) {
            return false;
        }
    }
    
    
    /**
     * F�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        // spezifische Variablen
        String sgbd = null;
        String job = null ;
        String clear = null;
        String ignoreMessageText = null;
        String temp = null;
        //boolean ignoreMessage = false;
        boolean errorPresent = false;
        String[] analyseNames = new String[0];
        String[] ignoreNumbers = new String[0];
        Pruefprozedur[] analysePruefprozeduren = new Pruefprozedur[0];
        Pruefprozedur clearPruefprozedur = null;
        Hashtable temporaryResults = new Hashtable();
        boolean DEBUG = true;
        
        //MBa: Paralleldiagnose:
        String tag = "";
        EdiabasProxyThread ept = null;
        boolean parallel = false;
        DeviceManager devMan = null;
        
       
        /***********************************************
         * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
         * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
         ***********************************************/
        try {
            UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
            myAnalyser.LogSetTestStepName(this.getName());
            getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
        } catch (Exception e) {
            try {
                getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
            }
            catch (Exception e1) {}
        }   
        
        //MBa: Paralleldiagnose
        try 
        {
            Object pr_var;

            pr_var=getPr�flingLaufzeitUmgebung().getPruefstandVariable(VariableModes.TEMPORARY, "DIAG_PARALLEL");
            if (pr_var!=null) 
            {
                if (pr_var instanceof String) 
                {
                    if ("TRUE".equalsIgnoreCase((String)pr_var))
                    {
                        parallel=true;
                    }
                }
                else if (pr_var instanceof Boolean)
                {
                    if (((Boolean)pr_var).booleanValue())
                    {
                        parallel=true;
                    }
                }
            }
        }
        catch (VariablesException e) 
        {
          	 result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "cannot read VariableModes.TEMPORARY, DIAG_PARALLEL", Ergebnis.FT_NIO_SYS );
           	 ergListe.add(result);
        }

        try {
            // Parameter holen / initialisieren
            try {
                // Parameter existenz
                if( checkArgs() == false ) throw new PPExecutionException("Parameterexistenz");
                // SGBD
                sgbd = extractValues( getArg( getRequiredArgs()[0] ) )[0];
                // Jobname
                job  = getArg( getOptionalArgs()[0] );
                if (job == null) job = "FS_LESEN";
                // Fehler die auszublenden sind
                temp = getArg( getOptionalArgs()[1] );
                if( temp != null ){
                    temp = extractErrorNumbers( temp );
                    ignoreNumbers = temp.split(";");
                }
                // Analyse Pr�fprozedur(en)
                temp = getArg( getOptionalArgs()[2] );
                if ( temp != null ) {
                    analyseNames = splitArg( temp );
                    analysePruefprozeduren = new Pruefprozedur[ analyseNames.length ];
                    for( int i=0; i<analysePruefprozeduren.length; i++ ) {
                        analysePruefprozeduren[i] = getPr�fling().getPr�fprozedur(analyseNames[i]);
                    }
                }
                // Job um Fehlerspeicher zu l�schen (falls vorhanden)
                clear = getArg( getOptionalArgs()[3] );
                // Sollen Fehler auf Fehlerprotokoll erscheinen, aber mit "Fehler kann ignoriert werden)
                temp = getArg( getOptionalArgs()[4] );
                if ( temp != null ) {
                    if ( temp.equalsIgnoreCase("TRUE") ) {
                        ignoreMessageText = PB.getString("ignoreMessage");
                        if (ignoreMessageText.startsWith("ignore")) {
                            ignoreMessageText = "Fehler kann ignoriert werden";
                        }
                    }
                }
                
                if (getArg("TAG")!=null) tag = getArg("TAG");
                
            } catch (PPExecutionException e) {
                if (e.getMessage() != null)
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            
            devMan = getPr�flingLaufzeitUmgebung().getDeviceManager();
            if (parallel)
            {
            	try
                {
                    ept = devMan.getEdiabasParallel(tag,sgbd);
                }
                catch (Exception ex)
                {
               	 result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "Wrong Cascade configuration for parallel diagnosys", Ergebnis.FT_NIO_SYS );
               	 ergListe.add(result);
               	 status = STATUS_EXECUTION_ERROR;
               	 throw new PPExecutionException();
                }
                catch (Throwable ex)
                {
               	 result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
               	 ergListe.add(result);
               	 status = STATUS_EXECUTION_ERROR;
               	 throw new PPExecutionException();
                }
            }
            else
            {
                ept=devMan.getEdiabas(tag);
            }
            
            // Ausf�hrung
            try {
                // EDIABAS Job ausf�hren
                temp = ept.executeDiagJob(sgbd,job,"","");
                if( temp.equals("OKAY") == false) {
                    result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
                    ergListe.add(result);
                    throw new PPExecutionException();
                } else {
                    result = new Ergebnis( "Status", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                    ergListe.add(result);
                }
                // Alle Fehlerspeicherinfos (tempor�r) speichern
                for (int i=1; i<(ept.getDiagJobSaetze()-1); i++) {
                    String fOrtNr = ept.getDiagResultValue(i, "F_ORT_NR");
                    String fOrtText = ept.getDiagResultValue(i, "F_ORT_TEXT");
                    
					String fVorhandenText = null;
					String fVorhandenNr = null;
					String fHexCode = null;
                    try {
                        fHexCode = ept.getDiagResultValue(i, "F_HEX_CODE");
                    } catch ( Exception ernfe ) {
                        // Nicht alle Steuerger�te unterst�tzen diese Zusatzinformation, also Fehler ignorieren
                        fHexCode = null;
                    }
					
					try {
						fVorhandenText = devMan.getEdiabas().getDiagResultValue(i, "F_VORHANDEN_TEXT");
					} catch( Exception ernfe ) {
						// Nicht alle Steuerger�te unterst�tzen diese Zusatzinformation, also Fehler ignorieren
						fVorhandenText = "";
					}
					
					try {
						fVorhandenNr = devMan.getEdiabas().getDiagResultValue(i, "F_VORHANDEN_NR");
					} catch( Exception ernfe ) {
						// Nicht alle Steuerger�te unterst�tzen diese Zusatzinformation, also Fehler ignorieren
						fVorhandenNr = "";
					}
					
                    String hwt = "";
                    String errorType = Ergebnis.FT_NIO;
                    // Flag setzen, notwendig um entscheiden zu k�nnen ob FS gel�scht werden soll
                    errorPresent = true;
                    // Falls Fehler ausgeblendet werden soll, FT_IGNORE oder Hinweistext setzen
                    for ( int j=0; j<ignoreNumbers.length; j++) {
                        if (ignoreNumbers[j].equalsIgnoreCase( fOrtNr )) {
                            if ( ignoreMessageText != null ) {
                                hwt = ignoreMessageText;
                            } else {
                                errorType = Ergebnis.FT_IGNORE;
                            }
                        }
                    }
                    // Existiert Zusatzinformation �ber einen Hex-Fehlercode?
                    if ( fHexCode!=null )
                        result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", fOrtNr+" (HEX "+fHexCode+")" , "", "", "0", "", fVorhandenNr, fVorhandenText, fOrtText, hwt, errorType );
                    else
                        result = new Ergebnis( "FS", "EDIABAS", sgbd, job, "", "F_ORT_NR", fOrtNr, "", "", "0", "", fVorhandenNr, fVorhandenText, fOrtText, hwt, errorType );
                    temporaryResults.put( fOrtNr, result );
                    if (DEBUG) writeDebug("error found "+fOrtNr+", "+fOrtText+", "+hwt+", "+errorType );
                }
            } catch( ApiCallFailedException e ) {
                result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), ept.apiErrorCode()+": " + ept.apiErrorText(), Ergebnis.FT_NIO );
                ergListe.add(result);
                throw new PPExecutionException();
            } catch( EdiabasResultNotFoundException e ) {
                if (e.getMessage() != null)
                    result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
                else
                    result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "", "", "", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO_SYS );
                ergListe.add(result);
                throw new PPExecutionException();
            }
            finally{
            
	            if (parallel ) //&& ept!=null)
	            {
	            	try{
	            		devMan.releaseEdiabasParallel(ept, tag, sgbd);
	            	}
	            	catch(Exception ex){
	            		result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
	            		ergListe.add(result);
	            		status = STATUS_EXECUTION_ERROR;
	            	} catch (Throwable t) {
	            		result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", "Wrong Cascade version for parallel-diagnosys", Ergebnis.FT_NIO_SYS );
	            		ergListe.add(result);
	            		status = STATUS_EXECUTION_ERROR;
	
	            	}
	            }
            }
            
            
            
            // Ggf. weitere Analyse f�r Fehler die (noch) nicht ausgeblendet werden k�nnen
            for ( int i=0; i<analysePruefprozeduren.length; i++) {
                boolean executeAnalyse = false;
                String[] tempErrors = extractErrorNumbers( analysePruefprozeduren[i].getArg("FORT") ).split(";");
                //int errorCount = 0;
                // Pr�fen ob Analysepr�fprozedur durchgef�hrt werden muss
                for (int j=0; j<tempErrors.length; j++) {
                    if (DEBUG) writeDebug("analyse PP "+analysePruefprozeduren[i].getName()+", error "+tempErrors[j] );
                    if (temporaryResults.containsKey( tempErrors[j] )) {
                        result = (Ergebnis) temporaryResults.get( tempErrors[j] );
                        if (result.getFehlerTyp() == Ergebnis.FT_NIO) {
                            if (DEBUG) writeDebug("analyse PP "+analysePruefprozeduren[i].getName()+", error "+tempErrors[j]+" match" );
                            analysePruefprozeduren[i].setAttribut("ERROR_SGBD", sgbd );
                            analysePruefprozeduren[i].setAttribut("ERROR_JOB", job );
                            temporaryResults.remove( tempErrors[j] );
                            executeAnalyse = true;
                        }
                    }
                }
                // Einzelne Analysepr�fprozedur(en) durchf�hren
                if (executeAnalyse) {
                    if (DEBUG) writeDebug("analyse PP "+analysePruefprozeduren[i].getName()+", executing" );
                    // Pr�fprozedur durchf�hren
                    analysePruefprozeduren[i].setPr�flingLaufzeitUmgebung(getPr�flingLaufzeitUmgebung());
                    analysePruefprozeduren[i].execute(info);
                    // Pr�fprozedur Ergebnis dokumentieren
                    if( analysePruefprozeduren[i].getExecStatus() != STATUS_EXECUTION_OK ) {
                        if (DEBUG) writeDebug("analyse PP "+analysePruefprozeduren[i].getName()+", niO" );
                        status = STATUS_EXECUTION_ERROR;
                        result = new Ergebnis( "FS", getPr�fling().getName()+"."+analysePruefprozeduren[i].getName(), "", "", "", "Status", PB.getString("ausfuehrungsstatusNIO") , "", "", "", "", "", "", "", "", Ergebnis.FT_NIO );
                        ergListe.add( result );
                    } else {
                        if (DEBUG) writeDebug("analyse PP "+analysePruefprozeduren[i].getName()+", iO");
                        result = new Ergebnis( "FS", getPr�fling().getName()+"."+analysePruefprozeduren[i].getName(), "", "", "", "Status", "" , "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
                        ergListe.add( result );
                    }
                }
            }
            // Nach der Analyse verbleibende Fehler zum Fehlerliste hinzuf�gen (und Status setzen)
            Enumeration myEnum = temporaryResults.elements();
            while ( myEnum.hasMoreElements() ) {
                result = (Ergebnis) myEnum.nextElement();
                if (result.getFehlerTyp() == Ergebnis.FT_NIO) {
                    status = STATUS_EXECUTION_ERROR;
                }
                ergListe.add( result );
            }
            
            if (DEBUG) writeDebug("TB errorPresent <" + errorPresent + ">" );
            if (DEBUG) writeDebug("TB status <" + status + ">" );
            if (DEBUG) writeDebug("TB clear PP<" + clear + ">" );

            
            
            
            // Nach der Analyse ist ggf. der Fehlerspeicher zu l�schen
            if (errorPresent && (status == STATUS_EXECUTION_OK) && (clear != null) ) {
                clearPruefprozedur = getPr�fling().getPr�fprozedur(clear);
                clearPruefprozedur.setPr�flingLaufzeitUmgebung(getPr�flingLaufzeitUmgebung());
                clearPruefprozedur.execute(info);
                status = clearPruefprozedur.getExecStatus();
                
                // APDM Eintrag
                result = new Ergebnis( "FS_CLEAR", getPr�fling().getName()+"."+clearPruefprozedur.getName(), "", "", "", "Status", "" , "", "", "", "", "", "", "", "", Ergebnis.FT_IO );
                ergListe.add( result );
                
                if (DEBUG) writeDebug("TB status after clear <" + status + ">" );
            }
            
            
            
        } catch (PPExecutionException e) {
            status = STATUS_EXECUTION_ERROR;
        } catch (Exception e) {
            e.printStackTrace();
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception ("+e.getMessage()+")", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
            e.printStackTrace();
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable ("+e.getMessage()+")", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
        setPPStatus( info, status, ergListe );
        
    }
    
    private void writeDebug(String info) {
        System.out.println("DiagLeseFS: "+getPr�fling().getName()+"."+getName()+" "+info);
    }
    
    /**
     * Hilfsmethode f�r Argument IGNORE: L�st den �bergebenen String auf
     * @param in der Argument IGNORE
     * @return vollst�ndige String (zB "1-3" wird als "1;2;3" zur�ckgeliefert)
     */
    private String extractErrorNumbers(String in) {
        String sub, subSub;
        int[] posArray;
        int counter = 1;
        //int Pos = 0;
        int min,max;
        int i,j;
        
        
        for (i = 0; i < in.length(); i++) {
            if (in.charAt(i) == ';') counter ++;
        }
        
        posArray = new int[counter+1];
        j = 0;
        posArray[j++] = -1;
        for (i = 0; i < in.length(); i++) {
            if (in.charAt(i) == ';') posArray[j++] = i;
        }
        posArray[j++] = in.length();
        
        StringBuffer temp = new StringBuffer();
        temp.append(";");
        for (i = 0; i < counter; i++) {
            sub = (in.substring(posArray[i]+1,posArray[i+1])).trim();
            if (sub.compareTo("") != 0) {
                j = sub.indexOf("-");
                if ((j < 1) || (j == (sub.length()-1))) {
                    temp.append(sub);
                    temp.append(";");
                } else {
                    subSub = sub.substring(0,j);
                    min = Integer.parseInt( subSub );
                    subSub = sub.substring(j+1,sub.length());
                    max = Integer.parseInt( subSub );
                    for (j=min;j<=max;j++) {
                        temp.append(j);
                        temp.append(";");
                    }
                }
            }
        }
        return temp.toString();
    }
    
}


package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.framework.udnext.UDNHandle;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.ImageLocation;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.UDNButtonConfiguration;
import com.bmw.cascade.pruefstand.framework.udnext.UserDialogNextRuntimeEnvironment;
import com.bmw.cascade.pruefstand.framework.udnext.UDNModel.UDNState;

/**
 * Implementierung der Pr�fprozedur, die eine Meldung an den Werker ausgibt, die dieser zu
 * quittieren hat. �ber das Argument STYLE kann gesteuert werden, ob eine Statusmeldung (STYLE = 1)
 * oder eine Alarmmeldung (STYLE=2) ausgegeben wird, ansonsten wird die Standardmeldung verwendet.
 * 
 * @author Winkler
 * @version Implementierung
 * @version 3_0_T Unterst�tzung @-Parameter implementiert f�r AWT
 */
public class WerkerQuittungUDN_1_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * Default-Konstruktor, nur fuer die Deserialisierung.
	 */
	public WerkerQuittungUDN_1_0_F_Pruefprozedur() {
	}

	/**
	 * Erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling Klasse des zugeh�rigen Pr�flings.
	 * @param pruefprozName Name der Pr�fprozedur.
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit.
	 */
	public WerkerQuittungUDN_1_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * Initialsiert die Argumente.
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * Liefert die optionalen Argumente.
	 * 
	 * @return String-Array mit den Arguementen.
	 */
	public String[] getOptionalArgs() {
		String[] args = { "TITEL", "IMAGE", "IMAGE_LOCATION" };
		return args;
	}

	/**
	 * Liefert die zwingend erforderlichen Argumente.
	 * 
	 * @return String-Array mit den Arguementen.
	 */
	public String[] getRequiredArgs() {
		String[] args = { "AWT", "STYLE" };
		return args;
	}

	/**
	 * Pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 * 
	 * @return <code>True</code> bei positiver Pr�fung, andernfalls <code>false</code>.
	 */
	public boolean checkArgs() {
		try {
			if( super.checkArgs() ) {
				Long.parseLong( getArg( getRequiredArgs()[1] ) ); //Parsen m�glich?
				return true;
			} else
				return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * F�hrt die Pr�fprozedur aus.
	 * 
	 * @param info Information zur Ausf�hrung.
	 */
	public void execute( ExecutionInfo info ) {
		// Immer notwendig.
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// Spezifische Variablen.
		String title;
		String awt;
		String image;
		String imageLocation;
		int style;
		UserDialogNextRuntimeEnvironment dlu = null;
		UDNHandle handle = null;

		try {
			// Parameter holen.
			try {
				if( !checkArgs() )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// Meldungstext.
				awt = getArg( getRequiredArgs()[0] );
				if( awt.indexOf( '@' ) != -1 ) {
					String[] awts = extractValues( awt );
					awt = "";
					for( int i = 0; i < awts.length; i++ )
						awt = awt + awts[i] + " ";
				} else
					awt = PB.getString( awt );
				
				// Style.
				if( getArg( getRequiredArgs()[1] ) != null )
					style = Integer.parseInt( getArg( getRequiredArgs()[1] ) );
				else
					style = -1;

				// Titel.
				title = getArg( getOptionalArgs()[0] );
				if( title == null )
					title = PB.getString( "meldung" );

				// Image. Dies ist das Bild, welches der Dialog anzeigen soll (falls gew�nscht).
				image = getArg( getOptionalArgs()[1] ); //kann auch null sein, dann kein Bild

				// Position des Bildes in Bezug zum Meldungstext.
				imageLocation = getArg( getOptionalArgs()[2] ); //kann auch null sein, dann existiert die Angabe nicht und der Default wird angenommen

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			// Dialog anzeigen.
			try {
				dlu = getPr�flingLaufzeitUmgebung().getUserDialogNext();
				handle = dlu.allocateUserDialogNext();

				if( handle != null ) {
					if( image != null )
						dlu.displayMessage( handle, toUDNState( style ), image, toImageLocation( imageLocation ), title, awt, 0, UDNButtonConfiguration.OK_BUTTON );
					else
						dlu.displayMessage( handle, toUDNState( style ), title, awt, 0, UDNButtonConfiguration.OK_BUTTON );

					dlu.releaseUserDialogNext( handle ); //!!!																			
				} else 
					throw new DeviceNotAvailableException();

			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			result = new Ergebnis( title, "Userdialog", "", "", "", "", "", "", "", "0", "", "", awt, "", "", Ergebnis.FT_IO );
			ergListe.add( result );

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		//Status setzen
		setPPStatus( info, status, ergListe );
	}

	/**
	 * Konvertiert den STYLE-Parameter der Pr�fprozedur in den 
	 * korrespondierenden UDN-State.
	 * 
	 * @param style STYLE-Angabe der Pr�fprozedur.
	 * @return Korrespondierender UDN-State.
	 */
	private UDNState toUDNState( int style ) {
		UDNState state;
		switch( style ) {
			case 1:
				state = UDNState.STATUS;
				break;
			case 2:
				state = UDNState.ALERT;
				break;
			case 3:
				state = UDNState.WARNING;
				break;
			case 4:
				state = UDNState.NONE;
				break;
			default:
				state = UDNState.MESSAGE;
		}
		return state;
	}

	/**
	 * Konvertiert den IMAGE_LOCATION-Parameter der Pr�fprozedur in die 
	 * korrespondierende UDN-ImageLocation Angabe. Bei �bergabe von 
	 * <code>null</code> wird ImageLocation.IMAGE_LEFT als Default zur�ck 
	 * geliefert.
	 *  
	 * @param code IMAGE_LOCATION-Parameter der Pr�fprozedur.  
	 * @return Korrespondierendes UDN-ImageLocation.
	 */
	private ImageLocation toImageLocation( String locationCode ) {
		ImageLocation location;

		if( "R".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_RIGHT;
		else if( "L".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_LEFT;
		else if( "T".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_TOP;
		else if( "B".equalsIgnoreCase( locationCode ) )
			location = ImageLocation.IMAGE_BOTTOM;
		else
			location = ImageLocation.IMAGE_LEFT;

		return location;
	}

}

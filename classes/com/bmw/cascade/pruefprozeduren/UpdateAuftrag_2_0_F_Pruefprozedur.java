/*
 * UpdateAuftrag_2_0_F_Pruefprozedur.java
 *
 * Created on 18.03.02
 */
package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.auftrag.*;

/**
 * Implementierung der Pr�fprozedur, die eine Update vom Auftrag durchf�hrt. Muss implements "UPDATE" Pr�fling
 * definiert werden, und wird alle anderen Pr�fprozeduren im "UPDATE" Pr�fling durchf�hren.
 * @author Crichton / TI-430
 * @version  NC  04.04.02  V0_0_1_FA  Ersterstellung
 * @version  NC  05.04.02  V0_0_1_FA  Kommentar hinzu, PP-Schleife Bug korrigiert, DEBUG hinzu
 * @version  SS  20.09.02  V0_0_2_FA  Ausblendung des Motordaten-Updates bei UPDATE_ALLE
 */
public class UpdateAuftrag_2_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {
    
    	static final long serialVersionUID = 1L;
  /**
   * DefaultKonstruktor, nur fuer die Deserialisierung
   */
  public UpdateAuftrag_2_0_F_Pruefprozedur() {}

  /**
     * erzeugt eine neue Pruefprozedur.
     * @param pruefling Klasse des zugeh. Pr�flings
     * @param pruefprozName Name der Pr�fprozedur
     * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
     */
    public UpdateAuftrag_2_0_F_Pruefprozedur(Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted) {
        super( pruefling, pruefprozName, hasToBeExecuted );
        attributeInit();
    }
    
    /**
     * initialsiert die Argumente
     */
    protected void attributeInit() {
        super.attributeInit();
    }
    
    /**
     * liefert die optionalen Argumente
     */
    public String[] getOptionalArgs() {
        String[] args = new String[0];
        return args;
    }
    
    /**
     * liefert die zwingend erforderlichen Argumente
     */
    public String[] getRequiredArgs() {
        String[] args = new String[0];
        return args;
    }
    
    /**
     * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert
     */
    public boolean checkArgs() {
        return super.checkArgs();
    }
    
    
    /**
     * f�hrt die Pr�fprozedur aus
     * @param info Information zur Ausf�hrung
     */
    public void execute( ExecutionInfo info ) {
        // immer notwendig
        Ergebnis result;
        Vector ergListe = new Vector();
        int status = STATUS_EXECUTION_OK;
        
        // spezifische Variablen
        boolean DEBUG = false;
        boolean error = false;
        Pruefprozedur[] updatePruefprozeduren;
        
        /**
         * Parameter holen und pr�fen
         */
        try {
            updatePruefprozeduren = this.getPr�fling().getPr�fprozeduren();
            if (DEBUG) System.out.println("Update Auftrag...");
            
            for (int i=0; i<updatePruefprozeduren.length; i++) {
                if (DEBUG) System.out.println("Update Auftrag... "+updatePruefprozeduren[i].getName());
                if ( (updatePruefprozeduren[i].getName().equalsIgnoreCase("ALLE") == false) &&
                     (updatePruefprozeduren[i].getName().startsWith("MOTOR_DATEN") == false) ){
                    System.out.println("Prozedurname: " + updatePruefprozeduren[i].getName());
                    updatePruefprozeduren[i].setPr�flingLaufzeitUmgebung(getPr�flingLaufzeitUmgebung());
                    updatePruefprozeduren[i].execute(info);
                    status = updatePruefprozeduren[i].getExecStatus();
                    if( status != STATUS_EXECUTION_OK ) {
                        if (DEBUG) System.out.println("Update Auftrag... NIO "+updatePruefprozeduren[i].getName());
                        result = new Ergebnis( "Status", "Pruefprozedur", "", "", "", updatePruefprozeduren[i].getPr�fling().getName()+"."+updatePruefprozeduren[i].getName(), ""+status, ""+STATUS_EXECUTION_OK, "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
                        error = true;
                    } else {
                        if (DEBUG) System.out.println("Update Auftrag... IO "+updatePruefprozeduren[i].getName());
                        result = new Ergebnis( "Status", "Pruefprozedur", "", "", "", updatePruefprozeduren[i].getPr�fling().getName()+"."+updatePruefprozeduren[i].getName(), ""+status, ""+STATUS_EXECUTION_OK, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
                    }
                    ergListe.add(result);
                }
            }
        
            // Sollte nur eine vom viele PPs ein Fehler haben, ist es notwnedig der Status richtig zu setzen...
            if (error == true) status = STATUS_EXECUTION_ERROR;
            
        } catch (Exception e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        } catch (Throwable e) {
            result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
            ergListe.add(result);
            status = STATUS_EXECUTION_ERROR;
        }
        
        //Status setzen
        setPPStatus( info, status, ergListe );
    }
    
}


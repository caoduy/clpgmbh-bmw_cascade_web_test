package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;

/** Implementierung der Pruefprozedur, die Ergebnisse von einem vorherigen Pr�fschritt mittels dem @-Konstrukt unterst�tzt.<BR>
 * <BR>
 * Created on 08.09.16<BR>
 * <BR>
 * @author BMW TI-545 K�ser<BR>
 * @version V1_0_F  08.09.2016  MK  Implementierung<BR>                                        
*/
public class AtKonstrukt_1_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public AtKonstrukt_1_0_F_Pruefprozedur() {
	}

	/**
	   * erzeugt eine neue Pruefprozedur.
	   * @param pruefling Klasse des zugeh. Prueflings
	   * @param pruefprozName Name der Pruefprozedur
	   * @param hasToBeExecuted Ausfuehrbedingung f�r Fehlerfreiheit
	   */
	public AtKonstrukt_1_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "Prueflingname", "Defaultvalue", "NIOifNoValue" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "Result-ID", "Pruefschritt" };
		return args;
	}

	/**
	 * prueft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 */
	public boolean checkArgs() {
		try {
			return super.checkArgs();
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * fuehrt die Pruefprozedur aus
	 * @param info Information zur Ausfuehrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String resultId = null, pruefschritt = null, prueflingname = null;
		String[] values, defaultvalue = null;
		Boolean nioIfNoValue = false;

		try {
			//Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				//Zwingenden Parameter Result-ID "checken"
				if( getArg( getRequiredArgs()[0] ) != null ) {
					resultId = getArg( getRequiredArgs()[0] ).trim();
				}

				//Zwingenden Parameter Pr�fschritt "checken"
				if( getArg( getRequiredArgs()[1] ) != null ) {
					pruefschritt = getArg( getRequiredArgs()[1] ).trim();
				}

				//Optionalen Parameter Prueflingname "checken"
				if( getArg( getOptionalArgs()[0] ) != null ) {
					prueflingname = getArg( getOptionalArgs()[0] ).trim();
				}

				//Optionalen Parameter Defaultvalue "checken"
				if( getArg( getOptionalArgs()[1] ) != null ) {
					defaultvalue = extractValues(getArg( getOptionalArgs()[1] ).trim());
				}

				//Optionalen Parameter IOifError "checken"
				if( getArg( getOptionalArgs()[2] ) != null ) {
					if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "TRUE" ) )
						nioIfNoValue = true;
				}

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			try {
				if( prueflingname != null ) {
					values = extractValues( resultId + "@" + prueflingname + "." + pruefschritt );
					result = new Ergebnis( "Value", "AtKonstrukt", "", "", "", "Value", values[0], values[0], "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				} else {
					values = extractValues( resultId + "@" + pruefschritt );
					result = new Ergebnis( "Value", "AtKonstrukt", "", "", "", "Value", values[0], values[0], "", "0", "", "", "", "", "", Ergebnis.FT_IO );
					ergListe.add( result );
				}
			} catch( PPExecutionException e ) {
				if( defaultvalue[0] != null ) {

					if( nioIfNoValue == false ) {
						result = new Ergebnis( "Value", "AtKonstrukt", "", "", "", "Defaultvalue", defaultvalue[0], defaultvalue[0], "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					} else {
						result = new Ergebnis( "Value", "AtKonstrukt", "", "", "", "Devaultvalue", defaultvalue[0], defaultvalue[0], "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
						ergListe.add( result );
						status = STATUS_EXECUTION_ERROR;
					}
				} else {
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
					ergListe.add( result );
					status = STATUS_EXECUTION_ERROR;
				}
			} catch( Exception e ) {
				result = new Ergebnis( "Status", "System", "", "", e.getMessage(), "Exception", "", "0", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
				throw new PPExecutionException();
			}
		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}
		setPPStatus( info, status, ergListe );
	}
}

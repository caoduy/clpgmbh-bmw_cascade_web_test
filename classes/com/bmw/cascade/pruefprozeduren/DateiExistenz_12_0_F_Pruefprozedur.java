package com.bmw.cascade.pruefprozeduren;

import java.io.*;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;

/** Implementierung der Pruefprozedur, die die Existenz einer Datei ueberprueft<BR>
 * <BR>
 * Created on 19.01.04<BR>
 * <BR>
 * bei Aenderung Hr. Buboltz TI-545 Tel. 35789 informieren!<BR>
 * <BR>
 * @author BMW TI-430 Buboltz<BR>
 * @version V0_0_1  19.01.2004  TB  Implementierung<BR>
 *          V0_0_2  24.04.2006  FM  Erweiterung um die optionalen Parameter
 *                                  DIR und EXTENSION<BR>
 *          V0_0_3  24.04.2006  FM  Bug-Fixing. 
 *                                  (Dank an Thomas Buboltz f�r die R�ckmeldung)
 *          V0_0_4  02.05.2006  TB  Quick-Fix bei den optionalen Par hole �ber Methode getOptionalArgs<BR>   
 *          V5_0_T  03.06.2008  CS  APDM Ergebnis Format Anpassung<BR>  
 *          V6_0_F  07.10.2008  CS  Freigabe 5_0_T<BR>    
 *          V7_0_T  28.04.2016  FS  Timeout hinzugef�gt.<BR>
 *          V8_0_F  10.05.2016  FS  Default Timeout auf 10 angepasst, produktive Freigabe.<BR>
 *          V9_0_T  10.05.2016  FS  Bugfix: Fehlender shutdown des executor Services verhinderte PP Ende.<BR>   
 *          V10_0_F 11.05.2016	FS	Produktive Freigabe Version V_9_0_T.<BR>  
 *          V11_0_T 07.06.2016	MK	Default Timeout auf 5 Sekunden angepasst.<BR>   
 *          V12_0_F 07.06.2016	MK	Produktive Freigabe Version V_11_0_T.<BR>                                   
*/
public class DateiExistenz_12_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;
	static final long DEFAULT_TIMEOUT = 5;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public DateiExistenz_12_0_F_Pruefprozedur() {
	}

	/**
	   * erzeugt eine neue Pruefprozedur.
	   * @param pruefling Klasse des zugeh. Prueflings
	   * @param pruefprozName Name der Pruefprozedur
	   * @param hasToBeExecuted Ausfuehrbedingung f�r Fehlerfreiheit
	   */
	public DateiExistenz_12_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "DIR", "EXTENSION", "INVERT", "TIMEOUT" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "DATEI" };
		return args;
	}

	/**
	 * prueft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 */
	public boolean checkArgs() {
		try {
			return super.checkArgs();
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * fuehrt die Pruefprozedur aus
	 * @param info Information zur Ausfuehrung
	 */
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector ergListe = new Vector();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String datei = "";
		String dir = "";
		String extension = "";
		long timeout = DEFAULT_TIMEOUT;
		boolean invert = false;

		//APDM Ergebnis Format->nur in param3 d�rfen variable Werte stehen
		String param3 = "";

		try {
			//Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				//Zwingenden Parameter DATEI "checken"
				if( getArg( getRequiredArgs()[0] ).indexOf( '@' ) != -1 )
					datei = getPPResult( getArg( getRequiredArgs()[0] ) ).trim();
				else
					datei = getArg( getRequiredArgs()[0] ).trim();

				//Optionalen Parameter DIR "checken" 
				if( getArg( getOptionalArgs()[0] ) != null ) {
					if( getArg( getOptionalArgs()[0] ).indexOf( '@' ) != -1 )
						dir = getPPResult( getArg( getOptionalArgs()[0] ) ).trim();
					else
						dir = getArg( getOptionalArgs()[0] ).trim();
				}

				//Optionalen Parameter EXTENSION "checken"
				if( getArg( getOptionalArgs()[1] ) != null ) {
					if( getArg( getOptionalArgs()[1] ).indexOf( '@' ) != -1 )
						extension = getPPResult( getArg( getOptionalArgs()[1] ) ).trim();
					else
						extension = getArg( getOptionalArgs()[1] ).trim();
				}

				//Optionalen Parameter TIMEOUT "checken"
				if( getArg( getOptionalArgs()[3] ) != null ) {
					timeout = Long.parseLong( getArg( getOptionalArgs()[3] ).trim() );
				}

				//Kompletten Datei-Namen zusammenbauen
				if( extension.length() != 0 ) {
					datei = datei + "." + extension;
				}
				if( dir.length() != 0 ) {
					datei = dir + "\\" + datei;
				}
				datei = datei.trim();

				//soll auf Nichtexistenz der Datei geprueft werden?
				if( getArg( getOptionalArgs()[2] ) != null ) {
					if( getArg( getOptionalArgs()[2] ).equalsIgnoreCase( "TRUE" ) )
						invert = true;
				}
			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//Dateiexistenz pruefen
			File f = new File( datei );
			try {
				// Pr�fe Dateiexistenz in Executorthread um einen Timeout zu erm�glichen
				ExecutorService exSrv = Executors.newSingleThreadExecutor();
				ExecutorCompletionService<Boolean> completionService = new ExecutorCompletionService<Boolean>( exSrv );
				completionService.submit( new CheckFileExists( f ) );
				Future<Boolean> fileExists = completionService.poll( timeout, TimeUnit.SECONDS );
				if( fileExists != null && fileExists.get() == true ) {
					if( invert ) {
						param3 = datei + "; " + "INVERT" + "; " + invert;
						result = new Ergebnis( "Status", "System", "", "", param3, "DATEI_EXISTENZ", "TRUE", "FALSE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						exSrv.shutdown();
						throw new PPExecutionException();
					} else {
						param3 = datei + "; " + "INVERT" + "; " + invert;
						result = new Ergebnis( "Status", "System", "", "", param3, "DATEI_EXISTENZ", "TRUE", "TRUE", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					}
				} else {
					if( invert ) {
						param3 = datei + "; " + "INVERT" + "; " + invert;
						result = new Ergebnis( "Status", "System", "", "", param3, "DATEI_EXISTENZ", "FALSE", "FALSE", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						ergListe.add( result );
					} else {
						param3 = datei + "; " + "INVERT" + "; " + invert;
						result = new Ergebnis( "Status", "System", "", "", param3, "DATEI_EXISTENZ", "FALSE", "TRUE", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
						ergListe.add( result );
						exSrv.shutdown();
						throw new PPExecutionException();
					}
				}
				exSrv.shutdown();
			} catch( CancellationException e ) {
				param3 = datei + "; " + "INVERT" + "; " + invert;
				result = new Ergebnis( "Status", "System", "", "", param3, "CANCELLATION_EXCEPTION", "", "0", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( InterruptedException e ) {
				param3 = datei + "; " + "INVERT" + "; " + invert;
				result = new Ergebnis( "Status", "System", "", "", param3, "INTERRUPTED_EXCEPTION", "", "0", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} catch( ExecutionException e ) {
				// An execution exception of CheckFileExists can only be due to a security exception in file.exists()
				param3 = datei + "; " + "INVERT" + "; " + invert;
				result = new Ergebnis( "Status", "System", "", "", param3, "SECURITY_EXCEPTION", "", "0", "", "0", "", "", "", PB.getString( "ausfuehrungsstatusNIO" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			}
		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			e.printStackTrace();
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		setPPStatus( info, status, ergListe );
	}

	/**
	 *  Die Klasse CheckFileExists erm�glicht ein threadbasiertes �berpr�fen auf Dateiexistenz.
	 * 
	 * @author Sch�nert Fabian, TI-545 BMW AG
	 *
	 */
	class CheckFileExists implements Callable {

		private File file;

		public CheckFileExists( File file ) {
			this.file = file;
		}

		@Override
		public Boolean call() throws Exception {
			return file.exists();
		}
	}

}

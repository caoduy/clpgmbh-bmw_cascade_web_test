/*
 * SDiagCBSResultTokenizer Pruefprozedur.java
 *
 * Created on 28.07.13
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.CascadeProperties;
import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.devices.ediabas.EdiabasProxyThread;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.variables.VariableModes;
import com.bmw.cascade.pruefstand.variables.VariablesException;

/**
 * Spezifische PP f�r die CBS (Condition Based Service) Detailanalyse. <BR>
 * Problem die richtigen CBS Ergebnisse sind in unterschiedlichen Results und es
 * muss vor einem Auslesen der richtigen Results, <BR>
 * zuerst der entsprechende EDIABAS Satz �ber ein Suchtoken bestehend aus
 * Resultname und Resultvalue ermittelt werden. <BR>
 * Wird das Suchtoken (Resultname + Resultvalue) gefunden werden die zus�tzlich
 * angegebenen Results in APDM dokumentiert und die PP ist i.O. <BR>
 * Ist das Suchtoken nicht vorhanden wird die PP NIO und die zus�tzlich
 * angegegben Results werden als nicht vorhanden NIO dokumentiert. <BR>
 * <BR>
 * Anforder: Wolfram Mark, 24.04.2014, LOP 1762 <BR>
 * 
 * @author Buboltz <BR>
 * @version 1_0_F 04.11.2014 TB Erstimplementierung <BR>
 * @version 2_0_T 03.03.2016 MK die zus�tzlichen Results k�nnen jetzt auch mit Result1-n ausgewertet und an APDM �bertragen werden, wie DiagTolerance <BR>
 * @version 3_0_F 17.03.2016 MK F-Version <BR>
 * @version 4_0_F 15.04.2016 MK Bug Fix der letzte Datensatz wurde nicht bewertet<BR>
 */
public class SDiagCBSResultTokenizer_4_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	boolean bDebug = false; // gibt an, ob Debug-Ausgaben erfolgen sollen

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public SDiagCBSResultTokenizer_4_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * 
	 * @param pruefling
	 *            Klasse des zugeh. Pr�flings
	 * @param pruefprozName
	 *            Name der Pr�fprozedur
	 * @param hasToBeExecuted
	 *            Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public SDiagCBSResultTokenizer_4_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	public String[] getOptionalArgs() {
		String[] args = { "CBS_RESULTS_LIST", "RESULT[1..N]", "MIN[1..N]", "MAX[1..N]", "NO_MIN_MAX[1..N]", "DEBUG" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	public String[] getRequiredArgs() {
		String[] args = { "SGBD", "JOB", "SEARCH_TOKEN_CBS_RESULTNAME", "SEARCH_TOKEN_CBS_RESULVALUE" };
		return args;
	}

	/**
	 * Pr�ft die Argumente auf Existenz und Wert.
	 *
	 * @return <code>False</code>, wenn die Pr�fung einen Fehler festgestellt
	 *         hat, ansonsten <code>true</code>.
	 */
	public boolean checkArgs() {

		int i, j;
		int maxIndex = 0;
		boolean bRequiredArg, bOptionalArg;
		String sResultLine;
		String[] saParamDetails;
		String[] mins, maxs;
		String[] optionalArgs;
		String givenkey;
		String temp;

		try {

			// Sind alle requiredArgs gesetzt
			String[] requiredArgs = getRequiredArgs();
			for( i = 0; i < requiredArgs.length; i++ ) {
				if( getArg( requiredArgs[i] ) == null )
					return false;
				if( getArg( requiredArgs[i] ).equals( "" ) == true )
					return false;
			}

			// optionalArgs = getOptionalArgs();

			// Ermittlung des h�chstwertigen Result index
			Enumeration enu = getArgs().keys();

			while( enu.hasMoreElements() ) {
				givenkey = (String) enu.nextElement();
				if( givenkey.startsWith( "RESULT" ) == true ) {
					temp = givenkey.substring( 6 );
				} else if( givenkey.startsWith( "NO_MIN_MAX" ) == true ) {
					temp = givenkey.substring( 10 );
				} else if( (givenkey.startsWith( "MIN" )) || (givenkey.startsWith( "MIN" )) )
					temp = givenkey.substring( 3 );
				else {
					temp = "";
				}
				if( temp != "" ) {
					try {
						j = Integer.parseInt( temp );
						if( j < 1 )
							return false;
						if( j > maxIndex )
							maxIndex = j;
					} catch( NumberFormatException e ) {
						return false;
					}
				}
			}

			if( (getArg( getOptionalArgs()[0] ) != null) && maxIndex > 0 ) {
				return false;
			}

			// Sind die Result- und Min-Argumente plausibel, Max-Argumente sind
			// optional und werden daher gefordert.
			// Existieren jedoch Max-Werte, wird die Anzahl sowie die
			// Konvertierung von Minwerten und Maxwerten zu Zahlen gepr�ft,
			// sofern es sich um kein Result einer anderen PP handelt
			//
			// Funktion wurde erweitert um NO_MIN_MAX
			// Wenn NO_MIN_MAX vorhanden, dann darf es kein MIN oder MAX geben
			for( i = 1; i <= maxIndex; i++ ) {
				sResultLine = getArg( "RESULT" + i );
				if( sResultLine == null )
					return false;
				// Wenn ein ';' im Resultnamen vorkommt (evtl. nach erstem ;
				// Ergebnissatz)
				if( sResultLine.indexOf( ';' ) != -1 ) {
					saParamDetails = sResultLine.split( ";" );
					if( saParamDetails.length == 2 ) {
						temp = saParamDetails[1];
						try {
							j = Integer.parseInt( temp );
							if( j < 0 )
								return false;
						} catch( NumberFormatException e ) {
							return false;
						}
					} else {
						return false;
					}
				}
				if( (i > 1) && (sResultLine.equals( getArg( "RESULT1" ) ) == true) )
					return false;
				temp = getArg( "MIN" + i );
				if( temp == null && getArg( "NO_MIN_MAX" + i ) == null )
					return false; // bei MIN, darf es kein NO_MIN_MAX geben
				if( getArg( "MAX" + i ) != null && getArg( "NO_MIN_MAX" + i ) != null )
					return false; // bei MAX, darf es kein NO_MIN_MAX geben
				if( getArg( "MAX" + i ) != null && getArg( "NO_MIN_MAX" + i ) == null ) {
					mins = splitArg( getArg( "MIN" + i ) );
					maxs = splitArg( getArg( "MAX" + i ) );
					if( mins.length != maxs.length )
						return false;
					for( j = 0; j < mins.length; j++ ) {
						try {
							if( mins[j].indexOf( '@' ) == -1 )
								parseDouble( mins[j] );
							else
								parseDouble( getPPResult( mins[j] ) );
							if( maxs[j].indexOf( '@' ) == -1 )
								parseDouble( maxs[j] );
							else
								parseDouble( getPPResult( maxs[j] ) );
						} catch( NumberFormatException e ) {
							return false;
						} catch( InformationNotAvailableException e ) {
							return false;
						}
					}
				}
			}

			return true;

		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}

	}

	/**
	 * Ermittelt die Sprache
	 * 
	 * @return true: Deutsch, false: Englisch
	 */
	private static boolean checkDE() {
		try {
			if( CascadeProperties.getLanguage().equalsIgnoreCase( "DE" ) == true )
				return true;
			else
				return false;
		} catch( Exception e ) {
			return false; // default english
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * 
	 * @param info
	 *            Information zur Ausf�hrung
	 */
	public void execute( ExecutionInfo info ) {

		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;
		int resAnzahl = 0;
		int[] iaResultSet = null;
		int i = 0, j, k;

		double d;

		boolean invert = false;
		boolean ok;
		boolean[] bNoMinMax = null;

		String sCurrentResultLine, sCurrentResultName, sCurrentResultSet;
		String sgbd = null, job = null, jobres = "", temp1 = "", temp2 = "", strTokenCBSResult = null, strTokenCBSResultvalue = null;
		String[] saCurrentResultDetails;
		String[] mins, maxs;
		String[] results = null;

		List<String> strCBSResultsList = null;
		// EDIABAS
		EdiabasProxyThread myEdiabas = null;
		final boolean DE = checkDE(); // Systemsprache DE wenn true

		/***********************************************
		 * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
		 * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		 ***********************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		try {
			// Parameter holen
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// DEBUG abfragen
				if( getArg( "DEBUG" ) != null ) {
					if( getArg( "DEBUG" ).equalsIgnoreCase( "TRUE" ) )
						bDebug = true;
					else if( getArg( "DEBUG" ).equalsIgnoreCase( "FALSE" ) )
						bDebug = false;
				} else {
					// noch Pr�fstandvariable pr�fen, ob Debug
					try {
						Object pr_var_debug;

						pr_var_debug = getPr�flingLaufzeitUmgebung().getPruefstandVariable( VariableModes.PS_CONFIG, "DEBUG" );
						if( pr_var_debug != null ) {
							if( pr_var_debug instanceof String ) {
								if( "TRUE".equalsIgnoreCase( (String) pr_var_debug ) ) {
									bDebug = true;
								} else {
									bDebug = false;
								}
							} else if( pr_var_debug instanceof Boolean ) {
								if( ((Boolean) pr_var_debug).booleanValue() ) {
									bDebug = true;
								} else {
									bDebug = false;
								}
							}
						} else {
							bDebug = false;
						}

					} catch( VariablesException e ) {
						bDebug = false;
					}
				}

				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				// SGBD
				sgbd = extractValues( getArg( "SGBD" ) )[0];

				// JOB
				job = extractValues( getArg( "JOB" ) )[0];

				// SEARCH_TOKEN_CBS_RESULTNAME
				strTokenCBSResult = extractValues( getArg( "SEARCH_TOKEN_CBS_RESULTNAME" ) )[0];

				// SEARCH_TOKEN_CBS_RESULVALUE
				strTokenCBSResultvalue = extractValues( getArg( "SEARCH_TOKEN_CBS_RESULVALUE" ) )[0];

				// CBS_RESULTS_LIST
				if( getArg( "CBS_RESULTS_LIST" ) != null ) {
					strCBSResultsList = Arrays.asList( extractValues( getArg( "CBS_RESULTS_LIST" ) ) );
				}

				// debug
				if( bDebug ) {
					System.out.println( "PP SDiagCBSResultTokenizer: Parameter processing Check is done. (SGBD: <" + sgbd + ">, JOB: <" + job + ">, SEARCH_TOKEN_CBS_RESULTNAME: <" + strTokenCBSResult + ">, SEARCH_TOKEN_CBS_RESULVALUE: <" + strTokenCBSResultvalue + "> )" );
					if( strCBSResultsList != null ) {
						System.out.println( "PP SDiagCBSResultTokenizer: Parameter processing Check is done. (CBS_RESULTS_LIST: <" + strCBSResultsList + ">)" );
					}
				}

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );

				if( bDebug ) {
					e.printStackTrace();
				}

				throw new PPExecutionException();
			}

			// Der eigentliche Check
			// Ausf�hrung EDIABAS Job

			// EDIABAS besorgen
			myEdiabas = this.getPr�flingLaufzeitUmgebung().getDeviceManager().getEdiabas();

			// EDIABAS Job ausf�hren
			String temp = myEdiabas.executeDiagJob( sgbd, job, "", "" );
			if( temp.equals( "OKAY" ) == false ) {
				result = new Ergebnis( "DiagFehler", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", PB.getString( "diagnosefehler" ), "", Ergebnis.FT_NIO );
				ergListe.add( result );
				throw new PPExecutionException();
			} else {
				result = new Ergebnis( "Status", "EDIABAS", sgbd, job, "", "JOB_STATUS", temp, "OKAY", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
				ergListe.add( result );
			}

			// Alle EDIABAS S�tze durchgehen
			int iSetFound = -1;
			for( i = 1; i < (myEdiabas.getDiagJobSaetze()); i++ ) {
				// Wenn das Token oder das Resultergebnis nicht passen, dann ist
				// keine weitere Verarbeitung notwendig
				try {
					if( !myEdiabas.getDiagResultValue( i, strTokenCBSResult ).equals( strTokenCBSResultvalue ) ) {
						continue;
					}
				} catch( Exception e ) { // ApiCallFailedException,
											// EdiabasResultNotFoundException
											// allgemein abfangen
											// Debug Infos
					if( bDebug ) {
						e.printStackTrace();
					}

					continue;
				}

				iSetFound = i;

				// Result und Ergebnis wurden in diesem Satz gefunden ->
				// dokumentiere die weiteren Results in APDM
				if( strCBSResultsList != null ) {
					for( String strCBSResult : strCBSResultsList ) {
						try {
							String strCBSResultValue = myEdiabas.getDiagResultValue( i, strCBSResult );
							result = new Ergebnis( strCBSResult, "EDIABAS", sgbd, job, "", strCBSResult, strCBSResultValue, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
							ergListe.add( result );
						} catch( Exception e ) { // ApiCallFailedException,
													// EdiabasResultNotFoundException
													// allgemein abfangen
													// Debug Infos
							if( bDebug ) {
								e.printStackTrace();
							}

							// Nicht vorhandene Results werden einfach
							// �bergangen
							continue;
						}
					}
				} else {

					while( (sCurrentResultLine = getArg( "RESULT" + (resAnzahl + 1) )) != null ) {
						resAnzahl++;
					}
					iaResultSet = new int[resAnzahl];

					int iCurrentResIndex = 0;
					int iCurrentResultSet = 1;
					// jobres = "";
					while( (sCurrentResultLine = getArg( "RESULT" + (iCurrentResIndex + 1) )) != null ) {
						if( sCurrentResultLine.indexOf( ';' ) != -1 ) {
							saCurrentResultDetails = sCurrentResultLine.split( ";" );
							if( saCurrentResultDetails.length == 2 ) {
								sCurrentResultName = saCurrentResultDetails[0];
								sCurrentResultSet = saCurrentResultDetails[1];
								try {
									iCurrentResultSet = Integer.parseInt( sCurrentResultSet );
									iaResultSet[iCurrentResIndex] = iCurrentResultSet;
									if( iCurrentResultSet != 1 ) {
										// bGetAllResults = true;
									}
								} catch( NumberFormatException e ) {
									throw new PPExecutionException( "Invalid set number in argument RESULT" + (iCurrentResIndex + 1) + " = " + sCurrentResultLine );
								}
							} else {
								throw new PPExecutionException( "Invalid number of parameter details in RESULT" + (iCurrentResIndex + 1) + " = " + sCurrentResultLine );
							}
						} else {
							sCurrentResultName = sCurrentResultLine;
							iaResultSet[iCurrentResIndex] = 1;
						}

						jobres = jobres + ";" + extractValues( sCurrentResultName )[0];
						iCurrentResIndex++;
					}
					// NO_MIN_MAX[1..N]
					bNoMinMax = new boolean[resAnzahl];
					if( resAnzahl > 0 ) {
						for( k = 0; k < resAnzahl; k++ ) {
							try {
								bNoMinMax[i] = new Boolean( extractValues( getArg( "NO_MIN_MAX" + (k + 1) ) )[0] ).booleanValue();
							} catch( Exception e ) {
								// nichts, bei Exception ist bNoMinMax[i]=false
							}
						}
					}
					// CHECK MIN / MAX
					if( resAnzahl > 0 ) {
						jobres = jobres.substring( 1 );
						results = splitArg( jobres ); // Links sind in jobres
														// bereits aufgel�st
						if( results.length != resAnzahl )
							throw new PPExecutionException( PB.getString( "parameterexistenz" ) + "1" );
						for( k = 1; k <= resAnzahl; k++ ) {
							if( getArg( "NO_MIN_MAX" + k ) != null )
								continue;
							mins = extractValues( getArg( "MIN" + k ) );
							if( getArg( "MAX" + k ) != null ) {
								maxs = extractValues( getArg( "MAX" + k ) );
								for( j = 0; j < mins.length; j++ ) {
									try {
										d = parseDouble( mins[j] );
										d = parseDouble( maxs[j] );
									} catch( NumberFormatException e ) {
										throw new PPExecutionException( PB.getString( "parameterexistenz" ) + "2" );
									}
								}
							}
						}
					} else {
						jobres = "JOB_STATUS";
					}
					// Analyse
					for( k = 1; k <= resAnzahl; k++ ) {
						// Ergebnis abholen
						// temp = myEdiabas.getDiagResultValue(iaResultSet[i -
						// 1], results[i - 1]);
						temp = myEdiabas.getDiagResultValue( iSetFound, results[k - 1] );
						if( getArg( "MIN" + k ) != null && !bNoMinMax[k - 1] ) {
							mins = extractValues( getArg( "MIN" + k ) );
							temp1 = mins[0];
							for( j = 1; j < mins.length; j++ ) {
								temp1 = temp1 + ";" + mins[j];
							}
							if( getArg( "MAX" + k ) != null ) {
								maxs = extractValues( getArg( "MAX" + k ) );
								temp2 = maxs[0];
								for( j = 1; j < maxs.length; j++ ) {
									temp2 = temp2 + ";" + maxs[j];
								}
							} else {
								maxs = null;
								temp2 = "";
							}
							if( maxs == null ) {
								// Stringanalyse
								ok = false;
								for( j = 0; j < mins.length; j++ ) {
									if( mins[j].equalsIgnoreCase( temp ) == true )
										ok = true;
								}

								if( (ok == true) && (invert == false) ) {
									// Toleranz, somit IO
									result = new Ergebnis( results[k - 1], "EDIABAS", sgbd, job, "", results[k - 1], temp, temp1, "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								} else if( (ok == false) && (invert == true) ) {
									// Invertierte Toleranz, somit IO
									result = new Ergebnis( results[k - 1], "EDIABAS", sgbd, job, "", results[k - 1], temp, "!(" + temp1 + ")", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
								} else if( (ok == false) && (invert == false) ) {
									// Toleranz, somit NIO
									result = new Ergebnis( results[k - 1], "EDIABAS", sgbd, job, "", results[k - 1], temp, temp1, "", "0", "", "", "", PB.getString( "toleranzFehler1" ), "", Ergebnis.FT_NIO );
									status = STATUS_EXECUTION_ERROR;
								} else {
									// Invertierte Toleranz mit ok == true,
									// somit NIO
									result = new Ergebnis( results[k - 1], "EDIABAS", sgbd, job, "", results[k - 1], temp, "!(" + temp1 + ")", "", "0", "", "", "", PB.getString( "toleranzFehler2" ), "", Ergebnis.FT_NIO );
									status = STATUS_EXECUTION_ERROR;
								}
							} else {
								// Zahlenanalyse (Interval Min-Max)
								d = parseDouble( temp );
								ok = false;
								for( j = 0; j < mins.length; j++ ) {
									if( (parseDouble( mins[j] ) <= d) && (parseDouble( maxs[j] ) >= d) )
										ok = true;
								}
								if( (ok == true) && (invert == false) ) {
									// Toleranz, somit IO
									result = new Ergebnis( results[k - 1], "EDIABAS", sgbd, job, "", results[k - 1], temp, temp1, temp2, "0", "", "", "", "", "", Ergebnis.FT_IO );
								} else if( (ok == false) && (invert == true) ) {
									// Invertierte Toleranz, somit IO
									result = new Ergebnis( results[k - 1], "EDIABAS", sgbd, job, "", results[k - 1], temp, "!(" + temp1 + ")", "!(" + temp2 + ")", "0", "", "", "", "", "", Ergebnis.FT_IO );
								} else if( (ok == false) && (invert == false) ) {
									// Toleranz, somit NIO
									result = new Ergebnis( results[k - 1], "EDIABAS", sgbd, job, "", results[k - 1], temp, temp1, temp2, "0", "", "", "", PB.getString( "toleranzFehler3" ), "", Ergebnis.FT_NIO );
									status = STATUS_EXECUTION_ERROR;
								} else {
									// Invertierte Toleranz mit ok == true,
									// somit NIO
									result = new Ergebnis( results[k - 1], "EDIABAS", sgbd, job, "", results[k - 1], temp, "!(" + temp1 + ")", "!(" + temp2 + ")", "0", "", "", "", PB.getString( "toleranzFehler4" ), "", Ergebnis.FT_NIO );
									status = STATUS_EXECUTION_ERROR;
								}
							}
						} else { // no MIN

							result = new Ergebnis( results[k - 1], "EDIABAS", sgbd, job, "", results[k - 1], temp, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
						}

						ergListe.add( result );

						// setze Variablen im Grafikkontext (1'ter-Istwert,
						// 2'ter-Minwert, 3'ter-Maxwert)
						try {
							this.setResult( (k - 1) * 3, new Integer( temp.substring( 0, temp.indexOf( "." ) < 0 ? temp.length() : temp.indexOf( "." ) ) ) );
							this.setResult( ((k - 1) * 3 + 1), new Integer( temp1.substring( 0, temp1.indexOf( "." ) < 0 ? temp1.length() : temp1.indexOf( "." ) ) ) );
							this.setResult( ((k - 1) * 3 + 2), new Integer( temp2.substring( 0, temp2.indexOf( "." ) < 0 ? temp2.length() : temp2.indexOf( "." ) ) ) );
						} catch( Exception e ) {
							// Tue nichts! Grafikanzeige wird halt nicht
							// aktualisiert. Logs extrem voll schreiben macht
							// auch keinen Sinn.
						}
					}

				}

			}

			// Bewertung des Gesamtergebnisses
			if( iSetFound == -1 ) {
				status = STATUS_EXECUTION_ERROR;
				result = new Ergebnis( "Result", "EDIABAS", sgbd, job, "", "TOKEN_RESULT", DE ? "Suchtoken und Ergebnis wurden NICHT gefunden!" : " Search token plus result were NOT found!", "", "", "0", "", "", "", "", "", Ergebnis.FT_NIO );
			} else {
				result = new Ergebnis( "Result", "EDIABAS", sgbd, job, "", "TOKEN_RESULT", DE ? "Suchtoken und Ergebnis wurden im Satz:" + iSetFound + " gefunden." : "Search token plus result were found in set:" + iSetFound, "", "", "0", "", "", "", "", "", Ergebnis.FT_IO );
			}
			ergListe.add( result );

		} catch( PPExecutionException e ) {

			status = STATUS_EXECUTION_ERROR;

			// Debug Infos
			if( bDebug ) {
				e.printStackTrace();
			}
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;

			// Debug Infos
			if( bDebug ) {
				e.printStackTrace();
			}
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;

			// Debug Infos
			if( bDebug ) {
				e.printStackTrace();
			}
		}

		// Status setzen
		setPPStatus( info, status, ergListe );
	}

	/**
	 * Pr�ft, ob der �bergebene Wert eine Zahl im hexadezimalen Format ist. Dies
	 * ist immer dann der Fall, wenn die Buchstaben a-f/A-F im Wert vorkommen.
	 * Achtung: Nat�rlich kann eine Zahl nur aus Ziffern bestehen und trotzdem
	 * hex sein. In dem Fall geht die Methode von einer Zahl im dezimalen Format
	 * aus, da eine Unterscheidung zwischen hex und dez nicht m�glich ist
	 * (Unsch�rfe).
	 * 
	 * @param value
	 *            Der zu pr�fende Wert.
	 * @return <code>True</code>, wenn die Zahl hexadezimal ist, andernfalls
	 *         <code>false</code>. Letztes ist auch der Fall, wenn es sich um
	 *         reinen Text handelt.
	 */
	private boolean isHex( String value ) {
		try {
			Double.parseDouble( value ); // pr�fen, ob wir als Double parsen
											// k�nnen
			return false;
		} catch( NumberFormatException e1 ) { // wenn nicht, dann pr�fen, ob wir
												// als hex parsen k�nnen
			try {
				Long.parseLong( value.replaceAll( " ", "" ), 16 ); // eventuelle
																	// Leerzeichen,
																	// die als
																	// Trennzeichen
																	// fungieren (z.
																	// B. "A2 FF
																	// 7E")
																	// entfernen
				return true;
			} catch( NumberFormatException e2 ) {
				return false;
			}
		}
	}

	/**
	 * Parst den �bergebenen Wert und wandelt ihn in ein Double um. Hierbei wird
	 * automatisch auf verschiedene Zahlenformate R�cksicht genommen.
	 * 
	 * @param value
	 *            Der zu parsende Wert.
	 * @return Der ermittelte Double-Wert.
	 * @throws NumberFormatException
	 *             Wenn kein Parsen m�glich ist, da entweder reiner Text
	 *             vorliegt oder das Zahlenformat unbekannt ist.
	 */
	private double parseDouble( String value ) throws NumberFormatException {
		if( isHex( value ) )
			return (double) Long.parseLong( value.replaceAll( " ", "" ), 16 ); // eventuelle
																				// Leerzeichen,
																				// die
																				// als
																				// Trennzeichen
																				// fungieren
																				// (z.
																				// B.
																				// "A2
																				// FF
																				// 7E")
																				// entfernen
		else
			return Double.parseDouble( value );
	}

}

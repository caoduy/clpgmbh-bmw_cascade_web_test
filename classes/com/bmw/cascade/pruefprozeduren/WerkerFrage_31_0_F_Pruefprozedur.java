/*
 * WerkerFrage_V0_0_4_FA_Pruefprozedur.java
 *
 */
package com.bmw.cascade.pruefprozeduren;

import java.util.*;

import com.bmw.cascade.auftrag.prueflinge.*;
import com.bmw.cascade.pruefstand.pruefumfang.*;
import com.bmw.cascade.pruefstand.visualisierung.UserDialog;
import com.bmw.cascade.pruefstand.devices.*;
import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;

/**
 * Implementierung der Pr�fprozedur, die f�r eine vorgegeben Zeitdauer eine Frage an den Werker ausgibt,
 * die dieser mit OK zu beantworten hat. Nach Ablauf der Zeit wird automatisch ein Nicht-OK angenommen.
 * @author Winkler
 * @version Implementierung
 * @version 0_0_3 27.01.2004  NM  Zusaetzlicher optionaler Parameter TITEL hinzu<BR>
 * @version 0_0_4 09.08.2004  AM  Eintrag der Antwort in das Ergebnisobjekt als String (YES/NO), (bisher nur als Zahl)<BR>
 * @version 0_0_5 04.02.2005  Konstante FehlerText hinzu, damit dem Ergebnis-Objekt im FT_NIO-Fall immer ein Fehlertext als Parameter uebergeben wird.<BR>
 * @version 0_2_5 21.02.2005  Gehring  Konstanten FehlerText durch PB.getString("werkerfrageAbbruch") ersetzt.<BR>
 * @version 0_2_6 24.03.2005  Gehring  FA - Version der V0_2_5_TA - Version
 * @version 21_0_T 16.03.2010 TB Alle Parameter unterst�tzen den @ Parameter
 * @version 22_0_F 27.06.2012 MS T- als F-Version
 * @version 23_0_T 06.07.2012 CW BugFix: ist ein ";" im Fragetext, so wird der Text hinter dem ";" auch angezeigt
 * @version 24_0_F 06.07.2012 CW BugFix: ist ein ";" im Fragetext, so wird der Text hinter dem ";" auch angezeigt
 * @version 25_0_T BL HWT als optionales Argument hinzugef�gt
 * @version 26_0_T TB Bugfix Abw�rtskompatibilit�t beim HWT <BR>
 * @version 27_0_F MS F-Version
 * @version 29_0_F MS neuen optionalen Parameter invert
 * @version 30_0_T TB LOP 2124: UserButton Auswahl im virt. Fzg. dokumentieren, Generics <BR>
 * @version 31_0_F TB F-Version <BR>
 */
public class WerkerFrage_31_0_F_Pruefprozedur extends AbstractBMWPruefprozedur {

	static final long serialVersionUID = 1L;

	/**
	 * DefaultKonstruktor, nur fuer die Deserialisierung
	 */
	public WerkerFrage_31_0_F_Pruefprozedur() {
	}

	/**
	 * erzeugt eine neue Pruefprozedur.
	 * @param pruefling Klasse des zugeh. Pr�flings
	 * @param pruefprozName Name der Pr�fprozedur
	 * @param hasToBeExecuted Ausf�hrbedingung f�r Fehlerfreiheit
	 */
	public WerkerFrage_31_0_F_Pruefprozedur( Pruefling pruefling, String pruefprozName, Boolean hasToBeExecuted ) {
		super( pruefling, pruefprozName, hasToBeExecuted );
		attributeInit();
	}

	/**
	 * initialsiert die Argumente
	 */
	@Override
	protected void attributeInit() {
		super.attributeInit();
	}

	/**
	 * liefert die optionalen Argumente
	 */
	@Override
	public String[] getOptionalArgs() {
		String[] args = { "TITEL", "HWT", "INVERT" };
		return args;
	}

	/**
	 * liefert die zwingend erforderlichen Argumente
	 */
	@Override
	public String[] getRequiredArgs() {
		String[] args = { "DAUER", "FT" };
		return args;
	}

	/**
	 * pr�ft - soweit als m�glich - die Argumente auf Existenz und Wert.
	 */
	@Override
	public boolean checkArgs() {
		boolean ok;

		try {
			ok = super.checkArgs();
			if( ok == true ) {
				try {
					if( (Integer.parseInt( getArg( getRequiredArgs()[0] ) )) < 0 )
						return false;
				} catch( NumberFormatException e ) {
					return false;
				}
			}
			return ok;
		} catch( Exception e ) {
			return false;
		} catch( Throwable e ) {
			return false;
		}
	}

	/**
	 * f�hrt die Pr�fprozedur aus
	 * @param info Information zur Ausf�hrung
	 */
	@Override
	public void execute( ExecutionInfo info ) {
		// immer notwendig
		Ergebnis result;
		Vector<Ergebnis> ergListe = new Vector<Ergebnis>();
		int status = STATUS_EXECUTION_OK;

		// spezifische Variablen
		String titel;
		String ft;
		String hwt;
		int dauer;
		String sAnswer = null;
		int iAnswer;

		boolean udInUse = false;
		boolean invert = false;

		/***********************************************
		 * Label in Transientdatei schreiben, die folgende Zeile ist auch notig
		 * import com.bmw.cascade.pruefstand.devices.uianalyse.UIAnalyserEcos;
		 ***********************************************/
		try {
			UIAnalyserEcos myAnalyser = getPr�flingLaufzeitUmgebung().getDeviceManager().getUIAnalyserEcos();
			myAnalyser.LogSetTestStepName( this.getName() );
			getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
		} catch( Exception e ) {
			try {
				getPr�flingLaufzeitUmgebung().getDeviceManager().releaseUIAnalyserEcos();
			} catch( Exception e1 ) {
			}
		}

		try {
			//Parameter holen 
			try {
				if( checkArgs() == false )
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );

				//DAUER
				try {
					dauer = Integer.parseInt( extractValues( getArg( getRequiredArgs()[0] ) )[0] );
				} catch( NumberFormatException e ) {
					throw new PPExecutionException( PB.getString( "parameterexistenz" ) );
				}

				//FT
				if( getArg( getRequiredArgs()[1] ).contains( "@" ) ) {
					ft = PB.getString( extractValues( getArg( getRequiredArgs()[1] ) )[0] );
				} else {
					ft = PB.getString( getArg( getRequiredArgs()[1] ) );
				}

				//TITEL
				titel = getArg( extractValues( getOptionalArgs()[0] )[0] );
				if( titel == null )
					titel = "frage";

				//HWT
				if( getArg( "HWT" ) != null ) {
					if( getArg( "HWT" ).contains( "@" ) ) {
						hwt = PB.getString( extractValues( getArg( "HWT" ) )[0] );
					} else {
						hwt = getArg( "HWT" );
					}
				} else {
					hwt = "";
				}

				//INVERT
				if( getArg( "INVERT" ) != null ) {
					if( getArg( "INVERT" ).equalsIgnoreCase( "TRUE" ) ) {
						invert = true;
					} else {
						invert = false;
					}
				} else {
					invert = false;
				}

			} catch( PPExecutionException e ) {
				if( e.getMessage() != null )
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), e.getMessage(), Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ParaFehler", "", "", "", "", "", "", "", "", "0", "", "", "", PB.getString( "parametrierfehler" ), "", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			//Devices holen
			try {
				iAnswer = getPr�flingLaufzeitUmgebung().getUserDialog().requestUserInputDigital( PB.getString( titel ), ft, dauer / 1000 );
				udInUse = true;
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else if( e instanceof DeviceNotAvailableException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceNotAvailableException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "get", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				throw new PPExecutionException();
			}

			if( (iAnswer == UserDialog.YES_KEY) && (invert == false) ) {
				sAnswer = "YES";
				result = new Ergebnis( PB.getString( titel ), "Userdialog", "", "", "", "BUTTON", "YES", "YES", "", "0", ft, sAnswer, "", "", "", Ergebnis.FT_IO );
			} else if( (iAnswer == UserDialog.NO_KEY) && (invert == true) ) {
				sAnswer = "YES";
				result = new Ergebnis( PB.getString( titel ), "Userdialog", "", "", "", "BUTTON", "YES (INVERT)", "YES (INVERT)", "", "0", ft, sAnswer + " (Ergebnis invert)", "", "", "", Ergebnis.FT_IO );
			} else {
				sAnswer = "NO";
				result = new Ergebnis( PB.getString( titel ), "Userdialog", "", "", "", "BUTTON", "NO", "YES", "", "0", ft, sAnswer, "", PB.getString( "werkerfrageAbbruch" ), hwt, Ergebnis.FT_NIO );
				status = STATUS_EXECUTION_ERROR;
			}
			ergListe.add( result );

		} catch( PPExecutionException e ) {
			status = STATUS_EXECUTION_ERROR;
		} catch( Exception e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Exception", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		} catch( Throwable e ) {
			result = new Ergebnis( "ExecFehler", "", "", "", "", "", "", "", "", "0", "", "", "", "Throwable", PB.getString( "unerwarteterLaufzeitfehler" ), Ergebnis.FT_NIO_SYS );
			ergListe.add( result );
			status = STATUS_EXECUTION_ERROR;
		}

		//Devices freigeben
		if( udInUse == true ) {
			try {
				getPr�flingLaufzeitUmgebung().releaseUserDialog();
			} catch( Exception e ) {
				if( e instanceof DeviceLockedException )
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "DeviceLockedException", "", Ergebnis.FT_NIO_SYS );
				else
					result = new Ergebnis( "ExecFehler", "Userdialog", "", "", "", "", "", "", "", "0", "", "", "", "Exception", "release", Ergebnis.FT_NIO_SYS );
				ergListe.add( result );
				status = STATUS_EXECUTION_ERROR;
			}
		}

		//Status setzen
		setPPStatus( info, status, ergListe );

	}

}

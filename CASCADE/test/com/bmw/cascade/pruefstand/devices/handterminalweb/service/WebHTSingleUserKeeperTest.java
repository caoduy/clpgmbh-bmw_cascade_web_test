package com.bmw.cascade.pruefstand.devices.handterminalweb.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;

import org.glassfish.grizzly.http.server.Request;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.bmw.cascade.pruefstand.devices.handterminalweb.domain.WebHTUserInfo;

@RunWith(MockitoJUnitRunner.class)
public class WebHTSingleUserKeeperTest {
	private static final long MAX_TOLLERANCE_100_MS = 100;
	private static final String IP_USER_1 = "ip_of_user_1";
	private static final String IP_USER_2 = "ip_of_user_2";
	
	@Mock 
	private Request USER_1_REQUEST;
	
	@Mock 
	private Request USER_2_REQUEST;
	
	private WebHTSingleUserKeeper sut;
	
	@Before
	public void init() {
		Mockito.when(USER_1_REQUEST.getHeader(WebHTSingleUserKeeper.HEADER_PARAM_USER_ID)).thenReturn("user-1-id");
		Mockito.when(USER_1_REQUEST.getRemoteAddr()).thenReturn(IP_USER_1);
		
		Mockito.when(USER_2_REQUEST.getHeader(WebHTSingleUserKeeper.HEADER_PARAM_USER_ID)).thenReturn("user-2-id");
		Mockito.when(USER_2_REQUEST.getRemoteAddr()).thenReturn(IP_USER_2);

		sut = new WebHTSingleUserKeeper();
	}
	
	@Test
	public void test_user_can_extends_own_time_to_live() throws Exception {
		assertThat(sut.iAmCurrentUser(USER_1_REQUEST)).isTrue();

		Thread.sleep(500);
		assertThat(sut.iAmCurrentUser(USER_1_REQUEST)).isTrue();

		WebHTUserInfo currentUser = sut.getCurrentUserInfo();
		assertThat(currentUser.getIp()).isEqualTo(IP_USER_1);
		assertThat(currentUser.getLastAccessTime()).isCloseTo(new Date(), MAX_TOLLERANCE_100_MS);
	}

	@Test
	public void test_i_am_current_user_is_false_because_in_use_of_other() throws Exception {
		assertThat(sut.iAmCurrentUser(USER_1_REQUEST)).isTrue();

		Thread.sleep(500);
		assertThat(sut.iAmCurrentUser(USER_2_REQUEST)).isFalse().as("USER_1 is using");

		Thread.sleep(500);
		assertThat(sut.iAmCurrentUser(USER_1_REQUEST)).isTrue();
	}

	@Test
	public void test_user1_time_to_live_exeeded_so_other_can_take_over() throws Exception {
		sut.setTimeToLiveInSeconds(1);
		assertThat(sut.iAmCurrentUser(USER_1_REQUEST)).isTrue();

		Thread.sleep(1500);
		assertThat(sut.iAmCurrentUser(USER_2_REQUEST)).isTrue().as("time to live of USER_1 is over!");

		Thread.sleep(500);
		assertThat(sut.iAmCurrentUser(USER_1_REQUEST)).isFalse().as("USER_2 is now the new kid on the block");
		assertThat(sut.getCurrentUserInfo().getIp()).isEqualTo(IP_USER_2);
	}

	@Test
	public void test_use_x_forwarded_for_instead_of_remote_adr() throws Exception {
		Request req = Mockito.mock(Request.class);

		Mockito.when(req.getRemoteAddr()).thenReturn("remote-adr-ip");
		Mockito.when(req.getHeader("X-Forwarded-For")).thenReturn("real-client-ip");

		assertThat(sut.iAmCurrentUser(req)).isTrue();
		assertThat(sut.getCurrentUserInfo().getIp()).isEqualTo("real-client-ip");
	}
}

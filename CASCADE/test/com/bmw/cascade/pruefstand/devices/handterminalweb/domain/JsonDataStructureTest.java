package com.bmw.cascade.pruefstand.devices.handterminalweb.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonDataStructureTest {

	private static final String TEST_RESOURCES = "test/resources";
	private ObjectMapper jacksonSerialierer = new ObjectMapper();

	@Test
	public void test_INPUT_NR() throws Exception {
		assertPruefAnweisungEqualToJson(AnweisungTyp.INPUT_NR, "spec_INPUT_NR.json");
	}

	@Test
	public void test_INPUT_STR() throws Exception {
		assertPruefAnweisungEqualToJson(AnweisungTyp.INPUT_STR, "spec_INPUT_STR.json");
	}

	@Test
	public void test_JA_NEIN() throws Exception {
		assertPruefAnweisungEqualToJson(AnweisungTyp.JA_NEIN, "spec_JA_NEIN.json");
	}

	@Test
	public void test_MESSAGE_WITH_OK() throws Exception {
		assertPruefAnweisungEqualToJson(AnweisungTyp.MESSAGE_WITH_OK, "spec_MESSAGE_WITH_OK.json");
	}

	@Test
	public void test_MESSAGE_WITH_PROGRESSBAR() throws Exception {
		assertPruefAnweisungEqualToJson(AnweisungTyp.MESSAGE_WITH_PROGRESSBAR, "spec_MESSAGE_WITH_PROGRESSBAR.json");
	}

	@Test
	public void test_MESSAGE() throws Exception {
		assertPruefAnweisungEqualToJson(AnweisungTyp.MESSAGE, "spec_MESSAGE.json");
	}

	@Test
	public void test_MULTIPLE_CHOICE() throws Exception {
		assertPruefAnweisungEqualToJson(AnweisungTyp.MULTIPLE_CHOICE, "spec_MULTIPLE_CHOICE.json");
	}

	@Test
	public void test_PLAY_BEEP_TONE() throws Exception {
		assertPruefAnweisungEqualToJson(AnweisungTyp.PLAY_BEEP_TONE, "spec_PLAY_BEEP_TONE.json");
	}

	@Test
	public void test_SCHRITT_WIEDERHOLUNG() throws Exception {
		assertPruefAnweisungEqualToJson(AnweisungTyp.SCHRITT_WIEDERHOLUNG, "spec_SCHRITT_WIEDERHOLUNG.json");
	}

	@Test
	public void test_SHOW_MENU() throws Exception {
		assertPruefAnweisungEqualToJson(AnweisungTyp.SHOW_MENU, "spec_SHOW_MENU.json");
	}

	@Test
	public void test_SINGLE_CHOICE() throws Exception {
		assertPruefAnweisungEqualToJson(AnweisungTyp.SINGLE_CHOICE, "spec_SINGLE_CHOICE.json");
	}

	@Test
	public void test_ERROR_MESSAGE() throws Exception {
		assertPruefAnweisungEqualToJson(AnweisungTyp.ERROR_MESSAGE, "spec_ERROR_MESSAGE.json");
	}

	private void assertPruefAnweisungEqualToJson(AnweisungTyp typ, String expectedJsonFile) throws JsonProcessingException, IOException {
		String jsonExpected = Files.readAllLines(Paths.get(TEST_RESOURCES, expectedJsonFile)).stream().collect(Collectors.joining());
		String jsonResult = jacksonSerialierer.writeValueAsString(createPruefAnweisung(typ));

		assertThat(jsonResult).isEqualTo(jsonExpected);
	}

	@Test
	public void test_Deutsche_Umlaute() throws Exception {
		AnyBean b = new AnyBean();
		b.setFoo("���� oder ���");

		String jsonResult = jacksonSerialierer.writeValueAsString(b);

		assertThat(jsonResult).isEqualTo("{\"foo\":\"���� oder ���\"}");
	}

	@Test
	public void test_WebHTConfiguration() throws Exception {
		String jsonExpected = Files.readAllLines(Paths.get(TEST_RESOURCES, "spec_WebHTConfiguration.json")).stream().collect(Collectors.joining());

		String jsonResult = jacksonSerialierer.writeValueAsString(new WebHTConfiguration());

		assertThat(jsonResult).isEqualTo(jsonExpected);
	}

	private PruefAnweisung createPruefAnweisung(AnweisungTyp typ) {
		PruefAnweisung pa = new PruefAnweisung();
		pa.setTyp(typ);
		pa.setId("dummy-UUID");
		pa.setTitle("dummy Title");
		pa.setMessage("dummy message");
		pa.setColorStyle("gruen"); // todo
		pa.setChoices(new String[] { "eins", "zwei" });
		pa.setProgress(45);
		pa.setPruefumfang("PA Name");
		pa.setShortVIN("B12345");
		pa.setTestStep("Test Schritt ECOS.XYZ");
		return pa;
	}

	private static class AnyBean {
		private String foo;

		public String getFoo() {
			return foo;
		}

		public void setFoo(String foo) {
			this.foo = foo;
		}
	}

}

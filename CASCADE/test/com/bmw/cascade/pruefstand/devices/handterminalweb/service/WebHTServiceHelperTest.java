package com.bmw.cascade.pruefstand.devices.handterminalweb.service;

import static com.bmw.cascade.pruefstand.devices.handterminalweb.service.WebHTServiceHelper.calculateIndex;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Test;

import com.bmw.cascade.pruefstand.devices.handterminalweb.PB;
import com.bmw.cascade.pruefstand.devices.handterminalweb.domain.WebHTConfiguration;

public class WebHTServiceHelperTest {

	@Test
	public void test_calculateIndex() throws Exception {

		String[] options = new String[] { "eins", "zwei", "drei", "vier" };

		// 1. Element ist immer als OK oder ABBRUCH zu werten. Danach kommen die
		// selektierten Indexe!

		assertThat(calculateIndex("1;eins", options)).isEqualTo(new int[] { 1, 0 }); // ok + 1

		assertThat(calculateIndex("0;\"eins\"", options)).isEqualTo(new int[] { 0, 0 }); // abbr + 1

		assertThat(calculateIndex("1;zwei", options)).isEqualTo(new int[] { 1, 1 }); // ok + 2

		assertThat(calculateIndex("[0;\"zwei\"]", options)).isEqualTo(new int[] { 0, 1 }); // abbr + 2

		assertThat(calculateIndex("[1;\"eins\";\"drei\"]", options)).isEqualTo(new int[] { 1, 0, 2 }); // OK + eins und drei

		assertThat(calculateIndex("[0;\"zwei\";\"vier\"]", options)).isEqualTo(new int[] { 0, 1, 3 }); // ABBR + zwei und vier

		assertThat(calculateIndex("[1;\"vier\";\"zwei\"]", options)).isEqualTo(new int[] { 1, 1, 3 }); // OK, zwei und vier

		assertThat(calculateIndex("[0;\"vier\";\"zwei\";drei]", options)).isEqualTo(new int[] { 0, 1, 2, 3 }); // ABBR + 2,3,4

		assertThat(calculateIndex("1;eins;vier;drei", options)).isEqualTo(new int[] { 1, 0, 2, 3 }); // ok 1,3,4
	}

	@Test
	public void test_createWebHtConfiguration_de() {
		PB.setLocale(Locale.GERMANY);

		WebHTConfiguration config = WebHTServiceHelper.createWebHTConfiguration();

		assertThat(config.getLabelYes()).isEqualTo("Ja");
		assertThat(config.getLabelNo()).isEqualTo("Nein");
		assertThat(config.getLabelOk()).isEqualTo("OK");
		assertThat(config.getLabelCancel()).isEqualTo("Abbruch");
		assertThat(config.getLabelTryAgain()).isEqualTo("Erneut versuchen");

		assertThat(config.getLabelF3()).isEqualTo("Pr�fumfang");
		assertThat(config.getLabelF5()).isEqualTo("Start");
		assertThat(config.getLabelF6()).isEqualTo("Wiederh.");

		assertThat(config.getLabelErrorInput1()).isEqualTo("Funktionsfehler");
		assertThat(config.getLabelErrorInput2()).isEqualTo("Teil fehlt");
		assertThat(config.getLabelErrorInput3()).isEqualTo("Teil falsch");
		assertThat(config.getLabelErrorInput4()).isEqualTo("Teil besch�digt");
		assertThat(config.getLabelErrorInput5()).isEqualTo("Kontrolle defekt");
		assertThat(config.getLabelErrorInput6()).isEqualTo("Teil vertauscht");
		assertThat(config.getLabelErrorInput7()).isEqualTo("Montagefehler");
		assertThat(config.getLabelErrorInput8()).isEqualTo("Schalter defekt");
		assertThat(config.getLabelErrorInput9()).isEqualTo("Nicht Pr�fbar");
		assertThat(config.getLabelErrorInputY()).isEqualTo("Weiter");
		assertThat(config.getLabelErrorInput0()).isEqualTo("Ger�usch (laut)");
		assertThat(config.getLabelErrorInputN()).isEqualTo("R�cksprung");

		assertThat(config.getConnectionErrorTitle()).isEqualTo("Verbindungsproblem");
		assertThat(config.getConnectionErrorMsg()).isEqualTo("Bitte Verbidung zum Pr�fstand kontrollieren und wiederherstellen!");

		assertThat(config.getHtWebTimeoutSec()).isEqualTo(4);
	}

	@Test
	public void test_createWebHtConfiguration_es() {
		PB.setLocale(new Locale("es"));

		WebHTConfiguration config = WebHTServiceHelper.createWebHTConfiguration();

		assertThat(config.getLabelYes()).isEqualTo("S�");
		assertThat(config.getLabelNo()).isEqualTo("No");
		assertThat(config.getLabelOk()).isEqualTo("OK");
		assertThat(config.getLabelCancel()).isEqualTo("Cancelar");
		assertThat(config.getLabelTryAgain()).isEqualTo("Intenta de nuevo");

		assertThat(config.getLabelF3().trim()).isEqualTo("Secuencia");
		assertThat(config.getLabelF5()).isEqualTo("Inicio");
		assertThat(config.getLabelF6()).isEqualTo("Repetir");

		assertThat(config.getLabelErrorInput1()).isEqualTo("Funktionsfehler");
		assertThat(config.getLabelErrorInput2()).isEqualTo("Teil fehlt");
		assertThat(config.getLabelErrorInput3()).isEqualTo("Teil falsch");
		assertThat(config.getLabelErrorInput4()).isEqualTo("Teil besch�digt");
		assertThat(config.getLabelErrorInput5()).isEqualTo("Kontrolle defekt");
		assertThat(config.getLabelErrorInput6()).isEqualTo("Teil vertauscht");
		assertThat(config.getLabelErrorInput7()).isEqualTo("Montagefehler");
		assertThat(config.getLabelErrorInput8()).isEqualTo("Schalter defekt");
		assertThat(config.getLabelErrorInput9()).isEqualTo("Nicht Pr�fbar");
		assertThat(config.getLabelErrorInputY()).isEqualTo("Continuar");
		assertThat(config.getLabelErrorInput0()).isEqualTo("Ger�usch (laut)");
		assertThat(config.getLabelErrorInputN()).isEqualTo("Espalda");

		assertThat(config.getConnectionErrorTitle()).isEqualTo("Error de conexi�n");
		assertThat(config.getConnectionErrorMsg()).isEqualTo("por favor cont�ctame");

		assertThat(config.getHtWebTimeoutSec()).isNotNull();
	}

	@Test
	public void test_calculateStatusColor_statusId_0() throws Exception {
		Integer egal = ThreadLocalRandom.current().nextInt(0, 100);

		assertThat(WebHTServiceHelper.calculateStatusColor(0, egal)).isEqualTo("");
	}

	@Test
	public void test_calculateStatusColor_statusId_2() throws Exception {
		assertThat(WebHTServiceHelper.calculateStatusColor(2, 50)).isEqualTo("green");
	}

	@Test
	public void test_calculateStatusColor_statusId_3_progress_50percent() throws Exception {
		assertThat(WebHTServiceHelper.calculateStatusColor(3, 50)).isEqualTo("dotted-red");
	}

	@Test
	public void test_calculateStatusColor_statusId_3_progress_100percent() throws Exception {
		assertThat(WebHTServiceHelper.calculateStatusColor(3, 100)).isEqualTo("red");
	}
}

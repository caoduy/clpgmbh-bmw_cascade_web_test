package com.bmw.cascade.pruefstand.devices.handterminalweb.webcontent;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.Charset;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.junit.Test;

import com.bmw.cascade.pruefstand.devices.handterminalweb.WlanHandterminalWeb;
import com.bmw.cascade.pruefstand.devices.handterminalweb.domain.PruefAnweisung;
import com.bmw.cascade.pruefstand.devices.handterminalweb.domain.WebHTUserInfo;
import com.bmw.cascade.pruefstand.devices.handterminalweb.service.WebHTSingleUserKeeper;
import com.fasterxml.jackson.databind.ObjectMapper;


public class WebHTRestServiceITest {

	@Test
	public void test_getJson_in_iso8859_1() throws Exception {
		WlanHandterminalWeb service = new WlanHandterminalWeb("momentan egal");

		String isoStr = new String("iso-8859-1 mit Umlauten ��� oder ����".getBytes(), Charset.forName("ISO-8859-1"));
		service.displayMessage(isoStr);

		WebHTUserInfo userInfo = WebHTSingleUserKeeper.getInstance(3).getCurrentUserInfo();

		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8080/service/anweisung");
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON)//
				.acceptEncoding("ISO-8859-1")//
				.header(WebHTSingleUserKeeper.HEADER_PARAM_USER_ID, userInfo.getUserId())//
				.header("HtWeb-Timeout-Sec", 1);
		String jsonResponse = invocationBuilder.get(String.class);

		PruefAnweisung anweisung = new ObjectMapper().readValue(jsonResponse, PruefAnweisung.class);
		assertThat(anweisung.getMessage()).isEqualTo(isoStr);

		WebHTRestService.stopWebServer();
		Thread.sleep(1000);
	}

}

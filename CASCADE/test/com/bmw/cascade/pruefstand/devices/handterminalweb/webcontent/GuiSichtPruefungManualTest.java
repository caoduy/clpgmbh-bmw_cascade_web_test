package com.bmw.cascade.pruefstand.devices.handterminalweb.webcontent;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;

import com.bmw.cascade.pruefstand.devices.DeviceIOException;
import com.bmw.cascade.pruefstand.devices.DeviceNotAvailableException;
import com.bmw.cascade.pruefstand.devices.handterminal.Handterminal;
import com.bmw.cascade.pruefstand.devices.handterminalweb.PB;
import com.bmw.cascade.pruefstand.devices.handterminalweb.WlanHandterminalWeb;

@Ignore
public class GuiSichtPruefungManualTest {

	private long delay = 1000;

	private static final Map<String, Integer> styleNameCodeMap = new HashMap<String, Integer>();
	static {
		styleNameCodeMap.put("Default", 0);
		styleNameCodeMap.put("Blau", 1);
		styleNameCodeMap.put("Rot", 2);
		styleNameCodeMap.put("Gr�n", 3);
	}

	@Test
	public void test_sicht_pruefung_jeden_masken_Typ() throws Exception {
		PB.getInstance().setLocale(new Locale("es"));
		delay = 1500;

		WlanHandterminalWeb service = startSericeAndOpenBrowser();
		Thread.sleep(5000);

		// CountDown
		for (int i = 10; i > 0; i--) {
			service.clearScreen();
			Thread.sleep(10);
			service.displayMessage(i + " sec. Wartezeit");
			Thread.sleep(1000);
		}

		service.displayMessage("Achtung CLEAR-SCREEN", "Gleich wird es f�r ein paar Sekunden einfach nichts angezeigt!");
		Thread.sleep(delay);

		// service.messageBeep();
		service.clearScreen();
		Thread.sleep(5000);

		service.displayMessageWithProgressBar("In Progress", "Irgendwas wird verarbeitet", 10);
		Thread.sleep(delay);

		service.clearScreen();
		service.requestUserInput("Text-Eingabe", "simple Text-Eingabe");
		Thread.sleep(delay);

		service.clearScreen();
		service.requestUserInputNumber("Number-Input", "Nummern Eingabe");
		Thread.sleep(delay);

		service.requestUserInputDigital("Frage", "Alles gut?", Handterminal.INPUT_TYPE_YES_NO);
		Thread.sleep(delay);

		service.requestUserInputDigital("Info", "Nachricht mit OK Button", Handterminal.INPUT_TYPE_OK);
		Thread.sleep(delay);

		service.requestUserInputDigital("Info", "Nachricht mit Cancel Button", Handterminal.INPUT_TYPE_CANCEL);
		Thread.sleep(delay);

		service.requestUserInputMultipleChoice("Multi-Choice", "choose some!", new String[] { "one", "two", "three" });
		Thread.sleep(delay);

		service.requestUserInputSelection("Schritt Wiederholung", "Schritt XYZ wiederholen?", new String[] { "one", "two", "three" });
		Thread.sleep(delay);

		service.requestUserInputSingleChoice("Single-Choice", "choose one!", new String[] { "one", "two", "three" });
		Thread.sleep(delay);

		service.showCascadeMenue();
		Thread.sleep(delay);

		service.close();
	}


	private WlanHandterminalWeb startSericeAndOpenBrowser() throws DeviceNotAvailableException, InterruptedException, IOException, DeviceIOException {
		WlanHandterminalWeb service = new WlanHandterminalWeb("irgendwelchen Connection String erst mal irrelevant");

		service.setShortVIN("B274612");
		service.setTeststep("ECO.BLA_BLUB");
		service.setStatus(0);
		service.setPUName("Pr�fumfang XXX");
		service.setProgress(12);
		service.showCascadeMenue();

		Runtime.getRuntime().exec("open -a FireFox http://localhost:8080/index.html");
		return service;
	}

}
package com.bmw.cascade.pruefstand.devices.handterminalweb.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import com.bmw.cascade.pruefstand.devices.handterminalweb.domain.AnweisungTyp;
import com.bmw.cascade.pruefstand.devices.handterminalweb.domain.PruefAntwort;

public class WebHTLoggerTest {

	@Test
	public void test_methoden_aufruf_ohne_Argumente() {
		assertThat(WebHTLogger.buildPrettyStringOfMethodCall("hello")).isEqualTo("hello();");
	}

	@Test
	public void test_methoden_aufruf_mit_nullValues_Argumente() {
		assertThat(WebHTLogger.buildPrettyStringOfMethodCall("hello", null, 1, null)).isEqualTo("hello(null, 1, null);");
	}

	@Test
	public void test_methoden_aufruf_mit_1_Argument() {
		assertThat(WebHTLogger.buildPrettyStringOfMethodCall("hi", "Bob")).isEqualTo("hi(Bob);");
	}

	@Test
	public void test_methoden_aufruf_mit_mehrere_Argumente() {
		assertThat(WebHTLogger.buildPrettyStringOfMethodCall("someMethode", "Bob", 32, true)).isEqualTo("someMethode(Bob, 32, true);");
	}

	@Test
	public void test_methoden_aufruf_mit_mehrere_Argumente_darunter_ArrayOfString() {
		// MultipleChoice(String title, String message, String[] choices)
		assertThat(WebHTLogger.buildPrettyStringOfMethodCall("requestUserInputMultipleChoice", "title", "message", new String[] { "1", "2", "3" }))
				.isEqualTo("requestUserInputMultipleChoice(title, message, {1, 2, 3});");
	}

	@Test
	public void test_methoden_aufruf_mit_PruefAntwort() {
		PruefAntwort pruefAntwort = new PruefAntwort("123", AnweisungTyp.MULTIPLE_CHOICE, "y,1");

		assertThat(WebHTLogger.buildPrettyStringOfMethodCall("someObject", pruefAntwort)) //
				.isEqualTo("someObject(@PruefAntwort={pruefAnweisungsID=123, typ=MULTIPLE_CHOICE, wert=y,1});");
	}
}

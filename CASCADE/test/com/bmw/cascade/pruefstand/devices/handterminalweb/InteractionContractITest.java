package com.bmw.cascade.pruefstand.devices.handterminalweb;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.glassfish.grizzly.http.server.Request;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.bmw.cascade.pruefstand.devices.handterminal.Handterminal;
import com.bmw.cascade.pruefstand.devices.handterminalweb.domain.AnweisungTyp;
import com.bmw.cascade.pruefstand.devices.handterminalweb.domain.PruefAnweisung;
import com.bmw.cascade.pruefstand.devices.handterminalweb.domain.WebHTUserInfo;
import com.bmw.cascade.pruefstand.devices.handterminalweb.service.WebHTSingleUserKeeper;
import com.bmw.cascade.pruefstand.devices.handterminalweb.webcontent.WebHTRestService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class InteractionContractITest {
	private static ExecutorService threadPool = Executors.newSingleThreadExecutor();
	private static WlanHandterminalWeb TEST_SCREEN = new WlanHandterminalWeb();
	private static WebHTRestService WEB_CLIENT = new WebHTRestService();

	private ObjectMapper jacksonSerialierer = new ObjectMapper();

	@AfterClass
	public static void afterAll() {
		threadPool.shutdownNow();
	}

	@Test
	public void test_displayMessage_1() throws Exception {
		TEST_SCREEN.displayMessage("hello");

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.MESSAGE);
		assertThat(anweisung.getTitle()).isEmpty();
		assertThat(anweisung.getMessage()).isEqualTo("hello");
	}

	@Test
	public void test_displayMessage_2() throws Exception {
		TEST_SCREEN.displayMessage("Meldung", "alles ok");

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.MESSAGE);
		assertThat(anweisung.getTitle()).isEqualTo("Meldung");
		assertThat(anweisung.getMessage()).isEqualTo("alles ok");
	}

	@Test
	public void test_displayMessage_4_red() throws Exception {
		TEST_SCREEN.setProgress(100);
		TEST_SCREEN.setStatus(3); // 3=Fehler (Pr�fumfang hat Fehler)
		TEST_SCREEN.displayMessage("Meldung", "NIO", 2); // style wird ignoriert, wichtig ist statusId

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.MESSAGE);
		assertThat(anweisung.getTitle()).isEqualTo("Meldung");
		assertThat(anweisung.getMessage()).isEqualTo("NIO");
		assertThat(anweisung.getColorStyle()).isEqualTo("red");
	}

	@Test
	public void test_displayMessage_4_green() throws Exception {
		TEST_SCREEN.setStatus(2); // status 2 = OK
		TEST_SCREEN.displayMessage("Meldung", "OK", 3); // color-style wird ignoriert, kommt sowieso falsch

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.MESSAGE);
		assertThat(anweisung.getTitle()).isEqualTo("Meldung");
		assertThat(anweisung.getMessage()).isEqualTo("OK");
		assertThat(anweisung.getColorStyle()).isEqualTo("green");

		// 4 ist auch gr�nn
		TEST_SCREEN.displayMessage("Meldung", "hello", 4);
		anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getColorStyle()).isEqualTo("green");

		// 0 ist irgendwie auch gr�nn ... total unzuverl�ssig was Cascade so als
		// Style-Color schickt
		TEST_SCREEN.displayMessage("Meldung", "hello", 0);
		anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getColorStyle()).isEqualTo("green");
	}

	@Test
	public void test_displayMessageWithProgressBar() throws Exception {
		TEST_SCREEN.displayMessageWithProgressBar("Meldung", "hello", 2); // die 2 ist nicht styleNr sondern Fortschritt in %

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.MESSAGE_WITH_PROGRESSBAR);
		assertThat(anweisung.getTitle()).isEqualTo("Meldung");
		assertThat(anweisung.getMessage()).isEqualTo("hello");
	}

	@Test
	public void test_requestUserInput_1() throws Exception {
		TEST_SCREEN.requestUserInput("eingabe");

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.INPUT_STR);
		assertThat(anweisung.getTitle()).isEmpty();
		assertThat(anweisung.getMessage()).isEqualTo("eingabe");

		WEB_CLIENT.answer(anweisung.getId(), "INPUT_STR", "bla");
		assertThat(TEST_SCREEN.waitForUserInput()).isEqualTo("bla");
	}

	@Test
	public void test_requestUserInput_2() throws Exception {
		TEST_SCREEN.requestUserInput("Anweisung", "eingabe");

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.INPUT_STR);
		assertThat(anweisung.getTitle()).isEqualTo("Anweisung");
		assertThat(anweisung.getMessage()).isEqualTo("eingabe");

		WEB_CLIENT.answer(anweisung.getId(), "INPUT_STR", "bla");
		assertThat(TEST_SCREEN.waitForUserInput()).isEqualTo("bla");
	}

	@Test
	public void test_requestUserInput_3() throws Exception {
		TEST_SCREEN.requestUserInput("eingabe", 0); // style wird ignoriert

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.INPUT_STR);
		assertThat(anweisung.getTitle()).isEmpty();
		assertThat(anweisung.getMessage()).isEqualTo("eingabe");

		WEB_CLIENT.answer(anweisung.getId(), "INPUT_STR", "bla");
		assertThat(TEST_SCREEN.waitForUserInput()).isEqualTo("bla");
	}

	@Test
	public void test_requestUserInput_4() throws Exception {
		TEST_SCREEN.requestUserInput("Meldung", "eingabe", 1);

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.INPUT_STR);
		assertThat(anweisung.getTitle()).isEqualTo("Meldung");
		assertThat(anweisung.getMessage()).isEqualTo("eingabe");

		WEB_CLIENT.answer(anweisung.getId(), "INPUT_STR", "bla");
		assertThat(TEST_SCREEN.waitForUserInput()).isEqualTo("bla");
	}

	@Test
	public void test_requestUserInputNumber_1() throws Exception {
		TEST_SCREEN.requestUserInputNumber("eingabe");

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.INPUT_NR);
		assertThat(anweisung.getTitle()).isEmpty();
		assertThat(anweisung.getMessage()).isEqualTo("eingabe");

		WEB_CLIENT.answer(anweisung.getId(), "INPUT_STR", "123");
		assertThat(TEST_SCREEN.waitForUserInput()).isEqualTo("123");
	}

	@Test
	public void test_requestUserInputNumber_2() throws Exception {
		TEST_SCREEN.requestUserInputNumber("Anweisung", "eingabe");

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.INPUT_NR);
		assertThat(anweisung.getTitle()).isEqualTo("Anweisung");
		assertThat(anweisung.getMessage()).isEqualTo("eingabe");

		WEB_CLIENT.answer(anweisung.getId(), "INPUT_STR", "123");
		assertThat(TEST_SCREEN.waitForUserInput()).isEqualTo("123");
	}

	@Test
	public void test_requestUserInputNumber_3() throws Exception {
		TEST_SCREEN.requestUserInputNumber("eingabe", 0);

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.INPUT_NR);
		assertThat(anweisung.getTitle()).isEmpty();
		assertThat(anweisung.getMessage()).isEqualTo("eingabe");

		WEB_CLIENT.answer(anweisung.getId(), "INPUT_STR", "123");
		assertThat(TEST_SCREEN.waitForUserInput()).isEqualTo("123");
	}

	@Test
	public void test_requestUserInputNumber_4() throws Exception {
		TEST_SCREEN.requestUserInputNumber("Meldung", "eingabe", 1);

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.INPUT_NR);
		assertThat(anweisung.getTitle()).isEqualTo("Meldung");
		assertThat(anweisung.getMessage()).isEqualTo("eingabe");

		WEB_CLIENT.answer(anweisung.getId(), "INPUT_STR", "123");
		assertThat(TEST_SCREEN.waitForUserInput()).isEqualTo("123");
	}

	@Test
	public void test_requestUserInputSingleChoice_OK() throws Exception {
		String[] choices = new String[] { "eins", "zwei", "drei" };

		TEST_SCREEN.requestUserInputSingleChoice("single-choice", "choose one", choices);

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.SINGLE_CHOICE);
		assertThat(anweisung.getTitle()).isEqualTo("single-choice");
		assertThat(anweisung.getMessage()).isEqualTo("choose one");
		assertThat(anweisung.getChoices()).isEqualTo(choices);

		WEB_CLIENT.answer(anweisung.getId(), "SINGLE_CHOICE", "1;drei");// 1=OK

		assertThat(TEST_SCREEN.waitForSingleUserChoice()).isEqualTo(new int[] { TEST_SCREEN.getOkTaste().charAt(0), 2 }); // 2=index of drei
	}

	@Test
	public void test_requestUserInputSingleChoice_mit_Selektion_aber_ABBRUCH() throws Exception {
		String[] choices = new String[] { "eins", "zwei", "drei" };

		TEST_SCREEN.requestUserInputSingleChoice("single-choice", "choose one", choices);
		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.SINGLE_CHOICE);

		WEB_CLIENT.answer(anweisung.getId(), "SINGLE_CHOICE", "0;drei"); // 0=Abbruch

		assertThat(TEST_SCREEN.waitForSingleUserChoice()).isEqualTo(new int[] { TEST_SCREEN.getNokTaste().charAt(0), 2 });
	}

	@Test
	public void test_requestUserInputSingleChoice_Keine_Selektion_ABBRUCH() throws Exception {
		String[] choices = new String[] { "eins", "zwei", "drei" };

		TEST_SCREEN.requestUserInputSingleChoice("single-choice", "choose one", choices);
		PruefAnweisung anweisung = webClientHoleAnweisung();
		WEB_CLIENT.answer(anweisung.getId(), "SINGLE_CHOICE", "0;"); // 0=Abbruch

		assertThat(TEST_SCREEN.waitForSingleUserChoice()).isEqualTo(new int[] { TEST_SCREEN.getNokTaste().charAt(0) });
	}

	@Test
	public void test_requestUserInputMultipleChoice_OK() throws Exception {
		String[] choices = new String[] { "eins", "zwei", "drei" };

		TEST_SCREEN.requestUserInputMultipleChoice("multi-choice", "choose some", choices);

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.MULTIPLE_CHOICE);
		assertThat(anweisung.getTitle()).isEqualTo("multi-choice");
		assertThat(anweisung.getMessage()).isEqualTo("choose some");
		assertThat(anweisung.getChoices()).isEqualTo(choices);

		WEB_CLIENT.answer(anweisung.getId(), "MULTIPLE_CHOICE", "1;drei;eins"); // 1=OK

		assertThat(TEST_SCREEN.waitForMultipleUserChoice()).isEqualTo(new int[] { TEST_SCREEN.getOkTaste().charAt(0), 0, 2 });
	}

	@Test
	public void test_requestUserInputMultipleChoice_ABBRUCH() throws Exception {
		String[] choices = new String[] { "eins", "zwei", "drei" };

		TEST_SCREEN.requestUserInputMultipleChoice("multi-choice", "choose some", choices);

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.MULTIPLE_CHOICE);
		assertThat(anweisung.getTitle()).isEqualTo("multi-choice");
		assertThat(anweisung.getMessage()).isEqualTo("choose some");
		assertThat(anweisung.getChoices()).isEqualTo(choices);

		WEB_CLIENT.answer(anweisung.getId(), "MULTIPLE_CHOICE", "0;drei;eins");

		assertThat(TEST_SCREEN.waitForMultipleUserChoice()).isEqualTo(new int[] { TEST_SCREEN.getNokTaste().charAt(0), 0, 2 });
	}


	@Test
	public void test_requestUserInputSelection() throws Exception {
		String okTaste = TEST_SCREEN.getOkTaste();
		String nokTaste = TEST_SCREEN.getNokTaste();

		TEST_SCREEN.requestUserInputSelection("Wiederholung", "Pr�fschritt X NIO", new String[] { okTaste, nokTaste, "1", "2", "3" });

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.SCHRITT_WIEDERHOLUNG);
		assertThat(anweisung.getTitle()).isEqualTo("Wiederholung");
		assertThat(anweisung.getMessage()).isEqualTo("Pr�fschritt X NIO");
		assertThat(anweisung.getChoices()).isEqualTo(new String[] { "1", "2", "3" });
		
		WEB_CLIENT.answer(anweisung.getId(), "SCHRITT_WIEDERHOLUNG", "1");

		String seletedValue = TEST_SCREEN.waitForSelection();
		assertThat(seletedValue).isEqualTo("1");
	}

	@Test
	public void test_messageBeep() throws Exception {
		TEST_SCREEN.displayMessage("Irgend ein Hinweis");

		PruefAnweisung anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.MESSAGE);

		// wartet nun auf n�chste Anweisung
		Future<PruefAnweisung> fAnweisung = Executors.newSingleThreadExecutor().submit(new Callable<PruefAnweisung>() {
			@Override
			public PruefAnweisung call() throws Exception {
				String jsonStr = (String) WEB_CLIENT.waitForNextAnweisung(5, mockRequest()).getEntity();
				return jacksonSerialierer.readValue(jsonStr, PruefAnweisung.class);
			}
		});
		
		TEST_SCREEN.messageBeep(); // einmaliger Beep-Ton!
		anweisung = fAnweisung.get();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.PLAY_BEEP_TONE);

		// Beep ist einmalig ... jetzt bekommt Client wieder die Urspr�ngliche Anweisung
		anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.MESSAGE);

		anweisung = webClientHoleAnweisung();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.MESSAGE);
	}

	@Test
	public void test_requestUserInputDigital_typ4_Yes() throws Exception {
		Future<PruefAnweisung> fAnweisung = webClientWartetAufAnweisung(30);

		TEST_SCREEN.setProgress(25);
		TEST_SCREEN.setTeststep("Testschritt-XYZ");
		TEST_SCREEN.setShortVIN("ShortVIN-XYZ");
		TEST_SCREEN.setStatus(1);
		TEST_SCREEN.requestUserInputDigital("Frage", "Alles g�t?", Handterminal.INPUT_TYPE_YES_NO);

		PruefAnweisung anweisung = fAnweisung.get();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.JA_NEIN);
		assertThat(anweisung.getTitle()).isEqualTo("Frage");
		assertThat(anweisung.getMessage()).isEqualTo("Alles g�t?");
		
		WEB_CLIENT.answer(anweisung.getId(), "JA_NEIN", "J");
		
		int resultYesKey = TEST_SCREEN.waitForKey();
		assertThat(resultYesKey).isEqualTo(Handterminal.YES_KEY);
	}

	@Test
	public void test_requestUserInputDigital_typ4_No() throws Exception {
		Future<PruefAnweisung> fAnweisung = webClientWartetAufAnweisung(30);

		TEST_SCREEN.setProgress(25);
		TEST_SCREEN.setTeststep("Testschritt-XYZ");
		TEST_SCREEN.setShortVIN("ShortVIN-XYZ");
		TEST_SCREEN.setStatus(1);
		TEST_SCREEN.requestUserInputDigital("Frage", "Alles g�t?", Handterminal.INPUT_TYPE_YES_NO);

		PruefAnweisung anweisung = fAnweisung.get();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.JA_NEIN);
		assertThat(anweisung.getTitle()).isEqualTo("Frage");
		assertThat(anweisung.getMessage()).isEqualTo("Alles g�t?");

		WEB_CLIENT.answer(anweisung.getId(), "JA_NEIN", "N");

		int resultYesKey = TEST_SCREEN.waitForKey();
		assertThat(resultYesKey).isEqualTo(Handterminal.NO_KEY);
	}

	@Test
	public void test_requestUserInputDigital_typ2() throws Exception {
		Future<PruefAnweisung> fAnweisung = webClientWartetAufAnweisung(30);

		TEST_SCREEN.setProgress(25);
		TEST_SCREEN.setTeststep("Testschritt-XYZ");
		TEST_SCREEN.setShortVIN("ShortVIN-XYZ");
		TEST_SCREEN.setStatus(1);
		TEST_SCREEN.requestUserInputDigital("Anweisung", "n�t ablehnbar", Handterminal.INPUT_TYPE_OK);

		PruefAnweisung anweisung = fAnweisung.get();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.MESSAGE_WITH_OK);
		assertThat(anweisung.getTitle()).isEqualTo("Anweisung");
		assertThat(anweisung.getMessage()).isEqualTo("n�t ablehnbar");

		WEB_CLIENT.answer(anweisung.getId(), "MESSAGE_WITH_OK", "ok");

		String resultStr = TEST_SCREEN.waitForUserInput(); // das fragt der Pr�fstand ab nach requestUserInputDigital(typ 2)
		assertThat(resultStr).isEqualTo("ok");
	}

	@Test
	public void test_requestUserInputDigital_typ3() throws Exception {
		Future<PruefAnweisung> fAnweisung = webClientWartetAufAnweisung(30);

		TEST_SCREEN.setProgress(25);
		TEST_SCREEN.setTeststep("Testschritt-XYZ");
		TEST_SCREEN.setShortVIN("ShortVIN-XYZ");
		TEST_SCREEN.setStatus(1);
		TEST_SCREEN.requestUserInputDigital("Anweisung", "Defrost Taster dr�cken", Handterminal.INPUT_TYPE_CANCEL);

		PruefAnweisung anweisung = fAnweisung.get();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.MESSAGE_WITH_CANCEL);
		assertThat(anweisung.getTitle()).isEqualTo("Anweisung");
		assertThat(anweisung.getMessage()).isEqualTo("Defrost Taster dr�cken");

		WEB_CLIENT.answer(anweisung.getId(), "MESSAGE_WITH_CANCEL", "nok");
		TEST_SCREEN.waitForCancel(); // das ruft der Pr�fstand auf nach requestUserInputDigital(typ 3)
	}

	@Test
	public void test_clearScreen() throws Exception {
		Future<PruefAnweisung> fAnweisung = webClientWartetAufAnweisung(30);

		TEST_SCREEN.setProgress(25);
		TEST_SCREEN.setTeststep("Testschritt-XYZ");
		TEST_SCREEN.setShortVIN("ShortVIN-XYZ");
		TEST_SCREEN.setStatus(1);
		TEST_SCREEN.clearScreen();

		PruefAnweisung anweisung = fAnweisung.get();
		assertThat(anweisung.getTyp()).isEqualTo(AnweisungTyp.CLEAR_SCREEN);
	}

	private PruefAnweisung webClientHoleAnweisung() throws Exception {
		String jsonStr = (String) WEB_CLIENT.waitForNextAnweisung(0, mockRequest()).getEntity();
		return jacksonSerialierer.readValue(jsonStr, PruefAnweisung.class);
	}

	private Future<PruefAnweisung> webClientWartetAufAnweisung(final int timeoutInSeconds) throws Exception {
		return threadPool.submit(new Callable<PruefAnweisung>() {
			@Override
			public PruefAnweisung call() throws Exception {
				String jsonStr = (String) WEB_CLIENT.waitForNextAnweisung(timeoutInSeconds, mockRequest()).getEntity();
				return jacksonSerialierer.readValue(jsonStr, PruefAnweisung.class);
			}
		});
	}

	private Request mockRequest() {
		Request mock = Mockito.mock(Request.class);
		WebHTUserInfo currentUser = WebHTSingleUserKeeper.getInstance(30).getCurrentUserInfo();
		if (currentUser != null) {
			Mockito.when(mock.getRemoteAddr()).thenReturn(currentUser.getIp());
			Mockito.when(mock.getHeader("User-Agent")).thenReturn(currentUser.getBrowserInfo());
		}
		return mock;
	}

}
